/******************************************************************************
*FILE         : oedt_Acousticin_TestFuncs.c
*
*SW-COMPONENT : ODT_FrmWrk
*
*DESCRIPTION  : This file implements the individual test cases for the
*               Acoustic device
*               Checking for valid pointer in each test cases is not required
*               as it is ensured in the calling function that that it cannot
*               be NULL.
*
*AUTHOR       : Bernd Schubart, 3SOFT
*
*COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
*
*HISTORY:       02.05.06  3SOFT-Schubart
*               Modified Acousticin Basic Tests, added new Acousticin testcases
*               25.07.05  3SOFT-Schubart37
*               Initial Revision.
*
*               26.04.07  RBIN/ECM-Soj Thomas
*               Adapted for OSAL Test for PND: Removed usage of Audio router,
*               Acousticin tests, NU_Sleep, MP3 tests, uses sds tests.
*               Removed function within UNUSED macro.

           27.01.2012 RBEI/ECM/ECF5-mem4kor
           Added following test cases:Capture + Play Test for stereo streams,
               simaltaneous capture and playback,simaltaneous
                capture and playback with different stream sizes,simaltaneous
                 capture and playback with different stream sizes 
                 with long duration.

               16-7-2012 RBEI/ECM/ECF5-mem4kor
         Error handler check test case  for acousticin component is added
               Initial Revision

               16-7-2013 RBEI/ECM/ECF5Z0-mdh4cob
               Fixed Prio2 lint warnings by disabling the functions that was
                not referenced.
*****************************************************************************/
#include <alsa/asoundlib.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"
#include "oedt_Display.h"
#include "oedt_Acousticin_TestFuncs.h"
#include "odt_Globals.h"
#include "odt_LinkedList.h"
#include "acousticout_public.h"
#include "acousticin_public.h"

extern const tU8 OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[];

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define OEDT_ACOUSTICIN_PRINTF_INFO(_X_) printf _X_
#define OEDT_ACOUSTICIN_PRINTF_ERROR(_X_) printf _X_
//#define OEDT_ACOUSTICIN_PRINTF_INFO(_X_) OEDT_ACOUSTICIN_HelperPrintf_Info _X_
//#define OEDT_ACOUSTICIN_PRINTF_ERROR(_X_) OEDT_ACOUSTICIN_HelperPrintf_Error _X_
//#define OEDT_ACOUSTICIN_NUMBER_OF_ARRAY_ELEMENTS(_X_) (sizeof(_X_) / sizeof((_X_)[0]))

/* open-string */
/* defines for PCM tests*/
#define C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH
#define C_U32_PCM_SAMPLERATE                 ((tU32)44100)
#define C_U32_PCM_ACIN_BUFFERSIZE            ((tU32)((2048)))
#define C_SZ_ACIN_PCMTESTFILE1                "/opt/bosch/test1.raw"


/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| prototypes (scope: module-local)
|-----------------------------------------------------------------------*/
static tVoid OEDT_ACOUSTICIN_HelperOsalError(void);



/*****************************************************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/*****************************************************************************/


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T000(void)
  *
  *  @brief         Prints only information about tests.
  *
  *  @param
  *
  *  @return   0
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T000(void)
{

  OEDT_ACOUSTICIN_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTICIN_T000\n"));
  OEDT_ACOUSTICIN_PRINTF_INFO(("For testing ACOUSTICIN you need a special /etc/asound.xxx.conf delivered by ECO\n"));
  OEDT_ACOUSTICIN_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));

  return 0UL;
}


/*****************************************************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/*****************************************************************************/
#define OEDT_ACOUSTICIN_T001_COUNT                        2
#define OEDT_ACOUSTICIN_T001_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithoutRate"
#define OEDT_ACOUSTICIN_T001_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithoutRate"


#define OEDT_ACOUSTICIN_T001_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T001_SAMPLE_RATE_OUT             48000
#define OEDT_ACOUSTICIN_T001_CHANNELS_IN                 1
#define OEDT_ACOUSTICIN_T001_CHANNELS_OUT                OEDT_ACOUSTICIN_T001_CHANNELS_IN
#define OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T001_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN
#define OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ             100


#define OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T001_OPEN_OUT_RESULT_ERROR_BIT               0x00000001
#define OEDT_ACOUSTICIN_T001_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000002
#define OEDT_ACOUSTICIN_T001_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T001_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T001_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICIN_T001_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000020
#define OEDT_ACOUSTICIN_T001_START_OUT_RESULT_ERROR_BIT              0x00000040
#define OEDT_ACOUSTICIN_T001_OPEN_IN_RESULT_ERROR_BIT                0x00000080

#define OEDT_ACOUSTICIN_T001_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000200
#define OEDT_ACOUSTICIN_T001_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T001_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T001_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00001000
#define OEDT_ACOUSTICIN_T001_START_IN_RESULT_ERROR_BIT               0x00002000
#define OEDT_ACOUSTICIN_T001_WRITE_RESULT_ERROR_BIT                  0x00004000
#define OEDT_ACOUSTICIN_T001_READ_RESULT_ERROR_BIT                   0x00008000

#define OEDT_ACOUSTICIN_T001_STOP_IN_RESULT_ERROR_BIT                0x04000000

#define OEDT_ACOUSTICIN_T001_CLOSE_IN_RESULT_ERROR_BIT               0x10000000
#define OEDT_ACOUSTICIN_T001_STOP_OUT_RESULT_ERROR_BIT               0x20000000
#define OEDT_ACOUSTICIN_T001_STOP_ACK_OUT_RESULT_ERROR_BIT           0x40000000
#define OEDT_ACOUSTICIN_T001_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000


static tBool OEDT_ACOUSTICIN_T001_bOutStopped = FALSE;

static tU8   OEDT_ACOUSTICIN_T001_u8PCMDoubleBuffer[OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ][OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN];






/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T001
*DESCRIPTION: used as device callback function for device test T001
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticInTstCallbackOut_T001 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICIN_T001_bOutStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n",\
                                 (unsigned int)rCbLastErrThr.enErrType, (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
    break;
  }
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T001(void)
  *
  *  @brief         Capture + Play Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T001(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin  = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T001(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T001_COUNT; iCount++)
  {
    /*OUT open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T001_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICIN_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T001_DEVICE_NAME_OUT, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T001_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /*OUT register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticInTstCallbackOut_T001;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));

      }
    }


    /*OUT configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T001_SAMPLE_RATE_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:"
                                     " SUCCESS SETSAMPLERATE OUT: %u\n",
                                     (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /*OUT configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICIN_T001_CHANNELS_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETCHANNELS_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
      }
    }

    /*OUT configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICIN_T001_BUFFERSIZE_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT start command */
    /*
          s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
          if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
              u32ResultBitMask |= OEDT_ACOUSTICIN_T001_START_OUT_RESULT_ERROR_BIT;
          }
          else //if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
          } //else //if(OSAL_OK != s32Ret)
    */
    /**********************************************************************************/
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /**********************************************************************************/

    /*IN open /dev/acousticin/speechreco */
    hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T001_DEVICE_NAME_IN, OSAL_EN_READONLY );
    if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T001_DEVICE_NAME_IN, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_OPEN_IN_RESULT_ERROR_BIT;
      hAcousticin = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T001_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
    } //else //if(OSAL_ERROR == hAcousticin)



    /*IN configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T001_SAMPLE_RATE_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:"
                                     "SUCCESS SETSAMPLERATE IN: %u\n",
                                     (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /*IN configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICIN_T001_CHANNELS_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin,
                                 OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETCHANNELS_IN_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
      }
    }

    /*IN configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*IN configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }


    /*IN start command */
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_START_IN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
    } //else //if(OSAL_OK != s32Ret)




    /*OUT start command */

    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_START_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
    } //else //if(OSAL_OK != s32Ret)




    // diretk echo (read write interleaved)
    //echoing pcm data from microphone  if no error occured before
    if(OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE == u32ResultBitMask)
    {
      //capture
      {
        tS32 s32ReadBytesTotal = 0;
        tS8 *ps8PCMIn          = NULL;
        int iCount2            = 0;
        int iBufferIndexIn     = 0;

        s32Ret = 1;
        while(iBufferIndexIn < OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ)
        {
          ps8PCMIn = (tS8*)OEDT_ACOUSTICIN_T001_u8PCMDoubleBuffer[iBufferIndexIn];
          iBufferIndexIn++;
          memset(ps8PCMIn, 0 , OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN);
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: READ OSAL_s32IORead\n"));
          s32Ret = OSAL_s32IORead( hAcousticin, ps8PCMIn, OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN );
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: READ OSAL_s32IORead s32Ret %d\n",(int)s32Ret));
          if(s32Ret <= 0)
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Read from AcousticIn %d\n", (int)s32Ret));
            u32ResultBitMask |= OEDT_ACOUSTICIN_T001_READ_RESULT_ERROR_BIT;
          }
          else //if(OSAL_OK != s32Ret)
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Read from AcousticIn %d (total %d, count %d)\n", (int)s32Ret, (int)s32ReadBytesTotal, iCount2));
            s32ReadBytesTotal += s32Ret;

            //debug: print some samples
                     #if 1
            {
              int i;
              OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: Samples: "));
              for(i = 0; i < 8; i++)
              {
                OEDT_ACOUSTICIN_PRINTF_INFO(("%04X ", (unsigned int)((unsigned short*)ps8PCMIn)[i]));
              } //for(i = 0; i < 16; i++)
              OEDT_ACOUSTICIN_PRINTF_INFO((" - "));
              for(i = 0; i < 8; i++)
              {
                OEDT_ACOUSTICIN_PRINTF_INFO(("%04X ", (unsigned int)((unsigned short*)&ps8PCMIn[OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN-18])[i]));
              } //for(i = 0; i < 16; i++)
              OEDT_ACOUSTICIN_PRINTF_INFO(("\n"));
            }
                     #endif //#if 0/1
          } //else //if(OSAL_OK != s32Ret)

          iCount2++;
        } //while
      } //capture block end

      //play buffers
      {
        tS8 *ps8PCMOut         = NULL;
        int iBufferIndexOut    = 0;
        while(iBufferIndexOut < OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ)
        {
          ps8PCMOut = (tS8*)OEDT_ACOUSTICIN_T001_u8PCMDoubleBuffer[iBufferIndexOut];
          iBufferIndexOut++;
          //write to acout
          int iWrite = OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN;
          s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCMOut, (tU32)iWrite);
          if(OSAL_ERROR == s32Ret)
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Write bytes (%d)\n", (int)iWrite));
            OEDT_ACOUSTICIN_HelperOsalError();
            u32ResultBitMask |= OEDT_ACOUSTICIN_T001_WRITE_RESULT_ERROR_BIT;
          }
          else
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Written Bytes (%d)\n", (int)iWrite));
          }
        } //while
      } //play block end


      /*IN  stop command */
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_STOP_IN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
      } //else //if(OSAL_OK != s32Ret)


      /*OUT  stop command */
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
      OEDT_ACOUSTICIN_T001_bOutStopped = FALSE;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T001_STOP_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICIN_T001_bOutStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICIN_T001_bOutStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICIN_T001_STOP_ACK_OUT_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE == u32ResultBitMask)


    /*OUT  close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)

    /*IN  close /dev/acousticout/speechreco */
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T001_CLOSE_IN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICIN_T001_COUNT; iCount++)


  if(OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T001 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICIN_T001_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICIN_T001(void)








/*****************************************************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICIN_T002_COUNT                        1
#define OEDT_ACOUSTICIN_T002_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithoutRate"
#define OEDT_ACOUSTICIN_T002_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithoutRate"


#define OEDT_ACOUSTICIN_T002_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T002_SAMPLE_RATE_OUT             44100 /*play slower, avoids buffer underruns*/
#define OEDT_ACOUSTICIN_T002_CHANNELS_IN                 1
#define OEDT_ACOUSTICIN_T002_CHANNELS_OUT                OEDT_ACOUSTICIN_T002_CHANNELS_IN
#define OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T002_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN
#define OEDT_ACOUSTICIN_T002_BUFFERS_TO_READ             254  /*tU8!-Counter*/


#define OEDT_ACOUSTICIN_T002_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T002_OPEN_OUT_RESULT_ERROR_BIT               0x00000001
#define OEDT_ACOUSTICIN_T002_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000002
#define OEDT_ACOUSTICIN_T002_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T002_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T002_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICIN_T002_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000020
#define OEDT_ACOUSTICIN_T002_START_OUT_RESULT_ERROR_BIT              0x00000040
#define OEDT_ACOUSTICIN_T002_OPEN_IN_RESULT_ERROR_BIT                0x00000080

#define OEDT_ACOUSTICIN_T002_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000200
#define OEDT_ACOUSTICIN_T002_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T002_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T002_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00001000
#define OEDT_ACOUSTICIN_T002_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT    0x00002000
#define OEDT_ACOUSTICIN_T002_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT   0x00004000
#define OEDT_ACOUSTICIN_T002_START_IN_RESULT_ERROR_BIT               0x00008000

#define OEDT_ACOUSTICIN_T002_CLOSE_IN_RESULT_ERROR_BIT               0x10000000

#define OEDT_ACOUSTICIN_T002_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000


static tBool OEDT_ACOUSTICIN_T002_bOutStopped = FALSE;

static tU8 OEDT_ACOUSTICIN_T002_u8PCMBufferArray[16][OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN];

static void OEDT_T002_vCaptureThread(void *pvData);
static void OEDT_T002_vPlaybackThread(void *pvData);


typedef struct OEDT_ACOUSTICIN_T002_ThreadData_tag
{
  OSAL_tIODescriptor hAcousticIn;
  OSAL_tIODescriptor hAcousticOut;
  volatile tU8 u8CaptureBufferIndex;
  volatile tU8 u8PlaybackBufferIndex;
  volatile tBool bCaptureThreadIsRunning;
  volatile tBool bPlaybackThreadIsRunning;
  volatile tBool bCaptureThreadForceEnd;
  volatile tBool bPlaybackThreadForceEnd;
}OEDT_ACOUSTICIN_T002_ThreadData_type;

static volatile OEDT_ACOUSTICIN_T002_ThreadData_type OEDT_T002_threadData;

static OSAL_tThreadID         OEDT_T002_captureThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T002_captureThreadAttr = {"OEDTT02c", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T002_vCaptureThread,
(void*)&OEDT_T002_threadData};
static OSAL_tThreadID         OEDT_T002_playbackThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T002_playbackThreadAttr = {"OEDTT02p", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T002_vPlaybackThread,
(void*)&OEDT_T002_threadData};



/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T002_vCaptureThread
  *
  *  @brief         Captures Data from AcousticIn
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 09.03.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static void OEDT_T002_vCaptureThread(void *pvData)
{
  int iCount;
  tS32 s32Ret;
  tS32 s32ReadBytesTotal = 0;
  OEDT_ACOUSTICIN_T002_ThreadData_type *pThreadData =
  (OEDT_ACOUSTICIN_T002_ThreadData_type*)pvData;
  tS8 *ps8PCM;
  //OSAL_tIODescriptor hAcousticout = *(OSAL_tIODescriptor*)pvData;
  //OSAL_trAcousticOutWaitEvent oWaitEvent;
  //oWaitEvent.nTimeout = OSAL_C_TIMEOUT_FOREVER;
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T002 CAPTURE THREAD START\n"));


  //ps8PCM = (tS8*)OEDT_ACOUSTICIN_T002_u8PCMBufferArray;
  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T002_BUFFERS_TO_READ; iCount++)
  {
    ps8PCM = (tS8*)OEDT_ACOUSTICIN_T002_u8PCMBufferArray[iCount%16];
    s32Ret = OSAL_s32IORead( pThreadData->hAcousticIn,
                             ps8PCM,
                             OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN );

    //OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: READ OSAL_s32IORead s32Ret %d\n",(int)s32Ret));
    if(s32Ret <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Read from AcousticIn %d\n",
                                   (int)s32Ret));
      //u32ResultBitMask |= OEDT_ACOUSTICIN_T002_READ_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "SUCCESS Read from AcousticIn "
                                   "%d (total %d, count %d)\n",
                                   (int)s32Ret,
                                   (int)s32ReadBytesTotal,
                                   iCount));
      s32ReadBytesTotal += s32Ret; //OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN;   //no partial reads can occur
      pThreadData->u8CaptureBufferIndex++;
    } //else //if(OSAL_OK != s32Ret)

    if(pThreadData->bCaptureThreadForceEnd)
    {
      break;
    } //if(pThreadData->bCaptureThreadForceEnd)
  } //for


  /*IN  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticIn, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
    //u32ResultBitMask |= OEDT_ACOUSTICIN_T002_STOP_IN_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
  } //else //if(OSAL_OK != s32Ret)

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T002 CAPTURE THREAD END\n"));
  //OSAL_vThreadExit();

  pThreadData->bCaptureThreadIsRunning  = FALSE;
}

/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T002_vPlaybackThread
  *
  *  @brief         play pcm data to AcousticOut
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 09.03.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static void OEDT_T002_vPlaybackThread(void *pvData)
{
  int iCount;
  tS32 s32Ret;
  OEDT_ACOUSTICIN_T002_ThreadData_type *pThreadData = (OEDT_ACOUSTICIN_T002_ThreadData_type*)pvData;
  tS8 *ps8PCM = NULL;
  //OSAL_trAcousticOutWaitEvent oWaitEvent;
  //oWaitEvent.nTimeout = OSAL_C_TIMEOUT_FOREVER;

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T002 PLAYBACK THREAD\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T002_BUFFERS_TO_READ; iCount++)
  {
    //wait until cature thread has filled buffers
    while((pThreadData->u8CaptureBufferIndex <= iCount)
          &&
          !(pThreadData->bPlaybackThreadForceEnd))
    {
      OSAL_s32ThreadWait(10);
    } //while(pThreadData->u8CaptureBufferIndex <= (tU8)iCount)

    if(pThreadData->bPlaybackThreadForceEnd)
    {
      break;
    } //if(pThreadData->bPlaybackThreadForceEnd)

    ps8PCM = (tS8*)OEDT_ACOUSTICIN_T002_u8PCMBufferArray[iCount%16];
    //write to acout
    s32Ret = OSAL_s32IOWrite(pThreadData->hAcousticOut, ps8PCM, (tU32)OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN);
    if(OSAL_ERROR == s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Write bytes (%d)\n",
                                   (int)OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN));
      OEDT_ACOUSTICIN_HelperOsalError();
      //u32ResultBitMask |= OEDT_ACOUSTICIN_T002_WRITE_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Written Bytes (%d), "
                                   "count %i\n",
                                   (int)OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN,
                                   iCount));
      pThreadData->u8PlaybackBufferIndex++;
    }
  }


  /*OUT  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
  OEDT_ACOUSTICIN_T002_bOutStopped = FALSE;
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticOut, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
    //u32ResultBitMask |= OEDT_ACOUSTICIN_T002_STOP_OUT_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
  } //else //if(OSAL_OK != s32Ret)

  //waiting for stop reply
  {
    int iEmergency = 50;
    while(!OEDT_ACOUSTICIN_T002_bOutStopped && (iEmergency-- > 0))
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
      OSAL_s32ThreadWait(100);
    } //while(!OEDT_ACOUSTICIN_T002_bOutStopped && iEmergency-- > 0)

    if(iEmergency <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
      //u32ResultBitMask |= OEDT_ACOUSTICIN_T002_STOP_ACK_OUT_RESULT_ERROR_BIT;
    }
    else //if(iEmergency <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
    } //else //if(iEmergency <= 0)
  }

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T002 PLAYBACK THREAD END\n"));
  pThreadData->bPlaybackThreadIsRunning = FALSE;
}


/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T002
*DESCRIPTION: used as device callback function for device test T002
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticInTstCallbackOut_T002 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICIN_T002_bOutStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n",\
                                 (unsigned int)rCbLastErrThr.enErrType, (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
    break;
  }
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T002(void)
  *
  *  @brief         Echo from Micro Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T002(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T002_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin  = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T002(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T002_COUNT; iCount++)
  {
    //clear audo buffers
    memset(OEDT_ACOUSTICIN_T002_u8PCMBufferArray, 0, sizeof(OEDT_ACOUSTICIN_T002_u8PCMBufferArray));

    /*OUT open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T002_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICIN_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T002_DEVICE_NAME_OUT, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T002_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /*OUT register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticInTstCallbackOut_T002;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));

      }
    }


    /*OUT configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T002_SAMPLE_RATE_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /*OUT configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICIN_T002_CHANNELS_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETCHANNELS_OUT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
      }
    }

    /*OUT configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICIN_T002_BUFFERSIZE_OUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT start command */
    /*
          s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
          if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
              u32ResultBitMask |= OEDT_ACOUSTICIN_T002_START_OUT_RESULT_ERROR_BIT;
          }
          else //if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
          } //else //if(OSAL_OK != s32Ret)
    */
    /**********************************************************************************/
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /*      IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN IN   */
    /**********************************************************************************/

    /*IN open /dev/acousticin/speechreco */
    hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T002_DEVICE_NAME_IN, OSAL_EN_READONLY );
    if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T002_DEVICE_NAME_IN, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_OPEN_IN_RESULT_ERROR_BIT;
      hAcousticin = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T002_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
    } //else //if(OSAL_ERROR == hAcousticin)



    /*IN configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T002_SAMPLE_RATE_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /*IN configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICIN_T002_CHANNELS_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS, (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETCHANNELS_IN_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
      }
    }

    /*IN configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /*IN configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICIN_T002_BUFFERSIZE_IN;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }


    /*IN start command */
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_START_IN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
    } //else //if(OSAL_OK != s32Ret)




    /*OUT start command */

    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_START_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
    } //else //if(OSAL_OK != s32Ret)




    OEDT_T002_threadData.hAcousticIn  = hAcousticin;
    OEDT_T002_threadData.hAcousticOut = hAcousticout;
    OEDT_T002_threadData.u8CaptureBufferIndex     = 0;
    OEDT_T002_threadData.u8PlaybackBufferIndex    = 0;
    OEDT_T002_threadData.bCaptureThreadIsRunning  = TRUE;
    OEDT_T002_threadData.bPlaybackThreadIsRunning = TRUE;
    OEDT_T002_threadData.bCaptureThreadForceEnd   = FALSE;
    OEDT_T002_threadData.bPlaybackThreadForceEnd  = FALSE;

    //run capture-thread
    {
      OEDT_T002_captureThreadID = OSAL_ERROR;
      OEDT_T002_captureThreadID = OSAL_ThreadSpawn(&OEDT_T002_captureThreadAttr);
      if(OSAL_ERROR == OEDT_T002_captureThreadID)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Capture Thread <%s> (count %d)\n",OEDT_T002_captureThreadAttr.szName, iCount));
        OEDT_ACOUSTICIN_HelperOsalError();
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT;
      }
      else //if(0 == OEDT_T002_captureThreadID)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Capture Thread <%s> (count %d)\n",OEDT_T002_captureThreadAttr.szName, iCount));
      } //else //if(0 == OEDT_T002_captureThreadID)
    }

    OSAL_s32ThreadWait(250);

    //run playback-thread
    {
      OEDT_T002_playbackThreadID = OSAL_ERROR;
      OEDT_T002_playbackThreadID = OSAL_ThreadSpawn(&OEDT_T002_playbackThreadAttr);
      if(OSAL_ERROR == OEDT_T002_playbackThreadID)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Playback Thread <%s> (count %d)\n",OEDT_T002_playbackThreadAttr.szName, iCount));
        OEDT_ACOUSTICIN_HelperOsalError();
        u32ResultBitMask |= OEDT_ACOUSTICIN_T002_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT;
      }
      else //if(0 == OEDT_T002_playbackThreadID)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Playback Thread <%s> (count %d)\n",OEDT_T002_playbackThreadAttr.szName, iCount));
      } //else //if(0 == OEDT_T002_playbackThreadID)
    }


    //wait for threads
    {
      int iEmergencyExit = 13; //wait 13secs
      while((
            OEDT_T002_threadData.bCaptureThreadIsRunning
            ||
            OEDT_T002_threadData.bPlaybackThreadIsRunning
            )
            &&
            (iEmergencyExit > 0)
           )
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 1, iEmergencyExit %d\n", iEmergencyExit));
        iEmergencyExit--;
        OSAL_s32ThreadWait(1000);
      }//while
    }

    OEDT_T002_threadData.bCaptureThreadForceEnd   = TRUE;
    OEDT_T002_threadData.bPlaybackThreadForceEnd  = TRUE;

    //wait for threads again
    {
      int iEmergencyExit = 5; //wait 5secs
      while((
            OEDT_T002_threadData.bCaptureThreadIsRunning
            ||
            OEDT_T002_threadData.bPlaybackThreadIsRunning
            )
            &&
            (iEmergencyExit > 0)
           )
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END iEmergencyExit %d\n", iEmergencyExit));
        iEmergencyExit--;
        OSAL_s32ThreadWait(1000);
      }//while
    }



    // delete Playback Thread if running yet
    if(OEDT_T002_threadData.bPlaybackThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T002_playbackThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T002_playbackThreadID);
        OEDT_T002_playbackThreadID = OSAL_ERROR;
      } //if(0 != OEDT_T002_playbackThreadID)

    }

    // delete Cature Thread if running yet
    if(OEDT_T002_threadData.bCaptureThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T002_captureThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T002_captureThreadID);
        OEDT_T002_captureThreadID = OSAL_ERROR;
      } //if(0 != OEDT_T002_threadID)

    }


    /*OUT  close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)

    /*IN  close /dev/acousticout/speechreco */
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T002_CLOSE_IN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(250);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICIN_T002_COUNT; iCount++)


  if(OEDT_ACOUSTICIN_T002_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T002 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICIN_T002_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(250);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICIN_T002(void)

static tVoid OEDT_ACOUSTICIN_HelperOsalError(void)
{
  tCString pcszErrorTxt;
  tU32 u32ErrorCode;
  u32ErrorCode = OSAL_u32ErrorCode();
  pcszErrorTxt = OSAL_coszErrorText(u32ErrorCode);


  if(pcszErrorTxt)
  {
    printf("OSAL_ERROR: <0x%08X> <%s>\n",  (unsigned int)u32ErrorCode, (const char*)pcszErrorTxt);
  }
  else //if(pcszErrorTxt)
  {
    printf("OSAL_ERROR: <0x%08X>\n",  (unsigned int)u32ErrorCode);
  } //else //if(pcszErrorTxt)
}


/*****************************************************************************/
/************************* TEST 003 ******************************************/
/*************************OEDT_ACOUSTICIN_T003******************************/
/**********************Capture + Play Test for stereo streams**************************/
#define OEDT_ACOUSTICIN_T003_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithFile"
#define OEDT_ACOUSTICIN_T003_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithFile"

#define OEDT_ACOUSTICIN_T003_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T003_SAMPLE_RATE_OUT             48000
#define OEDT_ACOUSTICIN_T003_CHANNELS_IN                 2
#define OEDT_ACOUSTICIN_T003_CHANNELS_OUT                OEDT_ACOUSTICIN_T003_CHANNELS_IN
#define OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T003_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN
#define OEDT_ACOUSTICIN_T003_BUFFERS_TO_READ             100

#define OEDT_ACOUSTICIN_T003_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T003_OPEN_OUT_RESULT_ERROR_BIT               0x00000003
#define OEDT_ACOUSTICIN_T003_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000002
#define OEDT_ACOUSTICIN_T003_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T003_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T003_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000030
#define OEDT_ACOUSTICIN_T003_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000020
#define OEDT_ACOUSTICIN_T003_START_OUT_RESULT_ERROR_BIT              0x00000040

#define OEDT_ACOUSTICIN_T003_OPEN_IN_RESULT_ERROR_BIT                0x00000080
#define OEDT_ACOUSTICIN_T003_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000200
#define OEDT_ACOUSTICIN_T003_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T003_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T003_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00003000
#define OEDT_ACOUSTICIN_T003_START_IN_RESULT_ERROR_BIT               0x00002000
#define OEDT_ACOUSTICIN_T003_WRITE_RESULT_ERROR_BIT                  0x00004000
#define OEDT_ACOUSTICIN_T003_READ_RESULT_ERROR_BIT                   0x00008000
#define OEDT_ACOUSTICIN_T003_STOP_IN_RESULT_ERROR_BIT                0x04000000
#define OEDT_ACOUSTICIN_T003_CLOSE_IN_RESULT_ERROR_BIT               0x10000000

#define OEDT_ACOUSTICIN_T003_STOP_OUT_RESULT_ERROR_BIT               0x20000000
#define OEDT_ACOUSTICIN_T003_STOP_ACK_OUT_RESULT_ERROR_BIT           0x40000000
#define OEDT_ACOUSTICIN_T003_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000

/*Turn this on to compare the captured and played out files */
#define OEDT_ACOUSTICIN_T003_LOOPBACK_FILE_COMPARE                   1

static tBool OEDT_ACOUSTICIN_bOutStopped = FALSE;
static tU8   OEDT_ACOUSTICIN_T003_u8PCMDoubleBuffer[OEDT_ACOUSTICIN_T003_BUFFERS_TO_READ][OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN];

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback
*DESCRIPTION: used as device callback function for device test T003,T004,T005,T006.
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     24-1-2012, mem4kor
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticOutTstCallback (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICIN_bOutStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
    break;
  }
}



/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T003(void)
  *
  *  @brief         Capture + Play Test for stereo streams
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 24-1-2012, mem4kor
  *    Initial revision.

  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T003(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T003_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout ;
  OSAL_tIODescriptor hAcousticin  ;
  tS32 s32Ret;
  tS32 iCount=0;
  OSAL_trAcousticOutCallbackReg rCallbackReg;
  tU32 u32Channels;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS32 iEmergency;
  tS32 s32ReadBytesTotal = 0;
  tS8 *ps8PCMIn     = NULL;
  tS32 iCount2       = 0;
  tS32 iBufferIndexIn  = 0;


#if OEDT_ACOUSTICIN_T003_LOOPBACK_FILE_COMPARE // Turn this on to compare the captured and played out files
  FILE *fp_acin;
  FILE *fp_acout;
  char var_acin=0, var_acout=0;
#endif

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T003(void)\n"));

  /*OUT open /dev/acousticout/speech */
  hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T003_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);

  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T003_DEVICE_NAME_OUT, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_OPEN_OUT_RESULT_ERROR_BIT;
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T003_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
  }

  /*OUT register callback function */
  rCallbackReg.pfEvCallback = vAcousticOutTstCallback; /* common call back function  */
  rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));

  }

  /*OUT configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T003_SAMPLE_RATE_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:"
                                 " SUCCESS SETSAMPLERATE OUT: %u\n",
                                 (unsigned int)rSampleRateCfg.nSamplerate));
  }

  /*OUT configure channels */
  u32Channels = OEDT_ACOUSTICIN_T003_CHANNELS_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                             (tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETCHANNELS_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
  }

  /*OUT configure sample format */
  rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }

  /*OUT configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T003_BUFFERSIZE_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
  }

  /*IN open /dev/acousticin/speechreco */
  hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T003_DEVICE_NAME_IN, OSAL_EN_READONLY );
  if(OSAL_ERROR == hAcousticin)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T003_DEVICE_NAME_IN, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_OPEN_IN_RESULT_ERROR_BIT;
    hAcousticin = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T003_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
  }

  /*IN configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T003_SAMPLE_RATE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:"
                                 "SUCCESS SETSAMPLERATE IN: %u\n",
                                 (unsigned int)rSampleRateCfg.nSamplerate));
  }

  /*IN configure channels */
  u32Channels = OEDT_ACOUSTICIN_T003_CHANNELS_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin,
                             OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,
                             (tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETCHANNELS_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
  }

  /*IN configure sample format */
  rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }

  /*IN configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
  }

  /*IN start command */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_START_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
  }

  /*OUT start command */
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_START_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
  }

  //echoing pcm data from microphone  if no error occured before
  if(OEDT_ACOUSTICIN_T003_RESULT_OK_VALUE == u32ResultBitMask)
  {
    /*capture       */
    s32Ret = 1;

    while(iBufferIndexIn < OEDT_ACOUSTICIN_T003_BUFFERS_TO_READ)
    {
      ps8PCMIn = (tS8*)OEDT_ACOUSTICIN_T003_u8PCMDoubleBuffer[iBufferIndexIn];
      iBufferIndexIn++;
      memset(ps8PCMIn, 0 , OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN);
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: READ OSAL_s32IORead\n"));
      s32Ret = OSAL_s32IORead( hAcousticin, ps8PCMIn, OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN );
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: READ OSAL_s32IORead s32Ret %d\n",(tS32)s32Ret));
      if(s32Ret <= 0)
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Read from AcousticIn %d\n", (tS32)s32Ret));
        u32ResultBitMask |= OEDT_ACOUSTICIN_T003_READ_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Read from AcousticIn %d (total %d, count %d)\n", (tS32)s32Ret, (tS32)s32ReadBytesTotal, iCount2));
        s32ReadBytesTotal += s32Ret;

        //debug: print some samples for easy debugging
        {
          tS32 i;
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: Samples: "));
          for(i = 0; i < 8; i++)
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("%04X ", (unsigned int)((unsigned short*)ps8PCMIn)[i]));
          }
          OEDT_ACOUSTICIN_PRINTF_INFO((" - "));
          for(i = 0; i < 8; i++)
          {
            OEDT_ACOUSTICIN_PRINTF_INFO(("%04X ", (unsigned int)((unsigned short*)&ps8PCMIn[OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN-18])[i]));
          }
          OEDT_ACOUSTICIN_PRINTF_INFO(("\n"));
        }
      }

      iCount2++;
    }
    //capture block end

    //play buffers
    {
      tS8 *ps8PCMOut         = NULL;
      tS32 iBufferIndexOut    = 0;
      while(iBufferIndexOut < OEDT_ACOUSTICIN_T003_BUFFERS_TO_READ)
      {
        ps8PCMOut = (tS8*)OEDT_ACOUSTICIN_T003_u8PCMDoubleBuffer[iBufferIndexOut];
        iBufferIndexOut++;
        //write to acout
        tS32 iWrite = OEDT_ACOUSTICIN_T003_BUFFERSIZE_IN;
        s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCMOut, (tU32)iWrite);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Write bytes (%d)\n", (tS32)iWrite));
          OEDT_ACOUSTICIN_HelperOsalError();
          u32ResultBitMask |= OEDT_ACOUSTICIN_T003_WRITE_RESULT_ERROR_BIT;
        }
        else
        {
          OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Written Bytes (%d)\n", (tS32)iWrite));
        }
      } //while
    } //play block end
    /*IN  stop command */
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T003_STOP_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
    }

    /*OUT  stop command */
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
    OEDT_ACOUSTICIN_bOutStopped = FALSE;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T003_STOP_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
    }

    //waiting for stop reply
    iEmergency = 50; //waiting time to stop the device
    while(!OEDT_ACOUSTICIN_bOutStopped && (iEmergency > 0))
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
      OSAL_s32ThreadWait(100);
      iEmergency = iEmergency - 1;
    }

    if(iEmergency <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T003_STOP_ACK_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
    }
  }


  /*OUT  close /dev/acousticout/speech */
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_CLOSE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
  }

  /*IN  close /dev/acousticout/speechreco */
  s32Ret = OSAL_s32IOClose(hAcousticin);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T003_CLOSE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
  }
  OSAL_s32ThreadWait(1000);
  if(OEDT_ACOUSTICIN_T003_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T003 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  }

  /*mem4kor: Binary comparison of captured data and playback data */
  /* Turn this on to compare the captured and played out files */
#if OEDT_ACOUSTICIN_T003_LOOPBACK_FILE_COMPARE
  fp_acin = fopen("/var/tmp/capture.raw","rb");
  fp_acout = fopen("/var/tmp/playback.raw","rb");
  if((fp_acout)&&(fp_acin))
  {
    do
    {
      fread(&var_acin, sizeof(char),1,fp_acin);
      fread(&var_acout, sizeof(char),1,fp_acout);
      if(var_acin != var_acout)
      {
        OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: CAPTURED data and PLAYBACK data are different  \n"));
      }
      if((feof( fp_acin) )|| (feof(fp_acout)))
      {
        break;
      }
    }while(var_acin == var_acout);

    /*We make sure whether the loop exited due to data mis match or due to End of data file*/
    if(var_acin == var_acout)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: CAPTURED data and PLAYBACK data are Same  \n"));
    }

    fclose(fp_acin);
    fclose(fp_acout);

  }
#endif
  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;

}


/*****************************************************************************/
/************************* TEST 004 ******************************************/
/*************************** OEDT_ACOUSTICIN_T004 ****************************/
/*************************simaltaneous capture and playback***********************************/
#define OEDT_ACOUSTICIN_T004_COUNT                        1
#define OEDT_ACOUSTICIN_T004_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithFile"
#define OEDT_ACOUSTICIN_T004_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithFile"


#define OEDT_ACOUSTICIN_T004_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T004_SAMPLE_RATE_OUT             48000 /*play slower, avoids buffer underruns*/
#define OEDT_ACOUSTICIN_T004_CHANNELS_IN                 2
#define OEDT_ACOUSTICIN_T004_CHANNELS_OUT                OEDT_ACOUSTICIN_T004_CHANNELS_IN
#define OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T004_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN
#define OEDT_ACOUSTICIN_T004_BUFFERS_TO_READ             32  /*tU8!-Counter*/


#define OEDT_ACOUSTICIN_T004_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T004_OPEN_OUT_RESULT_ERROR_BIT               0x00000001
#define OEDT_ACOUSTICIN_T004_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000004
#define OEDT_ACOUSTICIN_T004_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T004_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T004_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICIN_T004_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000040
#define OEDT_ACOUSTICIN_T004_START_OUT_RESULT_ERROR_BIT              0x00000040
#define OEDT_ACOUSTICIN_T004_OPEN_IN_RESULT_ERROR_BIT                0x00000080

#define OEDT_ACOUSTICIN_T004_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000400
#define OEDT_ACOUSTICIN_T004_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T004_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T004_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00001000
#define OEDT_ACOUSTICIN_T004_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT    0x00004000
#define OEDT_ACOUSTICIN_T004_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT   0x00004000
#define OEDT_ACOUSTICIN_T004_START_IN_RESULT_ERROR_BIT               0x00008000

#define OEDT_ACOUSTICIN_T004_CLOSE_IN_RESULT_ERROR_BIT               0x10000000

#define OEDT_ACOUSTICIN_T004_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000
#define OEDT_ACOUSTICIN_T004_WRITE_LOOP_COUNT                        32
#define OEDT_ACOUSTICIN_T004_NUMBEROF_BYTES_READ                     4096

static tU8 OEDT_ACOUSTICIN_T004_u8PCMBufferArray[OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN*OEDT_ACOUSTICIN_T004_BUFFERS_TO_READ];

static void OEDT_T004_vCaptureThread(void *pvData);
static void OEDT_T004_vPlaybackThread(void *pvData);

typedef struct OEDT_ACOUSTICIN_T004_ThreadData_tag
{
  OSAL_tIODescriptor hAcousticIn;
  OSAL_tIODescriptor hAcousticOut;
  volatile tU8 u8CaptureBufferIndex;
  volatile tU8 u8PlaybackBufferIndex;
  volatile tBool bCaptureThreadIsRunning;
  volatile tBool bPlaybackThreadIsRunning;
  volatile tBool bCaptureThreadForceEnd;
  volatile tBool bPlaybackThreadForceEnd;
}OEDT_ACOUSTICIN_T004_ThreadData_type;

static volatile OEDT_ACOUSTICIN_T004_ThreadData_type OEDT_T004_threadData;

static OSAL_tThreadID         OEDT_T004_captureThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T004_captureThreadAttr = {"OEDTT02c", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T004_vCaptureThread,
(void*)&OEDT_T004_threadData};
static OSAL_tThreadID         OEDT_T004_playbackThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T004_playbackThreadAttr = {"OEDTT02p", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T004_vPlaybackThread,
(void*)&OEDT_T004_threadData};

/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T004_vCaptureThread
  *
  *  @brief         Captures Data from AcousticIn
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 25-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T004_vCaptureThread(void *pvData)
{
  tS32 iCount ;
  tS32 s32Ret = 0;
  tS32 s32ReadBytesTotal = 0;
  OEDT_ACOUSTICIN_T004_ThreadData_type *pThreadData =
  (OEDT_ACOUSTICIN_T004_ThreadData_type*)pvData;
  tS8 *ps8PCM ;

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T004 CAPTURE THREAD START\n"));

  ps8PCM = (tS8*)&OEDT_ACOUSTICIN_T004_u8PCMBufferArray[0];

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T004_BUFFERS_TO_READ; iCount++)
  {
    s32Ret = OSAL_s32IORead( pThreadData->hAcousticIn,
                             ps8PCM,
                             OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN );
    ps8PCM = ps8PCM + OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN;

    if(s32Ret <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Read from AcousticIn %d\n",
                                   (tS32)s32Ret));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "SUCCESS Read from AcousticIn "
                                   "%d (total %d, count %d)\n",
                                   (tS32)s32Ret,
                                   (tS32)s32ReadBytesTotal,
                                   iCount));
      s32ReadBytesTotal += s32Ret; //OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN;   //no partial reads can occur
      pThreadData->u8CaptureBufferIndex++;
    }

    if(pThreadData->bCaptureThreadForceEnd)
    {
      break;
    }
  } //for
  /*IN  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticIn, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
  }
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T004 CAPTURE THREAD END\n"));
  pThreadData->bCaptureThreadIsRunning  = FALSE;
}

/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T004_vPlaybackThread
  *
  *  @brief         play pcm data to AcousticOut
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 25-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T004_vPlaybackThread(void *pvData)
{
  tS32 s32Ret = 0;
  tU32 iReadLen ;
  tS32 Writecount= 0;
  OEDT_ACOUSTICIN_T004_ThreadData_type *pThreadData = (OEDT_ACOUSTICIN_T004_ThreadData_type*)pvData;
  tS8 *ps8PCM ;
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T004 PLAYBACK THREAD\n"));
  ps8PCM = (tS8 *)&OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[0];
  iReadLen = OEDT_ACOUSTICIN_T004_NUMBEROF_BYTES_READ;
  tS32 iEmergency ;

  while(Writecount < OEDT_ACOUSTICIN_T004_WRITE_LOOP_COUNT)
  {
    s32Ret = OSAL_s32IOWrite(pThreadData->hAcousticOut, ps8PCM, (tU32)OEDT_ACOUSTICIN_T004_BUFFERSIZE_OUT);
    Writecount++;
    ps8PCM = ps8PCM + OEDT_ACOUSTICIN_T004_BUFFERSIZE_OUT;

    if(OSAL_ERROR == s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Aout Write bytes (%u)  \n", (unsigned int)iReadLen ));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n",  (unsigned int)iReadLen));
    }
  }

  /*OUT  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
  OEDT_ACOUSTICIN_bOutStopped = FALSE;
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticOut, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
  }

  //waiting for stop reply
  iEmergency = 50; //waiting time to stop device
  while(!OEDT_ACOUSTICIN_bOutStopped && (iEmergency > 0))
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
    OSAL_s32ThreadWait(100);
    iEmergency--;
  }
  if(iEmergency <= 0)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
  }
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T004 PLAYBACK THREAD END\n"));
  pThreadData->bPlaybackThreadIsRunning = FALSE;
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T004(void)
  *
  *  @brief         Simultaneous Capture and Playout
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 25-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T004(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T004_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin  = OSAL_ERROR;
  tS32 s32Ret = 0;
  tS32  iCount ;
  OSAL_trAcousticOutCallbackReg rCallbackReg;
  OSAL_trAcousticSampleRateCfg  rSampleRateCfg;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  tU32 u32Channels;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS32 iEmergencyExit;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T004(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T004_COUNT; iCount++)
  {
    //clear audo buffers
    memset(OEDT_ACOUSTICIN_T004_u8PCMBufferArray, 0, sizeof(OEDT_ACOUSTICIN_T004_u8PCMBufferArray));

    /*OUT open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T004_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);

    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T004_DEVICE_NAME_OUT, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T004_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
    }

    /*OUT register callback function */
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback; /* common callback function */
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));
    }

    /*OUT configure sample rate */
    rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T004_SAMPLE_RATE_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    }

    /*OUT configure channels */
    u32Channels = OEDT_ACOUSTICIN_T004_CHANNELS_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                               (tS32)u32Channels);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETCHANNELS_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
    }


    /*OUT configure sample format */
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    }

    /*OUT configure buffersize */
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICIN_T004_BUFFERSIZE_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
    }


    /*IN open /dev/acousticin/speechreco */
    hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T004_DEVICE_NAME_IN, OSAL_EN_READONLY );
    if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T004_DEVICE_NAME_IN, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_OPEN_IN_RESULT_ERROR_BIT;
      hAcousticin = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T004_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
    }

    /*IN configure sample rate */
    rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T004_SAMPLE_RATE_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    }


    /*IN configure channels */
    u32Channels = OEDT_ACOUSTICIN_T004_CHANNELS_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS, (tS32)u32Channels);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETCHANNELS_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
    }

    /*IN configure sample format */
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    }

    /*IN configure buffersize */
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICIN_T004_BUFFERSIZE_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
    }


    /*IN start command */
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_START_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
    }

    /*OUT start command */
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_START_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
    }

    OEDT_T004_threadData.hAcousticIn  = hAcousticin;
    OEDT_T004_threadData.hAcousticOut = hAcousticout;
    OEDT_T004_threadData.u8CaptureBufferIndex     = 0;
    OEDT_T004_threadData.u8PlaybackBufferIndex    = 0;
    OEDT_T004_threadData.bCaptureThreadIsRunning  = TRUE;
    OEDT_T004_threadData.bPlaybackThreadIsRunning = TRUE;
    OEDT_T004_threadData.bCaptureThreadForceEnd   = FALSE;
    OEDT_T004_threadData.bPlaybackThreadForceEnd  = FALSE;

    //run capture-thread
    OEDT_T004_captureThreadID = OSAL_ERROR;
    OEDT_T004_captureThreadID = OSAL_ThreadSpawn(&OEDT_T004_captureThreadAttr);
    if(OSAL_ERROR == OEDT_T004_captureThreadID)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Capture Thread <%s> (count %d)\n",OEDT_T004_captureThreadAttr.szName, iCount));
      OEDT_ACOUSTICIN_HelperOsalError();
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Capture Thread <%s> (count %d)\n",OEDT_T004_captureThreadAttr.szName, iCount));
    }
    OSAL_s32ThreadWait(250);

    //run playback-thread
    OEDT_T004_playbackThreadID = OSAL_ERROR;
    OEDT_T004_playbackThreadID = OSAL_ThreadSpawn(&OEDT_T004_playbackThreadAttr);
    if(OSAL_ERROR == OEDT_T004_playbackThreadID)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Playback Thread <%s> (count %d)\n",OEDT_T004_playbackThreadAttr.szName, iCount));
      OEDT_ACOUSTICIN_HelperOsalError();
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Playback Thread <%s> (count %d)\n",OEDT_T004_playbackThreadAttr.szName, iCount));
    }

    //wait for threads
    iEmergencyExit = 13; //wait 13secs
    while((
          OEDT_T004_threadData.bCaptureThreadIsRunning
          ||
          OEDT_T004_threadData.bPlaybackThreadIsRunning
          )
          &&
          (iEmergencyExit > 0)
         )
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 1, iEmergencyExit %d\n", iEmergencyExit));
      iEmergencyExit--;
      OSAL_s32ThreadWait(1000);
    }//while

    OEDT_T004_threadData.bCaptureThreadForceEnd   = TRUE;
    OEDT_T004_threadData.bPlaybackThreadForceEnd  = TRUE;

    //wait for threads again
    iEmergencyExit = 5; //wait 5secs
    while((
          OEDT_T004_threadData.bCaptureThreadIsRunning
          ||
          OEDT_T004_threadData.bPlaybackThreadIsRunning
          )
          &&
          (iEmergencyExit > 0)
         )
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END iEmergencyExit %d\n", iEmergencyExit));
      iEmergencyExit--;
      OSAL_s32ThreadWait(1000);
    }//while

    // delete Playback Thread if running yet
    if(OEDT_T004_threadData.bPlaybackThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T004_playbackThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T004_playbackThreadID);
        OEDT_T004_playbackThreadID = OSAL_ERROR;
      } //if(0 != OEDT_T004_playbackThreadID)
    }

    // delete Cature Thread if running yet
    if(OEDT_T004_threadData.bCaptureThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T004_captureThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T004_captureThreadID);
        OEDT_T004_captureThreadID = OSAL_ERROR;
      } //if(0 != OEDT_T004_threadID)
    }

    /*OUT  close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    }

    /*IN  close /dev/acousticout/speechreco */
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T004_CLOSE_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    }
    OSAL_s32ThreadWait(250);
  }


  if(OEDT_ACOUSTICIN_T004_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T004 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  }
  OSAL_s32ThreadWait(250);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICIN_T004(void)

/*****************************************************************************/
/************************* TEST 005 ******************************************/
/************************* OEDT_ACOUSTICIN_T005 ******************************/
/**********simaltaneous capture and playback with different stream sizes******/
#define OEDT_ACOUSTICIN_T005_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithFile"
#define OEDT_ACOUSTICIN_T005_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithFile"


#define OEDT_ACOUSTICIN_T005_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T005_SAMPLE_RATE_OUT             48000
#define OEDT_ACOUSTICIN_T005_CHANNELS_IN                 2
#define OEDT_ACOUSTICIN_T005_CHANNELS_OUT                OEDT_ACOUSTICIN_T005_CHANNELS_IN
#define OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN



#define OEDT_ACOUSTICIN_T005_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T005_OPEN_OUT_RESULT_ERROR_BIT               0x00000001
#define OEDT_ACOUSTICIN_T005_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000005
#define OEDT_ACOUSTICIN_T005_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T005_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T005_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICIN_T005_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000050
#define OEDT_ACOUSTICIN_T005_START_OUT_RESULT_ERROR_BIT              0x00000040
#define OEDT_ACOUSTICIN_T005_OPEN_IN_RESULT_ERROR_BIT                0x00000080

#define OEDT_ACOUSTICIN_T005_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000500
#define OEDT_ACOUSTICIN_T005_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T005_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T005_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00001000
#define OEDT_ACOUSTICIN_T005_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT    0x00005000
#define OEDT_ACOUSTICIN_T005_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT   0x00004000
#define OEDT_ACOUSTICIN_T005_START_IN_RESULT_ERROR_BIT               0x00008000

#define OEDT_ACOUSTICIN_T005_CLOSE_IN_RESULT_ERROR_BIT               0x10000000

#define OEDT_ACOUSTICIN_T005_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000

#define Playback_Array_Size                                          264600
#define OEDT_ACOUSTICIN_T005_LOOP_COUNT                              48

static tU8 OEDT_ACOUSTICIN_T005_u8PCMBufferArray[OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN*64];

static void OEDT_T005_vCaptureThread(void *pvData);
static void OEDT_T005_vPlaybackThread(void *pvData);


typedef struct OEDT_ACOUSTICIN_T005_ThreadData_tag
{
  OSAL_tIODescriptor hAcousticIn;
  OSAL_tIODescriptor hAcousticOut;
  volatile tU8 u8CaptureBufferIndex;
  volatile tU8 u8PlaybackBufferIndex;
  volatile tBool bCaptureThreadIsRunning;
  volatile tBool bPlaybackThreadIsRunning;
  volatile tBool bCaptureThreadForceEnd;
  volatile tBool bPlaybackThreadForceEnd;
}OEDT_ACOUSTICIN_T005_ThreadData_type;

static volatile OEDT_ACOUSTICIN_T005_ThreadData_type OEDT_T005_threadData;

static OSAL_tThreadID         OEDT_T005_captureThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T005_captureThreadAttr = {"OEDTT02c", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T005_vCaptureThread,
(void*)&OEDT_T005_threadData};
static OSAL_tThreadID         OEDT_T005_playbackThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T005_playbackThreadAttr = {"OEDTT02p", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T005_vPlaybackThread,
(void*)&OEDT_T005_threadData};

/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T005_vCaptureThread
  *
  *  @brief         Captures Data from AcousticIn
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 26-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T005_vCaptureThread(void *pvData)
{
  tS32 iCount, icapturelength;
  tS32 s32Ret;
  tS32 s32ReadBytesTotal = 0;
  OEDT_ACOUSTICIN_T005_ThreadData_type *pThreadData =
  (OEDT_ACOUSTICIN_T005_ThreadData_type*)pvData;
  tS8 *ps8PCM ;
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T005 CAPTURE THREAD START\n"));
  ps8PCM = (tS8*)&OEDT_ACOUSTICIN_T005_u8PCMBufferArray[0];
  icapturelength = 0;

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T005_LOOP_COUNT; iCount++)
  {
    if(iCount < 32)
    {
      icapturelength = OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN;
    }
    else
    {
      icapturelength = 2*OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN;
    }
    s32Ret = OSAL_s32IORead( pThreadData->hAcousticIn,
                             ps8PCM,
                             (unsigned long)icapturelength );
    ps8PCM = ps8PCM + icapturelength;
    if(s32Ret <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Read from AcousticIn %d\n",
                                   (tS32)s32Ret));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "SUCCESS Read from AcousticIn "
                                   "%d (total %d, count %d)\n",
                                   (tS32)s32Ret,
                                   (tS32)s32ReadBytesTotal,
                                   iCount));
      s32ReadBytesTotal += s32Ret; //OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN;   //no partial reads can occur
      pThreadData->u8CaptureBufferIndex++;
    }
    if(pThreadData->bCaptureThreadForceEnd)
    {
      break;
    }
  } //for


  /*IN  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticIn, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
  }
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T005 CAPTURE THREAD END\n"));
  pThreadData->bCaptureThreadIsRunning  = FALSE;
}

/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T005_vPlaybackThread
  *
  *  @brief         play pcm data to AcousticOut
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 26-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T005_vPlaybackThread(void *pvData)
{
  tS32 s32Ret;
  tU32 iReadLen=0, BytesWritten=0, BytesRamaining = 0;
  tS32 Writecount=0;
  tS8 *ps8PCM ;
  tS32 iEmergency;
  OEDT_ACOUSTICIN_T005_ThreadData_type *pThreadData = (OEDT_ACOUSTICIN_T005_ThreadData_type*)pvData;
  ps8PCM = (tS8 *)&OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[0];

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T005 PLAYBACK THREAD\n"));
  while(BytesWritten < Playback_Array_Size)
  {
    BytesRamaining = Playback_Array_Size -  BytesWritten;

    if(Writecount%4 == 0)
      iReadLen = OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT/4;
    else if(Writecount%4 == 1)
      iReadLen = OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT/2;
    else if(Writecount%4 == 2)
      iReadLen = OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT;
    else if(Writecount%4 == 3)
      iReadLen = 2*OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT;
    if(BytesRamaining < iReadLen)
      break;
    s32Ret = OSAL_s32IOWrite(pThreadData->hAcousticOut, ps8PCM, (tU32)iReadLen);
    Writecount++;
    ps8PCM = ps8PCM + iReadLen;
    BytesWritten += iReadLen;
    if(OSAL_ERROR == s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Aout Write bytes (%u)  \n", (unsigned int) iReadLen ));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n",  (unsigned int)iReadLen));
    }
  }

  /*OUT  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
  OEDT_ACOUSTICIN_bOutStopped = FALSE;
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticOut, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
  }

  //waiting for stop reply
  iEmergency = 50; //waiting time to stop device
  while(!OEDT_ACOUSTICIN_bOutStopped && (iEmergency > 0))
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
    OSAL_s32ThreadWait(100);
    iEmergency--;
  }
  if(iEmergency <= 0)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
  }
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T005 PLAYBACK THREAD END\n"));
  pThreadData->bPlaybackThreadIsRunning = FALSE;
}
/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T005(void)
  *
  *  @brief         Simultaneous Capture and Playout with different stream sizes
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 26-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T005(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T005_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout ;
  OSAL_tIODescriptor hAcousticin ;
  tS32 s32Ret ;
  tS32  iCount = 0;
  OSAL_trAcousticOutCallbackReg rCallbackReg;
  OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  tU32 u32Channels;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS32 iEmergencyExit;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T005(void)\n"));


  //clear audo buffers
  memset(OEDT_ACOUSTICIN_T005_u8PCMBufferArray, 0, sizeof(OEDT_ACOUSTICIN_T005_u8PCMBufferArray));

  /*OUT open /dev/acousticout/speech */
  hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T005_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T005_DEVICE_NAME_OUT, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_OPEN_OUT_RESULT_ERROR_BIT;
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T005_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
  }

  /*OUT register callback function */
  rCallbackReg.pfEvCallback = vAcousticOutTstCallback; /* common callback function */
  rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));
  }

  /*OUT configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T005_SAMPLE_RATE_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
  }

  /*OUT configure channels */
  u32Channels = OEDT_ACOUSTICIN_T005_CHANNELS_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                             (tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETCHANNELS_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
  }

  /*OUT configure sample format */
  rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }

  /*OUT configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T005_BUFFERSIZE_OUT;
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
  }


  /*IN open /dev/acousticin/speechreco */
  hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T005_DEVICE_NAME_IN, OSAL_EN_READONLY );
  if(OSAL_ERROR == hAcousticin)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T005_DEVICE_NAME_IN, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_OPEN_IN_RESULT_ERROR_BIT;
    hAcousticin = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T005_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
  }

  /*IN configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T005_SAMPLE_RATE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
  }

  /*IN configure channels */
  u32Channels = OEDT_ACOUSTICIN_T005_CHANNELS_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS, (tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETCHANNELS_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
  }

  /*IN configure sample format */
  rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }

  /*IN configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T005_BUFFERSIZE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
  }


  /*IN start command */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_START_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
  }

  /*OUT start command */
  s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_START_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
  }

  OEDT_T005_threadData.hAcousticIn  = hAcousticin;
  OEDT_T005_threadData.hAcousticOut = hAcousticout;
  OEDT_T005_threadData.u8CaptureBufferIndex     = 0;
  OEDT_T005_threadData.u8PlaybackBufferIndex    = 0;
  OEDT_T005_threadData.bCaptureThreadIsRunning  = TRUE;
  OEDT_T005_threadData.bPlaybackThreadIsRunning = TRUE;
  OEDT_T005_threadData.bCaptureThreadForceEnd   = FALSE;
  OEDT_T005_threadData.bPlaybackThreadForceEnd  = FALSE;

  //run capture-thread
  OEDT_T005_captureThreadID = OSAL_ERROR;
  OEDT_T005_captureThreadID = OSAL_ThreadSpawn(&OEDT_T005_captureThreadAttr);
  if(OSAL_ERROR == OEDT_T005_captureThreadID)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Capture Thread <%s> (count %d)\n",OEDT_T005_captureThreadAttr.szName, iCount));
    OEDT_ACOUSTICIN_HelperOsalError();
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Capture Thread <%s> (count %d)\n",OEDT_T005_captureThreadAttr.szName, iCount));
  }

  OSAL_s32ThreadWait(250);

  //run playback-thread
  OEDT_T005_playbackThreadID = OSAL_ERROR;
  OEDT_T005_playbackThreadID = OSAL_ThreadSpawn(&OEDT_T005_playbackThreadAttr);
  if(OSAL_ERROR == OEDT_T005_playbackThreadID)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Playback Thread <%s> (count %d)\n",OEDT_T005_playbackThreadAttr.szName, iCount));
    OEDT_ACOUSTICIN_HelperOsalError();
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT;
  }
  else //if(0 == OEDT_T005_playbackThreadID)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Playback Thread <%s> (count %d)\n",OEDT_T005_playbackThreadAttr.szName, iCount));
  } //else //if(0 == OEDT_T005_playbackThreadID)

  //wait for threads

  iEmergencyExit = 13; //wait 13secs
  while((
        OEDT_T005_threadData.bCaptureThreadIsRunning
        ||
        OEDT_T005_threadData.bPlaybackThreadIsRunning
        )
        &&
        (iEmergencyExit > 0)
       )
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 1, iEmergencyExit %d\n", iEmergencyExit));
    iEmergencyExit--;
    OSAL_s32ThreadWait(1000);
  }//while

  OEDT_T005_threadData.bCaptureThreadForceEnd   = TRUE;
  OEDT_T005_threadData.bPlaybackThreadForceEnd  = TRUE;

  //wait for threads again
  iEmergencyExit = 5; //wait 5secs
  while((
        OEDT_T005_threadData.bCaptureThreadIsRunning
        ||
        OEDT_T005_threadData.bPlaybackThreadIsRunning
        )
        &&
        (iEmergencyExit > 0)
       )
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END iEmergencyExit %d\n", iEmergencyExit));
    iEmergencyExit--;
    OSAL_s32ThreadWait(1000);
  }//while

  // delete Playback Thread if running yet
  if(OEDT_T005_threadData.bPlaybackThreadIsRunning)
  {
    if(OSAL_ERROR != OEDT_T005_playbackThreadID)
    {
      OSAL_s32ThreadDelete(OEDT_T005_playbackThreadID);
      OEDT_T005_playbackThreadID = OSAL_ERROR;
    } //if(0 != OEDT_T005_playbackThreadID)
  }

  // delete Cature Thread if running yet
  if(OEDT_T005_threadData.bCaptureThreadIsRunning)
  {
    if(OSAL_ERROR != OEDT_T005_captureThreadID)
    {
      OSAL_s32ThreadDelete(OEDT_T005_captureThreadID);
      OEDT_T005_captureThreadID = OSAL_ERROR;
    } //if(0 != OEDT_T005_threadID)
  }


  /*OUT  close /dev/acousticout/speech */
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_CLOSE_OUT_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
  }

  /*IN  close /dev/acousticout/speechreco */
  s32Ret = OSAL_s32IOClose(hAcousticin);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T005_CLOSE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
  }
  OSAL_s32ThreadWait(250);

  if(OEDT_ACOUSTICIN_T005_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T005 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  }
  OSAL_s32ThreadWait(250);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICIN_T005(void)

/*****************************************************************************/
/************************* TEST 006 ******************************************/
/*****************************OEDT_ACOUSTICIN_T006**************************/
/*simaltaneous capture and playback with different stream sizes with long duration.*/
#define OEDT_ACOUSTICIN_T006_COUNT                        20
#define OEDT_ACOUSTICIN_T006_DEVICE_NAME_OUT \
             OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithFile"
#define OEDT_ACOUSTICIN_T006_DEVICE_NAME_IN \
              OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO "/OedtWithFile"


#define OEDT_ACOUSTICIN_T006_SAMPLE_RATE_IN              48000
#define OEDT_ACOUSTICIN_T006_SAMPLE_RATE_OUT             48000
#define OEDT_ACOUSTICIN_T006_CHANNELS_IN                 2
#define OEDT_ACOUSTICIN_T006_CHANNELS_OUT                OEDT_ACOUSTICIN_T006_CHANNELS_IN
#define OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN               4096
#define OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT              OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN



#define OEDT_ACOUSTICIN_T006_RESULT_OK_VALUE                         0x00000000
#define OEDT_ACOUSTICIN_T006_OPEN_OUT_RESULT_ERROR_BIT               0x00000001
#define OEDT_ACOUSTICIN_T006_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   0x00000006
#define OEDT_ACOUSTICIN_T006_SETSAMPLERATE_OUT_RESULT_ERROR_BIT      0x00000004
#define OEDT_ACOUSTICIN_T006_SETCHANNELS_OUT_RESULT_ERROR_BIT        0x00000008
#define OEDT_ACOUSTICIN_T006_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICIN_T006_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      0x00000060
#define OEDT_ACOUSTICIN_T006_START_OUT_RESULT_ERROR_BIT              0x00000040
#define OEDT_ACOUSTICIN_T006_OPEN_IN_RESULT_ERROR_BIT                0x00000080

#define OEDT_ACOUSTICIN_T006_SETSAMPLERATE_IN_RESULT_ERROR_BIT       0x00000600
#define OEDT_ACOUSTICIN_T006_SETCHANNELS_IN_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICIN_T006_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     0x00000800
#define OEDT_ACOUSTICIN_T006_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       0x00001000
#define OEDT_ACOUSTICIN_T006_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT    0x00006000
#define OEDT_ACOUSTICIN_T006_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT   0x00004000
#define OEDT_ACOUSTICIN_T006_START_IN_RESULT_ERROR_BIT               0x00008000

#define OEDT_ACOUSTICIN_T006_CLOSE_IN_RESULT_ERROR_BIT               0x10000000

#define OEDT_ACOUSTICIN_T006_CLOSE_OUT_RESULT_ERROR_BIT              0x80000000

#define Playback_Array_Size_T006                                     264600
#define OEDT_ACOUSTICIN_T006_LOOP_COUNT                              48

static tU8 OEDT_ACOUSTICIN_T006_u8PCMBufferArray[OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN*64];

static void OEDT_T006_vCaptureThread(void *pvData);
static void OEDT_T006_vPlaybackThread(void *pvData);


typedef struct OEDT_ACOUSTICIN_T006_ThreadData_tag
{
  OSAL_tIODescriptor hAcousticIn;
  OSAL_tIODescriptor hAcousticOut;
  volatile tU8 u8CaptureBufferIndex;
  volatile tU8 u8PlaybackBufferIndex;
  volatile tBool bCaptureThreadIsRunning;
  volatile tBool bPlaybackThreadIsRunning;
  volatile tBool bCaptureThreadForceEnd;
  volatile tBool bPlaybackThreadForceEnd;
}OEDT_ACOUSTICIN_T006_ThreadData_type;

static volatile OEDT_ACOUSTICIN_T006_ThreadData_type OEDT_T006_threadData;

static OSAL_tThreadID         OEDT_T006_captureThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T006_captureThreadAttr = {"OEDTT02c", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T006_vCaptureThread,
(void*)&OEDT_T006_threadData};
static OSAL_tThreadID         OEDT_T006_playbackThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T006_playbackThreadAttr = {"OEDTT02p", //name
OSAL_C_U32_THREAD_PRIORITY_LOWEST,       //Prio
1024,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T006_vPlaybackThread,
(void*)&OEDT_T006_threadData};


/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T006_vCaptureThread
  *
  *  @brief         Captures Data from AcousticIn
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 27-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T006_vCaptureThread(void *pvData)
{
  tS32 iCount , icapturelength = 0;
  tS32 s32Ret = 0;
  tS32 s32ReadBytesTotal = 0;
  OEDT_ACOUSTICIN_T006_ThreadData_type *pThreadData =
  (OEDT_ACOUSTICIN_T006_ThreadData_type*)pvData;
  tS8 *ps8PCM ;
  ps8PCM = (tS8*)&OEDT_ACOUSTICIN_T006_u8PCMBufferArray[0];

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T006 CAPTURE THREAD START\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T006_LOOP_COUNT; iCount++)
  {
    if(iCount < 32)
      icapturelength = OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN;
    else
      icapturelength = 2*OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN;

    s32Ret = OSAL_s32IORead( pThreadData->hAcousticIn,
                             ps8PCM,
                             (unsigned long)icapturelength );
    ps8PCM = ps8PCM + icapturelength;

    if(s32Ret <= 0)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Read from AcousticIn %d\n",
                                   (tS32)s32Ret));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "SUCCESS Read from AcousticIn "
                                   "%d (total %d, count %d)\n",
                                   (tS32)s32Ret,
                                   (tS32)s32ReadBytesTotal,
                                   iCount));
      s32ReadBytesTotal += s32Ret; //OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN;   //no partial reads can occur
      pThreadData->u8CaptureBufferIndex++;
    }
    if(pThreadData->bCaptureThreadForceEnd)
    {
      break;
    }
  } //for

  /*IN  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP IN command\n"));
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticIn, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP IN\n"));
  }

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T006 CAPTURE THREAD END\n"));
  pThreadData->bCaptureThreadIsRunning  = FALSE;
}
/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T006_vPlaybackThread
  *
  *  @brief         play pcm data to AcousticOut
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 27-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
static void OEDT_T006_vPlaybackThread(void *pvData)
{
  tS32 s32Ret = 0;
  tU32 iReadLen=0, BytesWritten=0, BytesRamaining = 0;
  tS32 Writecount=0;
  OEDT_ACOUSTICIN_T006_ThreadData_type *pThreadData = (OEDT_ACOUSTICIN_T006_ThreadData_type*)pvData;
  tS8 *ps8PCM ;
  tS32 iEmergency;
  ps8PCM = (tS8 *)&OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[0];

  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T006 PLAYBACK THREAD\n"));

  while(BytesWritten < Playback_Array_Size_T006)
  {
    BytesRamaining = Playback_Array_Size_T006 - BytesWritten;
    if(Writecount%4 == 0)
      iReadLen = OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT/4;
    else if(Writecount%4 == 1)
      iReadLen = OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT/2;
    else if(Writecount%4 == 2)
      iReadLen = OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT;
    else if(Writecount%4 == 3)
      iReadLen = 2*OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT;

    if(BytesRamaining < iReadLen)
      break;
    s32Ret = OSAL_s32IOWrite(pThreadData->hAcousticOut, ps8PCM, (tU32)iReadLen);
    Writecount++;
    ps8PCM = ps8PCM + iReadLen;
    BytesWritten += iReadLen;

    if(OSAL_ERROR == s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                   "ERROR Aout Write bytes (%u)  \n",(unsigned int) iReadLen ));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n",  (unsigned int)iReadLen));
    }
  }

  /*OUT  stop command */
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP OUT command\n"));
  OEDT_ACOUSTICIN_bOutStopped = FALSE;
  s32Ret = OSAL_s32IOControl(pThreadData->hAcousticOut, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP OUT\n"));
  }

  //waiting for stop reply
  iEmergency = 50; //waiting time to stop device
  while(!OEDT_ACOUSTICIN_bOutStopped && (iEmergency > 0))
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP OUT\n"));
    OSAL_s32ThreadWait(100);
    iEmergency--;
  }

  if(iEmergency <= 0)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP OUT acknowledge\n"));
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED OUT\n"));
  }
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: T006 PLAYBACK THREAD END\n"));
  pThreadData->bPlaybackThreadIsRunning = FALSE;
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICIN_T006(void)
  *
  *  @brief         Simultaneous Capture and Playout with different stream sizes for long duration
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 27-1-2012, mem4kor
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICIN_T006(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICIN_T006_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin  = OSAL_ERROR;
  tS32 s32Ret = 0;
  tS32  iCount ;
  OSAL_trAcousticOutCallbackReg rCallbackReg;
  OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
  tU32 u32Channels;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS32 iEmergencyExit;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T006(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICIN_T006_COUNT; iCount++)
  {
    //clear audo buffers
    memset(OEDT_ACOUSTICIN_T006_u8PCMBufferArray, 0, sizeof(OEDT_ACOUSTICIN_T006_u8PCMBufferArray));

    /*OUT open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICIN_T006_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T006_DEVICE_NAME_OUT, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T006_DEVICE_NAME_OUT, (unsigned int)hAcousticout, iCount));
    }

    /*OUT register callback function */
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback; /* common callback function */
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify OUT\n"));

    }

    /*OUT configure sample rate */
    rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T006_SAMPLE_RATE_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETSAMPLERATE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE OUT: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    }

    /*OUT configure channels */
    u32Channels = OEDT_ACOUSTICIN_T006_CHANNELS_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                               (tS32)u32Channels);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETCHANNELS_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
    }

    /*OUT configure sample format */
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    }

    /*OUT configure buffersize */
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICIN_T006_BUFFERSIZE_OUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
    }

    /*IN open /dev/acousticin/speechreco */
    hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T006_DEVICE_NAME_IN, OSAL_EN_READONLY );
    if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_ACOUSTICIN_T006_DEVICE_NAME_IN, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_OPEN_IN_RESULT_ERROR_BIT;
      hAcousticin = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open IN <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T006_DEVICE_NAME_IN, (unsigned int)hAcousticin, iCount));
    }

    /*IN configure sample rate */
    rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleRateCfg.nSamplerate = OEDT_ACOUSTICIN_T006_SAMPLE_RATE_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    }

    /*IN configure channels */
    u32Channels = OEDT_ACOUSTICIN_T006_CHANNELS_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS, (tS32)u32Channels);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETCHANNELS_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
    }

    /*IN configure sample format */
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    }

    /*IN configure buffersize */
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICIN_T006_BUFFERSIZE_IN;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
    } //else //if(OSAL_OK != s32Ret)

    /*IN start command */
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_START_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
    }

    /*OUT start command */

    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_START_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START OUT\n"));
    }

    OEDT_T006_threadData.hAcousticIn  = hAcousticin;
    OEDT_T006_threadData.hAcousticOut = hAcousticout;
    OEDT_T006_threadData.u8CaptureBufferIndex     = 0;
    OEDT_T006_threadData.u8PlaybackBufferIndex    = 0;
    OEDT_T006_threadData.bCaptureThreadIsRunning  = TRUE;
    OEDT_T006_threadData.bPlaybackThreadIsRunning = TRUE;
    OEDT_T006_threadData.bCaptureThreadForceEnd   = FALSE;
    OEDT_T006_threadData.bPlaybackThreadForceEnd  = FALSE;

    //run capture-thread
    OEDT_T006_captureThreadID = OSAL_ERROR;
    OEDT_T006_captureThreadID = OSAL_ThreadSpawn(&OEDT_T006_captureThreadAttr);
    if(OSAL_ERROR == OEDT_T006_captureThreadID)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Capture Thread <%s> (count %d)\n",OEDT_T006_captureThreadAttr.szName, iCount));
      OEDT_ACOUSTICIN_HelperOsalError();
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Capture Thread <%s> (count %d)\n",OEDT_T006_captureThreadAttr.szName, iCount));
    }
    OSAL_s32ThreadWait(250);

    //run playback-thread
    OEDT_T006_playbackThreadID = OSAL_ERROR;
    OEDT_T006_playbackThreadID = OSAL_ThreadSpawn(&OEDT_T006_playbackThreadAttr);
    if(OSAL_ERROR == OEDT_T006_playbackThreadID)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Playback Thread <%s> (count %d)\n",OEDT_T006_playbackThreadAttr.szName, iCount));
      OEDT_ACOUSTICIN_HelperOsalError();
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Playback Thread <%s> (count %d)\n",OEDT_T006_playbackThreadAttr.szName, iCount));
    }

    //wait for threads

    iEmergencyExit = 13; //wait 13secs
    while((
          OEDT_T006_threadData.bCaptureThreadIsRunning
          ||
          OEDT_T006_threadData.bPlaybackThreadIsRunning
          )
          &&
          (iEmergencyExit > 0)
         )
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 1, iEmergencyExit %d\n", iEmergencyExit));
      iEmergencyExit--;
      OSAL_s32ThreadWait(1000);
    }//while

    OEDT_T006_threadData.bCaptureThreadForceEnd   = TRUE;
    OEDT_T006_threadData.bPlaybackThreadForceEnd  = TRUE;

    //wait for threads again
    iEmergencyExit = 5; //wait 5secs
    while((
          OEDT_T006_threadData.bCaptureThreadIsRunning
          ||
          OEDT_T006_threadData.bPlaybackThreadIsRunning
          )
          &&
          (iEmergencyExit > 0)
         )
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END iEmergencyExit %d\n", iEmergencyExit));
      iEmergencyExit--;
      OSAL_s32ThreadWait(1000);
    }//while

    // delete Playback Thread if running yet
    if(OEDT_T006_threadData.bPlaybackThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T006_playbackThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T006_playbackThreadID);
        OEDT_T006_playbackThreadID = OSAL_ERROR;
      }
    }

    // delete Cature Thread if running yet
    if(OEDT_T006_threadData.bCaptureThreadIsRunning)
    {
      if(OSAL_ERROR != OEDT_T006_captureThreadID)
      {
        OSAL_s32ThreadDelete(OEDT_T006_captureThreadID);
        OEDT_T006_captureThreadID = OSAL_ERROR;
      }
    }


    /*OUT  close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    }

    /*IN  close /dev/acousticout/speechreco */
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T006_CLOSE_IN_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
    }

    OSAL_s32ThreadWait(250);
  }

  if(OEDT_ACOUSTICIN_T006_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: T006 bit coded ERROR: 0x%08X\n",
                                  (unsigned int)u32ResultBitMask));
  }
  OSAL_s32ThreadWait(250);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICIN_T006(void)


/*****************************************************************************/
/*******************************TEST 007**************************************/
/***************************************************************************/
#define OSAL_EN_ACOUSTIC_ENC_INVALID OSAL_EN_ACOUSTIC_ENC_MP3  ////MP3 is not supported by AcousticIn.
#define OEDT_ACOUSTICIN_T007_CHANNELS_INVALID 4
#define OSAL_EN_ACOUSTIC_SF_INVALID 15
#define OEDT_ACOUSTICIN_T007_BUFFERSIZE_INVALID 512
#define OEDT_ACOUSTICIN_T007_DEVICE_NAME \
                        (OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO)

#define OEDT_ACOUSTICIN_T007_DEVICE_NAME_INVALID    "/dev/invalid"

#define OEDT_ACOUSTICIN_T007_CHANNELS_IN                              2
#define OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN                            4096
#define OEDT_ACOUSTICIN_T007_SAMPLE_RATE_IN                           16000
#define OEDT_ACOUSTICIN_T007_SAMPLE_RATE_INVALID                      4000
#define OEDT_ACOUSTICIN_T007_RESULT_OK_VALUE                          0x00000000
#define OEDT_ACOUSTICIN_T007_OPEN_OUT_RESULT_ERROR_BIT                0x00000001
#define OEDT_ACOUSTICIN_T007_SETSAMPLERATE_NULLCHECK_ERROR_BIT        0x00000002
#define OEDT_ACOUSTICIN_T007_SETSAMPLERATE_DECODERCHECK_ERROR_BIT     0x00000004
#define OEDT_ACOUSTICIN_T007_SETCHANNELS_RESULT_ERROR_BIT             0x00000008
#define OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_NULLCHECK_ERROR_BIT      0x00000010
#define OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_DECODERCHECK_ERROR_BIT   0x00000020
#define OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_CHECK_ERROR_BIT          0x00000040
#define OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_NULLCHECK_ERROR_BIT        0x00000080
#define OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_CHECK_ERROR_BIT            0x00000100
#define OEDT_ACOUSTICIN_T007_SETSAMPLERATE_CHECK_ERROR_BIT            0x00000200
#define OEDT_ACOUSTICIN_T007_SETSAMPLERATE_MALLOC_ERROR_BIT           0x00000400
#define OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_MALLOC_ERROR_BIT         0x00000800
#define OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_MALLOC_ERROR_BIT           0x00001000
#define OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_DECODERCHECK_ERROR_BIT     0x00002000
#define OEDT_ACOUSTICOUT_T007_CLOSE_RESULT_ERROR_BIT                  0x00004000
#define OEDT_ACOUSTICINREAD_T007_SETSAMPLERATE_IN_RESULT_ERROR_BIT    0x00008000
#define OEDT_ACOUSTICINREAD_T007_SETCHANNELS_IN_RESULT_ERROR_BIT      0x00010000
#define OEDT_ACOUSTICINREAD_T007_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT  0x00020000
#define OEDT_ACOUSTICINREAD_T007_SETBUFFERSIZE_IN_RESULT_ERROR_BIT    0x00040000
#define OEDT_ACOUSTICINREAD_T007_START_IN_RESULT_ERROR_BIT            0x00080000
#define OEDT_ACOUSTICINREAD_T007_NULLBUFPOINTER_CHECK_ERROR_BIT       0x00100000
#define OEDT_ACOUSTICINREAD_T007_BYTESTOREAD_CHECK_ERROR_BIT          0x00200000
#define OEDT_ACOUSTICINREAD_T007_NULLDEVPOINTER_CHECK_ERROR_BIT       0x00400000
#define OEDT_ACOUSTICIN_T007_OPEN_CHECK_ERROR_BIT                     0x00800000
#define OEDT_ACOUSTICIN_T007_CLOSE_CHECK_ERROR_BIT                    0x01000000
#define OEDT_ACOUSTICINEXTREAD_T007_NULLPOINTER_CHECK_ERROR_BIT       0x02000000
#define OEDT_ACOUSTICINEXTREAD_T007_NULLBUFPOINTER_CHECK_ERROR_BIT    0x04000000
#define OEDT_ACOUSTICINEXTREAD_T007_BUFFERSIZE_CHECK_ERROR_BIT        0x08000000


static tU8 OEDT_ACOUSTICIN_T007_u8PCMBufferArray[OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN];



/*****************************************************************************
* FUNCTION      :  OEDT_ACOUSTICIN_T007
* PARAMETER     :  None
* RETURNVALUE   :  None
* DESCRIPTION   :  Error handler checks  for acousticin component
* HISTORY       :
*  - 16-07-2012, mem4kor
*    Initial revision
*  - 24-08-2012, Niyatha Rao(nro2kor)
*    Included additional error checks for
*    Open device, Close device
*    Read and Ext Read operations
*
******************************************************************************/

tU32 OEDT_ACOUSTICIN_T007(void)
{

  tU32 u32ResultBitMask = OEDT_ACOUSTICIN_T007_RESULT_OK_VALUE;
  static OSAL_tIODescriptor hAcousticin = OSAL_ERROR;
  tS32 s32Ret ;
  int  iCount = 0;

  OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_ACOUSTICIN_T007(void)\n"));

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR OPEN OPERATION*/
  /********************************************************************************************************************************************/
  /*INVALD DEVICE CHECK*/
  hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T007_DEVICE_NAME_INVALID, OSAL_EN_READONLY);
  if(OSAL_ERROR== hAcousticin)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_DOESNOTEXIST== (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:OPENING INAVLID DEVICE IS CORRECTLY HANDLED : \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:OPENING INAVLID DEVICE IS NOT CORRECTLY HANDLED: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_OPEN_CHECK_ERROR_BIT;
  }

  /********************************************************************************************************************************************/
  /* OPEN /DEV/ACOUSTICOUT/SPEECH */
  /********************************************************************************************************************************************/
  hAcousticin = OSAL_IOOpen(OEDT_ACOUSTICIN_T007_DEVICE_NAME, OSAL_EN_READONLY);
  if(OSAL_ERROR == hAcousticin)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICIN_T007_DEVICE_NAME, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_OPEN_OUT_RESULT_ERROR_BIT;
    hAcousticin = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICIN_T007_DEVICE_NAME, (unsigned int)hAcousticin, iCount));
  }

  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK FOR SET SAMPLE RATE */
  /********************************************************************************************************************************************/

  /* NULL POINTER CHECK */
  OSAL_trAcousticSampleRateCfg* rSampleRateCfg= NULL;

  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE,(tS32)rSampleRateCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLERATE_NULLCHECK_ERROR_BIT;
  }

  rSampleRateCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticSampleRateCfg));

  if(rSampleRateCfg == OSAL_NULL)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("malloc failed"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLERATE_MALLOC_ERROR_BIT;
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_CLOSE_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
    }
    return u32ResultBitMask;
  }
  /*CODEC CHECK */
  rSampleRateCfg->enCodec  = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_ENC_INVALID;/*invalid codec */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE,(tS32)rSampleRateCfg);

  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_TEMP_NOT_AVAILABLE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
    }
  }

  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLERATE_DECODERCHECK_ERROR_BIT;
  }
  /* SAMPLERATE CHECK */
  rSampleRateCfg->enCodec  = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg->nSamplerate = OEDT_ACOUSTICIN_T007_SAMPLE_RATE_INVALID;/*invalid samplerate */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE,(tS32)rSampleRateCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL:\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLERATE_CHECK_ERROR_BIT;
  }

  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK FOR SET CHANNELS */
  /********************************************************************************************************************************************/

  /*INVALID NUMBER OF CHANNELS */
  tU32 u32Channels = OEDT_ACOUSTICIN_T007_CHANNELS_INVALID;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,(tS32)u32Channels);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE ==(tU32) s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: CHANNEL CHECK IS CORRECTLY HANDLED IN THE SETCHANNELS IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:CHANNEL CHECK IS NOT HANDLED IN THE SETCHANNELS IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETCHANNELS_RESULT_ERROR_BIT;
  }

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR SET  SAMPLE FORMAT */
  /********************************************************************************************************************************************/

  /*NULL CHECK*/
  OSAL_trAcousticSampleFormatCfg* rSampleFormatCfg=NULL;

  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
    u32ResultBitMask |=OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_NULLCHECK_ERROR_BIT;
  }

  rSampleFormatCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticSampleFormatCfg));
  if(rSampleFormatCfg == OSAL_NULL)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("malloc failed"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_MALLOC_ERROR_BIT;
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_CLOSE_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
    }
    OSAL_vMemoryFree(rSampleRateCfg);
    return u32ResultBitMask;
  }
  /*INVALID CODEC */
  rSampleFormatCfg->enCodec = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_ENC_INVALID;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_TEMP_NOT_AVAILABLE ==(tU32) s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_DECODERCHECK_ERROR_BIT;
  }

  /*INVALID SAMPLE FORMAT*/
  rSampleFormatCfg->enCodec = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg->enSampleformat = (OSAL_tenAcousticSampleFormat)OSAL_EN_ACOUSTIC_SF_INVALID;

  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLE FORMAT CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: :SAMPLE FORMAT CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL:  \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETSAMPLEFORMAT_CHECK_ERROR_BIT;
  }

  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK FOR SET BUFFERSIZE */
  /********************************************************************************************************************************************/
  /*NULL CHECK*/
  OSAL_trAcousticBufferSizeCfg* rCfg = NULL;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE,(tS32)rCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: :NULL CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_NULLCHECK_ERROR_BIT;
  }
  rCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticBufferSizeCfg));
  if(rCfg ==OSAL_NULL)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("malloc failed"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_MALLOC_ERROR_BIT;
    s32Ret = OSAL_s32IOClose(hAcousticin);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_CLOSE_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
    }
    OSAL_vMemoryFree(rSampleRateCfg);
    OSAL_vMemoryFree(rSampleFormatCfg);
    return u32ResultBitMask;
  }

  /*INVALID CODEC*/
  rCfg->enCodec = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_ENC_INVALID;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE,(tS32)rCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_TEMP_NOT_AVAILABLE ==(tU32) s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
    }
    else
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_DECODERCHECK_ERROR_BIT;
    }

  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
  }

  /*INVALID BUFFERSIZE*/
  rCfg->enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg->nBuffersize = OEDT_ACOUSTICIN_T007_BUFFERSIZE_INVALID;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE,(tS32)rCfg);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFERSIZE CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFERSIZE CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_SETBUFFERSIZE_CHECK_ERROR_BIT ;
  }

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR READ OPERATION*/
  /********************************************************************************************************************************************/

  /*IN CONFIGURE SAMPLE RATE */
  rSampleRateCfg->enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg->nSamplerate = OEDT_ACOUSTICIN_T007_SAMPLE_RATE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE IN: %u\n", (unsigned int)rSampleRateCfg->nSamplerate));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_SETSAMPLERATE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:SUCCESS SETSAMPLERATE IN: %u\n",(unsigned int)rSampleRateCfg->nSamplerate));
  }

  /*IN CONFIGURE CHANNELS */
  u32Channels = OEDT_ACOUSTICIN_T007_CHANNELS_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,(tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_SETCHANNELS_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
  }

  /*IN CONFIGURE SAMPLE FORMAT */
  rSampleFormatCfg->enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg->enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg->enSampleformat));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg->enSampleformat));
  }

  /*IN CONFIGURE BUFFERSIZE */
  rCfg->enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg->nBuffersize = OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)rCfg);
  OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg->nBuffersize));
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg->nBuffersize));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg->nBuffersize));
  }

  /*IN START COMMAND */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_START_IN_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
  }

  /*NULL BUFFER POINTER CHECK */
  tS8 *readbuffer = NULL;
  s32Ret = OSAL_s32IORead( hAcousticin, readbuffer, OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN );
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL POINTER CHECK IS CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : NULL POINTER CHECK IS NOT CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_NULLBUFPOINTER_CHECK_ERROR_BIT;
  }

  /* CHECK FOR NUMBER OF BYTES TO READ */
  readbuffer = (tS8*)&OEDT_ACOUSTICIN_T007_u8PCMBufferArray[0];
  memset(readbuffer, 0 , OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN);
  s32Ret = OSAL_s32IORead( hAcousticin, readbuffer, (tU32)0 );
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_NOSPACE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:CHECK FOR NUMBER OF BYTES TO READ IS CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : CHECK FOR NUMBER OF BYTES TO READ IS NOT CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_BYTESTOREAD_CHECK_ERROR_BIT ;
  }

  /* NULL DEVICE POINTER CHECK FOR READ OPERATION*/
  s32Ret = OSAL_s32IORead( (OSAL_tIODescriptor)NULL, readbuffer, OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN );
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:CHECK FOR NULL DEVICE POINTER IS CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : CHECK FOR NULL DEVICE POINTER IS NOT CORRECTLY HANDLED IN THE OSAL_s32IORead CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINREAD_T007_NULLDEVPOINTER_CHECK_ERROR_BIT ;
  }

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR EXT READ OPERATION*/
  /********************************************************************************************************************************************/

  /* NULL POINTER CHECK */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_EXTREAD, (tS32)(NULL));
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: NULL CHECK IS CORRECTLY HANDLED IN THE EXTREAD IOCTRL CALL: \n"));

    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS  NOT HANDLED IN THE EXTREAD IOCTRL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINEXTREAD_T007_NULLPOINTER_CHECK_ERROR_BIT;
  }

  /*BUFFER POINTER CHECK*/
  OSAL_trAcousticInRead pExtReadInfo;
  pExtReadInfo.pvBuffer = NULL;
  pExtReadInfo.u32BufferSize = OEDT_ACOUSTICIN_T007_BUFFERSIZE_IN;
  pExtReadInfo.nTimeout = OSAL_C_TIMEOUT_FOREVER;
  pExtReadInfo.u32Timestamp = 0;

  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_EXTREAD, (tS32)&pExtReadInfo);

  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFER POINTER CHECK FOR EXTREAD IS CORRECTLY HANDLED IN THE EXTREAD IOCTL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFER POINTER CHECK FOR EXTREAD IS NOT CORRECTLY HANDLED IN THE EXTREAD IOCTL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINEXTREAD_T007_NULLBUFPOINTER_CHECK_ERROR_BIT;
  }

  /*BUFFER SIZE CHECK*/
  pExtReadInfo.pvBuffer = (void*)OEDT_ACOUSTICIN_T007_u8PCMBufferArray;
  pExtReadInfo.u32BufferSize = OEDT_ACOUSTICIN_T007_BUFFERSIZE_INVALID;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_EXTREAD, (tS32)&pExtReadInfo);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_NOSPACE== (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFERSIZE CHECK FOR EXTREAD IS CORRECTLY HANDLED IN THE EXTREAD IOCTL CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFERSIZE CHECK FOR EXTREAD IS NOT CORRECTLY HANDLED IN THE EXTREAD IOCTL CALL: \n"));
    u32ResultBitMask |= OEDT_ACOUSTICINEXTREAD_T007_BUFFERSIZE_CHECK_ERROR_BIT ;
  }

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR CLOSE OPERATION*/
  /********************************************************************************************************************************************/

  s32Ret = OSAL_s32IOClose((OSAL_tIODescriptor)NULL);
  if(OSAL_ERROR== s32Ret)
  {
    s32Ret = (tS32)OSAL_u32ErrorCode();
    if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
    {
      OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:DEVICE NULL CHECK IS CORRECTLY HANDLED IN THE OSAL_s32IOClose CALL: \n"));
    }
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE OSAL_s32IOClose CALL:\n"));
    u32ResultBitMask |= OEDT_ACOUSTICIN_T007_CLOSE_CHECK_ERROR_BIT;
  }

  /********************************************************************************************************************************************/
  /* CLOSE /DEV/ACOUSTICOUT/SPEECH */
  /********************************************************************************************************************************************/
  s32Ret = OSAL_s32IOClose(hAcousticin);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_CLOSE_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
  }
  OSAL_vMemoryFree(rSampleRateCfg);
  OSAL_vMemoryFree(rSampleFormatCfg);
  OSAL_vMemoryFree(rCfg);

  return u32ResultBitMask;
}


/******************************************FunctionHeaderBegin************
*FUNCTION:    u32AcousticinPCMBasicTest
*DESCRIPTION:
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     14.08.06  Bernd Schubart, 3SOFT
*             changed setup, KW
*Initial Revision.... 23.06.09 RBEI
                     Changed - Linux acoustic in addes to osal
******************************************FunctionHeaderEnd*************/
#define OEDT_ACOUSTICIN_PCMBASIC_TEST01_SPEECHRECO_OPEN_ERROR_BITMASK 0x00000001
#define OEDT_ACOUSTICIN_PCMBASIC_TEST01_FILE_OPEN_ERROR_BITMASK       0x00000002
#define OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_SAMPLE_RATE_ERROR_BITMASK 0x00000004
#define OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_BUFFERSIZE_ERROR_BITMASK  0x00000008
#define OEDT_ACOUSTICIN_PCMBASIC_TEST01_READ_ERROR_BITMASK            0x00000040


tU32 u32AcousticinPCMBasicTest1_OsalFileOpen(tVoid)
{
  tU32 u32Ret = 0UL;
  static tS8  as8Buffer[C_U32_PCM_ACIN_BUFFERSIZE]={'\0'};
  tS32 s32ReadBytesTotal = 0;
  tS32 s32FileSize = C_U32_PCM_ACIN_BUFFERSIZE * 15;
  OSAL_tIODescriptor hPCMFile;   // L I N T = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin;// L I N T = OSAL_ERROR;

  printf("Acoustic In Test 1\n");

  hAcousticin = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );
  if(OSAL_ERROR == hAcousticin)
  {
    printf("ERROR AcousticInPCMBasic: SpeechReco not open\n");
    u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SPEECHRECO_OPEN_ERROR_BITMASK;
  }
  else
  {
    printf("SUCCESS AcousticInPCMBasic: SpeechReco opened\n");
  }

  hPCMFile = OSAL_IOOpen( C_SZ_ACIN_PCMTESTFILE1, (OSAL_tenAccess)((tU32)OSAL_EN_READWRITE | (tU32)OSAL_EN_BINARY));
  if(OSAL_ERROR == hPCMFile)
  {
    printf("ERROR AcousticInPCMBasic: Test File not open <%s>\n", C_SZ_ACIN_PCMTESTFILE1);
    u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_FILE_OPEN_ERROR_BITMASK;
  }
  else
  {
    printf("SUCCESS AcousticInPCMBasic: Test File opened <%s>\n", C_SZ_ACIN_PCMTESTFILE1);
  }



  if((hPCMFile != OSAL_ERROR) && (hAcousticin != OSAL_ERROR))
  {
    OSAL_trAcousticSampleRateCfg rSampleRateCfg;

    rSampleRateCfg.enCodec = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleRateCfg.nSamplerate = C_U32_PCM_SAMPLERATE;
    OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg );

    OSAL_trAcousticBufferSizeCfg rBuffsizecfg;
    rBuffsizecfg.enCodec  = OSAL_EN_ACOUSTIC_ENC_PCM;
    rBuffsizecfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;
    tS32 s32Return = OSAL_s32IOControl(hAcousticin,
                                       OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE,
                                       (tS32)&rBuffsizecfg );
    printf ("\n s32Return SETBUFFERSIZE %d \n",s32Return);
    tU32 u32Channel = 2 ;
    s32Return = OSAL_s32IOControl( hAcousticin,
                                   OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,
                                   (tS32)u32Channel );
    OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
    printf ("\n s32Return SETCHANNELS %d \n",s32Return);
    while(s32ReadBytesTotal < s32FileSize)
    {
      memset(as8Buffer,0,C_U32_PCM_ACIN_BUFFERSIZE);
      s32Return= OSAL_s32IORead( hAcousticin, as8Buffer,
                                 C_U32_PCM_ACIN_BUFFERSIZE );
      printf ("\n s32Return IN BUFFERSIZE %d \n",s32Return);
      OSAL_s32IOWrite(hPCMFile, as8Buffer, C_U32_PCM_ACIN_BUFFERSIZE);
      s32ReadBytesTotal += (tS32)C_U32_PCM_ACIN_BUFFERSIZE;
    }
    OSAL_s32IOClose( hPCMFile );
    OSAL_s32IOControl( hAcousticin,
                       OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,
                       (tS32)NULL);
    OSAL_s32IOClose( hAcousticin );
    // Sleep ( 5000 );

  }
  else
  {
    printf("ERROR AcousticInPCMBasic - test not executed\n");

  }
  return u32Ret;
}

//same as above, but uses std open for file access


//try an echo of accin to accout
tU32 u32AcousticinPCMInOutTest1(unsigned int uiSRIN, unsigned int uiChannelsIN)
{
  unsigned int uiSROUT = uiSRIN;
  unsigned int uiChannelsOUT = uiChannelsIN;
  tU32 u32Ret = 0UL;
  tS32 s32Ret;
  static tS8 as8Buffer[C_U32_PCM_ACIN_BUFFERSIZE]={'\0'};
  tS32 s32ReadBytesTotal = 0;
  tS32 s32FileSize = C_U32_PCM_ACIN_BUFFERSIZE * 100;
  //OSAL_tIODescriptor hPCMFile = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin;//L I N T  = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticout;//L I N T  = OSAL_ERROR;


  printf("u32AcousticinPCMInOutTest1\n");

  /* open /dev/acousticin/speechreco */
  hAcousticin = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );
  if(OSAL_ERROR == hAcousticin)
  {
    OEDT_ACOUSTICIN_HelperOsalError();
    printf("ERROR u32AcousticinPCMInOutTest1: SpeechReco not open\n");
  }
  else
  {
    printf("SUCCESS u32AcousticinPCMInOutTest1: SpeechReco opened\n");
  }



  /* open /dev/acousticout/speech */
  hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICIN_HelperOsalError();
    printf("AOUT: ERROR Open\n");
  }
  else
  {
    printf("AOUT: SUCCESS Open %u\n", (unsigned int)hAcousticout);

  }

  if((hAcousticin != OSAL_ERROR) && (hAcousticout != OSAL_ERROR))
  {

    /*IN  configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg rSampleRateCfgIN;
      rSampleRateCfgIN.enCodec = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleRateCfgIN.nSamplerate = uiSRIN; //C_U32_PCM_SAMPLERATE;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfgIN );
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest1: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_SAMPLE_RATE_ERROR_BITMASK;
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
      }
    }

    /*OUT  configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg rSampleRateCfgOUT;
      rSampleRateCfgOUT.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfgOUT.nSamplerate = uiSROUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfgOUT);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
      }
      else
      {
        printf("AOUT: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
      }
    }


    /*IN configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest1: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
    }

    /*OUT configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
      else
      {
        printf("AOUT: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
    }


    /*IN configure channels */
    {
      tS32 s32Channel =  (int)uiChannelsIN;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,s32Channel);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest1: SetChannels %d\n", (int)s32Channel);
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: SetChannels %d\n", (int)s32Channel);
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT configure channels */
    {
      tS32 s32Channel =  (int)uiChannelsOUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS, s32Channel);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETCHANNELS: %d\n", (int)s32Channel);
      }
      else
      {
        printf("AOUT: SUCCESS SETCHANNELS: %d\n", (int)s32Channel);
      }
    }


    /*IN configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rBuffsizecfg;
      rBuffsizecfg.enCodec  = OSAL_EN_ACOUSTIC_ENC_PCM;
      rBuffsizecfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rBuffsizecfg );
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest1: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_BUFFERSIZE_ERROR_BITMASK;
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
      }
    }

    /*OUT configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
      }
      else
      {
        printf("AOUT: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
      }
    }


    /*IN issue start command */
    {
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest1: START\n");
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: START\n");
      } //else //if(OSAL_OK != s32Ret)
    }

    /*OUT issue start command */
    {
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR START\n");
      }
      else
      {
        printf("AOUT: SUCCESS START\n");
      }
    }

    s32Ret = 1;
    int iCount = 0;
    while(s32ReadBytesTotal < s32FileSize && s32Ret > 0)
    {
      //printf("AcousticInPCMBasic: READ 0 (wait 2s)\n");
      //OSAL_s32ThreadWait(2000);
      memset(as8Buffer,0,C_U32_PCM_ACIN_BUFFERSIZE);
      printf("u32AcousticinPCMInOutTest1: READ OSAL_s32IORead 1\n");
      s32Ret = OSAL_s32IORead( hAcousticin, as8Buffer, C_U32_PCM_ACIN_BUFFERSIZE );
      printf("AcousticInPCMBasic: READ OSAL_s32IORead s32Ret %d\n",(int)s32Ret);
      if(s32Ret <= 0)
      {
        OEDT_ACOUSTICIN_HelperOsalError();
        printf("ERROR u32AcousticinPCMInOutTest1: Read from AcousticIn %d\n", (int)s32Ret);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_READ_ERROR_BITMASK;
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest1: Read from AcousticIn %d (total %d, count %d)\n", (int)s32Ret, (int)s32ReadBytesTotal, iCount);
        s32ReadBytesTotal += (tS32)s32Ret;

        //write to acout
        int iWrite = C_U32_PCM_ACIN_BUFFERSIZE;
        s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)iWrite);
        if(OSAL_ERROR == s32Ret)
        {
          printf("AOUT: ERROR Aout Write bytes (%d)\n", (int)iWrite);
          OEDT_ACOUSTICIN_HelperOsalError();
        }
        else
        {
          printf("AOUT: SUCCESS AOut Written Bytes (%d)\n", (int)iWrite);
        }

      } //else //if(OSAL_OK != s32Ret)
      //
      iCount++;
    }
    // Sleep ( 5000 );

  }
  else
  {
    printf("ERROR u32AcousticinPCMInOutTest1 - test not executed (one or both devices not open)\n");

  }

  //   OSAL_s32ThreadWait(2000);
  s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    printf("ERROR u32AcousticinPCMInOutTest1: IN STOP\n");
  }
  else //if(OSAL_OK != s32Ret)
  {
    printf("SUCCESS u32AcousticinPCMInOutTest1: IN STOPPED\n");
  } //else //if(OSAL_OK != s32Ret)

  s32Ret = OSAL_s32IOControl( hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    printf("ERROR u32AcousticinPCMInOutTest1: OUT STOP\n");
  }
  else //if(OSAL_OK != s32Ret)
  {
    printf("SUCCESS u32AcousticinPCMInOutTest1: OUT STOPPED\n");
  } //else //if(OSAL_OK != s32Ret)


  //printf("WAIT 2s for close AcI\n");
  //OSAL_s32ThreadWait(2000);
  printf("Close AcIn\n");
  OSAL_s32IOClose( hAcousticin );

  //printf("WAIT 2s for close AcOUT\n");
  //OSAL_s32ThreadWait(2000);
  printf("Close AcOUT\n");
  OSAL_s32IOClose( hAcousticout );
  printf("OUT + IN CLOSED\n");
  //printf("WAIT 2s until return\n");
  //OSAL_s32ThreadWait(2000);
  return u32Ret;
}






/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback
*DESCRIPTION: used as device callback function for device test
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     19.09.05  Robert Schedel, 3SOFT
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static OSAL_trAcousticErrThrCfg rCbLastErrThr_Temp;
static tBool OEDT_bAudioOutStopped = FALSE;
static tVoid vAcousticOutTstCallback_Temp (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  /*tS32 s32Ret = OSAL_OK;

  s32Ret = OSAL_s32EventPost(hAcTstCbEvt,
  (1 << enCbReason),
  OSAL_EN_EVENTMASK_OR);*/
  (void)pvAddData;

  //pvCbLastCookie = pvCookie;
  (void)pvCookie;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n");
    /* no additional data */
    OEDT_bAudioOutStopped = TRUE;
    break;

    //case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED:
    //episode stop event received
    //gu32CbEpisodeStopped++;
    //break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
    //ash u32CbErrThrCnt++;
    rCbLastErrThr_Temp = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n", (unsigned int)rCbLastErrThr_Temp.enErrType, (unsigned int)rCbLastErrThr_Temp.enErrType);
    switch(rCbLastErrThr_Temp.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      //ash u32CbErrXrunCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      //ash u32CbErrBitstreamCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      //ash u32CbErrNoValidDataCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      //ash u32CbErrWrongFormatCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      //ash u32CbErrInternalCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      //ash u32CbErrFatalCnt++;
      break;

    default:
      /* unknown error type */
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n");
    //ash u32CbTimerCnt++;
    //ash rCbLastTimestamp = *(OSAL_trAcousticTimestamp*)pvAddData;
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n");
    //ash rCbLastMarker = *(OSAL_trAcousticOutMarkerEventInfo*)pvAddData;
    //ash u32CbEventMarkerCnt++;
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n");
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    printf("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n");
    break;




  default: /* callback unsupported by test code */
    printf("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason);
    break;
  }
}

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback
*DESCRIPTION: used as device callback function for device test
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     19.09.05  Robert Schedel, 3SOFT
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static OSAL_trAcousticErrThrCfg rCbLastErrThrIn_Temp;
static tBool OEDT_bAudioInStopped = FALSE;
static tVoid vAcousticInTstCallback_Temp (OSAL_tenAcousticInEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICIN_EVAUDIOSTOPPED:
    printf("AIN CALLBACK: OSAL_EN_ACOUSTICIN_EVAUDIOSTOPPED STOPPED STOPPED STOPPED STOPPED STOPPED STOPPED STOPPED \n");
    /* no additional data */
    OEDT_bAudioInStopped = TRUE;
    break;

    //case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED:
    //episode stop event received
    //gu32CbEpisodeStopped++;
    //break;

  case OSAL_EN_ACOUSTICIN_EVERRTHRESHREACHED:
    //ash u32CbErrThrCnt++;
    rCbLastErrThrIn_Temp = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    printf("AIN CALLBACK: OSAL_EN_ACOUSTICIN_EVERRTHRESHREACHED: %u == 0x%08X\n", (unsigned int)rCbLastErrThrIn_Temp.enErrType, (unsigned int)rCbLastErrThrIn_Temp.enErrType);
    switch(rCbLastErrThr_Temp.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      //ash u32CbErrXrunCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      //ash u32CbErrBitstreamCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      //ash u32CbErrNoValidDataCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      //ash u32CbErrWrongFormatCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      //ash u32CbErrInternalCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      //ash u32CbErrFatalCnt++;
      break;

    default:
      /* unknown error type */
      break;
    }
    break;

  default: /* callback unsupported by test code */
    printf("AIN CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason);
    break;
  }
}

//open-close sequence as Thorsten Neumann used in voice recognition
tU32 u32AcousticinPCMInOutTest2(unsigned int uiSRIN, unsigned int uiChannelsIN, const char *pcszPCM)
{
  int iFD_pcm_file;

  unsigned int uiSROUT = 22050;
  unsigned int uiChannelsOUT = 1;
  tU32 u32Ret = 0UL;
  tS32 s32Ret;
  static tS8 as8Buffer[C_U32_PCM_ACIN_BUFFERSIZE]={'\0'};
  tS32 s32ReadBytesTotal = 0;
  tS32 s32FileSize = C_U32_PCM_ACIN_BUFFERSIZE * 64 * 5;
  //OSAL_tIODescriptor hPCMFile = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticout;//L I N T  = OSAL_ERROR;


  printf("u32AcousticinPCMInOutTest2\n");

  /* open PCM file */
  //hPCMFile = OSAL_IOOpen(pcszPCM, OSAL_EN_READONLY);
  //if(OSAL_ERROR == hPCMFile)
  iFD_pcm_file = open(pcszPCM, O_RDWR); //>=0 success
  if(iFD_pcm_file < 0)
  {
    printf("AOUT: ERROR Open File <%s>\n", pcszPCM);
  }
  else
  {
    printf("AOUT: SUCCESS Open File <%s>\n", pcszPCM);
  }





  /* open /dev/acousticout/speech */
  {
    hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_HelperOsalError();
      printf("AOUT: ERROR Open\n");
    }
    else
    {
      printf("AOUT: SUCCESS Open %u\n", (unsigned int)hAcousticout);

    }
  }

  /* register callback function */
  {
    OSAL_trAcousticOutCallbackReg rCallbackReg;
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback_Temp;
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);

    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR RegNotify\n");
      hAcousticout = (OSAL_tIODescriptor)NULL;
    }
    else
    {
      printf("AOUT: SUCCESS RegNotify\n");

    }
  }




  if((hAcousticout != OSAL_ERROR))
  {

    /*OUT  configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg rSampleRateCfgOUT;
      rSampleRateCfgOUT.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfgOUT.nSamplerate = uiSROUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfgOUT);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
      }
      else
      {
        printf("AOUT: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
      }
    }



    /*OUT configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
      else
      {
        printf("AOUT: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
    }


    /*OUT configure channels */
    {
      tS32 s32Channel =  (int)uiChannelsOUT;
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS, s32Channel);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETCHANNELS: %d\n", (int)s32Channel);
      }
      else
      {
        printf("AOUT: SUCCESS SETCHANNELS: %d\n", (int)s32Channel);
      }
    }



    /*OUT configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
      }
      else
      {
        printf("AOUT: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
      }
    }


    /*OUT issue start command */
    {
      s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        printf("AOUT: ERROR START\n");
      }
      else
      {
        printf("AOUT: SUCCESS START\n");
      }
    }

    /* read file and write to acousticout */
    {

      static tS8  as8BufferOut[C_U32_PCM_ACIN_BUFFERSIZE];
      int iRead = 0;
      int iWrite = 0;
      //int iSamples;
      printf("\n");
      do
      {
        iRead = read(iFD_pcm_file, as8BufferOut, C_U32_PCM_ACIN_BUFFERSIZE);
        //printf("AOUT:Read %d\n", iRead);
        if(iRead <= 0)
        {
          printf("\nAOUT:file read error\t%i\n", iRead);
        }
        else
        {
          iWrite = iRead;
          s32Ret = OSAL_s32IOWrite(hAcousticout, as8BufferOut, (tU32)iWrite);
          if(OSAL_ERROR == s32Ret)
          {
            printf("\nAOUT: ERROR Aout Write bytes (%u)\n", (unsigned int)iWrite);
            OEDT_ACOUSTICIN_HelperOsalError();
          }
          else
          {
            printf("AOUT: SUCCESS AOut Write Bytes (%u)\r", (unsigned int)iWrite);
          }
        }
      }
      while(iRead > 0 && OSAL_ERROR != s32Ret);
      printf("\n");
    }
    // Sleep ( 5000 );





    OEDT_bAudioOutStopped = FALSE;
    s32Ret = OSAL_s32IOControl( hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      printf("ERROR u32AcousticinPCMInOutTest2: OUT STOP\n");
    }
    else //if(OSAL_OK != s32Ret)
    {
      printf("SUCCESS u32AcousticinPCMInOutTest2: OUT STOPPED\n");
    } //else //if(OSAL_OK != s32Ret)


    while(!OEDT_bAudioOutStopped)
    {
      printf("SUCCESS u32AcousticinPCMInOutTest2: Wait while Audio Out stopping\n");
      OSAL_s32ThreadWait(200);
    }
    printf("Close AcOUT\n");
    OSAL_s32IOClose( hAcousticout );
    hAcousticout = OSAL_ERROR;
    printf("OUT CLOSED\n");




    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
    //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN

    /* open /dev/acousticin/speechreco */
    hAcousticin = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );
    if(OSAL_ERROR == hAcousticin)
    {
      OEDT_ACOUSTICIN_HelperOsalError();
      printf("ERROR u32AcousticinPCMInOutTest2: SpeechReco not open\n");
    }
    else
    {
      printf("SUCCESS u32AcousticinPCMInOutTest2: SpeechReco opened\n");
    }



    /*IN  configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg rSampleRateCfgIN;
      rSampleRateCfgIN.enCodec = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleRateCfgIN.nSamplerate = uiSRIN; //C_U32_PCM_SAMPLERATE;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfgIN );
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest2: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_SAMPLE_RATE_ERROR_BITMASK;
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
      }
    }

    /*IN configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest2: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
      }
    }

    /*IN configure channels */
    {
      tS32 s32Channel =  (int)uiChannelsIN;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,s32Channel);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest2: SetChannels %d\n", (int)s32Channel);
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: SetChannels %d\n", (int)s32Channel);
      } //else //if(OSAL_OK != s32Ret)
    }

    /*IN configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rBuffsizecfg;
      rBuffsizecfg.enCodec  = OSAL_EN_ACOUSTIC_ENC_PCM;
      rBuffsizecfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rBuffsizecfg );
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest2: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_BUFFERSIZE_ERROR_BITMASK;
      }
      else
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
      }
    }



    /*IN issue start command */
    {
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        printf("ERROR u32AcousticinPCMInOutTest2: START\n");
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: START\n");
      } //else //if(OSAL_OK != s32Ret)
    }

    s32Ret = 1;
    int iCount = 0;
    printf("\n");
    while(s32ReadBytesTotal < s32FileSize && s32Ret > 0)
    {
      //printf("AcousticInPCMBasic: READ 0 (wait 2s)\n");
      //OSAL_s32ThreadWait(2000);
      memset(as8Buffer,0,C_U32_PCM_ACIN_BUFFERSIZE);
      //printf("u32AcousticinPCMInOutTest2: READ OSAL_s32IORead 1\n");
      s32Ret = OSAL_s32IORead( hAcousticin, as8Buffer, C_U32_PCM_ACIN_BUFFERSIZE );
      //printf("AcousticInPCMBasic: READ OSAL_s32IORead s32Ret %d\n",(int)s32Ret);
      if(s32Ret <= 0)
      {
        OEDT_ACOUSTICIN_HelperOsalError();
        printf("\nERROR u32AcousticinPCMInOutTest2: Read from AcousticIn %d\n", (int)s32Ret);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_READ_ERROR_BITMASK;
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest2: Read from AcousticIn %d (total %d, count %d)\r", (int)s32Ret, (int)s32ReadBytesTotal, iCount);
        s32ReadBytesTotal += s32Ret;
      } //else //if(OSAL_OK != s32Ret)
      //
      iCount++;
    }
    // Sleep ( 5000 );

    printf("\n");


  }
  else
  {
    printf("ERROR u32AcousticinPCMInOutTest2 - test not executed (one or both devices not open)\n");

  }

  //   OSAL_s32ThreadWait(2000);
  s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    printf("ERROR u32AcousticinPCMInOutTest2: IN STOP\n");
  }
  else //if(OSAL_OK != s32Ret)
  {
    printf("SUCCESS u32AcousticinPCMInOutTest2: IN STOPPED\n");
  } //else //if(OSAL_OK != s32Ret)



  printf("Close AcIn\n");
  OSAL_s32IOClose( hAcousticin );
  //L I N T hAcousticin = OSAL_ERROR;
  printf("AcIn CLosed\n");

  //printf("WAIT 2s for close AcOUT\n");
  //OSAL_s32ThreadWait(2000);
  //printf("WAIT 2s until return\n");
  //OSAL_s32ThreadWait(2000);
  return u32Ret;
}



//open-close sequence as Thorsten Neumann used in voice recognition
// AcIn dauerhaft ge??et - nur STOP-START STOP-START STOP-START
tU32 u32AcousticinPCMInOutTest4(unsigned int uiSRIN, unsigned int uiChannelsIN, const char *pcszPCM)
{
  int iFD_pcm_file;

  unsigned int uiSROUT = 22050;
  unsigned int uiChannelsOUT = 1;
  tU32 u32Ret = 0UL;
  tS32 s32Ret;
  static tS8 as8Buffer[C_U32_PCM_ACIN_BUFFERSIZE]={'\0'};
  tS32 s32ReadBytesTotal = 0;
  tS32 s32FileSize = C_U32_PCM_ACIN_BUFFERSIZE * 64 * 5;
  //OSAL_tIODescriptor hPCMFile = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin;//L I N T  = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticout;//L I N T  = OSAL_ERROR;



  printf("u32AcousticinPCMInOutTest3\n");


  /* open PCM file */
  //hPCMFile = OSAL_IOOpen(pcszPCM, OSAL_EN_READONLY);
  //if(OSAL_ERROR == hPCMFile)
  iFD_pcm_file = open(pcszPCM, O_RDWR); //>=0 success
  if(iFD_pcm_file < 0)
  {
    printf("AOUT: ERROR Open File <%s>\n", pcszPCM);
  }
  else
  {
    printf("AOUT: SUCCESS Open File <%s>\n", pcszPCM);
  }



  /* open /dev/acousticin/speechreco */
  hAcousticin = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );
  if(OSAL_ERROR == hAcousticin)
  {
    OEDT_ACOUSTICIN_HelperOsalError();
    printf("ERROR u32AcousticinPCMInOutTest3: SpeechReco not open\n");
  }
  else
  {
    printf("SUCCESS u32AcousticinPCMInOutTest3: SpeechReco opened\n");
  }


  /* open /dev/acousticout/speech */
  {
    hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICIN_HelperOsalError();
      printf("AOUT: ERROR Open\n");
    }
    else
    {
      printf("AOUT: SUCCESS Open %u\n", (unsigned int)hAcousticout);

    }
  }


  /* register callback function */
  {
    OSAL_trAcousticOutCallbackReg rCallbackReg;
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback_Temp;
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);

    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR RegNotify\n");
      hAcousticout = (OSAL_tIODescriptor)NULL;
    }
    else
    {
      printf("AOUT: SUCCESS RegNotify\n");

    }
  }





  /*OUT  configure sample rate */
  {
    OSAL_trAcousticSampleRateCfg rSampleRateCfgOUT;
    rSampleRateCfgOUT.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfgOUT.nSamplerate = uiSROUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE, (tS32)&rSampleRateCfgOUT);
    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
    }
    else
    {
      printf("AOUT: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfgOUT.nSamplerate);
    }
  }



  /*OUT configure sample format */
  {
    OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
    }
    else
    {
      printf("AOUT: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
    }
  }


  /*OUT configure channels */
  {
    tS32 s32Channel =  (int)uiChannelsOUT;
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS, s32Channel);
    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR SETCHANNELS: %d\n", (int)s32Channel);
    }
    else
    {
      printf("AOUT: SUCCESS SETCHANNELS: %d\n", (int)s32Channel);
    }
  }



  /*OUT configure buffersize */
  {
    OSAL_trAcousticBufferSizeCfg rCfg;
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
    }
    else
    {
      printf("AOUT: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize);
    }
  }


  /*OUT issue start command */
  {
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      printf("AOUT: ERROR START\n");
    }
    else
    {
      printf("AOUT: SUCCESS START\n");
    }
  }

  /* read file and write to acousticout */
  {

    static tS8  as8BufferOut[C_U32_PCM_ACIN_BUFFERSIZE];
    int iRead = 0;
    int iWrite = 0;
    //int iSamples;
    printf("\n");
    do
    {
      iRead = read(iFD_pcm_file, as8BufferOut, C_U32_PCM_ACIN_BUFFERSIZE);
      //printf("AOUT:Read %d\n", iRead);
      if(iRead <= 0)
      {
        printf("\nAOUT:file read error\t%i\n", iRead);
      }
      else
      {
        iWrite = iRead;

        s32Ret = OSAL_s32IOWrite(hAcousticout, as8BufferOut, (tU32)iWrite);
        if(OSAL_ERROR == s32Ret)
        {
          printf("\nAOUT: ERROR Aout Write bytes (%u)\n", (unsigned int)iWrite);
          OEDT_ACOUSTICIN_HelperOsalError();
        }
        else
        {
          printf("AOUT: SUCCESS AOut Write Bytes (%u)\r", (unsigned int)iWrite);
        }
      }
    }
    while(iRead > 0 && OSAL_ERROR != s32Ret);
    printf("\n");
  }
  // Sleep ( 5000 );





  OEDT_bAudioOutStopped = FALSE;
  s32Ret = OSAL_s32IOControl( hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    printf("ERROR u32AcousticinPCMInOutTest3: OUT STOP\n");
  }
  else //if(OSAL_OK != s32Ret)
  {
    printf("SUCCESS u32AcousticinPCMInOutTest3: OUT STOPPED\n");
  } //else //if(OSAL_OK != s32Ret)


  while(!OEDT_bAudioOutStopped)
  {
    printf("SUCCESS u32AcousticinPCMInOutTest3: Wait while Audio Out stopping\n");
    OSAL_s32ThreadWait(200);
  }
  printf("Close AcOUT\n");
  OSAL_s32IOClose( hAcousticout );
  hAcousticout = OSAL_ERROR;
  printf("OUT CLOSED\n");




  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //IN ININ ININ ININ ININ ININ ININ ININ ININ ININ ININ IN
  //


  /*IN register callback function */
  {
    OSAL_trAcousticInCallbackReg rCallbackReg;
    rCallbackReg.pfEvCallback = vAcousticInTstCallback_Temp;
    rCallbackReg.pvCookie = (tPVoid)&hAcousticin;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_REG_NOTIFICATION, (tS32)&rCallbackReg);

    if(OSAL_OK != s32Ret)
    {
      printf("AIN: ERROR RegNotify\n");
      hAcousticout = (OSAL_tIODescriptor)NULL;
    }
    else
    {
      printf("AIN: SUCCESS RegNotify\n");

    }
  }


  /*IN  configure sample rate */
  {
    OSAL_trAcousticSampleRateCfg rSampleRateCfgIN;
    rSampleRateCfgIN.enCodec = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleRateCfgIN.nSamplerate = uiSRIN; //C_U32_PCM_SAMPLERATE;
    s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfgIN );
    if(OSAL_OK != s32Ret)
    {
      printf("ERROR u32AcousticinPCMInOutTest3: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
      u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_SAMPLE_RATE_ERROR_BITMASK;
    }
    else
    {
      printf("SUCCESS u32AcousticinPCMInOutTest3: SetSampleRate %u\n", (unsigned int)rSampleRateCfgIN.nSamplerate);
    }
  }

  /*IN configure sample format */
  {
    OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      printf("ERROR u32AcousticinPCMInOutTest3: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
    }
    else
    {
      printf("SUCCESS u32AcousticinPCMInOutTest3: SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat);
    }
  }

  /*IN configure channels */
  {
    tS32 s32Channel =  (int)uiChannelsIN;
    s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,s32Channel);
    if(OSAL_OK != s32Ret)
    {
      printf("ERROR u32AcousticinPCMInOutTest3: SetChannels %d\n", (int)s32Channel);
    }
    else //if(OSAL_OK != s32Ret)
    {
      printf("SUCCESS u32AcousticinPCMInOutTest3: SetChannels %d\n", (int)s32Channel);
    } //else //if(OSAL_OK != s32Ret)
  }

  /*IN configure buffersize */
  {
    OSAL_trAcousticBufferSizeCfg rBuffsizecfg;
    rBuffsizecfg.enCodec  = OSAL_EN_ACOUSTIC_ENC_PCM;
    rBuffsizecfg.nBuffersize = C_U32_PCM_ACIN_BUFFERSIZE;
    s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rBuffsizecfg );
    if(OSAL_OK != s32Ret)
    {
      printf("ERROR u32AcousticinPCMInOutTest3: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
      u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_SET_BUFFERSIZE_ERROR_BITMASK;
    }
    else
    {
      printf("SUCCESS u32AcousticinPCMInOutTest3: SetBufferSize %u\n", (unsigned int)rBuffsizecfg.nBuffersize);
    }
  }


  int iI;
  for(iI = 0; iI < 5 ;iI++)
  {
    printf("\nLOOP u32AcousticinPCMInOutTest3: %d\n", iI);
    /*IN issue start command */
    {
      s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICIN_HelperOsalError();
        printf("ERROR u32AcousticinPCMInOutTest3: START\n");
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest3: START\n");
      } //else //if(OSAL_OK != s32Ret)
    }

    s32Ret = 1;
    int iCount = 0;
    printf("\n");
    s32ReadBytesTotal = 0;
    while(s32ReadBytesTotal < s32FileSize && s32Ret > 0)
    {
      //printf("AcousticInPCMBasic: READ 0 (wait 2s)\n");
      //OSAL_s32ThreadWait(2000);
      memset(as8Buffer,0,C_U32_PCM_ACIN_BUFFERSIZE);
      //printf("u32AcousticinPCMInOutTest3: READ OSAL_s32IORead 1\n");
      s32Ret = OSAL_s32IORead( hAcousticin, as8Buffer, C_U32_PCM_ACIN_BUFFERSIZE );
      //printf("AcousticInPCMBasic: READ OSAL_s32IORead s32Ret %d\n",(int)s32Ret);
      if(s32Ret <= 0)
      {
        OEDT_ACOUSTICIN_HelperOsalError();
        printf("\nERROR u32AcousticinPCMInOutTest3: Read from AcousticIn %d\n", (int)s32Ret);
        u32Ret |= OEDT_ACOUSTICIN_PCMBASIC_TEST01_READ_ERROR_BITMASK;
      }
      else //if(OSAL_OK != s32Ret)
      {
        printf("SUCCESS u32AcousticinPCMInOutTest3: Read from AcousticIn %d (total %d, count %d)\r", (int)s32Ret, (int)s32ReadBytesTotal, iCount);
        s32ReadBytesTotal += s32Ret;
      } //else //if(OSAL_OK != s32Ret)
      //
      iCount++;
    }
    // Sleep ( 5000 );

    printf("\n");




    OEDT_bAudioInStopped = FALSE;
    s32Ret = OSAL_s32IOControl( hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP, (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICIN_HelperOsalError();
      printf("ERROR u32AcousticinPCMInOutTest3: IN STOP s32Ret 0x%08X\n", (unsigned int)s32Ret);
    }
    else //if(OSAL_OK != s32Ret)
    {
      printf("SUCCESS u32AcousticinPCMInOutTest3: IN STOPPED\n");
    } //else //if(OSAL_OK != s32Ret)


    printf("\nLOOP WAIT S\n");
    while(!OEDT_bAudioInStopped)
    {
      printf("SUCCESS u32AcousticinPCMInOutTest2: Wait while Audio In stopping\n");
      OSAL_s32ThreadWait(50);
    }

    //OSAL_s32ThreadWait(4000);
    printf("\nLOOP WAIT E\n");
  } //for(iI = 0; iI < 2 ;iI++)

  printf("\nEND LOOP u32AcousticinPCMInOutTest3\n");


  printf("Close AcIn\n");
  OSAL_s32IOClose( hAcousticin );
  hAcousticin = OSAL_ERROR;
  printf("AcIn CLosed\n");

  //printf("WAIT 2s for close AcOUT\n");
  //OSAL_s32ThreadWait(2000);
  //printf("WAIT 2s until return\n");
  //OSAL_s32ThreadWait(2000);
  return u32Ret;
}





/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/*100825 srt2hi . first Tests on Linux */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
static snd_output_t *snd_output = NULL;


tS32 OEDT_AcousticIn_Main(int iTestNumber, const char *pcszUser1, const char *pcszUser2, const char *pcszUser3)
{
  tS32 s32Ret = 0;
  unsigned int uiSR          = 44100;
  unsigned int uiChannels    = 2;
  const char *pcszCaptureDevice = NULL;

  if(pcszUser1!=NULL && pcszUser2!=NULL)
  {
    uiSR              = (unsigned int)atoi(pcszUser1);
    uiChannels        = (unsigned int)atoi(pcszUser2);
    pcszCaptureDevice = pcszUser3;
    printf("OEDT_AcousticIn_Main: INFO Test# %d, Samplerate %u, Channels %u, CaptureDevice <%s>\n", iTestNumber, uiSR, uiChannels, pcszCaptureDevice);
  }
  else
  {
    printf("OEDT_AcousticIn_Main: ERROR Test# %d, Samplerate %u, Channels %u, CaptureDevice <%s>\n", iTestNumber, uiSR, uiChannels, pcszCaptureDevice);
  }

  snd_output_stdio_attach(&snd_output, stdout, 0);




  printf("OEDT_AcousticIn_Main: START TEST %d ----------------------\n",
         iTestNumber);
  switch(iTestNumber)
  {
  case 1:
    printf("ACOUSTICIN - TEST 1 uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    //s32Ret = (tS32)u32AcousticinPCMBasicTest1(uiSR, uiChannels, C_SZ_ACIN_PCMTESTFILE1);

    /*OSAL_s32ThreadWait(2000);
    printf("ACOUSTICIN - TEST 1 SECOND RUN ----------------------------------------------------------------\n");
    iRet = u32AcousticinPCMBasicTest1(uiSR, uiChannels, C_SZ_ACIN_PCMTESTFILE2);
    printf("ACOUSTICIN - TEST 1 3 RUN ----------------------------------------------------------------\n");
    iRet = u32AcousticinPCMBasicTest1(uiSR, uiChannels, C_SZ_ACIN_PCMTESTFILE3);
    printf("ACOUSTICIN - TEST 1 4 RUN ----------------------------------------------------------------\n");
    iRet = u32AcousticinPCMBasicTest1(uiSR, uiChannels, C_SZ_ACIN_PCMTESTFILE4);
    printf("ACOUSTICIN - TEST 1 5 RUN ----------------------------------------------------------------\n");
    iRet = u32AcousticinPCMBasicTest1(uiSR, uiChannels, C_SZ_ACIN_PCMTESTFILE5);
    */
    break;

  case 2:
    printf("ACOUSTICIN - TEST 2 (IN-OUT 1 ECHO fom In to Out) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    {
      int iO;
      for(iO = 0; iO < 10 ; iO++)
      {
        //s32Ret = (tS32)u32AcousticinPCMInOutTest1(uiSR, uiChannels);
      }
    }
    /*printf("ACOUSTICIN - TEST 2 (IN-OUT 2) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    iRet = u32AcousticinPCMInOutTest1(uiSR, uiChannels);
    printf("ACOUSTICIN - TEST 2 (IN-OUT 3) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    iRet = u32AcousticinPCMInOutTest1(uiSR, uiChannels);
    //iRet = u32AcousticinPCMBasicTest2();
    */
    break;

  case 3:
    printf("ACOUSTICIN - TEST 3 1 (IN-OUT VoiceRecognition style) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    //s32Ret = (tS32)u32AcousticinPCMInOutTest2(uiSR, uiChannels, "/opt/bosch/dtmf_22050Hz_mono_16bit_LE.pcm");
    /*printf("ACOUSTICIN - TEST 3 2 (IN-OUT VoiceRecognition style) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    iRet = u32AcousticinPCMInOutTest2(uiSR, uiChannels, "/opt/bosch/dtmf_22050Hz_mono_16bit_LE.pcm");
    printf("ACOUSTICIN - TEST 3 3 (IN-OUT VoiceRecognition style) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    iRet = u32AcousticinPCMInOutTest2(uiSR, uiChannels, "/opt/bosch/dtmf_22050Hz_mono_16bit_LE.pcm");
    */
    /*printf("ACOUSTICIN - TEST 3\n");
    iRet = u32AcousticinPCMBasicTest3();*/
    break;

  case 4:
    printf("ACOUSTICIN - TEST 4 1 (IN-OUT VoiceRecognition style AcInDauerOpen) uiSR=%uHz, %u Channels\n", uiSR, uiChannels);
    {
      int iO;
      for(iO = 0; iO < 1 ; iO++)
      {
        printf("ACOUSTICIN - TEST 4 START LOOP Count %d\n", iO);

        //s32Ret = (tS32)u32AcousticinPCMInOutTest4(uiSR, uiChannels, "/opt/bosch/dtmf_22050Hz_mono_16bit_LE.pcm");

        printf("ACOUSTICIN - TEST 4 END LOOP Count %d\n", iO);
      } //for(iO = 0; iO < 3 ; iO++)
    }

    /*printf("ACOUSTICIN - TEST 4\n");
    iRet = u32AcousticinPCMBasicTest4();*/
    break;

  case 5:
    printf("ACOUSTICIN - TEST 5\n");
    break;

  case 6:
    printf("ACOUSTICIN - TEST 6\n");
    break;

  case 7:
    printf("ACOUSTICIN - TEST 7\n");
    break;

  case 8:
    printf("ACOUSTICIN - TEST 8\n");
    break;

  case 1001:
    printf("ACOUSTICIN - TEST ALSA 1001 SR %u, CH %u, Dev <%s>\n", uiSR, uiChannels, pcszCaptureDevice);
    //s32Ret = (tS32)test_PCMCaptureAlsa(uiSR, uiChannels, pcszCaptureDevice);
    OSAL_s32ThreadWait(2000);
    //printf("ACOUSTICIN - TEST 1001 SECOND RUN ----------------------------------------------------------------\n");
    //iRet = test_PCMCaptureAlsa(uiSR, uiChannels, pcszCaptureDevice);
    break;



  default:
    printf("ACOUSTICIN - TEST ?\n");
    break;
  }

  printf("OEDT_AcousticIn_Main: END TEST %d Ret:%d ----------------------\n",
         iTestNumber, (int)s32Ret);
  return s32Ret;
}

#define AC_ERROR_OPEN_GPIO                  0x00000001
#define AC_ERROR_DIAG1_ENABLE_SET           0x00000002
#define AC_ERROR_DIAG2_ENABLE_SET           0x00000004
#define AC_ERROR_HW_POWER_SET               0x00000008
#define AC_ERROR_MIC_SELECT_SET             0x00000010
#define AC_ERROR_DIAG1_ENABLE_ACTIVE        0x00000020
#define AC_ERROR_DIAG2_ENABLE_ACTIVE        0x00000040
#define AC_ERROR_HW_POWER_ACTIVE            0x00000080
#define AC_ERROR_MIC_SELECT_ACTIVE          0x00000100
#define AC_ERROR_OPEN_ACOSUTIC              0x00000200
#define AC_ERROR_SET_SAMPLE_RATE            0x00000400
#define AC_ERROR_SET_CHANNELS               0x00000800
#define AC_ERROR_SET_SAMPLE_FORMAT          0x00001000
#define AC_ERROR_SET_BUFFER_SIZE            0x00002000
#define AC_ERROR_ACOSUTIC_START             0x00004000
#define AC_ERROR_ACOSUTIC_READ              0x00008000
#define AC_ERROR_ACOSUTIC_STOP              0x00010000
#define AC_ERROR_ACOSUTIC_CLOSE             0x00020000
#define AC_ERROR_DIAG1_ENABLE_INACTIVE      0x00040000
#define AC_ERROR_DIAG2_ENABLE_INACTIVE      0x00080000
#define AC_ERROR_HW_POWER_INACTIVE          0x00100000
#define AC_ERROR_MIC_SELECT_INACTIVE        0x00200000
#define AC_ERROR_CLOSE_GPIO                 0x00400000
#define T008_SAMPLING_RATE                  16000
#define T009_SAMPLING_RATE                  44100
#define ACOUSTIC_NUMBER_CHANNELS            2

/*****************************************************************************
* FUNCTION      :  OEDT_ACOUSTICIN_T008
* PARAMETER     :  None
* RETURNVALUE   :  None
* DESCRIPTION   :  To validate external MIC-ADC with SR = 16000 Hz
* HISTORY       :
*
*  Initial version
******************************************************************************/
tU32 OEDT_ACOUSTICIN_T008(void)
{
  /* : MIC-ADC OEDT TEST*/
  OSAL_tIODescriptor hFd;
  tU32 u32Result = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  OSAL_tIODescriptor hAcousticin;
  tS32 s32Ret;
  tS32 s32ReadBytesTotal = 0;
  FILE * pFile;
  OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
  tU32 u32Channels = ACOUSTIC_NUMBER_CHANNELS;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS8 *ps8PCMIn      = NULL;
  int iBufferIndexIn     = 0;

  /* Open the device */
  hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
  if(OSAL_ERROR == hFd)
  {
    u32Result |= AC_ERROR_OPEN_GPIO;
  }

  /* Set device to Output mode to OSAL_EN_MIC_DIAG1_ENABLE*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_SET;
  }

  /* Set device to Output mode OSAL_EN_MIC_DIAG2_ENABLE*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_SET;
  }
  /* Set device to Output mode  OSAL_EN_MIC_HW_POWER_CTRL*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_SET;
  }
  /*Set device to Output mode  OSAL_EN_MIC_SELECT_CTRL*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd, OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_SET;
  }

  usleep(200);

  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_ACTIVE;
  }

  hAcousticin = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );

  if(OSAL_ERROR == hAcousticin)
  {
    u32Result |= AC_ERROR_OPEN_ACOSUTIC;
  }

  /*IN configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg.nSamplerate = T008_SAMPLING_RATE;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_SET_SAMPLE_RATE;
  }

  /*IN configure channels */
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,(tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {

    u32Result |= AC_ERROR_SET_CHANNELS;
  }

  /*IN configure sample format */
  rSampleFormatCfg.enCodec    = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {

    u32Result |= AC_ERROR_SET_SAMPLE_FORMAT;
  }


  /*IN configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_SET_BUFFER_SIZE;
  }

  /*IN start command */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_START;
  }

  pFile = fopen ("/tmp/oedt_test.pcm", "wb");


  /*Start Capture*/
  s32Ret = 1;

  while(iBufferIndexIn < OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ)
  {
    ps8PCMIn = (tS8*)OEDT_ACOUSTICIN_T001_u8PCMDoubleBuffer[iBufferIndexIn];
    iBufferIndexIn++;
    memset(ps8PCMIn, 0 , OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN);

    s32Ret = OSAL_s32IORead( hAcousticin, ps8PCMIn, OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN );

    if(s32Ret <= 0)
    {
      u32Result |= AC_ERROR_ACOSUTIC_READ;

    }
    if(pFile)
      fwrite (ps8PCMIn , sizeof(char),OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN, pFile);
    s32ReadBytesTotal += s32Ret;
  }

  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_STOP;
  }

  usleep(10000);

  /*IN  close /dev/acousticout/speechreco */
  s32Ret = OSAL_s32IOClose(hAcousticin);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_CLOSE;
  }

  if(s32ReadBytesTotal > 0)
  {
    usleep(10000);
    if(pFile)
      fclose (pFile);
    system("sync");
    usleep(10000);
  }

  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_INACTIVE;
  }

  /* Close the device */
  if(OSAL_ERROR == OSAL_s32IOClose(hFd))
  {
    u32Result |= AC_ERROR_CLOSE_GPIO;
  }
  return u32Result;
}

/*****************************************************************************
* FUNCTION      :  OEDT_ACOUSTICIN_T009
* PARAMETER     :  None
* RETURNVALUE   :  None
* DESCRIPTION   :  To validate external MIC-ADC with SR = 44100 Hz
* HISTORY       :
*
*  Initial version
******************************************************************************/
tU32 OEDT_ACOUSTICIN_T009(void)
{
  /* : MIC-ADC OEDT TEST*/
  OSAL_tIODescriptor hFd;
  tU32 u32Result = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  OSAL_tIODescriptor hAcousticin;
  tS32 s32Ret;
  tS32 s32ReadBytesTotal = 0;
  FILE * pFile;
  OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
  tU32 u32Channels = ACOUSTIC_NUMBER_CHANNELS;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  OSAL_trAcousticBufferSizeCfg rCfg;
  tS8 *ps8PCMIn      = NULL;
  int iBufferIndexIn     = 0;

  /* Open the device */
  hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
  if(OSAL_ERROR == hFd)
  {
    u32Result |= AC_ERROR_OPEN_GPIO;
  }

  /* Set device to Output mode to OSAL_EN_MIC_DIAG1_ENABLE*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_SET;
  }

  /* Set device to Output mode OSAL_EN_MIC_DIAG2_ENABLE*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_SET;
  }
  /* Set device to Output mode  OSAL_EN_MIC_HW_POWER_CTRL*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_SET;
  }
  /*Set device to Output mode  OSAL_EN_MIC_SELECT_CTRL*/
  if(OSAL_ERROR == OSAL_s32IOControl(hFd, OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_SET;
  }

  usleep(200);

  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_ACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_ACTIVE;
  }

  hAcousticin = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO, OSAL_EN_READONLY );

  if(OSAL_ERROR == hAcousticin)
  {
    u32Result |= AC_ERROR_OPEN_ACOSUTIC;
  }

  /*IN configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleRateCfg.nSamplerate = T009_SAMPLING_RATE;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE, (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_SET_SAMPLE_RATE;
  }

  /*IN configure channels */
  s32Ret = OSAL_s32IOControl(hAcousticin,OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS,(tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {

    u32Result |= AC_ERROR_SET_CHANNELS;
  }

  /*IN configure sample format */
  rSampleFormatCfg.enCodec    = OSAL_EN_ACOUSTIC_ENC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_SET_SAMPLE_FORMAT;
  }


  /*IN configure buffersize */
  rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
  rCfg.nBuffersize = OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN;
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_SET_BUFFER_SIZE;
  }

  /*IN start command */
  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_START;
  }

  pFile = fopen ("/tmp/oedt_test.pcm", "wb");


  /*Start Capture*/
  s32Ret = 1;

  while(iBufferIndexIn < OEDT_ACOUSTICIN_T001_BUFFERS_TO_READ)
  {
    ps8PCMIn = (tS8*)OEDT_ACOUSTICIN_T001_u8PCMDoubleBuffer[iBufferIndexIn];
    iBufferIndexIn++;
    memset(ps8PCMIn, 0 , OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN);

    s32Ret = OSAL_s32IORead( hAcousticin, ps8PCMIn, OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN );

    if(s32Ret <= 0)
    {
      u32Result |= AC_ERROR_ACOSUTIC_READ;

    }
    if(pFile)
      fwrite (ps8PCMIn , sizeof(char),OEDT_ACOUSTICIN_T001_BUFFERSIZE_IN, pFile);
    s32ReadBytesTotal += s32Ret;
  }

  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_STOP;
  }

  usleep(10000);

  /*IN  close /dev/acousticout/speechreco */
  s32Ret = OSAL_s32IOClose(hAcousticin);
  if(OSAL_OK != s32Ret)
  {
    u32Result |= AC_ERROR_ACOSUTIC_CLOSE;
  }

  if(s32ReadBytesTotal > 0)
  {
    usleep(10000);
    if(pFile)
      fclose (pFile);
    system("sync");
    usleep(10000);
  }

  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_DIAG1_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG1_ENABLE_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_DIAG2_ENABLE))
  {
    u32Result |= AC_ERROR_DIAG2_ENABLE_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_HW_POWER_CTRL))
  {
    u32Result |= AC_ERROR_HW_POWER_INACTIVE;
  }
  if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) OSAL_EN_MIC_SELECT_CTRL))
  {
    u32Result |= AC_ERROR_MIC_SELECT_INACTIVE;
  }

  /* Close the device */
  if(OSAL_ERROR == OSAL_s32IOClose(hFd))
  {
    u32Result |= AC_ERROR_CLOSE_GPIO;
  }
  return u32Result;
}



