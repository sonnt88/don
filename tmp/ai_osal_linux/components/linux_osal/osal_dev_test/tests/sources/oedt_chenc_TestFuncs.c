/******************************************************************************
 * FILE         : oedt_chenc_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the chenc  
 *                driver for GM-GE hardware.
 *              
 * AUTHOR(s)    : Anoop Chandran (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date       |                  | Author & comments
 * 30.06.2008 | Initial revision | Anoop Chandran (RBEI/ECM1)
 *            | version 0.1      |
 *----------------------------------------------------------------------
 * 02.04.2009 | version 0.2      | Shilpa Bhat (RBEI/ECF1)
 *            |                  | Lint Removal 
 *----------------------------------------------------------------------
 * 19.07.2013 | version 0.2      | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 *            |                  | Modified test cases for GEN3 China encoder.
 ****************************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_chenc_TestFuncs.h"
/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
/* Dummy values */
#define OEDT_CHENC_DUMMY_GPS_WEEK      5   
#define OEDT_CHENC_DUMMY_GPS_SECOND       59 
#define OEDT_CHENC_DUMMY_BASE_ALT 200
/* Dummy but with in china boundry in radians */
#define OEDT_CHENC_DUMMY_BASE_LAT (0.6108652375 ) 
#define OEDT_CHENC_DUMMY_BASE_LON (1.60570291)
#define OSAL_INVALID_ACCESS_MODE       0xfffffff
/*****************************************************************************
* FUNCTION:      u32ChencOpenCloseDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CHENC_001
* DESCRIPTION:  Open and close the Chenc device.
* HISTORY:      Created by Anoop Chandran (RBEI/ECM1) 30/06/2008
******************************************************************************/
tU32 u32ChencOpenCloseDevice(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
    
    /* Open device in readwrite mode */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
       u32Ret = 1;
    }//if ( hDevice == OSAL_ERROR)
    else
   {
      /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }//if( OSAL_s32IOClose ())
   }//if-else ( hDevice == OSAL_ERROR)
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:      u32ChencOpenDeviceInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CHENC_002
* DESCRIPTION:  Opens the Chenc device with invalid access permission
* HISTORY:      Created by Anoop Chandran (RBEI/ECM1) 30/06/2008
******************************************************************************/
tU32 u32ChencOpenDeviceInvalParam(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = (OSAL_tenAccess)OSAL_INVALID_ACCESS_MODE;
   tU32 u32Ret = 0;
    
    /* Open device in readwrite mode */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
    if ( hDevice != OSAL_ERROR)
    {
       u32Ret = 1;
            /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }//if( OSAL_s32IOClose ())
   }//if ( hDevice != OSAL_ERROR)
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:      u32ChencMultOpenDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CHENC_003
* DESCRIPTION:  Try to opens the Chenc which is already opened
* HISTORY:      Created by Anoop Chandran (RBEI/ECM1) 30/06/2008
******************************************************************************/
tU32 u32ChencMultOpenDevice(tVoid)
{
   OSAL_tIODescriptor hDevice1 = 0;
   OSAL_tIODescriptor hDevice2 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   /* Open device in readwrite mode */    
   hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
   if ( hDevice1 == OSAL_ERROR)
   {
      u32Ret += 1;
   }
   else//if ( hDevice1 == OSAL_ERROR)
   {
      /* Open device in readwrite mode */    
      hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
      if ( hDevice2 != OSAL_ERROR)
      {   
         /* Close device */
         if( OSAL_s32IOClose ( hDevice2 ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }//if( OSAL_s32IOClose ( hDevice2 ))
         
      }//if-else( hDevice2 == OSAL_ERROR)

      /* Close device */
      if( OSAL_s32IOClose ( hDevice1 ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }//if( OSAL_s32IOClose ( hDevice1 ))
   }//if-else( hDevice1 == OSAL_ERROR)
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:      u32ChencOpenDeviceDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CHENC_004
* DESCRIPTION:  Opens the Chenc device with different Access mode
                OSAL_EN_READWRITE,   
                OSAL_EN_READONLY,
                OSAL_EN_WRITEONLY
* HISTORY:      Created by Anoop Chandran (RBEI/ECM1) 30/06/2008
******************************************************************************/
tU32 u32ChencOpenDeviceDiffModes(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   /* Open device in readwrite mode */    
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
   if ( hDevice == OSAL_ERROR)
   {
      u32Ret = 1;
   }//if ( hDevice == OSAL_ERROR)
   else
   {
      /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }//if( OSAL_s32IOClose ())

   }//if-else ( hDevice == OSAL_ERROR)


   /* Open device in read only mode */    
   enAccess = OSAL_EN_READONLY;
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
   if ( hDevice == OSAL_ERROR)
   {
      u32Ret += 10;
   }//if ( hDevice == OSAL_ERROR)
   else
   {
      /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }//if( OSAL_s32IOClose ())

   }//if-else ( hDevice == OSAL_ERROR)

   /* Open device in write only mode */    
   enAccess = OSAL_EN_WRITEONLY;
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
   if ( hDevice == OSAL_ERROR)
   {
      u32Ret += 60;
   }//if ( hDevice == OSAL_ERROR)
   else
   {
      /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 100;
      }//if( OSAL_s32IOClose ())

   }//if-else ( hDevice == OSAL_ERROR)
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:      u32ChenDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CHENC_005
* DESCRIPTION:  
               1) Opens the Chenc device
               2) Close the chenc device
               3) Try to close the chenc, which had been closed.
* HISTORY:      Created by Anoop Chandran (RBEI/ECM1) 30/06/2008
******************************************************************************/
tU32 u32ChenDevCloseAlreadyClosed(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
    
    /* Open device in readwrite mode */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
       u32Ret = 1;
    }//if ( hDevice == OSAL_ERROR)
    else
   {
      /* Close device */
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         /* Try to close the device which had been closed.*/
         if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
         {
            u32Ret = 10;
         }
      }
      //if( OSAL_s32IOClose ())
   }//if-else ( hDevice == OSAL_ERROR)
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:    u32ChenDevEncodePosInfo()
* PARAMETER:   none
* RETURNVALUE: tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:
               1) Opens the Chenc device
               2) Enable encoding
               3) Use some dummy positioning info for encoding
               4) Close the device.
* HISTORY:     Created by Ramachandra Madhu Kiran (CM-AI/PJ-CF35) 19/07/2013
******************************************************************************/
tU32 u32ChenDevEncodePosInfo(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   OSAL_trChencConversionData rChencConversionData;
   rChencConversionData.dAltitude = OEDT_CHENC_DUMMY_BASE_ALT;
   rChencConversionData.dLatitude = OEDT_CHENC_DUMMY_BASE_LAT;
   rChencConversionData.dLongitude = OEDT_CHENC_DUMMY_BASE_LON;
   rChencConversionData.s32GpsWeek = OEDT_CHENC_DUMMY_GPS_WEEK;
   rChencConversionData.s32InitializationFlag = 1;
   rChencConversionData.u32GpsSecond = OEDT_CHENC_DUMMY_GPS_SECOND;
    
    /* Open device in readwrite mode */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
       u32Ret = 1;
    }//if ( hDevice == OSAL_ERROR)
    else
   {
      /* Try Encoding positioning data*/
      if ( OSAL_OK != OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_CHENC_CONVERT, (tS32)&rChencConversionData ))
      {
         u32Ret = 2;
      }
      /* Close the device */
      if ( OSAL_OK !=  OSAL_s32IOClose( hDevice ) )
      {
         u32Ret += 4;
      }
   }
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:    u32ChenDevEncodeChkDisFeature()
* PARAMETER:   none
* RETURNVALUE: tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  
               1) Opens the Chenc device
               2) Disable encoding
               3) Use some dummy positioning info for encoding it should fail
               4) Enable Encoding.
               5) Try Encoding. It should pass
               6) Close the device
* HISTORY:      Created by Ramachandra Madhu Kiran (CM-AI/PJ-CF35) 19/07/2013
******************************************************************************/
tU32 u32ChenDevEncodeChkDisFeature(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tBool bDisable = TRUE;
   OSAL_trChencConversionData rChencConversionData;
   rChencConversionData.dAltitude = OEDT_CHENC_DUMMY_BASE_ALT;
   rChencConversionData.dLatitude = OEDT_CHENC_DUMMY_BASE_LAT;
   rChencConversionData.dLongitude = OEDT_CHENC_DUMMY_BASE_LON;
   rChencConversionData.s32GpsWeek = OEDT_CHENC_DUMMY_GPS_WEEK;
   rChencConversionData.s32InitializationFlag = 1;
   rChencConversionData.u32GpsSecond = OEDT_CHENC_DUMMY_GPS_SECOND;
    
    /* Open device in readwrite mode */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CHENC, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
       u32Ret = 1;
    }//if ( hDevice == OSAL_ERROR)
    else
   {
      /* Disable the divice */
      if ( OSAL_OK !=  OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_CHENC_TEMP_DISABLE, (tS32)bDisable ))
      {
         u32Ret = 2;
      }
      /* Try to encode data: it should fail */
      else if ( OSAL_ERROR != OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_CHENC_CONVERT, (tS32)&rChencConversionData ))
      {
         u32Ret = 4;
      }
      /* Expected error code should be OSAL_E_TEMP_NOT_AVAILABLE */
      else if ( OSAL_E_TEMP_NOT_AVAILABLE != OSAL_u32ErrorCode() )
      {
         u32Ret = 8;
      }
      else 
      {
         /* Enable the device */
         bDisable = FALSE;
         if ( OSAL_OK !=  OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_CHENC_TEMP_DISABLE, (tS32)bDisable ))
         {
            u32Ret = 16;
         }
         /* Try to encode data: Now it should pass */
         else if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_CHENC_CONVERT, (tS32)&rChencConversionData ))
         {
            u32Ret = 32;
         }

      }
      /* Close device and exit */
      if ( OSAL_OK !=  OSAL_s32IOClose( hDevice ) )
      {
         u32Ret += 64;
      }
    }
   return u32Ret;
}
/***************************** End of file ************************/
