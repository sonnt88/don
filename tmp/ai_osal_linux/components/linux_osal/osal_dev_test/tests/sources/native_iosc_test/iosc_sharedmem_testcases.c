#include <iosc_sharedmem_testcases.h>
#include <math.h>

/*****************************************************************************
* FUNCTION		:  vTestSharedMemAllocAndFree()
* PARAMETER		:  trfpMemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test cases 1 to 11 w.r.to malloc and free with different memory sizes
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/

OEDT_TESTSTATE TestSharedMemAllocAndFree(tU32 u32Size)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  tPU32 pU32TestPtr = (tPU32)(iosc_shared_malloc(u32Size));

  OEDT_EXPECT_DIFFERS(NULL, pU32TestPtr, &state);

	if (pU32TestPtr)
    OEDT_EXPECT_EQUALS(IOSC_OK, iosc_shared_free(pU32TestPtr), &state );
	
	return state;
}

OEDT_TESTSTATE TestSharedMemAllocSingleTest(tU32 u32NumOfIterations, tU32 MemoryBaseSize, const char* sMeasurementUnit)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  tU32 i;

  for (i = 0; i < u32NumOfIterations; i++)
  {
    OEDT_TESTSTATE singleTestState = TestSharedMemAllocAndFree ( MemoryBaseSize * (tU32)pow(2, i) );
    state = OEDT_CONCAT_TESTSTATES(&state, &singleTestState);
    if (singleTestState.error_code)
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) try to allocate %d %s of shared memory: FAILED", (tU32)pow(2, i), sMeasurementUnit);
  }
  return state;
}

OEDT_TEST(SharedMemAllocWithGrowingSizes)
{
  // Tests with 1 to 32kB
  OEDT_TESTSTATE state1 = TestSharedMemAllocSingleTest(6, 1024, "kB");

  // Tests with 1 to 32MB
  OEDT_TESTSTATE state2 = TestSharedMemAllocSingleTest(6, 1024 * 1024, "MB");

  OEDT_TESTSTATE stateOverall = OEDT_CONCAT_TESTSTATES(&state1, &state2);
  return stateOverall.error_code;
}

/*****************************************************************************
* FUNCTION		:  vTestfree()
* PARAMETER		:  trfpMemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test cases 12 and 13 w.r.to free
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TESTSTATE TestFree(tPU32* ppU32TestPtr)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();

  tPU32 pU32TestPtr = *ppU32TestPtr;
  pU32TestPtr = (tPU32)iosc_shared_malloc(1024*1024*8);

  OEDT_EXPECT_TRUE(pU32TestPtr != NULL, &state);

  if (pU32TestPtr)
    OEDT_EXPECT_EQUALS(IOSC_OK, iosc_shared_free(pU32TestPtr), &state);

  return state;
}

OEDT_TESTSTATE TestDoubleFree(tPU32* ppU32TestPtr)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  OEDT_EXPECT_EQUALS(IOSC_BADARG, iosc_shared_free(*ppU32TestPtr), &state);
  return state;
}

OEDT_TESTSTATE TestPerformFreeOnNULLPointer()
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  OEDT_EXPECT_DIFFERS(IOSC_OK, iosc_shared_free(NULL), &state);
  return state;
}

OEDT_TEST(SharedMemFree)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  OEDT_TESTSTATE stateSingleTest;

  tPU32 pU32TestPtr = NULL;

  // try to free a NULL pointer
  stateSingleTest = TestPerformFreeOnNULLPointer();
  if (stateSingleTest.error_code)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) try free on NULL pointer: FAILED");
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // allocate and free 8MB of shared memory
  stateSingleTest = TestFree(&pU32TestPtr);
  if (stateSingleTest.error_code)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc and free 8MB: FAILED");
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // try a another free on an already freed pointer
  stateSingleTest = TestDoubleFree(&pU32TestPtr);
  if (stateSingleTest.error_code)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) double free 8MB: FAILED");
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  stateSingleTest = TestFree(&pU32TestPtr);
  if (stateSingleTest.error_code)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) realloc and free 8MB: FAILED");
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  return state.error_code;
}

/*****************************************************************************
* FUNCTION		:  vTestMallocWithId()
* PARAMETER		:  trfpMemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test cases 12 and 13 w.r.to free
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/

tPU32 pu32TestMallocWithId(tBool bShouldAlreadyExist, tBool* pbExistenceExpectationIsTrue, tS32 s32MemID)
{
  tPU32 pU32TestPtr = NULL;
  tS32 s32MemAlreadyExists;

  tU32 SharedMemSize = 1024*1024*8;

  pU32TestPtr = (tPU32)(iosc_shared_malloc_with_id((SharedMemSize), s32MemID, &s32MemAlreadyExists));

  if (( bShouldAlreadyExist && (s32MemAlreadyExists == 1) ) || (!bShouldAlreadyExist && (s32MemAlreadyExists == 0) ))
    *pbExistenceExpectationIsTrue = TRUE;
  else
    *pbExistenceExpectationIsTrue = FALSE;

  return pU32TestPtr;
}

OEDT_TESTSTATE TestMallocWithIdSingleStep(tPU32* vSharedMemPointers, tU32 u32CurrentElement, tS32 s32MemID)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();

  tBool bShouldAlreadyExist = (u32CurrentElement != 0);
  tBool bExistanceExpectationIsTrue;

  tPU32 pU32TestPtr = pu32TestMallocWithId(bShouldAlreadyExist, &bExistanceExpectationIsTrue, s32MemID);
  vSharedMemPointers[u32CurrentElement] = pU32TestPtr;
  OEDT_EXPECT_TRUE(pU32TestPtr != NULL, &state);

  if (!pU32TestPtr)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: NULL pointer returned: FAILED");
  else {
    OEDT_EXPECT_TRUE(bExistanceExpectationIsTrue, &state);
    if (!bExistanceExpectationIsTrue) 
    {
      if (bShouldAlreadyExist) 
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: shared memory was not expected to exist but already existed: FAILED", u32CurrentElement);
      else 
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: shared memory was expected to exist but did not exist: FAILED", u32CurrentElement);
    }
#ifdef OSAL_OS
#if OSAL_OS == OSAL_TENGINE
    else 
    {
      if ( (u32CurrentElement > 0) && (vSharedMemPointers[0] != pU32TestPtr) )
      {
        OEDT_ADD_FAILURE( &state );
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB (realloc=%d): shared memory pointers are not equal: FAILED", (tU32) bShouldAlreadyExist);
      }
    }
#endif
#else
#error unknown OSAL configuration
#endif
  }
  return state;
}

OEDT_TESTSTATE TestFreeSharedMemory(tPU32* vSharedMemPointers, tU32 NumOfElements)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  tU32 currentElement = NumOfElements;
  tPU32 pU32TestPtr;

  OEDT_TEST_BEGIN_LOOP_STATE(&state);
  while(currentElement > 0)
  {
    currentElement--;
    pU32TestPtr = vSharedMemPointers[currentElement];
    if (pU32TestPtr)
      OEDT_EXPECT_EQUALS(IOSC_OK, iosc_shared_free(pU32TestPtr), &state);
  }
  OEDT_TEST_END_LOOP_STATE(&state);

  if (state.error_code)
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (shared mem) free shared memory: FAILED");
  
  return state;
}

OEDT_TEST(SharedMemAllocWithIdMultipleTimes)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  OEDT_TESTSTATE stateSingleTest;
  tPU32 vSharedMemPointers[5];
  tPU32 vSharedMemPointers2[1];
  tU32 i;

  // get the shared memory segment 5 times. The pointer that is returned should alway be the 
  // same and IOSC is expected to be aware of an already existing shared memory segment.
  for (i = 0; i < 5; i++)
  {
    stateSingleTest = TestMallocWithIdSingleStep(vSharedMemPointers, i, IOSC_TEST_MEM_ID);
    state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);
  }

  // free all shared memory pointers
  stateSingleTest = TestFreeSharedMemory(vSharedMemPointers, 5);
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // try to reallocate the shared memory segment -> the segment is expected as non-existing now
  stateSingleTest = TestMallocWithIdSingleStep((tPU32*)&vSharedMemPointers, 0, IOSC_TEST_MEM_ID);
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // try to allocated another shared memory segment just to see if two different shared memory segements can exist next to another
  stateSingleTest = TestMallocWithIdSingleStep((tPU32*)&vSharedMemPointers2, 0, IOSC_TEST_MEM_ID2);
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // free the first shared memory segment again
  stateSingleTest = TestFreeSharedMemory(vSharedMemPointers, 1);
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  // free the second shared memory segment again
  stateSingleTest = TestFreeSharedMemory(vSharedMemPointers2, 1);
  state = OEDT_CONCAT_TESTSTATES(&state, &stateSingleTest);

  return state.error_code;
}

