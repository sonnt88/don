#include <iosc_ringbuffer_testcases.h>

/*****************************************************************************
* FUNCTION		:  vTestRingBufCreate()
* PARAMETER		:  trfpRngBufIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 28 w.r.to Ring buffer
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TEST(RingBufCreate)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;
	IoscRingbuffer ringbuffer1 = IOSC_RINGBUFFER_INVALID;
	
	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
  OEDT_EXPECT_DIFFERS(IOSC_RINGBUFFER_INVALID, ringbuffer, &state);
	if (ringbuffer == IOSC_RINGBUFFER_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: create first ringbuffer: FAILED");
		return state.error_code;
	}

	ringbuffer1 = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
  OEDT_EXPECT_DIFFERS(IOSC_RINGBUFFER_INVALID, ringbuffer1, &state);
	if (ringbuffer1 == IOSC_RINGBUFFER_INVALID)
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: create second ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( ringbuffer, ringbuffer1, &state);
	if (ringbuffer != ringbuffer1)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: check if ringbuffers are equal: FAILED");
		iosc_destroy_ringbuffer(ringbuffer1);
    iosc_destroy_ringbuffer(ringbuffer);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer), &state);
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: destroy first ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer1);
		return state.error_code;
	}

	OEDT_EXPECT_EQUALS(IOSC_OK, iosc_destroy_ringbuffer(ringbuffer1), &state);
	if (state.error_code)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: destroy second ringbuffer: FAILED");
		return state.error_code;
	}

	return state.error_code;
}



/*****************************************************************************
* FUNCTION		:  vTestRingBufDestroy()
* PARAMETER		:  trfpRngBufIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 29 w.r.to Ring buffer
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TEST(RingBufDoubleDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;
	
	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
  OEDT_EXPECT_DIFFERS(IOSC_RINGBUFFER_INVALID, ringbuffer, &state);
	if (ringbuffer == IOSC_RINGBUFFER_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: create ringbuffer: FAILED");
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer), &state);
	if (state.error_code)
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: destroy ringbuffer: FAILED");
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_destroy_ringbuffer(ringbuffer), &state);
	if (state.error_code)
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: second destroy on ringbuffer: FAILED");
    return state.error_code;
	}

	return state.error_code;
}



/*****************************************************************************
* FUNCTION		:  vTestRingBufSendReceive()
* PARAMETER		:  trfpRngBufIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 30 w.r.to Ring buffer
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TEST(RingBufSendReceive)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;

	unsigned char test_snd_buffer[256] = {"TEST BUFF 12345678901234678910234569854721"};
	unsigned char test_rcv_buffer[256] = {""};
	unsigned int string_size = strlen(test_snd_buffer);
	
	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
  OEDT_EXPECT_DIFFERS(IOSC_RINGBUFFER_INVALID, ringbuffer, &state);
	if (ringbuffer == IOSC_RINGBUFFER_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: create ringbuffer FAILED");
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_sendto_ringbuffer(ringbuffer, string_size, test_snd_buffer, IOSC_TIMEOUT_100 ), &state );
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: send to ringbuffer FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_recfrom_ringbuffer(ringbuffer, string_size, test_rcv_buffer, IOSC_TIMEOUT_100 ), &state);
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: receive from ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
    return state.error_code;
	}
	
  OEDT_EXPECT_TRUE(strcmp(test_snd_buffer, test_rcv_buffer ) == 0, &state);
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: check for consistency of sent and received buffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer), &state );
	if (state.error_code)
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: destroy ringbuffer: FAILED");
		return state.error_code;
	}

  return state.error_code;
}
