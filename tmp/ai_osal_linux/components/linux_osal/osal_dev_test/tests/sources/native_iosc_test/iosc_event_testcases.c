#include <iosc_event_testcases.h>
#include <iosc.h>

/*****************************************************************************
* FUNCTION		:  vTestEventCreateDestroy()
* PARAMETER		:  trfpEventIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 31 w.r.to IOSC events
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TEST(EventCreateDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	IoscEvent event1 = IOSC_EVENT_INVALID;
	IoscEvent event2 = IOSC_EVENT_INVALID;

	event1 = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, event1, &state);
	if (event1 == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: create event1: FAILED");
		return state.error_code;
	}

	event2 = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, event2, &state);
	if (event2 == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: create event2: FAILED");
		iosc_destroy_event(event1);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS(event1, event2, &state);
	if (event1 != event2)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: check consistency of the two created event handles: FAILED");
		iosc_destroy_event(event1);
    iosc_destroy_event(event2);
    return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_destroy_event(event1), &state);
	if (state.error_code) 
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: destroy event1: FAILED");
		iosc_destroy_event(event2);
    return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_destroy_event(event2), &state);
	if (state.error_code) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: destroy event2: FAILED");
    return state.error_code;
	}

  return state.error_code;
}

OEDT_TEST(EventDoubleDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hEvent = IOSC_EVENT_INVALID;

  hEvent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hEvent, &state);
	if (hEvent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: create ewvent: FAILED");
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_destroy_event(hEvent), &state);
  if (state.error_code) 
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: destroy event: FAILED");
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS(IOSC_BADARG, iosc_destroy_event(hEvent), &state);
  if (state.error_code) 
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: second destroy on event handle: FAILED");
    return state.error_code;
	}

  return state.error_code;
}


/*****************************************************************************
* FUNCTION		:  vTestEventSetClearWait()
* PARAMETER		:  trfpEventIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test cases 32 to 37 w.r.to IOSC event Set/Clear
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/

OEDT_TEST(EventSetAndWaitForEvent_AND)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK), &state);
	if (state.error_code)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event: set event (complete bit matching): FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got,IOSC_EVENT_AND, IOSC_TIMEOUT_100), &state); 
	if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): wait for event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS(IOSC_TEST_EVENT_MASK, mask_got, &state);
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): check event bitmask: FAILED");
		iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state);
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventSetAndWaitForEvent_OR)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK_OR), &state);
	if (state.error_code)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): set event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got,IOSC_EVENT_OR, IOSC_TIMEOUT_100), &state);
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): wait for event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_TEST_EVENT_MASK_OR, mask_got, &state );
	if ( mask_got != IOSC_TEST_EVENT_MASK_OR )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): check event bitmask: FAILED");
		iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK << 1, &mask_got,IOSC_EVENT_OR, IOSC_TIMEOUT_100), &state );
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): wait for event with wrong bitmask: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventSetAndWaitForEvent_OR2)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK), &state);
	if (state.error_code)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): set event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK << 2, &mask_got, IOSC_EVENT_OR, IOSC_TIMEOUT_100), &state);
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): wait for event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventClear)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK),  &state);
	if (state.error_code)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: set event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK), &state);
  if (iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK) != IOSC_OK )
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: clear event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}

  mask_got = 0;
	
  OEDT_EXPECT_EQUALS( IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND, IOSC_TIMEOUT_100), &state );
	if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: wait for event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( 0, mask_got, &state);
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: check if returned bitmask is untouched: FAILED");
		iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent),  &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventWaitWithClearFlag)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK), &state);
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: set event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND | IOSC_EVENT_CLEAR, IOSC_TIMEOUT_100), &state);
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: wait for event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_TEST_EVENT_MASK, mask_got, &state ); 
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: check event bitmask: FAILED");
		iosc_destroy_event(hevent);
    return state.error_code;
	}

  mask_got = 0;

  OEDT_EXPECT_EQUALS(IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND, IOSC_TIMEOUT_100), &state);
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: wait for null event: FAILED");
    iosc_destroy_event(hevent);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( 0, mask_got, &state );
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: check if returned bitmask is untouched: FAILED");
		iosc_destroy_event(hevent);
    return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventClearBadArg)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_clear_event(hevent, IOSC_TEST_EVENT_NULL_MASK),  &state );
  if ( state.error_code ) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): clear event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventWaitBadArg)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;
  unsigned long mask_got = 0;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_NULL_MASK, &mask_got, IOSC_EVENT_OR | IOSC_EVENT_CLEAR, IOSC_TIMEOUT_100), &state );
  if ( state.error_code ) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): wait for event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): destroy event: FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(EventPerformSetAfterDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent),  &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: destroy event: FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK), &state );
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: set event: FAILED");
		return state.error_code;
	}

  return state.error_code;
}

OEDT_TEST(EventPerformClearAfterDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscEvent hevent = IOSC_EVENT_INVALID;

  hevent = iosc_create_event(TEST_EVENT_ID);
  OEDT_EXPECT_DIFFERS(IOSC_EVENT_INVALID, hevent, &state);
	if (hevent == IOSC_EVENT_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: create event: FAILED");
    iosc_destroy_event(hevent);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_event(hevent), &state);
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: destroy event: FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK), &state );
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: set event: FAILED");
		return state.error_code;
	}

  return state.error_code;
}



OEDT_TESTSTATE CreateAndDestroyEvents(tU32 u32Length, tBool bDifferentIDs)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	tU32 Cnt = 0;

  tU32 u32DifferentIDCoeff = (bDifferentIDs) ? 1 : 0;
  char* sInsertString = (bDifferentIDs) ? "different IDs" : "single ID";

  tBool bSuccess = TRUE;

  IoscEvent* hEvents = (IoscEvent*) malloc ( sizeof (IoscEvent) * u32Length );

  OEDT_EXPECT_FALSE(hEvents == NULL, &state);
  if (!hEvents)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to create the max number of event handles (%s): allocate memory for event handles: %s", sInsertString, "FAILED");	
    return state;
  }
	
	for (Cnt = 0; Cnt < u32Length; Cnt++)
		hEvents[Cnt]= iosc_create_event(TEST_EVENT_ID + (u32DifferentIDCoeff * Cnt) );

	for (Cnt = 0; Cnt < u32Length; Cnt++)
	{
    if ( hEvents[Cnt] == IOSC_EVENT_INVALID)
      bSuccess = FALSE;

    if ( hEvents[Cnt] != hEvents[u32Length - 1] && !bDifferentIDs)
      bSuccess = FALSE;

		if (iosc_destroy_event(hEvents[Cnt]) != IOSC_OK)
      bSuccess = FALSE;		
	}

  OEDT_EXPECT_TRUE(bSuccess, &state);

	if (!bSuccess)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (event) try to create the max number of event handles (%s): FAILED", sInsertString);	

  free(hEvents);

  return state;
}

OEDT_TEST(EventCreateMaxNumOfHandlesForOneEvent)
{
  return CreateAndDestroyEvents(IOSC_TEST_MAX_EVENT, FALSE).error_code;
}

OEDT_TEST(EventCreateMaxNumOfHandlesForDiffEvents)
{
  return CreateAndDestroyEvents(IOSC_TEST_MAX_EVENT, TRUE).error_code;
}
