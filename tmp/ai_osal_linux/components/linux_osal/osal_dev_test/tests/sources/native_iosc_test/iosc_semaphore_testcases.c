#include <iosc_semaphore_testcases.h>

/*****************************************************************************
* FUNCTION		:  vTestSemCreate()
* PARAMETER		:  trfpSemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test cases 22 to 23 w.r.to Semaphore
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/

tBool vDestroySemaphores(IoscSemaphore* Semaphores, tU32 u32Count)
{
  tU32 i;
  tBool bSuccess = TRUE;

  for (i = 0; i < u32Count; i++)
  {
    if (Semaphores[i] != IOSC_SEMAPHORE_INVALID)
    {
      if (iosc_destroy_semaphore(Semaphores[i]) != IOSC_OK)
        bSuccess = FALSE;
    }
  }

  return bSuccess;
}

IoscSemaphore* TestCreateSemaphores(tU32 u32Count, tBool bDifferentIDs, OEDT_TESTSTATE* state)
{
  tU32 i;
  tS32 s32SemID;
  tS32 s32SemInitValue;
  char* sInsertString;

  s32SemInitValue = (bDifferentIDs) ? 1 : u32Count;
  sInsertString = (bDifferentIDs) ? "different" : "equal";

  IoscSemaphore *Semaphores = (IoscSemaphore*)malloc (u32Count * sizeof (IoscSemaphore));
  OEDT_EXPECT_DIFFERS(NULL, Semaphores, state);
  if (!Semaphores)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) create semaphores with %s IDs: memory allocation for semaphores: %s", sInsertString, "FAILED");
    return NULL;
  }

  for (i = 0; i < u32Count; i++)
  {
    Semaphores[i] = IOSC_SEMAPHORE_INVALID;
  }

  for (i = 0; i < u32Count; i++)
	{
    s32SemID = (bDifferentIDs) ? TEST_SEMAPHORE + i : TEST_SEMAPHORE;
		Semaphores[i] = iosc_create_semaphore(s32SemID, s32SemInitValue);
		if (Semaphores[i] == IOSC_SEMAPHORE_INVALID)
		{
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) create semaphores with %s IDs: create semaphore no. %d:", i, "FAILED");
			vDestroySemaphores(Semaphores, u32Count);
      free(Semaphores);
      OEDT_ADD_FAILURE(state);
      return NULL;
		}
	}
  OEDT_ADD_SUCCESS(state);

  return Semaphores;
}

void vTestDestroySemaphores(IoscSemaphore* Semaphores, tU32 u32Count, OEDT_TESTSTATE* state)
{
  if (vDestroySemaphores(Semaphores, u32Count))
    OEDT_ADD_SUCCESS( state );
  else
  {
    OEDT_ADD_FAILURE( state );
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) delete semaphores: %s", "FAILED");
  }
}

void vTestObtainSemaphores(IoscSemaphore* Semaphores, tU32 u32Count, tBool ShouldBeObtainable, OEDT_TESTSTATE* state)
{
  tU32 i;
  char* sInsertString;
  tBool bSuccess = TRUE;
  tS32 s32ExpectedReturnValue;

  s32ExpectedReturnValue = (ShouldBeObtainable) ? IOSC_OK : IOSC_BADARG;
  sInsertString = (ShouldBeObtainable) ? "valid" : "invalid";

  for (i = 0; i < u32Count; i++)
  {
    if (iosc_obtain_semaphore(Semaphores[i], 1, IOSC_TIMEOUT_100) != s32ExpectedReturnValue)
      bSuccess = FALSE;
  }

  if (bSuccess)
    OEDT_ADD_SUCCESS( state );
  else
  { 
    OEDT_ADD_FAILURE( state );
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain %s semaphores: %s", sInsertString, "FAILED");
  }
}

OEDT_TEST(SemaphoreCreateWithDiffIDsAndObtainAfterDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore* Semaphores = TestCreateSemaphores(IOSC_TEST_MAX_SEM, FALSE, &state);

  if (Semaphores)
  {
    vTestDestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM, &state);
    vTestObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, FALSE, &state);
    free(Semaphores);
  }
  
  return state.error_code;
}

OEDT_TEST(SemaphoreCreateWithEqIDsAndObtainAfterDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore* Semaphores = TestCreateSemaphores(IOSC_TEST_MAX_SEM, TRUE, &state);

  if (Semaphores)
  {
    vTestDestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM, &state);
    vTestObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, FALSE, &state);
    free(Semaphores);
  }

  return state.error_code;
}

OEDT_TEST(SemaphoreCreateWithDiffIDsAndObtainBeforeDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore* Semaphores = TestCreateSemaphores(IOSC_TEST_MAX_SEM, FALSE, &state);

  if (Semaphores)
  {
    vTestObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, TRUE, &state);
    vTestDestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM, &state);
    free(Semaphores);
  }
  
  return state.error_code;
}

OEDT_TEST(SemaphoreCreateWithEqIDsAndObtainBeforeDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore* Semaphores = TestCreateSemaphores(IOSC_TEST_MAX_SEM, TRUE, &state);

  if (Semaphores)
  {
    vTestObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, TRUE, &state);
    vTestDestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM, &state);
    free(Semaphores);
  }
  
  return state.error_code;
}

OEDT_TEST(SemaphoreTryObtainWithValueNull)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();

  IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
  OEDT_EXPECT_DIFFERS( IOSC_SEMAPHORE_INVALID, semaphore, &state );
  if (semaphore == IOSC_SEMAPHORE_INVALID)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: create semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_TIMEOUT, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_100), &state );
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: obtain semaphore: %s", "FAILED");
    iosc_destroy_semaphore(semaphore);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_semaphore(semaphore), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: destroy semaphore: %s", "FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(SemaphoreTryObtainWithNegativeValue)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
  OEDT_EXPECT_DIFFERS( IOSC_SEMAPHORE_INVALID, semaphore, &state );
  if (semaphore == IOSC_SEMAPHORE_INVALID)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: create semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_obtain_semaphore(semaphore, -10, IOSC_TIMEOUT_100), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: obtain semaphore: %s", "FAILED");
    iosc_destroy_semaphore(semaphore);
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_semaphore(semaphore), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: destroy semaphore: %s", "FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(SemaphoreTryDoubleDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
  OEDT_EXPECT_DIFFERS( IOSC_SEMAPHORE_INVALID, semaphore, &state );
  if (semaphore == IOSC_SEMAPHORE_INVALID)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: create semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_semaphore(semaphore), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: destroy semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_destroy_semaphore(semaphore), &state );
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: second destroy did not return IOSC_BADARG: %s", "FAILED");
    return state.error_code;
  }

  return state.error_code;
}

OEDT_TEST(SemaphoreTryReleaseAfterDestroy)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
  OEDT_EXPECT_DIFFERS( IOSC_SEMAPHORE_INVALID, semaphore, &state );
  if (semaphore == IOSC_SEMAPHORE_INVALID)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: create semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_semaphore(semaphore), &state ); 
  if ( state.error_code )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: destroy semaphore: %s", "FAILED");
    return state.error_code;
  }

  OEDT_EXPECT_EQUALS( IOSC_BADARG, iosc_release_semaphore(semaphore, 10), &state ); 
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: release semaphore: %s", "FAILED");
		return state.error_code;
	}

  return state.error_code;
}

/*****************************************************************************
* FUNCTION		:  vTestSemObtain()
* PARAMETER		:  trfpSemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 26 w.r.to Semaphore obtain
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
OEDT_TEST(SemaphoreObtainAndRelease)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE, 10);
  OEDT_EXPECT_DIFFERS( IOSC_SEMAPHORE_INVALID, semaphore, &state );
	if (semaphore == IOSC_SEMAPHORE_INVALID)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: create semaphore: %s", "FAILED");
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_FOREVER), &state );
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS(IOSC_OK, iosc_release_semaphore(semaphore, 10), &state); 
	if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: release semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_release_semaphore(semaphore, 10), &state ); 
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: second release semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		return state.error_code;
	}

  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_FOREVER), &state );
  if ( state.error_code )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: second obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		return state.error_code;
	}
	
  OEDT_EXPECT_EQUALS( IOSC_OK, iosc_destroy_semaphore(semaphore), &state );
	if ( state.error_code )
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: destroy semaphore: %s", "FAILED");
		return state.error_code;
	}

  return state.error_code;
}
