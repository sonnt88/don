/******************************************************************************
 *FILE         : oedt_PWM_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for the PWM
 *               device	for GM-GE hardware.
 *              
 *AUTHOR(s)    :  Anoop Chandran (RBIN/ECM1)
 *HISTORY      :  
 					22-Dec-2008 Initial  Version v1.0 - Anoop Chandran
 *******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_pwm_fan_testfunc.h"
#include "gpio.h"



#define OEDT_FAN_GPIO     JV_IRQ_GPIO1_21

static tVoid u32PWMFan_EnableFAN_GPIO(tVoid);
static tVoid u32PWMFan_DisableFAN_GPIO(tVoid);
static tVoid u32PWMFan_SetFAN_GPIO(tBool b_enable);



static tVoid u32PWMFan_EnableFAN_GPIO(tVoid){
    u32PWMFan_SetFAN_GPIO(TRUE);
    return;
    
}
static tVoid u32PWMFan_DisableFAN_GPIO(tVoid){
    u32PWMFan_SetFAN_GPIO(FALSE);
    return;
    
}

static tVoid u32PWMFan_SetFAN_GPIO(tBool b_enable){
    
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    
    OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OEDT_FAN_GPIO;
    OSAL_trGPIOData Data;
    tU32 u32Ret = 0;
    /* Open the device */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
    if ( OSAL_ERROR == hFd ){
        u32Ret = 2;
    }else{
        /* Set device to output mode */
        if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                                               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) ){
            u32Ret = 3;
        }else{
            /* Set state of output pin to HIGH */
            Data.tId = DevID;
            Data.unData.bState = b_enable;
            if(b_enable == TRUE){
                u32Ret = (tU32) OSAL_s32IOControl ( hFd,
                                                    OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                                                    (tS32) Data.tId);
            }else{
                u32Ret = (tU32) OSAL_s32IOControl ( hFd,
                                                    OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,
                                                    (tS32) Data.tId);
            }
            
        }
        
        
        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 6;
            }
    }
    return;        
}

/*****************************************************************************
* FUNCTION:		u32PWMFanDevOpenClose()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_001
* DESCRIPTION:  Open and Close the PWM3 device
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
 ******************************************************************************/
tU32 u32PWMFanDevOpenClose(tVoid)
{
	OSAL_tIODescriptor hDevice 	= 0;
 	tU32 u32Ret 						= 0;
	tS32 s32Status 					= 0;
	OSAL_tenAccess enAccess 		= OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
    else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanDevOpenCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_002
* DESCRIPTION:  Attempt to Open and Close the PWM3 device with invalid
*				parameter
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanDevOpenCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 				   = 0;
	OSAL_tenAccess enAccess    = (OSAL_tenAccess)OSAL_C_PWM_INVALID_ACCESS_MODE;

 	/* Open the device in writeonly mode with invalid device name */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR != hDevice )
    {
		u32Ret = 1;
    	/* If successfully opened, indicate error and close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
	}// end of if ( OSAL_ERROR != hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanDevMultipleOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_003
* DESCRIPTION:  Attempt to Close the ILLUMINATION device which has already 
*				been closed
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanDevMultipleOpen(tVoid)	
{
	OSAL_tIODescriptor hDeviceOne = 0; 
	OSAL_tIODescriptor hDeviceTwo = 0;
 	tU32 u32Ret 						= 0;
	tS32 s32Status 					= 0;
	OSAL_tenAccess enAccess 		= OSAL_EN_WRITEONLY;

 	/* Open the first device in writeonly mode */
 	hDeviceOne = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PWM3 , enAccess );
   if ( OSAL_ERROR == hDeviceOne )
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}// end of switch(s32Status)
	}
   else
	{
		/* Open the device with out colsing 
			the first device in writeonly mode */
	 	hDeviceTwo = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PWM3, enAccess );
	    if ( OSAL_ERROR != hDeviceTwo )
	    {
			/* check the error status returned */
					u32Ret += 30;
	   		/* Close the second device */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )
			{
				u32Ret = 100;
			}// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )
	   	   
		 }//end of if ( OSAL_ERROR == hDeviceTwo )

		/* Close the first device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )
		{
			u32Ret += 500;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )

	}// end of if ( OSAL_ERROR == hDeviceOne )
   	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_004
* DESCRIPTION:  Attempt to Close the PWM device which has already 
*				been closed
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanDevCloseAlreadyClosed(tVoid)	
{
	OSAL_tIODescriptor hDevice = {0};
	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;

 	/* Open the first device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
    else
	{
	   	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}
		else
		{
			/* Try to close the device which allready Close  */
			if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret = 10;
			}
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
   	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanGetVersion() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_005
* DESCRIPTION:  Get device version
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanGetVersion(tVoid)
{
	OSAL_tIODescriptor hDevice 	= {0};
 	tU32 u32Ret 						= 0;
	tS32 s32Status 					= 0;
	tS32 s32VersionInfo 				= 0;
	OSAL_tenAccess enAccess 		= OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
		/* Get device version */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{   
			u32Ret = 5;
		}//end of if ( OSAL_ERROR == OSAL_s32IOControl())
				/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
	}//end of if ( OSAL_ERROR == hDevice )
 
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanGetVersionInvalParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_006
* DESCRIPTION:  Attempt to get device version with invalid parameters
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008 
******************************************************************************/
tU32 u32PWMFanGetVersionInvalParam(tVoid)
{
   OSAL_tIODescriptor hDevice = {0};
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
  	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
		/* Get device version */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,OSAL_NULL) )
		{   
			u32Ret = 5;
		}
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanSetPWMInRange()    
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_007
* DESCRIPTION:  Set PWM duty cycle with h minimum, medium, max permissible
* 				value of duty cycle value
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008 
******************************************************************************/
tU32 u32PWMFanSetPWMInRange( tVoid ) 
{
	OSAL_tIODescriptor hDevice = {0};
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
        
        u32PWMFan_EnableFAN_GPIO();
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN, enAccess );
        if ( OSAL_ERROR == hDevice )
            {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch( s32Status )
                    {
                    case OSAL_E_ALREADYEXISTS:
                        u32Ret = 1;
                        break;
                    case OSAL_E_UNKNOWN:
                        u32Ret = 2;
                        break;
                    default:
                        u32Ret = 3;
                    }
            }
	else
            {
		
                
                /*Set frequency as 1KHz(1KHz is dummy value) */	
                tPWMFreqDuty.hz = 1000;
		/* Set PWM duty cycle with value 0 */	
                tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MINVAL;
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                                                       OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret = 5;
		}
                OSAL_s32ThreadWait( 1000 );
		/* Set PWM duty cycle with value 32767 */	
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret += 10;
		}
                OSAL_s32ThreadWait( 1000 );
		/* Set PWM duty cycle with value 65535 */	
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MAXVAL;
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret += 20;
		}
                OSAL_s32ThreadWait( 1000 );
		/* Close the device */
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 50;
		}//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )


        u32PWMFan_DisableFAN_GPIO();
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanSetPWMOutRange()    
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_008
* DESCRIPTION:  Set PWM duty cycle with invalid value
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008 
******************************************************************************/
tU32 u32PWMFanSetPWMOutRange(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
   OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else
	{
		 /*Set frequency as 1KHz(1KHz is dummy value) */	
	    tPWMFreqDuty.hz = 1000;
		 /*set duty cycle as out of range*/
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_INVAL_MIN;

		/* Set PWM duty cycle with value -1*/	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret = 5;
		}
		 /*set duty cycle as out of range*/
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_INVAL_MAX;
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret += 20;
		}
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 50;
		}// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanGetPWMData()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_009
* DESCRIPTION:  Get PWM duty cycle 
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanGetPWMData(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	//tU32 u32DutyCycle 			= 0; lint
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		} //end of switch(s32Status)

    }
	else
	{
	
		/* Get PWM duty cycle value */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDuty ) )
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )


	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanGetPWMInvalParam()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_010
* DESCRIPTION:  Get PWM duty cycle with invalid parameters 
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanGetPWMInvalParam(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else
	{
		/* Get PWM duty cycle value */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_PWM_GET_PWM, OSAL_NULL ) )
		{
			u32Ret = 5;
		}

		/* Close the device */
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanSetGetPWM()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_011
* DESCRIPTION:  Set PWM duty cycle and frequency.
*					 Read back and verify if predetermined value
*	 				is correctly set.
* HISTORY:
							Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanSetGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice 			= 0;
 	tU32 u32Ret 								= 0;
	tS32 s32Status 							= 0;
	//tU32 u32DutyCycle 						= 0; lint
	OSAL_tenAccess enAccess 				= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty 		= {0,0};
	OSAL_trPWM_SetPwm tPWMFreqDutyGet 	= {0,0};
	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{
		 /*Set frequency as 1KHz(1KHz is dummy value) */	
	    tPWMFreqDuty.hz = 1000;
	    /* Set PWM duty cycle with value 32767 */	
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;

		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
		{
			u32Ret += 50;
		}
		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDutyGet  ) )
		{
			u32Ret += 500;
		}
		/*Verify if set value and re-read value are matching*/
		if
		(
			(tPWMFreqDutyGet.hz != tPWMFreqDuty.hz) 
			|| 
			(tPWMFreqDutyGet.percent_high !=tPWMFreqDuty.percent_high) 

			//(tPWMFreqDuty.percent_high-1<= tPWMFreqDutyGet.percent_high >=tPWMFreqDuty.percent_high+1) 
		)
		{
			u32Ret += 700;
		}
		OEDT_HelperPrintf
		(
			TR_LEVEL_USER_1, 
			"Frequancy = %d\n Duty Cycle = %d\n",
			tPWMFreqDuty.hz,
			tPWMFreqDuty.percent_high
		);
		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}

	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanInvalidSetGetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_012
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
* 				is correctly set
* HISTORY:	
					Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanInvalidSetGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice 			= 0;
 	tU32 u32Ret 								= 0;
	tS32 s32Status 							= 0;
	OSAL_tenAccess enAccess 				= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty 			= {0,0};
	OSAL_trPWM_SetPwm tPWMFreqDutyGet 			= {0,0};
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
	else
	{
		 /*Set frequency as 1KHz(1KHz is dummy value) */	
	    tPWMFreqDuty.hz = 1000;
	    /* Set PWM duty cycle with value 32767 */	
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;

		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
		{
			u32Ret += 5;
		}
	    
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_INVAL_MIN;
		/* Set PWM duty cycle with value -1 */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
		{
			u32Ret += 50;
		}// end of if ( OSAL_ERROR != OSAL_s32IOControl())

	    tPWMFreqDuty.hz = 1000;
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;
		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDutyGet  ) )
		{
			u32Ret += 500;
		}
		/*Verify if set value and re-read value are matching*/
		if
		(
			(tPWMFreqDutyGet.hz != tPWMFreqDuty.hz) 
			|| 
			(tPWMFreqDutyGet.percent_high != tPWMFreqDuty.percent_high) 
		)
		{
			u32Ret += 700;
		}
		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}//end of if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )

		 
	}//end of if ( OSAL_ERROR == hDevice )


	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32PWMFanInvalFuncParamToIOCtrl()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_013
* DESCRIPTION:  Attempt to perform IOCtrl opertion with invalid parameter
* HISTORY:		Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanInvalFuncParamToIOCtrl(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	tU32 u32Version 				= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	
   /* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
	else
	{
		/* IOCtrl operation with invalid parameter */	
		if (OSAL_ERROR != OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_INVAL_FUNC_PWM,(tS32)&u32Version))
		{
			u32Ret = 5;
		}
	   		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanMultSetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_014
* DESCRIPTION:  Multiple setting of the PWM 
* HISTORY:	
				Created by Anoop Chandran (RBEI/ECM1)22-Dec-2008
******************************************************************************/
tU32 u32PWMFanMultSetPWM(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	tS32 s32Count 					= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
   /* Open the device in writeonly mode */
 	hDevice 
 	=
 	OSAL_IOOpen
 	(
 		OSAL_C_STRING_DEVICE_TIMER_FAN ,
 		enAccess 
 	);
	if ( OSAL_ERROR == hDevice )
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
	}
	else
	{
		/*Set frequency as 1KHz(1KHz is dummy value) */	
		tPWMFreqDuty.hz = 1000;
		/* Set PWM duty cycle with value 32767 */	
		tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;

		/* Multiple setting of the PWM for illumination with delay */	
		for (; s32Count <=100; ++s32Count)
		{
			s32Status 
			= 
			OSAL_s32IOControl 
			( 
				hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, 
				(tS32)&tPWMFreqDuty 
			);
			if (s32Status == OSAL_ERROR)
			{
					u32Ret = (tU32)((tU32)20 + (tU32)s32Count);
					break;
			}
			OSAL_s32ThreadWait(10);
		}
		/* Multiple setting of the PWM for illumination with out delay */	
		for (s32Count = 0; s32Count <=100; ++s32Count)
		{
			s32Status 
			= 
			OSAL_s32IOControl 
			( 
				hDevice,
				OSAL_C_S32_IOCTRL_PWM_SET_PWM, 
				(tS32)&tPWMFreqDuty 
			);
			if (s32Status == OSAL_ERROR)
			{
					u32Ret = (tU32)((tU32)50 + (tU32)s32Count);
					break;
			}
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 1000;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PWMFanIOControlsAfterClose() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_015
* DESCRIPTION:  	1) open the device
*						2) close the device
*						3) Get device version 
*						4)	Set PWM duty cycle
*						5)	Get the PWM duty cycle
* HISTORY:	  
				Created by Anoop Chandran (RBEI/ECM1)22-Dec-2008
******************************************************************************/
tU32 u32PWMFanIOControlsAfterClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	tS32 s32VersionInfo 			= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;
	//OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0}; lint
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
	  	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

		/* Get device version */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{   
			u32Ret += 50;
		}//end of if ( OSAL_ERROR == OSAL_s32IOControl())

		 
	}//end of if ( OSAL_ERROR == hDevice )
 
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PWMFanSetGetInval()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_016
* DESCRIPTION:  Try Set/get PWM3 with OSAL_C_S32_IOCTRL_ILLUMINATION_GET and 
*					OSAL_C_S32_IOCTRL_ILLUMINATION_SET.
* HISTORY:
							Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMFanSetGetInval(tVoid) 
{
	OSAL_tIODescriptor hDevice 			= 0;
 	tU32 u32Ret 								= 0;
	tS32 s32Status 							= 0;
	//tU32 u32DutyCycle 						= 0; lint
	OSAL_tenAccess enAccess 				= OSAL_EN_WRITEONLY;
	OSAL_trPWM_SetPwm tPWMFreqDuty 			= {0,0};
	OSAL_trPWM_SetPwm tPWMFreqDutyGet 			= {0,0};
	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{
		 /*Set frequency as 1KHz(1KHz is dummy value) */	
	    tPWMFreqDuty.hz = 1000;
	    /* Set PWM duty cycle with value 32767 */	
		 tPWMFreqDuty.percent_high = OEDT_PWM_DUTYCYCLE_MIDVAL;
		/* try to set with OSAL_C_S32_IOCTRL_ILLUMINATION_SET */ 
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_SET, (tS32)&tPWMFreqDuty ))
		{
			u32Ret += 50;
		}
		/* try to set with OSAL_C_S32_IOCTRL_ILLUMINATION_GET */ 	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&tPWMFreqDutyGet  ) )
		{
			u32Ret += 500;
		}
		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}

	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}

/************** End of Test Code developed by RBEI*****************/  //   @Anoop
