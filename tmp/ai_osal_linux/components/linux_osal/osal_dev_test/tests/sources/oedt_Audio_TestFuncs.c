/******************************************************************************
*FILE         : oedt_Audio_TestFuncs.c
*
*SW-COMPONENT : OEDT_FrmWrk
*
*DESCRIPTION  : This file implements the individual test cases for the
*               Acoustic device
*               Checking for valid pointer in each test cases is not required
*               as it is ensured in the calling function that that it cannot
*               be NULL.
*
*AUTHOR       : Bernd Schubart, 3SOFT
*
*COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
*
*HISTORY:       02.05.06  3SOFT-Schubart
*               Modified Acousticin Basic Tests, added new Acousticin testcases
*               25.07.05  3SOFT-Schubart
*               Initial Revision.
*
*****************************************************************************/
  

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

//#include "osal_oedt_audio_helper/oedt_Audio_HelperFuncs.h"
//#include "osal_oedt_audio_helper/oedt_Audio_HelperTrace.h"
#include "oedt_Audio_HelperFuncs.h"
#include "oedt_Audio_HelperTrace.h"
#include "oedt_Audio_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#if 0
#define OEDT_AUDIO_TEST_WAV_FILE        ((tU8*) "file:/uda1/oedt/wav_test.wav")
#define OEDT_AUDIO_TEST_MP3_FILE        ((tU8*) "file:/uda1/oedt/mp3_test.mp3")
#endif
/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/



/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

#if 0
tU32 u32_oedt_audio_test_mp3_out_codec(tVoid){
    
    enAudioTestError en_audio_ret = OEDT_AUDIO_TEST_RES_ERROR;
    tU32 u32_status = 0;
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_mp3_out_codec entered",0,0,0,0);
    
    en_audio_ret = en_oedt_audio_reset_CS42888();
    if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
        
        en_audio_ret =  fg_start_test(OEDT_AUDIO_TEST_MP3_FILE);
        if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
            OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"fg_start_test successfull",en_audio_ret ,0,0,0);
        }else{

            fg_end_test();
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"fg_start_test failed",en_audio_ret ,0,0,0);
            u32_status = 2;
        }


    }else{
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"u32_oedt_audio_test_mp3_out_codec error",en_audio_ret ,0,0,0);
        u32_status = 1;
    }
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_mp3_out_codec left",0,0,0,0);
    return u32_status;
    
}

tU32 u32_oedt_audio_test_wav_out_codec(tVoid){


    enAudioTestError en_audio_ret = OEDT_AUDIO_TEST_RES_ERROR;
    tU32 u32_status = 0;
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_wav_out_codec entered",0,0,0,0);

    en_audio_ret = en_oedt_audio_reset_CS42888();
    if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){

        en_audio_ret =  fg_start_test(OEDT_AUDIO_TEST_WAV_FILE);
        if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
            OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"fg_start_test successfull",en_audio_ret ,0,0,0);
        }else{
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"fg_start_test failed",en_audio_ret ,0,0,0);
            fg_end_test();
            u32_status = 2;
        }
    }else{
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"u32_oedt_audio_test_wav_out_codec error",en_audio_ret ,0,0,0);
        u32_status = 1;
    }
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_wav_out_codec left",0,0,0,0);
    return u32_status;
}


tU32 u32_oedt_audio_cyrrus_setup(tVoid){
    enAudioTestError en_audio_ret = OEDT_AUDIO_TEST_RES_ERROR;
    tU32 u32_status = 0;
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_cyrrus_setup entered",0,0,0,0);

    en_audio_ret = en_oedt_audio_reset_CS42888();
    if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
            OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"en_oedt_audio_reset_CS42888 successfull",en_audio_ret ,0,0,0);
    }else{
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"u32_oedt_audio_cyrrus_setup error",en_audio_ret ,0,0,0);
        u32_status = 1;
    }
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_cyrrus_setup left",0,0,0,0);
    return u32_status;
    
    
    
}



tU32 u32_oedt_audio_test_voice_out_codec(tVoid){

    enAudioTestError en_audio_ret = OEDT_AUDIO_TEST_RES_ERROR;
    tU32 u32_status = 0;
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_voice_out_codec entered",0,0,0,0);

    en_audio_ret = en_oedt_audio_reset_CS42888();
    if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){

        // en_audio_ret =  fg_setup_route(6);
        if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
            OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"fg_setup_route successfull",en_audio_ret ,0,0,0);


            en_audio_ret =  en_oedt_audio_VoiceOut();
            if(en_audio_ret == OEDT_AUDIO_TEST_RES_OK){
                OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"en_oedt_audio_VoiceOut successfull",en_audio_ret ,0,0,0);
            }else{
                OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"en_oedt_audio_VoiceOut failed",en_audio_ret ,0,0,0);
                fg_end_test();
                u32_status = 2;
            }


        }else{
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"fg_start_test failed",en_audio_ret ,0,0,0);
            fg_end_test();
            u32_status = 2;
        }
    }else{
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"u32_oedt_audio_test_voice_out_codec error",en_audio_ret ,0,0,0);
        u32_status = 1;
    }
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"u32_oedt_audio_test_voice_out_codec left",0,0,0,0);
    return u32_status;

}

#endif


/*****************************************************************************
* FUNCTION:		u32_oedt_audio_test_open_close_devs()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Open AUDIO Devices
*				        2. Close AUDIO Devices
*
* HISTORY:		Created by Martin Langer (CM-AI/PJ-CF33) on  2011-09-14
*               
******************************************************************************/
tU32 u32_oedt_audio_test_open_close_devs(void)
{
  tU32 u32Status = 0;
	tU32 u32Ret = 0;
	int i=0;
	
	struct oedt_device_test audio_device[] = {
    {OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH,      OSAL_OK},
    {OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO,   OSAL_OK},
    {OEDT_STRING_DEVICE_ACOUSTICIN,                   OSAL_E_DOESNOTEXIST},
    {OEDT_STRING_DEVICE_ACOUSTICOUT,                  OSAL_E_DOESNOTEXIST},    
		{(tCString)NULL, (tU32)NULL}
	};
	
	for (i=0; audio_device[i].dev_name!=NULL; i++)
	{
		u32Status = u32OEDT_OpenCloseDevice( audio_device[i].dev_name, audio_device[i].ret_val, OSAL_EN_READWRITE );
		if(u32Status != 0)
		{
			u32Ret += u32Status + i*100;
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32OEDT_OpenCloseDevice() return value '%i' using OSAL_EN_READWRITE mode on device '%s'", u32Status, audio_device[i].dev_name);	
			return u32Ret;
		}
    
		u32Status = u32OEDT_OpenCloseDevice( audio_device[i].dev_name, audio_device[i].ret_val, OSAL_EN_READONLY );
		if(u32Status != 0)
		{
			u32Ret += u32Status + i*1000;
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32OEDT_OpenCloseDevice() return value '%i' using OSAL_EN_READONLY mode on device '%s'", u32Status, audio_device[i].dev_name);	
			return u32Ret;
		}
    
		u32Status = u32OEDT_OpenCloseDevice( audio_device[i].dev_name, audio_device[i].ret_val, OSAL_EN_WRITEONLY );
		if(u32Status != 0)
		{
			u32Ret += u32Status + i*10000;
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32OEDT_OpenCloseDevice() return value '%i' using OSAL_EN_WRITEONLY mode on device '%s'", u32Status, audio_device[i].dev_name);	
			return u32Ret;
		}
	}
	
	return u32Ret;
}





