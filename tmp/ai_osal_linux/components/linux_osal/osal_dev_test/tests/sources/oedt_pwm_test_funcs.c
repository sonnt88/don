/******************************************************************************
 *FILE         : oedt_pwm_testfunc.c
 *
 *SW-COMPONENT : OEDT_FrmWrk
 *
 *DESCRIPTION  : This file implements the individual test cases for the PWM
 *               device.
 *
 *AUTHOR(s)    :  Anoop Chandran (RBIN/ECF1)
 *HISTORY      :
                    7-7-2008 Initial  Version v1.0
                    Haribabu Sannapaneni (RBEI/ECF1)
                    25-03-2010 Added test cases for PWM Version 1.1
 *******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_pwm_test_funcs.h"
extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...);


/*****************************************************************************
* FUNCTION:     u32PWMDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Open and Close the PWM3 device
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
 ******************************************************************************/
tU32 u32PWMDevOpenClose(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice  = 0;
    tU32 u32Ret                         = 0;
    tS32 s32Status                  = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_WRITEONLY;

    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)
    }
    else
    {
        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret = 5;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMDevOpenCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Attempt to Open and Close the PWM3 device with invalid
*               parameter
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMDevOpenCloseInvalParam(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret                    = 0;
    OSAL_tenAccess enAccess    = (OSAL_tenAccess)OSAL_C_PWM_INVALID_ACCESS_MODE;

    /* Open the device in writeonly mode with invalid device name */
    hDevice = OSAL_IOOpen( (tCString)ps8Channel_name , enAccess );
    if ( OSAL_ERROR != hDevice )
    {
        u32Ret = 1;
        /* If successfully opened, indicate error and close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 2;
        }
    }// end of if ( OSAL_ERROR != hDevice )

    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMDevMultipleOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Attempt to Close the ILLUMINATION device which has already
*               been closed
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMDevMultipleOpen(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDeviceOne = 0;
    OSAL_tIODescriptor hDeviceTwo = 0;
    tU32 u32Ret                         = 0;
    tS32 s32Status                  = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_WRITEONLY;

    /* Open the first device in writeonly mode */
    hDeviceOne = OSAL_IOOpen( (tCString) ps8Channel_name , enAccess );
   if ( OSAL_ERROR == hDeviceOne )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }// end of switch(s32Status)
    }
   else
    {
        /* Open the device with out colsing
            the first device in writeonly mode */
        hDeviceTwo = OSAL_IOOpen( (tCString)ps8Channel_name, enAccess );
        if ( OSAL_ERROR != hDeviceTwo )
        {
            /* check the error status returned */
                    u32Ret += 30;
            /* Close the second device */
            if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )
            {
                u32Ret = 100;
            }// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )

         }
         else
         {

            /* check the error status returned */
            s32Status = (tS32) OSAL_u32ErrorCode();
            switch(s32Status)
            {
                case OSAL_E_ALREADYEXISTS:
                    u32Ret = 0;
                    break;
                case OSAL_E_ALREADYOPENED:
                    u32Ret = 0;
                    break;
                case OSAL_E_UNKNOWN:
                    u32Ret = 50;
                    break;
                default:
                    u32Ret = 1000;
            }// end of switch(s32Status)

         }//end of if ( OSAL_ERROR == hDeviceTwo )

        /* Close the first device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )
        {
            u32Ret += 500;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )

    }// end of if ( OSAL_ERROR == hDeviceOne )
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Attempt to Close the PWM device which has already
*               been closed
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMDevCloseAlreadyClosed(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice = {0};
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;

    /* Open the first device in writeonly mode */
    hDevice = OSAL_IOOpen( (tCString)ps8Channel_name, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }
    }
    else
    {
        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret = 5;
        }
        else
        {
            /* Try to close the device which allready Close  */
            if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
            {
                u32Ret = 10;
            }
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMGetVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Get device version
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMGetVersion(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice  = {0};
    tU32 u32Ret                         = 0;
    tS32 s32Status                  = 0;
    tS32 s32VersionInfo                 = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_WRITEONLY;

    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)

    }
    else
    {
        /* Get device version */
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
        {
            u32Ret = 5;
        }//end of if ( OSAL_ERROR == OSAL_s32IOControl())
                /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 10;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
    }//end of if ( OSAL_ERROR == hDevice )

    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMGetVersionInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Attempt to get device version with invalid parameters
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMGetVersionInvalParam(const tS8 * ps8Channel_name)
{
   OSAL_tIODescriptor hDevice = {0};
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;

    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen( (tCString)ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
    /* check the error status returned */
    s32Status = (tS32) OSAL_u32ErrorCode();
    switch(s32Status)
    {
        case OSAL_E_ALREADYEXISTS:
            u32Ret = 1;
            break;
        case OSAL_E_UNKNOWN:
            u32Ret = 2;
            break;
        default:
            u32Ret = 3;
    }//end of switch(s32Status)

    }
    else
    {
        /* Get device version */
        if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_VERSION,OSAL_NULL) )
        {
            u32Ret = 5;
        }
        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 10;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )

    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMSetPWMInRange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Set PWM duty cycle with h minimum, medium, max permissible
*               value of duty cycle value
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMSetPWMInRange( const tS8 * ps8Channel_name, tS32 s32Freuency,tS32 S32MInValue,
                                  tS32 s32MidValue, tS32 MaxValue )
{
    OSAL_tIODescriptor hDevice = {0};
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;
    OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen( (tCString)ps8Channel_name, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch( s32Status )
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }
    }
    else
    {
         /*Set frequency (Hz is dummy value) */
        tPWMFreqDuty.hz = s32Freuency;

        /* Set PWM duty cycle with value  */
         tPWMFreqDuty.percent_high = S32MInValue;
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret = 5;
        }
        /* Set PWM duty cycle with value  */
         tPWMFreqDuty.percent_high = s32MidValue;
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret += 10;
        }
        /* Set PWM duty cycle with value  */
         tPWMFreqDuty.percent_high = MaxValue;
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret += 20;
        }

        /* Close the device */
        if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 50;
        }//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )

    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMSetPWMOutRange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Set PWM duty cycle with invalid value
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMSetPWMOutRange(const tS8 * ps8Channel_name, tS32 s32Freuency,tS32 S32InValMin,
                                  tS32 InValMax)
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
   OSAL_tenAccess enAccess  = OSAL_EN_WRITEONLY;
    OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)

    }
    else
    {
         /*Set frequency (Hz is dummy value) */
        tPWMFreqDuty.hz = s32Freuency;
         /*set duty cycle as out of range*/
         tPWMFreqDuty.percent_high = S32InValMin;

        /* Set PWM duty cycle with value*/
        if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret = 5;
        }
         /*set duty cycle as out of range*/
         tPWMFreqDuty.percent_high = InValMax;
        if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret += 20;
        }
        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 50;
        }// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMGetPWMData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Get PWM duty cycle
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMGetPWMData(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    //tU32 u32DutyCycle             = 0; lint
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;
    OSAL_trPWM_SetPwm tPWMFreqDuty = {0,0};
    /* Open the device in readonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        } //end of switch(s32Status)

    }
    else
    {

        /* Get PWM duty cycle value */
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDuty ) )
        {
            u32Ret = 5;
        }

        /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 10;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )


    }//end of if ( OSAL_ERROR == hDevice )

    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMGetPWMInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Get PWM duty cycle with invalid parameters
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMGetPWMInvalParam(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;

    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)

    }
    else
    {
        /* Get PWM duty cycle value */
        if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
                OSAL_C_S32_IOCTRL_PWM_GET_PWM, OSAL_NULL ) )
        {
            u32Ret = 5;
        }

        /* Close the device */
        if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 10;
        }//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMSetGetPWM()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Set PWM duty cycle and frequency.
*                    Read back and verify if predetermined value
*                   is correctly set.
* HISTORY:
                            Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMSetGetPWM(const tS8 * ps8Channel_name, tS32 s32Freuency, tS32 s32DutyCycle)
{
    OSAL_tIODescriptor hDevice          = 0;
    tU32 u32Ret                                 = 0;
    tS32 s32Status                          = 0;
    //tU32 u32DutyCycle                         = 0; lint
    OSAL_tenAccess enAccess                 = OSAL_EN_WRITEONLY;
    OSAL_trPWM_SetPwm tPWMFreqDuty      = {0,0};
    OSAL_trPWM_SetPwm tPWMFreqDutyGet   = {0,0};
    /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret += 10;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret += 20;
                break;
            default:
                u32Ret += 30;
        }//end of switch(s32Status)
    }
    else
    {
         /*Set frequency (Hz is dummy value) */
        tPWMFreqDuty.hz = s32Freuency;
        /* Set PWM duty cycle with value  */
         tPWMFreqDuty.percent_high = s32DutyCycle;

        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
        {
            u32Ret += 50;
        }
        /* Now, read PWM duty cycle value again */
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDutyGet  ) )
        {
            u32Ret += 500;
        }
        /*Verify if set value and re-read value are matching*/
        if
        (
            (tPWMFreqDutyGet.hz != tPWMFreqDuty.hz)
            ||
            (tPWMFreqDutyGet.percent_high !=tPWMFreqDuty.percent_high)

            //(tPWMFreqDuty.percent_high-1<= tPWMFreqDutyGet.percent_high >=tPWMFreqDuty.percent_high+1)
        )
        {
            u32Ret += 700;
        }
        OEDT_HelperPrintf
        (
            TR_LEVEL_USER_1,
            "Frequancy = %d\n Duty Cycle = %d\n",
            tPWMFreqDuty.hz,
            tPWMFreqDuty.percent_high
        );
        /* Close the device */
        if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
        {
            u32Ret += 900;
        }


    }//end of if ( OSAL_ERROR == hDevice )

    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMInvalidSetGetPWM()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
*               is correctly set
* HISTORY:
                    Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMInvalidSetGetPWM(const tS8 * ps8Channel_name,tS32 s32Freuency, tS32 s32DutyCycle,
                                     tS32 s32DutyCycleInval)
{
    OSAL_tIODescriptor hDevice          = 0;
    tU32 u32Ret                                 = 0;
    tS32 s32Status                          = 0;
    OSAL_tenAccess enAccess                 = OSAL_EN_WRITEONLY;
    OSAL_trPWM_SetPwm tPWMFreqDuty          = {0,0};
    OSAL_trPWM_SetPwm tPWMFreqDutyGet           = {0,0};
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)
    }
    else
    {
         /*Set frequency (Hz is dummy value) */
        tPWMFreqDuty.hz = s32Freuency;
        /* Set PWM duty cycle with value  */
         tPWMFreqDuty.percent_high = s32DutyCycle;

        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
        {
            u32Ret += 5;
        }

         tPWMFreqDuty.percent_high = s32DutyCycleInval;
        /* Set PWM duty cycle with Inval value */
        if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_PWM_SET_PWM, (tS32)&tPWMFreqDuty ))
        {
            u32Ret += 50;
        }// end of if ( OSAL_ERROR != OSAL_s32IOControl())

        tPWMFreqDuty.hz = s32Freuency;
         tPWMFreqDuty.percent_high = s32DutyCycle;
        /* Now, read PWM duty cycle value again */
        if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
                    OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&tPWMFreqDutyGet  ) )
        {
            u32Ret += 500;
        }
        /*Verify if set value and re-read value are matching*/
        if
        (
            (tPWMFreqDutyGet.hz != tPWMFreqDuty.hz)
            ||
            (tPWMFreqDutyGet.percent_high != tPWMFreqDuty.percent_high)
        )
        {
            u32Ret += 700;
        }
        /* Close the device */
        if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
        {
            u32Ret += 900;
        }//end of if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )


    }//end of if ( OSAL_ERROR == hDevice )


    return u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32PWMInvalFuncParamToIOCtrl()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Attempt to perform IOCtrl opertion with invalid parameter
* HISTORY:      Created by Anoop Chandran (RBIN/ECM1) on 22-Dec-2008
******************************************************************************/
tU32 u32PWMInvalFuncParamToIOCtrl(const tS8 * ps8Channel_name)
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret                     = 0;
    tS32 s32Status              = 0;
    tU32 u32Version                 = 0;
    OSAL_tenAccess enAccess     = OSAL_EN_WRITEONLY;

   /* Open the device in writeonly mode */
    hDevice = OSAL_IOOpen((tCString) ps8Channel_name , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
        /* check the error status returned */
        s32Status = (tS32) OSAL_u32ErrorCode();
        switch(s32Status)
        {
            case OSAL_E_ALREADYEXISTS:
                u32Ret = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Ret = 2;
                break;
            default:
                u32Ret = 3;
        }//end of switch(s32Status)
    }
    else
    {
        /* IOCtrl operation with invalid parameter */
        if (OSAL_ERROR != OSAL_s32IOControl (hDevice,
                    OSAL_C_S32_IOCTRL_INVAL_FUNC_PWM,(tS32)&u32Version))
        {
            u32Ret = 5;
        }
            /* Close the device */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
        {
            u32Ret += 10;
        }//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

    }//end of if ( OSAL_ERROR == hDevice )
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMChanelOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_001
* DESCRIPTION:  Open and Close all the PWM channels.
* HISTORY:
 ******************************************************************************/
tU32 u32PWMChanelOpenClose(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;

    /* Open and Close PWM channel 0 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM0);
    if (u32Status > 0)
    {
    u32Ret = 1;
    }


    /* Open and Close PWM channel 1 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM1);
    if (u32Status > 0)
    {
         u32Ret += 2;
    }


    /* Open and Close PWM channel 2 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM2);
    if (u32Status > 0)
    {
     u32Ret += 5;
    }


    /* Open and Close PWM channel 3 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM3);
    if (u32Status > 0)
    {
     u32Ret += 10;
    }

    /* Open and Close PWM channel 4 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM4);
    if (u32Status > 0)
    {
     u32Ret += 20;
    }

    /* Open and Close PWM channel 5 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM5);
    if (u32Status > 0)
    {
     u32Ret += 50;
    }

    /* Open and Close PWM channel 6 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM6);
    if (u32Status > 0)
    {
     u32Ret += 100;
    }

    /* Open and Close PWM channel 7 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM7);
    if (u32Status > 0)
    {
     u32Ret += 200;
    }
    /* Open and Close PWM channel 8 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM8);
    if (u32Status > 0)
    {
     u32Ret += 500;
    }


    /* Open and Close PWM channel 9 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM9);
    if (u32Status > 0)
    {
     u32Ret += 1000;
    }

    /* Open and Close PWM channel 10 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM10);
    if (u32Status > 0)
    {
     u32Ret += 2000;
    }

    /* Open and Close PWM channel 11 */
    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM11);
    if (u32Status > 0)
    {
     u32Ret += 5000;
    }

    /* Open and Close PWM channel 12 */

    u32Status = u32PWMDevOpenClose(OSAL_C_STRING_DEVICE_PWM12);
    if (u32Status > 0)
    {
     u32Ret += 10000;
    }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelMultipleOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_002
* DESCRIPTION:  Attempt to Open the PWM channel which is already opened.
*
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelMultipleOpen(tVoid)
{
    tU32 u32Ret                         = 0;
    u32Ret =(tU32) u32PWMDevMultipleOpen(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_003
* DESCRIPTION:  Attempt to Close the PWM device which is already closed
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelCloseAlreadyClosed(tVoid)
{
    tU32 u32Ret                     = 0;
   u32Ret =(tU32) u32PWMDevCloseAlreadyClosed(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelGetVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_004
* DESCRIPTION:  Get device version
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelGetVersion(tVoid)
{
    tU32 u32Ret                         = 0;
    u32Ret =(tU32) u32PWMGetVersion(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelGetVersionInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_005
* DESCRIPTION:  Attempt to get the device version with invalid parameters
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelGetVersionInvalParam(tVoid)
{
    tU32 u32Ret                     = 0;
    u32Ret =(tU32) u32PWMGetVersionInvalParam(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelSetPWMInRange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_006
* DESCRIPTION:  Set PWM duty cycle with h minimum, medium, max permissible
*               value of duty cycle value
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelSetPWMInRange( tVoid )
{
    tU32 u32Ret                     = 0;
    u32Ret =(tU32) u32PWMSetPWMInRange
                (
                    OSAL_C_STRING_DEVICE_PWM0,
                    OEDT_PWM_FREQUENCY,
                    OEDT_PWM_DUTYCYCLE_MINVAL ,
                    OEDT_PWM_DUTYCYCLE_MIDVAL,
                    OEDT_PWM_DUTYCYCLE_MAXVAL
                );
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelSetPWMOutRange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_007
* DESCRIPTION:  Set PWM duty cycle with invalid value
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelSetPWMOutRange(tVoid)
{
    tU32 u32Ret                     = 0;
    u32Ret =(tU32) u32PWMSetPWMOutRange
                (
                    OSAL_C_STRING_DEVICE_PWM0,
                    OEDT_PWM_FREQUENCY,
                    OEDT_PWM_DUTYCYCLE_INVAL_MAX,
                    OEDT_PWM_DUTYCYCLE_INVAL_MIN
                );
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMGetPWMData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_008
* DESCRIPTION:  Get PWM duty cycle
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelGetPWMData(tVoid)
{
    tU32 u32Ret                     = 0;
    u32Ret =(tU32) u32PWMGetPWMData(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMChannelGetPWMInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_09
* DESCRIPTION:  Get PWM duty cycle with invalid parameters
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelGetPWMInvalParam(tVoid)
{
    tU32 u32Ret                     = 0;
    u32Ret =(tU32) u32PWMGetPWMInvalParam(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32PWMChannelSetGetPWM()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_010
* DESCRIPTION:  Set PWM duty cycle and frequency.
*                    Read back and verify if predetermined value
*                   is correctly set.
* HISTORY:

******************************************************************************/
tU32 u32PWMChannelSetGetPWM(tVoid)
{
    tU32 u32Ret                                 = 0;
    u32Ret =(tU32) u32PWMSetGetPWM
                (
                    OSAL_C_STRING_DEVICE_PWM0,
                    OEDT_PWM_FREQUENCY,
                    OEDT_PWM_DUTYCYCLE_MIDVAL
                );
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32PWMChannelInvalidSetGetPWM()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_011
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
*               is correctly set
* HISTORY:

******************************************************************************/
tU32 u32PWMChannelInvalidSetGetPWM(tVoid)
{
    tU32 u32Ret                                 = 0;
    u32Ret =(tU32) u32PWMInvalidSetGetPWM
                (
                    OSAL_C_STRING_DEVICE_PWM0,
                    OEDT_PWM_FREQUENCY,
                    OEDT_PWM_DUTYCYCLE_MIDVAL,
                    OEDT_PWM_DUTYCYCLE_INVAL_MAX
                );
    return u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32PWMChannelInvalFuncParamToIOCtrl()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_PWM_012
* DESCRIPTION:  Attempt to perform IOCtrl opertion with invalid parameter
* HISTORY:
******************************************************************************/
tU32 u32PWMChannelInvalFuncParamToIOCtrl(tVoid)
{
    tU32 u32Ret                     = 0;

    u32Ret =(tU32) u32PWMInvalFuncParamToIOCtrl(OSAL_C_STRING_DEVICE_PWM0);
    return u32Ret;
}

/************** End of Test Code developed by RBEI*****************/  //   @Anoop
