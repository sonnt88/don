/******************************************************************************
 *FILE         : oedt_Acoustic_TestFuncs.c
 *
 *SW-COMPONENT : ODT_FrmWrk
 *
 *DESCRIPTION  : This file implements the individual test cases for the
 *               Acoustic device
 *               Checking for valid pointer in each test cases is not required
 *               as it is ensured in the calling function that that it cannot
 *               be NULL.
 *
 *AUTHOR       : Bernd Schubart, 3SOFT
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
 *
 *HISTORY:       02.05.06  3SOFT-Schubart
 *               Modified Acousticin Basic Tests, added new Acousticin testcases
 *               25.07.05  3SOFT-Schubart
 *               Initial Revision.
 *
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "acousticout.h"
#include "oedt_Acoustic_TestFuncs.h"

#include "odt_Globals.h"
#include "odt_ErrorCodes.h"
#include "odt_LinkedList.h"

#include "oedt_Types.h"


/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/* acousticout testfiles */
#define C_SZ_ACOUT_PCMTESTFILE               "/dev/nor0/RECORD.PCM"

/* open-string */
#define C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH

/* defines for PCM tests*/
#define C_U32_PCM_SAMPLERATE                 ((tU32)44100)
#define C_U32_PCM_CHUNKSIZE                  ((tU32)8192)

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/


tS32 s32AcousticTestInit()
{

   tS32 s32Ret=0;

   return s32Ret;
}

tS32 s32AcousticTestDeInit()
{

   tS32 s32Ret=0;

   return s32Ret;
}


tVoid vAcousticoutCleanup(OSAL_tIODescriptor hAcousticout,
                          OSAL_tIODescriptor hAudiorouter,
                          OSAL_tIODescriptor hFile)
{
   if(hAcousticout)
   {
      OSAL_s32IOClose(hAcousticout);
      hAcousticout = NULL;
   }

   if(hAudiorouter)
   {
      OSAL_s32IOClose(hAudiorouter);
      hAudiorouter = NULL;
   }

   if(hFile)
   {
      OSAL_s32IOClose(hFile);
      hFile = NULL;
   }

	s32AcousticTestDeInit();

}


/******************************************FunctionHeaderBegin************
 *FUNCTION:    u32AcousticoutPCMBasicTest
 *DESCRIPTION:
 *PARAMETER:
 *
 *RETURNVALUE:
 *
 *HISTORY:     31.08.05  Bernd Schubart, 3SOFT
 *
 *Initial Revision.
 ******************************************FunctionHeaderEnd*************/
tU32 u32AcousticoutPCMBasicTest(tVoid)
{
   OSAL_tIODescriptor hPCMFile = NULL, hAcousticout = NULL, hAudiorouter = NULL;
   tS32 s32FileSize;
   tS32 s32ReadBytesTotal = 0;
   tS32 s32ReadBytes = 0;
   tS8  as8Buffer[C_U32_PCM_CHUNKSIZE];
   tS32 s32Ret;
   //tS32 s32Streamtype = (tS32)C_ACOUT_STREAMTYPE_SPEECH;
   OSAL_trAcousticSampleRateCfg rSampleRateCfg;

   s32Ret = s32AcousticTestInit();
   if(s32Ret != 0) return 10000;

   /* open /dev/acousticout/speech */
   hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);

   if(OSAL_ERROR == hAcousticout)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 3;      /* open acousticout failed */
   }

   /* open PCM file */
   hPCMFile = OSAL_IOOpen(C_SZ_ACOUT_PCMTESTFILE, OSAL_EN_READONLY);

   if(OSAL_ERROR == hPCMFile)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 0xf11e;      /* open PCM file failed */
   }

   /* configure sample rate */
   rSampleRateCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
   rSampleRateCfg.nSamplerate = C_U32_PCM_SAMPLERATE;
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                              (tS32)&rSampleRateCfg);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 5;      /* IOCTRL SETSAMPLERATE failed */
   }

   /* issue start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 6;      /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = OSALUTIL_s32FGetSize(hPCMFile);

   while(s32ReadBytesTotal < s32FileSize)
   {
      s32ReadBytes = OSAL_s32IORead(hPCMFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 7;      /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue stop command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 8;      /* IOCTRL STOP failed */
   }

   //wait a little, to ensure audio playback is finished
   //normally, should wait for EVENT_AUDIO_STOPPED
   OSAL_s32ThreadWait(500);

   /* close pcm file */
   s32Ret = OSAL_s32IOClose(hPCMFile);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 9;      /* close PCM file failed */
   }

   /* close acousticout */
   s32Ret = OSAL_s32IOClose(hAcousticout);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 10;     /* close acousticout failed */
   }

   return 0;
}


/******************************************FunctionHeaderBegin************
 *FUNCTION:    u32AcousticoutPCMCfgTest
 *DESCRIPTION:
 *PARAMETER:
 *
 *RETURNVALUE:
 *
 *HISTORY:     19.09.05  Robert Schedel, 3SOFT
 *
 *Initial Revision.
 ******************************************FunctionHeaderEnd*************/
tU32 u32AcousticoutPCMCfgTest(tVoid)
{
   OSAL_tIODescriptor hPCMFile = NULL, hAcousticout = NULL, hAudiorouter = NULL;
   tS32 s32FileSize;
   tS32 s32ReadBytesTotal = 0;
   tS32 s32ReadBytes = 0;
   tS8  as8Buffer[C_U32_PCM_CHUNKSIZE];
   tS32 s32Ret;
   //tS32 s32Streamtype = (tS32)C_ACOUT_STREAMTYPE_SPEECH;
   tU32 u32Idx = 0;
   OSAL_trAcousticSampleRateCapability rSampleRateCap;
   OSAL_trAcousticSampleFormatCapability rSampleFormatCap;
   OSAL_trAcousticChannelCapability rChannelnumCap;
   OSAL_trAcousticBufferSizeCapability rBufferSizeCap;
   OSAL_trAcousticSampleRateCfg rSampleRateCfg;
   OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
   OSAL_trAcousticBufferSizeCfg rBufferSizeCfg;

   /* Buffers for retrieving device capabilities */
   OSAL_tAcousticSampleRate anSampleRateFromCap[5], anSampleRateToCap[5];
   OSAL_tenAcousticSampleFormat aenSampleformatCap[5];
   tU32 au32ChannelnumCap[5];
   OSAL_tAcousticBuffersize anBuffersizeCap[5];

   /* sample rates to be tested (adapt to project values!) */
   const OSAL_tAcousticSampleRate anSampleRate[] = 
   {
      44100
   };

   /* sample formats to be tested (adapt to project values!) */
   const OSAL_tenAcousticSampleFormat aenSampleformats[] =
   {
      OSAL_EN_ACOUSTIC_SF_S16
   };

   /* channel numbers to be tested (adapt to project values!) */
   const tU16 au16ChannelNum[] =
   {
      2
   };

   /* buffer sizes to be tested (adapt to project values!) */
   const OSAL_tAcousticBuffersize anPcmBuffersize[] =
   {
      8192
   };

   s32Ret = s32AcousticTestInit();
   if(s32Ret != 0) return 10000;

   /* open /dev/acousticout/speech */
   hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);

   if(OSAL_ERROR == hAcousticout)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 3;      /* open acousticout failed */
   }

   /* open PCM file */
   hPCMFile = OSAL_IOOpen(C_SZ_ACOUT_PCMTESTFILE, OSAL_EN_READONLY);

   if(OSAL_ERROR == hPCMFile)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 0xf11e;   /* open PCM file failed */
   }

   /* get supp parameter */
   rSampleRateCap.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
   rSampleRateCap.u32ElemCnt = sizeof(anSampleRateFromCap)/sizeof(anSampleRateFromCap[0]);
   rSampleRateCap.pnSamplerateFrom = &(anSampleRateFromCap[0]);
   rSampleRateCap.pnSamplerateTo = &(anSampleRateToCap[0]);
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLERATE,
                              (tS32)&rSampleRateCap);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 5;      /* IOCTRL GETSUPP_SAMPLERATE failed */
   }

   for (u32Idx=0; u32Idx<sizeof(anSampleRate)/sizeof(anSampleRate[0]); u32Idx++)
   {
      /* set parameter */
      rSampleRateCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = anSampleRate[u32Idx];
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);

      if(OSAL_OK != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 6;      /* IOCTRL SETSAMPLERATE failed */
      }

      /* get parameter */
      rSampleRateCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);

      if((OSAL_OK != s32Ret) || (rSampleRateCfg.nSamplerate != anSampleRate[u32Idx]))
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 7;      /* IOCTRL GETSAMPLERATE failed */
      }
   }

   /* get supp parameter */
   rSampleFormatCap.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
   rSampleFormatCap.u32ElemCnt = sizeof(aenSampleformatCap)/sizeof(aenSampleformatCap[0]);
   rSampleFormatCap.penSampleformats = &(aenSampleformatCap[0]);
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLEFORMAT,
                              (tS32)&rSampleFormatCap);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 8;      /* IOCTRL GETSUPP_SAMPLEFORMAT failed */
   }

   for (u32Idx=0; u32Idx<sizeof(aenSampleformats)/sizeof(aenSampleformats[0]); u32Idx++)
   {
      /* set parameter */
      rSampleFormatCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = aenSampleformats[u32Idx];
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);

      if(OSAL_OK != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 9;      /* IOCTRL SETSAMPLEFORMAT failed */
      }

      /* get parameter */
      rSampleFormatCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);

      if((OSAL_OK != s32Ret) || (rSampleFormatCfg.enSampleformat != aenSampleformats[u32Idx]))
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 10;     /* IOCTRL GETSAMPLEFORMAT failed */
      }
   }

   /* get supp parameter */
   rChannelnumCap.u32ElemCnt = sizeof(au32ChannelnumCap)/sizeof(au32ChannelnumCap[0]);
   rChannelnumCap.pu32NumChannels = &(au32ChannelnumCap[0]);
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_CHANNELS,
                              (tS32)&rChannelnumCap);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 11;     /* IOCTRL GETSUPP_CHANNELS failed */
   }

   for (u32Idx=0; u32Idx<sizeof(au16ChannelNum)/sizeof(au16ChannelNum[0]); u32Idx++)
   {
      tU16 u16ChannelNum = 0;

      /* set parameter */
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)au16ChannelNum[u32Idx]);

      if(OSAL_OK != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 12;     /* IOCTRL SETCHANNELS failed */
      }

      /* get parameter */
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETCHANNELS,
                                 (tS32)&u16ChannelNum);

      if((OSAL_OK != s32Ret) || (u16ChannelNum != au16ChannelNum[u32Idx]))
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 13;     /* IOCTRL GETCHANNELS failed */
      }
   }

   /* get supp parameter */
   rBufferSizeCap.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
   rBufferSizeCap.u32ElemCnt = sizeof(anBuffersizeCap)/sizeof(anBuffersizeCap[0]);
   rBufferSizeCap.pnBuffersizes = &(anBuffersizeCap[0]);
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_BUFFERSIZE,
                              (tS32)&rBufferSizeCap);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 14;     /* IOCTRL GETSUPP_BUFFERSIZE failed */
   }

   for (u32Idx=0; u32Idx<sizeof(anPcmBuffersize)/sizeof(anPcmBuffersize[0]); u32Idx++)
   {
      /* set parameter */
      rBufferSizeCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      rBufferSizeCfg.nBuffersize = anPcmBuffersize[u32Idx];
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rBufferSizeCfg);

      if(OSAL_OK != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 15;     /* IOCTRL SETBUFFERSIZE failed */
      }

      /* get parameter */
      rBufferSizeCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETBUFFERSIZE,
                                 (tS32)&rBufferSizeCfg);

      if((OSAL_OK != s32Ret) || (rBufferSizeCfg.nBuffersize != anPcmBuffersize[u32Idx]))
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 16;     /* IOCTRL GETBUFFERSIZE failed */
      }
   }

   /* issue start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 17;     /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = OSALUTIL_s32FGetSize(hPCMFile);

   while(s32ReadBytesTotal < s32FileSize)
   {
      s32ReadBytes = OSAL_s32IORead(hPCMFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
         return 18;     /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue stop command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 19;     /* IOCTRL STOP failed */
   }

   /* close pcm file */
   s32Ret = OSAL_s32IOClose(hPCMFile);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 20;     /* close PCM file failed */
   }

   /* close acousticout */
   s32Ret = OSAL_s32IOClose(hAcousticout);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hPCMFile);
      return 21;     /* close acousticout failed */
   }

   return 0;
}


/******************************************FunctionHeaderBegin************
 *FUNCTION:    u32AcousticoutPCMPlayModeTest
 *DESCRIPTION:
 *PARAMETER:
 *
 *RETURNVALUE:
 *
 *HISTORY:     31.08.05  Bernd Schubart, 3SOFT
 *
 *Initial Revision.
 ******************************************FunctionHeaderEnd*************/
tU32 u32AcousticoutPCMPlayModeTest(tVoid)
{
   OSAL_tIODescriptor hFile = NULL, hAcousticout = NULL, hAudiorouter= NULL;
   tS32 s32FileSize;
   tS32 s32FirstBlock, s32SecondBlock;
   tS32 s32ReadBytesTotal = 0;
   tS32 s32ReadBytes = 0;
   tS8  as8Buffer[C_U32_PCM_CHUNKSIZE];
   tS32 s32Ret;

   s32Ret = s32AcousticTestInit();
   if(s32Ret != 0) return 10000;

   /* open /dev/acousticout/speech */
   hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);

   if(OSAL_ERROR == hAcousticout)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 3;      /* open acousticout failed */
   }

   /* open file */
   hFile = OSAL_IOOpen(C_SZ_ACOUT_PCMTESTFILE, OSAL_EN_READONLY);

   if(OSAL_ERROR == hFile)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 0xf11e;      /* open file failed */
   }

   /* issue start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 4;      /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = OSALUTIL_s32FGetSize(hFile);

   s32FirstBlock = s32FileSize / 2;
   s32SecondBlock = s32FileSize;

   /* transfer first block of data */
   while((s32ReadBytesTotal < s32FirstBlock)&&(s32ReadBytesTotal < s32FileSize))
   {
      s32ReadBytes = OSAL_s32IORead(hFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
         return 5;     /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue pause command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_PAUSE,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 6;     /* IOCTRL PAUSE failed */
   }

   /* wait 3 seconds, then issue start command */
   OSAL_s32ThreadWait(3000);

   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);


   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 7;     /* IOCTRL START failed */
   }

   /* transfer second block of data */
   while(s32ReadBytesTotal < s32SecondBlock)
   {
      s32ReadBytes = OSAL_s32IORead(hFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR!= s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
         return 8;     /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue stop command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 9;     /* IOCTRL ABORT failed */
   }

   /* close  file */
   s32Ret = OSAL_s32IOClose(hFile);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 10;     /* close file failed */
   }

   /* close acousticout */
   s32Ret = OSAL_s32IOClose(hAcousticout);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, hAudiorouter, hFile);
      return 11;     /* close acousticout failed */
   }

   return 0;
}


/******************************************FunctionHeaderBegin************
 *FUNCTION:    u32AcousticoutPCMStartStopAbortTest
 *DESCRIPTION:
 *PARAMETER:
 *
 *RETURNVALUE:
 *
 *HISTORY:     14.08.06  Bernd Schubart, 3SOFT
 *
 *Initial Revision.
 ******************************************FunctionHeaderEnd*************/
tU32 u32AcousticoutPCMStartStopAbortTest(tVoid)
{
   OSAL_tIODescriptor hFile = NULL, hAcousticout = NULL;

   tS32 s32FileSize;
   tS32 s32ReadBytesTotal = 0;
   tS32 s32ReadBytes = 0;
   tS8  as8Buffer[C_U32_PCM_CHUNKSIZE];
   tS32 s32Ret;

   s32Ret = s32AcousticTestInit();
   if(s32Ret != 0) return 10000;

   /* open /dev/acousticout/speech */
   hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);

   if(OSAL_ERROR == hAcousticout)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 3;      /* open acousticout failed */
   }

   /* open file */
   hFile = OSAL_IOOpen(C_SZ_ACOUT_PCMTESTFILE, OSAL_EN_READONLY);

   if(OSAL_ERROR == hFile)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 0xf11e;      /* open mp3 file failed */
   }

   /* issue start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 8;      /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = C_U32_PCM_CHUNKSIZE * 20;  /* just play 100 buffers to shorten test duration */
   s32ReadBytesTotal = 0;

   while(s32ReadBytesTotal < s32FileSize)
   {
      s32ReadBytes = OSAL_s32IORead(hFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, NULL, hFile);
         return 9;      /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue stop command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 10;        /* IOCTRL STOP failed */
   }

   //wait a little, to ensure audio playback is finished
   //normally, should wait for EVENT_AUDIO_STOPPED
   OSAL_s32ThreadWait(1000);

   /* issue new start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 11;      /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = C_U32_PCM_CHUNKSIZE * 20;  /* just play 100 buffers to shorten test duration */
   s32ReadBytesTotal = 0;

   while(s32ReadBytesTotal < s32FileSize)
   {
      s32ReadBytes = OSAL_s32IORead(hFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, NULL, hFile);
         return 12;      /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   /* issue abort command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                              NULL);

   //wait a little, to ensure audio playback is finished
   //normally, should wait for EVENT_AUDIO_STOPPED
   OSAL_s32ThreadWait(1000);

   /* issue new start command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 13;      /* IOCTRL START failed */
   }

   /* read file and write to acousticout */
   s32FileSize = C_U32_PCM_CHUNKSIZE * 20;  /* just play 100 buffers to shorten test duration */
   s32ReadBytesTotal = 0;

   while(s32ReadBytesTotal < s32FileSize)
   {
      s32ReadBytes = OSAL_s32IORead(hFile, as8Buffer, C_U32_PCM_CHUNKSIZE);

      s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

      if ((tS32)OSAL_E_NOERROR != s32Ret)
      {
         vAcousticoutCleanup(hAcousticout, NULL, hFile);
         return 14;      /* write to acousticout failed */
      }

      s32ReadBytesTotal += s32ReadBytes;
   }

   //wait a little, to ensure audio playback is finished
   //normally, should wait for EVENT_AUDIO_STOPPED
   OSAL_s32ThreadWait(500);

   /* issue final stop command */
   s32Ret = OSAL_s32IOControl(hAcousticout,
                              OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                              NULL);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 15;      /* IOCTRL START failed */
   }


   /* close file */
   s32Ret = OSAL_s32IOClose(hFile);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 16;        /* close MP3 file failed */
   }

   /* close acousticout */
   s32Ret = OSAL_s32IOClose(hAcousticout);

   if(OSAL_OK != s32Ret)
   {
      vAcousticoutCleanup(hAcousticout, NULL, hFile);
      return 17;        /* close acousticout failed */
   }

   return 0;
}

