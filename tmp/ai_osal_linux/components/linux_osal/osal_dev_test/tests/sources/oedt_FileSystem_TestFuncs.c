/******************************************************************************
 * FILE         : oedt_FS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the file system test cases for the CDROM, 
 *                FFS1, FFS2, FFS3, USBH, RamDisk, SDCard  
 *                          
 * AUTHOR(s)    :  Sriranjan U (RBEI/ECM1) and Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * --.--.--       | Initial revision    | ------------
 * 21 Oct, 2008   | version 1.0         | Sriranjan U (RBEI/ECM1)
 *                |                     | Shilpa Bhat(RBEI/ECM1)
 ------------------------------------------------------------------------------*/
#define vTtfisTrace(...)

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FileSystem_TestFuncs.h"

#define OSAL_C_TRACELEVEL1  TR_LEVEL_ERRORS

tS32 s32ParallelRetval_th1, s32ParallelRetval_th2;
tS8 *ps8UserBuf; 
OSAL_tEventHandle parallel_access;
OSAL_tEventMask ResultMask;
OSAL_tSemHandle hSemAsyncFileSystem;

#define OEDT_FS_FILE_PATH_LEN 					44
#define FILE_NAME_MAX_LEN                       255
#define FILE_NAME_MAX_LEN_EXCEEDED              256
#define MB_SIZE                             	((tU32)1048576)	
#define CDROM_TEXT_FILE 						"/dev/cdrom/TestCD/00_first_folder/00_cd_test_firstfile.dat"
#define CDROM_TEXT_LAST 						"/dev/cdrom/TestCD/ZZ_last/last_00/last_01/last_02/last_03/last_04/last_06/99_cd_test_lastfile.dat"
#define CDROM_MP3_FILE 							"/dev/cdrom/TestCD/Creed/Weathered/01-Creed-bullets.mp3"
#define FS_THREAD1               		        0x0001
#define FS_THREAD2                         		0x0002
#define EVENT_NAME 								"FS_ThrOp1"
#define EVENT_NAME1 							"FS_ThrOp2"
#define CDROM_BYTES_TO_READ_TPUT 				1048576
#define WRITE_SIZE								((tU32)16000000)	
tU32 Dev_Error[7] = {1000, 2000, 3000, 4000, 5000, 6000, 7000};
enum Dev_Name {
	FFS1,
	FFS2,
	FFS3,
	RAMDISK,						
	USB,
	SDCARD
};

tU32 Dev_Identity[7][3] = {	/* GM    JRL    FORD */
				/*CDROM*/	    1,     0,     0,
				/*FFS1*/	    1,     0,     1,
				/*FFS2*/		1,     1,     1,	
				/*FFS3*/        1,     1,     1,
				/*RAMDISK*/		1,     0,     1,
				/*USB*/			1,     1,     1,
				/*SDCARD*/		0,     0,     1,
};
 
/*Return Parameters*/
tU32 u32tr1FS = 0;
tU32 u32tr2FS = 0;

/*Event Handles*/
OSAL_tEventHandle hEventFS;
OSAL_tEventHandle hEventFS1;

/*Device Handles to be used in threads*/
OSAL_tIODescriptor     hDeviceFS = 0;

/*Global buffer parameters*/
tS8 *HugeBufferTr1FS = NULL;
tS8 *HugeBufferTr2FS = NULL;

tU8 * namebuffer = NULL;
const tU8* filename = NULL;
OSAL_tSemHandle hSemAsyncFS;

tVoid file_name(tU32 , tU8 * , const tU8* );
tVoid file_name(tU32 count, tU8 * pu8namebuffer, const tU8* pu8filename)
{
	switch(count)
	{
		 case FFS1:
			OSAL_pvMemoryMove( pu8namebuffer,OEDTTEST_C_STRING_DEVICE_FFS1,OSAL_u32StringLength(OEDTTEST_C_STRING_DEVICE_FFS1));
			if( pu8filename )
				(tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;
		 case FFS2:
			OSAL_pvMemoryMove( pu8namebuffer,OEDTTEST_C_STRING_DEVICE_FFS2,OSAL_u32StringLength(OEDTTEST_C_STRING_DEVICE_FFS2));
			if( pu8filename )
				(tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;
	   	 case FFS3:
			OSAL_pvMemoryMove( pu8namebuffer,OEDTTEST_C_STRING_DEVICE_FFS3,OSAL_u32StringLength(OEDTTEST_C_STRING_DEVICE_FFS3));
			if( pu8filename )
				(tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;
		case RAMDISK:
			OSAL_pvMemoryMove( pu8namebuffer,OSAL_C_STRING_DEVICE_RAMDISK,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_RAMDISK));
			if( pu8filename )
				(tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;
		case USB:
			OSAL_pvMemoryMove( pu8namebuffer,OSAL_C_STRING_DEVICE_USB,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_USB));
			if( pu8filename )
				(tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;		
  		case SDCARD:
			OSAL_pvMemoryMove( pu8namebuffer,OSAL_C_STRING_DEVICE_CARD,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_CARD));
			if( pu8filename )
			  (tVoid)OSAL_szStringConcat(pu8namebuffer,pu8filename );
			break;
		default:
			break;		
	}
}

/************************************************************************
*FUNCTION:      vOEDTFSAsyncCallback 
* PARAMETER:    none
* RETURNVALUE:  "void"
* DESCRIPTION:  Call back function for Async read operation
* HISTORY:		Created Shilpa Bhat(RBIN/ECM1)on 22 Oct, 2008
 ************************************************************************/
tVoid vOEDTFSAsyncCallback ( OSAL_trAsyncControl *prAsyncCtrl)
{
	if ( OSAL_u32IOErrorAsync((OSAL_trAsyncControl*)prAsyncCtrl) == OSAL_E_NOERROR )
	{
	    OSAL_s32SemaphorePost ( hSemAsyncFS );   
	}
} 

/************************************************************************
 *FUNCTION:     vOEDTFSAsyncWrCallback 
* PARAMETER:    none
* RETURNVALUE:  "void" 
* DESCRIPTION:  Call back function for Async write operation
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
*************************************************************************/


tVoid vOEDTFSAsyncWrCallback ( OSAL_trAsyncControl *prAsyncCtrl)
{

  if(OSAL_E_NOERROR == OSAL_u32IOErrorAsync((OSAL_trAsyncControl*)prAsyncCtrl))
   {
	  /* Release semaphore */
	   OSAL_s32SemaphorePost ( hSemAsyncFS );      	
   }

}

/*****************************************************************************
* FUNCTION:		OpenThreadFS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Open file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void OpenThreadFS(const tVoid * DevName )
{
	tU8 *strDevName;

	strDevName = (tU8 *) DevName;
	/* Open file */
	hDeviceFS = OSAL_IOOpen ( strDevName,OSAL_EN_READWRITE );	
	if(hDeviceFS == OSAL_ERROR)
	{
		u32tr1FS = 1;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD1,OSAL_EN_EVENTMASK_OR );
	OSAL_s32EventPost( hEventFS1,FS_THREAD1,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}
/*****************************************************************************
* FUNCTION:		CloseThreadFS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Close file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void CloseThreadFS(const tVoid* strDevName)
{
	
	OSAL_tEventMask EM  = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(strDevName);

	/*Wait for threads to Post status*/
 	OSAL_s32EventWait( hEventFS1, FS_THREAD1,
   					 OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&EM );
    OSAL_s32EventPost( hEventFS1,~EM,OSAL_EN_EVENTMASK_AND );
	
	if( OSAL_s32IOClose ( hDeviceFS ) == OSAL_ERROR )
	{
		u32tr2FS = 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD2,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		WriteThread1FS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Writes data to a file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void WriteThread1FS( const tVoid *pvData )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Write some data to File1*/
	if( OSAL_ERROR == OSAL_s32IOWrite( hDeviceFS,HugeBufferTr1FS,MB_SIZE ) )
	{
		u32tr1FS = 1;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD1,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		WriteThread2FS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Writes data to a file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void WriteThread2FS(const tVoid *pvData )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Write some data to File1*/
	if( OSAL_ERROR == OSAL_s32IOWrite( hDeviceFS,HugeBufferTr2FS,MB_SIZE ) )
	{
		u32tr2FS = 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD2,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:	    WriteThreadFS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write data to file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void WriteThreadFS( const tVoid *pvData )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Write some data to File1*/
	if( OSAL_ERROR == OSAL_s32IOWrite( hDeviceFS,HugeBufferTr1FS,MB_SIZE ) )
	{
		u32tr1FS = 1;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD1,OSAL_EN_EVENTMASK_OR );
	OSAL_s32EventPost( hEventFS1,FS_THREAD1,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		ReadThreadFS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Reads data from file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void ReadThreadFS(const tVoid *pvData )
{
	OSAL_tEventMask EM  = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Wait for threads to Post status*/
 	OSAL_s32EventWait( hEventFS1, FS_THREAD1,
					 OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&EM );
    OSAL_s32EventPost( hEventFS1,~EM,OSAL_EN_EVENTMASK_AND );
	
	/*Read data*/
	if( OSAL_ERROR == OSAL_s32IORead( hDeviceFS,HugeBufferTr2FS,MB_SIZE ) )
	{
		u32tr2FS = 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD2,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		ReadThread1FS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Reads data from file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void ReadThread1FS(const tVoid *pvData )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Read data*/
	if( OSAL_ERROR == OSAL_s32IORead( hDeviceFS,HugeBufferTr1FS,10 ) )
	{
		u32tr2FS = 1;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD1,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		ReadThread2FS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Reads data from file.
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void ReadThread2FS( const tVoid *pvData )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);

	/*Read data*/
	if( OSAL_ERROR == OSAL_s32IORead( hDeviceFS,HugeBufferTr2FS,10 ) )
	{
		u32tr2FS = 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventFS,FS_THREAD2,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION:		Generate_File_Name_fs()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Generate file name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/
tVoid Generate_File_Name_fs(tChar* ptrstr,tU32 len,tU32 limit)
{
	/*Declarations*/
	tChar ch = 'a';
    tU32 i;
	
	/*Generate filename*/
    for(i=len;i<limit-5;i++)	    
    {
    	/*Fill the string*/
        *(ptrstr+i)=ch;
        /*Goto next alphabet*/
        ch = ch+1; 
        /*Compare for limit*/
        if(ch == 'z'+1)
        {
        	/*Reset charater*/
            ch='a';
        }
     }
     /*Extension*/
     *(ptrstr+i)='.';
     *(ptrstr+i+1)='t';*(ptrstr+i+2)='x';*(ptrstr+i+3)='t';   
     /*String terminator*/
     *(ptrstr+i+4)='\0';
}

/*****************************************************************************
* FUNCTION:		u32FileSystemCreateCommonFile()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Craetes a data file and writes data into it.This function will 
*				be used in Test functions for read,write and other
*           	file-related operations.
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008  
******************************************************************************/
tU32 u32FilesystemCreateCommonFile(const char* filename_arg)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;	 
	tCS8 DataToWrite[FILE_NAME_MAX_LEN + 1] = 
			"Writing some test data to the data file";
	tU32 u32Ret = 0; 
	tU32 u8BytesToWrite = 40;
	tS32 s32BytesWritten = 0;
	/*--create the the Data file common.txt--*/
	if ( (hFile = OSAL_IOCreate (filename_arg, enAccess) ) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{ 
		/*--Write the Data into common.txt--*/
		s32BytesWritten = OSAL_s32IOWrite (hFile, DataToWrite,
												(tU32) u8BytesToWrite);
		if( (s32BytesWritten == (tS32)OSAL_ERROR)||(s32BytesWritten != (tS32)u8BytesToWrite))
			u32Ret += 3;

		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			u32Ret += 10;
	 }
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FilesystemRemoveCommonFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Remove data file.This function will be used in Test functions 
*				for read,write and other file-related operations.
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008  
******************************************************************************/
tU32 u32FilesystemRemoveCommonFile(const char* filename_arg)
{
	tU32 u32Ret = 0 ;
	/*-- Remove the Data file Common.txt--*/
	if(OSAL_s32IORemove(filename_arg) == OSAL_ERROR )
		u32Ret = 1;
	return u32Ret ;
}

/*****************************************************************************
* FUNCTION:		Parallel_Access_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:	Try to oprn the directory  
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 31, 2008  
******************************************************************************/
static tVoid Parallel_Access_Thread_1(tVoid)
{
	OSAL_tIODescriptor hDir1[NO_OF_DEVICE] = {0} ;
	tU32 u32Ret = 0; 
	tU32 dev_count = 0;
	tChar* dir_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_DEVICE_FFS1_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						 OEDT_C_STRING_DEVICE_USB_ROOT,
						 OEDT_C_STRING_DEVICE_CARD_ROOT
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlDirent rDirent[NO_OF_DEVICE];
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/* Open directory in readwrite mode */
		hDir1[dev_count] = OSAL_IOOpen ( dir_name[dev_count], enAccess );
		if( hDir1[dev_count] == (OSAL_tIODescriptor)OSAL_OK )
		{
			u32Ret += 1;
		}
		else
		{
			/* Copy sub-directory name to structure variable */
			(tVoid)OSAL_szStringCopy ( rDirent[dev_count].s8Name, SUBDIR_NAME );
			/* Create sub-directory */
			if ( OSAL_s32IOControl ( hDir1[dev_count],OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent[dev_count]) == OSAL_ERROR )
			{
				u32Ret += 3;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		s32ParallelRetval_th1 += (tS32)u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	/* Wait for other thread to finish*/
	OSAL_s32EventPost( parallel_access,EVENT_POST_THREAD_2_STOP_WAIT,
									OSAL_EN_EVENTMASK_OR );
	if(OSAL_OK != OSAL_s32EventWait(parallel_access, EVENT_POST_THREAD_1_STOP_WAIT,
							OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, 
							&ResultMask))
	{
		u32Ret = 100;
	}
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/* Remove sub-directory */
		if ( OSAL_s32IOControl ( hDir1[dev_count],OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent[dev_count]) == OSAL_ERROR )
		{
			u32Ret += 4;
		}
		else if (hDir1[dev_count] != OSAL_ERROR)
		{
			if( OSAL_s32IOClose ( hDir1[dev_count] ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		else
		{
			u32Ret += 25;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);   
		}
		s32ParallelRetval_th1 += (tS32)u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	OSAL_s32EventPost( parallel_access,EVENT_POST_THREAD_1,
									OSAL_EN_EVENTMASK_OR );
	/*Exit thread*/
	OSAL_vThreadExit();

}


/*****************************************************************************
* FUNCTION:		Parallel_Access_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:	Try to Rename / Delete the directory  
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 31, 2008  
******************************************************************************/
static tVoid Parallel_Access_Thread_2(tVoid)
{
	tU32 u32Ret = 0; 
	tU32 dev_count = 0;
	tChar* dir_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_DEVICE_FFS1_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						 OEDT_C_STRING_DEVICE_USB_ROOT,
						 OEDT_C_STRING_DEVICE_CARD_ROOT
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	/* Wait for other thread */
	if(OSAL_OK !=OSAL_s32EventWait(parallel_access, EVENT_POST_THREAD_2_STOP_WAIT,
							OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, 
							&ResultMask))
	{
		u32Ret = 100;
	}	
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/* Delete the directory */
		if( OSAL_s32IORemove ( dir_name[dev_count] ) != OSAL_ERROR )
 		{
 			u32Ret += 20;
 		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);   
		}
		s32ParallelRetval_th2 += (tS32)u32Ret;
		u32Ret = 0;
	}
	OSAL_s32EventPost( parallel_access,EVENT_POST_THREAD_1_STOP_WAIT,
									OSAL_EN_EVENTMASK_OR );
	OSAL_s32EventPost( parallel_access,EVENT_POST_THREAD_1,
					OSAL_EN_EVENTMASK_OR );
	/*Exit thread*/
	OSAL_vThreadExit();
}



/*****************************************************************************
* FUNCTION:	    u32FileSystemOpenClosedevice( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_001
* DESCRIPTION:  Opens and closes all filesystem devices
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008  
******************************************************************************/
 tU32 u32FileSystemOpenClosedevice(void)
 { 
 	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;
	tU32 dev_count = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	tChar* device_name []= {
					OSAL_C_STRING_DEVICE_CDROM,
					OEDTTEST_C_STRING_DEVICE_FFS1,
					OEDTTEST_C_STRING_DEVICE_FFS2,
					OEDTTEST_C_STRING_DEVICE_FFS3,
					OSAL_C_STRING_DEVICE_RAMDISK,
					OSAL_C_STRING_DEVICE_USB,
					OSAL_C_STRING_DEVICE_CARD
					};
 	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
 	{
 		hDevice = OSAL_IOOpen( device_name[dev_count] , enAccess );
	   if ( OSAL_ERROR == hDevice )
	   {
	   		u32Ret += 1;
	   }
	   else
		{
			/* Close the device */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret += 2;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
 	}
 	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemOpendevInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_002
* DESCRIPTION:  Try to Open all filesystem devices with invalid parameters
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32 u32FileSystemOpendevInvalParm(void)
{
	OSAL_tIODescriptor hDevice = 0;
   	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS3,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;    
    /* Try to open the device with an invalid device name*/
   hDevice = OSAL_IOOpen( OEDT_C_STRING_DEVICE_INVAL, enAccess );
	if ( OSAL_ERROR != hDevice  )
   {
   	u32Ret += 1; 
    	/* If successfully opened, indicate an error and close the device */  
		if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
	}
	/* Try to open the device with an invalid access mode */
	enAccess = (OSAL_tenAccess)OEDT_INVALID_ACCESS_MODE;
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDevice = OSAL_IOOpen( device_name[dev_count] , enAccess );
		/* If successfully opened, indicate an error and close the device */
		if ( OSAL_OK == hDevice )
		{
			u32Ret += 4;
			if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret += 8;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemReOpendev( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_003
* DESCRIPTION:  Try to Re-Open all filesystem devices
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
 tU32 u32FileSystemReOpendev(void)
 {
 	OSAL_tIODescriptor hDevice1 = 0;
 	OSAL_tIODescriptor hDevice2 = 0;
 	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;   
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDevice1 = OSAL_IOOpen( device_name[dev_count], enAccess );
    	if (OSAL_ERROR == hDevice1 )
    	{
    		u32Ret += 1;
    	}
		else
		{
			/* Re-open the device, should be successfull */
			hDevice2 = OSAL_IOOpen( device_name[dev_count], enAccess );
			if (OSAL_ERROR == hDevice2  )
			{
				u32Ret += 2;
			}
			else
			{	/* If open for the second time is successfull, close the second 
				device handle */
				if( OSAL_ERROR == OSAL_s32IOClose ( hDevice2 ) )
				{
					u32Ret += 4;
				}
			}
			/* Now close the first device handle */
			if( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice1 ) )
			{
				u32Ret += 8;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemOpendevDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_004
* DESCRIPTION:  Try to Re-Open all filesystem devices
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32 u32FileSystemOpendevDiffModes(void)
{
  	OSAL_tIODescriptor hDevice = 0;
  	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;  

	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		/* Open the file in READONLY mode */
		hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, enAccess );
		if(OSAL_ERROR == hDevice  )
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			if( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice )  )
			{
				u32Ret_cdrom += 2;
			}
		}
		/* Open the file in READWRITE mode */
		/* Should not be able to open the device in this mode */
		enAccess = OSAL_EN_READWRITE;
		hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, enAccess );
		if(OSAL_OK == hDevice  )
		{
			u32Ret_cdrom += 4;
			/* If open with this mode is successfull, indicate error and close 
				the device */
			if( OSAL_ERROR == OSAL_s32IOClose ( hDevice )  )
			{
				u32Ret_cdrom += 8;
			}
		}

		/* Open the file in WRITEONLY mode */
		enAccess = OSAL_EN_WRITEONLY;
		/* Should not be able to open the device in this mode */
		hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, enAccess );
		if(OSAL_ERROR != hDevice  )
		{
			u32Ret_cdrom += 16;
			/* If open with this mode is successfull, indicate error and close 
				the device */
			if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret_cdrom += 32;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
		if(OSAL_ERROR == hDevice  )
		{
			u32Ret += 1;
		}
		else
		{
			if( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice )	)
			{
				u32Ret += 2;
			}
		}
		/* Open the file in READWRITE mode */
		enAccess = OSAL_EN_READWRITE;
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
		if(OSAL_ERROR == hDevice  )
		{
			u32Ret += 4;
		}
		else 
		{ /* If open with this mode is successfull close  the device */
			if( OSAL_ERROR == OSAL_s32IOClose ( hDevice )  )
			{
				u32Ret += 8;
			}
		}
		/* Open the file in WRITEONLY mode */
		enAccess = OSAL_EN_WRITEONLY;
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
		if(OSAL_ERROR == hDevice  )
		{
			u32Ret += 16;
		}
		else 
		{
			/* If open with this mode is successfull close the device */
			if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret += 32;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemClosedevAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_005
* DESCRIPTION:  Try to Re-Open all filesystem devices
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32  u32FileSystemClosedevAlreadyClosed(void)
 {
 	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY; 
    
	/* CDROM */
	/* Open the device */
	if ((Dev_Identity[0][device_id] == 1))
	{
	    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, enAccess );
	    if (OSAL_ERROR ==  hDevice )
	    {
	    	u32Ret_cdrom += 1;
	    }
	    else
		{
			/* Close the device with the device handle obtained in open */
			if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				/* Re Closing , Try to close with the same handle.
				Must not be possible */
				if( OSAL_OK == OSAL_s32IOClose ( hDevice )  )
				{
					u32Ret_cdrom += 20;
				}
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
    	if (OSAL_ERROR ==  hDevice )
    	{
    		u32Ret += 1;
    	}
   		else
		{
			/* Close the device with the device handle obtained in open */
			if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret += 2;
			}
			else
			{
				/* Re Closing , Try to close with the same handle.
				Must not be possible */
				if( OSAL_ERROR != OSAL_s32IOClose ( hDevice )  )
				{
					u32Ret += 20;
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemOpenClosedir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_006
* DESCRIPTION:  Try to Open and closes a directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32  u32FileSystemOpenClosedir(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
							 OEDT_C_STRING_CD_DIR1,
							 OEDT_C_STRING_DEVICE_FFS1_ROOT,
							 OEDT_C_STRING_DEVICE_FFS2_ROOT,
							 OEDT_C_STRING_DEVICE_FFS2_ROOT,
							 OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
							 OEDT_C_STRING_DEVICE_USB_ROOT,
							 OEDT_C_STRING_DEVICE_CARD_ROOT
						 };

	tU32 device_id = u32OEDT_BoardName() - 1;
	
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSALUTIL_prOpenDir (device_name[dev_count]);
		if( OSAL_NULL	== hDir) 
		{
			u32Ret += 1;
		}
		else 
		{
			/* Close the directory */
			if( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir ) )
			{
				u32Ret += 2;
			}
		}		
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemOpendirInvalid( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_007
* DESCRIPTION:  Try to Open invalid directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32  u32FileSystemOpendirInvalid(void)
{
	OSAL_tIODescriptor hDir1 = 0;
   tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* inv_dir_name []= {
						 OEDT_C_STRING_CD_NONEXST,
						 OEDT_C_STRING_FFS1_NONEXST,
						 OEDT_C_STRING_FFS2_NONEXST,
						 OEDT_C_STRING_FFS2_NONEXST,
						 OEDT_C_STRING_RAMDISK_NONEXST,
						 OEDT_C_STRING_USB_NONEXST,
						 OEDT_C_STRING_SDCARD_NONEXST
						 };
	tChar* dir_name []= {
						OEDT_C_STRING_CD_DIR1,
						OEDT_C_STRING_DEVICE_FFS1_ROOT,
						OEDT_C_STRING_DEVICE_FFS2_ROOT,
						OEDT_C_STRING_DEVICE_FFS2_ROOT,
						OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						OEDT_C_STRING_DEVICE_USB_ROOT,
						OEDT_C_STRING_DEVICE_CARD_ROOT
						};
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
		{
			enAccess = OSAL_EN_READONLY;
   		/*Try to open a invalid directory  */
   		hDir1 = OSAL_IOOpen ( inv_dir_name[dev_count], enAccess );
			{
				if(OSAL_OK == hDir1)
				{
					u32Ret += 1;
		
					/* If open is successful, indicate an error and close the directory */
					if(OSAL_ERROR  == OSAL_s32IOClose( hDir1 ) )
					{
						u32Ret += 2;
					}
				}
			}
			/* Try to open a valid directory with an invalid access mode */
			hDir1 = OSAL_IOOpen ( dir_name[dev_count], (OSAL_tenAccess)OEDT_INVALID_ACCESS_MODE );
			{
				if(OSAL_OK == hDir1)
				{
					u32Ret += 4;
			
					/* If open is successful, indicate an error and close the directory */
					if(OSAL_OK != OSAL_s32IOClose( hDir1 ) )
					{
						u32Ret += 8;
					}
				}
			}
			if ( u32Ret)
			{
				u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
			}
			Old_u32Ret += u32Ret;
			u32Ret = 0;
			if(dev_count == FFS3_POS - 1)
			{
				dev_count++;
			}

		}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateDelDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_008
* DESCRIPTION:  Try create and delete directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 23, 2008 
******************************************************************************/
tU32  u32FileSystemCreateDelDir(void)
{
	OSAL_trIOCtrlDir* hDir1 = NULL;
	OSAL_tIODescriptor hDir2 = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tS32 DirRet = 0;
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tU32 dev_count = 0;
	tChar* dir_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_DEVICE_FFS1_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						 OEDT_C_STRING_DEVICE_USB_ROOT,
						 OEDT_C_STRING_DEVICE_CARD_ROOT
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlDirent rDirent;
	//tPS8 Retval = 0;
	
	/* CDROM Open the parent directory */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hDir1 = OSALUTIL_prOpenDir(SUBDIR_CREAT_CDROM_PATH);
		if (OSAL_NULL == hDir1)
		{
		   u32Ret_cdrom += 1;	
		}
		else
		{
			/* Attempt to Create a Directory, though the parent directory is opened 
			in read only mode */
			DirRet = OSALUTIL_s32CreateDir(hDir1->fd,
											OEDT_CDROM_CREAT_DIR_PATH);
			if(OSAL_OK == DirRet)
			{
				u32Ret_cdrom += 2;
				/* If the directory is created, indicate an error and 
				remove the directory */
				if(OSAL_ERROR == OSALUTIL_s32RemoveDir(hDir1->fd
														,OEDT_C_STRING_CDROM_DIR2))
				{
					u32Ret_cdrom += 4;
				}
			}
			/* Close the parent directory */
			if( OSAL_ERROR == OSALUTIL_s32CloseDir ( hDir1 ) )
				{
					u32Ret_cdrom += 8;
				}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/* Open directory in readwrite mode */
		hDir2 = OSAL_IOOpen ( dir_name[dev_count], enAccess );
		if( hDir2 == (OSAL_tIODescriptor)OSAL_OK )
		{
			u32Ret += 1;
		}
		else
		{
			/* Copy sub-directory name to structure variable */
			(tVoid)OSAL_szStringCopy ((tString)(rDirent.s8Name), (tCString)(SUBDIR_NAME) );
			/* Create sub-directory */
			if ( OSAL_s32IOControl ( hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 3;
			}
			else
			{
				/* Remove sub-directory */
				if ( OSAL_s32IOControl ( hDir2,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 4;
				}
			}
			/* Close directory */
			if( OSAL_s32IOClose ( hDir2 ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);   
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateDelSubDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_009
* DESCRIPTION:  Try create and delete sub directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemCreateDelSubDir(tVoid)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_tIODescriptor hDir1 = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
	tChar szSubdirName1 [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW1";
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_trIOCtrlDirent rDirent;
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN];
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count],enAccess );
		if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 4;
			}
			(tVoid)OSAL_szStringCopy ((tString)(temp_ch), (tCString)(device_name[dev_count]) );
			(tVoid)OSAL_szStringConcat(temp_ch,"/MYNEW" );
			hDir1 = OSAL_IOOpen ( temp_ch, OSAL_EN_READONLY );
			if( hDir == OSAL_ERROR )
			{
				u32Ret += 8;
			}
			else
			{
				(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName1 );
				if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
											(tS32)&rDirent) != OSAL_ERROR )
				{
					u32Ret += 16;	
				}
			}
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName);
			if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
			{
				u32Ret += 64;
			}
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 128;
			}			
			if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
			{
				u32Ret += 256;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}

	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateDirInvalName( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_010
* DESCRIPTION:  Try create and delete directory with invalid path
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemCreateDirInvalName(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* invdir_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDT_C_STRING_FFS1_DIR_INV_NAME,
						OEDT_C_STRING_FFS2_DIR_INV_NAME,
						OEDT_C_STRING_FFS2_DIR_INV_NAME,
						OEDT_C_STRING_RAMDISK_DIR_INV_NAME,
						OEDT_C_STRING_USB_DIR_INV_NAME,
						OEDT_C_STRING_SDCARD_DIR_INV_NAME
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
    	if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,invdir_name[dev_count] );	  
	    	if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret += 2;
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemRmNonExstngDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_011
* DESCRIPTION:  Try to remove non exsisting directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemRmNonExstngDir(void)
{
	OSAL_tIODescriptor hDir = 0;
    OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir == (OSAL_tIODescriptor)OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,SUBDIR_NAME_INVAL);
	    	
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
									(tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret += 3;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}

	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemRmDirUsingIOCTRL( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_012
* DESCRIPTION:  Delete directory with which is not a directory (with files) 
*				using IOCTRL
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemRmDirUsingIOCTRL(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0,hDir1 = 0;
	OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};

	tChar* create_file_name[]= {
							 OEDT_C_STRING_FFS1_FILE1,
							 OEDT_C_STRING_FFS1_FILE1,
							 OEDT_C_STRING_FFS2_FILE1,
							 OEDT_C_STRING_FFS2_FILE1,
							 OEDT_C_STRING_RAMDISK_FILE1,
							 OEDT_C_STRING_USB_FILE1,
							 OEDT_C_STRING_SDCARD_FILE1
							 };
	
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir1 = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir1 == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			hFile = OSAL_IOCreate (create_file_name[dev_count], enAccess);
			if ( hFile == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				(tVoid)OSAL_szStringCopy ( rDirent.s8Name, 
												create_file_name[dev_count] );
				if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
									  (tS32)&rDirent) != OSAL_ERROR )
				{
					u32Ret += 10;
					if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
					{
						u32Ret += 50;
					}
					else if( OSAL_s32IORemove ( create_file_name[dev_count] )==OSAL_ERROR )
					{
						u32Ret += 20;
					}
				}
				else
				{
					if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
					{
						u32Ret += 100;
					}
					else if( OSAL_s32IORemove ( create_file_name[dev_count] )==OSAL_ERROR )
					{
						u32Ret += 20;
					}
				}	
			}
	    	if(OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR)
	    	{
	 	 	  u32Ret +=150;
	    	}	
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemGetDirInfo( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_013
* DESCRIPTION:  Get directory information
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 25, 2008 
******************************************************************************/
tU32 u32FileSystemGetDirInfo(void)
{
	OSAL_tIODescriptor hDir1 = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlDirent rDirent1,rDirent2;
	OSAL_trIOCtrlDir rDirCtrl;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};

	tU32 device_id = u32OEDT_BoardName() - 1;
	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hDir1 = OSAL_IOOpen ( device_name[0] ,OSAL_EN_READONLY );
	  	if ( hDir1 == OSAL_ERROR )
	   	{
	   		u32Ret_cdrom += 1;
	   	}
		else
		{
			/* Get DIR info for root DIR so that it displays the newly created Sub-dir */
			(tVoid)OSAL_szStringCopy ( rDirent2.s8Name,device_name[0] );
			rDirCtrl.fd = hDir1;
			rDirCtrl.s32Cookie = 0;
			rDirCtrl.dirent = rDirent2;
			if( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOREADDIR,
		                      (tS32) &rDirCtrl) == OSAL_ERROR	)
			{
				u32Ret_cdrom += 3;
			}
			else
			{
				OEDT_HelperPrintf(  OSAL_C_TRACELEVEL1,"DIR Info :\n\t %s \n"
									,(tString)rDirCtrl.dirent.s8Name );
				if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
				{
					u32Ret_cdrom += 5;
				}
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret;
			u32Ret = 0;
		}
	}
	/* For other device*/
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		(tVoid)OSAL_szStringCopy ( rDirent1.s8Name, SUBDIR_NAME );	  
		hDir1 = OSAL_IOOpen ( device_name[dev_count] ,enAccess );
    	if ( hDir1 == OSAL_ERROR )
    	{
    		u32Ret += 1;
    	}
		else
		{
			if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				/* Get DIR info for root DIR so that it displays the newly created Sub-dir */
				(tVoid)OSAL_szStringCopy ( rDirent2.s8Name,device_name[dev_count] );
				rDirCtrl.fd = hDir1;
				rDirCtrl.s32Cookie = 0;
				rDirCtrl.dirent = rDirent2;
				if( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOREADDIR,
		                       (tS32) &rDirCtrl) == OSAL_ERROR	)
				{
					u32Ret += 3;
				}
				else
				{
					OEDT_HelperPrintf(  OSAL_C_TRACELEVEL1,"DIR Info :\n\t %s \n"
										,(tString)rDirCtrl.dirent.s8Name );
					if ( OSAL_s32IOControl ( hDir1,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent1) == OSAL_ERROR )
					{
						u32Ret += 4;
					}
					else
					{
						if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
						{
							u32Ret += 5;
						}
					}
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemOpenDirDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_014
* DESCRIPTION:  Try to open directory in different modes
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemOpenDirDiffModes(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_tIODescriptor hDir1 = 0;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};

	tChar* subdir_path []= {
						 OEDT_C_STRING_CD_DIR1,
						 SUBDIR_PATH_FFS1,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_RAMDISK,
						 SUBDIR_PATH_USB,
						 SUBDIR_PATH_SDCARD
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	if ((Dev_Identity[0][device_id] == 1))
	{
		/* CDROM Open the device in READONLY mode */	
		hDir = OSAL_IOOpen ( OEDT_C_STRING_CD_DIR1,OSAL_EN_READONLY );
		if(OSAL_ERROR  == hDir )
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Close the device */
			if(OSAL_ERROR  ==  OSAL_s32IOClose(hDir))
			{
				u32Ret_cdrom += 2;
			}
		}
		/* Open the dir in READWRITE Mode */
		hDir = OSAL_IOOpen ( OEDT_C_STRING_CD_DIR1,OSAL_EN_READWRITE );
		if(OSAL_ERROR != hDir  )
		{
			//OEDT_HelperPrintf(TR_LEVEL_USER_1, "Open CDROM Dirin RW mode %s", OSAL_coszErrorText(hDir));
			u32Ret_cdrom += 4;
			/* The directory should not be opened in the READWRITE Mode.
			   If it does get opened, indicate an error and close it */
			if(OSAL_ERROR == OSAL_s32IOClose(hDir) )
			{
				u32Ret_cdrom += 8;
			}
			else
			{
				// OEDT_HelperPrintf(TR_LEVEL_USER_1, "Close CDROM Dir which opened in RW mode %s", OSAL_coszErrorText(hDir));
			}
		}
		/* Open the device in WRITEONLY Mode */
		hDir = OSAL_IOOpen ( OEDT_C_STRING_CD_DIR1,OSAL_EN_WRITEONLY );
		if(OSAL_ERROR != hDir  )
		{
			u32Ret_cdrom += 16;
			/* The directory should not be opened in the WRITEONLY Mode.
			   If it does get opened, indicate an error and close it */
			if(OSAL_ERROR == OSAL_s32IOClose(hDir)  )
			{
				u32Ret_cdrom += 32;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		enAccess = OSAL_EN_READWRITE;
 		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
  			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,SUBDIR_NAME );
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 3;
			}
			else
			{
				/*In Read write mode*/
			   	enAccess = OSAL_EN_READWRITE;
				hDir1 = OSAL_IOOpen ( subdir_path[dev_count], enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret += 4;
				}
				else 
				{  
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret += 5;
					}
				}
				/*In Write only mode*/
			  	enAccess = OSAL_EN_WRITEONLY;
				hDir1 = OSAL_IOOpen ( subdir_path[dev_count], enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret += 10;
				}
				else 
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret += 20;
					}
				}
				/*In Read only mode*/
			  	enAccess = OSAL_EN_READONLY;
				hDir1 = OSAL_IOOpen ( subdir_path[dev_count], enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret += 40;
				}
				else 
				{ 
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret += 80;
					}
				}
				
				if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 160;
				}
			}
			if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
			{
				u32Ret += 360;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemReOpenDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_015
* DESCRIPTION:  Try to reopen directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemReOpenDir(void)
{
	OSAL_trIOCtrlDir* hDir1 = NULL,*hDir2=NULL;
	OSAL_tIODescriptor hDir=0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;
	tU32 u32Ret_cdrom = CDROM_ERROR;	
	tU32 dev_count = 0;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar* dir_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_FFS1_DIR1,
						 OEDT_C_STRING_FFS2_DIR1,
						 OEDT_C_STRING_FFS2_DIR1,
						 OEDT_C_STRING_RAMDISK_DIR1,
						 OEDT_C_STRING_USB_DIR1,
						 OEDT_C_STRING_SDCARD_DIR1
						 };
	tChar* device_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_DEVICE_FFS1_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_FFS2_ROOT,
						 OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						 OEDT_C_STRING_DEVICE_USB_ROOT,
						 OEDT_C_STRING_DEVICE_CARD_ROOT
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	/* For CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hDir1 = OSALUTIL_prOpenDir ( dir_name[0]);
		if( OSAL_NULL  == hDir1) 
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Open the same directory again */ 
			hDir2 = OSALUTIL_prOpenDir ( dir_name[0]);
			if( OSAL_NULL  == hDir2) 
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				/* After opening twice, close the first handle */
				if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir2 ) )
				{
					u32Ret_cdrom += 4;
				}
			}
			/* Close the first handle */
			if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir1 ) )
			{
				u32Ret_cdrom += 8;
			}
		}	
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}			
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir == (OSAL_tIODescriptor)OSAL_OK )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name, SUBDIR_NAME );
			/* Create Dir */
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				hDir1 = OSALUTIL_prOpenDir ( dir_name[dev_count]);
				if( OSAL_NULL  == hDir1) 
				{
					u32Ret += 3;
				}
				else
				{
					/* Open the same directory again */ 
					hDir2 = OSALUTIL_prOpenDir ( dir_name[dev_count]);
					if( OSAL_NULL  == hDir2) 
					{
						u32Ret += 4;
					}
					else
					{
						/* After opening twice, close the first handle */
						if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir2 ) )
						{
							u32Ret += 5;
						}
					}
					/* Close the first handle */
					if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir1 ) )
					{
						u32Ret += 8;
					}
				}
				if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 160;
				}
			}	
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
 	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemDirParallelAccess( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_016
* DESCRIPTION:  When directory is accessed by another thread try to rename it
*				delete it
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 31, 2008 
******************************************************************************/
tU32 u32FileSystemDirParallelAccess(void)
{
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	/**************************************************/
	tU32 u32Ret = 0;

	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Access_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = FILESYSTEM_THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Access_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = FILESYSTEM_THREAD_STACK_SIZE;

	ps8UserBuf = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1)));
	if( OSAL_NULL == ps8UserBuf )
	{
		u32Ret += 2;
	}
	else
	{	/*Fill the buffer with some value*/
		OSAL_pvMemorySet( ps8UserBuf,'\0',(MB_SIZE+1) );
		OSAL_pvMemorySet( ps8UserBuf,'a',MB_SIZE);
	}
	/*thread stuff*/
	if(OSAL_OK == OSAL_s32EventCreate("Parallel_Access", &parallel_access))
	{
		/*create and activate thread1 */
		TID_1 = OSAL_ThreadSpawn(&rThAttr1);
		if(OSAL_ERROR != TID_1)
		{
			/*create thread2*/
			TID_2 = OSAL_ThreadSpawn(&rThAttr2);
			if(OSAL_ERROR != TID_2)
			{
				//wait for 2 events signalling the end of read operation
				if(OSAL_OK != OSAL_s32EventWait(parallel_access, FILESYSTEM_EVENT_WAIT,
							OSAL_EN_EVENTMASK_OR, FILESYSTEM_EVENT_WAIT_TIMEOUT, 
							&ResultMask))
				{
					u32Ret += 100;
				}
			}
			else
			{
				//thread2 creation failed
				u32Ret += 40;
			}
		}
		else
		{
			// thread1 creation failed
			u32Ret += 20;
		}
		
		//close the event
		if(OSAL_ERROR == OSAL_s32EventClose(parallel_access))
		{
			u32Ret += 200;
		}
		//delete the event
		if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Access"))
		{
			u32Ret += 400;
		}
	}	 
	else
	{
		//event creation failed
		u32Ret += 10;
	}
	//free the memory allocated
	if (OSAL_NULL != ps8UserBuf )
		OSAL_vMemoryFree(ps8UserBuf);
	return ( u32Ret + (tU32)s32ParallelRetval_th1 + (tU32)s32ParallelRetval_th2); 

}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateDirMultiTimes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_017
* DESCRIPTION:  Try to Create directory with similar name 
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 24, 2008 
******************************************************************************/
tU32 u32FileSystemCreateDirMultiTimes(void)
{
	OSAL_tIODescriptor hDir1 = 0,hDir2 = 0,hDir3 = 0;
	OSAL_trIOCtrlDirent rDirent1,rDirent2,rDirent3;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;						  
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDT_C_STRING_DEVICE_FFS1_ROOT,
						OEDT_C_STRING_DEVICE_FFS2_ROOT,
						OEDT_C_STRING_DEVICE_FFS2_ROOT,
						OEDT_C_STRING_DEVICE_RAMDISK_ROOT,
						OEDT_C_STRING_DEVICE_USB_ROOT,
						OEDT_C_STRING_DEVICE_CARD_ROOT

						};
	tChar* subdir_path1 []= {
						 OEDT_C_STRING_CD_DIR1,
						 SUBDIR_PATH_FFS1,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_RAMDISK,
						 SUBDIR_PATH_USB,
						 SUBDIR_PATH_SDCARD
						 };

	tChar* subdir_path2 []= {
							 OEDT_C_STRING_CD_DIR1,
							 SUBDIR_PATH2_FFS1,
							 SUBDIR_PATH2_FFS2,
							 SUBDIR_PATH2_FFS2,
							 SUBDIR_PATH2_RAMDISK,
							 SUBDIR_PATH2_USB,
							 SUBDIR_PATH2_SDCARD
							 };
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir1 = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir1 == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{ 
			(tVoid)OSAL_szStringCopy ( rDirent1.s8Name, SUBDIR_NAME );	  
			if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR, 
													(tS32)&rDirent1) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				(tVoid)OSAL_szStringCopy ( rDirent2.s8Name, SUBDIR_NAME2 );
				hDir2 = OSAL_IOOpen ( subdir_path1[dev_count], enAccess );
				if( hDir2 == OSAL_ERROR )
				{
					u32Ret += 3;
				}
				else
				{
					if ( OSAL_s32IOControl ( hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
													(tS32)&rDirent2) == OSAL_ERROR)
					{
						u32Ret += 4;
					}
					else
					{
						(tVoid)OSAL_szStringCopy ( rDirent3.s8Name,SUBDIR_NAME3 );
						hDir3 = OSAL_IOOpen ( subdir_path2[dev_count], enAccess );
						if( hDir3 == OSAL_ERROR )
						{
							u32Ret += 5;
						}
						else
						{
							if ( OSAL_s32IOControl ( hDir3, OSAL_C_S32_IOCTRL_FIOMKDIR,
												(tS32)&rDirent3) == OSAL_ERROR )
							{
								u32Ret += 6;
							}
							else
							{
								OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
													"\n\t Third level Sub-Dir created" );
								if ( OSAL_s32IOControl ( hDir3, OSAL_C_S32_IOCTRL_FIORMDIR,
										(tS32)&rDirent3) == OSAL_ERROR )
								{
									u32Ret += 20;
								}
							}
							OSAL_s32IOClose ( hDir3 );
						}
						if ( OSAL_s32IOControl ( hDir2, OSAL_C_S32_IOCTRL_FIORMDIR,
										(tS32)&rDirent2) == OSAL_ERROR )
						{
							u32Ret += 40;
						}
					}
					OSAL_s32IOClose ( hDir2 );
				}	
				if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
									(tS32)&rDirent1) == OSAL_ERROR )
				{
					u32Ret += 80;
				}
			}
			OSAL_s32IOClose ( hDir1 );
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}

	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateRemDirInvalPath( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_018
* DESCRIPTION:  Create/ remove directory with Invalid path
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32FileSystemCreateRemDirInvalPath(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* dir_invpath []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_FFS1_DIR_INV_PATH,
						 OEDT_C_STRING_FFS2_DIR_INV_PATH,
						 OEDT_C_STRING_FFS2_DIR_INV_PATH,
						 OEDT_C_STRING_RAMDISK_DIR_INV_PATH,
						 OEDT_C_STRING_USB_DIR_INV_PATH,
						 OEDT_C_STRING_SDCARD_DIR_INV_PATH
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
    	if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}	
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,dir_invpath[dev_count] );	  
	    	if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret += 2;
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if ( OSAL_s32IOClose ( hDir )== OSAL_ERROR )
			{
				u32Ret += 20;
			}
		}	
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32FileSystemCreateRmNonEmptyDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_019
* DESCRIPTION:  Try to remove non Empty directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32FileSystemCreateRmNonEmptyDir(void)
{
	OSAL_tIODescriptor hDir1 = 0,hDir2 = 0;
    OSAL_trIOCtrlDirent rDirent1,rDirent2;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* subdir_path1 []= {
						 OEDT_C_STRING_CD_DIR1,
						 SUBDIR_PATH_FFS1,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_RAMDISK,
						 SUBDIR_PATH_USB,
						 SUBDIR_PATH_SDCARD
						 };
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir1 = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir1 == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( (tString)rDirent1.s8Name, (tCString)SUBDIR_NAME );	  
		   if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
											  (tS32)&rDirent1) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				hDir2 = OSAL_IOOpen ( subdir_path1[dev_count], enAccess );
				if( hDir2 == OSAL_ERROR )
				{
					u32Ret += 3;
					if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent1) == OSAL_ERROR )
					{
						u32Ret += 7;
					}
				}
				else
				{
					(tVoid)OSAL_szStringCopy ( rDirent2.s8Name, SUBDIR_NAME2 );	
					if ( OSAL_s32IOControl( hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
												(tS32)&rDirent2) == OSAL_ERROR )
					{
						u32Ret += 14;
						if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
													(tS32)&rDirent1) == OSAL_ERROR )
						{
							u32Ret += 28;
						}
					}
					else
				   	{			
				   		if ( OSAL_s32IOControl( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent1) != OSAL_ERROR )
						{
							u32Ret += 56;
						}
					    else
						{
					    	if ( OSAL_s32IOControl( hDir2,OSAL_C_S32_IOCTRL_FIORMDIR,
												  (tS32)&rDirent2) == OSAL_ERROR )
							{
								u32Ret += 112;
							}
							else
							{
								OSAL_s32IOClose ( hDir2 );
								if ( OSAL_s32IOControl ( hDir1,
														OSAL_C_S32_IOCTRL_FIORMDIR,
													(tS32)&rDirent1) == OSAL_ERROR)
								{
									u32Ret += 224;
								}
							}
						}
					}
				}
			}
		OSAL_s32IOClose ( hDir1 );
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 	  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCopyDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_020
* DESCRIPTION:  Copy the files in source directory to destination directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct cc, 2008 
******************************************************************************/
tU32 u32FileSystemCopyDir(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
	OSAL_trIOCtrlDir* pDir;
   OSAL_trIOCtrlDirent* pEntry;
	tS32 hDir1 = 0;
	tS32 hDir2 = 0;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tChar* arrFileName_tmp[2]={OSAL_NULL};
	tChar arrFileName[2][256];
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* files13 []= {
						 OEDT_C_STRING_CD_DIR1,
						 FILE13_FFS1,
						 FILE13_FFS2,
						 FILE13_FFS2,
						 FILE13_RAMDISK,
						 FILE13_USB,
						 FILE13_SDCARD
						 };
	tChar* files14 []= {
						 OEDT_C_STRING_CD_DIR1,
						 FILE14_FFS1,
						 FILE14_FFS2,
						 FILE14_FFS2,
						 FILE14_RAMDISK,
						 FILE14_USB,
						 FILE14_SDCARD
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)	
	{
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
		if (OSAL_ERROR == hDevice)
		{
			u32Ret += 1;
		}
		else
		{
			/*Create the source directory  in readwrite mode */
	    	hDir1 = OSALUTIL_s32CreateDir (hDevice,"/"DIR1);
			if (hDir1 == OSAL_ERROR)
			{
				u32Ret += 2;
				OSAL_s32IOClose(hDevice);
			}
			else
			{
				/*Create the destination directory  in readwrite mode */
		        hDir2 = OSALUTIL_s32CreateDir (hDevice,"/"DIR); 
				if (hDir2 == OSAL_ERROR)
				{
					u32Ret += 3;
					OSAL_s32IOClose(hDevice);
				}
	         else 
				{
					/*Create the first file in source directory */
					hFile1 = OSAL_IOCreate(files13[dev_count] , enAccess);
					if (hFile1 == OSAL_ERROR )
	    			{
						u32Ret += 4;
						OSALUTIL_s32RemoveDir(hDevice,DIR1);
						OSAL_s32IOClose(hDevice);
					}
					else 
					{
						/*Create second file in source directory*/
	   				hFile2 = OSAL_IOCreate (files14[dev_count] ,enAccess);
						if (hFile2 == OSAL_ERROR)
						{
							u32Ret += 5;
							OSAL_s32IOClose(hFile1);
							OSAL_s32IORemove(files13[dev_count]);
							OSALUTIL_s32RemoveDir(hDevice,DIR1);
							OSALUTIL_s32RemoveDir(hDevice,DIR);
							OSAL_s32IOClose(hDevice);
						}
						else 
						{
							(tVoid)OSAL_szStringCopy ( arrFileName[0], device_name[dev_count] );
							(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR1); /* Source     Directory */
							(tVoid)OSAL_szStringCopy ( arrFileName[1], device_name[dev_count] );
							(tVoid)OSAL_szStringConcat(arrFileName[1],"/"DIR ); /* Source     Directory */
							arrFileName_tmp[0]= arrFileName[0];
							arrFileName_tmp[1]= arrFileName[1];
							if(OSAL_ERROR == OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOCOPYDIR,
	    		    										(tS32)arrFileName_tmp ))
							{
								u32Ret += 6;
								OSAL_s32IOClose(hFile1);
								OSAL_s32IOClose(hFile2);
								OSAL_s32IORemove(files13[dev_count]);
								OSAL_s32IORemove(files14[dev_count]);
								OSALUTIL_s32RemoveDir(hDevice,DIR1);
								OSALUTIL_s32RemoveDir(hDevice,DIR);
								OSAL_s32IOClose(hDevice);
							}
							else  
							{	
								pDir = OSALUTIL_prOpenDir (arrFileName_tmp[1]);
							   pEntry = OSALUTIL_prReadDir(pDir);
								if (OSAL_NULL == pDir )
								{

									u32Ret += 25;
								}
								else
								{
									while (pEntry != NULL)
								   {
								   	OEDT_HelperPrintf(TR_LEVEL_USER_1, "\tDir read value %s\n",pEntry->s8Name);
								      pEntry = OSALUTIL_prReadDir(pDir);							      
								   }
								}
							   OSALUTIL_s32CloseDir (pDir);
							    /*Close the first file in source directory*/ 
								if  (OSAL_ERROR == OSAL_s32IOClose(hFile1))
								{	
									u32Ret += 7;
									OSAL_s32IOClose(hDevice);
								}
								else
									
								{
								 	/*Close the second file in source directory*/ 
			       				if (OSAL_ERROR == OSAL_s32IOClose(hFile2))
			       				{
			       					u32Ret += 8;
										OSAL_s32IOClose(hDevice);
			    					}
									else 
									{	
									
							   		/*Remove the first file in the source directory*/
							   		if(OSAL_ERROR ==  OSAL_s32IORemove(files13[dev_count]))
										{
											u32Ret += 9;
									   	OSAL_s32IOClose(hDevice);
										}
										else
										{
											/*Remove the second file in the source directory*/ 
											if  (OSAL_ERROR ==  OSAL_s32IORemove(files14[dev_count]))					
											{
												u32Ret += 10;
												OSAL_s32IOClose(hDevice);
											}
											else
											{
									   		/* Remove the first copied file in the destination directory*/
												(tVoid)OSAL_szStringCopy (arrFileName[0], device_name[dev_count] );
												(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File13.txt" );
												if(OSAL_ERROR ==  OSAL_s32IORemove(arrFileName[0]))
												{
													u32Ret += 11;
													OSAL_s32IOClose(hDevice);
												}
								        		else
												{
													/*Remove the second copied file in the destination  directory*/
													(tVoid)OSAL_szStringCopy (arrFileName[0], device_name[dev_count] );
													(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File14.txt");
										 			if(OSAL_ERROR ==  OSAL_s32IORemove(arrFileName[0]))
													{
														u32Ret += 12;
														OSAL_s32IOClose(hDevice);
													}
													else 
													{
														/*Remove the source directory */
														if (OSAL_ERROR == OSALUTIL_s32RemoveDir(
																				hDevice,DIR1))
														{
															u32Ret += 13;
															OSAL_s32IOClose(hDevice);
														}
														else
														{
												    		/*Remove the destination directory*/
															if  (OSAL_ERROR == OSALUTIL_s32RemoveDir(
																							hDevice,DIR))
															{
																u32Ret += 14;
																OSAL_s32IOClose(hDevice);
															}
															else
													   		{
																if  (OSAL_ERROR == OSAL_s32IOClose
																						(hDevice))
																{
																	u32Ret += 15;
																}	
															}/*Sucess of removal of destination directory*/
														}/*Sucess of removal of source directory*/
													}/*Sucess of removal of second file in destination*/
												}/*Sucess of removal of first file in destination*/		
											}/*Sucess of removal of second file in source*/
										}/*Sucess of removal of first file in source*/				
									}/*Sucess of closure of second file in source*/
								}/*Sucess of closure of first file in source*/
							}/*Sucess of IO Control function*/
							
						}/*Sucess of creation of second file in source*/
					}/*Sucess of creation of first file in source*/
				}/*Sucess of creation of destination directory*/
			}/*	Sucess of creation of source directory*/
		}/*Sucess of open device*/
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemMultiCreateDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_021
* DESCRIPTION:  Create multiple sub directories
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemMultiCreateDir(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)  ); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess ); /* open the root dir */
	   if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name, SUBDIR_NAME3 );	  
	    	if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
											  (tS32)&rDirent) != OSAL_ERROR )
				{
					u32Ret += 3;
				}
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if ( OSAL_s32IOClose ( hDir )== OSAL_ERROR )
			{
				u32Ret += 100;
			}
		}
		if (u32Ret) 
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		Old_u32Ret += u32Ret; 
		u32Ret = 0;
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCreateSubDir( )
* PARAMETER:   none
* RETURNVALUE: tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:   TU_OEDT_FILESYSTEM_022
* DESCRIPTION: Attempt to create sub-directory within a directory opened
*              in READONLY modey
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemCreateSubDir(tVoid)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_tIODescriptor hDir1 = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* subdir_path1 []= {
						 OEDT_C_STRING_CD_DIR1,
						 SUBDIR_PATH_FFS1,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_FFS2,
						 SUBDIR_PATH_RAMDISK,
						 SUBDIR_PATH_USB,
						 SUBDIR_PATH_SDCARD
						 };
	OSAL_trIOCtrlDirent rDirent;
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
		if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,SUBDIR_NAME );
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
				
			hDir1 = OSAL_IOOpen ( subdir_path1[dev_count], OSAL_EN_READONLY );
			if( hDir == OSAL_ERROR )
			{
				u32Ret += 20;
			}
			else
			{
				(tVoid)OSAL_szStringCopy ( rDirent.s8Name, SUBDIR_NAME2 );
				if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
												(tS32)&rDirent) != OSAL_ERROR )
				{
					/* If successful, indicate error and remove directory */
					u32Ret += 50;	
					if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
					{
						u32Ret += 100;
					}
				}
			}
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name, SUBDIR_NAME);
			if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
			{
				u32Ret += 130;
			}
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 260;
			}			
			if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
			{
				u32Ret += 520;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemDelInvDir ( )
* PARAMETER:	none
* RETURNVALUE: tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:   TU_OEDT_FILESYSTEM_023
* DESCRIPTION: Delete invalid Directory
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32  u32FileSystemDelInvDir(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar* device_name []= {
						OSAL_C_STRING_DEVICE_CDROM,
						OEDTTEST_C_STRING_DEVICE_FFS1,
						OEDTTEST_C_STRING_DEVICE_FFS2,
						OEDTTEST_C_STRING_DEVICE_FFS3,
						OSAL_C_STRING_DEVICE_RAMDISK,
						OSAL_C_STRING_DEVICE_USB,
						OSAL_C_STRING_DEVICE_CARD
						};
	tChar* inv_dir_name []= {
						 OEDT_C_STRING_CD_DIR1,
						 OEDT_C_STRING_FFS1_DIR_INV_NAME,
						 OEDT_C_STRING_FFS2_DIR_INV_NAME,
						 OEDT_C_STRING_FFS2_DIR_INV_NAME,
						 OEDT_C_STRING_RAMDISK_DIR_INV_NAME,
						 OEDT_C_STRING_USB_DIR_INV_NAME,
						 OEDT_C_STRING_SDCARD_DIR_INV_NAME
						 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDir = OSAL_IOOpen ( device_name[dev_count], enAccess );
   		if( hDir == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			(tVoid)OSAL_szStringCopy ( rDirent.s8Name,inv_dir_name[dev_count] );	  
	   	if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 				  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}		
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemCopyDirRec( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_024
* DESCRIPTION:  Copy directories recursively
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32  u32FileSystemCopyDirRec(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
	OSAL_trIOCtrlDir* pDir;
   	OSAL_trIOCtrlDirent* pEntry;
	tS32 hDir1 = 0;
	tS32 hDir2 = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tChar* arrFileName_tmp[2]={OSAL_NULL};
	tChar arrFileName[2][256];
	tU32 dev_count = 0;
	tChar* device_name []= {
							OSAL_C_STRING_DEVICE_CDROM,
							OEDTTEST_C_STRING_DEVICE_FFS1,
							OEDTTEST_C_STRING_DEVICE_FFS2,
							OEDTTEST_C_STRING_DEVICE_FFS3,
							OSAL_C_STRING_DEVICE_RAMDISK,
							OSAL_C_STRING_DEVICE_USB,
							OSAL_C_STRING_DEVICE_CARD
							};
	tChar* files13 []= {
							 OEDT_C_STRING_CD_DIR1,
							 FILE13_FFS1,
							 FILE13_FFS2,
							 FILE13_FFS2,
							 FILE13_RAMDISK,
							 FILE13_USB,
							 FILE13_SDCARD
							 };
	tChar* files14 []= {
							 OEDT_C_STRING_CD_DIR1,
							 FILE14_FFS1,
							 FILE14_FFS2,
							 FILE14_FFS2,
							 FILE14_RAMDISK,
							 FILE14_USB,
							 FILE14_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
		if (OSAL_ERROR == hDevice)
		{
			u32Ret += 1;
		}
		else
		{
			/*Create the source directory  in readwrite mode */
			hDir1 = OSALUTIL_s32CreateDir (hDevice,"/"DIR1);
			if (hDir1 == OSAL_ERROR)
			{
				u32Ret += 2;
				OSAL_s32IOClose(hDevice);
			}
			else
			{
				/*Create the destination directory	in readwrite mode */
				hDir2 =OSALUTIL_s32CreateDir (hDevice,"/"DIR); 
				if (hDir2 == OSAL_ERROR)
				{
					u32Ret += 3;
					OSAL_s32IOClose(hDevice);
				}
				else 
				{	/*Create the first file in source directory */
					hFile1 = OSAL_IOCreate(files13[dev_count] , enAccess);
					if (hFile1 == OSAL_ERROR )
					{
						u32Ret += 4;
						OSALUTIL_s32RemoveDir(hDevice,DIR1);
						OSAL_s32IOClose(hDevice);
					}
					else 
					{
						/*Create second file in source directory*/
						hFile2 = OSAL_IOCreate (files14[dev_count] ,enAccess);
						if (hFile2 == OSAL_ERROR)
						{
							u32Ret += 5;
							OSAL_s32IOClose(hFile1);
							OSAL_s32IORemove(files13[dev_count]);
							OSALUTIL_s32RemoveDir(hDevice,DIR1);
							OSAL_s32IOClose(hDevice);
						}
						else 
						{
							(tVoid)OSAL_szStringCopy ( arrFileName[0], device_name[dev_count] );
							(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR1 ); /* Source		Directory */
							(tVoid)OSAL_szStringCopy ( arrFileName[1], device_name[dev_count] );
							(tVoid)OSAL_szStringConcat(arrFileName[1],"/"DIR ); /* Source	  Directory */
							arrFileName_tmp[0]= arrFileName[0];
							arrFileName_tmp[1]= arrFileName[1];
							if(OSAL_ERROR == OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOCOPYDIR,
															(tS32)arrFileName_tmp ))
							{
								u32Ret += 6;
								OSAL_s32IOClose(hFile1);
								OSAL_s32IOClose(hFile2);
								OSAL_s32IORemove(files13[dev_count]);
								OSAL_s32IORemove(files14[dev_count]);
								OSALUTIL_s32RemoveDir(hDevice,DIR1);
								OSALUTIL_s32RemoveDir(hDevice,DIR);
								OSAL_s32IOClose(hDevice);
							}
							else	
							{	
								pDir = OSALUTIL_prOpenDir (arrFileName_tmp[1]);
							   pEntry = OSALUTIL_prReadDir(pDir);
								if (OSAL_NULL == pDir )
								{

									u32Ret += 25;
								}
								else
								{
									while (pEntry != NULL)
								   {
								   	OEDT_HelperPrintf(TR_LEVEL_USER_1, "\tDir read value %s\n",pEntry->s8Name);
								      pEntry = OSALUTIL_prReadDir(pDir);							      
								   }
								}
							   OSALUTIL_s32CloseDir (pDir);
								/*Close the first file in source directory*/ 
								if  (OSAL_ERROR == OSAL_s32IOClose(hFile1))
								{	
									u32Ret += 7;
									OSAL_s32IOClose(hDevice);
								}
								else
								{
									/*Close the second file in source directory*/ 
									if (OSAL_ERROR == OSAL_s32IOClose(hFile2))
									{
										u32Ret += 8;
										OSAL_s32IOClose(hDevice);
									}
									else 
									{	
										/*Remove the first file in the source directory*/ 
										if(OSAL_ERROR ==	OSAL_s32IORemove(files13[dev_count]))
										{
											u32Ret += 9;
											OSAL_s32IOClose(hDevice);
										}
										else
										{
											/*Remove the second file in the source directory*/ 
											if  (OSAL_ERROR ==  OSAL_s32IORemove(files14[dev_count]))					
											{
													u32Ret += 10;
												OSAL_s32IOClose(hDevice);
											}
											else
											{
												/* Remove the first copied file in the destination directory*/
												(tVoid)OSAL_szStringCopy (arrFileName[0], device_name[dev_count] );
												(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File13.txt" );
												if(OSAL_ERROR ==	OSAL_s32IORemove(arrFileName[0]))
												{
													u32Ret += 11;
													OSAL_s32IOClose(hDevice);
												}
												else
												{
													/*Remove the second copied file in the destination  directory*/
													(tVoid)OSAL_szStringCopy (arrFileName[0], device_name[dev_count] );
													(tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File14.txt");
													if(OSAL_ERROR ==	OSAL_s32IORemove(arrFileName[0]))
													{
														u32Ret += 12;
														OSAL_s32IOClose(hDevice);
													}
													else 
													{
														/*Remove the source directory */
														if (OSAL_ERROR == OSALUTIL_s32RemoveDir(
																				hDevice,DIR1))
														{
															u32Ret += 13;
															OSAL_s32IOClose(hDevice);
														}
														else
														{
															/*Remove the destination directory*/
															if  (OSAL_ERROR == OSALUTIL_s32RemoveDir(
																							hDevice,DIR))
															{
																u32Ret += 14;
																OSAL_s32IOClose(hDevice);
															}
															else
															{
																if  (OSAL_ERROR == OSAL_s32IOClose
																					(hDevice))
																{
																	u32Ret += 15;
																}	
															}/*Sucess of removal of destination directory*/
														}/*Sucess of removal of source directory*/
													}/*Sucess of removal of second file in destination*/
												}/*Sucess of removal of first file in destination*/		
											}/*Sucess of removal of second file in source*/
										}/*Sucess of removal of first file in source*/				
									}/*Sucess of closure of second file in source*/
								}/*Sucess of closure of first file in source*/
							}/*Sucess of IO Control function*/
						}/*Sucess of creation of second file in source*/
					}/*Sucess of creation of first file in source*/
				}/*Sucess of creation of destination directory*/
			}/*	Sucess of creation of source directory*/
		}/*Sucess of open device*/
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}




/*****************************************************************************
* FUNCTION:		u32FileSystemRemoveDir ( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_025
* DESCRIPTION:  Remove directories recursively
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemRemoveDir(void)
{
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 dev_count = 0;
	tChar temp_ch[200];
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar* device_name []= {
							OSAL_C_STRING_DEVICE_CDROM,
							OEDTTEST_C_STRING_DEVICE_FFS1,
							OEDTTEST_C_STRING_DEVICE_FFS2,
							OEDTTEST_C_STRING_DEVICE_FFS3,
							OSAL_C_STRING_DEVICE_RAMDISK,
							OSAL_C_STRING_DEVICE_USB,
							OSAL_C_STRING_DEVICE_CARD
							};
	tChar* files11 []= {
							 OEDT_C_STRING_CD_DIR1,
							 FILE11_FFS1,
							 FILE11_FFS2,
							 FILE11_FFS2,
							 FILE11_RAMDISK,
							 FILE11_USB,
							 FILE11_SDCARD
							 };
	tChar* files12 []= {
							 OEDT_C_STRING_CD_DIR1,
							 FILE12_FFS1,
							 FILE12_FFS2,
							 FILE12_FFS2,
							 FILE12_RAMDISK,
							 FILE12_USB,
							 FILE12_SDCARD
							 };
	tChar* files12_rec []= {
							 OEDT_C_STRING_CD_DIR1,
							 FILE12_RECR2_FFS1,
							 FILE12_RECR2_FFS2,
							 FILE12_RECR2_FFS2,
							 FILE12_RECR2_RAMDISK,
							 FILE12_RECR2_USB,
							 FILE12_RECR2_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/*Open the device in read write mode */
	   hDevice = OSAL_IOOpen( device_name[dev_count], enAccess );
	   if(hDevice == OSAL_ERROR)
	   {
			u32Ret += 1;
	   }
	   else
	   {
	   	/*Create the directory in the device */
			if(OSAL_ERROR == OSALUTIL_s32CreateDir(hDevice, DIR ))
			{
				u32Ret += 20;
			}

		 	/*Create some files in read write mode */
	      hFile1 = OSAL_IOCreate(files11[dev_count] , enAccess);
	      if (hFile1 == OSAL_ERROR )
	      {
				u32Ret += 1;
			}
		 	else 
		 	{
		   	if(OSAL_ERROR == OSALUTIL_s32CreateDir(hDevice, DIR"/"DIR_RECR1 ))
		   	{
		   		u32Ret += 30;
		   	}
				/*Create another file */
		   	hFile2 = OSAL_IOCreate (files12_rec[dev_count] ,enAccess);
		   	if (hFile2 == OSAL_ERROR)
				{
					u32Ret += 2;
				}
				else 
				{
		      	OSAL_s32IOClose(hFile1);
					OSAL_s32IOClose(hFile2);
		            
		    		if(OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
		    											(tS32) DIR )) 
					{
						u32Ret += 3;
						OSAL_s32IORemove(files11[dev_count]);
						OSAL_s32IORemove(files12[dev_count]);
						(tVoid)OSAL_szStringCopy ( temp_ch, device_name[dev_count] );
						(tVoid)OSAL_szStringConcat(temp_ch, "/"DIR );
						OSALUTIL_s32RemoveDir(hDevice, temp_ch );
						(tVoid)OSAL_szStringCopy ( temp_ch, device_name[dev_count] );
						(tVoid)OSAL_szStringConcat(temp_ch, "/"DIR"/"DIR_RECR1 );
		            	OSALUTIL_s32RemoveDir(hDevice, temp_ch);
						OSAL_s32IOClose(hDevice);
					}
					else
					{
						/*Remove the first file in the dir*/
						if (OSAL_ERROR !=  OSAL_s32IORemove(files11[dev_count]))
						{
							u32Ret += 6;
							OSAL_s32IOClose(hDevice);
						}
						else
						{
							/*Remove the  second file in the dir*/
							if (OSAL_ERROR !=  OSAL_s32IORemove(files12[dev_count]))					
							{
								u32Ret += 7;
								OSAL_s32IOClose(hDevice);
							}
							else
							{
								/*Check for directory removal*/
								(tVoid)OSAL_szStringCopy ( temp_ch, device_name[dev_count] );
								(tVoid)OSAL_szStringConcat(temp_ch, "/"DIR );
		     					if (OSAL_ERROR != OSALUTIL_s32RemoveDir(hDevice, 
		     								temp_ch))
								{
									u32Ret += 8;
									OSAL_s32IOClose(hDevice);
								}
								else
								{
									/*Close the device */
									if (OSAL_ERROR == OSAL_s32IOClose(hDevice))
									{
										u32Ret += 9;
									}
								}
							}
						}
					}
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
 	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateDel( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_026
* DESCRIPTION:  Create/ Delete file  with correct parameters
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileCreateDel(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	
	tU32 dev_count = 0;
	tChar* file_path []= {
							 OEDT_C_STRING_CD_NONEXST,
							 CREAT_FILE_PATH_FFS1,
							 CREAT_FILE_PATH_FFS2,
							 CREAT_FILE_PATH_FFS2,
							 CREAT_FILE_PATH_RAMDISK,
							 CREAT_FILE_PATH_USB,
							 CREAT_FILE_PATH_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;

	/* CDROM */
	/* Open a directory first and then attempt to create a file within it */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hDir = OSALUTIL_prOpenDir(OEDT_C_STRING_CD_DIR_CRE_PAR);
		if(OSAL_NULL == hDir)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Attempt to create a file within this directory
			   Should fail	 */
			hFile = OSAL_IOCreate (CREAT_FILE_PATH_CDROM, enAccess);

			if (OSAL_ERROR != hFile )
			{
				u32Ret_cdrom += 2;
				/* If the file is successfully created	indicate an error 
					and close the file before removing it */
				if(OSAL_ERROR == OSAL_s32IOClose ( hFile ))
				{
					u32Ret_cdrom += 4;
				}
				/* Now remove the file */
				else if( OSAL_ERROR == OSAL_s32IORemove ( CREAT_FILE_PATH_CDROM ))
				{
					u32Ret_cdrom += 8;
				}
			}
			/* Close the directory */
			if (OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
			{
				u32Ret_cdrom += 16;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;

	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hFile = OSAL_IOCreate (file_path[dev_count], enAccess);
		if ( hFile == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 3;
			}

			else if( OSAL_s32IORemove ( file_path[dev_count] ) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);		  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileOpenClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_027
* DESCRIPTION:  Open /close File
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileOpenClose(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tU32 dev_count = 0;
	tChar* file_path []= {
							 OSAL_TEXT_FILE_FIRST_CDROM,
							 OSAL_TEXT_FILE_FIRST_FFS1,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_RAMDISK,
							 OSAL_TEXT_FILE_FIRST_USB,
							 OSAL_TEXT_FILE_FIRST_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	/* Open the file in CDROM*/
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST_CDROM, OSAL_EN_READONLY);
		if(OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Close the file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile) )
			{
				u32Ret_cdrom += 2;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;

	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if ( (hFile = OSAL_IOCreate (file_path[dev_count], enAccess)) 
									== OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				hFile = OSAL_IOOpen ( file_path[dev_count], enAccess );
				if( hFile == OSAL_ERROR )
				{
					u32Ret += 10;
					if( OSAL_s32IORemove ( file_path[dev_count] ) 
										== OSAL_ERROR )
					{
						u32Ret += 50;
					}
				}
				else
				{
					if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
					{
						u32Ret += 20;
					}
					else if( OSAL_s32IORemove ( file_path[dev_count] )
								== OSAL_ERROR )
					{
						u32Ret += 30;
					}
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}	


/*****************************************************************************
* FUNCTION:		u32FileSystemFileOpenInvalPath( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_028
* DESCRIPTION:  Open file with invalid path name (should fail)
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileOpenInvalPath(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 dev_count = 0;
	tChar* file_path []= {
							 OEDT_C_STRING_FILE_INVPATH_CDROM,
							 OEDT_C_STRING_FILE_INVPATH_FFS1,
							 OEDT_C_STRING_FILE_INVPATH_FFS2,
							 OEDT_C_STRING_FILE_INVPATH_FFS2,
							 OEDT_C_STRING_FILE_INVPATH_RAMDISK,
							 OEDT_C_STRING_FILE_INVPATH_USB,
							 OEDT_C_STRING_FILE_INVPATH_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hFile = OSAL_IOOpen ( file_path[dev_count] ,enAccess );
		if(  OSAL_ERROR != hFile  )
		{
			u32Ret += 1;
			/* If Open is successfull, indicate an error and close the file */
			if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
			{
				u32Ret += 2;
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileOpenInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_029
* DESCRIPTION:  Open a file with invalid parameters (should fail), 
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileOpenInvalParam(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_INVALID_ACCESS_MODE;
	
	tU32 dev_count = 0;
	tChar* file_path []= {
							 OSAL_TEXT_FILE_FIRST_CDROM,
							 OSAL_TEXT_FILE_FIRST_FFS1,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_RAMDISK,
							 OSAL_TEXT_FILE_FIRST_USB,
							 OSAL_TEXT_FILE_FIRST_SDCARD
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 0; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		hFile = OSAL_IOOpen(file_path[dev_count], enAccess);
		if(OSAL_ERROR != hFile)
		{
			u32Ret += 4;
			/* If open is successfull, indicate an error and close the file */
			if(OSAL_ERROR == OSAL_s32IOClose (hFile) )
			{
				u32Ret += 8;
			}
		} 
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_030
* DESCRIPTION:  Try to open and close the file which is already opened
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32  u32FileSystemFileReOpen(void)
{
	OSAL_tIODescriptor hFile1 = 0,hFile2 = 0, hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 device_id = u32OEDT_BoardName() - 1;
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tU32 dev_count = 0;
	tChar* file_path []= {
							 OSAL_TEXT_FILE_FIRST_CDROM,
							 OSAL_TEXT_FILE_FIRST_FFS1,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_FFS2,
							 OSAL_TEXT_FILE_FIRST_RAMDISK,
							 OSAL_TEXT_FILE_FIRST_USB,
							 OSAL_TEXT_FILE_FIRST_SDCARD
							 };

	/* CRDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile1 = OSAL_IOOpen ( file_path[0], enAccess );
		if( OSAL_ERROR == hFile1 )
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Re Opening The File, should be possible */
			hFile2 = OSAL_IOOpen ( file_path[0], enAccess );
			if(OSAL_ERROR == hFile2 )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				/* close the second handle of the file */
				if( OSAL_ERROR == OSAL_s32IOClose ( hFile2 ) )
				{
					u32Ret_cdrom += 4;
				}
			}
			/* Close the first handle of the file */
			if(  OSAL_ERROR == OSAL_s32IOClose ( hFile1 ) )
			{
				u32Ret_cdrom += 8;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}

	/* For others*/
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		/* File create*/
		hFile = OSAL_IOCreate (file_path[dev_count], enAccess);
		if ( hFile == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else 
		{
			hFile1 = OSAL_IOOpen ( file_path[dev_count], enAccess );
			if( OSAL_ERROR == hFile1 )
			{
				u32Ret += 1;
			}
			else
			{
				/* Re Opening The File, should be possible */
				hFile2 = OSAL_IOOpen ( file_path[dev_count], enAccess );
				if(OSAL_ERROR == hFile2 )
				{
					u32Ret += 2;
				}
				else
				{
					/* close the second handle of the file */
					if( OSAL_ERROR == OSAL_s32IOClose ( hFile2 ) )
					{
						u32Ret += 4;
					}
				}
				/* Close the first handle of the file */
				if(  OSAL_ERROR == OSAL_s32IOClose ( hFile1 ) )
				{
					u32Ret += 8;
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 3;
				}
				else if( OSAL_s32IORemove ( file_path[dev_count] ) == OSAL_ERROR )
				{
					u32Ret += 2;
				}
			}
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileRead( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_031
* DESCRIPTION:  Read data from already exsisting file
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileRead(void)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0, Old_u32Ret = 0;	 
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tS8 *ps8ReadBuffer = NULL;     
	tU32 u32BytesToRead = 10;
	tS32 s32BytesRead = 0;	
	tU32 dev_count = 0;
	tChar* common_file []= {
							 OEDT_FFS1_COMNFILE,
							 OEDT_FFS1_COMNFILE,
							 OEDT_FFS2_COMNFILE,
							 OEDT_FFS2_COMNFILE,
							 OEDT_RAMDISK_COMNFILE,
							 OEDT_USB_COMNFILE,
							 OEDT_SDCARD_COMNFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;

	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
	  	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret_cdrom += 1;
		}
		else
		{  	
			ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead + 1);
			if ( ps8ReadBuffer == NULL )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,
		        		u32BytesToRead );
				if ( (s32BytesRead == (tS32)OSAL_ERROR)||(s32BytesRead != (tS32)u32BytesToRead) )		 
		 		{
		 			u32Ret_cdrom += 3;	
				}
				else
		 		{
					OEDT_HelperPrintf( OSAL_C_TRACELEVEL1, 
							"\n\t Bytes read : %d",s32BytesRead );	
				}
				if(ps8ReadBuffer != OSAL_NULL)
				{
					OSAL_vMemoryFree( ps8ReadBuffer );
					ps8ReadBuffer = OSAL_NULL;
				}
			}
		 	if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		 	{
		 		u32Ret_cdrom += 10;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(common_file[dev_count])!= FILE_OPER_SUCCESS)
		{
		 	u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{	   
		 	hFile = OSAL_IOOpen ( common_file[dev_count],enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{  	
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead + 1);
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret += 2;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,
		                                u32BytesToRead );
					if ( (s32BytesRead ==  (tS32)OSAL_ERROR)||(s32BytesRead !=  (tS32)u32BytesToRead) )
					{
						u32Ret += 3;	
		    		}
					else
					{
						OEDT_HelperPrintf( OSAL_C_TRACELEVEL1,
			 							"\n\t Bytes read : %d",s32BytesRead );	
					}
					if(ps8ReadBuffer)
					{
						OSAL_vMemoryFree( ps8ReadBuffer );
						ps8ReadBuffer = OSAL_NULL;
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			} 
			if( u32FilesystemRemoveCommonFile(common_file[dev_count]) != FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileWrite( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_032
* DESCRIPTION:  Write data to an existing file
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileWrite(void)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tCS8 DataToWrite[FILE_NAME_MAX_LEN + 1] = "Writing data";
	tU32 u32Ret = 0, Old_u32Ret = 0;	
   tU32 u8BytesToWrite = 15; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;	
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if( (hFile = (OSAL_IOCreate (write_file[dev_count], enAccess)))== OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{				
			s32BytesWritten = OSAL_s32IOWrite (hFile, DataToWrite, 
							 u8BytesToWrite );
			if ( (s32BytesWritten == (tS32)OSAL_ERROR)||(s32BytesWritten != (tS32)u8BytesToWrite)  )
			{
				u32Ret += 3;
			}
			if(OSAL_s32IOClose ( hFile )== OSAL_ERROR)
			{
				u32Ret += 20;
			}
			else if( OSAL_s32IORemove ( write_file[dev_count] ) )
			{
				u32Ret += 30;
			}
		}	
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;	
}	


/*****************************************************************************
* FUNCTION:		u32FileSystemGetPosFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_033
* DESCRIPTION:  Get File Position from begining of file
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemGetPosFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tS32 s32PosToSet = 10; 
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 s32FilePos = 0;
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	/* Open the file of .dat format */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/*	OSAL_C_S32_IOCTRL_FIOWHERE -- Gives the file pos 
				from beginning of the file*/
			if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet
		                           ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				/*Get position of file pointer from beginning of common file*/
				if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE, 
						(tS32)&s32FilePos ) == OSAL_ERROR )
				{
					u32Ret_cdrom += 3;
				}
				else
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
							"\n\t Bytes between Current Position and BOF : %d",
							s32FilePos );	
				}
			}
			/* Close the file */
		  	if( OSAL_ERROR ==  OSAL_s32IOClose ( hFile )  )
			{
				u32Ret_cdrom += 8;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; Old_u32Ret += u32Ret; u32Ret = 0;
		}
	}
	/* For other device*/
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count]))
		{
			return COMMON_FILE_ERROR;
		}
		/*Open the commmon file*/
		hFile = OSAL_IOOpen ( write_file[dev_count], enAccess );
		/*Check for success of open*/
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{  	
			/*Seek the file to desired position*/
			if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet
	                           ) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				/*Get position of file pointer from beginning of common file*/
				if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE, 
						(tS32)&s32FilePos ) == OSAL_ERROR )
				{
					u32Ret += 3;
				}
				else
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
		 				"\n\t Bytes between Current Position and BOF : %d",
		 				s32FilePos );	
				}
			}
			/*Close the common file*/	
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		/*Remove the common file*/
		if(u32FilesystemRemoveCommonFile(write_file[dev_count]))
		{
			u32Ret +=COMMON_FILE_ERROR;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemGetPosFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_034
* DESCRIPTION:  Get File Position from end of file
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemGetPosFrmEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tS32 s32FilePos = 0;
	
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;

	/* CDROM */
	/* open a file (.dat) on the CDROM device */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if(OSAL_ERROR == hFile )
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* OSAL_C_S32_IOCTRL_FIONREAD -- Gives the file pos 
			from the end of file */
			if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
									(tS32)&s32FilePos)  )
			{
				u32Ret_cdrom += 2;
			}
			/* Check if the returned value is within the bounds of the file size */
			if((s32FilePos <OEDT_CDROM_ZERO_PARAMETER)||(s32FilePos > OEDT_FILESYSTEM_SIZE_OF_DATA_FILE ))
			{
				u32Ret_cdrom += 4;
			}
			
			if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
			{
				u32Ret_cdrom += 8;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; Old_u32Ret += u32Ret; u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count]))
		{
			u32Ret = (COMMON_FILE_ERROR )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
			continue;
		}
		/*Open the common file*/
		hFile = OSAL_IOOpen ( write_file[dev_count],enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 1;
		}
		else
		{  	
			/*Call the control function*/
			if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
	                              (tS32)&s32FilePos ) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
			else
			{
				OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
		 				"\n\t Bytes between Current Position and EOF : %d"
		 				,s32FilePos );
			}
			/*Close the common file*/
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		/*Remove the common file*/
		if(u32FilesystemRemoveCommonFile(write_file[dev_count]))
		{
			u32Ret += COMMON_FILE_ERROR;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadNegOffsetFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_035
* DESCRIPTION:  Read with a negative offset from BOF
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32   u32FileSystemFileReadNegOffsetFrmBOF()
{
	OSAL_tIODescriptor hFile=0;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tS32 ReadRet =0;
	tS32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;

	/* CDROM  */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 2;
		}
		else
		{
			s32PosToSet = OEDT_CDROM_NEGATIVE_POS_TO_SET;
			/* Seek to the byte position specified */
			if(OSAL_ERROR  != OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
								s32PosToSet) )
			{
			  	/*Dynamically Allocate Memory for the read buffer */
				ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ);
				/* Read the specified number of bytes into an array
				If a negative offset can be set,*/
				ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,OEDT_CDROM_BYTES_TO_READ);
				/* The read number is important. Compare if expected number 
				is read */
				if(OSAL_ERROR != ReadRet)
				{
					u32Ret_cdrom += 4; 
				}
			}
			/* If the seek to a negative offset is not possible, just close
			the file and exit */					
			if(OSAL_ERROR == OSAL_s32IOClose(hFile))
			{
				u32Ret_cdrom += 16;
			}
		}
		/* free the dynamically allocated memory */
		if(ps8ReadBuffer != OSAL_NULL)
		{
			OSAL_vMemoryFree(ps8ReadBuffer);
			ps8ReadBuffer = OSAL_NULL;
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	/* For other device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count]))
		{
			return COMMON_FILE_ERROR;
		}
		hFile = OSAL_IOOpen ( write_file[dev_count], enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret += 2;
		}
		else
		{
			s32PosToSet = OEDT_CDROM_NEGATIVE_POS_TO_SET;
			/* Seek to the byte position specified */
			if(OSAL_ERROR	!= OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
								s32PosToSet) )
			{
				/*Dynamically Allocate Memory for the read buffer */
				ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ);
				ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,OEDT_CDROM_BYTES_TO_READ);
				if(OSAL_ERROR != ReadRet)
				{
					u32Ret += 4; 
				}
			}				
			if(OSAL_ERROR == OSAL_s32IOClose(hFile))
			{
				u32Ret += 16;
			}
		}
		/* free the dynamically allocated memory */
		if(ps8ReadBuffer != OSAL_NULL)
		{
			OSAL_vMemoryFree(ps8ReadBuffer);
			ps8ReadBuffer = OSAL_NULL;
		}
		/*Remove the common file*/ 
		if(u32FilesystemRemoveCommonFile(write_file[dev_count]))
		{
			u32Ret +=COMMON_FILE_ERROR;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadOffsetBeyondEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_036
* DESCRIPTION:  Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 29, 2008 
******************************************************************************/
tU32 u32FileSystemFileReadOffsetBeyondEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tS32 s32PosToSet = 0; 
	tS32 CurFilePos=0;
	tS32 s32FileOff_frm_end = OEDT_CDROM_OFFSET_BEYOND_END;
	tS32 s32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesRead = 0;
	tS32 s32FileSz = 0;
	tU32 u32BytesToRead = 10;
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	
	tU32 device_id = u32OEDT_BoardName() - 1;
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ);
   	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Retreive the number of bytes between current position and EOF */
			if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
												(tS32)&s32FilePos))
			{
				u32Ret_cdrom += 2 ;
			}
			/* Check if the returned value is inside the bounds of the file size */
			if((s32FilePos <OEDT_CDROM_ZERO_PARAMETER)||(s32FilePos > OEDT_FILESYSTEM_SIZE_OF_DATA_FILE) )
			{
				u32Ret_cdrom += 4;
			}
			else
			{
				/* Now retreive the current file position */
				if(OSAL_ERROR == OSAL_s32IOControl (hFile,
						OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
				{
					u32Ret_cdrom += 4 ;
				}
				/* Check if returned value is non negative */
				if(CurFilePos <OEDT_CDROM_ZERO_PARAMETER)
				{
					u32Ret_cdrom += 8;
				}
				else
				{
					/* This will point to the position from where read has to
					begin, i.e. at the specified offset from the end */
					s32PosToSet =  s32FilePos+CurFilePos+s32FileOff_frm_end;
					if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
							OSAL_C_S32_IOCTRL_FIOSEEK,	(tS32)s32PosToSet))
					{
						u32Ret_cdrom += 8;
					}
					else
					{
						s32BytesRead = OSAL_s32IORead( hFile,
										ps8ReadBuffer,OEDT_CDROM_BYTES_TO_READ );
						/* The read number of bytes is important, hence verify */
						if((OSAL_ERROR != s32BytesRead))
						{
							u32Ret_cdrom += 16;
						}
					}
				}
			}
			/* Close the file */
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 32;
			}
		}
			/* free the dynamically allocated memory */
		if(ps8ReadBuffer != OSAL_NULL)
		{
			OSAL_vMemoryFree(ps8ReadBuffer);
			ps8ReadBuffer = OSAL_NULL;
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	/* For other device*/
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file [dev_count])!= FILE_OPER_SUCCESS)
		{
		 	u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{		
			hFile = OSAL_IOOpen ( write_file [dev_count], enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{  	
			   if ( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		                              s32FileSz) != OSAL_ERROR )
				{
					/* Set position at EOF */
					s32PosToSet = OSALUTIL_s32FGetSize(hFile);
					if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		                    	          s32PosToSet) == OSAL_ERROR )
					{
						u32Ret += 2;
					}
					else
					{	
						ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( 
																u32BytesToRead +1);
						if ( ps8ReadBuffer == NULL)
						{
							u32Ret += 3;
						}
						else
						{
							s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
				                                 u32BytesToRead);
							if ( s32BytesRead > 0 )
							{
								u32Ret += 4;	
				    		}
							if(ps8ReadBuffer != OSAL_NULL)
							{
								OSAL_vMemoryFree( ps8ReadBuffer );
								ps8ReadBuffer = OSAL_NULL;
							}
						}
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if( u32FilesystemRemoveCommonFile(write_file [dev_count]) != FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadOffsetFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_037
* DESCRIPTION:  Read from offset from Beginning of file
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemFileReadOffsetFrmBOF(void)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR; 
   tS8 *ps8ReadBuffer = NULL;     
   tU32 u32BytesToRead = 10;
   tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };

	tU32 device_id = u32OEDT_BoardName() - 1;

	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret_cdrom += 1;
		}
		else
		{  	
			/* Set position at an Offset from BOF */
			s32PosToSet = 5;
			if( OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		                              s32PosToSet) == OSAL_ERROR )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1);
				if ( ps8ReadBuffer == NULL)
				{
					u32Ret_cdrom += 3;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
		                                u32BytesToRead);
					if ( (s32BytesRead == (tS32)OSAL_ERROR)||(s32BytesRead != (tS32)u32BytesToRead) )
					{
						u32Ret_cdrom += 4;	
		    		}
					else
					{
						OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
		 								"\n\t Bytes read: %d ",s32BytesRead);
					}
					if(ps8ReadBuffer != OSAL_NULL)
					{
						OSAL_vMemoryFree( ps8ReadBuffer );
						ps8ReadBuffer = OSAL_NULL;
					}
				}
			}
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 10;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;
	/* For other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count])!= FILE_OPER_SUCCESS)
		{
			u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{		
			hFile = OSAL_IOOpen ( write_file[dev_count], enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{		
				/* Set position at an Offset from BOF */
				s32PosToSet = 5;
				if( OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
												s32PosToSet) == OSAL_ERROR )
				{
					u32Ret += 2;
				}
				else
				{
					ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1);
					if ( ps8ReadBuffer == NULL)
					{
						u32Ret += 3;
					}
					else
					{
						s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
													   u32BytesToRead);
						if ( (s32BytesRead == (tS32)OSAL_ERROR)||(s32BytesRead != (tS32)u32BytesToRead) )
						{
							u32Ret += 4; 
						}
						else
						{
							OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
											"\n\t Bytes read: %d ",s32BytesRead);
						}
						if(ps8ReadBuffer != OSAL_NULL)
						{
							OSAL_vMemoryFree( ps8ReadBuffer );
							ps8ReadBuffer = OSAL_NULL;
						}
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if( u32FilesystemRemoveCommonFile(write_file[dev_count]) != 
																FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER); 				  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadOffsetFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_038
* DESCRIPTION:  Read file with few offsets from EOF
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemFileReadOffsetFrmEOF(void)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;		
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR; 
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
	tS32 s32FilePos = 0;
   	tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	tS32 s32offFrmEOF = 10;  /* This is the offset from EOF */
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };

	tU32 device_id = u32OEDT_BoardName() - 1;
	
	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{
		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret_cdrom += 1;
		}
		else
		{  	
			if(OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		                             (tS32)&s32FilePos) == OSAL_ERROR )
			{
				u32Ret_cdrom += 2;
			}
			else
			{
				s32PosToSet = s32FilePos - s32offFrmEOF;	
				if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet) == OSAL_ERROR )
				{
					u32Ret_cdrom += 3;
				}
		   		else
				{
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1);
					if ( ps8ReadBuffer == NULL)
					{
						u32Ret_cdrom += 4;
					}
					else
					{
						s32BytesRead = OSAL_s32IORead(hFile,ps8ReadBuffer,
			                               u32BytesToRead);
						if ( (s32BytesRead == (tS32)OSAL_ERROR)||(s32BytesRead != 
														(tS32)u32BytesToRead) )
						{
							u32Ret_cdrom += 5;	
			    		}
						else
						{
							OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
		 								"\n\t Bytes read: %d ",s32BytesRead);
						}
						if(ps8ReadBuffer != OSAL_NULL)
						{
							OSAL_vMemoryFree( ps8ReadBuffer );
							ps8ReadBuffer = OSAL_NULL;
						}
					}
				}
			}
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 10;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count])!= 
											FILE_OPER_SUCCESS)
		{
		 	u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{
			hFile = OSAL_IOOpen ( write_file[dev_count], enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{  	
				if(OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		                              (tS32)&s32FilePos) == OSAL_ERROR )
				{
					u32Ret += 2;
				}
				else
				{
					s32PosToSet = s32FilePos - s32offFrmEOF;
				
					if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		                              s32PosToSet) == OSAL_ERROR )
					{
						u32Ret += 3;
					}
			   		else
					{
						ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate(
																u32BytesToRead +1);
						if ( ps8ReadBuffer == NULL)
						{
							u32Ret += 4;
						}
						else
						{
							s32BytesRead = OSAL_s32IORead(hFile,ps8ReadBuffer,
				                               u32BytesToRead);
							if ( (s32BytesRead == (tS32)OSAL_ERROR)||(s32BytesRead != 
																(tS32)u32BytesToRead) )
							{
								u32Ret += 5;	
				    		}
							else
							{
								OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
			 								"\n\t Bytes read: %d ",s32BytesRead);
							}
							if(ps8ReadBuffer != OSAL_NULL)
							{
								OSAL_vMemoryFree( ps8ReadBuffer );
								ps8ReadBuffer = OSAL_NULL;
							}
						}
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
	   	if( u32FilesystemRemoveCommonFile(write_file[dev_count]) !=
				 											FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadEveryNthByteFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_039
* DESCRIPTION:  Reads file by skipping certain offsets at specified intervals.
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemFileReadEveryNthByteFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tS32 s32PosToSet = 0;
	tS32 CurFilePos = 0;
	tS32 s32FilePos = 0;
	tS32 FileSize = 0;
	tS32 s32BytesRead = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tS32 s32OffsetFrmStart = 0;
	tU32 u32BytesToRead = 10;/*Number of  bytes to read */
	
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };

	tU32 device_id = u32OEDT_BoardName() - 1;
	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
		{
		/*Dynamically Allocate Memory for the read buffer */
	 	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ);
	 
	   	/* Open the file */
		hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Retreive the number of bytes between current position and EOF */
			if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
												(tS32)&s32FilePos))
			{
				u32Ret_cdrom += 2 ;
			}
			/* Check if the returned value is inside the bounds of the file size */
			if((s32FilePos <OEDT_CDROM_ZERO_PARAMETER)||(s32FilePos >OEDT_FILESYSTEM_SIZE_OF_DATA_FILE))
			{
				u32Ret_cdrom += 4;
			}

			else
			{		
				/* Now retreive the current file position */
				if(OSAL_ERROR == OSAL_s32IOControl (hFile,
							OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
				{
					u32Ret_cdrom += 4 ;
				}
				/* Check if returned value is non negative */
				if(CurFilePos < OEDT_CDROM_ZERO_PARAMETER)
				{
					u32Ret_cdrom += 8;
				}
				else
				{
					/* Get the file size, this will be necessary to ensure that 
					while reading we do not go beyond the file size */
					FileSize = (s32FilePos+CurFilePos)/4;
					/* Start off with an offset of 0 */
					s32PosToSet =  0;
					/* Make sure that the position being set is less than the 
						file size */
					while(s32PosToSet<= FileSize-(FILESYSTEM_SKIP_COUNT+FILESYSTEM_BYTES_TO_READ_N))
					{
						if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
								OSAL_C_S32_IOCTRL_FIOSEEK,	(tS32)s32PosToSet))
						{
							u32Ret += 8;
						}
						else
						{
							/* Now read 5 bytes from that offset */
							s32BytesRead = OSAL_s32IORead( hFile,
											ps8ReadBuffer,FILESYSTEM_BYTES_TO_READ_N);
							/* The number of bytes that are read is important, 
							hence verify the count */
					   	if(s32BytesRead != FILESYSTEM_BYTES_TO_READ_N)
					   	{
					   		u32Ret_cdrom += 16;
								break;
					   	}
							else
							{
								/* Now move over to the new offset */
						  		s32PosToSet += FILESYSTEM_SKIP_COUNT;
							}
						}
					} /* End of while */
				}
			}
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 64;
			}
		}													   
		if(ps8ReadBuffer != OSAL_NULL)
		{
			OSAL_vMemoryFree(ps8ReadBuffer);
			ps8ReadBuffer = OSAL_NULL;
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
					(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count])!= FILE_OPER_SUCCESS)
		{
		 	u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{	
			hFile = OSAL_IOOpen ( write_file[dev_count], enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{  
				s32OffsetFrmStart = 4;   /* This is the value of 'N' */
				while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		                              (tS32)&s32FilePos) != OSAL_ERROR )
				{
					if(s32FilePos <= 0)
						break;

					if( OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
			                              s32OffsetFrmStart) == OSAL_ERROR )
					{
						u32Ret += 2;
						break;
					}
					else
					{
						ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( 
																		u32BytesToRead +1);
						if ( ps8ReadBuffer == NULL)
						{
							u32Ret += 3;
							break;
						}
						else
						{
							s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
				                                 u32BytesToRead);
							if ( s32BytesRead == OSAL_ERROR )
							{
								u32Ret += 4;
								break;	
				    		}
							if(ps8ReadBuffer != OSAL_NULL)
							{
								OSAL_vMemoryFree( ps8ReadBuffer );
								ps8ReadBuffer = NULL;
							}
						}
					}
					s32OffsetFrmStart += s32OffsetFrmStart;
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if( u32FilesystemRemoveCommonFile(write_file[dev_count]) != 
																		FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}	
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);  
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadEveryNthByteFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_040
* DESCRIPTION:  Reads file by skipping certain offsets at specified intervals.
*				(This is from EOF)		
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemFileReadEveryNthByteFrmEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; tU32 Old_u32Ret = 0;	
	tU32 u32Ret_cdrom = CDROM_ERROR;
	tS32 s32PosToSet = 0;
	tS32 CurFilePos=0;
	tS32 s32FilePos=0;
	tS32 s32BytesRead=0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32FileSz = 0;
	tS32 s32OffSet = 5; /* This is the offset from EOF */
	tU32 u32BytesToRead = 10;/*Number of  bytes to read */
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{

		/*Dynamically Allocate Memory for the read buffer */
		ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ);

		/* Open the file */
		hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( OSAL_ERROR == hFile)
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			/* Retreive the number of bytes between current position and EOF */
			if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
												(tS32)&s32FilePos))
			{
				u32Ret_cdrom += 2 ;
			}
			/* Check if the returned value is inside the bounds of the file size */
			if((s32FilePos <OEDT_CDROM_ZERO_PARAMETER)||(s32FilePos > OEDT_FILESYSTEM_SIZE_OF_DATA_FILE) )
			{
				u32Ret_cdrom += 4;
			}
			else
			{
				/* Now retreive the current file position */
				if(OSAL_ERROR == OSAL_s32IOControl (hFile,
						OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
				{
					u32Ret_cdrom += 4 ;
				}
				/* Check if returned value is non negative */
				if(CurFilePos <OEDT_CDROM_ZERO_PARAMETER)
				{
					u32Ret_cdrom += 8;
				}
				else
				{
					s32PosToSet =  (s32FilePos+CurFilePos-FILESYSTEM_SKIP_COUNT)/4;
					while(s32PosToSet>0)
					{
						if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
							OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)s32PosToSet))
						{
							u32Ret_cdrom += 8;
						}
						else
						{
							/* Now read 5 bytes from that offset */
							s32BytesRead = OSAL_s32IORead( hFile,
									ps8ReadBuffer,FILESYSTEM_BYTES_TO_READ_N);
							/* The number of bytes that are read is important, 
								hence verify the count */
					   		if(s32BytesRead != FILESYSTEM_BYTES_TO_READ_N)
					   		{
					   			u32Ret_cdrom += 16;
								break;
					   		}
							else
							{	
								s32PosToSet = s32PosToSet - FILESYSTEM_SKIP_COUNT;
							}
						}
					}/* End of while */
				}
			}
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 64;
			}
		}
		/* free the dynamically allocated memory */
		if(ps8ReadBuffer != OSAL_NULL)
		{
			OSAL_vMemoryFree(ps8ReadBuffer);
			ps8ReadBuffer = OSAL_NULL;
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret; 
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{		
		if(u32FilesystemCreateCommonFile(write_file[dev_count])!= FILE_OPER_SUCCESS)
		{
			u32Ret += FILE_CREATE_FAIL;
		}
		else
		{
			hFile = OSAL_IOOpen ( write_file[dev_count],enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{		
				if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
												(tS32)&u32FileSz)== OSAL_ERROR )
				{
					u32Ret += 2;
				}
				else
				{
						/* Set position at EOF */
					//u32FileSz = OSALUTIL_s32FGetSize(hFile);
					s32PosToSet = (tS32)u32FileSz - s32OffSet;
					while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
							  (tS32)&s32FilePos) != OSAL_ERROR && (s32PosToSet) > 0 )
						
					{
						//OEDT_HelperPrintf(TR_LEVEL_USER_1, "Pos %d :: %d :: %d\n\n", dev_count,s32PosToSet,u32FileSz );
						if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
														s32PosToSet) == OSAL_ERROR )
						{
							u32Ret += 3;
							break;
						}
						else
						{
							ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1);
							if ( ps8ReadBuffer == NULL )
							{
								u32Ret += 4;
								break;
							}
							else
							{
								s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,
													 u32BytesToRead);
								if ( s32BytesRead == (tS32)OSAL_ERROR )
								{
									u32Ret += 5; 
									break;
								}
								if(ps8ReadBuffer != OSAL_NULL)
								{
									OSAL_vMemoryFree( ps8ReadBuffer );
									ps8ReadBuffer = NULL;
								}
							}
						}
						s32PosToSet -= s32OffSet; 
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if( u32FilesystemRemoveCommonFile(write_file[dev_count]) != FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		//OEDT_HelperPrintf(TR_LEVEL_USER_1, "ERR :: %d\n\n", Old_u32Ret );
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}		
	}
	return Old_u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemGetFileCRC( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FILESYSTEM_041
* DESCRIPTION:  Get CRC value
* HISTORY:		Created by Sriranjan (RBEI/ECM1) on Oct 30, 2008 
******************************************************************************/
tU32 u32FileSystemGetFileCRC(void)
{
	OSAL_tIODescriptor hFile = 0; 
   	tU32 u32Ret = 0;
	tU32 Old_u32Ret = 0;
	tU32 u32Ret_cdrom = CDROM_ERROR;
   	tS32 s32FilePosFrmEnd = 0;
	tS32 s32FilePosFrmStart = 0;
   	tS8 as8ReadBuffer [2];
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 dev_count = 0;
	tChar* write_file []= {
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS1_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_FFS2_WRITEFILE,
							 OEDT_RAMDISK_WRITEFILE,
							 OEDT_USB_WRITEFILE,
							 OEDT_SDCARD_WRITEFILE
							 };
	tU32 device_id = u32OEDT_BoardName() - 1;
	
	/* CDROM */
	if ((Dev_Identity[0][device_id] == 1))
	{

		hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST_CDROM, enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret_cdrom += 1;
		}
		else
		{
			if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,
		       				(tS32)&s32FilePosFrmStart ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 2;
			}
			else
		   	{
				if ( OSAL_s32IOControl (hFile, OSAL_C_S32_IOCTRL_FIONREAD,
							(tS32)&s32FilePosFrmEnd ) == OSAL_ERROR )
				{
					u32Ret_cdrom += 3;
				}
				else
				{
					if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
						s32FilePosFrmStart + s32FilePosFrmEnd - 2) == OSAL_ERROR )
					{
			 			u32Ret_cdrom += 4;	
			 		}
			 		else
			 		{
			 			if ( OSAL_s32IORead ( hFile, as8ReadBuffer, 2
			 							) == OSAL_ERROR )
			 			{
							u32Ret_cdrom += 5;
						}
						else
						{
							OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
											"\n\t data read: %s ",as8ReadBuffer);
						}
					}
				}
			}
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret_cdrom += 10;
			}
		}
		if (u32Ret_cdrom!= CDROM_ERROR)
		{
			u32Ret+= u32Ret_cdrom; 
			Old_u32Ret += u32Ret;
			u32Ret = 0;
		}
	}
	enAccess = OSAL_EN_READWRITE;
	
	/* For Other Device */
	for (dev_count = 1; ((dev_count < NO_OF_DEVICE) && 
				(Dev_Identity[dev_count][device_id] == 1)); dev_count++)
	{
		if(u32FilesystemCreateCommonFile(write_file[dev_count])!= FILE_OPER_SUCCESS)
		{
		 	u32Ret += FILE_CREATE_FAIL ;
		}
		else
		{
			hFile = OSAL_IOOpen ( write_file[dev_count],enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 1;
			}
			else
			{
				if( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,
		        				(tS32)&s32FilePosFrmStart ) == OSAL_ERROR )
				{
					u32Ret += 2;
			   }
				else
		   	{
					if ( OSAL_s32IOControl (hFile, OSAL_C_S32_IOCTRL_FIONREAD,
								(tS32)&s32FilePosFrmEnd ) == OSAL_ERROR )
					{
						u32Ret += 3;
					}
					else
					{
						if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
							s32FilePosFrmStart + s32FilePosFrmEnd - 2) == OSAL_ERROR )
						{
							u32Ret += 4;	
						}
						else
						{
							if ( OSAL_s32IORead ( hFile, as8ReadBuffer, 2
											) == OSAL_ERROR )
							{
								u32Ret += 5;
							}
							else
							{
								OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
			 								"\n\t data read: %s ",as8ReadBuffer);
							}
						}
					}
				}
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
			if( u32FilesystemRemoveCommonFile(write_file[dev_count]) != FILE_OPER_SUCCESS )
				u32Ret += FILE_REMOVE_FAIL ;
		}
		if ( u32Ret)
		{
			u32Ret = (u32Ret )+ ((dev_count + 1)*ERROR_MULTIPLIER);
		}
		Old_u32Ret += u32Ret;
		u32Ret = 0;
		if(dev_count == FFS3_POS - 1)
		{
			dev_count++;
		}
	}
	return Old_u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FSCreateCommonFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Create common data file
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
******************************************************************************/
tU32 u32FSCreateCommonFile(tVoid)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;	 
   	tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = 
   									"Writing some test data to the data file";
	tU32 u32Ret = 0;    
	tU8 u8BytesToWrite = 40;
    tS32 s32BytesWritten = 0;
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer =  (tU8 *)OSAL_pvMemoryAllocate ( sizeof(namebuffer)+25 );
	/*Check if memory allocated successfully*/
   	if ( namebuffer == (tU8 *)OSAL_NULL )
	{
	   	u32Ret = 1;
	}

	for( count = 0; count < 6; count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);
			file_name(count,namebuffer,"/common_test.txt");
			if ( ((hFile = OSAL_IOCreate (namebuffer, enAccess)) == OSAL_ERROR))
			{
				u32Ret = 2;
				tFlag = TRUE;
			}
			else
			{							
				/* Write data to file */
				s32BytesWritten = OSAL_s32IOWrite (
				    			hFile, DataToWrite,(tU32) u8BytesToWrite);
				/* Check status of write */
				if ( s32BytesWritten == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}
				/* Close the common file */
				if( OSAL_s32IOClose ( hFile )== OSAL_ERROR )
				{
					u32Ret += 10;
					tFlag = TRUE;
				}
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1] ;
				tFlag = FALSE;
			}
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32FSRemoveCommonFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Remove common data file
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 23 Oct, 2008
******************************************************************************/
tU32 u32FSRemoveCommonFile(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for( count = 0; count < 6; count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);
			file_name(count,namebuffer,"/common_test.txt");
			/*Remove the common File*/
			if(OSAL_s32IORemove(namebuffer) == OSAL_ERROR)
			{
				u32Ret = 1;
				u32Ret += Dev_Error[count+1];
			}		  				
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		AsyncRead()
* PARAMETER:    OSAL_tIODescriptor, Bytes To Read
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Async read
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
******************************************************************************/
tU32 AsyncRead(OSAL_tIODescriptor hFile, tU32 u32BytesToRead)
{
    OSAL_trAsyncControl rAsyncCtrl = {0};   	
    tS8 *ps8ReadBuffer = NULL;
    tS32 s32BytesRead = 0;
	tChar szSemName []   = "SemAsyncFS";
   	tS32 s32RetVal = 0;
	tU32 u32Ret = 0; 

	/*Allocate memory*/
	ps8ReadBuffer =  (tS8 *)OSAL_pvMemoryAllocate ( u32BytesToRead+1 );
	/*Check if memory allocated successfully*/
   	if ( ps8ReadBuffer == NULL )
	{
	   	u32Ret = 1;
	}
	else
	{

   		/*
      		Create semaphore for event signalling used by async callback.
   		*/
		s32RetVal = OSAL_s32SemaphoreCreate (
                                          szSemName,
                                          &hSemAsyncFS,
                                          0
                                       );

		/*Check if semaphore is already existent*/
	   	if(s32RetVal == OSAL_ERROR)
	    {
			u32Ret += 2;      
	    }
		else
		{
			/*Fill the asynchronous control structure*/
			rAsyncCtrl.id = hFile;
		    rAsyncCtrl.s32Offset = 0;
		    rAsyncCtrl.pvBuffer = ps8ReadBuffer;
		    rAsyncCtrl.u32Length = u32BytesToRead;
		    rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTFSAsyncCallback;
		    rAsyncCtrl.pvArg = &rAsyncCtrl;

		   /* 
		      If IOReadAsync returns OSAL_ERROR, Callback function is 
		      not called so semaphore is not waited upon.
		   */
			if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
		    {
		    	u32Ret += 4;     
			}
			else
			{
			      /*
			         Wait for 5 seconds for asynchronous read to get 
			         over. If semaphore not posted by the callback within 
			         5 seconds,then cancel the asynchronous read.
			      */
				s32RetVal = OSAL_s32SemaphoreWait (
                                       hSemAsyncFS,
                                       5000
                                      );

  				if (
        				( s32RetVal != OSAL_OK )
        				&&
        				( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
       				)
  				{
     				s32RetVal = OSAL_s32IOCancelAsync (
                                          rAsyncCtrl.id,
                                          &rAsyncCtrl
                                      	 );
					u32Ret +=  10;
    			
    			}
  				else
  				{

     				s32RetVal = (tS32)OSAL_u32IOErrorAsync ( &rAsyncCtrl );
     				
     				if (!(s32RetVal == (tS32)OSAL_E_INPROGRESS||
           				s32RetVal == (tS32) OSAL_E_CANCELED))
					{
						s32BytesRead = (tS32) OSAL_s32IOReturnAsync(&rAsyncCtrl);
						
						if( s32BytesRead != (tS32)u32BytesToRead)
        				{
        					u32Ret += 20;
        				}     	
					}
     	    	}
	    	}
		}		    	      
		/* Free the memory allocated */
		if(ps8ReadBuffer != NULL)
		{
			OSAL_vMemoryFree (ps8ReadBuffer);
			ps8ReadBuffer = NULL;
		}

	}
	if( OSAL_ERROR == OSAL_s32SemaphoreClose ( hSemAsyncFS ))
	{
		u32Ret += 40;
	}

	if(OSAL_ERROR == OSAL_s32SemaphoreDelete( szSemName ) )
	{
		u32Ret += 100;
	}

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemReadAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_042
* DESCRIPTION:  Read data asyncronously from a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadAsync(tVoid)
{	
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
    tU32 u32BytesToRead = 10;
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 20] = CDROM_TEXT_FILE;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	if(	Dev_Identity[0][Device_ID])
	{
		/*Open the common file*/
	   	hFile = OSAL_IOOpen ( szFilePath,OSAL_EN_READONLY );
		/*Check if open successful*/
		if( hFile == OSAL_ERROR )
		{
			u32Ret = 1;
			tFlag = TRUE;
		}
		else
		{
			if(AsyncRead(hFile, u32BytesToRead))
			{
				u32Ret += 2;
				tFlag = TRUE;
			}

			/*Close the common file*/
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 4;
				tFlag = TRUE;
			}	
		}
		if(tFlag)
		{
			u32Ret += Dev_Error[0];	
			tFlag = FALSE;
		}
	} /*end of if */
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(AsyncRead(hFile, u32BytesToRead))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}

				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}	
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemLargeReadAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_043
* DESCRIPTION:  Read data asyncronously from a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemLargeReadAsync(tVoid)
{	
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
    tU32 u32BytesToRead = 1000;
	tU32 u32LoopCount = 0;
	tCS8 sWriteData[ ] = "Datatowrite";
	tU32 u32WriteDataLen = strlen((tCString)sWriteData);
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				/* Writing data into the Largefile.txt*/	
				for(u32LoopCount = 0; u32LoopCount < u32BytesToRead; u32LoopCount++)
				{
					OSAL_s32IOWrite(hFile, sWriteData, u32WriteDataLen);
				}
				/*End of write data*/
			
				if(AsyncRead(hFile, u32BytesToRead))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}

				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}			 		
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		AsyncWrite()
* PARAMETER:    OSAL_tIODescriptor, Bytes To Write
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Async write
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
******************************************************************************/
tU32 AsyncWrite(OSAL_tIODescriptor hFile, tU32 u32BytesToWrite)
{
    OSAL_trAsyncControl rAsyncCtrl = {0};   	
   	tS32 s32BytesWritten = 0;
	tS8 *ps8WriteBuffer = NULL;
	tChar szSemName []   = "SemAsyncFS";
   	tS32 s32RetVal       = 0;
	tU32 u32Ret = 0; 

	/*Allocate memory*/
	ps8WriteBuffer =  (tS8 *)OSAL_pvMemoryAllocate ( u32BytesToWrite+1 );
	/*Check if memory allocated successfully*/
   	if ( ps8WriteBuffer == NULL )
	{
	   	u32Ret = 1;
	}
	else
	{
		 OSAL_pvMemorySet(ps8WriteBuffer,'a',u32BytesToWrite);
   		/*
	      	Create semaphore for event signalling used by async callback.
	   	*/
	   	s32RetVal = OSAL_s32SemaphoreCreate (
	                                          szSemName,
	                                          &hSemAsyncFS,
	                                          0
	                                       );
		/*Check if semaphore already existing in the system*/
	   	if(s32RetVal == OSAL_ERROR)
	   	{
	    	u32Ret += 2;
	   	}
		else
		{
	
			/*Fill the asynchronous control structure*/
			rAsyncCtrl.s32Offset = 0;
			rAsyncCtrl.id = hFile;
			rAsyncCtrl.pvBuffer = ps8WriteBuffer;
			rAsyncCtrl.u32Length = u32BytesToWrite;
			rAsyncCtrl.pCallBack = (OSAL_tpfCallback) vOEDTFSAsyncWrCallback;
			rAsyncCtrl.pvArg = &rAsyncCtrl;

			/* 
				If IOWriteAsync returns OSAL_ERROR, Callback function 
				is not called so semaphore is not waited upon.
			*/
			if ( OSAL_s32IOWriteAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			{
			   u32Ret += 4;
			}
			else
			{
	         	/*
		            Wait for 5 seconds for asynchronous write to get over.
		            If semaphore not posted by the callback within 5 seconds,
		            then cancel the asynchronous write.
		         */
		        s32RetVal = OSAL_s32SemaphoreWait (
		                                              hSemAsyncFS,
		                                              5000
		                                           );

	         	if (
	               	( s32RetVal != OSAL_OK )
	               	&&
	               	(OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
	            	)
	         	{
	            	s32RetVal = OSAL_s32IOCancelAsync (
	                                                 	rAsyncCtrl.id,
	                                                 	&rAsyncCtrl
	                                              	);
	                u32Ret += 10;
	         	}
	         	else
	         	{

	            	s32RetVal = (tS32)OSAL_u32IOErrorAsync ( &rAsyncCtrl );
	            	if (!(s32RetVal == (tS32)OSAL_E_INPROGRESS||
	                  	s32RetVal == (tS32)OSAL_E_CANCELED))
	            	{
	                	s32BytesWritten = (tS32)OSAL_s32IOReturnAsync (&rAsyncCtrl);
						if(	s32BytesWritten != (tS32)u32BytesToWrite)
						{
						      u32Ret += 20;
						}
	            	}
				}
			}
		}		    	      
		/* Free the memory allocated */
		if(ps8WriteBuffer != NULL)
		{
			OSAL_vMemoryFree (ps8WriteBuffer);
			ps8WriteBuffer = NULL;
		}
	}
	if(OSAL_ERROR == OSAL_s32SemaphoreClose ( hSemAsyncFS ))
	{
		u32Ret += 40;	
	}
   	if(OSAL_ERROR == OSAL_s32SemaphoreDelete( szSemName ))
	{
		u32Ret += 100;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_044
* DESCRIPTION:  Write data asyncronously to a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteAsync(tVoid)
{	
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
   	tU32 u32BytesToWrite = 10;
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(AsyncWrite(hFile, u32BytesToWrite))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}
				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}			 		
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}
	
/*****************************************************************************
* FUNCTION:		u32FileSystemLargeWriteAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_045
* DESCRIPTION:  Write data asyncronously to a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemLargeWriteAsync(tVoid)
{	
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
   	tU32 u32BytesToWrite = 1048576;
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(AsyncWrite(hFile, u32BytesToWrite))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}
				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}			 		
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		WritetoOddBuf()
* PARAMETER:    OSAL_tIODescriptor
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write data to an odd buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 WritetoOddBuf(OSAL_tIODescriptor hFd)
{
    tU32 u32Ret = 0;
    tString pBuffer= OSAL_NULL;
    tString pBuffer2= OSAL_NULL;
    size_t sizeBuf = 16;
	
	/* Memory Allocation */
    pBuffer = (tString)OSAL_pvMemoryAllocate(sizeBuf);

	 if ( OSAL_NULL != pBuffer)
	 {
			OSAL_pvMemorySet(pBuffer, '\0',
								(sizeBuf));


			OSAL_pvMemorySet(pBuffer, 'a',
								(sizeBuf-1));

	 	


		/* Write to file */  
	  	if(OSAL_s32IOWrite(hFd, (tPCS8)pBuffer, (sizeBuf - 1) ) != (tS32)(sizeBuf - 1))
	  	{
	    	u32Ret = 2;
	  	}
	  	else
	  	{
		    if(OSAL_s32IOControl(hFd, OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)0) == OSAL_ERROR)
		    {
		    	u32Ret = 3;
		    }
		    else
		    {
		    	/* Memory Allocation */
		      	pBuffer2 = (tString)OSAL_pvMemoryAllocate(sizeBuf - 1);
				 if ( OSAL_NULL != pBuffer2)
					{
							OSAL_pvMemorySet(pBuffer2, '\0',
												(sizeBuf));
						
						 
					/* Read specified number of bytes */
			      	if(OSAL_s32IORead(hFd, (tPS8)pBuffer2, sizeBuf - 1) != (tS32)(sizeBuf - 1))
			      	{
			        	u32Ret = 4;
			      	}
			      	else
			      	{
			        	if(OSAL_s32StringNCompare((tCString)pBuffer,(tCString) pBuffer2, (sizeBuf - 1)) != 0)
			        	{
			          		u32Ret = 5;
			        	}
			      	}
						/* Free memory */
					  	if(pBuffer2 != NULL)
					  	{
					    	free((tString)pBuffer2);
							pBuffer2 = NULL;
					  	}

					}
					else
					{
					 	u32Ret += 20;
					}
		    }
	  	}
		/* Free memory */
	  	if(pBuffer != NULL)
	  	{
	    	free((tString)pBuffer);
			pBuffer = NULL;
	  	}
	}
	else
	{
	 	u32Ret += 50;
	}
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteOddBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_046
* DESCRIPTION:  Write data to an odd buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteOddBuffer(tVoid)
{
    OSAL_tIODescriptor hFile=0;
    tU32 u32Ret = 0;
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(WritetoOddBuf(hFile))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}

				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}			 		
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileMultipleHandles()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_047
* DESCRIPTION:  Open file multiple times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileMultipleHandles(tVoid)
{
	tU32 u32Ret = 0;
    OSAL_tIODescriptor hFd1=0;
    OSAL_tIODescriptor hFd2=0;
	tBool tFlag = FALSE;
	tU32 count = 0;
	tU32 Device_ID = 0;
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	 
    /*CDROM*/
    if(	Dev_Identity[0][Device_ID])
	{
		hFd1 = OSAL_IOOpen(CDROM_TEXT_FILE, OSAL_EN_READONLY);
	    if(hFd1 != OSAL_ERROR) 
	    {
			hFd2 = OSAL_IOOpen(CDROM_TEXT_FILE, OSAL_EN_READONLY);
		    if(hFd2 != OSAL_ERROR) 
			{
				OSAL_s32IOClose(hFd2);
			}
			else
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			OSAL_s32IOClose(hFd1);
	    }
		else
		{
			u32Ret = 2;
			tFlag = TRUE;
		}
		if(tFlag)
		{
			u32Ret += Dev_Error[0];
			tFlag = FALSE;
		}
	}
	
	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
    {
		u32Ret += 8000;
	}

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
	
	    	hFd1 = OSAL_IOOpen( namebuffer, 
    				OSAL_EN_READONLY);
	    	if(hFd1 != OSAL_ERROR) 
	    	{
				hFd2 = OSAL_IOOpen(namebuffer, 
    				OSAL_EN_READONLY);
		    	if(hFd2 != OSAL_ERROR) 
				{
					OSAL_s32IOClose(hFd2);
				}
				else
				{
					u32Ret = 1;
					tFlag = TRUE;
				}
				OSAL_s32IOClose(hFd1);
			}
	    	else
			{
				u32Ret = 2;
				tFlag = TRUE;
			}
		}	
		if(tFlag)
		{
			u32Ret += Dev_Error[1];
			tFlag = FALSE;
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	  	{
		u32Ret += 9000;
	}
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		WriteToBuffer()
* PARAMETER:    OSAL_tIODescriptor
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Write data to a buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 WriteToBuffer(OSAL_tIODescriptor hFile)
{
    tU32  u32Ret = 0;
   	tS32 s32RetVal = 0;
  	tU32  iPos   = 0;
  	tCS8 DatatoWrite[]= "abcdefg";
	
	s32RetVal = OSAL_s32IOWrite(hFile, DatatoWrite, ((tU32)(-1)));
	if(s32RetVal != (tS32)OSAL_ERROR)
	{
  		u32Ret = 1;
	}
	else
	{
  		OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOWHERE, (tS32)&iPos);
  		if(iPos != 0)
  		{
    		u32Ret = 2;
  		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteFileWithInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_048
* DESCRIPTION:  Write data to a file with invalid size
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteFileWithInvalidSize(tVoid)
{
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(WriteToBuffer(hFile))
				{
					u32Ret += 2;
					tFlag = TRUE;
				}
				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}	
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		WriteToInvalBuffer()
* PARAMETER:    OSAL_tIODescriptor
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write data to a invalid buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 WriteToInvalBuffer(OSAL_tIODescriptor hFile)
{
    tU32  u32Ret = 0;
   	tS32 s32RetVal = 0;
  	tU32  u32BytesToWrite = 3;
  	tS8* pBuffer = OSAL_NULL;

	s32RetVal = OSAL_s32IOWrite(hFile, pBuffer, u32BytesToWrite);
	if(s32RetVal != OSAL_ERROR)
	{
  		u32Ret = 1;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteFileInvalidBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_049
* DESCRIPTION:  Write data to a file with invalid buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteFileInvalidBuffer(tVoid)
{
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 1;
				tFlag = TRUE;
			}
			else
			{
				if(WriteToInvalBuffer(hFile))
					u32Ret += 2;
				/*Close the common file*/
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 4;
					tFlag = TRUE;
				}	
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32FileSystemWriteFileStepByStep()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_050
* DESCRIPTION:  Write data to a file stepwise
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteFileStepByStep(tVoid)
{
	OSAL_tIODescriptor hFile= 0;
    tU32 u32Ret = 0;               
  	tS32 iCnt=0;
  	tU32 iStepWidth = 262144; /* 256 kB */ 
  	tPS8 pBuffer = (tPS8) OSAL_NULL;
  	tU32 iFileIdx=0;
  	tU32 iFullSize = 1048576; /* 1MB */ 
	tBool tFlag = FALSE;
	tU32 count = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
  	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	/* Allocate memory */
  	pBuffer = (tPS8)OSAL_pvMemoryAllocate(iStepWidth);
	if ( pBuffer == NULL)
   	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
   	}
	else
	{
		OSAL_pvMemorySet(pBuffer, '\0',	iStepWidth);
		OSAL_pvMemorySet(pBuffer, 'a', iStepWidth);

		for(count=0;count<6;count++)
		{
			if(Dev_Identity[count+1][Device_ID]) 
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,"/common_test.txt");

				iFileIdx = 0;
				while((iFileIdx < iFullSize) && (u32Ret == 0))
				{
			  		/* Open file */
					  		hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE);
					if(hFile == OSAL_ERROR)
					{
				  		u32Ret = 1;
						tFlag = TRUE;
					}
					else
					{
				  		if(OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOSEEK, 
				  					(tS32)iFileIdx) == OSAL_ERROR)
			  			{
			      			u32Ret += 2;
							tFlag = TRUE;
							/* Close file */
			  		  		if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
			  		  		{
			    		  		u32Ret += 4;
			  		  		}
							break;
				  		}
				  		else
				  		{
					  		/* Write to file */
					  		iCnt = OSAL_s32IOWrite(hFile, pBuffer, iStepWidth);
					  		if(iCnt != (tS32)iStepWidth)
					  		{
				  		  		u32Ret += 10;
								tFlag = TRUE;
									/* Close file */
				  		  		if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
				  		  		{
				    		  		u32Ret += 20;
				  		  		}
								break;
					  		}
				      		else
					  		{
								/* Close file */
				  		  		if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
				  		  		{
				    		  		u32Ret += 40;
									tFlag = TRUE;
				  		  		}					
					  		}
				  		}
					}
					iFileIdx += iStepWidth;
				}
				if(tFlag)
				{
							u32Ret += Dev_Error[1];
					tFlag = FALSE;
				}
			}
		}
		/* Free Memory */
		if(OSAL_NULL != pBuffer)
		{
			free((tS8*)pBuffer);
		  	pBuffer = NULL;
			
	  	}
	}
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemGetFreeSpace()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_051
* DESCRIPTION:  Get free space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemGetFreeSpace (tVoid)
{
  	OSAL_tIODescriptor hFile=0;
  	OSAL_trIOCtrlDeviceSize rSize = {0};
  	tU32 u32Ret = 0;
  	tS32 s32Result = 0;
	tBool tFlag = FALSE;
	tU32 count = 0;
	tU32 Device_ID = 0;

  	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
  	else
  	{
		for(count=0;count<6;count++)
	    {
			if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,OSAL_NULL);
				/* Open device */
			  	hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE);
			  	if(hFile == OSAL_ERROR)
			  	{
				   	u32Ret = 1;
					tFlag = TRUE;
			  	}
			  	else
			  	{
				    rSize.u32High = 0;
				    rSize.u32Low  = 0;
					/* Get free space */
				    s32Result = OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE, (tS32)&rSize);
				    if(((rSize.u32Low == 0) && (rSize.u32High == 0)) || (s32Result == OSAL_ERROR))
				    {
				      u32Ret += 2;
					  tFlag = TRUE;
				    }
					/* Close device */
				    if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
				    {
				      u32Ret += 4;
					  tFlag = TRUE;
				    }
			  	}
				if(tFlag)
				{
								u32Ret += Dev_Error[0];
					tFlag = FALSE;
				}
			}
  		}
		if(namebuffer != NULL)
		{
			OSAL_vMemoryFree (namebuffer);
			namebuffer = NULL;
		}  
	}

  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemGetTotalSpace()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_052
* DESCRIPTION:  Get total space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemGetTotalSpace (tVoid)
{
  	OSAL_tIODescriptor hFile = 0;
  	OSAL_trIOCtrlDeviceSize rSize;
  	tU32 u32Ret = 0;
  	tS32 s32Ret = 0;
	tBool tFlag = FALSE;
	tU32 count = 0;
	tU32 Device_ID = 0;
	  
  	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
  	else
  	{
		for(count=0;count<6;count++)
		{
			if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,OSAL_NULL);
				/* Open device */
				hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE);
			  	if(hFile == OSAL_ERROR)
			  	{
					u32Ret = 1;
					tFlag = TRUE;
			  	}
			  	else
			  	{
				    rSize.u32High = 0;
				    rSize.u32Low  = 0;
					/* Get total space */
				    s32Ret = OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOTOTALSIZE, 
				    					(tS32)&rSize);
				    if(((rSize.u32Low == 0) && (rSize.u32High == 0)) || 
				    					(s32Ret == OSAL_ERROR))
					{
				      u32Ret += 2;
					  tFlag = TRUE;
				    }
					/* Close device */
				    if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
				    {
				      u32Ret += 4;
					  tFlag = TRUE;
				    }
			  	}
				if(tFlag)
				{
							u32Ret += Dev_Error[0];
					tFlag = FALSE;
				}
			}
  		}
		if(namebuffer != NULL)
		{
			OSAL_vMemoryFree (namebuffer);
			namebuffer = NULL;
		}
	}

  	return u32Ret;
}
	
/*****************************************************************************
* FUNCTION:		u32FileSystemFileOpenCloseNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_053
* DESCRIPTION:  Open a non existing file 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileOpenCloseNonExstng(tVoid)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_CDROM"/Dummy.txt";
	tU32 count = 0;
	tBool tFlag = FALSE;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		/* Open a non-existing file */
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile != OSAL_ERROR )
		{
			/* If successful, indicate error and close file */		
			u32Ret = 1;   
			tFlag = TRUE;
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 2;
			}
		}
		if(tFlag)
		{
			u32Ret += Dev_Error[0];
			tFlag = FALSE;
		}
	}
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/dummy.txt");
			/*Open the common file*/
		   	hFile = OSAL_IOOpen ( namebuffer,OSAL_EN_READWRITE );
			/*Check if open successful*/
			if( hFile != OSAL_ERROR )
			{
				/* If successful, indicate error and close file */		
				u32Ret = 1;
				tFlag = TRUE;   
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 2;
				}
			}
			if(tFlag)
			{
				u32Ret += Dev_Error[count+1];
				tFlag = FALSE;
			}
		}
	}
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

	
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		DelFileWithoutClse()
* PARAMETER:    File path
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Delete file 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 DelFileWithoutClse(const tU8 * strDevName)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0;
	tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Writing data";
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	tU32 u32Ret = 0;
	tBool tFlgSet = FALSE;

	hFile = OSAL_IOOpen ( strDevName,enAccess );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Write File */	
		s32BytesWritten = OSAL_s32IOWrite (
	              hFile, DataToWrite,(tU32) u8BytesToWrite);
		if ( s32BytesWritten == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		
		/* Remove file without closing */
		if( OSAL_s32IORemove ( strDevName ) != OSAL_ERROR )
		{
			u32Ret += 4;
			tFlgSet = TRUE;
		}
		else
		{
			/* If successful, indicate error and close file */
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
	}

	/* Remove file */
	if(!tFlgSet)
	{
		if( OSAL_s32IORemove ( strDevName ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileDelWithoutClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_054
* DESCRIPTION:  Delete a file without closing 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileDelWithoutClose(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = DelFileWithoutClse(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		SetFilePos()
* PARAMETER:    File path
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Set file position
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 SetFilePos(const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
   	tS32 s32PosToSet = 0;

	/* Open the common file */
	hFile = OSAL_IOOpen ( strDevName,enAccess );
	/* Check if open successful */
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/* With +ve Offset */	
		s32PosToSet = 10;
		/* Seek to positive offset */
		if( OSAL_s32IOControl (hFile, 
			OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		
		/* Close file */		
	    if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
	    {
			u32Ret += 4;
	    }
	}
	
	/*Open the common file*/
	hFile = OSAL_IOOpen ( strDevName,enAccess );
	/*Check if open is successful*/
	if ( hFile != OSAL_ERROR )
	{
	
		/* With -ve Offset from BOF */
		s32PosToSet = -7;
		/*Seek through the common file with neg offset from file beginning*/
		if( OSAL_s32IOControl (
	                              hFile,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet
	                           ) != OSAL_ERROR )
		{
			u32Ret += 10;
		}
		
  		if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
		{
			u32Ret += 20;
		}
	}
	else
	{
		u32Ret += 40;
	}

	/*Open the common file*/
	hFile = OSAL_IOOpen ( strDevName,enAccess );
	/*Check if open is successful*/
	if( hFile != OSAL_ERROR )
	{
		/* Set position at EOF */
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&s32PosToSet
                           ) != OSAL_ERROR )
		{
			/*Seek within the common file*/
			if( OSAL_s32IOControl (
	                              hFile,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet
	                           ) == OSAL_ERROR )
			{
				u32Ret += 100;
			}
			else
			{

				/* Try Setting position beyond EOF */
				s32PosToSet = 10;
				if( OSAL_s32IOControl (
	             	                 hFile,
	                	              OSAL_C_S32_IOCTRL_FIOSEEK,
	                    	          s32PosToSet
	                        	   ) == OSAL_ERROR )
				{
					u32Ret += 200;
				}
			}
		}
		
	  	if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}
	else
	{
		u32Ret += 1000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemSetFilePosDiffOff()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_055
* DESCRIPTION:  Set file position to different offsets
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemSetFilePosDiffOff(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
	   	
	/* Create the common file */
	if(u32FSCreateCommonFile())
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = SetFilePos(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	/*Remove the common file*/	
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
		
  	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		CreateFileMulti()
* PARAMETER:    file name to be created
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  create file multiple times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 CreateFileMulti(const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile= 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	if ( ((hFile = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR))
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32FileSystemCreateFileMultiTimes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_056
* DESCRIPTION:  create file multiple times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemCreateFileMultiTimes(void)
{
	tU32 u32Ret = 0;
 	tU32 u32count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}	
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
		
	for(u32count =0;u32count <5;u32count++)
	{
		if((Dev_Identity[u32count+1][Device_ID]) && u32count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(u32count,namebuffer,"/common_test.txt");
			u32Val = CreateFileMulti(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[u32count+1];
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		CreateFileUniCode()
* PARAMETER:    unicode file name to be created
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  create file with unicode characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 CreateFileUniCode(const tU8* strDevName)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Creation of file with chinese characters */
	hFile = OSAL_IOCreate (strDevName, enAccess);
	if ( OSAL_ERROR == hFile )
	{
		u32Ret = 1;
	}
	else
	{
		/* Close file */
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
		{
			u32Ret = 2;
		}

		/* Remove file */
		if( OSAL_ERROR == OSAL_s32IORemove ( strDevName ) )
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateUnicodeName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_057
* DESCRIPTION:  create file with unicode characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileCreateUnicodeName(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/��汪�.txt");
			u32Val = CreateFileUniCode(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FSFileCreateInvalName()
* PARAMETER:    invalid file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  create file with invalid characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 CreateFileInvalChar(const tU8* strDevName)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
							
	/* Create file with invalid access name */
	hFile = OSAL_IOCreate (strDevName, enAccess);
	if ( hFile != OSAL_ERROR )
	{
		/* If successful, indicate error and close file */ 
		u32Ret = 1;
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
		
		/* Remove file */
		if( OSAL_s32IORemove ( strDevName ) == OSAL_ERROR )
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateInvalName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_058
* DESCRIPTION:  create file with invalid characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileCreateInvalName(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/*/</>>.txt");
			u32Val = CreateFileInvalChar(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		CreateFileLongName()
* PARAMETER:    long file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  create file with long name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 CreateFileLongName(const tU8 * strDevName)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tChar file_long_name[255] = {0};
	tChar file_long_name_ex[256] = {0};
	tChar file_long_name_ex_[257] = {0};
	
	/* Initialize the allocated memory */
	OSAL_pvMemorySet(file_long_name,'\0',FILE_NAME_MAX_LEN);

	/* Copy USB path */
	(tVoid)OSAL_szStringCopy( (tString)file_long_name, (tString)strDevName);

	/* Add '/' character */
	(tVoid)OSAL_szStringConcat(file_long_name,"/");

	/* Call filename generator */
	Generate_File_Name_fs(file_long_name,OSAL_u32StringLength
							(strDevName)+1,(FILE_NAME_MAX_LEN - OSAL_u32StringLength(file_long_name)));

	/* Create the file */
	if ( (hFile = OSAL_IOCreate (file_long_name, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*Close the file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		/*Remove the file*/
		if( OSAL_s32IORemove ( file_long_name ) == OSAL_ERROR )
		{
			u32Ret += 4;
		}
	}

	/* Initialize the allocated memory */
	OSAL_pvMemorySet(file_long_name_ex,'\0',FILE_NAME_MAX_LEN_EXCEEDED);

	/* Copy USB path */
	(tVoid)OSAL_szStringCopy(file_long_name_ex,strDevName);

	/* Add '/' character */
	(tVoid)OSAL_szStringConcat(file_long_name_ex,"/");

	/* Call filename generator */
	Generate_File_Name_fs(file_long_name_ex,OSAL_u32StringLength
			(strDevName)+1,(FILE_NAME_MAX_LEN_EXCEEDED - OSAL_u32StringLength(file_long_name_ex)));

	/* Create the file */
	if ( (hFile = OSAL_IOCreate (file_long_name_ex, enAccess)) != OSAL_ERROR )
	{
		u32Ret +=10;
	   /* Close the file */
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
		/* Remove the file */
		if( OSAL_s32IORemove ( file_long_name_ex ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}

	/* Initialize the allocated memory */
	OSAL_pvMemorySet(file_long_name_ex_,'\0',FILE_NAME_MAX_LEN_EXCEEDED+1);

	/* Copy USB path */
	(tVoid)OSAL_szStringCopy(file_long_name_ex_,strDevName);

	/* Add '/' character */
	(tVoid)OSAL_szStringConcat(file_long_name_ex_,"/");

	/* Call filename generator */
	Generate_File_Name_fs(file_long_name_ex_,OSAL_u32StringLength
				(strDevName)+1,FILE_NAME_MAX_LEN_EXCEEDED+1);

	/* Create the file */
	if ( (hFile = OSAL_IOCreate (file_long_name_ex_, enAccess)) != OSAL_ERROR )
	{
		u32Ret += 100;
		/* If successful, indicate error and Close the file */
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
		/* Remove the file  */
		if( OSAL_s32IORemove ( file_long_name_ex_ ) == OSAL_ERROR )
		{
			u32Ret += 400;
		}
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateLongName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_059
* DESCRIPTION:  create file with long file name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileCreateLongName(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
		return u32Ret;
  	}
	else
	{
        for(count=0;count<6;count++)
	    {
			if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,OSAL_NULL);
				u32Val = CreateFileLongName(namebuffer);
				if(u32Val)	
					u32Ret = u32Val + Dev_Error[count+1];
			}
		}
		if(namebuffer != NULL)
		{
			OSAL_vMemoryFree (namebuffer);
			namebuffer = NULL;
		}
		if(namebuffer != NULL)
		{
			OSAL_vMemoryFree (namebuffer);
			namebuffer = NULL;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		CreateFileDiffModes()
* PARAMETER:    file name to be created
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Create file with different access parameters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 CreateFileDiffModes(const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile= 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0;

	if ( ((hFile = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR))
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		/*Remove the common File*/
		else if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		{
			u32Ret = 3;
		}	
	}

	enAccess = OSAL_EN_WRITEONLY;
	if ( ((hFile = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR))
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 14;
		}
		/*Remove the common File*/
		else if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		{
			u32Ret += 19 ;
		}	
	}

	enAccess = OSAL_EN_READWRITE;
	if ( ((hFile = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR))
	{
		u32Ret += 20;
	}
	else
	{
	  	if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 24;
		}
		/*Remove the common File*/
		else if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		{
			u32Ret += 29;
		}	
	}

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_060
* DESCRIPTION:  Create file with different access modes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileCreateDiffModes(tVoid)
{
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer =  (tU8 *)OSAL_pvMemoryAllocate ( sizeof(namebuffer)+25 );
	/*Check if memory allocated successfully*/
	if ( namebuffer == (tU8 *)OSAL_NULL )
	{
	   	u32Ret = 1;
	}
	else
	{
       for(count=0;count<6;count++)
       {
			if((Dev_Identity[count+1][Device_ID]) && count != 2) 
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,"/common_test.txt");
				u32Val = CreateFileDiffModes(namebuffer);
				if(u32Val)	
					u32Ret = u32Val + Dev_Error[count+1];
			}
	   }
	   if(namebuffer != NULL)
	   {
			OSAL_vMemoryFree (namebuffer);
			namebuffer = NULL;
	   }
	}

	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		CreateFileInvalPath()
* PARAMETER:    invalid filepath
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Create file with invalid path
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 CreateFileInvalPath(const tU8 * strDevName)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	hFile = OSAL_IOCreate (strDevName, enAccess);
	if ( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
		 if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		if( OSAL_s32IORemove ( strDevName ) == OSAL_ERROR )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileCreateInvalPath()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_061
* DESCRIPTION:  Create file with invalid path
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileCreateInvalPath(tVoid)
{
    tU32 u32Ret = 0;               
	tU32 u32Val = 0;
	tU32 count = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/dummydir/file.txt");
			u32Val = CreateFileInvalPath(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		StrSearch()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  String search
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 StrSearch(const tU8 * strDevName)
{
	/*Declarations*/
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 StrngCnt = 0;
	tU32 u32Ret = 0;   	
	tChar szSearchString [5] = "dat";
	tU32 u32StringLen = 3;
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 3; /* this must be equal to length of the search string */
   	tS32 s32BytesRead = 0;
	tU32 u32PosToSet = 0;
	tU32 u32FilePos = 0;
	tU32 s32FileSz = 0;

	/*Open the common file*/		
	hFile = OSAL_IOOpen ( strDevName,enAccess );
	/*Check status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*End of file*/
		if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
      	(tS32)&s32FileSz)== OSAL_ERROR )
		{
			u32Ret = 2;
		}

		while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )  
		/*Check if current file-position is within file boundaries*/
		{

			if( s32FileSz < u32PosToSet)
				break;
			/*Seek through the common file*/
			if( OSAL_s32IOControl (
                              		hFile,
                              		OSAL_C_S32_IOCTRL_FIOSEEK,
                              		(tS32)u32PosToSet
                           		  ) == OSAL_ERROR )
			{
				u32Ret += 4;
				break;
			}
			else
			{
				/*Allocate memory dynamically*/
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate
												 ( u32BytesToRead +1);
				/*Check if memory allocation successful*/
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret += 10;
					break;
				}
				else
				{
					/*Read from common file*/
					s32BytesRead = OSAL_s32IORead (
	                                hFile,
	                                ps8ReadBuffer,
	                                 u32BytesToRead
	                             );
					/*Check status of read*/
					if ( s32BytesRead == OSAL_ERROR )
					{
						u32Ret += 20;
						break;	
	    			}
					else
					{
						/*Do a string comparison*/
						if ( OSAL_s32StringNCompare ( 
							                          ps8ReadBuffer,
															  szSearchString,
															  u32StringLen
														  )
							== 0 )
					   	{
							/*Increment occurences*/
							StrngCnt++;
						}
					}											
					/*Deallocate memory*/
					if( ps8ReadBuffer != NULL)
					{ 
						OSAL_vMemoryFree( ps8ReadBuffer );
						ps8ReadBuffer = NULL;
					}
				}
			}
			u32PosToSet++;
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	
	/*Check for error code*/
   	if ( u32Ret == 0)
	{
		if( StrngCnt == 0 )
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t String not found" );
		}
		else
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t String found %u times",StrngCnt);
		}
	
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemStringSearch()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_062
* DESCRIPTION:  Search for string 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemStringSearch(tVoid)
{
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;

	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			u32Val = StrSearch(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		OpenFileDiffModes()
* PARAMETER:    filename to be created
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Open file with different access parameters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 OpenFileDiffModes(const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile= 0;
	OSAL_tIODescriptor hFile1= 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	if ( ((hFile1 = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR))
	{
		u32Ret = 1;
	}
	else
	{
		enAccess = OSAL_EN_READONLY;
		/*Open the file*/
	   	hFile = OSAL_IOOpen ( strDevName,enAccess );
		/*Check if open successful*/
		if( hFile == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			/*Close the common file*/
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 4;
			}
		}

		enAccess = OSAL_EN_WRITEONLY;
		/*Open the file*/
	   	hFile = OSAL_IOOpen ( strDevName,enAccess );
		/*Check if open successful*/
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		else
		{
			/*Close the common file*/
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 20;
			}
		}

		enAccess = OSAL_EN_READWRITE;
		/*Open the file*/
	   	hFile = OSAL_IOOpen ( strDevName,enAccess );
		/*Check if open successful*/
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 40;
		}
		else
		{
			/*Close the common file*/
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 100;
			}
		}

		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
			
		/*Remove the common File*/
		if(OSAL_s32IORemove(strDevName) == OSAL_ERROR)
		{
			u32Ret += 400;
		}			
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileOpenDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_063
* DESCRIPTION:  Create file with different access modes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileOpenDiffModes(tVoid)
{
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			u32Val = OpenFileDiffModes(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		FileReadAccessChk()
* PARAMETER:    filename 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  File Read
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 FileReadAccessChk(const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	tU32 u32Ret = 0;  
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;

	hFile = OSAL_IOOpen ( strDevName,enAccess );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead + 1);
		if ( ps8ReadBuffer == NULL)
		{
			u32Ret = 2;
		}
		else
		{
			s32BytesRead = OSAL_s32IORead (
                                hFile,
                                ps8ReadBuffer,
                                 u32BytesToRead
                             );
			if ( s32BytesRead != OSAL_ERROR )
			{
				u32Ret += 4;	
    		}
			
			if( ps8ReadBuffer != NULL)
			{
				OSAL_vMemoryFree( ps8ReadBuffer );
				ps8ReadBuffer = NULL;
			}
		}

		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}			
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadAccessCheck()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_064
* DESCRIPTION:  Try to read from the file which has no access rights 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileReadAccessCheck(tVoid)
{	
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 20] = CDROM_TEXT_FILE;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		u32Val = FileReadAccessChk(szFilePath);
		if(u32Val)
			u32Ret = u32Val + Dev_Error[0];

	}
		
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			u32Val = FileReadAccessChk(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		FileWriteAccessChk()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  File Write
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/  
tU32 FileWriteAccessChk( const tU8 * strDevName)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;  
   	tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Writing data";
	tU32 u32Ret = 0;
   	tU32 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	
	hFile = OSAL_IOOpen ( strDevName,enAccess );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		s32BytesWritten = OSAL_s32IOWrite (hFile, DataToWrite,
							(tU32) u8BytesToWrite);
		if ( s32BytesWritten != OSAL_ERROR )
		{
			u32Ret = 2;
		}
		if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 4;
		}
	}
	
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32FileSystemFileWriteAccessCheck()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_065
* DESCRIPTION:  Try to write from the file which has no access rights 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileWriteAccessCheck(tVoid)
{	 
    tU32 u32Ret = 0;               
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 20] = CDROM_TEXT_FILE;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		u32Val = FileWriteAccessChk(szFilePath);
		if(!u32Val)	
			u32Ret = u32Val + Dev_Error[0];
	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
			u32Val = FileWriteAccessChk(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}	

/*****************************************************************************
* FUNCTION:		ReadFrmSubDirFile()
* PARAMETER:    subdirectory path
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read file from sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 ReadFrmSubDirFile(const tU8* strDevName )
{
	OSAL_tIODescriptor hDir = 0,subDir=0,hFile = 0,hFile1 = 0;
    OSAL_trIOCtrlDirent rDirent = {0};  
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;
    tU8 u8BytesToWrite = 20; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
 	tChar szSubDirPath [OEDT_FS_FILE_PATH_LEN] = {0};

	tChar szSubdirName [OEDT_FS_FILE_PATH_LEN] ={0};
	tChar szDirPath [OEDT_FS_FILE_PATH_LEN] = {0};
	tChar szFileName [OEDT_FS_FILE_PATH_LEN] = {0};
	
	tCS8 DataToWrite[30] = "Writing data to Sub-dir";

	/*Subdri name*/
	(tVoid)OSAL_szStringCopy(szSubdirName,"/NEWSUB53");

	/*Subdri path name*/
	(tVoid)OSAL_szStringCopy(szSubDirPath,strDevName);

	(tVoid)OSAL_szStringConcat(szSubDirPath,"/NEWSUB53");


	/*path name for Filesystem*/
	(tVoid)OSAL_szStringCopy(szDirPath,strDevName);

	(tVoid)OSAL_szStringConcat(szDirPath,"/");
	/*path name for subdir File*/
	(tVoid)OSAL_szStringCopy(szFileName,strDevName);

	(tVoid)OSAL_szStringConcat(szFileName,"/NEWSUB53/Myfile.txt");
	
	/*Initialize the Dirent structure*/
	(tVoid)OSAL_pvMemorySet(rDirent.s8Name,'\0',OSAL_C_U32_MAX_PATHLENGTH);
	
	hDir = OSAL_IOOpen ( szDirPath,enAccess );/* szSubDirPath */
	if( hDir ==OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
		    subDir= OSAL_IOOpen ( szSubDirPath,enAccess );
			if( subDir != OSAL_ERROR )
			{

				if ( (hFile = OSAL_IOCreate ( szFileName,enAccess )) != OSAL_ERROR )
				{
					if( hFile != OSAL_ERROR )
					{
						s32BytesWritten = OSAL_s32IOWrite (
					                           hFile, DataToWrite,
													(tU32) u8BytesToWrite
														    );
						if ( s32BytesWritten == OSAL_ERROR )
						{
							u32Ret += 4;
						}
						OSAL_s32IOClose ( hFile );
					}
					hFile1 = OSAL_IOOpen ( szFileName,enAccess );
					if( hFile1 != OSAL_ERROR )
					{
						ps8ReadBuffer = (tS8 *)
										 OSAL_pvMemoryAllocate 
										 		( u32BytesToRead +1);
						s32BytesRead = OSAL_s32IORead (
					                                hFile1,
					                                ps8ReadBuffer,
					                                 u32BytesToRead
				                             			);
						if ( s32BytesRead == OSAL_ERROR )
						{
							u32Ret += 10;	
					    }
						else
						{
							vTtfisTrace( OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes read : %d",s32BytesRead );	
						}
							
						if( ps8ReadBuffer != NULL)
						{
							OSAL_vMemoryFree( ps8ReadBuffer );
							ps8ReadBuffer = NULL;
						}
						if ((OSAL_s32IOClose ( hFile1 ))!=OSAL_ERROR)
						{				
							OSAL_s32IORemove ( szFileName );
						}

					}
					
				}
				else
				{
				   u32Ret += 20;	
				}
			}
		   if ((OSAL_s32IOClose ( subDir ))!=OSAL_ERROR)
		   {	 
		   		if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 40;
				}
		   }	
	   
		OSAL_s32IOClose ( hDir );
		}
	}
	return u32Ret;   
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileReadSubDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_066
* DESCRIPTION:  Read file present in sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileReadSubDir(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
			u32Val = ReadFrmSubDirFile(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
	return u32Ret;
} 

/*****************************************************************************
* FUNCTION:		WriteToSubDirFile()
* PARAMETER:    subdirectory path
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write to file in sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 WriteToSubDirFile(const tU8* strDevName)
{	
	OSAL_tIODescriptor hDir = 0,subDir1=0,subDir2 = 0,hFile = 0;
    OSAL_trIOCtrlDirent rDirent1 = {0};
    OSAL_trIOCtrlDirent rDirent2 = {0};
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU8 u8BytesToWrite = 20; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	tChar szDirPath [OEDT_FS_FILE_PATH_LEN] = {'\0'};
	tChar szSubDirPath1 [OEDT_FS_FILE_PATH_LEN] = {'\0'};
	tChar szSubDirPath2 [OEDT_FS_FILE_PATH_LEN] = {'\0'};
	tChar szSubdirName1 [OEDT_FS_FILE_PATH_LEN] = {'\0'};
	tChar szSubdirName2 [OEDT_FS_FILE_PATH_LEN] = {'\0'};

	tChar szSubDirFileName [OEDT_FS_FILE_PATH_LEN] = {'\0'};
	tCS8 DataToWrite[30] = "Writing data to Sub-dir";

	/*Subdri1 name*/
	(tVoid)OSAL_szStringCopy(szSubdirName1,"/SUB59");
	/*Subdri1 name*/
	(tVoid)OSAL_szStringCopy(szSubdirName2,"/SUBTWO");

	/*Subdri1 path name*/
	(tVoid)OSAL_szStringCopy(szSubDirPath1,strDevName);

	(tVoid)OSAL_szStringConcat(szSubDirPath1,"/SUB59");

	/*Subdri2 path name*/
	(tVoid)OSAL_szStringCopy(szSubDirPath2,szSubDirPath1);

	(tVoid)OSAL_szStringConcat(szSubDirPath2,"/SUBTWO");


	/*path name for Filesystem*/
	(tVoid)OSAL_szStringCopy(szDirPath,strDevName);

	(tVoid)OSAL_szStringConcat(szDirPath,"/");
	/*path name for subdir File*/
	(tVoid)OSAL_szStringCopy(szSubDirFileName,strDevName);

	(tVoid)OSAL_szStringConcat(szSubDirFileName,"/SUB59/SUBTWO/Myfile.txt");


	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir ==OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent1.s8Name,szSubdirName1 );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
		    subDir1 = OSAL_IOOpen ( szSubDirPath1,enAccess ); 
			if( subDir1 != OSAL_ERROR )
			{
			   (tVoid)OSAL_szStringCopy ( rDirent2.s8Name,szSubdirName2 );
				  if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent2) == OSAL_ERROR )
		        {
			       u32Ret += 4;
		        }
				else
				{
				  subDir2 = OSAL_IOOpen ( szSubDirPath2,enAccess ); 
					  if( subDir2 != OSAL_ERROR )
					  {
				  
						 if ( (hFile = OSAL_IOCreate ( szSubDirFileName,enAccess )) != OSAL_ERROR )
						 {							
						  	s32BytesWritten = OSAL_s32IOWrite (
							                           hFile, DataToWrite,
															(tU32) u8BytesToWrite
																    );
						  	if ( s32BytesWritten == OSAL_ERROR )
						  	{
						   		u32Ret += 10;
						  	}
							OSAL_s32IOClose ( hFile );
							OSAL_s32IORemove ( szSubDirFileName );
						 }
						 else
						 {
							u32Ret += 20;
						 }
									
					  }
					  OSAL_s32IOClose ( subDir2 );

			   			if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent2) == OSAL_ERROR )
							{
								u32Ret += 40;
							}

			    }
			}
			OSAL_s32IOClose ( subDir1 );

			if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent1) == OSAL_ERROR )
					{
						u32Ret += 100;
					}
		}
		
		OSAL_s32IOClose ( hDir );

	}
	return u32Ret;   
} 

/*****************************************************************************
* FUNCTION:		u32FileSystemFileWriteSubDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_067
* DESCRIPTION:  Write to file present in sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileWriteSubDir(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
			u32Val = WriteToSubDirFile(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
		
	return u32Ret;
} 

/*****************************************************************************
* FUNCTION:		ReadWriteTimeMeasure1()
* PARAMETER:    filename , number of bytes to be read/written
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB, 10KB, 1MB
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
tU32 ReadWriteTimeMeasure1(const tU8* strDevName, tU32 u32ReadCount)
{	
    OSAL_tMSecond StartTime	= 0;
    OSAL_tMSecond TimetakenToWrite = 0;
    OSAL_tMSecond TimetakenToRead = 0;
	OSAL_tMSecond EndTime	= 0;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
	  	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = u32ReadCount;/* Number of bytes to read */
	tU32 u32BlockSize = u32ReadCount;
   	tS32 s32BytesRead = 0;
   	tS8 *ps8WriteBuffer = NULL;
    tS32 s32BytesWritten = 0;	

	/*File path name*/
	(tVoid)OSAL_szStringCopy(szFilePath,strDevName);
	
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);	
	/*Check if allocation successful*/
	if(	ps8WriteBuffer == NULL)
		u32Ret = 1;
	else
	{
		/*Initialize data*/
		OSAL_pvMemorySet( ps8WriteBuffer,'\0',u32BlockSize );
		/*Fill the buffer with data*/
	 	OSAL_pvMemorySet( ps8WriteBuffer,'F',u32BlockSize-1 );	
	 
		if( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
		{
			u32Ret = 2;
		}		
		else
		{
		
			StartTime = OSAL_ClockGetElapsedTime();
	   		s32BytesWritten = OSAL_s32IOWrite (hFile, ps8WriteBuffer,
										   u32BlockSize);
		    EndTime = OSAL_ClockGetElapsedTime();

	   		/*Wrapping?*/
	  		if( EndTime > StartTime )
	  		{
		   		TimetakenToWrite = 	EndTime - StartTime;	
	  		}
	  		else
	  		{
				TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
	  		}
	  			
			if ( s32BytesWritten == OSAL_ERROR )
			{
				u32Ret = 4;
			}
	  		
	  		OSAL_s32IOClose ( hFile );
			
			hFile = OSAL_IOOpen ( szFilePath,enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 10;
			}
			else
			{
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);

				/*Check if allocation successful*/
				if(	ps8ReadBuffer == NULL )
				{
				   OSAL_vMemoryFree( ps8WriteBuffer );
				   OSAL_s32IOClose ( hFile );
				   OSAL_s32IORemove ( szFilePath );
				   return (tU32)OSAL_ERROR;
				}

				StartTime = OSAL_ClockGetElapsedTime();
				s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
		        	                   u32BytesToRead);
				EndTime = OSAL_ClockGetElapsedTime();

		   		/*Wrapping*/
		  		if( EndTime > StartTime )
		  		{
		  			TimetakenToRead = EndTime-StartTime;
		  		}
		  		else
		  		{
					TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
		  		}	            
				vPrintOutTime(TimetakenToRead,TimetakenToWrite,(tU64)u32BytesToRead,(tU64)u32BlockSize);
				if ( s32BytesRead == OSAL_ERROR )
				{
					u32Ret += 20;	
				}
				if(ps8ReadBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8ReadBuffer );
					ps8ReadBuffer = NULL;
				}

				if( ps8WriteBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8WriteBuffer );
					ps8WriteBuffer = NULL;
				}

				OSAL_s32IOClose ( hFile );
			}
		}
	}
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof1KbFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_068
* DESCRIPTION:  Read Write time measure for 1KB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof1KbFile(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 u32ReadCount = 1024;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadWriteTimeMeasure1(namebuffer, u32ReadCount);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	} 
	
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	 		
			
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof10KbFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_069
* DESCRIPTION:  Read Write time measure for 10KB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof10KbFile(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 10240;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadWriteTimeMeasure1(namebuffer, u32ReadCount);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
					
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof100KbFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_070
* DESCRIPTION:  Read Write time measure for 10KB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof100KbFile(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 102400;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadWriteTimeMeasure1(namebuffer, u32ReadCount);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		ReadWriteTimeMeasure2()
* PARAMETER:    filename , number of bytes to be read/written, blocksize
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB file 1000 times 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
tU32 ReadWriteTimeMeasure2(const tU8* strDevName, tU32 u32ReadCount, tU32 u32Size)
{	
    OSAL_tMSecond StartTime	= 0,TimetakenToWrite = 0,TimetakenToRead = 0;
	OSAL_tMSecond EndTime	= 0;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] ={'\0'};
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = u32ReadCount;/* Number of bytes to read */
	tU32 u32BlockSize = u32Size;
   	tS32 s32BytesRead = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	

	/*File path name*/
	(tVoid)OSAL_szStringCopy(szFilePath,strDevName);

	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);	
	/*Check if allocation successful*/
	if(	ps8WriteBuffer == NULL)
		u32Ret = 1;
	else
	{
		/*Initialize data*/
		OSAL_pvMemorySet( ps8WriteBuffer,'\0',u32BlockSize );
		/*Fill the buffer with data*/
	 	OSAL_pvMemorySet( ps8WriteBuffer,'F',u32BlockSize-1 );	
	 
		if( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
		{
			u32Ret = 2;
		}		
		else
		{
		
			StartTime = OSAL_ClockGetElapsedTime();
	   		for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
			{
	   		s32BytesWritten = OSAL_s32IOWrite (hFile, ps8WriteBuffer,
										    u32BlockSize);
			 if ( ( s32BytesWritten == (tS32)OSAL_ERROR ) || (	s32BytesWritten != (tS32)u32BlockSize ))
				{
					u32Ret = 4;
					break;
				}
			
			}
				   					
		    EndTime = OSAL_ClockGetElapsedTime();

	   		/*Wrapping?*/
	  		if( EndTime > StartTime )
	  		{
		   		TimetakenToWrite = 	EndTime - StartTime;	
	  		}
	  		else
	  		{
				TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
	  		}
	  			
	  		OSAL_s32IOClose ( hFile );
			
			hFile = OSAL_IOOpen ( szFilePath,enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 10;
			}
			else
			{
				if(ps8WriteBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8WriteBuffer );
					ps8WriteBuffer = NULL;
				}

				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( 1048576 +1);

				/*Check if allocation successful*/
				if(	ps8ReadBuffer == NULL )
				{
				   OSAL_vMemoryFree( ps8ReadBuffer );
				   OSAL_s32IOClose ( hFile );
				   OSAL_s32IORemove ( szFilePath );
				   return (tU32)OSAL_ERROR;
				}

				StartTime = OSAL_ClockGetElapsedTime();
				s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
		        	                    u32BytesToRead);
				EndTime = OSAL_ClockGetElapsedTime();

		   		/*Wrapping*/
		  		if( EndTime > StartTime )
		  		{
		  			TimetakenToRead = EndTime-StartTime;
		  		}
		  		else
		  		{
					TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
		  		}	            

				if ( s32BytesRead == OSAL_ERROR )
				{
					u32Ret += 20;	
				}
				vPrintOutTime(TimetakenToRead,TimetakenToWrite,(tU64)u32BytesToRead,(tU64)u32BlockSize);

				if(ps8ReadBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8ReadBuffer );
					ps8ReadBuffer = NULL;	
				}
				
				OSAL_s32IOClose ( hFile );
			}
		}
	}
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof1MBFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_071
* DESCRIPTION:  Read Write time measure for 100KB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof1MBFile(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 1048576;
	tU32 u32Size = (256*1024);
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadWriteTimeMeasure2(namebuffer, u32ReadCount, u32Size);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		ReadWriteTimeMeasure3()
* PARAMETER:    device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB file 10000 times, 10KB 1000 times
*               100KB 100 times   
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
tU32 ReadWriteTimeMeasure3(const tU8* strDevName, tU32 u32ReadCount, tU32 u32Loop)
{	
    OSAL_tMSecond StartTime	= 0,TimetakenToWrite = 0,TimetakenToRead = 0;
	OSAL_tMSecond EndTime	= 0;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = u32ReadCount;/* Number of bytes to read */
	tU32 u32BlockSize = u32ReadCount;
   	tS32 s32BytesRead = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	
	/*File path name*/
	(tVoid)OSAL_szStringCopy(szFilePath,strDevName);
	
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);	
	/*Check if allocation successful*/
	if(	ps8WriteBuffer == NULL)
		u32Ret = 1;
	else
	{
		/*Initialize data*/
		OSAL_pvMemorySet( ps8WriteBuffer,'\0',u32BlockSize );
		/*Fill the buffer with data*/
	 	OSAL_pvMemorySet( ps8WriteBuffer,'F',u32BlockSize-1 );	
	 
		if( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
		{
			u32Ret = 2;
		}		
		else
		{
		
			StartTime = OSAL_ClockGetElapsedTime();
	   		for( loop_cnt = 0;loop_cnt < u32Loop;loop_cnt ++ )
			{
	   		s32BytesWritten = OSAL_s32IOWrite (hFile, ps8WriteBuffer,
										    u32BlockSize);
			 if (	s32BytesWritten != (tS32)u32BlockSize )
				{
					u32Ret = 4;
					break;
				}
			
			}
				   					
		    EndTime = OSAL_ClockGetElapsedTime();

	   		/*Wrapping?*/
	  		if( EndTime > StartTime )
	  		{
		   		TimetakenToWrite = 	EndTime - StartTime;	
	  		}
	  		else
	  		{
				TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
	  		}
	  			
				if ( s32BytesWritten == OSAL_ERROR )
				{
					u32Ret += 10;
				}

	  		OSAL_s32IOClose ( hFile );
			
			hFile = OSAL_IOOpen ( szFilePath,enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 20;
			}
			else
			{
			ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( 1048576 +1);

				/*Check if allocation successful*/
				if(	ps8ReadBuffer == NULL )
				{
				   OSAL_vMemoryFree( ps8ReadBuffer );
				   OSAL_s32IOClose ( hFile );
				   OSAL_s32IORemove ( szFilePath );
				   return (tU32)OSAL_ERROR;
				}

				StartTime = OSAL_ClockGetElapsedTime();
				for( loop_cnt = 0;loop_cnt < 1000;loop_cnt ++ )
				{
					s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
		        	                    u32BytesToRead);
					if(s32BytesRead != (tS32)u32BytesToRead)
					{
						u32Ret += 40;
						break;
					}
				 }
				EndTime = OSAL_ClockGetElapsedTime();

		   		/*Wrapping*/
		  		if( EndTime > StartTime )
		  		{
		  			TimetakenToRead = EndTime-StartTime;
		  		}
		  		else
		  		{
					TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
		  		}	            

				if ( s32BytesRead == OSAL_ERROR )
				{
					u32Ret += 100;	
				}
				vPrintOutTime(TimetakenToRead,TimetakenToWrite,(tU64)u32BytesToRead,(tU64)u32BlockSize);
				if(ps8ReadBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8ReadBuffer );
					ps8ReadBuffer = NULL;

				}

				if(ps8WriteBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8WriteBuffer );
					ps8WriteBuffer = NULL;
				}

				OSAL_s32IOClose ( hFile );
			}
		}
	}
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof1KbFilefor10000times()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_072
* DESCRIPTION:  Read write time measure for 1KB file 10000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof1KbFilefor10000times(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 1024;
	tU32 u32Loop = 10000;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
	    OEDT_HelperPrintf(TR_LEVEL_USER_1,"\n%s\n",namebuffer);
                      

		   	u32Val = ReadWriteTimeMeasure3(namebuffer, u32ReadCount, u32Loop);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof10KbFilefor1000times()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_073
* DESCRIPTION:  Read write time measure for 10KB file 1000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof10KbFilefor1000times(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 10240;
	tU32 u32Loop = 1000;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadWriteTimeMeasure3(namebuffer, u32ReadCount, u32Loop);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
				
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof100KbFilefor100times()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_074
* DESCRIPTION:  Read write time measure for 10KB file 1000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof100KbFilefor100times(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32ReadCount = 102400;
	tU32 u32Loop = 100;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
	    OEDT_HelperPrintf(TR_LEVEL_USER_1,"\n%s\n",namebuffer);

		   	u32Val = ReadWriteTimeMeasure3(namebuffer, u32ReadCount, u32Loop);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	
				
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		ReadWriteTimeMeasure4()
* PARAMETER:    filename, number of bytes to read/write, blocksize
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1MB for 16 times   
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
tU32 ReadWriteTimeMeasure4(const tU8* strDevName)
{	
    OSAL_tMSecond StartTime	= 0,TimetakenToWrite = 0,TimetakenToRead = 0;
	OSAL_tMSecond EndTime	= 0;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 1048576;/* Number of bytes to read */
	tU32 u32BlockSize = 256*1024;
   	tS32 s32BytesRead = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
	tU32 loop_count = 0;
    tS32 s32BytesWritten = 0;	

	/*File path name*/
	(tVoid)OSAL_szStringCopy(szFilePath,strDevName);
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);	
	/*Check if allocation successful*/
	if(	ps8WriteBuffer == NULL)
		u32Ret = 1;
	else
	{
		/*Initialize data*/
		OSAL_pvMemorySet( ps8WriteBuffer,'\0',u32BlockSize );
		/*Fill the buffer with data*/
	 	OSAL_pvMemorySet( ps8WriteBuffer,'F',u32BlockSize-1 );	
	 
		if( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
		{
			u32Ret = 2;
		}		
		else
		{
			StartTime = OSAL_ClockGetElapsedTime();
			for( loop_count = 0;loop_count <16;loop_count ++ )
		    {
		   		for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
				{
		   		s32BytesWritten = OSAL_s32IOWrite (hFile, ps8WriteBuffer,
											    u32BlockSize);
					if ( s32BytesWritten == (tS32)OSAL_ERROR )
					{
						u32Ret = 4;
						break;
					}
				
				}
		    }	   					
		    EndTime = OSAL_ClockGetElapsedTime();

	   		/*Wrapping?*/
	  		if( EndTime > StartTime )
	  		{
		   		TimetakenToWrite = 	EndTime - StartTime;	
	  		}
	  		else
	  		{
				TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
	  		}
	  			
	  		OSAL_s32IOClose ( hFile );
			
			hFile = OSAL_IOOpen ( szFilePath,enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret += 10;
			}
			else
			{
			ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( 1048576 +1);

				/*Check if allocation successful*/
				if(	ps8ReadBuffer == NULL )
				{
				   OSAL_vMemoryFree( ps8ReadBuffer );
				   OSAL_s32IOClose ( hFile );
				   OSAL_s32IORemove ( szFilePath );
				   return (tU32)OSAL_ERROR;
				}

				StartTime = OSAL_ClockGetElapsedTime();
			 for( loop_cnt = 0;loop_cnt < 16;loop_cnt ++ )
				 {
				s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,
		        	                   u32BytesToRead);
				if ( s32BytesRead == (tS32)OSAL_ERROR )
				{
					u32Ret += 20;	
					break;
				}
				 }
				EndTime = OSAL_ClockGetElapsedTime();

		   		/*Wrapping*/
		  		if( EndTime > StartTime )
		  		{
		  			TimetakenToRead = EndTime-StartTime;
		  		}
		  		else
		  		{
					TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
		  		}	            
				vPrintOutTime(TimetakenToRead,TimetakenToWrite,(tU64)u32BytesToRead,(tU64)u32BlockSize);

				if(ps8ReadBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8ReadBuffer );
					ps8ReadBuffer = NULL;
				}
				
				if(ps8WriteBuffer != NULL)
				{
					OSAL_vMemoryFree( ps8WriteBuffer );				
					ps8WriteBuffer = NULL;
				}

				OSAL_s32IOClose ( hFile );
			}

			/* Remove file without closing */
			if( OSAL_s32IORemove ( strDevName ) == OSAL_ERROR )
			{
				u32Ret += 40;
			}
		}
	}
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadWriteTimeMeasureof1MBFilefor16times()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_075
* DESCRIPTION:  Read write time measure for 1MB file 16 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadWriteTimeMeasureof1MBFilefor16times(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
	OSAL_trIOCtrlDeviceSize rSize = {0};
	OSAL_tIODescriptor hFile = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
			hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE);
			if(hFile == OSAL_ERROR)
			{
				break;
			}
			  	
			rSize.u32High = 0;
		    rSize.u32Low  = 0;
			/* Get free space */
		    u32Ret = (tU32)OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE, (tS32)&rSize);
		    if(((rSize.u32Low == 0) && (rSize.u32High == 0)) || (u32Ret == (tU32)OSAL_ERROR) || (rSize.u32Low < WRITE_SIZE))
		  	{
		  		OEDT_HelperPrintf(TR_LEVEL_USER_1,"%sInsufficient Size\n",namebuffer);
			}
			else
			{
				OSAL_pvMemorySet(namebuffer,'\0',29);		
				file_name(count,namebuffer,"/common_test.txt");
		    	OEDT_HelperPrintf(TR_LEVEL_USER_1,"\n%s\n",namebuffer);
		
			   	u32Val = ReadWriteTimeMeasure4(namebuffer);
				if(u32Val)	
					u32Ret = u32Val + Dev_Error[count+1];
			}
			OSAL_s32IOClose ( hFile );
		}
	}	
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
		
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		FSThroughPutFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Calculate throughput
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32  FSThroughPutFile(const tU8* strDevName)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 Read_ThroughPut=0;
   	OSAL_tMSecond StartTime=0;
   	OSAL_tMSecond EndTime = 0;
   	OSAL_tMSecond ReadTime = 0;
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	
	/*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (CDROM_BYTES_TO_READ_TPUT);

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( strDevName,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 0;
		/* Seek to the byte position specified */
		if(OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
		{
			u32Ret = 2;
		}	
	   /* Read the time before the start of the read operation */	
	   StartTime = OSAL_ClockGetElapsedTime();
	   	
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,CDROM_BYTES_TO_READ_TPUT);
#ifdef TSIM_OSAL
                /* reading is tooo fast on TSIM */
                /* So delay to make the time measurement meaningful*/
                OSAL_s32ThreadWait(10);
#endif
		/* Read the time before the end of the read operation */	
	  	EndTime = OSAL_ClockGetElapsedTime();
	  	
	  	/* The read number is important. Compare if expected number 
		is read. But get the time before this as the time calculation in
		critical only for the read operation, not the verification */
	
		if( (OSAL_ERROR == ReadRet) || (CDROM_BYTES_TO_READ_TPUT != ReadRet ) )
		{
			u32Ret += 4; 
		}
		else
		{
		    /* Check if end time greater than start time */
			if(EndTime > StartTime)
			{
                            /* Calculate the time required for read in millisec */
                            ReadTime =   (EndTime - StartTime); 
                        }else{
                        ReadTime =   (0xFFFFFFFF-StartTime) +EndTime+1;
                    }
					/* Throughput needs to be expressed in kB/s */
                    /* check for ReadTime != 0 */
                    if(ReadTime != 0){
                        /* now we have bytes/ms which is == kB/s */ 
                        Read_ThroughPut = (CDROM_BYTES_TO_READ_TPUT) / (ReadTime);                                                              
                    }else{
                        Read_ThroughPut = 0;
                    }
                    /* If the throughput < 300, indicate an error condition */			
                    if(	Read_ThroughPut < 300 )
				{
					u32Ret += 10;
				}
			}
		/* Close the file */	
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 20;
		}
	}
	/* free the dynamically allocated memory */
	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileThroughPutFirstFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_076
* DESCRIPTION:  Calculate throughput for first CDROM file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileThroughPutFirstFile(tVoid)
{
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 20] = CDROM_TEXT_FILE;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
		
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		u32Val = FSThroughPutFile(szFilePath);
		if(u32Val)	
			u32Ret = u32Val + Dev_Error[0];
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileThroughPutSecondFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_077
* DESCRIPTION:  Calculate throughput for last CDROM file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileThroughPutSecondFile(tVoid)
{
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 80] = CDROM_TEXT_LAST;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
		
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		u32Val = FSThroughPutFile(szFilePath);
		if(u32Val)	
			u32Ret = u32Val + Dev_Error[0];
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFileThroughPutMP3File()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_078
* DESCRIPTION:  Calculate throughput for CDROM MP3 file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 u32FileSystemFileThroughPutMP3File(tVoid)
{
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 20] = CDROM_MP3_FILE;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
		
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	if(	Dev_Identity[0][Device_ID])
	{
		u32Val = FSThroughPutFile(szFilePath);
		if(u32Val)	
			u32Ret = u32Val + Dev_Error[0];
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		FSRenameFile()
* Parameter:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Rename file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/
tU32 FSRenameFile(const tU8* strDevName)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0, hDev=0;
	tU32 u32Ret = 0;    
	tChar* arrFileName[2]= {OSAL_NULL};
	tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] ={'\0'};
	tChar szReFilePath [OEDT_FS_FILE_PATH_LEN + 1] ={'\0'};
	tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "This is a Test File";
	/*File path name*/
	(tVoid)OSAL_szStringCopy(szFilePath,strDevName);
	(tVoid)OSAL_szStringConcat(szFilePath,"/File1.txt");
	/*Renamed File path */
	(tVoid)OSAL_szStringCopy(szReFilePath,strDevName);
	(tVoid)OSAL_szStringConcat(szReFilePath,"/File2.txt");


    /*Create the file in readwrite mode */
 	hFile = OSAL_IOCreate (szFilePath, enAccess);
	OSAL_s32IOWrite (hFile,(tPCS8)DataToWrite,19);
	OSAL_s32IOClose(hFile);

    if (hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
   		arrFileName[0] = (tChar*)"File1.txt";        /* old name*/ 
   		arrFileName[1] = (tChar*)"File2.txt";       /* new name */ 
 	    
	 	hDev = OSAL_IOOpen (strDevName,OSAL_EN_READWRITE);
        if (hDev == OSAL_ERROR )
		{
		   u32Ret = 2;
		}
    	else 
		{
    	  if(OSAL_ERROR == OSAL_s32IOControl(hDev, OSAL_C_S32_IOCTRL_FIORENAME,(tS32)arrFileName ))
		  {
			u32Ret += 4;
 		    /*If rename fails, close  the  */
		    if (OSAL_ERROR == OSAL_s32IORemove(szFilePath))
			{
				u32Ret += 10;
			}
		  }
		  else
		  {
				/*Delete the old  file */
				if (OSAL_ERROR == OSAL_s32IORemove(szReFilePath))
				{
					u32Ret += 20;
				}
		  }/*Sucess of close of the old file*/ 
						
 	      /*Close the new  file */
		  if (OSAL_ERROR == OSAL_s32IOClose(hDev))
		  {
				u32Ret += 40;
		  }
		}/*	Sucess of  IO Control function*/
	}

 	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemRenameFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_079
* DESCRIPTION:  Rename file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemRenameFile(tVoid)
{
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
			u32Val = FSRenameFile(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		WriteFrmBOF()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write to file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 WriteFrmBOF(const tU8* strDevName)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;  
   	tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "New data written";
   	tS8 *ps8WriteBuffer = NULL;
    tU8 u8BytesToWrite = 19; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
  	tS32 s32PosToSet = 0;
	tPS8 Retval = NULL; 	

	hFile = OSAL_IOOpen ( strDevName,enAccess );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		s32PosToSet = 5;
		if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			ps8WriteBuffer = (tS8 *) OSAL_pvMemoryAllocate( u8BytesToWrite +1);
			if ( ps8WriteBuffer == NULL )
			{
				u32Ret = 3;
			}
			else
			{
				 Retval=(tPS8)OSAL_szStringCopy( ps8WriteBuffer,DataToWrite );
				if(*Retval == OSAL_ERROR)
				{
					u32Ret = 4;
				}
				else
				{

					s32BytesWritten = OSAL_s32IOWrite(hFile,ps8WriteBuffer,
		                                (tS32) u8BytesToWrite);
					if ( (s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != u8BytesToWrite) )
					{
						u32Ret = 5;	
		    		}
				}
				OSAL_vMemoryFree( ps8WriteBuffer );
				ps8WriteBuffer = NULL;
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_080
* DESCRIPTION:  Write to file from BOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteFrmBOF(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
		
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = WriteFrmBOF(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		WriteFrmEOF()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write from EOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
******************************************************************************/  
tU32 WriteFrmEOF(const tU8* strDevName)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;  
	tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Appending more data";
   	tS8 *ps8WriteBuffer = NULL;
    tU8 u8BytesToWrite = 25; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
  	tS32 s32PosToSet = 0;
	tS32 s32FileSz = 0;
	tPS8 Retval = NULL;

	hFile = OSAL_IOOpen ( strDevName,enAccess );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
	                              	(tS32)&s32FileSz)== OSAL_ERROR )
		{
			u32Ret = 2;
		}

		else
		{
			s32PosToSet = s32FileSz;
			if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet) == OSAL_ERROR )
			{
				u32Ret += 4;
			}
			else
			{
				ps8WriteBuffer = (tS8 *) OSAL_pvMemoryAllocate( u8BytesToWrite );
				if ( ps8WriteBuffer == NULL )
				{
					u32Ret += 10;
				}
				else
				{
					Retval = (tPS8)OSAL_szStringCopy
							    ( ps8WriteBuffer,DataToWrite );
					    
					if (*Retval == OSAL_ERROR )
					{
						u32Ret += 20;
					}
					else
					{

						s32BytesWritten = OSAL_s32IOWrite (
			                                hFile,
			                                ps8WriteBuffer,
			                                (tS32) u8BytesToWrite
			                             );
						if ( s32BytesWritten == OSAL_ERROR )
						{
							u32Ret += 40;	
			    		}
					}
					OSAL_vMemoryFree( ps8WriteBuffer );
					ps8WriteBuffer = NULL;
				}
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_081
* DESCRIPTION:  Write to file from EOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteFrmEOF(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
		
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = WriteFrmEOF(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		FSLargeFileRead()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 FSLargeFileRead(const tU8* strDevName)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
   	tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "testdata";
	tU32 u32Ret = 0;
   	tS8 *ps8ReadBuffer = NULL;     
   	tS32 s32BytesRead = 0;
	tU32 u32Slen = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	
	tU32 u32BlockSize = 0;	/* 256 KB */
	tU32 u32BytesToRead = 0;/* Number of bytes to read - 1 MB */
		
	u32Slen = strlen( DataToWrite );
	u32BlockSize = 1024*32*u32Slen;
    u32BytesToRead = u32BlockSize *4;
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);

	/*Check if Allocation is successful*/
	if(	ps8WriteBuffer == NULL)
	{
		return (tU32)OSAL_ERROR;
	}

	/* Write 256 KB of data to a buffer */
	OSAL_pvMemorySet(ps8WriteBuffer,'\0',u32BlockSize);
	for( loop_cnt = 0;loop_cnt < 1024*32;loop_cnt ++ )
	{
		(tVoid)OSAL_szStringNConcat(ps8WriteBuffer,DataToWrite,u32Slen);
	}
	for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
	{
		s32BytesWritten = OSAL_s32IOWrite (
                            hFile, ps8WriteBuffer,
										  u32BlockSize
								    );
		if ( (s32BytesWritten == (tS32)OSAL_ERROR)||(s32BytesWritten != (tS32)u32BlockSize) )
		{
			u32Ret = 3;
			break;
		}
	}	   					
	if(ps8WriteBuffer != NULL)
	{
		OSAL_vMemoryFree( ps8WriteBuffer );
		ps8WriteBuffer = NULL ;		
	}

	hFile = OSAL_IOOpen ( strDevName,enAccess );
	if( hFile == OSAL_ERROR )
	{
		OSAL_s32IORemove ( strDevName ); 
		u32Ret += 10;
	}
	else
	{
		ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);
		
		/*Check if allocation successful*/
		if(ps8ReadBuffer == NULL)
		{
			/*Shutdown*/
			OSAL_s32IOClose(hFile);
			OSAL_s32IORemove(strDevName);
			return (tU32)OSAL_ERROR;
		}
					
		s32BytesRead = OSAL_s32IORead (
                            hFile,
                            ps8ReadBuffer,
                             u32BytesToRead
                         );
		if ( (s32BytesRead == (tS32)OSAL_ERROR) && (s32BytesRead != (tS32)u32BytesToRead) )
		{
			u32Ret += 100;	
		}

		if(ps8ReadBuffer != NULL)
		{
			OSAL_vMemoryFree( ps8ReadBuffer );
			ps8ReadBuffer = NULL;
		}

		if ( OSAL_s32IOClose ( hFile )!=OSAL_ERROR )
		{
			OSAL_s32IORemove ( strDevName );
		}
	}
	return u32Ret;	

}

/*****************************************************************************
* FUNCTION:		u32FileSystemLargeFileRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_082
* DESCRIPTION:  Read large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemLargeFileRead(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/LargeFile.txt");
		   	u32Val = FSLargeFileRead(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
		
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
			
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		FSLargeFileWrite()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write large data to file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 FSLargeFileWrite(const tU8* strDevName)
{	
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
   	tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "TestData";
	tU32 u32Ret = 0;
	tU32 u32Slen = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	
	tU32 u32BlockSize = 0;	
		
	u32Slen = strlen( DataToWrite );
	u32BlockSize = 1024*32*u32Slen;	
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize+1);

	/*Check if allocation successful*/
	if(ps8WriteBuffer == NULL)
		return (tU32)OSAL_ERROR;

	OSAL_pvMemorySet(ps8WriteBuffer,0,u32BlockSize+1);
	/* Write 256 KB of data to a buffer */
   	for( loop_cnt = 0;loop_cnt < 1024*32;loop_cnt ++ )
	{
		(tVoid)OSAL_szStringNConcat(ps8WriteBuffer,DataToWrite,u32Slen);
	}
	
	if( (hFile = OSAL_IOCreate (strDevName, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}		
	else
	{
   		for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
		{
			s32BytesWritten = OSAL_s32IOWrite (hFile, ps8WriteBuffer,
										  u32BlockSize);
			if ( (s32BytesWritten == (tS32)OSAL_ERROR) && (s32BytesWritten!=(tS32)(u32BlockSize)) )
			{
				u32Ret = 3;
				break;
			}
		}
	   	if(ps8WriteBuffer != NULL)
	   	{
	   		OSAL_vMemoryFree( ps8WriteBuffer );					
			ps8WriteBuffer = NULL;
		}

    	if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;			
		}
		else
		{
			if ( OSAL_s32IORemove ( strDevName )==OSAL_ERROR )
			{
				u32Ret += 20;
			}	   					
	   	}
	}
	return u32Ret;	

}	

/*****************************************************************************
* FUNCTION:		u32FileSystemLargeFileWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_083
* DESCRIPTION:  Write large data to file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemLargeFileWrite(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	/* Create the common file */
	if(OSAL_NULL != (u32Ret = u32FSCreateCommonFile()))
	{
		u32Ret += 8000;
	}
		
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	for(count=0;count<6;count++)
	{
		if(Dev_Identity[count+1][Device_ID]) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/LargeFile.txt");
		   	u32Val = FSLargeFileWrite(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
		
	/* Remove common file */
	if(OSAL_NULL != (u32Ret = u32FSRemoveCommonFile()))
	{
		u32Ret += 9000;
	}
			
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		OpenCloseThread()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Open and close file 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 OpenCloseThread(tU8* strDevName)
{
	/*Definitions*/
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	OSAL_tEventMask EM  = 0;
	static tU32 var = 0;
   	/*Thread attributes*/
	OSAL_trThreadAttribute tr1_atr = {0};
	OSAL_trThreadAttribute tr2_atr = {0};

	/*Initialize thread return code*/
	u32tr1FS = 0;
	u32tr2FS = 0;

	/*Initialize the event handle*/
	hEventFS = 0;

	/*Create file */
	hFile = OSAL_IOCreate( strDevName , OSAL_EN_READWRITE );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	/*Fill thread Attributes*/
	tr1_atr.szName  = "FSThread1";
	tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	tr1_atr.s32StackSize = 4096;
	tr1_atr.pfEntry      = (FP)OpenThreadFS;
	tr1_atr.pvArg        = strDevName;

	/*Fill thread Attributes*/
	tr2_atr.szName 	    = "FSThread2";
	tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	tr2_atr.s32StackSize = 4096;
	tr2_atr.pfEntry      = (FP)CloseThreadFS;
	tr2_atr.pvArg        = NULL;

	/*Create the Event field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
	{
		u32Ret += 2;
	}

	/*Create the Event field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME1 , &hEventFS1 ) )
	{
		u32Ret += 4;
	}
	 	
	/*Create the first thread*/
	if( OSAL_ERROR ==  OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr1_atr )  )
	{
	 	u32Ret += 10;
	}
	/*Create the first thread*/
	if( OSAL_ERROR ==  OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr2_atr )  )
	{
	 	u32Ret += 20;
	}
	/*Wait for threads to Post status*/
	if( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1|FS_THREAD2,
	                 	   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM ) )
	{
	 	u32Ret += 40;
	}
    OEDT_HelperPrintf(TR_LEVEL_USER_1,
                      " open close thread relase %d\n",var);

    
	 ++var;
	 OSAL_s32EventPost( hEventFS,~EM,OSAL_EN_EVENTMASK_AND );
	/*Close event*/
	if( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
	{
		u32Ret += 100;
	}
	/*Delete event*/
	if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
	{
		u32Ret += 200;
	}

	/*Close event*/
	if( OSAL_ERROR == OSAL_s32EventClose( hEventFS1 ) )
	{
		u32Ret += 400;
	}
	/*Delete event*/
	if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME1 ) )
	{
		u32Ret += 1000;
	}
		
	if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
	{
		u32Ret += 2000;
	}
	
	/*Remove the file*/
	if( OSAL_ERROR == OSAL_s32IORemove( strDevName ) )
	{
		u32Ret += 4000;
	}

	/*Include also the thread error code*/
	u32Ret += (u32tr1FS+u32tr2FS);

	/*Return the error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemOpenCloseMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_084
* DESCRIPTION:  Open and close file from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemOpenCloseMultiThread(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;
	
	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = OpenCloseThread(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
					
	return u32Ret;
}

/*****************************************************************************
* FUNCTION	  :	 WriteThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Writes to file using two threads
* HISTORY     :	 Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*******************************************************************************/
tU32  WriteThread(const tU8* strDevName)
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	OSAL_tEventMask EM = 0;

   	/*Thread attributes*/
	OSAL_trThreadAttribute tr1_atr ={OSAL_NULL};
	OSAL_trThreadAttribute tr2_atr ={OSAL_NULL};


	/*Initialize thread return code*/
	u32tr1FS = 0;
	u32tr2FS = 0;

	/*Initialize the event handle*/
	hEventFS = 0;

	/*Create file */
	if( ( hDeviceFS = OSAL_IOCreate( strDevName , OSAL_EN_READWRITE ) )
	    == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	if( !u32Ret )
	{
   		/*Allocate memory dynamically for thread 1*/
		HugeBufferTr1FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr1FS )
		{
			u32Ret += 2;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr1FS,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr1FS,'d',MB_SIZE);
		}
		/*Fill thread Attributes*/
		 tr1_atr.szName 	    = "FSThread1";
		 tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		 tr1_atr.s32StackSize = 4096;
		 tr1_atr.pfEntry      = (FP)WriteThread1FS;
		 tr1_atr.pvArg        = NULL;

		
		/*Allocate memory dynamically for thread 2*/
		HugeBufferTr2FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr2FS )
		{
			u32Ret += 4;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr2FS,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr2FS,'e',MB_SIZE);
		}
		/*Fill thread Attributes*/
		tr2_atr.szName 	    = "FSThread2";
		tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr2_atr.s32StackSize = 4096;
		tr2_atr.pfEntry      = (FP)WriteThread2FS;
		tr2_atr.pvArg        = NULL;

		/*Create the Event field*/
		if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
		{
			u32Ret += 10;
		}
		 
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr1_atr ) )
		 {
		 	u32Ret += 20;
		 }
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr2_atr ) )
		 {
		 	u32Ret += 40;
		 }
		 /*Wait for threads to Post status*/
		 if( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1|FS_THREAD2,
		                 	   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM ) )
		 {
		 	u32Ret += 100;
		 }
		 OSAL_s32EventPost( hEventFS,~EM,OSAL_EN_EVENTMASK_AND );
		 /*Close event*/
		if( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
		{
			u32Ret += 200;
		}
		/*Delete event*/
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			u32Ret += 400;
		}
		
		/*Close the file*/
		if( OSAL_ERROR == OSAL_s32IOClose( hDeviceFS ) )
		{
			u32Ret += 1100;
		}
		/*Remove the fil from FFS2*/
		if( OSAL_ERROR == OSAL_s32IORemove( strDevName ) )
		{
			u32Ret += 1200;
		}

	   	/*Free the dynamically allocated memory*/
		if(HugeBufferTr1FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr1FS );
			HugeBufferTr1FS = OSAL_NULL;
		}
		if(HugeBufferTr2FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr2FS );
			HugeBufferTr2FS = OSAL_NULL;
		}
		
		/*Include also the thread error code*/
		u32Ret += (u32tr1FS+u32tr2FS);
	}				   
	/*Return the error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_085
* DESCRIPTION:  write to file from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteMultiThread(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = WriteThread(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
				
	return u32Ret;
}

/*****************************************************************************
* FUNCTION	  :	 WriteReadThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Write and read to file using two threads
* HISTORY     :	 Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*******************************************************************************/
tU32  WriteReadThread(const tU8* strDevName)
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	OSAL_tEventMask EM = 0;

   	/*Thread attributes*/
	OSAL_trThreadAttribute tr1_atr ={OSAL_NULL};
	OSAL_trThreadAttribute tr2_atr ={OSAL_NULL};


	/*Initialize thread return code*/
	u32tr1FS = 0;
	u32tr2FS = 0;

	/*Initialize the event handle*/
	hEventFS = 0;

	/*Create file */
	if( ( hDeviceFS = OSAL_IOCreate( strDevName , OSAL_EN_READWRITE ) )
	    == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	if( !u32Ret )
	{
   		/*Allocate memory dynamically for thread 1*/
		HugeBufferTr1FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr1FS )
		{
			u32Ret += 2;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr1FS,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr1FS,'d',MB_SIZE);
		}
		/*Fill thread Attributes*/
		 tr1_atr.szName 	    = "FSThread1";
		 tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		 tr1_atr.s32StackSize = 4096;
		 tr1_atr.pfEntry      = (FP)WriteThreadFS;
		 tr1_atr.pvArg        = NULL;

		
		/*Allocate memory dynamically for thread 2*/
		HugeBufferTr2FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr2FS )
		{
			u32Ret += 4;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr2FS,'\0',(MB_SIZE+1) );
//			OSAL_pvMemorySet( HugeBufferTr2FS,'e',MB_SIZE);
		}
		/*Fill thread Attributes*/
		tr2_atr.szName 	    = "FSThread2";
		tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr2_atr.s32StackSize = 4096;
		tr2_atr.pfEntry      = (FP)ReadThreadFS;
		tr2_atr.pvArg        = NULL;

		/*Create the Event field*/
		if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
		{
			u32Ret += 10;
		}
		 
		/*Create the Event field*/
		if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME1 , &hEventFS1 ) )
		{
			u32Ret += 20;
		}
		 
		 
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr1_atr ) )
		 {
		 	u32Ret += 40;
		 }
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr2_atr ) )
		 {
		 	u32Ret += 100;
		 }
		 /*Wait for threads to Post status*/
		 if( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1|FS_THREAD2,
		                 	   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM ) )
		 {
		 	u32Ret += 200;
		 }
		 OSAL_s32EventPost( hEventFS,~EM,OSAL_EN_EVENTMASK_AND );
		 /*Close event*/
		if( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
		{
			u32Ret += 400;
		}
		/*Delete event*/
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			u32Ret += 1000;
		}
		
		/*Close event*/
		if( OSAL_ERROR == OSAL_s32EventClose( hEventFS1 ) )
		{
			u32Ret += 1200;
		}
		/*Delete event*/
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME1 ) )
		{
			u32Ret += 1400;
		}
		
		/*Close the file*/
		if( OSAL_ERROR == OSAL_s32IOClose( hDeviceFS ) )
		{
			u32Ret += 2000;
		}
		/*Remove the fil from FFS2*/
		if( OSAL_ERROR == OSAL_s32IORemove( strDevName ) )
		{
			u32Ret += 2100;
		}

	   	/*Free the dynamically allocated memory*/
		if(HugeBufferTr1FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr1FS );
			HugeBufferTr1FS = OSAL_NULL;
		}
		if(HugeBufferTr2FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr2FS );
			HugeBufferTr2FS = OSAL_NULL;
		}
		
		/*Include also the thread error code*/
		u32Ret += (u32tr1FS+u32tr2FS);
	}				   
	/*Return the error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemWriteReadMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_086
* DESCRIPTION:  write and read to file from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemWriteReadMultiThread(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = WriteReadThread(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}

	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION	  :	 ReadThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Read from file using two threads
* HISTORY     :	 Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*******************************************************************************/
tU32  ReadThread(const tU8* strDevName)
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	OSAL_tEventMask EM = 0;

   	/*Thread attributes*/
	OSAL_trThreadAttribute tr1_atr ={0};
	OSAL_trThreadAttribute tr2_atr ={0};


	/*Initialize thread return code*/
	u32tr1FS = 0;
	u32tr2FS = 0;

	/*Initialize the event handle*/
	hEventFS = 0;

	/*Create file */
	if( ( hDeviceFS = OSAL_IOCreate( strDevName , OSAL_EN_READWRITE ) )
	    == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	if( !u32Ret )
	{
   		/*Allocate memory dynamically for thread 1*/
		HugeBufferTr1FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr1FS )
		{
			u32Ret += 2;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr1FS,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr1FS,'d',MB_SIZE);
		}
		/*Fill thread Attributes*/
		 tr1_atr.szName 	    = "FSThread1";
		 tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		 tr1_atr.s32StackSize = 4096;
		 tr1_atr.pfEntry      = (FP)ReadThread1FS;
		 tr1_atr.pvArg        = NULL;

		
		/*Allocate memory dynamically for thread 2*/
		HugeBufferTr2FS = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr2FS )
		{
			u32Ret += 4;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr2FS,'\0',(MB_SIZE+1) );
//			OSAL_pvMemorySet( HugeBufferTr2FS,'e',MB_SIZE);
		}
		/*Fill thread Attributes*/
		tr2_atr.szName 	    = "FSThread2";
		tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr2_atr.s32StackSize = 4096;
		tr2_atr.pfEntry      = (FP)ReadThread2FS;
		tr2_atr.pvArg        = NULL;

		/*Create the Event field*/
		if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
		{
			u32Ret += 10;
		}
		 
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr1_atr ) )
		 {
		 	u32Ret += 40;
		 }
		 /*Create the first thread*/
		 if( OSAL_ERROR == OSAL_ThreadSpawn( (OSAL_trThreadAttribute*)&tr2_atr ) )
		 {
		 	u32Ret += 100;
		 }
		 /*Wait for threads to Post status*/
		 if( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1|FS_THREAD2,
		                 	   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM ) )
		 {
		 	u32Ret += 200;
		 }
		 OSAL_s32EventPost( hEventFS,~EM,OSAL_EN_EVENTMASK_AND );
		 /*Close event*/
		if( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
		{
			u32Ret += 400;
		}
		/*Delete event*/
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			u32Ret += 1000;
		}
		
		
		/*Close the file*/
		if( OSAL_ERROR == OSAL_s32IOClose( hDeviceFS ) )
		{
			u32Ret += 1100;
		}
		/*Remove the fil from FFS2*/
		if( OSAL_ERROR == OSAL_s32IORemove( strDevName ) )
		{
			u32Ret += 1200;
		}

	   	/*Free the dynamically allocated memory*/
		if(HugeBufferTr1FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr1FS );
			HugeBufferTr1FS = OSAL_NULL;
		}
		if(HugeBufferTr2FS != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr2FS );
			HugeBufferTr2FS = OSAL_NULL;
		}
		
		/*Include also the thread error code*/
		u32Ret += (u32tr1FS+u32tr2FS);
	}				   
	/*Return the error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemReadMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_087
* DESCRIPTION:  Read from file from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemReadMultiThread(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 2 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,"/common_test.txt");
		   	u32Val = ReadThread(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
	
					
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		FSFormatDrive()
* PARAMETER:    device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Format Drive
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 FSFormatDrive(const tU8* strDevName)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tS32 s32Ret = 0; 

	hFd = OSAL_IOOpen(strDevName, enAccess);
	if(hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		s32Ret = OSAL_s32IOControl(hFd, OSAL_C_S32_IOCTRL_FIODISKFORMAT, (tS32)0);
		if( s32Ret == (tS32)OSAL_ERROR)
		{
		  u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
		  u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemFormat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_088
* DESCRIPTION:  Format FS
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFormat(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 0 && count != 3 && count != 4 && count != 5 ) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
		   	u32Val = FSFormatDrive(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}

		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		FSChkDisk()
* PARAMETER:    device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check disk
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
#if 0
tU32 FSChkDisk(const tU8* strDevName)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tS32 s32Ret = 0; 

	hFd = OSAL_IOOpen(strDevName, enAccess);
	if(hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		s32Ret = OSAL_s32IOControl(hFd, OSAL_C_S32_IOCTRL_FIOCHKDSK, (tS32)0);
		if(OSAL_ERROR == (tU32)s32Ret)
		{
		  u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
		  u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32FileSystemCheckDisk()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_089
* DESCRIPTION:  Run Check disk
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemCheckDisk(tVoid)
{	
	tU32 u32Ret = 0;
	tU32 count = 0;
	tU32 u32Val = 0;
	tU32 Device_ID = 0;

	Device_ID = u32OEDT_BoardName();
	Device_ID = Device_ID -1;
	
	namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29);
	if ( namebuffer == NULL)
  	{
	  	u32Ret = 8001;
  	  	return  u32Ret;
  	}
	
	for(count=0;count<6;count++)
	{
		if((Dev_Identity[count+1][Device_ID]) && count != 0 && count != 1 && count != 2 && count != 3) 
		{
			OSAL_pvMemorySet(namebuffer,'\0',29);		
			file_name(count,namebuffer,OSAL_NULL);
		   	u32Val = FSChkDisk(namebuffer);
			if(u32Val)	
				u32Ret = u32Val + Dev_Error[count+1];
		}
	}
	if(namebuffer != NULL)
	{
		OSAL_vMemoryFree (namebuffer);
		namebuffer = NULL;
	}
				
	return u32Ret;
}
#endif

/* EOF */
