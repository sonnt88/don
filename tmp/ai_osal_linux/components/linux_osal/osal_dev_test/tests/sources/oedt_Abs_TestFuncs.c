/******************************************************************************
 *FILE         : oedt_Abs_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the  test cases for the abs 
 *               device.
 *               
 *AUTHOR       : Sai Chowdary Samineni (RBEI/ECF5)
 *
 *COPYRIGHT    : 
 *
 *HISTORY      : 22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
                 12.11.15  (RBEI/ECF5) Shivasharnappa  Mothpalli
                           Removed unused IOCTRL calls(Fix for CFG3-1622).
                           OSAL_C_S32_IOCTRL_ABS_FLUSH,
                           OSAL_C_S32_IOCTRL_ABS_GETCNT.
 *
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"

//#define CHECK_abs
//#define UNDERSTAND
/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define   BLOCK_READ_COUNT                       30
//#define   READ_COUNT_MORE_THAN_BUFFER_LENGTH    520  /*commented to remove lint*/
//#define   WAIT_IN_MS_FOR_NEXT_READ              100  /*commented to remove lint*/
//#define   MAX_LOOP_CNT_FOR_GETCNT               100  /*commented to remove lint*/
//#define   NO_RECORDS_ADD_ILLEGAL                200  /*commented to remove lint*/


/* Macros for Stub test */
//Uncomment below lines until #endif during stub test 

//#ifndef   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS
//#define   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS
//#endif


#ifdef   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS

#define   ABS_DIRE_READ_NO_OF_ENTRIES        20

/*SCC sends the following direction values to IMX

Direction from SCC
00  -- DATA INVALID
01  -- FORWARD direction
10  -- REVERSE direction
11  -- UNKNOWN direction

Handling of direction is done in the following way in IMX

Direction data in IMX
00  -- UNKNOWN direction
01  -- FORWARD direction
10  -- REVERSE direction
11  -- DATA INVALID
*/

#define  OEDT_STUB_ABS_DIRE_MASK        (0x03)
#define  OEDT_STUB_ABS_DIRE_UNKW        (0)
#define  OEDT_STUB_ABS_DIRE_FORW        (1)
#define  OEDT_STUB_ABS_DIRE_REVE        (2)


#endif



/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define    ABS_DEVICE_OPEN_ERROR            35
#define    ABS_IOREAD_ERROR                 36
#define    ABS_GET_VERSION_ERROR            37
#define    ABS_RESET_ERROR                  38
#define    ABS_INIT_ERROR                   39
#define    ABS_FLUSH_ERROR                  40
#define    ABS_GETCNT_ERROR                 41
#define    ABS_GETDIRECTION_ERROR           42
#define    ABS_GETWHEELCOUNTER_ERROR        43
#define    ABS_SETDIRECTION_ERROR           44
#define    ABS_SETTIMEOUT_VALUE_ERROR       45
#define    ABS_GETTIMEOUT_VALUE_ERROR       46
#define    ABS_ACTIVATE_OIC_ERROR           47
#define    ABS_INACTIVATE_OIC_ERROR         48
#define    ABS_GET_RESOUTION_ERROR          49
#define    ABS_CYCLE_TIME_VALUE_MISMATCH_ERROR         50
#define    ABS_CLOSE_ERROR                  51
#define    ABS_IILEGAL_READ_TIMEOUT_ERROR   52
#define    ABS_TIMEOUT_EQUAL_ERROR          53
#define    ABS_NOT_SUPPORTED_ERROR          54
#define    ABS_BLOCK_READ_ERROR             55
#define    ABS_READ_AFTER_FLUSH_ERROR       56
#define    ABS_UNKNOWN_ERROR                57
#define    ABS_GET_SAMPLING_INTERVAL_ERROR  58
#define    ABS_RE_OPEN_ERROR                59
#define    ABS_RE_CLOSE_ERROR               60
#define    ABS_STAT_NOT_NORMAL              61
#define	   ABS_BOTH_STAT_NOT_INVALID         62
#define	   ABS_BOTH_STAT_NOT_INTERNAL       63
#define    ABS_UNEXPECTED_BEHAV             64
#define   ABS_IOCTRL_GET_CYCLE_TIME_ERROR   65

 
#define    ABS_DEFAULT_TIME_INTERVAL         100000000



#ifdef   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS      //To solve lint
#define    ABS_STUB_READ_VALUE_MISMATCH     65
#endif


//#define    ABS_NO_OF_ENTRIES                10   /*commented to remove lint*/

tU8 aAbsErr_oedt[] = {
                       ABS_DEVICE_OPEN_ERROR,            //00
                       ABS_IOREAD_ERROR,                 //01
                       ABS_GET_VERSION_ERROR,            //02
                       ABS_RESET_ERROR,                  //03
                       ABS_INIT_ERROR,                   //04
                       ABS_FLUSH_ERROR,                  //05
                       ABS_GETCNT_ERROR,                 //06
                       ABS_GETDIRECTION_ERROR,           //07
                       ABS_GETWHEELCOUNTER_ERROR,        //08
                       ABS_SETDIRECTION_ERROR,           //09
                       ABS_SETTIMEOUT_VALUE_ERROR,       //10
                       ABS_GETTIMEOUT_VALUE_ERROR,       //11
                       ABS_ACTIVATE_OIC_ERROR,           //12
                       ABS_INACTIVATE_OIC_ERROR,         //13
                       ABS_GET_RESOUTION_ERROR,          //14
                       ABS_CYCLE_TIME_VALUE_MISMATCH_ERROR,         //15
                       ABS_CLOSE_ERROR,                  //16
                       ABS_IILEGAL_READ_TIMEOUT_ERROR,   //17
                       ABS_TIMEOUT_EQUAL_ERROR,          //18
                       ABS_NOT_SUPPORTED_ERROR,          //19
                       ABS_BLOCK_READ_ERROR,             //20
                       ABS_READ_AFTER_FLUSH_ERROR,       //21
                       ABS_UNKNOWN_ERROR,                //22
                       ABS_GET_SAMPLING_INTERVAL_ERROR,  //23
                       ABS_RE_OPEN_ERROR,                //24
                       ABS_RE_CLOSE_ERROR,                //25
                       ABS_STAT_NOT_NORMAL,              //26
                       ABS_BOTH_STAT_NOT_INVALID,        //27
                       ABS_BOTH_STAT_NOT_INTERNAL,       //28
                       ABS_UNEXPECTED_BEHAV,             //29
                     };

//#define    DEFAULT_TIMEOUT_IN_SEC   5            /*commented to remove lint*/
#define    OEDT_ABS_TEST_PASS       0


/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| function declaration (scope: module-local)
|----------------------------------------------------------------*/


/*tVoid vAbsTimerCallBack_oedt(tPVoid pDummyArg);
OSAL_tTimerHandle hAbsStartTimer_oedt();
void AbsStopTimer_oedt(OSAL_tTimerHandle phTimer);*/


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
* FUNCTION    : u32AbsGetCycleTime()
* DESCRIPTION : Reads the cycle time from Abs .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 24.11.15  Shivasharnappa Mothpalli(RBEI/ECF5)
******************************************************************/
tU32 u32AbsGetCycleTime(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;
   tU32 AbsGetCyleTime = 0;

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ABS,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = ABS_DEVICE_OPEN_ERROR;
   }
   else
   {
      if (  OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ABS_GET_SAMPLING_INTERVAL,(tS32)&AbsGetCyleTime) == OSAL_ERROR)
      {
         u32RetVal = ABS_IOCTRL_GET_CYCLE_TIME_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Abs get cycle time failed error no  : %d",OSAL_u32ErrorCode() );
      }
       //PQM_authorized_multi_550
      else if  (AbsGetCyleTime != ABS_DEFAULT_TIME_INTERVAL)//lint !e774
      {
         u32RetVal = ABS_CYCLE_TIME_VALUE_MISMATCH_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Mismatch in Abs cycle time ,AbsGetCyleTime : %d",AbsGetCyleTime );
      }
      else
      { 
         u32RetVal = OEDT_ABS_TEST_PASS;
      }
      
   }

   if ( (hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = ABS_CLOSE_ERROR;
   }

   return u32RetVal;
}


/************************************************************************
 *FUNCTION:    u32AbsOpenDev
 *DESCRIPTION: 1.Opens the device.
               2.Close the device.
 
 *PARAMETER:   Nil
 *
 *RETURNVALUE:   Open and Close ABS Device
 *HISTORY:       22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 u32AbsOpenDev(void)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   OSAL_s32ThreadWait(10);

   if(bErrorOccured == FALSE)
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return  u32RetVal;
}

/************************************************************************
 *FUNCTION:    tu32AbsBasicRead 
 *DESCRIPTION:  1.Opens the device.
                2.Read one record from the device.
                3.Close the device.
 
 *PARAMETER:   Nil
 *
 *RETURNVALUE: Error Number or NULL to indicate that test is passed
 *HISTORY:     22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 tu32AbsBasicRead(void)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   OSAL_trAbsData rAbsData;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   #ifdef CHECK_THREAD_ABS
      OSAL_tThreadID ThreadID;
      ThreadID = OSAL_ThreadWhoAmI();
      vTtfisTrace(OSAL_C_TRACELEVEL1, "Thread No \t: %d",(tS32)ThreadID);
   #endif

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   //Sequential read
   if(FALSE == bErrorOccured )
   {
      OSAL_s32ThreadWait(1000);
      if(OSAL_s32IORead (hDevice,(tS8 *)&rAbsData,sizeof(rAbsData)) == OSAL_ERROR)
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 1;
         OSAL_s32IOClose( hDevice );
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"RR whl cntr %u RL whl cntr %u RR Dir %d RL Dir %d TS: %u errcnt: %u ",
                                                 rAbsData.u32CounterRearRight, rAbsData.u32CounterRearLeft, 
                                                 rAbsData.u8DirectionRearRight, rAbsData.u8DirectionRearLeft,
                                                 rAbsData.u32TimeStamp, rAbsData.u16ErrorCounter );
       }
   }

   if(FALSE == bErrorOccured )
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return u32RetVal;

}

/************************************************************************
 *FUNCTION:    tu32AbsBasicReadSeq 
 *DESCRIPTION: 1.Opens the device.
               2.Reads 30 data records from the device.
               3.Close the device.
 
 *PARAMETER:   Nil
 *
 *RETURNVALUE:   Error Number or NULL to indicate that test is passed
 *HISTORY:       22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 tu32AbsBasicReadSeq(void)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   OSAL_trAbsData rAbsData;
   OSAL_trAbsData arAbsDataBlockRead[BLOCK_READ_COUNT];
   tU32 NoOfRecsForRead = 60;
   tU8 u8Index;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   #ifdef CHECK_THREAD_ABS
      OSAL_tThreadID ThreadID;
      ThreadID = OSAL_ThreadWhoAmI();
      vTtfisTrace(OSAL_C_TRACELEVEL1,"Thread Id: %d",(tS32)ThreadID);
   #endif

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   OSAL_s32ThreadWait(1000);
   
   //Block read
   if(FALSE == bErrorOccured )
   {
      if( OSAL_s32IORead (hDevice,(tS8 *)arAbsDataBlockRead,sizeof(arAbsDataBlockRead)) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 20;
         OSAL_s32IOClose ( hDevice );
      }
   }
   
   
   //Sequential read
   if(FALSE == bErrorOccured )
   {
      for(u8Index = 0;u8Index < NoOfRecsForRead; u8Index++)
      {
         OSAL_s32ThreadWait(200);
         if(OSAL_s32IORead (hDevice,(tS8 *)&rAbsData,sizeof(rAbsData)) == OSAL_ERROR)
         {
            if(OSAL_E_TIMEOUT != OSAL_u32ErrorCode())
            {
               bErrorOccured = TRUE;
               u8ErrorNo = 1;
               OSAL_s32IOClose ( hDevice );
               break;
            }
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"RR whl cntr %u RL whl cntr %u RR Dir %d RL Dir %d TS: %u errcnt: %u ",
                                                   rAbsData.u32CounterRearRight, rAbsData.u32CounterRearLeft, 
                                                   rAbsData.u8DirectionRearRight, rAbsData.u8DirectionRearLeft,
                                                   rAbsData.u32TimeStamp, rAbsData.u16ErrorCounter);
         }
      }
   }
   

   if(FALSE == bErrorOccured )
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return  u32RetVal;

}

/************************************************************************
 *FUNCTION    : tu32AbsInterfaceCheckSeq

 *DESCRIPTION : 1.Opens the device.
                2.Flush the buffer.
                3.Gets the count of No.of records in the buffer
                4.Get the sampling time
                5.Close the device.
 
 *PARAMETER   : Nil
 *
 *RETURNVALUE : Error Number or NULL to indicate that test is passed
 
 *HISTORY     : 22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
                12.11.15  (RBEI/ECF5) Shivasharnappa  Mothpalli
                          Removed unused IOCTRL calls(Fix for CFG3-1622).
                          OSAL_C_S32_IOCTRL_ABS_FLUSH,
                          OSAL_C_S32_IOCTRL_ABS_GETCNT.

 ************************************************************************/
tU32 tu32AbsInterfaceCheckSeq(void)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   tS32 s32GetSampTime =0 ;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   (tVoid)s32GetSampTime;  //to remove lint

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   if( FALSE == bErrorOccured )
   {
      if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ABS_GET_SAMPLING_INTERVAL,(tS32)&s32GetSampTime) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 23;
         OSAL_s32IOClose( hDevice );
      }
   }

   if(FALSE == bErrorOccured )
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION:     tu32AbsReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:     1. Open Device 
*                  2. Attempt to open the ABS device which has already been 
*                     opened (should fail)
*                  3. Close Device

* HISTORY:    22.07.14  Sai Chowdary Samineni (RBEI/ECF5)

*****************************************************************************/
tU32 tu32AbsReOpen(tVoid)    
{
   OSAL_tIODescriptor hFd1;
   OSAL_tIODescriptor hFd2 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   //Open the device
   hFd1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hFd1 == OSAL_ERROR)
   {
     bErrorOccured = TRUE;
     u8ErrorNo = 0;
   }

   if( bErrorOccured == FALSE )
   {
      hFd2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS , enAccess );
      if( OSAL_ERROR != hFd2)
      {
         u8ErrorNo = 24;
         bErrorOccured = TRUE;
         OSAL_s32IOClose( hFd1 );
      }
   }

   if(FALSE == bErrorOccured )
   {
      if ( OSAL_s32IOClose ( hFd1 ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION:     tu32AbsCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ACC_002
* DESCRIPTION:  1. Open Device 
*               2. Close Device    
*               3. Attempt to Close the ABS device which has already been 
*                  closed (should fail)
*
* HISTORY:  22.07.14  Sai Chowdary Samineni (RBEI/ECF5)  
*****************************************************************************/
tU32 tu32AbsCloseAlreadyClosed(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   OSAL_s32ThreadWait(10);

   if(bErrorOccured == FALSE)
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   OSAL_s32ThreadWait(10);

   if(bErrorOccured == FALSE)
   {
      if ( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 25;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return  u32RetVal;
}

/************************************************************************
*FUNCTION:    tu32AbsReadpassingnullbuffer
*DESCRIPTION:   1.Opens the device.
                 2.Reads the data by passing buffer as NULL
                 3.Close the device.  
*PARAMETER:   None
*
*
*RETURNVALUE: Error Number or NULL to indicate that test is passed
*
*HISTORY: 22.07.14  Sai Chowdary Samineni (RBEI/ECF5)  
************************************************************************/
tU32 tu32AbsReadpassingnullbuffer(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   tU32 u32RetVal = 0;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   if(FALSE == bErrorOccured )
   {
      if(OSAL_s32IORead (hDevice,OSAL_NULL,sizeof(OSAL_trAbsData)) != OSAL_ERROR)
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 1;
         OSAL_s32IOClose( hDevice );
      }
   }

   if( FALSE == bErrorOccured )
   {
      if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u32RetVal = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u32RetVal = aAbsErr_oedt[u8ErrorNo];
   }

   return  u32RetVal;
}


/************************************************************************
*FUNCTION:    u32AbsStatusCheck
*DESCRIPTION:   1.Opens the device.
                2.Reads one record and checks the status of the record
                3.Close the device.  
*PARAMETER:   None
*
*
*RETURNVALUE: Error Number in case of failure
              NULL to indicate that test is passed
*
*HISTORY: 17.06.14  Srinivas Prakash Anvekar (RBEI/ECF5)  
************************************************************************/
tU32 u32AbsStatusCheck(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 22;
   OSAL_trAbsData rAbsData;
   
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " ABS Device open failed " );
   }
   
   if( FALSE == bErrorOccured )
   {
      if( OSAL_s32IORead( hDevice, (tS8 *)&rAbsData, sizeof(rAbsData) ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 1;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " ABS Read failed %u : ", OSAL_u32ErrorCode() );
      }
      else
      {
         if( rAbsData.u16ErrorCounter == 0 )
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Test PASS with expected behaviour "
            " with status as normal");
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " DRR : %u, "
            "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
            rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
            rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
         }
         else
         {
            switch( rAbsData.u8StatusRearRight )
            {
               case OSAL_C_U8_ABS_STATUS_NORMAL:
                  {
                     //This case must never happen.
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Test FAIL as status cannot be normal"
                     " when error counter is other than zero ");
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " DRR : %u, "
                     "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
                     rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
                     rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
                     u8ErrorNo = 26;
                     bErrorOccured = TRUE;
                  }
               break;
               
               case OSAL_C_U8_ABS_STATUS_DATA_INVALID:
                  {
                     if( rAbsData.u8StatusRearLeft == OSAL_C_U8_ABS_STATUS_DATA_INVALID )
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Test PASS with expected behaviour"
                        " with status as invalid");
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " DRR : %u, "
                        "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
                        rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
                        rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
                     }
                     else
                     {
                        //This case must never happen    
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Test FAIL, both the status of wheels"
                        "must be set to invalid ");
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " DRR : %u, "
                        "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
                        rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
                        rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
                        u8ErrorNo = 27;
                        bErrorOccured = TRUE;
                     }
                  }
               break;
               
               case OSAL_C_U8_ABS_STATUS_INTERNAL_ERROR:
                  {
                     if( rAbsData.u8StatusRearLeft == OSAL_C_U8_ABS_STATUS_INTERNAL_ERROR )
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "Test PASS with expected behaviour "
                        " with status as internal error");
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " DRR : %u, "
                        "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
                        rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
                        rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
                     }
                     else
                     {
                        //This case must never happen    
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Test FAIL, both the status of wheels"
                        " must be set to internal error in case of timeout event");
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " DRR : %u, "
                        "DRL : %u, SRR %u, SRL %u, ERRCNT : %u", rAbsData.u8DirectionRearRight,
                        rAbsData.u8DirectionRearLeft, rAbsData.u8StatusRearRight,
                        rAbsData.u8StatusRearLeft, rAbsData.u16ErrorCounter );
                        u8ErrorNo = 28;
                        bErrorOccured = TRUE;
                     }
                  }
               break;
               
               default:
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Test Failed due to unexpected behaviour ");
                     u8ErrorNo = 29;
                     bErrorOccured = TRUE;
                  };
            }
            
         }
      }
   }
   
   
   if( hDevice != OSAL_NULL )
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 16;
      }
   }

   if( FALSE == bErrorOccured )
   {
      u8ErrorNo = OEDT_ABS_TEST_PASS;
   }
   else
   {
      u8ErrorNo = aAbsErr_oedt[u8ErrorNo];
   }

   return  u8ErrorNo;
}

/************************************************************************
 *FUNCTION:    tu32AbsStubTestReadDirection 
 *DESCRIPTION:    1.Opens the device.
                           2.Reads ABS_DIRE_READ_NO_OF_ENTRIES  data records from the device.
                           3.Closes the device.
 
 *PARAMETER:   Nil
 *
 *RETURNVALUE:   Error Number on failure
 *                        PASS - on successful execution of OEDT
 *HISTORY: 
 *
 * ************************************************************************/
tU32 tu32AbsStubTestReadDirection(void)
{
   tU32 u32RetVal = OEDT_ABS_TEST_PASS;

#ifdef   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   OSAL_trAbsData rAbsData;
   tU8 u8NoOfRecsForRead = ABS_DIRE_READ_NO_OF_ENTRIES; 
   tU8 u8Index;
   /* the array expected_dir[] should be same as one given in the stub sensor_proxy_chk_for_abs_direction.c */
   tU8 expected_dir[ABS_DIRE_READ_NO_OF_ENTRIES] = {
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_UNKW,
                                                      OEDT_STUB_ABS_DIRE_REVE,
                                                      OEDT_STUB_ABS_DIRE_FORW,
                                                      OEDT_STUB_ABS_DIRE_UNKW
                                                   };


#ifdef CHECK_THREAD_ABS
      OSAL_tThreadID ThreadID;
      ThreadID = OSAL_ThreadWhoAmI();
      vTtfisTrace(OSAL_C_TRACELEVEL1, "Thread No \t: %d",(tS32)ThreadID);
#endif

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ABS, OSAL_EN_READWRITE );
   if(hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u32RetVal = ABS_DEVICE_OPEN_ERROR;
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
      "OSAL_s32IOOpen Failed for ABS device");
   }

   //Sequential read
   if(FALSE == bErrorOccured )
   {
      for(u8Index = 0; u8Index < u8NoOfRecsForRead; u8Index++)
      {
         OSAL_s32ThreadWait(1000);
         if(OSAL_s32IORead (hDevice,(tS8 *)&rAbsData,sizeof(rAbsData)) == OSAL_ERROR)
         {
            bErrorOccured = TRUE;
            u32RetVal = ABS_IOREAD_ERROR;
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
            "OSAL_s32IORead Failed for ABS. Closing the device now");
            OSAL_s32IOClose( hDevice );
         }
         else
         {
            if((expected_dir[u8Index] == (rAbsData.u8DirectionRearLeft & OEDT_STUB_ABS_DIRE_MASK)) && 
               (expected_dir[u8Index] == (rAbsData.u8DirectionRearRight & OEDT_STUB_ABS_DIRE_MASK)))
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
               "Direction received from stub matches the expected direction");
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"RR whl cntr %u RL whl cntr %u RR Dir %d RL Dir %d TS: %u errcnt: %u ",
                                                    rAbsData.u32CounterRearRight, rAbsData.u32CounterRearLeft, 
                                                    rAbsData.u8DirectionRearRight, rAbsData.u8DirectionRearLeft,
                                                    rAbsData.u32TimeStamp, rAbsData.u16ErrorCounter );
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
               "Direction received from stub does NOT match the expected direction");
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"RR whl cntr %u RL whl cntr %u RR Dir %d RL Dir %d TS: %u errcnt: %u ",
                                                    rAbsData.u32CounterRearRight, rAbsData.u32CounterRearLeft, 
                                                    rAbsData.u8DirectionRearRight, rAbsData.u8DirectionRearLeft,
                                                    rAbsData.u32TimeStamp, rAbsData.u16ErrorCounter );
               u32RetVal = ABS_STUB_READ_VALUE_MISMATCH;
               break;
            }
          }
      }
   }

   if(FALSE == bErrorOccured )
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u32RetVal = ABS_CLOSE_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
         "OSAL_s32IOClose Failed for ABS device");
      }
   }

   if(( u32RetVal == OEDT_ABS_TEST_PASS ) && (FALSE == bErrorOccured))  // (FALSE == bErrorOccured) is added to solve lint
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST FAILED with err No : %d", u32RetVal);
   }
#endif

#ifndef   SENSOR_PROXY_TEST_STUB_ACTIVE_ABS
   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"OEDT is valid only when stub is active ");
#endif

   return u32RetVal;
   
}


//EOF
