/******************************************************************************
 * FILE         : oedt_Timer_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the TIMER 
 *                device for GM-GE hardware.
 *              
 * AUTHOR(s)    :  Shilpa Bhat (RBIN/EDI3)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          | 		     		           | Author & comments
 * --.--.--      | Initial revision            | ------------
 * 26 Sept,2007  | version 0.1                 | Shilpa Bhat(RBIN/EDI3)
 *------------------------------------------------------------------------------
 * 27 Sept,2007 |  version 0.2                 | Shilpa Bhat(RBIN/EDI3)
 *   		    |                              | Updated the names of functions 
 *              |                              | as per fixed standard and  
 *              |                              | review comments received
 *_____________________________________________________________________________
 *April 8,2008  |  version 0.3                 | Tinoy Mathews( RBIN/ECM1 )
 *				|							   | Contributed test cases for 
 *				|							   | Timer Channels 0,1,2 and 3
 *				|							   |
 *				|							   |
 * _____________|______________________________|_______________________________
 *April 13,2009  |  version 0.4                 | Jeryn Mathew( RBIN/ECF1 )
 *				|							   | Removed Lint and Compiler
 *				|							   | Warnings. 
 *				|							   |
 *				|							   |
 * _____________|______________________________|_______________________________
*******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_Timer_TestFuncs.h"

#include "oedt_local_dtt.h"


/*Global Variables*/
tChar aCharTimerChannel[NO_OF_TIMER_CHANNELS][PATH_LENGTH] = {
																OSAL_C_STRING_DEVICE_TIMER0,
																OSAL_C_STRING_DEVICE_TIMER1,
																OSAL_C_STRING_DEVICE_TIMER2,
																OSAL_C_STRING_DEVICE_TIMER3
															 };
/*Global Variables*/

#if TIMER_FAN_SUPPORT
/*****************************************************************************
* FUNCTION:		u32TimerDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_001
* DESCRIPTION:  Open and Close the TIMER device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerDevOpenClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;

	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
    else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerDevOpenCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_002
* DESCRIPTION:  Attempt to Open and Close the TIMER device with invalid parameter
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/
tU32 u32TimerDevOpenCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_C_STRING_INVALID_ACCESS_MODE;

 	/* Open the device in readonly mode with invalid device name */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR != hDevice )
    {
		u32Ret = 1;
    	/* If successfully opened, indicate error and close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
	}

	/* Close device with invalid parameter*/
   	hDevice = TIMER_INVAL_PARAM;
	if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
   	{
   		u32Ret += 4;
   	}
   	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerDevCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_004
* DESCRIPTION:  Attempt to Close the TIMER device which has already been closed
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007
******************************************************************************/
tU32 u32TimerDevCloseAlreadyClosed(tVoid)	
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;

 	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
    else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
			//break;
		}

		/* Close the device which is already closed, if successful indicate error*/
		if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}

	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerDevOpenCloseDiffModes() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_005
* DESCRIPTION:  Open and Close the TIMER device in different modes
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007
******************************************************************************/	
tU32 u32TimerDevOpenCloseDiffModes(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;

 	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
    else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}
	}

	enAccess = OSAL_EN_WRITEONLY;

 	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
				break;
		}    
	}
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 50;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerDevOpenCloseInvalAccessMode() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_006
* DESCRIPTION:  Attempt to Open and Close the TIMER device with invalid access
				mode
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/	
tU32 u32TimerDevOpenCloseInvalAccessMode(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_C_STRING_INVALID_ACCESS_MODE;

 	/* Open the device in invalid mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR != hDevice )
    {
		u32Ret = 1; 
    	/* If successfully opened, indicate an error and close the device */  
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
    }
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerGetVersion() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_007
* DESCRIPTION:  Get device version
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007
******************************************************************************/
tU32 u32TimerGetVersion(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else 
	{
		/* Get device version */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerGetVersionInvalParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_008
* DESCRIPTION:  Attempt to get device version with invalid parameters
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/
tU32 u32TimerGetVersionInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
		
	/* Get device version */	
	if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,OSAL_C_S32_IOCTRL_VERSION,
										 TIMER_INVAL_PARAM))

	{
		u32Ret = 1;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerSetPWMMinRange() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_009
* DESCRIPTION:  Set PWM duty cycle with minimum permissible value
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerSetPWMMinRange(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = PWM_DUTYCYCLE_MINVAL;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Set PWM duty cycle with value 0 */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
				OSAL_C_S32_IOCTRL_TIMER_SET_PWM,u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerSetPWMMidRange() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_010
* DESCRIPTION:  Set PWM with mid-range of permissible duty cycle value
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerSetPWMMidRange(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = PWM_DUTYCYCLE_MIDVAL;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Set PWM duty cycle with value 50 */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
			OSAL_C_S32_IOCTRL_TIMER_SET_PWM,u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerSetPWMMaxRange()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_011
* DESCRIPTION:  Set PWM duty cycle with maximum permissible value
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerSetPWMMaxRange(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = PWM_DUTYCYCLE_MAXVAL;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Set PWM duty cycle with value 100 */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_TIMER_SET_PWM,u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerSetPWMInval()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_012
* DESCRIPTION:  Set PWM duty cycle with invalid value
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerSetPWMInval(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = PWM_DUTYCYCLE_INVAL;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Set PWM duty cycle with value 101 */	
		if (OSAL_ERROR != OSAL_s32IOControl (hDevice,
				OSAL_C_S32_IOCTRL_TIMER_SET_PWM,u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerGetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_013
* DESCRIPTION:  Get PWM duty cycle 
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007 
******************************************************************************/
tU32 u32TimerGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Get PWM duty cycle value */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
				OSAL_C_S32_IOCTRL_TIMER_GET_PWM,(tS32)&u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerGetPWMInvalParam()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_014
* DESCRIPTION:  Get PWM duty cycle with invalid parameters 
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/
tU32 u32TimerGetPWMInvalParam(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* Get PWM duty cycle value */	
		if (OSAL_ERROR != OSAL_s32IOControl (hDevice,
				OSAL_C_S32_IOCTRL_TIMER_GET_PWM, TIMER_INVAL_PARAM))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerSetGetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_015
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
* 				is correctly set
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/
tU32 u32TimerSetGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	tU32 u32DutyCycleToSet = PWM_DUTYCYCLE_MIDVAL;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* First, get PWM duty cycle value set */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
						OSAL_C_S32_IOCTRL_TIMER_GET_PWM,(tS32)&u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR == OSAL_s32IOClose(hDevice))
		{
			u32Ret += 9;
		}
	}

	enAccess = OSAL_EN_WRITEONLY;
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
				break;
		}
    }
    else
	{
	    /* Set PWM duty cycle with value 50 */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_TIMER_SET_PWM,u32DutyCycleToSet))
		{
			u32Ret += 50;
		}

		/* Close the device */
		if(OSAL_ERROR == OSAL_s32IOClose(hDevice))
		{
			u32Ret += 90;
		}
	}

	/* provide a delay */
	OSAL_s32ThreadWait(500);

	enAccess = OSAL_EN_READONLY;
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 100;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 200;
				break;
			default:
				u32Ret += 300;
				break;
		}
    }
	else
	{
		/* Now, read PWM duty cycle value again */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_TIMER_GET_PWM,(tS32)&u32DutyCycle))
		{
			u32Ret += 500;
		}

		/* Close the device */
		if(OSAL_ERROR == OSAL_s32IOClose(hDevice))
		{
			u32Ret += 900;
		}
	}
	/* Compare the read value with the predetermined value and verify if they
			 are the same. If unsuccessful, indicate error */
	if(u32DutyCycleToSet != u32DutyCycle)
	{
		u32Ret +=1000;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TimerInvalFuncParamToIOCtrl()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_016
* DESCRIPTION:  Attempt to perform IOCtrl opertion with invalid parameter
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 26 Sept,2007
*				Updated by Shilpa Bhat (RBIN/EDI3) on 27 Sept,2007 
******************************************************************************/
tU32 u32TimerInvalFuncParamToIOCtrl(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TIMER_FAN , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
				break;
		}
    }
	else
	{
		/* IOCtrl operation with invalid parameter */	
		if (OSAL_ERROR == OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_INVAL_FUNC,(tS32)&u32DutyCycle))
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/************** End of Test Code developed by RBIN *****************/  //   @Shilpa


/********** Existing Timer Test Code in osal_dev_test ***************/

#if 0 /*Compilation problems in user mode because of LLD call*/
tU32 u32TestTimer1( tVoid )
{
   tS32 s32Ret, s32Error;
   OSAL_tIODescriptor hDevice;

   LLD_timer_vStringTrace((tU32) TR_LEVEL_FATAL, "oedt test 1");
   
   /*
   // LLD timer hw init 
   RetStatus = (tS32) lld_timer_hwinit(0, 0);
   if (RetStatus != NU_SUCCESS)
   {
      return 1;
   }

   // drv timer create 
   RetStatus = (tS32) drv_timer_create(0, 0);
   if (RetStatus != OSAL_OK)
   {
      return 2;
   }
   */

   // drv_timer open --> calls lld_timer also
   hDevice = OSAL_IOOpen( "/dev/timer/fan", OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      s32Error = (tS32) OSAL_u32ErrorCode();
      switch (s32Error)
      {
      case OSAL_E_ALREADYEXISTS:
         return 3;
      case OSAL_E_UNKNOWN:
         return 4;
      case OSAL_E_CANCELED:
         return 5;
      default:
         return 6;
      }
   }

   /*drv_timer close --> calls lld_timer close */
   s32Ret = OSAL_s32IOClose( hDevice );
   if( s32Ret == OSAL_ERROR )
   {
      return 100;
   }

   /* Test passed */
   return 0;
}
#endif

#if 0			//code commented as few macro definitions are missing and hence code 
				//not compiling --> @Shilpa

/* complete functionality check */
tU32 u32TestTimer2( tVoid )
{
   tS32 s32Ret, s32Error, s32GetPWM = 0;
   OSAL_tIODescriptor hDevice;

   /* LLD timer hw init successful? */
   if (bCSLInitSuccess == LLD_TIMER_C_FALSE)
   {
      return 1;
   }

   /* drv timer create */
   if(TIMER_bCreateDone == TIMER_C_FALSE)
   {
      return 2;
   }

   /* drv_timer open --> calls lld_timer */
   hDevice = OSAL_IOOpen( "/dev/timer/fan", OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      s32Error = (tS32) OSAL_u32ErrorCode();
      switch (s32Error)
      {
      case OSAL_E_ALREADYEXISTS:
         return 3;
      case OSAL_E_UNKNOWN:
         return 4;
      case OSAL_E_CANCELED:
         return 5;
      default:
         return 6;
      }
   }

   /* todo: enable GPIO? */
   

   /* todo: set and get pwm values */
   /* start with init: 0% DTC means full speed */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 0);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 0);
   if(s32Ret == OSAL_ERROR)
   {
      return 7;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 8;
   }
   else if(s32GetPWM != 0)
   {
      return 9;
   }
   /* 50% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 50);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 50);
   if(s32Ret == OSAL_ERROR)
   {
      return 10;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 11;
   }
   else if(s32GetPWM != 50)
   {
      return 12;
   }
   
   
   /* check also invalid values --> should return error */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 101);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 101);
   if(s32Ret == OSAL_OK)
   {
      /* must return error because value out of range */
      return 13;
   }
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, -1);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, -1);
   if(s32Ret == OSAL_OK)
   {
      /* must return error because value out of range */
      return 14;
   }
   
   
   /*drv_timer close --> calls lld_timer close */
   s32Ret = OSAL_s32IOClose( hDevice );
   if( s32Ret == OSAL_ERROR )
   {
      return 100;
   }

   /* Test passed */
   return 0;
}


/* complete functionality check */
tU32 u32TestTimer3( tVoid )
{
   tS32 s32Ret, s32Error, s32GetPWM = 0;
   OSAL_tIODescriptor hDevice, hFAN_PWR_Device;
   OSAL_trGPIOData      data;

   /* LLD timer hw init successful? */
   if (bCSLInitSuccess == LLD_TIMER_C_FALSE)
   {
      return 1;
   }

   /* drv timer create */
   if(TIMER_bCreateDone == TIMER_C_FALSE)
   {
      return 2;
   }

   /* drv_timer open --> calls lld_timer */
   hDevice = OSAL_IOOpen( "/dev/timer/fan", OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      s32Error = (tS32) OSAL_u32ErrorCode();
      switch (s32Error)
      {
      case OSAL_E_ALREADYEXISTS:
         return 3;
      case OSAL_E_UNKNOWN:
         return 4;
      case OSAL_E_CANCELED:
         return 5;
      default:
         return 6;
      }
   }

   /* enable GPIO? */
    hFAN_PWR_Device = OSAL_IOOpen("/dev/gpio", OSAL_EN_READWRITE ); 
    if (hFAN_PWR_Device == OSAL_ERROR)
    {
      s32Error = (tS32) OSAL_u32ErrorCode();
    }
    else
    {
        //configure FAN Pin as output
        data.tId = (tS32) DTT_GPIO_FAN_ON;
        data.unData.bState = TRUE; 
        if (OSAL_s32IOControl(hFAN_PWR_Device, OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,  (tS32) &data.tId)==OSAL_ERROR)
        {
            NORMAL_M_ASSERT_ALWAYS();
        }
        if (OSAL_s32IOControl(hFAN_PWR_Device, OSAL_C_32_IOCTRL_GPIO_SET_STATE,  (tS32) &data)==OSAL_ERROR)
        {
         
         NORMAL_M_ASSERT_ALWAYS();
        }

    }


   /* todo: set and get pwm values */
   /* start with init: 0% DTC means full speed */
   
    s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 0);//TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 0);
   if(s32Ret == OSAL_ERROR)
   {
      return 7;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 8;
   }
   else if(s32GetPWM != 0)
   {
      return 9;
   }
   /* wait 1 sec (needed for warm up) */
   OSAL_s32ThreadWait(1000);
   
   /* 10% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 10);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 10);
   if(s32Ret == OSAL_ERROR)
   {
      return 10;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 11;
   }
   else if(s32GetPWM != 10)
   {
      return 12;
   }

   /* 20% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 20);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 20);
   if(s32Ret == OSAL_ERROR)
   {
      return 20;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 21;
   }
   else if(s32GetPWM != 20)
   {
      return 22;
   }
   /* wait 0,5 sec  */
   OSAL_s32ThreadWait(500);
   
   /* 30% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 30);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 30);
   if(s32Ret == OSAL_ERROR)
   {
      return 30;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 31;
   }
   else if(s32GetPWM != 30)
   {
      return 32;
   }
   
   /* wait 0,5 sec  */
   OSAL_s32ThreadWait(500);
   
   /* 40% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 40);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 40);
   if(s32Ret == OSAL_ERROR)
   {
      return 40;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 41;
   }
   else if(s32GetPWM != 40)
   {
      return 42;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);

   /* 50% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 50);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 50);
   if(s32Ret == OSAL_ERROR)
   {
      return 50;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 51;
   }
   else if(s32GetPWM != 50)
   {
      return 52;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);
  
   /* 60% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 60);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 60);
   if(s32Ret == OSAL_ERROR)
   {
      return 60;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 61;
   }
   else if(s32GetPWM != 60)
   {
      return 62;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);
   
   /* 70% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 70);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 70);
   if(s32Ret == OSAL_ERROR)
   {
      return 70;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 71;
   }
   else if(s32GetPWM != 70)
   {
      return 72;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);

   /* 80% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 80);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 80);
   if(s32Ret == OSAL_ERROR)
   {
      return 60;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 81;
   }
   else if(s32GetPWM != 80)
   {
      return 82;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);
   
   /* 90% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 90);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 90);
   if(s32Ret == OSAL_ERROR)
   {
      return 90;
   }
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 91;
   }
   else if(s32GetPWM != 90)
   {
      return 92;
   }
   /* wait 0,5 sec () */
   OSAL_s32ThreadWait(500);

   /* 100% DTC */
   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 100);
   //s32Ret = TIMER_s32IOControl(OSAL_C_S32_IOCTRL_TIMER_SET_PWM, 100);
   if(s32Ret == OSAL_ERROR)
   {
      return 100;
   }

   s32Ret = OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_TIMER_GET_PWM, (tS32) &s32GetPWM);
   if(s32Ret == OSAL_ERROR)
   {
      return 101;
   }
   else if(s32GetPWM != 100)
   {
      return 102;
   }


   //disable FAN
   data.unData.bState = FALSE; 
   if (OSAL_s32IOControl(hFAN_PWR_Device, OSAL_C_32_IOCTRL_GPIO_SET_STATE,  (tS32) &data)==OSAL_ERROR)
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   /*drv_gpio close  */
   s32Ret = OSAL_s32IOClose( hFAN_PWR_Device );
   if( s32Ret == OSAL_ERROR )
   {
      return 201;
   }
   /*drv_timer close --> calls lld_timer close */
   s32Ret = OSAL_s32IOClose( hDevice );
   if( s32Ret == OSAL_ERROR )
   {
      return 200;
   }

   /* Test passed */
   return 0;
}
#endif

#endif /*TIMER_FAN_SUPPORT*/

/*****************************************************************************
* FUNCTION:		s32CalcDutyCycRand()  
* PARAMETER:    None
* RETURNVALUE:  tS32 ( Duty Cycle )
* DESCRIPTION:  Geenrates Duty Cycle Randomly
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tS32 s32CalcDutyCycRand( tVoid )
{
	/*Declarations*/
	OSAL_tMSecond elapsedTime = 0;
	tS32 s32RandDC 			  = 0x7FFFFFFF;

	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Until Positive Number from 1 - 100*/
	do
	{
		/*Random Number Generate*/
		s32RandDC = OSAL_s32Random( )%101;
	}while( s32RandDC <= 0 );

	return s32RandDC;
}


/*****************************************************************************
* FUNCTION:		u32GenericTimerOpenClose()  
* PARAMETER:    Channel Number( tU8 ),Access Mode
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer open close function for valid and invalid access
                modes
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerOpenClose( tU8 u8Channel,OSAL_tenAccess enAccess )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	OSAL_tIODescriptor hTimer   	= 0;
	
	/*Read only or a Writeonly access are the only valid access
	modes as per drv_timer.c*/
	if( ( OSAL_EN_READONLY == enAccess ) || ( OSAL_EN_WRITEONLY == enAccess ) )
	{
		/*For an open close sequence, expectation is a success*/
		if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
		{
			/*Close the Timer*/
			if( OSAL_ERROR == OSAL_s32IOClose( hTimer ) )
			{
				u32Ret += 1;
			}
		}
		else
		{
			u32Ret += 5;
		}
	}
	else 
	{
		/*For an open, expectation is a failure,if open succeeds
		then update error code and attempt a close*/
		if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
		{
			/*Update error code*/
			u32Ret += 10;
			/*Attempt to Close the Timer*/
			OSAL_s32IOClose( hTimer );	
		}
	}			
				
	/*Return Error code to caller*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GenericTimerReOpen()  
* PARAMETER:    Channel Number( tU8 ),Access Mode( Either
				OSAL_EN_WRITEONLY or OSAL_EN_READONLY )
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer ReOpen Function
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerReOpen( tU8 u8Channel,OSAL_tenAccess enAccess )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	OSAL_tIODescriptor hTimer   	= 0;
	OSAL_tIODescriptor hTimer_   	= 0;
					
	if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
	{
		if( OSAL_ERROR != ( hTimer_ = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
		{
			/*ReOpen Should not happen*/
			u32Ret += 10;
			/*Attempt to close the Timer*/
			OSAL_s32IOClose( hTimer_ );
		}

		/*Close an already open Timer*/
		if( OSAL_ERROR == OSAL_s32IOClose( hTimer ) )
		{
			/*Timer could not be closed*/
			u32Ret += 20;
		}
	}
	else
	{
		/*Error due to other reasons..Check access mode for example*/
		u32Ret +=  30;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GenericTimerReClose()  
* PARAMETER:    Channel Number( tU8 ),Access Mode( Either
				OSAL_EN_WRITEONLY or OSAL_EN_READONLY )
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer ReOpen Function
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerReClose( tU8 u8Channel,OSAL_tenAccess enAccess )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	OSAL_tIODescriptor hTimer   	= 0;

 	if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
	{
		/*Close an already open Timer*/
		if( OSAL_ERROR != OSAL_s32IOClose( hTimer ) )
		{
			if( OSAL_ERROR != OSAL_s32IOClose( hTimer ) )
			{
				/*If a second close is successful, then flag error*/
				u32Ret += 1;
			}
		}
		else
		{
			/*Timer could not be closed*/
			u32Ret += 5;	
		}
	}
	else
	{
		/*Open failed due to some reason*/
		u32Ret += 10;
	}
	   
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GenericTimerVersionQuery()  
* PARAMETER:    Channel Number( tU8 ),Access Mode( Either
				OSAL_EN_WRITEONLY or OSAL_EN_READONLY ),Address Parameter
				for control fucntion.
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer Version Query
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerVersionQuery( tU8 u8Channel,OSAL_tenAccess enAccess,tS32* pVersion )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	OSAL_tIODescriptor hTimer   	= 0;

	if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
	{
		if( pVersion )
		{
			if( OSAL_ERROR == OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_VERSION,( tS32 )pVersion ) )
			{
				u32Ret += 1;
			}
		}
		else
		{
			if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_VERSION,( tS32 )pVersion ) )
			{
				u32Ret += 3;
			}
		}
		
		/*Close the Timer*/
		if( OSAL_ERROR == OSAL_s32IOClose( hTimer )	)
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}
									   
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GenericTimerGetPWM()  
* PARAMETER:    Channel Number( tU8 ),Access Mode( Either
				OSAL_EN_WRITEONLY or OSAL_EN_READONLY ),Address Parameter
				for control fucntion.
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer PWM Query( Retrieving the Duty cycle )
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerGetPWM( tU8 u8Channel,OSAL_tenAccess enAccess,tS32* pDutyCycle )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	OSAL_tIODescriptor hTimer   	= 0;

	if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
	{
		if( pDutyCycle )
		{
			/*Duty Cycle Address not NULL*/
			if( OSAL_ERROR == OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_GET_PWM,( tS32 )pDutyCycle ) )
			{
				/*Duty cycle query failed*/
				u32Ret += 5;
			}
		}
		else
		{
			/*Duty cycle has OSAL_NULL*/
			if( OSAL_ERROR !=  OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_GET_PWM,( tS32 )pDutyCycle ) )
			{
				/*duty cycle query should not pass*/
				u32Ret += 7;
			}
		}

		/*Close the Timer*/
		if( OSAL_ERROR == OSAL_s32IOClose( hTimer ) )
		{
			/*Close failed*/
			u32Ret += 10;
		}
	}
	else
	{
		/*Open on Timer failed*/
		u32Ret += 15;
	}
					

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GenericTimerSetPWM()  
* PARAMETER:    Channel Number( tU8 ),Access Mode( Either
				OSAL_EN_WRITEONLY or OSAL_EN_READONLY )
* RETURNVALUE:  tU32.( Non Zero in case of error ,Zero if success ) 
* DESCRIPTION:  Generic Timer Set PWM ( Set the Duty cycle )
* HISTORY:		Created by Tinoy Mathews( RBEI/ECM1 ) April 8, 2008
******************************************************************************/
tU32 u32GenericTimerSetPWM( tU8 u8Channel,OSAL_tenAccess enAccess )
{
	/*Declarations*/
	tU32 u32Ret 					= 0;
	tS32 s32DutyCycle               = 0;
	tS32 s32TempDutyCycle           = -1;
	tS32 s32RandDutyCycle           = -1;
	OSAL_tIODescriptor hTimer   	= 0;

	/*Select Randomly a Percent of DutyCycle*/
	s32RandDutyCycle = s32CalcDutyCycRand( );

	if( OSAL_ERROR != ( hTimer = OSAL_IOOpen( aCharTimerChannel[u8Channel],enAccess ) ) )
	{
		/*Get the Current Duty Cycle allocated to Timer Channel 0*/
		if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_GET_PWM,( tS32 )&s32DutyCycle ) )
		{
			/*Set Duty Cycle to 0%*/
			if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_SET_PWM,PWM_DUTYCYCLE_MINVAL ) )
			{
				/*Verify with a Get PWM*/
				if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_GET_PWM,( tS32 )&s32TempDutyCycle ) )
				{
					if( PWM_DUTYCYCLE_MINVAL != s32TempDutyCycle )
					{
						u32Ret += 100;
					}
				}
				else
				{
					u32Ret += 150;
				}
			}
			else
			{
				u32Ret += 200;
			}

			/*Set Duty Cycle to s32RandDutyCycle%*/
			if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_SET_PWM,s32RandDutyCycle ) )
			{
				/*Verify with a Get PWM*/
				if( OSAL_ERROR != OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_GET_PWM,( tS32 )&s32TempDutyCycle ) )
				{
					if( s32RandDutyCycle != s32TempDutyCycle )
					{
						u32Ret += 300;
					}
				}
				else
				{
					u32Ret += 350;
				}
			}
			else
			{
				u32Ret += 400;
			}
																    
			/*Restore the Original Duty Cycle*/
			if( OSAL_ERROR == OSAL_s32IOControl( hTimer,OSAL_C_S32_IOCTRL_TIMER_SET_PWM,s32DutyCycle ) )
			{
				u32Ret += 250;
			}
		}
		else
		{
			u32Ret += 300;
		}
						
		/*Close the Timer*/
		if( OSAL_ERROR == OSAL_s32IOClose( hTimer ) )
		{
			u32Ret += 10;
		}
	}
	else
	{
		u32Ret += 15;
	}
									
					

	/*Return error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32OpenCloseValidTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_001
* DESCRIPTION:  Open Close on Timer 0 with Valid parameters
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseValidTimer0( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 0 in READONLY mode*/
   //Return of 0 is success. Non zero return value is failure
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER0,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	

	/*Issue Open Close on Timer 0 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER0,OSAL_EN_WRITEONLY ) ) != 0  )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseValidTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_002
* DESCRIPTION:  Open Close on Timer 1 with Valid parameters
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseValidTimer1( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 1 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER1,OSAL_EN_READONLY ) ) != 0  )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 1 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER1,OSAL_EN_WRITEONLY ) ) != 0  )
	{
	   /*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseValidTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_003
* DESCRIPTION:  Open Close on Timer 2 with Valid parameters
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseValidTimer2( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 2 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER2,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 2 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER2,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseValidTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_004
* DESCRIPTION:  Open Close on Timer 3 with Valid parameters
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseValidTimer3( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER3,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER3,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseInvalAccessTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_005
* DESCRIPTION:  Try Open Close on Timer 0 with invalid access mode
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseInvalAccessTimer0( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 0 in READWRITE mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER0,OSAL_EN_READWRITE ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseInvalAccessTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_006
* DESCRIPTION:  Try Open Close on Timer 1 with invalid access mode
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseInvalAccessTimer1( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 1 in READWRITE mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER1,OSAL_EN_READWRITE ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseInvalAccessTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_007
* DESCRIPTION:  Try Open Close on Timer 2 with invalid access mode
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseInvalAccessTimer2( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 2 in READWRITE mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER2,OSAL_EN_READWRITE ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32OpenCloseInvalAccessTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_008
* DESCRIPTION:  Try Open Close on Timer 3 with invalid access mode
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32OpenCloseInvalAccessTimer3( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READWRITE mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerOpenClose( TIMER3,OSAL_EN_READWRITE ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReOpenTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_009
* DESCRIPTION:  Try to ReOpen Timer 0 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReOpenTimer0( tVoid )
{
  	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER0,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER0,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReOpenTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_010
* DESCRIPTION:  Try to ReOpen Timer 1 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReOpenTimer1( tVoid )
{
  	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER1,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER1,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReOpenTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_011
* DESCRIPTION:  Try to ReOpen Timer 2 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReOpenTimer2( tVoid )
{
  	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER2,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER2,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReOpenTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_012
* DESCRIPTION:  Try to ReOpen Timer 3 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReOpenTimer3( tVoid )
{
  	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER3,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReOpen( TIMER3,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReCloseTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_013
* DESCRIPTION:  Try to ReClose Timer 0 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReCloseTimer0( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 0 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER0,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 0 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER0,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReCloseTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_014
* DESCRIPTION:  Try to ReClose Timer 1 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReCloseTimer1( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 1 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER1,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 1 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER1,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReCloseTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_015
* DESCRIPTION:  Try to ReClose Timer 2 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReCloseTimer2( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 2 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER2,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 2 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER2,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32ReCloseTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_016
* DESCRIPTION:  Try to ReClose Timer 3 in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32ReCloseTimer3( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER3,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerReClose( TIMER3,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetVersionTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_017
* DESCRIPTION:  Get Version on Timer 0 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
******************************************************************************/
tU32 u32GetVersionTimer0( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32VersionInfo    = 0;

	/*Issue Version query on Timer 0 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_READONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 0 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_WRITEONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	#if CPU_EXCEPTION
	/*Issue Version query on Timer 0 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 0 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetVersionTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_018
* DESCRIPTION:  Get Version on Timer 1 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetVersionTimer1( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32VersionInfo    = 0;

	/*Issue Version query on Timer 1 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_READONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 1 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_WRITEONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	#if CPU_EXCEPTION
	/*Issue Version query on Timer 1 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 1 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetVersionTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_019
* DESCRIPTION:  Get Version on Timer 2 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetVersionTimer2( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32VersionInfo    = 0;

	/*Issue Version query on Timer 2 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_READONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 2 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_WRITEONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	#if CPU_EXCEPTION
	/*Issue Version query on Timer 2 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 2 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetVersionTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_020
* DESCRIPTION:  Get Version on Timer 3 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetVersionTimer3( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32VersionInfo    = 0;

	/*Issue Version query on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_READONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_WRITEONLY,&s32VersionInfo ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	#if CPU_EXCEPTION
	/*Issue Version query on Timer 3 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Version query on Timer 3 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetPWMTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_021
* DESCRIPTION:  Get PWM( Duty Cycle ) as on Timer 0 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetPWMTimer0( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32DutyCycle      = 0;

	/*Issue Duty Cycle Query on Timer 0 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER0,OSAL_EN_READONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 1000;
	}

	/*Reinitialize the Duty Cycle*/
	s32DutyCycle = 0;

	/*Issue Duty Cycle Query on Timer 0 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER0,OSAL_EN_WRITEONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 2000;
	}

	#if CPU_EXCEPTION
	/*Issue Duty Cycle Query on Timer 0 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Duty Cycle Query  on Timer 0 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER0,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetPWMTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_022
* DESCRIPTION:  Get PWM( Duty Cycle ) as on Timer 1 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetPWMTimer1( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32DutyCycle      = 0;

	/*Issue Duty Cycle Query on Timer 1 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER1,OSAL_EN_READONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 1000;
	}

	/*Reinitialize the Duty Cycle*/
	s32DutyCycle = 0;

	/*Issue Duty Cycle Query on Timer 1 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER1,OSAL_EN_WRITEONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 2000;
	}

	#if CPU_EXCEPTION
	/*Issue Duty Cycle Query on Timer 1 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Duty Cycle Query  on Timer 1 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER1,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetPWMTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_023
* DESCRIPTION:  Get PWM( Duty Cycle ) as on Timer 2 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
******************************************************************************/
tU32 u32GetPWMTimer2( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32DutyCycle      = 0;

	/*Issue Duty Cycle Query on Timer 2 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER2,OSAL_EN_READONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 1000;
	}

	/*Reinitialize the Duty Cycle*/
	s32DutyCycle = 0;

	/*Issue Duty Cycle Query on Timer 2 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER2,OSAL_EN_WRITEONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 2000;
	}

	#if CPU_EXCEPTION
	/*Issue Duty Cycle Query on Timer 2 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Duty Cycle Query  on Timer 2 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER2,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32GetPWMTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_024
* DESCRIPTION:  Get PWM( Duty Cycle ) as on Timer 3 Channel.
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32GetPWMTimer3( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;
	tS32 s32DutyCycle      = 0;

	/*Issue Duty Cycle Query on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER3,OSAL_EN_READONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 1000;
	}

	/*Reinitialize the Duty Cycle*/
	s32DutyCycle = 0;

	/*Issue Duty Cycle Query on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerGetPWM( TIMER3,OSAL_EN_WRITEONLY,&s32DutyCycle ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Check the Limits of the Duty Cycle*/
	if( ( s32DutyCycle < 0 ) || ( s32DutyCycle > 100 ) )
	{
		u32Ret += 2000;
	}

	#if CPU_EXCEPTION
	/*Issue Duty Cycle Query on Timer 3 in READONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_READONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Duty Cycle Query  on Timer 3 in WRITEONLY mode,with invalid address*/
	if( ( u32RetFromGenFunc = u32GenericTimerVersionQuery( TIMER3,OSAL_EN_WRITEONLY,OSAL_NULL ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}
	#endif

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32SetPWMTimer0()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_025
* DESCRIPTION:  Set PWM on Timer 0 Channel in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32SetPWMTimer0( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 0 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER0,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 0 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER0,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32SetPWMTimer1()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_026
* DESCRIPTION:  Set PWM on Timer 1 Channel in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32SetPWMTimer1( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 1 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER1,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 1 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER1,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32SetPWMTimer2()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_027
* DESCRIPTION:  Set PWM on Timer 2 Channel in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32SetPWMTimer2( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 2 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER2,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 2 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER2,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32SetPWMTimer3()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_TIMER_028
* DESCRIPTION:  Set PWM on Timer 3 Channel in WriteOnly and ReadOnly modes
* HISTORY:		Created By Tinoy Mathews( RBEI/ECM1 ), April 8, 2008
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
******************************************************************************/
tU32 u32SetPWMTimer3( tVoid )		
{
	/*Declarations*/
	tU32 u32Ret 		   = 0;
	tU32 u32RetFromGenFunc = 0;

	/*Issue Open Close on Timer 3 in READONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER3,OSAL_EN_READONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Issue Open Close on Timer 3 in WRITEONLY mode*/
	if( ( u32RetFromGenFunc = u32GenericTimerSetPWM( TIMER3,OSAL_EN_WRITEONLY ) ) != 0 )
	{
		/*Update Error code*/
		u32Ret += u32RetFromGenFunc;
		/*Reinitialize General Return code*/
		u32RetFromGenFunc = 0;
	}

	/*Return error code*/
	return u32Ret;
}





/************************ end of file ********************************/

