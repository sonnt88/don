/******************************************************************************
*FILE         : oedt_Acousticout_TestFuncs.c
*
*SW-COMPONENT : OEDT
*
*DESCRIPTION  : This file implements the individual test cases for the
*               Acoustic device
*
*AUTHOR       : srt2hi Softec
*
*COPYRIGHT    : (c) 2011 Bosch
*
*HISTORY:     initial release

              16-7-2012 RBEI/ECM/ECF5-mem4kor
        Error handler check test case  for acousticout component is added
              Initial Revision
              Lint Fix
              CFG3-1504       Kranthi Kiran(RBEI/ECF5)
*****************************************************************************/
/*#include <stdarg.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>*/
#include <alsa/asoundlib.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"
#include "oedt_Display.h"

//#include "oedt_helper_funcs.h"

#include "oedt_Acousticout_TestFuncs.h"

#include "odt_Globals.h"
#include "odt_LinkedList.h"


#include "acousticout_public.h"
#include "acousticin_public.h"




/* -== ACOUSTICOUT ==- */
/*
OEDT_FUNC_CFG oedt_pACOUSTICOUT_Entries[] =
{
  {u32AcousticoutEmptyStartStopAbortTest,    OEDT_HW_EXC_NO, 25, "EmptyStartAbortStop"},
  {u32AcousticoutStartStopAbortTest,         OEDT_HW_EXC_NO, 25, "startAbortStop"},
  {u32AcousticoutAMRMarkerTest,              OEDT_HW_EXC_NO, 25, "AMR Marker test"},
  {u32AcousticoutPCMBasicTest,               OEDT_HW_EXC_NO, 25, "PCM basic test"},
  {u32AcousticoutPCMCfgTest,                 OEDT_HW_EXC_NO, 25, "PCM configuration test"},
  {u32AcousticoutPCMMarkerTest,              OEDT_HW_EXC_NO, 25, "PCM marker test"},
  {u32AcousticoutAMRBasicTest,               OEDT_HW_EXC_NO, 25, "AMR basic test"},
  {u32AcousticoutAMRFormatErrorTest,         OEDT_HW_EXC_NO, 25, "AMR Format Error test"},
  {u32AcousticoutAMRDataErrorTest,           OEDT_HW_EXC_NO, 25, "AMR Data Error test"},
  {u32AcousticoutConcatTest,                 OEDT_HW_EXC_NO, 25, "PCM/AMR concatenation"},
  {u32AcousticoutConcurrentTest,             OEDT_HW_EXC_NO, 25, "MUSIC/SPEECH concurrent test"},
  {NULL, OEDT_HW_EXC_ALL, 5, (tU8)NULL}
};*/

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
//#define OEDT_ACOUSTICOUT_PRINTF_INFO(_X_) OEDT_ACOUSTICOUT_HelperPrintf_Info _X_
//#define OEDT_ACOUSTICOUT_PRINTF_ERROR(_X_) OEDT_ACOUSTICOUT_HelperPrintf_Error _X_
#define OEDT_ACOUSTICOUT_PRINTF_INFO(_X_) printf _X_
#define OEDT_ACOUSTICOUT_PRINTF_ERROR(_X_) printf _X_
#define OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(_X_) (sizeof(_X_) / sizeof((_X_)[0]))

#define SR_SIZE  32
#define SF_SIZE  32
#define DE_SIZE  32
#define CH_SIZE  32
#define BS_SIZE  32

/* open-string */
#define C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC   OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_MUSIC
#define C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH



/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
#if 0
static tBool OEDT_ACOUSTICOUT_bDoTraceOutput = TRUE;
#endif //#if 0

static int  OEDT_ACOUSTICOUT_iPCMDataBufferIndex;
extern const tU8 OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array[66150];
extern const tU8 OEDT_u8Acoustic_dtmf_22050Hz_stereo_16bit_LE_Array[132300];
extern const tU8 OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[264600];


/************************************************************************
|function prototype
|-----------------------------------------------------------------------*/
extern tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec);


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/


/*********************************************************************FunctionHeaderBegin***
* oedt_vSend2TTFis()
* sends a message to the configured output device
**********************************************************************FunctionHeaderEnd***/
#if 0
 #define OEDT_TRACE_DEVICE            "/dev/trace"
 #define OEDT_TRACE_PERMISSION        OSAL_EN_READWRITE
#endif //#if 0

/********************************************************************/ /**
  *  FUNCTION:      static void vResetPCMData()
  *
  *  @brief         set pcm-Data to start of array (!of all PCM Arrays!)
  *
  *
  *  @return   none
  *
  *  HISTORY:
  *
  ************************************************************************/
static void vResetPCMData(void)
{
  OEDT_ACOUSTICOUT_iPCMDataBufferIndex = 0;
}

/********************************************************************/ /**
  *  FUNCTION:      static int iGetPCMData22050mono(void *vBuffer, int iBufferLen)
  *
  *  @brief         reads pCM-Data 22050Hz mono 16 bit_LE
  *
  *
  *  @return   none
  *
  *  HISTORY:
  *
  ************************************************************************/
static int iGetPCMData22050mono(void *vBuffer, int iBufferLen)
{
  int iPCMBytesCopied = 0;
  int iPCMRecentReadIndex = OEDT_ACOUSTICOUT_iPCMDataBufferIndex;
  int iPCMBytesLeft;
  int iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array);

  iPCMBytesLeft = iPCMByteLen - iPCMRecentReadIndex;

  if(iPCMBytesLeft > 0)
  {
    iPCMBytesCopied = iPCMBytesLeft > iBufferLen ? iBufferLen : iPCMBytesLeft;
    memcpy(vBuffer,
           &OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array[iPCMRecentReadIndex],
           (unsigned int)iPCMBytesCopied);
  } //if(iPCMBytesLeft > 0)

  OEDT_ACOUSTICOUT_iPCMDataBufferIndex += iPCMBytesCopied;
  return iPCMBytesCopied;
}

/********************************************************************/ /**
  *  FUNCTION:      static int iGetPCMData22050stereo(void *vBuffer, int iBufferLen)
  *
  *  @brief         reads pCM-Data 22050Hz stereo 16 bit_LE
  *
  *
  *  @return   none
  *
  *  HISTORY:
  *
  ************************************************************************/
static int iGetPCMData22050stereo(void *vBuffer, int iBufferLen)
{
  int iPCMBytesCopied = 0;
  int iPCMRecentReadIndex = OEDT_ACOUSTICOUT_iPCMDataBufferIndex;
  int iPCMBytesLeft;
  int iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_dtmf_22050Hz_stereo_16bit_LE_Array);

  iPCMBytesLeft = iPCMByteLen - iPCMRecentReadIndex;

  if(iPCMBytesLeft > 0)
  {
    iPCMBytesCopied = iPCMBytesLeft > iBufferLen ? iBufferLen : iPCMBytesLeft;
    memcpy(vBuffer,
           &OEDT_u8Acoustic_dtmf_22050Hz_stereo_16bit_LE_Array[iPCMRecentReadIndex],
           (unsigned int)iPCMBytesCopied);
  } //if(iPCMBytesLeft > 0)

  OEDT_ACOUSTICOUT_iPCMDataBufferIndex += iPCMBytesCopied;
  return iPCMBytesCopied;
}

/********************************************************************/ /**
  *  FUNCTION:      static int iGetPCMData44100stereo(void *vBuffer, int iBufferLen)
  *
  *  @brief         reads pCM-Data 44100Hz stereo 16 bit_LE
  *
  *
  *  @return   none
  *
  *  HISTORY:
  *
  ************************************************************************/
static int iGetPCMData44100stereo(void *vBuffer, int iBufferLen)
{
  int iPCMBytesCopied = 0;
  int iPCMRecentReadIndex = OEDT_ACOUSTICOUT_iPCMDataBufferIndex;
  int iPCMBytesLeft;
  int iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array);

  iPCMBytesLeft = iPCMByteLen - iPCMRecentReadIndex;

  if(iPCMBytesLeft > 0)
  {
    iPCMBytesCopied = iPCMBytesLeft > iBufferLen ? iBufferLen : iPCMBytesLeft;
    memcpy(vBuffer,
           &OEDT_u8Acoustic_dtmf_44100Hz_stereo_16bit_LE_Array[iPCMRecentReadIndex],
           (unsigned int)iPCMBytesCopied);
  } //if(iPCMBytesLeft > 0)

  OEDT_ACOUSTICOUT_iPCMDataBufferIndex += iPCMBytesCopied;
  return iPCMBytesCopied;
}


/********************************************************************/ /**
  *  FUNCTION:      static void vSend2TTFis(tPCS8 ps8Msg, tU32 u32Len)
  *
  *  @brief         Prints information trace message.
  *
  *
  *  @return   none
  *
  *  HISTORY:
  *
  ************************************************************************/
#if 0
static void vSend2TTFis(tPCS8 ps8Msg, tU32 u32Len)
{
  tS8 buf[4 + 255];
  OSAL_trIOWriteTrace rMsg2Trace;
  OSAL_tIODescriptor  fdTrace;

  memset(buf, 0, (tU8)sizeof(buf));
  buf[0] = (tS8)_OEDT_TTFIS_ODTDEV_ID;
  buf[1] = (tS8)_OEDT_TTFIS_HELPER_TRACE_ID;
  //buf[2] = OEDT_RESPONSEMSG_TESTENTRY;
  memcpy(&buf[2], ps8Msg, u32Len);


  rMsg2Trace.u32Class    = (tU32)TR_COMP_OSALTEST;
  rMsg2Trace.u32Level    = (tU32)TR_LEVEL_FATAL;
  rMsg2Trace.u32Length   = u32Len + 2;
  rMsg2Trace.pcos8Buffer = buf;

  fdTrace = OSAL_IOOpen(OEDT_TRACE_DEVICE, OEDT_TRACE_PERMISSION);

  if(fdTrace != OSAL_ERROR)
  {
    OSAL_s32IOWrite(fdTrace, (tPCS8)&rMsg2Trace, sizeof(rMsg2Trace));
    OSAL_s32IOClose(fdTrace);
  }
}
#endif //#if 0


/*****************************************************************************
*
* FUNCTION:
*     getArgList
  *
* DESCRIPTION:
*     This stupid function is used for satifying L I N T
*
*
* PARAMETERS: *va_list pointer to a va_list
*
* RETURNVALUE:
*     va_list
*
*
*
*****************************************************************************/
#if 0
static va_list getArgList(va_list* a)
{
  (void)a;
  return *a;
}
#endif //#if 0

/********************************************************************/ /**
  *  FUNCTION:      tVoid OEDT_ACOUSTICOUT_HelperPrintf(tPCChar pchFormat, ...)
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u8_trace_level     TTFIS trace level
  *  @param         tChar *pchFormat   VA Format string
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
  ************************************************************************/
#if 0
static tVoid OEDT_ACOUSTICOUT_HelperPrintf_Info(tPCChar pchFormat, ...)
{
  tU8 u8_trace_level = TR_LEVEL_USER_4;
  va_list argList;
  if(OEDT_ACOUSTICOUT_bDoTraceOutput)
  {
    argList = getArgList(&argList);
    va_start(argList, pchFormat);
    //OEDT_HelperPrintf(u8_trace_level, pchFormat, argList);
    vprintf(pchFormat, argList);
    va_end(argList);
  } //if(OEDT_ACOUSTICOUT_bDoTraceOutput)
}
#endif //#if 0
/********************************************************************/ /**
  *  FUNCTION:      tVoid OEDT_ACOUSTICOUT_HelperPrintf(tPCChar pchFormat, ...)
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u8_trace_level     TTFIS trace level
  *  @param         tChar *pchFormat   VA Format string
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
  ************************************************************************/
/*static tVoid OEDT_ACOUSTICOUT_HelperPrintf_Error(tPCChar pchFormat, ...)
{
    tU8 u8_trace_level = TR_LEVEL_FATAL;
    va_list argList;

    if(OEDT_ACOUSTICOUT_bDoTraceOutput)
    {
        argList = getArgList(&argList);
        va_start(argList, pchFormat);
        //OEDT_HelperPrintf(u8_trace_level, pchFormat, argList);
        vprintf(pchFormat, argList);
        va_end(argList);
    } //if(OEDT_ACOUSTICOUT_bDoTraceOutput)

}*/











/*****************************************************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/*****************************************************************************/


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T000(void)
  *
  *  @brief         Prints only information about tests.
  *
  *  @param
  *
  *  @return   0
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T000(void)
{
  OEDT_ACOUSTICOUT_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));
  OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTICOUT_T000\n"));
  OEDT_ACOUSTICOUT_PRINTF_INFO(("For testing ACOUSTICOUT you need a special /etc/asound.xxx.conf delivered by ECO\n"));
  OEDT_ACOUSTICOUT_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));
  return 0UL;
}


/*****************************************************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/*****************************************************************************/
#define OEDT_ACOUSTICOUT_T001_COUNT                        3
#define OEDT_ACOUSTICOUT_T001_DEVICE_NAME \
                                   OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH
#define OEDT_ACOUSTICOUT_T001_RESULT_OK_VALUE                      0x00000000
#define OEDT_ACOUSTICOUT_T001_OPEN_OUT_RESULT_ERROR_BIT            0x00000001
#define OEDT_ACOUSTICOUT_T001_GETSUPPSAMPLERATE_RESULT_ERROR_BIT   0x00000002
#define OEDT_ACOUSTICOUT_T001_GETSUPPSAMPLEFORMAT_RESULT_ERROR_BIT 0x00000004
#define OEDT_ACOUSTICOUT_T001_GETSUPPDECODER_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T001_GETSUPPCHANNELS_RESULT_ERROR_BIT     0x00000010
#define OEDT_ACOUSTICOUT_T001_GETSUPPBUFFERSIZE_RESULT_ERROR_BIT   0x00000020
#define OEDT_ACOUSTICOUT_T001_GETDECODER_RESULT_ERROR_BIT          0x00000040
#define OEDT_ACOUSTICOUT_T001_GETBUFFERSIZE_RESULT_ERROR_BIT       0x00000080

#define OEDT_ACOUSTICOUT_T001_CLOSE_OUT_RESULT_ERROR_BIT           0x80000000



/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T001(void)
  *
  *  @brief         Open-Close Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T001(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T001_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T001(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T001_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T001_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n", OEDT_ACOUSTICOUT_T001_DEVICE_NAME, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICOUT_T001_DEVICE_NAME, (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_ERROR == hAcousticout)

    /* get supported samplerates*/
    {
      OSAL_trAcousticSampleRateCapability rSampleRateCap;
      OSAL_tAcousticSampleRate rFrom[SR_SIZE];
      OSAL_tAcousticSampleRate rTo[SR_SIZE];

      rSampleRateCap.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCap.pnSamplerateFrom = rFrom;
      rSampleRateCap.pnSamplerateTo   = rTo;
      rSampleRateCap.u32ElemCnt       = SR_SIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLERATE,
                                 (tS32)&rSampleRateCap);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetSampleRateCap\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETSUPPSAMPLERATE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        int i;
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetSampleRateCap %u: ",
                                      (unsigned int)rSampleRateCap.u32ElemCnt));
        for(i = 0; i < (int)rSampleRateCap.u32ElemCnt; ++i)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("%u-%u ",
                                        (unsigned int)rSampleRateCap.pnSamplerateFrom[i],
                                        (unsigned int)rSampleRateCap.pnSamplerateTo[i]));
        } //for(i = 0; i < rSampleRateCap.u32ElemCnt; ++i)
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\n"));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* get supported sampleformat*/
    {
      OSAL_trAcousticSampleFormatCapability rSampleFormatCap;
      OSAL_tenAcousticSampleFormat          rSF[SR_SIZE];

      rSampleFormatCap.enCodec           = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCap.penSampleformats  = rSF;
      rSampleFormatCap.u32ElemCnt        = SF_SIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLEFORMAT,
                                 (tS32)&rSampleFormatCap);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetSampleFormatCap\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETSUPPSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        int i;
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetSampleFormatCap %u: ",
                                      (unsigned int)rSampleFormatCap.u32ElemCnt));
        for(i = 0; i < (int)rSampleFormatCap.u32ElemCnt; ++i)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("%u ",
                                        (unsigned int)rSampleFormatCap.penSampleformats[i]));
        }
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\n"));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* get supported decoders*/
    {
      OSAL_trAcousticCodecCapability      rCap;
      OSAL_tenAcousticCodec               rDE[DE_SIZE];

      rCap.penCodecs                     = rDE;
      rCap.u32ElemCnt                    = DE_SIZE;
      rCap.u32MaxCodecCnt                = DE_SIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_DECODER,
                                 (tS32)&rCap);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetDecoderCap\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETSUPPDECODER_RESULT_ERROR_BIT;
      }
      else
      {
        int i;
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS GetDecoderCap MaxDecoders %u: ",
                                      (unsigned int)rCap.u32MaxCodecCnt));
        for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("%u ",
                                        (unsigned int)rCap.penCodecs[i]));
        }
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\n"));

      }
    }

    /* get supported channels*/
    {
      OSAL_trAcousticChannelCapability rCap;
      tU32 u32Num[CH_SIZE];
      rCap.pu32NumChannels = u32Num;
      rCap.u32ElemCnt      = CH_SIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_CHANNELS,
                                 (tS32)&rCap);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetChannelCap %u\n", (unsigned int)rCap.u32ElemCnt));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETSUPPCHANNELS_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        int i;
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS GetChannelCap %u: ",
                                      (unsigned int)rCap.u32ElemCnt));
        for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("%u ",
                                        (unsigned int)rCap.pu32NumChannels[i]));
        } //for(i = 0; i < rCap.u32ElemCnt; ++i)
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\n"));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* get supported buffersizes*/
    {
      OSAL_trAcousticBufferSizeCapability  rCap;
      OSAL_tAcousticBuffersize             rBS[BS_SIZE];

      rCap.enCodec           = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCap.pnBuffersizes     = rBS;
      rCap.u32ElemCnt        = BS_SIZE;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_BUFFERSIZE,
                                 (tS32)&rCap);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetBufferSizeCap\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETSUPPBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        int i;
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS GetBufferSizeCap %d: ",
                                      (int)rCap.u32ElemCnt));
        for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("%u ",
                                        (unsigned int)rCap.pnBuffersizes[i]));
        } //for(i = 0; i < rCap.u32ElemCnt; ++i)
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\n"));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* get default decoder*/
    {
      OSAL_tenAcousticCodec trDecoder;
      trDecoder = OSAL_EN_ACOUSTIC_CODECLAST;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETDECODER,
                                 (tS32)&trDecoder);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetDecoder\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETDECODER_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS GetDecoder: %d\n", (int)trDecoder));
      } //else //if(OSAL_OK != s32Ret)
    }


    /* get default buffersize*/
    {
      OSAL_trAcousticBufferSizeCfg trBS;

      trBS.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;

      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETBUFFERSIZE,
                                 (tS32)&trBS);

      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GetBufferSize\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_GETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS GetBufferSize: %u\n",
                                      (unsigned int)trBS.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }


    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T001_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u (count %d)\n",
                                    (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T001_COUNT; iCount++)

  if(OEDT_ACOUSTICOUT_T001_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T001 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T001_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T001(void)



/*****************************************************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/************************* TEST 002 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T002_COUNT                        3
#define OEDT_ACOUSTICOUT_T002_DEVICE_NAME \
                      OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH
#define OEDT_ACOUSTICOUT_T002_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T002_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T002_BUFFERSIZE                   2048

#define OEDT_ACOUSTICOUT_T002_RESULT_OK_VALUE                  0x00000000
#define OEDT_ACOUSTICOUT_T002_OPEN_OUT_RESULT_ERROR_BIT        0x00000001

#define OEDT_ACOUSTICOUT_T002_SETSAMPLERATE_RESULT_ERROR_BIT   0x00000004
#define OEDT_ACOUSTICOUT_T002_SETCHANNELS_RESULT_ERROR_BIT     0x00000008
#define OEDT_ACOUSTICOUT_T002_SETSAMPLEFORMAT_RESULT_ERROR_BIT 0x00000010
#define OEDT_ACOUSTICOUT_T002_SETBUFFERSIZE_RESULT_ERROR_BIT   0x00000020

#define OEDT_ACOUSTICOUT_T002_CLOSE_OUT_RESULT_ERROR_BIT       0x80000000



/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T002(void)
  *
  *  @brief         Set Parameter Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T002(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T002_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T002(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T002_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T002_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICOUT_T002_DEVICE_NAME, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, (count %d)\n",
                                    OEDT_ACOUSTICOUT_T002_DEVICE_NAME,
                                    (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_ERROR == hAcousticout)



    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T002_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T002_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %u\n",
                                      (unsigned int)rSampleFormatCfg.enSampleformat));
      }
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T002_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      }
    }

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T002_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u (count %d)\n",
                                    (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T002_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T002_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T002 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T002_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T002(void)




/*****************************************************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/************************* TEST 003 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T003_COUNT                        3
#define OEDT_ACOUSTICOUT_T003_DEVICE_NAME \
                       OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH
#define OEDT_ACOUSTICOUT_T003_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T003_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T003_BUFFERSIZE                   2048

#define OEDT_ACOUSTICOUT_T003_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T003_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T003_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_T003_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T003_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T003_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T003_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T003_START_RESULT_ERROR_BIT            0x00000040

#define OEDT_ACOUSTICOUT_T003_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T003_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T003_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T003_bStopped = FALSE;


/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T003
*DESCRIPTION: used as device callback function for device test T003
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticOutTstCallback_T003 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICOUT_T003_bStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK:"
                                  " OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:"
                                  " %u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr.enErrType,
                                  (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "DEFAULT %u == 0x%08X\n",
                                  (unsigned int)enCbReason,
                                  (unsigned int)enCbReason));
    break;
  }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T003(void)
  *
  *  @brief         Start/Stop Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T003(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T003_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T003(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T003_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T003_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> (count %d)\n",
                                     OEDT_ACOUSTICOUT_T003_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, (count %d)\n",
                                    OEDT_ACOUSTICOUT_T003_DEVICE_NAME,
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /* register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T003;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                 (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_REG_NOTIFICATION_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

      }
    }


    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T003_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T003_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %u\n",
                                      (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T003_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }



    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)


    OEDT_ACOUSTICOUT_T003_bStopped = FALSE;
    /* stop command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_STOP_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
    } //else //if(OSAL_OK != s32Ret)

    //waiting for stop reply
    {
      int iEmergency = 50;
      while(!OEDT_ACOUSTICOUT_T003_bStopped && (iEmergency-- > 0))
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
        OSAL_s32ThreadWait(100);
      } //while(!OEDT_ACOUSTICOUT_T003_bStopped && iEmergency-- > 0)

      if(iEmergency <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR no STOP acknowledge\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_STOP_ACK_RESULT_ERROR_BIT;
      }
      else //if(iEmergency <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
      } //else //if(iEmergency <= 0)
    }


    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T003_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u (count %d)\n",
                                    (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T003_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T003_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T003 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T003_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T003(void)








/*****************************************************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/************************* TEST 004 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T004_COUNT                        3
#define OEDT_ACOUSTICOUT_T004_PCMCOUNT                     5
#define OEDT_ACOUSTICOUT_T004_DEVICE_NAME \
                 OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate"
#define OEDT_ACOUSTICOUT_T004_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T004_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T004_BUFFERSIZE                   (4096)
//#define OEDT_ACOUSTICOUT_T004_BYTES_PER_SAMPLE             (OEDT_ACOUSTICOUT_T004_CHANNELS * 2)

#define OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T004_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T004_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_T004_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T004_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T004_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T004_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T004_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T004_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T004_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T004_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T004_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T004_bStopped = FALSE;
static tU8   OEDT_ACOUSTICOUT_T004_u8PCMBuffer[OEDT_ACOUSTICOUT_T004_BUFFERSIZE];

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T004
*DESCRIPTION: used as device callback function for device test T004
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid
vAcousticOutTstCallback_T004 (OSAL_tenAcousticOutEvent enCbReason,
                              tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICOUT_T004_bStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr.enErrType,
                                  (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "DEFAULT %u == 0x%08X\n",
                                  (unsigned int)enCbReason,
                                  (unsigned int)enCbReason));
    break;
  }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T004(void)
  *
  *  @brief         Play PCM Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T004(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T004(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T004_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T004_DEVICE_NAME,
                               OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> (count %d)\n",
                                     OEDT_ACOUSTICOUT_T004_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, (count %d)\n",
                                    OEDT_ACOUSTICOUT_T004_DEVICE_NAME,
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /* register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T004;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                 (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_REG_NOTIFICATION_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

      }
    }


    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T004_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %u\n",
                                       (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T004_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETCHANNELS: %u\n",
                                       (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLEFORMAT: %u\n",
                                       (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %u\n",
                                      (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T004_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETBUFFERSIZE: %u\n",
                                       (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }


    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE == u32ResultBitMask)
    {
      int iReadLen;
      int iPCMCount;
      tS8 *ps8PCM       = (tS8*)OEDT_ACOUSTICOUT_T004_u8PCMBuffer;
      //tS16 *ps16PCMData = (tS16 *)OEDT_ACOUSTICOUT_T004_u8PCMBuffer;

      //Play sinus
            #if 0
      {
        int iDeg;
        double dSin;
        double dSample;
        int i;
        int iDegStep = 360 / 20;
        int *piSampleBuffer = (int*)OEDT_ACOUSTICOUT_T004_u8PCMBuffer;
        int iNumberOfSamplesInBuffer = OEDT_ACOUSTICOUT_T004_BUFFERSIZE / OEDT_ACOUSTICOUT_T004_BYTES_PER_SAMPLE;  // Byte  per sample


        iDeg = 0;
        iReadLen = OEDT_ACOUSTICOUT_T004_BUFFERSIZE;

        for(iPCMCount = 0; iPCMCount < 200; iPCMCount++)
        {
          for(i = 0; i < iNumberOfSamplesInBuffer; i++)
          {
            dSin = sin((double)iDeg * 3.1415926 / 180.0);
            dSample = dSin * (double)10000.0;
            piSampleBuffer[i] = (int)dSample;
            iDeg += iDegStep;
            iDeg %= 360;
          } //for(i = 0; i < iNumberOfSamplesInBuffer; i++)
          s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iReadLen);
          if(OSAL_ERROR == s32Ret)
          {
            OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                          "ERROR Aout Write bytes "
                                          "(%u) PCMCount %d\n",
                                          (unsigned int)iReadLen,
                                          iPCMCount));
            u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_WRITE_RESULT_ERROR_BIT;
          }
          else
          {
            //   OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)iRead));
          }
        } //for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T004_PCMCOUNT; iPCMCount++)
      }
            #endif // play sinus

      //play DTMF mono 16bit 22050Hz pcm
      {
        for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T004_PCMCOUNT; iPCMCount++)
        {
          vResetPCMData();
          while(
               (iReadLen =
                iGetPCMData22050mono(
                                    OEDT_ACOUSTICOUT_T004_u8PCMBuffer,
                                    OEDT_ACOUSTICOUT_T004_BUFFERSIZE)
               )
               >
               0
               )
          {
            //int iPCMNumberOfSamples = iReadLen / sizeof(tS16);

            s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iReadLen);
            if(OSAL_ERROR == s32Ret)
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "ERROR Aout Write bytes (%u) "
                                            "PCMCount %d\n",
                                            (unsigned int)iReadLen,
                                            iPCMCount));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_WRITE_RESULT_ERROR_BIT;
            }
            else
            {
              //   OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)iRead));
            }
          } //while((iReadLen = iGetPCMData(OEDT_ACOUSTICOUT_T004_u8PCMBuffer, OEDT_ACOUSTICOUT_T004_BUFFERSIZE)) > 0)
        } //for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T004_PCMCOUNT; iPCMCount++)
      }





      /* stop command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "INFO Send STOP command\n"));
      OEDT_ACOUSTICOUT_T004_bStopped = FALSE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_STOP_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T004_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T004_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE == u32ResultBitMask)


    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR AOUT CLOSE handle %u (count %d)\n",
                                     (unsigned int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T004_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u"
                                    "(count %d)\n",
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T004_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                   "T004 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T004_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T004(void)






/*****************************************************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/************************* TEST 005 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T005_COUNT                        3
#define OEDT_ACOUSTICOUT_T005_DEVICE_NAME \
                  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate"

#define OEDT_ACOUSTICOUT_T005_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T005_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T005_BUFFERSIZE                   16384

#define OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T005_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T005_THREADSPAWN_RESULT_ERROR_BIT      0x00000002
#define OEDT_ACOUSTICOUT_T005_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T005_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T005_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T005_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T005_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T005_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T005_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T005_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T005_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T005_bStopped = FALSE;

static void OEDT_T005_vThread(void *pvData);

static OSAL_tThreadID         OEDT_T005_threadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T005_threadAttr = {"OEDTT005", //name
101,       //Prio
65536,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T005_vThread,
(void*)NULL};



/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T005_vThread()
  *
  *  @brief         Wait-Thread (receive STOP-Event)
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static void OEDT_T005_vThread(void *pvData)
{
  tBool bRun = TRUE;
  //int iCount = 0;
  OSAL_tIODescriptor hAcousticout = *(OSAL_tIODescriptor*)pvData;
  tS32 s32Result;
  OSAL_trAcousticOutWaitEvent oWaitEvent;
  oWaitEvent.nTimeout = OSAL_C_TIMEOUT_FOREVER;
  (void)pvData;

  while(bRun)
  {
    //ash        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is RUNNING <%s> (count %d)\n",OEDT_T005_threadAttr.szName, iCount));
    //OSAL_s32ThreadWait(1000);

    s32Result = OSAL_s32IOControl(hAcousticout,
                                  OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT,
                                  (tS32)&oWaitEvent);

    if(OSAL_OK != s32Result)
    {
      // device is closed so leave inner loop and go to outer loop
      //ash           OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: Thread IOCTRL WAITEVENT canceled\n"));
      bRun = FALSE;
    } //if ((OSAL_ERROR == s32Result) || (OSAL_E_CANCELED == s32Result))
    else
    {
      switch(oWaitEvent.enEvent)
      {
      case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
        OEDT_ACOUSTICOUT_T005_bStopped = TRUE;
        break;

      case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVTIMER:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED\n"));
        break;
      default:
        //ash              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread Unknown Event %u\n", (unsigned int)oWaitEvent.enEvent));
        ;
      } //switch(oWaitEvent.enEvent)
    } //if(OSAL_OK != s32Result)


  } //while(1)

  //ash    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is Killed <%s>\n",OEDT_T005_threadAttr.szName));
  //OSAL_vSetErrorCode(OSAL_E_NOERROR);
  OSAL_vThreadExit();
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T005(void)
  *
  *  @brief         Play PCM Test, wait for STOP via OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T005(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE;
  static OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T005(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T005_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T005_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> (count %d)\n",
                                     OEDT_ACOUSTICOUT_T005_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, "
                                    "(count %d)\n",
                                    OEDT_ACOUSTICOUT_T005_DEVICE_NAME,
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)

    //run wait-thread
    {
      OEDT_T005_threadID = OSAL_ERROR;
      OEDT_T005_threadAttr.pvArg = &hAcousticout;
      OEDT_T005_threadID = OSAL_ThreadSpawn(&OEDT_T005_threadAttr);
      if(OSAL_ERROR == OEDT_T005_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR Create Thread <%s> "
                                       "(count %d)\n",
                                       OEDT_T005_threadAttr.szName,
                                       iCount));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_THREADSPAWN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_ERROR == OEDT_T005_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS Creating Thread <%s> "
                                      "(count %d)\n",
                                      OEDT_T005_threadAttr.szName,
                                      iCount));
      } //else //if(OSAL_ERROR == OEDT_T005_threadID)
    }

    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T005_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %u\n",
                                       (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T005_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETCHANNELS: %u\n",
                                       (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLEFORMAT: %u\n",
                                       (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %u\n",
                                      (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T005_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETBUFFERSIZE: %u\n",
                                       (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }


    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE == u32ResultBitMask)
    {
      tUInt iPCMByteLen =
      OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(
                                               OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array);
      tS8 *ps8PCM = (tS8*)OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array;
      tUInt iBufferLen;

      iBufferLen = OEDT_ACOUSTICOUT_T005_BUFFERSIZE;

      /*ENABLE THIS CHECK IF THE SIZE OF THE INPUT DATA IS DECIDED RUN TIME OR
      THE SIZE BECOMES <    OEDT_ACOUSTICOUT_T005_BUFFERSIZE
      COMMENTED NOW TO MAKE LINT HAPPY */
    #if 0
      if(iPCMByteLen < iBufferLen)
      {
        iBufferLen = iPCMByteLen;
      }
    #endif

      while(iPCMByteLen)
      {

        s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iBufferLen);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "ERROR Aout Write bytes (%d)\n",
                                        (int)iBufferLen));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_WRITE_RESULT_ERROR_BIT;
          break;
        }
        else
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "SUCCESS AOut Write Bytes (%d)\n",
                                        (int)iBufferLen));
        }
        ps8PCM += iBufferLen;
        if(iPCMByteLen < iBufferLen)
        {
          iBufferLen = iPCMByteLen;
        }
        else
        {
          iPCMByteLen -= iBufferLen;
        }
      }


      OEDT_ACOUSTICOUT_T005_bStopped = FALSE;

      /* stop command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_STOP_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T005_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T005_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE == u32ResultBitMask)


    // delete Thread
    {
      if(OSAL_ERROR != OEDT_T005_threadID)
      {
        // thread destroys itself OSAL_s32ThreadDelete(OEDT_T005_threadID);
        OSAL_s32ThreadJoin(OEDT_T005_threadID, 1000);
        OEDT_T005_threadID = OSAL_ERROR;
      } //if(OSAL_ERROR != OEDT_T005_threadID)

    }

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR AOUT CLOSE handle %u "
                                     "(count %d)\n",
                                     (unsigned int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T005_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u "
                                    "(count %d)\n",
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T005_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                   "T005 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T005_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T005(void)




/*****************************************************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/************************* TEST 006 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T006_COUNT                        3
#define OEDT_ACOUSTICOUT_T006_PCMCOUNT                     5
#define OEDT_ACOUSTICOUT_T006_DEVICE_NAME \
                     OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH"/OedtWithoutRate"
#define OEDT_ACOUSTICOUT_T006_SAMPLE_RATE                  44100
#define OEDT_ACOUSTICOUT_T006_CHANNELS                     2
#define OEDT_ACOUSTICOUT_T006_BUFFERSIZE                   (8192)
//#define OEDT_ACOUSTICOUT_T006_BYTES_PER_SAMPLE             (OEDT_ACOUSTICOUT_T006_CHANNELS * 2)

#define OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T006_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T006_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_T006_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T006_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T006_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T006_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T006_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T006_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T006_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T006_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T006_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T006_bStopped = FALSE;
static tU8   OEDT_ACOUSTICOUT_T006_u8PCMBuffer[OEDT_ACOUSTICOUT_T006_BUFFERSIZE];

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T006
*DESCRIPTION: used as device callback function for device test T006
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticOutTstCallback_T006 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICOUT_T006_bStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: "
                                  "%u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr.enErrType,
                                  (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "DEFAULT %u == 0x%08X\n",
                                  (unsigned int)enCbReason,
                                  (unsigned int)enCbReason));
    break;
  }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T006(void)
  *
  *  @brief         Play PCM Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T006(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T006(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T006_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T006_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> (count %d)\n",
                                     OEDT_ACOUSTICOUT_T006_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, "
                                    "(count %d)\n",
                                    OEDT_ACOUSTICOUT_T006_DEVICE_NAME,
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /* register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T006;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                 (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_REG_NOTIFICATION_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

      }
    }


    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T006_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %u\n",
                                       (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T006_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETCHANNELS: %u\n",
                                       (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLEFORMAT: %u\n",
                                       (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %u\n",
                                      (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T006_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETBUFFERSIZE: %u\n",
                                       (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE == u32ResultBitMask)
    {
      int iReadLen;
      int iPCMCount;
      tS8 *ps8PCM       = (tS8*)OEDT_ACOUSTICOUT_T006_u8PCMBuffer;
      //play DTMF mono 16bit 22050Hz pcm
      {
        for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T006_PCMCOUNT; iPCMCount++)
        {
          vResetPCMData();
          while((iReadLen = iGetPCMData44100stereo(OEDT_ACOUSTICOUT_T006_u8PCMBuffer,
                                                   OEDT_ACOUSTICOUT_T006_BUFFERSIZE)) > 0)
          {
            //int iPCMNumberOfSamples = iReadLen / sizeof(tS16);

            s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iReadLen);
            if(OSAL_ERROR == s32Ret)
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "ERROR Aout Write bytes (%d) "
                                            "PCMCount %d\n",
                                            iReadLen,
                                            iPCMCount));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_WRITE_RESULT_ERROR_BIT;
            }
            else
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "SUCCESS AOut Write Bytes (%u)\n",
                                            (unsigned int)iReadLen));
            }
          } //while((iReadLen = iGetPCMData(OEDT_ACOUSTICOUT_T006_u8PCMBuffer, OEDT_ACOUSTICOUT_T006_BUFFERSIZE)) > 0)
        } //for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T006_PCMCOUNT; iPCMCount++)
      }


      /* stop command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "INFO Send STOP command\n"));
      OEDT_ACOUSTICOUT_T006_bStopped = FALSE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_STOP_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T006_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T006_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE == u32ResultBitMask)

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR AOUT CLOSE handle %u "
                                     "(count %d)\n",
                                     (unsigned int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T006_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %u "
                                    "(count %d)\n",
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T006_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                   "T006 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T006_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T006(void)


/*****************************************************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/************************* TEST 007 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T007_COUNT                        3
#define OEDT_ACOUSTICOUT_T007_PCMCOUNT                     5
#define OEDT_ACOUSTICOUT_T007_DEVICE_NAME \
                  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate"
#define OEDT_ACOUSTICOUT_T007_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T007_CHANNELS                     2
#define OEDT_ACOUSTICOUT_T007_BUFFERSIZE                   (4096)
//#define OEDT_ACOUSTICOUT_T007_BYTES_PER_SAMPLE             (OEDT_ACOUSTICOUT_T007_CHANNELS * 2)

#define OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T007_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T007_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_T007_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T007_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T007_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T007_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T007_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T007_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T007_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T007_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T007_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T007_bStopped = FALSE;
static tU8   OEDT_ACOUSTICOUT_T007_u8PCMBuffer[OEDT_ACOUSTICOUT_T007_BUFFERSIZE];

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T007
*DESCRIPTION: used as device callback function for device test T007
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticOutTstCallback_T007 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICOUT_T007_bStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: "
                                  "%u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr.enErrType,
                                  (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "DEFAULT %u == 0x%08X\n",
                                  (unsigned int)enCbReason,
                                  (unsigned int)enCbReason));
    break;
  }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T007(void)
  *
  *  @brief         Play PCM Test
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T007(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T007(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T007_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T007_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> "
                                     "(count %d)\n",
                                     OEDT_ACOUSTICOUT_T007_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %u, "
                                    "(count %d)\n",
                                    OEDT_ACOUSTICOUT_T007_DEVICE_NAME,
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /* register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T007;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                 (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_REG_NOTIFICATION_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

      }
    }


    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T007_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %u\n",
                                       (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T007_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETCHANNELS: %u\n",
                                       (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLEFORMAT: %u\n",
                                       (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %d\n",
                                      (int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T007_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETBUFFERSIZE: %u\n",
                                       (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %u\n",
                                      (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }
    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE == u32ResultBitMask)
    {
      int iReadLen;
      int iPCMCount;
      tS8 *ps8PCM       = (tS8*)OEDT_ACOUSTICOUT_T007_u8PCMBuffer;

      //play DTMF mono 16bit 22050Hz pcm
      {
        for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T007_PCMCOUNT; iPCMCount++)
        {
          vResetPCMData();
          while((iReadLen = iGetPCMData22050stereo(OEDT_ACOUSTICOUT_T007_u8PCMBuffer, OEDT_ACOUSTICOUT_T007_BUFFERSIZE)) > 0)
          {
            //int iPCMNumberOfSamples = iReadLen / sizeof(tS16);

            s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iReadLen);
            if(OSAL_ERROR == s32Ret)
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "ERROR Aout Write bytes (%d) "
                                            "PCMCount %d\n",
                                            iReadLen,
                                            iPCMCount));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_WRITE_RESULT_ERROR_BIT;
            }
            else
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS "
                                            "AOut Write Bytes (%d)\n",
                                            (int)iReadLen));
            }
          } //while((iReadLen = iGetPCMData(OEDT_ACOUSTICOUT_T007_u8PCMBuffer, OEDT_ACOUSTICOUT_T007_BUFFERSIZE)) > 0)
        } //for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T007_PCMCOUNT; iPCMCount++)
      }





      /* stop command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "INFO Send STOP command\n"));
      OEDT_ACOUSTICOUT_T007_bStopped = FALSE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_STOP_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS STOP\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T007_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T007_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE == u32ResultBitMask)

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR AOUT CLOSE handle %d "
                                     "(count %d)\n",
                                     (int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T007_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %d "
                                    "(count %d)\n",
                                    (int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T007_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                   "T007 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T007_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T007(void)



/*****************************************************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/************************* TEST 008 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T008_COUNT                        3
#define OEDT_ACOUSTICOUT_T008_PCMCOUNT                     1
#define OEDT_ACOUSTICOUT_T008_DEVICE_NAME \
                  (OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate")
#define OEDT_ACOUSTICOUT_T008_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T008_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T008_BUFFERSIZE                   (4096)
//#define OEDT_ACOUSTICOUT_T008_BYTES_PER_SAMPLE             (OEDT_ACOUSTICOUT_T008_CHANNELS * 2)

#define OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T008_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T008_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_T008_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T008_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T008_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T008_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T008_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T008_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T008_ABORT_RESULT_ERROR_BIT            0x20000000
#define OEDT_ACOUSTICOUT_T008_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T008_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T008_bStopped = FALSE;
static tU8   OEDT_ACOUSTICOUT_T008_u8PCMBuffer[OEDT_ACOUSTICOUT_T008_BUFFERSIZE];

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T008
*DESCRIPTION: used as device callback function for device test T008
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticOutTstCallback_T008 (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  OSAL_trAcousticErrThrCfg rCbLastErrThr;
  (void)pvCookie;
  (void)pvAddData;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    OEDT_ACOUSTICOUT_T008_bStopped = TRUE;
    break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:

    rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: "
                                  "%u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr.enErrType,
                                  (unsigned int)rCbLastErrThr.enErrType));
    switch(rCbLastErrThr.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      break;

    default:
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;

  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: "
                                  "DEFAULT %u == 0x%08X\n",
                                  (unsigned int)enCbReason,
                                  (unsigned int)enCbReason));
    break;
  }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T008(void)
  *
  *  @brief         Play PCM Test - ABORT test with callback
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T008(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T008(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T008_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T008_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> "
                                     "(count %d)\n",
                                     OEDT_ACOUSTICOUT_T008_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %d, "
                                    "(count %d)\n",
                                    OEDT_ACOUSTICOUT_T008_DEVICE_NAME,
                                    (int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)


    /* register callback function */
    {
      OSAL_trAcousticOutCallbackReg rCallbackReg;
      rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T008;
      rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                 (tS32)&rCallbackReg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_REG_NOTIFICATION_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

      }
    }


    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T008_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %u\n",
                                       (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLERATE: %u\n",
                                      (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T008_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETCHANNELS: %u\n",
                                       (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETCHANNELS: %u\n",
                                      (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLEFORMAT: %d\n",
                                       (int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETSAMPLEFORMAT: %d\n",
                                      (int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T008_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETBUFFERSIZE: %d\n",
                                       (int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS SETBUFFERSIZE: %d\n",
                                      (int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE == u32ResultBitMask)
    {
      int iReadLen;
      int iPCMCount;
      tS8 *ps8PCM       = (tS8*)OEDT_ACOUSTICOUT_T008_u8PCMBuffer;

      //play DTMF mono 16bit 22050Hz pcm
      {
        for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T008_PCMCOUNT; iPCMCount++)
        {
          vResetPCMData();
          while((iReadLen = iGetPCMData22050mono(OEDT_ACOUSTICOUT_T008_u8PCMBuffer, OEDT_ACOUSTICOUT_T008_BUFFERSIZE)) > 0)
          {
            //int iPCMNumberOfSamples = iReadLen / sizeof(tS16);

            s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iReadLen);
            if(OSAL_ERROR == s32Ret)
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "ERROR Aout Write bytes (%d) "
                                            "PCMCount %d\n",
                                            iReadLen,
                                            iPCMCount));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_WRITE_RESULT_ERROR_BIT;
            }
            else
            {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS "
                                            "AOut Write Bytes (%d)\n",
                                            (int)iReadLen));
            }
          } //while((iReadLen = iGetPCMData(OEDT_ACOUSTICOUT_T008_u8PCMBuffer, OEDT_ACOUSTICOUT_T008_BUFFERSIZE)) > 0)
        } //for(iPCMCount = 0; iPCMCount < OEDT_ACOUSTICOUT_T008_PCMCOUNT; iPCMCount++)
      }





      /* abort command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "INFO Send ABORT command\n"));
      OEDT_ACOUSTICOUT_T008_bStopped = FALSE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR ABORT\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_ABORT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS ABORT\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for abort-stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T008_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T008_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE == u32ResultBitMask)

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR AOUT CLOSE handle %d "
                                     "(count %d)\n",
                                     (int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T008_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS AOUT CLOSE handle %d "
                                    "(count %d)\n",
                                    (int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T008_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T008 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T008_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T008(void)


/*****************************************************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/************************* TEST 009 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T009_COUNT                        3
#define OEDT_ACOUSTICOUT_T009_DEVICE_NAME \
                  (OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate")

#define OEDT_ACOUSTICOUT_T009_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T009_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T009_BUFFERSIZE                   8192

#define OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T009_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T009_THREADSPAWN_RESULT_ERROR_BIT      0x00000002
#define OEDT_ACOUSTICOUT_T009_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T009_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T009_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T009_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T009_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T009_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_T009_ABORT_RESULT_ERROR_BIT            0x20000000
#define OEDT_ACOUSTICOUT_T009_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T009_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T009_bStopped = FALSE;

static void OEDT_T009_vThread(void *pvData);

static OSAL_tThreadID         OEDT_T009_threadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T009_threadAttr = {"OEDTT009", //name
101,       //Prio
65536,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T009_vThread,
(void*)NULL};



/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T009_vThread()
  *
  *  @brief         Wait-Thread (receive STOP-Event)
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static void OEDT_T009_vThread(void *pvData)
{
  tBool bRun = TRUE;
  //int iCount = 0;
  OSAL_tIODescriptor hAcousticout = *(OSAL_tIODescriptor*)pvData;
  tS32 s32Result;
  OSAL_trAcousticOutWaitEvent oWaitEvent;
  oWaitEvent.nTimeout = OSAL_C_TIMEOUT_FOREVER;
  (void)pvData;

  while(bRun)
  {
    //  iCount++;
    //ash        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is RUNNING <%s> (count %d)\n",OEDT_T009_threadAttr.szName, iCount));
    //OSAL_s32ThreadWait(1000);

    s32Result = OSAL_s32IOControl(hAcousticout,
                                  OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT,
                                  (tS32)&oWaitEvent);

    if(OSAL_OK != s32Result)
    {
      // device is closed so leave inner loop and go to outer loop
      //ash           OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: Thread IOCTRL WAITEVENT canceled\n"));
      bRun = FALSE;
    }
    else //if(OSAL_OK != s32Result)
    {

      switch(oWaitEvent.enEvent)
      {
      case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
        OEDT_ACOUSTICOUT_T009_bStopped = TRUE;
        break;

      case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVTIMER:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED\n"));
        break;
      default:
        //ash              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread Unknown Event %u\n", (unsigned int)oWaitEvent.enEvent));
        ;
      } //switch(oWaitEvent.enEvent)
    } //else //if(OSAL_OK != s32Result)
  } //while(1)

  //ash    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is Killed <%s>\n",OEDT_T009_threadAttr.szName));
  //OSAL_vSetErrorCode(OSAL_E_NOERROR);
  OSAL_vThreadExit();
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T009(void)
  *
  *  @brief         Play PCM Test, wait for STOP via OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T009(void)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE;
  static OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T009(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T009_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T009_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "ERROR Open <%s> (count %d)\n",
                                     OEDT_ACOUSTICOUT_T009_DEVICE_NAME,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                    "SUCCESS Open <%s> == %d, "
                                    "(count %d)\n",
                                    OEDT_ACOUSTICOUT_T009_DEVICE_NAME,
                                    (int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_ERROR == hAcousticout)

    //run wait-thread
    {
      OEDT_T009_threadID = OSAL_ERROR;
      OEDT_T009_threadAttr.pvArg = &hAcousticout;
      OEDT_T009_threadID = OSAL_ThreadSpawn(&OEDT_T009_threadAttr);
      if(OSAL_ERROR == OEDT_T009_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR Create Thread <%s> "
                                       "(count %d)\n",
                                       OEDT_T009_threadAttr.szName,
                                       iCount));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_THREADSPAWN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_ERROR == OEDT_T009_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS Creating Thread <%s> "
                                      "(count %d)\n",
                                      OEDT_T009_threadAttr.szName,
                                      iCount));
      } //else //if(OSAL_ERROR == OEDT_T009_threadID)
    }

    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T009_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR SETSAMPLERATE: %d\n",
                                       (int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T009_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS: %u\n", (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T009_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE == u32ResultBitMask)
    {
      tUInt iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(
                                                                   OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array);

      tS8 *ps8PCM = (tS8*)OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array;
      tUInt iBufferLen;

      iBufferLen = OEDT_ACOUSTICOUT_T009_BUFFERSIZE;

      /*ENABLE THIS CHECK IF THE SIZE OF THE INPUT DATA IS DECIDED RUN TIME OR
      THE SIZE BECOMES <    OEDT_ACOUSTICOUT_T005_BUFFERSIZE
      COMMENTED NOW TO MAKE LINT HAPPY */

    #if 0
      if(iPCMByteLen < iBufferLen)
      {
        iBufferLen = iPCMByteLen;
      }
    #endif

      while(iPCMByteLen)
      {

        s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iBufferLen);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "ERROR Aout Write bytes (%d)\n",
                                        (tInt)iBufferLen));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_WRITE_RESULT_ERROR_BIT;
          break;
        }
        else
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "SUCCESS AOut Write Bytes (%d)\n",
                                        (tInt)iBufferLen));
        }
        ps8PCM += iBufferLen;
        if(iPCMByteLen < iBufferLen)
        {
          iBufferLen = iPCMByteLen;
        }
        else
        {
          iPCMByteLen -= iBufferLen;
        }
      }

      OEDT_ACOUSTICOUT_T009_bStopped = FALSE;
      /* abort command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send ABORT command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR ABORT\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_ABORT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS ABORT\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T009_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T009_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE == u32ResultBitMask)


    // delete Thread
    {
      if(OSAL_ERROR != OEDT_T009_threadID)
      {
        // thread destroys itself OSAL_s32ThreadDelete(OEDT_T009_threadID);
        OSAL_s32ThreadJoin(OEDT_T009_threadID, 1000);
        OEDT_T009_threadID = OSAL_ERROR;
      } //if(OSAL_ERROR != OEDT_T009_threadID)

    }

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR "
                                     "AOUT CLOSE handle %u "
                                     "(count %d)\n",
                                     (unsigned int)hAcousticout,
                                     iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T009_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS "
                                    "AOUT CLOSE handle %u "
                                    "(count %d)\n",
                                    (unsigned int)hAcousticout,
                                    iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T009_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T009 bit coded ERROR: "
                                   "0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T009_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T009(void)


/*****************************************************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/************************* TEST 010 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_T010_COUNT                        3
#define OEDT_ACOUSTICOUT_T010_DEVICE_NAME \
                  (OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate")
#define OEDT_ACOUSTICOUT_T010_SAMPLE_RATE                  22050
#define OEDT_ACOUSTICOUT_T010_CHANNELS                     1
#define OEDT_ACOUSTICOUT_T010_BUFFERSIZE                   4096

#define OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_T010_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_T010_THREADSPAWN_RESULT_ERROR_BIT      0x00000002
#define OEDT_ACOUSTICOUT_T010_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_T010_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_T010_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_T010_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_T010_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICOUT_T010_WRITE_RESULT_ERROR_BIT            0x00000080
#define OEDT_ACOUSTICOUT_T010_PAUSE_RESULT_ERROR_BIT            0x00000100
#define OEDT_ACOUSTICOUT_T010_STARTRESUME_RESULT_ERROR_BIT      0x00000200

#define OEDT_ACOUSTICOUT_T010_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_T010_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_T010_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_T010_bStopped = FALSE;

static void OEDT_T010_vThread(void *pvData);

static OSAL_tThreadID         OEDT_T010_threadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_T010_threadAttr = {"OEDTT010", //name
101,       //Prio
65536,     //Stacksize
(OSAL_tpfThreadEntry)OEDT_T010_vThread,
(void*)NULL};



/********************************************************************/ /**
  *  FUNCTION:      void OEDT_T010_vThread()
  *
  *  @brief         Wait-Thread (receive STOP-Event)
  *
  *  @param
  *
  *  @return   no
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static void OEDT_T010_vThread(void *pvData)
{
  tBool bRun = TRUE;
  //int iCount = 0;
  OSAL_tIODescriptor hAcousticout = *(OSAL_tIODescriptor*)pvData;
  tS32 s32Result;
  OSAL_trAcousticOutWaitEvent oWaitEvent;
  oWaitEvent.nTimeout = OSAL_C_TIMEOUT_FOREVER;
  (void)pvData;

  while(bRun)
  {
    //  iCount++;
    //ash        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is RUNNING <%s> (count %d)\n",OEDT_T010_threadAttr.szName, iCount));
    //OSAL_s32ThreadWait(1000);

    s32Result = OSAL_s32IOControl(hAcousticout,
                                  OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT,
                                  (tS32)&oWaitEvent);

    if(OSAL_OK != s32Result)
    {
      // device is closed so leave inner loop and go to outer loop
      //ash OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: Thread IOCTRL WAITEVENT canceled\n"));
      bRun = FALSE;
    }
    else //if(OSAL_OK != s32Result)
    {

      switch(oWaitEvent.enEvent)
      {
      case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
        OEDT_ACOUSTICOUT_T010_bStopped = TRUE;
        break;

      case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVTIMER:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
        break;

      case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
        //ash               OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED\n"));
        break;
      default:
        //ash              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread Unknown Event %u\n", (unsigned int)oWaitEvent.enEvent));
        ;
      } //switch(oWaitEvent.enEvent)
    } //else //if(OSAL_OK != s32Result)
  } //while(1)

  //ash    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Thread is Killed <%s>\n",OEDT_T010_threadAttr.szName));
  //OSAL_vSetErrorCode(OSAL_E_NOERROR);
  OSAL_vThreadExit();
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_T010(void)
  *
  *  @brief         Play PCM Test, wait for STOP via OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT
  *
  *  @param
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICOUT_T010(void)
#if (!defined (GEN3ARM) && !defined(GEN3X86))
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE;
  static OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;

  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T010(void)\n"));

  for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T010_COUNT; iCount++)
  {
    /* open /dev/acousticout/speech */
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T010_DEVICE_NAME, OSAL_EN_WRITEONLY);
    //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICOUT_T010_DEVICE_NAME, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_OPEN_OUT_RESULT_ERROR_BIT;
      hAcousticout = OSAL_ERROR;
    }
    else //if(OSAL_ERROR == hAcousticout)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICOUT_T010_DEVICE_NAME, (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_ERROR == hAcousticout)

    //run wait-thread
    {
      OEDT_T010_threadID = OSAL_ERROR;
      OEDT_T010_threadAttr.pvArg = &hAcousticout;
      OEDT_T010_threadID = OSAL_ThreadSpawn(&OEDT_T010_threadAttr);
      if(OSAL_ERROR == OEDT_T010_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Thread <%s> (count %d)\n",OEDT_T010_threadAttr.szName, iCount));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_THREADSPAWN_RESULT_ERROR_BIT;
      }
      else //if(OSAL_ERROR == OEDT_T010_threadID)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Thread <%s> (count %d)\n",OEDT_T010_threadAttr.szName, iCount));
      } //else //if(OSAL_ERROR == OEDT_T010_threadID)
    }

    /* configure sample rate */
    {
      OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
      rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T010_SAMPLE_RATE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                 (tS32)&rSampleRateCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETSAMPLERATE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      }
    }


    /* configure channels */
    {
      tU32 u32Channels = OEDT_ACOUSTICOUT_T010_CHANNELS;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                 (tS32)u32Channels);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETCHANNELS_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS: %u\n", (unsigned int)u32Channels));
      }
    }

    /* configure sample format */
    {
      OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
      rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
      rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                 (tS32)&rSampleFormatCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* configure buffersize */
    {
      OSAL_trAcousticBufferSizeCfg rCfg;
      rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
      rCfg.nBuffersize = OEDT_ACOUSTICOUT_T010_BUFFERSIZE;
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                 (tS32)&rCfg);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETBUFFERSIZE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
      } //else //if(OSAL_OK != s32Ret)
    }

    /* issue start command */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_START_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    } //else //if(OSAL_OK != s32Ret)




    //write some samples if no error occured before
    if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE == u32ResultBitMask)
    {
      tUInt iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array);

      tS8 *ps8PCM = (tS8*)OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array;
      tUInt iBufferLen;

      iBufferLen = OEDT_ACOUSTICOUT_T010_BUFFERSIZE;
      while(iPCMByteLen)
      {

        s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iBufferLen);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "ERROR Aout Write bytes (%d)\n",
                                        (tInt)iBufferLen));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_WRITE_RESULT_ERROR_BIT;
        }
        else
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "SUCCESS AOut Write Bytes (%d)\n",
                                        (tInt)iBufferLen));
        }
        ps8PCM += iBufferLen;
        if(iPCMByteLen < iBufferLen)
        {
          iBufferLen = iPCMByteLen;
        }
        else
        {
          iPCMByteLen -= iBufferLen;
        }
      }

      OSAL_s32ThreadWait(100);
      /* pause command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send PAUSE command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_PAUSE,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR PAUSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_PAUSE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS PAUSE\n"));
      } //else //if(OSAL_OK != s32Ret)

      OSAL_s32ThreadWait(2000);

      /* start (resume) command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send START-RESUME command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START-RESUME\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STARTRESUME_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START-RESUME\n"));
      } //else //if(OSAL_OK != s32Ret)


      OEDT_ACOUSTICOUT_T010_bStopped = FALSE;
      /* abort command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STOP_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
      } //else //if(OSAL_OK != s32Ret)

      //waiting for stop reply
      {
        int iEmergency = 50;
        while(!OEDT_ACOUSTICOUT_T010_bStopped && (iEmergency-- > 0))
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
          OSAL_s32ThreadWait(100);
        } //while(!OEDT_ACOUSTICOUT_T010_bStopped && iEmergency-- > 0)

        if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP acknowledge\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STOP_ACK_RESULT_ERROR_BIT;
        }
        else //if(iEmergency <= 0)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
        } //else //if(iEmergency <= 0)
      }

    } //if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE == u32ResultBitMask)


    // delete Thread
    {
      if(OSAL_ERROR != OEDT_T010_threadID)
      {
        // thread destroys itself OSAL_s32ThreadDelete(OEDT_T010_threadID);
        OSAL_s32ThreadJoin(OEDT_T010_threadID, 1000);
        OEDT_T010_threadID = OSAL_ERROR;
      } //if(0 != OEDT_T010_threadID)

    }

    /* close /dev/acousticout/speech */
    s32Ret = OSAL_s32IOClose(hAcousticout);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_CLOSE_OUT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    } //else //if(OSAL_OK != s32Ret)

    OSAL_s32ThreadWait(1000);
  } //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T010_COUNT; iCount++)


  if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T010 bit coded ERROR: 0x%08X\n",
                                   (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE != u32ResultBitMask)

  OSAL_s32ThreadWait(1000);
  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T010(void)

#else
{
tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE;
static OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
tS32 s32Ret;
tU32 errorcode;
int  iCount;

OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T010(void)\n"));

for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T010_COUNT; iCount++)
{
  /* open /dev/acousticout/speech */
  hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T010_DEVICE_NAME, OSAL_EN_WRITEONLY);
  //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICOUT_T010_DEVICE_NAME, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_OPEN_OUT_RESULT_ERROR_BIT;
    hAcousticout = OSAL_ERROR;
  }
  else //if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICOUT_T010_DEVICE_NAME, (unsigned int)hAcousticout, iCount));
  } //else //if(OSAL_ERROR == hAcousticout)

  //run wait-thread
  {
    OEDT_T010_threadID = OSAL_ERROR;
    OEDT_T010_threadAttr.pvArg = &hAcousticout;
    OEDT_T010_threadID = OSAL_ThreadSpawn(&OEDT_T010_threadAttr);
    if(OSAL_ERROR == OEDT_T010_threadID)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Thread <%s> (count %d)\n",OEDT_T010_threadAttr.szName, iCount));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_THREADSPAWN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == OEDT_T010_threadID)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Thread <%s> (count %d)\n",OEDT_T010_threadAttr.szName, iCount));
    } //else //if(OSAL_ERROR == OEDT_T010_threadID)
  }

  /* configure sample rate */
  {
    OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
    rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfg.nSamplerate = OEDT_ACOUSTICOUT_T010_SAMPLE_RATE;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                               (tS32)&rSampleRateCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETSAMPLERATE_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
    }
  }


  /* configure channels */
  {
    tU32 u32Channels = OEDT_ACOUSTICOUT_T010_CHANNELS;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                               (tS32)u32Channels);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETCHANNELS_RESULT_ERROR_BIT;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS: %u\n", (unsigned int)u32Channels));
    }
  }

  /* configure sample format */
  {
    OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
    rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                               (tS32)&rSampleFormatCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
    } //else //if(OSAL_OK != s32Ret)
  }

  /* configure buffersize */
  {
    OSAL_trAcousticBufferSizeCfg rCfg;
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICOUT_T010_BUFFERSIZE;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                               (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_SETBUFFERSIZE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
    } //else //if(OSAL_OK != s32Ret)
  }

  /* issue start command */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                             (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_START_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
  } //else //if(OSAL_OK != s32Ret)




  //write some samples if no error occured before
  if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE == u32ResultBitMask)
  {
    tUInt iPCMByteLen = OEDT_ACOUSTICOUT_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array);

    tS8 *ps8PCM = (tS8*)OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array;
    tUInt iBufferLen;

    iBufferLen = OEDT_ACOUSTICOUT_T010_BUFFERSIZE;
    while(iPCMByteLen)
    {

      s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iBufferLen);
      if(OSAL_ERROR == s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "ERROR Aout Write bytes (%d)\n",
                                      (tInt)iBufferLen));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_WRITE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS AOut Write Bytes (%d)\n",
                                      (tInt)iBufferLen));
      }
      ps8PCM += iBufferLen;
      if(iPCMByteLen < iBufferLen)
      {
        iBufferLen = iPCMByteLen;
      }
      else
      {
        iPCMByteLen -= iBufferLen;
      }
    }

    OSAL_s32ThreadWait(100);
    /* pause command */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send PAUSE command\n"));
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_PAUSE,
                               (tS32)NULL);

    errorcode = OSAL_u32ErrorCode();

    if(OSAL_E_NOTSUPPORTED == errorcode)
    {

      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: PAUSE operation not supported by H/w\n"));
    }
    else
    {
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR PAUSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_PAUSE_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS PAUSE\n"));
      } //else //if(OSAL_OK != s32Ret)

      OSAL_s32ThreadWait(2000);

      /* start (resume) command */
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send START-RESUME command\n"));
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START-RESUME\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STARTRESUME_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START-RESUME\n"));
      } //else //if(OSAL_OK != s32Ret)
    }

    OEDT_ACOUSTICOUT_T010_bStopped = FALSE;
    /* abort command */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP command\n"));
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STOP_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
    } //else //if(OSAL_OK != s32Ret)

    //waiting for stop reply
    {
      int iEmergency = 50;
      while(!OEDT_ACOUSTICOUT_T010_bStopped && (iEmergency-- > 0))
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
        OSAL_s32ThreadWait(100);
      } //while(!OEDT_ACOUSTICOUT_T010_bStopped && iEmergency-- > 0)

      if(iEmergency <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP acknowledge\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_STOP_ACK_RESULT_ERROR_BIT;
      }
      else //if(iEmergency <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
      } //else //if(iEmergency <= 0)
    }

  } //if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE == u32ResultBitMask)


  // delete Thread
  {
    if(OSAL_ERROR != OEDT_T010_threadID)
    {
      // thread destroys itself OSAL_s32ThreadDelete(OEDT_T010_threadID);
      OSAL_s32ThreadJoin(OEDT_T010_threadID, 1000);
      OEDT_T010_threadID = OSAL_ERROR;
    } //if(0 != OEDT_T010_threadID)

  }

  /* close /dev/acousticout/speech */
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T010_CLOSE_OUT_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
  } //else //if(OSAL_OK != s32Ret)

  OSAL_s32ThreadWait(1000);
} //for(iCount = 0; iCount < OEDT_ACOUSTICOUT_T010_COUNT; iCount++)


if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE != u32ResultBitMask)
{
  OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: T010 bit coded ERROR: 0x%08X\n",
                                 (unsigned int)u32ResultBitMask));
} //if(OEDT_ACOUSTICOUT_T010_RESULT_OK_VALUE != u32ResultBitMask)

OSAL_s32ThreadWait(1000);
return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_T010(void)
#endif

/*****************************************************************************/
/******************************TEST 011***************************************/
/****************************************************************************/
#define OSAL_EN_ACOUSTIC_DEC_INVALID OSAL_EN_ACOUSTIC_DEC_MP3 //MP3 is not supported by AcousticOut.
#define OEDT_ACOUSTICOUT_T011_CHANNELS_INVALID 4
#define OSAL_EN_ACOUSTIC_SF_INVALID 15
#define OEDT_ACOUSTICOUT_T011_BUFFERSIZE_INVALID 2000
#define OEDT_ACOUSTICOUT_T011_DEVICE_NAME \
                        (OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH)

#define OEDT_ACOUSTICOUT_T011_DEVICE_NAME_INVALID  "/dev/invalid"
#define OEDT_ACOUSTICOUT_T011_SAMPLE_RATE_MIN                          4000
#define OEDT_ACOUSTICOUT_T011_SAMPLE_RATE_MAX                          50000
#define OEDT_ACOUSTICOUT_T011_BUFFERSIZE                               1024


#define OEDT_ACOUSTICOUT_T011_RESULT_OK_VALUE                          0x00000000
#define OEDT_ACOUSTICOUT_T011_OPEN_OUT_RESULT_ERROR_BIT                0x00000001
#define OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_NULLCHECK_ERROR_BIT        0x00000002
#define OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_DECODERCHECK_ERROR_BIT   0x00000004
#define OEDT_ACOUSTICOUT_T011_SETCHANNELS_RESULT_ERROR_BIT           0x00000008
#define OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_NULLCHECK_ERROR_BIT    0x00000010
#define OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_DECODERCHECK_ERROR_BIT 0x00000020
#define OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_CHECK_ERROR_BIT        0x00000040
#define OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_NULLCHECK_ERROR_BIT      0x00000080
#define OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_DECODERCHECK_ERROR_BIT   0x00000100
#define OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_CHECK_ERROR_BIT          0x00000200
#define OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_MALLOC_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_MALLOC_ERROR_BIT       0x00000800
#define OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_MALLOC_ERROR_BIT         0x00001000
#define OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_RESULT_ERROR_BIT         0x00002000
#define OEDT_ACOUSTICOUT_T011_START_RESULT_ERROR_BIT                 0x00004000
#define OEDT_ACOUSTICOUT_T011_BUFFERSIZEPOINTER_CHECK_ERROR_BIT      0x00008000
#define OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_CHECK_ERROR_BIT          0x00010000
#define OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT                 0x00020000
#define OEDT_ACOUSTICOUT_T011_EXTWRITE_NULLCHECK_ERROR_BIT           0x00040000
#define OEDT_ACOUSTICOUT_T011_OPEN_CHECK_ERROR_BIT                   0x00080000
#define OEDT_ACOUSTICOUT_T011_CLOSE_CHECK_ERROR_BIT                  0x00100000
#define OEDT_ACOUSTICOUT_T011_NULLDEVICEPTR_CHECK_ERROR_BIT          0X00200000


/*****************************************************************************
* FUNCTION      :  OEDT_ACOUSTICOUT_T011
* PARAMETER     :  None
* RETURNVALUE   :  None
* DESCRIPTION   :  Error handler checks for acousticout component
* HISTORY       :
*  - 16-07-2012, mem4kor
*    Initial revision
*  - 24-08-2012, Niyatha Rao(nro2kor)
*    Included additional error checks for
*    Open device, Close device
*
******************************************************************************/


tU32 OEDT_ACOUSTICOUT_T011(void)
{

  tU32 u32ResultBitMask = OEDT_ACOUSTICOUT_T011_RESULT_OK_VALUE;
  static OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  tS32 s32Ret ;
  int  iCount = 0;


  OEDT_ACOUSTICOUT_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_T011(void)\n"));

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR OPEN*/
  /********************************************************************************************************************************************/
  {
    hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T011_DEVICE_NAME_INVALID, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR== hAcousticout)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_DOESNOTEXIST== (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:OPENING INAVLID DEVICE IS CORRECTLY HANDLED : \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:OPENING INAVLID DEVICE IS NOT CORRECTLY HANDLED: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_OPEN_CHECK_ERROR_BIT;
    }
  }
  /********************************************************************************************************************************************/
  /* OPEN /DEV/ACOUSTICOUT/SPEECH */
  /********************************************************************************************************************************************/
  hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_T011_DEVICE_NAME, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_ACOUSTICOUT_T011_DEVICE_NAME, iCount));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_OPEN_OUT_RESULT_ERROR_BIT;
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %u, (count %d)\n", OEDT_ACOUSTICOUT_T011_DEVICE_NAME, (unsigned int)hAcousticout, iCount));
  }

  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK FOR SET SAMPLE RATE */
  /********************************************************************************************************************************************/
  {
    /* NULL POINTER CHECK */
    OSAL_trAcousticSampleRateCfg* rSampleRateCfg = NULL;

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,(tS32)rSampleRateCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_NULLCHECK_ERROR_BIT;
    }
    rSampleRateCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticSampleRateCfg));
    if(rSampleRateCfg == OSAL_NULL)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("malloc failed"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_MALLOC_ERROR_BIT ;
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
      }
      return u32ResultBitMask;
    }
    /*CODEC CHECK */
    rSampleRateCfg->enCodec  = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_DEC_INVALID; /*invalid codec */
    s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,(tS32)rSampleRateCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_TEMP_NOT_AVAILABLE ==(tU32) s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_DECODERCHECK_ERROR_BIT;
    }
    /* SAMPLERATE CHECK BELOW MINIMUM VALUE */
    rSampleRateCfg->enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfg->nSamplerate = OEDT_ACOUSTICOUT_T011_SAMPLE_RATE_MIN; /* invalid smapling rate */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,(tS32)rSampleRateCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32) s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL:\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_CHECK_ERROR_BIT;
    }
    /* SAMPLERATE CHECK ABOVE MAXIMUM VALUE */
    rSampleRateCfg->enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCfg->nSamplerate = OEDT_ACOUSTICOUT_T011_SAMPLE_RATE_MAX; /* invalid smapling rate */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,(tS32)rSampleRateCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32) s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLERATE CHECK IS NOT HANDLED IN THE SET_SAMPLE_RATE IOCTRL CALL:\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLERATE_CHECK_ERROR_BIT;
    }
    OSAL_vMemoryFree(rSampleRateCfg);
  }
  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK FOR SET CHANNELS */
  /********************************************************************************************************************************************/
  {
    tU32 u32Channels = OEDT_ACOUSTICOUT_T011_CHANNELS_INVALID;/*invalid number of channels */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,(tS32)u32Channels);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: CHANNEL CHECK IS CORRECTLY HANDLED IN THE SETCHANNELS IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC:CHANNEL CHECK IS NOT HANDLED IN THE SETCHANNELS IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETCHANNELS_RESULT_ERROR_BIT;
    }
  }

  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR  SAMPLE FORMAT */
  /********************************************************************************************************************************************/
  {
    /* NULL POINTER CHECK */
    OSAL_trAcousticSampleFormatCfg* rSampleFormatCfg=NULL;

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
      u32ResultBitMask |=OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_NULLCHECK_ERROR_BIT;
    }
    rSampleFormatCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticSampleFormatCfg));
    if(rSampleFormatCfg == OSAL_NULL)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("malloc failed"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_MALLOC_ERROR_BIT;
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
      }
      return u32ResultBitMask;
    }
    /*CODEC CHECK */
    rSampleFormatCfg->enCodec = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_DEC_INVALID;/*invalid codec */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_TEMP_NOT_AVAILABLE ==(tU32) s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_DECODERCHECK_ERROR_BIT;
    }
    /* SAMPLEFORMAT CHECK */
    rSampleFormatCfg->enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCfg->enSampleformat = (OSAL_tenAcousticSampleFormat)OSAL_EN_ACOUSTIC_SF_INVALID;/*Invalid sampleformat */

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,(tS32)rSampleFormatCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:SAMPLE FORMAT CHECK IS CORRECTLY HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: :SAMPLE FORMAT CHECK IS NOT HANDLED IN THE SET_SAMPLEFORMAT IOCTRL CALL:  \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETSAMPLEFORMAT_CHECK_ERROR_BIT;
    }
    OSAL_vMemoryFree(rSampleFormatCfg);
  }
  /********************************************************************************************************************************************/
  /* ERROR HANDLER CHECK BUFFERSIZE */
  /********************************************************************************************************************************************/
  {
    /* NULL POINTER CHECK */
    OSAL_trAcousticBufferSizeCfg* rCfg = NULL;

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,(tS32)rCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32) s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: :NULL CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_NULLCHECK_ERROR_BIT;
    }
    rCfg = OSAL_pvMemoryAllocate(sizeof(OSAL_trAcousticBufferSizeCfg));
    if(rCfg == OSAL_NULL)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("malloc failed"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_MALLOC_ERROR_BIT;
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
      }
      return u32ResultBitMask;
    }
    /*CODEC CHECK */
    rCfg->enCodec  = (OSAL_tenAcousticCodec)OSAL_EN_ACOUSTIC_DEC_INVALID;/*invalid codec */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,(tS32)rCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:DECODER CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : DECODER CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_DECODERCHECK_ERROR_BIT ;
    }
    /*BUFFERSIZE CHECK */
    rCfg->enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg->nBuffersize = OEDT_ACOUSTICOUT_T011_BUFFERSIZE_INVALID;/* invalid Buffersize */
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,(tS32)rCfg);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFERSIZE CHECK IS CORRECTLY HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFERSIZE CHECK IS NOT HANDLED IN THE SETBUFFERSIZE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_CHECK_ERROR_BIT ;
    }
    OSAL_vMemoryFree(rCfg);
  }

  /********************************************************************************************************************************************/
  /*BUFFERSIZE AND BUFFER POINTER CHECKS DURING WRITE OPERATION*/
  /********************************************************************************************************************************************/
  {
    OSAL_trAcousticBufferSizeCfg rCfg;
    rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = OEDT_ACOUSTICOUT_T011_BUFFERSIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,(tS32)&rCfg);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_SETBUFFERSIZE_RESULT_ERROR_BIT;
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
      }
      return u32ResultBitMask;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
    }
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,(tS32)NULL);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_START_RESULT_ERROR_BIT;
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
      }
      else
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
      }
      return u32ResultBitMask;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
    }

    /* NULL DEVICE POINTER CHECK */
    tS8 *ps8PCM = (tS8*)OEDT_u8Acoustic_dtmf_22050Hz_mono_16bit_LE_Array;
    int iBufferLen = OEDT_ACOUSTICOUT_T011_BUFFERSIZE_INVALID;/* invalid Buffersize */
    s32Ret = OSAL_s32IOWrite((OSAL_tIODescriptor)NULL, ps8PCM, (tU32)iBufferLen);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL DEVICE POINTER CHECK IS CORRECTLY HANDLED IN THE OSAL_s32IOWrite CALL : \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : NULL DEVICE POINTER CHECK IS NOT HANDLED IN THE OSAL_s32IOWrite CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_NULLDEVICEPTR_CHECK_ERROR_BIT ;
    }

    /* BUFFER POINTER CHECK */
    ps8PCM=NULL;
    iBufferLen = OEDT_ACOUSTICOUT_T011_BUFFERSIZE;
    s32Ret = OSAL_s32IOWrite(hAcousticout, ps8PCM, (tU32)iBufferLen);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFERPOINTER CHECK IS CORRECTLY HANDLED IN THE OSAL_s32IOWrite CALL : \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFERPOINTER CHECK IS NOT HANDLED IN THE OSAL_s32IOWrite CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_BUFFERSIZEPOINTER_CHECK_ERROR_BIT ;
    }

  }
  /********************************************************************************************************************************************/
  /*BUFFERSIZE AND BUFFER POINTER AND NULL CHECKS DURING  EXTRENAL WRITE OPERATION */
  /********************************************************************************************************************************************/
  {
    /* NULL POINTER CHECK */
    OSAL_trAcousticOutWrite *writeinfo = NULL;
    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_EXTWRITE,(tS32)writeinfo);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: NULL CHECK IS CORRECTLY HANDLED IN THE EXTWRITE IOCTRL CALL: \n"));

      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC:NULL CHECK IS  NOT HANDLED IN THE EXTWRITE IOCTRL CALL: \n"));
      u32ResultBitMask |=OEDT_ACOUSTICOUT_T011_EXTWRITE_NULLCHECK_ERROR_BIT;
    }

    /* BUFFER POINTER CHECK */
    OSAL_trAcousticOutWrite extwriteinfo;
    extwriteinfo.pvBuffer = NULL;
    extwriteinfo.u32BufferSize=OEDT_ACOUSTICOUT_T011_BUFFERSIZE;
    extwriteinfo.nTimeout= 10000;
    extwriteinfo.u32PhraseStartEvtId= 0;
    extwriteinfo.enBufStatus=OSAL_EN_ACOUSTIC_DATA_VALID;
    extwriteinfo.bEpisodeComplete= 1;

    s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_EXTWRITE,(tS32)&extwriteinfo);

    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE ==(tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:BUFFER POINTER CHECK IS CORRECTLY HANDLED IN THE EXTWRITE IOCTRL CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: : BUFFERPOINTER CHECK IS NOT HANDLED IN THE EXTWRITE IOCTRL CALL: \n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_BUFFERSIZEPOINTER_CHECK_ERROR_BIT ;
    }
  }
  /********************************************************************************************************************************************/
  /*ERROR HANDLER CHECK FOR CLOSE OPERATION*/
  /********************************************************************************************************************************************/
  {
    s32Ret = OSAL_s32IOClose((OSAL_tIODescriptor)NULL);
    if(OSAL_ERROR== s32Ret)
    {
      s32Ret = (tS32)OSAL_u32ErrorCode();
      if(OSAL_E_INVALIDVALUE == (tU32)s32Ret)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS CORRECTLY HANDLED IN THE OSAL_s32IOClose CALL: \n"));
      }
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC:NULL CHECK IS NOT HANDLED IN THE OSAL_s32IOClose CALL:\n"));
      u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_CHECK_ERROR_BIT;
    }
  }
  /********************************************************************************************************************************************/
  /* CLOSE /DEV/ACOUSTICOUT/SPEECH */
  /********************************************************************************************************************************************/
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR CLOSE\n"));
    u32ResultBitMask |= OEDT_ACOUSTICOUT_T011_CLOSE_RESULT_ERROR_BIT;
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS CLOSE\n"));
  }

  return u32ResultBitMask;
}





/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/*100825 srt2hi . first Tests on Linux */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */
/* ******************************************************************************************************* */


#define OEDT_ACOUSTICOUT_WAVEOUTPUT_BUFFER_SIZE 8192
// #define OEDT_ACOUSTICOUT_WAVEOUTPUT_PCM_DEVICE_NAME  "AdevPromptOut"
#define OEDT_ACOUSTICOUT_WAVEOUTPUT_PCM_DEVICE_NAME  "devacout"

static snd_pcm_stream_t stream = SND_PCM_STREAM_PLAYBACK;
static snd_pcm_t *playback_handle = NULL;
unsigned char buf[OEDT_ACOUSTICOUT_WAVEOUTPUT_BUFFER_SIZE];

static void test_PCM(unsigned int uiSR, unsigned int uiChannels, const char *pcszPCM)
{
  int iRet;
  int iFD_pcm_file;
  const char *pcszFileName = pcszPCM; //"/opt/bosch/knorkator_44khz_stereo_16le.pcm";
  //const char *pcszFileName = "/opt/bosch/knorkator_16khz_mono_16le.pcm";
  //unsigned int uiChannels = 1;
  int err;
  char device[] = {OEDT_ACOUSTICOUT_WAVEOUTPUT_PCM_DEVICE_NAME};
  //char device[] = {"my_rate_convert"};
  //char device[64] = {"hw:0,0"};
  snd_pcm_hw_params_t *hw_params;
  unsigned int uiSRnear = uiSR; //samplerate
  unsigned int uiSRmin;
  unsigned int uiSRmax;
  int iDir;


  OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:open file <%s>\n", pcszFileName));

  iFD_pcm_file = open(pcszFileName, O_RDWR); //>=0 success
  OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:open file\t%i\n", iFD_pcm_file));








  if((err = snd_pcm_open (&playback_handle, device/*argv[1]*/, stream, 0)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot open audio device %s (%s)\n",
                                  device,
                                  snd_strerror (err)));
  }

  if((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot allocate hardware parameter structure (%s)\n",
                                  snd_strerror (err)));
  }

  if((err = snd_pcm_hw_params_any (playback_handle, hw_params)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot initialize hardware parameter structure (%s)\n",
                                  snd_strerror (err)));
  }

  uiSRmin = 8000;
  uiSRmax = 48000;
  /*if ((err = snd_pcm_hw_params_set_rate_minmax (playback_handle, hw_params, &uiSRmin, 0, &uiSRmax, 0)) < 0) {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set sample minmax (%s)\n",
           snd_strerror (err));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: SUCCESS RATE minmax %u - %u\n", uiSRmin, uiSRmax));
  }*/


  if((err = snd_pcm_hw_params_set_access (playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set access type (%s)\n",
                                  snd_strerror (err)));
  }

  //if ((err = snd_pcm_hw_params_set_format (playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
  if((err = snd_pcm_hw_params_set_format (playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set sample format (%s)\n",
                                  snd_strerror (err)));
  }

  if((err = snd_pcm_hw_params_set_channels (playback_handle, hw_params, uiChannels)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set_channels count (%s)\n",
                                  snd_strerror (err)));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: SUCCESS set_channels - Channels = %u\n", uiChannels));
  }



  //1== ON
  /*
      if ((err = snd_pcm_hw_params_set_rate_resample (playback_handle, hw_params, 1)) < 0) {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set rate-resample (%s)\n",
               snd_strerror (err)));
      }*/



  if((err = snd_pcm_hw_params_set_rate_near (playback_handle, hw_params, &uiSRnear, NULL)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set sample rate near (%s)\n",
                                  snd_strerror (err)));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: SUCCESS RATE NEAR %u\n", uiSRnear));
  }

  /*if ((err = snd_pcm_hw_params_set_rate (playback_handle, hw_params, uiSRnear, 0)) < 0) {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set sample rate (%s)\n",
           snd_strerror (err)));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: SUCCESS RATE %u\n", uiSRnear));
  }*/



  /*{
    snd_pcm_uframes_t uiFrames = uiChannels * 2 * 8;  //Bits je Frame
    int iDir = 0;
    if ((err = snd_pcm_hw_params_set_period_size_near (playback_handle, hw_params, &uiFrames, &iDir)) < 0) {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set period size near (%s)\n",
             snd_strerror (err)));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: SUCCESS PERIOD SIZE %u\n", (unsigned int)uiFrames));
    }
  }*/




  //alsa_pcm_hw_params_print_debug(hw_params);

  if((err = snd_pcm_hw_params (playback_handle, hw_params)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot set parameters (%s)\n",
                                  snd_strerror (err)));
  }


  if((err = snd_pcm_prepare (playback_handle)) < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot prepare audio interface for use (%s)\n",
                                  snd_strerror (err)));
  }

  /*if ((err = snd_pcm_start (playback_handle)) < 0) {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot start audio interface for use (%s)\n",
           snd_strerror (err)));
  }*/

  // CHECK config

  //SAMPLERATE
  {
    if((err = snd_pcm_hw_params_get_rate (hw_params, &uiSRnear, &iDir)) < 0)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot get sample rate (%s)\n",
                                    snd_strerror (err)));
    }

    if((err = snd_pcm_hw_params_get_rate_min (hw_params, &uiSRmin, &iDir)) < 0)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot get sample rate min (%s)\n",
                                    snd_strerror (err)));
    }

    if((err = snd_pcm_hw_params_get_rate_max (hw_params, &uiSRmax, &iDir)) < 0)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:cannot get sample rate max (%s)\n",
                                    snd_strerror (err)));
    }
    OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:RATE %u (%u .. %u)\n", uiSRnear, uiSRmin, uiSRmax));
  }



  {

    int iRead = 0;
    unsigned int uiSamples;
    do
    {
      iRead = read(iFD_pcm_file, buf, OEDT_ACOUSTICOUT_WAVEOUTPUT_BUFFER_SIZE);
      //OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:Read %d\n", iRead);
      if(iRead <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:read error\t%i\n", iRead));
      }
      else
      {
        uiSamples = ((unsigned int) iRead / 2) / uiChannels;
        err = snd_pcm_writei(playback_handle,
                             buf,
                             (snd_pcm_uframes_t)uiSamples);
        if(err != (int)uiSamples)
        {

          OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT: "
                                        "write to audio "
                                        "interface failed (%s)\n",
                                        snd_strerror (err)));
        }
      }
    }
    while(iRead > 0 && err > 0);
  }

  sleep(1);

  snd_pcm_drain (playback_handle);
  snd_pcm_close (playback_handle);
  OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:snd_pcm_close\n"));

  snd_pcm_hw_params_free (hw_params);
  OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:snd_pcm_hw_params_free\n"));

  iRet = close(iFD_pcm_file); //0 == success
  OEDT_ACOUSTICOUT_PRINTF_INFO(("ACOUSTOUT:close file\t%i\n", iRet));


}



#define OEDT_AOIT_PCMBASIC_BUFFER_SIZE    8192
static tS8  as8Buffer[OEDT_AOIT_PCMBASIC_BUFFER_SIZE];

#define OEDT_AOIT_AMRBASIC_BUFFER_SIZE    512
static tS8  as8AMRBuffer[OEDT_AOIT_AMRBASIC_BUFFER_SIZE];



static void test_AOUT()
{
  OSAL_tIODescriptor hAcousticout;
  tS32 s32Ret;
  /* open /dev/acousticout/music */
  hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Open\n"));
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open %u\n", (unsigned int)hAcousticout));

  }

  /* get supported samplerates*/
  {
    OSAL_trAcousticSampleRateCapability rSampleRateCap;
    OSAL_tAcousticSampleRate rFrom[SR_SIZE];
    OSAL_tAcousticSampleRate rTo[SR_SIZE];

    rSampleRateCap.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleRateCap.pnSamplerateFrom = rFrom;
    rSampleRateCap.pnSamplerateTo   = rTo;
    rSampleRateCap.u32ElemCnt       = SR_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLERATE,
                               (tS32)&rSampleRateCap);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetSampleRateCap\n"));
    }
    else
    {
      int i;
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetSampleRateCap\n"));
      for(i = 0; i < (int)rSampleRateCap.u32ElemCnt; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\tRate %02i = <%u> - <%u>\n", i, (unsigned int)rSampleRateCap.pnSamplerateFrom[i],(unsigned int)rSampleRateCap.pnSamplerateTo[i]));
      }

    }
  }

  /* get supported sampleformat*/
  {
    OSAL_trAcousticSampleFormatCapability rSampleFormatCap;
    OSAL_tenAcousticSampleFormat          rSF[SR_SIZE];

    rSampleFormatCap.enCodec           = OSAL_EN_ACOUSTIC_DEC_PCM;
    rSampleFormatCap.penSampleformats  = rSF;
    rSampleFormatCap.u32ElemCnt        = SF_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLEFORMAT,
                               (tS32)&rSampleFormatCap);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetSampleFormatCap\n"));
    }
    else
    {
      int i;
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetSampleFormatCap\n"));
      for(i = 0; i < (int)rSampleFormatCap.u32ElemCnt; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\tFormat %02i = <%u>\n", i, (unsigned int)rSampleFormatCap.penSampleformats[i]));
      }

    }
  }

  /* get supported decoders*/
  {
    OSAL_trAcousticCodecCapability      rCap;
    OSAL_tenAcousticCodec               rDE[DE_SIZE];

    rCap.penCodecs                     = rDE;
    rCap.u32ElemCnt                    = DE_SIZE;
    rCap.u32MaxCodecCnt                = DE_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_DECODER,
                               (tS32)&rCap);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetDecoderCap\n"));
    }
    else
    {
      int i;
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetDecoderCap MaxDecoders %u\n", (unsigned int)rCap.u32MaxCodecCnt));
      for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\tDecoder %02i = <%u>\n", i, (unsigned int)rCap.penCodecs[i]));
      }

    }
  }

  /* get supported channels*/
  {
    OSAL_trAcousticChannelCapability rCap;
    tU32 u32Num[CH_SIZE];
    rCap.pu32NumChannels = u32Num;
    rCap.u32ElemCnt      = CH_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_CHANNELS,
                               (tS32)&rCap);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetChannelCap %u\n", (unsigned int)rCap.u32ElemCnt));
    }
    else
    {
      int i;
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetChannelCap %u\n", (unsigned int)rCap.pu32NumChannels));
      for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\tChannels %02i = <%u>\n", i, (unsigned int)rCap.pu32NumChannels[i]));
      }
    }
  }


  /* get supported buffersizes*/
  {
    OSAL_trAcousticBufferSizeCapability  rCap;
    OSAL_tAcousticBuffersize             rBS[BS_SIZE];

    rCap.enCodec           = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCap.pnBuffersizes     = rBS;
    rCap.u32ElemCnt        = BS_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_BUFFERSIZE,
                               (tS32)&rCap);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetBufferSizeCap\n"));
    }
    else
    {
      int i;
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetBufferSizeCap\n"));
      for(i = 0; i < (int)rCap.u32ElemCnt; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("\tBufferSizet %02i = <%u>\n", i, (unsigned int)rCap.pnBuffersizes[i]));
      }

    }
  }


  /* get parameter */
  {
    OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
    rSampleFormatCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSAMPLEFORMAT,
                               (tS32)&rSampleFormatCfg);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR GetSampleFormat\n"));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS GetSampleFormat: %d\n", (int)rSampleFormatCfg.enSampleformat));
    }
  }


  /* close acousticout */
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Close\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Close\n"));
  }



}





/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback
*DESCRIPTION: used as device callback function for device test
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     19.09.05  Robert Schedel, 3SOFT
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static OSAL_trAcousticErrThrCfg rCbLastErrThr_Temp;
static tBool AOUT_bStopped = FALSE;
static tVoid vAcousticOutTstCallback_Temp (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
  (void)pvCookie;
  (void)pvAddData;
  /*tS32 s32Ret = OSAL_OK;

  s32Ret = OSAL_s32EventPost(hAcTstCbEvt,
  (1 << enCbReason),
  OSAL_EN_EVENTMASK_OR);*/

  //pvCbLastCookie = pvCookie;

  switch(enCbReason)
  {
  case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
    AOUT_bStopped = TRUE;
    /* no additional data */
    //ash u32CbAudioStopped++;
    break;

    //case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED:
    //episode stop event received
    //gu32CbEpisodeStopped++;
    //break;

  case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
    //ash u32CbErrThrCnt++;
    rCbLastErrThr_Temp = *(OSAL_trAcousticErrThrCfg*)pvAddData;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n",\
                                  (unsigned int)rCbLastErrThr_Temp.enErrType, (unsigned int)rCbLastErrThr_Temp.enErrType));
    switch(rCbLastErrThr_Temp.enErrType)
    {
    case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
      //ash u32CbErrXrunCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
      //ash u32CbErrBitstreamCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
      //ash u32CbErrNoValidDataCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
      //ash u32CbErrWrongFormatCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
      //ash u32CbErrInternalCnt++;
      break;

    case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
      //ash u32CbErrFatalCnt++;
      break;

    default:
      /* unknown error type */
      break;
    }
    break;

  case OSAL_EN_ACOUSTICOUT_EVTIMER:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
    //ash u32CbTimerCnt++;
    //ash rCbLastTimestamp = *(OSAL_trAcousticTimestamp*)pvAddData;
    break;

  case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
    //ash rCbLastMarker = *(OSAL_trAcousticOutMarkerEventInfo*)pvAddData;
    //ash u32CbEventMarkerCnt++;
    break;

  case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
    break;

  case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
    break;




  default: /* callback unsupported by test code */
    OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
    break;
  }
}


/******************************************FunctionHeaderBegin************
*FUNCTION:    u32AcousticoutPCMBasicTest
*DESCRIPTION:
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     31.08.05  Bernd Schubart, 3SOFT
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tU32 test_PCMBasic(unsigned int uiSR, unsigned int uiChannels, const char *pcszPCM)
{
  int iFD_pcm_file;
  int iRet;
  tU32 u32Channels = uiChannels;

  //OSAL_tIODescriptor hPCMFile = OSAL_ERROR,
  OSAL_tIODescriptor hAcousticout;

  //tS32 s32FileSize;
  //tS32 s32ReadBytesTotal = 0;
  //tS32 s32ReadBytes = 0;
  tS32 s32Ret;
  //tS32 s32Streamtype;
  OSAL_trAcousticSampleRateCfg rSampleRateCfg;
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  OSAL_trAcousticOutCallbackReg rCallbackReg;



  /* open /dev/acousticout/speech */
  //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
  hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Open\n"));
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open %u\n", (unsigned int)hAcousticout));

  }


  /* register callback function */
  {
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback_Temp;
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                               (tS32)&rCallbackReg);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
      hAcousticout = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

    }
  }



  /* open PCM file */
  //hPCMFile = OSAL_IOOpen(pcszPCM, OSAL_EN_READONLY);
  //if(OSAL_ERROR == hPCMFile)
  iFD_pcm_file = open(pcszPCM, O_RDWR); //>=0 success
  if(iFD_pcm_file < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Open File <%s>\n", pcszPCM));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open File <%s>\n", pcszPCM));
  }

  /* configure sample rate */
  rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleRateCfg.nSamplerate = uiSR;
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                             (tS32)&rSampleRateCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLERATE: %u\n", (unsigned int)rSampleRateCfg.nSamplerate));
  }


  /* configure channels */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                             (tS32)u32Channels);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR SETCHANNELS: %u\n", (unsigned int)u32Channels));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCHANNELS: %u\n", (unsigned int)u32Channels));
  }

  /* configure sample format */
  rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
  rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                             (tS32)&rSampleFormatCfg);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETSAMPLEFORMAT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
  }

  /* configure buffersize */
  {
    OSAL_trAcousticBufferSizeCfg rCfg;

    rCfg.enCodec    = OSAL_EN_ACOUSTIC_DEC_PCM;
    rCfg.nBuffersize = OEDT_AOIT_PCMBASIC_BUFFER_SIZE;

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                               (tS32)&rCfg);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETBUFFERSIZE: %u\n", (unsigned int)rCfg.nBuffersize));
    }
  }

  /* issue start command */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                             (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR START\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
  }


  /* read file and write to acousticout */
  {

    int iRead = 0;
    //int iSamples;
    do
    {
      iRead = read(iFD_pcm_file, as8Buffer, OEDT_AOIT_PCMBASIC_BUFFER_SIZE);
      //OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT:Read %d\n", iRead));
      if(iRead <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT:read error\t%i\n", iRead));
      }
      else
      {

        s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)iRead);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Aout Write bytes (%u)\n", (unsigned int)iRead));
        }
        else
        {
          //   OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)iRead));
        }
      }
    }
    while(iRead > 0 && OSAL_ERROR != s32Ret);
  }

  /* read file and write to acousticout */
  /*    s32FileSize = OSALUTIL_s32FGetSize(hPCMFile);

      while(s32ReadBytesTotal < s32FileSize)
      {
          s32ReadBytes = OSAL_s32IORead(hPCMFile, as8Buffer, OEDT_AOIT_PCMBASIC_BUFFER_SIZE);

          s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

          if(OSAL_ERROR == s32Ret)
          {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Aout Write bytes (%u)\n", (unsigned int)s32ReadBytes));
          }
          else
          {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)s32ReadBytes));
          }

          s32ReadBytesTotal += s32ReadBytes;
      }*/


  /* issue abort command /should call alsa DRAIN*/
    #if 0
  if(0)
  {
    OSAL_s32ThreadWait(2000);
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Send ABORT\n"));
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR ABORT\n"));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS ABORT\n"));
    }
  }
    #endif



  AOUT_bStopped = FALSE;
  /* issue stop command */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                             (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR STOP\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
  }

  {
    int iEmergency = 50;
    while(!AOUT_bStopped && iEmergency-- > 0)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
      OSAL_s32ThreadWait(100);
    }
  }
  OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));

  /* close pcm file */

  //s32Ret = OSAL_s32IOClose(hPCMFile);
  //if(OSAL_OK != s32Ret)
  iRet = close(iFD_pcm_file); //0 == success
  if(iRet != 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR FILE CLOSE\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS FILE CLOSE\n"));
  }

  /* close acousticout */
  OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Wait befor CLOSE\n"));
  //wait a little, to ensure audio playback is finished
  //normally, should wait for EVENT_AUDIO_STOPPED

  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR AOUT CLOSE\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE\n"));
  }
  return 0;
}





/******************************************FunctionHeaderBegin************
*FUNCTION:    u32AcousticoutAMRBasicTest
*DESCRIPTION:
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     31.08.05  Bernd Schubart, 3SOFT
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tU32 test_AMRBasic(unsigned int uiSR, unsigned int uiChannels, const char *pcszAMR)
{
  int iFD_pcm_file;
  int iRet;
  OSAL_tIODescriptor hAcousticout;
  //OSAL_tIODescriptor hPCMFile =  OSAL_ERROR, hAcousticout = OSAL_ERROR;
  //, hAudiorouter = OSAL_ERROR;

  //tS32 s32FileSize;
  //tS32 s32ReadBytesTotal = 0;
  //tS32 s32ReadBytes = 0;
  tS32 s32Ret;
  //tS32 s32Streamtype;
  //OSAL_trAcousticSampleRateCfg rSampleRateCfg;
  //OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  OSAL_trAcousticOutCallbackReg rCallbackReg;

  (void)uiSR;
  (void)uiChannels;

  /* open /dev/acousticout/speech */
  hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hAcousticout)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Open\n"));
    hAcousticout = OSAL_ERROR;
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open %u\n", (unsigned int)hAcousticout));

  }


  /* register callback function */
  {
    rCallbackReg.pfEvCallback = vAcousticOutTstCallback_Temp;
    rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                               (tS32)&rCallbackReg);

    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
      hAcousticout = OSAL_ERROR;
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

    }
  }


  /* open PCM file */
  //hPCMFile = OSAL_IOOpen(pcszAMR, OSAL_EN_READONLY);
  //if(OSAL_ERROR == hPCMFile)
  iFD_pcm_file = open(pcszAMR, O_RDWR); //>=0 success
  if(iFD_pcm_file < 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Open File <%s>\n", pcszAMR));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open File <%s>\n", pcszAMR));
  }

  /* set amr decoder */
  {
    OSAL_tenAcousticCodec enAcousticCodec;
    enAcousticCodec = OSAL_EN_ACOUSTIC_DEC_AMRWB;

    /* signal need for AMR decoder */
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_NEXTNEEDEDDECODER,
                               (tS32)enAcousticCodec);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR NEXTNEEDEDDECODER\n"));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS NEXTNEEDEDDECODER\n"));
    }

    /* wait a little */
    OSAL_s32ThreadWait(250);

    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETDECODER,
                               (tS32)enAcousticCodec);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR SETCODEC AMR\n"));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS SETCODEC AMR WB\n"));
    }
  }


  /* issue start command */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                             (tS32)NULL);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR START\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
  }


  /* read file and write to acousticout */
  {

    int iRead = 0;
    //int iSamples;
    do
    {
      iRead = read(iFD_pcm_file, as8AMRBuffer, OEDT_AOIT_AMRBASIC_BUFFER_SIZE);
      //OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT:Read %d\n", iRead);
      if(iRead <= 0)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("AOUT:read error\t%i\n", iRead));
      }
      else
      {

        s32Ret = OSAL_s32IOWrite(hAcousticout, as8AMRBuffer, (tU32)iRead);
        if(OSAL_ERROR == s32Ret)
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Aout Write AMR bytes (%u)\n", (unsigned int)iRead));
        }
        else
        {
          OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write AMR Bytes (%u)\n", (unsigned int)iRead));
        }
      }
    }
    while(iRead > 0 && OSAL_ERROR != s32Ret);
  }

  /* read file and write to acousticout */
  /*    s32FileSize = OSALUTIL_s32FGetSize(hPCMFile);

      while(s32ReadBytesTotal < s32FileSize)
      {
          s32ReadBytes = OSAL_s32IORead(hPCMFile, as8Buffer, OEDT_AOIT_PCMBASIC_BUFFER_SIZE);

          s32Ret = OSAL_s32IOWrite(hAcousticout, as8Buffer, (tU32)s32ReadBytes);

          if(OSAL_ERROR == s32Ret)
          {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Aout Write bytes (%u)\n", (unsigned int)s32ReadBytes));
          }
          else
          {
              OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)s32ReadBytes));
          }

          s32ReadBytesTotal += s32ReadBytes;
      }*/


  /* issue abort command /should call alsa DRAIN*/
    #if 0
  if(0)
  {
    OSAL_s32ThreadWait(2000);
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Send ABORT\n"));
    s32Ret = OSAL_s32IOControl(hAcousticout,
                               OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT,
                               (tS32)NULL);
    if(OSAL_OK != s32Ret)
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR ABORT\n"));
    }
    else
    {
      OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS ABORT\n"));
    }
  }
    #endif //#if 0




  /* issue stop command */
  s32Ret = OSAL_s32IOControl(hAcousticout,
                             OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                             (tS32)NULL);

  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR STOP\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
  }



  /* close pcm file */

  //s32Ret = OSAL_s32IOClose(hPCMFile);
  //if(OSAL_OK != s32Ret)
  iRet = close(iFD_pcm_file); //0 == success
  if(iRet != 0)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR FILE CLOSE\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS FILE CLOSE\n"));
  }

  /* close acousticout */
  OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: Wait befor CLOSE\n"));
  //wait a little, to ensure audio playback is finished
  //normally, should wait for EVENT_AUDIO_STOPPED
  OSAL_s32ThreadWait(10000);
  s32Ret = OSAL_s32IOClose(hAcousticout);
  if(OSAL_OK != s32Ret)
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR AOUT CLOSE\n"));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOUT CLOSE\n"));
  }
  return 0;
}





tS32 OEDT_AcousticOut_Main(int iTestNumber, const char *pcszUser1, const char *pcszUser2, const char *pcszUser3)
{
  unsigned int uiSR          = 44100;
  unsigned int uiChannels    = 2;
  const char *pcszFile       = NULL;
  int iRet                   = 0;

  if(pcszUser1!=NULL && pcszUser2!=NULL)
  {
    uiSR           = (unsigned int)atoi(pcszUser1);
    uiChannels     = (unsigned int)atoi(pcszUser2);
    pcszFile       = pcszUser3;
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_AcousticOut_Main: INFO Test# %d, Samplerate %u, Channels %u, File <%s>\n",
                                  iTestNumber,
                                  uiSR,
                                  uiChannels,
                                  pcszFile));
  }
  else
  {
    OEDT_ACOUSTICOUT_PRINTF_INFO(("OEDT_AcousticOut_Main: ERROR Test# %d, Samplerate %u, Channels %u, File <%s>\n",
                                  iTestNumber,
                                  uiSR,
                                  uiChannels,
                                  pcszFile));
  }


  switch(iTestNumber)
  {
  case 0:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("TEST 0  test_PCM (Ausgabe �ber ALSA)\n"));
    test_PCM(uiSR, uiChannels, pcszFile);      //play with AlsaLib
    break;

  case 1:
    {
      int i;
      for(i = 0; i < 4; ++i)
      {
        OEDT_ACOUSTICOUT_PRINTF_INFO(("TEST 1 %d test_PCMBasic (Ausgabe �ber DEV_AOUT)\n",i));
        test_PCMBasic(uiSR, uiChannels, pcszFile);    // via DEV_ACousticOut
      }
    }
    break;

  case 2:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("TEST 2  test_AOUT (Print capabilities)\n"));
    test_AOUT();  //print capabilities
    break;

  case 3:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("TEST 3  test_AMRBasic (Ausgabe �ber DEV_AOUT)\n"));
    test_AMRBasic(uiSR, uiChannels, pcszFile);    // via DEV_ACousticOut
    break;



  default:
    OEDT_ACOUSTICOUT_PRINTF_INFO(("SYNTAX: 0 48000 /opt/bosch/pcm.pcm\n"));
    break;
  }

  return iRet;
}

