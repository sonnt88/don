/*******************************************************************************
*                                                                               
* FILE:         oedt_spm_TestFuncs.c
*                                                                              
* SW-COMPONENT: OEDT-Framework
*                                                                              
* PROJECT:      ADIT Gen2 Platform
*                                                                              
* DESCRIPTION:  This file implements testcases for ...
*
*                 - DEV_VOLT
*
* AUTHORS:      Suresh Dhandapani
*                                                                               
* COPYRIGHT:    (c) 2012 Robert Bosch Engineering and Business solutions Limited
*                                                                               
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_helper_funcs.h"
#include "oedt_testing_macros.h"
#include "oedt_teststate.h"
#include "oedt_spm_TestFuncs.h"

// Actually there is no prototype defined for the function OSAL_s32ThreadJoin().
// So I have to define it myself until this is fixed.
extern tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec);
static tVoid vSPM_OverVoltageListenerThread(tPVoid pvArg);
static tVoid vSPM_StartOverVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static tVoid vSPM_StopOverVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static tVoid vSPM_StartUserVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static tVoid vSPM_StopUserVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static tVoid vSPM_UserVoltageListenerThread(tPVoid pvArg);
static tVoid vSPM_CVMLowVoltageListenerThread(tPVoid pvArg);
static tVoid vSPM_StartCVMLowVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static tVoid vSPM_StopCVMLowVoltageListenerThread(OEDT_TESTSTATE* pteststate);
static OSAL_tThreadID g_s32ThreadIdLowVoltageListener  = OSAL_ERROR;
/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/
#define OEDT_SPM_C_STRING_VOLTAGE_HISTORY_EVENT_NAME             "VOLTAGE_HISTORY"
#define OEDT_SPM_C_STRING_OVER_VOLTAGE_LISTENER_EVENT_NAME       "SPM_OVER_VOLTAGE_LISTENER"
#define OEDT_SPM_C_STRING_USER_VOLTAGE_LISTENER_EVENT_NAME       "SPM_USER_VOLTAGE_LISTENER"
#define OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED  0x00000001
#define OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP     0x00000002

#define OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASKS (OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED | \
   OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP)

#define OEDT_SPM_C_STRING_LOW_VOLTAGE_LISTENER_EVENT_NAME       "SPM_LOW_VOLTAGE_LISTENER"
#define OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_CHANGED  0x00000001
#define OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_STOP     0x00000002

#define OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASKS (OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_CHANGED | \
   OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_STOP)

#define OEDT_SPM_THREAD_STACKSIZE       10000
#define OEDT_SPM_THREAD_PRIO            0x0000004B
#define OEDT_SPM_THREAD_WAIT            1000
#define OEDT_SPM_INITIALIZE             0
#define OEDT_SPM_ID_THRESHOLD           1
#define OEDT_SPM_DEVICES                2
#define OEDT_SPM_ID_GROUP               5
#define OEDT_SPM_USERVOLTAGE            6000
#define OEDT_SPM_HYSTERESIS             500
#define OEDT_SPM_CURRENTSCALE           3210

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static OSAL_tThreadID g_s32ThreadIdVoltageListener = OSAL_ERROR;
static OSAL_tThreadID g_s32ThreadIdUserVoltageListener = OSAL_ERROR;
static OEDT_TESTSTATE g_teststate;

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTIONS                                                            */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* FUNCTION: tCString coszSPM_GetSystemVoltageString()
*
* DESCRIPTION: Get a zero terminated string with the name of the system voltage.
*
* PARAMETER: [IN] u8SystemVoltageState = System voltage state which can be one of 
*
*                                         - OSAL_VOLT_CRITICAL_LOW_VOLTAGE 
*                                         - OSAL_VOLT_LOW_VOLTAGE          
*                                         - OSAL_VOLT_OPERATING_VOLTAGE    
*                                         - OSAL_VOLT_OVER_VOLTAGE         
*                                         - OSAL_VOLT_CRITICAL_OVER_VOLTAGE
*
* RETURNVALUE: Zero terminated string with the name of the system voltage state.
*
*******************************************************************************/
static tCString coszSPM_GetSystemVoltageString(tU8 u8SystemVoltageState)
{
   switch (u8SystemVoltageState)
   {
      case OSAL_VOLT_CRITICAL_LOW_VOLTAGE  : return("CRITICAL_LOW_VOLTAGE");
      case OSAL_VOLT_LOW_VOLTAGE           : return("LOW_VOLTAGE");
      case OSAL_VOLT_OPERATING_VOLTAGE     : return("OPERATING_VOLTAGE");
      case OSAL_VOLT_OVER_VOLTAGE          : return("OVER_VOLTAGE");
      case OSAL_VOLT_CRITICAL_OVER_VOLTAGE : return("CRITICAL_OVER_VOLTAGE");
      default                              : return("UNKNOWN");
   }
}

/*******************************************************************************
*
* FUNCTION: tCString coszSPM_GetSYSVoltageString()
*
* DESCRIPTION: Get a zero terminated string with the name of the system voltage.
*
* PARAMETER: [IN] u8SystemVoltageState = System voltage state which can be one of
*
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_START
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_END
*                                         - OSALCVM_LOW_VOLTAGE_START
*                                         - OSALCVM_LOW_VOLTAGE_END
*                                         - OSALCVM_HIGH_VOLTAGE_START
*                                         - OSALCVM_HIGH_VOLTAGE_END
*                                         - OSALCVM_CRITICAL_HIGH_VOLTAGE_START
*                                         - OSALCVM_CRITICAL_HIGH_VOLTAGE_END
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP
*
* RETURNVALUE: Zero terminated string with the name of the system voltage state.
*
*******************************************************************************/
static tCString coszSPM_GetSYSVoltageString(tU8 u8SystemVoltageState)
{
   switch (u8SystemVoltageState)
   {
      case OSALCVM_CRITICAL_LOW_VOLTAGE_START        : return("CRITICAL_LOW_VOLTAGE_START");
      case OSALCVM_CRITICAL_LOW_VOLTAGE_END          : return("CRITICAL_LOW_VOLTAGE_END");
      case OSALCVM_LOW_VOLTAGE_START                 : return("LOW_VOLTAGE_START");
      case OSALCVM_LOW_VOLTAGE_END                   : return("LOW_VOLTAGE_END");
      case OSALCVM_HIGH_VOLTAGE_START                : return("HIGH_VOLTAGE_START");
      case OSALCVM_HIGH_VOLTAGE_END                  : return("HIGH_VOLTAGE_END");
      case OSALCVM_CRITICAL_HIGH_VOLTAGE_START       : return("CRITICAL_HIGH_VOLTAGE_START");
      case OSALCVM_CRITICAL_HIGH_VOLTAGE_END         : return("CRITICAL_HIGH_VOLTAGE_END");
      case OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP   : return("CRITICAL_LOW_VOLTAGE_AT_STARTUP");
      default                                        : return("UNKNOWN");
   }
}


/*******************************************************************************
*
* FUNCTION: tCString coszSPM_GetCVMVoltageString()
*
* DESCRIPTION: Get a zero terminated string with the name of the 
*              critical-voltage-manager (CVM) voltage.
*
* PARAMETER: [IN] u8CVMVoltageState = CVM voltage state which can be one of 
*
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_START     
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_END       
*                                         - OSALCVM_LOW_VOLTAGE_START              
*                                         - OSALCVM_LOW_VOLTAGE_END                
*                                         - OSALCVM_HIGH_VOLTAGE_START             
*                                         - OSALCVM_HIGH_VOLTAGE_END               
*                                         - OSALCVM_CRITICAL_HIGH_VOLTAGE_START    
*                                         - OSALCVM_CRITICAL_HIGH_VOLTAGE_END      
*                                         - OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP
*
* RETURNVALUE: Zero terminated string with the name of the CVM voltage state.
*
*******************************************************************************/
static tCString coszSPM_GetCVMVoltageString(tU8 u8CVMVoltageState)
{
   switch (u8CVMVoltageState)
   {
      case OSALCVM_CRITICAL_LOW_VOLTAGE_START      : return("OSALCVM_CRITICAL_LOW_VOLTAGE_START");
      case OSALCVM_CRITICAL_LOW_VOLTAGE_END        : return("OSALCVM_CRITICAL_LOW_VOLTAGE_END");
      case OSALCVM_LOW_VOLTAGE_START               : return("OSALCVM_LOW_VOLTAGE_START");
      case OSALCVM_LOW_VOLTAGE_END                 : return("OSALCVM_LOW_VOLTAGE_END");
      case OSALCVM_HIGH_VOLTAGE_START              : return("OSALCVM_HIGH_VOLTAGE_START");
      case OSALCVM_HIGH_VOLTAGE_END                : return("OSALCVM_HIGH_VOLTAGE_END");
      case OSALCVM_CRITICAL_HIGH_VOLTAGE_START     : return("OSALCVM_CRITICAL_HIGH_VOLTAGE_START");
      case OSALCVM_CRITICAL_HIGH_VOLTAGE_END       : return("OSALCVM_CRITICAL_HIGH_VOLTAGE_END");
      case OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP : return("OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP");
      default                                      : return("UNKNOWN");
   }
}

/*******************************************************************************
*
* FUNCTION: tCString coszSPM_GetUserVoltageEvent()
*
* DESCRIPTION: Get a zero terminated string with the name of the
*              critical-voltage-manager (CVM) voltage.
*
* PARAMETER: [IN] u8UserVoltageState = User voltage Event state which can be one of
*
*                                         - OSAL_VOLT_IDX_OVERRUN
*                                         - OSAL_VOLT_IDX_UNDERRUN
*                                         - OSAL_VOLT_IDX_INRANGE
*
*
* RETURNVALUE: Zero terminated string with the name of the CVM voltage state.
*
*******************************************************************************/

static tCString coszSPM_GetUserVoltageEvent(tU8 u8UserVoltageState)
{
   switch (u8UserVoltageState)
   {
      case OSAL_VOLT_IDX_OVERRUN         : return("OSALUSER_OVERRUN_OCCURED");
      case OSAL_VOLT_IDX_UNDERRUN        : return("OSALUSER_UNDERRUN_OCCURED");
      case OSAL_VOLT_IDX_INRANGE         : return("OSALUSER_INRANGE_OCCURED");
      default                            : return("UNKNOWN");
   }
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_OverVoltageListenerThread()
*
* DESCRIPTION: This is the overvoltage listener thread which registers an 
*              overvoltage notification client at the /dev/volt OSAL driver and 
*              prints the indicated overvoltage state changes to TTFis.
*
* PARAMETER: [OUT] pvArg = Reference to the OEDT_TESTSTATE.
*
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_OverVoltageListenerThread(tPVoid pvArg)
{
   OSAL_tIODescriptor hIODescVoltDriver             = OSAL_ERROR;
   OSAL_tEventHandle  hEventOverVoltage             = OSAL_C_INVALID_HANDLE;
   tS32               s32VoltageNoficiationClientId = OEDT_SPM_INITIALIZE;
   OSAL_tEventMask    u32EventResultMask            = OEDT_SPM_INITIALIZE;
   tU32               u32SystemVoltageLevel         = OSAL_VOLT_OPERATING_VOLTAGE;
   tBool              bOverVoltageListenerActive    = TRUE;
   OEDT_TESTSTATE*    pteststate                    = (OEDT_TESTSTATE*)pvArg;
   OSAL_tVoltSystemThresholdHistory rVoltSystemThresholdHistory;
   OSAL_tVoltSystemThresholdNotification rVoltSystemThresholdNotification;
   OSAL_tVoltRemoveThresholdNotification rRemnotification;
   tS32               s32ReturnValue;
   tU32 u32Index;

   /*Create event*/
   s32ReturnValue = OSAL_s32EventCreate((tCString)OEDT_SPM_C_STRING_OVER_VOLTAGE_LISTENER_EVENT_NAME, &hEventOverVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*Open the VOLT driver*/
      hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, pteststate);

      if (!pteststate->error_code)
      {
         /*create client before accessing*/
         s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                            (tS32)&s32VoltageNoficiationClientId);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            /*Initialize the system threshold structure*/
            rVoltSystemThresholdNotification.idClient = s32VoltageNoficiationClientId;
            rVoltSystemThresholdNotification.hdl      = hEventOverVoltage;
            rVoltSystemThresholdNotification.kind     = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;
            rVoltSystemThresholdNotification.mask     = OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED;
            /*register system threshold*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,OSAL_C_S32_IOCTRL_VOLT_SET_SYSTEM_THRESHOLD_NOTIFICATION,
                                               (tS32)&rVoltSystemThresholdNotification);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               /*loop until bOverVoltageListenerActive is active*/
               while (TRUE == bOverVoltageListenerActive)
               {
                  /*wait for an event*/
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Waiting for Event");
                  s32ReturnValue = OSAL_s32EventWait(hEventOverVoltage,OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASKS,
                                                      OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&u32EventResultMask);
                  OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                  if (!pteststate->error_code)
                  {
                     /*check the event occured from driver*/
                     if (u32EventResultMask & OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED)
                     {
                        /*Get system voltage level*/
                        s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver, OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL,
                                                           (tS32)&u32SystemVoltageLevel);
                        OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                        if (!pteststate->error_code)
                        {
                           OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread() => Battery system voltage level changed to = %s", coszSPM_GetSystemVoltageString((tU8)u32SystemVoltageLevel));
                        }
                        rVoltSystemThresholdHistory.idClient = s32VoltageNoficiationClientId;
                        rVoltSystemThresholdHistory.kind     = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;
                        rVoltSystemThresholdHistory.idx      = OEDT_SPM_INITIALIZE;
                        /*Get system voltage level history*/
                        s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                           OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL_HISTORY,
                                                           (tS32)&rVoltSystemThresholdHistory);
                        OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                        if (!pteststate->error_code)
                        {

                           for (u32Index = OEDT_SPM_INITIALIZE; u32Index < rVoltSystemThresholdHistory.idx; u32Index++)
                           {
                              OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT,
                              "vSPM_OverVoltageListenerThread() => System history = %s",
                              coszSPM_GetSYSVoltageString(rVoltSystemThresholdHistory.arEvents[u32Index]));
                           }
                        }
                     }

                     /*check for STOP event*/
                     if (u32EventResultMask & OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP)
                     {
                        /*set this flag to stop*/
                        bOverVoltageListenerActive = FALSE;
                     }
                     /*reinitialize the mask*/
                     s32ReturnValue = OSAL_s32EventPost(hEventOverVoltage, ~u32EventResultMask,OSAL_EN_EVENTMASK_AND);
                     OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
                     if (pteststate->error_code)
                     {
                        OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Event Post Failed");
                     }
                  }
                  else
                  {
                     bOverVoltageListenerActive = FALSE;
                  }
               }
               /*deregister the system threshold notification*/
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread thread exited");
               rRemnotification.idClient = s32VoltageNoficiationClientId;
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_DEREG_SYSTEM_THRESHOLD_NOTIFICATION,
                                                   (tS32)&rRemnotification);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
               if (pteststate->error_code)
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Deregistration system threshold Failed");
               }
            }

            /*remove the created client*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver, OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                                (tS32)&rVoltSystemThresholdNotification.idClient);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
            if (pteststate->error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Client remove Failed");
            }
         }
         /*close the driver*/
         OEDT_EXPECT_EQUALS(OSAL_OK, OSAL_s32IOClose(hIODescVoltDriver), pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Driver close Failed");
         }
      }
      /*close the event*/
      OEDT_EXPECT_EQUALS(OSAL_OK, OSAL_s32EventClose(hEventOverVoltage), pteststate);

      if (!pteststate->error_code)
      {
         /*Delete the event*/
         s32ReturnValue = OSAL_s32EventDelete((tCString)OEDT_SPM_C_STRING_OVER_VOLTAGE_LISTENER_EVENT_NAME);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Event delete Failed");
         }
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_OverVoltageListenerThread : Event close Failed");
      }
   }
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_StartOverVoltageListenerThread()
*
* DESCRIPTION: This function starts the overvoltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdVoltageListener = Thread ID of the over voltage 
*                                         listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_StartOverVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{ 
   OSAL_trThreadAttribute rThreadAttribute;

   rThreadAttribute.szName       = (tString)"OverVoltageListenerThread";
   rThreadAttribute.s32StackSize = OEDT_SPM_THREAD_STACKSIZE;
   rThreadAttribute.u32Priority  = OEDT_SPM_THREAD_PRIO;
   rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)vSPM_OverVoltageListenerThread;
   rThreadAttribute.pvArg        = pteststate;

   /*Spawn the thread*/
   g_s32ThreadIdVoltageListener = OSAL_ThreadSpawn(&rThreadAttribute);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, g_s32ThreadIdVoltageListener, pteststate);

   if (!pteststate->error_code)
   {
      // Give the over voltage listener thread some time to eventually
      // set an error code to the passed OEDT_TESTSTATE.
      OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);
   }
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_StopOverVoltageListenerThread()
*
* DESCRIPTION: This function stops the overvoltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdVoltageListener = Thread ID of the over voltage 
*                                         listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_StopOverVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{
   OSAL_tEventHandle hEventOverVoltage = OSAL_C_INVALID_HANDLE;
   tS32             s32ReturnValue;

   /*open the event which is already created*/
   s32ReturnValue = OSAL_s32EventOpen((tString)OEDT_SPM_C_STRING_OVER_VOLTAGE_LISTENER_EVENT_NAME,
                                       &hEventOverVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*Post the STOP event*/
      s32ReturnValue = OSAL_s32EventPost(hEventOverVoltage,
                                          OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP,
                                          OSAL_EN_EVENTMASK_OR);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

      if (!pteststate->error_code)
      {
         /*close the event*/
         s32ReturnValue = OSAL_s32EventClose(hEventOverVoltage);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            // Give the over voltage listener thread some time to terminate
            // itself and afterwards check if it really doesn't exist anymore.
            OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);
            s32ReturnValue = OSAL_s32ThreadJoin(g_s32ThreadIdVoltageListener, OEDT_SPM_THREAD_WAIT);
            OEDT_EXPECT_EQUALS(OSAL_ERROR, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               OEDT_EXPECT_TRUE((OSAL_u32ErrorCode() == OSAL_E_DOESNOTEXIST), pteststate);
            }
         }
      }
   }
}

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*****************************************************************************
* FUNCTION:     tU32 u32SPM_OpenCloseDevs()
* PARAMETER:    none
* RETURNVALUE:  "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Open SPM Device
*               2. Close SPM Device
*               
* HISTORY:      Created by Martin Langer (CM-AI/PJ-CF33) on  2011-09-08
*               Ported by Suresh Dhandapani (RBEI/ECF5)
*
******************************************************************************/
tU32 u32SPM_OpenCloseDevs(void)
{

   tU32 u32Status = OEDT_SPM_INITIALIZE;
   tU32 u32Device;
   OEDT_TESTSTATE rstate;

   /*Initialize with VOLT driver*/
   struct device spm_device[OEDT_SPM_DEVICES] = {{OSAL_C_STRING_DEVICE_VOLT},{(tCString)NULL}};
   rstate = OEDT_CREATE_TESTSTATE();

   /*check until NULL*/
   for (u32Device = 0; spm_device[u32Device].dev_name!=NULL; u32Device++)
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "LSIM test: opening device '%s'", spm_device[u32Device].dev_name );
      /*perform open/close test*/
      u32Status = u32OEDT_OpenCloseDevice( (tCString)spm_device[u32Device].dev_name,(tU32)OSAL_OK, OSAL_EN_READONLY );
      OEDT_EXPECT_EQUALS( OSAL_OK, u32Status, &rstate );
   }
   return rstate.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_StartOverVoltageListener()
*
* DESCRIPTION: This functions executes the test : Start Over Voltage Listener.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop over 
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/
tU32 u32SPM_StartOverVoltageListener(void)
{
   /*Initialize the state with zero*/
   g_teststate = OEDT_CREATE_TESTSTATE();

   vSPM_StartOverVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_StopOverVoltageListener()
*
* DESCRIPTION: This functions executes the test : Stop Over Voltage Listener.
*
*              To run properly this test needs the above test 'Start Over 
*              Voltage Listener' to be executed first.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop over 
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/
tU32 u32SPM_StopOverVoltageListener(void)
{
   vSPM_StopOverVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_StartUserVoltageListenerThread()
*
* DESCRIPTION: This function starts the uservoltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdVoltageListener = Thread ID of the user voltage 
*                                         listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/

static tVoid vSPM_StartUserVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{ 
   OSAL_trThreadAttribute rThreadAttribute;

   rThreadAttribute.szName       = (tString)"UserVoltageListenerThread";
   rThreadAttribute.s32StackSize = OEDT_SPM_THREAD_STACKSIZE;
   rThreadAttribute.u32Priority  = OEDT_SPM_THREAD_PRIO;
   rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)vSPM_UserVoltageListenerThread;
   rThreadAttribute.pvArg        = pteststate;

   /*spawn the user voltage thread*/
   g_s32ThreadIdUserVoltageListener = OSAL_ThreadSpawn(&rThreadAttribute);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, g_s32ThreadIdUserVoltageListener, pteststate);

   if (!pteststate->error_code)
   {
      // Give the user voltage listener thread some time to eventually
      // set an error code to the passed OEDT_TESTSTATE.
      OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);
   }
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_StartUserVoltageListener()
*
* DESCRIPTION: This functions executes the test : Start user Voltage Listener.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop user 
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_StartUserVoltageListener(void)
{
   /*Initialize the state with zero*/
   g_teststate = OEDT_CREATE_TESTSTATE();

   vSPM_StartUserVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}


/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_StopUserVoltageListener()
*
* DESCRIPTION: This functions executes the test : Stop user Voltage Listener.
*
*              To run properly this test needs the above test 'Start user 
*              Voltage Listener' to be executed first.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop user 
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_StopUserVoltageListener(void)
{
   vSPM_StopUserVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_UserVoltageListenerThread()
*
* DESCRIPTION: This is the uservoltage listener thread which registers an 
*              uservoltage notification client at the /dev/volt OSAL driver and 
*              prints the indicated uservoltage state changes to TTFis.
*
* PARAMETER: [OUT] pvArg = Reference to the OEDT_TESTSTATE.
*
* RETURNVALUE: None.
*
*******************************************************************************/

static tVoid vSPM_UserVoltageListenerThread(tPVoid pvArg)
{
   OSAL_tIODescriptor hIODescVoltDriver             = OSAL_ERROR;
   OSAL_tVoltUserThresholdHistory rUserLevelHistory;
   OSAL_tEventHandle  hEventUserVoltage             = OSAL_C_INVALID_HANDLE;
   tS32               s32VoltageNoficiationClientId = OEDT_SPM_INITIALIZE;
   OSAL_tEventMask    u32EventResultMask            = OEDT_SPM_INITIALIZE;
   tBool              bUserVoltageListenerActive    = TRUE;
   OEDT_TESTSTATE*    pteststate                    = (OEDT_TESTSTATE*)pvArg;
   tU32 u32History;
   OSAL_tVoltUserThresholdNotification rUserNotification;
   OSAL_tVoltRemoveThresholdNotification rRemnotification;
   tS32             s32ReturnValue;

   /*create the event*/
   s32ReturnValue = OSAL_s32EventCreate((tCString)OEDT_SPM_C_STRING_USER_VOLTAGE_LISTENER_EVENT_NAME, &hEventUserVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*open the VOLT driver*/
      hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, pteststate);

      if (!pteststate->error_code)
      {
         /*create client to access driver*/
         s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                             OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                             (tS32)&s32VoltageNoficiationClientId);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            rUserNotification.hdl = hEventUserVoltage;
            rUserNotification.idClient = s32VoltageNoficiationClientId;
            rUserNotification.idGroup = OEDT_SPM_ID_GROUP;
            rUserNotification.mask = OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED;
            rUserNotification.idThreshold = OEDT_SPM_ID_THRESHOLD;
            rUserNotification.thresholdVoltage = OEDT_SPM_USERVOLTAGE;
            rUserNotification.thresholdHysteresis = OEDT_SPM_HYSTERESIS;

            /*register user threshold*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_SET_USER_THRESHOLD_NOTIFICATION,
                                                (tS32)&rUserNotification);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               while (TRUE == bUserVoltageListenerActive)
               {
                  /*wait for an event*/
                  s32ReturnValue = OSAL_s32EventWait(hEventUserVoltage,
                                                      OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASKS,
                                                      OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                                                      &u32EventResultMask);
                  OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                  if (!pteststate->error_code)
                  {
                     /*check for the registered mask*/
                     if (u32EventResultMask & OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED)
                     {

                        if (!pteststate->error_code)
                        {
                           OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_UserVoltageListenerThread() => Battery user voltage level observed");
                           rUserLevelHistory.idClient = s32VoltageNoficiationClientId;
                           rUserLevelHistory.idThreshold = OEDT_SPM_ID_THRESHOLD;
                           rUserLevelHistory.idGroup = OEDT_SPM_ID_GROUP;
                           rUserLevelHistory.idx = OEDT_SPM_INITIALIZE;

                           /*Get the user level history*/
                           s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                               OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_LEVEL_HISTORY,
                                                               (tS32)&rUserLevelHistory);
                           OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &g_teststate);
                           if (!g_teststate.error_code)
                           {
                              for(u32History = 0; u32History<rUserLevelHistory.idx; u32History++)
                              {
                                 OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "rUserLevelHistory id threshold:%d, Events:%s,",rUserLevelHistory.arClients[u32History],coszSPM_GetUserVoltageEvent(rUserLevelHistory.arEvents[u32History]));
                              }
                           }
                        }
                     }
                     /*check for STOP mask*/
                     if (u32EventResultMask & OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP)
                     {
                        bUserVoltageListenerActive = FALSE;
                     }
                     /*post the event to reinitialize*/
                     s32ReturnValue = OSAL_s32EventPost(hEventUserVoltage, ~u32EventResultMask,
                                                         OSAL_EN_EVENTMASK_AND);
                     OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
                  }
                  else
                  {
                     bUserVoltageListenerActive = FALSE;
                  }
               }
               rRemnotification.idClient = s32VoltageNoficiationClientId;
               rRemnotification.idThreshold = OEDT_SPM_ID_THRESHOLD;

               /*Deregister the user threshold notification*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_DEREG_USER_THRESHOLD_NOTIFICATION,
                                                   (tS32)&rRemnotification);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
            }
            /*remove registered client*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                                (tS32)&rUserNotification.idClient);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
         }
         /*close VOLT driver*/
         OEDT_EXPECT_EQUALS(OSAL_OK, OSAL_s32IOClose(hIODescVoltDriver), pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_UserVoltageListenerThread : Driver close Failed");
         }
      }
      /*Close event*/
      OEDT_EXPECT_EQUALS(OSAL_OK, OSAL_s32EventClose(hEventUserVoltage), pteststate);

      if (!pteststate->error_code)
      {
         /*Delete the event*/
         s32ReturnValue = OSAL_s32EventDelete((tCString)OEDT_SPM_C_STRING_USER_VOLTAGE_LISTENER_EVENT_NAME);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_UserVoltageListenerThread : Event delete Failed");
         }
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_UserVoltageListenerThread : Event close Failed");
      }
   }
}

/*******************************************************************************
*
* FUNCTION: tVoid vSPM_StopUserVoltageListenerThread()
*
* DESCRIPTION: This function stops the uservoltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdVoltageListener = Thread ID of the over voltage 
*                                         listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/

static tVoid vSPM_StopUserVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{
   OSAL_tEventHandle hEventUserVoltage = OSAL_C_INVALID_HANDLE;
   tS32             s32ReturnValue;

   /*open the event*/
   s32ReturnValue = OSAL_s32EventOpen((tString)OEDT_SPM_C_STRING_USER_VOLTAGE_LISTENER_EVENT_NAME,
                                       &hEventUserVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*post the event with STOP mask*/
      s32ReturnValue = OSAL_s32EventPost(hEventUserVoltage,
                                          OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_STOP,
                                          OSAL_EN_EVENTMASK_OR);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

      if (!pteststate->error_code)
      {
         /*close the event*/
         s32ReturnValue = OSAL_s32EventClose(hEventUserVoltage);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            // Give the over voltage listener thread some time to terminate
            // itself and afterwards check if it really doesn't exist anymore.
            OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);
            s32ReturnValue = OSAL_s32ThreadJoin(g_s32ThreadIdUserVoltageListener, OEDT_SPM_THREAD_WAIT);
            OEDT_EXPECT_EQUALS(OSAL_ERROR, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               OEDT_EXPECT_TRUE((OSAL_u32ErrorCode() == OSAL_E_DOESNOTEXIST), pteststate);
            }
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close failed");
         }
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event post failed");
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event open failed");
   }
}


/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_CreateRemoveNotifications()
*
* DESCRIPTION: This is to create and delete the client.
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_CreateRemoveNotifications(void)
{
   tS32             s32ReturnValue;
   tS32 s32VoltageNoficiationClientId = OEDT_SPM_INITIALIZE;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);

   if (!rState.error_code)
   {
      /*create client ID*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                          (tS32)&s32VoltageNoficiationClientId);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Notification client created successfully");
         /*Remove client*/
         s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                             OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                             (tS32)&s32VoltageNoficiationClientId);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
         if (!rState.error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Notification client deleted successfully");
         }
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver Close Failed");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_BoardVoltage()
*
* DESCRIPTION: This is to read the board voltage,voltage in mv,RAW voltage .
*
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_BoardVoltage(void)
{
   tS32             s32ReturnValue;
   tU32 s32Voltage = OEDT_SPM_INITIALIZE;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);

   if (!rState.error_code)
   {
      /*Get the board voltage*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE,
                                          (tS32)&s32Voltage);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Board voltage:%d",s32Voltage);
      }
      /*Get board voltage in MV*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_MV,
                                          (tS32)&s32Voltage);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Board voltage in millivolt:%d",s32Voltage);
      }
      /*Get RAW board voltage*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_RAW,
                                          (tS32)&s32Voltage);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Board voltage RAW:%d",s32Voltage);
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver Close Failed");
      }
   }
   return rState.error_code;
}


/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_UserVoltageRange()
*
* DESCRIPTION: This is to read the voltage range supported by this LSIM .
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_UserVoltageRange(void)
{
   tS32             s32ReturnValue;
   OSAL_tIODescriptor hIODescVoltDriver;
   OSAL_tVoltUserThresholdRange rUserVoltRange;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);

   if (!rState.error_code)
   {
      /*Get the user voltage range*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_RANGE,
                                          (tS32)&rUserVoltRange);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Range for Highest voltage:%d and Lowest voltage:%d",rUserVoltRange.highestVoltage,rUserVoltRange.lowestVoltage);
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver Close Failed");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_BoardCurrentScale()
*
* DESCRIPTION: This function sets and reads the board current scale.
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_BoardCurrentScale(void)
{
   tS32             s32ReturnValue;
   tU32  s32CurScale = OEDT_SPM_CURRENTSCALE;
   tU32  s32readCurScale;
   tU32  s32readedScale;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*Open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);
   if (!rState.error_code)
   {
      /*Get board voltage scale*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_SCALE,
                                          (tS32)&s32readCurScale);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         s32readedScale = s32readCurScale;
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Previous Current scale:%d",s32readCurScale);
         /*set board voltage scale*/
         s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                             OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF,
                                             (tS32)&s32CurScale);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
         if (!rState.error_code)
         {
            /*verify the board voltage scale*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_SCALE,
                                                (tS32)&s32readCurScale);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Updated Current scale:%d",s32readCurScale);
            }
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF,
                                                (tS32)&s32readedScale);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Updated Current scale to Default FAILED");
            }
         }
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver close FAILED");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_SysLevelHistory()
*
* DESCRIPTION: This function reads the system level history.
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_SysLevelHistory(void)
{
   tS32             s32ReturnValue;
   tU32 u32History;
   OSAL_tVoltSystemThresholdHistory rSysLevelHistory;
   tS32 s32VoltageNoficiationClientId = OEDT_SPM_INITIALIZE;
   OSAL_tIODescriptor hIODescVoltDriver;
   OSAL_tVoltSystemThresholdNotification rVoltSystemThresholdNotification;
   OSAL_tVoltRemoveThresholdNotification rRemnotification;
   OSAL_tEventHandle  hEventHistory       = OSAL_C_INVALID_HANDLE;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);

   if (!rState.error_code)
   {
      /*create client*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                         OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                         (tS32)&s32VoltageNoficiationClientId);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         /*create event*/
         s32ReturnValue = OSAL_s32EventCreate((tCString)OEDT_SPM_C_STRING_VOLTAGE_HISTORY_EVENT_NAME, &hEventHistory);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
         if (!rState.error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event created successfully");
            rVoltSystemThresholdNotification.hdl      = hEventHistory;
            rVoltSystemThresholdNotification.idClient = s32VoltageNoficiationClientId;
            rVoltSystemThresholdNotification.kind     = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;
            rVoltSystemThresholdNotification.mask     = OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED;
            /*register system threshold*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_SET_SYSTEM_THRESHOLD_NOTIFICATION,
                                                (tS32)&rVoltSystemThresholdNotification);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "system Notification created successfully");
               rSysLevelHistory.idClient = s32VoltageNoficiationClientId;                        /* client id from create client              */
               rSysLevelHistory.kind = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;                            /* pure system or cvm thresholds             */
               rSysLevelHistory.idx = OEDT_SPM_INITIALIZE;
               /*get system voltage level history*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL_HISTORY,
                                                   (tS32)&rSysLevelHistory);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
               if (!rState.error_code)
               {
                  for(u32History = 0; u32History < rSysLevelHistory.idx; u32History++)
                  {
                     OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "sysLevelHistory Events:%d,",rSysLevelHistory.arEvents[u32History]);
                  }
               }

               rRemnotification.idClient = s32VoltageNoficiationClientId;
               /*Deregister system threshold*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_DEREG_SYSTEM_THRESHOLD_NOTIFICATION,
                                                   (tS32)&rRemnotification);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
               if (!rState.error_code)
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "system Notification client deleted successfully");
               }
               else
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "system Notification client deletion failed");
               }
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "system Notification creation failed");
            }

            /*remove created client*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                                (tS32)&s32VoltageNoficiationClientId);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Notification client deleted successfully");
            }
            /*close the event*/
            s32ReturnValue = OSAL_s32EventClose(hEventHistory);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close success");
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close failed");
            }
            /*Delete the event*/
            s32ReturnValue = OSAL_s32EventDelete((tCString)OEDT_SPM_C_STRING_VOLTAGE_HISTORY_EVENT_NAME);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event Deletion success");
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event Deletion failed");
            }
         }
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver close failed");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_UserLevelHistory()
*
* DESCRIPTION: This function reads the user level history.
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_UserLevelHistory(void)
{
   tS32             s32ReturnValue;
   tU32 u32History;
   OSAL_tVoltUserThresholdNotification rUserNotification;
   OSAL_tVoltUserThresholdHistory rUserLevelHistory;
   tS32 s32VoltageNoficiationClientId = OEDT_SPM_INITIALIZE;
   OSAL_tIODescriptor hIODescVoltDriver;
   OSAL_tVoltRemoveThresholdNotification rRemnotification;
   OSAL_tEventHandle  hEventHistory       = OSAL_C_INVALID_HANDLE;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);

   if (!rState.error_code)
   {
      /*create client*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                          (tS32)&s32VoltageNoficiationClientId);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         /*create event*/
         s32ReturnValue = OSAL_s32EventCreate((tCString)OEDT_SPM_C_STRING_VOLTAGE_HISTORY_EVENT_NAME, &hEventHistory);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
         if (!rState.error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event created successfully");
            rUserNotification.hdl = hEventHistory;
            rUserNotification.idClient = s32VoltageNoficiationClientId;
            rUserNotification.idGroup = OEDT_SPM_ID_GROUP;
            rUserNotification.mask = OEDT_SPM_C_U32_OVER_VOLTAGE_LISTENER_EVENT_MASK_CHANGED;
            rUserNotification.idThreshold = OEDT_SPM_ID_THRESHOLD;
            rUserNotification.thresholdVoltage = OEDT_SPM_USERVOLTAGE;
            rUserNotification.thresholdHysteresis = OEDT_SPM_HYSTERESIS;
            /*set user threshold*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_SET_USER_THRESHOLD_NOTIFICATION,
                                                (tS32)&rUserNotification);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

            if (!rState.error_code)
            {
               rUserLevelHistory.idClient = s32VoltageNoficiationClientId;
               rUserLevelHistory.idThreshold = OEDT_SPM_ID_THRESHOLD;
               rUserLevelHistory.idGroup = OEDT_SPM_ID_GROUP;
               rUserLevelHistory.idx = OEDT_SPM_INITIALIZE;
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "user Notification created successfully");
               /*Get the user voltage level history*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_LEVEL_HISTORY,
                                                   (tS32)&rUserLevelHistory);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
               if (!rState.error_code)
               {
                  for(u32History = 0; u32History < rUserLevelHistory.idx; u32History++)
                  {
                     OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "userLevelHistory id threshold:%d, Events:%s,",rUserLevelHistory.arClients[u32History],coszSPM_GetUserVoltageEvent(rUserLevelHistory.arEvents[u32History]));
                  }
               }
               rRemnotification.idClient = s32VoltageNoficiationClientId;
               rRemnotification.idThreshold = OEDT_SPM_ID_THRESHOLD;
               /*Deregister user threshold*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_DEREG_USER_THRESHOLD_NOTIFICATION,
                                                   (tS32)&rRemnotification);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
               if (!rState.error_code)
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "user Notification client deleted successfully");
               }
               else
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "user Notification client deletion failed");
               }
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "user Notification creation failed");
            }
            /*remove created client*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                                (tS32)&s32VoltageNoficiationClientId);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Notification client deleted successfully");
            }
            /*close the event*/
            s32ReturnValue = OSAL_s32EventClose(hEventHistory);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close success");
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close failed");
            }
            /*Delete the event*/
            s32ReturnValue = OSAL_s32EventDelete((tCString)OEDT_SPM_C_STRING_VOLTAGE_HISTORY_EVENT_NAME);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
            if (!rState.error_code)
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event Deletion success");
            }
            else
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event Deletion failed");
            }
         }
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver close failed");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_VoltageLevel()
*
* DESCRIPTION: This function reads the system and CVM voltage level.
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_VoltageLevel(void)
{
   tS32             s32ReturnValue;
   tU32 Voltlevel;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*Open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);
   if (!rState.error_code)
   {
      /*Get the system voltage level*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL,
                                          (tS32)&Voltlevel);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "system voltage level:%s,",coszSPM_GetSystemVoltageString((tU8)Voltlevel));
      }
      /*Get the CVM voltage level*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_CVM_VOLTAGE_LEVEL,
                                          (tS32)&Voltlevel);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "CVM voltage level:%s,",coszSPM_GetCVMVoltageString((tU8)Voltlevel));
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver close Failed");
      }
   }
   return rState.error_code;
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_ReadBoardCurrent()
*
* DESCRIPTION: This function reads the Board current.
*
* PARAMETER: none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/
tU32 u32SPM_Volt_ReadBoardCurrent(void)
{
   tS32             s32ReturnValue;
   tU32 u32Current;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();

   /*Open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);
   if (!rState.error_code)
   {
      /*Obtain the present current*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_CURRENT,
                                          (tS32)&u32Current);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Board current:%d,",u32Current);
      }
      /*close the driver*/
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, " Driver close Failed");
      }
   }
   return rState.error_code;
}



/*******************************************************************************
*
* FUNCTION: u32SPM_StartCVMLowVoltageListener()
*
* DESCRIPTION: This functions executes the test : Start CVM Low Voltage Listener.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop CVM low
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/
tU32 u32SPM_StartCVMLowVoltageListener(void)
{
   /*Initialize the state with zero*/
   g_teststate = OEDT_CREATE_TESTSTATE();

   vSPM_StartCVMLowVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}

/*******************************************************************************
*
* FUNCTION: vSPM_StartCVMLowVoltageListenerThread()
*
* DESCRIPTION: This function starts the CVM low voltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdLowVoltageListener = Thread ID of the CVM low voltage
*                                            listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_StartCVMLowVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{ 
   OSAL_trThreadAttribute rThreadAttribute;

   rThreadAttribute.szName       = (tString)"LowVoltageListenerThread";
   rThreadAttribute.s32StackSize = OEDT_SPM_THREAD_STACKSIZE;
   rThreadAttribute.u32Priority  = OEDT_SPM_THREAD_PRIO;
   rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)vSPM_CVMLowVoltageListenerThread;
   rThreadAttribute.pvArg        = pteststate;

   /*spawn the CVM voltage thread*/
   g_s32ThreadIdLowVoltageListener = OSAL_ThreadSpawn(&rThreadAttribute);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, g_s32ThreadIdLowVoltageListener, pteststate);

   if (!pteststate->error_code)
   {
      // Give the low voltage listener thread some time to eventually
      // set an error code to the passed OEDT_TESTSTATE.
      OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);
   }
}



/*******************************************************************************
*
* FUNCTION: tVoid vSPM_CVMLowVoltageListenerThread()
*
* DESCRIPTION: This is the CVM LOW VOLTAGE listener thread which registers a system
*              CVM threshold notification client of type SYSTEM_CVM_THRESHOLD at the
*              /dev/volt OSAL driver and prints the indicated state changes to 
*              TTFis.
*
* PARAMETER: [OUT] pvArg = Reference to the OEDT_TESTSTATE.
*
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_CVMLowVoltageListenerThread(tPVoid pvArg)
{
   tS32               s32ReturnValue;
   OSAL_tIODescriptor hIODescVoltDriver;
   tS32               s32VoltageNotificationClientId = OEDT_SPM_INITIALIZE;
   tBool              bLowVoltageListenerActive      = TRUE;
   tU32               u32CVMVoltageLevel             = OSAL_VOLT_OPERATING_VOLTAGE;
   OSAL_tEventHandle  hEventLowVoltage               = OSAL_C_INVALID_HANDLE;
   OSAL_tEventMask    u32EventResultMask             = OEDT_SPM_INITIALIZE;
   OEDT_TESTSTATE*    pteststate                     = (OEDT_TESTSTATE*)pvArg;
   OSAL_tVoltSystemThresholdNotification rVoltSystemThresholdNotification;
   tU32 u32Index;

   /*create the event*/
   s32ReturnValue = OSAL_s32EventCreate((tCString)OEDT_SPM_C_STRING_LOW_VOLTAGE_LISTENER_EVENT_NAME, &hEventLowVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*Open the VOLT driver*/
      hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, pteststate);

      if (!pteststate->error_code)
      {
         /*Get the CVM voltage level*/
         s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                             OSAL_C_S32_IOCTRL_VOLT_GET_CVM_VOLTAGE_LEVEL,
                                             (tS32)&u32CVMVoltageLevel);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_CVMLowVoltageListenerThread() => Actual CVM voltage level = %s", coszSPM_GetCVMVoltageString((tU8)u32CVMVoltageLevel));
            /*create the client ID*/
            s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                                (tS32)&s32VoltageNotificationClientId);
            OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               rVoltSystemThresholdNotification.idClient = s32VoltageNotificationClientId;
               rVoltSystemThresholdNotification.hdl      = hEventLowVoltage;
               rVoltSystemThresholdNotification.kind     = OSAL_VOLT_KIND_SYSTEM_CVM_THRESHOLDS;
               rVoltSystemThresholdNotification.mask     = OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_CHANGED;
               /*register the system threshold*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_SET_SYSTEM_THRESHOLD_NOTIFICATION,
                                                   (tS32)&rVoltSystemThresholdNotification);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

               if (!pteststate->error_code)
               {
                  while (TRUE == bLowVoltageListenerActive)
                  {
                     /*wait for an event*/
                     s32ReturnValue = OSAL_s32EventWait(hEventLowVoltage,
                                                         OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASKS,
                                                         OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                                                         &u32EventResultMask);
                     OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                     if (!pteststate->error_code)
                     {
                        /*check for the mask which is set by driver*/
                        if (u32EventResultMask & OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_CHANGED)
                        {
                           OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_CVMLowVoltageListenerThread: voltage change occured");
                           OSAL_tVoltSystemThresholdHistory rVoltSystemThresholdHistory;

                           rVoltSystemThresholdHistory.idClient = s32VoltageNotificationClientId;
                           rVoltSystemThresholdHistory.kind     = OSAL_VOLT_KIND_SYSTEM_CVM_THRESHOLDS;
                           rVoltSystemThresholdHistory.idx      = OEDT_SPM_INITIALIZE;
                           /*obtain the system voltage level history*/
                           s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                               OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL_HISTORY,
                                                               (tS32)&rVoltSystemThresholdHistory);
                           OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

                           if (!pteststate->error_code)
                           {

                              for (u32Index = OEDT_SPM_INITIALIZE; u32Index < rVoltSystemThresholdHistory.idx; u32Index++)
                              {
                                 OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT,
                                 "vSPM_CVMLowVoltageListenerThread() => CVM voltage level changed to = %s",
                                 coszSPM_GetCVMVoltageString(rVoltSystemThresholdHistory.arEvents[u32Index]));
                              }
                           }
                        }
                        /*check for STOP mask*/
                        if (u32EventResultMask & OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_STOP)
                        {
                           bLowVoltageListenerActive = FALSE;
                        }
                        /*post the event to reinitialize*/
                        s32ReturnValue = OSAL_s32EventPost(hEventLowVoltage, ~u32EventResultMask,
                                                            OSAL_EN_EVENTMASK_AND);
                        OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
                     }
                     else
                     {
                        bLowVoltageListenerActive = FALSE;
                     }
                  }
                  OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "vSPM_CVMLowVoltageListenerThread:Exited");
                  /*Deregister the system threshold*/
                  s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                      OSAL_C_S32_IOCTRL_VOLT_DEREG_SYSTEM_THRESHOLD_NOTIFICATION,
                                                      (tS32)&rVoltSystemThresholdNotification);
                  OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
               }
               /*remove the created client*/
               s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                                   OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                                   (tS32)&rVoltSystemThresholdNotification.idClient);
               OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
            }
         }
         /*close the driver*/
         s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver Close failed");
         }
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver Open failed");
      }
      /*close the event*/
      s32ReturnValue = OSAL_s32EventClose(hEventLowVoltage);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

      if (!pteststate->error_code)
      {
         /*Delete the event*/
         s32ReturnValue = OSAL_s32EventDelete((tCString)OEDT_SPM_C_STRING_LOW_VOLTAGE_LISTENER_EVENT_NAME);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);
         if (pteststate->error_code)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event delete failed");
         }
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close failed");
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event creation failed");
   }
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_StopCVMLowVoltageListener()
*
* DESCRIPTION: This functions executes the test : Stop CVM Low Voltage Listener.
*
*              To run properly this test needs the above test 'Start CVM Low
*              Voltage Listener' to be executed first.
*
* PARAMETER: None.
*
* GLOBALS: g_teststate = Global OEDT_TESTSTATE used by the start and stop CVM low
*                        voltage listener tests.
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/
tU32 u32SPM_StopCVMLowVoltageListener(void)
{
   vSPM_StopCVMLowVoltageListenerThread(&g_teststate);

   return g_teststate.error_code;
}



/*******************************************************************************
*
* FUNCTION: tU32 vSPM_StopCVMLowVoltageListenerThread()
*
* DESCRIPTION: This function stops the CVM low voltage listener thread.
*
* PARAMETER: [OUT] pteststate = Reference to the OEDT_TESTSTATE.
*
* GLOBALS: g_s32ThreadIdLowVoltageListener = Thread ID of the CVM low voltage
*                                            listener thread. 
* RETURNVALUE: None.
*
*******************************************************************************/
static tVoid vSPM_StopCVMLowVoltageListenerThread(OEDT_TESTSTATE* pteststate)
{
   tS32             s32ReturnValue;
   OSAL_tEventHandle hEventLowVoltage = OSAL_C_INVALID_HANDLE;

   /*open the event*/
   s32ReturnValue = OSAL_s32EventOpen((tString)OEDT_SPM_C_STRING_LOW_VOLTAGE_LISTENER_EVENT_NAME,
                                       &hEventLowVoltage);
   OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

   if (!pteststate->error_code)
   {
      /*post the event with STOP mask*/
      s32ReturnValue = OSAL_s32EventPost(hEventLowVoltage,
                                          OEDT_SPM_C_U32_LOW_VOLTAGE_LISTENER_EVENT_MASK_STOP,
                                          OSAL_EN_EVENTMASK_OR);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

      if (!pteststate->error_code)
      {
         /*close the event*/
         s32ReturnValue = OSAL_s32EventClose(hEventLowVoltage);
         OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, pteststate);

         if (!pteststate->error_code)
         {
            // Give the over voltage listener thread some time to terminate
            // itself and afterwards check if it really doesn't exist anymore.
            OSAL_s32ThreadWait(OEDT_SPM_THREAD_WAIT);

            s32ReturnValue = OSAL_s32ThreadJoin(g_s32ThreadIdLowVoltageListener, OEDT_SPM_THREAD_WAIT);
            OEDT_EXPECT_EQUALS(OSAL_ERROR, s32ReturnValue, pteststate);

            if (!pteststate->error_code)
            {
               OEDT_EXPECT_TRUE((OSAL_u32ErrorCode() == OSAL_E_DOESNOTEXIST), pteststate);
            }
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event close failed");
         }
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Event open failed");
   }
}

/*******************************************************************************
*
* FUNCTION: tU32 u32SPM_Volt_IOCTRL_Verify()
*
* DESCRIPTION: This function runs the some IOcontrols which is to verify its reaches.
*
* PARAMETER:none
*
* RETURNVALUE: tU32 => OEDT error code.
*
*******************************************************************************/

tU32 u32SPM_Volt_IOCTRL_Verify(void)
{
   tS32             s32ReturnValue;
   OSAL_tIODescriptor hIODescVoltDriver;
   OEDT_TESTSTATE rState = OEDT_CREATE_TESTSTATE();
   tS32 s32Pass;

   /*open the VOLT driver*/
   hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

   OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescVoltDriver, &rState);
   if (!rState.error_code)
   {
      /*Verify IOctrl*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_SCALE,
                                          (tS32)&s32Pass);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);

      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT,
         "u32SPM_Volt_IOCTRL_Verify:OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_SCALE is success");
      }

      /*Verify IOctrl*/
      s32ReturnValue = OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_CURRENT_SCALE,
                                          (tS32)&s32Pass);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (!rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT,
         "u32SPM_Volt_IOCTRL_Verify:OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_CURRENT_SCALE is success");
      }
      s32ReturnValue = OSAL_s32IOClose(hIODescVoltDriver);
      OEDT_EXPECT_EQUALS(OSAL_OK, s32ReturnValue, &rState);
      if (rState.error_code)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver close failed");
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_COMPONENT, "Driver open failed");
   }
   return rState.error_code;
}

