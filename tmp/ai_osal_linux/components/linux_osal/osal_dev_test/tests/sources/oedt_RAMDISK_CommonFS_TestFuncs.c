/******************************************************************************
 * FILE				: oedt_RAMDISK_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT	: OEDT_FrmWrk 
 *
 * DESCRIPTION		: This file implements the file system test cases for the RAMDISK  
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			|						|	Author & comments
 * --.--.--			|	Initial revision	|	------------
 * 01 Dec, 2008	|	version 1.0			|	Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   |  version 1.1    |   Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Mar, 2009	|  version 1.2    | Lint Removal
 *			         |						| Shilpa Bhat(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009   |  version 1.3    |  Commenting the Test cases which uses 
 *					   |  					|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *					   |	   				|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010   | version 1.4            |  Updated the Device name 
 *				      |			                |  Anoop Chandran (RBEI/ECF1)
 * ------------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_RAMDISK_CommonFS_TestFuncs.h"


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpenClosedevice( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_001
* DESCRIPTION	:	Opens and closes RamDisk device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSOpenClosedevice(tVoid)
{ 
	return u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpendevInvalParm( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_002
* DESCRIPTION	:	Try to Open RamDisk device with invalid parameters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32RAMDISK_CommonFSOpendevInvalParm(tVoid)
{
	return u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReOpendev( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_003
* DESCRIPTION	:	Try to Re-Open RamDisk device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSReOpendev(tVoid)
{
	return u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpendevDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_004
* DESCRIPTION	:	FFS2 device in different modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSOpendevDiffModes(tVoid)
{
	return u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSClosedevAlreadyClosed( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_005
* DESCRIPTION	:	Close RamDisk device which is already closed
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSClosedevAlreadyClosed(tVoid)
{
	return u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpenClosedir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_006
* DESCRIPTION	:	Try to Open and closes a directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSOpenClosedir(tVoid)
{
	return u32FSOpenClosedir((const tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpendirInvalid( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_007
* DESCRIPTION	:	Try to Open invalid directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSOpendirInvalid(tVoid)
{
	tPS8 dev_name[2] = {
					(tPS8)OEDT_C_STRING_RAMDISK_CFS_NONEXST,
					(tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT
					};
	return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateDelDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_008
* DESCRIPTION	:	Try create and delete directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSCreateDelDir(tVoid)
{
	return u32FSCreateDelDir((const tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateDelSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_009
* DESCRIPTION	:	Try create and delete sub directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateDelSubDir(tVoid)
{
	return u32FSCreateDelSubDir((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateDirInvalName( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_010
* DESCRIPTION	:	Try create and delete directory with invalid path
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateDirInvalName(tVoid)
{
	tPS8 dev_name[2] = {
						(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
						(tPS8)OEDT_C_STRING_RAMDISK_CFS_DIR_INV_NAME
						};
	return u32FSCreateDirInvalName(dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSRmNonExstngDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_011
* DESCRIPTION	:	Try to remove non exsisting directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSRmNonExstngDir(tVoid)
{
	return u32FSRmNonExstngDir((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSRmDirUsingIOCTRL( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_012
* DESCRIPTION	:	Delete directory with which is not a directory (with files) 
*						using IOCTRL
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSRmDirUsingIOCTRL(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)OEDT_C_STRING_RAMDISK_CFS_FILE1
							};
	return u32FSRmDirUsingIOCTRL( dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetDirInfo( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_013
* DESCRIPTION	:	Get directory information
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSGetDirInfo(tVoid)
{
	return u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpenDirDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_014
* DESCRIPTION	:	Try to open directory in different modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSOpenDirDiffModes(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)SUBDIR_PATH_RAMDISK_CFS
							};
	return u32FSOpenDirDiffModes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReOpenDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_015
* DESCRIPTION	:	Try to reopen directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSReOpenDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT,
							(tPS8)OEDT_C_STRING_RAMDISK_CFS_DIR1
							};
	return u32FSReOpenDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSDirParallelAccess( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_016
* DESCRIPTION	:	When directory is accessed by another thread try to rename it
*						delete it
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSDirParallelAccess(tVoid)
{
	return u32FSDirParallelAccess((const tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT); 
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateDirMultiTimes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_017
* DESCRIPTION	:	Try to Create directory with similar name 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateDirMultiTimes(tVoid)
{
	tPS8 dev_name[3] = {
								(tPS8)OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT,
								(tPS8)SUBDIR_PATH_RAMDISK_CFS,
								(tPS8)SUBDIR_PATH2_RAMDISK_CFS
								};
	return u32FSCreateDirMultiTimes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateRemDirInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_018
* DESCRIPTION	:	Create/ remove directory with Invalid path
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateRemDirInvalPath(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)OEDT_C_STRING_RAMDISK_CFS_DIR_INV_PATH
							};
	return u32FSCreateRemDirInvalPath(dev_name);
}



/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateRmNonEmptyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_019
* DESCRIPTION	:	Try to remove non Empty directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateRmNonEmptyDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)SUBDIR_PATH_RAMDISK_CFS
							};
	return u32FSCreateRmNonEmptyDir(dev_name, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCopyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_020
* DESCRIPTION	:	Copy the files in source directory to destination directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSCopyDir(tVoid)
{
	tPS8 dev_name[3] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)FILE13_RAMDISK_CFS,
							(tPS8)FILE14_RAMDISK_CFS
							};
	return u32FSCopyDir(dev_name,TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSMultiCreateDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_021
* DESCRIPTION	:	Create multiple sub directories
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSMultiCreateDir(tVoid)
{
	return u32FSMultiCreateDir((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_022
* DESCRIPTION	:	Attempt to create sub-directory within a directory opened
* 						in READONLY modey
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSCreateSubDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)SUBDIR_PATH_RAMDISK_CFS
							};

	return u32FSCreateSubDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSDelInvDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_023
* DESCRIPTION	:	Delete invalid Directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSDelInvDir(tVoid)
{
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
								(tPS8)OEDT_C_STRING_RAMDISK_CFS_DIR_INV_NAME
								};
	return u32FSDelInvDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCopyDirRec( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_024
* DESCRIPTION	:	Copy directories recursively
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32RAMDISK_CommonFSCopyDirRec(tVoid)
{
	tPS8 dev_name[3] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)FILE13_RAMDISK_CFS,
							(tPS8)FILE14_RAMDISK_CFS
							};
	return u32FSCopyDirRec(dev_name);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSRemoveDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_025
* DESCRIPTION	:	Remove directories recursively
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSRemoveDir(tVoid)
{
	tPS8 dev_name[4] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK,
							(tPS8)FILE11_RAMDISK_CFS,
							(tPS8)FILE12_RAMDISK_CFS,
							(tPS8)FILE12_RECR2_RAMDISK_CFS
							};
	return u32FSRemoveDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateDel( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_026
* DESCRIPTION	:  Create/ Delete file  with correct parameters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileCreateDel(tVoid)
{
	return u32FSFileCreateDel((const tPS8)CREAT_FILE_PATH_RAMDISK_CFS);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_027
* DESCRIPTION	:	Open /close File
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileOpenClose(tVoid)
{
	return u32FSFileOpenClose((const tPS8)OSAL_TEXT_FILE_FIRST_RAMDISK_CFS, TRUE);
}	


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileOpenInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_028
* DESCRIPTION	:  Open file with invalid path name (should fail)
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileOpenInvalPath(tVoid)
{
	return u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_RAMDISK_CFS);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileOpenInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_029
* DESCRIPTION	:	Open a file with invalid parameters (should fail), 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileOpenInvalParam(tVoid)
{
	return u32FSFileOpenInvalParam((const tPS8)OSAL_TEXT_FILE_FIRST_RAMDISK_CFS, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReOpen( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_030
* DESCRIPTION	:  Try to open and close the file which is already opened
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32  u32RAMDISK_CommonFSFileReOpen(tVoid)
{
	return u32FSFileReOpen((const tPS8)OSAL_TEXT_FILE_FIRST_RAMDISK_CFS, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_031
* DESCRIPTION	:	Read data from already exsisting file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileRead(tVoid)
{	
	return u32FSFileRead((const tPS8)OEDT_RAMDISK_COMNFILE_CFS, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileWrite( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_032
* DESCRIPTION	:  Write data to an existing file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileWrite(tVoid)
{	
	return u32FSFileWrite((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);	
}	


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetPosFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_033
* DESCRIPTION	:  Get File Position from begining of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSGetPosFrmBOF(tVoid)
{
	return u32FSGetPosFrmBOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetPosFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_034
* DESCRIPTION	:	Get File Position from end of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSGetPosFrmEOF(tVoid)
{
	return u32FSGetPosFrmEOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_035
* DESCRIPTION	:	Read with a negative offset from BOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32   u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF()
{
	return u32FSFileReadNegOffsetFrmBOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_036
* DESCRIPTION	:	Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
	return u32FSFileReadOffsetBeyondEOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_037
* DESCRIPTION	:  Read from offset from Beginning of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadOffsetFrmBOF(tVoid)
{	
	return u32FSFileReadOffsetFrmBOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_038
* DESCRIPTION	:	Read file with few offsets from EOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadOffsetFrmEOF(tVoid)
{	
	return u32FSFileReadOffsetFrmEOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_039
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmBOF(
									(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_040
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*						(This is from EOF)		
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmEOF(
									(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetFileCRC( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_041
* DESCRIPTION	:	Get CRC value
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32RAMDISK_CommonFSGetFileCRC(tVoid)
{
	return u32FSGetFileCRC((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_042
* DESCRIPTION	:	Read data asyncronously from a file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadAsync(tVoid)
{	
	return u32FSReadAsync((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);

}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSLargeReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_043
* DESCRIPTION	:	Read data asyncronously from a large file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSLargeReadAsync(tVoid)
{	
	return u32FSLargeReadAsync((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteAsync()
* PARAMETER		:	none
* RETURNVALUE	:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_044
* DESCRIPTION	:	Write data asyncronously to a file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteAsync(tVoid)
{	
	return u32FSWriteAsync((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSLargeWriteAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_045
* DESCRIPTION	:	Write data asyncronously to a large file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSLargeWriteAsync(tVoid)
{	
	return u32FSLargeWriteAsync((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}

	
/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteOddBuffer()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_046
* DESCRIPTION	:	 Write data to an odd buffer
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteOddBuffer(tVoid)
{
	return u32FSWriteOddBuffer((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteFileWithInvalidSize()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_FS_047
* DESCRIPTION	:  Write data to a file with invalid size
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteFileWithInvalidSize(tVoid)
{
	return u32FSWriteFileWithInvalidSize (
							(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteFileInvalidBuffer()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_048
* DESCRIPTION	:	Write data to a file with invalid buffer
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteFileInvalidBuffer(tVoid)
{
	return u32FSWriteFileInvalidBuffer((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}
/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteFileStepByStep()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_049
* DESCRIPTION	:	Write data to a file stepwise
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteFileStepByStep(tVoid)
{
	return u32FSWriteFileStepByStep ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetFreeSpace()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_050
* DESCRIPTION	:	Get free space of device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSGetFreeSpace (tVoid)
{
	return u32FSGetFreeSpace((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSGetTotalSpace()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_051
* DESCRIPTION	:	Get total space of device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSGetTotalSpace (tVoid)
{
	return u32FSGetTotalSpace ((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}
	
/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileOpenCloseNonExstng()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_052
* DESCRIPTION	:	Open a non existing file 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileOpenCloseNonExstng(tVoid)
{
	return u32FSFileOpenCloseNonExstng (
								(const tPS8)OEDT_C_STRING_FILE_INVPATH_RAMDISK_CFS);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileDelWithoutClose()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_053
* DESCRIPTION	:	Delete a file without closing 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileDelWithoutClose(tVoid)
{
	return u32FSFileDelWithoutClose ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSSetFilePosDiffOff()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_054
* DESCRIPTION	:	Set file position to different offsets
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSSetFilePosDiffOff(tVoid)
{
	return u32FSSetFilePosDiffOff ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSCreateFileMultiTimes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_055
* DESCRIPTION	:	create file multiple times
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSCreateFileMultiTimes(tVoid)
{
	return u32FSCreateFileMultiTimes ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateUnicodeName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_056
* DESCRIPTION	:	create file with unicode characters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileCreateUnicodeName(tVoid)
{
	return u32FSFileCreateUnicodeName (
											(const tPS8)OEDT_RAMDISK_CFS_UNICODEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateInvalName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_057
* DESCRIPTION	:	create file with invalid characters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileCreateInvalName(tVoid)
{
	return u32FSFileCreateInvalName(
									(const tPS8)OEDT_RAMDISK_CFS_INVALCHAR_FILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateLongName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_058
* DESCRIPTION	:	create file with long file name
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileCreateLongName(tVoid)
{
	return u32FSFileCreateLongName ((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateDiffModes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_059
* DESCRIPTION	:	Create file with different access modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileCreateDiffModes(tVoid)
{
	return u32FSFileCreateDiffModes ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileCreateInvalPath()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_060
* DESCRIPTION	:	Create file with invalid path
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileCreateInvalPath(tVoid)
{
	return u32FSFileCreateInvalPath ((const tPS8)OEDT_RAMDISK_CFS_INVALIDFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSStringSearch()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_061
* DESCRIPTION	:	Search for string 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSStringSearch(tVoid)
{
	return u32FSStringSearch ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileOpenDiffModes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_062
* DESCRIPTION	:	Create file with different access modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSFileOpenDiffModes(tVoid)
{
	return u32FSFileOpenDiffModes ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE	);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadAccessCheck()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_063
* DESCRIPTION	:	Try to read from the file which has no access rights 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadAccessCheck(tVoid)
{	
	return u32FSFileReadAccessCheck((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileWriteAccessCheck()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_064
* DESCRIPTION	:	Try to write from the file which has no access rights 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileWriteAccessCheck(tVoid)
{	 
	return u32FSFileWriteAccessCheck((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);	
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileReadSubDir()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_065
* DESCRIPTION	:  Read file present in sub-directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileReadSubDir(tVoid)
{	
	return u32FSFileReadSubDir ((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
} 


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSFileWriteSubDir()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_066
* DESCRIPTION	:	Write to file present in sub-directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileWriteSubDir(tVoid)
{	
	return u32FSFileWriteSubDir((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK );
} 


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_067
* DESCRIPTION	:	Read Write time measure for 1KB file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile(tVoid)
{	
	return u32FSTimeMeasureof1KbFile((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE);
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_068
* DESCRIPTION	:	Read Write time measure for 10KB file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile(tVoid)
{	
	return u32FSTimeMeasureof10KbFile((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_069
* DESCRIPTION	:	Read Write time measure for 10KB file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile(tVoid)
{	
	return u32FSTimeMeasureof100KbFile((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_070
* DESCRIPTION	:	Read Write time measure for 1MB file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile(tVoid)
{	
	return u32FSTimeMeasureof1MBFile((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_071
* DESCRIPTION	:	Read write time measure for 1KB file 1000 times
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid)
{	
	return u32FSTimeMeasureof1KbFilefor1000times(
									(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_072
* DESCRIPTION	:	Read write time measure for 10KB file 1000 times
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid)
{	
	return u32FSTimeMeasureof10KbFilefor1000times(
								(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_073
* DESCRIPTION	:	Read write time measure for 100KB file 100 times
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid)
{	
	return u32FSTimeMeasureof100KbFilefor100times(
									(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_074
* DESCRIPTION	:	Read write time measure for 1MB file 16 times
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid)
{	
	return u32FSTimeMeasureof1MBFilefor16times(
					(const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK, 
					(const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSRenameFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_075
* DESCRIPTION	:	Rename file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSRenameFile(tVoid)
{
	return u32FSRenameFile((const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteFrmBOF()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_076
* DESCRIPTION	:	Write to file from BOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteFrmBOF(tVoid)
{	
	return u32FSWriteFrmBOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteFrmEOF()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_077
* DESCRIPTION	:	Write to file from EOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteFrmEOF(tVoid)
{	
	return u32FSWriteFrmEOF((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSLargeFileRead()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_078
* DESCRIPTION	:	Read large file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSLargeFileRead(tVoid)
{	
	return u32FSLargeFileRead ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSLargeFileWrite()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_079
* DESCRIPTION	:	Write large data to file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSLargeFileWrite(tVoid)
{	
	return u32FSLargeFileWrite ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSOpenCloseMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_080
* DESCRIPTION	:	Open and close file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSOpenCloseMultiThread(tVoid)
{	
	return u32FSOpenCloseMultiThread ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_081
* DESCRIPTION	:	write to file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteMultiThread(tVoid)
{	
	return u32FSWriteMultiThread ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSWriteReadMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_082
* DESCRIPTION	:	write and read to file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSWriteReadMultiThread(tVoid)
{	
	return u32FSWriteReadMultiThread ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE	);
}


/*****************************************************************************
* FUNCTION		:	u32RAMDISK_CommonFSReadMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RAMDISK_CFS_083
* DESCRIPTION	:	Read from file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32RAMDISK_CommonFSReadMultiThread(tVoid)
{	
	return u32FSReadMultiThread ((const tPS8)OEDT_RAMDISK_CFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION:		u32RAMDISK_CommonFSFileGetExt()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDISK_CFS_084
* DESCRIPTION:  Read file/directory contents 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileGetExt(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt( (const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDISK_CommonFSFileGetExt2()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDISK_CFS_085
* DESCRIPTION:  Read file/directory contents with 2 IOCTL 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32RAMDISK_CommonFSFileGetExt2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt2( (const tPS8)OEDTTEST_C_STRING_DEVICE_RAMDISK );
	return u32Ret;
}


/* EOF */

