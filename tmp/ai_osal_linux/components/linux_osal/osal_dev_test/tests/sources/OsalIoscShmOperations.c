//#include "OsalIoscShmOperations.h"
#include <OsalIoscShmOperations.h>
tU32 IOSC_TEST_SHM_SIZE = 8192;

OSAL_tShMemHandle hShm = 0;
tVoid* pMap = NULL;

tVoid IOSC_TEST_shm_create( OEDT_TESTSTATE* state )
{
  hShm = OSAL_SharedMemoryCreate(IOSC_TEST_SHM_NAME, OSAL_EN_READWRITE, IOSC_TEST_SHM_SIZE);
  OEDT_ADD_RESULT( state, (hShm == OSAL_ERROR) ? TEST_FAILURE : TEST_SUCCESS );

  pMap = OSAL_pvSharedMemoryMap(hShm , OSAL_EN_READWRITE, IOSC_TEST_SHM_SIZE, 0);
  OEDT_ADD_RESULT( state, ( pMap == NULL ) ? TEST_FAILURE : TEST_SUCCESS );
}

tVoid IOSC_TEST_shm_open( OEDT_TESTSTATE* state )
{
  hShm = OSAL_SharedMemoryOpen(IOSC_TEST_SHM_NAME, OSAL_EN_READWRITE);
  OEDT_ADD_RESULT( state, (hShm == OSAL_ERROR) ? TEST_FAILURE : TEST_SUCCESS );

  pMap = OSAL_pvSharedMemoryMap(hShm , OSAL_EN_READWRITE, IOSC_TEST_SHM_SIZE, 0);
  OEDT_ADD_RESULT( state, ( pMap == NULL ) ? TEST_FAILURE : TEST_SUCCESS );
}

tVoid IOSC_TEST_shm_write( OEDT_TESTSTATE* state )
{
  tS32 s32Index;
  OEDT_ADD_RESULT ( state , ( pMap == NULL ) ? TEST_FAILURE : TEST_SUCCESS );
  if (!pMap)
    return;

  tU8* ps8Map = (tU8*)pMap;
  for ( s32Index = 0; s32Index < IOSC_TEST_SHM_SIZE; s32Index ++ )
  {
    ps8Map[s32Index] = rand() % 256;
  }
}

tVoid IOSC_TEST_shm_read( OEDT_TESTSTATE* state )
{
  tU8 u8Buffer[IOSC_TEST_SHM_SIZE];
  OEDT_ADD_RESULT ( state , ( pMap == NULL ) ? TEST_FAILURE : TEST_SUCCESS );
  if (!pMap)
    return;

  memcpy(u8Buffer, pMap, IOSC_TEST_SHM_SIZE );
}

tVoid IOSC_TEST_shm_close( OEDT_TESTSTATE* state )
{
  tS32 s32ErrorCode = OSAL_s32SharedMemoryUnmap(pMap, IOSC_TEST_SHM_SIZE);
  OEDT_ADD_RESULT( state, (s32ErrorCode != OSAL_OK) ? TEST_FAILURE : TEST_SUCCESS );

  pMap = NULL;

  s32ErrorCode = OSAL_s32SharedMemoryClose( hShm );
  OEDT_ADD_RESULT( state, (s32ErrorCode != OSAL_OK) ? TEST_FAILURE : TEST_SUCCESS );
}

tVoid IOSC_TEST_shm_delete( OEDT_TESTSTATE* state )
{
  tS32 s32ErrorCode = OSAL_s32SharedMemoryDelete( IOSC_TEST_SHM_NAME );
  OEDT_ADD_RESULT( state, (s32ErrorCode != OSAL_OK) ? TEST_FAILURE : TEST_SUCCESS );
}

tVoid IOSC_TEST_shm_SetHalfSize( tVoid )
{
  IOSC_TEST_SHM_SIZE /= 2;
}

tVoid IOSC_TEST_shm_SetDoubleSize( tVoid )
{
  IOSC_TEST_SHM_SIZE *= 2;
}
