/******************************************************************************
 *FILE         : oedt_RFS_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for the RFS
 *               (remote file system) exported by Linux to RTOS
 *              
 *AUTHOR       : pwaechtler
 *
*****************************************************************************/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"

#include "oedt_RFS_CommonFS_TestFuncs.h"

/*****************************************************************************
* FUNCTION:		OEDT_RFS_TESTDIR_available()
* PARAMETER:    string of the RFS_TESTDIR 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  checks if the test dir is available
******************************************************************************/
tU32 OEDT_RFS_TESTDIR_available(void)
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   const tS8* dev_name = OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR;
   

   hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR == hDevice )
   {
      u32Ret += 1;
   }
   else
   {
      /* Close the device */
      if ( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}
