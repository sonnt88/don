/******************************************************************************
 * FILE         : oedt_FS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the file system test cases for the CDROM,
 *                FFS1, FFS2, FFS3, USBH, RamDisk, SDCard, USBDNL
 *
 * AUTHOR(s)    :  Sriranjan U (RBEI/ECM1) and Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                       | Author & comments
 * --.--.--       | Initial revision      | ------------
 * 2 Dec, 2008    | version 1.0           | Sriranjan U (RBEI/ECM1)
 *                |                       | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   | version 1.1           | Shilpa Bhat(RBEI/ECM1)
 ----------------------------------------------------------------------------------
 * 25 Mar, 2009   | Version 1.2           | Vijay D (RBEI/ECF1)
 *---------------------------------------------------------- -------------------
 * 25 /03/ 2009   | versio 1.3            | Lint Remove Anoop Chandran(RBEI/ECF1)
 *---------------------------------------------------------- -------------------
 * 14 /04/ 2009   | version 1.4           |  Anoop Chandran(RBEI/ECF1)
 *                | Modified              |
 *                |TU_OEDT_FS_014         |
 *---------------------------------------------------------- -------------------
 * 20 /05/ 2009   | version 1.5           |  Anoop Chandran(RB EI/ECF1)
 *                | Modified              |
 *                |TU_OEDT_FS_004         |
 *-----------------------------------------------------------------------------
 * 20 Jul, 2009   | version 1.6           | Sriranjan U (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 17 Aug, 2009   | version 1.7           | Sriranjan U (RBEI/ECF1)
 *                | Semaphore timeout     |
 *                | increased             |
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009   | version 1.8           |  Commenting the Test cases which uses
 *                |                       |  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *-----------------------------------------------------------------------------
 * 16 Mar, 2010   | version 1.9           |  OEDT to measure the open time
 *                |                       |  Anoop Chandran
 *-----------------------------------------------------------------------------
 * 10 May, 2010   | version 2.0           |  Added new OEDT	u32FSCopyDirFS1FS2()
 *                |                       |  Anoop Chandran
 *-----------------------------------------------------------------------------
 * 02 June, 2010  | version 2.1           |  changed u32FSCopyDirFS1FS2() to
 *                |                       |   u32FSCopyDirTest()
 *                |                       |  by Anoop Chandran
 *-----------------------------------------------------------------------------
 * 19 April, 2011 | version 2.2           |  Added following functions from gen1
 *                |                       |  u32FSReadDirExtPartByPart
 *                |                       |  u32FSReadDirExt2PartByPart
 *                |                       |  Anooj Gopi (RBEI/ECF1)
 * 26 Feb,  2013  | Version 2.3           |  Removed u32FSPrepareEject()
 *                |                       |  Under Unused  Iocontrol removal 
 *                |                       |  By SWM2KOR
 *-----------------------------------------------------------------------------
 * 26 Feb , 2013  | version 2.3           |  updated oedt u32FSRmRecursiveCancel
 *                |                       |   Smruti Sanjay Sali (RBEI/ECF5)
 *------------------------------------------------------------------------------
 * 26 Mar, 2015   | version 2.4           |  updated u32FSFileRead
 *                |                       |  Deepak Kumar (RBEI/ECF5)
 * -----------------------------------------------------------------------------
 * 05 Feb, 2016   | version 2.5           |  updated u32FFSCommonFSSaveNowIOCTRL_AsynWrite
 *                |                       |  Chakitha Saraswathi (RBEI/ECF5)
 * -----------------------------------------------------------------------------
 * 11 Apr, 2016   | version 2.6           |  updated u32FFSCommonFSSaveNowIOCTRL_AsynWrite
 *                |                       |  Ganesh Chandra Satish (RBEI/ECF5)
 * -----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_helper_funcs.h"
#include "oedt_testing_macros.h"

#include "oedt_FS_TestFuncs.h"

#define OEDT_FS_EVENT_WAIT_TIMEOUT (60000)
#define OEDT_FS_EVENT_POST (0x00000001)
#define OEDT_FS_EVENT_WAIT (0x00000001)
#define CFS_BLOCK_SIZE_256K (256*1024)
#define CFS_WRITE_TEST_DATA "Writing data abcdefghijklmnopqrst"
#define CFS_DIR_NAME "1"
#define CFS_DIR_NAME_2 "2"
#define CFS_DIR_NAME_3 "3"
#define CFS_THREAD_1_MASK 0x00000001
#define CFS_THREAD_2_MASK 0x00000010
#define CFS_THREAD_3_MASK 0x00000100
#define CFS_THREAD_4_MASK 0x00001000
#define CFS_THREAD_5_MASK 0x00010000
#define CFS_THREAD_ALL_MASK 0x00011111
#define CFS_OEDT_SEM_TIMEOUT 35000
#define OEDT_FS_TestFile "/TestFile.txt"

#define OEDT_1ST_ENTRY_READ 5
#define OEDT_2ND_ENTRY_READ 7
#define OEDT_1ST_EXT_ENTRY_READ 6
#define OEDT_2ST_EXT_ENTRY_READ 9

static tS32 s32ParallelRetval_th1 = 0;
static tS32 s32ParallelRetval_th2 = 0;
static tS8 *ps8UserBuf = OSAL_NULL;
static OSAL_tEventHandle CFS_parallel_access_1 = 0;
static OSAL_tEventHandle CFS_parallel_access_2 = 0;
static OSAL_tEventHandle CFS_parallel_access_end_1 = 0;
static OSAL_tEventHandle CFS_parallel_access_end_2 = 0;
static OSAL_tEventHandle CFS_dir_recursive = 0;
static OSAL_tEventHandle CFS_create_file = 0;
static OSAL_tEventHandle CFS_delete_file = 0;
static OSAL_tEventHandle CFS_close_file = 0;
static OSAL_tEventHandle CFS_open_file = 0;
static OSAL_tEventHandle CFS_read_file = 0;
static OSAL_tEventHandle CFS_write_file = 0;
static OSAL_tEventHandle CFS_file_op = 0;
static OSAL_tEventMask ResultMask = 0;

/*Return Parameters*/
static tU32 u32tr1ComFS = 0;
static tU32 u32tr2ComFS = 0;

/*Event Handles*/
static OSAL_tEventHandle hEventFS = 0;
static OSAL_tEventHandle hEventFS1 = 0;
static OSAL_tEventHandle hAsynWrite = 0;
static OSAL_tEventMask FSEventResult = 0;

/*Device Handles to be used in threads*/
static OSAL_tIODescriptor     hDeviceComFS = 0;

/*Global buffer parameters*/
static tS8 *HugeBufferTr1ComFS = OSAL_NULL;
static tS8 *HugeBufferTr2ComFS = OSAL_NULL;

static OSAL_tSemHandle hSemAsyncFS;

static tU32 u32CreateTestFiles( tChar * szDirpath );
static tVoid vRemoveTestFiles( tChar * szDirpath );

/************************************************************************
* FUNCTION     :  vOEDTComFSAsyncCallback
* PARAMETER    :  none
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for Async read operation
* HISTORY      :  Created Shilpa Bhat(RBIN/ECM1)on 22 Oct, 2008
 ************************************************************************/
tVoid vOEDTComFSAsyncCallback ( OSAL_trAsyncControl *prAsyncCtrl )
{
   if ( OSAL_u32IOErrorAsync( ( OSAL_trAsyncControl* )prAsyncCtrl ) == OSAL_E_NOERROR )
   {
      OSAL_s32SemaphorePost ( hSemAsyncFS );
   }
}

/************************************************************************
*FUNCTION      :  vOEDTComFSAsyncWrCallback
* PARAMETER    :  none
* RETURNVALUE  :  "void"
* DESCRIPTION  :  Call back function for Async write operation
* HISTORY      :  Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
*************************************************************************/
tVoid vOEDTComFSAsyncWrCallback ( OSAL_trAsyncControl *prAsyncCtrl )
{
   if ( OSAL_E_NOERROR == OSAL_u32IOErrorAsync( ( OSAL_trAsyncControl* )prAsyncCtrl ) )
   {
      /* Release semaphore */
      OSAL_s32SemaphorePost ( hSemAsyncFS );
   }
}


/*****************************************************************************
* FUNCTION     :  OpenThreadFS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Open file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void OpenThreadFS( const tVoid * DevName )
{
   tU8 *strDevName;
   strDevName = ( tU8 * ) DevName;

   if ( DevName == OSAL_NULL )
   {
      u32tr1ComFS = 27;
      OSAL_vThreadExit();
   }

   /* Open file */
   hDeviceComFS = OSAL_IOOpen ( ( tCString )strDevName, OSAL_EN_READWRITE );

   if ( hDeviceComFS == OSAL_ERROR )
   {
      u32tr1ComFS = 1;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   OSAL_s32EventPost( hEventFS1, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}
/*****************************************************************************
* FUNCTION     :  CloseThreadFS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Close file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
static void CloseThreadFS( const tVoid* strDevName )
{
   OSAL_tEventMask EM  = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( strDevName );
   /*Wait for threads to Post status*/
   OSAL_s32EventWait( hEventFS1, FS_THREAD1,
                      OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &EM );
   OSAL_s32EventPost( hEventFS1, ~EM, OSAL_EN_EVENTMASK_AND );

   if ( OSAL_s32IOClose ( hDeviceComFS ) == OSAL_ERROR )
   {
      u32tr2ComFS = 2;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD2, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  WriteThread1FS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Writes data to a file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void WriteThread1FS( const tVoid *pvData )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );

   /*Write some data to File1*/
   if ( OEDT_CFST_1000  != ( tU32 )OSAL_s32IOWrite( hDeviceComFS, HugeBufferTr1ComFS,
         OEDT_CFST_1000 ) )
   {
      u32tr1ComFS = 1;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}


/*****************************************************************************
* FUNCTION     :  WriteThread2FS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Writes data to a file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void WriteThread2FS( const tVoid *pvData )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );

   /*Write some data to File1*/
   if ( OEDT_CFST_1000 != ( tU32 )OSAL_s32IOWrite( hDeviceComFS,
         HugeBufferTr2ComFS, OEDT_CFST_1000 ) )
   {
      u32tr2ComFS = 2;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD2, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  WriteThreadFS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Write data to file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008 8
******************************************************************************/
static void WriteThreadFS( const tVoid *pvData )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );

   /*Write some data to File1*/
   if ( OEDT_CFST_1000 != ( tU32 )OSAL_s32IOWrite( hDeviceComFS, HugeBufferTr1ComFS, OEDT_CFST_1000 ) )
   {
      u32tr1ComFS = 1;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   OSAL_s32EventPost( hEventFS1, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  ReadThreadFS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Reads data from file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void ReadThreadFS( const tVoid *pvData )
{
   OSAL_tEventMask EM  = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );
   /*Wait for threads to Post status*/
   OSAL_s32EventWait( hEventFS1, FS_THREAD1,
                      OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &EM );
   OSAL_s32EventPost( hEventFS1, ~EM, OSAL_EN_EVENTMASK_AND );

   if ( OSAL_s32IOControl ( hDeviceComFS, OSAL_C_S32_IOCTRL_FIOSEEK, 0
                          ) == OSAL_ERROR )
   {
      u32tr2ComFS += 21;
   }/*Read data*/
   else if ( OEDT_CFST_1000 != ( tU32 )OSAL_s32IORead( hDeviceComFS, HugeBufferTr2ComFS, OEDT_CFST_1000 ) )
   {
      u32tr2ComFS = 2;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD2, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  ReadThread1FS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Reads data from file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void ReadThread1FS( const tVoid *pvData )
{
   tU32 u32WriteSize = 10;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );

   /*Read data*/
   if ( OSAL_s32IOControl ( hDeviceComFS, OSAL_C_S32_IOCTRL_FIOSEEK, 0
                          ) == OSAL_ERROR )
   {
      u32tr2ComFS += 21;
   }/*Read data*/
   else if ( u32WriteSize != ( tU32 )OSAL_s32IORead( hDeviceComFS, HugeBufferTr1ComFS,
             u32WriteSize ) )
   {
      u32tr2ComFS = 1;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD1, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}


/*****************************************************************************
* FUNCTION     :  ReadThread2FS()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Reads data from file.
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
static void ReadThread2FS( const tVoid *pvData )
{
   tU32 u32ReadSize = 10;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pvData );

   /*Read data*/
   if ( OSAL_s32IOControl ( hDeviceComFS, OSAL_C_S32_IOCTRL_FIOSEEK, 0
                          ) == OSAL_ERROR )
   {
      u32tr2ComFS += 21;
   }/*Read data*/
   else if ( u32ReadSize != ( tU32 )OSAL_s32IORead( hDeviceComFS, HugeBufferTr2ComFS,
             u32ReadSize ) )
   {
      u32tr2ComFS = 2;
   }

   /*Post an event to the main thread*/
   OSAL_s32EventPost( hEventFS, FS_THREAD2, OSAL_EN_EVENTMASK_OR );
   /*Exit thread*/
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  Generate_File_Name_comfs()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Generate file name
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
******************************************************************************/
tVoid Generate_File_Name_comfs( tChar* ptrstr, tU32 len, tU32 limit )
{
   /*Declarations*/
   tChar ch = 'a';
   tU32 i;

   if ( ptrstr == OSAL_NULL )
   {
      return ;
   }

   /*Generate filename, the terminating zero does NOT count */
   for ( i = len; i < ( limit + len - 4 ); i++ )
   {
      /*Fill the string*/
      *( ptrstr + i ) = ch;
      /*Goto next alphabet*/
      ch = ch + 1;

      /*Compare for limit*/
      if ( ch == 'z' + 1 )
      {
         /*Reset charater*/
         ch = 'a';
      }
   }

   /*Extension*/
   *( ptrstr + i ) = '.';
   *( ptrstr + i + 1 ) = 't';
   *( ptrstr + i + 2 ) = 'x';
   *( ptrstr + i + 3 ) = 't';
   /*String terminator*/
   *( ptrstr + i + 4 ) = '\0';
}


/*****************************************************************************
* FUNCTION     :  u32ComFSCreateCommonFile()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                                FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create common data file
* HISTORY      :  Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
tU32 u32ComFSCreateCommonFile( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] =
      "Writing some test data to the data file";
   tU32 u32Ret = 0;
   tU8 u8BytesToWrite = 40;
   tS32 s32BytesWritten = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( !bCreateRemove )
   {
      /*if bCreate is FALSE no need to create the file */
      return u32Ret;
   }

   if ( ( ( hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
   {
      u32Ret = 5000;
   }
   else
   {
      /* Write data to file */
      s32BytesWritten = OSAL_s32IOWrite ( hFile, DataToWrite,
                                          ( tU32 ) u8BytesToWrite );

      /* Check status of write */
      if ( s32BytesWritten == OSAL_ERROR )
      {
         u32Ret += 6000;
      }

      /* Close the common file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 7000;
      }
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION     :  u32ComFSRemoveCommonFile()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                                FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Remove common data file
* HISTORY      :  Created by Shilpa Bhat (RBIN/EDI3) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2008
******************************************************************************/
tU32 u32ComFSRemoveCommonFile( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( !bCreateRemove )
   {
      /*if bCreate is FALSE no need to create the file */
      return u32Ret;
   }

   /*Remove the common File*/
   if ( OSAL_s32IORemove( ( tCString )ps8file_name ) == OSAL_ERROR )
   {
      u32Ret = 10000;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  Parallel_Access_Thread_1()
* PARAMETER    :  tPVoid
* RETURNVALUE  :  None
* DESCRIPTION  :  Try to oprn the directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2008
******************************************************************************/
static tVoid Parallel_Access_Thread_1( const tS8 * dev_name )
{
   OSAL_tIODescriptor hDir1 = 0 ;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trIOCtrlDirent rDirent = {""};
   /* Open directory in readwrite mode */
   s32ParallelRetval_th1 = 0;

   if ( dev_name == OSAL_NULL )
   {
      s32ParallelRetval_th1 += 51;
      OSAL_vThreadExit();
   }

   hDir1 = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if ( hDir1 == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_IOOpen in thread 1 failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 10;
   }
   else
   {
      /* Copy sub-directory name to structure variable */
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME );

      /* Create sub-directory */
      if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(MKDIR) in thread 1 failed with error %i", OSAL_u32ErrorCode());
         u32Ret += 20;
      }

      s32ParallelRetval_th1 += ( tS32 )u32Ret;
      u32Ret = 0;
      /* Wait for other thread to finish*/
      OSAL_s32EventPost( CFS_parallel_access_2, EVENT_POST_THREAD_2_STOP_WAIT,
                         OSAL_EN_EVENTMASK_OR );

      if ( OSAL_OK != OSAL_s32EventWait( CFS_parallel_access_1, EVENT_POST_THREAD_1_STOP_WAIT,
                                         OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                                         &ResultMask ) )
      {
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait in thread 1 failed with error %i", OSAL_u32ErrorCode());
         u32Ret += 30;
      }

      /* Remove sub-directory */
      if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(RMDIR) in thread 1 failed with error %i", OSAL_u32ErrorCode());
         u32Ret += 40;
      }

      if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOClose() in thread 1 failed with error %i", OSAL_u32ErrorCode());
         u32Ret += 50;
      }
   }

   if ( OSAL_ERROR == OSAL_s32EventPost( CFS_parallel_access_end_1, EVENT_POST_THREAD_1, OSAL_EN_EVENTMASK_OR ) )
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventPost() in thread 1 failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 55;   
   }
   
   s32ParallelRetval_th1 += ( tS32 )u32Ret;
   
   /*Exit thread*/
   OSAL_vThreadExit();
}


/*****************************************************************************
* FUNCTION     :  Parallel_Access_Thread_2()
* PARAMETER    :  tPVoid
* RETURNVALUE  :  None
* DESCRIPTION  :  Try to Rename / Delete the directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2008
******************************************************************************/
static tVoid Parallel_Access_Thread_2( const tS8 * dev_name )
{
   tU32 u32Ret = 0;
   s32ParallelRetval_th2 = 0;

   /* Wait for other thread */
   if ( OSAL_OK != OSAL_s32EventWait( CFS_parallel_access_2, EVENT_POST_THREAD_2_STOP_WAIT,
                                      OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER,
                                      &ResultMask ) )
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait in thread 2 failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 1;
   }

   if ( dev_name == OSAL_NULL )
   {
      s32ParallelRetval_th2 += ( tS32 )u32Ret;
      OSAL_vThreadExit();
   }

   /* For Other Device */

   /* Delete the directory */
   if ( OSAL_s32IORemove ( ( tCString )dev_name ) != OSAL_ERROR )
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IORemove was ok? This is an unexpected failure");
      u32Ret += 2;
   }

   if ( OSAL_ERROR == OSAL_s32EventPost( CFS_parallel_access_1, EVENT_POST_THREAD_1_STOP_WAIT, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: 1st OSAL_s32EventPost in thread 2 failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 4;
   }
   if ( OSAL_ERROR == OSAL_s32EventPost( CFS_parallel_access_end_2, EVENT_POST_THREAD_2, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: 2nd OSAL_s32EventPost in thread 2 failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 8;
   }
   
   s32ParallelRetval_th2 += ( tS32 )u32Ret;
   
   /*Exit thread*/
   OSAL_vThreadExit();
}


/*****************************************************************************
* FUNCTION     :  u32FSOpenClosedevice( )
* PARAMETER    :  device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_001
* DESCRIPTION  :  Opens and closes all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpenClosedevice( const tS8* dev_name )
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR == hDevice )
   {
      u32Ret += 1;
   }
   else
   {
      /* Close the device */
      if ( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSOpendevInvalParm( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_002
* DESCRIPTION  :  Try to Open all filesystem devices with invalid parameters
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpendevInvalParm( const tS8* dev_name )
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Try to open the device with an invalid device name*/
   hDevice = OSAL_IOOpen( ( tCString )OEDT_C_STRING_DEVICE_INVAL, enAccess );

   if ( OSAL_ERROR != hDevice  )
   {
      u32Ret += 1;

      /* If successfully opened, indicate an error and close the device */
      if ( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
      {
         u32Ret += 2;
      }
   }

   /* Try to open the device with an invalid access mode */
   enAccess = ( OSAL_tenAccess )OEDT_INVALID_ACCESS_MODE;
   hDevice = OSAL_IOOpen( ( tCString )dev_name , enAccess );

   /* If successfully opened, indicate an error and close the device */
   if ( OSAL_ERROR != hDevice )
   {
      u32Ret += 4;

      if ( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
      {
         u32Ret += 8;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSReOpendev( )
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_003
* DESCRIPTION  :  Try to Re-Open all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSReOpendev( const tS8* dev_name )
{
   OSAL_tIODescriptor hDevice1 = 0;
   OSAL_tIODescriptor hDevice2 = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice1 = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR == hDevice1 )
   {
      u32Ret += 1;
   }
   else
   {
      /* Re-open the device, should be successfull */
      hDevice2 = OSAL_IOOpen( ( tCString )dev_name, enAccess );

      if ( OSAL_ERROR == hDevice2  )
      {
         u32Ret += 2;
      }
      else
      {
         /* If open for the second time is successfull, close the second
         device handle */
         if ( OSAL_ERROR == OSAL_s32IOClose ( hDevice2 ) )
         {
            u32Ret += 4;
         }
      }

      /* Now close the first device handle */
      if ( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice1 ) )
      {
         u32Ret += 8;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSOpendevDiffModes( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_004
* DESCRIPTION  :  Try to Re-Open all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*              :  CDROM has to open only with update OSAL_EN_READONLY.
*                  update done by anc3kor  May 20, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpendevDiffModes( const tS8* dev_name )
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR == hDevice  )
   {
      u32Ret += 1;
   }
   else
   {
      if ( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice )  )
      {
         u32Ret += 2;
      }
   }

   /*CDROM shouldn't run for OSAL_EN_READWRITE and  OSAL_EN_WRITEONLY*/
   if (
      OSAL_s32MemoryCompare
      (
         dev_name,
         OSAL_C_STRING_DEVICE_CDROM,
         strlen( OSAL_C_STRING_DEVICE_CDROM )
      )
   )
   {
      /* Open the file in READWRITE mode */
      enAccess = OSAL_EN_READWRITE;
      hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

      if ( OSAL_ERROR == hDevice  )
      {
         u32Ret += 4;
      }
      else
      {  /* If open with this mode is successfull close  the device */
         if ( OSAL_ERROR == OSAL_s32IOClose ( hDevice )  )
         {
            u32Ret += 8;
         }
      }

      /* Open the file in WRITEONLY mode */
      enAccess = OSAL_EN_WRITEONLY;
      hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

      if ( OSAL_ERROR == hDevice  )
      {
         u32Ret += 16;
      }
      else
      {
         /* If open with this mode is successfull close the device */
         if ( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
         {
            u32Ret += 32;
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSClosedevAlreadyClosed( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_005
* DESCRIPTION  :  Try to Re-Open all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32  u32FSClosedevAlreadyClosed( const tS8* dev_name )
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR ==  hDevice )
   {
      u32Ret += 1;
   }
   else
   {
      /* Close the device with the device handle obtained in open */
      if ( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
      {
         u32Ret += 2;
      }
      else
      {
         /* Re Closing , Try to close with the same handle.
         Must not be possible */
         if ( OSAL_ERROR != OSAL_s32IOClose ( hDevice )  )
         {
            u32Ret += 20;
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSOpenClosedir( )
* PARAMETER    :  Root directory name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_006
* DESCRIPTION  :  Try to Open and closes a directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32  u32FSOpenClosedir( const tS8* dev_name )
{
   OSAL_tIODescriptor hDir = 0;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name, ( OSAL_tenAccess )OSAL_EN_READONLY );

   if ( OSAL_ERROR == hDir )
   {
      u32Ret += 1;
   }
   else
   {
      /* Close the directory */
      if ( OSAL_ERROR  == OSAL_s32IOClose ( hDir ) )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSOpendirInvalid( )
* PARAMETER    :  Root directory name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_007
* DESCRIPTION  :  Try to Open invalid directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 u32FSOpendirInvalid( const tPS8* dev_name )
{
   OSAL_tIODescriptor hDir1 = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Try to open a invalid directory  */
   hDir1 = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( OSAL_ERROR != hDir1 )
   {
      u32Ret += 1;

      /* If open is successful, indicate an error and close the directory */
      if ( OSAL_ERROR  == OSAL_s32IOClose( hDir1 ) )
      {
         u32Ret += 2;
      }
   }

   /* Try to open a valid directory with an invalid access mode */
   //Invalid access mode is not handled by OSAL IF
   /*hDir1 = OSAL_IOOpen ( ( tCString )dev_name[1], ( OSAL_tenAccess )OEDT_INVALID_ACCESS_MODE );
   {
      if ( OSAL_ERROR != hDir1 )
      {
         u32Ret += 4;

         /* If open is successful, indicate an error and close the directory * /
         if ( OSAL_OK != OSAL_s32IOClose( hDir1 ) )
         {
            u32Ret += 8;
         }
      }
   }*/
   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateDelDir( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_008
* DESCRIPTION  :  Try create and delete directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32  u32FSCreateDelDir( const tS8* dev_name )
{
   //OSAL_trIOCtrlDir* hDir1 = OSAL_NULL;
   OSAL_tIODescriptor hDir2 = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trIOCtrlDirent rDirent = {""};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Open directory in readwrite mode */
   hDir2 = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if ( hDir2 == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      /* Copy sub-directory name to structure variable */
      ( tVoid )OSAL_szStringCopy ( ( tString )( rDirent.s8Name ),
                                   ( tCString )( SUBDIR_NAME ) );

      /* Create sub-directory */
      if ( OSAL_s32IOControl ( hDir2, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 3;
      }
      else
      {
         /* Remove sub-directory */
         if ( OSAL_s32IOControl ( hDir2, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
      }

      /* Close directory */
      if ( OSAL_s32IOClose ( hDir2 ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateDelSubDir( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_009
* DESCRIPTION  :  Try create and delete sub directory in each root direcroty like /nor0/MYNEW/MYNEW1
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateDelSubDir( const tS8* dev_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szSubdirName1 [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW1";
   tU32 u32Ret = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }

      ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( dev_name ) );
      ( tVoid )OSAL_szStringConcat( temp_ch, "/MYNEW" );
      hDir1 = OSAL_IOOpen ( ( tCString )temp_ch, enAccess );

      if ( hDir1 == OSAL_ERROR )
      {
         u32Ret += 8;
      }
      else
      {
         OSAL_pvMemorySet( rDirent.s8Name, '\0', sizeof( rDirent.s8Name ) );
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName1 );

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 16;
         }

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 35;
         }
         
         if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
         {
            u32Ret += 64;
         }
      }

      OSAL_pvMemorySet( rDirent.s8Name, '\0', sizeof( rDirent.s8Name ) );
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: got '%i' from OSAL_s32IOControl( OSAL_C_S32_IOCTRL_FIORMDIR )", OSAL_u32ErrorCode());
         u32Ret += 128;
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 256;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateDirInvalName( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_010
* DESCRIPTION  :  Try create and delete directory with invalid path
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateDirInvalName( const tPS8* dev_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )dev_name[1] );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) != OSAL_ERROR )
      {
         u32Ret += 2;

         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSRmNonExstngDir( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_011
* DESCRIPTION  :  Try to remove non exsisting directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSRmNonExstngDir( const tS8* dev_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if ( hDir == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME_INVAL );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent ) != OSAL_ERROR )
      {
         u32Ret += 3;
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSRmDirUsingIOCTRL( )
* PARAMETER    :  Devicename and Filename name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_012
* DESCRIPTION  :  Delete directory with which is not a directory (with files)
*                 using IOCTRL
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSRmDirUsingIOCTRL( const tPS8* dev_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0, hDir1 = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir1 = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir1 == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      hFile = OSAL_IOCreate ( ( tCString )dev_name[1], enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )dev_name[1] );

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) != OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 100;
         }
         else if ( OSAL_s32IORemove ( ( tCString )dev_name[1] ) == OSAL_ERROR )
         {
            u32Ret += 220;
         }
      }

      if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
      {
         u32Ret += 150;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSGetDirInfo( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_013
* DESCRIPTION  :  Get directory information
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetDirInfo( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess;
   OSAL_trIOCtrlDirent rDirent1 = {""}, rDirent2 = {""};
   OSAL_trIOCtrlDir rDirCtrl;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }

   ( tVoid )OSAL_szStringCopy ( rDirent1.s8Name, ( tCString )SUBDIR_NAME );
   hDir1 = OSAL_IOOpen ( ( tCString )dev_name , enAccess );

   if ( hDir1 == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if ( bCreateRemove )
      {
         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent1 ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
      }

      if ( u32Ret == 0 )
      {
         /* Get DIR info for root DIR so that it displays the newly created Sub-dir */
         ( tVoid )OSAL_szStringCopy ( rDirent2.s8Name, ( tCString )dev_name );
         rDirCtrl.fd = hDir1;
         rDirCtrl.s32Cookie = 0;
         rDirCtrl.dirent = rDirent2;

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOREADDIR,
                                  ( tS32 ) &rDirCtrl ) == OSAL_ERROR )
         {
            u32Ret += 3;
         }
         else
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_1, "DIR Info :\n\t %s \n",
                               rDirCtrl.dirent.s8Name );

            if ( bCreateRemove )
            {
               if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                        ( tS32 )&rDirent1 ) == OSAL_ERROR )
               {
                  u32Ret += 4;
               }
            }
         }
      }

      if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSOpenDirDiffModes( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_014
* DESCRIPTION  :  Try to open directory in different modes
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*              :  Update the testcase,anc3kor,14-04-2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpenDirDiffModes( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hDir1 = 0;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if ( bCreateRemove )
      {
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME );

         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 3;
         }
      }

      if ( u32Ret == 0 )
      {
         /*In Read only mode*/
         enAccess = OSAL_EN_READONLY;
         hDir1 = OSAL_IOOpen ( ( tCString )dev_name[1], enAccess );

         if ( hDir1 == OSAL_ERROR )
         {
            u32Ret += 40;
         }
         else
         {
            if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
            {
               u32Ret += 80;
            }
         }

         if ( bCreateRemove )
         {
            /*In Read write mode*/
            enAccess = OSAL_EN_READWRITE;
            hDir1 = OSAL_IOOpen ( ( tCString )dev_name[1], enAccess );

            if ( hDir1 == OSAL_ERROR )
            {
               u32Ret += 4;
            }
            else
            {
               if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
               {
                  u32Ret += 5;
               }
            }

            /*In Write only mode*/
            enAccess = OSAL_EN_WRITEONLY;
            hDir1 = OSAL_IOOpen ( ( tCString )dev_name[1], enAccess );

            if ( hDir1 == OSAL_ERROR )
            {
               u32Ret += 10;
            }
            else
            {
               if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
               {
                  u32Ret += 20;
               }
            }

            if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                                     ( tS32 )&rDirent ) == OSAL_ERROR )
            {
               u32Ret += 160;
            }
         }
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 360;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSReOpenDir( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_015
* DESCRIPTION  :  Try to reopen directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 u32FSReOpenDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_trIOCtrlDir* hDir1 = OSAL_NULL, *hDir2 = OSAL_NULL;
   OSAL_tIODescriptor hDir = 0;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess ;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if ( bCreateRemove )
      {
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME );

         /* Create Dir */
         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
      }

      if ( u32Ret == 0 )
      {
         hDir1 = OSALUTIL_prOpenDir ( ( tCString )dev_name[1] );

         if ( OSAL_NULL  == hDir1 )
         {
            u32Ret += 3;
         }
         else
         {
            /* Open the same directory again */
            hDir2 = OSALUTIL_prOpenDir ( ( tCString )dev_name[1] );

            if ( OSAL_NULL  == hDir2 )
            {
               u32Ret += 4;
            }
            else
            {
               /* After opening twice, close the first handle */
               if ( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir2 ) )
               {
                  u32Ret += 5;
               }
            }

            /* Close the first handle */
            if ( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir1 ) )
            {
               u32Ret += 8;
            }
         }

         if ( bCreateRemove )
         {
            if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                                     ( tS32 )&rDirent ) == OSAL_ERROR )
            {
               u32Ret += 160;
            }
         }
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 180;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSDirParallelAccess( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_016
* DESCRIPTION  :  When directory is accessed by another thread try to rename it
*                 delete it
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSDirParallelAccess( const tS8* dev_name )
{
   OSAL_trThreadAttribute rThAttr1, rThAttr2;
   OSAL_tThreadID TID_1, TID_2;
   /**************************************************/
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*thread stuff*/
   rThAttr1.szName  = "Thread_1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )Parallel_Access_Thread_1;
   rThAttr1.pvArg   =   ( tPS8 ) dev_name;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "Thread_2";
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )Parallel_Access_Thread_2;
   rThAttr2.pvArg   =   ( tPS8 ) dev_name;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   ps8UserBuf = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

   if ( OSAL_NULL == ps8UserBuf )
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_pvMemoryAllocate failed");
      u32Ret += 100;
   }
   else
   {  /*Fill the buffer with some value*/
      OSAL_pvMemorySet( ps8UserBuf, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
      OSAL_pvMemorySet( ps8UserBuf, 'a', OEDT_CFST_MB_SIZE );

      /*thread stuff*/
      if ( OSAL_OK == OSAL_s32EventCreate( "Parallel_Access_1", &CFS_parallel_access_1 ) )
      {
        if ( OSAL_ERROR == OSAL_s32EventCreate( "Parallel_Access_2", &CFS_parallel_access_2 ) )
        {
           OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate failed with error %i", OSAL_u32ErrorCode());
           u32Ret += 1000;
        }
        if ( OSAL_ERROR == OSAL_s32EventCreate( "Parallel_Access_End_1", &CFS_parallel_access_end_1 ) )
        {
           OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate failed with error %i", OSAL_u32ErrorCode());
           u32Ret += 2000;
        }
        if ( OSAL_ERROR == OSAL_s32EventCreate( "Parallel_Access_End_2", &CFS_parallel_access_end_2 ) )
        {
           OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate failed with error %i", OSAL_u32ErrorCode());
           u32Ret += 3000;
        }

         /*create and activate thread1 */
         TID_1 = OSAL_ThreadSpawn( &rThAttr1 );

         if ( OSAL_ERROR != TID_1 )
         {
            /*create thread2*/
            TID_2 = OSAL_ThreadSpawn( &rThAttr2 );

            if ( OSAL_ERROR != TID_2 )
            {
               //wait for 2 events signalling the end of read operation
               if ( OSAL_OK != OSAL_s32EventWait( CFS_parallel_access_end_1, EVENT_POST_THREAD_1,
                                                  OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                                  &ResultMask ) )
               {
                  OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait failed with error %i", OSAL_u32ErrorCode());
                  u32Ret += 200;
               }

               if ( OSAL_OK != OSAL_s32EventWait( CFS_parallel_access_end_2, EVENT_POST_THREAD_2,
                                                  OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                                  &ResultMask ) )
               {
                  OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait failed with error %i", OSAL_u32ErrorCode());
                  u32Ret += 200;
               }
            }
            else
            {
               //thread2 creation failed
               OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_ThreadSpawn failed with error %i", OSAL_u32ErrorCode());
               u32Ret += 300;
            }
         }
         else
         {
            // thread1 creation failed
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_ThreadSpawn failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 400;
         }

         //close the events
         if ( OSAL_ERROR == OSAL_s32EventClose( CFS_parallel_access_1 ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 4500;
         }
         if ( OSAL_ERROR == OSAL_s32EventClose( CFS_parallel_access_2 ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 5500;
         }
         if ( OSAL_ERROR == OSAL_s32EventClose( CFS_parallel_access_end_1 ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 6500;
         }
         if ( OSAL_ERROR == OSAL_s32EventClose( CFS_parallel_access_end_2 ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 7500;
         }
         
         //delete the events
         if ( OSAL_ERROR == OSAL_s32EventDelete( "Parallel_Access_1" ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 600;
         }
         if ( OSAL_ERROR == OSAL_s32EventDelete( "Parallel_Access_2" ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 1600;
         }
         if ( OSAL_ERROR == OSAL_s32EventDelete( "Parallel_Access_End_1" ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 2600;
         }
         if ( OSAL_ERROR == OSAL_s32EventDelete( "Parallel_Access_End_2" ) )
         {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 3600;
         }
      }
      else
      {
         //event creation failed
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate failed with error %i", OSAL_u32ErrorCode());
         u32Ret += 700;
      }

      //free the memory allocated
      if ( OSAL_NULL != ps8UserBuf )
         OSAL_vMemoryFree( ps8UserBuf );
   }

   return ( u32Ret + ( tU32 )s32ParallelRetval_th1 + ( tU32 )s32ParallelRetval_th2 );
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateDirMultiTimes( )
* PARAMETER    :  dev_name: device name, and filename
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_017
* DESCRIPTION  :  Try to Create directory with similar name
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateDirMultiTimes( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir1 = 0, hDir2 = 0, hDir3 = 0;
   OSAL_trIOCtrlDirent rDirent1 = {""}, rDirent2 = {""}, rDirent3 = {""};
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir1 = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir1 == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      ( tVoid )OSAL_szStringCopy ( rDirent1.s8Name, ( tCString )SUBDIR_NAME );

      if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent1 ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( rDirent2.s8Name, ( tCString )SUBDIR_NAME2 );
         hDir2 = OSAL_IOOpen ( ( tCString )dev_name[1], enAccess );

         if ( hDir2 == OSAL_ERROR )
         {
            u32Ret += 3;
         }
         else
         {
            if ( OSAL_s32IOControl ( hDir2, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                     ( tS32 )&rDirent2 ) == OSAL_ERROR )
            {
               u32Ret += 4;
            }
            else
            {
               ( tVoid )OSAL_szStringCopy ( rDirent3.s8Name, ( tCString )SUBDIR_NAME3 );
               hDir3 = OSAL_IOOpen ( ( tCString )dev_name[2], enAccess );

               if ( hDir3 == OSAL_ERROR )
               {
                  u32Ret += 5;
               }
               else
               {
                  if ( OSAL_s32IOControl ( hDir3, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                           ( tS32 )&rDirent3 ) == OSAL_ERROR )
                  {
                     u32Ret += 6;
                  }
                  else
                  {
                     OEDT_HelperPrintf( TR_LEVEL_USER_1,
                                        "\n\t Third level Sub-Dir created" );

                     if ( OSAL_s32IOControl ( hDir3, OSAL_C_S32_IOCTRL_FIORMDIR,
                                              ( tS32 )&rDirent3 ) == OSAL_ERROR )
                     {
                        u32Ret += 20;
                     }
                  }

                  OSAL_s32IOClose ( hDir3 );
               }

               if ( OSAL_s32IOControl ( hDir2, OSAL_C_S32_IOCTRL_FIORMDIR,
                                        ( tS32 )&rDirent2 ) == OSAL_ERROR )
               {
                  u32Ret += 40;
               }
            }

            OSAL_s32IOClose ( hDir2 );
         }

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent1 ) == OSAL_ERROR )
         {
            u32Ret += 80;
         }
      }

      OSAL_s32IOClose ( hDir1 );
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateRemDirInvalPath( )
* PARAMETER    :  dev_name: device name and file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_018
* DESCRIPTION  :  Create/ remove directory with Invalid path
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateRemDirInvalPath( const tPS8* dev_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }

   ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )dev_name[1] );

   if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                            ( tS32 )&rDirent ) != OSAL_ERROR )
   {
      u32Ret += 10;
   }

   if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
   {
      u32Ret += 20;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateRmNonEmptyDir( )
* PARAMETER    :  dev_name: device name and file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_019
* DESCRIPTION  :  Try to remove non Empty directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateRmNonEmptyDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir1 = 0, hDir2 = 0;
   OSAL_trIOCtrlDirent rDirent1 = {""}, rDirent2 = {""};
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir1 = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir1 == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      ( tVoid )OSAL_szStringCopy ( ( tString )rDirent1.s8Name, ( tCString )SUBDIR_NAME );

      if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent1 ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         hDir2 = OSAL_IOOpen ( ( tCString )dev_name[1], enAccess );

         if ( hDir2 == OSAL_ERROR )
         {
            u32Ret += 3;

            if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                     ( tS32 )&rDirent1 ) == OSAL_ERROR )
            {
               u32Ret += 7;
            }
         }
         else
         {
            ( tVoid )OSAL_szStringCopy ( rDirent2.s8Name, ( tCString )SUBDIR_NAME2 );

            if ( OSAL_s32IOControl( hDir2, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                    ( tS32 )&rDirent2 ) == OSAL_ERROR )
            {
               u32Ret += 14;
               OSAL_s32IOClose ( hDir2 );

               if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                        ( tS32 )&rDirent1 ) == OSAL_ERROR )
               {
                  u32Ret += 28;
               }
            }
            else
            {
               if ( OSAL_s32IOControl( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                       ( tS32 )&rDirent1 ) != OSAL_ERROR )
               {
                  u32Ret += 56;
               }
               else
               {
                  if ( OSAL_s32IOControl( hDir2, OSAL_C_S32_IOCTRL_FIORMDIR,
                                          ( tS32 )&rDirent2 ) == OSAL_ERROR )
                  {
                     u32Ret += 112;
                  }
                  else
                  {
                     OSAL_s32IOClose ( hDir2 );

                     if ( OSAL_s32IOControl ( hDir1,
                                              OSAL_C_S32_IOCTRL_FIORMDIR,
                                              ( tS32 )&rDirent1 ) == OSAL_ERROR )
                     {
                        u32Ret += 224;
                     }
                  }
               }
            }
         }

         OSAL_s32IOClose ( hDir1 );
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCopyDir( )
* PARAMETER    :  dev_name: device name and file names
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_020
* DESCRIPTION  :  Copy the files in source directory to destination directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCopyDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_tIODescriptor hFile2 = 0;
   OSAL_trIOCtrlDir* pDir = OSAL_NULL;
   OSAL_trIOCtrlDirent* pEntry = OSAL_NULL;
   tS32 hDir1 = 0;
   tS32 hDir2 = 0;
   tU32 u32Ret = 0;
   tChar* arrFileName_tmp[2] = {OSAL_NULL};
   tChar arrFileName[2][256] = {"", ""};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name[0], enAccess );

   if ( OSAL_ERROR == hDevice )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      /*Create the source directory  in readwrite mode */
      hDir1 = OSALUTIL_s32CreateDir ( hDevice, "/"DIR1 );

      if ( hDir1 == OSAL_ERROR )
      {
         u32Ret += 2;
         OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSALUTIL_s32CreateDir() fails with error '%i'", OSAL_u32ErrorCode());
         OSAL_s32IOClose( hDevice );
      }
      else
      {
         /*Create the destination directory  in readwrite mode */
         hDir2 = OSALUTIL_s32CreateDir ( hDevice, "/"DIR );

         if ( hDir2 == OSAL_ERROR )
         {
            u32Ret += 3;
            OSAL_s32IOClose( hDevice );
         }
         else
         {
            /*Create the first file in source directory */
            hFile1 = OSAL_IOCreate( ( tCString )dev_name[1] , enAccess );

            if ( hFile1 == OSAL_ERROR )
            {
               u32Ret += 4;
               OSALUTIL_s32RemoveDir( hDevice, DIR1 );
               OSAL_s32IOClose( hDevice );
            }
            else
            {
               /*Create second file in source directory*/
               hFile2 = OSAL_IOCreate ( ( tCString )dev_name[2] , enAccess );

               if ( hFile2 == OSAL_ERROR )
               {
                  u32Ret += 5;
                  OSAL_s32IOClose( hFile1 );
                  OSAL_s32IORemove( ( tCString )dev_name[1] );
                  OSALUTIL_s32RemoveDir( hDevice, DIR1 );
                  OSALUTIL_s32RemoveDir( hDevice, DIR );
                  OSAL_s32IOClose( hDevice );
               }
               else
               {
                  ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                  ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR1 ); /* Source     Directory */
                  ( tVoid )OSAL_szStringCopy ( arrFileName[1], ( tCString )dev_name[0] );
                  ( tVoid )OSAL_szStringConcat( arrFileName[1], "/"DIR ); /* Source     Directory */
                  arrFileName_tmp[0] = arrFileName[0];
                  arrFileName_tmp[1] = arrFileName[1];

                  if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIOCOPYDIR,
                                                        ( tS32 )arrFileName_tmp ) )
                  {
                     u32Ret += 6;
                     OSAL_s32IOClose( hFile1 );
                     OSAL_s32IOClose( hFile2 );
                     OSAL_s32IORemove( ( tCString )dev_name[1] );
                     OSAL_s32IORemove( ( tCString )dev_name[2] );
                     OSALUTIL_s32RemoveDir( hDevice, DIR1 );
                     OSALUTIL_s32RemoveDir( hDevice, DIR );
                     OSAL_s32IOClose( hDevice );
                  }
                  else
                  {
                     pDir = OSALUTIL_prOpenDir ( arrFileName_tmp[1] );
                     pEntry = OSALUTIL_prReadDir( pDir );

                     if ( OSAL_NULL == pDir )
                     {
                        u32Ret += 25;
                     }
                     else
                     {
                        while ( pEntry != OSAL_NULL )
                        {
                           OEDT_HelperPrintf( TR_LEVEL_USER_1, "\tDir read value %s\n", pEntry->s8Name );
                           pEntry = OSALUTIL_prReadDir( pDir );
                        }
                     }

                     OSALUTIL_s32CloseDir ( pDir );

                     /*Close the first file in source directory*/
                     if  ( OSAL_ERROR == OSAL_s32IOClose( hFile1 ) )
                     {
                        u32Ret += 7;
                        OSAL_s32IOClose( hDevice );
                     }
                     else
                     {
                        /*Close the second file in source directory*/
                        if ( OSAL_ERROR == OSAL_s32IOClose( hFile2 ) )
                        {
                           u32Ret += 8;
                           OSAL_s32IOClose( hDevice );
                        }
                        else
                        {
                           /*Remove the first file in the source directory*/
                           if ( OSAL_ERROR ==  OSAL_s32IORemove( ( tCString )dev_name[1] ) )
                           {
                              u32Ret += 9;
                              OSAL_s32IOClose( hDevice );
                           }
                           else
                           {
                              /*Remove the second file in the source directory*/
                              if  ( OSAL_ERROR ==  OSAL_s32IORemove( ( tCString )dev_name[2] ) )
                              {
                                 u32Ret += 10;
                                 OSAL_s32IOClose( hDevice );
                              }
                              else
                              {
                                 /* Remove the first copied file in the destination directory*/
                                 ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                                 ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR"/File13.txt" );

                                 if ( OSAL_ERROR ==  OSAL_s32IORemove( arrFileName[0] ) )
                                 {
                                    u32Ret += 11;
                                    OSAL_s32IOClose( hDevice );
                                 }
                                 else
                                 {
                                    /*Remove the second copied file in the destination  directory*/
                                    ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                                    ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR"/File14.txt" );

                                    if ( OSAL_ERROR ==  OSAL_s32IORemove( arrFileName[0] ) )
                                    {
                                       u32Ret += 12;
                                       OSAL_s32IOClose( hDevice );
                                    }
                                    else
                                    {
                                       /*Remove the source directory */
                                       if ( OSAL_ERROR == OSALUTIL_s32RemoveDir(
                                                hDevice, DIR1 ) )
                                       {
                                          u32Ret += 13;
                                          OSAL_s32IOClose( hDevice );
                                       }
                                       else
                                       {
                                          /*Remove the destination directory*/
                                          if  ( OSAL_ERROR == OSALUTIL_s32RemoveDir(
                                                   hDevice, DIR ) )
                                          {
                                             u32Ret += 14;
                                             OSAL_s32IOClose( hDevice );
                                          }
                                          else
                                          {
                                             if  ( OSAL_ERROR == OSAL_s32IOClose
                                                   ( hDevice ) )
                                             {
                                                u32Ret += 15;
                                             }
                                          }/*Sucess of removal of destination directory*/
                                       }/*Sucess of removal of source directory*/
                                    }/*Sucess of removal of second file in destination*/
                                 }/*Sucess of removal of first file in destination*/
                              }/*Sucess of removal of second file in source*/
                           }/*Sucess of removal of first file in source*/
                        }/*Sucess of closure of second file in source*/
                     }/*Sucess of closure of first file in source*/
                  }/*Sucess of IO Control function*/
               }/*Sucess of creation of second file in source*/
            }/*Sucess of creation of first file in source*/
         }/*Sucess of creation of destination directory*/
      }/*   Sucess of creation of source directory*/
   }/*Sucess of open device*/

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSMultiCreateDir( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_021
* DESCRIPTION  :  Create multiple sub directories
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSMultiCreateDir( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name, enAccess ); /* open the root dir */

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME3 );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent ) != OSAL_ERROR )
         {
            u32Ret += 3;
         }

         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 100;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateSubDir( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_022
* DESCRIPTION  :  Attempt to create sub-directory within a directory opened
*                 in READONLY modey
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateSubDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlDirent rDirent = {""};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }

      hDir1 = OSAL_IOOpen ( ( tCString )dev_name[1], OSAL_EN_READONLY );

      if ( hDir == OSAL_ERROR )
      {
         u32Ret += 20;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME2 );

         if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  ( tS32 )&rDirent ) != OSAL_ERROR )
         {
            /* If successful, indicate error and remove directory */
            u32Ret += 50;

            if ( OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR,
                                     ( tS32 )&rDirent ) == OSAL_ERROR )
            {
               u32Ret += 100;
            }
         }
      }

      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )SUBDIR_NAME );

      if ( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
      {
         u32Ret += 130;
      }

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 260;
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 520;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSDelInvDir ( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_023
* DESCRIPTION  :  Delete invalid Directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32  u32FSDelInvDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDir = OSAL_IOOpen ( ( tCString )dev_name[0], enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if ( bCreateRemove )
      {
         ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, ( tCString )dev_name[1] );

         if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) != OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 50;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSCopyDirRec( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_024
* DESCRIPTION  :  Copy directories recursively
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32  u32FSCopyDirRec( const tPS8* dev_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_tIODescriptor hFile2 = 0;
   OSAL_trIOCtrlDir* pDir = OSAL_NULL;
   OSAL_trIOCtrlDirent* pEntry = OSAL_NULL;
   tS32 hDir1 = 0;
   tS32 hDir2 = 0;
   tU32 u32Ret = 0;
   tChar* arrFileName_tmp[2] = {OSAL_NULL};
   tChar arrFileName[2][256] = {"", ""};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name[0], enAccess );

   if ( OSAL_ERROR == hDevice )
   {
      u32Ret += 1;
   }
   else
   {
      /*Create the source directory  in readwrite mode */
      hDir1 = OSALUTIL_s32CreateDir ( hDevice, "/"DIR1 );

      if ( hDir1 == OSAL_ERROR )
      {
         u32Ret += 2;
         OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSALUTIL_s32CreateDir() fails with error '%i'", OSAL_u32ErrorCode());
         OSAL_s32IOClose( hDevice );
      }
      else
      {
         /*Create the destination directory  in readwrite mode */
         hDir2 = OSALUTIL_s32CreateDir ( hDevice, "/"DIR );

         if ( hDir2 == OSAL_ERROR )
         {
            u32Ret += 3;
            OSAL_s32IOClose( hDevice );
         }
         else
         {  /*Create the first file in source directory */
            hFile1 = OSAL_IOCreate( ( tCString )dev_name[1] , enAccess );

            if ( hFile1 == OSAL_ERROR )
            {
               u32Ret += 4;
               OSALUTIL_s32RemoveDir( hDevice, DIR1 );
               OSAL_s32IOClose( hDevice );
            }
            else
            {
               /*Create second file in source directory*/
               hFile2 = OSAL_IOCreate ( ( tCString )dev_name[2] , enAccess );

               if ( hFile2 == OSAL_ERROR )
               {
                  u32Ret += 5;
                  OSAL_s32IOClose( hFile1 );
                  OSAL_s32IORemove( ( tCString )dev_name[1] );
                  OSALUTIL_s32RemoveDir( hDevice, DIR1 );
                  OSAL_s32IOClose( hDevice );
               }
               else
               {
                  ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                  ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR1 ); /* Source    Directory */
                  ( tVoid )OSAL_szStringCopy ( arrFileName[1], ( tCString )dev_name[0] );
                  ( tVoid )OSAL_szStringConcat( arrFileName[1], "/"DIR ); /* Source    Directory */
                  arrFileName_tmp[0] = arrFileName[0];
                  arrFileName_tmp[1] = arrFileName[1];

                  if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIOCOPYDIR,
                                                        ( tS32 )arrFileName_tmp ) )
                  {
                     u32Ret += 6;
                     OSAL_s32IOClose( hFile1 );
                     OSAL_s32IOClose( hFile2 );
                     OSAL_s32IORemove( ( tCString )dev_name[1] );
                     OSAL_s32IORemove( ( tCString )dev_name[2] );
                     OSALUTIL_s32RemoveDir( hDevice, DIR1 );
                     OSALUTIL_s32RemoveDir( hDevice, DIR );
                     OSAL_s32IOClose( hDevice );
                  }
                  else
                  {
                     pDir = OSALUTIL_prOpenDir ( arrFileName_tmp[1] );
                     pEntry = OSALUTIL_prReadDir( pDir );

                     if ( OSAL_NULL == pDir )
                     {
                        u32Ret += 25;
                     }
                     else
                     {
                        while ( pEntry != OSAL_NULL )
                        {
                           OEDT_HelperPrintf( TR_LEVEL_USER_1, "\tDir read value %s\n", pEntry->s8Name );
                           pEntry = OSALUTIL_prReadDir( pDir );
                        }
                     }

                     OSALUTIL_s32CloseDir ( pDir );

                     /*Close the first file in source directory*/
                     if  ( OSAL_ERROR == OSAL_s32IOClose( hFile1 ) )
                     {
                        u32Ret += 7;
                        OSAL_s32IOClose( hDevice );
                     }
                     else
                     {
                        /*Close the second file in source directory*/
                        if ( OSAL_ERROR == OSAL_s32IOClose( hFile2 ) )
                        {
                           u32Ret += 8;
                           OSAL_s32IOClose( hDevice );
                        }
                        else
                        {
                           /*Remove the first file in the source directory*/
                           if ( OSAL_ERROR ==  OSAL_s32IORemove( ( tCString )dev_name[1] ) )
                           {
                              u32Ret += 9;
                              OSAL_s32IOClose( hDevice );
                           }
                           else
                           {
                              /*Remove the second file in the source directory*/
                              if  ( OSAL_ERROR ==  OSAL_s32IORemove( ( tCString )dev_name[2] ) )
                              {
                                 u32Ret += 10;
                                 OSAL_s32IOClose( hDevice );
                              }
                              else
                              {
                                 /* Remove the first copied file in the destination directory*/
                                 ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                                 ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR"/File13.txt" );

                                 if ( OSAL_ERROR ==  OSAL_s32IORemove( arrFileName[0] ) )
                                 {
                                    u32Ret += 11;
                                    OSAL_s32IOClose( hDevice );
                                 }
                                 else
                                 {
                                    /*Remove the second copied file in the destination  directory*/
                                    ( tVoid )OSAL_szStringCopy ( arrFileName[0], ( tCString )dev_name[0] );
                                    ( tVoid )OSAL_szStringConcat( arrFileName[0], "/"DIR"/File14.txt" );

                                    if ( OSAL_ERROR ==  OSAL_s32IORemove( arrFileName[0] ) )
                                    {
                                       u32Ret += 12;
                                       OSAL_s32IOClose( hDevice );
                                    }
                                    else
                                    {
                                       /*Remove the source directory */
                                       if ( OSAL_ERROR == OSALUTIL_s32RemoveDir(
                                                hDevice, DIR1 ) )
                                       {
                                          u32Ret += 13;
                                          OSAL_s32IOClose( hDevice );
                                       }
                                       else
                                       {
                                          /*Remove the destination directory*/
                                          if  ( OSAL_ERROR == OSALUTIL_s32RemoveDir(
                                                   hDevice, DIR ) )
                                          {
                                             u32Ret += 14;
                                             OSAL_s32IOClose( hDevice );
                                          }
                                          else
                                          {
                                             if  ( OSAL_ERROR == OSAL_s32IOClose
                                                   ( hDevice ) )
                                             {
                                                u32Ret += 15;
                                             }
                                          }/*Sucess of removal of destination directory*/
                                       }/*Sucess of removal of source directory*/
                                    }/*Sucess of removal of second file in destination*/
                                 }/*Sucess of removal of first file in destination*/
                              }/*Sucess of removal of second file in source*/
                           }/*Sucess of removal of first file in source*/
                        }/*Sucess of closure of second file in source*/
                     }/*Sucess of closure of first file in source*/
                  }/*Sucess of IO Control function*/
               }/*Sucess of creation of second file in source*/
            }/*Sucess of creation of first file in source*/
         }/*Sucess of creation of destination directory*/
      }/*   Sucess of creation of source directory*/
   }/*Sucess of open device*/

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSRemoveDir ( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_025
* DESCRIPTION  :  Remove directories recursively
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSRemoveDir( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_tIODescriptor hFile2 = 0;
   tU32 u32Ret = 0;
   tChar temp_ch[200] = {OSAL_NULL};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Open the device in read write mode */
   hDevice = OSAL_IOOpen( ( tCString )dev_name[0], enAccess );

   if ( hDevice == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      /*Create the directory in the device */
      if ( OSAL_ERROR == OSALUTIL_s32CreateDir( hDevice, DIR ) )
      {
         u32Ret += 20;
      }

      /*Create some files in read write mode */
      hFile1 = OSAL_IOCreate( ( tCString )dev_name[1], enAccess );

      if ( hFile1 == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         if ( OSAL_ERROR == OSALUTIL_s32CreateDir( hDevice, DIR"/"DIR_RECR1 ) )
         {
            u32Ret += 30;
         }

         /*Create another file */
         hFile2 = OSAL_IOCreate ( ( tCString )dev_name[3] , enAccess );

         if ( hFile2 == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            OSAL_s32IOClose( hFile1 );
            OSAL_s32IOClose( hFile2 );

            if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
                                                  ( tS32 ) DIR ) )
            {
               u32Ret += 3;
               OSAL_s32IORemove( ( tCString )dev_name[1] );
               OSAL_s32IORemove( ( tCString )dev_name[2] );
               ( tVoid )OSAL_szStringCopy ( temp_ch, ( tCString )dev_name[0] );
               ( tVoid )OSAL_szStringConcat( temp_ch, "/"DIR );
               OSALUTIL_s32RemoveDir( hDevice, temp_ch );
               ( tVoid )OSAL_szStringCopy ( temp_ch, ( tCString )dev_name[0] );
               ( tVoid )OSAL_szStringConcat( temp_ch, "/"DIR"/"DIR_RECR1 );
               OSALUTIL_s32RemoveDir( hDevice, temp_ch );
               OSAL_s32IOClose( hDevice );
            }
            else
            {
               /*Remove the first file in the dir*/
               if ( OSAL_ERROR !=  OSAL_s32IORemove( ( tCString )dev_name[1] ) )
               {
                  u32Ret += 6;
                  OSAL_s32IOClose( hDevice );
               }
               else
               {
                  /*Remove the  second file in the dir*/
                  if ( OSAL_ERROR !=  OSAL_s32IORemove( ( tCString )dev_name[2] ) )
                  {
                     u32Ret += 7;
                     OSAL_s32IOClose( hDevice );
                  }
                  else
                  {
                     /*Check for directory removal*/
                     ( tVoid )OSAL_szStringCopy ( temp_ch, ( tCString )dev_name[0] );
                     ( tVoid )OSAL_szStringConcat( temp_ch, "/"DIR );

                     if ( OSAL_ERROR != OSALUTIL_s32RemoveDir( hDevice,
                           temp_ch ) )
                     {
                        u32Ret += 8;
                        OSAL_s32IOClose( hDevice );
                     }
                     else
                     {
                        /*Close the device */
                        if ( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
                        {
                           u32Ret += 9;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileCreateDel( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_026
* DESCRIPTION  :  Create/ Delete file  with correct parameters
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateDel( const tS8* dev_name )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOCreate ( ( tCString )dev_name, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 3;
      }
      else if ( OSAL_s32IORemove ( ( tCString )dev_name ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileOpenClose( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_027
* DESCRIPTION  :  Open /close File
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileOpenClose( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      enAccess = OSAL_EN_READWRITE;

      if ( ( hFile = OSAL_IOCreate ( ( tCString )dev_name, enAccess ) )
            == OSAL_ERROR )
      {
         u32Ret += 1;
      }
   }
   else
   {
      enAccess = OSAL_EN_READONLY;

      if ( ( hFile = OSAL_IOOpen( ( tCString )dev_name, enAccess ) )
            == OSAL_ERROR )
      {
         u32Ret += 1;
      }
   }

   if ( u32Ret == 0 )
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 10;

            if ( OSAL_s32IORemove ( ( tCString )dev_name )
                  == OSAL_ERROR )
            {
               u32Ret += 50;
            }
         }
         else
         {
            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
            else if ( bCreateRemove )
            {
               if ( OSAL_s32IORemove ( ( tCString )dev_name )
                     == OSAL_ERROR )
               {
                  u32Ret += 30;
               }
            }
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileOpenInvalPath( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_028
* DESCRIPTION  :  Open file with invalid path name (should fail)
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileOpenInvalPath( const tS8* dev_name )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if (  OSAL_ERROR != hFile  )
   {
      u32Ret += 1;

      /* If Open is successfull, indicate an error and close the file */
      if ( OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileOpenInvalParam( )
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_029
* DESCRIPTION  :  Open a file with invalid parameters (should fail),
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileOpenInvalParam( const tS8* dev_name,  tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = ( OSAL_tenAccess )OEDT_INVALID_ACCESS_MODE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, ( tBool )bCreateRemove ) )
   {
      return FILE_CREATE_FAIL;
   }

   hFile = OSAL_IOOpen( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR != hFile )
   {
      u32Ret += 4;

      /* If open is successfull, indicate an error and close the file */
      if ( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
      {
         u32Ret += 8;
      }
   }

   if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      u32Ret += FILE_REMOVE_FAIL ;

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReOpen( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_030
* DESCRIPTION  :  Try to open and close the file which is already opened
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32  u32FSFileReOpen( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile1 = 0, hFile2 = 0, hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      hFile = OSAL_IOCreate ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile1 = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( OSAL_ERROR == hFile1 )
      {
         u32Ret += 1;
      }
      else
      {
         /* Re Opening The File, should be possible */
         hFile2 = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

         if ( OSAL_ERROR == hFile2 )
         {
            u32Ret += 2;
         }
         else
         {
            /* close the second handle of the file */
            if ( OSAL_ERROR == OSAL_s32IOClose ( hFile2 ) )
            {
               u32Ret += 4;
            }
         }

         /* Close the first handle of the file */
         if (  OSAL_ERROR == OSAL_s32IOClose ( hFile1 ) )
         {
            u32Ret += 8;
         }

         if ( bCreateRemove )
         {
            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               u32Ret += 3;
            }

            if ( OSAL_s32IORemove ( ( tCString )dev_name ) == OSAL_ERROR )
            {
               u32Ret += 2;
            }
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileRead( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_031
* DESCRIPTION  :  Read data from already exsisting file
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*              :       Updated by Deepak Kumar(RBEI/ECF5) on Mar 26, 2015
******************************************************************************/
tU32 u32FSFileRead( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 10;
   tS32 s32BytesRead = 0;
   tU32 u32Ret = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

         if ( ps8ReadBuffer == OSAL_NULL )
         {
            u32Ret += 2;
         }
         else
         {
            s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                            u32BytesToRead );

            if ( ( s32BytesRead ==  ( tS32 )OSAL_ERROR ) || ( s32BytesRead !=  ( tS32 )u32BytesToRead ) )
            {
               u32Ret += 3;
            }
            else
            {
               OEDT_HelperPrintf( TR_LEVEL_USER_1, "\n Bytes read : %d",
                                  s32BytesRead );
            }
		//commented to fix lint warning
         //   if ( ps8ReadBuffer)
         //  {
            
            
               OSAL_vMemoryFree( ps8ReadBuffer );
               ps8ReadBuffer = OSAL_NULL;
            
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileWrite( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_032
* DESCRIPTION  :  Write data to an existing file
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileWrite( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tCS8 DataToWrite[50] = "Writing data abcdefghijklmnopqrst";
   tU32 u32Ret = 0;
   tU32 u8BytesToWrite = 15; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( ( hFile = ( OSAL_IOCreate ( ( tCString )dev_name, enAccess ) ) ) == OSAL_ERROR )
      {
         u32Ret += 1;
      }
   }

   if ( u32Ret == 0 )
   {
      s32BytesWritten = OSAL_s32IOWrite ( hFile, DataToWrite, u8BytesToWrite );

      if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) ||
            ( s32BytesWritten != ( tS32 )u8BytesToWrite )  )
      {
         u32Ret += 3;
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
      else if ( OSAL_s32IORemove ( ( tCString )dev_name ) )
      {
         u32Ret += 30;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSGetPosFrmBOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_033
* DESCRIPTION  :  Get File Position from begining of file
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetPosFrmBOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tS32 s32PosToSet = 10;
   tU32 s32FilePos = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         return COMMON_FILE_ERROR;
      }
   }

   /*Open the commmon file*/
   hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   /*Check for success of open*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      /*Seek the file to desired position*/
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet
                             ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         /*Get position of file pointer from beginning of common file*/
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,
                                  ( tS32 )&s32FilePos ) == OSAL_ERROR )
         {
            u32Ret += 3;
         }
         else
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_1,
                               "\n\t Bytes between Current Position and BOF : %d",
                               s32FilePos );
         }
      }

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }
   }

   /*Remove the common file*/
   if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
   {
      u32Ret += COMMON_FILE_ERROR;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSGetPosFrmEOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_034
* DESCRIPTION  :  Get File Position from end of file
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetPosFrmEOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32Ret = 0;
   tS32 s32FilePos = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += COMMON_FILE_ERROR;
      }
   }

   if ( u32Ret == 0 )
   {
      /*Open the common file*/
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         /*Call the control function*/
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                  ( tS32 )&s32FilePos ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_1,
                               "\n\t Bytes between Current Position and EOF : %d"
                               , s32FilePos );
         }

         /*Close the common file*/
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      /*Remove the common file*/
      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += COMMON_FILE_ERROR;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadNegOffsetFrmBOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_035
* DESCRIPTION  :  Read with a negative offset from BOF
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadNegOffsetFrmBOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tS32 ReadRet = 0;
   tS32 s32PosToSet = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         return COMMON_FILE_ERROR;
      }
   }

   hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

   if ( OSAL_ERROR == hFile )
   {
      u32Ret += 2;
   }
   else
   {
      s32PosToSet = OEDT_CDROM_NEGATIVE_POS_TO_SET;

      /* Seek to the byte position specified */
      if ( OSAL_ERROR  != OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                              s32PosToSet ) )
      {
         /*Dynamically Allocate Memory for the read buffer */
         ps8ReadBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( OEDT_CDROM_BYTES_TO_READ );
         ReadRet = OSAL_s32IORead( hFile, ps8ReadBuffer, OEDT_CDROM_BYTES_TO_READ );

         if ( OSAL_ERROR != ReadRet )
         {
            u32Ret += 4;
         }
      }

      if ( OSAL_ERROR == OSAL_s32IOClose( hFile ) )
      {
         u32Ret += 16;
      }
   }

   /* free the dynamically allocated memory */
   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   /*Remove the common file*/
   if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
   {
      u32Ret += COMMON_FILE_ERROR;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadOffsetBeyondEOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_036
* DESCRIPTION  :  Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadOffsetBeyondEOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tS32 s32PosToSet = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tS32 s32BytesRead = 0;
   tS32 s32FileSz = 0;
   tU32 u32BytesToRead = 10;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                  s32FileSz ) != OSAL_ERROR )
         {
            /* Set position at EOF */
            s32PosToSet = OSALUTIL_s32FGetSize( hFile );

            if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                     s32PosToSet ) == OSAL_ERROR )
            {
               u32Ret += 2;
            }
            else
            {
               ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate(
                                  u32BytesToRead + 1 );

               if ( ps8ReadBuffer == OSAL_NULL )
               {
                  u32Ret += 3;
               }
               else
               {
                  s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                                  u32BytesToRead );

                  if ( s32BytesRead > 0 )
                  {
                     u32Ret += 4;
                  }
               }
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( ps8ReadBuffer != OSAL_NULL )
         {
            OSAL_vMemoryFree( ps8ReadBuffer );
            ps8ReadBuffer = OSAL_NULL;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadOffsetFrmBOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_037
* DESCRIPTION  :  Read from offset from Beginning of file
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadOffsetFrmBOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32Ret = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 10;
   tS32 s32BytesRead = 0;
   tS32 s32PosToSet = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         /* Set position at an Offset from BOF */
         s32PosToSet = 5;

         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                  s32PosToSet ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate( u32BytesToRead + 1 );

            if ( ps8ReadBuffer == OSAL_NULL )
            {
               u32Ret += 3;
            }
            else
            {
               s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                               u32BytesToRead );

               if ( ( s32BytesRead == ( tS32 )OSAL_ERROR ) ||
                     ( s32BytesRead != ( tS32 )u32BytesToRead ) )
               {
                  u32Ret += 4;
               }
               else
               {
                  OEDT_HelperPrintf( TR_LEVEL_USER_1,
                                     "\n\t Bytes read: %d ", s32BytesRead );
               }
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( ps8ReadBuffer != OSAL_NULL )
         {
            OSAL_vMemoryFree( ps8ReadBuffer );
            ps8ReadBuffer = OSAL_NULL;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadOffsetFrmEOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_038
* DESCRIPTION  :  Read file with few offsets from EOF
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadOffsetFrmEOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32Ret = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 10;
   tS32 s32FilePos = 0;
   tS32 s32BytesRead = 0;
   tS32 s32PosToSet = 0;
   tS32 s32offFrmEOF = 10;  /* This is the offset from EOF */

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                  ( tS32 )&s32FilePos ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            s32PosToSet = s32FilePos - s32offFrmEOF;

            if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                     s32PosToSet ) == OSAL_ERROR )
            {
               u32Ret += 3;
            }
            else
            {
               ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate(
                                  u32BytesToRead + 1 );

               if ( ps8ReadBuffer == OSAL_NULL )
               {
                  u32Ret += 4;
               }
               else
               {
                  s32BytesRead = OSAL_s32IORead( hFile, ps8ReadBuffer,
                                                 u32BytesToRead );

                  if ( ( s32BytesRead == ( tS32 )OSAL_ERROR ) || ( s32BytesRead !=
                        ( tS32 )u32BytesToRead ) )
                  {
                     u32Ret += 5;
                  }
                  else
                  {
                     OEDT_HelperPrintf( TR_LEVEL_USER_1,
                                        "\n\t Bytes read: %d ", s32BytesRead );
                  }
               }
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( ps8ReadBuffer != OSAL_NULL )
         {
            OSAL_vMemoryFree( ps8ReadBuffer );
            ps8ReadBuffer = OSAL_NULL;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadEveryNthByteFrmBOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_039
* DESCRIPTION  :  Reads file by skipping certain offsets at specified intervals.
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadEveryNthByteFrmBOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tS32 s32FilePos = 0;
   tS32 s32BytesRead = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tS32 s32OffsetFrmStart = 0;
   tU32 u32BytesToRead = 10;/*Number of  bytes to read */

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         s32OffsetFrmStart = 4;   /* This is the value of 'N' */

         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                  ( tS32 )&s32FilePos ) == OSAL_ERROR )
         {
            u32Ret += 289;
         }

         ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate(
                            u32BytesToRead + 1 );

         if ( ps8ReadBuffer == OSAL_NULL )
         {
            u32Ret += 3;
         }
         else
         {
            while ( s32OffsetFrmStart < OSALUTIL_s32FGetSize( hFile ) )
            {
               if ( OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                       s32OffsetFrmStart ) == OSAL_ERROR )
               {
                  u32Ret += 2;
                  break;
               }
               else
               {
                  s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                                  u32BytesToRead );

                  if ( s32BytesRead == OSAL_ERROR )
                  {
                     u32Ret += 4;
                     break;
                  }
               }

               s32OffsetFrmStart += s32OffsetFrmStart;
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( ps8ReadBuffer != OSAL_NULL )
         {
            OSAL_vMemoryFree( ps8ReadBuffer );
            ps8ReadBuffer = OSAL_NULL;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileReadEveryNthByteFrmEOF( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_040
* DESCRIPTION  :  Reads file by skipping certain offsets at specified intervals.
*                 (This is from EOF)
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadEveryNthByteFrmEOF( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tS32 s32PosToSet = 0;
   tS32 s32BytesRead = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32FileSz = 0;
   tS32 s32OffSet = 5; /* This is the offset from EOF */
   tU32 u32BytesToRead = 10;/*Number of  bytes to read */

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                  ( tS32 )&u32FileSz ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            /* Set position at EOF */
            //u32FileSz = OSALUTIL_s32FGetSize(hFile);
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate( u32BytesToRead + 1 );

            if ( ps8ReadBuffer == OSAL_NULL )
            {
               u32Ret += 4;
            }
            else
            {
               s32PosToSet = ( tS32 )u32FileSz - s32OffSet;
               //PQM_authorized_multi_589: Disable the lint info 681:Boolean within 'if' always evaluates to False 
               //lint -e681
               while ( s32PosToSet > 0 )
               {
                  if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                           s32PosToSet ) == OSAL_ERROR )
                  {
                     u32Ret += 3;
                     break;
                  }
                  else
                  {
                     s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                                     u32BytesToRead );

                     if ( s32BytesRead == ( tS32 )OSAL_ERROR )
                     {
                        u32Ret += 5;
                        break;
                     }
                  }

                  s32PosToSet = ( s32PosToSet - ( s32OffSet + ( tS32 )( u32FileSz / 1000 ) ) ) ;
               }
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }

         if ( ps8ReadBuffer != OSAL_NULL )
         {
            OSAL_vMemoryFree( ps8ReadBuffer );
            ps8ReadBuffer = OSAL_NULL;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSGetFileCRC( )
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                                FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_041
* DESCRIPTION  :  Get CRC value
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetFileCRC( const tS8* dev_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tS32 s32FilePosFrmEnd = 0;
   tS32 s32FilePosFrmStart = 0;
   tS8 as8ReadBuffer [2] = {OSAL_NULL};
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( u32ComFSCreateCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
      {
         u32Ret += FILE_CREATE_FAIL ;
      }
   }

   if ( u32Ret == 0 )
   {
      hFile = OSAL_IOOpen ( ( tCString )dev_name, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 1;
      }
      else
      {
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,
                                  ( tS32 )&s32FilePosFrmStart ) == OSAL_ERROR )
         {
            u32Ret += 2;
         }
         else
         {
            if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                                     ( tS32 )&s32FilePosFrmEnd ) == OSAL_ERROR )
            {
               u32Ret += 3;
            }
            else
            {
               if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                        s32FilePosFrmStart + s32FilePosFrmEnd - 2 ) == OSAL_ERROR )
               {
                  u32Ret += 4;
               }
               else
               {
                  if ( OSAL_s32IORead ( hFile, as8ReadBuffer, 2
                                      ) == OSAL_ERROR )
                  {
                     u32Ret += 5;
                  }
                  else
                  {
                     OEDT_HelperPrintf( TR_LEVEL_USER_1,
                                        "\n\t data read: %s ", as8ReadBuffer );
                  }
               }
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }

      if ( u32ComFSRemoveCommonFile( ( tPS8 )dev_name, bCreateRemove ) )
         u32Ret += FILE_REMOVE_FAIL ;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  FSAsyncRead()
* PARAMETER    :  OSAL_tIODescriptor, Bytes To Read
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Async read
* HISTORY      :  Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
******************************************************************************/
tU32 FSAsyncRead( OSAL_tIODescriptor hFile, tU32 u32BytesToRead )
{
   OSAL_trAsyncControl rAsyncCtrl = {0};
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tS32 s32BytesRead = 0;
   tChar szSemName []   = "SemAsyncFS";
   tS32 s32RetVal = 0;
   tU32 u32Ret = 0;
   /*Allocate memory*/
   ps8ReadBuffer =  ( tS8 * )OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

   /*Check if memory allocated successfully*/
   if ( ps8ReadBuffer == OSAL_NULL )
   {
      u32Ret = 2;
   }
   else
   {
      /*
         Create semaphore for event signalling used by async callback.
      */
      s32RetVal = OSAL_s32SemaphoreCreate ( szSemName, &hSemAsyncFS, 0 );

      /*Check if semaphore is already existent*/
      if ( s32RetVal == OSAL_ERROR )
      {
         u32Ret += 5;
      }
      else
      {
         /*Fill the asynchronous control structure*/
         rAsyncCtrl.id = hFile;
         rAsyncCtrl.s32Offset = 0;
         rAsyncCtrl.pvBuffer = ps8ReadBuffer;
         rAsyncCtrl.u32Length = u32BytesToRead;
         rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vOEDTComFSAsyncCallback;
         rAsyncCtrl.pvArg = &rAsyncCtrl;

         /*
            If IOReadAsync returns OSAL_ERROR, Callback function is
            not called so semaphore is not waited upon.
         */
         if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            /*
               Wait for 35 seconds for asynchronous read to get
               over. If semaphore not posted by the callback within
               35 seconds,then cancel the asynchronous read.
            */
            /*Here time out is 35 sec since if there is a single bit error then
            execution will be slower hence more time required*/
            s32RetVal = OSAL_s32SemaphoreWait( hSemAsyncFS, CFS_OEDT_SEM_TIMEOUT );

            if ( ( s32RetVal != OSAL_OK ) &&
                  ( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT ) )
            {
               s32RetVal = OSAL_s32IOCancelAsync ( rAsyncCtrl.id, &rAsyncCtrl );
               u32Ret +=  30;
            }
            else
            {
               s32RetVal = ( tS32 )OSAL_u32IOErrorAsync ( &rAsyncCtrl );

               if ( !( s32RetVal == ( tS32 )OSAL_E_INPROGRESS ||
                       s32RetVal == ( tS32 ) OSAL_E_CANCELED ) )
               {
                  s32BytesRead = ( tS32 ) OSAL_s32IOReturnAsync( &rAsyncCtrl );

                  if ( s32BytesRead != ( tS32 )u32BytesToRead )
                  {
                     u32Ret += 50;
                  }
               }
            }
         }
      }
   }

   if ( OSAL_ERROR == OSAL_s32SemaphoreClose ( hSemAsyncFS ) )
   {
      u32Ret += 80;
   }

   if ( OSAL_ERROR == OSAL_s32SemaphoreDelete( szSemName ) )
   {
      u32Ret += 100;
   }

   /* Free the memory allocated */
   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree ( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSReadAsync()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_042
* DESCRIPTION  :  Read data asyncronously from a file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSReadAsync( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tU32 u32BytesToRead = 10;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret ;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString )ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      u32Ret += FSAsyncRead( hFile, u32BytesToRead );

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 200;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret = u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      u32Ret += 9000;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSLargeReadAsync()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_043
* DESCRIPTION  :  Read data asyncronously from a large file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSLargeReadAsync( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tU32 u32BytesToRead = 1000;
   tU32 u32LoopCount = 0;
   tCS8 sWriteData[ ] = "Datatowrite";
   tU32 u32WriteDataLen = OSAL_u32StringLength( ( tCString )sWriteData );

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret ;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /* Writing data into the Largefile.txt*/
      for ( u32LoopCount = 0; u32LoopCount < u32BytesToRead; u32LoopCount++ )
      {
         if ( u32WriteDataLen != ( tU32 )OSAL_s32IOWrite( hFile, sWriteData, u32WriteDataLen ) )
         {
            u32Ret += 21;
            break;
         }
      }

      /*End of write data*/

      if ( FSAsyncRead( hFile, u32BytesToRead ) )
      {
         u32Ret += 2;
      }

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  FSAsyncWrite()
* PARAMETER    :  OSAL_tIODescriptor, Bytes To Write
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Async write
* HISTORY      :  Created by Shilpa Bhat (RBIN/EDI3) on 22 Oct, 2008
******************************************************************************/
tU32 FSAsyncWrite( OSAL_tIODescriptor hFile, tU32 u32BytesToWrite )
{
   OSAL_trAsyncControl rAsyncCtrl   = {0};
   tS32 s32BytesWritten = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tChar szSemName [] = "SemAsyncFS";
   tS32 s32RetVal = 0;
   tU32 u32Ret = 0;
   /*Allocate memory*/
   ps8WriteBuffer =  ( tS8 * )OSAL_pvMemoryAllocate ( u32BytesToWrite + 1 );

   /*Check if memory allocated successfully*/
   if ( ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 1;
   }
   else
   {
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', ( u32BytesToWrite + 1 ) );
      OSAL_pvMemorySet( ps8WriteBuffer, 'a', u32BytesToWrite );
      /*
         Create semaphore for event signalling used by async callback.
      */
      s32RetVal = OSAL_s32SemaphoreCreate ( szSemName, &hSemAsyncFS, 0 );

      /*Check if semaphore already existing in the system*/
      if ( s32RetVal == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         /*Fill the asynchronous control structure*/
         rAsyncCtrl.s32Offset = 0;
         rAsyncCtrl.id = hFile;
         rAsyncCtrl.pvBuffer = ps8WriteBuffer;
         rAsyncCtrl.u32Length = u32BytesToWrite;
         rAsyncCtrl.pCallBack = ( OSAL_tpfCallback ) vOEDTComFSAsyncWrCallback;
         rAsyncCtrl.pvArg = &rAsyncCtrl;

         /*
            If IOWriteAsync returns OSAL_ERROR, Callback function
            is not called so semaphore is not waited upon.
         */
         if ( OSAL_s32IOWriteAsync ( &rAsyncCtrl ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
         else
         {
            /*
               Wait for 35 seconds for asynchronous write to get over.
               If semaphore not posted by the callback within 35 seconds,
               then cancel the asynchronous write.
            */
            /*Here time out is 35 sec since if there is a single bit error then
            execution will be slower hence more time required*/
            s32RetVal = OSAL_s32SemaphoreWait ( hSemAsyncFS, CFS_OEDT_SEM_TIMEOUT );

            if ( ( s32RetVal != OSAL_OK ) &&
                  ( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT ) )
            {
               s32RetVal = OSAL_s32IOCancelAsync( rAsyncCtrl.id, &rAsyncCtrl );
               u32Ret += 10;
            }
            else
            {
               /* check for the async write status*/
               s32RetVal = ( tS32 )OSAL_u32IOErrorAsync ( &rAsyncCtrl );

               if ( !( s32RetVal == ( tS32 )OSAL_E_INPROGRESS ||
                       s32RetVal == ( tS32 )OSAL_E_CANCELED ) )
               {
                  s32BytesWritten = ( tS32 )OSAL_s32IOReturnAsync ( &rAsyncCtrl );

                  if (   s32BytesWritten != ( tS32 )u32BytesToWrite )
                  {
                     u32Ret += 20;
                  }
               }
            }
         }
      }
   }

   /* free the osal resources*/
   if ( OSAL_ERROR == OSAL_s32SemaphoreClose ( hSemAsyncFS ) )
   {
      u32Ret += 40;
   }

   if ( OSAL_ERROR == OSAL_s32SemaphoreDelete( szSemName ) )
   {
      u32Ret += 100;
   }

   /* Free the memory allocated */
   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree ( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSWriteAsync()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_044
* DESCRIPTION  :  Write data asyncronously to a file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteAsync( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   tU32 u32Ret = 0;
   tU32 u32BytesToWrite = 10;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 500;
   }
   else
   {
      /* write data to file asynchronously */
      u32Ret += FSAsyncWrite( hFile, u32BytesToWrite );

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 600;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSLargeWriteAsync()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_045
* DESCRIPTION  :  Write data asyncronously to a large file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 22 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSLargeWriteAsync( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString )ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 500;
   }
   else
   {
      /* write data to file asynchronously */
      u32Ret += FSAsyncWrite( hFile, OEDT_CFST_MB_SIZE );

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 600;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  FSWritetoOddBuf()
* PARAMETER    :  OSAL_tIODescriptor
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Write data to an odd buffer
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 FSWritetoOddBuf( OSAL_tIODescriptor hFd )
{
   tU32 u32Ret = 0;
   tString pBuffer = OSAL_NULL;
   tString pBuffer2 = OSAL_NULL;
   size_t sizeBuf = 16;
   /* Memory Allocation */
   pBuffer = ( tString )OSAL_pvMemoryAllocate( sizeBuf );

   if ( OSAL_NULL != pBuffer )
   {
      /*clear the buffer*/
      OSAL_pvMemorySet( pBuffer, '\0', ( sizeBuf ) );
      /*set the buffer*/
      OSAL_pvMemorySet( pBuffer, 'a', ( sizeBuf - 1 ) );

      /* Write to file */
      if ( OSAL_s32IOWrite( hFd, ( tPCS8 )pBuffer, ( sizeBuf - 1 ) ) != ( tS32 )( sizeBuf - 1 ) )
      {
         u32Ret = 2;
      }
      else
      {
         if
         (
            OSAL_s32IOControl( hFd, OSAL_C_S32_IOCTRL_FIOSEEK, ( tS32 )0 ) ==
            OSAL_ERROR )
         {
            u32Ret = 3;
         }
         else
         {
            /* Memory Allocation */
            pBuffer2 = ( tString )OSAL_pvMemoryAllocate( sizeBuf - 1 );

            if ( OSAL_NULL != pBuffer2 )
            {
               OSAL_pvMemorySet( pBuffer2, '\0', ( sizeBuf - 1 ) );

               /* Read specified number of bytes */
               if ( OSAL_s32IORead( hFd, ( tPS8 )pBuffer2, sizeBuf - 1 ) !=
                     ( tS32 )( sizeBuf - 1 ) )
               {
                  u32Ret = 4;
               }
               else
               {
                  if ( OSAL_s32StringNCompare( ( tCString )pBuffer,
                                               ( tCString ) pBuffer2, ( sizeBuf - 1 ) ) != 0 )
                  {
                     u32Ret = 5;
                  }
               }
            }
            else
            {
               u32Ret += 20;
            }

            /* Free memory */
            if ( pBuffer2 != OSAL_NULL )
            {
               OSAL_vMemoryFree( ( tString )pBuffer2 );
               pBuffer2 = OSAL_NULL;
            }
         }
      }
   }
   else
   {
      u32Ret += 50;
   }

   /* Free memory */
   if ( pBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ( tString )pBuffer );
      pBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSWriteOddBuffer()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_046
* DESCRIPTION  :  Write data to an odd buffer
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteOddBuffer( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*Write data to an odd buffer*/
      if ( FSWritetoOddBuf( hFile ) )
      {
         u32Ret += 2;
      }

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  FSWriteToBuffer()
* PARAMETER    :  OSAL_tIODescriptor
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Write data to a buffer
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/
tU32 FSWriteToBuffer( OSAL_tIODescriptor hFile )
{
   tU32  u32Ret = 0;
   tS32 s32RetVal = 0;
   tU32  iPos = 0;
   tCS8 DatatoWrite[]   = "abcdefg";
   s32RetVal = OSAL_s32IOWrite( hFile, DatatoWrite, ( ( tU32 )( -1 ) ) );

   if ( s32RetVal != ( tS32 )OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FIOWHERE, ( tS32 )&iPos );
     //PQM_authorized_multi_588: Disable the lint info 774:Boolean within 'if' always evaluates to False 
     //lint -e774
      if ( iPos != 0 )
      {
         u32Ret = 2;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSWriteFileWithInvalidSize()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_047
* DESCRIPTION  :  Write data to a file with invalid size
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteFileWithInvalidSize ( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 10;
   }
   else
   {
      u32Ret += FSWriteToBuffer( hFile );

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 40;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  FSWriteToInvalBuffer()
* PARAMETER    :  OSAL_tIODescriptor
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Write data to a invalid buffer
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/
tU32 FSWriteToInvalBuffer( OSAL_tIODescriptor hFile )
{
   tU32 u32Ret = 0;
   tS32 s32RetVal = 0;
   tU32 u32BytesToWrite = 3;
   tS8* pBuffer = OSAL_NULL;
   s32RetVal = OSAL_s32IOWrite( hFile, pBuffer, u32BytesToWrite );

   if ( s32RetVal != OSAL_ERROR )
   {
      u32Ret = 1;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSWriteFileInvalidBuffer()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_048
* DESCRIPTION  :  Write data to a file with invalid buffer
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteFileInvalidBuffer ( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, OSAL_EN_READWRITE );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 10;
   }
   else
   {
      u32Ret += FSWriteToInvalBuffer( hFile );

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 40;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSWriteFileStepByStep()
* PARAMETER    :  ps8file_name: file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_049
* DESCRIPTION  :  Write data to a file stepwise
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteFileStepByStep( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   tU32 u32Ret = 0;
   tS32 iCnt   = 0;
   tU32 iStepWidth = OEDT_CFST_256KB_SIZE; /* 256 kB */
   tPS8 pBuffer = ( tPS8 ) OSAL_NULL;
   tU32 iFileIdx = 0;
   tU32 iFullSize = OEDT_CFST_MB_SIZE; /* 1MB */

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /* Allocate memory */
   pBuffer = ( tPS8 )OSAL_pvMemoryAllocate( iStepWidth );

   if ( pBuffer == OSAL_NULL )
   {
      u32Ret = 8001;
      return  u32Ret;
   }
   else
   {
      OSAL_pvMemorySet( pBuffer, '\0',  iStepWidth );
      OSAL_pvMemorySet( pBuffer, 'a', iStepWidth );
      iFileIdx = 0;

      while ( ( iFileIdx < iFullSize ) && ( u32Ret == 0 ) )
      {
         /* Open file */
         hFile = OSAL_IOOpen( ( tCString )ps8file_name, OSAL_EN_READWRITE );

         if ( hFile == OSAL_ERROR )
         {
            u32Ret = 1;
         }
         else
         {
            if
            (
               OSAL_s32IOControl
               (
                  hFile,
                  OSAL_C_S32_IOCTRL_FIOSEEK,
                  ( tS32 )iFileIdx
               )
               ==
               OSAL_ERROR
            )
            {
               u32Ret += 2;

               /* Close file */
               if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
               {
                  u32Ret += 4;
               }

               break;
            }
            else
            {
               /* Write to file */
               iCnt = OSAL_s32IOWrite( hFile, pBuffer, iStepWidth );

               if ( iCnt != ( tS32 )iStepWidth )
               {
                  u32Ret += 10;

                  /* Close file */
                  if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
                  {
                     u32Ret += 20;
                  }

                  break;
               }
               else
               {
                  /* Close file */
                  if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
                  {
                     u32Ret += 40;
                  }
               }
            }
         }

         iFileIdx += iStepWidth;
      }
   }

   /* Free Memory */
   OSAL_vMemoryFree( pBuffer );
   pBuffer = OSAL_NULL;

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSGetFreeSpace()
* PARAMETER    :  ps8file_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_050
* DESCRIPTION  :  Get free space of device
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetFreeSpace ( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hFile   = 0;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   tU32 u32Ret = 0;
   tS32 s32Result = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Open device */
   hFile = OSAL_IOOpen( ( tCString )ps8file_name, OSAL_EN_READWRITE );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      rSize.u32High = 0;
      rSize.u32Low  = 0;
      /* Get free space */
      s32Result = OSAL_s32IOControl
                  (
                     hFile,
                     OSAL_C_S32_IOCTRL_FIOFREESIZE,
                     ( tS32 ) & rSize
                  );

      if
      (
         ( ( rSize.u32Low == 0 ) && ( rSize.u32High == 0 ) )
         ||
         ( s32Result == OSAL_ERROR )
      )
      {
         u32Ret += 2;
      }

      /* Close device */
      if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSGetTotalSpace()
* PARAMETER    :  ps8file_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_051
* DESCRIPTION  :  Get total space of device
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSGetTotalSpace ( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hFile   = 0;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   tU32 u32Ret = 0;
   tS32 s32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOOpen( ( tCString )ps8file_name, OSAL_EN_READWRITE );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      rSize.u32High = 0;
      rSize.u32Low  = 0;
      /* Get total space */
      s32Ret = OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FIOTOTALSIZE,
                                  ( tS32 ) & rSize );

      if ( ( ( rSize.u32Low == 0 ) && ( rSize.u32High == 0 ) )
            || ( s32Ret == OSAL_ERROR ) )
      {
         u32Ret += 2;
      }

      /* Close device */
      if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileOpenCloseNonExstng()
* PARAMETER    :  ps8file_name: Invalid file name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_052
* DESCRIPTION  :  Open a non existing file
* HISTORY      :  Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileOpenCloseNonExstng( const tS8* ps8file_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, enAccess );

   /*Check if open successful*/
   if ( hFile != OSAL_ERROR )
   {
      /* If successful, indicate error and close file */
      u32Ret = 1;

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileDelWithoutClose()
* PARAMETER:
*              ps8file_name: file name
*
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_053
* DESCRIPTION:  Delete a file without closing
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileDelWithoutClose( const tS8* ps8file_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile   = 0;
   tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1]  = "Writing data";
   /* Number of bytes to write */
   tU8 u8BytesToWrite = 15;
   tS32 s32BytesWritten = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess );

   /*Check if open successful*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /* Write File */
      s32BytesWritten = OSAL_s32IOWrite
                        (
                           hFile,
                           DataToWrite,
                           ( tU32 ) u8BytesToWrite
                        );

      if ( s32BytesWritten == OSAL_ERROR )
      {
         u32Ret = 2;
      }

      /* Remove file without closing */
      if ( OSAL_s32IORemove ( ( tCString ) ps8file_name ) != OSAL_ERROR )
      {
         //u32Ret += 4;
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
             u32Ret += 4;
         }
      }
      else
      {
         /* If successful, indicate error and close file */
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            /* Remove the file after close*/
            if ( OSAL_s32IORemove ( ( tCString )ps8file_name ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
         }
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSSetFilePosDiffOff()
* PARAMETER:   ps8file_name: file name
*              bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_054
* DESCRIPTION:  Set file position to different offsets
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSSetFilePosDiffOff( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tS32 s32PosToSet = 0;

   if( bCreateRemove == 0)
   	enAccess = OSAL_EN_READONLY;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /* Open the common file */
   hFile = OSAL_IOOpen ( ( tCString )ps8file_name, enAccess );

   /* Check if open successful */
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /* With +ve Offset */
      s32PosToSet = 10;

      /* Seek to positive offset */
      if ( OSAL_s32IOControl ( hFile,
                               OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }

      /* Close file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString )ps8file_name, enAccess );

   /*Check if open is successful*/
   if ( hFile != OSAL_ERROR )
   {
      /* With -ve Offset from BOF */
      s32PosToSet = -7;

      /*Seek through the common file with neg offset from file beginning*/
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet ) != OSAL_ERROR )
      {
         u32Ret += 10;
      }

      /* Close file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 40;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, enAccess );

   /*Check if open is successful*/
   if ( hFile != OSAL_ERROR )
   {
      /* Set position at EOF */
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD, ( tS32 )&s32PosToSet ) != OSAL_ERROR )
      {
         /*Seek within the common file*/
         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet ) == OSAL_ERROR )
         {
            u32Ret += 100;
         }
         else
         {
            /* Try Setting position beyond EOF */
            s32PosToSet = 10;

            if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet ) == OSAL_ERROR )
            {
               u32Ret += 200;
            }
         }
      }

      /* Close file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 400;
      }
   }
   else
   {
      u32Ret += 1000;
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    u32FSCreateFileMultiTimes()
* PARAMETER:   ps8file_name: file name
*              bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_055
* DESCRIPTION:  create file multiple times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateFileMultiTimes( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;
   OSAL_tIODescriptor hFile   = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   if ( ( ( hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
   {
      u32Ret = 10;
   }
   else
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret = 20;
      }
   }

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileCreateUnicodeName()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_056
* DESCRIPTION:  create file with unicode characters
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateUnicodeName( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   /* Creation of file with chinese characters */
   hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess );

   if ( OSAL_ERROR == hFile )
   {
      u32Ret = 1;
   }
   else
   {
      /* Close file */
      if ( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
      {
         u32Ret = 2;
      }
      /* Remove file */
      else if ( OSAL_ERROR == OSAL_s32IORemove ( ( tCString ) ps8file_name ) )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileCreateInvalName()
* PARAMETER:   ps8file_name: invalid file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_057
* DESCRIPTION:  create file with invalid characters
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateInvalName( const tS8* ps8file_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create file with invalid access name */
   hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess );

   if ( hFile != OSAL_ERROR )
   {
      /* If successful, indicate error and close file */
      u32Ret = 1;

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else if ( OSAL_s32IORemove ( ( tCString ) ps8file_name ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileCreateLongName()
* PARAMETER:   ps8file_name: Device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_058
* DESCRIPTION:  create file with long file name
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateLongName( const tS8* ps8file_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile   = OSAL_NULL;
   tU32 u32Ret = 0;
   tChar file_long_name[1024] = {0,};

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Initialize the allocated memory */
   OSAL_pvMemorySet( file_long_name, '\0', sizeof( file_long_name ) );
   /* Copy USB path */
   ( tVoid )OSAL_szStringCopy( ( tString )file_long_name, ( tString )ps8file_name );
   /* Add '/' character */
   ( tVoid )OSAL_szStringConcat( file_long_name, "/" );
   /* Call filename generator */
   Generate_File_Name_comfs ( file_long_name, OSAL_u32StringLength( ps8file_name ) + 1,
                              FILE_NAME_MAX_LEN );

   /* Create the file */
   if ( ( hFile = OSAL_IOCreate ( ( tCString )file_long_name, enAccess ) ) == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*Close the file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         /*Remove the file*/

         if ( OSAL_s32IORemove ( file_long_name ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
		 
      }
   }

   /* Initialize the allocated memory */
   OSAL_pvMemorySet( file_long_name, '\0', sizeof( file_long_name ) );
   /* Copy USB path */
   ( tVoid )OSAL_szStringCopy( file_long_name, ps8file_name );
   /* Add '/' character */
   ( tVoid )OSAL_szStringConcat( file_long_name, "/" );
   /* Call filename generator */
   Generate_File_Name_comfs
   (
      file_long_name,
      OSAL_u32StringLength( ps8file_name ) + 1,
      FILE_NAME_MAX_LEN_EXCEEDED
   );

   /* Create the file */
   if ( ( hFile = OSAL_IOCreate ( ( tCString )file_long_name, enAccess ) ) != OSAL_ERROR )
   {
      u32Ret += 10;

      /* Close the file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
      else
      {
         /* Remove the file */
         if ( OSAL_s32IORemove ( file_long_name ) == OSAL_ERROR )
         {
            u32Ret += 40;
         }
      }
   }

   /* Initialize the allocated memory */
   OSAL_pvMemorySet( file_long_name, '\0', sizeof( file_long_name ) );
   /* Copy USB path */
   ( tVoid )OSAL_szStringCopy( file_long_name, ps8file_name );
   /* Add '/' character */
   ( tVoid )OSAL_szStringConcat( file_long_name, "/" );
   /* Call filename generator */
   Generate_File_Name_comfs
   (
      file_long_name,
      OSAL_u32StringLength( ps8file_name ) + 1,
      FILE_NAME_MAX_LEN_EXCEEDED_1
   );

   /* Create the file */
   if ( ( hFile = OSAL_IOCreate ( ( tCString )file_long_name, enAccess ) ) != OSAL_ERROR )
   {
      u32Ret += 100;

      /* If successful, indicate error and Close the file */
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 200;
      }
      else
      {
         /* Remove the file  */
         if ( OSAL_s32IORemove ( file_long_name ) == OSAL_ERROR )
         {
            u32Ret += 400;
         }
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileCreateDiffModes()
* PARAMETER:   ps8file_name: Device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_059
* DESCRIPTION:  Create file with different access modes
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateDiffModes( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hFile   = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( ( ( hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
   {
      u32Ret = 1;
   }
   else
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      /*Remove the common File*/
      else if ( OSAL_s32IORemove( ( tCString )ps8file_name ) == OSAL_ERROR )
      {
         u32Ret = 3;
      }
   }

   enAccess = OSAL_EN_WRITEONLY;

   if ( ( ( hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
   {
      u32Ret += 10;
   }
   else
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 14;
      }
      /*Remove the common File*/
      else if ( OSAL_s32IORemove( ( tCString )ps8file_name ) == OSAL_ERROR )
      {
         u32Ret += 19 ;
      }
   }

   enAccess = OSAL_EN_READWRITE;

   if ( ( ( hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
   {
      u32Ret += 20;
   }
   else
   {
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret = 24;
      }
      /*Remove the common File*/
      else if ( OSAL_s32IORemove( ( tCString )ps8file_name ) == OSAL_ERROR )
      {
         u32Ret += 29;
      }
   }

   return u32Ret;
}



/*****************************************************************************
* FUNCTION:    u32FSFileCreateInvalPath()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_060
* DESCRIPTION:  Create file with invalid path
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileCreateInvalPath( const tS8* ps8file_name )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess );

   if ( hFile != OSAL_ERROR )
   {
      u32Ret = 1;

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         if ( OSAL_s32IORemove ( ( tCString )ps8file_name ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    FSStrSearch()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  String search
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSStrSearch( const tU8 * strDevName )
{
   /*Declarations*/
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 StrngCnt  = 0;
   tU32 u32Ret  = 0;
   tChar szSearchString [5] = "dat";
   tU32 u32StringLen = 3;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   /* this must be equal to length of the search string */
   tU32 u32BytesToRead = 3;
   tS32 s32BytesRead = 0;
   tU32 u32PosToSet = 0;
   tU32 u32FilePos = 0;
   tU32 s32FileSz  = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Open the common file*/
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   /*Check status of open*/
   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*End of file*/
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                               ( tS32 )&s32FileSz ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      //PQM_authorized_multi_589: Disable the lint info 681:Boolean within 'if' always evaluates to False 
      //lint -e681
      while ( ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,
                                    ( tS32 )&u32FilePos ) != OSAL_ERROR )  && ( s32FileSz > u32PosToSet ) )
         /*Check if current file-position is within file boundaries*/
      {
         /*Seek through the common file*/
         if ( OSAL_s32IOControl (
                  hFile,
                  OSAL_C_S32_IOCTRL_FIOSEEK,
                  ( tS32 )u32PosToSet
               ) == OSAL_ERROR )
         {
            u32Ret += 4;
            break;
         }
         else
         {
            /*Allocate memory dynamically*/
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

            /*Check if memory allocation successful*/
            if ( ps8ReadBuffer == OSAL_NULL )
            {
               u32Ret += 10;
               break;
            }
            else
            {
               /*Read from common file*/
               s32BytesRead = OSAL_s32IORead (
                                 hFile,
                                 ps8ReadBuffer,
                                 u32BytesToRead
                              );

               /*Check status of read*/
               if ( s32BytesRead == OSAL_ERROR )
               {
			   	  /*Deallocate memory*/
				  OSAL_vMemoryFree( ps8ReadBuffer );
				  ps8ReadBuffer = OSAL_NULL;				  
                  u32Ret += 20;
                  break;
               }
               else
               {
                  /*Do a string comparison*/
                  if ( OSAL_s32StringNCompare ( ps8ReadBuffer, szSearchString, u32StringLen ) == 0 )
                  {
                     /*Increment occurences*/
                     StrngCnt++;
                  }
               }
            }
			
			/*Deallocate memory*/
			OSAL_vMemoryFree( ps8ReadBuffer );
			ps8ReadBuffer = OSAL_NULL;
         }

         u32PosToSet += 3;
		 
      }

      /*Close the common file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 40;
      }
   }

   /*Check for error code*/
   if ( u32Ret == 0 )
   {
      if ( StrngCnt == 0 )
      {
         OEDT_HelperPrintf( TR_LEVEL_USER_1,
                            "\n\t String not found" );
      }
      else
      {
         OEDT_HelperPrintf( TR_LEVEL_USER_1,
                            "\n\t String found %u times", StrngCnt );
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSStringSearch()
* PARAMETER:   ps8file_name: file name
*              bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_061
* DESCRIPTION:  Search for string
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSStringSearch( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   u32Ret += FSStrSearch( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    u32FSFileOpenDiffModes()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_062
* DESCRIPTION:  Create file with different access modes
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileOpenDiffModes( const tS8* ps8file_name, tBool bCreateRemove )
{
   OSAL_tIODescriptor hFile   = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret  = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( bCreateRemove )
   {
      if ( ( ( hFile1 = OSAL_IOCreate ( ( tCString )ps8file_name, enAccess ) ) == OSAL_ERROR ) )
      {
         u32Ret = 1;
      }
   }

   if ( u32Ret == 0 )
   {
      enAccess = OSAL_EN_READONLY;
      /*Open the file*/
      hFile = OSAL_IOOpen ( ( tCString )ps8file_name, enAccess );

      /*Check if open successful*/
      if ( hFile == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else
      {
         /*Close the common file*/
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
      }

      if ( bCreateRemove )
      {
         enAccess = OSAL_EN_WRITEONLY;
         /*Open the file*/
         hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, enAccess );

         /*Check if open successful*/
         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            /*Close the common file*/
            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
         }

         enAccess = OSAL_EN_READWRITE;
         /*Open the file*/
         hFile = OSAL_IOOpen ( ( tCString ) ps8file_name, enAccess );

         /*Check if open successful*/
         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 40;
         }
         else
         {
            /*Close the common file*/
            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               u32Ret += 100;
            }
         }

         /*Close the common file*/
         if ( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }

         /*Remove the common File*/
         if ( OSAL_s32IORemove( ( tCString )ps8file_name ) == OSAL_ERROR )
         {
            u32Ret += 400;
         }
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSFileReadAccessChk()
* PARAMETER:   file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  File Read
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSFileReadAccessChk( const tU8 * strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
   tU32 u32Ret = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 10;
   tS32 s32BytesRead = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* open the file in write access mode*/
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*allocate the memory for read buffer*/
      ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

      if ( ps8ReadBuffer == OSAL_NULL )
      {
         u32Ret = 2;
      }
      else
      {
         /*Try to read the file in write access mode*/
         s32BytesRead = OSAL_s32IORead
                        (
                           hFile,
                           ps8ReadBuffer,
                           u32BytesToRead
                        );

         if ( s32BytesRead != OSAL_ERROR )
         {
            u32Ret += 4;
         }
      }

      /*Close the file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }

      /*deallocate the memory for read buffer*/
      if ( ps8ReadBuffer != OSAL_NULL )
      {
         OSAL_vMemoryFree( ps8ReadBuffer );
         ps8ReadBuffer = OSAL_NULL;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileReadAccessCheck()
* PARAMETER:   ps8file_name: file name
*              bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_063
* DESCRIPTION:  Try to read from the file which has no access rights
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadAccessCheck ( const tS8* ps8file_name,
                                tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Read access check function to check the access mode*/
   u32Ret += FSFileReadAccessChk( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION:    FSFileWriteAccessChk()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  File Write
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSFileWriteAccessChk( const tU8 * strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Writing data";
   tU32 u32Ret = 0;
   /* Number of bytes to write */
   tU32 u8BytesToWrite = 15;
   tS32 s32BytesWritten = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*open the file in read mode*/
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*try to write in file, which open in read mode*/
      s32BytesWritten = OSAL_s32IOWrite
                        (
                           hFile,
                           DataToWrite,
                           ( tU32 ) u8BytesToWrite
                        );

      if ( s32BytesWritten != OSAL_ERROR )
      {
         u32Ret = 2;
      }

      /*close the file*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    u32FSFileWriteAccessCheck()
* PARAMETER:   ps8file_name: file name
*              bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_064
* DESCRIPTION:  Try to write from the file which has no access rights
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 28 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileWriteAccessCheck( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret += u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   /*Try to write from the file which has no access rights*/
   u32Ret += FSFileWriteAccessChk( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileReadSubDir()
* PARAMETER:   ps8file_name: Device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_065
* DESCRIPTION:  Read file present in sub-directory
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileReadSubDir( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor subDir = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret  = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 10;
   tS32 s32BytesRead  = 0;
   tU8 u8BytesToWrite = 20; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;
   tChar szSubDirPath [OEDT_FS_FILE_PATH_LEN]   = {0};
   tChar szSubdirName [OEDT_FS_FILE_PATH_LEN]   = {0};
   tChar szDirPath [OEDT_FS_FILE_PATH_LEN] = {0};
   tChar szFileName [OEDT_FS_FILE_PATH_LEN] = {0};
   tCS8 DataToWrite[30] = "Writing data to Sub-dir";

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Subdri name*/
   ( tVoid )OSAL_szStringCopy( szSubdirName, "/NEWSUB53" );
   /*Subdri path name*/
   ( tVoid )OSAL_szStringCopy( szSubDirPath, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szSubDirPath, "/NEWSUB53" );
   /*path name for FS*/
   ( tVoid )OSAL_szStringCopy( szDirPath, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szDirPath, "/" );
   /*path name for subdir File*/
   ( tVoid )OSAL_szStringCopy( szFileName, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szFileName, "/NEWSUB53/Myfile.txt" );
   /*Initialize the Dirent structure*/
   ( tVoid )OSAL_pvMemorySet( rDirent.s8Name, '\0', OSAL_C_U32_MAX_PATHLENGTH );
   hDir = OSAL_IOOpen ( ( tCString )szDirPath, enAccess );/* szSubDirPath */

   if ( hDir == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         subDir = OSAL_IOOpen ( ( tCString )szSubDirPath, enAccess );

         if ( subDir != OSAL_ERROR )
         {
            if ( ( hFile = OSAL_IOCreate ( ( tCString )szFileName, enAccess ) ) != OSAL_ERROR )
            {
               if ( hFile != OSAL_ERROR )
               {
                  s32BytesWritten = OSAL_s32IOWrite (
                                       hFile, DataToWrite,
                                       ( tU32 ) u8BytesToWrite
                                    );

                  if ( s32BytesWritten == OSAL_ERROR )
                  {
                     u32Ret += 4;
                  }

                  OSAL_s32IOClose ( hFile );
               }

               hFile1 = OSAL_IOOpen ( ( tCString ) szFileName, enAccess );

               if ( hFile1 != OSAL_ERROR )
               {
                  ps8ReadBuffer = ( tS8 * )
                                  OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );
                  s32BytesRead = OSAL_s32IORead (
                                    hFile1,
                                    ps8ReadBuffer,
                                    u32BytesToRead
                                 );

                  if ( ( tU32 )s32BytesRead != u32BytesToRead )
                  {
                     u32Ret += 10;
                  }

                  if ( ( OSAL_s32IOClose ( hFile1 ) ) != OSAL_ERROR )
                  {
                     if ( OSAL_s32IORemove ( szFileName ) == OSAL_ERROR )
                     {
                        u32Ret += 20;
                     }
                  }

                  if ( ps8ReadBuffer != OSAL_NULL )
                  {
                     OSAL_vMemoryFree( ps8ReadBuffer );
                     ps8ReadBuffer = OSAL_NULL;
                  }
               }
            }
            else
            {
               u32Ret += 40;
            }

            if ( ( OSAL_s32IOClose ( subDir ) ) != OSAL_ERROR )
            {
               if ( OSAL_s32IOControl ( hDir,
                                        OSAL_C_S32_IOCTRL_FIORMDIR,
                                        ( tS32 )&rDirent ) == OSAL_ERROR )
               {
                  u32Ret += 100;
               }
            }
         }

         if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileWriteSubDir()
* PARAMETER:   ps8file_name: Device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_066
* DESCRIPTION:  Write to file present in sub-directory
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileWriteSubDir( const tS8* ps8file_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor subDir1 = 0;
   OSAL_tIODescriptor subDir2 = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_trIOCtrlDirent rDirent1 = {""};
   OSAL_trIOCtrlDirent rDirent2 = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   /* Number of bytes to write */
   tU8 u8BytesToWrite = 20;
   tS32 s32BytesWritten = 0;
   tChar szDirPath [OEDT_FS_FILE_PATH_LEN] = {'\0'};
   tChar szSubDirPath1 [OEDT_FS_FILE_PATH_LEN]  = {'\0'};
   tChar szSubDirPath2 [OEDT_FS_FILE_PATH_LEN]  = {'\0'};
   tChar szSubdirName1 [OEDT_FS_FILE_PATH_LEN]  = {'\0'};
   tChar szSubdirName2 [OEDT_FS_FILE_PATH_LEN]  = {'\0'};
   tChar szSubDirFileName [OEDT_FS_FILE_PATH_LEN] = {'\0'};
   tCS8 DataToWrite[30] = "Writing data to Sub-dir";

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Subdri1 name*/
   ( tVoid )OSAL_szStringCopy( szSubdirName1, "/SUB59" );
   /*Subdri1 name*/
   ( tVoid )OSAL_szStringCopy( szSubdirName2, "/SUBTWO" );
   /*Subdri1 path name*/
   ( tVoid )OSAL_szStringCopy( szSubDirPath1, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szSubDirPath1, "/SUB59" );
   /*Subdri2 path name*/
   ( tVoid )OSAL_szStringCopy( szSubDirPath2, szSubDirPath1 );
   ( tVoid )OSAL_szStringConcat( szSubDirPath2, "/SUBTWO" );
   /*path name for FS*/
   ( tVoid )OSAL_szStringCopy( szDirPath, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szDirPath, "/" );
   /*path name for subdir File*/
   ( tVoid )OSAL_szStringCopy( szSubDirFileName, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szSubDirFileName, "/SUB59/SUBTWO/Myfile.txt" );
   hDir = OSAL_IOOpen ( ( tCString )szDirPath, enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent1.s8Name, szSubdirName1 );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent1 ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         subDir1 = OSAL_IOOpen ( ( tCString )szSubDirPath1, enAccess );

         if ( subDir1 != OSAL_ERROR )
         {
            ( tVoid )OSAL_szStringCopy ( rDirent2.s8Name, szSubdirName2 );

            if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                     ( tS32 )&rDirent2 ) == OSAL_ERROR )
            {
               u32Ret += 4;
            }
            else
            {
               subDir2 = OSAL_IOOpen ( ( tCString )szSubDirPath2, enAccess );

               if ( subDir2 != OSAL_ERROR )
               {
                  if ( ( hFile = OSAL_IOCreate ( ( tCString )szSubDirFileName, enAccess ) ) != OSAL_ERROR )
                  {
                     s32BytesWritten = OSAL_s32IOWrite (
                                          hFile, DataToWrite,
                                          ( tU32 ) u8BytesToWrite
                                       );

                     if ( s32BytesWritten == OSAL_ERROR )
                     {
                        u32Ret += 10;
                     }

                     OSAL_s32IOClose ( hFile );
                     OSAL_s32IORemove ( szSubDirFileName );
                  }
                  else
                  {
                     u32Ret += 20;
                  }
               }

               OSAL_s32IOClose ( subDir2 );

               if ( OSAL_s32IOControl ( hDir,
                                        OSAL_C_S32_IOCTRL_FIORMDIR,
                                        ( tS32 )&rDirent2 ) == OSAL_ERROR )
               {
                  u32Ret += 40;
               }
            }
         }

         OSAL_s32IOClose ( subDir1 );

         if ( OSAL_s32IOControl ( hDir,
                                  OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent1 ) == OSAL_ERROR )
         {
            u32Ret += 100;
         }
      }

      OSAL_s32IOClose ( hDir );
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSReadWriteTimeMeasure1()
* PARAMETER:    filename , number of bytes to be read/written
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB, 10KB, 1MB
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSReadWriteTimeMeasure1( const tU8* strDevName, tU32 u32ReadCount )
{
   OSAL_tMSecond StartTime = 0;
   OSAL_tMSecond TimetakenToWrite = 0;
   OSAL_tMSecond TimetakenToRead  = 0;
   OSAL_tMSecond EndTime = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   tS8 *ps8ReadBuffer = OSAL_NULL;
   /* Number of bytes to read */
   tU32 u32BytesToRead = u32ReadCount;
   tU32 u32BlockSize = u32ReadCount;
   tS32 s32BytesRead = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tS32 s32BytesWritten = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, strDevName );
   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if allocation successful*/
   if (   ps8WriteBuffer == OSAL_NULL )
      u32Ret = 1;
   else
   {
      /*Initialize data*/
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize );
      /*Fill the buffer with data*/
      OSAL_pvMemorySet( ps8WriteBuffer, 'F', u32BlockSize - 1 );

      if ( ( hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess ) ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         StartTime = OSAL_ClockGetElapsedTime();
         s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                             u32BlockSize );
         EndTime = OSAL_ClockGetElapsedTime();

         /*Wrapping?*/
         if ( EndTime > StartTime )
         {
            TimetakenToWrite =   EndTime - StartTime;
         }
         else
         {
            TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
         }

         if ( s32BytesWritten == OSAL_ERROR )
         {
            u32Ret = 4;
         }

         OSAL_s32IOClose ( hFile );
         hFile = OSAL_IOOpen ( ( tCString )szFilePath, enAccess );

         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

            /*Check if allocation successful*/
            if (   ps8ReadBuffer == OSAL_NULL )
            {
               OSAL_s32IOClose ( hFile );
               OSAL_s32IORemove ( szFilePath );
               OSAL_vMemoryFree( ps8WriteBuffer );
               return ( tU32 )OSAL_ERROR;
            }

            StartTime = OSAL_ClockGetElapsedTime();
            s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                            u32BytesToRead );
            EndTime = OSAL_ClockGetElapsedTime();

            /*Wrapping*/
            if ( EndTime > StartTime )
            {
               TimetakenToRead = EndTime - StartTime;
            }
            else
            {
               TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }

            vPrintOutTime( TimetakenToRead, TimetakenToWrite, ( tU64 )u32BytesToRead, ( ( tU64 )u32BlockSize*( tU64 )4 ) );

            if ( ( tU32 )s32BytesRead != u32BytesToRead )
            {
               u32Ret += 20;
            }
         }

         /*Close the file*/
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
      }
   }

   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof1KbFile()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_067
* DESCRIPTION:  Read Write time measure for 1KB file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof1KbFile( const tS8 * ps8file_name )
{
   tU32 u32Ret = 0;
   tU32 u32ReadCount = OEDT_CFST_1KB_SIZE;
   tBool bCreateRemove = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadWriteTimeMeasure1( ( tU8* )ps8file_name, u32ReadCount );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof10KbFile()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_068
* DESCRIPTION:  Read Write time measure for 10KB file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof10KbFile( const tS8* ps8file_name )
{
   tU32 u32Ret  = 0;
   tU32 u32ReadCount = OEDT_CFST_10KB_SIZE;
   tBool bCreateRemove  = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret = FSReadWriteTimeMeasure1( ( tU8* )ps8file_name, u32ReadCount );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof100KbFile()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_069
* DESCRIPTION:  Read Write time measure for 10KB file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof100KbFile( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   tU32 u32ReadCount = OEDT_CFST_100KB_SIZE;
   tBool bCreateRemove  = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret = FSReadWriteTimeMeasure1( ( tU8* )ps8file_name, u32ReadCount );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSReadWriteTimeMeasure2()
* PARAMETER:    filename , number of bytes to be read/written, blocksize
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB file 1000 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSReadWriteTimeMeasure2( const tU8* strDevName )
{
   OSAL_tMSecond StartTime = 0;
   OSAL_tMSecond TimetakenToWrite = 0;
   OSAL_tMSecond TimetakenToRead = 0;
   OSAL_tMSecond EndTime = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   tS8 *ps8ReadBuffer = OSAL_NULL;
   /* Number of bytes to read */
   tU32 u32BytesToRead = 1048576;/* Number of bytes to read */
   tU32 u32BlockSize = 256 * 1024;
   tS32 s32BytesRead = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 loop_cnt = 0;
   tS32 s32BytesWritten = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, strDevName );
   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if allocation successful*/
   if (   ps8WriteBuffer == OSAL_NULL )
      u32Ret = 1;
   else
   {
      /*Initialize data*/
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize );
      /*Fill the buffer with data*/
      OSAL_pvMemorySet( ps8WriteBuffer, 'F', u32BlockSize - 1 );

      if ( ( hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess ) ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         StartTime = OSAL_ClockGetElapsedTime();

         for ( loop_cnt = 0; loop_cnt <= 3; loop_cnt++ )
         {
            s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                                u32BlockSize );

            if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) || ( s32BytesWritten != ( tS32 )u32BlockSize ) )
            {
               u32Ret += 4;
               break;
            }
         }

         EndTime = OSAL_ClockGetElapsedTime();

         /*Wrapping?*/
         if ( EndTime > StartTime )
         {
            TimetakenToWrite =   EndTime - StartTime;
         }
         else
         {
            TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
         }

         OSAL_s32IOClose ( hFile );
         hFile = OSAL_IOOpen ( ( tCString )szFilePath, enAccess );

         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

            /*Check if allocation successful*/
            if (   ps8ReadBuffer == OSAL_NULL )
            {
               OSAL_vMemoryFree( ps8WriteBuffer );
               OSAL_s32IOClose ( hFile );
               OSAL_s32IORemove ( szFilePath );
               return ( tU32 )OSAL_ERROR;
            }

            StartTime = OSAL_ClockGetElapsedTime();
            s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                            u32BytesToRead );
            EndTime = OSAL_ClockGetElapsedTime();

            /*Wrapping*/
            if ( EndTime > StartTime )
            {
               TimetakenToRead = EndTime - StartTime;
            }
            else
            {
               TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }

            if ( ( tU32 )s32BytesRead != u32BytesToRead )
            {
               u32Ret += 20;
            }

            if ( u32Ret == 0 )
               vPrintOutTime( TimetakenToRead, TimetakenToWrite, ( tU64 )u32BytesToRead, ( ( tU64 )u32BlockSize*( tU64 )4 ) );

            OSAL_s32IOClose ( hFile );
         }
      }
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof1MBFile()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_070
* DESCRIPTION:  Read Write time measure for 100KB file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
******************************************************************************/
tU32 u32FSTimeMeasureof1MBFile( const tS8 * ps8file_name )
{
   tU32 u32Ret = 0;
   tBool bCreateRemove     = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadWriteTimeMeasure2( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSReadWriteTimeMeasure3()
* PARAMETER:    file name, read count, loop count
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1KB file 10000 times, 10KB 1000 times
*               100KB 100 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 FSReadWriteTimeMeasure3( const tU8* strDevName, tU32 u32ReadCount, tU32 u32Loop )
{
   OSAL_tMSecond StartTime         = 0;
   OSAL_tMSecond TimetakenToWrite      = 0;
   OSAL_tMSecond TimetakenToRead       = 0;
   OSAL_tMSecond EndTime         = 0;
   OSAL_tIODescriptor hFile         = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   tU32 u32Ret             = 0;
   tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1]  = {'\0'};
   tS8 *ps8ReadBuffer           = OSAL_NULL;
   tU32 u32BytesToRead           = u32ReadCount;/* Number of bytes to read */
   tU32 u32BlockSize           = u32ReadCount;
   tS32 s32BytesRead           = 0;
   tS8 *ps8WriteBuffer           = OSAL_NULL;
   tU32 loop_cnt             = 0;
   tS32 s32BytesWritten          = 0;
   OSAL_trIOCtrlDeviceSize rSize       = {0};

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, strDevName );

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if allocation successful*/
   if ( ps8WriteBuffer == OSAL_NULL )
      u32Ret = 1;
   else
   {
      /*Initialize data*/
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize );
      /*Fill the buffer with data*/
      OSAL_pvMemorySet( ps8WriteBuffer, 'F', u32BlockSize - 1 );

      if ( ( hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess ) ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         rSize.u32High = 0;
         rSize.u32Low  = 0;
         /* Get free space */
         u32Ret = ( tU32 )OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE, ( tS32 ) & rSize );

         if ( ( ( rSize.u32Low == 0 ) && ( rSize.u32High == 0 ) ) || ( u32Ret == ( tU32 )OSAL_ERROR )
               || ( rSize.u32Low < ( u32ReadCount*u32Loop ) ) )
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_1, "%s Insufficient Size \n", strDevName );
            OSAL_s32IOClose ( hFile );
         }
         else
         {
            StartTime = OSAL_ClockGetElapsedTime();

            for ( loop_cnt = 0; loop_cnt < u32Loop; loop_cnt ++ )
            {
               s32BytesWritten = OSAL_s32IOWrite( hFile, ps8WriteBuffer,
                                                  u32BlockSize );

               if ( s32BytesWritten != ( tS32 )u32BlockSize )
               {
                  u32Ret = 4;
                  break;
               }
            }

            EndTime = OSAL_ClockGetElapsedTime();

            /*Wrapping?*/
            if ( EndTime > StartTime )
            {
               TimetakenToWrite =  EndTime - StartTime;
            }
            else
            {
               TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }

            if ( s32BytesWritten == OSAL_ERROR )
            {
               u32Ret += 10;
            }

            OSAL_s32IOClose ( hFile );
            hFile = OSAL_IOOpen ( ( tCString )szFilePath, enAccess );

            if ( hFile == OSAL_ERROR )
            {
               u32Ret += 20;
            }
            else
            {
               ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32ReadCount + 1 );

               /*Check if allocation successful*/
               if ( ps8ReadBuffer == OSAL_NULL )
               {
                  OSAL_vMemoryFree( ps8WriteBuffer );
                  OSAL_s32IOClose ( hFile );
                  OSAL_s32IORemove ( szFilePath );
                  return ( tU32 )OSAL_ERROR;
               }

               StartTime = OSAL_ClockGetElapsedTime();

               for ( loop_cnt = 0; loop_cnt < u32Loop; loop_cnt ++ )
               {
                  s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer, u32BytesToRead );

                  if ( s32BytesRead != ( tS32 )u32BytesToRead )
                  {
                     u32Ret += 40;
                     break;
                  }
               }

               EndTime = OSAL_ClockGetElapsedTime();

               /*Wrapping*/
               if ( EndTime > StartTime )
               {
                  TimetakenToRead = EndTime - StartTime;
               }
               else
               {
                  TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
               }

               if ( ( tU32 )s32BytesRead != u32BytesToRead )
               {
                  u32Ret += 100;
               }

               if ( u32Ret == 0 )
                  vPrintOutTime( TimetakenToRead, TimetakenToWrite, ( ( tU64 )u32BytesToRead*( tU64 )u32Loop ), ( ( tU64 )u32BlockSize*( tU64 )u32Loop ) );

               OSAL_s32IOClose ( hFile );
            }
         }
      }
   }

   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof1KbFilefor1000times()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_071
* DESCRIPTION:  Read write time measure for 1KB file 1000 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof1KbFilefor1000times( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   tU32 u32ReadCount = OEDT_CFST_1KB_SIZE;
   tU32 u32Loop = OEDT_CFST_1000;
   tBool bCreateRemove  = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadWriteTimeMeasure3( ( tU8* )ps8file_name, u32ReadCount, u32Loop );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof10KbFilefor1000times()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_072
* DESCRIPTION:  Read write time measure for 10KB file 1000 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof10KbFilefor1000times( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   tU32 u32ReadCount = OEDT_CFST_10KB_SIZE;
   tU32 u32Loop = OEDT_CFST_1000;
   tBool bCreateRemove  = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadWriteTimeMeasure3( ( tU8* )ps8file_name, u32ReadCount, u32Loop );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof100KbFilefor100times()
* PARAMETER:   ps8file_name: file name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_073
* DESCRIPTION:  Read write time measure for 10KB file 1000 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof100KbFilefor100times( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   tU32 u32ReadCount = OEDT_CFST_100KB_SIZE;
   tU32 u32Loop = OEDT_CFST_100;
   tBool bCreateRemove  = TRUE;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadWriteTimeMeasure3( ( tU8* )ps8file_name, u32ReadCount, u32Loop );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    FSReadWriteTimeMeasure4()
* PARAMETER:    filename, number of bytes to read/write, blocksize
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read write time measure for 1MB for 16 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSReadWriteTimeMeasure4( const tU8* strDevName )
{
   OSAL_tMSecond StartTime = 0;
   OSAL_tMSecond TimetakenToWrite = 0;
   OSAL_tMSecond TimetakenToRead  = 0;
   OSAL_tMSecond EndTime = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = 1048576;/* Number of bytes to read */
   tU32 u32BlockSize = 256 * 1024;
   tS32 s32BytesRead = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 loop_cnt = 0;
   tU32 loop_count = 0;
   tS32 s32BytesWritten = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, strDevName );
   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if allocation successful*/
   if (   ps8WriteBuffer == OSAL_NULL )
      u32Ret = 1;
   else
   {
      /*Initialize data*/
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize );
      /*Fill the buffer with data*/
      OSAL_pvMemorySet( ps8WriteBuffer, 'F', u32BlockSize - 1 );

      if ( ( hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess ) ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         StartTime = OSAL_ClockGetElapsedTime();

         for ( loop_count = 0; loop_count < 16; loop_count ++ )
         {
            for ( loop_cnt = 0; loop_cnt <= 3; loop_cnt++ )
            {
               s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                                   u32BlockSize );

               if ( s32BytesWritten != ( tS32 )u32BlockSize )//(tS32)OSAL_ERROR )
               {
                  u32Ret += 4;
                  break;
               }
            }
         }

         EndTime = OSAL_ClockGetElapsedTime();

         /*Wrapping?*/
         if ( EndTime > StartTime )
         {
            TimetakenToWrite =   EndTime - StartTime;
         }
         else
         {
            TimetakenToWrite = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
         }

         OSAL_s32IOClose ( hFile );
         hFile = OSAL_IOOpen ( ( tCString )szFilePath, enAccess );

         if ( hFile == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( 1048576 + 1 );

            /*Check if allocation successful*/
            if (   ps8ReadBuffer == OSAL_NULL )
            {
               OSAL_vMemoryFree( ps8WriteBuffer );
               OSAL_s32IOClose ( hFile );
               OSAL_s32IORemove ( szFilePath );
               return ( tU32 )OSAL_ERROR;
            }

            StartTime = OSAL_ClockGetElapsedTime();

            for ( loop_cnt = 0; loop_cnt < 16; loop_cnt ++ )
            {
               s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                               u32BytesToRead );

               if ( s32BytesRead != ( tS32 )u32BytesToRead )
               {
                  u32Ret += 20;
                  break;
               }
            }

            EndTime = OSAL_ClockGetElapsedTime();

            /*Wrapping*/
            if ( EndTime > StartTime )
            {
               TimetakenToRead = EndTime - StartTime;
            }
            else
            {
               TimetakenToRead = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }

            if ( u32Ret == 0 )
               vPrintOutTime( TimetakenToRead, TimetakenToWrite, ( ( tU64 )u32BytesToRead*( tU64 )16 ), ( ( tU64 )u32BlockSize*( tU64 )64 ) );

            OSAL_s32IOClose ( hFile );
         }

         /* Remove file without closing */
         if ( OSAL_s32IORemove ( ( tCString )strDevName ) == OSAL_ERROR )
         {
            u32Ret += 40;
         }
      }
   }

   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSTimeMeasureof1MBFilefor16times()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_074
* DESCRIPTION:  Read write time measure for 1MB file 16 times
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSTimeMeasureof1MBFilefor16times( const tS8* DevName, const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   OSAL_tIODescriptor hFile = 0;

   if ( DevName == OSAL_NULL || ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOOpen( ( tCString )DevName, OSAL_EN_READWRITE );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      rSize.u32High = 0;
      rSize.u32Low  = 0;
      /* Get free space */
      u32Ret = ( tU32 )OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE, ( tS32 ) & rSize );

      if ( ( ( rSize.u32Low == 0 ) && ( rSize.u32High == 0 ) ) || ( u32Ret == ( tU32 )OSAL_ERROR ) || ( rSize.u32Low < WRITE_SIZE ) )
      {
         OEDT_HelperPrintf( TR_LEVEL_USER_1, "%s Insufficient Size\n", DevName );
      }
      else
      {
         u32Ret += FSReadWriteTimeMeasure4( ( tU8* )ps8file_name );
      }

      OSAL_s32IOClose ( hFile );
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION:    ThroughPutFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Calculate throughput
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32  ThroughPutFile( const tU8* strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tU32 Read_ThroughPut = 0;
   OSAL_tMSecond StartTime = 0;
   OSAL_tMSecond EndTime = 0;
   OSAL_tMSecond ReadTime = 0;
   tU32 s32PosToSet = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tS32 ReadRet = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Dynamically Allocate Memory for the read buffer */
   ps8ReadBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( CDROM_BYTES_TO_READ_TPUT );

   if ( ps8ReadBuffer == OSAL_NULL )
   {
      u32Ret = 1;
   }
   else
   {
      /* Open the file of .dat format */
      hFile = OSAL_IOOpen ( ( tCString )strDevName, OSAL_EN_READONLY );

      if ( OSAL_ERROR == hFile )
      {
         u32Ret = 2;
      }
      else
      {
         s32PosToSet = 0;

         /* Seek to the byte position specified */
         if ( OSAL_ERROR  == OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                                 ( tS32 )s32PosToSet ) )
         {
            u32Ret = 3;
         }

         /* Read the time before the start of the read operation */
         StartTime = OSAL_ClockGetElapsedTime();
         /* Read the specified number of bytes into an array */
         ReadRet = OSAL_s32IORead( hFile, ps8ReadBuffer, CDROM_BYTES_TO_READ_TPUT );
#ifdef TSIM_OSAL
         /* reading is tooo fast on TSIM */
         /* So delay to make the time measurement meaningful*/
         OSAL_s32ThreadWait( 10 );
#endif
         /* Read the time before the end of the read operation */
         EndTime = OSAL_ClockGetElapsedTime();
         /* The read number is important. Compare if expected number
         is read. But get the time before this as the time calculation in
         critical only for the read operation, not the verification */

         if ( ( OSAL_ERROR == ReadRet ) || ( CDROM_BYTES_TO_READ_TPUT != ReadRet ) )
         {
            u32Ret += 4;
         }
         else
         {
            /* Check if end time greater than start time */
            if ( EndTime > StartTime )
            {
               /* Calculate the time required for read in millisec */
               ReadTime =   ( EndTime - StartTime );
            }
            else
            {
               ReadTime =   ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }

            /* Throughput needs to be expressed in kB/s */

            /* check for ReadTime != 0 */
            if ( ReadTime != 0 )
            {
               /* now we have bytes/ms which is == kB/s */
               Read_ThroughPut = ( CDROM_BYTES_TO_READ_TPUT ) / ( ReadTime );
            }
            else
            {
               Read_ThroughPut = 0;
            }

            /* If the throughput < 300, indicate an error condition */
            if (   Read_ThroughPut < 300 )
            {
               u32Ret += 10;
            }
         }

         /* Close the file */
         if ( OSAL_ERROR == OSAL_s32IOClose( hFile ) )
         {
            u32Ret += 20;
         }
      }
   }

   /* free the dynamically allocated memory */
   if ( OSAL_NULL != ps8ReadBuffer )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   if ( u32Ret == 0 )
      OEDT_HelperPrintf( TR_LEVEL_USER_1,
                         "Time taken: Read :%lu ms -> %lu kB/s",
                         ReadTime,
                         Read_ThroughPut );

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileThroughPutFirstFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_075
* DESCRIPTION:  Calculate throughput for first CDROM file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileThroughPutFirstFile ( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += ThroughPutFile( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileThroughPutSecondFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_076
* DESCRIPTION:  Calculate throughput for last CDROM file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileThroughPutSecondFile ( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += ThroughPutFile( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFileThroughPutMP3File()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_077
* DESCRIPTION:  Calculate throughput for CDROM MP3 file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileThroughPutMP3File ( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += ThroughPutFile( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    RenameFile()
* Parameter:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Rename file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 RenameFile( const tU8* strDevName )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0, hDev = 0;
   tU32 u32Ret = 0;
   tChar* arrFileName[2] = {OSAL_NULL};
   tChar szFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   tChar szReFilePath [OEDT_FS_FILE_PATH_LEN + 1] = {'\0'};
   tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "This is a Test File";

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, strDevName );
   ( tVoid )OSAL_szStringConcat( szFilePath, "/File1.txt" );
   /*Renamed File path */
   ( tVoid )OSAL_szStringCopy( szReFilePath, strDevName );
   ( tVoid )OSAL_szStringConcat( szReFilePath, "/File2.txt" );

   /*Create the file in readwrite mode */
   hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      /*19 bytes will be written*/
      if ( 19 != OSAL_s32IOWrite ( hFile, ( tPCS8 )DataToWrite, 19 ) )
      {
         u32Ret += 49;
      }

      if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
      {
         u32Ret += 59;
      }

      arrFileName[0] = ( tChar* )"File1.txt";      /* old name*/
      arrFileName[1] = ( tChar* )"File2.txt";     /* new name */
      hDev = OSAL_IOOpen ( ( tCString )strDevName, OSAL_EN_READWRITE );

      if ( hDev == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         if ( OSAL_ERROR == OSAL_s32IOControl( hDev, OSAL_C_S32_IOCTRL_FIORENAME, ( tS32 )arrFileName ) )
         {
            u32Ret += 4;

            /*If rename fails, close  the  */
            if ( OSAL_ERROR == OSAL_s32IORemove( szFilePath ) )
            {
               u32Ret += 10;
            }
         }
         else
         {
            /*Delete the old  file */
            if ( OSAL_ERROR == OSAL_s32IORemove( szReFilePath ) )
            {
               u32Ret += 20;
            }
         }/*Sucess of close of the old file*/

         /*Close the new  file */
         if ( OSAL_ERROR == OSAL_s32IOClose( hDev ) )
         {
            u32Ret += 40;
         }
      }/*   Sucess of  IO Control function*/
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSRenameFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_078
* DESCRIPTION:  Rename file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSRenameFile( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += RenameFile( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSWriteFrmBOF()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write to file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSWriteFrmBOF( const tU8* strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "New data written";
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU8 u8BytesToWrite = 19; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;
   tS32 s32PosToSet = 0;
   tPS8 Retval = OSAL_NULL;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      s32PosToSet = 5;

      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                               s32PosToSet ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         ps8WriteBuffer = ( tS8 * ) OSAL_pvMemoryAllocate( u8BytesToWrite + 1 );

         if ( ps8WriteBuffer == OSAL_NULL )
         {
            u32Ret = 3;
         }
         else
         {
            Retval = ( tPS8 )OSAL_szStringCopy( ps8WriteBuffer, DataToWrite );

            if ( *Retval == OSAL_ERROR )
            {
               u32Ret = 4;
            }
            else
            {
               s32BytesWritten = OSAL_s32IOWrite( hFile, ps8WriteBuffer,
                                                  ( tS32 ) u8BytesToWrite );

               if ( ( s32BytesWritten == OSAL_ERROR ) || ( s32BytesWritten != u8BytesToWrite ) )
               {
                  u32Ret = 5;
               }
            }
         }
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 10;
      }

      if ( ps8WriteBuffer != OSAL_NULL )
      {
         OSAL_vMemoryFree( ps8WriteBuffer );
         ps8WriteBuffer = OSAL_NULL;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSWriteFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_079
* DESCRIPTION:  Write to file from BOF
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteFrmBOF( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret ;
   }

   u32Ret += FSWriteFrmBOF( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSWriteFrmEOF()
* PARAMETER:    filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write from EOF
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 24 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FSWriteFrmEOF( const tU8* strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tChar DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Appending more data";
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU8 u8BytesToWrite = 25; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;
   tS32 s32PosToSet = 0;
   tS32 s32FileSz = 0;
   tPS8 Retval = OSAL_NULL;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIONREAD,
                               ( tS32 )&s32FileSz ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         s32PosToSet = s32FileSz;

         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,
                                  s32PosToSet ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
         else
         {
            ps8WriteBuffer = ( tS8 * ) OSAL_pvMemoryAllocate( u8BytesToWrite );

            if ( ps8WriteBuffer == OSAL_NULL )
            {
               u32Ret += 10;
            }
            else
            {
               Retval = ( tPS8 )OSAL_szStringCopy ( ps8WriteBuffer, DataToWrite );

               if ( *Retval == OSAL_ERROR )
               {
                  u32Ret += 20;
               }
               else
               {
                  s32BytesWritten = OSAL_s32IOWrite (
                                       hFile,
                                       ps8WriteBuffer,
                                       ( tS32 ) u8BytesToWrite
                                    );

                  if ( s32BytesWritten == OSAL_ERROR )
                  {
                     u32Ret += 40;
                  }
               }
            }
         }
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 100;
      }

      if ( ps8WriteBuffer != OSAL_NULL )
      {
         OSAL_vMemoryFree( ps8WriteBuffer );
         ps8WriteBuffer = OSAL_NULL;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSWriteFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_080
* DESCRIPTION:  Write to file from EOF
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 30 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteFrmEOF( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret ;
   }

   u32Ret += FSWriteFrmEOF( ( tU8* )ps8file_name );

   /* Remove common file */
   if ( 0 != ( u32Ret += u32ComFSRemoveCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    LargeFileRead()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read large file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 LargeFileRead( const tU8* strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tChar DataToWrite[] = "testdata";
   tU32 u32Ret = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tS32 s32BytesRead = 0;
   tU32 u32Slen = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 loop_cnt = 0;
   tS32 s32BytesWritten = 0;
   tU32 u32BlockSize = 0;  /* 256 KB */
   tU32 u32BytesToRead = 0;/* Number of bytes to read - 1 MB */
   u32Slen = strlen( DataToWrite );
   u32BlockSize = 1024 * 32 * u32Slen;
   u32BytesToRead = u32BlockSize * 4;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if Allocation is successful*/
   if (   ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 500;
   }
   else
   {
      tS8 *ps8tmp;
      /* Write 256 KB of data to a buffer */
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize );

      ps8tmp = ps8WriteBuffer;
      for ( loop_cnt = 0; loop_cnt < 1024*32; loop_cnt ++ )
      {
         (tVoid)OSAL_szStringCopy ((tString)ps8tmp, (tCString)DataToWrite );
         ps8tmp += u32Slen;
      }

      /*Open the common file*/
      hFile = OSAL_IOOpen ( ( tCString ) strDevName, OSAL_EN_READWRITE );

      /*Check if open successful*/
      if ( hFile == OSAL_ERROR )
      {
         u32Ret = 1;
      }
      else
      {
         for ( loop_cnt = 0; loop_cnt <= 3; loop_cnt++ )
         {
            s32BytesWritten = OSAL_s32IOWrite (
                                 hFile, ps8WriteBuffer,
                                 u32BlockSize
                              );

            if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) && ( s32BytesWritten != ( tS32 )u32BlockSize ) )
            {
               u32Ret += 3;
               break;
            }
         }

         OSAL_s32IOClose( hFile );
         hFile = OSAL_IOOpen ( ( tCString ) strDevName, enAccess );

         if ( hFile == OSAL_ERROR )
         {
            OSAL_s32IORemove ( ( tCString ) strDevName );
            u32Ret += 10;
         }
         else
         {
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 );

            /*Check if allocation successful*/
            if ( ps8ReadBuffer == OSAL_NULL )
            {
               /*Shutdown*/
               OSAL_s32IOClose( hFile );
               OSAL_s32IORemove( ( tCString )strDevName );
               OSAL_vMemoryFree( ps8WriteBuffer );
               return ( tU32 )OSAL_ERROR;
            }

            s32BytesRead = OSAL_s32IORead (
                              hFile,
                              ps8ReadBuffer,
                              u32BytesToRead
                           );

            if ( ( s32BytesRead == ( tS32 )OSAL_ERROR ) && ( s32BytesRead != ( tS32 )u32BytesToRead ) )
            {
               u32Ret += 100;
            }

            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               u32Ret += 200;
            }

            if ( OSAL_s32IORemove ( ( tCString ) strDevName ) == OSAL_ERROR )
            {
               u32Ret += 400;
            }
         }
      }
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSLargeFileRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_081
* DESCRIPTION:  Read large file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSLargeFileRead( const tS8* ps8file_name, tBool bCreateRemove )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /* Create the common file */
   if ( 0 != ( u32Ret = u32ComFSCreateCommonFile( ps8file_name, bCreateRemove ) ) )
   {
      return u32Ret ;
   }

   u32Ret += LargeFileRead( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    LargeFileWrite()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Write large data to file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 LargeFileWrite( const tU8* strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tChar DataToWrite[] = "TestData";
   tU32 u32Ret = 0;
   tU32 u32Slen = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 loop_cnt = 0;
   tS32 s32BytesWritten = 0;
   tU32 u32BlockSize = 0;
   u32Slen = strlen( DataToWrite );
   u32BlockSize = 1024 * 32 * u32Slen;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 );

   /*Check if allocation successful*/
   if ( ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 40;
   }
   else
   {
      tS8 *ps8tmp;
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', u32BlockSize + 1 );

      ps8tmp = ps8WriteBuffer;
      /* Write 256 KB of data to a buffer */
      for ( loop_cnt = 0; loop_cnt < 1024*32; loop_cnt ++ )
      {
         (tVoid)OSAL_szStringCopy ((tString)ps8tmp, (tCString)DataToWrite );
         ps8tmp += u32Slen;
      }

      if ( ( hFile = OSAL_IOCreate ( ( tCString )strDevName, enAccess ) ) == OSAL_ERROR )
      {
         u32Ret = 1;
      }
      else
      {
         for ( loop_cnt = 0; loop_cnt <= 3; loop_cnt++ )
         {
            s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                                u32BlockSize );

            if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) && ( s32BytesWritten != ( tS32 )( u32BlockSize ) ) )
            {
               u32Ret = 3;
               break;
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 10;
         }
         else
         {
            if ( OSAL_s32IORemove ( ( tCString ) strDevName ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
         }
      }
   }

   if ( ps8WriteBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSLargeFileWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_082
* DESCRIPTION:  Write large data to file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSLargeFileWrite( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += LargeFileWrite( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    FSOpenCloseThread()
* PARAMETER:    Filename
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Open and close file
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Updated by Deepak Kumar (RBEI/ECF5) on Mar 26, 2015
******************************************************************************/
tU32 FSOpenCloseThread( const tU8* strDevName )
{
   /*Definitions*/
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   OSAL_tEventMask EM  = 0;
   /*Thread attributes*/
   OSAL_trThreadAttribute tr1_atr = {0};
   OSAL_trThreadAttribute tr2_atr = {0};
   /*Initialize thread return code*/
   u32tr1ComFS = 0;
   u32tr2ComFS = 0;
   /*Initialize the event handle*/
   hEventFS = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Create file */
   hFile = OSAL_IOCreate( ( tCString )strDevName , OSAL_EN_READWRITE );

   if ( hFile == OSAL_ERROR )
   {
      u32Ret = 1;
   }

   /*Fill thread Attributes*/
   tr1_atr.szName  = "FSThread1";
   tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   tr1_atr.s32StackSize = 4096;
   tr1_atr.pfEntry      = ( OSAL_tpfThreadEntry )OpenThreadFS;    //lint fix
   tr1_atr.pvArg        = ( tPVoid )strDevName;
   /*Fill thread Attributes*/
   tr2_atr.szName        = "FSThread2";
   tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   tr2_atr.s32StackSize = 4096;
   tr2_atr.pfEntry      = ( OSAL_tpfThreadEntry )CloseThreadFS;   //lint fix
   tr2_atr.pvArg        = OSAL_NULL;

   /*Create the Event field*/
   if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
   {
      u32Ret += 2;
   }

   /*Create the Event field*/
   if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME1 , &hEventFS1 ) )
   {
      u32Ret += 4;
   }

   /*Create the first thread*/
   if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr1_atr ) )
   {
      u32Ret += 10;
   }

   /*Create the first thread*/
   if ( OSAL_ERROR ==  OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr2_atr ) )
   {
      u32Ret += 20;
   }

   /*Wait for threads to Post status*/
   if ( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1 | FS_THREAD2,
                                         OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &EM ) )
   {
      u32Ret += 40;
   }

   OSAL_s32EventPost( hEventFS, ~EM, OSAL_EN_EVENTMASK_AND );

   /*Close event*/
   if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
   {
      u32Ret += 100;
   }

   /*Delete event*/
   if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
   {
      u32Ret += 200;
   }

   /*Close event*/
   if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS1 ) )
   {
      u32Ret += 400;
   }

   /*Delete event*/
   if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME1 ) )
   {
      u32Ret += 1000;
   }

   if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 2000;
   }
   /*Remove the file*/
   else if ( OSAL_ERROR == OSAL_s32IORemove( ( tCString ) strDevName ) )
   {
      u32Ret += 4000;
   }

   /*Include also the thread error code*/
   u32Ret += ( u32tr1ComFS + u32tr2ComFS );
   /*Return the error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSOpenCloseMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_083
* DESCRIPTION:  Open and close file from multiple threads
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpenCloseMultiThread( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSOpenCloseThread( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :    FSWriteThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Writes to file using two threads
* HISTORY     :    Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Updated by Deepak Kumar (RBEI/ECF5) on Mar 26, 2015
*******************************************************************************/
tU32  FSWriteThread( const tU8* strDevName )
{
   /*Definitions*/
   tU32 u32Ret  = 0;
   OSAL_tEventMask EM = 0;
   /*Thread attributes*/
   OSAL_trThreadAttribute tr1_atr = {OSAL_NULL};
   OSAL_trThreadAttribute tr2_atr = {OSAL_NULL};
   /*Initialize thread return code*/
   u32tr1ComFS = 0;
   u32tr2ComFS = 0;
   /*Initialize the event handle*/
   hEventFS = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Create file */
   if ( ( hDeviceComFS = OSAL_IOCreate( ( tCString ) strDevName , OSAL_EN_READWRITE ) )
         == OSAL_ERROR )
   {
      u32Ret = 1;
   }

   if ( !u32Ret )
   {
      /*Allocate memory dynamically for thread 1*/
      HugeBufferTr1ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

      if ( OSAL_NULL == HugeBufferTr1ComFS )
      {
         u32Ret += 2;
      }
      else
      {  /*Fill the buffer with some value*/
         OSAL_pvMemorySet( HugeBufferTr1ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
         OSAL_pvMemorySet( HugeBufferTr1ComFS, 'd', OEDT_CFST_MB_SIZE );
      }

      /*Fill thread Attributes*/
      tr1_atr.szName        = "FSThread1";
      tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      tr1_atr.s32StackSize = 4096;
      tr1_atr.pfEntry      = ( OSAL_tpfThreadEntry )WriteThread1FS;        //lint fix
      tr1_atr.pvArg        = OSAL_NULL;
      /*Allocate memory dynamically for thread 2*/
      HugeBufferTr2ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

      if ( OSAL_NULL == HugeBufferTr2ComFS )
      {
         u32Ret += 4;
      }
      else
      {  /*Fill the buffer with some value*/
         OSAL_pvMemorySet( HugeBufferTr2ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
         OSAL_pvMemorySet( HugeBufferTr2ComFS, 'e', OEDT_CFST_MB_SIZE );
      }

      /*Fill thread Attributes*/
      tr2_atr.szName        = "FSThread2";
      tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      tr2_atr.s32StackSize = 4096;
      tr2_atr.pfEntry      = ( OSAL_tpfThreadEntry )WriteThread2FS;     //lint fix
      tr2_atr.pvArg        = OSAL_NULL;

      /*Create the Event field*/
      if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
      {
         u32Ret += 10;
      }

      /*Create the first thread*/
      if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr1_atr ) )
      {
         u32Ret += 20;
      }

      /*Create the first thread*/
      if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr2_atr ) )
      {
         u32Ret += 40;
      }

      /*Wait for threads to Post status*/
      if ( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1 | FS_THREAD2,
                                            OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &EM ) )
      {
         u32Ret += 100;
      }

      OSAL_s32EventPost( hEventFS, ~EM, OSAL_EN_EVENTMASK_AND );

      /*Close event*/
      if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
      {
         u32Ret += 200;
      }

      /*Delete event*/
      if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
      {
         u32Ret += 400;
      }

      /*Close the file*/
      if ( OSAL_ERROR == OSAL_s32IOClose( hDeviceComFS ) )
      {
         u32Ret += 1100;
      }

      /*Remove the fil from FFS2*/
      if ( OSAL_ERROR == OSAL_s32IORemove( ( tCString )strDevName ) )
      {
         u32Ret += 1200;
      }

      /*Free the dynamically allocated memory*/
      if ( HugeBufferTr1ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr1ComFS );
         HugeBufferTr1ComFS = OSAL_NULL;
      }

      if ( HugeBufferTr2ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr2ComFS );
         HugeBufferTr2ComFS = OSAL_NULL;
      }

      /*Include also the thread error code*/
      u32Ret += ( u32tr1ComFS + u32tr2ComFS );
   }

   /*Return the error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSWriteMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_084
* DESCRIPTION:  write to file from multiple threads
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteMultiThread( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSWriteThread( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :    FSWriteReadThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Write and read to file using two threads
* HISTORY     :    Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Updated by Deepak Kumar(RBEI/ECF5) on Mar 26, 2015
*******************************************************************************/
tU32  FSWriteReadThread( const tU8* strDevName )
{
   /*Definitions*/
   tU32 u32Ret  = 0;
   OSAL_tEventMask EM = 0;
   /*Thread attributes*/
   OSAL_trThreadAttribute tr1_atr = {OSAL_NULL};
   OSAL_trThreadAttribute tr2_atr = {OSAL_NULL};
   /*Initialize thread return code*/
   u32tr1ComFS = 0;
   u32tr2ComFS = 0;
   /*Initialize the event handle*/
   hEventFS = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Create file */
   if ( ( hDeviceComFS = OSAL_IOCreate( ( tCString )strDevName , OSAL_EN_READWRITE ) )
         == OSAL_ERROR )
   {
      u32Ret = 1;
   }

   if ( !u32Ret )
   {
      /*Allocate memory dynamically for thread 1*/
      HugeBufferTr1ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

      if ( OSAL_NULL == HugeBufferTr1ComFS )
      {
         u32Ret += 2;
      }
      else
      {  /*Fill the buffer with some value*/
         OSAL_pvMemorySet( HugeBufferTr1ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
         OSAL_pvMemorySet( HugeBufferTr1ComFS, 'd', OEDT_CFST_MB_SIZE );
      }

      /*Fill thread Attributes*/
      tr1_atr.szName       = "FSThread1";
      tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      tr1_atr.s32StackSize = 4096;
      tr1_atr.pfEntry      = ( OSAL_tpfThreadEntry )WriteThreadFS;     //lint fix
      tr1_atr.pvArg        = OSAL_NULL;
      /*Allocate memory dynamically for thread 2*/
      HugeBufferTr2ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

      if ( OSAL_NULL == HugeBufferTr2ComFS )
      {
         u32Ret += 4;
      }
      else
      {  /*Fill the buffer with some value*/
         OSAL_pvMemorySet( HugeBufferTr2ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
//       OSAL_pvMemorySet( HugeBufferTr2ComFS,'e',OEDT_CFST_MB_SIZE);
      }

      /*Fill thread Attributes*/
      tr2_atr.szName        = "FSThread2";
      tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      tr2_atr.s32StackSize = 4096;
      tr2_atr.pfEntry      = ( OSAL_tpfThreadEntry )ReadThreadFS;         //lint fix
      tr2_atr.pvArg        = OSAL_NULL;

      /*Create the Event field*/
      if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
      {
         u32Ret += 10;
      }

      /*Create the Event field*/
      if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME1 , &hEventFS1 ) )
      {
         u32Ret += 20;
      }

      /*Create the first thread*/
      if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr1_atr ) )
      {
         u32Ret += 40;
      }

      /*Create the first thread*/
      if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr2_atr ) )
      {
         u32Ret += 100;
      }

      /*Wait for threads to Post status*/
      if ( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1 | FS_THREAD2,
                                            OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &EM ) )
      {
         u32Ret += 200;
      }

      OSAL_s32EventPost( hEventFS, ~EM, OSAL_EN_EVENTMASK_AND );

      /*Close event*/
      if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
      {
         u32Ret += 400;
      }

      /*Delete event*/
      if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
      {
         u32Ret += 1000;
      }

      /*Close event*/
      if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS1 ) )
      {
         u32Ret += 1200;
      }

      /*Delete event*/
      if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME1 ) )
      {
         u32Ret += 1400;
      }

      /*Close the file*/
      if ( OSAL_ERROR == OSAL_s32IOClose( hDeviceComFS ) )
      {
         u32Ret += 2000;
      }

      /*Remove the fil from FFS2*/
      if ( OSAL_ERROR == OSAL_s32IORemove( ( tCString ) strDevName ) )
      {
         u32Ret += 2100;
      }

      /*Free the dynamically allocated memory*/
      if ( HugeBufferTr1ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr1ComFS );
         HugeBufferTr1ComFS = OSAL_NULL;
      }

      if ( HugeBufferTr2ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr2ComFS );
         HugeBufferTr2ComFS = OSAL_NULL;
      }

      /*Include also the thread error code*/
      u32Ret += ( u32tr1ComFS + u32tr2ComFS );
   }

   /*Return the error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSWriteReadMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_085
* DESCRIPTION:  write and read to file from multiple threads
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteReadMultiThread( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSWriteReadThread( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :    FSReadThread()
* PARAMETER   :  Filename
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :  Read from file using two threads
* HISTORY     :    Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Updated by Deepak Kumar (RBEI/ECF5) on Mar 26, 2015
*******************************************************************************/
tU32  FSReadThread( const tU8* strDevName )
{
   /*Definitions*/
   tU32 u32Ret  = 0;
   OSAL_tEventMask EM = 0;
   /*Thread attributes*/
   OSAL_trThreadAttribute tr1_atr = {0};
   OSAL_trThreadAttribute tr2_atr = {0};
   /*Initialize thread return code*/
   u32tr1ComFS = 0;
   u32tr2ComFS = 0;
   /*Initialize the event handle*/
   hEventFS = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Create file */
   if ( ( hDeviceComFS = OSAL_IOCreate( ( tCString ) strDevName , OSAL_EN_READWRITE ) )
         == OSAL_ERROR )
   {
      u32Ret = 1;
   }

   if ( !u32Ret )
   {
      /*Allocate memory dynamically for thread 1*/
      HugeBufferTr1ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

      if ( OSAL_NULL == HugeBufferTr1ComFS )
      {
         u32Ret += 2;
      }
      else
      {  /*Fill the buffer with some value*/
         OSAL_pvMemorySet( HugeBufferTr1ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
         OSAL_pvMemorySet( HugeBufferTr1ComFS, 'd', OEDT_CFST_MB_SIZE );

         /*Write some data to File1*/
         if ( OEDT_CFST_MB_SIZE != ( tU32 )OSAL_s32IOWrite( hDeviceComFS, HugeBufferTr1ComFS, OEDT_CFST_MB_SIZE ) )
         {
            u32Ret += 61;
         }

         /*Fill thread Attributes*/
         tr1_atr.szName       = "FSThread1";
         tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
         tr1_atr.s32StackSize = 4096;
         tr1_atr.pfEntry      = ( OSAL_tpfThreadEntry )ReadThread1FS;       //lint fix
         tr1_atr.pvArg        = OSAL_NULL;
         /*Allocate memory dynamically for thread 2*/
         HugeBufferTr2ComFS = ( tS8* )OSAL_pvMemoryAllocate( ( tU32 )( sizeof( tS8 ) * ( OEDT_CFST_MB_SIZE + 1 ) ) );

         if ( OSAL_NULL == HugeBufferTr2ComFS )
         {
            u32Ret += 4;
         }
         else
         {  /*Fill the buffer with some value*/
            OSAL_pvMemorySet( HugeBufferTr2ComFS, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
            //       OSAL_pvMemorySet( HugeBufferTr2ComFS,'e',OEDT_CFST_MB_SIZE);
         }

         /*Fill thread Attributes*/
         tr2_atr.szName        = "FSThread2";
         tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
         tr2_atr.s32StackSize = 4096;
         tr2_atr.pfEntry      = ( OSAL_tpfThreadEntry )ReadThread2FS;      //lint fix
         tr2_atr.pvArg        = OSAL_NULL;

         /*Create the Event field*/
         if ( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , &hEventFS ) )
         {
            u32Ret += 10;
         }

         /*Create the first thread*/
         if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr1_atr ) )
         {
            u32Ret += 40;
         }

         /*Create the first thread*/
         if ( OSAL_ERROR == OSAL_ThreadSpawn( ( OSAL_trThreadAttribute* )&tr2_atr ) )
         {
            u32Ret += 100;
         }

         /*Wait for threads to Post status*/
         if ( OSAL_ERROR == OSAL_s32EventWait( hEventFS, FS_THREAD1 | FS_THREAD2,
                                               OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &EM ) )
         {
            u32Ret += 200;
         }

         OSAL_s32EventPost( hEventFS, ~EM, OSAL_EN_EVENTMASK_AND );

         /*Close event*/
         if ( OSAL_ERROR == OSAL_s32EventClose( hEventFS ) )
         {
            u32Ret += 400;
         }

         /*Delete event*/
         if ( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
         {
            u32Ret += 1000;
         }
      }

      /*Close the file*/
      if ( OSAL_ERROR == OSAL_s32IOClose( hDeviceComFS ) )
      {
         u32Ret += 1100;
      }

      /*Remove the fil from FFS2*/
      if ( OSAL_ERROR == OSAL_s32IORemove( ( tCString ) strDevName ) )
      {
         u32Ret += 1200;
      }

      /*Free the dynamically allocated memory*/
      if ( HugeBufferTr1ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr1ComFS );
         HugeBufferTr1ComFS = OSAL_NULL;
      }

      if ( HugeBufferTr2ComFS != OSAL_NULL )
      {
         OSAL_vMemoryFree( HugeBufferTr2ComFS );
         HugeBufferTr2ComFS = OSAL_NULL;
      }

      /*Include also the thread error code*/
      u32Ret += ( u32tr1ComFS + u32tr2ComFS );
   }

   /*Return the error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSReadMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_086
* DESCRIPTION:  Read from file from multiple threads
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSReadMultiThread( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FSReadThread( ( tU8* )ps8file_name );
   return u32Ret;
}



/*****************************************************************************
* FUNCTION:    FormatDrive()
* PARAMETER:    device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Format Drive
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 FormatDrive( const tU8* strDevName )
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tS32 s32Ret = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFd = OSAL_IOOpen( ( tCString )strDevName, enAccess );

   if ( hFd == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      s32Ret = OSAL_s32IOControl( hFd, OSAL_C_S32_IOCTRL_FIODISKFORMAT, ( tS32 )0 );

      if ( s32Ret == ( tS32 )OSAL_ERROR )
      {
         u32Ret = 2;
      }

      if ( OSAL_s32IOClose( hFd ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSFormat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_087
* DESCRIPTION:  Format FS
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFormat( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += FormatDrive( ( tU8* )ps8file_name );
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    ChkDisk()
* PARAMETER:    device name
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check disk
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 ChkDisk( const tU8* strDevName )
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tS32 s32Ret = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hFd = OSAL_IOOpen( ( tCString )strDevName, enAccess );

   if ( hFd == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      s32Ret = OSAL_s32IOControl( hFd, OSAL_C_S32_IOCTRL_FIOCHKDSK, ( tS32 )0 );

      if ( ( tU32 )OSAL_ERROR == ( tU32 )s32Ret )
      {
         u32Ret = 2;
      }

      if ( OSAL_s32IOClose( hFd ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    u32FSCheckDisk()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FS_088
* DESCRIPTION:  Run Check disk
* HISTORY:     Created by Shilpa Bhat (RBIN/ECM1) on 31 Oct, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCheckDisk( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   u32Ret += ChkDisk( ( const tU8* )ps8file_name );
   return u32Ret;
}
/*****************************************************************************
* FUNCTION     :  u32FSRW_Performance_Diff_BlockSize()
* PARAMETER    :  path to a common file
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_FileSystem_089
* DESCRIPTION  :  Try to measure the time to read from and write to a 2K,10k
*                 32k,50k,100k,300k,500k and    1MB file
* HISTORY      :  Created by Sriranjan(RBIN/ECM1) on 02 Dec, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSRW_Performance_Diff_BlockSize( const tS8* ps8file_name )
{
   tU32 u32Ret = 0;
   OSAL_tIODescriptor filehandle1 = 0;  /*lint !e578 */ /* same name desired*/
   tU8 *u8WriteBuf = OSAL_NULL, *u8ReadBuf = OSAL_NULL;
   tU32 u32Count, u32NumBytes = 0;
   tFloat flAvgWrite_DataRate = 0, flAvgRead_DataRate = 0;
   tFloat aflData_Rate_Wr[8] = {0}, aflData_Rate_Rd[8] = {0};
   OSAL_tMSecond starttime_wr = 0, endtime_wr = 0, totaltime_wr = 0;
   OSAL_tMSecond starttime_rd = 0, endtime_rd = 0, totaltime_rd = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Allocate memory dynamically for write buffer*/
   u8WriteBuf = ( tU8* )OSAL_pvMemoryAllocate( OEDT_CFST_MB_SIZE + 1 );

   if ( OSAL_NULL == u8WriteBuf )
   {
      u32Ret += 1;
   }
   else
   {  /*Fill the buffer with some value*/
      OSAL_pvMemorySet( u8WriteBuf, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
      OSAL_pvMemorySet( u8WriteBuf, 'b', OEDT_CFST_MB_SIZE );
   }

   /*Allocate memory dynamically for write buffer*/
   u8ReadBuf = ( tU8* )OSAL_pvMemoryAllocate( OEDT_CFST_MB_SIZE + 1 );

   if ( OSAL_NULL == u8ReadBuf )
   {
      u32Ret += 2;
   }
   else
   {
      OSAL_pvMemorySet( u8ReadBuf, '\0', ( OEDT_CFST_MB_SIZE + 1 ) );
   }

   if ( u8WriteBuf != OSAL_NULL && u8ReadBuf != OSAL_NULL )
   {
      for ( u32Count = 0; u32Count < 8; u32Count++ )
      {
         switch ( u32Count )
         {
            case 0:
               u32NumBytes = 2048;   //2k
               break;
            case 1:
               u32NumBytes = 10240;  //10k
               break;
            case 2:
               u32NumBytes = 32768;  //32k
               break;
            case 3:
               u32NumBytes = 51200;  //50k
               break;
            case 4:
               u32NumBytes = 102400; //100k
               break;
            case 5:
               u32NumBytes = 307200; //300k
               break;
            case 6:
               u32NumBytes = 512000; //500k
               break;
            case 7:
               u32NumBytes = 1048576;//1M
               break;
            default:
               break;
         }

         /*Initialize the time variables before write operation*/
         starttime_wr = 0;
         endtime_wr = 0;
         totaltime_wr = 0;
         filehandle1 = OSAL_IOOpen( ( tCString )ps8file_name, OSAL_EN_READWRITE );

         if ( filehandle1 == OSAL_ERROR )
         {
            filehandle1 = OSAL_IOCreate( ( tCString )ps8file_name, OSAL_EN_READWRITE );
         }

         if ( filehandle1 != OSAL_ERROR )
         {
            starttime_wr = OSAL_ClockGetElapsedTime();

            //write in to the file
            if ( OSAL_s32IOWrite( filehandle1, ( tPCS8 )u8WriteBuf, u32NumBytes )
                  == OSAL_ERROR )
            {
               u32Ret += 4;
            }

            endtime_wr = OSAL_ClockGetElapsedTime();
            totaltime_wr = endtime_wr - starttime_wr;
            /*calculating the data rate for read operation(MB/S)
            (converting msec to sec by multiplying with 1000 and
            converting bytes to Megabytes by multiplying with
            1024*1024 */
            aflData_Rate_Wr[u32Count] = ( ( tFloat )u32NumBytes * ( tFloat )1000 ) / ( ( tFloat )totaltime_wr * ( tFloat )1024 * ( tFloat )1024 );

            if ( OSAL_s32IOClose( filehandle1 ) != OSAL_OK )
            {
               //file remove error
               u32Ret += 5;
            }

            /*Initialize the time variables before read operation*/
            starttime_rd = 0;
            endtime_rd = 0;
            totaltime_rd = 0;
            // read from the file
            filehandle1 = OSAL_IOOpen( ( tCString )ps8file_name, OSAL_EN_READWRITE );

            if ( filehandle1 != OSAL_ERROR )
            {
               starttime_rd = OSAL_ClockGetElapsedTime();

               //Read from the file
               if ( OSAL_s32IORead( filehandle1, ( tPS8 )u8ReadBuf, u32NumBytes )
                     == OSAL_ERROR )
               {
                  u32Ret += 7;
               }

               endtime_rd = OSAL_ClockGetElapsedTime();
               totaltime_rd = endtime_rd - starttime_rd;
               /*calculating the data rate for read operation(MB/S)
               (converting msec to sec by multiplying with 1000 and
               converting bytes to Megabytes by multiplying with
               1024*1024 */
               aflData_Rate_Rd[u32Count] = ( ( tFloat )u32NumBytes * ( tFloat )1000 ) / ( ( tFloat )totaltime_rd * ( tFloat )1024 * ( tFloat )1024 );

               if ( OSAL_s32IOClose( filehandle1 ) != OSAL_OK )
               {
                  //file remove error
                  u32Ret += 8;
               }

               // compare the written and read bytes
               if ( 0 != OSAL_s32MemoryCompare( u8WriteBuf, u8ReadBuf, u32NumBytes ) )
               {
                  u32Ret += 9;
               }
               else
               {
                  /*Does nothing*/
               }
            }
            else
            {
               // opening the file for read failed
               u32Ret += 10;
            }
         }
         else
         {
            // file create error
            u32Ret += 11;
         }
      }

      if ( OSAL_s32IORemove( ( tCString )ps8file_name ) != OSAL_OK )
      {
         if ( u32Ret == 0 )
         {
            //file remove error
            u32Ret += 12;
         }
      }
   }
   else
   {
      //error buffer allocation failed
      u32Ret += 15;
   }

   if ( OSAL_NULL != u8WriteBuf )
   {
      OSAL_vMemoryFree( u8WriteBuf );
      u8WriteBuf = OSAL_NULL;
   }

   if ( OSAL_NULL != u8ReadBuf )
   {
      OSAL_vMemoryFree( u8ReadBuf );
      u8ReadBuf = OSAL_NULL;
   }

   if ( u32Ret == 0 )
   {
      OEDT_HelperPrintf( TR_LEVEL_USER_1, "The Data Rate for Read and Write operations are:\n" );

      for ( u32Count = 0; u32Count < 8; u32Count++ )
      {
         switch ( u32Count )
         {
            case 0:
               u32NumBytes = 2;   //2k
               break;
            case 1:
               u32NumBytes = 10;  //10k
               break;
            case 2:
               u32NumBytes = 32;  //32k
               break;
            case 3:
               u32NumBytes = 50;  //50k
               break;
            case 4:
               u32NumBytes = 100; //100k
               break;
            case 5:
               u32NumBytes = 300; //300k
               break;
            case 6:
               u32NumBytes = 500; //500k
               break;
            case 7:
               u32NumBytes = 1024;//1M
               break;
            default:
               break;
         }

         flAvgWrite_DataRate =   flAvgWrite_DataRate + aflData_Rate_Wr[u32Count];
         flAvgRead_DataRate = flAvgRead_DataRate + aflData_Rate_Rd[u32Count];
         OEDT_HelperPrintf ( TR_LEVEL_USER_1,
                             "The data rate for %dK Read is	:	%f MB/S \t",
                             u32NumBytes,
                             aflData_Rate_Rd[u32Count]
                           );
         OEDT_HelperPrintf ( TR_LEVEL_USER_1,
                             "The data rate for %dK Write is	:	%f MB/S \n",
                             u32NumBytes,
                             aflData_Rate_Wr[u32Count]
                           );
      }

      //calculating the average write and read data rates
      flAvgWrite_DataRate =   flAvgWrite_DataRate / CFST_FILESYSTEM_NO_IOP;
      flAvgRead_DataRate =    flAvgRead_DataRate / CFST_FILESYSTEM_NO_IOP;
      OEDT_HelperPrintf(   TR_LEVEL_USER_1,
                           "The Average Data rate for Read Operation is		:%f MB/s \t",
                           flAvgRead_DataRate
                       );
      OEDT_HelperPrintf ( TR_LEVEL_USER_1,
                          "The Average Data rate for Write Operation is	:%f MB/s \n",
                          flAvgWrite_DataRate
                        );
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSFileGetExt()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FileSystem_090
* DESCRIPTION  :  Read file/directory contents
* HISTORY      :  Created by Shilpa Bhat(RBIN/ECM1) on 13 Jan, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileGetExt( const tS8 * strDevName )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0, hFile1 = 0, hFile2 = 0;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExtDir s32Stat = {0};
   OSAL_trIOCtrlExtDirent pDir;
   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szFilePath [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW/file.txt";
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW";
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;
   pDir.enFileFlags = ( OSAL_tenFileFlags )0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   OSAL_pvMemorySet( pDir.s8Name, '\0', sizeof( pDir.s8Name ) );
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;
   OSAL_pvMemorySet( temp_ch, '\0', sizeof( temp_ch ) );
   ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile != OSAL_ERROR )
   {
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath1 );
         hFile1 = OSAL_IOOpen ( ( tCString )temp_ch, enAccess );
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath );
         hFile2 = OSAL_IOCreate ( ( tCString )temp_ch, enAccess );

         if ( hFile2 == OSAL_ERROR )
         {
            u32Ret = 4;
         }
         else
         {
            if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 10;
            }

            if ( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }

            if ( OSAL_s32IOClose ( hFile2 ) == OSAL_ERROR )
            {
               u32Ret += 40;
            }
            else
            {
               if ( OSAL_s32IORemove ( ( tCString )temp_ch ) == OSAL_ERROR )
               {
                  u32Ret += 100;
               }
            }
         }

         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }

         if ( (OSAL_s32IOClose( hFile )) == OSAL_ERROR )
         {
            u32Ret += 300;
         }
      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSFileGetExt2()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FileSystem_091
* DESCRIPTION  :  Read file/directory contents with 2 IOCTL
* HISTORY      :  Created by Shilpa Bhat(RBIN/ECM1) on 13 Jan, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileGetExt2( const tS8 * strDevName )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0, hFile1 = 0, hFile2 = 0;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExt2Dir s32Stat = {0};
   OSAL_trIOCtrlExt2Dirent pDir;
   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szFilePath [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW/file.txt";
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW";
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;
   pDir.enFileFlags = ( OSAL_tenFileFlags )0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   OSAL_pvMemorySet( pDir.s8Name, '\0', sizeof( pDir.s8Name ) );
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;
   OSAL_pvMemorySet( temp_ch, '\0', sizeof( temp_ch ) );
   ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile != OSAL_ERROR )
   {
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath1 );
         hFile1 = OSAL_IOOpen ( ( tCString ) temp_ch, enAccess );
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath );
         hFile2 = OSAL_IOCreate ( ( tCString )temp_ch, enAccess );

         if ( hFile2 == OSAL_ERROR )
         {
            u32Ret = 4;
         }
         else
         {
            if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 10;
            }

            if ( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }

            if ( OSAL_s32IOClose ( hFile2 ) == OSAL_ERROR )
            {
               u32Ret += 40;
            }
            else
            {
               if ( OSAL_s32IORemove ( ( tCString ) temp_ch ) == OSAL_ERROR )
               {
                  u32Ret += 100;
               }
            }
         }

         if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }

         if ( (OSAL_s32IOClose( hFile )) == OSAL_ERROR )
         {
            u32Ret += 300;
         }

      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}




/*****************************************************************************
* FUNCTION:    u32FFSCommonFSSaveNowIOCTRL_SyncWrite()
* PARAMETER:    FFS device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with sync write
* HISTORY:     Created by Vijay D (RBEI/ECF1) on 25 Mar, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FFSCommonFSSaveNowIOCTRL_SyncWrite( const tS8* strDevName )
{
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tS32 s32arg = 0, s32BytesWritten = 0;
   tU32 loop_cnt = 0, u32NoBytes = 262144; //256KB
   tU32 u32Ret = 100;  //default error value
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32NoBytes ); //256KB
   /*Check if allocation successful*/
   if ( ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 101;
   }
   else
   {
      /* Write 256 KB of data to a buffer */
      for ( loop_cnt = 0; loop_cnt < u32NoBytes; loop_cnt ++ )
      {
         /*Make sure the buf is filled with values 0-127*/
         ps8WriteBuffer[loop_cnt] =  ( tS8 )( loop_cnt % 128 );
      }

      hFile = OSAL_IOOpen( ( tCString )strDevName, enAccess );

      if ( hFile == OSAL_ERROR )
      {
         hFile = OSAL_IOCreate( ( tCString )strDevName, enAccess );
      }

      if ( hFile == OSAL_ERROR )
      {
         u32Ret = 102;
      }
      else
      {
         s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                             u32NoBytes );

         if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR )
               || ( s32BytesWritten != ( tS32 )( u32NoBytes ) ) )
         {
            u32Ret = 103;
         }
         else if ( OSAL_s32IOControl( hFile, OSAL_C_S32_IOCTRL_FFS_SAVENOW,
                                      s32arg ) == OSAL_OK )
         {
            u32Ret = 104;
         }
         else
         {
            /*Test case has passed succeesfully*/
            u32Ret = 0;
         }
      }

      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:    vAsyncWriteCallback()
* PARAMETER:    void
* RETURNVALUE:  Void
* DESCRIPTION:  Callback fn for asyn write
* HISTORY:     Created by Vijay D (RBEI/ECF1) on 25 Mar, 2009
******************************************************************************/
tVoid vAsyncWriteCallback ( tVoid )
{
   //post event for Callback fn
   OSAL_s32EventPost( hAsynWrite, OEDT_FS_EVENT_POST,
                      OSAL_EN_EVENTMASK_OR );
}

/*****************************************************************************
* FUNCTION:    u32FFSCommonFSSaveNowIOCTRL_AsynWrite()
* PARAMETER:    FFS device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with async write
* HISTORY:     Created by Vijay D (RBEI/ECF1) on 25 Mar, 2009
*              Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*              Updated by Chakitha Saraswathi (RBEI/ECF5) on 05 Feb,2016.
*              Updated by Ganesh Chandra Satish (RBEI/ECF5) on 11 Apr,2016.
******************************************************************************/
tU32 u32FFSCommonFSSaveNowIOCTRL_AsynWrite( const tS8* strDevName )
{
   tS8 *ps8WriteBuffer = OSAL_NULL;
   #ifdef OSAL_GEN3
   /* CF3PF-3742: Removing OSAL_C_S32_IOCTRL_FFS_SAVENOW as it
            is not supported in Gen3(FFS3 moved to eMMC).*/
   #else
   tS32 s32arg = 0; 
   #endif
   tS32 s32BytesWritten = 0;
   tU32 loop_cnt = 0, u32NoBytes = 6291456; //6MB
   tU32 u32Errorcode = 0, u32Ret = 200; //default error value
   OSAL_tIODescriptor hAsyncfilehandle = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trAsyncControl  rAsyncCtrl;
   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32NoBytes ); //6MB
   /*Check if allocation successful*/
   if ( ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 201;
   }
   else
   {
      /* Write 256 KB of data to a buffer */
      for ( loop_cnt = 0; loop_cnt < u32NoBytes; loop_cnt ++ )
      {
         /*Make sure the buf is filled with values 0-127*/
         ps8WriteBuffer[loop_cnt] =  ( tS8 )( loop_cnt % 128 );
      }

      hAsyncfilehandle = OSAL_IOOpen( ( tCString )strDevName, enAccess );

      if ( hAsyncfilehandle == OSAL_ERROR )
      {
         hAsyncfilehandle = OSAL_IOCreate( ( tCString )strDevName, enAccess );
      }

      if ( hAsyncfilehandle == OSAL_ERROR )
      {
         u32Ret = 202;
      }
      else if ( OSAL_s32EventCreate( "Asyn_write", &hAsynWrite ) != OSAL_OK )
      {
         /*Event creation failed*/
         u32Ret = 203;
      }
      else
      {
         /*Prepare for Async write*/
         rAsyncCtrl.id         = hAsyncfilehandle;
         rAsyncCtrl.s32Offset  = 0;
         rAsyncCtrl.pvBuffer   = ps8WriteBuffer;
         rAsyncCtrl.u32Length  = u32NoBytes;
         rAsyncCtrl.pCallBack  = ( OSAL_tpfCallback )vAsyncWriteCallback;
         rAsyncCtrl.pvArg      = OSAL_NULL;
         rAsyncCtrl.u32ErrorCode = 0;
         rAsyncCtrl.s32Status = 0;
         s32BytesWritten = OSAL_s32IOWriteAsync( &rAsyncCtrl );

         if ( s32BytesWritten != ( tS32 )OSAL_OK )
         {
            u32Ret = 204;
         }
         else if ( ( ( u32Errorcode = OSAL_u32IOErrorAsync( &rAsyncCtrl ) )
                     == OSAL_E_NOERROR ) || u32Errorcode == OSAL_E_CANCELED )
         {
            /*before calling wait, make sure
            the async operation started*/
            u32Ret = 205;
         }
         else if ( OSAL_s32ThreadWait( 10 ) != OSAL_OK )
         {
            /*wait for the asyn io to start,
            b4 calling save now ioctr*/
            u32Ret = 206;
         }
         #if defined (OSAL_GEN3) || defined (OSAL_GEN3_SIM )
         /* CF3PF-3742: Removing OSAL_C_S32_IOCTRL_FFS_SAVENOW as it
            is not supported in Gen3(FFS3 moved to eMMC).*/
         #else   
         else if ( OSAL_s32IOControl( hAsyncfilehandle,
                                      OSAL_C_S32_IOCTRL_FFS_SAVENOW,
                                      s32arg ) != OSAL_OK )
         {
            /*Save now IOCTRL error*/
            u32Ret = 207;
         }
         #endif
         else if ( OSAL_s32EventWait( hAsynWrite,
                                      OEDT_FS_EVENT_WAIT,
                                      OSAL_EN_EVENTMASK_AND,
                                      OEDT_FS_EVENT_WAIT_TIMEOUT,
                                      &FSEventResult ) != OSAL_OK )
         {
            /*Callback fn failed*/
            u32Ret = 208;
         }
         else if ( OSAL_u32IOErrorAsync( &rAsyncCtrl ) != OSAL_E_NOERROR )
         {
            /*Async Error*/
            u32Ret = 209;
         }
         else if ( ( tU32 )( OSAL_s32IOReturnAsync( &rAsyncCtrl ) ) != u32NoBytes )
         {
            /*Async write Error*/
            u32Ret = 210;
         }
         else
         {
            /*Test case succeed*/
            u32Ret = 0;
         }

         /*Close the event*/
         if ( OSAL_s32EventClose( hAsynWrite ) != OSAL_OK )
         {
            /*Event close error*/
            u32Ret = 211;
         }

         /*Delete the event*/
         if ( OSAL_s32EventDelete( ( tCString )"Asyn_write" ) != OSAL_OK )
         {
            /*Event delete error*/
            u32Ret = 212;
         }
      }

      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSReadWriteHugeData()
* PARAMETER    :  strFileName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_094
* DESCRIPTION  :  Write/Read the Huge data
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSReadWriteHugeData ( const tS8 * ps8file_name, tChar cData )
{
   OSAL_tIODescriptor hFile   = 0;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   tU32 u32Ret = 0;
   tS32 s32Result = 0;
   tChar szFileName [OEDT_FS_FILE_PATH_LEN] = {0};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BlockSize = CFS_BLOCK_SIZE_256K;
   tS32 s32BytesRead = 0;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU64 u64FreeSpace = 0;
   tS32 s32BytesWritten = 0;
   tU64 u64BytesToWriteRead = 0;
   tU32 u32index = 0;

   if ( ps8file_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFileName, ps8file_name );
   ( tVoid )OSAL_szStringConcat( szFileName, "/large_file.txt" );
   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( CFS_BLOCK_SIZE_256K + 1 );

   /*Check if Allocation is successful*/
   if (   ps8WriteBuffer == OSAL_NULL )
   {
      return ( tU32 )OSAL_ERROR;
   }
   /* Open device */
   else if ( ( hFile = OSAL_IOCreate ( ( tCString )szFileName, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 4;
   }
   else
   {
      rSize.u32High = 0;
      rSize.u32Low  = 0;
      /* Write 256 KB of data to a buffer */
      OSAL_pvMemorySet( ps8WriteBuffer, cData, u32BlockSize );
      /* Get free space */
      s32Result = OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE,
                                      ( tS32 ) & rSize );

      if ( ( ( rSize.u32Low == 0 ) && ( rSize.u32High == 0 ) ) ||
            ( s32Result == OSAL_ERROR ) )
      {
         u32Ret += 2;
         u64BytesToWriteRead = 0; /*Dont write anything*/
         u64FreeSpace = 0; /*Dont write anything*/
      }
      else
      {
         u64FreeSpace = rSize.u32High;
         u64FreeSpace = ( tU64 )( u64FreeSpace << 32 );
         u64FreeSpace += rSize.u32Low;
         u64BytesToWriteRead = u64FreeSpace;
      }

      /*Choose block size as 256k = 256*1024*/
      while ( u64BytesToWriteRead > 0 )
      {
         if ( u64BytesToWriteRead > u32BlockSize )
         {
            u32BlockSize = CFS_BLOCK_SIZE_256K;
         }
         else
         {
            // Since there is error in free size brak for now
            u64BytesToWriteRead  = 0;
            break;
         }

         s32BytesWritten = OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                                             u32BlockSize );

         if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) ||
               ( s32BytesWritten != ( tS32 )u32BlockSize ) )
         {
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32IOWrite() fails with error '%i' and values '%i', '%i'", OSAL_u32ErrorCode(), s32BytesWritten, u32BlockSize );
//            u32Ret = OSAL_u32ErrorCode();
            u32Ret += 5;
            break;
         }

         u64BytesToWriteRead = u64BytesToWriteRead - ( tU32 )s32BytesWritten;
      } /*End of While*/

      if ( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
      {
         u32Ret += 6;
      }
   }/*End of Else*/

   OSAL_vMemoryFree( ps8WriteBuffer );
   ps8WriteBuffer = OSAL_NULL;

   /*Now read the file back and check for single bit error */
   if ( ( hFile = OSAL_IOOpen ( ( tCString )szFileName, enAccess ) ) == OSAL_ERROR )
   {
      OSAL_s32IORemove ( ( tCString )szFileName );
      u32Ret += 8;
      return u32Ret;
   }
   else if ( ( ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( CFS_BLOCK_SIZE_256K + 1 ) ) == OSAL_NULL )
   {
      /*Shutdown*/
      OSAL_s32IOClose( hFile );
      OSAL_s32IORemove( ( tCString )szFileName );
      u32Ret += 9;
      return u32Ret;
   }
   else
   {
      u64BytesToWriteRead = u64FreeSpace;
      u32BlockSize =  CFS_BLOCK_SIZE_256K;

      while ( u64BytesToWriteRead > 0 )
      {
         OSAL_pvMemorySet( ps8ReadBuffer, 0, u32BlockSize ); /*Reset Mem*/

         if ( u64BytesToWriteRead > u32BlockSize )
         {
            u32BlockSize = CFS_BLOCK_SIZE_256K;
         }
         else
         {
            // Since there is error in free size brak for now
            u64BytesToWriteRead  = 0;
            break;
         }

         s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer, u32BlockSize );

         if ( ( s32BytesRead == ( tS32 )OSAL_ERROR ) ||
               ( s32BytesRead != ( tS32 )u32BlockSize ) )
         {
            /*Terminate the while loop in case of error */
            u64BytesToWriteRead = 0;
            u32Ret += 10;
         }
         else
         {
            u64BytesToWriteRead = u64BytesToWriteRead - ( tU32 )s32BytesRead;

            for ( u32index = 0; u32index < ( tU32 )s32BytesRead ; u32index ++ )
            {
               if ( ps8ReadBuffer[u32index] != cData )
               {
                  OEDT_HelperPrintf ( TR_LEVEL_USER_1,
                                      "Read and write data are different\n" );
                  u32Ret += 11;
                  /*Terminate the while loop in case of error */
                  u64BytesToWriteRead = 0;
                  break;
               }
            }
         }
      }/*End of While*/

      OSAL_vMemoryFree( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 12;
         OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32IOClose() fails with error '%i'", OSAL_u32ErrorCode() );
      }

      if ( OSAL_s32IORemove ( ( tCString ) szFileName ) == OSAL_ERROR )
      {
         u32Ret += 13;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  vFSAsyncWrRdCallbackAbort()
* PARAMETER    :  none
* RETURNVALUE  :  None
* DESCRIPTION  :  Call back function for Async Read/Write
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
******************************************************************************/

tVoid vFSAsyncWrRdCallbackAbort ( OSAL_trAsyncControl *prAsyncCtrl )
{
   if ( OSAL_E_NOERROR == OSAL_u32IOErrorAsync( ( OSAL_trAsyncControl* )prAsyncCtrl ) )
   {
      OEDT_HelperPrintf ( TR_LEVEL_USER_1,
                          "OEDT ERROR: Async Callback is called with no error\n" );
   }
}

#ifdef CURRENTLY_NOT_SUPPORTED /*see MMS 239246 */
/*****************************************************************************
* FUNCTION     :  u32FSWriteHugeDataAbort()
* PARAMETER    :  strFileName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_095
* DESCRIPTION  :  Write the Huge data and cancel the operation in between
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
******************************************************************************/
tU32 u32FSWriteHugeDataAbort( const tS8 * strFileName )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tU32 u32Slen = 32;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 u32BlockSize = 0;
   tS32 s32ReturnVal = 0;
   OSAL_trAsyncControl rAsyncCtrl   = {0};
   u32BlockSize = 1024 * 256 * u32Slen; /*256k * string length*/

   if ( ( ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 ) )
         == OSAL_NULL )
   {
      u32Ret = 1;
   }
   /*Create File*/
   else if ( ( hFile = OSAL_IOCreate ( ( tCString ) strFileName, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 2;
   }
   else
   {
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', ( u32BlockSize + 1 ) );
      OSAL_pvMemorySet( ps8WriteBuffer, 'a', u32BlockSize );
      /*Fill the asynchronous control structure*/
      rAsyncCtrl.s32Offset = 0;
      rAsyncCtrl.id = hFile;
      rAsyncCtrl.pvBuffer = ps8WriteBuffer;
      rAsyncCtrl.u32Length = u32BlockSize;
      rAsyncCtrl.pCallBack = ( OSAL_tpfCallback ) vFSAsyncWrRdCallbackAbort; /*Call Back fn*/
      rAsyncCtrl.pvArg = &rAsyncCtrl;

      /*
         If IOWriteAsync returns OSAL_ERROR, Callback function
         is not called so semaphore is not waited upon.
      */
      if ( OSAL_s32IOWriteAsync ( &rAsyncCtrl ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
      else if ( ( OSAL_s32IOCancelAsync( rAsyncCtrl.id, &rAsyncCtrl ) )
                == OSAL_ERROR )
      {
         u32Ret += 5;
      }
      else if ( ( s32ReturnVal = ( tS32 )OSAL_u32IOErrorAsync ( &rAsyncCtrl ) )
                != ( tS32 )OSAL_E_CANCELED )
      {
         u32Ret += 6;
      }
      /* OSAL_s32IOReturnAsync should be called only if error code is not
         equal to OSAL_E_INPROGRESS and OSAL_E_CANCELLED*/
      else if ( s32ReturnVal == ( tS32 )OSAL_E_CANCELED ||
                s32ReturnVal == ( tS32 )OSAL_E_INPROGRESS )
      {
         if ( ( ( tS32 )OSAL_s32IOReturnAsync ( &rAsyncCtrl ) )
               == ( tS32 )u32BlockSize  )
         {
            u32Ret += 7;
         }
      }

      /*Cleanup Task*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 8;
      }
      else if ( OSAL_s32IORemove ( ( tCString ) strFileName ) == OSAL_ERROR )
      {
         u32Ret += 9;
      }
      /* Free the memory allocated */
      else if ( ps8WriteBuffer == OSAL_NULL )
      {
         u32Ret += 10;
      }
      else
      {
         OSAL_vMemoryFree ( ps8WriteBuffer );
         ps8WriteBuffer = OSAL_NULL;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSReadHugeDataAbort()
* PARAMETER    :  strFileName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_096
* DESCRIPTION  :  Read Huge data and cancel the operation in between
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
******************************************************************************/
tU32 u32FSReadHugeDataAbort( const tS8 * strFileName )
{
   OSAL_tIODescriptor hFile = 0;
   tChar pc8DataToWrite[] = "TestDataToTestAbort";
   tU32 u32Ret = 0;
   tU32 u32Slen = 0;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BlockSize = 0;
   OSAL_trAsyncControl rAsyncCtrl   = {0};
   tU32 u32LoopCount = 0;
   tS32 s32ReturnVal = 0;
   u32Slen = strlen( pc8DataToWrite );
   u32BlockSize = 1024 * 256 * u32Slen; /*256k * string length*/

   if ( ( ps8ReadBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BlockSize + 1 ) )
         == OSAL_NULL )
   {
      u32Ret = 1;
   }
   /*Create File*/
   else if ( ( hFile = OSAL_IOCreate ( ( tCString ) strFileName, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 2;
   }
   else
   {
      /*Write in to file */
      OSAL_pvMemorySet( ps8ReadBuffer, '\0', ( u32BlockSize + 1 ) );

      for ( u32LoopCount = 0; u32LoopCount < ( 1024*512 ); u32LoopCount++ )
      {
         if ( OSAL_ERROR == ( OSAL_s32IOWrite( hFile, ( tPCS8 )pc8DataToWrite,
                                               u32Slen ) ) )
         {
            u32Ret += 3;
            break;
         }
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
      else if ( ( hFile = OSAL_IOOpen ( ( tCString ) strFileName, OSAL_EN_READONLY ) )
                == OSAL_ERROR )
      {
         u32Ret += 5;
      }
      else
      {
         /*Fill the asynchronous control structure*/
         rAsyncCtrl.s32Offset = 0;
         rAsyncCtrl.id = hFile;
         rAsyncCtrl.pvBuffer = ps8ReadBuffer;
         rAsyncCtrl.u32Length = u32BlockSize;
         rAsyncCtrl.pCallBack = ( OSAL_tpfCallback ) vFSAsyncWrRdCallbackAbort; /*Call Back fn*/
         rAsyncCtrl.pvArg = &rAsyncCtrl;

         /*
            If IOWriteAsync returns OSAL_ERROR, Callback function
            is not called so semaphore is not waited upon.
         */
         if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
         {
            u32Ret += 4;
         }
         else if ( ( OSAL_s32IOCancelAsync( rAsyncCtrl.id, &rAsyncCtrl ) )
                   == OSAL_ERROR )
         {
            u32Ret += 5;
         }
         else if ( ( s32ReturnVal = ( tS32 )OSAL_u32IOErrorAsync ( &rAsyncCtrl ) )
                   != ( tS32 )OSAL_E_CANCELED )
         {
            u32Ret += 6;
         }
         /* OSAL_s32IOReturnAsync should be called only if error code is not
         equal to OSAL_E_INPROGRESS and OSAL_E_CANCELLED*/
         else if ( s32ReturnVal == ( tS32 )OSAL_E_CANCELED ||
                   s32ReturnVal == ( tS32 )OSAL_E_INPROGRESS )
         {
            if ( ( ( tS32 )OSAL_s32IOReturnAsync ( &rAsyncCtrl ) )
                  == ( tS32 )u32BlockSize  )
            {
               u32Ret += 7;
            }
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            u32Ret += 8;
         }
      }

      if ( OSAL_s32IORemove ( ( tCString ) strFileName ) == OSAL_ERROR )
      {
         u32Ret += 9;
      }
   }

   /* Free the memory allocated */
   if ( ps8ReadBuffer != OSAL_NULL )
   {
      OSAL_vMemoryFree ( ps8ReadBuffer );
      ps8ReadBuffer = OSAL_NULL;
   }

   return u32Ret;
}
#endif

/*****************************************************************************
* FUNCTION     :  u32FSGetFileSize()
* PARAMETER    :  strDevName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_097
* DESCRIPTION  :  Tests the API OSALUTIL_s32FGetSize
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
******************************************************************************/
tU32 u32FSGetFileSize( const tS8 * strDevName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tS8 *ps8WriteBuffer = OSAL_NULL;
   tU32 u32BytesToWrite = CFS_BLOCK_SIZE_256K;
   tU32 u32BytesWriten = 0;
   tU32 u32Ret = 0;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ps8WriteBuffer = ( tS8 * )OSAL_pvMemoryAllocate ( u32BytesToWrite + 1 );

   if ( ps8WriteBuffer == OSAL_NULL )
   {
      u32Ret = 1;
   }
   else if ( ( hFile = OSAL_IOCreate( ( tCString )strDevName, enAccess ) )
             ==  OSAL_ERROR )
   {
      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
      u32Ret += 2;
   }
   else
   {
      OSAL_pvMemorySet( ps8WriteBuffer, '\0', ( u32BytesToWrite + 1 ) );
      OSAL_pvMemorySet( ps8WriteBuffer, 'a', u32BytesToWrite );
      u32BytesWriten = ( tU32 )OSAL_s32IOWrite ( hFile, ps8WriteBuffer,
                       u32BytesToWrite );

      if ( ( ( tS32 )u32BytesWriten ==  ( tS32 )OSAL_ERROR ) ||
            ( ( tS32 )u32BytesToWrite !=  ( tS32 )u32BytesWriten ) )
      {
         u32Ret += 3;
      }
      else
      {
         OEDT_HelperPrintf( TR_LEVEL_USER_1, "\n Bytes Written : %d",
                            u32BytesWriten );
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
      else if ( ( hFile = OSAL_IOOpen( ( tCString )strDevName, enAccess ) )
                ==  OSAL_ERROR )
      {
         u32Ret += 5;
      }
      /*get file size*/
      else if ( ( OSALUTIL_s32FGetSize( hFile ) ) != ( tS32 )u32BytesWriten )
      {
         u32Ret += 6;
      }

      /* Cleanup*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 7;
      }

      if ( OSAL_s32IORemove( ( tCString )strDevName ) )
      {
         u32Ret += 8 ;
      }

      OSAL_vMemoryFree( ps8WriteBuffer );
      ps8WriteBuffer = OSAL_NULL;
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSReadDirValidate()
* PARAMETER    :  ps8_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_098
* DESCRIPTION  :  Creates the known directory sructure and and validates
*                 against OSALUTIL_prReadDir() call
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSReadDirValidate( const tS8 * ps8_name )
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_trIOCtrlDirent rDirent1 = {""};
   OSAL_trIOCtrlDirent rDirent2 = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trIOCtrlDir* pDir = OSAL_NULL;
   OSAL_trIOCtrlDirent* pEntry = OSAL_NULL;
   tU32 u32Ret  = 0;
   tChar szDirName1 [] = "NEWSUB53";
   tChar szDirName2 [] = "NEWSUB54";
   tChar szFileName [] = "File1.txt";
   tChar szFilePath [FILE_NAME_MAX_LEN] = {0};
   tU32 u32DirCount = 3; /*Total 3 dir*/

   if ( ps8_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*path name for File*/
   ( tVoid )OSAL_szStringCopy( szFilePath, ps8_name );
   ( tVoid )OSAL_szStringConcat( szFilePath, "/" );
   ( tVoid )OSAL_szStringConcat( szFilePath, szFileName );

   /*Initialize the Dirent structure*/
   ( tVoid )OSAL_pvMemorySet( rDirent1.s8Name, '\0', OSAL_C_U32_MAX_PATHLENGTH );
   ( tVoid )OSAL_pvMemorySet( rDirent2.s8Name, '\0', OSAL_C_U32_MAX_PATHLENGTH );
   hDir = OSAL_IOOpen ( ( tCString )ps8_name, enAccess );

   if ( hDir == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      ( tVoid )OSAL_szStringCopy ( rDirent1.s8Name, szDirName1 );
      ( tVoid )OSAL_szStringCopy ( rDirent2.s8Name, szDirName2 );

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent1 ) == OSAL_ERROR )
      {
         u32Ret += 2;
      }
      else if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIOMKDIR,
                                    ( tS32 )&rDirent2 ) == OSAL_ERROR )
      {
         u32Ret += 3;
      }
      else if ( ( ( hFile = OSAL_IOCreate ( ( tCString )szFilePath, enAccess ) )
                  == OSAL_ERROR ) )
      {
         u32Ret += 4;
      } /*Close file*/
      else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 9;
      }
      /* e.g.
         /nor0/NEWSUB53
         /nor0/NEWSUB54
         /nor0/File1.txt
      */
      else if ( ( pDir = OSALUTIL_prOpenDir ( ( tCString )ps8_name ) ) == OSAL_NULL )
      {
         u32Ret += 10;
      }
      else
      {
         pEntry = OSALUTIL_prReadDir( pDir );

         while ( pEntry != OSAL_NULL )
         {
            if ( OSAL_s32MemoryCompare( pEntry->s8Name, szDirName1,
                                        sizeof( szDirName1 ) ) == 0 ||
                  OSAL_s32MemoryCompare( pEntry->s8Name, szDirName2,
                                         sizeof( szDirName2 ) ) == 0 ||
                  OSAL_s32MemoryCompare( pEntry->s8Name, szFileName,
                                         sizeof( szFileName ) ) == 0 )
            {
               OEDT_HelperPrintf( TR_LEVEL_USER_1, "\tDir is same %s\n", pEntry->s8Name );
               u32DirCount--;
            }

            pEntry = OSALUTIL_prReadDir( pDir );
         }/*End of While*/

         if ( OSAL_ERROR == OSALUTIL_s32CloseDir ( pDir ) )
         {
            u32Ret += 11;
         }
         else if ( u32DirCount != 0 )
         {
            u32Ret += 12;
         }
      }

      /*Cleanup*/
      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent1 ) == OSAL_ERROR )
      {
         u32Ret += 13;
      }

      if ( OSAL_s32IOControl ( hDir, OSAL_C_S32_IOCTRL_FIORMDIR,
                               ( tS32 )&rDirent2 ) == OSAL_ERROR )
      {
         u32Ret += 14;
      }

      /*Close dir*/
      if ( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
      {
         u32Ret += 15;
      }

      if ( OSAL_ERROR == OSAL_s32IORemove( ( tCString )szFilePath ) )
      {
         u32Ret += 16;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  vRMDirRecThread()
* PARAMETER    :  pvData: Thread parameter of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Removes a given directory recursively
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static void vRMDirRecThread( const tVoid *ThParm )
{
   OSAL_tIODescriptor hDevice = 0;
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( ( hDevice = OSAL_IOOpen( ( tCString )thread_param->szDevName,
                                    OSAL_EN_READWRITE ) ) == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         OSAL_s32EventPost( CFS_dir_recursive, EVENT_POST_DIR_RECURSIVE_START,
                            OSAL_EN_EVENTMASK_OR );

         if ( OSAL_ERROR == OSAL_s32IOControl( hDevice,
                     OSAL_C_S32_IOCTRL_FIORMRECURSIVE, ( tS32 )CFS_DIR_NAME ) )
         {
            thread_param->u32Ret = OSAL_u32ErrorCode();
         }

         /*Post an event to the main thread*/
         OSAL_s32EventPost( CFS_dir_recursive, EVENT_POST_DIR_RECURSIVE_STOP,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  u32FSRmRecursiveCancel()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_099
* DESCRIPTION  :  Removes the directory recursively and cancels it
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
                  updated by sja3kor on feb 26 ,2013  
******************************************************************************/
tU32 u32FSRmRecursiveCancel( const tS8* dev_name )
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	FSThreadParam ThParam = {0};
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szFilePath [CFS_FILE_NAME_MAX_LEN_WITHOUT_DEV_NAME + sizeof("/"CFS_DIR_NAME)] = {0};
	OSAL_trThreadAttribute tr1_atr = {0};
	ThParam.szDevName = ( tString )dev_name;

	if ( dev_name == OSAL_NULL )
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: dev_name is OSAL_NULL");
		u32Ret = 9000;
		return u32Ret;
	}

	/*Open the device in read write mode */
	hDevice = OSAL_IOOpen( ( tCString )dev_name, enAccess );

	if ( hDevice == OSAL_ERROR )
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_IOOpen() failed with error %i", OSAL_u32ErrorCode());
		u32Ret += 1;
	}
	else
	{
		/*path name for Dir*/
		( tVoid )OSAL_szStringCopy( szFilePath, "/"CFS_DIR_NAME );
		while ( OSAL_ERROR != OSALUTIL_s32CreateDir( hDevice, szFilePath ) )
		{
			( tVoid )OSAL_szStringConcat( szFilePath, "/"CFS_DIR_NAME);
			if ( strlen( szFilePath ) >= CFS_FILE_NAME_MAX_LEN_WITHOUT_DEV_NAME )
			break;
		}

		( tVoid )OSAL_szStringCopy( szFilePath, "/"CFS_DIR_NAME );
		( tVoid )OSAL_szStringConcat( szFilePath, "/"CFS_DIR_NAME_2);
		while ( OSAL_ERROR != OSALUTIL_s32CreateDir( hDevice, szFilePath ) )
		{
			( tVoid )OSAL_szStringConcat( szFilePath, "/"CFS_DIR_NAME_2);
			if ( strlen( szFilePath ) >= CFS_FILE_NAME_MAX_LEN_WITHOUT_DEV_NAME )
			break;
		}

		( tVoid )OSAL_szStringCopy( szFilePath, "/"CFS_DIR_NAME );
		( tVoid )OSAL_szStringConcat( szFilePath, "/"CFS_DIR_NAME_3);
		while ( OSAL_ERROR != OSALUTIL_s32CreateDir( hDevice, szFilePath ) )
		{
			( tVoid )OSAL_szStringConcat( szFilePath, "/"CFS_DIR_NAME_3);
			if ( strlen( szFilePath ) >= CFS_FILE_NAME_MAX_LEN_WITHOUT_DEV_NAME )
			break;
		}

		/*Fill thread Attributes*/
		tr1_atr.szName       = "FSDelDir";
		tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr1_atr.s32StackSize = FS_THREAD_STACK_SIZE;
		tr1_atr.pfEntry      = ( OSAL_tpfThreadEntry )vRMDirRecThread;
		tr1_atr.pvArg        = &ThParam;

		if ( OSAL_OK == OSAL_s32EventCreate( "RemRec", &CFS_dir_recursive ) )
		{
			( tVoid )OSAL_szStringCopy( szFilePath, CFS_DIR_NAME );

			/*create and activate thread1 */
			if ( OSAL_ERROR != OSAL_ThreadSpawn( &tr1_atr ) )
			{
				if ( OSAL_OK != OSAL_s32EventWait( CFS_dir_recursive,
							EVENT_POST_DIR_RECURSIVE_START, OSAL_EN_EVENTMASK_AND,
							FS_EVENT_WAIT_TIMEOUT, &ResultMask ) )
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait() failed with error %i", OSAL_u32ErrorCode());
					u32Ret += 10;
				}
				
				OSAL_s32ThreadWait(50);

				if ( OSAL_OK  != OSAL_s32IOControl( hDevice,
							OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL, ( tS32 )CFS_DIR_NAME ) )
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL) failed with error %i", OSAL_u32ErrorCode());
					u32Ret += OSAL_u32ErrorCode();
				}

				if ( OSAL_OK != OSAL_s32EventWait( CFS_dir_recursive,
							EVENT_POST_DIR_RECURSIVE_STOP, OSAL_EN_EVENTMASK_AND,
							FS_EVENT_WAIT_TIMEOUT, &ResultMask ) )
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait() failed with error %i", OSAL_u32ErrorCode());
					u32Ret += OSAL_u32ErrorCode();
				}
			}
			
			/*Close the event*/
			if ( OSAL_s32EventClose( CFS_dir_recursive ) != OSAL_OK )
			{
				/*Event close error*/
				u32Ret = 211;
			}

			/*Delete the event*/
			if ( OSAL_s32EventDelete( ( tCString )"RemRec" ) != OSAL_OK )
			{
				/*Event delete error*/
				u32Ret = 212;
			}
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate() failed with error %i", OSAL_u32ErrorCode());
			u32Ret += 24;
		}
        
		if(ThParam.u32Ret == OSAL_E_CANCELED)    //if OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL successuful then only do rmrecursive.sja3kor
		{
		   ThParam.u32Ret = 0;
		/*Check for if the dir actually deleted or not*/
			if ( OSAL_OK != OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
					( tS32 ) CFS_DIR_NAME ) )
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_FIORMRECURSIVE) failed with error %i", OSAL_u32ErrorCode());
					u32Ret += OSAL_u32ErrorCode();
				}
	    }	
	}

	return u32Ret + ThParam.u32Ret;
}


/*****************************************************************************
* FUNCTION     :  vFSCreateFileThread()
* PARAMETER    :  ps8_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Creates the file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSCreateFileThread( const tVoid * ThParm )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( ( hFile = OSAL_IOCreate ( ( tString )( thread_param->szDevName ), enAccess ) )
            == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         thread_param->u32Ret += 2;
      }
      else
      {
         OSAL_s32EventPost( CFS_create_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateManyThreadDeleteMain()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_100
* DESCRIPTION  :  Creates the file in many threads and deletes it in main thread
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateManyThreadDeleteMain( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tU32 u32Ret = 0;
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB853" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB854" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File81.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File83.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File84.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;
   /*thread stuff*/
   rThAttr1.szName  = "ThCr1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSCreateFileThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThCr2";;
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSCreateFileThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThCr3";;
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSCreateFileThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThCr4";;
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSCreateFileThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThCr5";;
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSCreateFileThread;
   rThAttr5.pvArg   = &ParamTh5;;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( OSAL_OK == OSAL_s32EventCreate( "FileCr", &CFS_create_file ) )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 1;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 2;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 3;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 4;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 5;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_create_file, CFS_THREAD_ALL_MASK,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 6;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_create_file, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_create_file ) )
      {
         u32Ret += 7;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileCr" ) )
      {
         u32Ret += 8;
      }
   }
   else
   {
      u32Ret += 24;
   }

   if ( ParamTh1.u32Ret == 0 )
   {
      if ( OSAL_s32IORemove ( ( tCString )szFilePath1 )
            == OSAL_ERROR )
      {
         u32Ret += 9;
      }
   }

   if ( ParamTh2.u32Ret == 0 )
   {
      if ( OSAL_s32IORemove ( ( tCString )szFilePath2 )
            == OSAL_ERROR )
      {
         u32Ret += 10;
      }
   }

   if ( ParamTh3.u32Ret == 0 )
   {
      if ( OSAL_s32IORemove ( ( tCString )szFilePath3 )
            == OSAL_ERROR )
      {
         u32Ret += 11;
      }
   }

   if ( ParamTh4.u32Ret == 0 )
   {
      if ( OSAL_s32IORemove ( ( tCString )szFilePath4 )
            == OSAL_ERROR )
      {
         u32Ret += 12;
      }
   }

   if ( ParamTh5.u32Ret == 0 )
   {
      if ( OSAL_s32IORemove ( ( tCString )szFilePath5 )
            == OSAL_ERROR )
      {
         u32Ret += 13;
      }
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSDeleteFileThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Removes the given file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSDeleteFileThread( const tVoid * ThParm )
{
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( OSAL_s32IORemove ( ( tString )( thread_param->szDevName ) ) == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         OSAL_s32EventPost( CFS_delete_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}



/*****************************************************************************
* FUNCTION     :  u32FSCreateManyDeleteInThread()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_101
* DESCRIPTION  :  Creates many files and deletes in thread
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSCreateManyDeleteInThread( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};
   tU32 u32Ret = 0;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB753" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB754" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File71.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File73.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File74.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;
   /*thread stuff*/
   rThAttr1.szName  = "ThDel1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSDeleteFileThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThDel2";;
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSDeleteFileThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThDel3";;
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSDeleteFileThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThDel4";;
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSDeleteFileThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThDel5";;
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSDeleteFileThread;
   rThAttr5.pvArg   = &ParamTh5;;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( ( hFile = OSAL_IOCreate ( szFilePath1, enAccess ) )
         == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 2;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath2, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 3;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 4;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath3, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 5;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 6;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath4, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 7;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 8;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath5, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 9;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 10;
   }
   else if ( OSAL_OK == OSAL_s32EventCreate( "FileDel", &CFS_delete_file ) )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 11;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 12;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 13;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 14;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 15;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_delete_file, CFS_THREAD_ALL_MASK,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 16;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_delete_file, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_delete_file ) )
      {
         u32Ret += 17;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileDel" ) )
      {
         u32Ret += 18;
      }
   }
   else
   {
      u32Ret += 24;
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSCloseFileThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Closes the fiven file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSCloseFileThread( const tVoid * ThParm )
{
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( OSAL_s32IOClose ( ( OSAL_tIODescriptor )( thread_param->ioHandle ) )
            == OSAL_ERROR )
      {
         thread_param->u32Ret += 2;
      }
      else if ( OSAL_s32IORemove ( ( tCString )( thread_param->szDevName ) )
                == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         OSAL_s32EventPost( CFS_close_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  u32FSOpenManyCloseInThread()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_102
* DESCRIPTION  :  Opens many file and closes in many threads
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpenManyCloseInThread( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tU32 u32Ret = 0;
   OSAL_tIODescriptor hFile1 = 0;
   OSAL_tIODescriptor hFile2 = 0;
   OSAL_tIODescriptor hFile3 = 0;
   OSAL_tIODescriptor hFile4 = 0;
   OSAL_tIODescriptor hFile5 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB553" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB554" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File61.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File63.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File64.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;

   if ( ( hFile1 = OSAL_IOCreate ( szFilePath1, enAccess ) )
         == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( ( hFile2 = OSAL_IOCreate ( szFilePath2, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 3;
   }
   else if ( ( hFile3 = OSAL_IOCreate ( szFilePath3, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 5;
   }
   else if ( ( hFile4 = OSAL_IOCreate ( szFilePath4, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 6;
   }
   else if ( ( hFile5 = OSAL_IOCreate ( szFilePath5, enAccess ) )
             == OSAL_ERROR )
   {
      u32Ret += 7;
   }
   else
   {
      ParamTh1.ioHandle = hFile1;
      ParamTh2.ioHandle = hFile2;
      ParamTh3.ioHandle = hFile3;
      ParamTh4.ioHandle = hFile4;
      ParamTh5.ioHandle = hFile5;
   }

   /*thread stuff*/
   rThAttr1.szName  = "ThClo1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSCloseFileThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThClo2";;
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSCloseFileThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThClo3";;
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSCloseFileThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThClo4";;
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSCloseFileThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThClo5";;
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSCloseFileThread;
   rThAttr5.pvArg   = &ParamTh5;;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( OSAL_OK == OSAL_s32EventCreate( "FileClose", &CFS_close_file )
         && u32Ret == 0 )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 8;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 9;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 10;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 11;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 12;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_close_file, CFS_THREAD_ALL_MASK,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 13;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_close_file, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_close_file ) )
      {
         u32Ret += 14;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileClose" ) )
      {
         u32Ret += 15;
      }
   }
   else
   {
      u32Ret += 24;
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSOpenFileThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Open the given file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSOpenFileThread( const tVoid * ThParm )
{
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;
      thread_param->ioHandle = OSAL_IOOpen ( ( tCString )thread_param->szDevName,
                                             OSAL_EN_READWRITE );

      if ( thread_param->ioHandle == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         OSAL_s32EventPost( CFS_open_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  u32FSOpenInThreadCloseMain()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_103
* DESCRIPTION  :  Open the many file in thread and close in main
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSOpenInThreadCloseMain( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tU32 u32Ret = 0;
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};
   OSAL_tIODescriptor hFile = 0;

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB1.wav" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB14" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File51.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File53.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File54.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;

   /* Create the files here */
   if ( ( hFile = OSAL_IOCreate ( szFilePath1, OSAL_EN_READWRITE ) )
         == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 2;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath2, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 3;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 4;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath3, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 5;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 6;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath4, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 7;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 8;
   }
   else if ( ( hFile = OSAL_IOCreate ( szFilePath5, OSAL_EN_READWRITE ) )
             == OSAL_ERROR )
   {
      u32Ret += 9;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 10;
   }
   else
   {
      /*thread stuff*/
      rThAttr1.szName  = "ThOpen1";
      rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSOpenFileThread;
      rThAttr1.pvArg   = &ParamTh1;
      rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
      rThAttr2.szName  = "ThOpen2";
      rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSOpenFileThread;
      rThAttr2.pvArg   = &ParamTh2;
      rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
      rThAttr3.szName  = "ThOpen3";
      rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSOpenFileThread;
      rThAttr3.pvArg   = &ParamTh3;
      rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
      rThAttr4.szName  = "ThOpen4";
      rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSOpenFileThread;
      rThAttr4.pvArg   = &ParamTh4;
      rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
      rThAttr5.szName  = "ThOpen5";
      rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSOpenFileThread;
      rThAttr5.pvArg   = &ParamTh5;
      rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

      if ( OSAL_OK == OSAL_s32EventCreate( "FileClose", &CFS_open_file ) )
      {
         /*create and activate thread1 */
         if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
         {
            u32Ret += 11;
         }
         else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
         {
            u32Ret += 12;
         }
         else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
         {
            u32Ret += 13;
         }
         else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
         {
            u32Ret += 14;
         }
         else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
         {
            u32Ret += 15;
         }

         //wait for 2 events signalling the end of read operation
         if ( OSAL_OK != OSAL_s32EventWait( CFS_open_file, CFS_THREAD_ALL_MASK,
                                            OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                            &ResultMask ) )
         {
            u32Ret += 16;
         }
         else
         {
            if ( OSAL_s32IOClose ( ParamTh1.ioHandle ) == OSAL_ERROR )
            {
               u32Ret += 17;
            }
            else if ( OSAL_s32IORemove ( szFilePath1 ) == OSAL_ERROR )
            {
               u32Ret += 18;
            }

            if ( OSAL_s32IOClose ( ParamTh2.ioHandle ) == OSAL_ERROR )
            {
               u32Ret += 19;
            }
            else if ( OSAL_s32IORemove ( szFilePath2 ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }

            if ( OSAL_s32IOClose ( ParamTh3.ioHandle ) == OSAL_ERROR )
            {
               u32Ret += 21;
            }
            else if ( OSAL_s32IORemove ( szFilePath3 ) == OSAL_ERROR )
            {
               u32Ret += 22;
            }

            if ( OSAL_s32IOClose ( ParamTh4.ioHandle ) == OSAL_ERROR )
            {
               u32Ret += 23;
            }
            else if ( OSAL_s32IORemove ( szFilePath4 ) == OSAL_ERROR )
            {
               u32Ret += 24;
            }

            if ( OSAL_s32IOClose ( ParamTh5.ioHandle ) == OSAL_ERROR )
            {
               u32Ret += 25;
            }
            else if ( OSAL_s32IORemove ( szFilePath5 ) == OSAL_ERROR )
            {
               u32Ret += 26;
            }
         }

         /* Clear the result mask*/
         OSAL_s32EventPost( CFS_open_file, ~ResultMask,
                            OSAL_EN_EVENTMASK_AND );

         //close the event
         if ( OSAL_ERROR == OSAL_s32EventClose( CFS_open_file ) )
         {
            u32Ret += 27;
         }
         //delete the event
         else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileClose" ) )
         {
            u32Ret += 28;
         }
      }
      else
      {
         u32Ret += 24;
      }
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSReadFileThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Read the given file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSReadFileThread( const tVoid * ThParm )
{
   OSAL_tIODescriptor hFile = 0;
   tS32 s32BytesRead = 0;
   tU32 u32BytesToRead = strlen( CFS_WRITE_TEST_DATA );
   tS8 tS8DataRead[50] = {0};
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;
      hFile = OSAL_IOOpen ( ( tCString )thread_param->szDevName, OSAL_EN_READONLY );

      if ( hFile == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         s32BytesRead = OSAL_s32IORead ( hFile, tS8DataRead, u32BytesToRead );

         if ( ( s32BytesRead ==  ( tS32 )OSAL_ERROR ) ||
               ( s32BytesRead !=  ( tS32 )u32BytesToRead ) )
         {
            thread_param->u32Ret += 2;
         }

         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            thread_param->u32Ret += 3;
         }

         if ( OSAL_s32IORemove ( ( tCString )thread_param->szDevName )
               == OSAL_ERROR )
         {
            thread_param->u32Ret += 4;
         }

         OSAL_s32EventPost( CFS_read_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}

/*****************************************************************************
* FUNCTION     :  u32CreateWriteFile()
* PARAMETER    :  FileName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create a file and write data in to it
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tU32 u32CreateWriteFile( const tS8 * FileName )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u8BytesToWrite = strlen( CFS_WRITE_TEST_DATA );
   tU32 u32Ret = 0;
   tCS8 pc8DataToWrite[50] = CFS_WRITE_TEST_DATA;

   if ( FileName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( ( hFile = OSAL_IOCreate ( ( tCString )FileName, enAccess ) )
         == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else if ( ( OSAL_s32IOWrite ( hFile, pc8DataToWrite,
                                 u8BytesToWrite ) ) != ( tS32 )u8BytesToWrite )
   {
      u32Ret += 2;
   }
   else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
   {
      u32Ret += 3;
   }

   return u32Ret;
}



/*****************************************************************************
* FUNCTION     :  u32FSWriteMainReadInThread()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_104
* DESCRIPTION  :  Writes to a file in main read in thread
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteMainReadInThread( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tU32 u32Ret = 0;
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB3" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB30" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File10.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File30.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File40.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;
   u32Ret += ( u32CreateWriteFile( ( tS8* )szFilePath1 ) * 1 );
   u32Ret += ( u32CreateWriteFile( ( tS8* )szFilePath2 ) * 2 );
   u32Ret += ( u32CreateWriteFile( ( tS8* )szFilePath3 ) * 3 );
   u32Ret += ( u32CreateWriteFile( ( tS8* )szFilePath4 ) * 4 );
   u32Ret += ( u32CreateWriteFile( ( tS8* )szFilePath5 ) * 5 );
   /*thread stuff*/
   rThAttr1.szName  = "ThRead1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSReadFileThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThRead2";;
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSReadFileThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThRead3";;
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSReadFileThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThRead4";;
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSReadFileThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThRead5";;
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSReadFileThread;
   rThAttr5.pvArg   = &ParamTh5;;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( OSAL_OK == OSAL_s32EventCreate( "FileRead", &CFS_read_file )
         && u32Ret == 0 )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 1;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 2;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 3;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 4;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 5;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_read_file, CFS_THREAD_ALL_MASK,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 6;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_read_file, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_read_file ) )
      {
         u32Ret += 7;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileRead" ) )
      {
         u32Ret += 8;
      }
   }
   else
   {
      u32Ret += 9;
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSWriteFileThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Writes data in to given file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSWriteFileThread( const tVoid* ThParm )
{
   OSAL_tIODescriptor hFile = 0;
   tU32 u32BytesToWrite = strlen( CFS_WRITE_TEST_DATA );
   tCS8 pc8DataToWrite[50] = CFS_WRITE_TEST_DATA;
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( ( hFile = OSAL_IOCreate ( ( tCString )thread_param->szDevName,
                                     OSAL_EN_READWRITE ) ) == OSAL_ERROR )
      {
         thread_param->u32Ret += 1;
      }
      else if ( ( OSAL_s32IOWrite ( hFile, pc8DataToWrite,
                                    u32BytesToWrite ) ) != ( tS32 )u32BytesToWrite )
      {
         thread_param->u32Ret += 2;
      }
      else if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         thread_param->u32Ret += 3;
      }
      else
      {
         OSAL_s32EventPost( CFS_write_file, thread_param->mask,
                            OSAL_EN_EVENTMASK_OR );
      }

      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  vFSReadCloseRemove()
* PARAMETER    :  FileName: File name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Read the data, close and removes the file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tU32 vFSReadCloseRemove( const tVoid * FileName )
{
   OSAL_tIODescriptor hFile = 0;
   tS8 s8DataToRead[50] = {0};
   tU32 u32Ret = 0;
   tS32 s32BytesRead = 0;
   tU32 u32BytesToRead = strlen( CFS_WRITE_TEST_DATA );

   if ( FileName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   if ( ( hFile = OSAL_IOOpen ( FileName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      s32BytesRead = OSAL_s32IORead ( hFile, s8DataToRead, u32BytesToRead );

      if ( ( s32BytesRead ==  ( tS32 )OSAL_ERROR ) || ( s32BytesRead !=  ( tS32 )u32BytesToRead ) )
      {
         u32Ret += 2;
      }

      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 3;
      }
      else if ( OSAL_s32IORemove ( FileName ) == OSAL_ERROR )
      {
         u32Ret += OSAL_u32ErrorCode();
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSWriteThreadReadInMain()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_105
* DESCRIPTION  :  Write in thread and read in main
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSWriteThreadReadInMain( const tS8 * dev_name )
{
   tU32 u32Ret = 0;
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB5.bmp" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB4" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File3.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File4.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File5.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;
   /*thread stuff*/
   rThAttr1.szName  = "ThWrite1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSWriteFileThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThWrite2";;
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSWriteFileThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThWrite3";;
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSWriteFileThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThWrite4";;
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSWriteFileThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThWrite5";;
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSWriteFileThread;
   rThAttr5.pvArg   = &ParamTh5;;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( OSAL_OK == OSAL_s32EventCreate( "FileWrit", &CFS_write_file ) )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 16;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 17;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 18;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 19;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 20;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_write_file, CFS_THREAD_ALL_MASK,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 21;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_write_file, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_write_file ) )
      {
         u32Ret += 22;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileWrit" ) )
      {
         u32Ret += 23;
      }

      /*Delete files one by one */
      u32Ret += ( vFSReadCloseRemove( szFilePath1 ) * 1 );
      u32Ret += ( vFSReadCloseRemove( szFilePath2 ) * 2 );
      u32Ret += ( vFSReadCloseRemove( szFilePath3 ) * 3 );
      u32Ret += ( vFSReadCloseRemove( szFilePath4 ) * 4 );
      u32Ret += ( vFSReadCloseRemove( szFilePath5 ) * 5 );
   }
   else
   {
      u32Ret += 24;
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}


/*****************************************************************************
* FUNCTION     :  vFSFileReadWriteThread()
* PARAMETER    :  ThParm: Thread argument of type FSThreadParam
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create, open write , read and remove a file
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
static tVoid vFSFileReadWriteThread( const tVoid* ThParm )
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tS8 *ps8ReadBuffer = OSAL_NULL;
   tU32 u32BytesToRead = strlen( CFS_WRITE_TEST_DATA );
   tS32 s32BytesRead = 0;
   tS32 s32BytesWritten = 0;
   tU32 u8BytesToWrite = strlen( CFS_WRITE_TEST_DATA );
   tCS8 pc8DataToWrite[] = CFS_WRITE_TEST_DATA;
   FSThreadParam *thread_param = OSAL_NULL;

   if ( ThParm == OSAL_NULL )
   {
      OSAL_vThreadExit();
   }
   else
   {
      thread_param = ( FSThreadParam * )ThParm;

      if ( ( ( hFile = OSAL_IOCreate ( ( tCString )thread_param->szDevName, enAccess ) )
             == OSAL_ERROR ) )
      {
         thread_param->u32Ret += 1;
      }
      else
      {
         /* Write data to file */
         s32BytesWritten = OSAL_s32IOWrite ( hFile, pc8DataToWrite,
                                             ( tU32 ) u8BytesToWrite );

         /* Check status of write */
         if ( s32BytesWritten == OSAL_ERROR )
         {
            thread_param->u32Ret = OSAL_u32ErrorCode();
            thread_param->u32Ret += 2;
         }

         /* Close the common file */
         if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
         {
            thread_param->u32Ret += 3;
         }
         else if ( thread_param->u32Ret == 0 )
         {
            hFile = OSAL_IOOpen ( ( tCString )thread_param->szDevName, enAccess );

            if ( hFile == OSAL_ERROR )
            {
               thread_param->u32Ret += 1;
            }
            else if ( ( ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( u32BytesToRead + 1 ) )
                      == OSAL_NULL )
            {
               thread_param->u32Ret += 2;
            }
            else if ( ( s32BytesRead = OSAL_s32IORead ( hFile, ps8ReadBuffer,
                                       u32BytesToRead ) ) == OSAL_ERROR )
            {
               thread_param->u32Ret += 3;
            }
            else if ( ( s32BytesRead !=  ( tS32 )u32BytesToRead ) )
            {
               thread_param->u32Ret += 4;
            }

            if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
            {
               thread_param->u32Ret += 10;
            }

            if ( ps8ReadBuffer == OSAL_NULL )
            {
               thread_param->u32Ret += 4;
            }
            else
            {
               OSAL_vMemoryFree( ps8ReadBuffer );
               ps8ReadBuffer = OSAL_NULL;
            }
         }

         if ( OSAL_s32IORemove( ( tCString )thread_param->szDevName ) )
         {
            thread_param->u32Ret += OSAL_u32ErrorCode();
         }
      }

      OSAL_s32EventPost( CFS_file_op, thread_param->mask,
                         OSAL_EN_EVENTMASK_OR );
      /*Exit thread*/
      OSAL_vThreadExit();
   }
}


/*****************************************************************************
* FUNCTION     :  u32FSFileAccInDiffThread()
* PARAMETER    :  dev_name: Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_106
* DESCRIPTION  :  Complete set of file opearion in different threads
* HISTORY      :  Created by Sriranjan U (RBEI/ECF1) on 20 Jul, 2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
******************************************************************************/
tU32 u32FSFileAccInDiffThread( const tS8 * dev_name )
{
   OSAL_trThreadAttribute rThAttr1 = {0};
   OSAL_trThreadAttribute rThAttr2 = {0};
   OSAL_trThreadAttribute rThAttr3 = {0};
   OSAL_trThreadAttribute rThAttr4 = {0};
   OSAL_trThreadAttribute rThAttr5 = {0};
   tU32 u32Ret = 0;
   tChar szFilePath1 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath2 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath3 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath4 [FILE_NAME_MAX_LEN] = {0};
   tChar szFilePath5 [FILE_NAME_MAX_LEN] = {0};
   FSThreadParam ParamTh1 = {0};
   FSThreadParam ParamTh2 = {0};
   FSThreadParam ParamTh3 = {0};
   FSThreadParam ParamTh4 = {0};
   FSThreadParam ParamTh5 = {0};

   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   ( tVoid )OSAL_szStringCopy( szFilePath1, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath1, "/NEWSUB530.abc" );
   ParamTh1.szDevName = szFilePath1;
   ParamTh1.mask = CFS_THREAD_1_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath2, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath2, "/NEWSUB540.def" );
   ParamTh2.szDevName = szFilePath2;
   ParamTh2.mask = CFS_THREAD_2_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath3, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath3, "/File100.txt" );
   ParamTh3.szDevName = szFilePath3;
   ParamTh3.mask = CFS_THREAD_3_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath4, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath4, "/File300.jpg" );
   ParamTh4.szDevName = szFilePath4;
   ParamTh4.mask = CFS_THREAD_4_MASK;
   ( tVoid )OSAL_szStringCopy( szFilePath5, dev_name );
   ( tVoid )OSAL_szStringConcat( szFilePath5, "/File400.mp3" );
   ParamTh5.szDevName = szFilePath5;
   ParamTh5.mask = CFS_THREAD_5_MASK;
   /*thread stuff*/
   rThAttr1.szName  = "ThFileOp1";
   rThAttr1.pfEntry = ( OSAL_tpfThreadEntry )vFSFileReadWriteThread;
   rThAttr1.pvArg   = &ParamTh1;
   rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr2.szName  = "ThFileOp2";
   rThAttr2.pfEntry = ( OSAL_tpfThreadEntry )vFSFileReadWriteThread;
   rThAttr2.pvArg   = &ParamTh2;
   rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr3.szName  = "ThFileOp3";
   rThAttr3.pfEntry = ( OSAL_tpfThreadEntry )vFSFileReadWriteThread;
   rThAttr3.pvArg   = &ParamTh3;
   rThAttr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr3.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr4.szName  = "ThFileOp4";
   rThAttr4.pfEntry = ( OSAL_tpfThreadEntry )vFSFileReadWriteThread;
   rThAttr4.pvArg   = &ParamTh4;
   rThAttr4.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr4.s32StackSize = FS_THREAD_STACK_SIZE;
   rThAttr5.szName  = "ThFileOp5";
   rThAttr5.pfEntry = ( OSAL_tpfThreadEntry )vFSFileReadWriteThread;
   rThAttr5.pvArg   = &ParamTh5;
   rThAttr5.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr5.s32StackSize = FS_THREAD_STACK_SIZE;

   if ( OSAL_OK == OSAL_s32EventCreate( "FileOp", &CFS_file_op ) )
   {
      /*create and activate thread1 */
      if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr1 ) )
      {
         u32Ret += 1;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr2 ) )
      {
         u32Ret += 2;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr3 ) )
      {
         u32Ret += 3;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr4 ) )
      {
         u32Ret += 4;
      }
      else if ( OSAL_ERROR == OSAL_ThreadSpawn( &rThAttr5 ) )
      {
         u32Ret += 5;
      }

      //wait for 2 events signalling the end of read operation
      if ( OSAL_OK != OSAL_s32EventWait( CFS_file_op, 0x0011111,
                                         OSAL_EN_EVENTMASK_AND, FS_EVENT_WAIT_TIMEOUT,
                                         &ResultMask ) )
      {
         u32Ret += 6;
      }

      /* Clear the result mask*/
      OSAL_s32EventPost( CFS_file_op, ~ResultMask,
                         OSAL_EN_EVENTMASK_AND );

      //close the event
      if ( OSAL_ERROR == OSAL_s32EventClose( CFS_file_op ) )
      {
         u32Ret += 7;
      }
      //delete the event
      else if ( OSAL_ERROR == OSAL_s32EventDelete( "FileOp" ) )
      {
         u32Ret += 8;
      }
   }
   else
   {
      u32Ret += 8;
   }

   return u32Ret + ( ParamTh1.u32Ret * 1 ) + ( ParamTh2.u32Ret * 2 )
          + ( ParamTh3.u32Ret * 3 ) + ( ParamTh4.u32Ret * 4 )
          + ( ParamTh5.u32Ret * 5 );
}

/*****************************************************************************
* FUNCTION     :  u32FSCreateTestDataFile( )
* PARAMETER    :  ps8dev_name: device name
                  DirPath: Directory path.
						s32FileCount: file count.
						s32dircnt   : Directory count.
						ps8File_name: file name.

* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_0
* DESCRIPTION  :  Open /close File
* HISTORY:	  Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FSCreateTestDataFile( const tS8* ps8dev_name, const tS8* DirPath, tS32 s32FileCount, const tS8* ps8File_name )
{
   OSAL_tIODescriptor hFD                          = 0;
   OSAL_tIODescriptor hFile                        = 0;
   OSAL_tenAccess enAccess                         = OSAL_EN_READWRITE;
   OSAL_trIOCtrlDirent rDirent                     = {""};
   OSAL_tMSecond StartTime                         = 0;
   OSAL_tMSecond TimetakenToOpen                   = 0;
   OSAL_tMSecond EndTime                           = 0;
   tU32 u32Ret                                     = 0;
   tU8 * DataToWrite                               = OSAL_NULL;
   tU8 * DataToWrite500B                           = OSAL_NULL;
   tU8 * DataToWrite5KB                            = OSAL_NULL;
   tU8 * DataToWrite10KB                           = OSAL_NULL;
	tS32 s32FileNum                                 = OEDT_FS_BASE_COUNT;
   tU32 u32BytesToWrite                            = 500;
   tS32	s32BytesWritten                           = 0;
	tU8 u32FilePathName[OEDT_FFS1_MAX_FILE_PATH_LEN]={0};
   
   if ( ps8dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }
	/*Allocate the data buffer write */
	DataToWrite500B = OSAL_pvMemoryAllocate(500);
	DataToWrite5KB = OSAL_pvMemoryAllocate(5*1024);
	DataToWrite10KB = OSAL_pvMemoryAllocate(10*1024);

	if( ( OSAL_NULL == DataToWrite500B ) && ( OSAL_NULL == DataToWrite5KB )&& ( OSAL_NULL == DataToWrite10KB ) )
	{
      u32Ret = 10000;
      return u32Ret;
	}
   DataToWrite = DataToWrite500B;
   /* Open device in readwrite mode */
   hFD = OSAL_IOOpen ( ( tCString )ps8dev_name, enAccess );

   if ( hFD == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      /* Copy sub-directory name to structure variable */
      ( tVoid )OSAL_szStringCopy ( ( tString )( rDirent.s8Name ),
                                   ( tCString )( DirPath ) );

      OEDT_HelperPrintf
				(
				   TR_LEVEL_USER_1, 
				   "rDirent.s8Name: %s   ",
					 rDirent.s8Name
				);

      /* Create sub-directory */
      if ( OSAL_s32IOControl ( hFD, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 3;
			 OEDT_HelperPrintf
				(
				   TR_LEVEL_ERRORS, 
				   "OSAL_C_S32_IOCTRL_FIOMKDIR failed "
				);

      }
      else
      {
		 if (DataToWrite != OSAL_NULL) 
		 {
			/*Populate the buffer with Random data*/
			( tVoid ) OSAL_pvMemorySet( DataToWrite, 'c', u32BytesToWrite );
		 }		 
         for( ; s32FileNum < OEDT_FS_BASE_COUNT + s32FileCount ; s32FileNum++ )
         {  
             if( s32FileNum == OEDT_FS_BASE_COUNT + 500)
				 {
                 DataToWrite = DataToWrite5KB;
				     u32BytesToWrite = 5*1024;
				 }
             if( s32FileNum == OEDT_FS_BASE_COUNT + 1000)
				 {
                 DataToWrite = DataToWrite10KB;
				     u32BytesToWrite = 10*1024;
				 }
             ( tVoid ) OSAL_pvMemorySet( u32FilePathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
             /*Populate the file path*/
             OSAL_s32PrintFormat
             (
                u32FilePathName,
                "%s%s%s%s%d%s",
                ps8dev_name,
                DirPath,
                "/",
                ps8File_name,
                s32FileNum,
                OEDT_FILE_TYPE
             );
				  OEDT_HelperPrintf
				(
				   TR_LEVEL_USER_4, 
				   "create file with file size %d \n",
				   u32BytesToWrite
				);
			   StartTime = 0;
			   EndTime = 0;
			   TimetakenToOpen = 0;
            StartTime = OSAL_ClockGetElapsedTime();
		      /*create file*/
				hFile = ( OSAL_IOCreate ( ( tCString )u32FilePathName, enAccess ) );

            EndTime = OSAL_ClockGetElapsedTime();
            if ( hFile  == OSAL_ERROR )
            {
               u32Ret += 10;
            }
   	      else
		      {
               
               /*Wrapping?*/
               if ( EndTime > StartTime )
               {
                  TimetakenToOpen =   EndTime - StartTime;
               }
               else
               {
                  TimetakenToOpen = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
               }
			      /*Trace the time take for open the file*/
			      OEDT_HelperPrintf
			      (
			         TR_LEVEL_USER_2, 
			         "Creat_Time %s %lu ms  ",
                  u32FilePathName,
                  TimetakenToOpen
               );

               
               if (u32BytesToWrite)
		         {
                 	/*write the data to file*/
                  s32BytesWritten = OSAL_s32IOWrite ( hFile,(tPCS8) DataToWrite, u32BytesToWrite );
                  if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) ||
                  ( s32BytesWritten != ( tS32 )u32BytesToWrite )  )
                  {
                     u32Ret += 30;
							OEDT_HelperPrintf
				         (
				            TR_LEVEL_ERRORS, 
				            "OSAL_s32IOWrite failed "
				         );

                  }
					}//if (u8BytesToWrite)
		      	/*close the file*/
               if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
               {
                  u32Ret += 20;
               }

			   }// if ( ( hFile = ( OSAL_IOCreate())- else

			} //for loop end
         
		}//if-else  OSAL_C_S32_IOCTRL_FIOMKDIR  
		   
		/* Close device */
      if ( OSAL_s32IOClose ( hFD ) == OSAL_ERROR )
      {
         u32Ret += 11;
      }
   }
      //free the memory allocated
      if ( OSAL_NULL != DataToWrite500B )
         OSAL_vMemoryFree( DataToWrite500B );
      if ( OSAL_NULL != DataToWrite5KB )
         OSAL_vMemoryFree( DataToWrite5KB );
      if ( OSAL_NULL != DataToWrite10KB )
         OSAL_vMemoryFree( DataToWrite10KB );
   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :  u32FSRemoveTestDataFile( )
* PARAMETER    :  ps8dev_name: device name

* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_0
* DESCRIPTION  :  remove the data set
* HISTORY:	  Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FSRemoveTestDataFile( const tS8* ps8dev_name )
{
   OSAL_tIODescriptor hFD                          = 0;
   tU32 u32Ret                                     = 0;
   OSAL_tenAccess enAccess                         = OSAL_EN_READWRITE;
   if ( ps8dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }
   /* Open device in readwrite mode */
   hFD = OSAL_IOOpen ( ( tCString )ps8dev_name, enAccess );

   if ( hFD == ( OSAL_tIODescriptor )OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
		/*remove test data*/   
      if ( OSAL_ERROR == OSAL_s32IOControl
                         (
                            hFD, 
                            OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
                            ( tS32 ) OEDT_DIR_NAME 
                          ) 
         )
      {
         u32Ret += 131;
		}
		/* Close device */
      if ( OSAL_s32IOClose ( hFD ) == OSAL_ERROR )
      {
         u32Ret += 101;
      }
   }
	  
   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSFileOpenCloseMultipleTime( )
* PARAMETER    :  ps8dev_name: device name
                  bCreateRemove: if file create is required TRUE,else FALSE
						s32FileCount: file count.
						s32dircnt   : Directory count.
						ps8File_name: file name.

* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_0
* DESCRIPTION  :  Test case is used to measure the time taken for open the file. 
        This test has the test data of s32FileCount files with file name length x and 
        opens these files accenting order.
* HISTORY:	  Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FSFileOpenCloseMultipleTime( const tS8* ps8dev_name, tBool bCreateRemove,tS32 s32FileCount,tS32 s32dircnt, const tS8* ps8File_name  )
{
   OSAL_tIODescriptor hFile                        = 0;
   OSAL_tMSecond StartTime                         = 0;
   OSAL_tMSecond TimetakenToOpen                   = 0;
   OSAL_tMSecond EndTime                           = 0;
   tU32 u32Ret                                     = 0;
	tS32 s32FileNum                                 = OEDT_FS_BASE_COUNT;
	tS32 s32DirNum                                  = 0;
   OSAL_tenAccess enAccess                         = OSAL_EN_READWRITE;
	tU8 u32FilePathName[OEDT_FFS1_MAX_FILE_PATH_LEN]={0};
	tU8 u32DirPathName[OEDT_FFS1_MAX_FILE_PATH_LEN] ={0};
   (tVoid)bCreateRemove;

   if ( ps8dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }
   /*Create the Test data set*/
	( tVoid )OSAL_szStringCopy( u32DirPathName,ps8dev_name);

   for( ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
    	/*create the test data*/
    	u32Ret = u32FSCreateTestDataFile( (tS8*)u32DirPathName, (tS8*)OEDT_DIR_NAME, s32FileCount, ps8File_name );
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
		
      if(! u32Ret )
      {
				OEDT_HelperPrintf
				(
				   TR_LEVEL_USER_4, 
				   "Open_Time %s is done  ",
					 u32DirPathName
				);
      } 
   }  
   ( tVoid ) OSAL_pvMemorySet( u32DirPathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
	( tVoid )OSAL_szStringCopy( u32DirPathName, ps8dev_name);

   for(s32DirNum = 0 ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
      
      for( s32FileNum = OEDT_FS_BASE_COUNT; s32FileNum < OEDT_FS_BASE_COUNT + s32FileCount ; s32FileNum++ )
      {  
   
       //  if( ( 1 == s32DirNum ) && (
         ( tVoid ) OSAL_pvMemorySet( u32FilePathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
         /*Populate the file path*/
         OSAL_s32PrintFormat
         (
            u32FilePathName,
            "%s%s%s%d%s",
            u32DirPathName,
            "/",
            ps8File_name,
            s32FileNum,
            OEDT_FILE_TYPE
         );
			StartTime = 0;
			EndTime = 0;
			TimetakenToOpen = 0;
         StartTime = OSAL_ClockGetElapsedTime();
         /*  Opening The File*/
         hFile = OSAL_IOOpen ( ( tCString )u32FilePathName, enAccess );
         EndTime = OSAL_ClockGetElapsedTime();
         if ( OSAL_ERROR == hFile )
         {
           u32Ret += 100;
         }
         else
         {
            /*measure the time take to open the file*/
            if ( EndTime > StartTime )
            {
               TimetakenToOpen =   EndTime - StartTime;
            }
            else
            {
               TimetakenToOpen = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }
            /*trace out  the time take to open the file*/
			   OEDT_HelperPrintf
			   (
			      TR_LEVEL_USER_1, 
			      "Open_Time %s %lu ms  ",
               u32FilePathName,
               TimetakenToOpen
             );
            /* close the second handle of the file */
            if ( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
            {
               u32Ret += 300;
			    OEDT_HelperPrintf
			   (
			      TR_LEVEL_ERRORS, 
			      "OSAL_s32IOClose faile!!!\n "
             );

            }
         }//if-else ( OSAL_ERROR == hFile )

		}//for loop for file count
   }//for loop for Dir count
  	/*remove the test data set*/
  	u32FSRemoveTestDataFile(ps8dev_name);
	return u32Ret;
}
/*****************************************************************************
* FUNCTION     :  u32FSFileOpenCloseMultipleTimeRandom1( )
* PARAMETER    :  ps8dev_name: device name
                  bCreateRemove: if file create is required TRUE,else FALSE
						s32FileCount: file count.
						s32dircnt   : Directory count.
						ps8File_name: file name.

* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_0
* DESCRIPTION  :  Test case is used to measure the time taken for open the file. 
        This test has the test data of s32FileCount files with file name length x and 
        opens these files accenting order.
* HISTORY:	  Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FSFileOpenCloseMultipleTimeRandom1( const tS8* ps8dev_name, tBool bCreateRemove,tS32 s32FileCount,tS32 s32dircnt,const tS8* ps8File_name )
{
   OSAL_tIODescriptor hFile                        = 0;
   OSAL_tMSecond StartTime                         = 0;
   OSAL_tMSecond TimetakenToOpen                   = 0;
   OSAL_tMSecond EndTime                           = 0;
   tU32 u32Ret                                     = 0;
	tS32 s32FileNum                                 = OEDT_FS_BASE_COUNT;
	tS32 s32FileNumStart                            = OEDT_FS_BASE_COUNT;
	tS32 s32FileNumEnd                              = OEDT_FS_BASE_COUNT + s32FileCount;
	tS32 s32DirNum                                  = 0;
   OSAL_tenAccess enAccess                         = OSAL_EN_READWRITE;
	tU8 u32FilePathName[OEDT_FFS1_MAX_FILE_PATH_LEN]={0};
	tU8 u32DirPathName[OEDT_FFS1_MAX_FILE_PATH_LEN] ={0};
	(tVoid)bCreateRemove;
   if ( ps8dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   /*Create the Test data set*/
	( tVoid )OSAL_szStringCopy( u32DirPathName,ps8dev_name);

   for( ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
    	/*create the test data set*/
    	u32Ret = u32FSCreateTestDataFile( (tS8*)u32DirPathName, (tS8*)OEDT_DIR_NAME, s32FileCount, ps8File_name );
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
		
      if(! u32Ret )
      {
				OEDT_HelperPrintf
				(
				   TR_LEVEL_USER_4, 
				   "Open_Time %s is done  ",
					 u32DirPathName
				);
      } 
   }  
   ( tVoid ) OSAL_pvMemorySet( u32DirPathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
	( tVoid )OSAL_szStringCopy( u32DirPathName, ps8dev_name);


       
   for(s32DirNum = 0 ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
       
      for(s32FileNum = OEDT_FS_BASE_COUNT ; s32FileNum < OEDT_FS_BASE_COUNT + s32FileCount ; s32FileNum++ )
      {  
   
         ( tVoid ) OSAL_pvMemorySet( u32FilePathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
         /*Populate the file path
         if( 0 == (s32FileNum % 2) ) is used to make open random*/
        if( 0 == (s32FileNum % 2) )
		  {
        
            
            OSAL_s32PrintFormat
            (
               u32FilePathName,
               "%s%s%s%d%s",
               u32DirPathName,
               "/",
               ps8File_name,
               s32FileNumStart,
               OEDT_FILE_TYPE
            );
				s32FileNumStart++;
			}
			else
			{
				s32FileNumEnd--;
            OSAL_s32PrintFormat
            (
               u32FilePathName,
               "%s%s%s%d%s",
               u32DirPathName,
               "/",
               ps8File_name,
               s32FileNumEnd,
               OEDT_FILE_TYPE
            );
			}
			StartTime = 0;
			EndTime = 0;
			TimetakenToOpen = 0;
         
         /*  Opening The File*/
			OEDT_HelperPrintf
			   (
			      TR_LEVEL_USER_4, 
			      "Open_file_name %s \n  ",
               u32FilePathName
               
             );
        StartTime = OSAL_ClockGetElapsedTime();
        hFile = OSAL_IOOpen ( ( tCString )u32FilePathName, enAccess );
         EndTime = OSAL_ClockGetElapsedTime();
         if ( OSAL_ERROR == hFile )
         {
			   OEDT_HelperPrintf
			   (
			      TR_LEVEL_ERRORS, 
			      "Open_file_failed %s \n  ",
               u32FilePathName
               
             );
           
           u32Ret += 100;
         }
         else
         {
            /*measure the time take to open the file*/
            if ( EndTime > StartTime )
            {
               TimetakenToOpen =   EndTime - StartTime;
            }
            else
            {
               TimetakenToOpen = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }
            /*Trace out the time take to open the file*/
			   OEDT_HelperPrintf
			   (
			      TR_LEVEL_USER_1, 
			      "Open_Time %s %lu ms  ",
               u32FilePathName,
               TimetakenToOpen
             );
            /* close the second handle of the file */
            if ( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
            {
               u32Ret += 300;

            }
         }//if-else ( OSAL_ERROR == hFile )
		}//for loop for file count
   }//for loop for Dir count

   /*remove the test data*/
	u32FSRemoveTestDataFile(ps8dev_name);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSFileOpenCloseMultipleTimeRandom2( )
* PARAMETER    :  ps8dev_name: device name
                  bCreateRemove: if file create is required TRUE,else FALSE
						s32FileCount: file count.
						s32dircnt   : Directory count.
						ps8File_name: file name.

* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_0
* DESCRIPTION  :  Test case is used to measure the time taken for open the file. 
        This test has the test data of s32FileCount files with file name length x and 
        opens these files accenting order.
* HISTORY:	  Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FSFileOpenCloseMultipleTimeRandom2( const tS8* ps8dev_name, tBool bCreateRemove,tS32 s32FileCount,tS32 s32dircnt, const tS8* ps8File_name  )
{
   OSAL_tIODescriptor hFile                        = 0;
   OSAL_tMSecond StartTime                         = 0;
   OSAL_tMSecond TimetakenToOpen                   = 0;
   OSAL_tMSecond EndTime                           = 0;
   tU32 u32Ret                                     = 0;
	tS32 s32FileNum                                 = OEDT_FS_BASE_COUNT;
	tS32 s32FileNumStart                            = OEDT_FS_BASE_COUNT;
	tS32 s32FileNumEnd                              = OEDT_FS_BASE_COUNT + (s32FileCount/2);
	tS32 s32DirNum                                  = 0;
   OSAL_tenAccess enAccess                         = OSAL_EN_READWRITE;
	tU8 u32FilePathName[OEDT_FFS1_MAX_FILE_PATH_LEN]={0};
	tU8 u32DirPathName[OEDT_FFS1_MAX_FILE_PATH_LEN] ={0};
   (tVoid)bCreateRemove;

   if ( ps8dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }
	( tVoid )OSAL_szStringCopy( u32DirPathName,ps8dev_name);

   for( ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
      /*Create the Test data set*/
    	u32Ret = u32FSCreateTestDataFile( (tS8*)u32DirPathName, (tS8*)OEDT_DIR_NAME, s32FileCount, ps8File_name );
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
		
      if(! u32Ret )
      {
				OEDT_HelperPrintf
				(
				   TR_LEVEL_USER_4, 
				   "Open_Time %s is done  ",
					 u32DirPathName
				);
      } 
   }  
   ( tVoid ) OSAL_pvMemorySet( u32DirPathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
	( tVoid )OSAL_szStringCopy( u32DirPathName, ps8dev_name);


       
   for(s32DirNum = 0 ; s32DirNum < s32dircnt ; s32DirNum++ )
   { 
      ( tVoid )OSAL_szStringConcat( u32DirPathName,OEDT_DIR_NAME); 
      for( s32FileNum = OEDT_FS_BASE_COUNT; s32FileNum < OEDT_FS_BASE_COUNT + s32FileCount ; s32FileNum++ )
      {  
   
         ( tVoid ) OSAL_pvMemorySet( u32FilePathName , 0 , OEDT_FFS1_MAX_FILE_PATH_LEN );
         /*Populate the file path
         if( 0 == (s32FileNum % 2) ) is used to make open random*/
        if( 0 == (s32FileNum % 2) )
		  {
        
            OSAL_s32PrintFormat
            (
               u32FilePathName,
               "%s%s%s%d%s",
               u32DirPathName,
               "/",
               ps8File_name,
               s32FileNumStart,
               OEDT_FILE_TYPE
            );
				s32FileNumStart++;

			}
			else
			{

            OSAL_s32PrintFormat
            (
               u32FilePathName,
               "%s%s%s%d%s",
               u32DirPathName,
               "/",
               ps8File_name,
               s32FileNumEnd,
               OEDT_FILE_TYPE
            );
				s32FileNumEnd++;
			}
			StartTime = 0;
			EndTime = 0;
			TimetakenToOpen = 0;
         StartTime = OSAL_ClockGetElapsedTime();
         /*  Opening The File*/
         hFile = OSAL_IOOpen ( ( tCString )u32FilePathName, enAccess );
         EndTime = OSAL_ClockGetElapsedTime();
         if ( OSAL_ERROR == hFile )
         {
			    OEDT_HelperPrintf
			   (
			      TR_LEVEL_ERRORS, 
			      "Open_file_name  %s failed!!!\n  ",
               u32FilePathName
               
             );
           u32Ret += 100;
         }
         else
         {
           
            /*measure the time take to open the file*/
            if ( EndTime > StartTime )
            {
               TimetakenToOpen =   EndTime - StartTime;
            }
            else
            {
               TimetakenToOpen = ( 0xFFFFFFFF - StartTime ) + EndTime + 1;
            }
            /*Tarce out the time take to open the file*/
			   OEDT_HelperPrintf
			   (
			      TR_LEVEL_USER_1, 
			      "Open_Time %s %lu ms  ",
               u32FilePathName,
               TimetakenToOpen
             );

            /* close the second handle of the file */
            if ( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
            {
               u32Ret += 300;

            }
         }//if-else ( OSAL_ERROR == hFile )
		}//for loop for file count
   }//for loop for Dir count
   /*remove the test data*/
	u32FSRemoveTestDataFile(ps8dev_name);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSCopyDirTest( )
* PARAMETER    :  dev_name: device name and file names
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Copy the files in source directory form one file system 
                   to other file system
* HISTORY      :  Created by anc2hi 10 May, 2010
					updated by Vijay Hiremath (RBEI/ECF5) on 16 Apr 2015
******************************************************************************/
tU32 u32FSCopyDirTest( const tPS8* dev_name, tBool bCreateRemove )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hFile = 0;
   tU32 u32Ret = 0;
   tChar* arrFileName_tmp[2] = {OSAL_NULL};
   tChar arrFileName[256] = {0};
   tChar arrDir1Name[256] = {0};
   tChar arrDir2Name[256] = {0};
   OSAL_trIOCtrlDirent rDirent = {""};
   if ( dev_name == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   hDevice = OSAL_IOOpen( ( tCString )dev_name[0], enAccess );

   if ( OSAL_ERROR == hDevice )
   {
      u32Ret += 1;
   }
   else if ( bCreateRemove )
   {
      /* directory name to structure variable */
      ( tVoid )OSAL_szStringCopy ( ( tString )rDirent.s8Name ,
                                   ( tCString )SUBDIR_NAME );
      /* Create directory */
      if ( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret += 2;
         OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_FIOMKDIR) fails with error '%i'", OSAL_u32ErrorCode() );
      }
      else
      {
         /*Create the first file in source directory */
			OSAL_s32PrintFormat
			(
			   ( tChar* )arrFileName,"%s%s%s",
			   ( tCString )dev_name[0],
			   "/"SUBDIR_NAME,
			   OEDT_FS_TestFile
			);
         hFile = OSAL_IOCreate( arrFileName , enAccess );
			if ( hFile == OSAL_ERROR )
         {
            u32Ret += 4;
            OSALUTIL_s32RemoveDir( hDevice, DIR1 );
            OSAL_s32IOClose( hDevice );
         }
         else
         {
            OSAL_s32PrintFormat
			   (
			      ( tChar* )arrDir1Name,"%s%s",
			      ( tCString )dev_name[0],
			      "/"SUBDIR_NAME
			   );
            OSAL_s32PrintFormat
			   (
			      ( tChar* )arrDir2Name,"%s%s",
			      ( tCString )dev_name[1],
			      "/"SUBDIR_NAME
			   );

            arrFileName_tmp[0] = arrDir1Name;
            arrFileName_tmp[1] = arrDir2Name;

            if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIOCOPYDIR,
                                                        ( tS32 )arrFileName_tmp ) )
            {
                     u32Ret += 6;
                     OSAL_s32IOClose( hFile );
                     OSAL_s32IORemove( arrFileName );
                     OSALUTIL_s32RemoveDir( hDevice, ( tCString )dev_name[1] );
                     OSAL_s32IOClose( hDevice );
            }
            else if ( OSAL_ERROR == OSAL_s32IOClose( hFile ) ) /*Close the second file in source directory*/
            {
               u32Ret += 8;
               OSAL_s32IOClose( hDevice );
            }
            else if ( OSAL_ERROR ==  OSAL_s32IORemove( arrFileName ) )	/*Remove the first file in the source directory*/
            {
               u32Ret += 20;
               OSAL_s32IOClose( hDevice );
            }
				else if ( OSAL_ERROR ==  OSALUTIL_s32RemoveDir( hDevice, SUBDIR_NAME ) )
            {
               u32Ret += 30;
               OSAL_s32IOClose( hDevice );
            }
			}
		}
      if ( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
      {
         u32Ret += 80;
      }
	}
	else
   {
      OSAL_s32PrintFormat
      (
         ( tChar* )arrDir2Name,"%s%s",
         ( tCString )dev_name[1],
         "/"SUBDIR_NAME
      );
      arrFileName_tmp[0] = (tChar* )dev_name[0];
      arrFileName_tmp[1] = arrDir2Name;

      if ( OSAL_ERROR == OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIOCOPYDIR,
                                ( tS32 )arrFileName_tmp ) )
      {      
         u32Ret += 1000;
         OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_FIOCOPYDIR) fails with error '%i'", OSAL_u32ErrorCode() );
      }
      if ( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
      {
         u32Ret += 2000;
      }

   }

   memset(arrFileName,0,sizeof(arrFileName));
   if(bCreateRemove)
   {
      OSAL_s32PrintFormat
	   (
	      ( tChar* )arrFileName,"%s%s%s",
	      ( tCString )dev_name[1],
	      "/"SUBDIR_NAME,
	      OEDT_FS_TestFile
	   );
	}
	else
   {
      OSAL_s32PrintFormat
	   (
	      ( tChar* )arrFileName,"%s%s%s",
	      ( tCString )dev_name[1],
	      "/"SUBDIR_NAME,
	      OEDT_CD_COPY_DIR_TEST_FILE
	   );
	}
	hFile = 0;
   hFile = OSAL_IOOpen( arrFileName, enAccess );
   if ( OSAL_ERROR == hFile )
   {
      u32Ret += 100;
      OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_IOOpen() fails with error '%i'", OSAL_u32ErrorCode() );
   }
   else if ( OSAL_ERROR == OSAL_s32IOClose( hFile ) )
   {
      u32Ret += 200;
      OSAL_s32IOClose( hFile );
   }
   hDevice = OSAL_IOOpen( ( tCString )dev_name[1], enAccess );
   if ( OSAL_ERROR == hFile )
   {
      u32Ret += 500;
      OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_IOOpen() fails with error '%i'", OSAL_u32ErrorCode() );
   }
   else 
   {
	   OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
                                  ( tS32 )SUBDIR_NAME );
      OSAL_s32IOClose( hDevice ) ;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSReadDirExtPartByPart()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FileSystem_090
* DESCRIPTION  :  Read file/directory contents
* HISTORY      :  Created by anc3kor 2.03.11
*                 Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported from Gen1
******************************************************************************/
tU32 u32FSReadDirExtPartByPart( const tS8 * strDevName )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tIODescriptor hFile1 = 0;
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExtDir s32Stat = {0};
   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW";
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};
   OSAL_trIOCtrlExtDirent Dirent[20];
   tU32 u32count = 0;
   memset( Dirent, 0, sizeof(Dirent) );
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;

   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }
   s32Stat.pDirent = Dirent;
   OSAL_pvMemorySet( temp_ch, '\0', sizeof( temp_ch ) );
   ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );
   /*open the device*/
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile != OSAL_ERROR )
   {

      /*create the Directory*/
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath1 );
         /*open the created directory*/
           hFile1 = OSAL_IOOpen ( ( tCString )temp_ch, OSAL_EN_ACCESS_DIR );

         /*create the files in opened directory*/
         if (OSAL_OK != u32CreateTestFiles( temp_ch ))
         {
            u32Ret = 4;
         }
         else
         {
            s32Stat.u32NbrOfEntries = OEDT_1ST_ENTRY_READ;
            /*read the first 5 entry*/
               if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 10;
            }
               else
               {

                   /*Trace out the read entry*/
                  for ( u32count = 0; u32count < s32Stat.u32NbrOfEntries;u32count++)
                  {
                     OEDT_HelperPrintf(TR_LEVEL_FATAL,"file read from 0 to 4 : %s",(s32Stat.pDirent[ u32count ]).s8Name);
                  }



               }
            s32Stat.u32NbrOfEntries = OEDT_2ND_ENTRY_READ;
            /*read the next 7 entry*/
               if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 10;
            }
               else
               {
                  /*Trace out the read entry*/
                   for ( u32count = 0; u32count < s32Stat.u32NbrOfEntries;u32count++)
                  {
                     OEDT_HelperPrintf(TR_LEVEL_FATAL,"file read from 5 to 11 : %s",(s32Stat.pDirent[ u32count ]).s8Name );
                  }

               }
               /*close the directory*/
               if ( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
            /*remove the files in the directory*/
            vRemoveTestFiles(temp_ch);
         }

         /*remove the directory*/
           if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }
      }
      /*close the Device*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32FSReadDirExt2PartByPart()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FileSystem_091
* DESCRIPTION  :  Read file/directory contents with 2 IOCTL
* HISTORY      :  Created by anc3kor 2.03.11
*                 Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported from Gen1
******************************************************************************/
tU32 u32FSReadDirExt2PartByPart( const tS8 * strDevName )
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0;
   OSAL_tIODescriptor hFile1 = 0;
   tU32 u32Ret = 0;
   tU32 u32count = 0;
   OSAL_trIOCtrlExt2Dir s32Stat = {0};
   OSAL_trIOCtrlExt2Dirent Dirent[20];

   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "/MYNEW";
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;


   if ( strDevName == OSAL_NULL )
   {
      u32Ret = 9000;
      return u32Ret;
   }

   s32Stat.pDirent = Dirent;
   OSAL_pvMemorySet( temp_ch, '\0', sizeof( temp_ch ) );
   ( tVoid )OSAL_szStringCopy ( rDirent.s8Name, szSubdirName );
   /*open the device*/
   hFile = OSAL_IOOpen ( ( tCString )strDevName, enAccess );

   if ( hFile != OSAL_ERROR )
   {
      /*create the directory*/
      if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOMKDIR,
                               ( tS32 )&rDirent ) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      else
      {
         ( tVoid )OSAL_szStringCopy ( ( tString )( temp_ch ), ( tCString )( strDevName ) );
         ( tVoid )OSAL_szStringConcat( temp_ch, szFilePath1 );
         /*Open the directory*/
           hFile1 = OSAL_IOOpen ( ( tCString ) temp_ch, OSAL_EN_ACCESS_DIR );
         /*Create the files */
         if ( OSAL_OK != u32CreateTestFiles( temp_ch ))
         {
            u32Ret = 4;
         }
         else
         {
            memset(Dirent, 0, sizeof(Dirent));
            s32Stat.u32NbrOfEntries = OEDT_1ST_EXT_ENTRY_READ;
            /*Read the first 6 entry*/
               if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 10;
            }
            else
               {
               /*Trace the read entry*/
                  for ( u32count = 0; u32count < s32Stat.u32NbrOfEntries;u32count++)
                  {
                     OEDT_HelperPrintf(TR_LEVEL_FATAL,"file read from 0 to 5 : %s",(s32Stat.pDirent[ u32count ]).s8Name);
                  }

               }
               memset(Dirent, 0, sizeof(Dirent));
               s32Stat.u32NbrOfEntries = OEDT_2ST_EXT_ENTRY_READ;
               /*read the next 9 entry*/
               if ( OSAL_s32IOControl( hFile1, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, ( tS32 )&s32Stat ) == OSAL_ERROR )
            {
               u32Ret += 100;
            }
            else
               {
                  /*Trace out the read entry*/
                  for ( u32count = 0; u32count < s32Stat.u32NbrOfEntries;u32count++)
                  {
                     OEDT_HelperPrintf(TR_LEVEL_FATAL,"file read from 6 to 14 : %s",(s32Stat.pDirent[ u32count ]).s8Name);
                  }

               }
            /*close the directory*/
               if ( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
            {
               u32Ret += 20;
            }
            /*remove the files in the directory*/
               vRemoveTestFiles(temp_ch);
         }

         /*remove the directory*/
            if ( OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIORMDIR,
                                  ( tS32 )&rDirent ) == OSAL_ERROR )
         {
            u32Ret += 200;
         }
      }
      /*close the Device*/
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :  u32CreateTestFiles()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  --
* DESCRIPTION  :  create 20 test file
* HISTORY      :  Created by anc3kor 2.03.11
*                 Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported from Gen1
******************************************************************************/
static tU32 u32CreateTestFiles( tChar * szDirpath )
{
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "";
   tS32 s32count = 0;
   tU32 u32ret = OSAL_OK;
   OSAL_tIODescriptor hFile;
   for(s32count = 0; s32count < 20; s32count++)
   {
      /*create the file path name*/
      OSAL_s32PrintFormat(szFilePath1,"%s%s%d%s",szDirpath,"/file",s32count,".txt");
      /*create the file*/
      if( OSAL_ERROR == ( hFile = OSAL_IOCreate ( ( tCString )szFilePath1, OSAL_EN_READWRITE ) ) )
      {
          u32ret++;
      }
      else
      {
         /*close the file*/
         OSAL_s32IOClose ( hFile ) ;
      }

   }
   return u32ret;
}

/*****************************************************************************
* FUNCTION     :  vRemoveTestFiles()
* PARAMETER    :  Device Name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  ---
* DESCRIPTION  :  delete 20 test file
* HISTORY      :  Created by anc3kor 2.03.11
*                 Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported from Gen1
******************************************************************************/
static tVoid vRemoveTestFiles( tChar * szDirpath )
{
   tChar szFilePath1 [OEDT_MAX_TOTAL_DIR_PATH_LEN + 1] = "";
   tS32 s32count = 0;
   for(s32count = 0; s32count < 20; s32count++)
   {
      /*create file path*/
      OSAL_s32PrintFormat(szFilePath1,"%s%s%d%s",szDirpath,"/file",s32count,".txt");
      /*remove the file*/
      OSAL_s32IORemove ( ( tCString )szFilePath1  );
   }
}

/*****************************************************************************
* FUNCTION      : vFSStressTest()
* DESCRIPTION   : Create 5 files with a size of 10 MBytes and remove them again.
* PARAMETER     : [IN]  strDevName - String of the device name.
*                 [OUT] pteststate - Pointer to the OEDT test state to store 
*                                    the result to.
* RETURNVALUE   : None.
* HISTORY       : 30.08.2012 - CM-AI/PJ-CF33-Kalms - Initial version.
******************************************************************************/
tVoid vFSStressTest(const tPS8 strDevName, OEDT_TESTSTATE* pteststate)
{
  OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() called.");

  OSAL_tIODescriptor hIODescriptor         = OSAL_ERROR;
  tU16               u16ActFileNumber      = 0;
  const tU16         cu16NumberOfFiles     = 5;
  tU16               u16NumberOfFileWrites = 0;
  OSAL_tMSecond      u32ElapsedTimeStartMs = 0;
  OSAL_tMSecond      u32ElapsedTimeEndMs   = 0;
  tFloat             fElapsedTimeSeconds   = 0.0;
  tChar              szFileWithPath[28]    = "\0"; // "/dev/ffs#/StressTest###.txt"+"\0" => 28 chars

  tU8                u8FileWriteBuffer[10000]; // 10 kB

  u32ElapsedTimeStartMs = OSAL_ClockGetElapsedTime();

  OEDT_EXPECT_DIFFERS(0, u32ElapsedTimeStartMs, pteststate);

  if (!pteststate->error_code)
  {
    OSAL_pvMemorySet(u8FileWriteBuffer, 0x000000FF, sizeof(u8FileWriteBuffer));

	/* --------------------------------------------------------------------------- */
	/* Create 5 files of 10 MByte size. Remove them before if they already exist.  */
	/* --------------------------------------------------------------------------- */

    for (u16ActFileNumber = 0; u16ActFileNumber < cu16NumberOfFiles; u16ActFileNumber++)
    {
      OEDT_EXPECT_DIFFERS(OSAL_ERROR, 
                          OSALUTIL_s32SaveNPrintFormat(szFileWithPath, 
                                                       sizeof(szFileWithPath), 
                                                       "%s%s%03u%s", 
                                                       strDevName, 
                                                       "/StressTest",
                                                       u16ActFileNumber,
                                                       ".txt"),
                          pteststate);

      if (!pteststate->error_code)
      {
        hIODescriptor = OSAL_IOCreate((tString)szFileWithPath, OSAL_EN_READWRITE);

        if (OSAL_ERROR == hIODescriptor)
        {
          OEDT_EXPECT_TRUE(OSAL_u32ErrorCode() == OSAL_E_ALREADYEXISTS, pteststate);

          if (!pteststate->error_code)
          {
            OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() => File '%s' already exists. Remove and create it again.", szFileWithPath);

            OEDT_EXPECT_DIFFERS(OSAL_ERROR, 
                                OSAL_s32IORemove((tString)szFileWithPath),
                                pteststate);

            if (!pteststate->error_code)
            {
              OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() => OSAL_s32IORemove('%s') done.", szFileWithPath);
            }

            hIODescriptor = OSAL_IOCreate((tString)szFileWithPath, OSAL_EN_READWRITE);
          }
        }

        OEDT_EXPECT_DIFFERS(OSAL_ERROR, hIODescriptor, pteststate);

        if (!pteststate->error_code)
        {
          tU32 u32BytesWritten = 0;

          OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() => OSAL_IOCreate('%s') done. hIODescriptor = '%d'.", szFileWithPath, hIODescriptor);

          for (u16NumberOfFileWrites = 0; u16NumberOfFileWrites < 1000; u16NumberOfFileWrites++)
          {
            OEDT_EXPECT_EQUALS(sizeof(u8FileWriteBuffer),
                               OSAL_s32IOWrite(hIODescriptor,
                                               (tPCS8)u8FileWriteBuffer,
                                               sizeof(u8FileWriteBuffer)),
                               pteststate);

            if (!pteststate->error_code)
            {
              u32BytesWritten += sizeof(u8FileWriteBuffer);
            }
          }

          if (!pteststate->error_code)
          {
            OEDT_HelperPrintf(TR_LEVEL_COMPONENT, 
                              "vFSStressTest() => OSAL_s32IOWrite() written %u bytes to file '%s'.",
                               u32BytesWritten,
                               szFileWithPath);
          }

          OEDT_EXPECT_DIFFERS(OSAL_ERROR,
                              OSAL_s32IOControl(hIODescriptor, OSAL_C_S32_IOCTRL_FIOFLUSH, 0),
                              pteststate);

          if (!pteststate->error_code)
          {
            OEDT_HelperPrintf(TR_LEVEL_COMPONENT, 
                              "vFSStressTest() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_FIOFLUSH) for hIODescriptor = %d successfull.",
                              hIODescriptor);
          }

          OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() => OSAL_IOClose() with hIODescriptor '%d' done.", hIODescriptor);

          OEDT_EXPECT_DIFFERS(OSAL_ERROR, 
                              OSAL_s32IOClose(hIODescriptor),
                              pteststate);

        }
      }
    }
  
	/* --------------------------------------------------------------------------- */
	/* Remove the just created files.                                              */
	/* --------------------------------------------------------------------------- */

    for (u16ActFileNumber = 0; u16ActFileNumber < cu16NumberOfFiles; u16ActFileNumber++)
    {
      OEDT_EXPECT_DIFFERS(OSAL_ERROR, 
                          OSALUTIL_s32SaveNPrintFormat(szFileWithPath, 
                                                       sizeof(szFileWithPath), 
                                                       "%s%s%03u%s", 
                                                       strDevName, 
                                                       "/StressTest",
                                                       u16ActFileNumber,
                                                       ".txt"),
                          pteststate);

      if (!pteststate->error_code)
      {
        OEDT_EXPECT_DIFFERS(OSAL_ERROR, 
                            OSAL_s32IORemove((tString)szFileWithPath),
                            pteststate);

        if (!pteststate->error_code)
        {
          OEDT_HelperPrintf(TR_LEVEL_COMPONENT, "vFSStressTest() => OSAL_s32IORemove('%s') done.", szFileWithPath);
        }
      }
	}

	/* --------------------------------------------------------------------------- */

    u32ElapsedTimeEndMs = OSAL_ClockGetElapsedTime();

    OEDT_EXPECT_DIFFERS(0, u32ElapsedTimeEndMs, pteststate);

    if (!pteststate->error_code)
    {
	  fElapsedTimeSeconds = (tFloat)(u32ElapsedTimeEndMs - u32ElapsedTimeStartMs) / 1000;
	}
  }

  OEDT_HelperPrintf(TR_LEVEL_COMPONENT, 
                    "vFSStressTest() finished after %.3f seconds.",
                    fElapsedTimeSeconds);
}

/* EOF */

