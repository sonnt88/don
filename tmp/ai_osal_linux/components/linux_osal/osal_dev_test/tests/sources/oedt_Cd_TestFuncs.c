/*****************************************************************************
*FILE         : oedt_cd_TestFuncs.c
*
*SW-COMPONENT : OEDT_FrmWrk  
*
*DESCRIPTION  : This file implements the individual test cases for the 
*               dev_cdaudio / dev_cdctrl OSAL devices
*
*AUTHOR(s)    : srt2hi
*
*COPYRIGHT    : (c) 2012 BSOT
*
*HISTORY      :
*                DATE    |   Modification               | Author 
*               18/11/16 | Added T159 - Voltage Handling| boc7kor
*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osdevice.h"
#include "osioctrl.h"
#include <fcntl.h>
//#include "oedt_helper_funcs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define OCDTR(TL,...) if(g_iTraceLevel >= (tInt)(TL))\
                             vTracePrintf(__VA_ARGS__)


#define OEDT_CD_LOWORD(U32) ((tU16)((U32)&(0xFFFFUL)))
#define OEDT_CD_HIWORD(U32) ((tU16)((U32) >> 16))

#define OEDT_CD_8TO32(_3_,_2_,_1_,_0_) (((tU32)(_3_) << 24) |\
                                       ((tU32)(_2_) << 16) |\
                                       ((tU32)(_1_) << 8) |\
                                       ((tU32)(_0_)))

#define OEDT_CD_ARSIZE(_X_) (sizeof(_X_) / sizeof((_X_)[0]))

#define OEDT_CD_RELOFFSETFROMPLAYINFO(PI) ((tU16)PI.rRelTrackAdr.u8MSFMinute \
                                           * 60 \
                                           + (tU16)PI.rRelTrackAdr.u8MSFSecond)

#define OEDT_CD_MS2SECS(msf) (((tU32)(msf).u8MSFMinute * 60UL) \
                                                    + (tU32)(msf).u8MSFSecond)
#define OEDT_CD_NOTI32(HW,LW) (((tU32)(OSAL_C_U16_NOTI_ ## HW)<<16)\
                               | ((tU32)(OSAL_C_U16_ ## LW)))

#define OEDT_CD_IGNORE32 0xFFFFFFFF

/*#define OEDT_CD_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24))*/

//#define OEDT_CD_MIN(X,Y) ((X) < (Y) ? (X) : (Y))


/*****************************************************************
| const definition (scope: module-local)
|----------------------------------------------------------------*/
#define OEDT_CD_MAX_ERR_STR_SIZE    128  /*error text buffer size*/
#define OEDT_CD_BPS                 2048 /*Bytes per sector*/
static const tU32 g_cu32ErrTxtFixSize = 11; /*formatted output of "0x%08X: "*/
static const tInt c0               = (tInt)TR_LEVEL_FATAL;
static const tInt c1               = (tInt)TR_LEVEL_ERRORS;
static const tInt c2               = (tInt)TR_LEVEL_SYSTEM_MIN;
static const tInt c3               = (tInt)TR_LEVEL_SYSTEM;

#define OEDT_CD_TRACE_DEVICE            "/dev/trace"
#define OEDT_CD_TRACE_PERMISSION        OSAL_EN_READWRITE
#define OEDT_CD_MAX_TRACE_STR_SIZE      256

#define OEDT_CD_HASH_SIZE     32
#define OEDT_CD_HASH_MASCA_01 "52526A30131728301316190013161900"
#define OEDT_CD_HASH_MASCA_02 "5AD0DDBD94949EA56A15AF8CB5358FB5"
#ifdef GEN3X86
#define OEDT_CD_HASH_MASCA_03 "341D8A92341D8A93341D8A90341D8A92"
#else
#define OEDT_CD_HASH_MASCA_03 "0A59803E0A49813F0A4D23EE0A4D23EE"
#endif
#define OEDT_CD_HASH_MASCA_05 "0B0E606B0B05616A0B01A0B50B01A0B5"

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/
static tInt g_iTraceLevel = (tInt)TR_LEVEL_ERRORS;

/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/
//static tVoid OCDTR(tInt iTraceLevel, tPCChar pchFormat, ...);
static tU32 u32T010_work(tBool bInSlot);


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/
/*****************************************************************************
*
* FUNCTION:    FileOperation
*
* DESCRIPTION: File Copy function           
*
* PARAMETER:   tString  sources file
*              tString  destination file
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.07.06  | Initial revision                       | MRK2HI
* 11.03.09  | MMS 223874 : Minor corrections to      | JEV1KOR
*           | ensure write returns during error.     |
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
/*static tS32 s32GetFileSize(ID fd)
{
  tS32 s32RetValue = OSAL_ERROR;
  tS32 s32CurrentPos;
  if((s32CurrentPos = lseek(fd,0,SEEK_CUR)) < E_OK)
  {
          // error
  }
  else
  {
     if((s32RetValue = lseek(fd,0,SEEK_END)) < E_OK)
     {
          // error
       s32RetValue = OSAL_ERROR;
     }
     else
    {
       s32RetValue = s32RetValue - s32CurrentPos;
       if(lseek(fd,s32CurrentPos,SEEK_SET) < E_OK)
      {
       // error
         s32RetValue = OSAL_ERROR;
      }
    }
  }
  return s32RetValue;
}
*/
/********************************************************FunctionHeaderBegin***
* vSend2TTFis()
* sends a message to the configured output device
***********************************************************FunctionHeaderEnd***/
static tVoid vSend2TTFis(tPCS8 ps8Msg, tU32 u32Len)
{
  OSAL_trIOWriteTrace rMsg2Trace;
  OSAL_tIODescriptor  fdTrace;

  rMsg2Trace.u32Class    = (tU32)TR_COMP_OSALTEST;
  rMsg2Trace.u32Level    = (tU32)TR_LEVEL_FATAL;
  rMsg2Trace.u32Length   = u32Len;
  rMsg2Trace.pcos8Buffer = ps8Msg;

  fdTrace = OSAL_IOOpen(OEDT_CD_TRACE_DEVICE, OEDT_CD_TRACE_PERMISSION);

  if(fdTrace != OSAL_ERROR)
  {
    OSAL_s32IOWrite(fdTrace, (tPCS8)&rMsg2Trace, sizeof(rMsg2Trace));
    OSAL_s32IOClose(fdTrace);
  }
}

/*********************************************************FunctionHeaderBegin***
* vTracePrintf
* TTFIS output in printf-style
***********************************************************FunctionHeaderEnd***/
static tVoid vTracePrintf(tPCChar pchFormat, ...)
{
  tChar pcBuffer[OEDT_CD_MAX_TRACE_STR_SIZE];
  va_list argList = {0};
  tU32 u32Len;
  va_start(argList, pchFormat);
  
  memset(pcBuffer, 0, sizeof(pcBuffer));
  pcBuffer[0] = 0x1A; // COMMAND_ID
  pcBuffer[1] = 0x02; //oedt.trc:1a:(6,1)==0x1a,(7,1)==0x02: INFO: %s(8,1,I,255)
   (void)vsnprintf(&pcBuffer[2], 250, pchFormat, argList);
  u32Len = (tU32)strlen(&pcBuffer[2]) + 2;
  vSend2TTFis((tS8*)pcBuffer, u32Len);
  va_end(argList);
}

/*****************************************************************************
* FUNCTION:     vTraceHex
* PARAMETER:    Buffer, Length of buffer, number of Bytes printed per Line
* RETURNVALUE:  result
* DESCRIPTION:  output hex dump to TTFIS
******************************************************************************/
static tVoid vTraceHex(const tU8 *pu8Buf, tInt iBufLen, tInt iBytesPerLine)
{
  tInt iBpl = iBytesPerLine;
  tInt iL, iC;
  char c;
  static char szTmp[32];
  static char szTxt[256];
  if(NULL == pu8Buf)
  {
    return;
  } //if(NULL == pu8Buf)


  for(iL = 0 ; iL < iBufLen ; iL += iBpl)
  {
    sprintf(szTxt, "%04X:",(unsigned int)(iL));
    for(iC = 0 ; iC < iBpl ; iC++)
    {
      tInt iIndex = iL + iC;
      if((iIndex < iBufLen))
      { //satisfy L I N T:
        //  Warning 662: Possible creation of out-of-bounds pointer
        //  (15 beyond end of data) by operator '['
        sprintf(szTmp, " %02X",(unsigned int)pu8Buf[iIndex]);
        strcat(szTxt, szTmp);
      }
    } //for(iC = 0 ; iC < iBpl ; iC++)

    strcat(szTxt, " ");
    for(iC = 0 ; iC < iBpl ; iC++)
    {
      tInt iIndex = iL + iC;
      if((iIndex < iBufLen))
      { //satisfy L I N T:
        //  Warning 662: Possible creation of out-of-bounds pointer
        //  (15 beyond end of data) by operator '['
        c = (char)pu8Buf[iIndex];
        if((c < 0x20) || (c >= 0x80))
        {
          c = '.';
        }
        sprintf(szTmp, "%c",(unsigned char)c);

        strcat(szTxt, szTmp);
      }
    } //for(iC = 0 ; iC < iBpl ; iC++)
    OCDTR(c3, "%s", szTxt);
  } //for(i = 0 ; i < iBufLen ; i += iBpl)
}


/*****************************************************************************
* FUNCTION:     pGetErrTxt
* PARAMETER:    void
* RETURNVALUE:  const char* of Osal-Error-text
* DESCRIPTION:  output Error text of last Osal -Error
******************************************************************************/
static const char* pGetErrTxt()
{
  return(const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}

/*****************************************************************************
* FUNCTION:     vPrintOsalResult
* PARAMETER:    Description
*               OSAL-return value
*               internal loop counter
* RETURNVALUE:  void
* DESCRIPTION:  Traces error or success
******************************************************************************/
static tVoid vPrintOsalResult(const char* pcszDescription,
                              tS32 s32Ret,
                              int iCount)
{
  if(g_iTraceLevel >= (tInt)TR_LEVEL_ERRORS)
  {
    tBool bOK = (s32Ret != OSAL_ERROR);
    if(bOK)
    {
      OCDTR(c2, "OEDT_CD [%s][%4d] SUCCESS", pcszDescription, iCount);
    }
    else //if(bOK)
    {
      OCDTR(c1, "OEDT_CD [%s][%4d] ERROR [%s]",
            pcszDescription,
            iCount,
            pGetErrTxt());
    } //else //if(bOK)
  } //if(g_iTraceLevel)
}

//used by test without cd - An Error-Code is treated as Success!
/*****************************************************************************
* FUNCTION:     vPrintOsalResultNoCD
* PARAMETER:    Description
*               OSAL-return value
*               internal loop counter
* RETURNVALUE:  void
* DESCRIPTION:  Traces error or success
*               This version treaded an OSAL-ERROR as "successfull tested"
******************************************************************************/
static tVoid vPrintOsalResultNoCD(const char* pcszDescription,
                                  tS32 s32Ret,
                                  int iCount)
{
  if(g_iTraceLevel >= (tInt)TR_LEVEL_ERRORS)
  {
    tBool bOK = (s32Ret != OSAL_ERROR);
    if(bOK)
    {
      OCDTR(c1, "OEDT_CD [%s][%4d] SUCCESS (== test f a i l e d)",
            pcszDescription, iCount);
    }
    else //if(bOK)
    {
      OCDTR(c2, "OEDT_CD [%s][%4d] ERROR (== test successfully) [%s]",
            pcszDescription,
            iCount,
            pGetErrTxt());
    } //else //if(bOK)
  } //if(g_iTraceLevel)
}


/*****************************************************************************
* FUNCTION:     vGetOsalErrorText
* PARAMETER:    Text Buffer
*               size of Text Buffer
* RETURNVALUE:  void
* DESCRIPTION:  copies error informations into given Text Buffer
******************************************************************************/
static void vGetOsalErrorText(tString pszErrorText, tU32 u32ErrorTextBufferSize)
{
  tU32     u32ErrorCode;
  tCString pcszErrorTxt;
  tU32     u32NeededBufferSize;

  u32ErrorCode = OSAL_u32ErrorCode();
  pcszErrorTxt = OSAL_coszErrorText(u32ErrorCode);

  if(g_cu32ErrTxtFixSize < u32ErrorTextBufferSize)
  {
    if(NULL != pcszErrorTxt)
    {
      u32NeededBufferSize = OSAL_u32StringLength(pcszErrorTxt) +
                            g_cu32ErrTxtFixSize;
      if(u32NeededBufferSize > u32ErrorTextBufferSize)
      {
        OSAL_s32PrintFormat(pszErrorText,
                            "0x%08X: <%s>",
                            (unsigned int)u32ErrorCode,
                            (const char*)pcszErrorTxt);
      }
      else //if(u32NeededBufferSize > u32ErrorTextBufferSize)
      {
        OSAL_s32PrintFormat(pszErrorText,
                            "0x%08X: <%s>",
                            (unsigned int)u32ErrorCode,
                            (const char*)pcszErrorTxt);
      } //else //if(u32NeededBufferSize > u32ErrorTextBufferSize)
    }
    else //if(NULL != pcszErrorTxt)
    {
      OSAL_s32PrintFormat(pszErrorText,
                          "0x%08X: <no error text available>",
                          (unsigned int)u32ErrorCode);
    } //else //if(NULL != pcszErrorTxt)
  }
  else//if(g_cu32ErrTxtFixSize >= u32ErrorTextBufferSize)
  {
  } //else//if(g_cu32ErrTxtFixSize >= u32ErrorTextBufferSize)

}

/******************************************************************************
 *FUNCTION     :  vEject
 *PARAMETER    :  void
 *RETURNVALUE  :  void
 *DESCRIPTION  :  send eject command without checking for errors
 *****************************************************************************/
static void vEject(void)
{
  OSAL_tIODescriptor hCDctrl;
  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR != hCDctrl)
  {
    //tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
    s32Arg = 0;
    OCDTR(c2, "vEject (EJECTMEDIA)");
    (void)OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    (void)OSAL_s32IOClose(hCDctrl);
  } //if(OSAL_ERROR != hCDctrl)
}

/******************************************************************************
 *FUNCTION     :  vLoad
 *PARAMETER    :  void
 *RETURNVALUE  :  void
 *DESCRIPTION  :  send closedoor command without error-check
 *****************************************************************************/
static void vLoad(void)
{
  OSAL_tIODescriptor hCDctrl;

  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR != hCDctrl)
  {
    tInt iEmergencyExit = 50;
    tS32 s32Fun, s32Arg;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
    s32Arg = 0;
    OCDTR(c2, "vLoad (CLOSEDOOR)");
    while(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(100);
    }

    (void)OSAL_s32IOClose(hCDctrl);
  } //if(OSAL_ERROR != hCDctrl)
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_NOCD
 *DESCRIPTION  :  This is no test. 
 *                Prints "REMOVE CD" and waits until cd is removed
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_NOCD(void)
{
  tU32 u32ResultBitMask = 0;
  OSAL_tIODescriptor hCDctrl;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;

  startMS = OSAL_ClockGetElapsedTime();


  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hCDctrl)
  {
    u32ResultBitMask |= 0x00000001;
  }
  else //if(OSAL_ERROR == hCDctrl)
  {
    tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    tBool bCDInserted;
    tInt iCount=0;
    //GETLOADERINFO - wait while cd is removed
    do
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= 0x00000002;
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_NO_MEDIA:
          bCDInserted = FALSE;
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
          //EJECTMEDIA
          {
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
            s32Arg = 0;
            (void)OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          }//EJECTMEDIA
          /*lint -fallthrough */
        case OSAL_C_U8_EJECT_IN_PROGRESS:
          /*lint -fallthrough */
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          bCDInserted = TRUE;
          if((iCount % 6) == 0)
          {
            OCDTR(c0, "%s", "***********************************");
            OCDTR(c0, "%s", "* REMOVE CD                       *");
            OCDTR(c0, "%s", "***********************************");
          }
          (void)OSAL_s32ThreadWait(500);
          break;

        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)

      iCount++;
      diffMS = OSAL_ClockGetElapsedTime() - startMS;
    }
    while(bCDInserted && (diffMS < (60 * 30 * 1000)));//GETLOADERINFO

    (void)OSAL_s32IOClose(hCDctrl);
  } //else //if(OSAL_ERROR == hCDctrl)

  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA01
 *DESCRIPTION  :  Prints "INSERT UDF CD"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
static tU32 u32WaitForCD(const char *pcszTxt)
{
  tU32 u32ResultBitMask = 0;
  OSAL_tIODescriptor hCDctrl;
  tBool bCDWasEjected = FALSE;


  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hCDctrl)
  {
    u32ResultBitMask |= 0x00000001;
  }
  else //if(OSAL_ERROR == hCDctrl)
  {
    tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    tBool bCDInserted;
    tInt iCount = 0;
    //GETLOADERINFO - wait while cd is inserted
    do
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= 0x00000002;
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          bCDInserted = TRUE;
          break;

        case OSAL_C_U8_EJECT_IN_PROGRESS:
          /*lint -fallthrough */
        case OSAL_C_U8_MEDIA_IN_SLOT:
          {
            tInt iEmergencyExit = 20;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
            s32Arg = 0;
            while(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg)
                  &&
                  (iEmergencyExit-- > 0))
            {
              (void)OSAL_s32ThreadWait(500);
            }
          }
          /*lint -fallthrough */
        case OSAL_C_U8_NO_MEDIA:
          /*lint -fallthrough */
        default:
          bCDWasEjected = TRUE;
          bCDInserted = FALSE;
          if((iCount % 16) == 0)
          {
            OCDTR(c0, "%s", "***********************************");
            OCDTR(c0, "%s", pcszTxt);
            OCDTR(c0, "%s", "***********************************");
          }
          (void)OSAL_s32ThreadWait(500);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount++;
    }
    while(!bCDInserted);//GETLOADERINFO

    (void)OSAL_s32IOClose(hCDctrl);
  } //else //if(OSAL_ERROR == hCDctrl)

  if(bCDWasEjected)
  {
    (void)OSAL_s32ThreadWait(10000);
  }
  else //if(bCDWasEjected)
  {
    (void)OSAL_s32ThreadWait(1000);
  } //else //if(bCDWasEjected)

  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA01
 *DESCRIPTION  :  Prints "INSERT CD MASCA 01"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA01(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 01' (UDF 1.02) *");
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA02
 *DESCRIPTION  :  Prints "INSERT CD MASCA 02"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA02(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 02' (ISO9660) *");
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA03
 *DESCRIPTION  :  Prints "INSERT CD MASCA 03"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA03(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 03' (CDDA) *");
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T000
 *DESCRIPTION  :  this is not Test, only set internal Trace to default value
 *                (level 1 == only ERRORS are printed to TTFIS)
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_T000(void)
{
  g_iTraceLevel = (tInt)TR_LEVEL_ERRORS;
  return 0;
}

/*****************************************************************************/
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*****************************************************************************/

#define OEDT_CD_T001_COUNT                                            100
#define OEDT_CD_T001_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T001_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T001_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T001_CLOSE_RESULT_ERROR_BIT                     0x80000000

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T001
 *
 *DESCRIPTION  :  open/close CDCTRL
 *
 *PARAMETER    :  void
 *             
 *RETURNVALUE  :  0
 *
 *HISTORY      :  
 *                Initial Revision.
 *****************************************************************************/
tU32 OEDT_CD_T001(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T001_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDAudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;

  OCDTR(c2, "tU32 OEDT_CD_T001(void)");


  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T001_COUNT; iCount++)
  {
    /* open device */
    hCDAudio = OSAL_IOOpen(OEDT_CD_T001_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDAudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T001_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDAudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T001_DEVICE_CTRL_NAME, (unsigned int)hCDAudio, iCount);
    } //else //if(OSAL_ERROR == hCDAudio)

    s32Ret = OSAL_s32IOClose(hCDAudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDAudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDAudio, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T001_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T001 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);

  if(OEDT_CD_T001_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T001 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T001_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*                            OEDT_CD_T002                          */
/*****************************************************************************/

#define OEDT_CD_T002_COUNT                                            100
#define OEDT_CD_T002_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO

#define OEDT_CD_T002_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T002_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T002_CLOSE_RESULT_ERROR_BIT                     0x80000000

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T002
 *
 *DESCRIPTION  :  Open/Close CDAUDIO
 *
 *PARAMETER    :  void
 *             
 *RETURNVALUE  :  0
 *
 *HISTORY      :  
 *                Initial Revision.
 *****************************************************************************/
tU32 OEDT_CD_T002(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T002_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;

  OCDTR(c2, "tU32 OEDT_CD_T002(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T002_COUNT; iCount++)
  {
    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T002_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T002_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T002_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T002_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T002_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T002_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;

  OCDTR(c2, "OEDT_CD: T002 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T002_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T002 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T002_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*                            OEDT_CD_T003                          */
/*****************************************************************************/

#define OEDT_CD_T003_COUNT                                               2
#define OEDT_CD_T003_IE_LOOP_COUNT                                       2
#define OEDT_CD_T003_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T003_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T003_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T003_GETLOADERINFO_RESULT_ERROR_BIT             0x00000002
#define OEDT_CD_T003_GETLOADERINFO_STATUS_ERROR_BIT             0x00000004

#define OEDT_CD_T003_CLOSE_RESULT_ERROR_BIT                     0x80000000

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T003
 *
 *DESCRIPTION  :  Eject/Insert-Loop (checks, whether cd is thrown from slot)
 *
 *PARAMETER    :  void
 *             
 *RETURNVALUE  :  0
 *
 *HISTORY      :  
 *                Initial Revision.
 *****************************************************************************/
tU32 OEDT_CD_T003(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T003_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;

  OCDTR(c2, "tU32 OEDT_CD_T003(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T003_COUNT; iCount++)
  {
    tInt iECLoop;

    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T003_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T003_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T003_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T003_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    // call insert/eject without delay
    // CD must not be thrown from slot!
    for(iECLoop = 0; iECLoop < OEDT_CD_T003_IE_LOOP_COUNT; iECLoop++)
    {
      //CLOSEDOOR
      {
        tInt iEmergencyExit = 5;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
        s32Arg = 0;
        while(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg)
              &&
              (iEmergencyExit-- > 0))
        { //control returns an error, while eject is in progress
          (void)OSAL_s32ThreadWait(1000);
        } //while
      }//CLOSEDOOR

      //EJECTMEDIA
      {
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
        s32Arg = 0;
        if(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg))
        {
          vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
          OCDTR(c1, "OEDT_CD: ERROR Eject <%s> (count %d): %s",
                OEDT_CD_T003_DEVICE_CTRL_NAME, iCount, szError);
        } //if !OK
      }//EJECTMEDIA

    } //for(iECLoop = 0; iECLoop < OEDT_CD_T003_IE_LOOP_COUNT; iECLoop++)


    //(void)OSAL_s32ThreadWait(5500);

    //GETLOADERINFO
    // check, wether cd is in slot yet after last eject or not
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T003_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_IN_SLOT:
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        default:
          OCDTR(c1, " ERROR Loader: [%d]", (int)rInfo.u8LoaderInfoByte);
          u32ResultBitMask |= OEDT_CD_T003_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)

      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]", (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T003_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T003_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;

  OCDTR(c2, "OEDT_CD: T003 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);

  if(OEDT_CD_T003_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T003 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T003_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}


/*****************************************************************************/
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*                            OEDT_CD_T010                          */
/*****************************************************************************/
#define OEDT_CD_T010_COUNT                                               2
#define OEDT_CD_T010_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T010_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T010_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T010_GET_DEVICE_VERSION_RESULT_ERROR_BIT        0x00000002
#define OEDT_CD_T010_GETDEVICEINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T010_GETDEVICEINFO_MEDIADEV_ERROR_BIT           0x00000008
#define OEDT_CD_T010_GET_DRIVE_VERSION_RESULT_ERROR_BIT         0x00000010
#define OEDT_CD_T010_GETDRIVEVERSION_RESULT_ERROR_BIT           0x00000020
#define OEDT_CD_T010_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000040
#define OEDT_CD_T010_SETDRIVESPEED_RESULT_ERROR_BIT             0x00000080
#define OEDT_CD_T010_CLOSEDOOR_RESULT_ERROR_BIT                 0x00000100
#define OEDT_CD_T010_SETMOTORON_RESULT_ERROR_BIT                0x00000200
#define OEDT_CD_T010_SETMOTOROFF_RESULT_ERROR_BIT               0x00000400
#define OEDT_CD_T010_EJECTMEDIA_RESULT_ERROR_BIT                0x00000800
#define OEDT_CD_T010_GETTEMP_RESULT_ERROR_BIT                   0x00001000
#define OEDT_CD_T010_GETTEMP_VALUE_ERROR_BIT                    0x00002000
#define OEDT_CD_T010_GETLOADERINFO_RESULT_ERROR_BIT             0x00004000
#define OEDT_CD_T010_GETLOADERINFO_STATUS_ERROR_BIT             0x00008000
#define OEDT_CD_T010_GETCDINFO_RESULT_ERROR_BIT                 0x00010000
#define OEDT_CD_T010_GETTRACKINFO_RESULT_ERROR_BIT              0x00020000
#define OEDT_CD_T010_READRAWDATA_RESULT_ERROR_BIT               0x00040000
#define OEDT_CD_T010_READRAWDATA_MSF_RESULT_ERROR_BIT           0x00080000
#define OEDT_CD_T010_GETDISKTYPE_RESULT_ERROR_BIT               0x00100000
#define OEDT_CD_T010_GETDISKTYPE_TYPE_ERROR_BIT                 0x00200000
#define OEDT_CD_T010_GETMEDIAINFO_RESULT_ERROR_BIT              0x00400000
#define OEDT_CD_T010_GETMEDIAINFO_TYPE_ERROR_BIT                0x00800000
#define OEDT_CD_T010_READERRORBUFFER_RESULT_ERROR_BIT           0x01000000
#define OEDT_CD_T010_SETPOWEROFF_RESULT_ERROR_BIT               0x02000000

#define OEDT_CD_T010_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T010_CLOSE_RESULT_ERROR_BIT                     0x80000000


/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T010_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16MediaState = 0;
  tU16 u16MediaType  = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T010_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        u16MediaType = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
        switch(u16MediaType)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      u16MediaState = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u16MediaState);

      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T010_nocd
 *DESCRIPTION  :  CDCTRL-IOCTRLs without CD
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T010_nocd(void)
{
  return u32T010_work(FALSE);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T010_inslot
 *DESCRIPTION  :  CDCTRL-IOCTRLs with CD InSLot
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T010_inslot(void)
{
  return u32T010_work(TRUE);
}


/******************************************************************************
 *FUNCTION     :  u32T010_work
 *DESCRIPTION  :  CDCTRL-IOCTRLs without CD inserted or with CD InSLot
 *PARAMETER    :  TRUE Test with CD in sLot
 *RETURNVALUE  :  0
 *HISTORY      :  
 *****************************************************************************/
static tU32 u32T010_work(tBool bInSlot)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask = OEDT_CD_T010_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T010(void)");


  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T010_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T010_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T010_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T010_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T010_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //GET_DEVICE_VERSION
    {
      tU32 u32Version = 0;

      s32Fun = OSAL_C_S32_IOCTRL_VERSION;
      s32Arg = (tS32)&u32Version;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GET_DEVICE_VERSION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GET_DEVICE_VERSION", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, "IOCtrlVersion: 0x%04X",
              (unsigned int)u32Version
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GET_DEVICE_VERSION

    //GETDEVICEINFO
    {
      volatile tU32 u32MediaDev = (tU32)OSAL_EN_MEDIADEV_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO;
      s32Arg = (tS32)&u32MediaDev;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GETDEVICEINFO_RESULT_ERROR_BIT 
                          : 0;

      if((tU32)OSAL_EN_MEDIADEV_CD != u32MediaDev)
      {
        u32ResultBitMask |= OEDT_CD_T010_GETDEVICEINFO_MEDIADEV_ERROR_BIT; 

      } //if(OSAL_EN_MEDIADEV_CD != u32MediaDev)

      vPrintOsalResult("GETDEVICEINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " DeviceInfo: MediaDev 0x%04X",
              (unsigned int)u32MediaDev
            );

      } //if(s32Ret != OSAL_ERROR)
    }//GETDEVICEINFO


    //GET_DRIVE_VERSION
    {
      OSAL_trCDDriveVersion rVersion;

      rVersion.u32HWVersion = 0;
      rVersion.u32SWVersion = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION;
      s32Arg = (tS32)&rVersion;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GET_DRIVE_VERSION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GET_DRIVE_VERSION", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, "Version: HW 0x%02X SW 0x%02X",
              (unsigned int)rVersion.u32HWVersion,
              (unsigned int)rVersion.u32SWVersion
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GET_DRIVE_VERSION

    //GETDRIVEVERSION
    {
      OSAL_trDriveVersion rVersion;

      rVersion.u16MajorVersionNumber = 0;
      rVersion.u16MinorVersionNumber = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDRIVEVERSION;
      s32Arg = (tS32)&rVersion;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GETDRIVEVERSION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETDRIVEVERSION", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        /*! au8.. are not zero teminated!
        it is only working without crash 
        as long Major/Minor contain at least one 0x00
        and are last members in structur*/
        OCDTR(c2, "VersionFULL:"
              " FirmwarRevision [%s],"
              " Model [%s],"
              " Serial [%s],"
              " MajorVersion 0x%04X"
              " MinorVersion 0x%04X",
              (const char*)rVersion.au8FirmwareRevision,
              (const char*)rVersion.au8ModelNumber,
              (const char*)rVersion.au8SerialNumber,
              (unsigned int)rVersion.u16MajorVersionNumber,
              (unsigned int)rVersion.u16MinorVersionNumber
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETDRIVEVERSION


    //REG_NOTIFICATION

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T010_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION


    //SETDRIVESPEED (not supported)
    {
      tU8 u8SpeedID = 0; // 0 normal, 1 High, 2 LowNoise

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED;
      s32Arg = (tS32)u8SpeedID;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_NOTSUPPORTED) ?
                          OEDT_CD_T010_SETDRIVESPEED_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD(" SETDRIVESPEED", s32Ret, iCount);
    }//SETDRIVESPEED



    //CLOSEDOOR (no media inserted!)
    if(!bInSlot)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_CLOSEDOOR_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("CLOSEDOOR", s32Ret, iCount);
    }//CLOSEDOOR


    //SETMOTORON, not supported
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_NOTSUPPORTED) ?
                          OEDT_CD_T010_SETMOTORON_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("SETMOTORON", s32Ret, iCount);
    }//SETMOTORON

    //SETMOTOROFF, not supported
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_NOTSUPPORTED) ?
                          OEDT_CD_T010_SETMOTOROFF_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("SETMOTOROFF", s32Ret, iCount);
    }//SETMOTOROFF

    //EJECTMEDIA
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_EJECTMEDIA_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("EJECTMEDIA", s32Ret, iCount);
    }//EJECTMEDIA


    //GETTEMP
    {
      OSAL_trDriveTemperature rTemp;

      rTemp.u8Temperature = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP;
      s32Arg = (tS32)&rTemp;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GETTEMP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETTEMP", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Temperature value: [%d]", (int)rTemp.u8Temperature);
      } //if(s32Ret != OSAL_ERROR)

      rTemp.u8Temperature -= 100;
      if((rTemp.u8Temperature < 15) || (rTemp.u8Temperature > 75))
      {
        u32ResultBitMask |= OEDT_CD_T010_GETTEMP_VALUE_ERROR_BIT;
        OCDTR(c1, " Temperature  < 15 or > 75: [%d]", (int)rTemp.u8Temperature);
      } //if((rTemp.u8Temperature < 15) || (rTemp.u8Temperature > 75))
    }//GETTEMP

    //GETLOADERINFO - No CD inserted in this test
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T010_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d] (bInSlot: %u)",
              (int)rInfo.u8LoaderInfoByte, 
              (tU32)bInSlot);
        //this test should not have CD inserted!
        if(bInSlot)
        {
          switch(rInfo.u8LoaderInfoByte)
          {
          case OSAL_C_U8_MEDIA_IN_SLOT:
          case OSAL_C_U8_EJECT_IN_PROGRESS:
            break;
          case OSAL_C_U8_MEDIA_INSIDE:
          case OSAL_C_U8_NO_MEDIA:
            /*lint -fallthrough */
          default:
            u32ResultBitMask |= OEDT_CD_T010_GETLOADERINFO_STATUS_ERROR_BIT;
            OCDTR(c1, " Loader ERROR WRONG STATUS: [%d] (bInSlot: %u)",
                  (int)rInfo.u8LoaderInfoByte, 
                  (tU32)bInSlot);
          } //switch(rInfo.u8LoaderInfoByte)
        }
        else //if(bInSlot)
        {
          switch(rInfo.u8LoaderInfoByte)
          {
          case OSAL_C_U8_NO_MEDIA:
            break;
          case OSAL_C_U8_MEDIA_INSIDE:
          case OSAL_C_U8_EJECT_IN_PROGRESS:
          case OSAL_C_U8_MEDIA_IN_SLOT:
            /*lint -fallthrough */
          default:
            OCDTR(c1, " Loader ERROR WRONG STATUS: [%d] (bInSlot: %u)",
                  (int)rInfo.u8LoaderInfoByte, 
                  (tU32)bInSlot);
            u32ResultBitMask |= OEDT_CD_T010_GETLOADERINFO_STATUS_ERROR_BIT;
          } //switch(rInfo.u8LoaderInfoByte)
        } //else //if(bInSlot)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO

    //GETCDINFO (no CD inserted!) OSAL_E_MEDIA_NOT_AVAILABLE
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T010_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETCDINFO

    //GETTRACKINFO (NO CD inserted!) OSAL_E_MEDIA_NOT_AVAILABLE
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = (tU8)(iCount % 100);

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T010_GETTRACKINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResultNoCD("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo:"
              " Track [%02d],"
              " StartLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)
    }//GETTRACKINFO


    //READRAWDATA (NO CD inserted!) OSAL_E_MEDIA_NOT_AVAILABLE
    {
      tS8 *ps8Buf;
      tU32 u32LBA       = (tU32)iCount;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawInfo rRRInfo;

      ps8Buf = (tS8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(ps8Buf)
      {
        memset(ps8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
      }

      rRRInfo.u32NumBlocks  = u32NumBlocks;
      rRRInfo.u32LBAAddress = u32LBA;
      rRRInfo.ps8Buffer     = ps8Buf;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
      s32Arg = (tS32)&rRRInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T010_READRAWDATA_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResultNoCD("READRAWDATA", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        //tU32 u32Block, u32BlockIndex;
        OCDTR(c2, "ReadRawDataLBA:"
              " LBA %u,"
              " Blocks %u,"
              " BufferAddress %p",
              (unsigned int)rRRInfo.u32LBAAddress,
              (unsigned int)rRRInfo.u32NumBlocks,
              rRRInfo.ps8Buffer
            );
      } //if(bIOCtrlOK(s32Fun, s32Ret))

      if(ps8Buf != NULL)
      {
        free(ps8Buf);
      } //if(ps8Buf != NULL)
    }//READRAWDATA

    //READRAWDATA_MSF (no disc) OSAL_E_MEDIA_NOT_AVAILABLE
    {
      tS8 *ps8Buf;
      tU8  u8Min   = 0;
      tU8  u8Sec   = 2;
      tU8  u8Frame = 0;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawMSFInfo rRRInfo;

      ps8Buf = (tS8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(ps8Buf)
      {
        memset(ps8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
      }

      rRRInfo.u32NumBlocks  = u32NumBlocks;
      rRRInfo.u8Minute      = u8Min;
      rRRInfo.u8Second      = u8Sec;
      rRRInfo.u8Frame       = u8Frame;
      rRRInfo.ps8Buffer     = ps8Buf;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF;
      s32Arg = (tS32)&rRRInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T010_READRAWDATA_MSF_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("READRAWDATA_MSF", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        //tU32 u32Block, u32BlockIndex;
        OCDTR(c2, " ReadRawDataMSF:"
              " MSF %02u:%02u:%02u,"
              " Blocks %u,"
              " BufferAddress %p",
              (unsigned int)rRRInfo.u8Minute,
              (unsigned int)rRRInfo.u8Second,
              (unsigned int)rRRInfo.u8Frame,
              (unsigned int)rRRInfo.u32NumBlocks,
              rRRInfo.ps8Buffer
            );
      } //if(bIOCtrlOK(s32Fun, s32Ret))

      if(ps8Buf != NULL)
      {
        free(ps8Buf);
      } //if(ps8Buf != NULL)
    }//READRAWDATA_MSF

    //GETDISKTYPE (no cd inserted)
    {
      OSAL_trDiskType rDiskType;

      rDiskType.u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
      rDiskType.u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
      s32Arg = (tS32)&rDiskType;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GETDISKTYPE_RESULT_ERROR_BIT 
                          : 0;

      if(rDiskType.u8DiskType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)
      {
        u32ResultBitMask |=OEDT_CD_T010_GETDISKTYPE_TYPE_ERROR_BIT;

      } //if(rDiskType.u8DiskSubType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)

      vPrintOsalResult("GETDISKTYPE", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetDiskType: Type %u, SubType %u",
              (unsigned int)rDiskType.u8DiskType,
              (unsigned int)rDiskType.u8DiskSubType
            );


      } //if(s32Ret != OSAL_ERROR)
    }//GETDISKTYPE


    //GETMEDIAINFO (no cd inserted)
    {
      OSAL_trMediaInfo rInfo;

      rInfo.nTimeZone = 0;
      rInfo.szcCreationDate[0] ='\0';
      rInfo.szcMediaID[0] ='\0';
      rInfo.u8FileSystemType = OSAL_C_U8_FS_TYPE_UNKNOWN;
      rInfo.u8MediaType = OSAL_C_U8_UNKNOWN_MEDIA;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_GETMEDIAINFO_RESULT_ERROR_BIT 
                          : 0;

      if(OSAL_C_U8_UNKNOWN_MEDIA != rInfo.u8MediaType)
      {
        u32ResultBitMask |= OEDT_CD_T010_GETMEDIAINFO_TYPE_ERROR_BIT;
      } //if(OSAL_C_U8_UNKNOWN_MEDIA == rInfo.u8MediaType)

      vPrintOsalResult("GETMEDIAINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c2, " MediaInfo: MediaType %u"
              " FS %u TimeZone %u"
              " Date [%s] ID [%s]",
              (unsigned int)rInfo.u8MediaType,
              (unsigned int)rInfo.u8FileSystemType,
              (unsigned int)rInfo.nTimeZone,
              (const char*)rInfo.szcCreationDate,
              (const char*)szTmp
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETMEDIAINFO




    //READERRORBUFFER not supported
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER;
      s32Arg = (tS32)0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_NOTSUPPORTED) ?
                          OEDT_CD_T010_READERRORBUFFER_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("READERRORBUFFER", s32Ret, iCount);
    }//READERRORBUFFER

    //SETPOWEROFF NOT SUPPORTED
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_NOTSUPPORTED) ?
                          OEDT_CD_T010_SETPOWEROFF_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResultNoCD("SETPOWEROFF", s32Ret, iCount);
    }//SETPOWEROFF


    //UNREG_NOTIFICATION

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T010_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T010_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T010_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T010 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T010_RESULT_OK_VALUE != u32ResultBitMask) /*lint !e774*/
  {
    OCDTR(c1, "OEDT_CD: T010 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T010_RESULT_OK_VALUE != u32ResultBitMask)


  return u32ResultBitMask;
}


/*****************************************************************************/
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*                            OEDT_CD_T011                          */
/*****************************************************************************/

#define OEDT_CD_T011_COUNT                                               5
#define OEDT_CD_T011_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO

#define OEDT_CD_T011_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T011_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000001
#define OEDT_CD_T011_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT    0x00000002
#define OEDT_CD_T011_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000004
#define OEDT_CD_T011_GETADDITIONALCDINFO_RESULT_ERROR_BIT       0x00000008
#define OEDT_CD_T011_SETPLAYRANGE_RESULT_ERROR_BIT              0x00000010
#define OEDT_CD_T011_PLAY_RESULT_ERROR_BIT                      0x00000020
#define OEDT_CD_T011_GETPLAYINFO_RESULT_ERROR_BIT               0x00000040
#define OEDT_CD_T011_FASTFORWARD_RESULT_ERROR_BIT               0x00000080
#define OEDT_CD_T011_FASTBACKWARD_RESULT_ERROR_BIT              0x00000100
#define OEDT_CD_T011_STOP_RESULT_ERROR_BIT                      0x00000200

#define OEDT_CD_T011_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT  0x40000000
#define OEDT_CD_T011_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000


/*****************************************************************************
* FUNCTION:     T011_vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback
******************************************************************************/
static tVoid T011_vPlayInfoNotify(tPVoid pvCookie,
                                  const OSAL_trPlayInfo* prInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);

  if(NULL != prInfo)
  {
    OCDTR(c2,"T011_vPlayInfoNotify:"
          " AbsMSF [%02u:%02u:%02u],"
          " RelTMSF [%02u-%02u:%02u:%02u],"
          " PlayStatus [0x%08X]",
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32TrackNumber,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32StatusPlay
        );

    switch(prInfo->u32StatusPlay)
    {
    case OSAL_C_S32_PLAY_PAUSED:
      OCDTR(c2,"PAUSE");
      break;
    case OSAL_C_S32_PLAY_COMPLETED:
      OCDTR(c2,"COMPLETED");
      break;
    case OSAL_C_S32_PLAY_IN_PROGRESS:
      OCDTR(c2,"PLAY_IN_PROGRESS");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
      OCDTR(c2,"STOPPED_DUE_TO_ERROR");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
      OCDTR(c2,"STOPPED_DUE_TO_UNDERVOLTAGE");
      break;
    case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
      OCDTR(c2,"NOT VALID");
      break;
    case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
      OCDTR(c2,"NO CURRENT AUDIO STATUS");
      break;
    default:
      OCDTR(c1,"- default -");
      break;
    } //switch(prInfo->u32StatusPlay)

  } //if(NULL != prInfo)

}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T011
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T011(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T011_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T011(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T011_COUNT; iCount++)
  {
    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T011_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T011_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T011_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T011_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)


    //REG_NOTIFICATION CDAUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T011_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T011_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//REG_NOTIFICATION CDAUDIO

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo = {0};
      tU8 u8Track;
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);

      for(u8Track = 0; u8Track <= 99; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ECode = OSAL_u32ErrorCode();
        u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                            OEDT_CD_T011_GETTRACKINFOAUDIO_RESULT_ERROR_BIT 
                            : 0;

        vPrintOsalResultNoCD("GETTRACKINFO AUDIO", s32Ret, iCount);
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO

    //GETADDITIONALCDINFO
    {
      OSAL_trAdditionalCDInfo rInfo = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);

      rInfo.u32DataTracks[0] = 0;
      rInfo.u32DataTracks[1] = 0;
      rInfo.u32DataTracks[2] = 0;
      rInfo.u32DataTracks[3] = 0;
      rInfo.u32InfoBits   = 0;

      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T011_GETADDITIONALCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("GETADDITIONALCDINFO", s32Ret, iCount);
    }//GETADDITIONALCDINFO

    //range+play
    {
      static const tU16 EOT=0xFFFF;
      //startTrack + start offset in seconds
      tU8  au8ST[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
        1,  1,  8,  7,  7,  16, 16,  2, 16, 16};
      tU16 au16SO[] = {0,0,0,0,0,0,0,0,0, 0, 0, 0, 0, 0, 0, 0,
        0,330,290,  0,180, 175,  0,260,180,188};
      //endTrack + end offset in seconds
      tU8  au8ET[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
        1,  2,  9,  7,     7,  16, 16,     2,    16,    16};
      tU16 au16EO[] = {1,1,2,2,3,3,4,4,5, 5, 4, 4, 2, 2, 1, 1,
        1,  5,  0, 10,EOT, 188,  1,EOT,EOT,EOT};
      tInt iCnt;
      tInt iASize  = OEDT_CD_ARSIZE(au8ST);

      for(iCnt = 0; iCnt < iASize; iCnt++)
      {
        //reset

        //SETPLAYRANGE
        {
          OSAL_trPlayRange rPlayRange = {0};
          OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rPlayRange);

          rPlayRange.rStartAdr.u8Track   = au8ST[iCnt];
          rPlayRange.rStartAdr.u16Offset = au16SO[iCnt];
          rPlayRange.rEndAdr.u8Track     = au8ET[iCnt];
          rPlayRange.rEndAdr.u16Offset   = au16EO[iCnt];

          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
          s32Arg = (tS32)&rPlayRange;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ECode = OSAL_u32ErrorCode();
          u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                              OEDT_CD_T011_SETPLAYRANGE_RESULT_ERROR_BIT
                              : 0;
          OCDTR(c2, "SETPLAYRANGE ArrayIndex %d "
                "STO[%02u-%04u] - ETO[%02u-%04u]",
                iCnt,
                (unsigned int)au8ST[iCnt],
                (unsigned int)au16SO[iCnt],
                (unsigned int)au8ET[iCnt],
                (unsigned int)au16EO[iCnt]);
          vPrintOsalResult("SETPLAYRANGE", s32Ret, iCount);
        }//SETPLAYRANGE

        //PLAY
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ECode = OSAL_u32ErrorCode();
          u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                              OEDT_CD_T011_PLAY_RESULT_ERROR_BIT
                              : 0;
          OCDTR(c2, "PLAY ArrayIndex %d", iCnt);
          vPrintOsalResultNoCD("PLAY", s32Ret, iCount);
        }//PLAY
      } //for(iCnt = 0; iCnt < iASize; iCnt++)
    } //range+ play


    //GETPLAYINFO
    {
      OSAL_trPlayInfo rInfo = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);

      rInfo.u32TrackNumber           = 0;
      rInfo.rAbsTrackAdr.u8MSFMinute = 0;
      rInfo.rAbsTrackAdr.u8MSFSecond = 0;
      rInfo.rAbsTrackAdr.u8MSFFrame  = 0;
      rInfo.rRelTrackAdr.u8MSFMinute = 0;
      rInfo.rRelTrackAdr.u8MSFSecond = 0;
      rInfo.rRelTrackAdr.u8MSFFrame  = 0;
      rInfo.u32StatusPlay            = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T011_GETPLAYINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResultNoCD("GETPLAYINFO", s32Ret, iCount);
    }//GETPLAYINFO


    //FASTFORWARD
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T011_FASTFORWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("FASTFORWARD", s32Ret, iCount);
    }//FASTFORWARD


    //FASTBACKWARD
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T011_FASTBACKWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResultNoCD("FASTBACKWARD", s32Ret, iCount);
    }//FASTBACKWARD


    //STOP
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T011_STOP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("STOP", s32Ret, iCount);
    }//STOP



    //UNREG_NOTIFICATION AUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T011_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T011_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T011_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)


  } //for(iCount = 0; iCount < OEDT_CD_T011_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T011 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T011_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T011 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T011_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}



/*****************************************************************************/
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*                            OEDT_CD_T012                          */
/*****************************************************************************/

#define OEDT_CD_T012_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T012_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T012_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T012_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T012_REG_NOTIFICATION2_RESULT_ERROR_BIT         0x00000004
#define OEDT_CD_T012_CLOSEDOOR_RESULT_ERROR_BIT                 0x00000008
#define OEDT_CD_T012_EJECTMEDIA_RESULT_ERROR_BIT                0x00000010
#define OEDT_CD_T012_MONITOR_SIZE_ERROR_BIT                     0x00000020
#define OEDT_CD_T012_MONITOR_MEDIACHANGEINFO_ERROR_BIT          0x00000040
#define OEDT_CD_T012_MONITOR_LOADERINFO_ERROR_BIT               0x00000080

#define OEDT_CD_T012_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x20000000
#define OEDT_CD_T012_UNREG_NOTIFICATION2_RESULT_ERROR_BIT       0x40000000
#define OEDT_CD_T012_CLOSE_RESULT_ERROR_BIT                     0x80000000



#define OEDT_CD_T012_MONITOR_SIZE                               50

typedef struct OEDT_CD_T012_monitor_tag
{
  OSAL_tMSecond tMS;
  tU32 u32MediaChangeInfo;
  tU8 u8LoaderInfoByte;

}OEDT_CD_T012_monitor_type;


static OEDT_CD_T012_monitor_type  T012_aMon[OEDT_CD_T012_MONITOR_SIZE];
static tInt                       T012_iMonIdx;
static OSAL_tIODescriptor         T012_hCDctrl = OSAL_ERROR;
static tBool                      T012_bMediaReady  = FALSE;
static tBool                      T012_bMediaDefect = FALSE;

//setpoints
//first cd insert = MASCA01, radio must be started without CD!
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca01[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  //  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,MEDIA_EJECTED)  ,OSAL_C_U8_NO_MEDIA},
  //  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_NO_MEDIA},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)     ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)     ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};

//second cd insert == MASCA02, cd was previously ejected
//                    or radio was started with cd inserted
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca02[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  //  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,MEDIA_EJECTED)  ,OSAL_C_U8_NO_MEDIA},
  //  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_NO_MEDIA},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)     ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)     ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};

//third cd insert == MASCA03, cd was previously ejected
//                    or radio was started with cd inserted
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca03[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  //  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,MEDIA_EJECTED)  ,OSAL_C_U8_NO_MEDIA},
  //  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_NO_MEDIA},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};

//fourth cd insert == MASCA04, cd was previously ejected
//                    or radio was started with cd inserted
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca04[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,INCORRECT_MEDIA),OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  //{1,OEDT_CD_NOTI32(DEFECT       ,DEFECT_DISCTOC) ,OSAL_C_U8_MEDIA_INSIDE},
  //{1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,INCORRECT_MEDIA),OSAL_C_U8_MEDIA_INSIDE},
  //{1,OEDT_CD_NOTI32(DEFECT       ,DEFECT_DISCTOC) ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};

//5th cd insert == MASCA05, cd was previously ejected
//                    or radio was started with cd inserted
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca05[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  //  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,MEDIA_EJECTED)  ,OSAL_C_U8_NO_MEDIA},
  //  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_NO_MEDIA},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};

//cd inserted while runnig regression data set == MASCA03
static const OEDT_CD_T012_monitor_type  T012_aMonCompMasca03_1[] = 
{
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};


static tU32 u32T012_work(const char *pcszTxt,
                         const OEDT_CD_T012_monitor_type *pMonComp);

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T012_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);

  if(pu32MediaChangeInfo != NULL)
  {
    tU16 u16NotiType;
    tU16 u16State;
    OCDTR(c2, "T012_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    if(OSAL_C_U16_NOTI_MEDIA_STATE == u16NotiType)
    {
      if(OSAL_C_U16_MEDIA_READY == u16State)
      {
        T012_bMediaReady = TRUE;
      } //if(OSAL_C_U16_MEDIA_READY == u16State)
    } //if(OSAL_C_U16_NOTI_MEDIA_STATE == u16NotiType)

    if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)
    {
      switch(u16State)
      {
      case OSAL_C_U16_DEFECT_DISCTOC:
        T012_bMediaDefect = TRUE;
        break;

      case OSAL_C_U16_DEFECT_LOAD_EJECT:
      case OSAL_C_U16_DEFECT_LOAD_INSERT:
      case OSAL_C_U16_DEFECT_DISC:
      case OSAL_C_U16_DEFECT_READ_ERR:
      case OSAL_C_U16_DEFECT_READ_OK:
      default:
        break;
      } //switch(u16State)
    } //if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)

    if(OSAL_C_U16_NOTI_MEDIA_CHANGE == u16NotiType)
    {
      switch(u16State)
      {
      case OSAL_C_U16_INCORRECT_MEDIA:
        T012_bMediaDefect = TRUE;
        break;

      case OSAL_C_U16_MEDIA_EJECTED:
      case OSAL_C_U16_DATA_MEDIA:
      case OSAL_C_U16_AUDIO_MEDIA:
      case OSAL_C_U16_UNKNOWN_MEDIA:
      default:
        break;
      } //switch(u16State)
    } //if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)

    if(T012_iMonIdx < OEDT_CD_T012_MONITOR_SIZE
       &&
       T012_iMonIdx >= 0)
    {
      T012_aMon[T012_iMonIdx].u32MediaChangeInfo = *pu32MediaChangeInfo;
      T012_aMon[T012_iMonIdx].tMS                = OSAL_ClockGetElapsedTime();

      //loaderinfo
      {
        OSAL_trLoaderInfo rInfo;
        tS32 s32Fun, s32Arg, s32Ret;

        rInfo.u8LoaderInfoByte = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);

        if(s32Ret == OSAL_ERROR)
        {
          T012_aMon[T012_iMonIdx].u8LoaderInfoByte = 0xFF;
        }
        else //if(s32Ret == OSAL_ERROR)
        {
          T012_aMon[T012_iMonIdx].u8LoaderInfoByte = rInfo.u8LoaderInfoByte;
        } //else //if(s32Ret == OSAL_ERROR)
      }
      T012_iMonIdx++;
    } // if(T012_iMonIdx < OEDT_CD_T012_MONITOR_SIZE ...
  } //if(pu32MediaChangeInfo != NULL)
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca01
 *DESCRIPTION  :  prints "Insert CD 'MASCA 01'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca01(void)
{
  return u32T012_work("*   Insert CD 'MASCA 01' (UDF)",
                      T012_aMonCompMasca01);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca02
 *DESCRIPTION  :  prints "Insert CD 'MASCA 02'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca02(void)
{
  return u32T012_work("* Insert CD 'MASCA 02' (ISO9660)",
                      T012_aMonCompMasca02);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca03
 *DESCRIPTION  :  prints "Insert CD 'MASCA 03'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca03(void)
{
  return u32T012_work("*   Insert CD 'MASCA 03' (CDDA)",
                      T012_aMonCompMasca03);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca04
 *DESCRIPTION  :  prints "Insert CD 'MASCA 04'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca04(void)
{
  return u32T012_work("*   Insert CD 'MASCA 04' (DVD)",
                      T012_aMonCompMasca04);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca05
 *DESCRIPTION  :  prints "Insert CD 'MASCA 05'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca05(void)
{
  return u32T012_work("*   Insert CD 'MASCA 05' (Mixed Mode CD)",
                      T012_aMonCompMasca05);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T012_masca03_1
 *DESCRIPTION  :  prints "Insert CD 'MASCA 03'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T012_masca03_1(void)
{
  return u32T012_work("*   Insert CD 'MASCA 03' (CDDA)",
                      T012_aMonCompMasca03_1);
}


/******************************************************************************
 *FUNCTION     :  u32T012_work
 *DESCRIPTION  :  Monitors inserting and detecting of CD
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
static tU32 u32T012_work(const char *pcszTxt,
                         const OEDT_CD_T012_monitor_type *pMonComp)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask = OEDT_CD_T012_RESULT_OK_VALUE;
  tS32 s32Ret;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 u32T012_work(void)");

  memset(T012_aMon, 0, sizeof(T012_aMon));
  T012_iMonIdx = 0;
  T012_bMediaReady = FALSE;
  T012_bMediaDefect = FALSE;


  startMS = OSAL_ClockGetElapsedTime();

  /* open device */
  T012_hCDctrl = OSAL_IOOpen(OEDT_CD_T012_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == T012_hCDctrl)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR Open <%s> : %s",
          OEDT_CD_T012_DEVICE_CTRL_NAME,  szError);
    u32ResultBitMask |= OEDT_CD_T012_OPEN_RESULT_ERROR_BIT;
  }
  else //if(OSAL_ERROR == T012_hCDctrl)
  {
    OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X",
          OEDT_CD_T012_DEVICE_CTRL_NAME, (unsigned int)T012_hCDctrl);
  } //else //if(OSAL_ERROR == T012_hCDctrl)


  //REG_NOTIFICATION
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;
    rReg.pCallback           = T012_vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_REG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("REG_NOTIFICATION", s32Ret, 0);
  }//REG_NOTIFICATION


  //GETLOADERINFO - wait while cd is inserted
  {
    tInt iCount = 1800;  //wait for max. 1800 secs
    tBool bCDInserted;
    OSAL_trLoaderInfo rInfo;
    do
    {
      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          bCDInserted = TRUE;
          break;

        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
        case OSAL_C_U8_NO_MEDIA:
          /*lint -fallthrough */
        default:
          bCDInserted = FALSE;
          if((iCount % 5) == 0)
          { //print text each 5 seconds
            OCDTR(c0, "%s", "*******************************************");
            OCDTR(c0, "%s", pcszTxt);
            OCDTR(c0, "%s", "*******************************************");
          }
          (void)OSAL_s32ThreadWait(1000);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount--;
    }
    while((FALSE == bCDInserted) && (iCount > 0));//GETLOADERINFO
  } //GETLOADERINFO - wait while cd is inserted

  //wait until media ready or defect
  {
    tInt iEmergencyExit = 25;
    while((TRUE != T012_bMediaReady)
          &&
          (TRUE != T012_bMediaDefect)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(1000);
    } //while(!ready)
  }

  (void)OSAL_s32ThreadWait(500);

  //UNREG_NOTIFICATION
  {
    OSAL_trRelNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, 0);
  }//UNREG_NOTIFICATION

  (void)OSAL_s32ThreadWait(1000);

  T012_iMonIdx = 0;
  T012_bMediaReady = FALSE;
  T012_bMediaDefect = FALSE;

  //REG_NOTIFICATION
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;
    rReg.pCallback           = T012_vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_REG_NOTIFICATION2_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("REG_NOTIFICATION", s32Ret, 0);
  }//REG_NOTIFICATION


  //EJECTMEDIA
  {
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
    s32Arg = 0;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_EJECTMEDIA_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("EJECTMEDIA", s32Ret, 0);
  }//EJECTMEDIA


  //GETLOADERINFO - wait while cd is INSLOT
  {
    tInt iCount = 10;  //wait for max. 10 secs
    tBool bCDInSlot;
    OSAL_trLoaderInfo rInfo;
    do
    {
      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        bCDInSlot = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_IN_SLOT:
          bCDInSlot = TRUE;
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_NO_MEDIA:
        default:
          bCDInSlot = FALSE;
          (void)OSAL_s32ThreadWait(250);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount--;
    }
    while((FALSE == bCDInSlot) && (iCount > 0));//GETLOADERINFO
  } //GETLOADERINFO - wait while cd is INSLOT

  (void)OSAL_s32ThreadWait(500);

  T012_bMediaReady = FALSE;
  T012_bMediaDefect = FALSE;
  //CLOSEDOOR
  {
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
    s32Arg = 0;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_CLOSEDOOR_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("CLOSEDOOR", s32Ret, 0);
  }//CLOSEDOOR

  //wait until media ready or defect
  {
    tInt iEmergencyExit = 25;
    while((TRUE != T012_bMediaReady)
          &&
          (TRUE != T012_bMediaDefect)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(1000);
    } //while(!ready)
  }
  
  (void)OSAL_s32ThreadWait(500);

  //UNREG_NOTIFICATION
  {
    OSAL_trRelNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T012_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T012_UNREG_NOTIFICATION2_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, 0);
  }//UNREG_NOTIFICATION

  s32Ret = OSAL_s32IOClose(T012_hCDctrl);
  if(OSAL_OK != s32Ret)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X: %s",
          (unsigned int)T012_hCDctrl, szError);
    u32ResultBitMask |= OEDT_CD_T012_CLOSE_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OCDTR(c2, "OEDT_CD: "
          "SUCCESS CLOSE handle %u",
          (unsigned int)T012_hCDctrl);
  } //else //if(OSAL_OK != s32Ret)

  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T012 Time: %u ms", (unsigned int)diffMS);

  //check monitored data
  {

    tInt i;
    tInt iMonCompSize;
    static char szTxt[256];
    static char szTxt1[32];

    //calculate comparison array size
    iMonCompSize = 0;
    while(pMonComp[iMonCompSize].tMS == 1)
    {
      iMonCompSize++;
    } //while(pMonComp[iMonCompSize].tMS == 1)


    //check
    if(iMonCompSize != T012_iMonIdx)
    {
      u32ResultBitMask |= OEDT_CD_T012_MONITOR_SIZE_ERROR_BIT;
      OCDTR(c1, "Monitored data size mismatch %d != %d",
            (unsigned int)T012_iMonIdx,
            (unsigned int)iMonCompSize);
    }
    else  //if(iMonCompSize != T012_iMonIdx)
    {
      for(i = 0; i < iMonCompSize; i++)
      {
        if(pMonComp[i].u32MediaChangeInfo != OEDT_CD_IGNORE32)
        {
          if(pMonComp[i].u32MediaChangeInfo
             != T012_aMon[i].u32MediaChangeInfo)
          {
            u32ResultBitMask |= OEDT_CD_T012_MONITOR_MEDIACHANGEINFO_ERROR_BIT;
            OCDTR(c1, "Monitored ChangeInfo not equal [%02d] 0x%08X != 0x%08X",
                  i,
                  (unsigned int)pMonComp[i].u32MediaChangeInfo,
                  (unsigned int)T012_aMon[i].u32MediaChangeInfo);
          } //if(u32MediaChangeInfo !=
        } //if(pMonComp[i].u32MediaChangeInfo != OEDT_CD_IGNORE32)

        if(pMonComp[i].u8LoaderInfoByte
           != T012_aMon[i].u8LoaderInfoByte)
        {
          u32ResultBitMask |= OEDT_CD_T012_MONITOR_LOADERINFO_ERROR_BIT;
          OCDTR(c1, "Monitored LoaderInfo not equal [%02d] 0x%02X != 0x%02X",
                i,
                (unsigned int)pMonComp[i].u8LoaderInfoByte,
                (unsigned int)T012_aMon[i].u8LoaderInfoByte);
        } //if(u32MediaChangeInfo !=
      } //for(i = 0; i < iMonCompSize; i++)
    } //else  //if(iMonCompSize != T012_iMonIdx)

    //print monitored data
    for(i = 0; i < T012_iMonIdx; i++)
    {
      tU32 u32Time     = T012_aMon[i].tMS;
      tU16 u16NotiType = OEDT_CD_HIWORD(T012_aMon[i].u32MediaChangeInfo);
      tU16 u16State    = OEDT_CD_LOWORD(T012_aMon[i].u32MediaChangeInfo);

      OCDTR(c3, "%2d: %6ums MediaChangeInfo 0x%08X, LoaderInfo 0x%02X",
            i,
            (unsigned int)T012_aMon[i].tMS,
            (unsigned int)T012_aMon[i].u32MediaChangeInfo,
            (unsigned int)T012_aMon[i].u8LoaderInfoByte);

      switch(u16NotiType)
      {
      case OSAL_C_U16_NOTI_MEDIA_CHANGE:
        {
          sprintf(szTxt, "  [%02d:%08ums] NOTI_MEDIA_CHANGE (0x%04X) -> ",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);

          switch(u16State)
          {
          case OSAL_C_U16_MEDIA_EJECTED:
            strcat(szTxt, "MEDIA_EJECTED");
            break;

          case OSAL_C_U16_INCORRECT_MEDIA:
            strcat(szTxt,  "INCORRECT_MEDIA");

            break;

          case OSAL_C_U16_DATA_MEDIA:
            strcat(szTxt,  "DATA_MEDIA");

            break;

          case OSAL_C_U16_AUDIO_MEDIA:
            strcat(szTxt,  "AUDIO_MEDIA");

            break;

          case OSAL_C_U16_UNKNOWN_MEDIA:
            strcat(szTxt,  "UNKNOWN_MEDIA");

            break;

          default:
            strcat(szTxt,  "MEDIA_TYPE_XXXXX");
          } //End of switch
        }
        break;

      case OSAL_C_U16_NOTI_TOTAL_FAILURE:
        {
          sprintf(szTxt, "  [%02d:%08ums] NOTI_TOTAL_FAILURE (0x%04X) ->",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);
          switch(u16State)
          {
          case OSAL_C_U16_DEVICE_OK:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_OK");

            break;
          case OSAL_C_U16_DEVICE_FAIL:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_FAIL");

            break;
          default:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_XXX");
          } //switch(u16State)
        }
        break;

      case OSAL_C_U16_NOTI_MODE_CHANGE:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_MODE_CHANGE",i,
                (unsigned int)u32Time);

        break;

      case OSAL_C_U16_NOTI_MEDIA_STATE:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_MEDIA_STATE (0x%04X) -> ",i,
                (unsigned int)u32Time,
                (unsigned int)u16State);

        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_NOT_READY:
          strcat(szTxt,  "OSAL_C_U16_MEDIA_NOT_READY");
          break;
        case OSAL_C_U16_MEDIA_READY:
          strcat(szTxt,  "OSAL_C_U16_MEDIA_READY");
          break;
        default:
          strcat(szTxt, "OSAL_C_U16_MEDIA_XXX");
        } //switch(u16MediaState)
        break;

      case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_DVD_OVR_TEMP",
                i,
                (unsigned int)u32Time);
        break;
      case OSAL_C_U16_NOTI_DEFECT:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_DEFECT (0x%04X) -> ",
                i,
                (unsigned int)u32Time,
                (unsigned int)u16State);
        switch(u16State)
        {
        case OSAL_C_U16_DEFECT_DISCTOC:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_DISCTOC");
          break;
        case  OSAL_C_U16_DEFECT_LOAD_EJECT:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_LOAD_EJECT");
          break;
        case OSAL_C_U16_DEFECT_LOAD_INSERT:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_LOAD_INSERT");
          break;
        case OSAL_C_U16_DEFECT_DISC:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_DISC");
          break;
        case OSAL_C_U16_DEFECT_READ_ERR:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_READ_ERR");
          break;
        case OSAL_C_U16_DEFECT_READ_OK:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_READ_OK");
          break;
        default:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_[DEFAULT]");
          break;
        } //switch(u16State)

        break;
      case OSAL_C_U16_NOTI_EJECTKEY:
        sprintf(szTxt, "[%02d:%08ums] NOTI_EJECTKEY",
                i,
                (unsigned int)u32Time);
        break;
      case OSAL_C_U16_NOTI_DEVICE_READY:
        {
          sprintf(szTxt, "[%02d:%08ums] NOTI_DEVICE_READY (0x%04X) -> ",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);
          switch(u16State)
          {
          case OSAL_C_U16_DEVICE_NOT_READY:
            strcat(szTxt,  "DEVICE_NOT_READY");

            break;
          case OSAL_C_U16_DEVICE_READY:
            strcat(szTxt,  "DEVICE_READY");

            break;
          case OSAL_C_U16_DEVICE_UV_NOT_READY:
            strcat(szTxt,  "DEVICE_UV_NOT_READY");

            break;
          case OSAL_C_U16_DEVICE_UV_READY:
            strcat(szTxt,  "DEVICE_UV_READY");

            break;
          default:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_XXX");
            break;

          } //switch(u16DeviceReady)
        }
        break;

      case OSAL_C_U16_NOTI_IDLE:
        sprintf(szTxt, "[%02d:%08ums] NOTI_IDLE",
                i,
                (unsigned int)u32Time);
        break;

      case OSAL_C_U16_NOTI_LOW_POW:
        sprintf(szTxt, "[%02d:%08ums] NOTI_LOW_POW",
                i,
                (unsigned int)u32Time);
        break;

      default:
        sprintf(szTxt, "[%02d:%08ums] NOTI_XXX",
                i,
                (unsigned int)u32Time);
      } //End of switch


      sprintf(szTxt1, ", Loader 0x%02X",
              (unsigned int)T012_aMon[i].u8LoaderInfoByte);
      strcat(szTxt, szTxt1);
      OCDTR(c2, "%s", szTxt);
    } //for(i = 0; i < T012_iMonIdx; i++)
  } //check monitored data

  if(OEDT_CD_T012_RESULT_OK_VALUE != u32ResultBitMask) /*lint !e774*/
  {
    OCDTR(c1, "OEDT_CD: T012 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T012_RESULT_OK_VALUE != u32ResultBitMask)


  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*                            OEDT_CD_T013                          */
/*****************************************************************************/

//#define OEDT_CD_T013_COUNT                                               1
#define OEDT_CD_T013_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T013_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T013_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T013_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002

#define OEDT_CD_T013_NOT_INSLOT_ERROR_BIT                       0x00000008
#define OEDT_CD_T013_EJECT_NOTI_ERROR_BIT                       0x00000010

#define OEDT_CD_T013_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T013_CLOSE_RESULT_ERROR_BIT                     0x80000000

//static tBool T013_bMediaReady;
static tBool T013_bMediaEjected;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T013_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T013_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          T013_bMediaEjected = TRUE;
          break;
        case OSAL_C_U16_INCORRECT_MEDIA:
        case OSAL_C_U16_DATA_MEDIA:
        case OSAL_C_U16_AUDIO_MEDIA:
        case OSAL_C_U16_UNKNOWN_MEDIA:
        default:
          ;
        } //End of switch
      }
      break;


    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_READY:
        //T013_bMediaReady = TRUE;
        break;
      case OSAL_C_U16_MEDIA_NOT_READY:
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
    case OSAL_C_U16_NOTI_DEVICE_READY:
    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
    case OSAL_C_U16_NOTI_MODE_CHANGE:
    default:
      OCDTR(c3, "T013_vMediaTypeNotify UNUSED: Type 0x%04X, State 0x%04X",
            (unsigned int)u16NotiType,
            (unsigned int)u16State);
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T013
 *DESCRIPTION  :  Ejects CD and monitors OSAL_C_U16_NOTI_MEDIA_CHANGE - 
                  OSAL_C_U16_MEDIA_EJECTED from PRM after CD is removed
                  from slot
 *PARAMETER    :  void
 *RETURNVALUE  :  bit coded error value
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T013(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask      = OEDT_CD_T013_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl; //L i n t  = OSAL_ERROR;
  tS32 s32Ret;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T013(void)");

  /* open device */
  hCDctrl = OSAL_IOOpen(OEDT_CD_T013_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hCDctrl)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR Open <%s>: %s",
          OEDT_CD_T013_DEVICE_CTRL_NAME, szError);
    u32ResultBitMask |= OEDT_CD_T013_OPEN_RESULT_ERROR_BIT;
  }
  else //if(OSAL_ERROR == hCDctrl)
  {
    OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X",
          OEDT_CD_T013_DEVICE_CTRL_NAME, (unsigned int)hCDctrl);
  } //else //if(OSAL_ERROR == hCDctrl)


  //T013_bMediaReady   = FALSE;
  T013_bMediaEjected = FALSE;
  //REG_NOTIFICATION
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;
    rReg.pCallback           = T013_vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T013_REG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("REG_NOTIFICATION", s32Ret, 0);
  }//REG_NOTIFICATION

  //GETLOADERINFO - wait until cd is in SLOT
  {
    OSAL_trLoaderInfo rInfo;
    tBool bCDInSlot = FALSE;
    OSAL_tMSecond startMS;
    OSAL_tMSecond diffMS;
    startMS = OSAL_ClockGetElapsedTime();

    do
    {

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      if(OSAL_OK == s32Ret)
      {
        OCDTR(c2, "OEDT_CD: LOADERINFO: 0x%02X",
              (unsigned int)rInfo.u8LoaderInfoByte);
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_IN_SLOT:
          bCDInSlot = TRUE;
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
          //EJECTMEDIA
          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
          s32Arg = 0;
          (void)OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          /*lint -fallthrough */
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_NO_MEDIA:
        default:
          bCDInSlot = FALSE;
          (void)OSAL_s32ThreadWait(250);
          break;
        } //switch(rInfo.u8LoaderInfoByte)
      } //if(OSAL_OK == s32Ret)
      diffMS = OSAL_ClockGetElapsedTime() - startMS;
    }
    while(!bCDInSlot && (diffMS < (15000)));//GETLOADERINFO

    if(!bCDInSlot)
    {
      OCDTR(c1, "OEDT_CD: ERROR InSlot"
            " -  CD not detected in slot after %ums, "
            " LoaderInfo 0x%02X",
            (unsigned int)diffMS,
            (unsigned int)rInfo.u8LoaderInfoByte);
      u32ResultBitMask |= OEDT_CD_T013_NOT_INSLOT_ERROR_BIT;
    } //if(!bCDInSlot)
  }//GETLOADERINFO - wait while cd is in SLOT


  //OSAL_C_U16_MEDIA_EJECTED - Noti has to be
  //   receive after cd is removed from slot
  T013_bMediaEjected = FALSE;

  //wait until media ejected
  {
    tInt iCount = 0;
    tInt iEmergencyExit = 2 * 60 * 30; //wait ~30min
    while((TRUE != T013_bMediaEjected)
          &&
          (iEmergencyExit-- > 0))
    {
      //print message each 3 secoends
      if((iCount++ % 6) == 0)
      {
        OCDTR(c0, "%s", "***********************************");
        OCDTR(c0, "%s", "* REMOVE CD FROM SLOT             *");
        OCDTR(c0, "%s", "***********************************");
      }

      (void)OSAL_s32ThreadWait(500);
    } //while(!ready)

    if(iEmergencyExit < 0)
    {
      u32ResultBitMask |= OEDT_CD_T013_EJECT_NOTI_ERROR_BIT;
    } //if(iEmergencyExit < 0)
  }



  //UNREG_NOTIFICATION
  {
    OSAL_trRelNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T013_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, 0);
  }//UNREG_NOTIFICATION


  s32Ret = OSAL_s32IOClose(hCDctrl);
  if(OSAL_OK != s32Ret)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X: %s",
          (unsigned int)hCDctrl, szError);
    u32ResultBitMask |= OEDT_CD_T013_CLOSE_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OCDTR(c2, "OEDT_CD: "
          "SUCCESS CLOSE handle %u",
          (unsigned int)hCDctrl);
  } //else //if(OSAL_OK != s32Ret)


  if(OEDT_CD_T013_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T013 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T013_RESULT_OK_VALUE != u32ResultBitMask)

  vEject();
  return u32ResultBitMask;
}



/*****************************************************************************/
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*                            OEDT_CD_T050                          */
/*****************************************************************************/

#define OEDT_CD_T050_COUNT                                               1
#define OEDT_CD_T050_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T050_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T050_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T050_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T050_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T050_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T050_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T050_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T050_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T050_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T050_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T050_GETDISKTYPE_RESULT_ERROR_BIT               0x00000200
#define OEDT_CD_T050_GETDISKTYPE_TYPE_ERROR_BIT                 0x00000400
#define OEDT_CD_T050_GETMEDIAINFO_RESULT_ERROR_BIT              0x00000800
#define OEDT_CD_T050_GETMEDIAINFO_TYPE_ERROR_BIT                0x00001000
#define OEDT_CD_T050_GETMEDIAINFO_FS_ERROR_BIT                  0x00002000
#define OEDT_CD_T050_GETMEDIAINFO_MEDIAID_ERROR_BIT             0x00004000


//#define OEDT_CD_T050_READRAWDATA_RESULT_ERROR_BIT               0x00020000
//#define OEDT_CD_T050_READRAWDATA_MSF_RESULT_ERROR_BIT           0x00040000

#define OEDT_CD_T050_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T050_CLOSE_RESULT_ERROR_BIT                     0x80000000


static tBool T050_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T050_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T050_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T050_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T050
 *DESCRIPTION  :  MASCA 01 CD (UDF) required - test filesystem
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T050(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T050_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T050(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T050_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T050_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T050_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T050_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T050_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T050_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T050_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T050_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T050_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T050_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO


    //wait until media ready
    {
      tInt iEmergencyExit = 25;
      while((TRUE != T050_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }


    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T050_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if singlession cd contains only one track
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 1))
      {
        u32ResultBitMask |= OEDT_CD_T050_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)
    }//GETCDINFO



    //GETTRACKINFO
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = 1;

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T050_GETTRACKINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo:"
              " Track [%02d],"
              " StartLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)

      //check values
      if(150 != rInfo.u32LBAAddress)
      {
        u32ResultBitMask |= OEDT_CD_T050_GETTRACKINFO_VAL_LBA_ERROR_BIT;
      } //if(150 != rInfo.u32LBAAddress)

      if(0x00 == (0x04 & rInfo.u32TrackControl))
      { //data bit not set
        u32ResultBitMask |= OEDT_CD_T050_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
      } //if()
    }//GETTRACKINFO


    //GETDISKTYPE
    {
      OSAL_trDiskType rDiskType;

      rDiskType.u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
      rDiskType.u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
      s32Arg = (tS32)&rDiskType;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T050_GETDISKTYPE_RESULT_ERROR_BIT 
                          : 0;

      if(rDiskType.u8DiskType != ATAPI_C_U8_DISK_TYPE_CD)
      {
        u32ResultBitMask |=OEDT_CD_T050_GETDISKTYPE_TYPE_ERROR_BIT;

      } //if(rDiskType.u8DiskSubType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)

      vPrintOsalResult("GETDISKTYPE", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetDiskType: Type %u, SubType %u",
              (unsigned int)rDiskType.u8DiskType,
              (unsigned int)rDiskType.u8DiskSubType
            );


      } //if(s32Ret != OSAL_ERROR)
    }//GETDISKTYPE


    //GETMEDIAINFO
    {
      OSAL_trMediaInfo rInfo;

      rInfo.nTimeZone          = 0;
      rInfo.szcCreationDate[0] ='\0';
      rInfo.szcMediaID[0]      ='\0';
      rInfo.u8FileSystemType   = OSAL_C_U8_FS_TYPE_UNKNOWN;
      rInfo.u8MediaType        = OSAL_C_U8_UNKNOWN_MEDIA;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T050_GETMEDIAINFO_RESULT_ERROR_BIT 
                          : 0;

      /*
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c0, "Type %u, FS %u, TZ %u, [%s] [%s]",(tU32)rInfo.u8MediaType,
                          (tU32)rInfo.u8FileSystemType,
                          (tU32)rInfo.nTimeZone,
                       (const char*)rInfo.szcCreationDate,
                       (const char*)szTmp
                     );*/



      if(OSAL_C_U8_DATA_MEDIA != rInfo.u8MediaType)
      {
        u32ResultBitMask |= OEDT_CD_T050_GETMEDIAINFO_TYPE_ERROR_BIT;
      } //if(OSAL_C_U8_DATA_MEDIA == rInfo.u8MediaType)

      if(OSAL_C_U8_FS_TYPE_UDFS != rInfo.u8FileSystemType)
      {
        u32ResultBitMask |= OEDT_CD_T050_GETMEDIAINFO_FS_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UDFS == rInfo.u8FileSystemType)
      
      if(memcmp(rInfo.szcMediaID, OEDT_CD_HASH_MASCA_01, OEDT_CD_HASH_SIZE)
         != 0)
      {
        u32ResultBitMask |= OEDT_CD_T050_GETMEDIAINFO_MEDIAID_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      vPrintOsalResult("GETMEDIAINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c2, " MediaInfo: MediaType %u"
              " FS %u TimeZone %u"
              " Date [%s] ID [%s]",
              (unsigned int)rInfo.u8MediaType,
              (unsigned int)rInfo.u8FileSystemType,
              (unsigned int)rInfo.nTimeZone,
              (const char*)rInfo.szcCreationDate,
              (const char*)szTmp
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETMEDIAINFO




    //READRAWDATA (NO CD inserted!)
    #if 0
    if(0)
    {
      tS8 *ps8Buf;
      tU32 u32LBA       = (tU32)iCount;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawInfo rRRInfo;

      ps8Buf = (tS8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(ps8Buf)
      {
        memset(ps8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
      }

      rRRInfo.u32NumBlocks  = u32NumBlocks;
      rRRInfo.u32LBAAddress = u32LBA;
      rRRInfo.ps8Buffer     = ps8Buf;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
      s32Arg = (tS32)&rRRInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T050_READRAWDATA_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("READRAWDATA", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        //tU32 u32Block, u32BlockIndex;
        OCDTR(c2, "ReadRawDataLBA:"
              " LBA %u,"
              " Blocks %u,"
              " BufferAddress %p",
              (unsigned int)rRRInfo.u32LBAAddress,
              (unsigned int)rRRInfo.u32NumBlocks,
              rRRInfo.ps8Buffer
            );
      } //if(bIOCtrlOK(s32Fun, s32Ret))

      if(ps8Buf != NULL)
      {
        free(ps8Buf);
      } //if(ps8Buf != NULL)
    }//READRAWDATA
    #endif

    #if 0
    //READRAWDATA_MSF
    if(0)
    {
      tS8 *ps8Buf;
      tU8  u8Min   = 0;
      tU8  u8Sec   = 2;
      tU8  u8Frame = 0;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawMSFInfo rRRInfo;

      ps8Buf = (tS8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(ps8Buf)
      {
        memset(ps8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
      }

      rRRInfo.u32NumBlocks  = u32NumBlocks;
      rRRInfo.u8Minute      = u8Min;
      rRRInfo.u8Second      = u8Sec;
      rRRInfo.u8Frame       = u8Frame;
      rRRInfo.ps8Buffer     = ps8Buf;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF;
      s32Arg = (tS32)&rRRInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ECode = OSAL_u32ErrorCode();
      u32ResultBitMask |= (u32ECode != OSAL_E_MEDIA_NOT_AVAILABLE) ?
                          OEDT_CD_T050_READRAWDATA_MSF_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("READRAWDATA_MSF", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        //tU32 u32Block, u32BlockIndex;
        OCDTR(c2, " ReadRawDataMSF:"
              " MSF %02u:%02u:%02u,"
              " Blocks %u,"
              " BufferAddress %p",
              (unsigned int)rRRInfo.u8Minute,
              (unsigned int)rRRInfo.u8Second,
              (unsigned int)rRRInfo.u8Frame,
              (unsigned int)rRRInfo.u32NumBlocks,
              rRRInfo.ps8Buffer
            );
      } //if(bIOCtrlOK(s32Fun, s32Ret))

      if(ps8Buf != NULL)
      {
        free(ps8Buf);
      } //if(ps8Buf != NULL)
    }//READRAWDATA_MSF
    #endif 


    //UNREG_NOTIFICATION

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T050_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T050_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T050_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T050 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T050_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T050 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T050_RESULT_OK_VALUE != u32ResultBitMask)

  vEject();
  return u32ResultBitMask;
}




/*****************************************************************************/
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*                            OEDT_CD_T100                          */
/*****************************************************************************/

#define OEDT_CD_T100_COUNT                                               2
#define OEDT_CD_T100_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T100_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T100_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T100_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T100_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T100_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T100_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T100_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T100_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T100_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T100_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T100_GETDISKTYPE_RESULT_ERROR_BIT               0x00000200
#define OEDT_CD_T100_GETDISKTYPE_TYPE_ERROR_BIT                 0x00000400
#define OEDT_CD_T100_GETMEDIAINFO_RESULT_ERROR_BIT              0x00000800
#define OEDT_CD_T100_GETMEDIAINFO_TYPE_ERROR_BIT                0x00001000
#define OEDT_CD_T100_GETMEDIAINFO_FS_ERROR_BIT                  0x00002000
#define OEDT_CD_T100_GETMEDIAINFO_MEDIAID_ERROR_BIT             0x00004000
#define OEDT_CD_T100_READRAWLBA_RESULT_ERROR_BIT                0x00008000
#define OEDT_CD_T100_READRAWLBA_VERIFY_ERROR_BIT                0x00010000
#define OEDT_CD_T100_READRAWLBA_SPEED_ERROR_BIT                 0x00020000
#define OEDT_CD_T100_READRAWLBA_UNCACHED_RESULT_ERROR_BIT       0x00040000
//#define OEDT_CD_T100_READRAWLBA_UNCACHED_VERIFY_ERROR_BIT       0x00080000
//#define OEDT_CD_T100_READRAWLBA_UNCACHED_SPEED_ERROR_BIT        0x00100000

#define OEDT_CD_T100_READRAWMSF_RESULT_ERROR_BIT                0x00200000
#define OEDT_CD_T100_READRAWMSF_SPEED_ERROR_BIT                 0x00400000

#define OEDT_CD_T100_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T100_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T100_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T100_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T100_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T100_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T100
 *DESCRIPTION  :  MASCA 02 CD (ISO9660) required - test FS, read+verify sectors
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T100(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T100_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T100(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T100_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T100_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T100_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T100_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T100_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T100_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T100_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T100_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T100_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }


    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T100_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T100_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO


    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T100_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if singlession cd contains only one track
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 1))
      {
        u32ResultBitMask |= OEDT_CD_T100_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)
    }//GETCDINFO



    //GETTRACKINFO
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = 1;

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T100_GETTRACKINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo:"
              " Track [%02d],"
              " StartLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)

      //check values
      if(150 != rInfo.u32LBAAddress)
      {
        u32ResultBitMask |= OEDT_CD_T100_GETTRACKINFO_VAL_LBA_ERROR_BIT;
      } //if(150 != rInfo.u32LBAAddress)

      if(0x00 == (0x04 & rInfo.u32TrackControl))
      { //data bit missing
        u32ResultBitMask |= OEDT_CD_T100_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
      } //if()
    }//GETTRACKINFO


    //GETDISKTYPE
    {
      OSAL_trDiskType rDiskType;

      rDiskType.u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
      rDiskType.u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
      s32Arg = (tS32)&rDiskType;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T100_GETDISKTYPE_RESULT_ERROR_BIT 
                          : 0;

      if(rDiskType.u8DiskType != ATAPI_C_U8_DISK_TYPE_CD)
      {
        u32ResultBitMask |=OEDT_CD_T100_GETDISKTYPE_TYPE_ERROR_BIT;

      } //if(rDiskType.u8DiskSubType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)

      vPrintOsalResult("GETDISKTYPE", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetDiskType: Type %u, SubType %u",
              (unsigned int)rDiskType.u8DiskType,
              (unsigned int)rDiskType.u8DiskSubType
            );


      } //if(s32Ret != OSAL_ERROR)
    }//GETDISKTYPE


    //GETMEDIAINFO
    {
      OSAL_trMediaInfo rInfo;

      rInfo.nTimeZone          = 0;
      rInfo.szcCreationDate[0] ='\0';
      rInfo.szcMediaID[0]      ='\0';
      rInfo.u8FileSystemType   = OSAL_C_U8_FS_TYPE_UNKNOWN;
      rInfo.u8MediaType        = OSAL_C_U8_UNKNOWN_MEDIA;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T100_GETMEDIAINFO_RESULT_ERROR_BIT 
                          : 0;

      /*
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c0, "Type %u, FS %u, TZ %u, [%s] [%s]",(tU32)rInfo.u8MediaType,
                          (tU32)rInfo.u8FileSystemType,
                          (tU32)rInfo.nTimeZone,
                       (const char*)rInfo.szcCreationDate,
                       (const char*)szTmp
                     );*/



      if(OSAL_C_U8_DATA_MEDIA != rInfo.u8MediaType)
      {
        u32ResultBitMask |= OEDT_CD_T100_GETMEDIAINFO_TYPE_ERROR_BIT;
      } //if(OSAL_C_U8_DATA_MEDIA == rInfo.u8MediaType)

      if(OSAL_C_U8_FS_TYPE_CDFS != rInfo.u8FileSystemType)
      {
        u32ResultBitMask |= OEDT_CD_T100_GETMEDIAINFO_FS_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UDFS == rInfo.u8FileSystemType)

      if(memcmp(rInfo.szcMediaID, OEDT_CD_HASH_MASCA_02, OEDT_CD_HASH_SIZE)
         != 0)
      {
        u32ResultBitMask |= OEDT_CD_T100_GETMEDIAINFO_MEDIAID_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      vPrintOsalResult("GETMEDIAINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c2, " MediaInfo: MediaType %u"
              " FS %u TimeZone %u"
              " Date [%s] ID [%s]",
              (unsigned int)rInfo.u8MediaType,
              (unsigned int)rInfo.u8FileSystemType,
              (unsigned int)rInfo.nTimeZone,
             (const char*)rInfo.szcCreationDate,
              (const char*)szTmp
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETMEDIAINFO




    //READRAWDATA LBA
    {
      tU8 *pu8Buf;
      tU32 u32LBA;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawInfo rRRInfo;
      tU32 u32NumberOfSecsToRead = 150;
      tU32 u32SCnt;
      OSAL_tMSecond sMS;
      OSAL_tMSecond dMS;
      double dMSpSector;

      sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
        u32SCnt = u32NumberOfSecsToRead;
        while((u32SCnt-- > 0)
              &&
              (u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE))
        {
          //sector LBA 21 is first sector of single file on Test-CD "MASCA_02"
          //sector number is written in sectordata!
          // Remark: highes readable sector depends on burn quality
          // cd buned on 17.december.2013: highest readable sector LBA 356441
          //u32LBA = 21 + (u32SCnt * (356444 / u32NumberOfSecsToRead));
          u32LBA = 21 + (u32SCnt * ((356420-u32SCnt) / u32NumberOfSecsToRead));
          if(u32SCnt == 0)
          { //try to read last sector of cd "MASCA 02"
            //u32LBA = 21 + 356444;
            u32LBA = 21 + 356420; //L i n t - u32SCnt;
          } //if(u32SCnt == 0)

          OCDTR(c2, "ReadRawDataLBA SectorCount %4u, "
                "Start-LBA %5u, "
                "NumberOfSectors %5u",
                (unsigned int)u32SCnt,
                (unsigned int)u32LBA,
                (unsigned int)u32NumBlocks);


          rRRInfo.u32NumBlocks  = u32NumBlocks;
          rRRInfo.u32LBAAddress = u32LBA;  /*LBA , NOT ZLBA!*/
          rRRInfo.ps8Buffer     = (tS8*)pu8Buf;

          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
          s32Arg = (tS32)&rRRInfo;
          s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                              OEDT_CD_T100_READRAWLBA_RESULT_ERROR_BIT
                              : 0;
          vPrintOsalResult("READRAWDATALBA", s32Ret, iCount);

          if(u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE)
          { // verify read data
            tU32 u32C, u32ReadData, u32CalcData;
            for(u32C = 0;
               (u32C < (OEDT_CD_BPS-4))
               &&
               (u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE);
               u32C += 4)
            {
              u32CalcData = u32LBA | ((u32C/4) << 23);
              u32ReadData = OEDT_CD_8TO32(pu8Buf[u32C + 3],
                                          pu8Buf[u32C + 2],
                                          pu8Buf[u32C + 1],
                                          pu8Buf[u32C + 0]);
              if(u32CalcData != u32ReadData)
              {
                OCDTR(c1, "ReadRawDataLBA Verify ERROR: "
                      "LBA %u, "
                      "ByteOffset 0x%04X, "
                      "Calc 0x%08X !=  Read 0x%08X",
                      (unsigned int)u32LBA,
                      (unsigned int)u32C,
                      (unsigned int)u32CalcData,
                      (unsigned int)u32ReadData
                    );
                vTraceHex(pu8Buf, OEDT_CD_BPS, 16);

                u32ResultBitMask |= OEDT_CD_T100_READRAWLBA_VERIFY_ERROR_BIT;
              } //if(u32CalcData != u32ReadData)
            } //for(u32C = 0; u32C < OEDT_CD_BPS; u32C += 4)

            OCDTR(c2, "ReadRawDataLBA:"
                  " LBA %u,"
                  " Blocks %u,"
                  " BufferAddress %p",
                  (unsigned int)rRRInfo.u32LBAAddress,
                  (unsigned int)rRRInfo.u32NumBlocks,
                  rRRInfo.ps8Buffer
                );
          } //if(u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE)
        } //while(u32SCnt-- > 0 && !error)

        free(pu8Buf);
      } //if(NULL != pu8Buf)


      dMS = OSAL_ClockGetElapsedTime() - sMS;
      dMSpSector = (double) ((double)dMS /  (double)u32NumberOfSecsToRead);
      OCDTR(c2, "ReadRawDataLBA: "
            "NumberOfSecsRead %u, "
            "Time %u ms, "
            "Speed %.1f ms/sector",
            (unsigned int)u32NumberOfSecsToRead,
            (unsigned int)dMS,
            dMSpSector
          );

      //check average read time per sector
      if(dMSpSector > 300.0)
      {
        OCDTR(c1, "ReadRawDataLBA Speed ERROR: "
              " %.1f ms/sector too slow (max. 300ms per sector)",
              dMSpSector
            );
        u32ResultBitMask |= OEDT_CD_T100_READRAWLBA_SPEED_ERROR_BIT;
      } //if(dMS > 5000)
    }//READRAWDATA LBA


    //READRAWDATA MSF
    {
      tU8 *pu8Buf;
      tU8 au8M[] = {0,0, 0,40,75};
      tU8 au8S[] = {2,2, 2, 0, 0};
      tU8 au8F[] = {0,1,16, 0,74};
      tU8 u8M,u8S,u8F;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawMSFInfo rRRInfo = {0};
      tU32 u32NumberOfSecsToRead = (tU32)OEDT_CD_ARSIZE(au8M);
      tU32 u32SCnt;
      OSAL_tMSecond sMS;
      OSAL_tMSecond dMS;
      double dMSpSector;

      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rRRInfo);

      sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
        u32SCnt = 0;
        while((u32SCnt < u32NumberOfSecsToRead)
              &&
              (u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE))
        {

          u8M = au8M[u32SCnt];
          u8S = au8S[u32SCnt];
          u8F = au8F[u32SCnt];

          OCDTR(c2, "ReadRawDataMSF Count %4u, MSF %02u:%02u:%02u", 
                (unsigned int)u32SCnt,
                (unsigned int)u8M,
                (unsigned int)u8S,
                (unsigned int)u8F);

          rRRInfo.u32NumBlocks  = u32NumBlocks;
          rRRInfo.u8Minute      = u8M;
          rRRInfo.u8Second      = u8S;
          rRRInfo.u8Frame       = u8F;
          rRRInfo.ps8Buffer     = (tS8*)pu8Buf;
          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF;
          s32Arg = (tS32)&rRRInfo;
          s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                              OEDT_CD_T100_READRAWMSF_RESULT_ERROR_BIT
                              : 0;
          vPrintOsalResult("READRAWDATAMSF", s32Ret, iCount);
          u32SCnt++;
        } //while(u32SCnt-- > 0 && !error)

        free(pu8Buf);
      } //if(NULL != pu8Buf)


      dMS = OSAL_ClockGetElapsedTime() - sMS;
      dMSpSector = (double) ((double)dMS /  (double)u32NumberOfSecsToRead);
      OCDTR(c2, "ReadRawDataMSF: "
            "NumberOfSecsRead %u, "
            "Time %u ms, "
            "Speed %.1f ms/sector",
            (unsigned int)u32NumberOfSecsToRead,
            (unsigned int)dMS,
            dMSpSector
          );

      //check average read time per sector
      if(dMSpSector > 1000.0)
      {
        OCDTR(c1, "ReadRawDataMSF Speed ERROR: "
              " %.1f ms/sector too slow (max. 1000ms per sector)",
              dMSpSector
            );
        u32ResultBitMask |= OEDT_CD_T100_READRAWMSF_SPEED_ERROR_BIT;
      } //if(dMS > xxx)
    }//READRAWDATA MSF

    //READRAWDATA LBA	UNCACHED
    {
      tU8 *pu8Buf;
      tU32 u32LBA;
      tU32 u32NumBlocks = 1;
      OSAL_trReadRawInfo rRRInfo;
      tU32 u32NumberOfSecsToRead = 25;
      tU32 u32SCnt;
      //OSAL_tMSecond sMS;
      //OSAL_tMSecond dMS;
      //double dMSpSector;

      //sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
        u32SCnt = u32NumberOfSecsToRead;
        while((u32SCnt-- > 0)
              &&
              (u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE))
        {
          //sector LBA 21 is first sector of single file on Test-CD "MASCA_02"
          //sector number is written in sectordata!
          // cd buned on 17.december.2013: highest readable sector LBA 356441
          //u32LBA = 21 + (u32SCnt * (356444 / u32NumberOfSecsToRead));
          //u32LBA = 21 + (u32SCnt * (356444 / u32NumberOfSecsToRead));
          u32LBA = 21 + (u32SCnt * ((356420-u32SCnt) / u32NumberOfSecsToRead));
          if(u32SCnt == 0)
          { //try to read last sector of cd "MASCA 02"
            //u32LBA = 21 + 356444;
            u32LBA = 21 + 356420; //L i n t  - u32SCnt;
          } //if(u32SCnt == 0)

          OCDTR(c2, "ReadRawDataLBAUncached Count %4u, LBA %5u, Blocks %u", 
                (unsigned int)u32SCnt,
                (unsigned int)u32LBA,
                (unsigned int)u32NumBlocks);


          rRRInfo.u32NumBlocks  = u32NumBlocks;
          rRRInfo.u32LBAAddress = u32LBA;  /*LBA , NOT ZLBA!*/
          rRRInfo.ps8Buffer   = (tS8*)pu8Buf;
          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATAUNCACHED;
          s32Arg = (tS32)&rRRInfo;
          s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                              OEDT_CD_T100_READRAWLBA_UNCACHED_RESULT_ERROR_BIT
                              : 0;
          vPrintOsalResult("READRAWDATALBAUncached", s32Ret, iCount);

          if(u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE)
          {
            OCDTR(c2, "ReadRawDataLBAUncached:"
                  " LBA %u,"
                  " Blocks %u,"
                  " BufferAddress %p",
                  (unsigned int)rRRInfo.u32LBAAddress,
                  (unsigned int)rRRInfo.u32NumBlocks,
                  rRRInfo.ps8Buffer
                );
          } //if(u32ResultBitMask == OEDT_CD_T100_RESULT_OK_VALUE)
        } //while(u32SCnt-- > 0 && !error)

        free(pu8Buf);
      } //if(NULL != pu8Buf)
    }//READRAWDATA LBA	UNCACHED

    //UNREG_NOTIFICATION

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T100_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T100_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T100_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T100 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T100_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T100 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T100_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}


/*****************************************************************************/
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*                            OEDT_CD_T101                          */
/*****************************************************************************/

#define OEDT_CD_T101_COUNT                                               1
#define OEDT_CD_T101_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T101_FORWARD_JUMP_READCOUNT                             20

#define OEDT_CD_T101_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T101_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T101_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T101_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T101_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T101_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T101_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T101_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T101_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T101_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T101_GETTRACKINFO_MAX_RESULT_ERROR_BIT          0x00000200
#define OEDT_CD_T101_GETTRACKINFO_MAX_VAL_LBA_ERROR_BIT         0x00000400

#define OEDT_CD_T101_READRAWLBA_RESULT_ERROR_BIT                0x00004000
#define OEDT_CD_T101_READRAWLBA_VERIFY_ERROR_BIT                0x00008000
#define OEDT_CD_T101_READRAWLBA_SPEED_ERROR_BIT                 0x00010000
#define OEDT_CD_T101_READRAWLBA_CONT_RESULT_ERROR_BIT           0x00020000
#define OEDT_CD_T101_READRAWLBA_CONT_SPEED_ERROR_BIT            0x00040000

#define OEDT_CD_T101_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T101_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T101_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T101_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T101_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T101_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T101
 *DESCRIPTION  :  MASCA 02 CD (ISO9660) required - read sectors +
 *                performance measurement
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T101(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T101_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  tU32 u32LBAMax = 0;
  tU32 u32LBAFirst = 0;

  //tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T101(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T101_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T101_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T101_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T101_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T101_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T101_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T101_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T101_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T101_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //after media ready from PRM
    //it takes some time, until dev_cdctrl is notified
    // because ISO-FS is blocking SED-callback
    // while reading from CD!
    //(void)OSAL_s32ThreadWait(8000);


    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T101_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T101_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO



    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T101_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if singlession cd contains only one track
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 1))
      {
        u32ResultBitMask |= OEDT_CD_T101_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)
    }//GETCDINFO



    //GETTRACKINFO FIRST TRACK
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = 1;

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T101_GETTRACKINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo:"
              " Track [%02d],"
              " StartLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)

      //check values
      if(150 != rInfo.u32LBAAddress)
      {
        u32ResultBitMask |= OEDT_CD_T101_GETTRACKINFO_VAL_LBA_ERROR_BIT;
      } //if(150 != rInfo.u32LBAAddress)
      u32LBAFirst = rInfo.u32LBAAddress - 150;

      if(0x00 == (0x04 & rInfo.u32TrackControl))
      { //data bit missing
        u32ResultBitMask |= OEDT_CD_T101_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
      } //if()
    }//GETTRACKINFO FIRST

    //GETTRACKINFO MAX LBA On CD
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = 255; //special: get max LBA on CD

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T101_GETTRACKINFO_MAX_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo Max:"
              " Track [%02d],"
              " MaxLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)

      //check values, MASCA 02 CD: max ZLBA == 356625
      if(356625 != rInfo.u32LBAAddress)
      {
        OCDTR(c1, " ERROR: GetTrackInfo: wrong max LBA:"
              " MaxLBA read from CD [%u], "
              " Expected MaxLBA [356625]",
              (unsigned int)rInfo.u32LBAAddress
            );
        u32ResultBitMask |= OEDT_CD_T101_GETTRACKINFO_MAX_VAL_LBA_ERROR_BIT;
      } //if(150 != rInfo.u32LBAAddress)
      u32LBAMax = rInfo.u32LBAAddress - 150;

    }//GETTRACKINFO MAX LBA On CD

    //READRAWDATA LBA forward with jumps
    {
      tU8 *pu8Buf;
      tU32 u32LBA;
      const tU32 u32NumBlocks = 167;
      OSAL_trReadRawInfo rRRInfo;
      tS32 s32ReadCount = 0;
      tU32 u32SCnt = 0;
      OSAL_tMSecond sMS;
      OSAL_tMSecond dMS;
      double dMSpSector;

      sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF, u32NumBlocks * OEDT_CD_BPS);
        while((s32ReadCount < OEDT_CD_T101_FORWARD_JUMP_READCOUNT)
              &&
              (u32ResultBitMask == OEDT_CD_T101_RESULT_OK_VALUE))
        {
          //sector LBA 21 is first sector of single file on Test-CD "MASCA_02"
          //sector number is written in sectordata!
          u32LBA = 21 + ((tU32)s32ReadCount * (u32LBAMax - u32NumBlocks)
                         / OEDT_CD_T101_FORWARD_JUMP_READCOUNT); 

          OCDTR(c2, "ReadRawDataLBA LoopCount %4u, LBA %5u, SecCount %u", 
                (unsigned int)u32SCnt,
                (unsigned int)u32LBA,
                (unsigned int)u32NumBlocks);


          rRRInfo.u32NumBlocks  = u32NumBlocks;
          rRRInfo.u32LBAAddress = u32LBA;  /*LBA , NOT ZLBA!*/
          rRRInfo.ps8Buffer     = (tS8*)pu8Buf;

          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
          s32Arg = (tS32)&rRRInfo;
          s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                              OEDT_CD_T101_READRAWLBA_RESULT_ERROR_BIT
                              : 0;
          vPrintOsalResult("READRAWDATALBA", s32Ret, iCount);

          if(u32ResultBitMask == OEDT_CD_T101_RESULT_OK_VALUE)
          { // verify read data
            tU32 u32S;
            for(u32S = 0 ; u32S < u32NumBlocks; u32S++)
            {

              tU32 u32C, u32ReadData, u32CalcData, u32I;
              for(u32C = 0;
                 (u32C < (OEDT_CD_BPS-4))
                 &&
                 (u32ResultBitMask == OEDT_CD_T101_RESULT_OK_VALUE);
                 u32C += 4)
              {
                u32CalcData = (u32LBA + u32S) | ((u32C/4) << 23);
                u32I = (u32S * OEDT_CD_BPS) + u32C;
                u32ReadData = OEDT_CD_8TO32(pu8Buf[u32I + 3],
                                            pu8Buf[u32I + 2],
                                            pu8Buf[u32I + 1],
                                            pu8Buf[u32I + 0]);
                if(u32CalcData != u32ReadData)
                {
                  OCDTR(c1, "ReadRawDataLBA Verify ERROR: "
                        "LBA %u, "
                        "ByteOffset 0x%04X, "
                        "Calc 0x%08X !=  Read 0x%08X",
                        (unsigned int)(u32LBA+u32S),
                        (unsigned int)u32C,
                        (unsigned int)u32CalcData,
                        (unsigned int)u32ReadData
                      );
                  vTraceHex(pu8Buf, OEDT_CD_BPS, 16);

                  u32ResultBitMask |= OEDT_CD_T101_READRAWLBA_VERIFY_ERROR_BIT;
                } //if(u32CalcData != u32ReadData)
              } //for(u32C = 0; u32C < OEDT_CD_BPS; u32C += 4)
            } //for(u32S = 0 ; u32S > u32NumBlocks; u32S++)
            OCDTR(c2, "ReadRawDataLBA:"
                  " LBA %u,"
                  " Blocks %u,"
                  " BufferAddress %p",
                  (unsigned int)rRRInfo.u32LBAAddress,
                  (unsigned int)rRRInfo.u32NumBlocks,
                  rRRInfo.ps8Buffer
                );
          } //if(u32ResultBitMask == OEDT_CD_T101_RESULT_OK_VALUE)

          u32SCnt += u32NumBlocks;
          s32ReadCount++;
        } //while((s32ReadCount < 20) && ...)

        free(pu8Buf);
      } //if(NULL != pu8Buf)


      dMS = OSAL_ClockGetElapsedTime() - sMS;
      dMSpSector = 0.0;
      if(u32SCnt != 0)
      {
        dMSpSector = (double) ((double)dMS /  (double)u32SCnt);
      }
      OCDTR(c2, "ReadRawDataLBA: "
            "NumberOfSectorsRead %u, "
            "Time %u ms, "
            "Speed %.1f ms/sector",
            (unsigned int)u32SCnt,
            (unsigned int)dMS,
            dMSpSector
          );

      //check average read time per sector
      if(dMSpSector > 12.0)
      {
        OCDTR(c1, "ReadRawDataLBA Speed ERROR: "
              " %.1f ms/sector too slow (max. 12ms)",
              dMSpSector
            );
        u32ResultBitMask |= OEDT_CD_T101_READRAWLBA_SPEED_ERROR_BIT;
      } //if(dMS > 5000)
    }//READRAWDATA LBA


    //READRAWDATA LBA continuous, speedcheck
    {
      tU8 *pu8Buf;
      tU32 u32LBA;
      tU32 u32NumBlocks = 5000; //read 10MB
      OSAL_trReadRawInfo rRRInfo = {0};
      OSAL_tMSecond sMS;
      OSAL_tMSecond dMS;
      double dMSpSector;

      (void)rRRInfo;

      OCDTR(c2, "Read Continuous: Number of Sectors [%d]",
            (unsigned int)u32NumBlocks);

      sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
        u32LBA = u32LBAFirst;  //start sector

        rRRInfo.u32NumBlocks  = u32NumBlocks;
        rRRInfo.u32LBAAddress = u32LBA;  /*LBA , NOT ZLBA!*/
        rRRInfo.ps8Buffer     = (tS8*)pu8Buf;

        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
        s32Arg = (tS32)&rRRInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T101_READRAWLBA_CONT_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("READRAWDATALBA CONT", s32Ret, iCount);
        free(pu8Buf);
      } //if(NULL != pu8Buf)

      dMS = OSAL_ClockGetElapsedTime() - sMS;
      dMSpSector = (double) ((double)dMS /  (double)u32NumBlocks);
      OCDTR(c2, "ReadRawDataLBA Continuous: "
            "NumberOfSecsRead %u, "
            "Time %u ms, "
            "Speed %.1f ms/sector",
            (unsigned int)u32NumBlocks,
            (unsigned int)dMS,
            dMSpSector
          );

      //check average read time per sector
      if(dMSpSector > 10.0)
      {
        OCDTR(c1, "ReadRawDataLBA continuous Speed ERROR: "
              " %.1f ms/sector too slow (max. 10ms)",
              dMSpSector
            );
        u32ResultBitMask |= OEDT_CD_T101_READRAWLBA_CONT_SPEED_ERROR_BIT;
      } //if(dMS > 5000)
    }//READRAWDATA LBA continuous

    //UNREG_NOTIFICATION
    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T101_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T101_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T101_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T101 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T101_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T101 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T101_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}


#if 0
/*****************************************************************************/
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*                            OEDT_CD_T102                          */
/*****************************************************************************/

#define OEDT_CD_T102_COUNT                                               1
#define OEDT_CD_T102_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T102_TE_FILE_NAME                    "/cdrom/LEER0000.DAT"

#define OEDT_CD_T102_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T102_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T102_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T102_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T102_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T102_OPEN_FILE_ERROR_BIT                        0x00000010
#define OEDT_CD_T102_ALLOC_ERROR_BIT                            0x00000020
#define OEDT_CD_T102_SEEK_FILE_ERROR_BIT                        0x00000040
#define OEDT_CD_T102_READ_FILE_ERROR_BIT                        0x00000080
#define OEDT_CD_T102_READ_FILE_SPEED_ERROR_BIT                  0x00000100

#define OEDT_CD_T102_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T102_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T102_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T102_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T102_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T102_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T102
 *DESCRIPTION  :  MASCA 02 CD (ISO9660) required - read file from /cdrom
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T102(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T102_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 OEDT_CD_T102(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T102_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T102_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T102_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T102_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T102_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T102_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T102_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T102_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T102_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }


    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T102_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T102_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)

      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO


    //UNREG_NOTIFICATION
    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T102_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T102_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T102_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)




    //read from file
    {
      tS32 s32Val;
      ID fd;
      int size;
      tU16 u16OpenMode = 0; //O_TEXT;
      unsigned char* pBuffer;

      fd = open(OEDT_CD_T102_TE_FILE_NAME, O_RDONLY,u16OpenMode);
      if(fd > E_OK)
      {
        size = s32GetFileSize(fd);
        OCDTR(c2, "OEDT_CD: SUCCESS Open file <%s> == 0x%08X, size %u,"
              "(count %d)",
              OEDT_CD_T102_TE_FILE_NAME,
              (unsigned int)fd,
              (unsigned int)size,
              iCount);

        size = 2 * 1024 * 1024; //Read 2MB
        pBuffer = (unsigned char*)OSAL_pvMemoryAllocate((tU32)size);
        if(NULL != pBuffer)
        {
          memset(pBuffer,0,(tU32)size);
          s32Val = lseek(fd,0,SEEK_SET);
          if(s32Val < E_OK)
          {
            OCDTR(c1, "OEDT_CD: ERROR Seek 0 file <%s> == 0x%08X,"
                  "(count %d)",
                  OEDT_CD_T102_TE_FILE_NAME,
                  (unsigned int)fd,
                  iCount);
            s32Val = -1;
            u32ResultBitMask |= OEDT_CD_T102_SEEK_FILE_ERROR_BIT;

          }
          else //if(s32Val < E_OK)
          {
            OSAL_tMSecond sMS;
            OSAL_tMSecond diffMS;
            double dBytesPerSecond;

            sMS = OSAL_ClockGetElapsedTime();

            s32Val = read(fd, pBuffer, (tU32)size);
            if(s32Val != size)
            {
              OCDTR(c1, "OEDT_CD: ERROR Read file <%s> == 0x%08X,"
                    " Read size %u != %u,"
                    "(count %d)",
                    OEDT_CD_T102_TE_FILE_NAME,
                    (unsigned int)fd,
                    (unsigned int)s32Val,
                    (unsigned int)size,
                    iCount);
              s32Val = -1;
              u32ResultBitMask |= OEDT_CD_T102_READ_FILE_ERROR_BIT;
            }
            else //if(s32Val != size)
            {
              OCDTR(c2, "OEDT_CD: SUCCESS Read file <%s> == 0x%08X, size %u,"
                    "(count %d)",
                    OEDT_CD_T102_TE_FILE_NAME,
                    (unsigned int)fd,
                    (unsigned int)s32Val,
                    iCount);

              //check read speed
              diffMS = OSAL_ClockGetElapsedTime() - sMS;
              if(diffMS > 0)
              {
                dBytesPerSecond = ((double)size
                                   / (double)diffMS)
                                  * 1000.0;
              }
              else //if(dMS > 0)
              {
                dBytesPerSecond = -1.0;
              } //else //if(dMS > 0)
              OCDTR(c2, "ReadFile: "
                    "BytesRead %u, "
                    "Time %u ms, "
                    "Speed %.1f bytes per second",
                    (unsigned int)size,
                    (unsigned int)diffMS,
                    dBytesPerSecond
                  );

              //check average read time per sector
              if(dBytesPerSecond < 200000.0) //should 314000 at full 2x Speed
              {
                OCDTR(c1, "ReadFile Speed ERROR: "
                      " %.1f Bytes/Second too slow (min 200000 Bytes/sec)",
                      dBytesPerSecond
                    );
                u32ResultBitMask |= OEDT_CD_T102_READ_FILE_SPEED_ERROR_BIT;
              } //if(dMS > 5000)
            } //else //if(s32Val != size)
          } //else //if(s32Val < E_OK)
          OSAL_vMemoryFree(pBuffer);
        }
        else //if(NULL != pBuffer)
        {
          OCDTR(c1, "OEDT_CD: ERROR Allocate memory %u Bytes, (count %d)",
                (unsigned int)size, iCount);
          u32ResultBitMask |= OEDT_CD_T102_ALLOC_ERROR_BIT;
        } //else //if(NULL != pBuffer)
        close(fd);
      }
      else //if(fd > E_OK)
      {
        OCDTR(c1, "OEDT_CD: ERROR Open file <%s> (count %d): %s",
              OEDT_CD_T102_TE_FILE_NAME, iCount, szError);
        u32ResultBitMask |= OEDT_CD_T102_OPEN_FILE_ERROR_BIT;
        //srt2hi TraceIOString((char*)"Open File Error");
      } //else //if(fd > E_OK)
    } //read from file


  } //for(iCount = 0; iCount < OEDT_CD_T102_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T102 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T102_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T102 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T102_RESULT_OK_VALUE != u32ResultBitMask)

  vEject();
  return u32ResultBitMask;
}
#endif //#if 0

/*****************************************************************************/
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*                            OEDT_CD_T150                          */
/*****************************************************************************/
#define OEDT_CD_T150_COUNT                                               2
#define OEDT_CD_T150_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T150_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T150_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T150_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T150_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T150_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T150_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T150_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T150_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T150_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T150_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T150_GETDISKTYPE_RESULT_ERROR_BIT               0x00000200
#define OEDT_CD_T150_GETDISKTYPE_TYPE_ERROR_BIT                 0x00000400
#define OEDT_CD_T150_GETMEDIAINFO_RESULT_ERROR_BIT              0x00000800
#define OEDT_CD_T150_GETMEDIAINFO_TYPE_ERROR_BIT                0x00001000
#define OEDT_CD_T150_GETMEDIAINFO_FS_ERROR_BIT                  0x00002000
#define OEDT_CD_T150_GETMEDIAINFO_MEDIAID_ERROR_BIT             0x00004000

#define OEDT_CD_T150_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T150_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T150_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T150_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T150_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T150_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T150
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required, detect CDDA cd
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T150(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T150_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T150(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T150_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T150_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T150_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T150_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T150_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T150_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T150_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T150_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T150_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T150_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO


    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T150_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //after media ready from PRM
    //it takes some time, until dev_cdctrl is notified
    // because ISO-FS is blocking SED-callback
    // while reading from CD!
    //(void)OSAL_s32ThreadWait(8000);

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T150_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 16))
      {
        u32ResultBitMask |= OEDT_CD_T150_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO



    //GETTRACKINFO
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T150_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T150_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T150_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO


    //GETDISKTYPE
    {
      OSAL_trDiskType rDiskType;

      rDiskType.u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
      rDiskType.u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
      s32Arg = (tS32)&rDiskType;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T150_GETDISKTYPE_RESULT_ERROR_BIT 
                          : 0;

      if(rDiskType.u8DiskType != ATAPI_C_U8_DISK_TYPE_CD)
      {
        u32ResultBitMask |=OEDT_CD_T150_GETDISKTYPE_TYPE_ERROR_BIT;

      } //if(rDiskType.u8DiskSubType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)

      vPrintOsalResult("GETDISKTYPE", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetDiskType: Type %u, SubType %u",
              (unsigned int)rDiskType.u8DiskType,
              (unsigned int)rDiskType.u8DiskSubType
            );


      } //if(s32Ret != OSAL_ERROR)
    }//GETDISKTYPE


    //GETMEDIAINFO
    {
      OSAL_trMediaInfo rInfo;

      rInfo.nTimeZone          = 0;
      rInfo.szcCreationDate[0] ='\0';
      rInfo.szcMediaID[0]      ='\0';
      rInfo.u8FileSystemType   = OSAL_C_U8_FS_TYPE_UNKNOWN;
      rInfo.u8MediaType        = OSAL_C_U8_UNKNOWN_MEDIA;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T150_GETMEDIAINFO_RESULT_ERROR_BIT 
                          : 0;

      /*
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c0, "Type %u, FS %u, TZ %u, [%s] [%s]",(tU32)rInfo.u8MediaType,
                          (tU32)rInfo.u8FileSystemType,
                          (tU32)rInfo.nTimeZone,
                       (const char*)rInfo.szcCreationDate,
                       (const char*)szTmp
                     );*/


      if(OSAL_C_U8_AUDIO_MEDIA != rInfo.u8MediaType)
      {
        u32ResultBitMask |= OEDT_CD_T150_GETMEDIAINFO_TYPE_ERROR_BIT;
      } //if(OSAL_C_U8_AUDIO_MEDIA == rInfo.u8MediaType)

      if(OSAL_C_U8_FS_TYPE_UNKNOWN != rInfo.u8FileSystemType)
      {
        u32ResultBitMask |= OEDT_CD_T150_GETMEDIAINFO_FS_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      if(memcmp(rInfo.szcMediaID, OEDT_CD_HASH_MASCA_03, OEDT_CD_HASH_SIZE)
         != 0)
      {
        u32ResultBitMask |= OEDT_CD_T150_GETMEDIAINFO_MEDIAID_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      vPrintOsalResult("GETMEDIAINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c2, " MediaInfo: MediaType %u"
              " FS %u TimeZone %u"
              " Date [%s] ID [%s]",
              (unsigned int)rInfo.u8MediaType,
              (unsigned int)rInfo.u8FileSystemType,
              (unsigned int)rInfo.nTimeZone,
              (const char*)rInfo.szcCreationDate,
              (const char*)szTmp
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETMEDIAINFO


    //UNREG_NOTIFICATION

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T150_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T150_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T150_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T150 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T150_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T150 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T150_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*                            OEDT_CD_T151                          */
/*****************************************************************************/

#define OEDT_CD_T151_COUNT                                               1
#define OEDT_CD_T151_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T151_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO
#define OEDT_CD_T151_MASCA03_TRACKS                                 (16+1)



#define OEDT_CD_T151_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T151_OPEN_CTRL_RESULT_ERROR_BIT                 0x00000001
#define OEDT_CD_T151_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000002
#define OEDT_CD_T151_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT     0x00000004
#define OEDT_CD_T151_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT    0x00000008


#define OEDT_CD_T151_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T151_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T151_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T151_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T151_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T151_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000200
#define OEDT_CD_T151_GETADDITIONALCDINFO_RESULT_ERROR_BIT       0x00000400
#define OEDT_CD_T151_SETPLAYRANGE_RESULT_ERROR_BIT              0x00000800
#define OEDT_CD_T151_PLAY_RESULT_ERROR_BIT                      0x00001000
//#define OEDT_CD_T151_WAIT_FOR_PLAY_ERROR_BIT                    0x00002000
#define OEDT_CD_T151_WAIT_FOR_COMPLETED_ERROR_BIT               0x00004000
#define OEDT_CD_T151_START_TIME_ERROR_BIT                       0x00008000
#define OEDT_CD_T151_END_TIME_ERROR_BIT                         0x00010000
//#define OEDT_CD_T151_GETPLAYINFO_RESULT_ERROR_BIT               0x00020000
//#define OEDT_CD_T151_FASTFORWARD_RESULT_ERROR_BIT               0x00004000
//#define OEDT_CD_T151_FASTBACKWARD_RESULT_ERROR_BIT              0x00008000
#define OEDT_CD_T151_STOP_RESULT_ERROR_BIT                      0x00040000


#define OEDT_CD_T151_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT   0x10000000
#define OEDT_CD_T151_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT  0x20000000
#define OEDT_CD_T151_CLOSE_CTRL_RESULT_ERROR_BIT                0x40000000
#define OEDT_CD_T151_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000

static volatile tBool T151_bIsFirstPlayInfo         = TRUE;
static volatile tBool T151_bMediaReady              = FALSE;
static volatile OSAL_trPlayInfo T151_rZeroPlayInfo  = {0};
static volatile OSAL_trPlayInfo T151_rFirstPlayInfo = {0};
static volatile OSAL_trPlayInfo T151_rLastPlayInfo  = {0};
//static volatile tBool T151_bStatusPlayInProgress  = FALSE;
static volatile tBool T151_bStatusCompleted       = FALSE;
//static tU32  T151_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
static OSAL_trTrackInfo T151_arTrackInfo[OEDT_CD_T151_MASCA03_TRACKS] = {0}; 

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T151_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T151_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T151_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}


/*****************************************************************************
* FUNCTION:     T151_vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback
******************************************************************************/
static tVoid T151_vPlayInfoNotify(tPVoid pvCookie,
                                  const OSAL_trPlayInfo* prInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);

  if(NULL != prInfo)
  {
    OCDTR(c2,"T151_vPlayInfoNotify:"
          " AbsMSF [%02u:%02u:%02u],"
          " RelTMSF [%02u-%02u:%02u:%02u],"
          " PlayStatus [0x%08X]",
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32TrackNumber,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32StatusPlay
        );

    switch(prInfo->u32StatusPlay)
    {
    case OSAL_C_S32_PLAY_PAUSED:
      OCDTR(c3,"PAUSE");
      break;
    case OSAL_C_S32_PLAY_COMPLETED:
      OCDTR(c3,"COMPLETED");
      T151_bStatusCompleted = TRUE;
      break;
    case OSAL_C_S32_PLAY_IN_PROGRESS:
      OCDTR(c3,"PLAY_IN_PROGRESS");
      //T151_bStatusPlayInProgress = TRUE;
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
      OCDTR(c3,"STOPPED_DUE_TO_ERROR");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
      OCDTR(c3,"STOPPED_DUE_TO_UNDERVOLTAGE");
      break;
    case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
      OCDTR(c3,"NOT VALID");
      break;
    case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
      OCDTR(c3,"NO CURRENT AUDIO STATUS");
      break;
    default:
      OCDTR(c1,"- default -");
      break;
    } //switch(prInfo->u32StatusPlay)


    //T151_u32StatusPlay  = prInfo->u32StatusPlay;

    if(T151_bIsFirstPlayInfo)
    {
      T151_bIsFirstPlayInfo = FALSE;
      T151_rFirstPlayInfo = *prInfo;
      T151_rLastPlayInfo = *prInfo;
    }
    else //if(T151_bIsFirstPlayInfo)
    {
      T151_rLastPlayInfo = *prInfo;
    } //else //if(T151_bIsFirstPlayInfo)
  } //if(NULL != prInfo)

}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T151
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T151(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T151_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl  = OSAL_ERROR;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T151(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T151_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T151_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T151_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T151_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T151_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T151_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T151_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T151_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T151_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    //REG_NOTIFICATION CDCTRL
    T151_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T151_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION CTRL", s32Ret, iCount);
    }//REG_NOTIFICATION

    //REG_NOTIFICATION CDAUDIO
    //T151_bMediaReady = FALSE;
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T151_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//REG_NOTIFICATION CDAUDIO

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T151_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        OCDTR(c2, "Wait while media ready");
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //after media ready from PRM
    //it takes some time, until dev_cdctrl is notified
    // because ISO-FS is blocking SED-callback
    // while reading from CD!
    //(void)OSAL_s32ThreadWait(8000);


    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T151_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 16))
      {
        u32ResultBitMask |= OEDT_CD_T151_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO

    //GETTRACKINFO CTRL
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T151_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T151_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T151_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO CTRL

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T151_GETTRACKINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          if(u8Track < OEDT_CD_T151_MASCA03_TRACKS)
          {
            T151_arTrackInfo[u8Track] = rInfo; 
          } //if(u8Track < OEDT_CD_T151_MASCA03_TRACKS)
          OCDTR(c2, " GetTrackInfoAudio:"
                " Track [%02d],"
                " Start MSF [%02u:%02u:%02u],"
                " End MSF [%02u:%02u:%02u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO

    //GETADDITIONALCDINFO
    {
      OSAL_trAdditionalCDInfo rInfo;

      rInfo.u32DataTracks[0] = 0;
      rInfo.u32DataTracks[1] = 0;
      rInfo.u32DataTracks[2] = 0;
      rInfo.u32DataTracks[3] = 0;
      rInfo.u32InfoBits   = 0;

      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_GETADDITIONALCDINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETADDITIONALCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " AdditionalCDInfo:"
              " DataTrackBits [0x%08X] [0x%08X] [0x%08X] [0x%08X],"
              " InfoBits [0x%08X],",
              (unsigned int)rInfo.u32DataTracks[0],
              (unsigned int)rInfo.u32DataTracks[1],
              (unsigned int)rInfo.u32DataTracks[2],
              (unsigned int)rInfo.u32DataTracks[3],
              (unsigned int)rInfo.u32InfoBits
            );

      } //if(s32Ret != OSAL_ERROR)
    }//GETADDITIONALCDINFO


    //range+play
    {
      static const tU16 EOT=0xFFFF;
      //startTrack + start offset in seconds
      tU8  au8ST[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  1,  8,  7,  7,  16, 16,  2, 16, 16};
      tU16 au16SO[] = {0,0,0,0,0,0,0,0,0, 0, 0, 0, 0, 0, 0, 0,0,330,290,  0,180, 175,  0,260,180,188};
      //endTrack + end offset in seconds
      tU8  au8ET[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  2,  9,  7,  7, 16, 16,  2, 16, 16};
      tU16 au16EO[] = {1,1,2,2,3,3,4,4,5, 5, 6, 6, 8, 8,10,10,1,  5,  0, 10,EOT,188,  1,EOT,EOT,EOT};

      /*      tU8  au8ST[]  = {16, 16};
            tU16 au16SO[] = {180,188};
            //endTrack + end offset in seconds
            tU8  au8ET[]  = {16, 16};
            tU16 au16EO[] = {EOT,EOT};*/

      /*      tU8  au8ST[]  = {1};
            tU16 au16SO[] = {0};
            //endTrack + end offset in seconds
            tU8  au8ET[]  = {1};
            tU16 au16EO[] = {6};*/


      /*
              tU8  au8ST[]  = {16};
              tU16 au16SO[] = {188};
              //endTrack + end offset in seconds
              tU8  au8ET[]  = {16};
              tU16 au16EO[] = {EOT};
      */

      tInt iCnt;
      tInt iASize  = OEDT_CD_ARSIZE(au8ST);

      for(iCnt = 0; iCnt < iASize; iCnt++)
      {
        //reset
        T151_bIsFirstPlayInfo = TRUE;
        T151_rFirstPlayInfo = T151_rZeroPlayInfo;
        T151_rLastPlayInfo  = T151_rZeroPlayInfo;
        //T151_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
        //T151_bStatusPlayInProgress  = FALSE;
        T151_bStatusCompleted       = FALSE;

        //SETPLAYRANGE
        {
          OSAL_trPlayRange rPlayRange = {0};
          OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rPlayRange);

          rPlayRange.rStartAdr.u8Track   = au8ST[iCnt];
          rPlayRange.rStartAdr.u16Offset = au16SO[iCnt];
          rPlayRange.rEndAdr.u8Track     = au8ET[iCnt];
          rPlayRange.rEndAdr.u16Offset   = au16EO[iCnt];

          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
          s32Arg = (tS32)&rPlayRange;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T151_SETPLAYRANGE_RESULT_ERROR_BIT 
                              : 0;
          OCDTR(c2, "SETPLAYRANGE ArrayIndex %d "
                "STO[%02u-%04u] - ETO[%02u-%04u]",
                iCnt,
                (unsigned int)au8ST[iCnt],
                (unsigned int)au16SO[iCnt],
                (unsigned int)au8ET[iCnt],
                (unsigned int)au16EO[iCnt]);
          vPrintOsalResult("SETPLAYRANGE", s32Ret, iCount);
        }//SETPLAYRANGE

        //PLAY
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T151_PLAY_RESULT_ERROR_BIT : 0;
          OCDTR(c2, "PLAY ArrayIndex %d", iCnt);
          vPrintOsalResult("PLAY", s32Ret, iCount);
        }//PLAY


        //wait for play-in-progress state
        #if 0
        if(OEDT_CD_T151_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 7;

          while((FALSE == T151_bStatusPlayInProgress)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while(FALSE == T151_bStatusPlayInProgress)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T151_WAIT_FOR_PLAY_ERROR_BIT : 0;
        } //if(OEDT_CD_T151_RESULT_OK_VALUE == u32ResultBitMask)
        #endif

        //wait for play completed
        if(OEDT_CD_T151_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 30;

          while((FALSE == T151_bStatusCompleted)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while((FALSE == T151_bStatusCompleted)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T151_WAIT_FOR_COMPLETED_ERROR_BIT : 0;
        } //if(OEDT_CD_T151_RESULT_OK_VALUE == u32ResultBitMask)


        //check play info against play range
        {
          tU8  u8FT, u8LT; //first/last track in playtime
          tU16 u16FO, u16LO; //first/last playtime offset get from playtime
          tU8  u8SetPointST; //start track
          tU16 u16SetPointSO; //start offset
          tU8  u8SetPointET; //end track
          tU16 u16SetPointEO; //end offset

          u8FT  = (tU8)T151_rFirstPlayInfo.u32TrackNumber;
          u16FO = OEDT_CD_RELOFFSETFROMPLAYINFO(T151_rFirstPlayInfo);

          u8LT  = (tU8)T151_rLastPlayInfo.u32TrackNumber;
          u16LO = OEDT_CD_RELOFFSETFROMPLAYINFO(T151_rLastPlayInfo);

          u8SetPointST  = au8ST[iCnt];
          u16SetPointSO = au16SO[iCnt];
          u8SetPointET  = au8ET[iCnt];
          if(0xFFFF == au16EO[iCnt])
          { //until End-Of-EndTrack
            tU32 u32StartSec =
            OEDT_CD_MS2SECS(T151_arTrackInfo[u8SetPointET].rStartAdr);
            tU32 u32EndSec   =
            OEDT_CD_MS2SECS(T151_arTrackInfo[u8SetPointET].rEndAdr);
            u16SetPointEO = (tU16)(u32EndSec - u32StartSec);
          }
          else //if(0xFFFF == au16EO[iCnt])
          {
            u16SetPointEO = au16EO[iCnt];
          } //else //if(0xFFFF == au16EO[iCnt])

          if(u8SetPointST != u8FT
             ||
             u16SetPointSO != u16FO)
          {
            u32ResultBitMask |= OEDT_CD_T151_START_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR Start Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }
          else
          {
            OCDTR(c2, "SUCCESS Start Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }

          if(u8SetPointET != u8LT
             ||
             u16SetPointEO != u16LO)
          {
            u32ResultBitMask |= OEDT_CD_T151_END_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR End Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }
          else
          {
            OCDTR(c2, "SUCCESS End Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }

        }
      } //for(iCnt = 0; iCnt < iASize; iCnt++)
    } //range+ play



    //GETPLAYINFO
    #if 0
    {
      OSAL_trPlayInfo rInfo = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);


      rInfo.u32TrackNumber           = 0;
      rInfo.rAbsTrackAdr.u8MSFMinute = 0;
      rInfo.rAbsTrackAdr.u8MSFSecond = 0;
      rInfo.rAbsTrackAdr.u8MSFFrame  = 0;
      rInfo.rRelTrackAdr.u8MSFMinute = 0;
      rInfo.rRelTrackAdr.u8MSFSecond = 0;
      rInfo.rRelTrackAdr.u8MSFFrame  = 0;
      rInfo.u32StatusPlay            = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_GETPLAYINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETPLAYINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " PlayInfo:"
              " Track [%02u],"
              " AbsMSF [%02u:%02u:%02u],"
              " RelMSF [%02u:%02u:%02u],"
              " PlayStatus [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.u32StatusPlay
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETPLAYINFO
    #endif



    //FASTFORWARD
    #if 0
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_FASTFORWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTFORWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(5000);
    }//FASTFORWARD


    //FASTBACKWARD
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_FASTBACKWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTBACKWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(3000);
    }//FASTBACWARD
    #endif 


    //STOP
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_STOP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("STOP", s32Ret, iCount);
    }//STOP



    //UNREG_NOTIFICATION AUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T151_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//UNREG_NOTIFICATION

    //UNREG_NOTIFICATION CDCTRL

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T151_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION CTRL", s32Ret, iCount);
    }//UNREG_NOTIFICATION CDCTRL


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T151_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T151_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

  } //for(iCount = 0; iCount < OEDT_CD_T151_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T151 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T151_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T151 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T151_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}






/*****************************************************************************/
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*                            OEDT_CD_T152                          */
/*****************************************************************************/

#define OEDT_CD_T152_COUNT                                               5
#define OEDT_CD_T152_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T152_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO
#define OEDT_CD_T152_MASCA03_TRACKS                                 (16+1)



#define OEDT_CD_T152_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T152_OPEN_CTRL_RESULT_ERROR_BIT                 0x00000001
#define OEDT_CD_T152_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000002
#define OEDT_CD_T152_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT     0x00000004


#define OEDT_CD_T152_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T152_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T152_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T152_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T152_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T152_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000200
#define OEDT_CD_T152_GETALBUMNAME_RESULT_ERROR_BIT              0x00000400
#define OEDT_CD_T152_GETALBUMNAME_SIZE_ERROR_BIT                0x00000800
#define OEDT_CD_T152_GETALBUMNAME_CMP_ERROR_BIT                 0x00001000
#define OEDT_CD_T152_GETTRACKCDINFOAUDIO_RESULT_ERROR_BIT       0x00002000
#define OEDT_CD_T152_GETTRACKCDINFOAUDIO_TSIZE_ERROR_BIT        0x00004000
#define OEDT_CD_T152_GETTRACKCDINFOAUDIO_ASIZE_ERROR_BIT        0x00008000
#define OEDT_CD_T152_GETTRACKCDINFOAUDIO_TCMP_ERROR_BIT         0x00010000
#define OEDT_CD_T152_GETTRACKCDINFOAUDIO_ACMP_ERROR_BIT         0x00020000



#define OEDT_CD_T152_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT   0x10000000
#define OEDT_CD_T152_CLOSE_CTRL_RESULT_ERROR_BIT                0x40000000
#define OEDT_CD_T152_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000

static tBool T152_bMediaReady = FALSE;
//static OSAL_trTrackInfo T152_arTrackInfo[OEDT_CD_T152_MASCA03_TRACKS] = {0}; 

static tChar *T152_apszTitle[OEDT_CD_T152_MASCA03_TRACKS] = {
  "abCdeFghIJklmNOp",
  "abCd",
  "AbcdefGh",
  "aBcdEfgHIjklmnOP",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH",
  "abCd",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH",
  "AbcdefGh",
  "aBcdEfgHIjklmnOP",
  "abCd",
  "AbcdefGh",
  "aBcdEfgHIjklmnOP",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH",
  " ",
  " ",
  " ",
  " "};


static tChar *T152_apszArtist[OEDT_CD_T152_MASCA03_TRACKS] = {
  "abCdeFghIJklmNOp",
  "abCd",
  "AbcdefGh",
  "aBcdEfgHIjklmnOP",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH",
  "abCd",
  "aBcdEfgHIjklmnOP	",
  "AbcdefGh",
  "",
  "",
  "",
  "",
  "abCd",
  "AbcdefGh",
  "aBcdEfgHIjklmnOP",
  "abcDeFGhijKlMNopQrsTuvWxyZsXaViH"};


/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T152_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T152_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T152_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}



/******************************************************************************
 *FUNCTION     :  OEDT_CD_T152
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required, reads CD-Text
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T152(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T152_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl  = OSAL_ERROR;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T152(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T152_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T152_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T152_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T152_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T152_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T152_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T152_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T152_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T152_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    //REG_NOTIFICATION CDCTRL
    T152_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T152_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T152_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION CTRL", s32Ret, iCount);
    }//REG_NOTIFICATION

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T152_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        OCDTR(c2, "Wait while media ready");
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T152_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 16))
      {
        u32ResultBitMask |= OEDT_CD_T152_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO

    //GETTRACKINFO CTRL
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T152_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T152_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T152_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO CTRL

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T152_GETTRACKINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          /*if(u8Track < OEDT_CD_T152_MASCA03_TRACKS)
          {
            T152_arTrackInfo[u8Track] = rInfo; 
          } //if(u8Track < OEDT_CD_T152_MASCA03_TRACKS)
          */
          OCDTR(c2, " GetTrackInfoAudio:"
                " Track [%02d],"
                " Start MSF [%02u:%02u:%02u],"
                " End MSF [%02u:%02u:%02u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO


    //GETALBUMNAME
    {
      tU8 u8NameArray[OSAL_C_S32_CDAUDIO_MAXNAMESIZE+1];

      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETALBUMNAME;
      s32Arg = (tS32)u8NameArray;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T152_GETALBUMNAME_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETALBUMNAME", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        size_t iCompSize;
        size_t iReadSize;

        OCDTR(c2, " AlbumName: [%s]", (const char *)u8NameArray);

        //album name
        iCompSize = strlen(T152_apszTitle[0]);
        iReadSize = strlen((const char *)u8NameArray);
        if(iCompSize != iReadSize)
        {
          u32ResultBitMask |= OEDT_CD_T152_GETALBUMNAME_SIZE_ERROR_BIT;

          OCDTR(c1, " ERROR text size mismatch "
                " %d != %d",
                (int)iCompSize,
                (int)iReadSize
              );
        }
        else  //if(iCompSize != iReadSize)
        {
          //size OK, compare
          if(0 != strcmp(T152_apszTitle[0], (const char *)u8NameArray))
          {
            u32ResultBitMask |= OEDT_CD_T152_GETALBUMNAME_CMP_ERROR_BIT;
            OCDTR(c1, " ERROR text mismatch"
                  " [%s] != [%s]",
                  T152_apszTitle[0],
                  (const char *)u8NameArray
                );

          } //if(iCompSize != iReadSize)
        } //else  //if(iCompSize != iReadSize)

      } //if(s32Ret != OSAL_ERROR)
    }//GETALBUMNAME


    //GETTRACKCDINFO AUDIO (cdtext)
    {
      OSAL_trTrackCdTextInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.rPerformer[0]  = '\0';
        rInfo.rTrackTitle[0] = '\0';
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKCDINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T152_GETTRACKCDINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKCDINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          //check cd-text
          if(u8Track < OEDT_CD_T152_MASCA03_TRACKS)
          {
            size_t iCompSize;
            size_t iReadSize;

            //title
            iCompSize = strlen(T152_apszTitle[u8Track]);
            iReadSize = strlen(rInfo.rTrackTitle);
            if(iCompSize != iReadSize)
            {
              u32ResultBitMask |= 
              OEDT_CD_T152_GETTRACKCDINFOAUDIO_TSIZE_ERROR_BIT;

              OCDTR(c1, " ERROR text size mismatch (Title)"
                    " Track [%02u], %d != %d",
                    (unsigned int)u8Track,
                    (int)iCompSize,
                    (int)iReadSize
                  );
            }
            else  //if(iCompSize != iReadSize)
            {
              //size OK, compare
              if(0 != strcmp(T152_apszTitle[u8Track],rInfo.rTrackTitle))
              {
                u32ResultBitMask |= 
                OEDT_CD_T152_GETTRACKCDINFOAUDIO_TCMP_ERROR_BIT;
                OCDTR(c1, " ERROR text mismatch (Title)"
                      " Track [%02u], [%s] != [%s]",
                      (unsigned int)u8Track,
                      T152_apszTitle[u8Track],
                      rInfo.rTrackTitle
                    );

              } //if(iCompSize != iReadSize)
            } //else  //if(iCompSize != iReadSize)

            //artist
            iCompSize = strlen(T152_apszArtist[u8Track]);
            iReadSize = strlen(rInfo.rPerformer);
            if(iCompSize != iReadSize)
            {
              u32ResultBitMask |= 
              OEDT_CD_T152_GETTRACKCDINFOAUDIO_ASIZE_ERROR_BIT;

              OCDTR(c1, " ERROR text size mismatch (Artist)"
                    " Track [%02u], %d != %d",
                    (unsigned int)u8Track,
                    (int)iCompSize,
                    (int)iReadSize
                  );
            }
            else  //if(iCompSize != iReadSize)
            {
              //size OK, compare
              if(0 != strcmp(T152_apszArtist[u8Track],rInfo.rPerformer))
              {
                u32ResultBitMask |= 
                OEDT_CD_T152_GETTRACKCDINFOAUDIO_ACMP_ERROR_BIT;

                OCDTR(c1, " ERROR text mismatch (Artist)"
                      " Track [%02u], [%s] != [%s]",
                      (unsigned int)u8Track,
                      T152_apszArtist[u8Track],
                      rInfo.rPerformer
                    );
              } //if(iCompSize != iReadSize)
            } //else  //if(iCompSize != iReadSize)
          } //if(u8Track < OEDT_CD_T152_MASCA03_TRACKS)
          OCDTR(c2, " GetTrackCDInfoAudio:"
                " Track [%02d],"
                " Title:     [%s]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rTrackTitle
              );
          OCDTR(c2, " GetTrackCDInfoAudio:"
                " Track [%02d],"
                " Performer: [%s]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rPerformer
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKCDINFO AUDIO (cdtext)


    //UNREG_NOTIFICATION CDCTRL

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T152_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION CTRL", s32Ret, iCount);
    }//UNREG_NOTIFICATION CDCTRL


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T152_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T152_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

  } //for(iCount = 0; iCount < OEDT_CD_T152_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T152 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T152_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T152 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T152_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}









/*****************************************************************************/
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*                            OEDT_CD_T153                          */
/*****************************************************************************/

#define OEDT_CD_T153_COUNT                                               2
#define OEDT_CD_T153_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T153_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO
#define OEDT_CD_T153_MASCA03_TRACKS                                 (16+1)



#define OEDT_CD_T153_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T153_OPEN_CTRL_RESULT_ERROR_BIT                 0x00000001
#define OEDT_CD_T153_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000002
#define OEDT_CD_T153_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT     0x00000004
#define OEDT_CD_T153_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT    0x00000008
#define OEDT_CD_T153_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T153_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T153_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T153_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T153_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T153_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000200
#define OEDT_CD_T153_SETPLAYRANGE_RESULT_ERROR_BIT              0x00000400
#define OEDT_CD_T153_FFW_RESULT_ERROR_BIT                       0x00000800
//#define OEDT_CD_T153_WAIT_FOR_PLAY_ERROR_BIT                    0x00001000
#define OEDT_CD_T153_WAIT_FOR_COMPLETED_ERROR_BIT               0x00002000
#define OEDT_CD_T153_START_TIME_ERROR_BIT                       0x00004000
#define OEDT_CD_T153_END_TIME_ERROR_BIT                         0x00008000
#define OEDT_CD_T153_STOP_RESULT_ERROR_BIT                      0x00010000

#define OEDT_CD_T153_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT   0x10000000
#define OEDT_CD_T153_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT  0x20000000
#define OEDT_CD_T153_CLOSE_CTRL_RESULT_ERROR_BIT                0x40000000
#define OEDT_CD_T153_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000

static volatile tBool T153_bIsFirstPlayInfo         = TRUE;
static volatile tBool T153_bMediaReady              = FALSE;
static volatile OSAL_trPlayInfo T153_rZeroPlayInfo  = {0};
static volatile OSAL_trPlayInfo T153_rFirstPlayInfo = {0};
static volatile OSAL_trPlayInfo T153_rLastPlayInfo  = {0};
//static volatile tBool T153_bStatusPlayInProgress  = FALSE;
static volatile tBool T153_bStatusCompleted       = FALSE;
//static tU32  T153_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
static OSAL_trTrackInfo T153_arTrackInfo[OEDT_CD_T153_MASCA03_TRACKS] = {0}; 

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T153_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T153_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T153_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}


/*****************************************************************************
* FUNCTION:     T153_vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback
******************************************************************************/
static tVoid T153_vPlayInfoNotify(tPVoid pvCookie,
                                  const OSAL_trPlayInfo* prInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);

  if(NULL != prInfo)
  {
    OCDTR(c2,"T153_vPlayInfoNotify:"
          " AbsMSF [%02u:%02u:%02u],"
          " RelTMSF [%02u-%02u:%02u:%02u],"
          " PlayStatus [0x%08X]",
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32TrackNumber,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32StatusPlay
        );

    switch(prInfo->u32StatusPlay)
    {
    case OSAL_C_S32_PLAY_PAUSED:
      OCDTR(c3,"PAUSE");
      break;
    case OSAL_C_S32_PLAY_COMPLETED:
      T153_bStatusCompleted = TRUE;
      OCDTR(c3,"COMPLETED");
      break;
    case OSAL_C_S32_PLAY_IN_PROGRESS:
      //T153_bStatusPlayInProgress = TRUE;
      OCDTR(c3,"PLAY_IN_PROGRESS");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
      OCDTR(c3,"STOPPED_DUE_TO_ERROR");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
      OCDTR(c3,"STOPPED_DUE_TO_UNDERVOLTAGE");
      break;
    case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
      OCDTR(c3,"NOT VALID");
      break;
    case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
      OCDTR(c3,"NO CURRENT AUDIO STATUS");
      break;
    default:
      OCDTR(c1,"- default -");
      break;
    } //switch(prInfo->u32StatusPlay)


    //T153_u32StatusPlay  = prInfo->u32StatusPlay;

    if(T153_bIsFirstPlayInfo)
    {
      T153_bIsFirstPlayInfo = FALSE;
      T153_rFirstPlayInfo = *prInfo;
      T153_rLastPlayInfo = *prInfo;
    }
    else //if(T153_bIsFirstPlayInfo)
    {
      T153_rLastPlayInfo = *prInfo;
    } //else //if(T153_bIsFirstPlayInfo)
  } //if(NULL != prInfo)

}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T153
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required
 *                Playrange check in FAstForwardMode
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T153(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask       = OEDT_CD_T153_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl  = OSAL_ERROR;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T153(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T153_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T153_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T153_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T153_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T153_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T153_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T153_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T153_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T153_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    //REG_NOTIFICATION CDCTRL
    T153_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T153_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T153_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION CTRL", s32Ret, iCount);
    }//REG_NOTIFICATION

    //REG_NOTIFICATION CDAUDIO
    //T153_bMediaReady = FALSE;
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T153_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T153_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//REG_NOTIFICATION CDAUDIO

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T153_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        OCDTR(c2, "Wait while media ready");
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T153_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 16))
      {
        u32ResultBitMask |= OEDT_CD_T153_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO

    //GETTRACKINFO CTRL
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T153_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T153_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T153_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO CTRL

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T153_GETTRACKINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          if(u8Track < OEDT_CD_T153_MASCA03_TRACKS)
          {
            T153_arTrackInfo[u8Track] = rInfo; 
          } //if(u8Track < OEDT_CD_T153_MASCA03_TRACKS)
          OCDTR(c2, " GetTrackInfoAudio:"
                " Track [%02d],"
                " Start MSF [%02u:%02u:%02u],"
                " End MSF [%02u:%02u:%02u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO


    //range+FWD (w/o play)
    {
      static const tU16 EOT=0xFFFF;
      //startTrack + start offset in seconds
      tU8  au8ST[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  1,  8,  7,   7,  16, 16,  2, 16, 16};
      tU16 au16SO[] = {0,0,0,0,0,0,0,0,0, 0, 0, 0, 0, 0, 0, 0,0,330,290,  0, 180, 175,  0,260,180,188};
      //endTrack + end offset in seconds
      tU8  au8ET[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  2,  9,  7,  7,   16, 16,  2, 16, 16};
      tU16 au16EO[] = {1,1,2,2,3,3,4,4,5, 5, 6, 6, 8, 8,10,10,1,  5,  0, 10, EOT, 188,  1,EOT,EOT,EOT};
      tInt iCnt;
      tInt iASize  = OEDT_CD_ARSIZE(au8ST);

      for(iCnt = 0; iCnt < iASize; iCnt++)
      {
        //reset
        T153_bIsFirstPlayInfo = TRUE;
        T153_rFirstPlayInfo = T153_rZeroPlayInfo;
        T153_rLastPlayInfo  = T153_rZeroPlayInfo;
        //T153_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
        //T153_bStatusPlayInProgress  = FALSE;

        //SETPLAYRANGE
        {
          OSAL_trPlayRange rPlayRange = {0};
          OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rPlayRange);

          rPlayRange.rStartAdr.u8Track   = au8ST[iCnt];
          rPlayRange.rStartAdr.u16Offset = au16SO[iCnt];
          rPlayRange.rEndAdr.u8Track     = au8ET[iCnt];
          rPlayRange.rEndAdr.u16Offset   = au16EO[iCnt];

          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
          s32Arg = (tS32)&rPlayRange;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T153_SETPLAYRANGE_RESULT_ERROR_BIT 
                              : 0;
          OCDTR(c2, "SETPLAYRANGE ArrayIndex %d "
                "STO[%02u-%04u] - ETO[%02u-%04u]",
                iCnt,
                (unsigned int)au8ST[iCnt],
                (unsigned int)au16SO[iCnt],
                (unsigned int)au8ET[iCnt],
                (unsigned int)au16EO[iCnt]);
          vPrintOsalResult("SETPLAYRANGE", s32Ret, iCount);
        }//SETPLAYRANGE

        T153_bStatusCompleted       = FALSE;
        //FFW
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T153_FFW_RESULT_ERROR_BIT : 0;
          OCDTR(c2, "FFW ArrayIndex %d", iCnt);
          vPrintOsalResult("FFW", s32Ret, iCount);
        }//FFW

        //wait for play-in-progress state
        #if 0
        if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 7;

          while((FALSE == T153_bStatusPlayInProgress)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while(FALSE == T153_bStatusPlayInProgress)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T153_WAIT_FOR_PLAY_ERROR_BIT : 0;
        } //if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)
        #endif

        //wait for play completed
        if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 30;

          while((FALSE == T153_bStatusCompleted)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(250);
          } //while((FALSE == T153_bStatusCompleted)

          if(iEmergencyExit <=0)
          {
            u32ResultBitMask |= OEDT_CD_T153_WAIT_FOR_COMPLETED_ERROR_BIT;

            OCDTR(c1, " ERROR waiting for COMPLETED ArrayIndex %d "
                  "STO[%02u-%04u] - ETO[%02u-%04u]",
                  iCnt,
                  (unsigned int)au8ST[iCnt],
                  (unsigned int)au16SO[iCnt],
                  (unsigned int)au8ET[iCnt],
                  (unsigned int)au16EO[iCnt]);

          } //if(iEmergencyExit <=0)
        } //if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)

        //check play info against play range
        if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tU8  u8FT, u8LT; //first/last track in playtime
          tU16 u16FO, u16LO; //first/last playtime offset get from playtime
          tU8  u8SetPointST; //start track
          tU16 u16SetPointSO; //start offset
          tU8  u8SetPointET; //end track
          tU16 u16SetPointEO; //end offset

          u8FT  = (tU8)T153_rFirstPlayInfo.u32TrackNumber;
          u16FO = OEDT_CD_RELOFFSETFROMPLAYINFO(T153_rFirstPlayInfo);

          u8LT  = (tU8)T153_rLastPlayInfo.u32TrackNumber;
          u16LO = OEDT_CD_RELOFFSETFROMPLAYINFO(T153_rLastPlayInfo);

          u8SetPointST  = au8ST[iCnt];
          u16SetPointSO = au16SO[iCnt];
          u8SetPointET  = au8ET[iCnt];
          if(0xFFFF == au16EO[iCnt])
          { //until End-Of-EndTrack
            tU32 u32StartSec = OEDT_CD_MS2SECS(
                                              T153_arTrackInfo[u8SetPointET].rStartAdr);
            tU32 u32EndSec   = OEDT_CD_MS2SECS(
                                              T153_arTrackInfo[u8SetPointET].rEndAdr);
            u16SetPointEO = (tU16)(u32EndSec - u32StartSec);
          }
          else //if(0xFFFF == au16EO[iCnt])
          {
            u16SetPointEO = au16EO[iCnt];
          } //else //if(0xFFFF == au16EO[iCnt])

          if(u8SetPointST != u8FT
             ||
             u16SetPointSO != u16FO)
          {
            u32ResultBitMask |= OEDT_CD_T153_START_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR Start Range [%d] - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }
          else
          {
            OCDTR(c3, "SUCCESS Start Range - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }


          if(u8SetPointET != u8LT
             ||
             u16SetPointEO != u16LO)
          {
            u32ResultBitMask |= OEDT_CD_T153_END_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR End Range [%d] - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }
          else
          {
            OCDTR(c3, "SUCCESS End Range - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }

        } //if(OEDT_CD_T153_RESULT_OK_VALUE == u32ResultBitMask)
        (void)OSAL_s32ThreadWait(250);
      } //for(iCnt = 0; iCnt < iASize; iCnt++)
    } //range+ play


    //STOP
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T153_STOP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("STOP", s32Ret, iCount);
    }//STOP



    //UNREG_NOTIFICATION AUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T153_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T153_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//UNREG_NOTIFICATION

    //UNREG_NOTIFICATION CDCTRL

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T153_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION CTRL", s32Ret, iCount);
    }//UNREG_NOTIFICATION CDCTRL


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T153_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T153_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

  } //for(iCount = 0; iCount < OEDT_CD_T153_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T153 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T153_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T153 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T153_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}






/*****************************************************************************/
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*                            OEDT_CD_T154                          */
/*****************************************************************************/

#define OEDT_CD_T154_COUNT                                               2
#define OEDT_CD_T154_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T154_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO
#define OEDT_CD_T154_MASCA03_TRACKS                                 (16+1)

#define OEDT_CD_T154_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T154_OPEN_CTRL_RESULT_ERROR_BIT                 0x00000001
#define OEDT_CD_T154_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000002
#define OEDT_CD_T154_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT     0x00000004
#define OEDT_CD_T154_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT    0x00000008
#define OEDT_CD_T154_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T154_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T154_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T154_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T154_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T154_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000200
#define OEDT_CD_T154_SETPLAYRANGE_RESULT_ERROR_BIT              0x00000400
//#define OEDT_CD_T154_PLAY_RESULT_ERROR_BIT                      0x00000800
#define OEDT_CD_T154_FBW_RESULT_ERROR_BIT                       0x00001000
//#define OEDT_CD_T154_WAIT_FOR_PLAY_ERROR_BIT                    0x00002000
#define OEDT_CD_T154_WAIT_FOR_COMPLETED_ERROR_BIT               0x00004000
#define OEDT_CD_T154_START_TIME_ERROR_BIT                       0x00008000
#define OEDT_CD_T154_END_TIME_ERROR_BIT                         0x00010000
//#define OEDT_CD_T154_GETPLAYINFO_RESULT_ERROR_BIT               0x00020000
#define OEDT_CD_T154_STOP_RESULT_ERROR_BIT                      0x00040000

#define OEDT_CD_T154_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT   0x10000000
#define OEDT_CD_T154_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT  0x20000000
#define OEDT_CD_T154_CLOSE_CTRL_RESULT_ERROR_BIT                0x40000000
#define OEDT_CD_T154_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000

static volatile tBool T154_bMediaReady = FALSE;
static volatile tBool T154_bIsFirstPlayInfo = TRUE;
static volatile OSAL_trPlayInfo T154_rZeroPlayInfo  = {0};
static volatile OSAL_trPlayInfo T154_rFirstPlayInfo = {0};
static volatile OSAL_trPlayInfo T154_rLastPlayInfo  = {0};
//static tU32  T154_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
//static volatile tBool T154_bStatusPlayInProgress  = FALSE;
static volatile tBool T154_bStatusCompleted       = FALSE;
static OSAL_trTrackInfo T154_arTrackInfo[OEDT_CD_T154_MASCA03_TRACKS] = {0}; 

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T154_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T154_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T154_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}


/*****************************************************************************
* FUNCTION:     T154_vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback
******************************************************************************/
static tVoid T154_vPlayInfoNotify(tPVoid pvCookie,
                                  const OSAL_trPlayInfo* prInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);

  if(NULL != prInfo)
  {
    OCDTR(c2,"T154_vPlayInfoNotify:"
          " AbsMSF [%02u:%02u:%02u],"
          " RelTMSF [%02u-%02u:%02u:%02u],"
          " PlayStatus [0x%08X]",
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32TrackNumber,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32StatusPlay
        );

    switch(prInfo->u32StatusPlay)
    {
    case OSAL_C_S32_PLAY_PAUSED:
      OCDTR(c3,"PAUSE");
      break;
    case OSAL_C_S32_PLAY_COMPLETED:
      T154_bStatusCompleted = TRUE;
      OCDTR(c3,"COMPLETED");
      break;
    case OSAL_C_S32_PLAY_IN_PROGRESS:
      //T154_bStatusPlayInProgress  = TRUE;
      OCDTR(c3,"PLAY_IN_PROGRESS");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
      OCDTR(c3,"STOPPED_DUE_TO_ERROR");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
      OCDTR(c3,"STOPPED_DUE_TO_UNDERVOLTAGE");
      break;
    case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
      OCDTR(c3,"NOT VALID");
      break;
    case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
      OCDTR(c3,"NO CURRENT AUDIO STATUS");
      break;
    default:
      OCDTR(c1,"- default -");
      break;
    } //switch(prInfo->u32StatusPlay)


    //T154_u32StatusPlay  = prInfo->u32StatusPlay;

    if(T154_bIsFirstPlayInfo)
    {
      T154_bIsFirstPlayInfo = FALSE;
      T154_rFirstPlayInfo = *prInfo;
      T154_rLastPlayInfo = *prInfo;
    }
    else //if(T154_bIsFirstPlayInfo)
    {
      T154_rLastPlayInfo = *prInfo;
    } //else //if(T154_bIsFirstPlayInfo)
  } //if(NULL != prInfo)

}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T154
 *DESCRIPTION  :  MASCA 03 CD (CDDA+CD-Text) required
 *                Playrange check in FAstBackwardMode
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T154(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask       = OEDT_CD_T154_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl  = OSAL_ERROR;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T154(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T154_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T154_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T154_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T154_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T154_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T154_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T154_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T154_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T154_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    //REG_NOTIFICATION CDCTRL
    T154_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T154_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION CTRL", s32Ret, iCount);
    }//REG_NOTIFICATION

    //REG_NOTIFICATION CDAUDIO
    //T154_bMediaReady = FALSE;
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T154_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//REG_NOTIFICATION CDAUDIO

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T154_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        OCDTR(c2, "Wait while media ready");
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T154_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 16))
      {
        u32ResultBitMask |= OEDT_CD_T154_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO

    //GETTRACKINFO CTRL
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T154_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T154_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T154_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO CTRL

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T154_GETTRACKINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          if(u8Track < OEDT_CD_T154_MASCA03_TRACKS)
          {
            T154_arTrackInfo[u8Track] = rInfo; 
          } //if(u8Track < OEDT_CD_T154_MASCA03_TRACKS)
          OCDTR(c2, " GetTrackInfoAudio:"
                " Track [%02d],"
                " Start MSF [%02u:%02u:%02u],"
                " End MSF [%02u:%02u:%02u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO


    //range+FWD (w/o play)
    {
      static const tU16 EOT=0xFFFF;
      //startTrack + start offset in seconds
      tU8  au8ST[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  1,  8,  7,  7,  16, 16,  2, 16, 16};
      tU16 au16SO[] = {0,0,0,0,0,0,0,0,0, 0, 0, 0, 0, 0, 0, 0,0,330,290,  0,180, 175,  0,260,180,186};
      //endTrack + end offset in seconds
      tU8  au8ET[]  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,  2,  9,  7,  7,  16, 16,  2, 16, 16};
      tU16 au16EO[] = {1,1,2,2,3,3,4,4,5, 5, 6, 6, 8, 8,10,10,1,  5,  0, 10,EOT, 188,  1,EOT,EOT,EOT};
      tInt iCnt;
      tInt iASize  = OEDT_CD_ARSIZE(au8ST);

      for(iCnt = 0; iCnt < iASize; iCnt++)
      {
        //reset
        T154_bIsFirstPlayInfo = TRUE;
        T154_rFirstPlayInfo = T154_rZeroPlayInfo;
        T154_rLastPlayInfo  = T154_rZeroPlayInfo;
        //T154_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
        //T154_bStatusPlayInProgress  = FALSE;
        T154_bStatusCompleted       = FALSE;

        //SETPLAYRANGE
        {
          OSAL_trPlayRange rPlayRange = {0};
          OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rPlayRange);

          rPlayRange.rStartAdr.u8Track   = au8ST[iCnt];
          rPlayRange.rStartAdr.u16Offset = au16SO[iCnt];
          rPlayRange.rEndAdr.u8Track     = au8ET[iCnt];
          rPlayRange.rEndAdr.u16Offset   = au16EO[iCnt];

          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
          s32Arg = (tS32)&rPlayRange;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T154_SETPLAYRANGE_RESULT_ERROR_BIT 
                              : 0;
          OCDTR(c2, "SETPLAYRANGE ArrayIndex %d "
                "STO[%02u-%04u] - ETO[%02u-%04u]",
                iCnt,
                (unsigned int)au8ST[iCnt],
                (unsigned int)au16SO[iCnt],
                (unsigned int)au8ET[iCnt],
                (unsigned int)au16EO[iCnt]);
          vPrintOsalResult("SETPLAYRANGE", s32Ret, iCount);
        }//SETPLAYRANGE

        //PLAY
        #if 0
        if(0)
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T154_PLAY_RESULT_ERROR_BIT : 0;
          OCDTR(c2, "PLAY ArrayIndex %d", iCnt);
          vPrintOsalResult("PLAY", s32Ret, iCount);
        }//PLAY
        #endif

        //FBW
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T154_FBW_RESULT_ERROR_BIT : 0;
          OCDTR(c2, "FBW ArrayIndex %d", iCnt);
          vPrintOsalResult("FBW", s32Ret, iCount);
        }//FBW

        //wait for play-in-progress state
        #if 0
        if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 7;

          while((FALSE == T154_bStatusPlayInProgress)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while(FALSE == T154_bStatusPlayInProgress)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T154_WAIT_FOR_PLAY_ERROR_BIT : 0;
        } //if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)
        #endif

        //wait for play completed
        if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 30;
          while((FALSE == T154_bStatusCompleted)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(250);
          } //if(FALSE == T154_bStatusCompleted)
          if(iEmergencyExit <=0)
          {
            u32ResultBitMask |= OEDT_CD_T154_WAIT_FOR_COMPLETED_ERROR_BIT;
            OCDTR(c1, " ERROR waiting for COMPLETED ArrayIndex %d "
                  "STO[%02u-%04u] - ETO[%02u-%04u]",
                  iCnt,
                  (unsigned int)au8ST[iCnt],
                  (unsigned int)au16SO[iCnt],
                  (unsigned int)au8ET[iCnt],
                  (unsigned int)au16EO[iCnt]);

          } //if(iEmergencyExit <=0)

        } //if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)

        //check play info against play range (backward - first/last are swapped)
        if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tU8  u8FT, u8LT; //first/last track in playtime
          tU16 u16FO, u16LO; //first/last playtime offset get from playtime
          tU8  u8SetPointST; //start track
          tU16 u16SetPointSO; //start offset
          tU8  u8SetPointET; //end track
          tU16 u16SetPointEO; //end offset

          u8FT  = (tU8)T154_rFirstPlayInfo.u32TrackNumber;
          u16FO = OEDT_CD_RELOFFSETFROMPLAYINFO(T154_rFirstPlayInfo);

          u8LT  = (tU8)T154_rLastPlayInfo.u32TrackNumber;
          u16LO = OEDT_CD_RELOFFSETFROMPLAYINFO(T154_rLastPlayInfo);

          u8SetPointST  = au8ST[iCnt];
          u16SetPointSO = au16SO[iCnt];
          u8SetPointET  = au8ET[iCnt];
          if(0xFFFF == au16EO[iCnt])
          { //until End-Of-EndTrack
            tU32 u32StartSec =
            OEDT_CD_MS2SECS(T154_arTrackInfo[u8SetPointET].rStartAdr);
            tU32 u32EndSec   =
            OEDT_CD_MS2SECS(T154_arTrackInfo[u8SetPointET].rEndAdr);
            u16SetPointEO = (tU16)(u32EndSec - u32StartSec);
          }
          else //if(0xFFFF == au16EO[iCnt])
          {
            u16SetPointEO = au16EO[iCnt];
          } //else //if(0xFFFF == au16EO[iCnt])

          if(u8SetPointST != u8LT
             ||
             u16SetPointSO != u16LO)
          {
            u32ResultBitMask |= OEDT_CD_T154_START_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR Start Range [%d]- "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }
          else
          {
            OCDTR(c3, "SUCCESS Start Range - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }


          if(u8SetPointET != u8FT
             ||
             u16SetPointEO != u16FO)
          {
            u32ResultBitMask |= OEDT_CD_T154_END_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR End Range [%d]- "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }
          else
          {
            OCDTR(c3, "SUCCESS End Range - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }

        } //if(OEDT_CD_T154_RESULT_OK_VALUE == u32ResultBitMask)
      } //for(iCnt = 0; iCnt < iASize; iCnt++)
    } //range+ play





    //GETPLAYINFO
    #if 0
    if(0)
    {
      OSAL_trPlayInfo rInfo = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);


      rInfo.u32TrackNumber           = 0;
      rInfo.rAbsTrackAdr.u8MSFMinute = 0;
      rInfo.rAbsTrackAdr.u8MSFSecond = 0;
      rInfo.rAbsTrackAdr.u8MSFFrame  = 0;
      rInfo.rRelTrackAdr.u8MSFMinute = 0;
      rInfo.rRelTrackAdr.u8MSFSecond = 0;
      rInfo.rRelTrackAdr.u8MSFFrame  = 0;
      rInfo.u32StatusPlay            = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_GETPLAYINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETPLAYINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " PlayInfo:"
              " Track [%02u],"
              " AbsMSF [%02u:%02u:%02u],"
              " RelMSF [%02u:%02u:%02u],"
              " PlayStatus [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.u32StatusPlay
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETPLAYINFO
    #endif



    //FASTFORWARD
    #if 0
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_FASTFORWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTFORWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(5000);
    }//FASTFORWARD


    //FASTBACKWARD
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_FASTBACKWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTBACKWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(3000);
    }//FASTBACWARD
    #endif 


    //STOP
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_STOP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("STOP", s32Ret, iCount);
    }//STOP



    //UNREG_NOTIFICATION AUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T154_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//UNREG_NOTIFICATION

    //UNREG_NOTIFICATION CDCTRL

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T154_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION CTRL", s32Ret, iCount);
    }//UNREG_NOTIFICATION CDCTRL


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T154_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T154_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

  } //for(iCount = 0; iCount < OEDT_CD_T154_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T154 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T154_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T154 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T154_RESULT_OK_VALUE != u32ResultBitMask)

  vEject();
  vLoad();
  vEject();
  return u32ResultBitMask;
}



/*****************************************************************************/
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*                            OEDT_CD_T155                          */
/*****************************************************************************/
#define OEDT_CD_T155_COUNT                                               2
#define OEDT_CD_T155_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T155_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T155_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T155_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T155_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T155_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T155_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T155_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T155_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T155_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T155_GETTRACKINFO_VAL_CTRL_AUDIO_ERROR_BIT      0x00000100
#define OEDT_CD_T155_GETTRACKINFO_VAL_CTRL_DATA_ERROR_BIT       0x00000200
#define OEDT_CD_T155_GETDISKTYPE_RESULT_ERROR_BIT               0x00000400
#define OEDT_CD_T155_GETDISKTYPE_TYPE_ERROR_BIT                 0x00000800
#define OEDT_CD_T155_GETMEDIAINFO_RESULT_ERROR_BIT              0x00001000
#define OEDT_CD_T155_GETMEDIAINFO_TYPE_ERROR_BIT                0x00002000
#define OEDT_CD_T155_GETMEDIAINFO_FS_ERROR_BIT                  0x00004000
#define OEDT_CD_T155_GETMEDIAINFO_MEDIAID_ERROR_BIT             0x00008000

#define OEDT_CD_T155_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T155_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T155_bMediaReady;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T155_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T155_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T155_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T155
 *DESCRIPTION  :  MASCA 05 CD (Mixed-Mode Fertigungs-CD) required,
 *                detect MixedMode CD
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T155(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T155_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T155(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T155_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T155_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T155_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T155_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T155_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T155_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T155_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T155_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T155_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T155_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO


    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T155_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //after media ready from PRM
    //it takes some time, until dev_cdctrl is notified
    // because ISO-FS is blocking SED-callback
    // while reading from CD!
    //(void)OSAL_s32ThreadWait(8000);

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T155_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 11))
      {
        u32ResultBitMask |= OEDT_CD_T155_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO



    //GETTRACKINFO
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T155_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T155_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        //track 1 is data, tracks 2 - 11 are audio tracks
        if(u8Track == 1)
        {
          if(0x00 == (0x04 & rInfo.u32TrackControl))
          { //data bit not set
            u32ResultBitMask |= OEDT_CD_T155_GETTRACKINFO_VAL_CTRL_DATA_ERROR_BIT;
          } //if()
        }
        else //if(u8Track == 1)
        {
          if(0x00 != (0x04 & rInfo.u32TrackControl))
          { //data bit set
            u32ResultBitMask |= OEDT_CD_T155_GETTRACKINFO_VAL_CTRL_AUDIO_ERROR_BIT;
          } //if()
        } //else //if(u8Track == 1)
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO


    //GETDISKTYPE
    {
      OSAL_trDiskType rDiskType;

      rDiskType.u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
      rDiskType.u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
      s32Arg = (tS32)&rDiskType;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T155_GETDISKTYPE_RESULT_ERROR_BIT 
                          : 0;

      if(rDiskType.u8DiskType != ATAPI_C_U8_DISK_TYPE_CD)
      {
        u32ResultBitMask |=OEDT_CD_T155_GETDISKTYPE_TYPE_ERROR_BIT;

      } //if(rDiskType.u8DiskSubType != ATAPI_C_U8_DISK_TYPE_UNKNOWN)

      vPrintOsalResult("GETDISKTYPE", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetDiskType: Type %u, SubType %u",
              (unsigned int)rDiskType.u8DiskType,
              (unsigned int)rDiskType.u8DiskSubType
            );


      } //if(s32Ret != OSAL_ERROR)
    }//GETDISKTYPE


    //GETMEDIAINFO
    {
      OSAL_trMediaInfo rInfo;

      rInfo.nTimeZone          = 0;
      rInfo.szcCreationDate[0] ='\0';
      rInfo.szcMediaID[0]      ='\0';
      rInfo.u8FileSystemType   = OSAL_C_U8_FS_TYPE_UNKNOWN;
      rInfo.u8MediaType        = OSAL_C_U8_UNKNOWN_MEDIA;

      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T155_GETMEDIAINFO_RESULT_ERROR_BIT 
                          : 0;

      /*
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c0, "Type %u, FS %u, TZ %u, [%s] [%s]",(tU32)rInfo.u8MediaType,
                          (tU32)rInfo.u8FileSystemType,
                          (tU32)rInfo.nTimeZone,
                       (const char*)rInfo.szcCreationDate,
                       (const char*)szTmp
                     );*/


      if(OSAL_C_U8_AUDIO_MEDIA != rInfo.u8MediaType)
      {
        u32ResultBitMask |= OEDT_CD_T155_GETMEDIAINFO_TYPE_ERROR_BIT;
      } //if(OSAL_C_U8_AUDIO_MEDIA == rInfo.u8MediaType)

      if(OSAL_C_U8_FS_TYPE_UNKNOWN != rInfo.u8FileSystemType)
      {
        u32ResultBitMask |= OEDT_CD_T155_GETMEDIAINFO_FS_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      if(memcmp(rInfo.szcMediaID, OEDT_CD_HASH_MASCA_05, OEDT_CD_HASH_SIZE)
         != 0)
      {
        u32ResultBitMask |= OEDT_CD_T155_GETMEDIAINFO_MEDIAID_ERROR_BIT;
      } //if(OSAL_C_U8_FS_TYPE_UNKNOWN == rInfo.u8FileSystemType)

      vPrintOsalResult("GETMEDIAINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        char szTmp[33];
        memcpy(szTmp, rInfo.szcMediaID, 32);
        szTmp[32]='\0';
        OCDTR(c2, " MediaInfo: MediaType %u"
              " FS %u TimeZone %u"
              " Date [%s] ID [%s]",
              (unsigned int)rInfo.u8MediaType,
              (unsigned int)rInfo.u8FileSystemType,
              (unsigned int)rInfo.nTimeZone,
              (const char*)rInfo.szcCreationDate,
              (const char*)szTmp
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETMEDIAINFO


    //UNREG_NOTIFICATION

    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T155_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION


    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T155_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T155_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T155 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T155_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T155 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T155_RESULT_OK_VALUE != u32ResultBitMask)

  (void)OSAL_s32ThreadWait(8000);
  //vEject();
  return u32ResultBitMask;
}


/*****************************************************************************/
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*                            OEDT_CD_T156                          */
/*****************************************************************************/

#define OEDT_CD_T156_COUNT                                               1
#define OEDT_CD_T156_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T156_DEVICE_AUDIO_NAME        OSAL_C_STRING_DEVICE_CDAUDIO
#define OEDT_CD_T156_MASCA05_TRACKS                                 (11+1)



#define OEDT_CD_T156_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T156_OPEN_CTRL_RESULT_ERROR_BIT                 0x00000001
#define OEDT_CD_T156_OPEN_AUDIO_RESULT_ERROR_BIT                0x00000002
#define OEDT_CD_T156_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT     0x00000004
#define OEDT_CD_T156_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT    0x00000008


#define OEDT_CD_T156_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T156_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T156_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T156_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000080
#define OEDT_CD_T156_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000100
#define OEDT_CD_T156_GETTRACKINFOAUDIO_RESULT_ERROR_BIT         0x00000200
#define OEDT_CD_T156_GETADDITIONALCDINFO_RESULT_ERROR_BIT       0x00000400
#define OEDT_CD_T156_SETPLAYRANGE_RESULT_ERROR_BIT              0x00000800
#define OEDT_CD_T156_PLAY_RESULT_ERROR_BIT                      0x00001000
//#define OEDT_CD_T156_WAIT_FOR_PLAY_ERROR_BIT                    0x00002000
#define OEDT_CD_T156_WAIT_FOR_COMPLETED_ERROR_BIT               0x00004000
#define OEDT_CD_T156_START_TIME_ERROR_BIT                       0x00008000
#define OEDT_CD_T156_END_TIME_ERROR_BIT                         0x00010000
//#define OEDT_CD_T156_GETPLAYINFO_RESULT_ERROR_BIT               0x00020000
//#define OEDT_CD_T156_FASTFORWARD_RESULT_ERROR_BIT               0x00004000
//#define OEDT_CD_T156_FASTBACKWARD_RESULT_ERROR_BIT              0x00008000
#define OEDT_CD_T156_STOP_RESULT_ERROR_BIT                      0x00040000


#define OEDT_CD_T156_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT   0x10000000
#define OEDT_CD_T156_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT  0x20000000
#define OEDT_CD_T156_CLOSE_CTRL_RESULT_ERROR_BIT                0x40000000
#define OEDT_CD_T156_CLOSE_AUDIO_RESULT_ERROR_BIT               0x80000000

static volatile tBool T156_bIsFirstPlayInfo         = TRUE;
static volatile tBool T156_bMediaReady              = FALSE;
static volatile OSAL_trPlayInfo T156_rZeroPlayInfo  = {0};
static volatile OSAL_trPlayInfo T156_rFirstPlayInfo = {0};
static volatile OSAL_trPlayInfo T156_rLastPlayInfo  = {0};
//static volatile tBool T156_bStatusPlayInProgress  = FALSE;
static volatile tBool T156_bStatusCompleted       = FALSE;
//static tU32  T156_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
static OSAL_trTrackInfo T156_arTrackInfo[OEDT_CD_T156_MASCA05_TRACKS] = {0}; 

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T156_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T156_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);


    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T156_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}


/*****************************************************************************
* FUNCTION:     T156_vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback
******************************************************************************/
static tVoid T156_vPlayInfoNotify(tPVoid pvCookie,
                                  const OSAL_trPlayInfo* prInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);

  if(NULL != prInfo)
  {
    OCDTR(c2,"T156_vPlayInfoNotify:"
          " AbsMSF [%02u:%02u:%02u],"
          " RelTMSF [%02u-%02u:%02u:%02u],"
          " PlayStatus [0x%08X]",
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32TrackNumber,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
          (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
          (unsigned int)prInfo->u32StatusPlay
        );

    switch(prInfo->u32StatusPlay)
    {
    case OSAL_C_S32_PLAY_PAUSED:
      OCDTR(c3,"PAUSE");
      break;
    case OSAL_C_S32_PLAY_COMPLETED:
      OCDTR(c3,"COMPLETED");
      T156_bStatusCompleted = TRUE;
      break;
    case OSAL_C_S32_PLAY_IN_PROGRESS:
      OCDTR(c3,"PLAY_IN_PROGRESS");
      //T156_bStatusPlayInProgress = TRUE;
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
      OCDTR(c3,"STOPPED_DUE_TO_ERROR");
      break;
    case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
      OCDTR(c3,"STOPPED_DUE_TO_UNDERVOLTAGE");
      break;
    case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
      OCDTR(c3,"NOT VALID");
      break;
    case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
      OCDTR(c3,"NO CURRENT AUDIO STATUS");
      break;
    default:
      OCDTR(c1,"- default -");
      break;
    } //switch(prInfo->u32StatusPlay)


    //T156_u32StatusPlay  = prInfo->u32StatusPlay;

    if(T156_bIsFirstPlayInfo)
    {
      T156_bIsFirstPlayInfo = FALSE;
      T156_rFirstPlayInfo = *prInfo;
      T156_rLastPlayInfo = *prInfo;
    }
    else //if(T156_bIsFirstPlayInfo)
    {
      T156_rLastPlayInfo = *prInfo;
    } //else //if(T156_bIsFirstPlayInfo)
  } //if(NULL != prInfo)

}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T156
 *DESCRIPTION  :  MASCA 05 CD (MixedMode) required
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T156(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T156_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl  = OSAL_ERROR;
  OSAL_tIODescriptor hCDaudio = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;
  tU8  u8MinT, u8MaxT; //first track, last tracl

  OCDTR(c2, "tU32 OEDT_CD_T156(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T156_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T156_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T156_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T156_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T156_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    /* open device */
    hCDaudio = OSAL_IOOpen(OEDT_CD_T156_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDaudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T156_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T156_OPEN_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDaudio)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T156_DEVICE_AUDIO_NAME, (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_ERROR == hCDaudio)

    //REG_NOTIFICATION CDCTRL
    T156_bMediaReady = FALSE;

    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T156_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_REG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION CTRL", s32Ret, iCount);
    }//REG_NOTIFICATION

    //REG_NOTIFICATION CDAUDIO
    //T156_bMediaReady = FALSE;
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T156_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_REG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//REG_NOTIFICATION CDAUDIO

    //wait until media ready

    {
      tInt iEmergencyExit = 25;
      while((TRUE != T156_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        OCDTR(c2, "Wait while media ready");
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //after media ready from PRM
    //it takes some time, until dev_cdctrl is notified
    // because ISO-FS is blocking SED-callback
    // while reading from CD!
    //(void)OSAL_s32ThreadWait(8000);


    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T156_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if MASCA 03 CD with 16 tracks
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 11))
      {
        u32ResultBitMask |= OEDT_CD_T156_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)


      u8MinT = (tU8)rCDInfo.u32MinTrack;
      u8MaxT = (tU8)rCDInfo.u32MaxTrack;
    }//GETCDINFO

    //GETTRACKINFO CTRL
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8Track;
      tU32 u32PrevLBA = 0;
      //start at track 2
      // (get info for track 1 would cause mixed mode switch to data)
      for(u8Track = u8MinT+1; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        rInfo.u32LBAAddress   = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T156_GETTRACKINFO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          OCDTR(c2, " GetTrackInfo:"
                " Track [%02d],"
                " StartLBA [%u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.u32LBAAddress,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

        //check values, start lba should be  ascending
        if(rInfo.u32LBAAddress < u32PrevLBA)
        {
          u32ResultBitMask |= OEDT_CD_T156_GETTRACKINFO_VAL_LBA_ERROR_BIT;
        } //if(rInfo.u32LBAAddress <= u32PrevLBA)
        u32PrevLBA = rInfo.u32LBAAddress;

        if(0x00 != (0x04 & rInfo.u32TrackControl))
        { //data bit set
          u32ResultBitMask |= OEDT_CD_T156_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
        } //if()
      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO CTRL

    //GETTRACKINFO AUDIO
    {
      OSAL_trTrackInfo rInfo;
      tU8 u8Track;
      //tU32 u32PrevLBA = 0;
      for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track++)
      {
        rInfo.u32TrackNumber  = (tU32)u8Track;
        rInfo.u32TrackControl = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
        u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                            OEDT_CD_T156_GETTRACKINFOAUDIO_RESULT_ERROR_BIT
                            : 0;
        vPrintOsalResult("GETTRACKINFO AUDIO", s32Ret, iCount);
        if(s32Ret != OSAL_ERROR)
        {
          if(u8Track < OEDT_CD_T156_MASCA05_TRACKS)
          {
            T156_arTrackInfo[u8Track] = rInfo; 
          } //if(u8Track < OEDT_CD_T156_MASCA05_TRACKS)
          OCDTR(c2, " GetTrackInfoAudio:"
                " Track [%02d],"
                " Start MSF [%02u:%02u:%02u],"
                " End MSF [%02u:%02u:%02u],"
                " Control  [0x%08X]",
                (unsigned int)rInfo.u32TrackNumber,
                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                (unsigned int)rInfo.u32TrackControl
              );

        } //if(s32Ret != OSAL_ERROR)

      } //for(u8Track = u8MinT; u8Track <= u8MaxT; u8Track)
    }//GETTRACKINFO AUDIO

    //GETADDITIONALCDINFO
    {
      OSAL_trAdditionalCDInfo rInfo;

      rInfo.u32DataTracks[0] = 0;
      rInfo.u32DataTracks[1] = 0;
      rInfo.u32DataTracks[2] = 0;
      rInfo.u32DataTracks[3] = 0;
      rInfo.u32InfoBits   = 0;

      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_GETADDITIONALCDINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETADDITIONALCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " AdditionalCDInfo:"
              " DataTrackBits [0x%08X] [0x%08X] [0x%08X] [0x%08X],"
              " InfoBits [0x%08X],",
              (unsigned int)rInfo.u32DataTracks[0],
              (unsigned int)rInfo.u32DataTracks[1],
              (unsigned int)rInfo.u32DataTracks[2],
              (unsigned int)rInfo.u32DataTracks[3],
              (unsigned int)rInfo.u32InfoBits
            );

      } //if(s32Ret != OSAL_ERROR)
    }//GETADDITIONALCDINFO

    //range+play
    {
      static const tU16 EOT=0xFFFF;
      //startTrack + start offset in seconds
      tU8  au8ST[]  = {2, 2,3,4,5,6,7,8,9,10,11, 10,  2,  3,  4,  5,  6,  2,  8,  7,  7, 11, 11,  2, 11, 11};
      tU16 au16SO[] = {0,35,0,0,0,0,0,0,0, 0, 0, 25, 30,155,154, 55,230, 30, 26,  0, 20, 25,  0, 35, 25, 29};
      //endTrack + end offset in seconds
      tU8  au8ET[]  = {2, 3,3,4,5,6,7,8,9,10,11, 10,  2,  3,  4,  5,  6,  3,  9,  7,  7, 11, 11,  2, 11, 11};
      tU16 au16EO[] = {1, 1,2,2,3,3,4,4,5, 5, 6,EOT,EOT,EOT,EOT,EOT,EOT,  5,  0, 10,EOT, 29,  1,EOT,EOT,EOT};

      tInt iCnt;
      tInt iASize  = OEDT_CD_ARSIZE(au8ST);

      for(iCnt = 0; iCnt < iASize; iCnt++)
      {
        //reset
        T156_bIsFirstPlayInfo = TRUE;
        T156_rFirstPlayInfo = T156_rZeroPlayInfo;
        T156_rLastPlayInfo  = T156_rZeroPlayInfo;
        //T156_u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
        //T156_bStatusPlayInProgress  = FALSE;
        T156_bStatusCompleted       = FALSE;

        //SETPLAYRANGE
        {
          OSAL_trPlayRange rPlayRange = {0};
          OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rPlayRange);

          rPlayRange.rStartAdr.u8Track   = au8ST[iCnt];
          rPlayRange.rStartAdr.u16Offset = au16SO[iCnt];
          rPlayRange.rEndAdr.u8Track     = au8ET[iCnt];
          rPlayRange.rEndAdr.u16Offset   = au16EO[iCnt];

          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
          s32Arg = (tS32)&rPlayRange;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T156_SETPLAYRANGE_RESULT_ERROR_BIT 
                              : 0;
          OCDTR(c2, "SETPLAYRANGE ArrayIndex %d "
                "STO[%02u-%04u] - ETO[%02u-%04u]",
                iCnt,
                (unsigned int)au8ST[iCnt],
                (unsigned int)au16SO[iCnt],
                (unsigned int)au8ET[iCnt],
                (unsigned int)au16EO[iCnt]);
          vPrintOsalResult("SETPLAYRANGE", s32Ret, iCount);
        }//SETPLAYRANGE

        //PLAY
        {
          s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
          s32Arg = 0;
          s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                              OEDT_CD_T156_PLAY_RESULT_ERROR_BIT : 0;
          OCDTR(c2, "PLAY ArrayIndex %d", iCnt);
          vPrintOsalResult("PLAY", s32Ret, iCount);
        }//PLAY


        //wait for play-in-progress state
        #if 0
        if(OEDT_CD_T156_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 7;

          while((FALSE == T156_bStatusPlayInProgress)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while(FALSE == T156_bStatusPlayInProgress)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T156_WAIT_FOR_PLAY_ERROR_BIT : 0;
        } //if(OEDT_CD_T156_RESULT_OK_VALUE == u32ResultBitMask)
        #endif

        //wait for play completed
        if(OEDT_CD_T156_RESULT_OK_VALUE == u32ResultBitMask)
        {
          tInt iEmergencyExit = 10 * 30;

          while((FALSE == T156_bStatusCompleted)
                &&
                (iEmergencyExit-- > 0))
          {
            (void)OSAL_s32ThreadWait(100);
          } //while((FALSE == T156_bStatusCompleted)
          u32ResultBitMask |= (iEmergencyExit <=0) ?
                              OEDT_CD_T156_WAIT_FOR_COMPLETED_ERROR_BIT : 0;
        } //if(OEDT_CD_T156_RESULT_OK_VALUE == u32ResultBitMask)


        //check play info against play range
        {
          tU8  u8FT, u8LT; //first/last track in playtime
          tU16 u16FO, u16LO; //first/last playtime offset get from playtime
          tU8  u8SetPointST; //start track
          tU16 u16SetPointSO; //start offset
          tU8  u8SetPointET; //end track
          tU16 u16SetPointEO; //end offset

          u8FT  = (tU8)T156_rFirstPlayInfo.u32TrackNumber;
          u16FO = OEDT_CD_RELOFFSETFROMPLAYINFO(T156_rFirstPlayInfo);

          u8LT  = (tU8)T156_rLastPlayInfo.u32TrackNumber;
          u16LO = OEDT_CD_RELOFFSETFROMPLAYINFO(T156_rLastPlayInfo);

          u8SetPointST  = au8ST[iCnt];
          u16SetPointSO = au16SO[iCnt];
          u8SetPointET  = au8ET[iCnt];
          if(0xFFFF == au16EO[iCnt])
          { //until End-Of-EndTrack
            tU32 u32StartSec =
            OEDT_CD_MS2SECS(T156_arTrackInfo[u8SetPointET].rStartAdr);
            tU32 u32EndSec   =
            OEDT_CD_MS2SECS(T156_arTrackInfo[u8SetPointET].rEndAdr);
            u16SetPointEO = (tU16)(u32EndSec - u32StartSec);
          }
          else //if(0xFFFF == au16EO[iCnt])
          {
            u16SetPointEO = au16EO[iCnt];
          } //else //if(0xFFFF == au16EO[iCnt])

          if(u8SetPointST != u8FT
             ||
             u16SetPointSO != u16FO)
          {
            u32ResultBitMask |= OEDT_CD_T156_START_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR Start Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }
          else
          {
            OCDTR(c2, "SUCCESS Start Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointST,
                  (unsigned int)u16SetPointSO,
                  (unsigned int)u8FT,
                  (unsigned int)u16FO);
          }

          if(u8SetPointET != u8LT
             ||
             u16SetPointEO != u16LO)
          {
            u32ResultBitMask |= OEDT_CD_T156_END_TIME_ERROR_BIT;
            OCDTR(c1, "ERROR End Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }
          else
          {
            OCDTR(c2, "SUCCESS End Range %02d - "
                  "setpoint Track-Offset %02u-%04u, "
                  "measurement T-O %02u-%04u",
                  (int)iCnt,
                  (unsigned int)u8SetPointET,
                  (unsigned int)u16SetPointEO,
                  (unsigned int)u8LT,
                  (unsigned int)u16LO);
          }

        }
      } //for(iCnt = 0; iCnt < iASize; iCnt++)
    } //range+ play

    //GETPLAYINFO
    #if 0
    {
      OSAL_trPlayInfo rInfo = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rInfo);

      rInfo.u32TrackNumber           = 0;
      rInfo.rAbsTrackAdr.u8MSFMinute = 0;
      rInfo.rAbsTrackAdr.u8MSFSecond = 0;
      rInfo.rAbsTrackAdr.u8MSFFrame  = 0;
      rInfo.rRelTrackAdr.u8MSFMinute = 0;
      rInfo.rRelTrackAdr.u8MSFSecond = 0;
      rInfo.rRelTrackAdr.u8MSFFrame  = 0;
      rInfo.u32StatusPlay            = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_GETPLAYINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETPLAYINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " PlayInfo:"
              " Track [%02u],"
              " AbsMSF [%02u:%02u:%02u],"
              " RelMSF [%02u:%02u:%02u],"
              " PlayStatus [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rAbsTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFMinute,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFSecond,
              (unsigned int)rInfo.rRelTrackAdr.u8MSFFrame,
              (unsigned int)rInfo.u32StatusPlay
            );
      } //if(s32Ret != OSAL_ERROR)
    }//GETPLAYINFO
    #endif

    //FASTFORWARD
    #if 0
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_FASTFORWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTFORWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(5000);
    }//FASTFORWARD

    //FASTBACKWARD
    if(0)
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_FASTBACKWARD_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("FASTBACKWARD", s32Ret, iCount);
      (void)OSAL_s32ThreadWait(3000);
    }//FASTBACWARD
    #endif 

    //STOP
    {
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
      s32Arg = 0;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_STOP_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("STOP", s32Ret, iCount);
    }//STOP

    //UNREG_NOTIFICATION AUDIO
    {
      OSAL_trPlayInfoReg rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pCallback = T156_vPlayInfoNotify;
      rReg.pvCookie  = NULL;
      s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDaudio, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_UNREG_NOTIFICATION_AUDIO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION AUDIO", s32Ret, iCount);
    }//UNREG_NOTIFICATION

    //UNREG_NOTIFICATION CDCTRL
    {
      OSAL_trRelNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T156_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION CTRL", s32Ret, iCount);
    }//UNREG_NOTIFICATION CDCTRL


    s32Ret = OSAL_s32IOClose(hCDaudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDaudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T156_CLOSE_AUDIO_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDaudio, iCount);
    } //else //if(OSAL_OK != s32Ret)

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T156_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

  } //for(iCount = 0; iCount < OEDT_CD_T156_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T156 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);


  if(OEDT_CD_T156_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T156 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T156_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*                            OEDT_CD_T157                          */
/*****************************************************************************/

#define OEDT_CD_T157_COUNT                                               1
#define OEDT_CD_T157_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T157_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T157_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T157_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T157_GETLOADERINFO_RESULT_ERROR_BIT             0x00000004
#define OEDT_CD_T157_GETLOADERINFO_STATUS_ERROR_BIT             0x00000008
#define OEDT_CD_T157_GETCDINFO_RESULT_ERROR_BIT                 0x00000010
#define OEDT_CD_T157_GETCDINFO_VALUE_ERROR_BIT                  0x00000020
#define OEDT_CD_T157_GETTRACKINFO_RESULT_ERROR_BIT              0x00000040
#define OEDT_CD_T157_DATA_MEDIA_ERROR_BIT                       0x00000080
#define OEDT_CD_T157_GETTRACKINFO_VAL_LBA_ERROR_BIT             0x00000100
#define OEDT_CD_T157_GETTRACKINFO_VAL_CTRL_ERROR_BIT            0x00000200


#define OEDT_CD_T157_READRAWLBA_RESULT_ERROR_BIT                0x00004000
//#define OEDT_CD_T157_READRAWLBA_VERIFY_ERROR_BIT                0x00008000
#define OEDT_CD_T157_READRAWLBA_SPEED_ERROR_BIT                 0x00010000


#define OEDT_CD_T157_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x40000000
#define OEDT_CD_T157_CLOSE_RESULT_ERROR_BIT                     0x80000000

static tBool T157_bMediaReady;
static tU16  T157_u16MediaType;

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T157_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType   = 0;
  tU16 u16State      = 0;
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);


  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    OCDTR(c2, "T157_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      {
        T157_u16MediaType = u16State;
        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED:
          break;

        case OSAL_C_U16_INCORRECT_MEDIA:
          break;

        case OSAL_C_U16_DATA_MEDIA:
          break;

        case OSAL_C_U16_AUDIO_MEDIA:
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA:
          break;

        default:
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      switch(u16State)
      {
      case OSAL_C_U16_DEVICE_OK:
        break;
      case OSAL_C_U16_DEVICE_FAIL:
        break;
      default:
        ;
      } //switch(u16State)
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY:
        break;
      case OSAL_C_U16_MEDIA_READY:
        T157_bMediaReady = TRUE;
        break;
      default:
        ;
      } //switch(u16MediaState;)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      break;

    case OSAL_C_U16_NOTI_DEVICE_READY:
      {
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY:
          break;
        case OSAL_C_U16_DEVICE_UV_READY:
          break;
        default:
          break;
        } //switch(u16DeviceReady)
      }
      break;


    default:
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
  else //Invalid pointer
  {
    //BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
  }

  //BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T157
 *DESCRIPTION  :  MASCA 05 CD (Mixed Mode) required - turn on DATA + 
 *                read sectors +
 *                performance measurement
 *PARAMETER    :  void
 *RETURNVALUE  :  0 if no error, bit coded errorcode in case of failure
 *HISTORY      :  
 *****************************************************************************/
tU32 OEDT_CD_T157(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T157_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;

  OCDTR(c2, "tU32 OEDT_CD_T157(void)");

  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T157_COUNT; iCount++)
  {
    /* open device */
    hCDctrl = OSAL_IOOpen(OEDT_CD_T157_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T157_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T157_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDctrl)
    {
      OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T157_DEVICE_CTRL_NAME, (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_ERROR == hCDctrl)

    //REG_NOTIFICATION
    T157_bMediaReady = FALSE;
    T157_u16MediaType = OSAL_C_U16_UNKNOWN_MEDIA;
    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.pu32Data            = 0;
      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u8Status            = 0;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;
      rReg.pCallback           = T157_vMediaTypeNotify;

      s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T157_REG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("REG_NOTIFICATION", s32Ret, iCount);
    }//REG_NOTIFICATION

    //wait until media ready
    {
      tInt iEmergencyExit = 25;
      while((TRUE != T157_bMediaReady)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
    }

    //GETLOADERINFO
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= OEDT_CD_T157_GETLOADERINFO_RESULT_ERROR_BIT;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          break;
        case OSAL_C_U8_NO_MEDIA:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          u32ResultBitMask |= OEDT_CD_T157_GETLOADERINFO_STATUS_ERROR_BIT;
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)


      vPrintOsalResult("GETLOADERINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " Loader: [%d]",
              (int)rInfo.u8LoaderInfoByte);
      } //if(s32Ret != OSAL_ERROR)
    }//GETLOADERINFO

    //GETCDINFO
    {
      OSAL_trCDInfo  rCDInfo;

      rCDInfo.u32MaxTrack = 0;
      rCDInfo.u32MinTrack = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
      s32Arg = (tS32)&rCDInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T157_GETCDINFO_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("GETCDINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " CDInfo: MinTrack %02u"
              " MaxTrack %02u",
              (unsigned int)rCDInfo.u32MinTrack,
              (unsigned int)rCDInfo.u32MaxTrack
            );
      } //if(s32Ret != OSAL_ERROR)

      //check, if cd contains 11 tracks (track 1 == data)
      if((rCDInfo.u32MinTrack != 1)
         ||
         (rCDInfo.u32MaxTrack != 11))
      {
        u32ResultBitMask |= OEDT_CD_T157_GETCDINFO_VALUE_ERROR_BIT;
      } //if(wrong cd)
    }//GETCDINFO

    //GETTRACKINFO track 1, turn on DATA Access
    T157_bMediaReady = FALSE;
    {
      OSAL_trCDROMTrackInfo rInfo;
      tU8 u8TrackNumber = 1;

      rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
      rInfo.u32TrackControl = 0;
      rInfo.u32LBAAddress   = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                          OEDT_CD_T157_GETTRACKINFO_RESULT_ERROR_BIT
                          : 0;
      vPrintOsalResult("GETTRACKINFO", s32Ret, iCount);
      if(s32Ret != OSAL_ERROR)
      {
        OCDTR(c2, " GetTrackInfo:"
              " Track [%02d],"
              " StartLBA [%u],"
              " Control  [0x%08X]",
              (unsigned int)rInfo.u32TrackNumber,
              (unsigned int)rInfo.u32LBAAddress,
              (unsigned int)rInfo.u32TrackControl
            );

      } //if(s32Ret != OSAL_ERROR)

      //wait until media ready after switching to data mode
      {
        tInt iEmergencyExit = 25;
        while((TRUE != T157_bMediaReady)
              &&
              (iEmergencyExit-- > 0))
        {
          (void)OSAL_s32ThreadWait(250);
        } //while(!ready)
      }

      //check data media
      if(T157_u16MediaType != OSAL_C_U16_DATA_MEDIA)
      {
        OCDTR(c2, " WRONG MEDIA TYPE %u:", (unsigned int)T157_u16MediaType);
        u32ResultBitMask |= OEDT_CD_T157_DATA_MEDIA_ERROR_BIT;
      }

      //check values
      if(150 != rInfo.u32LBAAddress)
      {
        u32ResultBitMask |= OEDT_CD_T157_GETTRACKINFO_VAL_LBA_ERROR_BIT;
      } //if(150 != rInfo.u32LBAAddress)

      if(0x00 == (0x04 & rInfo.u32TrackControl))
      { //data bit missing
        u32ResultBitMask |= OEDT_CD_T157_GETTRACKINFO_VAL_CTRL_ERROR_BIT;
      } //if()
    }//GETTRACKINFO

    //READRAWDATA LBA forward
    {
      tU8 *pu8Buf;
      tU32 u32LBA;
      tU32 u32NumBlocks = 20;
      OSAL_trReadRawInfo rRRInfo = {0};
      tU32 u32NumberOfSecsToRead = u32NumBlocks * 200;
      tU32 u32SCnt;
      OSAL_tMSecond sMS;
      OSAL_tMSecond dMS;
      double dMSpSector;

      (void)rRRInfo; //L i n t

      sMS = OSAL_ClockGetElapsedTime();
      pu8Buf = (tU8*)malloc(u32NumBlocks * OEDT_CD_BPS);
      if(NULL != pu8Buf)
      {
        memset(pu8Buf,0xFF,u32NumBlocks * OEDT_CD_BPS);
        u32SCnt = 0;
        u32LBA = 21;  //start sector
        while((u32SCnt < u32NumberOfSecsToRead)
              &&
              (u32ResultBitMask == OEDT_CD_T157_RESULT_OK_VALUE))
        {
          //sector LBA 21 is first sector of single file on Test-CD "MASCA_02"
          //sector number is written in sectordata!

          OCDTR(c2, "ReadRawDataLBA LoopCount %4u, LBA %5u, SecCount %u", 
                (unsigned int)u32SCnt,
                (unsigned int)u32LBA,
                (unsigned int)u32NumBlocks);


          rRRInfo.u32NumBlocks  = u32NumBlocks;
          rRRInfo.u32LBAAddress = u32LBA;  /*LBA , NOT ZLBA!*/
          rRRInfo.ps8Buffer     = (tS8*)pu8Buf;

          s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
          s32Arg = (tS32)&rRRInfo;
          s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                              OEDT_CD_T157_READRAWLBA_RESULT_ERROR_BIT
                              : 0;
          vPrintOsalResult("READRAWDATALBA", s32Ret, iCount);
          u32LBA  += u32NumBlocks;
          u32SCnt += u32NumBlocks;
        } //while(u32SCnt-- > 0 && !error)

        free(pu8Buf);
      } //if(NULL != pu8Buf)


      dMS = OSAL_ClockGetElapsedTime() - sMS;
      dMSpSector = (double) ((double)dMS /  (double)u32NumberOfSecsToRead);
      OCDTR(c2, "ReadRawDataLBA: "
            "NumberOfSecsRead %u, "
            "Time %u ms, "
            "Speed %.1f ms/sector",
            (unsigned int)u32NumberOfSecsToRead,
            (unsigned int)dMS,
            dMSpSector
          );

      //check average read time per sector
      if(dMSpSector > 20.0)
      {
        OCDTR(c1, "ReadRawDataLBA Speed ERROR: "
              " %.1f ms/sector too slow (max. 20ms)",
              dMSpSector
            );
        u32ResultBitMask |= OEDT_CD_T157_READRAWLBA_SPEED_ERROR_BIT;
      } //if(dMS > 5000)
    }//READRAWDATA LBA

    //UNREG_NOTIFICATION
    {
      OSAL_trNotifyData rReg = {0};
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

      rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
      rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
      rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                  | OSAL_C_U16_NOTI_MEDIA_STATE
                                  | OSAL_C_U16_NOTI_DEVICE_READY
                                  | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                  | OSAL_C_U16_NOTI_DEFECT;

      s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
      s32Arg = (tS32)&rReg;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
      u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T157_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                          : 0;
      vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, iCount);
    }//UNREG_NOTIFICATION

    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (count %d): %s",
            (unsigned int)hCDctrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T157_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u (count %d)",
            (unsigned int)hCDctrl, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T157_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T157 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);

  if(OEDT_CD_T157_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "OEDT_CD: T157 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T157_RESULT_OK_VALUE != u32ResultBitMask)

  //vEject();
  return u32ResultBitMask;
}

/*****************************************************************************/
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*                            OEDT_CD_T158                          */
/*****************************************************************************/

#define OEDT_CD_T158_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL

#define OEDT_CD_T158_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T158_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T158_REG_NOTIFICATION_RESULT_ERROR_BIT          0x00000002
#define OEDT_CD_T158_REG_NOTIFICATION2_RESULT_ERROR_BIT         0x00000004
#define OEDT_CD_T158_CLOSEDOOR_RESULT_ERROR_BIT                 0x00000008
#define OEDT_CD_T158_EJECTMEDIA_RESULT_ERROR_BIT                0x00000010
#define OEDT_CD_T158_MONITOR_SIZE_ERROR_BIT                     0x00000020
#define OEDT_CD_T158_MONITOR_MEDIACHANGEINFO_ERROR_BIT          0x00000040
#define OEDT_CD_T158_MONITOR_LOADERINFO_ERROR_BIT               0x00000080
#define OEDT_CD_T158_GETTRACKINFO1_RESULT_ERROR_BIT             0x00000100
#define OEDT_CD_T158_GETTRACKINFO2_RESULT_ERROR_BIT             0x00000200

#define OEDT_CD_T158_UNREG_NOTIFICATION_RESULT_ERROR_BIT        0x20000000
#define OEDT_CD_T158_UNREG_NOTIFICATION2_RESULT_ERROR_BIT       0x40000000
#define OEDT_CD_T158_CLOSE_RESULT_ERROR_BIT                     0x80000000



#define OEDT_CD_T158_MONITOR_SIZE                               50

typedef struct OEDT_CD_T158_monitor_tag
{
  OSAL_tMSecond tMS;
  tU32 u32MediaChangeInfo;
  tU8 u8LoaderInfoByte;

}OEDT_CD_T158_monitor_type;


static OEDT_CD_T158_monitor_type  T158_aMon[OEDT_CD_T158_MONITOR_SIZE];
static tInt                       T158_iMonIdx;
static OSAL_tIODescriptor         T158_hCDctrl = OSAL_ERROR;
static tBool                      T158_bMediaReady  = FALSE;
static tBool                      T158_bMediaDefect = FALSE;

//setpoints
//MASCA05, cd was previously inserted, MIxedmode CD switch monitor
static const OEDT_CD_T158_monitor_type  T158_aMonCompMasca05[] = 
{
  //step 1 CD was inserted, data mode (!, from previous test), reg for notify
  {1,OEDT_CD_NOTI32(DEVICE_READY ,DEVICE_READY)   ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},  /*previous test leaves mixed mode cd in data mode!*/
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(TOTAL_FAILURE,DEVICE_OK)      ,OSAL_C_U8_MEDIA_INSIDE},

  //step 2 eject CD
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_EJECT_IN_PROGRESS},

  //step 3 load CD
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},

  //step 4 CD is inserted in Audio mode, switch to DATA mode
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,DATA_MEDIA)     ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},

  //step 5 CD is inserted, switch back to Audio mode
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_NOT_READY),OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_CHANGE ,AUDIO_MEDIA)    ,OSAL_C_U8_MEDIA_INSIDE},
  {1,OEDT_CD_NOTI32(MEDIA_STATE  ,MEDIA_READY)    ,OSAL_C_U8_MEDIA_INSIDE},
  {0,0,0}
};


static tU32 u32T158_work(const char *pcszTxt,
                         const OEDT_CD_T158_monitor_type *pMonComp);

/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
static tVoid T158_vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);

  if(pu32MediaChangeInfo != NULL)
  {
    tU16 u16NotiType;
    tU16 u16State;
    OCDTR(c2, "T158_vMediaTypeNotify: 0x%08X",
          (unsigned int)*pu32MediaChangeInfo);

    u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
    u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
    if(OSAL_C_U16_NOTI_MEDIA_STATE == u16NotiType)
    {
      if(OSAL_C_U16_MEDIA_READY == u16State)
      {
        T158_bMediaReady = TRUE;
      } //if(OSAL_C_U16_MEDIA_READY == u16State)
    } //if(OSAL_C_U16_NOTI_MEDIA_STATE == u16NotiType)

    if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)
    {
      switch(u16State)
      {
      case OSAL_C_U16_DEFECT_DISCTOC:
        T158_bMediaDefect = TRUE;
        break;

      case OSAL_C_U16_DEFECT_LOAD_EJECT:
      case OSAL_C_U16_DEFECT_LOAD_INSERT:
      case OSAL_C_U16_DEFECT_DISC:
      case OSAL_C_U16_DEFECT_READ_ERR:
      case OSAL_C_U16_DEFECT_READ_OK:
      default:
        break;
      } //switch(u16State)
    } //if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)

    if(OSAL_C_U16_NOTI_MEDIA_CHANGE == u16NotiType)
    {
      switch(u16State)
      {
      case OSAL_C_U16_INCORRECT_MEDIA:
        T158_bMediaDefect = TRUE;
        break;

      case OSAL_C_U16_MEDIA_EJECTED:
      case OSAL_C_U16_DATA_MEDIA:
      case OSAL_C_U16_AUDIO_MEDIA:
      case OSAL_C_U16_UNKNOWN_MEDIA:
      default:
        break;
      } //switch(u16State)
    } //if(OSAL_C_U16_NOTI_DEFECT == u16NotiType)

    if(T158_iMonIdx < OEDT_CD_T158_MONITOR_SIZE
       &&
       T158_iMonIdx >= 0)
    {
      T158_aMon[T158_iMonIdx].u32MediaChangeInfo = *pu32MediaChangeInfo;
      T158_aMon[T158_iMonIdx].tMS                = OSAL_ClockGetElapsedTime();

      //loaderinfo
      {
        OSAL_trLoaderInfo rInfo;
        tS32 s32Fun, s32Arg, s32Ret;

        rInfo.u8LoaderInfoByte = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
        s32Arg = (tS32)&rInfo;
        s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);

        if(s32Ret == OSAL_ERROR)
        {
          T158_aMon[T158_iMonIdx].u8LoaderInfoByte = 0xFF;
        }
        else //if(s32Ret == OSAL_ERROR)
        {
          T158_aMon[T158_iMonIdx].u8LoaderInfoByte = rInfo.u8LoaderInfoByte;
        } //else //if(s32Ret == OSAL_ERROR)
      }
      T158_iMonIdx++;
    } // if(T158_iMonIdx < OEDT_CD_T158_MONITOR_SIZE ...
  } //if(pu32MediaChangeInfo != NULL)
}


/******************************************************************************
 *FUNCTION     :  OEDT_CD_T158_masca05
 *DESCRIPTION  :  prints "Insert CD 'MASCA 05'" - text
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
tU32 OEDT_CD_T158_masca05(void)
{
  return u32T158_work("*   Insert CD 'MASCA 05' (Mixed Mode CD)  *",
                      T158_aMonCompMasca05);
}


/******************************************************************************
 *FUNCTION     :  u32T158_work
 *DESCRIPTION  :  Monitors switching of Mixed Mode CD
 *DESCRIPTION  :   (data-eject-insert-data-audio)
 *PARAMETER    :  void
 *RETURNVALUE  :  SUCCESS: 0, FAILURE: bit coded error value
 *****************************************************************************/
static tU32 u32T158_work(const char *pcszTxt,
                         const OEDT_CD_T158_monitor_type *pMonComp)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask = OEDT_CD_T158_RESULT_OK_VALUE;
  tS32 s32Ret;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;
  tS32 s32Fun, s32Arg;
  //tU32 u32ECode;

  OCDTR(c2, "tU32 u32T158_work(void)");

  memset(T158_aMon, 0, sizeof(T158_aMon));
  T158_iMonIdx = 0;
  T158_bMediaReady = FALSE;
  T158_bMediaDefect = FALSE;


  startMS = OSAL_ClockGetElapsedTime();

  /* open device */
  T158_hCDctrl = OSAL_IOOpen(OEDT_CD_T158_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == T158_hCDctrl)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR Open <%s> : %s",
          OEDT_CD_T158_DEVICE_CTRL_NAME,  szError);
    u32ResultBitMask |= OEDT_CD_T158_OPEN_RESULT_ERROR_BIT;
  }
  else //if(OSAL_ERROR == T158_hCDctrl)
  {
    OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X",
          OEDT_CD_T158_DEVICE_CTRL_NAME, (unsigned int)T158_hCDctrl);
  } //else //if(OSAL_ERROR == T158_hCDctrl)

  //REG_NOTIFICATION
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;
    rReg.pCallback           = T158_vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_REG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("REG_NOTIFICATION", s32Ret, 0);
  }//REG_NOTIFICATION

  //GETLOADERINFO - wait while cd is inserted
  {
    tInt iCount = 1800;  //wait for max. 1800 secs
    tBool bCDInserted;
    OSAL_trLoaderInfo rInfo;
    do
    {
      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          bCDInserted = TRUE;
          break;

        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_MEDIA_IN_SLOT:
        case OSAL_C_U8_NO_MEDIA:
          /*lint -fallthrough */
        default:
          bCDInserted = FALSE;
          if((iCount % 5) == 0)
          { //print text each 5 seconds
            OCDTR(c0, "%s", "***********************************");
            OCDTR(c0, "%s", pcszTxt);
            OCDTR(c0, "%s", "***********************************");
          }
          (void)OSAL_s32ThreadWait(1000);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount--;
    }
    while((FALSE == bCDInserted) && (iCount > 0));//GETLOADERINFO
  } //GETLOADERINFO - wait while cd is inserted

  //wait until media ready or defect
  {
    tInt iEmergencyExit = 50;
    while((TRUE != T158_bMediaReady)
          &&
          (TRUE != T158_bMediaDefect)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(250);
    } //while(!ready)
    OCDTR(c2, " Wait Ready 1 DONE:"
              " iEmergencyExit [%d],"
              " T158_bMediaReady [%u],"
              " T158_bMediaDefect [%u]",
              (int)iEmergencyExit,
              (unsigned int)T158_bMediaReady,
              (unsigned int)T158_bMediaDefect
          );
  }
  (void)OSAL_s32ThreadWait(250);

  //UNREG_NOTIFICATION
  {
    OSAL_trRelNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_UNREG_NOTIFICATION_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, 0);
  }//UNREG_NOTIFICATION

  //(void)OSAL_s32ThreadWait(250);

  T158_iMonIdx = 0;
  T158_bMediaReady = FALSE;
  T158_bMediaDefect = FALSE;

  //REG_NOTIFICATION
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;
    rReg.pCallback           = T158_vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_REG_NOTIFICATION2_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("REG_NOTIFICATION", s32Ret, 0);
  }//REG_NOTIFICATION

  //EJECTMEDIA
  {
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
    s32Arg = 0;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_EJECTMEDIA_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("EJECTMEDIA", s32Ret, 0);
  }//EJECTMEDIA

  //GETLOADERINFO - wait while cd is INSLOT
  {
    tInt iCount = 10;  //wait for max. 10 secs
    tBool bCDInSlot;
    OSAL_trLoaderInfo rInfo;
    do
    {
      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        bCDInSlot = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_IN_SLOT:
          bCDInSlot = TRUE;
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
        case OSAL_C_U8_EJECT_IN_PROGRESS:
        case OSAL_C_U8_NO_MEDIA:
        default:
          bCDInSlot = FALSE;
          (void)OSAL_s32ThreadWait(250);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount--;
    }
    while((FALSE == bCDInSlot) && (iCount > 0));//GETLOADERINFO
  } //GETLOADERINFO - wait while cd is INSLOT

  //(void)OSAL_s32ThreadWait(1000);

  T158_bMediaReady = FALSE;
  T158_bMediaDefect = FALSE;
  //CLOSEDOOR
  {
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
    s32Arg = 0;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_CLOSEDOOR_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("CLOSEDOOR", s32Ret, 0);
  }//CLOSEDOOR

  //wait until media ready or defect
  {
    tInt iEmergencyExit = 40;
    while((TRUE != T158_bMediaReady)
          &&
          (TRUE != T158_bMediaDefect)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(500);
    } //while(!ready)
    OCDTR(c2, " Wait Ready 2 DONE:"
              " iEmergencyExit [%d],"
              " T158_bMediaReady [%u],"
              " T158_bMediaDefect [%u]",
              (int)iEmergencyExit,
              (unsigned int)T158_bMediaReady,
              (unsigned int)T158_bMediaDefect
          );
  }
  //(void)OSAL_s32ThreadWait(1000);

  T158_bMediaReady = FALSE;
  T158_bMediaDefect = FALSE;
  //switch to DATA mode
  {
    OSAL_trCDROMTrackInfo rInfo;
    tU8 u8TrackNumber = 1;

    rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
    rInfo.u32TrackControl = 0;
    rInfo.u32LBAAddress   = 0;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
    s32Arg = (tS32)&rInfo;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                        OEDT_CD_T158_GETTRACKINFO1_RESULT_ERROR_BIT
                        : 0;
    vPrintOsalResult("GETTRACKINFO", s32Ret, 1);
    if(s32Ret != OSAL_ERROR)
    {
      OCDTR(c2, " GetTrackInfo:"
            " Track [%02d],"
            " StartLBA [%u],"
            " Control  [0x%08X]",
            (unsigned int)rInfo.u32TrackNumber,
            (unsigned int)rInfo.u32LBAAddress,
            (unsigned int)rInfo.u32TrackControl
          );

    } //if(s32Ret != OSAL_ERROR)

    //wait until media ready or defect
    {
      tInt iEmergencyExit = 50;
      while((TRUE != T158_bMediaReady)
            &&
            (TRUE != T158_bMediaDefect)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
      OCDTR(c2, " Wait Ready 3 DONE:"
                " iEmergencyExit [%u],"
                " T158_bMediaReady [%u],"
                " T158_bMediaDefect [%u]",
                (unsigned int)iEmergencyExit,
                (unsigned int)T158_bMediaReady,
                (unsigned int)T158_bMediaDefect
            );
    }
    //(void)OSAL_s32ThreadWait(1000);
  }

  //switch to AUDIO mode
  T158_bMediaReady  = FALSE;
  T158_bMediaDefect = FALSE;
  //switch to DATA mode
  {
    OSAL_trCDROMTrackInfo rInfo;
    tU8 u8TrackNumber = 2;

    rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
    rInfo.u32TrackControl = 0;
    rInfo.u32LBAAddress   = 0;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
    s32Arg = (tS32)&rInfo;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret != OSAL_OK) ?
                        OEDT_CD_T158_GETTRACKINFO2_RESULT_ERROR_BIT
                        : 0;
    vPrintOsalResult("GETTRACKINFO", s32Ret, 1);
    if(s32Ret != OSAL_ERROR)
    {
      OCDTR(c2, " GetTrackInfo:"
            " Track [%02u],"
            " StartLBA [%u],"
            " Control  [0x%08X]",
            (unsigned int)rInfo.u32TrackNumber,
            (unsigned int)rInfo.u32LBAAddress,
            (unsigned int)rInfo.u32TrackControl
          );

    } //if(s32Ret != OSAL_ERROR)

    //wait until media ready or defect
    {
      tInt iEmergencyExit = 50;
      while((TRUE != T158_bMediaReady)
            &&
            (TRUE != T158_bMediaDefect)
            &&
            (iEmergencyExit-- > 0))
      {
        (void)OSAL_s32ThreadWait(250);
      } //while(!ready)
      OCDTR(c2, " Wait Ready 4 DONE:"
                " iEmergencyExit [%u],"
                " T158_bMediaReady [%u],"
                " T158_bMediaDefect [%u]",
                (unsigned int)iEmergencyExit,
                (unsigned int)T158_bMediaReady,
                (unsigned int)T158_bMediaDefect
            );
    }
  }

  (void)OSAL_s32ThreadWait(500);
  
  //UNREG_NOTIFICATION
  {
    OSAL_trRelNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);

    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u16NotificationType =  OSAL_C_U16_NOTI_MEDIA_CHANGE
                                | OSAL_C_U16_NOTI_MEDIA_STATE
                                | OSAL_C_U16_NOTI_DEVICE_READY
                                | OSAL_C_U16_NOTI_TOTAL_FAILURE
                                | OSAL_C_U16_NOTI_DEFECT;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(T158_hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                        OEDT_CD_T158_UNREG_NOTIFICATION2_RESULT_ERROR_BIT 
                        : 0;
    vPrintOsalResult("UNREG_NOTIFICATION", s32Ret, 0);
  }//UNREG_NOTIFICATION

  //(void)OSAL_s32ThreadWait(1000);

  s32Ret = OSAL_s32IOClose(T158_hCDctrl);
  if(OSAL_OK != s32Ret)
  {
    vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
    OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X: %s",
          (unsigned int)T158_hCDctrl, szError);
    u32ResultBitMask |= OEDT_CD_T158_CLOSE_RESULT_ERROR_BIT;
  }
  else //if(OSAL_OK != s32Ret)
  {
    OCDTR(c2, "OEDT_CD: "
          "SUCCESS CLOSE handle %u",
          (unsigned int)T158_hCDctrl);
  } //else //if(OSAL_OK != s32Ret)

  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "OEDT_CD: T158 Time: %u ms", (unsigned int)diffMS);

  //check monitored data
  {
    tInt i;
    tInt iMonCompSize;
    static char szTxt[256];
    static char szTxt1[32];

    //calculate comparison array size
    iMonCompSize = 0;
    while(pMonComp[iMonCompSize].tMS == 1)
    {
      iMonCompSize++;
    } //while(pMonComp[iMonCompSize].tMS == 1)


    //check
    if(iMonCompSize != T158_iMonIdx)
    {
      u32ResultBitMask |= OEDT_CD_T158_MONITOR_SIZE_ERROR_BIT;
      OCDTR(c1, "Monitored data size mismatch %d != %d",
            (unsigned int)T158_iMonIdx,
            (unsigned int)iMonCompSize);
    }
    else  //if(iMonCompSize != T158_iMonIdx)
    {
      for(i = 0; i < iMonCompSize; i++)
      {
        if(pMonComp[i].u32MediaChangeInfo != OEDT_CD_IGNORE32)
        {
          if(pMonComp[i].u32MediaChangeInfo
             != T158_aMon[i].u32MediaChangeInfo)
          {
            u32ResultBitMask |= OEDT_CD_T158_MONITOR_MEDIACHANGEINFO_ERROR_BIT;
            OCDTR(c1, "Monitored ChangeInfo not equal [%02d] 0x%08X != 0x%08X",
                  i,
                  (unsigned int)pMonComp[i].u32MediaChangeInfo,
                  (unsigned int)T158_aMon[i].u32MediaChangeInfo);
          } //if(u32MediaChangeInfo !=
        } //if(pMonComp[i].u32MediaChangeInfo != OEDT_CD_IGNORE32)

        if(pMonComp[i].u8LoaderInfoByte
           != T158_aMon[i].u8LoaderInfoByte)
        {
          u32ResultBitMask |= OEDT_CD_T158_MONITOR_LOADERINFO_ERROR_BIT;
          OCDTR(c1, "Monitored LoaderInfo not equal [%02d] 0x%02X != 0x%02X",
                i,
                (unsigned int)pMonComp[i].u8LoaderInfoByte,
                (unsigned int)T158_aMon[i].u8LoaderInfoByte);
        } //if(u32MediaChangeInfo !=
      } //for(i = 0; i < iMonCompSize; i++)
    } //else  //if(iMonCompSize != T158_iMonIdx)

    //print monitored data
    for(i = 0; i < T158_iMonIdx; i++)
    {
      tU32 u32Time     = T158_aMon[i].tMS;
      tU16 u16NotiType = OEDT_CD_HIWORD(T158_aMon[i].u32MediaChangeInfo);
      tU16 u16State    = OEDT_CD_LOWORD(T158_aMon[i].u32MediaChangeInfo);

      OCDTR(c3, "%2d: %6ums MediaChangeInfo 0x%08X, LoaderInfo 0x%02X",
            i,
            (unsigned int)T158_aMon[i].tMS,
            (unsigned int)T158_aMon[i].u32MediaChangeInfo,
            (unsigned int)T158_aMon[i].u8LoaderInfoByte);

      switch(u16NotiType)
      {
      case OSAL_C_U16_NOTI_MEDIA_CHANGE:
        {
          sprintf(szTxt, "  [%02d:%08ums] NOTI_MEDIA_CHANGE (0x%04X) -> ",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);

          switch(u16State)
          {
          case OSAL_C_U16_MEDIA_EJECTED:
            strcat(szTxt, "MEDIA_EJECTED");
            break;

          case OSAL_C_U16_INCORRECT_MEDIA:
            strcat(szTxt,  "INCORRECT_MEDIA");

            break;

          case OSAL_C_U16_DATA_MEDIA:
            strcat(szTxt,  "DATA_MEDIA");

            break;

          case OSAL_C_U16_AUDIO_MEDIA:
            strcat(szTxt,  "AUDIO_MEDIA");

            break;

          case OSAL_C_U16_UNKNOWN_MEDIA:
            strcat(szTxt,  "UNKNOWN_MEDIA");

            break;

          default:
            strcat(szTxt,  "MEDIA_TYPE_XXXXX");
          } //End of switch
        }
        break;

      case OSAL_C_U16_NOTI_TOTAL_FAILURE:
        {
          sprintf(szTxt, "  [%02d:%08ums] NOTI_TOTAL_FAILURE (0x%04X) ->",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);
          switch(u16State)
          {
          case OSAL_C_U16_DEVICE_OK:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_OK");

            break;
          case OSAL_C_U16_DEVICE_FAIL:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_FAIL");

            break;
          default:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_XXX");
          } //switch(u16State)
        }
        break;

      case OSAL_C_U16_NOTI_MODE_CHANGE:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_MODE_CHANGE",i,
                (unsigned int)u32Time);

        break;

      case OSAL_C_U16_NOTI_MEDIA_STATE:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_MEDIA_STATE (0x%04X) -> ",i,
                (unsigned int)u32Time,
                (unsigned int)u16State);

        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_NOT_READY:
          strcat(szTxt,  "OSAL_C_U16_MEDIA_NOT_READY");
          break;
        case OSAL_C_U16_MEDIA_READY:
          strcat(szTxt,  "OSAL_C_U16_MEDIA_READY");
          break;
        default:
          strcat(szTxt, "OSAL_C_U16_MEDIA_XXX");
        } //switch(u16MediaState)
        break;

      case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_DVD_OVR_TEMP",
                i,
                (unsigned int)u32Time);
        break;
      case OSAL_C_U16_NOTI_DEFECT:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_DEFECT (0x%04X) -> ",
                i,
                (unsigned int)u32Time,
                (unsigned int)u16State);
        switch(u16State)
        {
        case OSAL_C_U16_DEFECT_DISCTOC:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_DISCTOC");
          break;
        case  OSAL_C_U16_DEFECT_LOAD_EJECT:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_LOAD_EJECT");
          break;
        case OSAL_C_U16_DEFECT_LOAD_INSERT:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_LOAD_INSERT");
          break;
        case OSAL_C_U16_DEFECT_DISC:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_DISC");
          break;
        case OSAL_C_U16_DEFECT_READ_ERR:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_READ_ERR");
          break;
        case OSAL_C_U16_DEFECT_READ_OK:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_READ_OK");
          break;
        default:
          strcat(szTxt,  "OSAL_C_U16_DEFECT_[DEFAULT]");
          break;
        } //switch(u16State)

        break;
      case OSAL_C_U16_NOTI_EJECTKEY:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_EJECTKEY",
                i,
                (unsigned int)u32Time);
        break;
      case OSAL_C_U16_NOTI_DEVICE_READY:
        {
          sprintf(szTxt, "  [%02d:%08ums] NOTI_DEVICE_READY (0x%04X) -> ",i,
                  (unsigned int)u32Time,
                  (unsigned int)u16State);
          switch(u16State)
          {
          case OSAL_C_U16_DEVICE_NOT_READY:
            strcat(szTxt,  "DEVICE_NOT_READY");

            break;
          case OSAL_C_U16_DEVICE_READY:
            strcat(szTxt,  "DEVICE_READY");

            break;
          case OSAL_C_U16_DEVICE_UV_NOT_READY:
            strcat(szTxt,  "DEVICE_UV_NOT_READY");

            break;
          case OSAL_C_U16_DEVICE_UV_READY:
            strcat(szTxt,  "DEVICE_UV_READY");

            break;
          default:
            strcat(szTxt,  "OSAL_C_U16_DEVICE_XXX");
            break;

          } //switch(u16DeviceReady)
        }
        break;

      case OSAL_C_U16_NOTI_IDLE:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_IDLE",
                i,
                (unsigned int)u32Time);
        break;

      case OSAL_C_U16_NOTI_LOW_POW:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_LOW_POW",
                i,
                (unsigned int)u32Time);
        break;

      default:
        sprintf(szTxt, "  [%02d:%08ums] NOTI_XXX",
                i,
                (unsigned int)u32Time);
      } //End of switch


      sprintf(szTxt1, ", Loader 0x%02X",
              (unsigned int)T158_aMon[i].u8LoaderInfoByte);
      strcat(szTxt, szTxt1);
      OCDTR(c2, "%s", szTxt);
    } //for(i = 0; i < T158_iMonIdx; i++)
  } //check monitored data

  if(OEDT_CD_T158_RESULT_OK_VALUE != u32ResultBitMask) /*lint !e774*/
  {
    OCDTR(c1, "OEDT_CD: T158 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T158_RESULT_OK_VALUE != u32ResultBitMask)


  return u32ResultBitMask;
}



/*****************************************************************************/
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*                            OEDT_CD_T159                          */
/*****************************************************************************/
 #define OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL                 2
 #define OEDT_CD_T159_INVALID_REG_VAL                               0x0000000F
 #define OEDT_CD_T159_DEVICE_CTRL_NAME             OSAL_C_STRING_DEVICE_CDCTRL
 #define OEDT_CD_T159_VIO(X)                      OSAL_C_S32_IOCTRL_VOLT_ ## X
 #define OEDT_CD_T159_INVALID_VOLTAGE_STATE                         0xFFFFFFFF
 #define OEDT_CD_T159_OPERATING_VOLTAGE_STATE                       0x00000004
 #define OEDT_CD_T159_LOW_VOLTAGE_STATE                             0x00000002
 #define OEDT_CD_T159_CRITICAL_LOW_VOLTAGE_STATE                    0x00000000
 #define OEDT_CD_T159_TIMEOUT                                            30000
 #define OEDT_CD_T159_REG_PATH  "/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEVICES/DEV_CD/"

 #define CD_DRIVE_STATUS_READY                                      0x00000001
 #define CD_DRIVE_STATUS_NOT_READY                                  0x00000002
 #define CD_DRIVE_STATUS_INVALID                                    0xFFFFFFFF
 #define OEDT_CD_T159_OP_TO_LOW                                              1
 #define OEDT_CD_T159_LOW_TO_C_LOW                                           2
 #define OEDT_CD_T159_C_LOW_TO_LOW                                           3 
 #define OEDT_CD_T159_LOW_TO_OP                                              4

 
 
 #define OEDT_CD_T159_RESULT_OK_VALUE                               0x00000000
 #define OEDT_CD_T159_DEVICE_NOT_READY                              0x00000001
 #define OEDT_CD_T159_CLEAR_EVENT_POST                              0x00000002
 #define OEDT_CD_T159_REG_NOTIFICATION_RESULT_ERROR_BIT             0x00000004
 #define OEDT_CD_T159_OPEN_CTRL_RESULT_ERROR_BIT                    0x00000008
 
 #define OEDT_CD_T159_OP_TO_LOW_WRONG_VSTATE_ERROR_BIT              0x00000010
 #define OEDT_CD_T159_OP_TO_LOW_EVENT_POST_ERROR_BIT                0x00000020
 #define OEDT_CD_T159_OP_TO_LOW_NO_NOTI_ERROR_BIT                   0x00000040
 #define OEDT_CD_T159_OP_TO_LOW_UNEXPECTED_NOTI_ERROR_BIT           0x00000080
 #define OEDT_CD_T159_OP_TO_LOW_WRONG_NOTI_ERROR_BIT                0x00000100
 
 #define OEDT_CD_T159_LOW_TO_C_LOW_EVENT_POST_ERROR_BIT             0x00000200
 #define OEDT_CD_T159_LOW_TO_C_LOW_WRONG_VSTATE_ERROR_BIT           0x00000400
 #define OEDT_CD_T159_LOW_TO_C_LOW_NO_NOTI_ERROR_BIT                0x00000800
 #define OEDT_CD_T159_LOW_TO_C_LOW_UNEXPECTED_NOTI_ERROR_BIT        0x00001000
 #define OEDT_CD_T159_LOW_TO_C_LOW_WRONG_NOTI_ERROR_BIT             0x00002000
 
 #define OEDT_CD_T159_C_LOW_TO_LOW_EVENT_POST_ERROR_BIT             0x00004000
 #define OEDT_CD_T159_C_LOW_TO_LOW_WRONG_VSTATE_ERROR_BIT           0x00008000
 #define OEDT_CD_T159_C_LOW_TO_LOW_NO_NOTI_ERROR_BIT                0x00010000
 #define OEDT_CD_T159_C_LOW_TO_LOW_UNEXPECTED_NOTI_ERROR_BIT        0x00020000
 #define OEDT_CD_T159_C_LOW_TO_LOW_WRONG_NOTI_ERROR_BIT             0x00040000
 
 #define OEDT_CD_T159_LOW_TO_OP_EVENT_POST_ERROR_BIT                0x00080000
 #define OEDT_CD_T159_LOW_TO_OP_WRONG_VSTATE_ERROR_BIT              0x00100000
 #define OEDT_CD_T159_LOW_TO_OP_NO_NOTI_ERROR_BIT                   0x00200000
 #define OEDT_CD_T159_LOW_TO_OP_UNEXPECTED_NOTI_ERROR_BIT           0x00400000
 #define OEDT_CD_T159_LOW_TO_OP_WRONG_NOTI_ERROR_BIT                0x00800000
 
 #define OEDT_CD_T159_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT      0x01000000
 #define OEDT_CD_T159_CLOSE_CTRL_RESULT_ERROR_BIT                   0x02000000

 
 #define OEDT_CD_T159_CREATE_EVENT_ERROR_BIT                        0x04000000 
 #define OEDT_CD_T159_DELETE_EVENT_ERROR_BIT                        0x08000000 
 
 

/*****************************************************************************
* FUNCTION:     T159_vMediaNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  Callback registered with /dev/cdctrl for receiving drive
*               status 
*                DATE    |   Modification   | Author 
*               18/11/16 | Added            | boc7kor
******************************************************************************/ 
static tVoid T159_vMediaNotify(tU32 *pu32MediaChangeInfo)
{
  tCString szEventName = "cddrivestatus";
  OSAL_tEventHandle hEvent;
  tS32 s32Ret;
  tU32 u32ErrBitMask = 0;
  OSAL_tEventMask tEventMask = CD_DRIVE_STATUS_INVALID;
  
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32MediaChangeInfo);
  s32Ret = OSAL_s32EventOpen(szEventName, &hEvent);
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1, "OSAL_s32EventOpen:  ERROR (%s)",OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ErrBitMask = u32ErrBitMask +1;
  }
  else 
  {
    OCDTR(c2, "OSAL_s32EventOpen: SUCCESS Open hEvent == 0x%08X", hEvent);
  }
    
  if((0 == u32ErrBitMask) && (pu32MediaChangeInfo != NULL))
  {
      tU16 u16NotiType;
      tU16 u16State;
      OCDTR(c2, "T159_vMediaTypeNotify: 0x%08X",
      (unsigned int)*pu32MediaChangeInfo);
      u16NotiType = OEDT_CD_HIWORD(*pu32MediaChangeInfo);
      u16State    = OEDT_CD_LOWORD(*pu32MediaChangeInfo);
               
     if(OSAL_C_U16_NOTI_DEVICE_READY == u16NotiType)
     {
        switch(u16State)
        {
           case OSAL_C_U16_DEVICE_NOT_READY:
           {
               tEventMask = CD_DRIVE_STATUS_NOT_READY;   
               OCDTR(c2,"DEVICE_NOT_READY");
               break;
           }
                
           case OSAL_C_U16_DEVICE_READY:
           { 
               tEventMask = CD_DRIVE_STATUS_READY;                 
               OCDTR(c2,"DEVICE_READY");
               break;
           }
           default:
           {
               OCDTR(c2,"OSAL_C_U16_DEVICE_UNINTERESTING");
               break;
           }
                
        }
            
    }
    else
    {
       OCDTR(c2,"Uninteresting Notification Type");
    }
        
    if(tEventMask != CD_DRIVE_STATUS_INVALID)
    {
       s32Ret = OSAL_s32EventPost(hEvent,tEventMask, OSAL_EN_EVENTMASK_OR);
       if(OSAL_ERROR == s32Ret)
        {
            OCDTR(c1,"OSAL_s32EventPost: ERROR (%s)",
                    OSAL_coszErrorText(OSAL_u32ErrorCode()));
            u32ErrBitMask = u32ErrBitMask + 2;
        }
        else
        {
            OCDTR(c2,"OSAL_s32EventPost: SUCCESS (0x%x)",tEventMask);
            
        }
    }
        
  }
  
  if(0 == (u32ErrBitMask & (1)))
  {  
    /*if open successful*/    
    s32Ret = OSAL_s32EventClose(hEvent);
    if(OSAL_ERROR == s32Ret)
    {
        OCDTR(c1, "OSAL_s32EventClose:  ERROR (%s)",OSAL_coszErrorText(OSAL_u32ErrorCode()));
    }
    else 
    {
        OCDTR(c2, "OSAL_s32EventClose: SUCCESS hEvent == 0x%08X", hEvent);
    }
  } 
  
  if(0 != u32ErrBitMask)
  {
      /*if some error occured*/
      OCDTR(c1,"T159_vMediaNotify: Error Bit Mask 0x%x",u32ErrBitMask);
  }      

} 
/******************************************************************************
 *FUNCTION     :  T159_u32GetSystemVoltage
 *DESCRIPTION  :  Gets current system voltage status from dev volt 
 *PARAMETER    :  void
 *RETURNVALUE  :  Current Voltage Status
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_u32GetSystemVoltage(void)
{
    DEV_VOLT_trClientRegistration sVolt;
    OSAL_tIODescriptor hVolt;
    DEV_VOLT_trSystemVoltageHistory rSVH;
    tU32 u32SVS = OEDT_CD_T159_INVALID_VOLTAGE_STATE;
    tS32 s32Ret = OSAL_ERROR;
    sVolt.u32ClientId = (tU32)-1;
    
    hVolt = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
    if(OSAL_ERROR == hVolt)
    {
        OCDTR(c1, "DEV_VOLT: ERROR OPEN (%s)",
                    OSAL_coszErrorText(OSAL_u32ErrorCode()));
    }
    else 
    {
        rSVH.u32ClientId = sVolt.u32ClientId;
        s32Ret = OSAL_s32IOControl(hVolt,
                               OEDT_CD_T159_VIO(GET_SYSTEM_VOLTAGE_HISTORY),
                                 (tS32)&rSVH);
        if(OSAL_OK == s32Ret )
        {
            u32SVS  = rSVH.rSystemVoltage.u32CurrentSystemVoltageState;
       
        }
    
        s32Ret = OSAL_s32IOClose(hVolt);
        if(OSAL_OK != s32Ret)
        {
            OCDTR(c1, "DEV_VOLT: ERROR CLOSE handle 0x%08X (%s)",
                (unsigned int)hVolt, OSAL_coszErrorText(OSAL_u32ErrorCode()));
        
        }
        else
        {
            OCDTR(c2, "DEV_VOLT: SUCCESS CLOSE handle %u ",
                (unsigned int)hVolt);
        } 
    } 
    return u32SVS;
    
}

/******************************************************************************
 *FUNCTION     :  T159_vPrintSystemVoltageStatus
 *DESCRIPTION  :  Prints the current voltage status 
 *PARAMETER    :  Current Voltage Status
 *RETURNVALUE  :  void
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static void T159_vPrintSystemVoltageStatus(tU32 u32VoltStatus)
{
    switch(u32VoltStatus)
    {
        case OEDT_CD_T159_OPERATING_VOLTAGE_STATE:
        OCDTR(c0,"Current System Voltage State: OPERATING");
        break;
        
        case OEDT_CD_T159_LOW_VOLTAGE_STATE:
        OCDTR(c0,"Current System Voltage State: LOW");
        break;
        
        case OEDT_CD_T159_CRITICAL_LOW_VOLTAGE_STATE:
        OCDTR(c0,"Current System Voltage State: CRITICAL LOW");
        break;

        default:
        OCDTR(c0,"Current System Voltage State: INVALID");
        break;        
     }
}

/******************************************************************************
 *FUNCTION     :  T159_vPrintStartMessage
 *DESCRIPTION  :  Prints instructions to test 
 *PARAMETER    :  Current Voltage Status
 *RETURNVALUE  :  void
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static void T159_vPrintStartMessage(tU32 u32Transistion)
{
  tU32  u32CurrentSystemVoltageState;
  
  OCDTR(c0,"*****************************************************");
  u32CurrentSystemVoltageState=T159_u32GetSystemVoltage();
  T159_vPrintSystemVoltageStatus(u32CurrentSystemVoltageState);
  switch(u32Transistion)
  {
      case OEDT_CD_T159_OP_TO_LOW:
      {
        OCDTR(c0,"REDUCE VOLTAGE(V) TO LOW VOLTAGE");
        break;
      }
      case OEDT_CD_T159_LOW_TO_C_LOW:
      {
        OCDTR(c0,"REDUCE VOLTAGE(V) TO CRITICAL VOLTAGE");
        break;
      }
      case OEDT_CD_T159_C_LOW_TO_LOW:
      {
        OCDTR(c0,"INCREASE VOLTAGE(V) TO LOW VOLTAGE");
        break;
      }
      case OEDT_CD_T159_LOW_TO_OP:
      {
        OCDTR(c0,"INCREASE VOLTAGE(V) TO OPERATING VOLTAGE");
        break;
      }
      default:
      {
          OCDTR(c0,"Unexpected u32Transistion");
          break;
      }
  }
  OCDTR(c0,"*****************************************************");
  OCDTR(c0,"Waiting 30 seconds for any device notification...");  
}

/******************************************************************************
 *FUNCTION     :  T159_u32ReadRegistryValue
 *DESCRIPTION  :  Called by OEDT_CD_T159 to read registry value 
 *                CD_LOW_VOLTAGE_HANDLING_THRESHOLD
 *PARAMETER    :  void
 *RETURNVALUE  :  Registry Value
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_u32ReadRegistryValue(void)
{
    OSAL_tIODescriptor    rRegistryIODescriptor;
    OSAL_trIOCtrlRegistry rIOCtrlRegistry;
    tU32                  u32RegistryValue      = 0x00000000;
    tU32                  u32Ret                = OEDT_CD_T159_INVALID_REG_VAL;
    rRegistryIODescriptor = OSAL_IOOpen(
                    OSAL_C_STRING_DEVICE_REGISTRY OEDT_CD_T159_REG_PATH,
                    OSAL_EN_READONLY);

    if (rRegistryIODescriptor == OSAL_ERROR) 
    {
        OCDTR(c1,"T159_u32ReadRegistryValue() => OSAL_IOOpen(DEVICE_REGISTRY) failed with error code = %s",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  
    }
    else
    {
      rIOCtrlRegistry.pcos8Name = (tCS8*)"CD_LOW_VOLTAGE_HANDLING_THRESHOLD";
      rIOCtrlRegistry.ps8Value  = (tU8*)&u32RegistryValue;
      rIOCtrlRegistry.u32Size   = sizeof(tS32);

      if (OSAL_s32IOControl(
          rRegistryIODescriptor,
          OSAL_C_S32_IOCTRL_REGGETVALUE,
          (tS32)&rIOCtrlRegistry) == OSAL_OK)
      {
         u32Ret = *(rIOCtrlRegistry.ps8Value);
         OCDTR(c2,"CD_LOW_VOLTAGE_HANDLING_THRESHOLD set to %u\n",u32Ret);
        
      }
      else
      {
         OCDTR(c1,
         "T159_u32ReadRegistryValue() => OSAL_s32IOControl(REGGETVALUE) failed with error code = %s",
           OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
    
      if (OSAL_s32IOClose(rRegistryIODescriptor) == OSAL_ERROR)
      {
         OCDTR(c2,
            "T159_u32ReadRegistryValue() => OSAL_IOClose(DEVICE_REGISTRY) failed with error code = %s",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
    }
    
    return u32Ret;
}

/******************************************************************************
 *FUNCTION     :  T159_OPERATING_TO_LOW
 *DESCRIPTION  :  Called by OEDT_CD_T159 to test the handling 
 *                during transition from operating to low voltage
 *PARAMETER    :  Registry Value
 *                Event Handle
 *RETURNVALUE  :  Error Bit Mask
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_OPERATING_TO_LOW(tU32 u32RegVal, OSAL_tEventHandle hEvent)
{
  tU32 u32ResultBitMask=OEDT_CD_T159_RESULT_OK_VALUE;
  tU32 u32CurrentSystemVoltageState;
  tS32 s32Ret;
  OSAL_tEventMask tWaitEventMask;
  tU32 u32ErrCode = 0;
  
  s32Ret=OSAL_s32EventWait(hEvent, CD_DRIVE_STATUS_NOT_READY,  
                            OSAL_EN_EVENTMASK_OR, OEDT_CD_T159_TIMEOUT,
                                &tWaitEventMask);
  if(OSAL_ERROR == s32Ret)
  {
    u32ErrCode = OSAL_u32ErrorCode();
    OCDTR(c2,"OSAL_s32EventWait: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventWait: SUCCESS (0x%x)",tWaitEventMask);
  }
  
  s32Ret=OSAL_s32EventPost(hEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);
  
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1,"OSAL_s32EventPost: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ResultBitMask |= OEDT_CD_T159_OP_TO_LOW_EVENT_POST_ERROR_BIT;
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventPost: SUCCESS (0x%x)",~tWaitEventMask);
  }
  
  u32CurrentSystemVoltageState = T159_u32GetSystemVoltage();
  
  if(OEDT_CD_T159_LOW_VOLTAGE_STATE != u32CurrentSystemVoltageState )
  {
    u32ResultBitMask 
          |= OEDT_CD_T159_OP_TO_LOW_WRONG_VSTATE_ERROR_BIT;  
  }
  
  if(0 == u32ResultBitMask)
  {
    if(OSAL_E_TIMEOUT == u32ErrCode)
    {
      /*Drive Status Notification NOT Received*/  
       if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL != u32RegVal)
       {
          u32ResultBitMask 
           |= OEDT_CD_T159_OP_TO_LOW_NO_NOTI_ERROR_BIT;
       }
    }
    else
    {
      /*Drive Status Notification Received*/ 
      if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal)
      {
        u32ResultBitMask 
         |= OEDT_CD_T159_OP_TO_LOW_UNEXPECTED_NOTI_ERROR_BIT;
      }
      else
      {
        if(CD_DRIVE_STATUS_NOT_READY != tWaitEventMask)
        {
          u32ResultBitMask 
            |= OEDT_CD_T159_OP_TO_LOW_WRONG_NOTI_ERROR_BIT;
        }
      }
    }
  }    
  
  return u32ResultBitMask;  
}

/******************************************************************************
 *FUNCTION     :  T159_LOW_TO_CRITICAL_LOW
 *DESCRIPTION  :  Called by OEDT_CD_T159 to test the handling 
 *                during transition from low to critical low voltage
 *PARAMETER    :  Registry Value
 *                Event Handle
 *RETURNVALUE  :  Error Bit Mask
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_LOW_TO_CRITICAL_LOW(tU32 u32RegVal, OSAL_tEventHandle hEvent)
{
  tU32 u32ResultBitMask=OEDT_CD_T159_RESULT_OK_VALUE;
  tU32 u32CurrentSystemVoltageState;
  tS32 s32Ret;
  OSAL_tEventMask tWaitEventMask;
  tU32 u32ErrCode = 0;
  
  s32Ret=OSAL_s32EventWait(hEvent, CD_DRIVE_STATUS_NOT_READY,  
                            OSAL_EN_EVENTMASK_OR, OEDT_CD_T159_TIMEOUT,
                                &tWaitEventMask); 
  if(OSAL_ERROR == s32Ret)
  {
    u32ErrCode = OSAL_u32ErrorCode();
    OCDTR(c2,"OSAL_s32EventWait: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventWait: SUCCESS (0x%x)",tWaitEventMask);
     
  }
  
  s32Ret=OSAL_s32EventPost(hEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);
  
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1,"OSAL_s32EventPost: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ResultBitMask |= OEDT_CD_T159_LOW_TO_C_LOW_EVENT_POST_ERROR_BIT;
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventPost: SUCCESS (0x%x)",~tWaitEventMask);
  }
  
  u32CurrentSystemVoltageState=T159_u32GetSystemVoltage();
  
  if(OEDT_CD_T159_CRITICAL_LOW_VOLTAGE_STATE != u32CurrentSystemVoltageState )
  {
    u32ResultBitMask 
          |= OEDT_CD_T159_LOW_TO_C_LOW_WRONG_VSTATE_ERROR_BIT;  
  }
  
  if(0 == u32ResultBitMask)
  {
    if(OSAL_E_TIMEOUT == u32ErrCode)
    {
        /*Drive Status Notification NOT Received*/ 
        if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal)
        {
            u32ResultBitMask 
            |= OEDT_CD_T159_LOW_TO_C_LOW_NO_NOTI_ERROR_BIT;
        }
    }
    else
    {
        /*Drive Status Notification Received*/  
        if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal )
        {
          if(CD_DRIVE_STATUS_NOT_READY != tWaitEventMask)
          {
              u32ResultBitMask 
               |= OEDT_CD_T159_LOW_TO_C_LOW_WRONG_NOTI_ERROR_BIT;
          }
          
        }
        else
        {
           u32ResultBitMask 
           |= OEDT_CD_T159_LOW_TO_C_LOW_UNEXPECTED_NOTI_ERROR_BIT;
        }
    }
  }
  
  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  T159_CRITICAL_LOW_TO_LOW
 *DESCRIPTION  :  Called by OEDT_CD_T159 to test the handling 
 *                during transition from critical low to low voltage
 *PARAMETER    :  Registry Value
 *                Event Handle
 *RETURNVALUE  :  Error Bit Mask
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_CRITICAL_LOW_TO_LOW(tU32 u32RegVal, OSAL_tEventHandle hEvent)
{
  tU32 u32ResultBitMask=OEDT_CD_T159_RESULT_OK_VALUE;
  tU32 u32CurrentSystemVoltageState;
  tS32 s32Ret;
  OSAL_tEventMask tWaitEventMask;
  tU32 u32ErrCode = 0;

  
  s32Ret=OSAL_s32EventWait(hEvent, CD_DRIVE_STATUS_READY,  
                            OSAL_EN_EVENTMASK_OR, OEDT_CD_T159_TIMEOUT,
                                &tWaitEventMask);
                                
  
  if(OSAL_ERROR == s32Ret)
  {
    u32ErrCode = OSAL_u32ErrorCode();
    OCDTR(c2,"OSAL_s32EventWait: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventWait: SUCCESS (0x%x)",tWaitEventMask);
  }
  
  s32Ret=OSAL_s32EventPost(hEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);
  
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1,"OSAL_s32EventPost: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ResultBitMask |= OEDT_CD_T159_C_LOW_TO_LOW_EVENT_POST_ERROR_BIT;
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventPost: SUCCESS (0x%x)",~tWaitEventMask);
  }
  
  u32CurrentSystemVoltageState=T159_u32GetSystemVoltage();
  
  if(OEDT_CD_T159_LOW_VOLTAGE_STATE != u32CurrentSystemVoltageState)
  {
    u32ResultBitMask 
          |= OEDT_CD_T159_C_LOW_TO_LOW_WRONG_VSTATE_ERROR_BIT;  
  }
  
  if(0 == u32ResultBitMask)
  {
    if(OSAL_E_TIMEOUT == u32ErrCode)
    {
      /*Drive Status Notification NOT Received*/
      if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal )
      {
          u32ResultBitMask 
          |= OEDT_CD_T159_C_LOW_TO_LOW_NO_NOTI_ERROR_BIT;
      }
    }
    else
    {
       /*Drive Status Notification Received*/ 
       if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal )
       {
          if(CD_DRIVE_STATUS_READY != tWaitEventMask)
          {
           u32ResultBitMask
              |= OEDT_CD_T159_C_LOW_TO_LOW_WRONG_NOTI_ERROR_BIT;
          }
       }
       else
       {
          u32ResultBitMask
           |= OEDT_CD_T159_C_LOW_TO_LOW_UNEXPECTED_NOTI_ERROR_BIT;
          
      }
    }
  }
  
  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  T159_LOW_TO_OPERATING
 *DESCRIPTION  :  Called by OEDT_CD_T159 to test the handling 
 *                during transition from operating low to operating voltage
 *PARAMETER    :  Registry Value
 *                Event Handle
 *RETURNVALUE  :  Error Bit Mask
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
static tU32 T159_LOW_TO_OPERATING(tU32 u32RegVal, OSAL_tEventHandle hEvent)
{
  tU32 u32ResultBitMask=OEDT_CD_T159_RESULT_OK_VALUE;
  tU32 u32CurrentSystemVoltageState;
  tS32 s32Ret;
  OSAL_tEventMask tWaitEventMask;
  tU32 u32ErrCode = 0;
  
   
  s32Ret=OSAL_s32EventWait(hEvent, CD_DRIVE_STATUS_READY,  
                            OSAL_EN_EVENTMASK_OR, OEDT_CD_T159_TIMEOUT,
                                &tWaitEventMask);
  if(OSAL_ERROR == s32Ret)
  {
    u32ErrCode = OSAL_u32ErrorCode();
    OCDTR(c2,"OSAL_s32EventWait: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventWait: SUCCESS (0x%x)",tWaitEventMask);
  }
  
  s32Ret=OSAL_s32EventPost(hEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);
  
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1,"OSAL_s32EventPost: ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ResultBitMask |= OEDT_CD_T159_LOW_TO_OP_EVENT_POST_ERROR_BIT;
  }
  else
  {
     OCDTR(c2,"OSAL_s32EventPost: SUCCESS (0x%x)",~tWaitEventMask);
  }
 
    
  u32CurrentSystemVoltageState = T159_u32GetSystemVoltage();
  
  if( OEDT_CD_T159_OPERATING_VOLTAGE_STATE != u32CurrentSystemVoltageState)
  {
    u32ResultBitMask 
          |= OEDT_CD_T159_LOW_TO_OP_WRONG_VSTATE_ERROR_BIT;  
  }
  
  if(0 == u32ResultBitMask)
  {
    if(OSAL_E_TIMEOUT == u32ErrCode)
    {
      /*Drive Status Notification NOT Received*/
      if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL != u32RegVal)
      {
          u32ResultBitMask
              |= OEDT_CD_T159_LOW_TO_OP_NO_NOTI_ERROR_BIT;
      }
    }
    else
    {
      /*Drive Status Notification Received*/
      if(OEDT_CD_T159_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITCAL == u32RegVal)
      {
          u32ResultBitMask
              |= OEDT_CD_T159_LOW_TO_OP_UNEXPECTED_NOTI_ERROR_BIT;  
      }
      else
      {
        if(CD_DRIVE_STATUS_READY != tWaitEventMask)
        {
           u32ResultBitMask
             |= OEDT_CD_T159_LOW_TO_OP_WRONG_NOTI_ERROR_BIT;
        }
      }
    }
  }    
  
    
  return u32ResultBitMask;
    
}


/******************************************************************************
 *FUNCTION     :  OEDT_CD_T159 T159 Voltage Handling
 *DESCRIPTION  :  This test checks the handling of voltage states by /dev/ctrl
 *PARAMETER    :  void
 *RETURNVALUE  :  Error Bit Mask
 *HISTORY      : 
 *                DATE    |   Modification   | Author 
 *               18/11/16 | Added            | boc7kor
 *****************************************************************************/
tU32 OEDT_CD_T159(void)
{
  OSAL_tIODescriptor hCDctrl = OSAL_ERROR;
  tS32 s32Fun;
  tS32 s32Arg; 
  tS32 s32Ret;
  tU32 u32ResultBitMask = OEDT_CD_T159_RESULT_OK_VALUE;
  tU32 u32RegVal;
  tCString szEventName = "cddrivestatus";
  OSAL_tEventHandle hEvent;
  OSAL_tEventMask tWaitEventMask;
  
  u32RegVal = T159_u32ReadRegistryValue();
  if(OEDT_CD_T159_INVALID_REG_VAL != u32RegVal)
  {
      OCDTR(c2,"T159_u32ReadRegistryValue: SUCCESS");
  }
  else
  {
      OCDTR(c1,"T159_u32ReadRegistryValue: FAILURE");
      u32RegVal=0x00000000;
  }
  
  s32Ret = OSAL_s32EventCreate(szEventName, &hEvent);
  if(OSAL_ERROR == s32Ret)
  {
    OCDTR(c1, "OSAL_s32EventCreate:  ERROR (%s)",
                OSAL_coszErrorText(OSAL_u32ErrorCode()));
    u32ResultBitMask |= OEDT_CD_T159_CREATE_EVENT_ERROR_BIT;
  }
  else 
  {
    OCDTR(c2, "OSAL_s32EventCreate: SUCCESS Open hEvent == 0x%08X", hEvent);
  }

  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)
  {
    hCDctrl = OSAL_IOOpen(OEDT_CD_T159_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDctrl)
    {
        OCDTR(c1, "OEDT_CD: ERROR Open <hCDctrl=%d>",hCDctrl);
        u32ResultBitMask |= OEDT_CD_T159_OPEN_CTRL_RESULT_ERROR_BIT;
    }
    else 
    {
        OCDTR(c2, "OEDT_CD: SUCCESS Open <%s> == 0x%08X",
            OEDT_CD_T159_DEVICE_CTRL_NAME, hCDctrl);
    } 
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);
    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType = OSAL_C_U16_NOTI_DEVICE_READY;
    rReg.pCallback           = T159_vMediaNotify;
 
    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                            OEDT_CD_T159_REG_NOTIFICATION_RESULT_ERROR_BIT 
                            : 0;
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)
  {  
    s32Ret=OSAL_s32EventWait(hEvent, CD_DRIVE_STATUS_READY,  
                            OSAL_EN_EVENTMASK_OR, OEDT_CD_T159_TIMEOUT,
                                &tWaitEventMask);
                                
  
    if(OSAL_ERROR == s32Ret)
    {
      OCDTR(c2,"OSAL_s32EventWait : ERROR (%s)",
              OSAL_coszErrorText(OSAL_u32ErrorCode()));
      u32ResultBitMask |= OEDT_CD_T159_DEVICE_NOT_READY;
    }
    else
    {
      OCDTR(c2,"OSAL_s32EventWait : SUCCESS (0x%x)",tWaitEventMask);
    }  
  
    s32Ret=OSAL_s32EventPost(hEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);
  
    if(OSAL_ERROR == s32Ret)
    {
      OCDTR(c1,"OSAL_s32EventPost : ERROR (%s)",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
      u32ResultBitMask |= OEDT_CD_T159_CLEAR_EVENT_POST;
    }
    else
    {
      OCDTR(c2,"OSAL_s32EventPost : SUCCESS (0x%x)",~tWaitEventMask);
      
    }
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)   
  {
    T159_vPrintStartMessage(OEDT_CD_T159_OP_TO_LOW);
    u32ResultBitMask |= T159_OPERATING_TO_LOW(u32RegVal, hEvent);
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)   
  {
    T159_vPrintStartMessage(OEDT_CD_T159_LOW_TO_C_LOW);
    u32ResultBitMask |= T159_LOW_TO_CRITICAL_LOW(u32RegVal, hEvent);
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)   
  {
    T159_vPrintStartMessage(OEDT_CD_T159_C_LOW_TO_LOW);
    u32ResultBitMask |= T159_CRITICAL_LOW_TO_LOW(u32RegVal, hEvent);
  }
  
  if(OEDT_CD_T159_RESULT_OK_VALUE == u32ResultBitMask)   
  {
    T159_vPrintStartMessage(OEDT_CD_T159_LOW_TO_OP);
    u32ResultBitMask |= T159_LOW_TO_OPERATING(u32RegVal, hEvent);
  }
  
  if(0 == ((OEDT_CD_T159_REG_NOTIFICATION_RESULT_ERROR_BIT) & u32ResultBitMask))
  {
    OSAL_trNotifyData rReg = {0};
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(rReg);
    rReg.pu32Data            = 0;
    rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status            = 0;
    rReg.u16NotificationType = OSAL_C_U16_NOTI_DEVICE_READY;
    rReg.pCallback           = T159_vMediaNotify;

    s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    u32ResultBitMask |= (s32Ret == OSAL_ERROR) ?
                          OEDT_CD_T159_UNREG_NOTIFICATION_CTRL_RESULT_ERROR_BIT 
                            : 0;
   }
    
  
  if(0 == ((OEDT_CD_T159_OPEN_CTRL_RESULT_ERROR_BIT) & u32ResultBitMask))
  {
    s32Ret = OSAL_s32IOClose(hCDctrl);
    if(OSAL_OK != s32Ret)
    {
        OCDTR(c1, "OEDT_CD: ERROR CLOSE handle 0x%08X (s32Ret: %d)",
                (unsigned int)hCDctrl, s32Ret);
        u32ResultBitMask |= OEDT_CD_T159_CLOSE_CTRL_RESULT_ERROR_BIT;
    }
    else 
    {
      OCDTR(c2, "OEDT_CD: "
            "SUCCESS CLOSE handle %u ",
            (unsigned int)hCDctrl);
    }
  }    
   
  
  if(0 == ((OEDT_CD_T159_CREATE_EVENT_ERROR_BIT) & u32ResultBitMask))
  {
    s32Ret=OSAL_s32EventClose(hEvent);
    if(OSAL_ERROR == s32Ret)
    {
        OCDTR(c1, "OSAL_s32EventClose:  ERROR (%s)",
                OSAL_coszErrorText(OSAL_u32ErrorCode()));
    }
    else 
    {
        OCDTR(c2, "OSAL_s32EventClose: SUCCESS hEvent == 0x%08X", hEvent);
    } 

    s32Ret = OSAL_s32EventDelete(szEventName);
    if(OSAL_ERROR == s32Ret)
    {
        OCDTR(c1, "OSAL_s32EventDelete:  ERROR (%s)",
                OSAL_coszErrorText(OSAL_u32ErrorCode()));
        u32ResultBitMask |= OEDT_CD_T159_DELETE_EVENT_ERROR_BIT;
    }
    else 
    {
        OCDTR(c2, "OSAL_s32EventDelete: SUCCESS Event == 0x%08X", hEvent);
    }
  }
  
  return u32ResultBitMask;
}


/******************************************************************************
 *FUNCTION     :  OEDT_CD_T255
 *DESCRIPTION  :  this is not a Test, only increasing internal Trace-Level
 *                if trace level > 8, set to level 0 (turn off all traces)
 *                Tracelevel 0 avoids calling of OEDT-Tracefunction completely!
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_T255(void)
{

  OCDTR(c2, "PRINTF OEDT DEV_CDAUDIO T255: 1 Trace Level %d",
        (int)g_iTraceLevel);
  g_iTraceLevel++;
  if(g_iTraceLevel>(tInt)TR_LEVEL_USER_4)
  {
    g_iTraceLevel=(tInt)TR_LEVEL_FATAL;
  }
  OCDTR(c2, "PRINTF OEDT DEV_CDAUDIO T255: 2 Trace Level %d",
        (int)g_iTraceLevel);
  return 0;
}

#ifdef __cplusplus
}
#endif

