/*********************FileHeaderBegin******************************************
* FILE:
*   oedt_ipod_acp_TestFuncs.c
*
* REVISION:
*   1.0
*
* AUTHOR:
*   Lars Tracht
*
* CREATED:
*   2008/12/01
*
* DESCRIPTION:
*   Implementation of OEDT tests for the Apple
*   Authentication CoProcessor (ACP).
*
* NOTES:
*   none
*
* MODIFIED:
* History: 
* 22-Apr-09    Sainath Kalpuri(RBEI/ECF1)   Removed Lint and
                                            Compiler warnings
*
* $Log$
*
*******************FileHeaderEnd*********************************************/

#include "basic.h"
#include "tk/tkernel.h"
#include "tk/util.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include  <extension/device.h>
#include <extension/outer.h>


#include "oedt_ipod_acp_TestFuncs.h"
#include "oedt_helper_funcs.h"

#ifndef TSIM_OSAL

static tU8 u8data[100];
static tU8 u8SignatureData[200];
static tU8 u8ChallengeData[] =
                      {
                         0x3A, 0x52, 0x19, 0xEF, 0x19, 0xA6, 0xE7, 0xC7, 0x00, 0xA0,
                         0x51, 0xE3, 0x55, 0xD5, 0x25, 0xDD, 0x0D, 0x07, 0x43, 0xCF
                      };

/* Defines */
LOCAL   tU32  g_ipod_apple_cp_reset              = 256 + 45; // JV_GPIO0_IRQBASE + JV_IRQ_GPIO1_13
LOCAL   tU32  g_ipod_apple_cp_ready              = 256 + 36; // JV_GPIO0_IRQBASE + JV_IRQ_GPIO1_4
LOCAL   tU8   g_iic_device[IPOD_DEVCONF_MAX_LEN] = {0};
LOCAL   ID   g_iicDev                           = 0;

/* Functions */
static tU32 iPodResetACP(void);
LOCAL   tS32  iPodTryWriteToACP(tS32 write_addr, VP buf, tS32 length, tS32* p_asize);
LOCAL   tS32  iPodTryReadFromACP(tS32 read_addr, VP buf, tS32 length, tS32* p_asize);


/****************************************************************************/
/*!
*\fn        tS32 iPodACPSelftest (void)
*
*\brief     This function runs the selftest of the ACP.
*
*\param
*
*\return    tS32: E_OK on success
*                error value on failure
*
*\par History:
*
****************************************************************************/
tS32 iPodACPSelftest (void)
{
    tS32 ret              =  IPOD_OK;
    I2C_info pInfoA      =  {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr              =  0x00;
    tU8 Send_ProcCtrl[5]  =  {0};
    tU32 ProcResult       =  0x00;
    tS32 write_addr       =  IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr        =  IPOD_AUTH_CP_READ_ADDR;
    tS32 asize            =  0;

    ret = (tS32)iPodResetACP();

    if (ret == IPOD_OK)
    {
        /* Write HW setup for I2C channel 1 */
        ret = tkse_swri_dev(g_iicDev,
                               DN_I2CSETUP,
                               &pInfoA,
                               sizeof(I2C_info),
                               &asize);
    }

    /* prepare AACP for selftest */
    if (ret == E_OK)
    {
        Send_ProcCtrl[IPOD_POS0] = IPOD_AUTH_CP_SELFTEST_ADDR;
        Send_ProcCtrl[IPOD_POS1] = 1;

        ret = iPodTryWriteToACP( write_addr,
                                  Send_ProcCtrl,
                                  IPOD_AUTH_CP_SELFTEST_SIZE,
                                  &asize);
    }

    /* read processing result from AACP */
    if (ret == E_OK)
    {
        addr = IPOD_AUTH_CP_SELFTEST_ADDR;

        ret = iPodTryWriteToACP( write_addr,
                                  &addr,
                                  sizeof(addr),
                                  &asize);
        if (ret == E_OK)
        {
            ret = iPodTryReadFromACP( read_addr,
                                       &ProcResult,
                                       1,
                                       &asize);
        }
    }

    return ret;
}


/****************************************************************************/
/*!
*\fn        tS32 iPodInitACP(void)
*
*\brief     Tries to open i2c device.
*
*\param
*
*\return    tS32: IPOD_OK on success
*                IPOD_ERROR on failure
*
*\par History:
*
****************************************************************************/
tS32 iPodInitACP(void)
{
    tS32 rc = IPOD_OK;

    if(g_iic_device[0] == '\0')
    {
        g_iic_device[0] = 'i';
        g_iic_device[1] = 'i';
        g_iic_device[2] = 'c';
        g_iic_device[3] = 'a';
    }

    if((rc == IPOD_OK) && (g_iicDev <= 0))
    {
        g_iicDev = tkse_opn_dev (g_iic_device,
                              (TD_UPDATE));
        if (g_iicDev <= 0)
        {
            rc = IPOD_ERROR;
            OEDT_HelperPrintf( TR_LEVEL_USER_1, "iPodInitACP(): Could not open device 'I2Ca'.");
        } else {
            OEDT_HelperPrintf( TR_LEVEL_USER_1, "iPodInitACP(): Device 'I2Ca' opened successfully.");
        }
    }
    return rc;
}


/****************************************************************************/
/*!
*\fn        UW iPodIsACPReady(void)
*
*\brief     Checks state of 'ready' pin.
*
*\param
*
*\return    UW: state of 'ready' pin.
*
*\par History:
*
****************************************************************************/
UW iPodIsACPReady(void)
{
    UW  CP_ready    =   0;
    tU32 count       =   0;

    /* make sure Apple CP is ready */
    do
    {
        OSAL_s32ThreadWait(IPOD_WAIT_FOR_CP_TIME);

        GPIO_read(g_ipod_apple_cp_ready ,
                  1,
                  &CP_ready);
        count++;
    } while ((CP_ready == 0) && (count <= IPOD_CP_READY_MAX_TEST_COUNT));

    return CP_ready;
}


/****************************************************************************/
/*!
*\fn        static tU32 iPodResetACP(void)
*
*\brief     This static fn is used to reset ACP.
*
*\param
*
*\return    tU32: IPOD_OK on success
*                 IPOD_ERROR on failure
*
*\par History:
*
****************************************************************************/
static tU32 iPodResetACP(void)
{
    tU32 u32Result = IPOD_OK;
    UW  is_ready =   0;
    tU8  count    =   0;

    /* reset AACP until it is accessable */
    for (count = 0; (is_ready == 0) && (count < IPOD_MAX_TRY_COUNT); count++)
    {
        GPIO_setup(g_ipod_apple_cp_reset ,
                   1,
                   (GPIO_mode_t)((int)GPIO_OUTPUT | (int)GPIO_LOW)); /* activate GPIO OUT and reset Apple CP */

        GPIO_write(g_ipod_apple_cp_reset ,
                   1,
                   0);

        OSAL_s32ThreadWait(IPOD_DELAY_50MS);

        GPIO_setup(g_ipod_apple_cp_ready ,
                   1,
                   GPIO_INPUT);

        OSAL_s32ThreadWait(IPOD_WAIT_FOR_CP_TIME);

        GPIO_write(g_ipod_apple_cp_reset ,
                   1,
                   1); /* set !reset to 1: end reset */

        is_ready = iPodIsACPReady();
    }

    /* set error */
    if (is_ready == 0)
    {
        u32Result = IPOD_ERROR;
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "iPodResetACP(): Reset failed.");
    } else {
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "iPodResetACP(): Reset done.");
    }

    return u32Result;
}


/****************************************************************************/
/*!
*\fn        tS32 iPodTryReadFromACP(tS32 read_addr, VP buf, tS32 length, tS32* p_asize)
*
*\brief     This function tries to read from the ACP.
*
*\param
*
*\return    tS32: E_OK on success
*                error value on failure
*
*\par History:
*
****************************************************************************/
tS32 iPodTryReadFromACP(tS32 read_addr, VP buf, tS32 length, tS32* p_asize)
{
    tS32 ret         = E_ABORT;
    ER  ioer        = E_ABORT;
    UW  CP_ready    = 0;
    tU8  count       = 0;

    for (count = 0; (ret != E_OK) && (count <= IPOD_USB_MAX_TRY_COUNT); count++)
    {
        CP_ready = iPodIsACPReady();
        if (CP_ready != 0)
        {
            ret = tkse_rea_dev(g_iicDev, read_addr, buf, length, IPOD_TIME_OUT);
            if (ret > 0)
            {
                ret = tkse_wai_dev(g_iicDev, ret, p_asize, &ioer, IPOD_TIME_OUT);
                if ( ret > 0 )
                {
                    ret = ioer;
                    if ( ret > 0 ) {
                        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Error in iPodTryReadFromACP(): %d", ret);
                    }
                }
            }
        }
    }

    tkse_dly_tsk(IPOD_DELAY_20MS);
    return ret;
}


/****************************************************************************/
/*!
*\fn        tS32 iPodTryWriteToACP(tS32 write_addr, VP buf, tS32 length, tS32* p_asize)
*
*\brief     This function tries to write to the ACP.
*
*\param
*
*\return    tS32: E_OK on success
*                error value on failure
*
*\par History:
*
****************************************************************************/
tS32 iPodTryWriteToACP(tS32 write_addr, VP buf, tS32 length, tS32* p_asize)
{
    tS32 ret         = E_ABORT;
    ER  ioer        = E_ABORT;
    UW  CP_ready    = 0;
    tU8  count       = 0;

    for (count = 0; (ret != E_OK) && (count <= IPOD_USB_MAX_TRY_COUNT); count++)
    {
        CP_ready = iPodIsACPReady();
        if (CP_ready != 0)
        {
            ret = tkse_wri_dev(g_iicDev, write_addr, buf, length, IPOD_TIME_OUT);
            if (ret > 0)
            {
                ret = tkse_wai_dev(g_iicDev, ret, p_asize, &ioer, IPOD_TIME_OUT);
                if ( ret > 0 )
                {
                    ret = ioer;
                    if ( ret  > 0 ) {
                        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Error in iPodTryWriteToACP(): %d", ret);
                    }
                }
            }
        }
    }

    return ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPAccessWhileSignatureGeneration(tVoid)
*
*\brief     This function is used to test ACP response during signature generation.
*           The ACP should not respond for any request when ACP busy during
*           signature generation process.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_007
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPAccessWhileSignatureGeneration(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr             =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU16 u16ChallengeLen  = 20; /* challenge length */
    tU8 i               = 0;

    ret =(tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 7000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 7001; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
	/* send address of challenge register */
	u8data[0]= 0x20;
    u8data[1] = (u16ChallengeLen >> 8) & 0xFF;
    u8data[2] = u16ChallengeLen & 0xFF;
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                3,
                                &asize );
    if ( ret < E_OK ) { return 7002; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* write challenge data to the challenge data register */
	addr = 0x21;
	u8data[0]= addr;
    /* copy challenge into array */
	for(i =1; i<21; i++)
	{
	  u8data[i] = u8ChallengeData[i-1];
	}
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                21,
                                &asize );
    if ( ret < E_OK ) { return 7003; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* write PROC_CONTROL = 1 */
	addr = 0x10;
	u8data[0] = addr;
	u8data[1] = 0x01;
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                2,
                                &asize );
    if ( ret < E_OK ) { return 7004; }

	/* allow some time (10s) to start signature generation */
	OSAL_s32ThreadWait(10000);

    /* Now try to read some info from Co-processor. Here, expected result is NACK. */
	/* Select the address to read firmware version. */
	addr = IPOD_AUTH_CP_FW_MAJ_VER_ADR;
    ret = iPodTryWriteToACP( write_addr,
                                &addr,
                                1,
                                &asize );
    if ( ret < E_OK ) {
        switch ( ret ) {
            case E_MACV:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "inaccessible address");
                break;
            case E_ID:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no device descriptor");
                break;
            case E_OACV:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "write not allowed");
                break;
            case E_NOMDA:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no device");
                break;
            case E_RONLY:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "read-only device");
                break;
            case E_LIMIT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "exceeded max number of requests");
                break;
            case E_TMOUT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "time out");
                break;
            case E_ABORT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "abort");
                break;
            case E_OBJ:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "reqid request is waiting for completion of other tasks");
                break;
            case E_NOEXS:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no processing request exists");
                break;
			default:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "invalid command");
                break;
			    
        }

        { return 7005; }
    }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* Read the Authenticaton status/control register */
	u8data[0] = 0xff;
    ret = iPodTryReadFromACP( read_addr,
                                u8data,
                                1,
                                &asize );
    if ( ret < E_OK ) {
        switch ( ret ) {
            case E_MACV:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "inaccessible address");
                break;
            case E_ID:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no device descriptor");
                break;
            case E_OACV:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "write not allowed");
                break;
            case E_NOMDA:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no device");
                break;
            case E_LIMIT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "exceeded max number of requests");
                break;
            case E_TMOUT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "time out");
                break;
            case E_ABORT:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "abort");
                break;
            case E_OBJ:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "reqid request is waiting for completion of other tasks");
                break;
            case E_NOEXS:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "no processing request exists");
                break;
			 default:
                OEDT_HelperPrintf( TR_LEVEL_USER_1, "invalid command");
                break;
				
        }

        { return 7006; }
    }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPGetCertificate(tVoid)
*
*\brief     This function returns the certificate from the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_005
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPGetCertificate(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr             =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU16 u16CertLen     = 0;
    tU8 u8ReadLen[2];
    tU8 *pu8ReadBuffer  = NULL;
    tU16 u16Count       = 0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 5000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 5001; }

    /* certificate data Register length is 2 bytes  */
    addr = IPOD_AUTH_CP_CERTIFICATE_ADR;
    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 5002; }

    ret = iPodTryReadFromACP( read_addr,
                               u8ReadLen,
                               IPOD_AUTH_CP_CERTIFICATE_DATA_REGISTER_LEN,
                               &asize );
    if ( ret < E_OK ) { return 5003; }

	/* read certificate */
	u16CertLen = (u8ReadLen[0]<<8)| u8ReadLen[1];
	pu8ReadBuffer = (tU8 *)calloc((((u16CertLen/128) == 0)?u16CertLen:
	                   (u16CertLen+(128 - (u16CertLen%128)))), sizeof(char));
	
	if(pu8ReadBuffer != NULL)
	{
		for(u16Count=0; u16Count < u16CertLen; u16Count += 128)
		{
		    addr = (tU8)(0x31 + (u16Count/128));
		    ret = iPodTryWriteToACP( write_addr,
		                              &addr,
		                              sizeof(addr),
		                              &asize );
		    if ( ret < E_OK ) 
		    { 
				free(pu8ReadBuffer);
				pu8ReadBuffer = NULL;
		    	return 5004;
		    }

		    ret = iPodTryReadFromACP( read_addr,
		                               pu8ReadBuffer + u16Count,
		                               IPOD_AUTH_CP_CERTIFICATE_LEN,
		                               &asize );
		    if ( ret < E_OK ) 
		    { 
				free(pu8ReadBuffer);
				pu8ReadBuffer = NULL;
		    	return 5005;
		    }
		}
	}
	else
	{
		return 5006; 
	}

    free(pu8ReadBuffer);
    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPGetDeviceID(tVoid)
*
*\brief     This function returns the device ID of the of the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_002
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPGetDeviceID(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8  addr            =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU32 tempDeviceID    =   0;
    tU32 DeviceID        =   0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 2000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 2001; }

    /* read ACP device id */
    addr = IPOD_AUTH_CP_DEVICE_ID_ADR;

    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 2002; }

    ret = iPodTryReadFromACP( read_addr,
                               &tempDeviceID,
                               IPOD_AUTH_CP_DEVICE_ID_LEN,
                               &asize );
    if ( ret < E_OK ) { return 2003; }

    if (ret == E_OK)
    {
        /* change byteorder of tempDeviceID and store in DeviceID due to endian issue */
        DeviceID = tempDeviceID & IPOD_U32_LOW_MASK;

        DeviceID = DeviceID << IPOD_SHIFT_8;
        tempDeviceID = tempDeviceID >> IPOD_SHIFT_8;
        DeviceID &= IPOD_U32_LOW_0_MASK;
        DeviceID |= tempDeviceID & IPOD_U32_LOW_MASK;

        DeviceID = DeviceID << IPOD_SHIFT_8;
        tempDeviceID = tempDeviceID >> IPOD_SHIFT_8;
        DeviceID &= IPOD_U32_LOW_0_MASK;
        DeviceID |= tempDeviceID & IPOD_U32_LOW_MASK;

        DeviceID = DeviceID << IPOD_SHIFT_8;
        tempDeviceID = tempDeviceID >> IPOD_SHIFT_8;
        DeviceID &= IPOD_U32_LOW_0_MASK;
        DeviceID |= tempDeviceID & IPOD_U32_LOW_MASK;

        OEDT_HelperPrintf( TR_LEVEL_USER_1, "IPOD_ACP_DEVICE_ID: 0x%x", DeviceID);
    }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPGetFirmwareVersion(tVoid)
*
*\brief     This function returns the firmware version of the of the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_003
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPGetFirmwareVersion(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr             =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU8 majorVer         =   0;
    tU8 minorVer         =   0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 3000; }

    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 3001; }

    /* read Authentication CP major firmware version No. */
    addr = IPOD_AUTH_CP_FW_MAJ_VER_ADR;

    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 3002; }

    ret = iPodTryReadFromACP( read_addr,
                               &majorVer,
                               IPOD_AUTH_CP_VERSION_LEN,
                               &asize );
    if ( ret < E_OK ) { return 3003; }

    /* read Authentication CP minor firmware version No. */
    addr = IPOD_AUTH_CP_FW_MIN_VER_ADR;
    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 3004; }

    ret = iPodTryReadFromACP( read_addr,
                               &minorVer,
                               IPOD_AUTH_CP_VERSION_LEN,
                               &asize );
    if ( ret < E_OK ) { return 3005; }

    if (ret == E_OK)
    {
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "IPOD_ACP_FIRMWARE_VERSION: %d.%d", majorVer, minorVer);
    }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPGetProtocolVersion(tVoid)
*
*\brief     This function returns the protocol version of the of the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_004
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPGetProtocolVersion(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr             =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU8 majorVer         =   0;
    tU8 minorVer         =   0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 4000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 4001; }

    /* read Authentication CP major protocol version No. */
    addr = IPOD_AUTH_CP_PROT_MAJ_VER_ADR;
    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 4002; }

    ret = iPodTryReadFromACP( read_addr,
                               &majorVer,
                               IPOD_AUTH_CP_VERSION_LEN,
                               &asize );
    if ( ret < E_OK ) { return 4003; }

    /* read Authentication CP minor protocol version No. */
    addr = IPOD_AUTH_CP_PROT_MIN_VER_ADR;
    ret = iPodTryWriteToACP( write_addr,
                              &addr,
                              sizeof(addr),
                              &asize );
    if ( ret < E_OK ) { return 4004; }

    ret = iPodTryReadFromACP( read_addr,
                               &minorVer,
                               IPOD_AUTH_CP_VERSION_LEN,
                               &asize );
    if ( ret < E_OK ) { return 4005; }

    if (ret == E_OK)
    {
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "IPOD_ACP_PROTOCOL_VERSION: %d.%d", majorVer, minorVer);
    }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPGetSignature(tVoid)
*
*\brief     This function returns the signature of the of the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_006
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPGetSignature(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    tU8 addr             =   0x00;
    tS32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    tS32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;
    tS32 asize           =   0;
    tU16 u16ChallengeLen  = 20; /* challenge length */
    tU16 u16SigDataLen  = 0;
    tU8 i               = 0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 6000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev( g_iicDev,
                                DN_I2CSETUP,
                                &pInfoA,
                                sizeof(I2C_info),
                                &asize );
    if ( ret < E_OK ) { return 6001; }

    /* init array */
    memset(u8SignatureData, 0, (sizeof(char)* 200) );
	memset(u8data, 0, sizeof(u8data));
	/* send address of challenge register */
	u8data[0]= 0x20;
    u8data[1] = (u16ChallengeLen >> 8) & 0xFF;
    u8data[2] = u16ChallengeLen & 0xFF;
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                3,
                                &asize );
    if ( ret < E_OK ) { return 6002; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* write challenge data to the challenge data register */
	addr = 0x21;
	u8data[0]= addr;
    /* copy challenge into array */
	for(i =1; i<21; i++)
	{
	  u8data[i] = u8ChallengeData[i-1];
	}
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                21,
                                &asize );
    if ( ret < E_OK ) { return 6003; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* write PROC_CONTROL = 1 */
	addr = 0x10;
	u8data[0] = addr;
	u8data[1] = 0x01;
    ret = iPodTryWriteToACP( write_addr,
                                u8data,
                                2,
                                &asize );
    if ( ret < E_OK ) { return 6004; }

    /* wait 70 sec here because signature generation takes approximately 60 sec */
    OSAL_s32ThreadWait(70000);

    /* check for PROC_RESULT == 1 */
    addr = 0x10;
    ret = iPodTryWriteToACP( write_addr,
                                &addr,
                                1,
                                &asize );
    if ( ret < E_OK ) { return 6005; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
    /* Read the Authenticaton status/control register */
	u8data[0] = 0xff;
    ret = iPodTryReadFromACP( read_addr,
                                u8data,
                                1,
                                &asize );
    if ( ret < E_OK ) { return 6006; }

	/* is signature-generation result valid? */
	if((u8data[0]>>4) != 0x01) {
        /* check for ERROR CODE REGISTER  */
        addr = 0x05;
        ret = iPodTryWriteToACP( write_addr,
                                    &addr,
                                    1,
                                    &asize );
        if ( ret < E_OK ) { return 6007; }

        /* Read the Authenticaton status/control register */
    	u8data[0] = 0xff;
        ret = iPodTryReadFromACP( read_addr,
                                    u8data,
                                    1,
                                    &asize );
        if ( ret < E_OK ) { return 6008; }

        OEDT_HelperPrintf( TR_LEVEL_USER_1, "ERROR_CODE: %x", u8data[0]);
        return 6009;
    }

    /* send Signatue data length register address to read the length of the signature data */
    addr = 0x11;
    ret = iPodTryWriteToACP( write_addr,
                                &addr,
                                1,
                                &asize );
    if ( ret < E_OK ) { return 6010; }

    /* init array */
	memset(u8data, 0, sizeof(u8data));
	/* get the contents of Signature data Length register for Length */
    ret = iPodTryReadFromACP( read_addr,
                                u8data,
                                2,
                                &asize );
    if ( ret < E_OK ) { return 6011; }

    /* calculate length */
    u16SigDataLen = (u8data[0]<<8)|u8data[1];

	/* read Signatue data */
    addr = 0x12;
    ret = iPodTryWriteToACP( write_addr,
                                &addr,
                                1,
                                &asize );
    if ( ret < E_OK ) { return 6012; }

    /* init array */
	memset(u8SignatureData, 0, sizeof(u8SignatureData));
    ret = iPodTryReadFromACP( read_addr,
                                u8SignatureData,
                                u16SigDataLen,
                                &asize );
    if ( ret < E_OK ) { ret = 6013; }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tU32 u32IPODACPResetWhileSignatureGenerationn(tVoid)
*
*\brief     This function is used to reset during ACP signature generation.
*           After reset, write/read some data to/from ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_008
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPResetWhileSignatureGenerationn(tVoid)
{
    tS32 ret             =   IPOD_OK;
    I2C_info pInfoA     =   {0x00, I2C_MASTER, I2C_CLOCK_STANDARD};
    /*U8 addr             =   0x00;
    S32 write_addr      =   IPOD_AUTH_CP_WRITE_ADDR;
    S32 read_addr       =   IPOD_AUTH_CP_READ_ADDR;	*/
    tS32 asize           =   0;

    ret = (tS32)iPodResetACP();
    if ( ret < IPOD_OK ) { return 8000; }

    /* Write HW setup for I2C channel 1 */
    ret = tkse_swri_dev(g_iicDev,
                           DN_I2CSETUP,
                           &pInfoA,
                           sizeof(I2C_info),
                           &asize);
    if ( ret < E_OK ) { return 8001; }

    return (tU32)ret;
}


/****************************************************************************/
/*!
*\fn        tS32 s32IPODACPSelftest(tVoid)
*
*\brief     This function initializes the I2C device and performs
*           the selftest of the ACP.
*
*\param     None
*
*\return    tS32: IPOD_OK on success
*                 error code on failure
*
*\par Test case:    OEDT_IPOD_ACP_001
*
*\par History:
*
****************************************************************************/
tU32 u32IPODACPSelftest(tVoid)
{
    tS32 s32Result = IPOD_OK;

    s32Result = iPodInitACP();
    if ( s32Result < IPOD_OK ) { return 1000; }

    s32Result = iPodACPSelftest();
    if ( s32Result < E_OK ) { s32Result = 1001; }

    return (tU32)s32Result;
}






/****************************************************************************/
/*! 
*\fn      tU32 u32IPODOEDT_AccessCPDuringSigGen(tVoid)
*
*\brief   This fn is used to test ACP response during singature generation.
*         the ACP should not respond for any request when ACP busy during
*         signature generation process. 
*
*\param   None                             
*
*\return  tS32: IPOD_OEDT_APPLECP_INIT_SUCCESS on success
                error code on failure
*
*\par History:
* 08-10-2008 | hka2kor | Modified test case to work for all apple chips.  
*
****************************************************************************/
//tU32 u32IPODOEDT_AccessCPDuringSigGen(tVoid)
//{
//    tU32 u32Result = IPOD_ACP_OEDT_FAILED;
//	tU32 u32Result = IPOD_ACP_OEDT_SUCCESS;
//	OSAL_tIODescriptor	i2cDeviceIPOD;
//	tS32 s32Bytes = 0;
//	OSAL_trI2CWriteData writeData;
//    OSAL_trI2CReadData readData;
//	tU8 u8RegAddr;
//	tU16 u16ChallenLen = 20; /* challenge length */
//    Osal_tenI2CDriverMode enMode = OSAL_I2C_OPMODE_INTERRUPT;
//
//	i2cDeviceIPOD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_IPOD_ACP, OSAL_EN_READWRITE);
//	if(i2cDeviceIPOD  == OSAL_ERROR)
//	{
//		return 10000;
//	}
//	u32Result = u32authProReset();
//	if( u32Result != IPOD_ACP_OEDT_SUCCESS)
//	{
//		OSAL_s32IOClose(i2cDeviceIPOD);
//	    return u32Result;
//	}
//	if (OSAL_s32IOControl(i2cDeviceIPOD,OSAL_C_S32_IOCTRL_I2C_SET_OPMODE, (tS32)enMode) == OSAL_OK)
//    {
//        /* write challenge length */
//		tU8 i;
//		memset(u8data, 0, sizeof(u8data));
//		/* first send address of challenge register */
//		u8RegAddr = 0x20;
//		u8data[0]= u8RegAddr;
//        u8data[1] = (u16ChallenLen >> 8) & 0xFF;
//        u8data[2] = u16ChallenLen & 0xFF;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 3);
//		if(	s32Bytes != 3)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 10002;
//		}
//
//		/* write challenge data to the challenge data register */
//		memset(u8data, 0, sizeof(u8data));
//		u8RegAddr = 0x21;
//		u8data[0]= u8RegAddr;
//		for(i =1; i<21; i++)
//		{
//		  u8data[i] = u8ChallenData[i-1];
//		}
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 21);
//		if(	s32Bytes != 21)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 10003;
//		}
//
//		/* write PROC_CONTROL = 1 */
//		memset(u8data, 0, sizeof(u8data));
//		u8RegAddr = 0x10;
//		u8data[0] = u8RegAddr;
//		u8data[1] = 0x01;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 2);
//		if(	s32Bytes != 2)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 10004;
//		}
//
//		/* allow some time to start signature generation */
//		OSAL_s32ThreadWait(10000);
//        /* Now try to read some info from Co-processor. here expected
//           result is NACK */
//		/* select the address to read firmware version */
//		u8RegAddr = 0x00;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = &u8RegAddr;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 1);
//		if(	s32Bytes != 1)
//		{
//           u32Result = OSAL_u32ErrorCode();
//           //if(u32Result != OSAL_E_I2C_NO_ACK_FROM_SLAVE)
//            if(u32Result < 0)
//           {
//		      OSAL_s32IOClose(i2cDeviceIPOD);
//	          return u32Result;
//           }
//           //else
//           else if(u32Result >= 0)
//           {
//    	      OSAL_s32IOClose(i2cDeviceIPOD);
//    	      return IPOD_ACP_OEDT_SUCCESS;
//           }
//		}
//		memset(u8data, 0, sizeof(u8data));
//		u8data[0] = 0xff;
//		readData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK;
//		readData.i2cRxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIORead(i2cDeviceIPOD,(tPS8)&readData,1);
//		if(s32Bytes != 1)
//		{
//           u32Result = OSAL_u32ErrorCode();
//           //if(u32Result != OSAL_E_I2C_NO_ACK_FROM_SLAVE)
//            if(u32Result < 0)
//           {
//    	      OSAL_s32IOClose(i2cDeviceIPOD);
//    	      return u32Result;
//           }
//           //else
//            else if(u32Result >= 0)
//           {
//              u32Result = IPOD_ACP_OEDT_SUCCESS;
//           }
//        }
//
//		if(OSAL_s32IOClose(i2cDeviceIPOD) != OSAL_OK)
//    	{
//		    u32Result = 10005;
//	    }
//	}
//	else
//	{
//       OSAL_s32IOClose(i2cDeviceIPOD);
//	   u32Result = 10006;
//	}
//
//	return u32Result;
//}
//

/****************************************************************************/
/*! 
*\fn      tU32 u32IPODOEDT_ResetCPDuringSigGen(tVoid)
*
*\brief   This fn is used to reset during ACP signature generation. 
*         After reset, write/read some data to/from ACP.
*
*\param   None                             
*
*\return  tS32: IPOD_OEDT_APPLECP_INIT_SUCCESS on success
                error code on failure
*
*\par History:  
* 08-10-2008 | hka2kor | Modified test case to work for all apple chips.  
*
****************************************************************************/
//tU32 u32IPODOEDT_ResetCPDuringSigGen(tVoid)
//{
//    tU32 u32Result = IPOD_ACP_OEDT_FAILED;
//	tU32 u32Result = IPOD_ACP_OEDT_SUCCESS;
//	OSAL_tIODescriptor	i2cDeviceIPOD;
//	tS32 s32Bytes = 0;
//	OSAL_trI2CReadData readData;
//	OSAL_trI2CWriteData writeData;
//	tU8 u8RegAddr;
//	tU16 u16ChallenLen = 20; /* challenge length */
//    Osal_tenI2CDriverMode enMode = OSAL_I2C_OPMODE_INTERRUPT;
//
//	i2cDeviceIPOD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_IPOD_ACP, OSAL_EN_READWRITE);
//	if(i2cDeviceIPOD  == OSAL_ERROR)
//	{
//		return 11000;
//	}
//	u32Result = u32authProReset();
//	if( u32Result != IPOD_ACP_OEDT_SUCCESS)
//	{
//		OSAL_s32IOClose(i2cDeviceIPOD);
//	    return u32Result;
//	}
//	if (OSAL_s32IOControl(i2cDeviceIPOD,OSAL_C_S32_IOCTRL_I2C_SET_OPMODE, (tS32)enMode) == OSAL_OK)
//    {
//        /* write challenge length */
//		tU8 i;
//
//		memset(u8data, 0, sizeof(u8data));
//		/* first send address of challenge register */
//		u8RegAddr = 0x20;
//		u8data[0]= u8RegAddr;
//        u8data[1] = (u16ChallenLen >> 8) & 0xFF;
//        u8data[2] = u16ChallenLen & 0xFF;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 3);
//		if(	s32Bytes != 3)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 11002;
//		}
//
//		/* write challenge data to the challenge data register */
//		memset(u8data, 0, sizeof(u8data));
//		u8RegAddr = 0x21;
//		u8data[0]= u8RegAddr;
//		for(i =1; i<21; i++)
//		{
//		  u8data[i] = u8ChallenData[i-1];
//		}
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 21);
//		if(	s32Bytes != 21)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 11003;
//		}
//
//		/* write PROC_CONTROL = 1 */
//		memset(u8data, 0, sizeof(u8data));
//		u8RegAddr = 0x10;
//		u8data[0] = u8RegAddr;
//		u8data[1] = 0x01;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 2);
//		if(	s32Bytes != 2)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 11004;
//		}
//
//		/* allow some time to start signature generation */
//		OSAL_s32ThreadWait(10000);
//        /* Now try to read some info from Co-processor. here expected
//           result is NACK */
//		/* select the address to read firmware version */
//		u8RegAddr = 0x00;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = &u8RegAddr;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 1);
//		if(	s32Bytes != 1)
//		{
//           u32Result = OSAL_u32ErrorCode();
//           //if(u32Result != OSAL_E_I2C_NO_ACK_FROM_SLAVE)
//            if(u32Result < 0)
//           {
//		      OSAL_s32IOClose(i2cDeviceIPOD);
//	          return u32Result;
//           }
// 	       u32Result = u32authProReset();
//	       if( u32Result != IPOD_ACP_OEDT_SUCCESS)
//	       {
//		      OSAL_s32IOClose(i2cDeviceIPOD);
//	          return u32Result;
//	       }
//		}
//		memset(u8data, 0, sizeof(u8data));
//		u8data[0] = 0xff;
//		readData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK;
//		readData.i2cRxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIORead(i2cDeviceIPOD,(tPS8)&readData,1);
//		if(s32Bytes != 1)
//		{
//           u32Result = OSAL_u32ErrorCode();
//           //if(u32Result != OSAL_E_I2C_NO_ACK_FROM_SLAVE)
//            if(u32Result < 0)
//           {
//    	      OSAL_s32IOClose(i2cDeviceIPOD);
//    	      return u32Result;
//           }
// 	       u32Result = u32authProReset();
//	       if( u32Result != IPOD_ACP_OEDT_SUCCESS)
//	       {
//		      OSAL_s32IOClose(i2cDeviceIPOD);
//	          return u32Result;
//	       }
//        }
//  		/* select the address to read firmware version */
//		u8RegAddr = 0x00;
//		writeData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;
//		writeData.i2cTxBuffer = &u8RegAddr;
//		s32Bytes = OEDT_IPODI2CIOWrite(i2cDeviceIPOD,(tPS8)&writeData, 1);
//		if(	s32Bytes != 1)
//		{
//
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 11005;
//		}
//
//		/* Read the contents of the firmware version register */
//		memset(u8data, 0, sizeof(u8data));
//		u8data[0] = 0xff;
//		readData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK;
//		readData.i2cRxBuffer = u8data;
//		s32Bytes = OEDT_IPODI2CIORead(i2cDeviceIPOD,(tPS8)&readData,1);
//		if(	s32Bytes != 1)
//		{
//			OSAL_s32IOClose(i2cDeviceIPOD);
//			return 11006;
//		}
//		if(OSAL_s32IOClose(i2cDeviceIPOD) != OSAL_OK)
//    	{
//		    u32Result = 11007;
//	    }
//	}
//	else
//	{
//       OSAL_s32IOClose(i2cDeviceIPOD);
//	   u32Result = 11008;
//	}
//
//	return u32Result;
//}
#endif /*#ifndef TSIM_OSAL*/
