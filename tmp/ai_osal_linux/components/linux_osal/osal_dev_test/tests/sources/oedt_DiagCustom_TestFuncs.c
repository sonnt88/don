/******************************************************************************
 *FILE         : oedt_DiagCustom_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the Dev_diagcustom.
 *               
 *AUTHOR       : Anoop Chandran (RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *  Version 1.0 , 18- 12- 2007
    Version 1.1 , 25-03-2009 Sainath Kalpuri (RBEI/ECF1)
	              Removed compiler and lint warnings
 *					
 *
 *****************************************************************************/

/*****************************************************************
| includes: 
|   1)system- and project- includes
|   2)needed interfaces from external components
|...3)internal and external interfaces from this component
|----------------------------------------------------------------*/
#include"oedt_DiagCustom_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
static tCString   strDiagCustom_EventName_test = "DiagCustomTest1";
static OSAL_tEventHandle DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;

static tU8 DiagCustom_arRequestMsg[OEDT_DIAGC_BUFF_SIZE];

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/




/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************************
* FUNCTION    :	   vDiagCustomOnCSMError()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  
				   1). post an event on CSMError			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
void vDiagCustomOnCSMError(tS32 s32code, tS32 s32status)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32code);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32status);
  OSAL_s32EventPost(DiagCustom_handleEvent_Test, OEDT_DIAGC_CSMERROR, OSAL_EN_EVENTMASK_OR);
}
/*****************************************************************************
* FUNCTION    :	   vDiagCustomCallBackWrite()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  
				   1). post an event on Write		       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
void vDiagCustomCallBackWrite(tU32 u32status)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32status);
  OSAL_s32EventPost(DiagCustom_handleEvent_Test, OEDT_DIAGC_WRITE, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
* FUNCTION    :	   vDiagCustomReceiveStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  
				   1). post an event on Receive status		       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
void vDiagCustomReceiveStatus(tU32 u32status)
{
  switch(u32status)
  {
    case 0:
      //Diagnosis Session Interrupted / Closed
      break;
    case 1:
      //Diagnosis Session Opened
      break;
    default:
    //  OS_TRACE(u32status);
      break;
  }
}
/*****************************************************************************
* FUNCTION    :	   vDiagCustomOnReceiveInd()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  
				   1). post an event on Receive Ind	       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
void vDiagCustomOnReceiveInd(tPCS8 ps8Buffer, tU32 u32nbytes)
{
  tU32 u32Count = OEDT_DIAGC_ZERO;
  if(OEDT_DIAGC_BUFF_SIZE < u32nbytes)
  {
     u32nbytes = OEDT_DIAGC_BUFF_SIZE;
  }

  for(u32Count = 0; u32Count < u32nbytes; u32Count++)
  {
    DiagCustom_arRequestMsg[u32Count] = (tU8)ps8Buffer[u32Count];
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
		              "Received value = %d",
		              DiagCustom_arRequestMsg[u32Count]);

  }
  OSAL_s32EventPost(DiagCustom_handleEvent_Test, OEDT_DIAGC_CALLBACK, OSAL_EN_EVENTMASK_OR);
}


/*****************************************************************************
* FUNCTION    :	   u32DiagCustomOpenClose()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_000

* DESCRIPTION :  
				   1). Open the DiagCustom
				   2). Close the DiagCustom
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomOpenClose( tVoid )
{
   tU32 u32Ret 						= OEDT_DIAGC_ZERO;
   OSAL_tIODescriptor Dfd 			= OEDT_DIAGC_ZERO;
   OSAL_tenAccess enAccess 			= OSAL_EN_READWRITE;

   /* Open the DiagCustom with Device Name */
   Dfd = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_DIAG_CUSTOM,enAccess );
   if( Dfd == OSAL_ERROR )
   {
   	  u32Ret = 1;
   }//if( Dfd == OSAL_ERROR )
   else 
   {  
      /* Close the with Device ID */
      if( OSAL_s32IOClose ( Dfd ) == OSAL_ERROR )
   	  {
   		 u32Ret = 2;
   	  }//if( OSAL_s32IOClose ())

   }//if-else( Dfd == OSAL_ERROR )
   
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DiagCustomOpenInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_001

* DESCRIPTION :  
				   1). Open the DiagCustom	with inval param
				   2). Close if open
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomOpenInval( tVoid )
{
#define TEST_STRING_DEVICE_DIAG_CUSTOM_INVALID       "diag_custom"     
   tU32 u32Ret 						= OEDT_DIAGC_ZERO;
   OSAL_tIODescriptor Dfd 			= OEDT_DIAGC_ZERO;
   OSAL_tenAccess enAccess 			= (OSAL_tenAccess)OEDT_DIAGC_STRING_INVALID_ACCESS_MODE;

  /* Open the DiagCustom with invalid Device Name and Invalid Access*/
  Dfd = OSAL_IOOpen ( TEST_STRING_DEVICE_DIAG_CUSTOM_INVALID,enAccess );
   if( Dfd != OSAL_ERROR )
   {
   	  u32Ret = 1;
      /* Close the with Device ID */
      if( OSAL_s32IOClose ( Dfd ) == OSAL_ERROR )
   	  {
   		 u32Ret = 2;
   	  }//if( OSAL_s32IOClose ())
  }//if( Dfd != OSAL_ERROR )
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DiagCustomOpenWithOutClose()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_002

* DESCRIPTION :  
				   1). Multiple Open the DiagCustom	
				   2). Close if open
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomOpenWithOutClose( tVoid )
{
   tU32 u32Ret 						= 0;
   OSAL_tIODescriptor Dfd1 			= OEDT_DIAGC_ZERO;
   OSAL_tIODescriptor Dfd2 			= OEDT_DIAGC_ZERO;
   OSAL_tenAccess enAccess 			= OSAL_EN_READWRITE;

   /* Open the DiagCustom with Device Name and Invalid Access*/
   Dfd1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_DIAG_CUSTOM,enAccess );
   if( Dfd1 != OSAL_ERROR )
   {
      /* Open the DiagCustom with Device Name without close*/
      Dfd2 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_DIAG_CUSTOM,enAccess );
	  if( Dfd2 != OSAL_ERROR )
	  {
	     u32Ret = 5; 
	     
	     /* Close the with Device ID */
	     if( OSAL_s32IOClose ( Dfd2 ) == OSAL_ERROR )
		 {
		    u32Ret = 6;
		 }//if( OSAL_s32IOClose(Dfd2))

	  }//if( Dfd2 != OSAL_ERROR )
      
      /* Close the with Device ID */
      if( OSAL_s32IOClose ( Dfd1 ) == OSAL_ERROR )
   	  {
   		 u32Ret += 2;
   	  }//if( OSAL_s32IOClose(Dfd1))

   }//if( Dfd1 != OSAL_ERROR )
   else
   {
      u32Ret = 1;
   }//if-else( Dfd1 != OSAL_ERROR )

   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32CDiagCustomDevCloseAlreadyClosed()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_003

* DESCRIPTION :  
           1). Open the DiagCustom	
				   2). Close if open
				   3). Try to close after first close
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomDevCloseAlreadyClosed( tVoid )
{
   tU32 u32Ret 						= OEDT_DIAGC_ZERO;
   OSAL_tIODescriptor Dfd 			= OEDT_DIAGC_ZERO;
   OSAL_tenAccess enAccess 			= OSAL_EN_READWRITE;

   /* Open the DiagCustom with Device Name and Invalid Access*/
   Dfd = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_DIAG_CUSTOM,enAccess );
   if( Dfd != OSAL_ERROR )
   {
      /* Close the with Device ID */
      if( OSAL_s32IOClose ( Dfd ) == OSAL_ERROR )
   	  {
   		 u32Ret = 2;
   	  }//if( OSAL_s32IOClose (Dfd))
	  else
	  {
	     /* Try to Close the with Device which already closed */
	     if( OSAL_s32IOClose ( Dfd ) != OSAL_ERROR )
		 {
		    u32Ret = 5;
		 }//if( OSAL_s32IOClose (Dfd))
	  }

   }//if( Dfd != OSAL_ERROR )
   else
   {
      u32Ret = 1;
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DiagCustomWrite()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_004

* DESCRIPTION :  
           1). Open the DiagCustom	
           2). Create event  for DiagCustom
           3). Register the call back with IOControl
           4). Wait for event
           5). Write to DiagCustom
				   6). Wait for the event from Write Callback
				   7). Clear the Callback
				   8). close the event
				   9). Close DiagCustom
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomWrite( tVoid )
{
   tU32 u32Ret 									= OEDT_DIAGC_ZERO; 
   tS32 s32BytesWritten							= OEDT_DIAGC_ZERO;
   tU32 u32ByteToWrite							= OEDT_DIAGC_ZERO;
   tU32 u32BuffSize								= OEDT_DIAGC_ZERO;
   tPCS8 PUWriteBuff							= OSAL_NULL;
   OSAL_tIODescriptor fd						= OEDT_DIAGC_ZERO;
   OSAL_trDiagCustomProxyCallback rCallback		= {OEDT_DIAGC_ZERO};
   OSAL_tEventMask rResultEventMask				= OEDT_DIAGC_ZERO;

   /* find the size of buffer to write */
   u32BuffSize = sizeof( OEDT_DIAGC_WRITE_DATA );
   /* check for PUWriteBuff NULL or not */ 
   if( OSAL_NULL == PUWriteBuff )
   {
      /* Allocate memory for buffer */
      PUWriteBuff = OSAL_pvMemoryAllocate( u32BuffSize );
	     if( OSAL_NULL == PUWriteBuff )
		 {
		    return 1;
		 }
		 else
		 {
		    OSAL_pvMemorySet((tPVoid)PUWriteBuff,0,u32BuffSize);
		 }
   }
   else
   {
      return 2;
   }
   
   /* open the diag_custom*/
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_CUSTOM, OSAL_EN_READWRITE);
  if(fd == OSAL_ERROR)
   {
      return 5;
   }
   else
   {
	  if(OSAL_C_INVALID_HANDLE == DiagCustom_handleEvent_Test)
	  {
	     /* Create the event*/
      if(OSAL_OK != OSAL_s32EventCreate(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
	     {
	         return 6;
	     }
	  	 else
		 {
        rCallback.m_pOnReceiveStatus = vDiagCustomReceiveStatus;
        rCallback.m_pOnReceiveCon = vDiagCustomCallBackWrite; 
        rCallback.m_pOnReceiveInd = vDiagCustomOnReceiveInd;
        /* Register the call for m_pOnReceiveInd,vDiagCustomCallBackWrite
         and m_pOnReceiveStatus*/ 
        if(OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32) &rCallback))
        {
          u32Ret = 3; 
        }
        else
        {
          (tVoid)OSAL_szStringNCopy( PUWriteBuff,OEDT_DIAGC_WRITE_DATA,OSAL_u32StringLength(OEDT_DIAGC_WRITE_DATA));
          /* Byte of data to write */
          u32ByteToWrite = (OSAL_u32StringLength(PUWriteBuff));
          /* wait for the event OEDT_DIAGC_CALLBACK to occur  */
          if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                 OEDT_DIAGC_CALLBACK, /*OSAL_tEventMask*/
                                                 OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                 OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                 &rResultEventMask /*OSAL_tEventMask*/))
          {
            u32Ret = 10; 
          }
          /* Write Data into DiagCustom */
          s32BytesWritten =OSAL_s32IOWrite(fd, PUWriteBuff,u32ByteToWrite);
          /* Check for Data Written */
          if(((s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != (tS32)u32ByteToWrite)))
          {
            u32Ret += 20;
          }
          /* wait for the event OEDT_DIAGC_WRITE to occur  */
          if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                 OEDT_DIAGC_WRITE, /*OSAL_tEventMask*/
                                                 OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                 OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                 &rResultEventMask /*OSAL_tEventMask*/))
          {
            u32Ret += 40; 
          }
        }
        /* clear the Registered the call backs */ 
        if(OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 0))
        {
          u32Ret += 100; 
        }
        /* Close the event*/
        if(OSAL_ERROR == OSAL_s32EventClose(DiagCustom_handleEvent_Test))
        {
          u32Ret += 200;
        }
        else
        {
          DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;
          /* Remove the event */
          if(OSAL_OK != OSAL_s32EventDelete(strDiagCustom_EventName_test))
          {
            u32Ret += 400;
          }
        }
      } 
    }
    /* close the event */
    if(OSAL_OK != OSAL_s32IOClose(fd))
    {
      u32Ret += 800;
	  }
   }
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32DiagCustomWriteExcBuffLen()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_005

* DESCRIPTION :  
           1). Open the DiagCustom	
           2). Create event  for DiagCustom
           3). Register the call back with IOControl
           4). Wait for event
           5). Write to DiagCustom
				   6). Write data into DiagCustom with number byte more than data to 
				       write.
				   7). Clear the Callback
				   8). close the event
				   9). Close DiagCustom
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomWriteExcBuffLen ( tVoid )
{
   tU32 u32Ret 									= OEDT_DIAGC_ZERO; 
   tS32 s32BytesWritten							= OEDT_DIAGC_ZERO;
   tU32 u32ByteToWrite							= OEDT_DIAGC_ZERO;
   tU32 u32BuffSize								= OEDT_DIAGC_ZERO;
   tPCS8 PUWriteBuff							= OSAL_NULL;
   OSAL_tIODescriptor fd						= OEDT_DIAGC_ZERO;
   OSAL_trDiagCustomProxyCallback rCallback		= {OEDT_DIAGC_ZERO};
   OSAL_tEventMask rResultEventMask				= OEDT_DIAGC_ZERO;

   /* find the size of buffer to write */
   u32BuffSize = sizeof( OEDT_DIAGC_WRITE_DATA );
   /* check for PUWriteBuff NULL or not */ 
   if( OSAL_NULL == PUWriteBuff )
   {
      /* Allocate memory for buffer */
      PUWriteBuff = OSAL_pvMemoryAllocate( u32BuffSize );
	     if( OSAL_NULL == PUWriteBuff )
		 {
		    return 1;
		 }
		 else
		 {
		    OSAL_pvMemorySet((tPVoid)PUWriteBuff,0,u32BuffSize);
		 }
   }
   else
   {
      return 2;
   }
   
   /* open the diag_custom*/
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_CUSTOM, OSAL_EN_READWRITE);
  if(fd == OSAL_ERROR)
   {
      return 1;
   }
   else
   {
	  if(OSAL_C_INVALID_HANDLE == DiagCustom_handleEvent_Test)
	  {
	     /* Create the event*/
	     if(OSAL_OK != OSAL_s32EventCreate(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
	     {
	         return 2;
	     }
	  	 else
		 {
        rCallback.m_pOnReceiveStatus = vDiagCustomReceiveStatus;
        rCallback.m_pOnReceiveCon = vDiagCustomCallBackWrite; 
        rCallback.m_pOnReceiveInd = vDiagCustomOnReceiveInd;
			/* Register the call for m_pOnReceiveInd,
			vDiagCustomCallBackWrite and m_pOnReceiveStatus*/ 
        if(OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32) &rCallback))
			{
			   u32Ret = 3; 
			}
			else 
			{
          (tVoid)OSAL_szStringNCopy(PUWriteBuff,OEDT_DIAGC_WRITE_DATA,OSAL_u32StringLength(OEDT_DIAGC_WRITE_DATA));
          /* Byte of data to write */
          u32ByteToWrite = (OSAL_u32StringLength(PUWriteBuff)+OEDT_DIAGC_ECX_BUFF_LEN);
          /* wait for the event OEDT_DIAGC_CALLBACK to occur  */
          if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                 OEDT_DIAGC_CALLBACK, /*OSAL_tEventMask*/
                                                 OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                 OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                 &rResultEventMask /*OSAL_tEventMask*/))
          {
            u32Ret = 10; 
          }
          /* Write Data into DiagCustom */
          s32BytesWritten =OSAL_s32IOWrite(fd, PUWriteBuff,u32ByteToWrite);
          /* Check for Data Written */
          if((s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != (tS32)u32ByteToWrite))
          {
            u32Ret += 20;
          }
          /* wait for the event OEDT_DIAGC_WRITE to occur  */
          if(OSAL_OK != (tS32) OSAL_s32EventWait(/*OSAL_tEventHandle*/ DiagCustom_handleEvent_Test, 
			   /*OSAL_tEventMask*/   OEDT_DIAGC_WRITE, 
			   /*OSAL_tenEventMaskFlag*/ OSAL_EN_EVENTMASK_OR,
			   /*OSAL_tMSecond*/ 		 OEDT_DIAGC_WAIT_TIME /*msec*/,
			   /*OSAL_tEventMask*/ &rResultEventMask))
			   {
            u32Ret += 40; 
			   }
			}
			/* clear the Registered the call backs */ 
        if(OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 0))
		    {
          u32Ret += 100; 
		    }
			/* Close the event*/
		    if(OSAL_ERROR == OSAL_s32EventClose(DiagCustom_handleEvent_Test))
		    {
          u32Ret += 200;
		    }
		 	else
			{
			   DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;
			   /* Remove the event */
			   if(OSAL_OK != OSAL_s32EventDelete(strDiagCustom_EventName_test))
			   {
            u32Ret += 400;
	    	}
	     } 
	  }
    }
    /* close the event */
    if(OSAL_OK != OSAL_s32IOClose(fd))
    {
      u32Ret += 800;
	  }
   }
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32DiagCustomWriteBuffNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_006

* DESCRIPTION :  
           1). Open the DiagCustom	
           2). Create event  for DiagCustom
           3). Register the call back with IOControl
           4). Wait for event
           5). Write to DiagCustom
				   6). Write data into DiagCustom with buff NULL
				   7). Clear the Callback
				   8). close the event
				   9). Close DiagCustom
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomWriteBuffNULL( tVoid )
{
   tU32 u32Ret 									= OEDT_DIAGC_ZERO; 
   tS32 s32BytesWritten							= OEDT_DIAGC_ZERO;
   tU32 u32ByteToWrite							= OEDT_DIAGC_ZERO;
   tU32 u32BuffSize								= OEDT_DIAGC_ZERO;
   tPCS8 PUWriteBuff							= OSAL_NULL;
   tPCS8 PUWriteSubBuff							= OSAL_NULL;
   OSAL_tIODescriptor fd						= OEDT_DIAGC_ZERO;
   OSAL_trDiagCustomProxyCallback rCallback		= {OEDT_DIAGC_ZERO};
   OSAL_tEventMask rResultEventMask				= OEDT_DIAGC_ZERO;

   /* find the size of buffer to write */
   u32BuffSize = sizeof( OEDT_DIAGC_WRITE_DATA );
   /* check for PUWriteBuff NULL or not */ 
   if( OSAL_NULL == PUWriteBuff )
   {
      /* Allocate memory for buffer */
      PUWriteSubBuff = OSAL_pvMemoryAllocate( u32BuffSize );
	     if( OSAL_NULL == PUWriteSubBuff )
		 {
		    return 1;
		 }
		 else
		 {
		    OSAL_pvMemorySet((tPVoid)PUWriteSubBuff,0,u32BuffSize);
		 }
   }
   else
   {
      return 2;
   }
   
   /* open the diag_custom*/
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_CUSTOM, OSAL_EN_READWRITE);
  if(fd == OSAL_ERROR)
   {
      return 1;
   }
   else
   {
	  if(OSAL_C_INVALID_HANDLE == DiagCustom_handleEvent_Test)
    {/* Create the event*/
	     if(OSAL_OK != OSAL_s32EventCreate(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
	     {
	         return 2;
	     }
	  	 else
		 {
        rCallback.m_pOnReceiveStatus = vDiagCustomReceiveStatus;
        rCallback.m_pOnReceiveCon = vDiagCustomCallBackWrite; 
        rCallback.m_pOnReceiveInd = vDiagCustomOnReceiveInd;
        /* Register the call for m_pOnReceiveInd,vDiagCustomCallBackWrite
         and m_pOnReceiveStatus*/ 
        if(OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32) &rCallback))
        {
          u32Ret = 3; 
        }
        /* wait for the event OEDT_DIAGC_CALLBACK to occur  */
        else
        {
          if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                 OEDT_DIAGC_CALLBACK, /*OSAL_tEventMask*/
                                                 OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                 OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                 &rResultEventMask /*OSAL_tEventMask*/))
          {
            u32Ret = 10; 
          }
          /* Write Data into DiagCustom  PUWriteBuff==NULL */
          s32BytesWritten =OSAL_s32IOWrite(fd,PUWriteBuff,u32ByteToWrite);
          /* Check for Data Written */
          if((s32BytesWritten == OSAL_ERROR))
          {
            (tVoid)OSAL_szStringNCopy(PUWriteSubBuff,OEDT_DIAGC_WRITE_DATA,OSAL_u32StringLength(OEDT_DIAGC_WRITE_DATA));
            /* Byte of data to write */
            u32ByteToWrite = (OSAL_u32StringLength(PUWriteSubBuff));
            s32BytesWritten =OSAL_s32IOWrite(fd, PUWriteSubBuff,u32ByteToWrite);
            /* Check for Data Written */
            if(((s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != (tS32)u32ByteToWrite)))
            {
              u32Ret += 20;
            }
            /* wait for the event OEDT_DIAGC_CALLBACK to occur  */
            if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                   OEDT_DIAGC_WRITE, /*OSAL_tEventMask*/
                                                   OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                   OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                   &rResultEventMask /*OSAL_tEventMask*/))
            {
              u32Ret += 40; 
            }
          }
          else
		    {
			   u32Ret += 80; 
            if(OSAL_OK != (tS32) OSAL_s32EventWait(DiagCustom_handleEvent_Test, /*OSAL_tEventHandle*/
                                                   OEDT_DIAGC_WRITE, /*OSAL_tEventMask*/
                                                   OSAL_EN_EVENTMASK_OR, /*OSAL_tenEventMaskFlag*/
                                                   OSAL_C_TIMEOUT_FOREVER /*msec*/, /*OSAL_tMSecond*/ 
                                                   &rResultEventMask /*OSAL_tEventMask*/))
            {
              u32Ret += 100; 
            }
          }
        }
        /* clear the Registered the call backs */ 
        if(OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 0))
        {
          u32Ret += 200; 
        }
        /* Close the event*/
        if(OSAL_ERROR == OSAL_s32EventClose(DiagCustom_handleEvent_Test))
        {
          u32Ret += 400;
        }
        else
        {
          DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;
          /* Remove the event */
          if(OSAL_OK != OSAL_s32EventDelete(strDiagCustom_EventName_test))
          {
            u32Ret += 800;
          }
        }
      } 
    }
    /* close the event */
    if(OSAL_OK != OSAL_s32IOClose(fd))
    {
      u32Ret += 1000;
	  }
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DiagCustomCSMErrReg()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_DiagCustom_007

* DESCRIPTION :  
           1). Open the DiagCustom	
           2). Create event  for DiagCustom
           3). Register the call back with IOControl
           4). Wait for event
				   5). Clear the Callback
				   6). close the event
				   7). Close DiagCustom
			       	
* HISTORY     :	   
* 18- 12- 2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32DiagCustomCSMErrReg(tVoid)
{
   tU32 u32Ret 										= OEDT_DIAGC_ZERO; 
   OSAL_tIODescriptor fd							= OEDT_DIAGC_ZERO;
   OSAL_trDiagCustomCsmErrorCallback rErrCallback	= {OEDT_DIAGC_ZERO};
   OSAL_tEventMask rResultEventMask					= OEDT_DIAGC_ZERO;
   /* open the diag_custom*/
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_CUSTOM, OSAL_EN_READWRITE);
  if(fd == OSAL_ERROR)
   {
      return 1;
   }
   else
   {
	  if(OSAL_C_INVALID_HANDLE == DiagCustom_handleEvent_Test)
    {/* Create the event*/
	     if(OSAL_OK != OSAL_s32EventCreate(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
	     {
	         return 2;
	     }
	  	 else
		 {
			rErrCallback.m_pOnReceiveError = vDiagCustomOnCSMError; 
			/* Register the call for m_pOnReceiveInd and m_pOnReceiveStatus*/ 
        if(OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_DIAGCUSTOM_REG_CSM_ERROR, (tS32) &rErrCallback))
			{
			   u32Ret = 3; 
			}
			/*wait for the event OEDT_DIAGC_CALLBACK to occur */
        else if(OSAL_OK != (tS32) OSAL_s32EventWait(/*OSAL_tEventHandle*/ DiagCustom_handleEvent_Test, 
			/*OSAL_tEventMask*/   OEDT_DIAGC_CSMERROR, 
			/*OSAL_tenEventMaskFlag*/ OSAL_EN_EVENTMASK_OR,
			/*OSAL_tMSecond*/ 		 OEDT_DIAGC_WAIT_TIME /*msec*/,
			/*OSAL_tEventMask*/ &rResultEventMask))
			{
			   u32Ret += 10; 
			}
			/* clear the Registered the call backs */ 
        if(OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_DIAGCUSTOM_REG_CSM_ERROR, 0))
		    {
			   u32Ret += 20; 
		    }
			/* Close the event*/
		    if(OSAL_ERROR == OSAL_s32EventClose(DiagCustom_handleEvent_Test))
		    {
			   u32Ret += 30;
		    }
		 	else
			{
			   DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;
			   /* Remove the event */
			   if(OSAL_OK != OSAL_s32EventDelete(strDiagCustom_EventName_test))
			   {
			      u32Ret += 50;
			   }
	    	}
	     } 
	  }
	  /* close the diag_custom*/
    if(OSAL_OK != OSAL_s32IOClose(fd))
	  {
	    u32Ret += 100;
	  }
   }
   return u32Ret;
}

/************************************************************************
|end of file oedt_DiagCustom_TestFuncs.c
|-----------------------------------------------------------------------*/














#if 0
/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/

static tCString   strDiagCustom_EventName_test = "DiagCustomTest1";
static OSAL_tEventHandle DiagCustom_handleEvent_Test = OSAL_C_INVALID_HANDLE;

static tU8 DiagCustom_arRequestMsg[255];

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DiagCustom_vTestOnReceiveStatus
 * CREATED     : 2006-02-17
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : void DiagCustom_vTestOnReceiveStatus(tU32 u32status)
 * ARGUMENTS   : 
 *               tU32 u32status
 * RETURN VALUE: 
 *               none
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

void DiagCustom_vTestOnReceiveStatus(tU32 u32status)
{
  switch(u32status)
  {
    case 0:
      //Diagnosis Session Interrupted / Closed
      break;
    case 1:
      //Diagnosis Session Opened
      break;
    default:
    //  OS_TRACE(u32status);
      break;
  }
}


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DiagCustom_vTestOnReceiveInd
 * CREATED     : 2006-02-17
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : void DiagCustom_vTestOnReceiveInd(tPCS8 ps8Buffer,tU32
 *      u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               none
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

void DiagCustom_vTestOnReceiveInd(tPCS8 ps8Buffer, tU32 u32nbytes)
{
  tU32 i;
  for(i = 0; i < u32nbytes; i++)
  {
    DiagCustom_arRequestMsg[i] = ps8Buffer[i];
  }
  OSAL_s32EventPost(DiagCustom_handleEvent_Test, 1, OSAL_EN_EVENTMASK_OR);
}


/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : u32DiagCustomTest
 * CREATED     : 2006-01-25
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tU32 u32DiagCustomTest(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tU32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tU32 u32DiagCustomTest(tVoid)
{
  tU32 u32Result = 0;
  OSAL_tIODescriptor fd;
  OSAL_trDiagCustomProxyCallback rCallback;
  OSAL_tEventMask rResultEventMask;

  fd = OSAL_IOOpen("/dev/diag_custom", OSAL_EN_READWRITE);
  if(fd == OSAL_ERROR)
  {
    return 1;
  }

  if(OSAL_C_INVALID_HANDLE == DiagCustom_handleEvent_Test)
  {
      if(OSAL_OK != OSAL_s32EventCreate(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
      {
          return 1;
      }
  }
  else
  {
      if(OSAL_OK != OSAL_s32EventOpen(strDiagCustom_EventName_test, &DiagCustom_handleEvent_Test))
      {
          return 1;
      }
  }

  rCallback.m_pOnReceiveStatus = DiagCustom_vTestOnReceiveStatus;
  rCallback.m_pOnReceiveCon = NULL; // TODO if OSAL_IOWrite is tested
  rCallback.m_pOnReceiveInd = DiagCustom_vTestOnReceiveInd;

  if(OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32) &rCallback))
  {
    u32Result = 3; 
  }
  else if(OSAL_OK == (tS32) OSAL_s32EventWait(
    /*OSAL_tEventHandle*/ DiagCustom_handleEvent_Test, 
    /*OSAL_tEventMask*/   1, 
    /*OSAL_tenEventMaskFlag*/ OSAL_EN_EVENTMASK_OR,
    /*OSAL_tMSecond*/ 		 10000 /*msec*/,
    /*OSAL_tEventMask*/ &rResultEventMask))
  {
    if(rResultEventMask & 1)
    {
      /* request message received */

      /* Clear event */
      if(OSAL_OK == OSAL_s32EventPost(DiagCustom_handleEvent_Test,
                         ~1, OSAL_EN_EVENTMASK_AND))
      {
        /* TODO: Send a response message */

        switch(DiagCustom_arRequestMsg[0])
        {
          case 0x3E:
            // Tester Present
            break;
          default:
            break;
        }
      }
    }
  }

  if(OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 0))
  {
    u32Result = 7; 
  }

  if(OSAL_OK != OSAL_s32EventClose(DiagCustom_handleEvent_Test))
  {
      return 1;
  }
/*  
  if(OSAL_OK != OSAL_s32EventDelete(strDiagCustom_EventName_test))
  {
      return 1;
  }
*/  
   

  if(OSAL_OK != OSAL_s32IOClose(fd))
  {
  {
    return 2;
  }

  return u32Result;
}

/* End of File odt_DiagCustom_TestFuncs.c                                  */

#endif
