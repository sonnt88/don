/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        oedt_DiagCanDesc_TestFuncs.c
 *
 *  O(E)DT test for the TKernel driver candesc
 * Version History:
 * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
**********************************************************************************/
#include <basic.h>
#include <tk/tkernel.h>
#include <extension/outer.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

#include "lld_candesc.h"  

#include  "oedt_DiagCanDescTestFuncs.h"
#include  <extension/device.h>


#define OEDT_DIAGCANDESC_WRITE_DATA							"Data to DIAGC"


/*****************************************************************************
* FUNCTION:		u32CanDescLldOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_000
* DESCRIPTION:  Open and close the CanDesc driver
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldOpenClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  {/*no error*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 2;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldOpenInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_001
* DESCRIPTION:  Open with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldOpenInvalParam(void)
{
  tU32  Vu32Ret=0;
  ID    Vid;

  /* open driver */
  Vid = tkse_opn_dev((UB*)"can",TD_UPDATE);  
  /* check id*/
  if(Vid >= E_OK)
  { /* error string "can" is wrong*/
    Vu32Ret = 1;
    /*close device*/
    if(tkse_cls_dev(Vid ,0) != E_OK)
    {/*error*/
      Vu32Ret += 10;
    }/*end if*/
  }/*end if*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldOpenNullParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_002
* DESCRIPTION:  Open with NULL param
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldOpenNullParam(void)
{
  tU32  Vu32Ret=0;
  ID    Vid;
  /* open driver with NULL*/
  Vid = tkse_opn_dev((UB*)NULL,TD_UPDATE);  
  /* check id*/
  if(Vid >= E_OK)
  { /* error must be returned*/
    Vu32Ret = 1;
    /*close device*/
    if(tkse_cls_dev(Vid ,0) != E_OK)
    {/*error*/
      Vu32Ret += 10;
    }/*end if*/
  }/*end if*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldCloseInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_003
* DESCRIPTION:  close with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldCloseInvalParam(void)
{
  tU32  Vu32Ret=0;
  /*close device with invalid param*/
  if(tkse_cls_dev(-1 ,0) == E_OK)
  {/*error*/
    Vu32Ret = 1;
  }/*end if*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_004
* DESCRIPTION:  Try to open the driver which is already opened
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldReOpen(void)
{
  tU32  Vu32Ret=0;
  ID    Vid1,Vid2;   

  /* open driver first*/
  Vid1 = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(Vid1 < E_OK)
  { /* error must be returned*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /* open driver second*/
    Vid2 = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
    if(Vid2 < E_OK)
    { /* error must be returned*/
      Vu32Ret = 10;
    }/*end if*/
    else
    { /*close device*/
      if(tkse_cls_dev(Vid2 ,0) != E_OK)
      {/*error*/
        Vu32Ret = 100;
      }/*end if*/
    }/*end else*/
    /*close device*/
    if(tkse_cls_dev(Vid1 ,0) != E_OK)
    {/*error*/
      Vu32Ret += 1000;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldReClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_005
* DESCRIPTION:  Try to close the driver which is already closed
* HISTORY:		Created Andrea B�ter (TMS)  02.06.2008 
******************************************************************************/
tU32 u32CanDescLldReClose(void)
{
  tU32  Vu32Ret=0;
  ID    Vid;   

  /* open driver first*/
  Vid = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(Vid < E_OK)
  { /* error must be returned*/
    Vu32Ret = 1;
  }/*end if*/
  else
  {
    if(tkse_cls_dev(Vid ,0) != E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    else
    {
      if(tkse_cls_dev(Vid ,0) == E_OK)
      {/*error*/
        Vu32Ret = 3;
      }/*end if*/
    }/*end else*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldRegisterClient()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_006
* DESCRIPTION:  register client for Status, Con and Ind
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldRegisterClient(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*register client*/
    tU32 Vu32Reg=LLD_CANDESC_REGISTER;
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldRegisterClientInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_007
* DESCRIPTION:  register client for invailid param
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldRegisterClientInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*register client*/
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,NULL,0,&ViSize)==E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldRegisterClientAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_008
* DESCRIPTION:  register client after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldRegisterClientAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
    else
    {/*register client*/
      tU32 Vu32Reg=LLD_CANDESC_REGISTER;
      if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret = 2;
      }/*end if*/
    }/*end else*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldUnRegisterClient()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_009
* DESCRIPTION:  unregister client for Status, Con and Ind
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldUnRegisterClient(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*unregister client*/
    tU32 Vu32Reg=LLD_CANDESC_UNREGISTER;
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldUnRegisterClientInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_010
* DESCRIPTION:  unregister client for invalid param
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldUnRegisterClientInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*unregister client*/  
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,NULL,0,&ViSize)==E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldUnRegisterClientAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_011
* DESCRIPTION:  unregister client after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  03.06.2008 
******************************************************************************/
tU32 u32CanDescLldUnRegisterClientAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
    else
    {/*unregister client*/
      tU32 Vu32Reg=LLD_CANDESC_UNREGISTER;
      if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret = 2;
      }/*end if*/
    }/*end else*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMTrigger()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_012
* DESCRIPTION:  write csm trigger
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMTrigger(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*set flag for trigger csm */
    UINT Flag = LLD_CANDESK_EVT_CSMCMD_CLR_DIAG_INFO|LLD_CANDESK_EVT_CSMCMD_CHECK_ERROR;
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_CSM_TRIGGER,&Flag,sizeof(Flag),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMTriggerInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_013
* DESCRIPTION:  write csm trigger with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMTriggerInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*set flag for trigger csm */  
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_CSM_TRIGGER,NULL,0,&ViSize)==E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMTriggerAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_014
* DESCRIPTION:  write csm trigger after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMTriggerAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 4;
    }/*end if*/
    else
    {/*set flag for trigger csm */
      UINT Flag = LLD_CANDESK_EVT_CSMCMD_CLR_DIAG_INFO|LLD_CANDESK_EVT_CSMCMD_CHECK_ERROR;
      if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_CSM_TRIGGER,&Flag,sizeof(Flag),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret = 2;
      }/*end if*/
    }
  }/*end else*/
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMResponse()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_015
* DESCRIPTION:  write csm response
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
* 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
******************************************************************************/
tU32 u32CanDescLldWriteCSMResponse(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  tU32  Vu32BuffSize=sizeof(OEDT_DIAGCANDESC_WRITE_DATA);
  tPCS8 VuWriteBuff=OSAL_pvMemoryAllocate(Vu32BuffSize);

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
	/*register client*/
	tU32 Vu32Reg=LLD_CANDESC_REGISTER;
	if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)!=E_OK)
	{/*error*/
	  Vu32Ret = 2;
	}/*end if*/
	else
	{ /*wait*/
	  OSAL_s32ThreadWait(10000);/*10 s*/
	  /*write csm response*/
	  if ( OSAL_szStringNCopy
		   ( 
			  (tString)VuWriteBuff, 
			  OEDT_DIAGCANDESC_WRITE_DATA,
			  OSAL_u32StringLength( OEDT_DIAGCANDESC_WRITE_DATA )
		   ) == (tString)VuWriteBuff )
	  {
		  if( tkse_swri_dev( VidCanDescLld, 0, &VuWriteBuff, (INT)Vu32BuffSize, &ViSize ) != E_OK )
		  { /*trace error*/ 
			Vu32Ret = 4;
		  }/*end if*/
	  }
	  else
	  {
		  Vu32Ret = 100;
	  }
	}/*end else*/   
	if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
	{/*error*/
	  Vu32Ret= 10;
	}/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMResponseInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_016
* DESCRIPTION:  write csm response with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMResponseInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*register client*/
    tU32 Vu32Reg=LLD_CANDESC_REGISTER;
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    else
    { /* wait*/
      OSAL_s32ThreadWait(2000);/*2000 ms*/
      /*write csm response*/
      if(tkse_swri_dev(VidCanDescLld, 0,NULL, 0, &ViSize)==E_OK)
      { /*trace error*/ 
        Vu32Ret = 4;
      }/*end if*/
    }/*end else*/   
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMResponseAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_017
* DESCRIPTION:  write csm response after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
* 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
******************************************************************************/
tU32 u32CanDescLldWriteCSMResponseAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  tU32  Vu32BuffSize=sizeof(OEDT_DIAGCANDESC_WRITE_DATA);
  tPCS8 VuWriteBuff=OSAL_pvMemoryAllocate(Vu32BuffSize);

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    /*register client*/
    tU32 Vu32Reg=LLD_CANDESC_REGISTER;
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_REGISTER_CLIENT,&Vu32Reg,sizeof(Vu32Reg),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret = 2;
    }/*end if*/
    else
    {
      if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
      {/*error*/
        Vu32Ret= 10;
      }/*end if*/
      else
      {/*write csm response*/
        if ( OSAL_szStringNCopy
			(
				(tString)VuWriteBuff,
				OEDT_DIAGCANDESC_WRITE_DATA,
				OSAL_u32StringLength(OEDT_DIAGCANDESC_WRITE_DATA)
			) == (tString)VuWriteBuff )
		{
			if(tkse_swri_dev(VidCanDescLld, 0,&VuWriteBuff, (INT)Vu32BuffSize, &ViSize)==E_OK)
			{ /*trace error*/ 
			  Vu32Ret = 4;
			}/*end if*/
		}
		else
		{
			Vu32Ret = 100;
		}
      }/*end else*/
    }/*end else*/   
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteUUDTFrame()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_018
* DESCRIPTION:  write UUDT frame
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
* 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
******************************************************************************/
tU32 u32CanDescLldWriteUUDTFrame(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  static tsCanDescUUDTFrame VtsUUDTFrame;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    VtsUUDTFrame.ubMessageNumber=8;
    VtsUUDTFrame.ubBuffer[0]=1;   
    VtsUUDTFrame.ubBuffer[2]=2;   
    VtsUUDTFrame.ubBuffer[3]=3;   
    VtsUUDTFrame.ubBuffer[4]=4;   
    VtsUUDTFrame.ubBuffer[5]=5;   
    VtsUUDTFrame.ubBuffer[6]=6;   
    //VtsUUDTFrame.ubBuffer[7]=7;  //Commented to remove lint warning. Out of bounds issue. jev1kor
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_UUDT_FRAME,&VtsUUDTFrame,sizeof(tsCanDescUUDTFrame),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret=4;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}

/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteUUDTFrameInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_019
* DESCRIPTION:  write UUDT frame with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteUUDTFrameInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/   
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_UUDT_FRAME,NULL,sizeof(tsCanDescUUDTFrame),&ViSize)==E_OK)
    {/*error*/
      Vu32Ret=4;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteUUDTFrameeAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_020
* DESCRIPTION:  write UUDT frame after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
* 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
******************************************************************************/
tU32 u32CanDescLldWriteUUDTFrameeAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  static tsCanDescUUDTFrame VtsUUDTFrame;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
    else
    {
      VtsUUDTFrame.ubMessageNumber=8;
      VtsUUDTFrame.ubBuffer[0]=1;   
      VtsUUDTFrame.ubBuffer[2]=2;   
      VtsUUDTFrame.ubBuffer[3]=3;   
      VtsUUDTFrame.ubBuffer[4]=4;   
      VtsUUDTFrame.ubBuffer[5]=5;   
      VtsUUDTFrame.ubBuffer[6]=6;   
      //VtsUUDTFrame.ubBuffer[7]=7; //Commented to remove lint. Out of bounds error. jev1kor
      if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_WRITE_UUDT_FRAME,&VtsUUDTFrame,sizeof(tsCanDescUUDTFrame),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret=4;
      }/*end if*/
    }/*end else*/ 
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteEventID()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_021
* DESCRIPTION:  write event id
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteEventID(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  ID    VidEvent=10;
  ID    VidEventRead;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/    
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_CSM_EVENTID,&VidEvent,sizeof(ID),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret=2;
    }/*end if*/
    else
    {/*read id*/
      if(tkse_srea_dev(VidCanDescLld, LLD_CANDESC_CSM_EVENTID,&VidEventRead,sizeof(ID),&ViSize)!=E_OK)
      {/*error*/
        Vu32Ret=4;
      }/*end if*/
      else if(VidEventRead!=VidEvent)
      {
        Vu32Ret=8;
      }
    }
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteEventIDInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_022
* DESCRIPTION:  write event id with invalid param 
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteEventIDInvalidParam(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;  

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/    
    if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_CSM_EVENTID,NULL,sizeof(ID),&ViSize)==E_OK)
    {/*error*/
      Vu32Ret=2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteEventIDAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_023
* DESCRIPTION:  write event id after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteEventIDAfterDevClose(void)
{
  tU32  Vu32Ret=0;
  ID    VidCanDescLld;
  INT   ViSize=0;
  ID    VidEvent=10;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/   
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
    else
    {
      if(tkse_swri_dev(VidCanDescLld, LLD_CANDESC_CSM_EVENTID,&VidEvent,sizeof(ID),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret=2;
      }/*end if*/
    }/*end else*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMError()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_024
* DESCRIPTION:  write csm error
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMError(void)
{
  tU32              Vu32Ret=0;
  ID                VidCanDescLld;
  INT               ViSize=0;
  tsCanDescCsmError VtsCsmError;   

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/    
    VtsCsmError.s32Code=0x4;
    VtsCsmError.s32Status=0x05;
    if(tkse_swri_dev(VidCanDescLld,LLD_CANDESC_WRITE_CSM_ERROR ,&VtsCsmError,sizeof(tsCanDescCsmError),&ViSize)!=E_OK)
    {/*error*/
      Vu32Ret=2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMerrorInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_025
* DESCRIPTION:  write csm error with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMerrorInvalidParam(void)
{
  tU32              Vu32Ret=0;
  ID                VidCanDescLld;
  INT               ViSize=0;

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/    
    if(tkse_swri_dev(VidCanDescLld,LLD_CANDESC_WRITE_CSM_ERROR ,NULL,sizeof(tsCanDescCsmError),&ViSize)==E_OK)
    {/*error*/
      Vu32Ret=2;
    }/*end if*/
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32CanDescLldWriteCSMTriggerAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CanDesc_026
* DESCRIPTION:  write csm trigger after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  04.06.2008 
******************************************************************************/
tU32 u32CanDescLldWriteCSMErrorAfterDevClose(void)
{
  tU32              Vu32Ret=0;
  ID                VidCanDescLld;
  INT               ViSize=0;
  tsCanDescCsmError VtsCsmError;   

  /* open driver */
  VidCanDescLld = tkse_opn_dev((UB*)"candesc",TD_UPDATE);  
  /* check id*/
  if(VidCanDescLld < E_OK)
  { /*error*/
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*no error*/    
    if(tkse_cls_dev(VidCanDescLld ,0) != E_OK)
    {/*error*/
      Vu32Ret= 10;
    }/*end if*/
    else
    {
      VtsCsmError.s32Code=0x4;
      VtsCsmError.s32Status=0x05;
      if(tkse_swri_dev(VidCanDescLld,LLD_CANDESC_WRITE_CSM_ERROR ,&VtsCsmError,sizeof(tsCanDescCsmError),&ViSize)==E_OK)
      {/*error*/
        Vu32Ret=2;
      }/*end if*/
    }/*end else*/   
  }/*end else*/
  return(Vu32Ret);
}



