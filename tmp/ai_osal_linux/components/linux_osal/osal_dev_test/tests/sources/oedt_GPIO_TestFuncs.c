/*****************************************************************************
* FILE         : oedt_GPIO_TestFuncs.c
*
* SW-COMPONENT : OEDT_FrmWrk 
*
* DESCRIPTION  : This file implements the individual test cases for the GPIO 
*                device for GM-GE hardware.
*              
* AUTHOR(s)    :  Shilpa Bhat (RBIN/EDI3)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
* 03 Oct,2007   | version 0.1               | Shilpa Bhat(RBIN/EDI3)
*               |                           | on 3 Oct, 2007
*-----------------------------------------------------------------------------
* 11 Oct,2007   | version 0.2               | Shilpa Bhat(RBIN/EDI3)
*               |                           | Updated based on review comments
*               |                           | on 11 Oct, 2007
*-----------------------------------------------------------------------------
* 21 Mar, 2008  | version 0.3               | Shilpa Bhat (RBIN/ECM1)
*               |                           | Updated case    u32GPIODevReOpen
*-----------------------------------------------------------------------------
* 28 Apr, 2008  | version 0.4               | Shilpa Bhat (RBEI/ECM1)
*               |                           | Added 4 cases for testing 
*               |                           | callback implementation
* ----------------------------------------------------------------------------
* 06 May, 2008  | version 0.5               | Shilpa Bhat (RBEI/ECM1) 
*               |                           | Modified case 
*               |                           | u32GPIOCallbackRegInvalFunc
*               |                           | Added new testcase 
*               |                           | u32GPIOResourceCreatRemovCheck
* ----------------------------------------------------------------------------
* 23 May, 2008  | version 0.6               | Shilpa Bhat (RBEI/ECM1) 
*               |                           | Added case
*               |                           | u32GPIO_OSAL_To_DTTEnumTest
* ----------------------------------------------------------------------------
* 22 Sep, 2008  | version 0.7               | Shilpa Bhat (RBEI/ECM1) 
*               |                           | Updated case
*               |                           | u32GPIOSetOutputInInputMode
* ----------------------------------------------------------------------------
* 22 Sep, 2008  | version 0.8               | Shilpa Bhat (RBEI/ECM1) 
*               |                           | Removed case
*               |                           | u32GPIOSetGetTestmode 
* ----------------------------------------------------------------------------
* 29 Oct, 2008  | version 0.9               | Anoop Chandran (RBEI/ECM1) 
*               |                           | update the testcases
*               |                           | TU_OEDT_GPIO_026-032 
*-----------------------------------------------------------------------------
* 14.11.2008    | version 1.0               | Shilpa Bhat (RBEI/ECM1) 
*               |                           | Added testcase
*               |                           | u32GPIOActiveStateTest
*-----------------------------------------------------------------------------
* 19.03.2009    | version 1.1               | Shilpa Bhat (RBEI/ECF1) 
*               |                           | Lint Removal
*-----------------------------------------------------------------------------
* 24.07.2009    | version 1.1               | Hari babu S (RBEI/ECF1) 
*               |                           | Modified for Gen2
*-----------------------------------------------------------------------------
* 24.03.2010    | version 1.3               | Vijay D (RBEI/ECF1) 
*               |                           | Modified for Gen2 for supporting
*               |                           | 128 pins
*-----------------------------------------------------------------------------
* 03.04.2012    | version 1.4               | Madhu Kiran Ramachandra (RBEI/ECF5)
*               |                           | Modified function u32GPIOCallbackTestCommon
*               |                           | to test callback registration feature for 
*               |                           | Positive, negative and Both together.
*-----------------------------------------------------------------------------
* 27.7.2012     | version 1.5               |Sudharsanan Sivagnanam (RBEI/ECF5)
*                                           |u32GPIO_OSAL_To_DTTEnumTest,u32GPIOResourceCreatRemovCheck,
*                                           |u32GPIOResourceCreatRemovCheckCommon,u32GPIODevCreateRemove,
*                                           |OEDT_TEST(GPIOSetExtPinViaI2c) were removed and Ported to linux side for LSIM
*-----------------------------------------------------------------------------
* 25.09.2012    | version 1.6               | Gopinathan Subu (RBEI/ECF5)
*               |                           | Invalid ID changed
*-----------------------------------------------------------------------------
* 08.10.2012    | version 1.7               | Matthias Thomae (CM-AI/PJ-CF31)
*               |                           | Modified for Linux Gen3
*-----------------------------------------------------------------------------
* 07.01.2013    | version 1.8               | Matthias Thomae (CM-AI/PJ-CF31)
*               |                           | Added remote GPIO test functions
*-----------------------------------------------------------------------------
* 14.02.2013    | version 1.9               | Matthias Thomae (CM-AI/PJ-CF31)
*               |                           | Reduce number of loops in worker
*               |                           | thread for MultiThreadTestRemote
*               |                           | to make it exec in time on target
*-----------------------------------------------------------------------------
* 03.05.2013    | version 2.0               | Gokulkrishnan N (RBEI/ECF5)
*               |                           | Process binary path adaptation for
*               |                           | multiple projects
*-----------------------------------------------------------------------------
* 6.2.2015      | version 2.1               | Shahida Mohammed Ashraf(RBEI/ECF5)
*               |                           | Modified for lint fix in MyCallback()
*               |                           | Created a function u32GPIOGetProcess\
*               |                           | Response() for lint fix.
*               |                           | Modified _u32GPIOMultiProcessTest()
*               |                           | replacing GetProcessResponse()
*               |                           | by u32GPIOGetProcessResponse().
*-----------------------------------------------------------------------------
* 12.11.2015    | version 2.2               | Bhagyashree Chidanand Burji (RBEI/ECF5) 
*               |                           | Modified for lint fix 
*-----------------------------------------------------------------------------
* 17.03.2016    | version 2.3               | Sadhna Sharma (RBEI/ECF5) 
*               |                           | Modified for GPIO tests on LSIM  
*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_GPIO_TestFuncs.h"
#include "oedt_helper_funcs.h"

#include "oedt_Types.h"

#define OEDT_NO_GPIO_BANKS             6
#define OEDT_GPIO0_0                   0
#define OEDT_GPIO3_31                  191
#define GPIO_MAX_LENGTH                20
#define NO_CHARS_IN_STRING             8
#define U32_MAX_DECIMALS               11
#define OSAL_C_S32_GPIO_IOCTRL_INVAL_FUNC       -1000
#define OSAL_C_STRING_GPIO_INVALID_ACCESS_MODE  -1

/*GPIO pins from 4 different banks*/
#define OEDT_GPIO_BANK1  10                             /*Add output pins here*/
#define OEDT_GPIO_BANK2  366                            /*Add output pins here*/
#define OEDT_GPIO_BANK3  402                            /*Add output pins here*/
#define OEDT_GPIO_BANK4  438                            /*Add output pins here*/
#define OEDT_GPIO_BANK5  1026                           /*Add output pins here*/
#define OEDT_GPIO_BANK6  1035                           /*Add output pins here*/

#define OEDT_GPIO_NUM_WORKER_THREADS      ((tU16) 3 )

#define NO_CALLBACKS_CALLED (10)

#define PRIORITY             100
#define INVALID_PID          ((tS32)-100)

/*Event values*/
#define EveValT0 (0x00000001)
#define EveValT1 (0x00000002)
#define EveValT2 (0x00000004)
#define EveValT3 (0x00000008)
#define EveValT4 (0x00000010)
#define EveValT5 (0x00000020)

#define EveMask  (0x0000003F)

typedef struct _tag_oedt_gpio_thread_param
{
   tInt            n_thread_id;
   OSAL_tGPIODevID GpioDevID;
} trOedtGpioThreadParam;

static OSAL_tGPIODevID gpio_input_1  = 0;
static OSAL_tGPIODevID gpio_input_2  = 0;
static OSAL_tGPIODevID gpio_output_1 = 0;
static OSAL_tGPIODevID gpio_output_2 = 0;
static OSAL_tGPIODevID gpio_output_3 = 0;

static tU16 num_loops_worker_thread = 1000;

static tBool threadspawn_error = FALSE;
tBool b_oedt_gpio_all_worker_threads_started = FALSE;
tBool b_oedt_gpio_worker_thread_error_occured[OEDT_GPIO_NUM_WORKER_THREADS] 
      = {FALSE};
tBool b_oedt_gpio_worker_thread_terminated[OEDT_GPIO_NUM_WORKER_THREADS]    
      = {FALSE};
trOedtGpioThreadParam tr_oedt_gpio_worker_params[OEDT_NO_GPIO_BANKS]
                      = {{0}};

static OSAL_tIODescriptor Fd = 0;

static OSAL_tEventHandle evHandle = 0;

static tU32 Check_Callback = 0;
static tU32 mycallback1 = 0;
static tU32 mycallback2 = 0;    
static tU32 mycallback3 = 0;
static tU32 mycallback4 = 0;
static tU32 mycallback5 = 0;
static tU32 mycallback6 = 0;

/*Capture error values of each thread*/
static tU32 u32RetTr[6] = {0};

/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
static tVoid OEDT_GPIO_Thread(tVoid *pvArg );

static tU32 u32GPIOGetProcessResponse(OSAL_tMQueueHandle mqHandle);

/*****************************************************************************
* FUNCTION:       MyCallback()
* DESCRIPTION:    Gets called whenver callback is set/cancelled. Clears
*                 pending
*                 interrupts, if any
* HISTORY:        Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*                 Modified for lint fix by Shahida Mohammed Ashraf on 6.2.2015
*****************************************************************************/
tVoid MyCallback(tVoid *Arg)
{
   (tVoid)Arg;//for lint
   OEDT_HelperPrintf(TR_LEVEL_USER_1, "MyCallback called");
   //do nothing
   Check_Callback++;
}

/*****************************************************************************
* FUNCTION:     MyCallback1()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 29 Apr, 2008
*****************************************************************************/
tVoid MyCallback1(tVoid)
{
   mycallback1++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback1);
}
/*****************************************************************************
* FUNCTION:     MyCallback2()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 29 Apr, 2008
*****************************************************************************/
tVoid MyCallback2(tVoid)
{
   mycallback2++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback2);
}

/*****************************************************************************
* FUNCTION:     MyCallback3()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 29 Apr, 2008
*****************************************************************************/
tVoid MyCallback3(tVoid)
{
   mycallback3++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback3);
}

/*****************************************************************************
* FUNCTION:     MyCallback4()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 29 Apr, 2008
*****************************************************************************/

tVoid MyCallback4(tVoid)
{
   mycallback4++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback4);
}
/*****************************************************************************
* FUNCTION:     MyCallback4()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Sudharsanan Sivagnanam (RBEI/ECF5) on 3 Aug, 2012
*****************************************************************************/

tVoid MyCallback5(tVoid)
{
   mycallback5++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback5);
}
/*****************************************************************************
* FUNCTION:     MyCallback4()
* DESCRIPTION:  Gets called whenver callback is set
* HISTORY:      Sudharsanan Sivagnanam (RBEI/ECF5) on 3 Aug, 2012
*****************************************************************************/

tVoid MyCallback6(tVoid)
{
   mycallback6++;
   OEDT_HelperPrintf(TR_LEVEL_USER_1,"CALL BACK FUNC Called no: %u",mycallback6);
}


static void vGPIOOedtWorkerThread(trOedtGpioThreadParam *p_arg)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = 0;
   OSAL_trGPIOData Data = {0};
   trOedtGpioThreadParam *ptr_param = NULL;
   tInt i = 0;
   ptr_param = (trOedtGpioThreadParam*) p_arg;


   while(b_oedt_gpio_all_worker_threads_started == FALSE)
   {
      OSAL_s32ThreadWait(10);
      if (threadspawn_error == TRUE)
      {
         break;
      }
   }

   if ((ptr_param->n_thread_id >= 0) &&
         (ptr_param->n_thread_id < (tInt) OEDT_GPIO_NUM_WORKER_THREADS)
         && threadspawn_error == FALSE)
   {
      for(i=0; i < (tInt) num_loops_worker_thread; i++)
      {
         /* Open the device */
         hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);

         OSAL_s32ThreadWait(10);

         if (OSAL_ERROR == hFd)
         {
            u32Ret = 2;
            break;
         }
         else
         {
            DevID = (OSAL_tGPIODevID) ptr_param->GpioDevID;

            /* Set device to output mode */
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) DevID))
            {
               u32Ret = 3;
               OSAL_s32IOClose(hFd);
               break;
            }

            /* Set state of output pin to active */
            Data.tId = (OSAL_tGPIODevID) DevID;
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE,
                 (tS32) &Data))
            {
               u32Ret = 4;
               OSAL_s32IOClose(hFd);
               break;
            }

            /* Close the device */
            if (OSAL_ERROR == OSAL_s32IOClose(hFd))
            {
               u32Ret = 5;
               break;
            }
         } /*if (OSAL_ERROR == hFd){*/

      } /*for(i=0;i<num_loops_worker_thread;i++){*/

      if (u32Ret != 0)
      {
         b_oedt_gpio_worker_thread_error_occured[ptr_param->n_thread_id]
                                                  = TRUE;
      }
      else
      {
         b_oedt_gpio_worker_thread_error_occured[ptr_param->n_thread_id]
                                                  = FALSE;
      }
   }
   b_oedt_gpio_worker_thread_terminated[ptr_param->n_thread_id] = TRUE;
   return;
}


/*****************************************************************************
* FUNCTION:     u32GPIODevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_003
* DESCRIPTION:  1. Open Device
*               2. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevOpenClose(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device in readwrite mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else if (OSAL_ERROR == OSAL_s32IOClose(hFd))
   {
      u32Ret = 2;
   }
   else
   {
      /*success*/
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_005
* DESCRIPTION:  1. Open Device
*               2. Close Device
*               3. Attempt to Close the GPIO device which has already been
*               closed (should fail)
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevCloseAlreadyClosed(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   /* Close the device */
   if (OSAL_ERROR == OSAL_s32IOClose(hFd))
   {
      u32Ret = 2;
   }

   /* Close the device which is already closed, if successful indicate
   error*/
   if (OSAL_ERROR != OSAL_s32IOClose(hFd))
   {
      u32Ret = 3;
   }
   else
   {
      /*success*/
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_006
* DESCRIPTION:  1. Open Device
*               2. Attempt to open the GPIO device which has already been
*                opened (should fail)
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Modified by Shilpa Bhat (RBIN/ECM1) on 21 Mar, 2008
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevReOpen(tVoid)
{
   OSAL_tIODescriptor hFd1 = 0;
   OSAL_tIODescriptor hFd2 = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd1)
   {
      u32Ret = 1;
   }
   else
   {
      /* Attempt to Re-open the device
      Mutiple open is supported. So it should pass*/
      hFd2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
      if (OSAL_ERROR != hFd2)
      {
          /* If successful, Close the device */
          if (OSAL_ERROR == OSAL_s32IOClose(hFd2))
          {
              u32Ret = 2;
          }
      }
      else
      {
          /* Multiple open failed */
          u32Ret = 3;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd1))
      {
          u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevOpenCloseDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_007
* DESCRIPTION:  1. Open the GPIO device in different modes-OSAL_EN_READONLY,
*               OSAL_EN_WRITEONLY, OSAL_EN_READWRITE
*               2. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevOpenCloseDiffModes(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Here open close is allowed even if one mode
   has failed, as the errors are collected*/
   /* Open the device in readwrite mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret = 2;
      }
   }

   enAccess = OSAL_EN_READONLY;
   /* Open the device in readonly mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret += 3;
   }
   else
   {
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   enAccess = OSAL_EN_WRITEONLY;
   /* Open the device in writeonly mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret += 5;
   }
   else
   {
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 6;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevOpenCloseInvalAccessMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_008
* DESCRIPTION:  1. Attempt to Open the GPIO device with invalid access mode
*               (should fail)
*               2. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevOpenCloseInvalAccessMode(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   enAccess = (OSAL_tenAccess)OSAL_C_STRING_GPIO_INVALID_ACCESS_MODE;
   /* Open the device with invalid access mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR != hFd)
   {
      u32Ret = 1;
      /*If successfully opened, indicate an error and close the device*/
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 2;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_009
* DESCRIPTION:  1. Open the GPIO device
*               2. Attempt to perform read operation (should fail)
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevRead(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tS8 *ps8ReadBuffer = NULL;
   tS32 s32BytesRead = 0;
   tU32 u32BytesToRead = 10;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Read is not supported and so the call should return failure */
      s32BytesRead = OSAL_s32IORead(hFd, ps8ReadBuffer, u32BytesToRead);
      if (OSAL_ERROR != s32BytesRead)
      {
         u32Ret = 2;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIODevWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_010
* DESCRIPTION:  1. Open the GPIO device
*               2. Attempt to perform write operation (should fail)
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIODevWrite(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tChar DataToWrite[GPIO_MAX_LENGTH + 1] = "Test Data";
   tU32 u32BytesToWrite = 10;
   tS32 s32BytesWritten = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      s32BytesWritten = OSAL_s32IOWrite(hFd, (tS8 *)DataToWrite,
                                        u32BytesToWrite);
      /* Write is not supported and so the call should return failure */
      if (OSAL_ERROR != s32BytesWritten)
      {
         u32Ret = 2;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOGetVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_011
* DESCRIPTION:  1. Open the GPIO device
*               2. Get device version
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIOGetVersion(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tS32 s32VersionInfo = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Get device version */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_S32_IOCTRL_VERSION, (tS32)&s32VersionInfo))
      {
         u32Ret = 2;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetInput()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_012
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
static tU32 _u32GPIOSetInput(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_input_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetInput(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   return _u32GPIOSetInput();
}

tU32 u32GPIOSetInputRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   return _u32GPIOSetInput();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutput()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_013
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
******************************************************************************/
static tU32 _u32GPIOSetOutput(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 2;
   }
   else
   {
      /* Set device to output mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) DevID))
      {
         u32Ret = 3;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetOutput(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetOutput();
}

tU32 u32GPIOSetOutputRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetOutput();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetRemoveCallback()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_014
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Set callback
*               4. Remove callback
*               5. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Updated by Shilpa Bhat (RBIN/EDI3) on 11 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
tU32 u32GPIOSetRemoveCallback(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trGPIOCallbackData CbData = {0};
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }
      else
      {
         CbData.rData.tId = DevID;
         CbData.rData.unData.pfCallback = MyCallback;
         // param to pass to the callback function
         CbData.pvArg = 0;
         /* Set callback */
         if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
         {
            u32Ret = 3;
         }
         else
         {
            CbData.rData.tId = DevID;
            CbData.rData.unData.pfCallback = NULL;
            /* Remove callback  */
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
            {
               u32Ret += 4;
            }
         }
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetTrigEdgeHIGHIntEnable()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_015
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Set Callback
*               4. Set trigger edge HIGH for input mode
*               5. Enable interrupts
*               6. Disable interrupts
*               7. Remove Callback
*               8. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Updated by Shilpa Bhat (RBIN/EDI3) on 11 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 08.10.2012
******************************************************************************/
static tU32 _u32GPIOSetTrigEdgeHIGHIntEnable(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_input_1;
   OSAL_trGPIOCallbackData CbData;
   OSAL_trGPIOData Data = {0};
   Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
          u32Ret = 2;
      }
      else
      {
         // param to pass to the callback function
         CbData.rData.tId = DevID;
         CbData.rData.unData.pfCallback = MyCallback;
         CbData.pvArg = NULL;

         //param for setting the trigger
         Data.tId = DevID;
         Data.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

         /* Set callback */
         if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
         {
            u32Ret = 3;
         }
         /* Set trigger edge HIGH  */
         else if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, (tS32)&Data))
         {
            u32Ret = 4;
            CbData.rData.unData.pfCallback = NULL;
            OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
         }
         else
         {
            // param for enabling interrupt
            Data.unData.bState = TRUE;

            /* Enable interrupts */
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
            {
               u32Ret = 5;
               CbData.rData.unData.pfCallback = NULL;
               OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
            }
            else
            {
               /* wait for 5 secs */
               OSAL_s32ThreadWait(5000);

               Data.unData.bState = FALSE;
               /* Disable interrupts */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
               {
                  u32Ret = 6;
               }
               CbData.rData.unData.pfCallback = NULL;
               /* Remove callback  */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
               {
                  u32Ret += 7;
               }
            }
         }
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
          u32Ret += 8;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetTrigEdgeHIGHIntEnable(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   return _u32GPIOSetTrigEdgeHIGHIntEnable();
}

tU32 u32GPIOSetTrigEdgeHIGHIntEnableRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   return _u32GPIOSetTrigEdgeHIGHIntEnable();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetTrigEdgeLOWIntEnable()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_016
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Set Callback
*               4. Set trigger edge LOW for input mode
*               5. Enable interrupts
*               6. Disable interrupts
*               7. Remove Callback
*               8. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Updated by Shilpa Bhat (RBIN/EDI3) on 11 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 08.10.2012
*****************************************************************************/
static tU32 _u32GPIOSetTrigEdgeLOWIntEnable(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_input_1;
   OSAL_trGPIOCallbackData CbData;
   OSAL_trGPIOData Data = {0};
   Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }
      else
      {
         // param to pass to the callback function
         CbData.rData.tId = DevID;
         CbData.rData.unData.pfCallback = MyCallback;
         CbData.pvArg = NULL;

         //param for setting the trigger
         Data.tId = DevID;
         Data.unData.u16Edge = OSAL_GPIO_EDGE_LOW;

         /* Set callback */
         if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
         {
            u32Ret = 3;
         }
         /* Set trigger edge LOW  */
         else if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, (tS32)&Data))
         {
            u32Ret = 4;
            CbData.rData.unData.pfCallback = NULL;
            OSAL_s32IOControl(hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
         }
         else
         {
            // param for enabling interrupt
            Data.unData.bState = TRUE;

            /* Enable interrupts */
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
            {
               u32Ret = 5;
               CbData.rData.unData.pfCallback = NULL;
               OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
            }
            else
            {
               /* wait for 5 secs */
               OSAL_s32ThreadWait(5000);

               Data.unData.bState = FALSE;
               /* Disable interrupts */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
               {
                  u32Ret = 6;
               }
               CbData.rData.unData.pfCallback = NULL;
               /* Remove callback  */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
               {
                  u32Ret += 7;
               }
            }
         }
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
          u32Ret += 8;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetTrigEdgeLOWIntEnable(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   return _u32GPIOSetTrigEdgeLOWIntEnable();
}

tU32 u32GPIOSetTrigEdgeLOWIntEnableRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   return _u32GPIOSetTrigEdgeLOWIntEnable();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetStateLOW()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_017
* DESCRIPTION:  1. Create Device
*               2. Open the GPIO device
*               3. Set device to output mode
*               4. Set state of output pin to LOW
*               5. Close Device
*               6. Remove Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOSetStateLOW(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   /* Set state of output pin to LOW */
   OSAL_trGPIOData Data = {DevID, FALSE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   /* Set device to output mode */
   else
   {
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 3;
         errorcode = OSAL_u32ErrorCode(); 
      }
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                   OSAL_C_32_IOCTRL_GPIO_SET_STATE,(tS32)&Data) )
      {
         errorcode = OSAL_u32ErrorCode(); 
                                
        /*OSAL_C_32_IOCTRL_GPIO_SET_STATE Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 4;
        }   
      }
      else
      {
         /*sucess*/
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
     
      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetStateHIGH()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_018
* DESCRIPTION:  1. Create Device
*               2. Open the GPIO device
*               3. Set device to output mode
*               4. Set state of output pin to HIGH
*               5. Close Device
*               6. Remove Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOSetStateHIGH(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   /* Set state of output pin to HIGH */
   OSAL_trGPIOData Data = {DevID, TRUE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   /* Set device to output mode */
   else
   {
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 3;
         errorcode = OSAL_u32ErrorCode();
      }
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                   OSAL_C_32_IOCTRL_GPIO_SET_STATE,(tS32)&Data) )
      {
         errorcode = OSAL_u32ErrorCode(); 
                                
        /*OSAL_C_32_IOCTRL_GPIO_SET_STATE Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 4;
        } 
      }
      else
      {
         /*sucess*/
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOGetState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_019
* DESCRIPTION:  1. Create Device
*               2. Open the GPIO device
*               3. Set device to input mode
*               4. Get state of input pin
*               5. Close Device
*               6. Remove Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOGetState(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   OSAL_trGPIOData Data = {DevID, TRUE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   else
   {
      /* Set device to input mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_INPUT,(tS32) DevID) )
      {
         u32Ret = 3;
         errorcode = OSAL_u32ErrorCode();
      }
      /* Get state of input pin */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                   OSAL_C_32_IOCTRL_GPIO_GET_STATE,(tS32)&Data) )
      {
         
         errorcode = OSAL_u32ErrorCode(); 
                                
        /*OSAL_C_32_IOCTRL_GPIO_GET_STATE Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 4;
        } 
      }
      else
      {
         /*success*/
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"State: %d",
         Data.unData.bState);
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetStateInactive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_017
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode
*               3. Set state of output pin to inactive
*               4. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Renamed/adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
*****************************************************************************/
static tU32 _u32GPIOSetStateInactive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;
   OSAL_trGPIOData Data = {DevID, FALSE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   /* Set device to output mode */
   else
   {
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_INACTIVE_STATE, (tS32) &Data))
      {
         u32Ret = 3;
      }
      else
      {
         /*success*/
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetStateInactive(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetStateInactive();
}

tU32 u32GPIOSetStateInactiveRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetStateInactive();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetStateActive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_018
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode
*               3. Set state of output pin to active
*               4. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Renamed/adapted by Matthias Thomae (CM-AI/PJ-CF31) - 19.07.2012
*****************************************************************************/
static tU32 _u32GPIOSetStateActive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;
   OSAL_trGPIOData Data = {DevID, FALSE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   /* Set device to output mode */
   else
   {
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE, (tS32) &Data))
      {
         u32Ret = 3;
      }
      else
      {
         /*success*/
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetStateActive(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetStateActive();
}

tU32 u32GPIOSetStateActiveRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetStateActive();
}


/*****************************************************************************
* FUNCTION:     u32GPIOIsStateActive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_019
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Get state of input pin
*               4. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Renamed/adapted by Matthias Thomae (CM-AI/PJ-CF31) - 18.07.2012
*****************************************************************************/
static tU32 _u32GPIOIsStateActive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_input_1;
   OSAL_trGPIOData Data = {DevID, TRUE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 2;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
         u32Ret = 3;
      }
      /* Get state of input pin */
      else if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE, (tS32) &Data))
      {
         u32Ret = 4;
      }
      else
      {
         /* success */
          OEDT_HelperPrintf(TR_LEVEL_USER_1, "State: %d", Data.unData.bState);
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 5;
      }
   }

   return u32Ret;
}

tU32 u32GPIOIsStateActive(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   return _u32GPIOIsStateActive();
}

tU32 u32GPIOIsStateActiveRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   return _u32GPIOIsStateActive();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputHIGH()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_020
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state HIGH
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOSetOutputHIGH(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state HIGH */
      if ( OSAL_ERROR == OSAL_s32IOControl(hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH, (tS32) DevID) )
      {
        errorcode = OSAL_u32ErrorCode(); 
        /*OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 2;
        } 
      }
      else
      {
         /*success*/
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputLOW()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_021
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state LOW
*               3. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 20.07.2012
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOSetOutputLOW(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state LOW */
      if ( OSAL_ERROR == OSAL_s32IOControl(hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW, (tS32) DevID) )
      {
        errorcode = OSAL_u32ErrorCode(); 
        /*OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 2;
        }
      }
      else
      {
         /*success*/
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputHIGHLOW()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_022
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state HIGH
*               3. Set device to output mode with initial state LOW
*               4. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 25.07.2012
* 17.03.2016 -  Modified for unsupported IOControls - Sadhna Sharma(RBEI/ECF5)
*****************************************************************************/
tU32 u32GPIOSetOutputHIGHLOW(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 errorcode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state HIGH */
      if ( OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH, (tS32) DevID) )
      {
         errorcode = OSAL_u32ErrorCode(); 
         /*OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret = 2;
        }
      }

      /* Set device to output mode with initial state LOW */
      if ( OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW, (tS32) DevID) )
      {
        errorcode = OSAL_u32ErrorCode(); 
        /*OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW Iocontrol is not supported and expected to return OSAL_E_NOTSUPPORTED */
        if (errorcode != OSAL_E_NOTSUPPORTED)
        {
           u32Ret += 3;
        }
      }
      if (errorcode != OSAL_E_NOERROR)
      {
         OEDT_HelperPrintf(TR_LEVEL_USER_4, "Errorcode =  %lu",
         OSAL_u32ErrorCode());
      }
      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputActive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_0XX
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state ACTIVE
*               3. Close Device
* HISTORY:      Created by Matthias Thomae (CM-AI/PJ-CF31) - 09.10.2012
*****************************************************************************/
static tU32 _u32GPIOSetOutputActive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state ACTIVE */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) DevID))
      {
         u32Ret = 2;
      }
      else
      {
         /*success*/
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetOutputActive(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetOutputActive();
}

tU32 u32GPIOSetOutputActiveRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetOutputActive();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputInactive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_0XX
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state INACTIVE
*               3. Close Device
* HISTORY:      Created by Matthias Thomae (CM-AI/PJ-CF31) - 09.10.2012
*****************************************************************************/
static tU32 _u32GPIOSetOutputInactive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state INACTIVE */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) DevID))
      {
         u32Ret = 2;
      }
      else
      {
          /*success*/
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetOutputInactive(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetOutputInactive();
}

tU32 u32GPIOSetOutputInactiveRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetOutputInactive();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputActiveInactive()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_0XX
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to output mode with initial state ACTIVE
*               3. Set device to output mode with initial state INACTIVE
*               4. Close Device
* HISTORY:      Created by Matthias Thomae (CM-AI/PJ-CF31) - 09.10.2012
*****************************************************************************/
static tU32 _u32GPIOSetOutputActiveInactive(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_output_1;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to output mode with initial state ACTIVE */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE, (tS32) DevID))
      {
         u32Ret = 2;
      }

      /* Set device to output mode with initial state INACTIVE */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE, (tS32) DevID))
      {
         u32Ret += 3;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetOutputActiveInactive(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   return _u32GPIOSetOutputActiveInactive();
}

tU32 u32GPIOSetOutputActiveInactiveRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   return _u32GPIOSetOutputActiveInactive();
}

/*****************************************************************************
* FUNCTION:     u32GPIOSetOutputInInputMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_023
* DESCRIPTION:  1. Open the GPIO device
*               2. Set device to input mode
*               3. Set state of input pin to HIGH -> fail
*               4. Close Device
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 25.07.2012
*****************************************************************************/
static tU32 _u32GPIOSetOutputInInputMode(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = gpio_input_1;
   OSAL_trGPIOData Data = {DevID, FALSE};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
         u32Ret = 2;
      }
      /* try to set state of input pin -> should fail */
      else if (OSAL_ERROR != OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE, (tS32) &Data))
      {
         u32Ret = 3;
      }
      else
      {
          /*success*/
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 4;
      }
   }

   return u32Ret;
}

tU32 u32GPIOSetOutputInInputMode(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   return _u32GPIOSetOutputInInputMode();
}

tU32 u32GPIOSetOutputInInputModeRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   return _u32GPIOSetOutputInInputMode();
}

/*****************************************************************************
* FUNCTION:     u32GPIOInvalFuncParamToIOCtrl()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_024
* DESCRIPTION:  Attempt to perform IOCtrl operation with invalid parameter
*               (should fail)
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 03 Oct, 2007 
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 26.07.2012
*****************************************************************************/
tU32 u32GPIOInvalFuncParamToIOCtrl(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32EnableTM = 0;

   /* Open the device in readwrite mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* IOCtrl operation with invalid parameter */
      if (OSAL_ERROR != OSAL_s32IOControl(hFd,
            OSAL_C_S32_GPIO_IOCTRL_INVAL_FUNC, (tS32)&u32EnableTM))
      {
         u32Ret = 2;
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
         u32Ret += 3;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOMultiThreadTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_025
* DESCRIPTION:  Attempt to perform multi thread operation
* HISTORY:      Created by Resch Carsten
*               Adapted by Matthias Thomae (CM-AI/PJ-CF31) - 26.07.2012
*****************************************************************************/
static tU32 _u32GPIOMultiThreadTest(tVoid)
{
   tU32 u32_ret = 0;
   tChar sz_base_name[NO_CHARS_IN_STRING] = "oedt_t";
   tChar sz_name[NO_CHARS_IN_STRING]      = "";
   tS16 i = 0;
   tInt n_wait_cnt = 0;
   OSAL_trThreadAttribute tr_thread_attr;
   OSAL_tThreadID osal_thread_id[OEDT_GPIO_NUM_WORKER_THREADS];
   threadspawn_error = FALSE;
   b_oedt_gpio_all_worker_threads_started = FALSE;

   for(i=0;i<(tS16)OEDT_GPIO_NUM_WORKER_THREADS;i++)
   {
      b_oedt_gpio_worker_thread_error_occured[i] = TRUE;
      b_oedt_gpio_worker_thread_terminated[i]    = FALSE;
   }

   for(i=0; i < (tS16)OEDT_GPIO_NUM_WORKER_THREADS; i++)
   {
      snprintf(&sz_name[0], sizeof(sz_name), "%s%d", sz_base_name, i);
      tr_oedt_gpio_worker_params[i].n_thread_id = i;

      switch(i)
      {
      case(0):
         tr_oedt_gpio_worker_params[i].GpioDevID = gpio_output_1;
         break;
      case(1):
         tr_oedt_gpio_worker_params[i].GpioDevID = gpio_output_2;
         break;
      case(2):
         tr_oedt_gpio_worker_params[i].GpioDevID = gpio_output_3;
         break;
      default:
         u32_ret = 1;
         break;
      }

      tr_thread_attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      tr_thread_attr.s32StackSize= 2048;
      tr_thread_attr.szName      = sz_name;
      /* Startroutine */
      tr_thread_attr.pfEntry     = (OSAL_tpfThreadEntry)vGPIOOedtWorkerThread;
      /* Argument der Startroutine */
      tr_thread_attr.pvArg       = &tr_oedt_gpio_worker_params[i];

      osal_thread_id[i] = OSAL_ThreadSpawn(&tr_thread_attr);
      if(osal_thread_id[i] == OSAL_ERROR)
      {
         u32_ret = 2;
         break;
      }
   }

   b_oedt_gpio_all_worker_threads_started = TRUE;
   if(u32_ret == 0)
   {
      for(i=0; i < (tS16)OEDT_GPIO_NUM_WORKER_THREADS; i++)
      {
         n_wait_cnt = 0;
         while((b_oedt_gpio_worker_thread_terminated[i] == FALSE) &&
             (n_wait_cnt < 200))
         {
            n_wait_cnt++;
            OSAL_s32ThreadWait(100);
         }
         if(b_oedt_gpio_worker_thread_terminated[i] == FALSE)
         {
            u32_ret += 3;
         }
         if(b_oedt_gpio_worker_thread_error_occured[i] == TRUE)
         {
            u32_ret += 4;
         }

         OSAL_s32ThreadDelete(osal_thread_id[i]);
      }
   }
   else
   {
      threadspawn_error = TRUE;
      /*wait till the threads are exited gracefully*/
      OSAL_s32ThreadWait(100);
      /*if 3rd time thread spawing fails, then we start deleting from
      the previous one, i.e, 2*/
      i = i -1;
      /*delete the threads that were successfully spawned*/
      for(;i >= 0;i--)
      {
         OSAL_s32ThreadDelete(osal_thread_id[i]);
      }
   }

   return u32_ret;
}

tU32 u32GPIOMultiThreadTest(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1;
   gpio_output_2 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_2;
   gpio_output_3 = OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_3;
   num_loops_worker_thread = 1000;
   return _u32GPIOMultiThreadTest();
}

tU32 u32GPIOMultiThreadTestRemote(tVoid)
{
   gpio_output_1 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1;
   gpio_output_2 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_2;
   gpio_output_3 = OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_3;
   num_loops_worker_thread = 200;
   return _u32GPIOMultiThreadTest();
}


/*****************************************************************************
* FUNCTION:     u32GPIOTwoInputPins()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_029
* DESCRIPTION:  1. Open the GPIO device
*               2. Set both pins to input mode
*               3. Register callback for first input pin
*               4. Set trigger for first input pin
*               5. Enable interrupt for first input pin
*               6. Register callback for second input pin
*               7. Set trigger for second input pin
*               8. Enable interrupt for second input pin
*               9. Disable interrupt for first input pins
*               10. Unregister callback for first input pin
*               11. Disable interrupt for second input pins
*               12. Unregister callback for second input pin
*               13. Close Device
* HISTORY:      Created by Matthias Thomae (CM-AI/PJ-CF31) - 07.08.2012
*****************************************************************************/
static tU32 _u32GPIOTwoInputPins(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tGPIODevID DevID1 = gpio_input_1;
    OSAL_tGPIODevID DevID2 = gpio_input_2;
    OSAL_trGPIOCallbackData CbData;
    OSAL_trGPIOData Data = {0};
    Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      return 1;
   }

   /* Set device 1 to input mode */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID1))
   {
      return 2;
   }
   /* Set device 2 to input mode */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID2))
   {
      return 2;
   }

   // param to pass to the callback function
   CbData.rData.unData.pfCallback = MyCallback;
   CbData.pvArg = NULL;

   //param for setting the trigger
   Data.unData.u16Edge = OSAL_GPIO_EDGE_BOTH;
   Data.tId = DevID1;
   CbData.rData.tId = DevID1;

   /* Set callback on device 1 */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData))
   {
      return 3;
   }

   /* Set trigger on device 1 */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 4;
   }

   /* Enable interrupt on device 1 */
   Data.unData.bState = TRUE;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 5;
   }

   /* wait for 5 secs */
   OSAL_s32ThreadWait(5000);

   //param for setting the trigger
   Data.unData.u16Edge = OSAL_GPIO_EDGE_BOTH;
   Data.tId = DevID2;
   CbData.rData.tId = DevID2;

   /* Set callback on device 2 */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData))
   {
      return 6;
   }

   /* Set trigger on device 2 */
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 7;
   }

   /* Enable interrupt on device 2 */
   Data.unData.bState = TRUE;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 8;
   }

   /* wait for 5 secs */
   OSAL_s32ThreadWait(5000);

   /* Disable interrupt on device 1 */
   Data.unData.bState = FALSE;
   Data.tId = DevID1;
   CbData.rData.tId = DevID1;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 9;
   }

   /* Remove callback on device 1 */
   CbData.rData.unData.pfCallback = NULL;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData))
   {
       return 10;
   }

   /* wait for 5 secs */
   OSAL_s32ThreadWait(5000);

   /* Disable interrupt on device 2 */
   Data.unData.bState = FALSE;
   Data.tId = DevID2;
   CbData.rData.tId = DevID2;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32) &Data))
   {
      CbData.rData.unData.pfCallback = NULL;
      OSAL_s32IOControl(hFd,
            OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData);
      return 11;
   }

   /* Remove callback on device 2 */
   CbData.rData.unData.pfCallback = NULL;
   if (OSAL_ERROR == OSAL_s32IOControl(hFd,
         OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32) &CbData))
   {
       return 12;
   }

   /* Close the device */
   if( OSAL_ERROR == OSAL_s32IOClose(hFd))
   {
      return 13;
   }

   return 0;
}

tU32 u32GPIOTwoInputPins(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   gpio_input_2 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_2;
   return _u32GPIOTwoInputPins();
}

tU32 u32GPIOTwoInputPinsRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   gpio_input_2 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_2;
   return _u32GPIOTwoInputPins();
}

/*****************************************************************************
* FUNCTION:     u32GPIOMultiProcessTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_030
* DESCRIPTION:  Access DRV GPIO from multiple processes
* HISTORY:      Created by Matthias Thomae (CM-AI/PJ-CF31) - 16.08.2012
*****************************************************************************/
tU32 _u32GPIOMultiProcessTest(tVoid)
{
   OSAL_tMQueueHandle mqHandleR = OSAL_C_INVALID_HANDLE;
   OSAL_trProcessAttribute prAtr = {OSAL_NULL};
   OSAL_tProcessID prID_1 = INVALID_PID;
   OSAL_tProcessID prID_2 = INVALID_PID;
   tChar sGpioInput1[U32_MAX_DECIMALS];
   tChar sGpioInput2[U32_MAX_DECIMALS];
   tU32 u32Ret = 0;
   tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";

   snprintf(sGpioInput1, U32_MAX_DECIMALS, "%d", gpio_input_1);
   snprintf(sGpioInput2, U32_MAX_DECIMALS, "%d", gpio_input_2);

   // Create or open MQ for both processes to send their responses
   if (OSAL_s32MessageQueueCreate(MQ_GPIO_MP_RESPONSE_NAME,
         MQ_GPIO_MP_CONTROL_MAX_MSG, MQ_GPIO_MP_CONTROL_MAX_LEN,
         OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR)
   {
      if (OSAL_s32MessageQueueOpen(MQ_GPIO_MP_RESPONSE_NAME,
            OSAL_EN_READONLY, &mqHandleR) == OSAL_ERROR)
      {
         return 1000;
      }
   }

   prAtr.szName         = "procgpiop1_out.out";
   OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
   prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
   prAtr.u32Priority    = PRIORITY;
   prAtr.szCommandLine  = sGpioInput1;
   prID_1 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_1)
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "proc1 spawn failed: %lu",
            OSAL_u32ErrorCode());
      return 1001;
   }

   prAtr.szName         = "procgpiop2_out.out";
   OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
   prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
   prAtr.u32Priority    = PRIORITY;
   prAtr.szCommandLine  = sGpioInput2;
   prID_2 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_2)
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "proc2 spawn failed: %lu",
            OSAL_u32ErrorCode());
      return 1002;
   }

   // Wait for processes to respond
   // TODO: All OSAL_s32MessageQueueWait use OSAL_C_TIMEOUT_FOREVER.
   //       So if one process gets stuck, the whole thing hangs
   u32Ret += u32GPIOGetProcessResponse(mqHandleR);
   u32Ret += u32GPIOGetProcessResponse(mqHandleR);

   return u32Ret;
}

tU32 u32GPIOMultiProcessTest(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_1;
   gpio_input_2 = OSAL_EN_TEST_GPIO_INPUT_LOCAL_2;
   return _u32GPIOMultiProcessTest();
}

tU32 u32GPIOMultiProcessTestRemote(tVoid)
{
   gpio_input_1 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_1;
   gpio_input_2 = OSAL_EN_TEST_GPIO_INPUT_REMOTE_2;
   return _u32GPIOMultiProcessTest();
}



/* tma2hi:
 * the following 3 tests (u32GPIOCallbackTest, u32GPIOCallbackRegOutputMode,
 * u32GPIOCallbackRegUnReg) rely on that interrupts can be enabled for output
 * pins, and that toggling the output pin state will then trigger interrupts.
 * The Linux GPIO Sysfs interface doesn't seem to support this, at least not
 * on the i.MX6. Hence these tests are not supported on the target for now.
 */
/*****************************************************************************
* FUNCTION:     u32GPIOCallbackTestCommon()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check callback functionality
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*-----------------------------------------------------------------------------
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*-----------------------------------------------------------------------------
*               Update by Madhu Kiran Ramachandra (RBEI/ECF5) on 3d april 2012
*               Modified function u32GPIOCallbackTestCommon to test
*               callback registration feature for Positive, negative
*               and Both together.
******************************************************************************/
tU32 u32GPIOCallbackTestCommon(OSAL_tGPIODevID GPIOpin,tU16 u16Trigger)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 u32Cnt = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = GPIOpin;
   OSAL_trGPIOData Data = {0};
   OSAL_trGPIOCallbackData CbData = {0};
   Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 1;
   }
   else
   {
      //callback arguments
      CbData.rData.tId = GPIOpin;
      CbData.rData.unData.pfCallback = MyCallback;
      CbData.pvArg = NULL;

      //set trigger arguments
      Data.tId = GPIOpin;
      Data.unData.u16Edge = u16Trigger;

      /* Set device to output mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 2;
      }
      /* Set callback */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CbData) )
      {
         u32Ret = 4;
      }
      /* Set trigger edge HIGH */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER,(tS32)&Data) )
      {
         u32Ret = 8;
      }
      else
      {
         //for disaling the interrupt
         Data.unData.bState = FALSE;
         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
         {
            u32Ret = 16;
         }
         else
         {
            /* Enable interrupts */
            Data.unData.bState = TRUE;
            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret = 32;
            }

            for(;(u32Cnt < NO_CALLBACKS_CALLED)
            && (u32Ret == 0); u32Cnt++)
            {
               /* Set state of output pin to HIGH */
               Data.tId = GPIOpin;
               Data.unData.bState = TRUE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                        (tS32)Data.tId) )
               {
                  u32Ret += 64;
               }
               (tVoid)OSAL_s32ThreadWait(100);

               /* Set state of output pin to LOW */
               Data.tId = GPIOpin;
               Data.unData.bState = FALSE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,
                        (tS32)Data.tId) )
               {
                  u32Ret += 128;
               }

               (tVoid)OSAL_s32ThreadWait(100);

            }

            switch(u16Trigger)
            {
            case OSAL_GPIO_EDGE_HIGH:
               {
                  if(Check_Callback != NO_CALLBACKS_CALLED)
                  {
                     u32Ret += 256;
                  }
                  break;
               }
            case OSAL_GPIO_EDGE_LOW:
               {
                  if(Check_Callback != NO_CALLBACKS_CALLED)
                  {
                     u32Ret += 256;
                  }
                  break;
               }
            case OSAL_GPIO_EDGE_BOTH:
               {
                  if(Check_Callback != (NO_CALLBACKS_CALLED)*2)
                  {
                     u32Ret += 256;
                  }
                  break;
               }
            default:
               u32Ret += 256;
               break;
            }

            /* Disable interrupts */
            Data.unData.bState = FALSE;
            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret += 512;
            }
         }
      }

      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 1024;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOCallbackTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_026
* DESCRIPTION:  Check callback functionality 
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*-----------------------------------------------------------------------------
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*-----------------------------------------------------------------------------
*               Update by Madhu Kiran Ramachandra (RBEI/ECF5) on 3d april 2012
*               Modified function u32GPIOCallbackTestCommon to test
*               callback registration feature for Positive, negative
*               and Both together.
*****************************************************************************/
tU32 u32GPIOCallbackTest(tVoid)
{
   tU32 u32Ret = 0;
   tU32 u32ErrVal= 0;
   /*Check the Board name and call
"u32GPIOCallbackTestCommon()" with HW specific pin*/

   /* GMGE */
   u32ErrVal = u32GPIOCallbackTestCommon((OSAL_tGPIODevID)OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1, OSAL_GPIO_EDGE_LOW);
   if(0 != u32ErrVal)
   {
      u32Ret += 1;
      OEDT_HelperPrintf(TR_LEVEL_ERRORS,"Check Call back for OSAL_GPIO_EDGE_LOW Failed Error:%u",u32ErrVal);

   }

   u32ErrVal = u32GPIOCallbackTestCommon((OSAL_tGPIODevID)OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1, OSAL_GPIO_EDGE_HIGH);
   if(0 != u32ErrVal)
   {
      u32Ret += 2;
      OEDT_HelperPrintf(TR_LEVEL_ERRORS,"Check Call back for OSAL_GPIO_EDGE_HIGH Failed Error:%u",u32ErrVal);

   }

   u32ErrVal = u32GPIOCallbackTestCommon((OSAL_tGPIODevID)OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1, OSAL_GPIO_EDGE_BOTH);
   if(0 != u32ErrVal)
   {
      u32Ret += 4;
      OEDT_HelperPrintf(TR_LEVEL_ERRORS,"Check Call back for OSAL_GPIO_EDGE_BOTH Failed Error:%u",u32ErrVal);

   }

   return u32Ret ;

}

/*****************************************************************************
* FUNCTION:     u32GPIOCallbackRegOutputModeCommon()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check callback functionality in output mode
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tU32 u32GPIOCallbackRegOutputModeCommon(OSAL_tGPIODevID GPIOpin)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   tU32 u32Cnt = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = GPIOpin;
   OSAL_trGPIOData Data = {0};
   OSAL_trGPIOCallbackData CbData;
   Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   else
   {
      //callback arguments
      CbData.rData.tId = DevID;
      CbData.rData.unData.pfCallback = MyCallback;
      CbData.pvArg = NULL;

      //set trigger arguments
      Data.tId = DevID;
      Data.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

      /* Set device to output mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 3;
      }
      /* Set callback */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CbData) )
      {
         u32Ret = 4;
      }
      /* Set trigger edge high */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER,(tS32)&Data) )
      {
         u32Ret = 5;
      }
      else
      {
         //for disaling the interrupt
         Data.unData.bState = FALSE;
         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
         {
            u32Ret = 6;
         }
         else
         {
            /* Enable interrupts */
            Data.unData.bState = TRUE;
            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret = 7;
            }

            for(;(u32Cnt < NO_CALLBACKS_CALLED)
            && (u32Ret == 0); u32Cnt++)
            {
               /* Set state of output pin to HIGH */
               Data.tId = GPIOpin;
               Data.unData.bState = TRUE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,(tS32)Data.tId) )
               {
                  u32Ret += 8;
               }
               OSAL_s32ThreadWait(100);

               /* Set state of output pin to LOW */
               Data.tId = GPIOpin;
               Data.unData.bState = FALSE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,(tS32)Data.tId) )
               {
                  u32Ret += 9;
               }

               OSAL_s32ThreadWait(100);

            }
         }
      }

      if(Check_Callback != u32Cnt)
      {
         u32Ret += 10;
      }

      /* Disable interrupts */
      Data.unData.bState = FALSE;
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
      {
         u32Ret += 11;
      }

      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 12;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOCallbackRegOutputMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_027
* DESCRIPTION:  Check callback functionality in output mode
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tU32 u32GPIOCallbackRegOutputMode(tVoid)
{
   tU32 u32Ret = 0;


   u32Ret = u32GPIOCallbackRegOutputModeCommon
   (
   (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1
   );

   return u32Ret ;

}

/*****************************************************************************
* FUNCTION:     u32GPIOCallbackRegUnRegCommon()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check callback functionality registration and unregistration
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Modified by Shilpa Bhat (RBEI/ECM1) on 7 May, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tU32 u32GPIOCallbackRegUnRegCommon(OSAL_tGPIODevID GPIOpin)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = GPIOpin;
   OSAL_trGPIOData Data= {0};
   OSAL_trGPIOCallbackData CbData = {0};
   Check_Callback = 0;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   else
   {
      //callback arguments
      CbData.rData.tId = DevID;
      CbData.rData.unData.pfCallback = MyCallback;
      CbData.pvArg = NULL;

      //parameter for set trigger
      Data.tId = DevID;
      Data.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

      /* Set device to output mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 3;
      }
      /* Set callback */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CbData) )
      {
         u32Ret = 4;
      }
      /* Set trigger edge LOW */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER,(tS32)&Data) )
      {
         u32Ret = 5;
      }
      else
      {
         //parameter for disabling interrupts
         Data.unData.bState = FALSE;
         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
         {
            u32Ret = 6;
         }
         else
         {
            /* Enable interrupts */
            Data.unData.bState = TRUE;
            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret = 7;
            }
            else
            {

               /* Set state of output pin to HIGH */
               Data.unData.bState = TRUE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                        (tS32)Data.tId) )
               {
                  u32Ret = 8;
               }

               OSAL_s32ThreadWait(100);

               /* Set state of output pin to LOW */
               Data.unData.bState = FALSE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,
                        (tS32)Data.tId) )
               {
                  u32Ret += 9;
               }
               OSAL_s32ThreadWait(100);
            }
         }
      }

      if (Check_Callback != 1)
      {
         u32Ret += 10;
      }
      else
      {

         /* Unregister Callback */
         CbData.rData.tId = GPIOpin;
         CbData.rData.unData.pfCallback = OSAL_NULL;
         CbData.pvArg = NULL;

         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CbData) )
         {
            u32Ret += 11;
         }
         else
         {
            Check_Callback = 0;

            /* Disable Interrupts */
            Data.tId = GPIOpin;
            Data.unData.bState = FALSE;

            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret += 12;
            }
            else
            {
               /*check if callback function is being called after
               unregistration*/
               /* Set state of output pin to HIGH */
               Data.tId = GPIOpin;
               Data.unData.bState = TRUE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                        (tS32)Data.tId) )
               {
                  u32Ret += 13;
               }
               OSAL_s32ThreadWait(100);

               /* Set state of output pin to LOW */
               Data.tId = GPIOpin;
               Data.unData.bState = FALSE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,
                        (tS32)Data.tId) )
               {
                  u32Ret += 14;
               }

               OSAL_s32ThreadWait(100);
            }

            /* Check if callback function has been called */
            if (Check_Callback != 0)
            {
               /* If yes, report error */
               u32Ret += 15;
            }
         }
      }

      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 16;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOCallbackRegUnReg()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_028
* DESCRIPTION:  Check callback functionality registration and unregistration
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Modified by Shilpa Bhat (RBEI/ECM1) on 7 May, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tU32 u32GPIOCallbackRegUnReg(tVoid)
{
   tU32 u32Ret = 0;


   u32Ret = u32GPIOCallbackRegUnRegCommon
   (
   (OSAL_tGPIODevID) OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1
   );

   return u32Ret ;

}

/*****************************************************************************
* FUNCTION:     u32GPIOThreadCallbackRegCommon()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check callback functionality with multiple pins
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*               Update by sudharsanan sivagnanam on 3 Aug, 2012
*               Modified for lint fix by Bhagyashree Chidanand Burji (RBEI/ECF5) on 12 Nov,2015
*****************************************************************************/
tU32 u32GPIOThreadCallbackRegCommon
(
OSAL_tGPIODevID GPIOpin1,
OSAL_tGPIODevID GPIOpin2,
OSAL_tGPIODevID GPIOpin3,
OSAL_tGPIODevID GPIOpin4,
OSAL_tGPIODevID GPIOpin5,
OSAL_tGPIODevID GPIOpin6
)
{
   tChar sz_base_name[NO_CHARS_IN_STRING] = "test_t";
   tChar sz_name[NO_CHARS_IN_STRING]      = "";
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID GPIOpin[OEDT_NO_GPIO_BANKS] = {  GPIOpin1,
      GPIOpin2,
      GPIOpin3,
      GPIOpin4,
      GPIOpin5,
      GPIOpin6
   };
   OSAL_tThreadID rGPIOthreadID[OEDT_NO_GPIO_BANKS] = {0};
   OSAL_trThreadAttribute thrAtr[OEDT_NO_GPIO_BANKS] = {0};
   tS32 s32cnt = 0;

   threadspawn_error = FALSE;
   b_oedt_gpio_all_worker_threads_started = FALSE;
   mycallback1 = 0; mycallback2 = 0; mycallback3 = 0; mycallback4 = 0;mycallback5 = 0;mycallback6 = 0;

   memset(tr_oedt_gpio_worker_params, 0,
   (sizeof(trOedtGpioThreadParam) * OEDT_NO_GPIO_BANKS));

   /* Open the device */
   Fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == Fd )
   {
      u32Ret = 2;
   }
   else
   {

      /* Create Event */
      if( OSAL_ERROR == OSAL_s32EventCreate("GPIO_OEDT_Event",
               &evHandle ) )
      {
         u32Ret = 3;
      }
      else
      {

         for(s32cnt = 0; s32cnt < OEDT_NO_GPIO_BANKS; s32cnt++)
         {
            snprintf(&sz_name[0], sizeof(sz_name),
            "%s%d", sz_base_name, s32cnt);
            tr_oedt_gpio_worker_params[s32cnt].n_thread_id = s32cnt;
            tr_oedt_gpio_worker_params[s32cnt].GpioDevID =
            GPIOpin[s32cnt];
            /* Initialize thread attributes */
            thrAtr[s32cnt].u32Priority =
            OSAL_C_U32_THREAD_PRIORITY_NORMAL;
            thrAtr[s32cnt].s32StackSize = 2048;
            thrAtr[s32cnt].pfEntry=
            (OSAL_tpfThreadEntry)OEDT_GPIO_Thread;

            thrAtr[s32cnt].pvArg = &tr_oedt_gpio_worker_params[s32cnt];
            thrAtr[s32cnt].szName = sz_name;

            /*Spawn the Thread*/
            rGPIOthreadID[s32cnt] = OSAL_ThreadSpawn(&thrAtr[s32cnt]);
            if(rGPIOthreadID[s32cnt] == OSAL_ERROR)
            {
               u32Ret = 4;
               break;
            }
         }

         b_oedt_gpio_all_worker_threads_started = TRUE;
         if(u32Ret == 0)
         {
            if(OSAL_s32EventWait( evHandle,
                     EveMask,
                     OSAL_EN_EVENTMASK_AND,
                     OSAL_C_TIMEOUT_FOREVER, OSAL_NULL)
                  == OSAL_ERROR)
            {
               u32Ret = 5;
            }
            else if((mycallback1 == NO_CALLBACKS_CALLED)&&
                  (mycallback2 == NO_CALLBACKS_CALLED)&&
                  (mycallback3 == NO_CALLBACKS_CALLED)&&
                  (mycallback4 == NO_CALLBACKS_CALLED)&&
                  (mycallback5 == NO_CALLBACKS_CALLED)&&
                  (mycallback6 == NO_CALLBACKS_CALLED))
            {
               /*do nothing. It is success*/
            }
            else
            {
               u32Ret = 6;
            }

            /*Delete Threads created*/
            for(s32cnt = 0; s32cnt < OEDT_NO_GPIO_BANKS; s32cnt++)
            {
               OSAL_s32ThreadDelete( rGPIOthreadID[s32cnt] ) ;
            }
         }
         else
         {
            /*delete the spawned tasks*/
            threadspawn_error = TRUE;
            OSAL_s32ThreadWait(100);
            /*if 3rd time thread spawing fails, then we start
            deleting from the previous one, i.e, 2*/
            s32cnt = s32cnt -1;
            /*delete the threads that were successfully spawned*/
            for(;s32cnt >= 0; s32cnt--)
            {
               OSAL_s32ThreadDelete(rGPIOthreadID[s32cnt]);
            }

         }

         /*Shutdown the event - close and delete*/
         if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
         {
            /*Event close failed*/
            u32Ret = 7;
         }
         else if( OSAL_ERROR == OSAL_s32EventDelete("GPIO_OEDT_Event"))
         {
            /*Delete event failed*/
            u32Ret = 8;
         }
         else
         {
            /*do nothing*/
         }
      }

      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( Fd ) )
      {
         u32Ret += 9;
      }
   }
   /*TODO: add prints for errors in each thread*/
   u32Ret += u32RetTr[0] + u32RetTr[1] + u32RetTr[2] + u32RetTr[3]+u32RetTr[4]+u32RetTr[5];
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOThreadCallbackReg()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_029
* DESCRIPTION:  Check callback functionality with multiple pins
* HISTORY:      Created by Shilpa Bhat (RBEI/ECM1) on 28th Apr, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tU32 u32GPIOThreadCallbackReg(tVoid)
{
   tU32 u32Ret = 0;

   u32Ret = u32GPIOThreadCallbackRegCommon
   (
   (OSAL_tGPIODevID) OEDT_GPIO_BANK1,
   (OSAL_tGPIODevID) OEDT_GPIO_BANK2,
   (OSAL_tGPIODevID) OEDT_GPIO_BANK3,
   (OSAL_tGPIODevID) OEDT_GPIO_BANK4,
   (OSAL_tGPIODevID) OEDT_GPIO_BANK5,
   (OSAL_tGPIODevID) OEDT_GPIO_BANK6
   );
   return u32Ret ;
}

/*****************************************************************************
* FUNCTION:     OEDT_GPIO_Thread()
* DESCRIPTION:  Used to register an GPIO pin for callback
* HISTORY:      Created by Shilpa Bhat (RBIN/EDI3) on 29 Apr, 2008
*               Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/
tVoid OEDT_GPIO_Thread(tVoid *pvArg )
{
   OSAL_trGPIOCallbackData CBData = {0};
   OSAL_trGPIOData pinData = {0};
   trOedtGpioThreadParam *prGpioThreadParam = (trOedtGpioThreadParam *)pvArg;
   OSAL_tGPIODevID GPIOpin = (OSAL_tGPIODevID)prGpioThreadParam->GpioDevID;
   tS32 s32thread_id = prGpioThreadParam->n_thread_id;
   tS32 s32EveVal = 0;
   tU32 u32Cnt = 0;
   OSAL_trGPIOData Data = {0};
   u32RetTr[s32thread_id] = 0;

   if(threadspawn_error == FALSE)
   {

      pinData.tId = GPIOpin;
      pinData.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

      /*parameters for  Set callback */
      CBData.rData.tId = GPIOpin;
      CBData.pvArg = NULL;
      switch(s32thread_id)
      {
      case 0:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback1);
         s32EveVal = EveValT0;
         break;
      case 1:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback2);
         s32EveVal = EveValT1;
         break;
      case 2:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback3);
         s32EveVal = EveValT2;
         break;
      case 3:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback4);
         s32EveVal = EveValT3;
         break;
      case 4:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback5);
         s32EveVal = EveValT4;
         break;
      case 5:
         CBData.rData.unData.pfCallback =
         (OSAL_tpfGPIOCallback)(MyCallback6);
         s32EveVal = EveValT5;
         break;
      default:
         /*do nothing*/
         break;
      }

      /* Set device to input mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32)pinData.tId ) )
      {
         u32RetTr[s32thread_id] = 1;
      }
      /* Set Trigger Level/Edge */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
               OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER,(tS32)&pinData))
      {
         u32RetTr[s32thread_id] = 2;
      }
      /* Set callback */

      else if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
               OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CBData) )
      {
         u32RetTr[s32thread_id] = 3;
      }
      else
      {
         /* Enable Interrupt */
         pinData.unData.bState = TRUE;
         if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&pinData))
         {
            u32RetTr[s32thread_id] = 4;
         }
         else
         {
            for(u32Cnt =0; u32Cnt < NO_CALLBACKS_CALLED; u32Cnt++)
            {
               /* Set state of output pin to HIGH */
               Data.tId = GPIOpin;
               Data.unData.bState = TRUE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                        (tS32)Data.tId) )
               {
                  u32RetTr[s32thread_id] = 5;
               }

               OSAL_s32ThreadWait(100);


               /* Set state of output pin to LOW */

               Data.unData.bState = FALSE;
               if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
                        OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,(tS32)Data.tId) )
               {
                  u32RetTr[s32thread_id] += 6;
               }

               OSAL_s32ThreadWait(100);



            }

            Data.unData.bState = FALSE;
            /* Disable interrupts */
            if ( OSAL_ERROR == OSAL_s32IOControl ( Fd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32RetTr[s32thread_id] += 7;
            }
         }
      }

      OSAL_s32EventPost(evHandle, s32EveVal, OSAL_EN_EVENTMASK_OR);


      OSAL_s32ThreadWait(1000);


   }

   return;
}

/*****************************************************************************
* FUNCTION:     u32GPIO_OSAL_test()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_032
* DESCRIPTION:  Check for all GPIO pins
* HISTORY:      Created by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*****************************************************************************/

tU32 u32GPIO_OSAL_test(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0, u32Cnt = 0, u32count = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tGPIODevID DevID = 0;
   OSAL_trGPIOData Data = {0};
   OSAL_trGPIOCallbackData CbData = {0};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   else
   {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,
      "Introduced the new PIN ID values starting from 0 to maximum "
      "number of gpio pins instead of using the hardware specific ID values");
      for( u32count = OEDT_GPIO0_0; u32count <= OEDT_GPIO3_31;
      ++u32count )
      {
         DevID = (OSAL_tGPIODevID)u32count;

         /*Arguments for Set callback */
         CbData.rData.tId = u32count;
         CbData.rData.unData.pfCallback = MyCallback;
         CbData.pvArg = NULL;

         /*Arguments for Set trigger edge HIGH */
         Data.tId = u32count;
         Data.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

         /* Set device to input mode */
         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
         {
            u32Ret = 3;
         }
         /* Set callback */
         else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,(tS32)&CbData) )
         {
            u32Ret = 4;
         }
         /* Set trigger edge HIGH */
         else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER,(tS32)&Data) )
         {
            u32Ret = 5;
         }
         /* Disable interrupts */
         else
         {
            /*Disable interrupts*/
            Data.unData.bState = FALSE;

            if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
            {
               u32Ret = 6;
            }
            else
            {

               Data.unData.bState = TRUE;
               /* Enable interrupts * /    */
               if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                        OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
               {
                  u32Ret = 7;
               }

               Check_Callback = 0;

               for(u32Cnt = 0;u32Cnt<2; u32Cnt++)
               {
                  /* Set state of output pin to HIGH */
                  Data.tId = u32count;
                  Data.unData.bState = TRUE;
                  if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                           OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH,
                           (tS32)Data.tId) )
                  {
                     u32Ret += 8;
                  }

                  OSAL_s32ThreadWait(100);

                  /* Set state of output pin to LOW */
                  Data.tId = u32count;
                  Data.unData.bState = FALSE;
                  if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                           OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW,
                           (tS32)Data.tId) )
                  {
                     u32Ret += 9;
                  }
                  OSAL_s32ThreadWait(100);

               }
            }
         }

         OEDT_HelperPrintf
         (
         TR_LEVEL_USER_1,
         "GPIO active pins are :%d and call back count %d\t",
         (u32count - OEDT_GPIO0_0),Check_Callback
         );

         Data.unData.bState = FALSE;
         /* Disable interrupts */
         if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,(tS32)&Data) )
         {
            u32Ret += 10;
         }
      }

      /* Close the device */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 11;
      }
   }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32GPIOActiveStateTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_GPIO_033
* DESCRIPTION:  Check for active/inactive state of pin
* HISTORY:      Created by Shilpa Bhat (RBIN/ECM1) on 14 Nov, 2008
*****************************************************************************/
tU32 u32GPIOActiveStateTest(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = OSAL_OK;
   OSAL_tGPIODevID DevID = OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_RESET;
   OSAL_trGPIOData Data;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 2;
   }
   else
   {
      Data.tId = (OSAL_tGPIODevID) OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_RESET;

      /* Set device to output mode */
      if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,(tS32) DevID) )
      {
         u32Ret = 3;
      }
      /* Set device to output mode */
      else if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
               OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE,(tS32) &Data) )
      {
         u32Ret = 4;
      }

      /*OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE only checks for state*/
      /* if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
      OSAL_C_32_IOCTRL_GPIO_GET_STATE,(tS32)&Data) )
      {
         u32Ret += 20;
      } */

      else if(Data.unData.bState)
      {
         if(OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_INACTIVE_STATE,
                  (tS32)&Data) == OSAL_OK)
         {
            OSAL_s32ThreadWait(50);
            if(OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE,
                     (tS32)&Data) != OSAL_OK)
            {
               u32Ret = 5;
            }
         }
      }
      else
      {
         if(OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE,
                  (tS32)&Data) == OSAL_OK)
         {
            OSAL_s32ThreadWait(50);

            if(OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_INACTIVE_STATE,
                     (tS32)&Data) != OSAL_OK)
            {
               u32Ret = 6;
            }
         }
      }

      /* Close device */
      if((OSAL_s32IOClose(hFd)) != OSAL_OK)
      u32Ret += 7;
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32GPIOGetProcessResponse()
* PARAMETER   :    mqHandle
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_GPIO_030
* DESCRIPTION :    Wrapper function for OSAL_s32MessageQueueWait()
* 				   Created for lint fix.
* HISTORY     :    Created by Shahida Mohammed Ashraf (RBEI/ECF5) Feb 17, 2015
*******************************************************************************/
static tU32 u32GPIOGetProcessResponse(OSAL_tMQueueHandle mqHandle)
{
    tU32    u32Ret = 0;
    tU32    inmsg, procerr;

    if(OSAL_s32MessageQueueWait(mqHandle, (tPU8)&inmsg, sizeof(inmsg), OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
        u32Ret += 1000;

    procerr = inmsg & 0x0000ffff;
    if(procerr != 0)
    {
        u32Ret += procerr * 10000;
    }

    return u32Ret;
}

/************************ End of file ********************************/




