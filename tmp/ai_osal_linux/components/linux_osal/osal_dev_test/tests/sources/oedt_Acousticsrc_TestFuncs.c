
/******************************************************************************
*FILE         : oedt_Acousticsrc_TestFuncs.c
*
*SW-COMPONENT : OEDT
*
*DESCRIPTION  : This file implements the individual test cases for the
*               Acoustic device AcousticSRC
*
*AUTHOR       : nro2kor, ECF5/RBEI
*
*HISTORY:     initial release, 1.0
| Date      | Author / Modification
| --.--.--  | ----------------------------------------
| 16.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
| 08.01.16  | Kranthi Kiran, ECF5, RBEI :
| --------- : The validation of the output file is done using USB Sound device instead Acousticout device. 
| --------- : Hence the dependenacy on Acousticout is removed.
| 05.02.16  : Lint Fix CFG3-1713 and CFG3-1714 by Kranthi Kiran Konganti (RBEI/ECF5)
*****************************************************************************/

#include <sys/stat.h>
#include <alsa/asoundlib.h>
#include <sys/stat.h>


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"
#include "oedt_Display.h"

#include "oedt_Acousticsrc_TestFuncs.h"

#include "odt_Globals.h"
#include "odt_LinkedList.h"


#include "acousticsrc_public.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define OEDT_ACOUSTICSRC_PRINTF_INFO(_X_) printf _X_
#define OEDT_ACOUSTICSRC_PRINTF_ERROR(_X_) printf _X_

#define OEDT_ACOUSTICSRC_NUMBER_OF_ARRAY_ELEMENTS(_X_) (sizeof(_X_) / sizeof((_X_)[0]))

/* Parameters for USB Soundcard device */
#define OEDT_ACOUSTICSRC_SAMPLING_RATE 22050
#define OEDT_ACOUSTICSRC_USB_SOUNDCARD_NAME "USBDevice"
#define OEDT_ACOUSTICSRC_NUMBER_OF_CHANNELS 1


/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static tU32  OEDT_ACOUSTICSRC_uiPCMDataBufferIndex;
extern const tChar OEDT_u8Acoustic_speech_16000Hz_mono_16bit_LE_Array[25042];

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/


/********************************************************************/ /**
  *  FUNCTION:     static void vResetPCMData()
  *
  *  @brief 	        set pcm-Data to start of array (!of all PCM Arrays!)
  *
  *  @return   none
  *
  *  HISTORY:
  *
  | Date	  | Author / Modification
  | --.--.--  | ----------------------------------------
  | 16.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
  ************************************************************************/
static void vResetPCMData(void)
{
    OEDT_ACOUSTICSRC_uiPCMDataBufferIndex = 0;
}


/********************************************************************/ /**
  *  FUNCTION:      	static int iGetPCMData16000mono(void *vBuffer, int iBufferLen)
  *
  *  @brief			reads pCM-Data 16000Hz mono 16 bit_LE
  *
  *  @return   		number of bytes copied in to the array
  *
  *  HISTORY:
  *
  | Date	  | Author / Modification
  | --.--.--  | ----------------------------------------
  | 16.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
  ************************************************************************/
static tU32 uiGetPCMData16000mono(void *vBuffer, unsigned int uiBufferLen)
{
    unsigned int uiPCMBytesCopied = 0;
    unsigned int uiPCMRecentReadIndex = OEDT_ACOUSTICSRC_uiPCMDataBufferIndex;
    int iPCMBytesLeft;
    unsigned int uiPCMByteLen = OEDT_ACOUSTICSRC_NUMBER_OF_ARRAY_ELEMENTS(OEDT_u8Acoustic_speech_16000Hz_mono_16bit_LE_Array);

    iPCMBytesLeft = (int)(uiPCMByteLen - uiPCMRecentReadIndex);

    if(iPCMBytesLeft > 0)
    {
        uiPCMBytesCopied = (tU32)iPCMBytesLeft > uiBufferLen ? uiBufferLen : (tU32)iPCMBytesLeft;
        memcpy(vBuffer,
               &OEDT_u8Acoustic_speech_16000Hz_mono_16bit_LE_Array[uiPCMRecentReadIndex],
               uiPCMBytesCopied);
    } 
    
    OEDT_ACOUSTICSRC_uiPCMDataBufferIndex += uiPCMBytesCopied;
    return uiPCMBytesCopied;
}


/********************************************************************/ /** 
*  FUNCTION:  	iHandleXRUN 
* 
*  @brief       Handles the XRUN error that occur in ALSA Playback or Capture 
*  
*  @return		0 for success, Error code in other cases  
*
*  HISTORY: 
* 
| Date		| Author / Modification
| --.--.--	| ----------------------------------------
| 08.01.16	| Kranthi Kiran, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/ 

static int iHandleXRUN(snd_pcm_t* AlsaPCM,int iRet)
{
    if(iRet >= 0)
    return iRet;

    // no error, return previous error code
    OEDT_ACOUSTICSRC_PRINTF_INFO(("XRUN Occured. Try to handle the return value %d ",iRet));
    if(iRet == -EAGAIN)
    {
        usleep(2000); // avoid a bug in ALSAlib 1.0.25
        return 0;
    }
    iRet = snd_pcm_recover(AlsaPCM,iRet,0);
    if(iRet == 0)
    {
        iRet = snd_pcm_start(AlsaPCM);
        if(iRet == 0)
        {
            OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: Successfully Restarted from XRUN ",__FUNCTION__));   
        }
        else
        {
            OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: snd_pcm_start error	",__FUNCTION__)); 
        }
    }
    else 
    {
        OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: snd_pcm_recover error [%d]",__FUNCTION__, iRet));
    } 
    return iRet;
}
			 
			 
		

/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/************************* TEST 000 ******************************************/
/*****************************************************************************/


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICSRC_T000(void)
  *
  *  @brief         Prints only information about tests.
  *
  *  @param         
  *
  *  @return   0
  *
  *  HISTORY:
  *
  | Date	  | Author / Modification
  | --.--.--  | ----------------------------------------
  | 16.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
  ************************************************************************/
tU32 OEDT_ACOUSTICSRC_T000(void)
{
    OEDT_ACOUSTICSRC_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));
    OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTICSRC_T000\n"));
    OEDT_ACOUSTICSRC_PRINTF_INFO(("For validation of the SRC file dump you need a DeLock-USB-Soundcard 61645 connected to target\n"));	
    OEDT_ACOUSTICSRC_PRINTF_INFO(("---------------------------------------------------------------------------------\n"));
    return 0UL;
}


/*****************************************************************************
* FUNCTION		:  OEDT_ACOUSTICSRC_T001
* PARAMETER	:  None
* RETURNVALUE	:  None
* DESCRIPTION	:  basic SRC testing
* HISTORY		:  
| Date		| Author / Modification
| --.--.--	| ----------------------------------------
| 16.10.12	| Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
******************************************************************************/

/*****************************************************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/*****************************************************************************/
#define OEDT_ACOUSTICSRC_T001_COUNT                        1
#define OEDT_ACOUSTICSRC_T001_DEVICE_NAME \
                 OSAL_C_STRING_DEVICE_ACOUSTIC_IF_SRC
#define OEDT_ACOUSTICSRC_T001_SAMPLE_RATE                  16000
#define OEDT_ACOUSTICSRC_T001_CHANNELS                     1
#define OEDT_ACOUSTICSRC_T001_BUFFERSIZE                   2048


static tBool OEDT_ACOUSTICSRC_bStopped = FALSE;
static tU8   OEDT_ACOUSTICSRC_T001_u8PCMBuffer[OEDT_ACOUSTICSRC_T001_BUFFERSIZE];

#define OEDT_ACOUSTICSRC_T001_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICSRC_T001_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICSRC_T001_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICSRC_T001_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICSRC_T001_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICSRC_T001_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICSRC_T001_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICSRC_T001_START_RESULT_ERROR_BIT            0x00000040
#define OEDT_ACOUSTICSRC_T001_CONVERT_RESULT_ERROR_BIT          0x00000080
#define OEDT_ACOUSTICSRC_T001_GETFILEPATH_RESULT_ERROR_BIT      0x00000100
#define OEDT_ACOUSTICSRC_T001_STOP_RESULT_ERROR_BIT             0x00000200
#define OEDT_ACOUSTICSRC_T001_STOP_ACK_RESULT_ERROR_BIT         0x00000400
#define OEDT_ACOUSTICSRC_T001_CLOSE_OUT_RESULT_ERROR_BIT        0x00000800
#define OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT		    0x00001000
#define OEDT_ACOUSTICSRC_T001_FILESTAT_RESULT_ERROR_BIT         0x00002000
#define OEDT_ACOUSTICSRC_T001_OPEN_FILE_RESULT_ERROR_BIT        0x00004000
#define OEDT_ACOUSTICSRC_T001_VALIDATION_OUTPUT_ERROR_BIT       0x00008000

/********************************************************************/ /** 
*  FUNCTION:  	u32PlaytheTestFile 
* 
*  @brief        	Checks the file size and plays the file using Alsa USB sound device
*  
*  @return		0 for success, Error code in other cases  
*
*  HISTORY: 
* 
| Date		| Author / Modification
| --.--.--	| ----------------------------------------
| 08.01.16	| Kranthi Kiran, ECF5, RBEI : Initial Revision, 1.0
| 05.02.16  | Kranthi Kiran : Lint Fix CFG3-1713 and CFG3-1714
************************************************************************/ 


tU32 u32PlaytheTestFile(const char * FileName)
{
   
       char * sDeviceName = OEDT_ACOUSTICSRC_USB_SOUNDCARD_NAME;
       unsigned int nChannels = OEDT_ACOUSTICSRC_NUMBER_OF_CHANNELS;
       unsigned int nSampleRate = OEDT_ACOUSTICSRC_SAMPLING_RATE;
       tU32 size;
       OSAL_tIODescriptor fp_input;
       char *Buffer;
       tS32 fRet;
       int iDirection = 0,err,iRet,sRet,loop;
       snd_pcm_t* pAlsaPCMPtr;
       snd_pcm_uframes_t uiPeriodSize,framesBufferSize,framesPeriodSize;
       snd_pcm_hw_params_t *hw_params;
       snd_pcm_sw_params_t *sw_params;
       tBool bExit = FALSE;
       tU32 u32ResultBitMask = OEDT_ACOUSTICSRC_T001_RESULT_OK_VALUE;
	   tU32 size_array, front = 0, back = 0;

	   
	   fp_input = OSAL_IOOpen(FileName,OSAL_EN_READWRITE);
	   if (fp_input == OSAL_ERROR) 
	   {	   
		   OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ValidateOutput: ERROR Open <%s>\n",    FileName));
		   u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_OPEN_FILE_RESULT_ERROR_BIT;
		   return u32ResultBitMask;
	   }   
	   if ((OSAL_ERROR == OSAL_s32IOControl(fp_input, OSAL_C_S32_IOCTRL_FIOWHERE, (tS32) &front))||
		   (OSAL_ERROR == OSAL_s32IOControl(fp_input, OSAL_C_S32_IOCTRL_FIONREAD, (tS32) &back)))
	   
	   {
		   u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_FILESTAT_RESULT_ERROR_BIT;
		   OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ValidateOutput: ERROR Invalid File Size <%s>\n",FileName));
		   return u32ResultBitMask;
	   }
	   size_array = front+back; //complete file size	  
	   OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ValidateOutput: SIZE OF THE OUTPUT FILE IS %lu \n", size_array));  


   	   /* open the pcm device for the USB sound card */
       err = snd_pcm_open(&pAlsaPCMPtr,sDeviceName,SND_PCM_STREAM_PLAYBACK,SND_PCM_NONBLOCK);
       if(err < 0 )
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:unable to open pcm device (%s): %s\n",__FUNCTION__,sDeviceName,snd_strerror(err)));
           pAlsaPCMPtr = NULL;
		   (void)pAlsaPCMPtr;// to fix lint
		   u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
       OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: The Device %s is opened with Playback Stream \n",__FUNCTION__,sDeviceName)); 
       
       /*Allocate hardware parameters object */	   
       snd_pcm_hw_params_alloca(&hw_params);/*lint !e717*/
	   
	   /*Check for hw_params to be valid */
	   if (hw_params == NULL)
       {
           snd_pcm_close(pAlsaPCMPtr);
		   u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
       
       /* set default hw parameters */
       err = snd_pcm_hw_params_any( pAlsaPCMPtr, hw_params);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:cannot set default params (%s)\n",
                                  __FUNCTION__,
                                  snd_strerror (err)));
		   snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
   
       /* set access to SND_PCM_ACCESS_RW_INTERLEAVED */
       err = snd_pcm_hw_params_set_access(pAlsaPCMPtr,hw_params,SND_PCM_ACCESS_RW_INTERLEAVED );
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:cannot set access (%s)\n", __FUNCTION__,snd_strerror (err)));
		   snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
   
       /* set format SND_PCM_FORMAT_S16_LE */
       err = snd_pcm_hw_params_set_format(pAlsaPCMPtr,hw_params,SND_PCM_FORMAT_S16_LE);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:cannot set format (%s)\n", __FUNCTION__,snd_strerror (err)));
		   snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
   
       /* Set number of Channels */
       err = snd_pcm_hw_params_set_channels(pAlsaPCMPtr,hw_params,nChannels);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:cannot set Channels %u (%s)\n", __FUNCTION__,nChannels, snd_strerror (err)));
		   snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
   
       /* set sampling rate */
       err = snd_pcm_hw_params_set_rate_near(pAlsaPCMPtr,hw_params,&nSampleRate,&iDirection);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:cannot set Rate %u (%s)\n", __FUNCTION__,nSampleRate, snd_strerror (err)));
		   snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
       OEDT_ACOUSTICSRC_PRINTF_INFO(("Sample Rate set is : (%u) \n",nSampleRate));
       iDirection = 0; //to set to nearest value
   
       /* set period size */
       framesPeriodSize = (nSampleRate * 16 /1000); //try to set period time as 16 ms

       err = snd_pcm_hw_params_set_period_size_near(pAlsaPCMPtr,hw_params,&framesPeriodSize,&iDirection);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: ERROR: cannot set period size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
           snd_pcm_close(pAlsaPCMPtr);
		   u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }

       framesBufferSize = (framesPeriodSize * 2); //number of periods per buffer set to 2
       err = snd_pcm_hw_params_set_buffer_size_near(pAlsaPCMPtr,hw_params,&framesBufferSize);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: ERROR: cannot set Buffer size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
           snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
       
       /*Write the hardware parameters to the PCM device */
       err = snd_pcm_hw_params(pAlsaPCMPtr,hw_params);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:unable to set hw parameters: %s \n",__FUNCTION__,snd_strerror(err) ));
           snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
   
       iDirection = 0; //to set to nearest value
       err = snd_pcm_hw_params_get_period_size(hw_params, &uiPeriodSize, &iDirection);
       OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:The Period Size is (%u frames)\n ",__FUNCTION__,(unsigned int)uiPeriodSize));
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s: INFO: cannot get periods",__FUNCTION__));
       }	
   
       /*Get the default Software Parameters for the PCM device */

	   snd_pcm_sw_params_alloca(&sw_params);/*lint !e717*/
	   /*Check for sw_params to be valid */
	   if (sw_params == NULL)
       {
           snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }
       err = snd_pcm_sw_params_current(pAlsaPCMPtr, sw_params);
       if(err < 0)
       {
           OEDT_ACOUSTICSRC_PRINTF_INFO(("%s:Unable to determine "
                                  "current sw_params for playback: %s \n",
                                  __FUNCTION__,
                                  snd_strerror(err)));
           snd_pcm_close(pAlsaPCMPtr);
           u32ResultBitMask = OEDT_ACOUSTICSRC_T001_PLAY_TEST_FILE_ERROR_BIT;
           return u32ResultBitMask;
       }

		   /* Use a buffer large enough to hold one period */
           size = (uiPeriodSize * 2 * nChannels); // 2 bytes for each sample
           Buffer = (char *)malloc(size);
   		   loop = (int)(size_array/size);
     
           do{
			
            iRet = snd_pcm_wait(pAlsaPCMPtr,100);
            if(iRet == 0)
                {
                    OEDT_ACOUSTICSRC_PRINTF_INFO(("Time out ERROR \n"));
                    bExit = TRUE;
					u32ResultBitMask = (tU32)OSAL_ERROR;
           		}
            else if(iRet < 0)
                {
                  iRet = iHandleXRUN(pAlsaPCMPtr, iRet);
                  if(iRet < 0)
                  {
                    OEDT_ACOUSTICSRC_PRINTF_INFO(("XRUN Could not be recovered \n"));
                    bExit = TRUE;
                  }
                }
            
            if(iRet > 0)
            {
                fRet = OSAL_s32IORead(fp_input,(tS8*)Buffer,size);
                if(fRet == OSAL_ERROR)
                    {
                        OEDT_ACOUSTICSRC_PRINTF_INFO(("Read from file Error \n"));
                        bExit = TRUE;
                    }
                
                sRet = snd_pcm_writei(pAlsaPCMPtr,Buffer,uiPeriodSize);
                if(sRet >= 0)
                    {
                        OEDT_ACOUSTICSRC_PRINTF_INFO(("Bytes written to alsa is %d\n",sRet));
                    }
                
               else if (sRet < 0) 
                {
                    iRet = iHandleXRUN(pAlsaPCMPtr,sRet);
                    if(iRet < 0)
                    {
                        OEDT_ACOUSTICSRC_PRINTF_INFO(("error from writei: %s\n",snd_strerror(iRet)));
                        u32ResultBitMask = (tU32)OSAL_ERROR;
                        bExit = TRUE;
                    }
                }
           }
          --loop;
         }while(!bExit && loop);
         if(OSAL_ERROR == OSAL_s32IOClose(fp_input))
		 	{
		 	  OEDT_ACOUSTICSRC_PRINTF_INFO(("Error in closeing the file %s\n",FileName));
		 	}
         snd_pcm_drain(pAlsaPCMPtr);
         snd_pcm_close(pAlsaPCMPtr);
         free(Buffer);  
		 return u32ResultBitMask;
		 	  
   }





/********************************************************************/ /** 
*  FUNCTION:  	uiValidateOutput 
* 
*  @brief        	validates the output file dump by playing it out through USB sound card
*  
*  @return		result status 
*
*  HISTORY: 
* 
| Date		| Author / Modification
| --.--.--	| ----------------------------------------
| 16.10.12	| Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
| 12.01.16	| Kranthi Kiran, ECF5, RBEI
| 05.02.16  | Kranthi Kiran, ECF5 : Lint Fix CFG3-1713 and CFG3-1714
************************************************************************/ 
tU32 uiValidateOutput (const char * SRC_output_file)
{
	tU32 u32ResultBitMask;
	
	OEDT_ACOUSTICSRC_PRINTF_INFO(("Validation of SRC output \n"));

	
    u32ResultBitMask = u32PlaytheTestFile(SRC_output_file);
	OSAL_s32ThreadWait(10);
	return u32ResultBitMask;
} 


/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticSrcTstCallback_T001
*DESCRIPTION: used as device callback function for device test T001
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     01.10.2012, Niyatha Srivathsa Rao
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid vAcousticSrcTstCallback (OSAL_tenAcousticSrcEvent enCbReason,
                               tPVoid pvAddData,tPVoid pvCookie)
{
	OSAL_trAcousticErrThrCfg rCbLastErrThr;
	(void)pvCookie;
	(void)pvAddData;

	switch (enCbReason)
	{
		case OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED:
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK:OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED\n"));
		OEDT_ACOUSTICSRC_bStopped = TRUE;
		break;

		case OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED:

		rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK:OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED: %u == 0x%08X\n",\
								  (unsigned int)rCbLastErrThr.enErrType, (unsigned int)rCbLastErrThr.enErrType));
		switch (rCbLastErrThr.enErrType)
		{
			case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
			break;

			case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
			break;

			case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
			break;

			case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
			break;

			case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
			break;

			case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
			break;

			default:
			break;
		}
		break;

		case OSAL_EN_ACOUSTICSRC_EVTIMER:
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK: OSAL_EN_ACOUSTICSRC_EVTIMER\n"));
		break;

		case OSAL_EN_ACOUSTICSRC_EVSTARTMARKREACHED:
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK:OSAL_EN_ACOUSTICSRC_EVSTARTMARKREACHED\n"));
		break;

		case OSAL_EN_ACOUSTICSRC_EVEPISODEFINISHED: /*!< Episode end Event */
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK:OSAL_EN_ACOUSTICSRC_EVEPISODEFINISHED\n"));
		break;

		case OSAL_EN_ACOUSTICSRC_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK:OSAL_EN_ACOUSTICSRC_LOAN_CB_REGISTERED\n"));
		break;

		default: /* callback unsupported by test code */
		OEDT_ACOUSTICSRC_PRINTF_INFO(("ASRC CALLBACK: DEFAULT %u == 0x%08X\n",
								(unsigned int)enCbReason,(unsigned int)enCbReason));
		break;
	}
}

/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICSRC_T001(void)
  *
  *  @brief         Play PCM Test file(written specifically for testing SRC functionality, hence to be used with the right asoundrc)
  *
  *  @param         
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY: 
  *
  *  - 01.10.2012, Niyatha Srivathsa Rao
  *    Initial revision.
  ************************************************************************/
tU32 OEDT_ACOUSTICSRC_T001(void)
{
	tU32 u32ResultBitMask           = OEDT_ACOUSTICSRC_T001_RESULT_OK_VALUE;
	OSAL_tIODescriptor hAcousticsrc = OSAL_ERROR;
	tS32 s32Ret;
	int  iCount;
	tU32 u32ReadLen;	
	tS8 *ps8PCM       = (tS8*)OEDT_ACOUSTICSRC_T001_u8PCMBuffer;
	OSAL_trAcousticSrcConvert src_info;
	OSAL_trEX_Open_Arg FilePathCfg;
			
	OEDT_ACOUSTICSRC_PRINTF_INFO(("tU32 OEDT_ACOUSTICSRC_T001(void)<ENTER>\n"));

	memset(&FilePathCfg,0,sizeof(FilePathCfg));	

	for(iCount = 0; iCount < OEDT_ACOUSTICSRC_T001_COUNT; iCount++)
	{
		/* open /dev/acoustic/src */
		hAcousticsrc = OSAL_IOOpen(OEDT_ACOUSTICSRC_T001_DEVICE_NAME, OSAL_EN_WRITEONLY);
		if(OSAL_ERROR == hAcousticsrc)
		{
			OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> \n",
											OEDT_ACOUSTICSRC_T001_DEVICE_NAME));
			u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_OPEN_OUT_RESULT_ERROR_BIT;
			hAcousticsrc = OSAL_ERROR;
		}
		else 
		{
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Open <%s> == %lu\n",
	                          OEDT_ACOUSTICSRC_T001_DEVICE_NAME,(tU32)hAcousticsrc));
		} 


		/* register callback function */
		{
			OSAL_trAcousticSrcCallbackReg rCallbackReg;
			rCallbackReg.pfEvCallback = vAcousticSrcTstCallback;
			rCallbackReg.pvCookie = (tPVoid)&hAcousticsrc;  // cookie unused
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
		                           OSAL_C_S32_IOCTRL_ACOUSTICSRC_REG_NOTIFICATION,
		                           (tS32)&rCallbackReg);
			if(OSAL_OK != s32Ret)
			{
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
			u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_REG_NOTIFICATION_RESULT_ERROR_BIT;
			}
			else
			{
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

			}
		}


		/* configure sample rate */
		{
			OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
			rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
			rSampleRateCfg.nSamplerate = OEDT_ACOUSTICSRC_T001_SAMPLE_RATE;
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
		                         OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETSAMPLERATE,
		                         (tS32)&rSampleRateCfg);
			if(OSAL_OK != s32Ret)
			{
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_SETSAMPLERATE_RESULT_ERROR_BIT;
			}
			else
			{
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: "
			                            "SUCCESS SETSAMPLERATE: %u\n",
			                            (unsigned int)rSampleRateCfg.nSamplerate));
			}
		}


		/* configure channels */
		{
			tU32 u32Channels = OEDT_ACOUSTICSRC_T001_CHANNELS;
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
		                     OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETCHANNELS,
		                     (tS32)u32Channels);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: "
			                         "ERROR SETCHANNELS: %u\n",
			                         (unsigned int)u32Channels));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_SETCHANNELS_RESULT_ERROR_BIT;
			}
			else
			{
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: "
			                        "SUCCESS SETCHANNELS: %u\n",
			                        (unsigned int)u32Channels));
			}
		}

		/* configure sample format */
		{
			OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
			rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
			rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
									 OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETSAMPLEFORMAT,
									 (tS32)&rSampleFormatCfg);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: "
		                                     "ERROR SETSAMPLEFORMAT: %u\n",
		                                     (unsigned int)rSampleFormatCfg.enSampleformat));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
			}
			else 
			{
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: "
		                                    "SUCCESS SETSAMPLEFORMAT: %u\n",
		                                    (unsigned int)rSampleFormatCfg.enSampleformat));
			} 
		}

		/* configure buffersize */
		{
			OSAL_trAcousticBufferSizeCfg rCfg;
			rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
			rCfg.nBuffersize = OEDT_ACOUSTICSRC_T001_BUFFERSIZE;
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
		                           OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETBUFFERSIZE,
		                           (tS32)&rCfg);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: "
											   "ERROR SETBUFFERSIZE: %u\n",
											   (unsigned int)rCfg.nBuffersize));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_SETBUFFERSIZE_RESULT_ERROR_BIT;
			}
			else
			{
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: "
											  "SUCCESS SETBUFFERSIZE: %u\n",
											  (unsigned int)rCfg.nBuffersize));
			}
		}


		/* issue start command */
		s32Ret = OSAL_s32IOControl(hAcousticsrc,OSAL_C_S32_IOCTRL_ACOUSTICSRC_START,(tS32)NULL);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
			u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_START_RESULT_ERROR_BIT;
		}
		else
		{
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
		}

		//convert the samples if no error occured before
		if(OEDT_ACOUSTICSRC_T001_RESULT_OK_VALUE == u32ResultBitMask)
		{
			vResetPCMData();
			while((u32ReadLen = uiGetPCMData16000mono(OEDT_ACOUSTICSRC_T001_u8PCMBuffer,OEDT_ACOUSTICSRC_T001_BUFFERSIZE)) > 0)
			{
				src_info.pvBuffer = ps8PCM;
				src_info.u32BufferSize = u32ReadLen;
				src_info.nTimeout = 10000;
				src_info.u32PhraseStartEvtId = 0;
				src_info.enBufStatus = OSAL_EN_ACOUSTIC_DATA_VALID;
				src_info.bEpisodeComplete = 1;
		   
				s32Ret = OSAL_s32IOControl(hAcousticsrc,OSAL_C_S32_IOCTRL_ACOUSTICSRC_CONVERT,(tS32)&src_info);

				if(OSAL_ERROR == s32Ret)
				{
					OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR ASRC convert bytes (%lu)\n", u32ReadLen));
					u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_CONVERT_RESULT_ERROR_BIT;
				}             		
			} 


			/* stop command */
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: INFO Send STOP command\n"));
			OEDT_ACOUSTICSRC_bStopped = FALSE;
			
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
	                               OSAL_C_S32_IOCTRL_ACOUSTICSRC_STOP, (tS32)NULL);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_STOP_RESULT_ERROR_BIT;
			}
			else 
			{		
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
			} 

			FilePathCfg.enAccess = OSAL_EN_READONLY;
			FilePathCfg.Fd = hAcousticsrc;
			
			s32Ret = OSAL_s32IOControl(hAcousticsrc,
								   OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETFILEPATH, (tS32)&FilePathCfg);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR GETTING THE FILE PATH\n"));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_GETFILEPATH_RESULT_ERROR_BIT;
			}
			else
			{
				OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: O/P WAV File path =%s \n",FilePathCfg.FileName));				
			}

			//waiting for stop reply
			{
				int iEmergency = 50;
				while(!OEDT_ACOUSTICSRC_bStopped && (iEmergency-- > 0))
				{
					OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
					OSAL_s32ThreadWait(10);
				} 

				if(iEmergency <= 0)
				{
					OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR no STOP acknowledge\n"));
					u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_STOP_ACK_RESULT_ERROR_BIT;
				}
				else 
				{
					OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
				} 
			}

		}

		/*Play out the SRC output file using USB sound card */
		
		if(OSAL_OK != (tS32)uiValidateOutput(FilePathCfg.FileName))
			{
			    OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR in the output Validation \n"));
				u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_VALIDATION_OUTPUT_ERROR_BIT;
			}

		/* close /dev/acousticout/speech */
		s32Ret = OSAL_s32IOClose(hAcousticsrc);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: "
	                                 "ERROR AOUT CLOSE handle %u \n",
	                                 (unsigned int)hAcousticsrc));
			u32ResultBitMask |= OEDT_ACOUSTICSRC_T001_CLOSE_OUT_RESULT_ERROR_BIT;
		}
		else 
		{
			OEDT_ACOUSTICSRC_PRINTF_INFO(("OEDT_ACOUSTIC:SUCCESS AOUT CLOSE handle %u", (unsigned int)hAcousticsrc));
		} 

	} 
	
	if(OEDT_ACOUSTICSRC_T001_RESULT_OK_VALUE != u32ResultBitMask)
	{
		OEDT_ACOUSTICSRC_PRINTF_ERROR(("OEDT_ACOUSTIC: "
	                         "T001 bit coded ERROR: 0x%08X\n",
	                         (unsigned int)u32ResultBitMask));
	} 

	OSAL_s32ThreadWait(10);
	OEDT_ACOUSTICSRC_PRINTF_INFO(("tU32 OEDT_ACOUSTICSRC_T001(void) <EXIT>\n"));  
	return u32ResultBitMask;
} 







