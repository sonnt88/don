/******************************************************************************
 * FILE         : oedt_CARD_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the file system test cases for SDCard  
 *                          
 * AUTHOR(s)    :  Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * --.--.--       | Initial revision    | ------------
 * 2 Dec, 2008    | version 1.0         | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   | version 1.1     	| Shilpa Bhat(RBEI/ECM1)
   23.03.09       | ver1.2             |Ravindran P (RBEI/ECF1) - Compiler/LINT warning removal
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009   | version 1.3        |  Commenting the Test cases which uses 
 *				   	|				   	   |  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *				   	|				   	   |  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010   | version 1.4	    	|  Updated the Device name 
 *				      |						   |  Anoop Chandran (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 28 May, 2010   | version 1.5	    	|  Added new OEDT for CARD STATE
 *         			|							|u32CARD_CommonFS_CardState()
 *				      |						   |  anc2hi
 * 26 Feb, 2013   | version 1.6	    	|  Removed  u32CARD_CommonFSPrepareEject
 *         			|							|for Osal Unsed IO control Removal
 *				      |						    | by  SWM2KOR
 ------------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_CARD_CommonFS_TestFuncs.h"


#define OEDT_CARD_CFS_COMMONFILE			OEDTTEST_C_STRING_DEVICE_CARD"/commonfile.txt"
#define OEDT_CARD_CFS_DUMMYFILE				OEDTTEST_C_STRING_DEVICE_CARD"/Dummy.txt"
#define OEDT_CARD_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_CARD"/Test.txt"
#define OEDT_CARD_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_CARD"/DIR/Test.txt"
#define OEDT_CARD_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_CARD"/��汪�.txt"
#define OEDT_CARD_CFS_INVALCHAR_FILE		OEDTTEST_C_STRING_DEVICE_CARD"//*/</>>.txt"

/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpenClosedevice( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_001
* DESCRIPTION	:	Opens and closes Device
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSOpenClosedevice(tVoid)
{ 
	tU32 u32Ret = 0;

	u32Ret = u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpendevInvalParm( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_002
* DESCRIPTION	:	Try to Open device with invalid parameters
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSOpendevInvalParm(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSReOpendev( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_003
* DESCRIPTION	:	Try to Re-Open device
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSReOpendev(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpendevDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_004
* DESCRIPTION	:	Try to open device with different modes
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSOpendevDiffModes(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSClosedevAlreadyClosed( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_005
* DESCRIPTION	:	Try to close device already closed
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSClosedevAlreadyClosed(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpenClosedir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_006
* DESCRIPTION	:	Try to Open and closes a directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSOpenClosedir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpenClosedir((const tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpendirInvalid( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_FILESYSTEM_007
* DESCRIPTION	:	Try to Open invalid directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSOpendirInvalid(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
					(tPS8)OEDT_C_STRING_CARD_NONEXST,
					(tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS
					};

	u32Ret = u32FSOpendirInvalid(dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateDelDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_008
* DESCRIPTION	:	Try create and delete directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSCreateDelDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSCreateDelDir((const tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateDelSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_009
* DESCRIPTION	:	Try create and delete sub directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSCreateDelSubDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSCreateDelSubDir((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateDirInvalName( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_010
* DESCRIPTION	:	Try create and delete directory with invalid name
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSCreateDirInvalName(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
						(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
						(tPS8)OEDT_C_STRING_CARD_DIR_INV_NAME
						};
	u32Ret = u32FSCreateDirInvalName(dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSRmNonExstngDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_011
* DESCRIPTION	:	Try to remove non exsisting directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSRmNonExstngDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSRmNonExstngDir((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSRmDirUsingIOCTRL( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_012
* DESCRIPTION	:	Delete directory with which is not a directory (with files) 
*						using IOCTRL
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSRmDirUsingIOCTRL(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)OEDT_C_STRING_CARD_FILE1
							};
	u32Ret = u32FSRmDirUsingIOCTRL( dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSGetDirInfo( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_013
* DESCRIPTION	:	Get directory information
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSGetDirInfo(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSOpenDirDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_014
* DESCRIPTION	:	Try to open directory in different modes
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSOpenDirDiffModes(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)SUBDIR_PATH_CARD
							};

	u32Ret = u32FSOpenDirDiffModes(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSReOpenDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_015
* DESCRIPTION	:	Try to reopen directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSReOpenDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS,
							(tPS8)OEDT_C_STRING_CARD_DIR1
							};

	u32Ret = u32FSReOpenDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSDirParallelAccess( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_016
* DESCRIPTION	:	When directory is accessed by another thread try to rename it,
*						delete it
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSDirParallelAccess(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSDirParallelAccess((const tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS); 
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateDirMultiTimes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_017
* DESCRIPTION	:	Try to Create directory with similar name 
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSCreateDirMultiTimes(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
								(tPS8)OEDT_C_STRING_DEVICE_CARD_ROOT_CFS,
								(tPS8)SUBDIR_PATH_CARD,
								(tPS8)SUBDIR_PATH2_CARD
								};

	u32Ret = u32FSCreateDirMultiTimes(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateRemDirInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_018
* DESCRIPTION	:	Create/ remove directory with Invalid path
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSCreateRemDirInvalPath(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)OEDT_C_STRING_CARD_DIR_INV_PATH
							};

	u32Ret = u32FSCreateRemDirInvalPath(dev_name);
	return u32Ret;
}



/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateRmNonEmptyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_019
* DESCRIPTION	:	Try to remove non Empty directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSCreateRmNonEmptyDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)SUBDIR_PATH_CARD
							};

	u32Ret = u32FSCreateRmNonEmptyDir(dev_name, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCopyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_020
* DESCRIPTION	:	Copy the files in source directory to destination directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSCopyDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
									(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
									(tPS8)FILE13_CARD,
									(tPS8)FILE14_CARD
									};

	u32Ret = u32FSCopyDir(dev_name,TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSMultiCreateDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_021
* DESCRIPTION	:	Create multiple sub directories
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSMultiCreateDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSMultiCreateDir((const tPS8)OEDTTEST_C_STRING_DEVICE_CARD, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCreateSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_022
* DESCRIPTION	:	Attempt to create sub-directory within a directory opened
* 						in READONLY modey
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CARD_CommonFSCreateSubDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
								(tPS8)SUBDIR_PATH_CARD
								};

	u32Ret = u32FSCreateSubDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSDelInvDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_023
* DESCRIPTION	:	Delete invalid Directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSDelInvDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
								(tPS8)OEDT_C_STRING_CARD_DIR_INV_NAME
								};

	u32Ret = u32FSDelInvDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSCopyDirRec( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_024
* DESCRIPTION	:	Copy directories recursively
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CARD_CommonFSCopyDirRec(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)FILE13_CARD,
							(tPS8)FILE14_CARD
							};

	u32Ret = u32FSCopyDirRec(dev_name);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSRemoveDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_025
* DESCRIPTION	:	Remove directories recursively
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSRemoveDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[4] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CARD,
							(tPS8)FILE11_CARD,
							(tPS8)FILE12_CARD,
							(tPS8)FILE12_RECR2_CARD
							};

	u32Ret = u32FSRemoveDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileCreateDel( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_026
* DESCRIPTION	:  Create/ Delete file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateDel(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileCreateDel((const tPS8)CREAT_FILE_PATH_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_027
* DESCRIPTION	:	Open /close File
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileOpenClose(tVoid)
{
	tU32 u32Ret = 0;

	u32Ret = u32FSFileOpenClose((const tPS8)OSAL_TEXT_FILE_FIRST_CARD, TRUE);
	return u32Ret;
}	


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileOpenInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_028
* DESCRIPTION	:   Open file with invalid path name (should fail)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileOpenInvalPath(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_CARD);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileOpenInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_029
* DESCRIPTION	:	Open a file with invalid parameters (should fail), 
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileOpenInvalParam(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileOpenInvalParam((const tPS8)OSAL_TEXT_FILE_FIRST_CARD, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReOpen( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_030
* DESCRIPTION	:  Try to open and close the file which is already opened
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32  u32CARD_CommonFSFileReOpen(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReOpen((const tPS8)OSAL_TEXT_FILE_FIRST_CARD, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_031
* DESCRIPTION	:	Read data from file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileRead(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileRead((const tPS8)OEDT_CARD_COMNFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileWrite( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_032
* DESCRIPTION	:  Write data to file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileWrite(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileWrite((const tPS8)OEDT_CARD_WRITEFILE, TRUE);	
	return u32Ret;
}	


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSGetPosFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_033
* DESCRIPTION	:  Get File Position from begining of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSGetPosFrmBOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetPosFrmBOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSGetPosFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_034
* DESCRIPTION	:	Get File Position from end of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSGetPosFrmEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetPosFrmEOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_035
* DESCRIPTION	:	Read with a negative offset from BOF
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32   u32CARD_CommonFSFileReadNegOffsetFrmBOF()
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadNegOffsetFrmBOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_036
* DESCRIPTION	:	Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetBeyondEOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_037
* DESCRIPTION	:  Read from offset from Beginning of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileReadOffsetFrmBOF(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetFrmBOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_038
* DESCRIPTION	:	Read file with few offsets from EOF
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileReadOffsetFrmEOF(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetFrmEOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_039
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*                   (From BOF)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadEveryNthByteFrmBOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_040
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*						(This is from EOF)		
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadEveryNthByteFrmEOF((const tPS8)OEDT_CARD_WRITEFILE, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CARD_CommonFSGetFileCRC( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CARD_CFS_041
* DESCRIPTION	:	Get CRC value
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CARD_CommonFSGetFileCRC(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetFileCRC((const tPS8)OEDT_CARD_WRITEFILE, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSReadAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_042
* DESCRIPTION:  Read data asyncronously from a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSReadAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSReadAsync((tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSLargeReadAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_043
* DESCRIPTION:  Read data asyncronously from a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSLargeReadAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeReadAsync((tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_044
* DESCRIPTION:  Write data asyncronously to a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteAsync((tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSLargeWriteAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_045
* DESCRIPTION:  Write data asyncronously to a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSLargeWriteAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeWriteAsync((tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteOddBuffer()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_046
* DESCRIPTION:  Write data to an odd buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteOddBuffer(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteOddBuffer((tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteFileWithInvalidSize()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_047
* DESCRIPTION:  Write data to a file with invalid size
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteFileWithInvalidSize(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileWithInvalidSize
				(
					(tPS8)OEDT_CARD_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteFileInvalidBuffer()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_048
* DESCRIPTION:  Write data to a file with invalid buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteFileInvalidBuffer(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileInvalidBuffer
				(
					(tPS8)OEDT_CARD_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteFileStepByStep()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_049
* DESCRIPTION:  Write data to a file stepwise
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteFileStepByStep(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileStepByStep
				(
					(tPS8)OEDT_CARD_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSGetFreeSpace()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_050
* DESCRIPTION:  Get free space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSGetFreeSpace(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSGetFreeSpace((tPS8)OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSGetTotalSpace()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_051
* DESCRIPTION:  Get total space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSGetTotalSpace(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSGetTotalSpace((tPS8)OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileOpenCloseNonExstng()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_052
* DESCRIPTION:  Open a non existing file 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileOpenCloseNonExstng(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseNonExstng((tPS8)OEDT_CARD_CFS_DUMMYFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSDelWithoutClose()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_053
* DESCRIPTION:  Delete a file without closing 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileDelWithoutClose(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileDelWithoutClose( (tPS8)OEDT_CARD_CFS_TESTFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSSetFilePosDiffOff()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_054
* DESCRIPTION:  Set file position to different offsets
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSSetFilePosDiffOff(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSSetFilePosDiffOff((tPS8)OEDT_CARD_CFS_COMMONFILE,TRUE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSCreateFileMultiTimes()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_055
* DESCRIPTION:  create file multiple times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSCreateFileMultiTimes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSCreateFileMultiTimes((tPS8) OEDT_CARD_CFS_COMMONFILE,TRUE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileCreateUnicodeName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_056
* DESCRIPTION:  create file with unicode characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateUnicodeName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateUnicodeName( (tPS8)OEDT_CARD_CFS_UNICODEFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSCreateInvalName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_057
* DESCRIPTION:  create file with invalid characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateInvalName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateInvalName((tPS8) OEDT_CARD_CFS_INVALCHAR_FILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileCreateLongName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_058
* DESCRIPTION:  create file with long file name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateLongName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateLongName((tPS8) OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileCreateDiffModes()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_059
* DESCRIPTION:  create file with different modes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateDiffModes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateDiffModes((tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileCreateInvalPath()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_060
* DESCRIPTION:  Create file with invalid path
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileCreateInvalPath(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateInvalPath((tPS8) OEDT_CARD_CFS_INVALIDFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileStringSearch()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_061
* DESCRIPTION:  Search for string 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileStringSearch(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSStringSearch((tPS8) OEDT_CARD_CFS_COMMONFILE,TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileOpenDiffModes()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_062
* DESCRIPTION:  File Open different modes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileOpenDiffModes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenDiffModes	((tPS8) OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_ommonFSFileReadAccessCheck()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_063
* DESCRIPTION:  Read access check
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileReadAccessCheck(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileReadAccessCheck((const tPS8) OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileWriteAccessCheck()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_064
* DESCRIPTION:  Write access check
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileWriteAccessCheck(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileWriteAccessCheck((const tPS8) OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileReadSubDir()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_065
* DESCRIPTION:  Read from sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileReadSubDir(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileReadSubDir((const tPS8) OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileWriteSubDir()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_066
* DESCRIPTION:  Write to sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFileWriteSubDir(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileWriteSubDir((const tPS8) OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof1KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_067
* DESCRIPTION:  Read/Write access time for 1kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof1KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1KbFile((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof10KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_068
* DESCRIPTION:  Read/Write access time for 10kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof10KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof10KbFile((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof100KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_069
* DESCRIPTION:  Read/Write access time for 100kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof100KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof100KbFile ((const tPS8) OEDT_CARD_CFS_COMMONFILE	);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof1MBbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_070
* DESCRIPTION:  Read/Write access time for 1MB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof1MBbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1MBFile( (const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}


/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof1KbFilefor1000times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_071
* DESCRIPTION:  Read/Write access time for 1kb file for 1000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof1KbFilefor1000times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1KbFilefor1000times((const tPS8)OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof10KbFilefor10000times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_072
* DESCRIPTION:  Read/Write access time for 10kb file for 10000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof10KbFilefor1000times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof10KbFilefor1000times((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof100KbFilefor100times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_073
* DESCRIPTION:  Read/Write access time for 100kb file for 100 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof100KbFilefor100times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof100KbFilefor100times	((const tPS8)OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
	
}


/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSTimeMeasureof1MBFilefor16times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_074
* DESCRIPTION:  Read/Write access time for 1MB file for 16 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSTimeMeasureof1MBFilefor16times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1MBFilefor16times
			    ( 
				(const tPS8)OEDTTEST_C_STRING_DEVICE_CARD, (const tPS8)OEDT_CARD_CFS_COMMONFILE
				);

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSRenameFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_075
* DESCRIPTION:  Rename file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSRenameFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSRenameFile((const tPS8) OEDTTEST_C_STRING_DEVICE_CARD);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteFrmBOF()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_076
* DESCRIPTION:  Write from BOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteFrmBOF(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFrmBOF ( (const tPS8)OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteFrmEOF()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_077
* DESCRIPTION:  Write from EOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteFrmEOF(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFrmEOF((const tPS8) OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSLargeFileRead()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_078
* DESCRIPTION:  Large file read
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSLargeFileRead(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeFileRead ((const tPS8) OEDT_CARD_CFS_COMMONFILE, TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSLargeFileWrite()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_079
* DESCRIPTION:  Large file write
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSLargeFileWrite(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeFileWrite((const tPS8) OEDT_CARD_CFS_COMMONFILE);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSOpenCloseMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_080
* DESCRIPTION:  Open/Close from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSOpenCloseMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSOpenCloseMultiThread((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_081
* DESCRIPTION:  Writ from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteMultiThread((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSWriteReadMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_082
* DESCRIPTION:  Read/Write from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSWriteReadMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteReadMultiThread((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSReadMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_083
* DESCRIPTION:  Read from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSReadMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSReadMultiThread((const tPS8) OEDT_CARD_CFS_COMMONFILE	);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFormat()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_084
* DESCRIPTION:  Format FS
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSFormat(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFormat((const tPS8) OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSCheckDisk()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_085
* DESCRIPTION:  Check Disk
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSCheckDisk(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSCheckDisk((const tPS8) OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSPerformance_Diff_BlockSize()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_086
* DESCRIPTION:  Read Write Performance check for different block sizes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CARD_CommonFSPerformance_Diff_BlockSize(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSRW_Performance_Diff_BlockSize((const tPS8) OEDT_CARD_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileGetExt()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_087
* DESCRIPTION:  Read file/directory contents 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32CARD_CommonFSFileGetExt(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt( (const tPS8)OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CARD_CommonFSFileGetExt2()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_CFS_088
* DESCRIPTION:  Read file/directory contents with 2 IOCTL 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32CARD_CommonFSFileGetExt2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt2( (const tPS8)OEDTTEST_C_STRING_DEVICE_CARD );
	return u32Ret;
}




/*****************************************************************************
* FUNCTION:         u32CARD_CommonFS_CardState()
* PARAMETER:       none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:        TU_OEDT_CARD_CFS_091
* DESCRIPTION:  Get the card state
* HISTORY:            Created by anc2hi, May 28, 2010
******************************************************************************/
tU32 u32CARD_CommonFS_CardState(tVoid)
{
   OSAL_trIOCtrlCardState cardstate = { 0 };
   tU32 u32Ret = 0; // u32Ret to identify the error
   OSAL_tIODescriptor hFd = 0;
   tS32 ercd = 0;
   hFd = OSAL_IOOpen( ( tCString )OEDTTEST_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE );

   if ( hFd == OSAL_ERROR )
   {
      u32Ret = 1;
   }
   else
   {
      if 
      ( 
         ( tU32 )OSAL_ERROR 
         == 
         ( tU32 )OSAL_s32IOControl( hFd, OSAL_C_S32_IOCTRL_CARD_STATE, ( tS32 )&cardstate ) 
      )
      {
         u32Ret = 2;
      }
      else
      {
     
         OEDT_HelperPrintf(TR_LEVEL_USER_1," bHW_WriteProtected   %d\n", cardstate.bHW_WriteProtected);
         
         OEDT_HelperPrintf(TR_LEVEL_USER_1," bSW_WriteProtected   %d\n", cardstate.bSW_WriteProtected);
            
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"(bMounted             %d)\n", cardstate.bMounted);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"(u16UncleanUnmountCnt %d)\n", cardstate.u16UncleanUnmountCnt);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"(u16UncleanWriteCnt   %d)\n", cardstate.u16UncleanWriteCnt);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"(u16UncleanReadCnt    %d)\n", cardstate.u16UncleanReadCnt);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1," u8ManufactureId      0x%02X\n", (tU32)cardstate.u8ManufactureId);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1," u32SerialNumber      0x%08X\n", (tU32)cardstate.u32SerialNumber);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1," u8SDCardSpecVersion  %d\n", cardstate.u8SDCardSpecVersion);
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1," u8CIDRegister        ");
       

         for (ercd=0; ercd<16; ercd++)
         {
            OEDT_HelperPrintf(TR_LEVEL_USER_1,"%02X ", cardstate.u8CIDRegister[ercd]);
         
         }
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"\n");
       
     //  sprintf(&cBuf[0]," u64CardSize         0x%016lX\n", (tU32)cardstate.u64CardSize);

         OEDT_HelperPrintf(TR_LEVEL_USER_1," u64CardSize High    0x%08lX\n", (tU32)(cardstate.u64CardSize >> 32));
       
         OEDT_HelperPrintf(TR_LEVEL_USER_1," u64CardSize Low     0x%08lX\n", (tU32)(cardstate.u64CardSize &0xFFFFFFFF));
       
      }

      if ( OSAL_s32IOClose( hFd ) == OSAL_ERROR )
      {
         u32Ret += 4;
      }
   }
   return u32Ret;
}
/* EOF */																	

