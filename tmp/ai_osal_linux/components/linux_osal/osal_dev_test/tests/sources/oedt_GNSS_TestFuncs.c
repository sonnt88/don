/******************************************************************************
 *FILE         : oedt_GNSS_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmmeWork
 *
 *DESCRIPTION  : This Is The Source File Of GNSS Proxy OEDT Test Functions
 *
 * 
 *AUTHOR       : Sanjay G(RBEI/ECF5)
 *
 *COPYRIGHT    : (C) COPYRIGHT RBEI - All Rights Reserved 
 *------------------------------------------------------------------------------
 *HISTORY               |    Author      | Version  |     Date
 *                      |   Sanjay G     |   1.0    | 30/April/2013
 *                      |   Deepak       |   1.1    | 19/Feb/2015
 -------------------------------------------------------------------------------
********************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#define GPS_PROXY_THREAD_WAIT_IN_MS_BEFORE_QUERY_AGAIN      (100)
#define GPS_PROXY_MAX_DEFAULT_TIME_WAIT_IN_MS_FOR_RECORD    (1500)
/* These bit fields are used keep track of received NMEA messages */
#define GNSS_OEDT_GPGGA_ID         (0x00000001)
#define GNSS_OEDT_GPGSA_ID         (0x00000002)
#define GNSS_OEDT_GPGSV_ID         (0x00000004)
#define GNSS_OEDT_GPRMC_ID         (0x00000008)
//#define GNSS_OEDT_GPVTG_ID         (0x00000010)
//#define GNSS_OEDT_PSTMKFCOV_ID     (0x00000020)
#define GNSS_OEDT_PSTMPV_ID        (0x00000040)
#define GNSS_OEDT_PSTMPVQ_ID       (0x00000080)
#define GNSS_OEDT_CHECKSUM_ERROR   (0x80000000)

#define GNSS_OEDT_NMEA_MSG_LIST_THREAD_NAME ("GnssNmeaLstThread")
#define GNSS_OEDT_NMEA_LIST_THREAD_STACK_SIZE (2048)

#define GNSS_OEDT_NMEA_LIST_EVENT_NAME ("GnssNmeaOedtList")
#define GNSS_NMEA_LIST_WAIT_TIME_MS (15000)

#define GNSS_OEDT_NMEA_LIST_PASSED  (0x000001)
#define GNSS_OEDT_NMEA_LIST_FAILED  (0x000002)

/* After firmware update is released, again the expected list has to be configured.
 * If configuration of Teseo changes, this has to be updated.  */
#define GNSS_PROXY_OEDT_EXPEC_NMEA_LIST ( (GNSS_OEDT_GPGGA_ID)| \
                                          (GNSS_OEDT_GPGSA_ID)| \
                                          (GNSS_OEDT_GPGSV_ID)| \
                                          (GNSS_OEDT_GPRMC_ID)| \
                                          /*(GNSS_OEDT_GPVTG_ID)| */ \
                                          /*(GNSS_OEDT_PSTMKFCOV_ID) ) */ \
                                          (GNSS_OEDT_PSTMPV_ID)| \
                                          (GNSS_OEDT_PSTMPVQ_ID) )

/* Location of teseo binaries for firmware update */
#define X_LOADER_BIN_PATH "/home/root/teseo/flasher.bin"
#define TESEO_FW_BIN_PATH "/home/root/teseo/teseofw.bin"

#define FIRMWARE_UPDATE_BUFF_SIZE (1024 * 16) /* 16KB  */
#define GNSS_OEDT_MAX_IOOPN_CLS_CNT 20

/* GNSS error codes */
#define GNSS_NO_ERROR                              (0)
#define GNSS_IOOPEN_ERROR                          (1)
#define GNSS_IOCLOSE_ERROR                         (2)
#define GNSS_IOCTRL_SET_EPOCH_ERROR                (3)
#define GNSS_IOCTRL_GET_EPOCH_ERROR                (4)
#define GNSS_GET_EPOCH_VALUE_MISMATCH              (5)
#define GNSS_GET_EPOCH_NEW_DATE_GEN_ERROR          (6)
#define GNSS_LAST_SAT_SYS_USED_FILE_DOESNOT_EXIST_ERROR  0x40 


/* Constants for months and number of days in non-leap and leap years */
#define NO_OF_MONTHS_IN_A_YEAR                        (12)

/*To fix Lint
#define NO_OF_DAYS_NON_LEAP_YEAR                      (tS32)(365)
#define NO_OF_DAYS_LEAP_YEAR                          (tS32)(366)
*/

#define MONTH_JAN                (1)
#define MONTH_FEB                (2)

/* To fix Lint 
#define MONTH_MAR                (3)
#define MONTH_APR                (4)
#define MONTH_MAY                (5)
#define MONTH_JUN                (6)
#define MONTH_JUL                (7)
#define MONTH_AUG                (8)
#define MONTH_SEP                (9)
#define MONTH_OCT                (10)
#define MONTH_NOV                (11)
#define MONTH_DEC                (12)
*/



#include "osal_if.h"

#define OEDT_GNSS_LAST_SATSYS_USED_FILENAME OSAL_C_STRING_DEVICE_FFS3"/lastSatSysUsed.dat"
#define OEDT_GNSS_GPS_INDEX                        0
#define OEDT_GNSS_GLONASS_INDEX                    1
#define OEDT_GNSS_SET_SAT_SYS_LIMIT                2
#define OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_227    1
#define OEDT_GNSS_SET_SAT_SYS_LMT_CFG_BLK_227      2
#define OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_200    4
#define OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK        14
#define OEDT_GNSS_SET_SAT_SYS_LMT_CFG_BLK          21




/*Local Function Declarations*/
static tS32 s32GetNoOfRecordsAvailable(OSAL_tIODescriptor hDevHandle);
static tS32 s32SetRequiredRecordFileds(OSAL_tIODescriptor hDevHandle);
static tVoid  GnssOedt_vNmeaListThread(tPVoid pVDummyArg );
static tU32 u32SetSatSys( tU32 u32Arg );
static tU32 u32GetSatSys( tVoid );
static tS32 s32DiagSetSatSys( tU32 u32Arg );
static tS32 s32DiagReadSatSysfromFile( tVoid );
static tU32 GnssOedt_u32GetTeseoFlasherCrc32( FILE *fp, tU32 *pu32FinalCrc, tU32 u32ImageLength );


static tBool bCheckDateLiesInWeek( const OSAL_trGnssTimeUTC * rStartDateOfWeek, const OSAL_trGnssTimeUTC * rDateToCheck );
static tBool bCheckLeapYear( tU16 u16Year );
static tS32 s32AddDaysToDate( OSAL_trGnssTimeUTC * rUTCDate, tS32 s32AddDays );



/* Global variables */

tU32 u32NmeaListRetVal;
OSAL_tEventHandle hGnssOedtNmeaListEvent;
static tS32  s32SendBinToTeseo(FILE *fp);
static tU32  u32_crc32(tU32 crc32val, const char *buf, int len);
static tU32 GnssOedt_u32GetTeseoFwCrc32( FILE *fp, tU32 *pu32FinalCrc, tU32 u32ImageLength );

OSAL_tIODescriptor hTeseoDevHandle = 0;    /*-----------changed handle name to remove lint-----------*/
static tU8 u8TesFwBuff[FIRMWARE_UPDATE_BUFF_SIZE];

OSAL_tIODescriptor hDeviceHandle = 0;

extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);


static tU32 crc32_tab[] = {
   0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
   0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
   0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
   0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
   0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
   0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
   0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
   0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
   0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
   0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
   0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
   0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
   0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
   0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
   0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
   0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
   0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
   0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
   0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
   0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
   0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
   0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
   0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
   0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
   0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
   0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
   0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
   0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
   0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
   0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
   0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
   0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
   0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
   0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
   0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
   0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
   0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
   0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
   0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
   0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
   0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
   0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
   0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};


/*Function Definitions*/

/************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyBasiOpenClose(tVoid)
||
||DESCRIPTION :  OEDT Test Case-1
||               Checks Basic Open And Close Interfaces Of GNSS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
************************************************************************/

tU32  u32OedtGNSSProxyBasiOpenClose(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
      OSAL_s32ThreadWait(10);
      /*Close GPS Proxy*/
      if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal = 2;
      }
   }
   return(u32RetVal);
}

/************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyMultipleIOOpenIOClose(tVoid)
||
||DESCRIPTION :  OEDT Test Case-2
||               Performs multiple IO Open and Close back to back .
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
************************************************************************/

tU32  u32OedtGNSSProxyMultipleIOOpenIOClose(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 OpenClsCount;
   tU32 u32RetVal = 0;

   for(OpenClsCount=0;(OpenClsCount<GNSS_OEDT_MAX_IOOPN_CLS_CNT) &&(u32RetVal==0);OpenClsCount++)
   {
      /*Opens GNSS Proxy*/
      hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
      if ( hDevHandle == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
         u32RetVal=1;
      }
      else 
      {
         if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
            u32RetVal=2;
         }
      }
   }
   
   return u32RetVal ;  
}

/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyReOpen(tVoid)
||
||DESCRIPTION :  OEDT Test Case-3
||               Checks Whether GNSS Proxy Allowing Multiple Opens.If Allows
||               Then It will Be Consider As Error. 
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/

tU32  u32OedtGNSSProxyReOpen(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal += 1;
   }
   else
   {
      /*Open Successful*/
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "First time device open SUCCESS");
      OSAL_s32ThreadWait(10);
      /*Try To Open Again.Here Open Should Fail Since
        GPS Proxy Shouldn't Support Multiple Opens*/
     if(OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY )!= OSAL_ERROR)
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Re open, SUCCESS, expected "
              "it to FAIL");
        u32RetVal += 2;
        OSAL_s32IOClose( hDevHandle ) ;
     }
      else if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal +=4;
      }
   }

   return(u32RetVal);
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyReClose(tVoid)
||
||DESCRIPTION :  OEDT Test Case-4
||               Checks Whether GNSS Proxy Allowing Multiple Closes.If Allows
||               Then It will Be Consider As Error. 
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGNSSProxyReClose(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
      OSAL_s32ThreadWait(10);
      /*Close GPS Proxy*/
      if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "First time device close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal += 2;
      }
      else
      {
         OSAL_s32ThreadWait(10);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "First time device close SUCCESS");
         /*ReClose GPS Proxy
         This Should Fail.Since GPS Proxy Shouldn't Allow Multiple Closes*/
         if( OSAL_s32IOClose( hDevHandle ) != OSAL_ERROR )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Re-closure, SUCCESS, expected "
                  "it to FAIL");
            u32RetVal += 4;
         }
      }
   }
   return(u32RetVal);
}


/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyBasicRead(tVoid)
||
||DESCRIPTION :  OEDT Test Case-5
||               Checks Read Interface Of GNSS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/

tU32  u32OedtGNSSProxyBasicRead(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   OSAL_trGnssFullRecord rGnssFullRecord;
   tU32 u32RetVal = 0;
   tS32 s32LoopCnt=0;

   OSAL_pvMemorySet(&rGnssFullRecord,0,sizeof(OSAL_trGnssFullRecord));

   /*Opens GNSS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
     if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                OSAL_u32ErrorCode());
        u32RetVal += 8;
     }   
     else if ( OSAL_ERROR == OSAL_s32IORead(hDevHandle,(tPS8)&rGnssFullRecord,sizeof(OSAL_trGnssFullRecord)))
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, " Read failed " );
        u32RetVal += 4;
     }
     else
     {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "----Read passed------");
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "----Position------lat-%f, lon-%f, Alt-%f, Geo-sep-%f",
                                        rGnssFullRecord.rPVTData.f64Latitude,
                                        rGnssFullRecord.rPVTData.f64Longitude,
                                        rGnssFullRecord.rPVTData.f32AltitudeWGS84,
                                        rGnssFullRecord.rPVTData.f32GeoidalSeparation );

         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                           "----time---- DD-MM-YY-> %lu:%lu:%lu, hh:mm:ss:ms-> %lu:%lu:%lu:%lu",
                           rGnssFullRecord.rPVTData.rTimeUTC.u8Day,
                           rGnssFullRecord.rPVTData.rTimeUTC.u8Month,
                           rGnssFullRecord.rPVTData.rTimeUTC.u16Year,
                           rGnssFullRecord.rPVTData.rTimeUTC.u8Hour,
                           rGnssFullRecord.rPVTData.rTimeUTC.u8Minute,
                           rGnssFullRecord.rPVTData.rTimeUTC.u8Second,
                           rGnssFullRecord.rPVTData.rTimeUTC.u16Millisecond);

         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                              "----Velocity---- N:E:V-> %f:%f:%f",
                              rGnssFullRecord.rPVTData.f32VelocityNorth,
                              rGnssFullRecord.rPVTData.f32VelocityEast,
                              rGnssFullRecord.rPVTData.f32VelocityUp );

         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                             "----Solution mode---- fix type %d,Quality %d,Sats-Visible: %d,Sats Used: %d,Sats Recvd: %d, Sat-Sys_Used:%x",
                             rGnssFullRecord.rPVTData.rFixStatus.enMode,
                             rGnssFullRecord.rPVTData.rFixStatus.enQuality,
                             rGnssFullRecord.rPVTData.u16SatsVisible,
                             rGnssFullRecord.rPVTData.u16SatsUsed,
                             rGnssFullRecord.rPVTData.u16Received,
                             rGnssFullRecord.rPVTData.u8SatSysUsed);

         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                        "----error estimations---- poscov N-E-u:%f:%f:%f velcov N-E-u:%f:%f:%f PDop: %f, TDop: %f,HDop:%f GDop: %f, VDop: %f",
                        rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem0,
                        rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem5,
                        rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem10,
                        rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem0,
                        rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem5,
                        rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem10,
                        rGnssFullRecord.rPVTData.f32PDOP,
                        rGnssFullRecord.rPVTData.f32TDOP,
                        rGnssFullRecord.rPVTData.f32HDOP,
                        rGnssFullRecord.rPVTData.f32GDOP,
                        rGnssFullRecord.rPVTData.f32VDOP);

         for (s32LoopCnt = 0; s32LoopCnt < OSAL_C_U8_GNSS_NO_CHANNELS; s32LoopCnt++)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                           "----Ch sts---- num:%2d \t Ch-Id:%03d \t Ch-Status:%02d \t azm:%.3f \t ele:%.3f \t C/N:%02d",
                           (s32LoopCnt+1),
                           rGnssFullRecord.rChannelStatus[s32LoopCnt].u16SvID,
                           rGnssFullRecord.rChannelStatus[s32LoopCnt].u16SatStatus,
                           rGnssFullRecord.rChannelStatus[s32LoopCnt].f32Azimuthal,
                           rGnssFullRecord.rChannelStatus[s32LoopCnt].f32Elevation,
                           rGnssFullRecord.rChannelStatus[s32LoopCnt].u8CarrierToNoiseRatio);
         }
     }
     /*Close GNSS Proxy*/
     if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d", OSAL_u32ErrorCode());
        u32RetVal += 2;
     }
   }
   return(u32RetVal);
}

/***************************************************************************
||FUNCTION    :  tU32  u32OedtGPSProxyBasicRead(tVoid)
||
||DESCRIPTION :  OEDT Test Case-6
||               Checks Read Interface Of GPS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGPSProxyBasicRead(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   OSAL_trGPSRecordHeader  rGpsHeader = {  0,0,0,0,
                                          {{0,0,0}}
                                        };
   OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
   tU32 au32Record[(OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE + 3) / 4] = {0};

   tU32 u32RetVal = 0;

   (tVoid)rReadRecordHeaderArgs; //to fix lint

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READONLY );
   if( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",
                                    OSAL_u32ErrorCode());
      u32RetVal += 1;
   }
   else
   {
      /*Open Successful*/

      /*Reading Records From OSAL GPS Driver Is Different From Other Sensors.
        1-->To Read Data From GPS First Application Has To Query For No.of Records Available.
            WhenEver This Is Done OSAL GPS Driver(dev_gps) Reads Data From GNSS Driver
            And Keeps One Copy Of Data And Return No Of Records Available As ONE(Only One Record
            Don't Expect More Since OSAL GPS Driver Doesn't Keep Stock Of Records With
            It i.e WhenEver Application Requests Then Only OSAL GPS Driver Reads Data From
            GNSS Driver Unlike Other Sensor OSAL Drivers )

        2-->In Step-1 OSAL GPS Driver Collected All The Data From GNSS Driver.But OSAL GPS Driver
            Provided An Option To The Application To Choose Which Data Fields It Wants.
            This Can Be Informed To OSAL GPS Driver With IOCTRL OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS.

        3-->Read Record Header(Only Header Not Data) Using OSAL_S32IORead Call.
        4-->Now Read The Interested Record By Using IOCTL:OSAL_C_S32_IOCTL_GPS_READ_RECORD
      */

      if(s32GetNoOfRecordsAvailable(hDevHandle) == OSAL_ERROR)
      {
         u32RetVal += 4;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Get no.of records Failed with error %d",
                                       OSAL_u32ErrorCode());
      }
     /*Tell OSAL GPS Driver That In sGPS Records*/
      else if(s32SetRequiredRecordFileds(hDevHandle) == OSAL_ERROR)
      {
         u32RetVal +=8 ;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "set record fields Failed with error %d",
                                    OSAL_u32ErrorCode());
      }
      else if(OSAL_s32IORead (hDevHandle,(tS8 *)&rGpsHeader,sizeof(rGpsHeader)) == OSAL_ERROR )
      {
         u32RetVal +=16;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "IORead Failed with error %d",OSAL_u32ErrorCode());
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Time stamp = %u", rGpsHeader.u32TimeStamp);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "No. of records to be read = %u", rGpsHeader.u32RecordId);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Over all bytes to be read = %d", rGpsHeader.s32Size);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "No. of fields to be read = %d", rGpsHeader.s32NoFields);

         rReadRecordHeaderArgs.pBuffer = au32Record;
         rReadRecordHeaderArgs.u32RecordId = rGpsHeader.u32RecordId;   /*Record ID Which Need To Be Read*/

         if( OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GPS_READ_RECORD,
                                 (tS32)&rReadRecordHeaderArgs )== OSAL_ERROR )
         {
            u32RetVal +=32;
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "IOCTL read record failed with error %d",
                                    OSAL_u32ErrorCode());
         }

      }

      if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close failed with error %d",
                                 OSAL_u32ErrorCode());
         u32RetVal += 2;
      }
   }
   return u32RetVal;
}

/* This supports u32OedtGPSProxyBasicRead */

static tS32 s32GetNoOfRecordsAvailable(OSAL_tIODescriptor hDevHandle)
{

   tS32 s32RetVal = OSAL_OK;
   tU32 u32TimeOutCounter=0;
   tU32 u32NoOfRecordsAvailable=0;
   tU8  u8Exit = 1;
   
   /*Get No.Of Records Available Using IOCTL.
     If Zero records Available Then Wait And Check Till Default TimeOut */
   do
   {
      if(OSAL_s32IOControl(hDevHandle, OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE,
                         (tS32)&u32NoOfRecordsAvailable ) != OSAL_ERROR )
      {
         if(u32NoOfRecordsAvailable < 1)
         {
            OSAL_s32ThreadWait(GPS_PROXY_THREAD_WAIT_IN_MS_BEFORE_QUERY_AGAIN);
            u32TimeOutCounter++;

            if(GPS_PROXY_MAX_DEFAULT_TIME_WAIT_IN_MS_FOR_RECORD <=
                  (u32TimeOutCounter * GPS_PROXY_THREAD_WAIT_IN_MS_BEFORE_QUERY_AGAIN))
            {//Oops!Waited Enough Time But Still No Record.Throw Error.

               u8Exit=0;
               OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Timeout occurred while getting records");
               s32RetVal = OSAL_ERROR;
            }
         }
         else
         {//No.Of Records Available Is Greater Than Zero.So Exit From Loop.
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Available records = %u", u32NoOfRecordsAvailable);
            u8Exit=0;
         }
      }
      else
      {//IOCOntrol Failed So Exit.
         u8Exit=0;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "IOCTL get no.of records available failed with error %d",
                                    OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
   } while( u8Exit );
   
   return  s32RetVal;
}
/* This supports u32OedtGPSProxyBasicRead */
static tS32 s32SetRequiredRecordFileds(OSAL_tIODescriptor hDevHandle)
{
   OSAL_trGPSRecordFields rGPSRecordFields = {0,0};
   tS32 s32RetVal = OSAL_OK;
   
   rGPSRecordFields.u32FieldSpecifiers = 0;
   rGPSRecordFields.u32FieldTypes= 0;

   rGPSRecordFields.u32FieldSpecifiers |= (OSAL_C_S32_GPS_FIELD_SPEC_TIME_UTC |
                                            OSAL_C_S32_GPS_FIELD_SPEC_POS_LLA |
                                            OSAL_C_S32_GPS_FIELD_SPEC_VEL_NED );
    
   rGPSRecordFields.u32FieldTypes|=(OSAL_C_S32_GPS_FIELD_TYPE_MEASURED_POSITION |
                                        OSAL_C_S32_GPS_FIELD_TYPE_TRACKING_DATA |
                                        OSAL_C_S32_GPS_FIELD_TYPE_VISIBLE_LIST  |
                                        OSAL_C_S32_GPS_FIELD_TYPE_SATELLITE_HEALTH);
                                  
   if ( OSAL_s32IOControl (hDevHandle,OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS,
                                              (tS32)&rGPSRecordFields ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "GPS_SET_RECORD_FIELDS failed with error %d",OSAL_u32ErrorCode());
      s32RetVal = OSAL_ERROR;
   }
   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," FieldSpecifiers = %d",
         rGPSRecordFields.u32FieldSpecifiers);
   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," FieldTypes = %d",
         rGPSRecordFields.u32FieldTypes);

   return  s32RetVal;
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyReadByPassingNullBufffer(tVoid)
||
||DESCRIPTION :  OEDT Test Case-7
||               Checks Read Interface Of GPS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGNSSProxyReadByPassingNullBufffer(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {
      OSAL_s32ThreadWait(10);
     
      if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                 OSAL_u32ErrorCode());
         u32RetVal += 8;
      } 
      /*Try To Read From OSAL GPS Driver By Passing NULL Buffer*/
      else if(OSAL_s32IORead (hDevHandle,(tS8 *)OSAL_NULL,0) != OSAL_ERROR )
      {  /*Open Successful*/
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device read with NULL buffer SUCCESSFUL,"
               "expected a FAILURE");
         u32RetVal += 4;
      }
      if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         u32RetVal += 2;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close Failed with error %d",OSAL_u32ErrorCode());
      }
   }

   return(u32RetVal);
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyIOCntInterfaceCheck(tVoid)
||
||DESCRIPTION :  OEDT Test Case-8
||               Checks Read Interface Of GPS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGNSSProxyIOCntInterfaceCheck(tVoid)
{

   OSAL_tIODescriptor hDevHandle;

   tU32 u32RetVal = 0;
   tU32 u32Arg = OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_GLONASS;
   OSAL_trGnssConfigData rGnssConfigData;

   OSAL_pvMemorySet(&rGnssConfigData, 0, sizeof(rGnssConfigData));

   (tVoid)rGnssConfigData; //to fix lint

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal += 1;
   }
   else
   {/*Open Successful*/
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "open success");

      if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS, ( tS32 )&u32Arg) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS failed %d",
                                                 OSAL_u32ErrorCode());
         u32RetVal += 4;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "SatSys Used %x", u32Arg);
      }
      if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS, ( tS32 )&u32Arg) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS failed %d",
                                                 OSAL_u32ErrorCode());
         u32RetVal += 8;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "SatSys Used %x", u32Arg);
      }
      if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA, ( tS32 )&rGnssConfigData) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA failed %d",
                                                 OSAL_u32ErrorCode());
         u32RetVal += 16;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                  "HwType %d, Sat-sys Supp %u, NumOfChan %u, GnssRecvBinVer:%x, UpdateFreq: %u, CRC:%x",
                  rGnssConfigData.enGnssHwType,
                  rGnssConfigData.u16GnssSatStatusSupported,
                  rGnssConfigData.u16NumOfChannels,
                  rGnssConfigData.u32GnssRecvBinVer,
                  rGnssConfigData.u8UpdateFrequency,
                  rGnssConfigData.u32GnssRecvFwCrc );
      }

      /*Close GPS Proxy*/
      if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal += 2;
      }
   }
   return ( u32RetVal );
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxyIOCntChkInvPar(tVoid)
||
||DESCRIPTION :  OEDT Test Case-9
||               Checks Read Interface Of GPS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGNSSProxyIOCntChkInvPar(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tS32 s32RetVal = 0;
   tU32 u32Arg = 0;

   (tVoid)u32Arg; //to fix lint

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      s32RetVal += 1;
   }
   else
   {/*Open Successful*/
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "open success");

      /* Disable all satellite systems- Not allowed */
      if(OSAL_ERROR != OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS, ( tS32 )&u32Arg) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "INV-Param OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal += 4;
      }

      /* Null param to set satellite systems- Not allowed */
      if(OSAL_ERROR != OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS, OSAL_NULL) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "INV-Param OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal += 8;
      }

      /* Get Sat Sys- Null Param */
      if(OSAL_ERROR != OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS, OSAL_NULL) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "INV-Param OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal += 16;
      }

      /* Get Config - Null Param */
      if(OSAL_ERROR != OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA, OSAL_NULL) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "INV-Param OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal += 32;
      }

      /*Close GPS Proxy*/
      if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "INV-Param Device Close failed with error %d",OSAL_u32ErrorCode());
         s32RetVal += 2;
      }
   }

   return ( (tU32)s32RetVal );
}
/************************************************************************
||FUNCTION    :  tU32  u32OedtGPSProxyBasiOpenClose(tVoid)
||
||DESCRIPTION :  OEDT Test Case-10
||               Checks Basic Open And Close Interfaces Of GPS Proxy.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
************************************************************************/
tU32  u32OedtGPSProxyBasiOpenClose(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
      OSAL_s32ThreadWait(10);
      /*Close GPS Proxy*/
      if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal = 2;
      }
   }
   return(u32RetVal);
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGPSProxyReOpen(tVoid)
||
||DESCRIPTION :  OEDT Test Case-11
||               Checks Whether GPS Proxy Allowing Multiple Opens.If Allows
||               Then It will Be Consider As Error.
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGPSProxyReOpen(tVoid)
{

   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {
      /*Open Successful*/
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "First time device open SUCCESS");
      OSAL_s32ThreadWait(10);
      /*Try To Open Again.Here Open Should Fail Since
        GPS Proxy Shouldn't Support Multiple Opens*/
     if(OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READONLY )!= OSAL_ERROR)
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Re open, SUCCESS, expected "
              "it to FAIL");
        u32RetVal += 4;
        OSAL_s32IOClose( hDevHandle ) ;
     }
      else if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal += 2;
      }
   }

   return(u32RetVal);
}
/***************************************************************************
||FUNCTION    :  tU32  u32OedtGPSProxyReClose(tVoid)
||
||DESCRIPTION :  OEDT Test Case-12
||               Checks Whether GPS Proxy Allowing Multiple Closes.If Allows
||               Then It will Be Consider As Error.
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  u32OedtGPSProxyReClose(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;

   /*Opens GPS Proxy*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READONLY );
   if( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
      OSAL_s32ThreadWait(10);
      /*Close GPS Proxy*/
      if( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "First time device close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal += 2;
      }
      else
      {
         OSAL_s32ThreadWait(10);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "First time device close SUCCESS");
         /*ReClose GPS Proxy
         This Should Fail.Since GPS Proxy Shouldn't Allow Multiple Closes*/
         if( OSAL_s32IOClose( hDevHandle ) != OSAL_ERROR )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Re-closure, SUCCESS, expected "
                  "it to FAIL");
            u32RetVal += 4;
         }
      }
   }
   return(u32RetVal);
}
/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_u32Teseo3FwUpdate(tVoid)
||
||DESCRIPTION :  OEDT Test Case
||               Do Teseo-3 firmware update
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32 GnssOedt_u32Teseo3FwUpdate(tVoid)
{

   FILE *Xldrfp;
   FILE *TesFwfp = OSAL_NULL;
   tS32 s32BtLdrSize = 0;
   tS32 s32FwImgSize = 0;
   tS32 s32RetVal = 0;
   trImageOptions rImageOptions;

   OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Firmware flashing started line %lu", __LINE__);

/* Open teseo boot loader */
   Xldrfp = fopen( X_LOADER_BIN_PATH, "r" );
   if ( OSAL_NULL == Xldrfp )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fopen failed for flasher.bin ");
      s32RetVal = 1;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
   }

   /* open Teseo firmware image */
   if ( 0 == s32RetVal)
   {
      TesFwfp = fopen( TESEO_FW_BIN_PATH, "r" );
      if ( OSAL_NULL == TesFwfp )
      {
          OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fopen failed for Teseo FW ");
          s32RetVal = 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }
   /* Get the size of boot loader */
   if ( ( Xldrfp != OSAL_NULL )&& (0 == s32RetVal))      /*-----------changes made to remove lint-----------*/
   {
      if( 0 != fseek( Xldrfp,0L, SEEK_END ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fseek failed for Boot loader");
         s32RetVal = 3;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }

   if ( ( Xldrfp != OSAL_NULL ) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      s32BtLdrSize = ftell (Xldrfp);
      if ( 0 == s32BtLdrSize )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Invalid bt-ldr size 0");
         s32RetVal = 4;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "btldr sz %lu line %lu",s32BtLdrSize, __LINE__ );
      }
      /* file position indicator to the Beginning */
      rewind(Xldrfp);
   }

   /* Get the size of firmware */
   if ( ( TesFwfp != OSAL_NULL) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      if( 0 != fseek( TesFwfp,0L, SEEK_END ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fseek failed for Firmware");
         s32RetVal = 5;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }
   if ( ( TesFwfp != OSAL_NULL) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      s32FwImgSize = ftell (TesFwfp);
      if ( 0 == s32FwImgSize )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Invalid Firmware size 0");
         s32RetVal = 6;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }

      /*file position indicator to the Beginning */
      rewind(TesFwfp);
   }

   /* Open GNSS device for flashing */
   if ( 0 == s32RetVal)
   {
      hTeseoDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_WRITEONLY );
      if ( hTeseoDevHandle == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
         s32RetVal = 8;
      }
   }
   
   if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_SET_CHIP_TYPE, (tS32)GNSS_CHIP_TYPE_TESEO_3 ))
   {
     OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTL_GNSS_SET_CHIP_TYPE failed %d",
                                             OSAL_u32ErrorCode());
 
      s32RetVal = 16;
   }

   if ( 0 == s32RetVal)
   {
      /* Get the CRC for Teseo boot loader */
      if ( OSAL_OK != GnssOedt_u32GetTeseoFlasherCrc32( Xldrfp ,&rImageOptions.u32ChkSum, (tU32)s32BtLdrSize ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Checksum calc failed for boot loader" );
                 s32RetVal = 15;
      }
   }

   /* Configuration to send Teseo Boot loader */
   if ( 0 == s32RetVal)
   {
      rImageOptions.enType = IMAGE_TESEO_FLASHER_BIN;
      rImageOptions.u32Size = (tU32)s32BtLdrSize;

      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "BL:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal = 9;
      }
   }
   /* Send Teseo Boot loader */
   if ( 0 == s32RetVal)
   {
      s32RetVal =  s32SendBinToTeseo(Xldrfp);
      if ( OSAL_OK != s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Failed to send Boot loader");
         s32RetVal = 10;
      }
   }
   if ( 0 == s32RetVal)
   {
      /* Get the CRC for Teseo Firmware */
      if ( OSAL_OK != GnssOedt_u32GetTeseoFwCrc32( TesFwfp ,&rImageOptions.u32ChkSum, (tU32)s32FwImgSize ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Checksum calc failed for firmware" );
                 s32RetVal = 11;
      }
   }

   if ( 0 == s32RetVal)
   {
      /* Configuration to send Teseo Firmware */
      rImageOptions.u32Size = (tU32)s32FwImgSize;
      rImageOptions.enType = IMAGE_TESEO_FIRMWARE;
      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FW:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal = 12;
      }
   }


   /* Send Teseo Firmware */
   if ( 0 == s32RetVal)
   {
      s32RetVal = s32SendBinToTeseo(TesFwfp);
      if ( OSAL_OK != s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Failed to send Firmware");
         s32RetVal = 13;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Flashing completed successfully");
      }
   }


   /*Close GNSS Proxy*/
   if ( OSAL_s32IOClose( hTeseoDevHandle ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
      s32RetVal = 14;
   }
   return (tU32)s32RetVal;
}


/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_u32Teseo2FwUpdate(tVoid)
||
||DESCRIPTION :  This can be used for GNSS firmware update
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :  |   Author      | Version  |     Date
||               |   dku6kor     | 1.1      |  20/Feb/2015 
***************************************************************************/
tU32 GnssOedt_u32Teseo2FwUpdate(tVoid)
{

   FILE *Xldrfp;
   FILE *TesFwfp = OSAL_NULL;
   tS32 s32BtLdrSize = 0;
   tS32 s32FwImgSize = 0;
   tS32 s32RetVal = 0;
   trImageOptions rImageOptions;

   OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Firmware flashing started line %lu", __LINE__);

/* Open teseo boot loader */
   Xldrfp = fopen( X_LOADER_BIN_PATH, "r" );
   if ( OSAL_NULL == Xldrfp )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fopen failed for flasher.bin ");
      s32RetVal = 1;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
   }

   /* open Teseo firmware image */
   if ( 0 == s32RetVal)
   {
      TesFwfp = fopen( TESEO_FW_BIN_PATH, "r" );
      if ( OSAL_NULL == TesFwfp )
      {
          OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fopen failed for Teseo FW ");
          s32RetVal = 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }
   /* Get the size of boot loader */
   if ( ( Xldrfp != OSAL_NULL )&& (0 == s32RetVal))      /*-----------changes made to remove lint-----------*/
   {
      if( 0 != fseek( Xldrfp,0L, SEEK_END ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fseek failed for Boot loader");
         s32RetVal = 3;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }

   if ( ( Xldrfp != OSAL_NULL ) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      s32BtLdrSize = ftell (Xldrfp);
      if ( 0 == s32BtLdrSize )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Invalid bt-ldr size 0");
         s32RetVal = 4;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "btldr sz %lu line %lu",s32BtLdrSize, __LINE__ );
      }
      /* file position indicator to the Beginning */
      rewind(Xldrfp);
   }

   /* Get the size of firmware */
   if ( ( TesFwfp != OSAL_NULL) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      if( 0 != fseek( TesFwfp,0L, SEEK_END ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fseek failed for Firmware");
         s32RetVal = 5;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }
   }
   if ( ( TesFwfp != OSAL_NULL) && (0 == s32RetVal))     /*-----------changes made to remove lint-----------*/
   {
      s32FwImgSize = ftell (TesFwfp);
      if ( 0 == s32FwImgSize )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Invalid Firmware size 0");
         s32RetVal = 6;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "line %lu", __LINE__ );
      }

      /*file position indicator to the Beginning */
      rewind(TesFwfp);
   }

#if 1
   tU32 u32crc = 0;

   /* Open GNSS device for flashing */
   if ( 0 == s32RetVal)
   {
      hTeseoDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_WRITEONLY );
      if ( hTeseoDevHandle == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
         s32RetVal = 8;
      }
   }
   if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC, (tS32)&u32crc) )
   {
     OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC failed %d",
                                             OSAL_u32ErrorCode());
 
   }
   else
   {

      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "CRC recvd %x", u32crc);

   }

   /* Configuration to send Teseo Boot loader */
   if ( 0 == s32RetVal)
   {
      rImageOptions.enType = IMAGE_TESEO_FLASHER_BIN;
      rImageOptions.u32Size = (tU32)s32BtLdrSize;

      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "BL:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal = 9;
      }
   }
   /* Send Teseo Boot loader */
   if ( 0 == s32RetVal)
   {
      s32RetVal =  s32SendBinToTeseo(Xldrfp);
      if ( OSAL_OK != s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Failed to send Boot loader");
         s32RetVal = 10;
      }
   }
   if ( 0 == s32RetVal)
   {
      /* Get the CRC for Teseo Firmware */
      if ( OSAL_OK != GnssOedt_u32GetTeseoFwCrc32( TesFwfp ,&rImageOptions.u32ChkSum, (tU32)s32FwImgSize ) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Checksum calc failed" );
                 s32RetVal = 11;
      }
   }

   if ( 0 == s32RetVal)
   {
      /* Configuration to send Teseo Firmware */
      rImageOptions.u32Size = (tU32)s32FwImgSize;
      rImageOptions.enType = IMAGE_TESEO_FIRMWARE;
      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FW:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                 OSAL_u32ErrorCode());
         s32RetVal = 12;
      }
   }


   /* Send Teseo Firmware */
   if ( 0 == s32RetVal)
   {
      s32RetVal = s32SendBinToTeseo(TesFwfp);
      if ( OSAL_OK != s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Failed to send Firmware");
         s32RetVal = 13;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Flashing completed successfully");
      }
   }


   /*Close GNSS Proxy*/
   if ( OSAL_s32IOClose( hTeseoDevHandle ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
      s32RetVal = 14;
   }
#endif

   return (tU32)s32RetVal;
}
/***************************************************************************
||FUNCTION    :  tS32  s32SendBinToTeseo(tVoid)
||
||DESCRIPTION :  This uses write calls to transfer binary image
||PARAMETER   :  handle to binary file
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
static tS32  s32SendBinToTeseo(FILE *fp)
{

   tS32 s32RetVal =  OSAL_OK;
   if( fp != OSAL_NULL  )
   {
      do
      {
         s32RetVal = (tS32)(fread( u8TesFwBuff, 1, FIRMWARE_UPDATE_BUFF_SIZE,fp ));
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "fread ret %d", s32RetVal);
         if( 0 >= s32RetVal )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "fread Failed");
            if (0 != feof(fp))
               s32RetVal = OSAL_ERROR;
            break;
         }

         if (s32RetVal != OSAL_s32IOWrite( hTeseoDevHandle, (tPCS8)u8TesFwBuff, (tU32)s32RetVal))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FAIL OSAL_s32IOWrite err %lu", OSAL_u32ErrorCode() );
            s32RetVal = OSAL_ERROR;
            break;
         }
          else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Success OSAL_s32IOWrite bytes %d", s32RetVal );
            s32RetVal =  OSAL_OK;
         }
      }while ( 0 == feof(fp) ) ;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "fp is a null file pointer");
      s32RetVal = OSAL_ERROR;
   }

   return s32RetVal;
}
/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_s32TesFwUpdateStub(tVoid)
||
||DESCRIPTION :  This can be used for GNSS firmware update testing using stub only
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32 GnssOedt_s32TesFwUpdateStub(tVoid)
{

   tS32 s32RetVal = 0;
   tU32 u32Arg = OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_GLONASS;
   //OSAL_trGnssConfigData rGnssConfigData;                /*----variable not used so commented to remove lint----*/

   char xloader[] = "I am sending X-Loader and alphabets: ABCDEFGHIJKLMNOPQRSTUVWXYZ The End :-)";
   char xloader2[] = "Second Piece of X-Loader and this will be the last piece of boot loader The End :-)";
   char TeseFw[] = "I am Transferring Teseo Firmware and Numbers 1234567890 - and alphabets: ABCDEFGHIJKLMNOPQRSTUVWXYZ. The End :-)";
   char TeseFw2[] = "Second Piece of Teseo FW and Numbers 1234567890 - and alphabets: ABCDEFGHIJKLMNOPQRSTUVWXYZ. The End :-)";

   trImageOptions rImageOptions;

   OSAL_pvMemorySet(&rImageOptions, 0, sizeof(rImageOptions));

   (tVoid)rImageOptions; //to fix lint

   rImageOptions.enType = IMAGE_TESEO_FLASHER_BIN;
   rImageOptions.u32Size = strlen(xloader)+strlen(xloader2);

   /*Opens GPS Proxy*/
   hTeseoDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_WRITEONLY );
   if ( hTeseoDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      s32RetVal += 1;
   }
   else
   {/*Open Successful*/
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "open success");

      
      /* Send Boot loader options */
      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions))
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FW:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                    OSAL_u32ErrorCode());
         s32RetVal += 4;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "BL:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE success", u32Arg);
      }
      

      /* Send Boot loader write 1 */
      if (0 == s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Write data size: %d", strlen(xloader));
         if ((tS32)strlen(xloader) != OSAL_s32IOWrite(hTeseoDevHandle,(tPCS8)xloader,strlen(xloader)))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FAIL OSAL_s32IOWrite err %lu", OSAL_u32ErrorCode() );
            s32RetVal += 8;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Success OSAL_s32IOWrite" );
         }
      }
      /* Send Boot loader write 2 */
      if (0 == s32RetVal)
      {
         if ((tS32)strlen(xloader2) != OSAL_s32IOWrite(hTeseoDevHandle,(tPCS8)xloader2,strlen(xloader2)))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FAIL OSAL_s32IOWrite err %lu", OSAL_u32ErrorCode() );
            s32RetVal += 16;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Success OSAL_s32IOWrite" );
         }
      }

      /* Send Firmware image options */
      if (0 == s32RetVal)
      {
         rImageOptions.enType = IMAGE_TESEO_FIRMWARE;
         rImageOptions.u32Size = strlen(TeseFw)+strlen(TeseFw2);
         rImageOptions.u32ChkSum = 0x01122;

         if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE, (tS32)&rImageOptions) )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FW:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE failed %d",
                                                    OSAL_u32ErrorCode());
            s32RetVal += 32;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FW:OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE success", u32Arg);
         }
      }

      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Fw image data size: %d", strlen(TeseFw));

      /* Send Firmware image write 1 */
      if (0 == s32RetVal)
      {
         if ((tS32)strlen(TeseFw) != OSAL_s32IOWrite(hTeseoDevHandle,(tPCS8)TeseFw,strlen(TeseFw)))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FAIL OSAL_s32IOWrite err %lu", OSAL_u32ErrorCode() );
            s32RetVal += 64;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Success OSAL_s32IOWrite" );
         }
      }

      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Fw image data size: %d", strlen(TeseFw2));

      /* Send Boot loader write 2 */
      if (0 == s32RetVal)
      {
         if ((tS32)strlen(TeseFw2) != OSAL_s32IOWrite(hTeseoDevHandle,(tPCS8)TeseFw2,strlen(TeseFw2)))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FAIL OSAL_s32IOWrite err %lu", OSAL_u32ErrorCode() );
            s32RetVal += 128;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Success OSAL_s32IOWrite" );
         }
      }

      /*Close GNSS Proxy*/
      if ( OSAL_s32IOClose( hTeseoDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         s32RetVal += 2;
      }
   }
   return (tU32)s32RetVal;
}
/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_u32GetTeseoFwCrc32(tVoid)
||
||DESCRIPTION :  Calculates CRC32 for fp of teseo-firmware
||PARAMETER   :  FILE * fp
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
static tU32 GnssOedt_u32GetTeseoFwCrc32( FILE *fp, tU32 *pu32FinalCrc, tU32 u32ImageLength )
{
   tS32 s32RetVal =  OSAL_OK;
   tU32 u32Crc32 = 0;

   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "CRC32 line %lu", __LINE__ );

   if ((OSAL_NULL == pu32FinalCrc) || (OSAL_NULL == fp) )
   {
      s32RetVal = OSAL_ERROR;
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "CRC32 NULL ptr %lu", __LINE__ );
   }
   else
   {
      /* calculate CRC on Image size field first*/
      u32Crc32 = u32_crc32( u32Crc32, (const char *)&u32ImageLength, (int)sizeof(tU32) );
      rewind (fp);
      do
      {
         s32RetVal = (tS32)(fread( u8TesFwBuff, 1, FIRMWARE_UPDATE_BUFF_SIZE,fp ));
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FW CRC:fread ret %d", s32RetVal);
         if( 0 >= s32RetVal )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FW CRC32 fread Failed");
            if (0 != feof(fp))
               s32RetVal = OSAL_ERROR;
            break;
         }
         else
         {
            u32Crc32 = u32_crc32( u32Crc32, (const char *)u8TesFwBuff, (int)s32RetVal );
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "FW:CRC32 line %lu crc %lu", __LINE__, u32Crc32 );
            s32RetVal = OSAL_OK;
         }

      }while (0 == feof(fp));

      if (OSAL_OK == s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Firmware CRC calc complete crc32- 0x%x", u32Crc32 );
         *pu32FinalCrc = u32Crc32;
      }
      rewind (fp);
   }
   return (tU32)s32RetVal;
}

/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_u32GetTeseoFlasherCrc32(tVoid)
||
||DESCRIPTION :  Calculates CRC32 for fp for teseo boot loader
||PARAMETER   :  FILE * fp
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
static tU32 GnssOedt_u32GetTeseoFlasherCrc32( FILE *fp, tU32 *pu32FinalCrc, tU32 u32ImageLength )
{
   tS32 s32RetVal =  OSAL_OK;
   tU32 u32Crc32 = 0;
   tU32 u32EntryAdr = 0;

   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "BT CRC32 line %lu", __LINE__ );

   if ((OSAL_NULL == pu32FinalCrc) || (OSAL_NULL == fp) )
   {
      s32RetVal = OSAL_ERROR;
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "BT CRC32 NULL ptr %lu", __LINE__ );
   }
   else
   {
      /* calculate CRC on Image size field first*/
      u32Crc32 = u32_crc32( u32Crc32, (const char *)&u32ImageLength, (int)sizeof(u32ImageLength) );
      /* calculate CRC on Flasher entry address field */
      u32Crc32 = u32_crc32( u32Crc32, (const char *)&u32EntryAdr, (int)sizeof(u32EntryAdr) );
      rewind (fp);
      /* calculate crc for full binary */
      do
      {
         s32RetVal = (tS32)(fread( u8TesFwBuff, 1, FIRMWARE_UPDATE_BUFF_SIZE,fp ));
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "BT CRC:fread ret %d", s32RetVal);
         if( 0 >= s32RetVal )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "BT CRC32 fread Failed");
            if (0 != feof(fp))
               s32RetVal = OSAL_ERROR;
            break;
         }
         else
         {
            u32Crc32 = u32_crc32( u32Crc32, (const char *)u8TesFwBuff, (int)s32RetVal );
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "BT CRC32 line %lu crc %lu", __LINE__, u32Crc32 );
            s32RetVal = OSAL_OK;
         }

      }while (0 == feof(fp));

      if (OSAL_OK == s32RetVal)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "FLASHER CRC calc complete crc32- 0x%x", u32Crc32 );
         *pu32FinalCrc = u32Crc32;
      }
      rewind (fp);
   }
   return (tU32)s32RetVal;
}

/***************************************************************************
||FUNCTION    :  u32_crc32
||
||DESCRIPTION :  Calculates CRC32
||PARAMETER   :  CRC, buffer, size,
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
static tU32  u32_crc32(tU32 crc32val, const char *buf, int len)
{

   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "CRC32 line %lu crc %lu", __LINE__, crc32val );
   int i;
   
   if (buf == 0) return 0L;
   
   crc32val = crc32val ^ 0xffffffff;
   for (i = 0;  i < len;  i++) {
       crc32val = crc32_tab[(crc32val ^ buf[i]) & 0xff] ^ (crc32val >> 8);
   }
   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "CRC32 line %lu crc ^ ~0U %x", __LINE__, (crc32val ^ 0xffffffff) );
  return crc32val ^ 0xffffffff;
}

/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_u32GetNmeaMsgList(tVoid)
||
||DESCRIPTION :  Gets the List of NMEA messages used in current config
||               Compares it with the expected list of NMEA messages.
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success if (received list == expected list)
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32  GnssOedt_u32GetNmeaMsgList(tVoid)
{
   OSAL_trThreadAttribute rThreadAttr;
   tU32 u32ResultMask;
   u32NmeaListRetVal = 0;
   /*Update thread attributes*/
   rThreadAttr.szName       = GNSS_OEDT_NMEA_MSG_LIST_THREAD_NAME;
   rThreadAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThreadAttr.s32StackSize = GNSS_OEDT_NMEA_LIST_THREAD_STACK_SIZE;
   rThreadAttr.pfEntry      = GnssOedt_vNmeaListThread;
   rThreadAttr.pvArg        = OSAL_NULL;

   /* Wait for previous test to complete. GNSS driver thread may take some time to shut down */
   OSAL_s32ThreadWait(2000);
   
   if( OSAL_OK != OSAL_s32EventCreate( GNSS_OEDT_NMEA_LIST_EVENT_NAME,
                                       &hGnssOedtNmeaListEvent ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4,"Event create failed err %lu", OSAL_u32ErrorCode() );
      u32NmeaListRetVal += 2;
   }
   else
   {
      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr) )
      {
         u32NmeaListRetVal += 4;
      }
      else if ( OSAL_ERROR == OSAL_s32EventWait( hGnssOedtNmeaListEvent,
                                                 (GNSS_OEDT_NMEA_LIST_PASSED | GNSS_OEDT_NMEA_LIST_FAILED),
                                                 OSAL_EN_EVENTMASK_OR,
                                                 GNSS_NMEA_LIST_WAIT_TIME_MS,
                                                 &u32ResultMask ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "Event wait failed err %d line %d",
                                                OSAL_u32ErrorCode(),__LINE__ );
         if ( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
         {
            u32NmeaListRetVal += 8;
         }
         else
         {
            u32NmeaListRetVal += 16;
         }
      }
      /* Event wait returned with a success, Clear the event */
      else if(OSAL_ERROR == OSAL_s32EventPost( hGnssOedtNmeaListEvent,
                                               (OSAL_tEventMask) ~u32ResultMask,
                                               OSAL_EN_EVENTMASK_AND))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "Event Post failed err %lu line %d",
                                                 OSAL_u32ErrorCode(), __LINE__ );
         u32NmeaListRetVal += 32;
      }

      if( OSAL_OK != OSAL_s32EventClose( hGnssOedtNmeaListEvent ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4,"Event CLOSE failed err %lu", OSAL_u32ErrorCode() );
         u32NmeaListRetVal += 8192;
      }
      else if ( OSAL_OK != OSAL_s32EventDelete( GNSS_OEDT_NMEA_LIST_EVENT_NAME ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4,"Event delete failed err %lu", OSAL_u32ErrorCode() );
         u32NmeaListRetVal += 16384;
      }
   }

   return u32NmeaListRetVal;
}

/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_vNmeaListThread(tPVoid)
||
||DESCRIPTION :  Gets the List of NMEA messages used in current config
||               Compares it with the expected list of NMEA messages.
||PARAMETER   :  Nil
||
||RETURNVALUE :  None
||
||HISTORY     :
||
***************************************************************************/
static tVoid  GnssOedt_vNmeaListThread(tPVoid pVDummyArg )
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32NmeaList =0;
   u32NmeaListRetVal = 0;
   (tVoid)pVDummyArg;   /*-------for lint fix -------*/
   
   /*------------------------------------------Open the device--------------------------------------------------*/
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32NmeaListRetVal += 64;
   }
   else
   {
      /*-----------------------------------------Get the NMEA LIST---------------------------------------------------*/
      /* Wait for some time so that messages are received */
      OSAL_s32ThreadWait(5000);
      // Flush GNSS buffer
      if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                OSAL_u32ErrorCode());
         u32NmeaListRetVal += 32768;
      }
      /* Get NMEA message list */
      if ( 0 == u32NmeaListRetVal )
      {
         if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle,
                                             OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST,
                                             (tS32)&u32NmeaList) )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, "OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST failed %d",
                                                     OSAL_u32ErrorCode());
            /* Some times UART communication between Teseo and V850 may be delayed
            * So wait for some time and retry */
            OSAL_s32ThreadWait(2000);
            if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST, (tS32)&u32NmeaList) )
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, "OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST failed %d",
                                                        OSAL_u32ErrorCode());
               u32NmeaListRetVal += 128;
            }
         }
      }
      /*-----------------------------------Retry in case of checksum error--------------------------------*/
      if ( 0 == u32NmeaListRetVal )
      {
         if ( u32NmeaList & GNSS_OEDT_CHECKSUM_ERROR ) //lint !e845, !e774
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, "Checksum Error, try again" );
            u32NmeaList = 0;
            /* Try getting the list for next fix */
            OSAL_s32ThreadWait(2000);
            if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle,
                                                OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST,
                                                (tS32)&u32NmeaList) )
            {
               OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, "OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST failed %d",
                                                        OSAL_u32ErrorCode());
               u32NmeaListRetVal += 256;
            }
         }
      }
      /*-----------------------------------Compare Received NMEA List with expected list--------------------------------*/
      if ( 0 == u32NmeaListRetVal )
      {
         if ( (u32NmeaList & GNSS_PROXY_OEDT_EXPEC_NMEA_LIST) != GNSS_PROXY_OEDT_EXPEC_NMEA_LIST ) //lint !e845, !e774 PQM_authorized_551
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, "INVALID Configuration received %x, Expected %x",
                                                     u32NmeaList, GNSS_PROXY_OEDT_EXPEC_NMEA_LIST );
            u32NmeaListRetVal +=512;
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "Teseo is configured correctly: %x", u32NmeaList);
         }
      }
      /*------------------------------------------CLOSE the device--------------------------------------------------*/
      if ( OSAL_ERROR == OSAL_s32IOClose(hDevHandle) )
      {
         u32NmeaListRetVal +=1024;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
      }
   }
   /*---------------------------------------------Post success or failure event---------------------------------------*/
   if ( 0 == u32NmeaListRetVal )
   {
      if(OSAL_ERROR == OSAL_s32EventPost( hGnssOedtNmeaListEvent,
                                          GNSS_OEDT_NMEA_LIST_PASSED,
                                          OSAL_EN_EVENTMASK_OR ))
      {
         u32NmeaListRetVal +=2048;
      }
   }
   else if(OSAL_ERROR == OSAL_s32EventPost( hGnssOedtNmeaListEvent,
                                            GNSS_OEDT_NMEA_LIST_FAILED,
                                            OSAL_EN_EVENTMASK_OR ))
   {
      u32NmeaListRetVal +=4096;
   }
}

/***************************************************************************
||FUNCTION    :  tU32  u32OedtGNSSProxySetEpoch(tVoid)
||
||DESCRIPTION :  Sets GNSS epoch
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
***************************************************************************/

tU32  u32OedtGNSSProxySetEpoch(tVoid)
{
   tS32 s32RetVal = 0;
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;
   OSAL_trGnssTimeUTC  rGPSTimeUTC[] = { {1999,8,21,0,0,0,0},
                                         {2014,12,31,0,0,0,0},
                                         {2015,1,1,0,0,0,0},
                                         {2030,8,1,0,0,0,0},
                                         {2014,8,21,0,0,0,0} };

   (tVoid)rGPSTimeUTC;

   //Open GNSS Proxy
   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      u32RetVal += 1;
   }
   else
   {  //Open Successful
      tU8 i = 0;
      OSAL_s32ThreadWait(10);
      do
      {
         s32RetVal = OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_SET_EPOCH, (tS32)&rGPSTimeUTC[i]);
         if(OSAL_ERROR == s32RetVal )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                               "IOCTL_SET_GNSS_EPOCH failed %d",OSAL_u32ErrorCode());
            u32NmeaListRetVal += 2;
            OSAL_s32IOClose( hDevHandle );
         }
         i++;
      }while( ( s32RetVal != OSAL_ERROR ) && ( i < 5 ) );
   }

   if( ( s32RetVal != OSAL_ERROR ) && ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR ) )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close failed with error %d",OSAL_u32ErrorCode());
      u32RetVal +=3;
   }

   return u32RetVal;
}


/***************************************************************************
||FUNCTION    :  tU32  GnssOedt_s32TesGetCrc(tVoid)
||
||DESCRIPTION :  Get the CRC of the Teseo firmware flashed in the device
||               
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success
||               Error code - failure
||HISTORY     :
||
***************************************************************************/
tU32 GnssOedt_s32TesGetCrc()
{
   tU32 u32crc = 0;
   tS32 s32RetVal = 0;

   /* Open GNSS device for flashing */
   hTeseoDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_WRITEONLY );
   if ( hTeseoDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
      s32RetVal = 1;
   }
   else
   {
      // get the CRC
      if(OSAL_ERROR == OSAL_s32IOControl( hTeseoDevHandle, OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC, (tS32)&u32crc) )
      {
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC failed %d",
                                                OSAL_u32ErrorCode());
        s32RetVal = 2;
      }
      else
      {

         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "CRC recvd %x", u32crc);

      }
      /*Close GNSS Proxy*/
      if ( OSAL_s32IOClose( hTeseoDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         s32RetVal += 4;
      }
   }

   return ((tU32)s32RetVal);
}

/***************************************************************************
||FUNCTION    :  tU32 u32SetSatSys( tVoid )
||
||DESCRIPTION :  Setting the satellite system value
||
||PARAMETER   :  tU32
||               u32Arg SAT_SYS value
||
||RETURNVALUE :  tU32
||               Sat Sys Value that was set         - success
||               Error code                         - failure
||HISTORY     :
||
***************************************************************************/


static tU32 u32SetSatSys( tU32 u32Arg )
{
   tU32 u32Argument = u32Arg;
   tU32 u32ReturnValue;
   /*---SET THE SAT SYS--*/
   if( OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS, ( tS32 )&u32Argument ) )
   {
      
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS failed at Line : "
                                             "%d with ErrorCode :%d ", __LINE__, OSAL_u32ErrorCode() );
      u32ReturnValue = (tU32)OSAL_ERROR;
      
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS successful "
                                                    "and Sat Sys is set to 0x%x ", u32Argument );
      u32ReturnValue = u32Argument;
   }

   return ( u32ReturnValue );
}

/***************************************************************************
||FUNCTION    :  tU32 u32GetSatSys( tVoid )
||
||DESCRIPTION :  Getting the current satellite system value
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               Sat Sys Value          - success
||               Error code             - failure
||HISTORY     :
||
***************************************************************************/



static tU32 u32GetSatSys( tVoid )
{

   tU32 u32ReturnValue = 0;

   /*---GET THE CURRENT SAT SYS--*/
   if( OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS, ( tS32 )&u32ReturnValue ) )
   {

      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS failed at "
                              "Line : %d with ErrorCode :%d ", __LINE__, OSAL_u32ErrorCode() );
      u32ReturnValue = (tU32)OSAL_ERROR;
   }
   else
   {

      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS successful " );
   
   }

   return ( ( tU32 )u32ReturnValue );
}

/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32SatInterChk( tVoid )
||
||DESCRIPTION :  The OEDT is implemented to check the satellite interface 
||               system and also to set the satellite system to desired value
||               and check for the same if it is set to desired 
||               value. Finally we are setting to the initial value 
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/


tU32 GNSSOedt_u32SatInterChk( tVoid )
{
   tU32 u32Arg = OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GLONASS;
   tU32 u32SetFunctionReturn = 0;
   tU32 u32GetFunctionReturn = 0;
   tU32 u32InitialSatValue = 0;
   tBool bErrorOccurred = FALSE;
   tU32 u32ErrorValue = 0;
   
   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
      bErrorOccurred = TRUE;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                                              OSAL_u32ErrorCode());
         u32ErrorValue += 2;
         bErrorOccurred = TRUE;
      }
      else
     {
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
     }
      
      
      if( bErrorOccurred == FALSE )
      {
         
         /*---Get the current Satellite System--- */
         u32InitialSatValue = u32GetSatSys();
         if( OSAL_ERROR == ( tS32 ) u32InitialSatValue )
         {
            u32ErrorValue += 4;
            bErrorOccurred = TRUE;
         }
         else
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Current Sat Sys being used is %d ", 
                                                                     u32InitialSatValue );
         }
      }
      
      if( bErrorOccurred == FALSE )
      {
         /*---Set the Satellite System to the value given---*/
         u32SetFunctionReturn = u32SetSatSys( u32Arg );
         if( OSAL_ERROR == ( tS32 ) u32SetFunctionReturn )
         {
            u32ErrorValue += 8;
            bErrorOccurred = TRUE;
         }
      }

      if( bErrorOccurred == FALSE )
      {
         /*---Get the Satellite System which was set and check if it is same as the one set---*/
         u32GetFunctionReturn = u32GetSatSys();
         if( OSAL_ERROR == ( tS32 )u32GetFunctionReturn )
         {
            u32ErrorValue += 16;
            bErrorOccurred = TRUE;
         }
         else
         {
            if( u32GetFunctionReturn != u32SetFunctionReturn )
            {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Setting of value has failed "
                                                            "at Line : %d ", __LINE__);
               u32ErrorValue += 32;
               bErrorOccurred = TRUE;
            }
            else
            {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Sat Sys is set to desired "
                                               "Sat Sys %d ", u32SetFunctionReturn );
            }
         }
      }
      
      if( bErrorOccurred == FALSE )
      {
         /*---Set the Initial Satellite System---*/
         u32SetFunctionReturn = u32SetSatSys( u32InitialSatValue );
         if( OSAL_ERROR == ( tS32 )u32SetFunctionReturn )
         {
            u32ErrorValue += 64;
            bErrorOccurred = TRUE;
         }
      }
      
      if( bErrorOccurred == FALSE )
      {
         /*---Get the current Satellite System---*/
         u32GetFunctionReturn = u32GetSatSys();
         if( OSAL_ERROR == ( tS32 )u32GetFunctionReturn )
         {
            u32ErrorValue += 128;
            bErrorOccurred = TRUE;
         }
         else
         {
            if( u32GetFunctionReturn != u32InitialSatValue )
            {
            
               OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Setting of value has "
                                              "failed at Line : %d ", __LINE__);
               u32ErrorValue += 256;
               bErrorOccurred = TRUE;
            }
            else
            {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Sat Sys is set successfully "
                                      "to its initial value %d ", u32InitialSatValue );
            }
         }
      }
   }
   
   if( bErrorOccurred == FALSE )
   {
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 512;
      }
   }
   return ( u32ErrorValue );
   
}
/******************************************************************************
||FUNCTION    :  tU32 u32OedtGNSSFwOpenCloseContinuously( tVoid )
||
||DESCRIPTION :  OPEN AND CLOSE GNSS FIRMWARE UPDATE DRIVER CONTINIOUSLY
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/
tU32 u32OedtGNSSFwOpenCloseContinuously( tVoid )
{
   tU32 u32RetVal = 0;
   tU32 u32LootCnt;

   for ( u32LootCnt =0; ((u32LootCnt < 10) && (0 == u32RetVal)); u32LootCnt++ )
   {
      /* Open GNSS device for flashing */
      hTeseoDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_WRITEONLY );
      if ( hTeseoDevHandle == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
         u32RetVal = 1;
      }
      /*Close GNSS Proxy*/
      else if ( OSAL_s32IOClose( hTeseoDevHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d",OSAL_u32ErrorCode());
         u32RetVal = 2;
      }
   }
   return (u32RetVal);
}

/******************************************************************************
||FUNCTION    :  tU32 u32OedtGNSSProxyInfiniteRead( tVoid )
||
||DESCRIPTION :  Device infinite read, Never Ends and should not be mved to regression data set
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/

tU32  u32OedtGNSSProxyInfiniteRead(tVoid)
   {
      OSAL_tIODescriptor hDevHandle;
      OSAL_trGnssFullRecord rGnssFullRecord;
      tU32 u32RetVal = 0;
      tS32 s32LoopCnt=0;
      tBool bEntry = TRUE;
   
      OSAL_pvMemorySet(&rGnssFullRecord,0,sizeof(OSAL_trGnssFullRecord));
   
      /*Opens GNSS Proxy*/
      hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
      if ( hDevHandle == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device open failed with error %d",OSAL_u32ErrorCode());
         u32RetVal = 1;
      }
      else
      {/*Open Successful*/
        if(OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
        {
           OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                   OSAL_u32ErrorCode());
        }   

        while (bEntry)
        {
           if ( OSAL_ERROR == OSAL_s32IORead(hDevHandle,(tPS8)&rGnssFullRecord,sizeof(OSAL_trGnssFullRecord)))
           {
              OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, " Read failed " );
              u32RetVal += 4;
           }
           else
           {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "----Read passed------");
               OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "----Position------lat-%f, lon-%f, Alt-%f, Geo-sep-%f",
                                              rGnssFullRecord.rPVTData.f64Latitude,
                                              rGnssFullRecord.rPVTData.f64Longitude,
                                              rGnssFullRecord.rPVTData.f32AltitudeWGS84,
                                              rGnssFullRecord.rPVTData.f32GeoidalSeparation );
      
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                                 "----time---- DD-MM-YY-> %lu:%lu:%lu, hh:mm:ss:ms-> %lu:%lu:%lu:%lu",
                                 rGnssFullRecord.rPVTData.rTimeUTC.u8Day,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u8Month,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u16Year,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u8Hour,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u8Minute,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u8Second,
                                 rGnssFullRecord.rPVTData.rTimeUTC.u16Millisecond);
      
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                                    "----Velocity---- N:E:V-> %f:%f:%f",
                                    rGnssFullRecord.rPVTData.f32VelocityNorth,
                                    rGnssFullRecord.rPVTData.f32VelocityEast,
                                    rGnssFullRecord.rPVTData.f32VelocityUp );
      
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                                   "----Solution mode---- fix type %d,Quality %d,Sats-Visible: %d,Sats Used: %d,Sats Recvd: %d, Sat-Sys_Used:%x",
                                   rGnssFullRecord.rPVTData.rFixStatus.enMode,
                                   rGnssFullRecord.rPVTData.rFixStatus.enQuality,
                                   rGnssFullRecord.rPVTData.u16SatsVisible,
                                   rGnssFullRecord.rPVTData.u16SatsUsed,
                                   rGnssFullRecord.rPVTData.u16Received,
                                   rGnssFullRecord.rPVTData.u8SatSysUsed);
      
               OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                              "----error estimations---- poscov N-E-u:%f:%f:%f velcov N-E-u:%f:%f:%f PDop: %f, TDop: %f,HDop:%f GDop: %f, VDop: %f",
                              rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem0,
                              rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem5,
                              rGnssFullRecord.rPVTData.rPositionCovarianceMatrix.f32Elem10,
                              rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem0,
                              rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem5,
                              rGnssFullRecord.rPVTData.rVelocityCovarianceMatrix.f32Elem10,
                              rGnssFullRecord.rPVTData.f32PDOP,
                              rGnssFullRecord.rPVTData.f32TDOP,
                              rGnssFullRecord.rPVTData.f32HDOP,
                              rGnssFullRecord.rPVTData.f32GDOP,
                              rGnssFullRecord.rPVTData.f32VDOP);
      
               for (s32LoopCnt = 0; s32LoopCnt < OSAL_C_U8_GNSS_NO_CHANNELS; s32LoopCnt++)
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                                 "----Ch sts---- num:%2d \t Ch-Id:%03d \t Ch-Status:%02d \t azm:%.3f \t ele:%.3f \t C/N:%02d",
                                 (s32LoopCnt+1),
                                 rGnssFullRecord.rChannelStatus[s32LoopCnt].u16SvID,
                                 rGnssFullRecord.rChannelStatus[s32LoopCnt].u16SatStatus,
                                 rGnssFullRecord.rChannelStatus[s32LoopCnt].f32Azimuthal,
                                 rGnssFullRecord.rChannelStatus[s32LoopCnt].f32Elevation,
                                 rGnssFullRecord.rChannelStatus[s32LoopCnt].u8CarrierToNoiseRatio);
               }
           }
        }
        /*Close GNSS Proxy*/
        if ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR )
        {
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device Close failed with error %d", OSAL_u32ErrorCode());
           u32RetVal += 2;
        }
      }
      return(u32RetVal);
   }

/************************************************************************
||FUNCTION       :  tU32  u32OedtGNSSProxyGetEpoch(tVoid)
||
||DESCRIPTION  :  OEDT Test Case-1
||                        Sets the value of epoch to 5 different values and reads back the set value each time.
||                        
||
||PARAMETER     :  Nil
||
||RETURNVALUE :  tU32 
||                        GNSS_NO_NOERROR     -                                   success 
||                        GNSS_IOCTRL_SET_EPOCH_ERROR                -    FAILURE in setting Epoch
||                        GNSS_IOCTRL_GET_EPOCH_ERROR                -    FAILURE in getting Epoch
||                        GNSS_GET_EPOCH_NEW_DATE_GEN_ERROR    -    FAILURE in generating new date
||                        GNSS_GET_EPOCH_VALUE_MISMATCH             -    FAILURE due to mismatch in set and get values
||                        GNSS_IOOPEN_ERROR                                 -     FAILURE in opening GNSS device
||                        GNSS_IOCLOSE_ERROR                -                     FAILURE in closing GNSS device
||HISTORY        :
||
************************************************************************/

tU32  u32OedtGNSSProxyGetEpoch(tVoid)
{
   tU32 u32ReturnValue = GNSS_NO_ERROR;
   OSAL_trGnssTimeUTC rEpochSetTime = {0};
   OSAL_trGnssTimeUTC rEpochSetTimeNextWeek = {0};
   OSAL_tIODescriptor hDevHandle;
   OSAL_trGnssTimeUTC rGPSTimeUTC[] = { {2015,12,9,0,0,0,0},
                                        {2060,2,29,0,0,0,0},
                                        {2060,3,1,0,0,0,0},
                                        {2000,1,1,0,0,0,0},
                                        {1999,12,31,0,0,0,0},
                                        {2115,12,3,0,0,0,0},
                                        {2088,2,29,0,0,0,0},
                                        {2010,12,31,0,0,0,0},
                                        {2011,1,1,0,0,0,0},
                                        {2099,12,31,0,0,0,0}};

   hDevHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "GNSS Device open failed with error %d", OSAL_u32ErrorCode());
      u32ReturnValue = GNSS_IOOPEN_ERROR;
   }
   else
   {
      tU8 i = 0;
      do
      {
         OSAL_s32ThreadWait(10);

         if( OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_SET_EPOCH, (tS32)&rGPSTimeUTC[i]) )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                               "IOCTL_SET_GNSS_EPOCH failed with ErrorCode : %d ",OSAL_u32ErrorCode());
            u32ReturnValue = GNSS_IOCTRL_SET_EPOCH_ERROR;
            OSAL_s32IOClose( hDevHandle );
         }
         else
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "Epoch date set to %02d/%02d/%4d",
                                                     (tU32)(rGPSTimeUTC[i].u8Day), (tU32)(rGPSTimeUTC[i].u8Month),
                                                     (tU32)(rGPSTimeUTC[i].u16Year));

            if( (tS32)OSAL_ERROR == OSAL_s32IOControl( hDevHandle, OSAL_C_S32_IOCTL_GNSS_GET_EPOCH, (tS32)&rEpochSetTime))
            {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " OSAL_C_S32_IOCTRL_GNSS_PROXY_GET_EPOCH failed at Line : "
                                                        "%d with ErrorCode :%d ", __LINE__, OSAL_u32ErrorCode() );
               u32ReturnValue = GNSS_IOCTRL_GET_EPOCH_ERROR;
               OSAL_s32IOClose( hDevHandle );
            }
            else
            {
               /* To get the date of the end of week, 6 days are added to rEpochSetTime */
               rEpochSetTimeNextWeek.u8Day   = rEpochSetTime.u8Day;
               rEpochSetTimeNextWeek.u8Month = rEpochSetTime.u8Month; 
               rEpochSetTimeNextWeek.u16Year = rEpochSetTime.u16Year;

               if((tS32)OSAL_E_NOERROR != s32AddDaysToDate(&rEpochSetTimeNextWeek, 6))
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " s32AddDaysToDate returned an error ");
                  u32ReturnValue = GNSS_GET_EPOCH_NEW_DATE_GEN_ERROR;
               }
               else
               {
                  if( TRUE == bCheckDateLiesInWeek( &rEpochSetTime, &rGPSTimeUTC[i] ))
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " OSAL_C_S32_IOCTRL_GNSS_PROXY_GET_EPOCH successful."
                                                              " Set and Get epoch match. Epoch was set during : %02d/%02d/%4d -- %02d/%02d/%4d ", 
                                                              (tU32)rEpochSetTime.u8Day, (tU32)rEpochSetTime.u8Month, 
                                                              (tU32)rEpochSetTime.u16Year, ((tU32)rEpochSetTimeNextWeek.u8Day), 
                                                              (tU32)rEpochSetTimeNextWeek.u8Month, (tU32)rEpochSetTimeNextWeek.u16Year);
                  }
                  else
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " Set and Get epoch results don't match ");
                     u32ReturnValue = GNSS_GET_EPOCH_VALUE_MISMATCH;
                  }
               }
               
            }
         }
         i++;
      }while((u32ReturnValue == GNSS_NO_ERROR) && (i < 10));
   }

   if( ( u32ReturnValue == GNSS_NO_ERROR ) && ( OSAL_s32IOClose( hDevHandle ) == OSAL_ERROR ) )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Device close failed with error %d",OSAL_u32ErrorCode());
      u32ReturnValue = (tS32)GNSS_IOCLOSE_ERROR;
   }
   return u32ReturnValue;
}

/************************************************************************
||FUNCTION       :  static tBool  bCheckDateLiesInWeek( const OSAL_trGnssTimeUTC * rStartDateOfWeek, const OSAL_trGnssTimeUTC * rDateToCheck )
||
||DESCRIPTION  :  Checks if the given date lies within the same week as the other date
||                        
||                        
||
||PARAMETER     :   OSAL_trGnssTimeUTC * rStartDateOfWeek
                             OSAL_trGnssTimeUTC * rDateToCheck
||
||RETURNVALUE :  tU8 
||                        FALSE - If the date does not lie in the same week as the other date 
||                        TRUE  - If the date lies in the same week as the other date
||HISTORY        :
||
************************************************************************/
static tBool bCheckDateLiesInWeek( const OSAL_trGnssTimeUTC * rStartDateOfWeek, const OSAL_trGnssTimeUTC * rDateToCheck )
{
   tBool bReturnValue = FALSE;
   tU8 u8WeekDayAdd;
   OSAL_trGnssTimeUTC rDateInWeek;

   rDateInWeek.u8Day   = rStartDateOfWeek->u8Day;
   rDateInWeek.u8Month = rStartDateOfWeek->u8Month;
   rDateInWeek.u16Year = rStartDateOfWeek->u16Year;

   if((rDateToCheck->u8Day == rStartDateOfWeek->u8Day) && (rDateToCheck->u8Month == rStartDateOfWeek->u8Month) && (rDateToCheck->u16Year == rStartDateOfWeek->u16Year))
   {
      bReturnValue = TRUE;
   }
   else
   {
      for( u8WeekDayAdd = 1; ((u8WeekDayAdd < 7) && (bReturnValue != TRUE)); u8WeekDayAdd++ )
      {
         if((tS32)OSAL_E_NOERROR == s32AddDaysToDate(&rDateInWeek, 1))
         {
            if((rDateToCheck->u8Day == rDateInWeek.u8Day) && (rDateToCheck->u8Month == rDateInWeek.u8Month) && (rDateToCheck->u16Year == rDateInWeek.u16Year))
            {
               bReturnValue = TRUE;
            }
         }
         else
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, " s32AddDaysToDate returned error ");
         }
      }
   }
   return bReturnValue;
}


/*************************************************************************************
* FUNCTION     : bCheckLeapYear
*
* PARAMETER    : tU16 u16Year - Year to check
*
* RETURNVALUE  : FALSE - If the year is common year
*                         TRUE - If the year is leap year
*
* DESCRIPTION  : Checks whether the input year is leap or not.
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
***************************************************************************************/
static tBool bCheckLeapYear( tU16 u16Year )
{

   tBool bRetVal = 0;

   if( u16Year % 4 != 0 )
   {
      bRetVal = FALSE;   //Common year
   }
   else if ( u16Year % 100 != 0 )
   {
      bRetVal = TRUE;   //Leap year
   }
   else if ( u16Year % 400 != 0 )
   {
      bRetVal = FALSE;   //Common year
   }
   else
   {
      bRetVal = TRUE;   //Leap year
   }

   return bRetVal;
}



/************************************************************************
||FUNCTION       :  static tS32 s32AddDaysToDate( OSAL_trGnssTimeUTC * rUTCDate, tS32 s32AddDays )
||
||DESCRIPTION  :  Adds specified number of days to a given date
||                        
||                        
||
||PARAMETER     :   OSAL_trGnssTimeUTC * rUTCDate
                             tS32 s32AddDays
||
||RETURNVALUE :  tS32 
||                        OSAL_E_NOERROR -            if the days are added to the given date without any errors
||                        OSAL_E_INVALIDVALUE -     if the date parameter is NULL
||HISTORY        :
||
************************************************************************/

static tS32 s32AddDaysToDate( OSAL_trGnssTimeUTC * rUTCDate, tS32 s32AddDays )
{
   tS32 s32ReturnValue      = (tS32)OSAL_E_NOERROR;
   tS32 s32Days             = 0;
   tU8  u8Month             = 0;
   tU16 u16Year             = 0;
   tU8  u8MonthTable[12]    = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
   tS32 s32DaysTemp         = 0;

   if( rUTCDate == OSAL_NULL )
   {
       s32ReturnValue = (tS32)OSAL_E_INVALIDVALUE;
   }
   else
   {
       s32Days = (tS32)(rUTCDate->u8Day);
       u8Month = rUTCDate->u8Month;
       u16Year = rUTCDate->u16Year;

       while(s32DaysTemp < s32AddDays )
       {
         s32Days += 1;

         if(s32Days > u8MonthTable[u8Month - 1])
         {
            if((u8Month == MONTH_FEB) && (TRUE == bCheckLeapYear(u16Year)) && (s32Days == 29))
            {
               s32Days = 29;
            }
            else
            {
               u8Month += 1;
               s32Days = 1;
            }
         }
         if(u8Month > NO_OF_MONTHS_IN_A_YEAR)
         {
            s32Days = 1;
            u8Month = MONTH_JAN;
            u16Year += 1;
         }
         s32DaysTemp ++;
       }

       rUTCDate->u16Year = u16Year;
       rUTCDate->u8Month = u8Month;
       rUTCDate->u8Day   = (tU8)s32Days;
   }

   return s32ReturnValue;
}


/***************************************************************************
||FUNCTION    :  tS32 s32DiagSetSatSys( tVoid )
||
||DESCRIPTION :  Setting the satellite system value using Diag IO Control
||
||PARAMETER   :  tU32
||               u32Arg SAT_SYS value
||
||RETURNVALUE :  tS32
||               Sat Sys Value that was set         - success
||               Error code                         - failure
||HISTORY     :
||
***************************************************************************/


static tS32 s32DiagSetSatSys( tU32 u32Arg )
{
   tU32 u32Argument = u32Arg;
   tS32 s32ReturnValue;
   /*---SET THE SAT SYS--*/
   if( OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS, ( tS32 )&u32Argument ) )
   {
      
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "s32DiagSetSatSys: OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS failed at Line : "
                                             "%d with ErrorCode :%d ", __LINE__, OSAL_u32ErrorCode() );
      s32ReturnValue = OSAL_ERROR;
      
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "s32DiagSetSatSys: OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS successful "
                                                    "and Sat Sys is set to %d ", u32Argument );
      s32ReturnValue = (tS32)u32Argument;
   }

   return ( s32ReturnValue );
}

/***************************************************************************
||FUNCTION    :  tS32 s32DiagReadSatSysfromFile( tVoid )
||
||DESCRIPTION :  Reading the satellite system set from the file
||
||PARAMETER   :  tU32
||               u32Arg SAT_SYS value
||
||RETURNVALUE :  tS32
||               Sat Sys Value that was set         - success
||               Error code                         - failure
||HISTORY     :
||
***************************************************************************/

static tS32 s32DiagReadSatSysfromFile( tVoid )
{

   OSAL_tIODescriptor hFile;
   tU32 u32lastUsedGnssSatSys = OSAL_C_U8_GNSS_SATSYS_UNKNOWN;
   tS32 s32Length = OSAL_ERROR;
   tS32 s32RetVal = OSAL_NULL;
   
   hFile = OSAL_IOOpen( OEDT_GNSS_LAST_SATSYS_USED_FILENAME, OSAL_EN_READONLY );
   
   if ( OSAL_ERROR == hFile )
   {  
      if ( OSAL_E_DOESNOTEXIST == OSAL_u32ErrorCode () )
      {  
         /* Returning the NO ERROR if File does not exist */
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "s32DiagReadSatSysfromFile: OSAL_s32IOOpen fail ,Fail does not exist : Fail line %d ,ErrorCode :%d"
         , __LINE__,OSAL_u32ErrorCode());
         s32RetVal = GNSS_LAST_SAT_SYS_USED_FILE_DOESNOT_EXIST_ERROR;
      }
      else
      {
         s32RetVal = OSAL_ERROR ;
      }
   }
   else
   {
      s32Length = OSAL_s32IORead ( hFile,
                       (tPS8 ) &u32lastUsedGnssSatSys,
                       sizeof ( u32lastUsedGnssSatSys ) );
      if(s32Length != (tS32)sizeof ( u32lastUsedGnssSatSys ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "s32DiagReadSatSysfromFile: OSAL_s32IORead : Fail line %d", __LINE__);
         s32RetVal = OSAL_ERROR;
      }
      else
      {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "s32DiagReadSatSysfromFile: Sat System read :  %d", u32lastUsedGnssSatSys);
            s32RetVal = (tS32)u32lastUsedGnssSatSys;
      }

      if (OSAL_s32IOClose(hFile) == OSAL_ERROR)
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, 
                              "s32DiagReadSatSysfromFile: GnssProxy_u32ReadLastUsedSatSys() => OSAL_IOClose() failed with error code = %s",
                              OSAL_coszErrorText(OSAL_u32ErrorCode()));
         s32RetVal = OSAL_ERROR;
      }
   }
   return ( s32RetVal );
}


/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32DIAGSatInterChk( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if the
||                       satellite system stored using Diag io control is set and
||                       also verify if the value is not stored in the persistent file( The satellite
||                       system stored using OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS
||                       should persist in the file ).
||                       
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/


tU32 GNSSOedt_u32DiagSatInterChk( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tS32 s32DiagSetFuncRet = 0;
   tS32 s32ValReadFromFile = 0;
   tBool bErrorOccurred = FALSE;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LIMIT] = {  OSAL_C_U8_GNSS_SATSYS_GPS, 
                                                         OSAL_C_U8_GNSS_SATSYS_GLONASS };
   tU16 u16Loop = 0;

   
   //---Open the GNSS driver---
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
      bErrorOccurred = TRUE;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32DiagSatInterChk: Device Open Successful " );
      
      //---Flush Sensor Data---
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32DiagSatInterChk: OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                                                                              OSAL_u32ErrorCode());
         u32ErrorValue += 2;
         bErrorOccurred = TRUE;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32DiagSatInterChk: OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( bErrorOccurred == FALSE )
      {
         //---Set the Satellite System to the value given---
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
               bErrorOccurred = TRUE;
            }

            if( bErrorOccurred == FALSE )
            {
               s32ValReadFromFile = s32DiagReadSatSysfromFile();
               if( OSAL_ERROR == s32ValReadFromFile )
               {
                  u32ErrorValue += 8;
                  bErrorOccurred = TRUE;
               }
               else
               {
                  if( OSAL_C_U8_GNSS_SATSYS_GPS == s32ValReadFromFile )
                  {
                     
                     //---Set the Satellite System to the value given---
                     s32DiagSetFuncRet = s32DiagSetSatSys( u8SatSystobeSet[OEDT_GNSS_GLONASS_INDEX] );
                     if( OSAL_ERROR == ( tS32 )s32DiagSetFuncRet )
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Line : %d Setting of sat sys using diag failed"
                           ": %d", __LINE__, OSAL_u32ErrorCode() );
                        bErrorOccurred = TRUE;
                        u32ErrorValue += 16;
                     }
                     else
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Sat Sys Set to %d : ", s32DiagSetFuncRet);
                     }
                  }
                  else if( OSAL_C_U8_GNSS_SATSYS_GLONASS == s32ValReadFromFile )
                  {
                     s32DiagSetFuncRet = s32DiagSetSatSys( u8SatSystobeSet[OEDT_GNSS_GPS_INDEX] );
                     if( OSAL_ERROR == s32DiagSetFuncRet )
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Line : %d Setting of sat sys using diag failed"
                           ": %d", __LINE__, OSAL_u32ErrorCode() );
                        bErrorOccurred = TRUE;
                        u32ErrorValue += 32;
                     }
                     else
                     {
                        OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Sat Sys Set to %d : ", s32DiagSetFuncRet);
                     }
                  }
                  else
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: value read from file is invalid");
                  }
               }
            }

            if( bErrorOccurred == FALSE )
            {
               s32ValReadFromFile = s32DiagReadSatSysfromFile();
               if( OSAL_ERROR == s32ValReadFromFile )
               {
                  u32ErrorValue += 64;
               }
               else
               {
                  if( s32ValReadFromFile == s32DiagSetFuncRet )
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Line : %d Setting of sat sys diag failed "
                              "because the value might have been stored persistently : %d", __LINE__, OSAL_u32ErrorCode() );
                     u32ErrorValue += 128;
                  }
                  else
                  {
                     OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Setting of sat sys using diag Passed " );
                  }
               }
            }
            
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32DiagSatInterChk: Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 256;
      }
   }

   (tVoid)bErrorOccurred;
   
   return ( u32ErrorValue );
   
}

/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32SatCfgBlk200( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if all
||                       the satelllites in Cfg block 200 are set as reqd.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/

tU32 GNSSOedt_u32SatCfgBlk200( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tU32 u32GetFuncRet = 0;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_200] = {  OSAL_C_U8_GNSS_SATSYS_GPS,
                                                                     OSAL_C_U8_GNSS_SATSYS_GLONASS,
                                                                     OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                                     OSAL_C_U8_GNSS_SATSYS_QZSS };
   tU16 u16Loop = 0;

   
   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk200: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200: Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32SatCfgBlk200: "
                           "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                           OSAL_u32ErrorCode());
         u32ErrorValue += 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200: "
                              "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( 0 == u32ErrorValue )
      {
         /*---Set the Satellite System to the value given---*/
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_200; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
            }

            if( 0 == u32ErrorValue )
            {
               u32GetFuncRet = u32GetSatSys();
               if( OSAL_ERROR == ( tS32 ) u32GetFuncRet )
               {
                  u32ErrorValue += 8;
               }
               else
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200:"
                                                   " Sat Sys Value = 0x%x ", u32GetFuncRet );
               }
            }

            if( 0 == u32ErrorValue )
            {
               if( u8SatSystobeSet[u16Loop] != u32GetFuncRet )
               {
                  u32ErrorValue += 16;
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200:"
                                     " Sat Sys Value to be Set = 0x%x and Sat Sys Value"
                                     "returned after setting 0x%x", u8SatSystobeSet[u16Loop], u32GetFuncRet );
               }
            }
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk200: Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 16;
      }
   }
   
   return ( u32ErrorValue );
   
}

/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32SatCfgBlk227( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if all
||                       the satelllites in Cfg block 227 are set as reqd.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/


tU32 GNSSOedt_u32SatCfgBlk227( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tU32 u32GetFuncRet = 0;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_227] = {OSAL_C_U8_GNSS_SATSYS_GALILEO};
   tU16 u16Loop = 0;

   
   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk227: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk227: Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32SatCfgBlk227: "
                           "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                           OSAL_u32ErrorCode());
         u32ErrorValue += 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk227: "
                              "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( 0 == u32ErrorValue )
      {
         /*---Set the Satellite System to the value given---*/
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_227; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
            }

            if( 0 == u32ErrorValue )
            {
               u32GetFuncRet = u32GetSatSys();
               if( OSAL_ERROR == ( tS32 ) u32GetFuncRet )
               {
                  u32ErrorValue += 8;
               }
               else
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk227:"
                                                   " Sat Sys Value = 0x%x ", u32GetFuncRet );
               }
            }

            if( 0 == u32ErrorValue )
            {
               if( u8SatSystobeSet[u16Loop] != u32GetFuncRet )
               {
                  u32ErrorValue += 16;
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200:"
                                     " Sat Sys Value to be Set = 0x%x and Sat Sys Value"
                                     "returned after setting 0x%x", u8SatSystobeSet[u16Loop], u32GetFuncRet );
               }
            }
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk227: Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 16;
      }
   }
   
   return ( u32ErrorValue );
   
}

/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32ChkSatCfgBlk227( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if all
||                       the satelllites in Cfg block 227 are set as reqd.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/


tU32 GNSSOedt_u32ChkSatCfgBlk227( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tU32 u32GetFuncRet = 0;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LMT_CFG_BLK_227] = {OSAL_C_U8_GNSS_SATSYS_GALILEO,
                                                                 OSAL_C_U8_GNSS_SATSYS_COMPASS};
   tU16 u16Loop = 0;

   
   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32ChkSatCfgBlk227: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk227: Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32ChkSatCfgBlk227: "
                           "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                           OSAL_u32ErrorCode());
         u32ErrorValue += 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk227: "
                            "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( 0 == u32ErrorValue )
      {
         /*---Set the Satellite System to the value given---*/
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK_227; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
            }

            if( 0 == u32ErrorValue )
            {
               u32GetFuncRet = u32GetSatSys();
               if( OSAL_ERROR == ( tS32 ) u32GetFuncRet )
               {
                  u32ErrorValue += 8;
               }
               else
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk227:"
                                     " Sat Sys Value = 0x%x ", u32GetFuncRet );
               }
            }

            if( 0 == u32ErrorValue )
            {
               if( u8SatSystobeSet[u16Loop] != u32GetFuncRet )
               {
                  u32ErrorValue += 16;
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk227:"
                                     " Sat Sys Value to be Set = 0x%x and Sat Sys Value"
                                     "returned after setting 0x%x", u8SatSystobeSet[u16Loop], u32GetFuncRet );
               }
            }
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32ChkSatCfgBlk227: Device Closing Failed at "
                            "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 16;
      }
   }
   
   return ( u32ErrorValue );
   
}



/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32ChkSatCfgBlk200227( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if combination of
||                       the satelllites in Cfg block 200 and Cfg Block 227
||                       are set as reqd.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/
tU32 GNSSOedt_u32ChkSatCfgBlk200227( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tU32 u32GetFuncRet = 0;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LMT_CFG_BLK] = {OSAL_C_U8_GNSS_SATSYS_ALL,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GLONASS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_COMPASS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS|\
                                                             OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS|\
                                                             OSAL_C_U8_GNSS_SATSYS_GLONASS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GLONASS|\
                                                             OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_GALILEO,
                                                             OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_COMPASS,
                                                             OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                             OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_COMPASS,
                                                             OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                             OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_COMPASS|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                             OSAL_C_U8_GNSS_SATSYS_COMPASS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_COMPASS|OSAL_C_U8_GNSS_SATSYS_SBAS|
                                                             OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_SBAS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                             OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO|\
                                                             OSAL_C_U8_GNSS_SATSYS_SBAS|OSAL_C_U8_GNSS_SATSYS_GLONASS};
   tU16 u16Loop = 0;

   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32ChkSatCfgBlk200227: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk200227: Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32ChkSatCfgBlk200227: "
                           "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                           OSAL_u32ErrorCode());
         u32ErrorValue += 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk200227: "
                              "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( 0 == u32ErrorValue )
      {
         /*---Set the Satellite System to the value given---*/
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
            }

            if( 0 == u32ErrorValue )
            {
               u32GetFuncRet = u32GetSatSys();
               if( OSAL_ERROR == ( tS32 ) u32GetFuncRet )
               {
                  u32ErrorValue += 8;
               }
               else
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk200227:"
                                                   " Sat Sys Value = 0x%x ", u32GetFuncRet );
               }
            }
            
            if( 0 == u32ErrorValue )
            {
               if( u8SatSystobeSet[u16Loop] != u32GetFuncRet )
               {
                  u32ErrorValue += 16;
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32ChkSatCfgBlk200227:"
                                     " Sat Sys Value to be Set = 0x%x and Sat Sys Value"
                                     "returned after setting 0x%x", u8SatSystobeSet[u16Loop], u32GetFuncRet );
               }
            }
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32ChkSatCfgBlk200227: Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 16;
      }
   }
   
   return ( u32ErrorValue );
   
}

/******************************************************************************
||FUNCTION    :  tU32 GNSSOedt_u32SatCfgBlk200227( tVoid )
||
||DESCRIPTION :  This OEDT is implemented to check if combination of
||                       the satelllites in Cfg block 200 and Cfg Block 227
||                       are set as reqd.
||
||PARAMETER   :  Nil
||
||RETURNVALUE :  tU32
||               0          - success 
||               Error code - failure
||HISTORY     :
||
******************************************************************************/
tU32 GNSSOedt_u32SatCfgBlk200227( tVoid )
{
   tU32 u32SetFuncRet = 0;
   tU32 u32GetFuncRet = 0;
   tU32 u32ErrorValue = 0;
   tU8 u8SatSystobeSet[OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK] = {OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GLONASS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS|\
                                                               OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_SBAS|\
                                                               OSAL_C_U8_GNSS_SATSYS_GLONASS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GLONASS|\
                                                               OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_GALILEO,
                                                               OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                               OSAL_C_U8_GNSS_SATSYS_GLONASS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_SBAS,
                                                               OSAL_C_U8_GNSS_SATSYS_GALILEO|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_SBAS|OSAL_C_U8_GNSS_SATSYS_QZSS,
                                                               OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO|\
                                                               OSAL_C_U8_GNSS_SATSYS_SBAS|OSAL_C_U8_GNSS_SATSYS_GLONASS};
   tU16 u16Loop = 0;

   /*---Open the GNSS driver---*/
   hDeviceHandle = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GNSS, OSAL_EN_READONLY );
   if( hDeviceHandle == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk200227: Device Opening Error at Line : %d with ErrorCode : %d ", 
                                                                         __LINE__, OSAL_u32ErrorCode() );
      u32ErrorValue += 1;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200227: Device Open Successful " );
      
      /*---Flush Sensor Data---*/
      if(OSAL_ERROR == OSAL_s32IOControl( hDeviceHandle, OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA, 0) )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERROR, "GNSSOedt_u32SatCfgBlk200227: "
                           "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA failed %d",
                           OSAL_u32ErrorCode());
         u32ErrorValue += 2;
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200227: "
                              "OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA Success ");
      }

      if( 0 == u32ErrorValue )
      {
         /*---Set the Satellite System to the value given---*/
         for( u16Loop = 0; u16Loop < OEDT_GNSS_SET_SAT_SYS_LIMIT_CFG_BLK; u16Loop ++)
         { 
            u32SetFuncRet = u32SetSatSys( u8SatSystobeSet[u16Loop] );
            if( OSAL_ERROR == ( tS32 ) u32SetFuncRet )
            {
               u32ErrorValue += 4;
            }

            if( 0 == u32ErrorValue )
            {
               u32GetFuncRet = u32GetSatSys();
               if( OSAL_ERROR == ( tS32 ) u32GetFuncRet )
               {
                  u32ErrorValue += 8;
               }
               else
               {
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200227:"
                                                   " Sat Sys Value = 0x%x ", u32GetFuncRet );
               }
            }
            
            if( 0 == u32ErrorValue )
            {
               if( u8SatSystobeSet[u16Loop] != u32GetFuncRet )
               {
                  u32ErrorValue += 16;
                  OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "GNSSOedt_u32SatCfgBlk200:"
                                     " Sat Sys Value to be Set = 0x%x and Sat Sys Value"
                                     "returned after setting 0x%x", u8SatSystobeSet[u16Loop], u32GetFuncRet );
               }
            }
         }
         
      }
      
      if( OSAL_s32IOClose( hDeviceHandle ) == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "GNSSOedt_u32SatCfgBlk200227: Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         u32ErrorValue += 16;
      }
   }
   
   return ( u32ErrorValue );
   
}



//EOF
