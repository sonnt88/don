/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_memory_testfuncs.c
 *
 * CREATED:     2005-03-08
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *
 * DESCRIPTION:
 *      OEDT tests for the memory  management system
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 * HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
                Version - 1.1
				Version - 1.2 Sainath Kalpuri (RBEI/ECF1) ON 25, March 2009
				Removed compiler and lint warnings
 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#include "OsalConf.h"

//#include "osal.h" tnn was here
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include <extension/memory.h>
#include "oedt_helper_funcs.h"

#define NO_OF_TESTS 1000L             /* no of malloc/free tests per task */
#define MAX_MEMORY_FOR_TEST 20000000L  /* max memory for all test tasks (without stack) */
#define STACK_USAGE 1                  /* stack usage by every recursion stage */

#define NO_OF_CONCURRENT_TASKS 5       /* no of concurrent tasks AND HISR's */
#define STACK_DEEP_TEST_ON 1           /* increase stack usage by tasks */

/* here a function from TKERNEL is needed */

extern tBool bGetPerfCounter(tU64* pu64Counter,tU32* pu32Option);


typedef struct
{
    size_t size;
    unsigned char *mem;
    unsigned char testPattern;
} memTest_object_t;


OSAL_tSemHandle protect_semaphore = (tU32)NULL;


static void init_protect(void)
{
    tS32 s32_err;
    if (protect_semaphore == (tU32)NULL)
    {
       
        s32_err = OSAL_s32SemaphoreCreate ("MemTestSem", &protect_semaphore, 1);
        if (s32_err == OSAL_ERROR)
        {
            FATAL_M_ASSERT_ALWAYS();
        }
    }
}

static void protect(void)
{
    
    init_protect();
    OSAL_s32SemaphoreWait(protect_semaphore, OSAL_C_TIMEOUT_FOREVER);
    return;
}

//-------------------------------------------------

static void unprotect(void)
{
    tS32 s32_err;
    init_protect();
    s32_err = OSAL_s32SemaphorePost(protect_semaphore);
    if (s32_err == OSAL_ERROR)
    {
        FATAL_M_ASSERT_ALWAYS();
    }
}

static unsigned long MemTestNormRandValue(unsigned long randValue, int norm)
{
    unsigned long result;
#ifdef TSIM_OSAL
    result =    (( ((unsigned long)(randValue)) * ((unsigned long)norm)) / (RAND_MAX));
#else /*#ifdef TSIM_OSAL*/
    result =    (( ((unsigned long)(randValue>>16)) * ((unsigned long)norm)) / (RAND_MAX>>16));
#endif/*#ifdef TSIM_OSAL*/
    if (result > (unsigned long)norm) result = (unsigned long)norm;
    return result;
}

#define NORM_RAND_VALUE(a, n) MemTestNormRandValue((unsigned long)(a), (int)(n))

static tU64 mallocTime = 0;
static tU64 realNumberOfMallocs = 0;
static tU64 freeTime = 0;
static tU64 realNumberOfFrees = 0;

static void _MemTestFree(void *ptr)
{
    tU64 startTime = 0;
    tU64 endTime = 0;
    tBool b_perf_err = FALSE;
    tU32  u32_perf_opt = 0;
#ifndef TSIM_OSAL
    if(bGetPerfCounter(&startTime, &u32_perf_opt) != TRUE){
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"bGetPerfCounter failed");
        b_perf_err = TRUE;
    }
#endif /*#ifndef TSIM_OSAL*/
    
    free(ptr);
#ifndef TSIM_OSAL
    if(bGetPerfCounter(&endTime, &u32_perf_opt) != TRUE){
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"bGetPerfCounter failed");
        b_perf_err = TRUE;
    }
#endif 
    if( b_perf_err == FALSE){
        if(endTime >= startTime){
            freeTime += (endTime   - startTime);
        }else{
            freeTime += (startTime - endTime);
        }
    }
    protect();
    realNumberOfFrees++;
    unprotect();
}

static void *_MemTestMalloc(size_t size, int retry)
{
    unsigned char *mem;
    tU64 startTime = 0;
    tU64 endTime = 0;
    tBool b_perf_err = FALSE;
    tU32  u32_perf_opt = 0;
    tInt retry_cnt = retry+1;
    /* try to alloc */
    mem = NULL;
    while((mem == NULL) &&
          (retry_cnt > 0))
        {
#ifndef TSIM_OSAL
            if(bGetPerfCounter(&startTime, &u32_perf_opt) != TRUE){
                OEDT_HelperPrintf(TR_LEVEL_FATAL,"bGetPerfCounter failed");
                b_perf_err = TRUE;
            }
#endif
            mem = malloc(size);
            if (!retry) return mem;
            OSAL_s32ThreadWait(100);
            retry_cnt--;
        }

#ifndef TSIM_OSAL
    /* statistics */
    if(bGetPerfCounter(&endTime, &u32_perf_opt) != TRUE){
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"bGetPerfCounter failed");
        b_perf_err = TRUE;
    }
#endif
    if( b_perf_err == FALSE){
        if(endTime >= startTime){
            mallocTime += (endTime   - startTime);
        }else{
            mallocTime += (startTime - endTime);
        }
    }
    protect();
    realNumberOfMallocs++;
    unprotect();

    /* return pointer */
    return(void *)mem;
}

static tU32 memTestTaskError = 0;
static tU32 memTestTaskEnd = 0;
static tU32 memTestTaskStart = 0;
static OSAL_tThreadID tasksHandles[NO_OF_CONCURRENT_TASKS];
int noOfConcurrentMalloc[] = {32, 64, 128, 256, 512, 1024, 2048, 4096, 8192}; // normal version 


static int memTestRecursiveFunction(int testPattern, int count)
{
    volatile int stackusage[STACK_USAGE]; /* this is to consume stack */
    int i;

    /* fill the pattern */
    for (i=0; i<STACK_USAGE; i++)
    {
        stackusage[i] = testPattern;
    }

    /* recurse? */
    if (count)
    {
        memTestRecursiveFunction(testPattern, count-1);
    }

    /* check the pattern */
    for (i=0; i<STACK_USAGE; i++)
    {
        if (stackusage[i] != testPattern)
        {
            return 1000;
        }
    }

    /* recursion */
    return 0;
}

static tU32 tU32MemTestAgressive(int noOfConcurrentMallocs, size_t maxMemory)
{
    int i;
    memTest_object_t *testObj;
    int j;
    int runs;
    size_t maxMallocSpace = maxMemory / (size_t)noOfConcurrentMallocs;
    int status = 0;
#if STACK_DEEP_TEST_ON
    int stackDeep;
#endif

    /* init the test */
    testObj = (memTest_object_t *)_MemTestMalloc((size_t)noOfConcurrentMallocs * sizeof(memTest_object_t), 1);
    if (!testObj) return 99;
    memset(testObj, 0, (size_t)noOfConcurrentMallocs * sizeof(memTest_object_t));

    /* first alloc space in random order / matter */
    for (i=0; i<noOfConcurrentMallocs; i++)
    {

        /* alloc space and register it */
        testObj[i].mem = NULL;
        while (!testObj[i].mem)
        {
            testObj[i].size = NORM_RAND_VALUE(rand(), 1 + maxMallocSpace);
            if(testObj[i].size == 0){
                testObj[i].size++;
            }
            testObj[i].mem = _MemTestMalloc(testObj[i].size, 0);
            if (testObj[i].mem) break;
            OSAL_s32ThreadWait(100);  
        }
        if (!testObj[i].mem)
        {
            for (j=0; j<i; j++)
            {
                _MemTestFree(testObj[j].mem);
            }
            _MemTestFree(testObj);
            return 1;
        }

        /* set testpattern */
        testObj[i].testPattern = (unsigned char)NORM_RAND_VALUE(rand(), 255);

        /* write it to memory */
        memset(testObj[i].mem, testObj[i].testPattern, testObj[i].size);
    }

    /* testloop */
    for (runs=0; runs < NO_OF_TESTS; runs++)
    {

#if STACK_DEEP_TEST_ON
        /* call a recursive function to consume stack and check that */
        stackDeep = (int)NORM_RAND_VALUE(rand(), 10+(runs/10));
        status = memTestRecursiveFunction(rand(), stackDeep);
        if (status)
        {

            /* free all memories */
            for (i=0; i<noOfConcurrentMallocs; i++)
            {
                _MemTestFree(testObj[i].mem);
            }
            _MemTestFree(testObj);
            return 4;
        }
#endif

        /* select a test object for retest */
        i = (int)NORM_RAND_VALUE(rand(), noOfConcurrentMallocs-1);

        /* retest memory */
        for (j=0; j<(int)testObj[i].size; j++)
        {
            if (testObj[i].mem[j] != testObj[i].testPattern) break;
        }

        /* error in compare? */
        if (j != (int)testObj[i].size)
        {

            /* free all memories */
            for (i=0; i<noOfConcurrentMallocs; i++)
            {
                _MemTestFree(testObj[i].mem);
            }
            _MemTestFree(testObj);
            return 2;
        }

        /* free the memory */
        _MemTestFree(testObj[i].mem);

        /* realloc memory for this test slot */
        testObj[i].mem = NULL;
        while (!testObj[i].mem)
        {
            testObj[i].size = NORM_RAND_VALUE(rand(), 1 + maxMallocSpace);
            if(testObj[i].size == 0){
                testObj[i].size++;
            }
            testObj[i].mem = _MemTestMalloc(testObj[i].size, 0);
            if (testObj[i].mem) break;
            OSAL_s32ThreadWait(100);
        }
        if (!testObj[i].mem)
        {

            /* save i */
            j = i;

            /* free all memories */
            for (i=0; i<noOfConcurrentMallocs; i++)
            {
                if (i != j)
                {
                    _MemTestFree(testObj[i].mem);
                }
            }
            _MemTestFree(testObj);
            return 3;
        }

        /* set testpattern */
        testObj[i].testPattern = (unsigned char)NORM_RAND_VALUE(rand(), 255);

        /* write it to memory */
        memset(testObj[i].mem, testObj[i].testPattern, testObj[i].size);
    }

    /* free the memories */
    for (i=0; i<noOfConcurrentMallocs; i++)
    {
        _MemTestFree(testObj[i].mem);
    }
    _MemTestFree(testObj);

    /* no error */
    return 0;
}

static int noOfConcurrentTasks;

static void memTestAgressiveTestTask(const int *num_conc_malloc)
{
    /* wait for concurrent start */

    while (!memTestTaskStart) OSAL_s32ThreadWait(100);

    /* do the test */
    memTestTaskError |= tU32MemTestAgressive((int)*num_conc_malloc, 
											(size_t)MAX_MEMORY_FOR_TEST/(size_t)noOfConcurrentTasks);

    /* unprotect the task */
    protect();
    memTestTaskEnd--;
    unprotect();
    
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread finished %d", (int)*num_conc_malloc);
    return;
}


tU32 u32MemTestAgressiveConcurrent(tVoid)
{
    int i;
    //ER status;
    tU32 startTime;
    tU32 stopTime;

    /* init */
    memTestTaskError = 0;
    memTestTaskEnd = 0;
    memTestTaskStart = 0;

    /* get start time */
    startTime = OSAL_ClockGetElapsedTime();

    /* test */
    srand(0);
    //OEDT_HelperPrintf(TR_LEVEL_USER_1,"rand()=%d, RAND_MAX=%d, norm(255)=%d", rand(), RAND_MAX, NORM_RAND_VALUE(rand(), 255));
    //OEDT_HelperPrintf(TR_LEVEL_USER_1,"rand()=%d, RAND_MAX=%d, norm(255)=%d", rand(), RAND_MAX, NORM_RAND_VALUE(rand(), 255));
    //OEDT_HelperPrintf(TR_LEVEL_USER_1,"rand()=%d, RAND_MAX=%d, norm(255)=%d", rand(), RAND_MAX, NORM_RAND_VALUE(rand(), 255));

    noOfConcurrentTasks = NO_OF_CONCURRENT_TASKS;

    /* start the tasks */
    for (i=0; i<noOfConcurrentTasks; i++)
    {

        OSAL_trThreadAttribute threadAttr;
        tChar sz_name[128] = "\0";
        /* start the memtest task */

        /* protect the task counter */
        protect();
        memTestTaskEnd++;
        unprotect();
        OSALUTIL_s32SaveNPrintFormat(sz_name, sizeof(sz_name) -1, "%s%d", "MemTestTask", i);
        sz_name[127] = '\0';
        
        threadAttr.u32Priority  = 120;
        threadAttr.s32StackSize = 1024;

        threadAttr.szName       = sz_name;
        threadAttr.pfEntry      = (OSAL_tpfThreadEntry) memTestAgressiveTestTask;
        threadAttr.pvArg        = (void*) &(noOfConcurrentMalloc[i]);


        tasksHandles[i] = OSAL_ThreadSpawn(&threadAttr);

        OEDT_HelperPrintf(TR_LEVEL_USER_1,"start task %d (allocs %d)", i+1, noOfConcurrentMalloc[i]);
    	OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                  "Task Handle = %d",
		                  tasksHandles[i]);

    }

    /* init the random number generator */
    srand(0);

    /* start all task conccurrent */
    memTestTaskStart = 1;

    /* wait for end of all tasks */

    while (memTestTaskEnd)             OSAL_s32ThreadWait(100);

    /* get end time */
    stopTime = OSAL_ClockGetElapsedTime();

    /* print time */
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32MemTestAgressiveConcurrent: duration=%d ms", stopTime-startTime);

    /* return error */
    return memTestTaskError;
}


static tU32 memTestConsumeMem(int runTime)
{
    tU32 retry;
    tU32 status = 0;
	tU32 noOfConcurrentMalloc_local = 1024;
    /* seed the rand */
    srand(0);

    /* do the test several times */
    for (retry=(tU32)runTime; retry; retry--)
    {

        /* init */

        /* do one agressive test */
        status = tU32MemTestAgressive((int)noOfConcurrentMalloc_local /*(int)NORM_RAND_VALUE(rand(), 1024)*/, MAX_MEMORY_FOR_TEST);

        /* check the tgest status */
        if (status) break;
    }

    /* end */
    return status;
}

#define NU_OF_RUNS 10
/*****************************************************************************
* FUNCTION:	    u32MemTestMemoryLeak(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                  
*               2. Does the Memory leak test.
*                  
*
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               Updated the code to measure the Free space availabe in Memory 
******************************************************************************/
tU32 u32MemTestMemoryLeak(tVoid)
{
	M_STATE	memsts;
    tU32 spaceBefore;
    tU32 spaceAfter;
    tU32 status;
	tS32 err = 0;
   // tU32 stackSpaceBefore;
   // tU32 stackSpaceAfter;
   // tU32 overheadSpaceBefore;
    //tU32 overheadSpaceAfter;
    static int firstConsume = 1;

    /* consume first the memory */  
    if (firstConsume)
    {
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32MemTestMemoryLeak: first consume ...");
        status = memTestConsumeMem(NU_OF_RUNS);
        if (status) return status;
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32MemTestMemoryLeak: first consume ... done");
        firstConsume = 0;
    }

    /* measure space before calls */
	/* Obtain memory information */
	err = tkse_mbk_sts(&memsts);
	if ( err < E_OK )
	 {
		return 1;
	 }
	 spaceBefore = (tU32)memsts.free;/* the number of remaining blocks*/
    /* re-consume the memory */ 
    status = memTestConsumeMem(NU_OF_RUNS);
    if (status) return status;

	/* Obtain memory information */
	err = tkse_mbk_sts(&memsts);
	if ( err < E_OK )
	 {
		return 10;
	 }
	 spaceAfter = (tU32)memsts.free; /* the number of remaining blocks*/
    /* check the spaces */
    if (spaceAfter != spaceBefore)
    {
		#if 0
        /* additional printout */
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"        space      stack      overhead");
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"before: 0x%08x 0x%08x 0x%08x", spaceBefore, stackSpaceBefore, overheadSpaceBefore);
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"after : 0x%08x 0x%08x 0x%08x", spaceAfter, stackSpaceAfter, overheadSpaceAfter);
		#endif
        /* error: memory leak! */
        return 100;
    }

    /* no error */
    return 0;
}

#define NO_OF_SPEED_CHECK_TESTS	 100 /* No of Speed check tests are decreased to 128 
from 1000 to avoid the timeout by the test - hbs2kor */
static tU32 tU32MemTestSpeed(int noOfConcurrentMallocs, size_t maxMemory)
{
    int i;
    memTest_object_t *testObj;
    int j;
    int runs;
    size_t maxMallocSpace = maxMemory / (size_t)noOfConcurrentMallocs;

    /* init the test */
    testObj = (memTest_object_t *)_MemTestMalloc((size_t)noOfConcurrentMallocs * sizeof(memTest_object_t), 1);
    if (!testObj) return 99;
    memset(testObj, 0, (size_t)noOfConcurrentMallocs * sizeof(memTest_object_t));

    
       /* first alloc space in random order / matter */
    for (i=0; i<noOfConcurrentMallocs; i++)
    {

        /* alloc space and register it */
        testObj[i].size = (size_t)NORM_RAND_VALUE(rand(), 1 + maxMallocSpace);
        if(testObj[i].size == 0){
            testObj[i].size++;
        }
        testObj[i].mem = _MemTestMalloc(testObj[i].size, 1);
        if (!testObj[i].mem)
        {
            for (j=0; j<i; j++)
            {
                _MemTestFree(testObj[j].mem);
            }
            _MemTestFree(testObj);
            return 1;
        }

        /* set testpattern */
        testObj[i].testPattern = (unsigned char)NORM_RAND_VALUE(rand(), 255);

        /* write it to memory */
        memset(testObj[i].mem, testObj[i].testPattern, testObj[i].size);
    }

    /* testloop */
    for (runs=0; runs < NO_OF_SPEED_CHECK_TESTS; runs++)
    {

        /* select a test object for retest */
        i = (int)NORM_RAND_VALUE(rand(), noOfConcurrentMallocs-1);

        /* retest memory */
        for (j=0; j<(int)testObj[i].size; j++)
        {
            if (testObj[i].mem[j] != testObj[i].testPattern) break;
        }

        /* error in compare? */
        if (j != (int)testObj[i].size)
        {

            /* free all memories */
            for (i=0; i<noOfConcurrentMallocs; i++)
            {
                _MemTestFree(testObj[i].mem);
            }
            _MemTestFree(testObj);
            return 2;
        }

        /* free the memory */
        _MemTestFree(testObj[i].mem);

        /* realloc memory for this test slot */
        testObj[i].size = (size_t)NORM_RAND_VALUE(rand(), 1 + maxMallocSpace);
        if(testObj[i].size == 0){
            testObj[i].size++;
        }
        testObj[i].mem = _MemTestMalloc(testObj[i].size, 1);
        if (!testObj[i].mem)
        {

            /* save i */
            j = i;

            /* free all memories */
            for (i=0; i<noOfConcurrentMallocs; i++)
            {
                if (i != j)
                {
                    _MemTestFree(testObj[i].mem);
                }
            }
            _MemTestFree(testObj);
            return 3;
        }

        /* set testpattern */
        testObj[i].testPattern = (unsigned char)NORM_RAND_VALUE(rand(), 255);

        /* write it to memory */
        memset(testObj[i].mem, testObj[i].testPattern, testObj[i].size);
    }

    /* free the memories */
    for (i=0; i<noOfConcurrentMallocs; i++)
    {
        _MemTestFree(testObj[i].mem);
    }
    _MemTestFree(testObj);

    /* no error */
    return 0;
}

#define NO_OF_MALLOCS_FOR_SPEED_TEST 128 /* No of tests are decreased to 128 from 1000
to avoid the timeout by the test */
#define SIZE_FOR_SPEED_TEST 1000000
/*****************************************************************************
* FUNCTION:	    u32MemTestSpeedCheck(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                  
*               2. Does the memory Speed test.
*                  
*
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               No of tests are decreased to 128 from 1000
                to avoid the timeout by the test
******************************************************************************/
tU32 u32MemTestSpeedCheck(tVoid)
{
    tU32 result;
    double dTime;
    int repeats;

    /* init */
    mallocTime = 0;
    realNumberOfMallocs = 0;
    freeTime = 0;
    realNumberOfFrees = 0;

    /* test */
    for (repeats=0; repeats<10; repeats++)
    {
        result = tU32MemTestSpeed(NO_OF_MALLOCS_FOR_SPEED_TEST, SIZE_FOR_SPEED_TEST);
        if (result) return result;
    }

    /* print result */
    //dTime = (double)(mallocTime * 104) / 10000.;
    dTime = (double)(mallocTime);
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"malloc # = %d", (tU32)realNumberOfMallocs);
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"malloc t = %f ns", dTime / realNumberOfMallocs);

    // dTime = (double)(freeTime * 104) / 10000.;
    dTime = (double)(freeTime);
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"free   # = %d", (tU32)realNumberOfFrees);
    OEDT_HelperPrintf(TR_LEVEL_USER_1,"free   t = %f ns", dTime / realNumberOfFrees);

    /* end */
    return 0;
}


/*****************************************************************************
* FUNCTION:	    u32MemTestHeap(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                  
*               2. Tries to allocate the memory from Heap until heap size exeeds
                   it's limit.
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tU32 u32MemTestHeap(tVoid)
{

    tU32 u32_ret = 0;
    tU32 i = 0, j = 0;
    M_STATE pk_sts = { 0 };
    ER ercd = 0;
    tU32 u32FreeMemoBefore = 0;
    tU32 u32_num_allocated_blocks = 0;
    tU8 **pp_u8_memblocks = NULL;
    
    ercd = tkse_mbk_sts(&pk_sts);
    OEDT_HelperPrintf(TR_LEVEL_USER_1, "Before the test: Free Memory : %lu", pk_sts.free);
    
    if(ercd == E_OK){
        u32_num_allocated_blocks = pk_sts.free - 10; // allocate all - 10 blocks      
        pp_u8_memblocks = (tU8**) OSAL_pvMemoryAllocate(u32_num_allocated_blocks * sizeof(tU8*));
        
        if(pp_u8_memblocks != OSAL_NULL){
            for(i=0;i<u32_num_allocated_blocks;i++){
                *(pp_u8_memblocks+i) = (tU8*) OSAL_pvMemoryAllocate(pk_sts.blksz * sizeof(tU8));
                if(*(pp_u8_memblocks+i) == OSAL_NULL){
                    u32_ret += 10;
                }
            }
            
            for(i=0;i<u32_num_allocated_blocks;i++){
                for(j=0;j<pk_sts.blksz;j++){
                    *( (*(pp_u8_memblocks+i)) + j ) = (i * pk_sts.blksz) + j;
                }
            }
            
            for(i=0;i<u32_num_allocated_blocks;i++){
                for(j=0;j<pk_sts.blksz;j++){
                    if( *( (*(pp_u8_memblocks+i)) + j ) != (i * pk_sts.blksz) + j){
                        u32_ret += 100;
                    }
                }
            }
            
            for(i=0;i<u32_num_allocated_blocks;i++){
                if(*(pp_u8_memblocks+i) != OSAL_NULL){
                    OSAL_vMemoryFree(  *(pp_u8_memblocks+i) );
                }
            }
            
            OSAL_vMemoryFree(pp_u8_memblocks );
            
        }else{
            u32_ret = 2;
        }
        
    }else{
        u32_ret = 1;
    }
    
    
    /* end of test */
    return u32_ret;
}


