/******************************************************************************
 *FILE         : oedt_KDS_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk  
 *
 *DESCRIPTION  : This file implements the individual test cases ( OEDT testcases)
 *				 for KDS device	(Konfiguration Daten Speicher).
 *               Checking for valid pointer in each test cases is not required
 *               as it is ensured in the calling function that that it cannot
 *               be NULL.
 *
 *AUTHOR(s)    : Venkateswara.N
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
*********************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 
#define SYSTEM_S_IMPORT_INTERFACE_KDS_DEF
#include "system_pif.h"
#include "oedt_KDS_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*********************************************************************************/
/*                         define                                                */
/*********************************************************************************/
#define  OSAL_C_S32_IOCTRL_INVAL_FUNC	                -1000
#define  ZERO_PARAMETER                                     0
#define  OEDT_KDS_M_KDS_ENTRY_FLAG_NONE                  ((tU16)0x0000)
#define  OEDT_KDS_M_KDS_ENTRY_FLAG_WRITE_PROTECTED       ((tU16)0x0001)

/*********************************************************************************/
/*                         macro                                                 */
/*********************************************************************************/

/*********************************************************************************/
/*                         static variable                                       */
/*********************************************************************************/
//static tU16 u16ReceivedEntryId;

/*********************************************************************************/
/*                         static function                                       */
/*********************************************************************************/
/*Support functions,called from Test functions*/
tU32 SetUpKDS(const OSAL_tIODescriptor * handle);
tU32 WriteDataToKDS(const OSAL_tIODescriptor * handle,tEntry EntryID,tU8 EntryLength,const tU8 u8data[]);
tU32 CheckKDS(const OSAL_tIODescriptor * handle);
tU32 Scenario_Existing_Entry(const OSAL_tIODescriptor * handle, tEntry Entry);
//tU32 Scenario_Non_Existing_Entry(OSAL_tIODescriptor * handle, tEntry Entry);
//tU32 Non_Existing_Entry_beyond_KDSID(OSAL_tIODescriptor * handle, tEntry Entry);

/*****************************************************************************
* FUNCTION:		u32KDSDevOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_001
* DESCRIPTION:  Open the KDS device
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevOpen(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevOpenInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_002
* DESCRIPTION:  Device open with invalid parameter (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevOpenInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen(NULL,OSAL_EN_READONLY);
  if(hDevice != OSAL_ERROR)
  {
    u32Ret += 10;
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 20;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_003
* DESCRIPTION:  Try to open the device which is already opened.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevReOpen(void)
{
  OSAL_tIODescriptor hDevice_1 = 0,hDevice_2 = 0;
  tU32 u32Ret = 0;

  hDevice_1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READONLY );
  if(hDevice_1 == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    hDevice_2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READONLY );
    if(hDevice_2 == OSAL_ERROR)
    {
      u32Ret = 10;
    }
    else
    {
      if(OSAL_s32IOClose ( hDevice_2 ) == OSAL_ERROR)
      {
        u32Ret += 20;
      }
    }
    if(OSAL_s32IOClose ( hDevice_1 ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevOpenDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_004
* DESCRIPTION:  Try to open the device in different modes.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevOpenDiffModes(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
  }
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_WRITEONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret += 10;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 20;
    }
  }
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret += 100;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 200;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32KDSDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_005
* DESCRIPTION:  Try to Close the Opened device.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS , OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevCloseInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_006
* DESCRIPTION:  Closing the device with invalid parameter (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevCloseInvalParm(void)
{
  OSAL_tIODescriptor hDevice_inval = 0;
  tU32 u32Ret = 0;

  hDevice_inval = -1;
  if(OSAL_s32IOClose ( hDevice_inval ) != OSAL_ERROR)
  {
    u32Ret = 2;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevReClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_007
* DESCRIPTION:  Try to close a device which is already closed(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevReClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      /* Re Closing */
      if(OSAL_s32IOClose ( hDevice ) != OSAL_ERROR)
      {
        u32Ret = 20;
      }
    }

  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSEnaDisAccessright()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_008
* DESCRIPTION:  Try to  enable/disable write access rights for the device.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSEnaDisAccessright(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {/* For Enabling*/
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {/* For Disabling*/
      bAccessOption = 0;
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
      {
        u32Ret = 30;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSEnaDisAccessrightWithInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_009
* DESCRIPTION:  Try to  enable/disable write access rights for the device
*				with invalid parameter (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSEnaDisAccessrightWithInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = (tBool)-1;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) != OSAL_ERROR)
    {
      u32Ret = 3;
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
} 
/*****************************************************************************
* FUNCTION:		u32KDSClearAllData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_010
* DESCRIPTION:  Try to erase all the data.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSClearAllData(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tsKDS_Info  rKDSInfo;
  tBool bAccessOption = 0;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(Before)%d",rKDSInfo.u32NumberOfActiveEntries);
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 30;     
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDevInit()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_011
* DESCRIPTION:  Try to write the initial KDS structures to flash.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDevInit(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tsKDS_Info  rKDSInfo;
  tBool bAccessOption = 0;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {  
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(Before):%d",rKDSInfo.u32NumberOfActiveEntries);
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of all entries(Before):%d",rKDSInfo.u32NumberOfEntries);
    }
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 30;     
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {     
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of all entries(After)%d",rKDSInfo.u32NumberOfEntries);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetDevInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_012
* DESCRIPTION:  Try to get the device information.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSGetDevInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tsKDS_Info  rKDSInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS,OSAL_EN_READONLY);
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret = 3;     
    }
    else
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of all entries: %d",rKDSInfo.u32NumberOfEntries);    
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of ActiveEntries: %d",rKDSInfo.u32NumberOfActiveEntries);      
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Space (Bytes) used by KDS: %d",rKDSInfo.u32KDS_Size);  
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Space Used by KDS-Entries: %d",rKDSInfo.u32KDS_UsedSize);     
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"ID of first damaged entry: %d",rKDSInfo.u16IdWithError);
      /*check rKDSInfo.u16StateReadFlashData*/
      if ((rKDSInfo.u16StateReadFlashData > M_KDS_INFO_READ_FLASH_DATA_BACKUP_FILE)&&(rKDSInfo.u16StateReadFlashData!=0xffff))
      {
        u32Ret = 4;   
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetDevInfoWithInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_013
* DESCRIPTION:  Try to get the device information with Invalid Parameter 
*				(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSGetDevInfoWithInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,ZERO_PARAMETER) != OSAL_ERROR)
    {
      u32Ret = 3;
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
} 
/*****************************************************************************
* FUNCTION:		u32KDSWriteEntry()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_014
* DESCRIPTION:  Try to write data to the specified Entry ID.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
*				    Modified by Shilpa Bhat (RBIN/ECM1) on 23 Jan, 2008
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/
tU32 u32KDSWriteEntry(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tChar Software_Version [25] = "PLATFORM_XXXX";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;
  tU32 u32NumberOfActiveEntries = 0;
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      rEntryInfo.u16EntryLength = 24;
      rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
      rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
      /* delete entry, if exist */ 
      OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,(intptr_t)rEntryInfo.u16Entry);
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret = 3;
      }
      else
      {
        u32NumberOfActiveEntries = rKDSInfo.u32NumberOfActiveEntries;
      }
      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, Software_Version); 
      /* write entry*/
      if(OSAL_s32IOWrite ( hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {       
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
        if((u32NumberOfActiveEntries+1) != rKDSInfo.u32NumberOfActiveEntries)
          u32Ret += 100;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSWriteEntryWithInvalID ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_015
* DESCRIPTION:  Try to write data for Invalid ID (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
        Updated by Tinoy Mathews(RBIN/EDI3) May 10,2007.
        Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/
tU32 u32KDSWriteEntryWithInvalID(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tChar Software_Version [25] = "PLATFORM_XXXX";  
  /*KDS entry structures for storing entries with invalid id*/
  tsKDSEntry rEntryInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {
      /* entry with invalid id 0xFFFF*/
      rEntryInfo.u16EntryLength = 32;
      rEntryInfo.u16Entry = 0xFFFF;
      (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Software_Version); 
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
      {
        u32Ret = 30;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSWriteEntryWithInvalParam ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_016
* DESCRIPTION:  Try to write data with Invalid parameter(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
*
******************************************************************************/ 
tU32 u32KDSWriteEntryWithInvalParam(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tChar Write_Buffer [15] = "KDS_TESTING\n";
  tsKDSEntry rEntryInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {
      rEntryInfo.u16EntryLength = 32;
      rEntryInfo.u16Entry = 2;
      (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Write_Buffer); 
      if(OSAL_s32IOWrite(hDevice,OSAL_NULL,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
      {
        u32Ret = 30;
      }
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,ZERO_PARAMETER) != OSAL_ERROR)
      {
        u32Ret += 300;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSReadEntry()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_017
* DESCRIPTION:  Try to read data for a particular ID.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11,2008
******************************************************************************/
tU32 u32KDSReadEntry(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tU32 EntryLength = 0;
  tU32 EntryID = 0;
  tsKDSEntry rEntryInfo;
  tsKDS_Info  rKDSInfo;
  tChar Software_Version [25] = "PLATFORM_XXXX";
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {/* Write the Entry */
      rEntryInfo.u16EntryLength = 24;
      rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
      rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
	  (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, Software_Version); 
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret = 3;
      }
      if(OSAL_s32IOWrite ( hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
      else
      {/* Read the Entry */
        rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
        EntryID = rEntryInfo.u16Entry; 
        rEntryInfo.u16EntryLength = 24;
        EntryLength = rEntryInfo.u16EntryLength; 
        memset(rEntryInfo.au8EntryData,0, sizeof(rEntryInfo.au8EntryData));
        if(OSAL_s32IORead(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
        {
          u32Ret += 100;      
        }
        else
        {
          if(OSAL_s32StringNCompare(Software_Version,rEntryInfo.au8EntryData, EntryLength))
            u32Ret += 200;

          if(EntryLength != rEntryInfo.u16EntryLength ||  EntryID != rEntryInfo.u16Entry)
          {
            u32Ret += 500 ;
          }     
          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Length of the entry:%d",rEntryInfo.u16EntryLength);         
          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Entry :%s",rEntryInfo.au8EntryData);          
          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Flags of the Entry :%d",rEntryInfo.u16EntryFlags);
        }
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {      
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries %d",rKDSInfo.u32NumberOfActiveEntries);
      }
    }

    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSReadEntryWithInvalID ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_018
* DESCRIPTION:  Try to read data for Invalid ID (should fail). 
*               (Header of next valid entry must be read.) 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
*				Updated by Tinoy Mathews(RBIN/EDI3) May 10,2007
                Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/
tU32 u32KDSReadEntryWithInvalID(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tsKDSEntry rEntryInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {/*Entry with invalid entry id 0xFFFF*/
    rEntryInfo.u16Entry = 0xFFFF;
    rEntryInfo.u16EntryLength = 16;
    rEntryInfo.au8EntryData[0] = 0;
    if(OSAL_s32IORead(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
    {
      u32Ret = 2 ;         
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Length of the entry:%",rEntryInfo.u16EntryLength);   
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Entry :%s",rEntryInfo.au8EntryData);   
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Flags of the Entry :%d",rEntryInfo.u16EntryFlags);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 3;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSReadEntryWithInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_019
* DESCRIPTION:  Try to read data with Invalid Parameter(should fail). 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSReadEntryWithInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tsKDSEntry rEntryInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    rEntryInfo.u16Entry = 1;
    rEntryInfo.u16EntryLength = 16;
    rEntryInfo.au8EntryData[0] = 0;
    if(OSAL_s32IORead(hDevice,OSAL_NULL,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
    {
      u32Ret = 3;       
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Length of the entry:%d",rEntryInfo.u16EntryLength);    
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Entry :%s",rEntryInfo.au8EntryData);    
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Flags of the Entry :%d",rEntryInfo.u16EntryFlags);
    }
    if(OSAL_s32IORead(hDevice,(tS8 *)&rEntryInfo,ZERO_PARAMETER) != OSAL_ERROR)
    {
      u32Ret += 30;     
    }
    else
    {      
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Length of the entry:%d",rEntryInfo.u16EntryLength); 
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Entry :%s",rEntryInfo.au8EntryData);      
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Flags of the Entry :%d",rEntryInfo.u16EntryFlags);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		SetUpKDS()
* PARAMETER:    A valid KDS device descriptor address.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Sets up write enable for KDS,clears KDS and inititiaizes KDS
* HISTORY:		Created by Tinoy Mathews(RBIN/EDI3)	on May 7,2007
******************************************************************************/
tU32 SetUpKDS(const OSAL_tIODescriptor *handle)
{
  tBool bAccessOption = 0;
  tU32 u32Ret = 0;

  /*Enable for write into KDS*/
  bAccessOption = 1;
  if(OSAL_s32IOControl (*handle,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption ) == OSAL_ERROR)
  {
    u32Ret += 3;
  }
  /*Clear KDS*/
  if(OSAL_s32IOControl (*handle,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
  {
    u32Ret += 5;      
  }
  /*Do a KDS init*/
  if(OSAL_s32IOControl (*handle,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
  {
    u32Ret += 30;     
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		WriteDataToKDS()
* PARAMETER:    A valid KDS device descriptor address.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Writes 10 entries into KDS
* HISTORY:		Created by Tinoy Mathews(RBIN/EDI3)	on May 7,2007
                Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11,2008
******************************************************************************/
tU32 WriteDataToKDS(const OSAL_tIODescriptor * handle, tEntry EntryID,tU8 EntryLength,const tU8 u8data[])
{
  tU32 u32Ret = 0;
  tsKDSEntry rEntryInfo;
  rEntryInfo.u16EntryLength = EntryLength;
  rEntryInfo.u16Entry =       EntryID;
  rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
  tU8 n;
  for(n=0;(n<EntryLength);n++)
  {
    rEntryInfo.au8EntryData[n] = (u8data[n]);
  }
  /*Write the KDS Entries*/
  if(OSAL_s32IOWrite(*handle,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
  {
    u32Ret += 30;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		CheckKDS()
* PARAMETER:    A valid KDS device descriptor address.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check the KDS.
* HISTORY:		Created by Tinoy Mathews(RBIN/EDI3)	on May 7,2007
******************************************************************************/
tU32 CheckKDS(const OSAL_tIODescriptor * handle)
{
  tsKDS_Info  rKDSInfo;
  tU32 u32Ret = 0;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  if(OSAL_s32IOControl (*handle,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
  {
    u32Ret += 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		Scenario_Existing_Entry()
* PARAMETER:    Address of a valid KDS dev handle
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Apply control function Get next id on an existing entry.
* HISTORY:		Created by Tinoy Mathews(RBIN/EDI3)	on May 7,2007
                Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/
tU32 Scenario_Existing_Entry(const OSAL_tIODescriptor * handle, tEntry Entry )
{
  tsKDSEntry rEntryInfoTemp;
  tU32 u32Ret = 0;

  /*Scenario 1 <---> On an already existing entry*/
  rEntryInfoTemp.u16Entry = Entry ;//rEntryInfoTemp.u16EntryLength = 20;
  /*Try to get next Entry*/
  if(OSAL_s32IOControl(*handle,OSAL_C_S32_IOCTRL_KDS_GET_NEXT_ID,(intptr_t)&rEntryInfoTemp ) == OSAL_ERROR)
  {
    u32Ret = 3;
  }
  /* If there is no next entry(i.e no greater ID found to the given ID), 
  It returns the same ID */
  if(!(rEntryInfoTemp.u16Entry >= Entry))
    u32Ret = 1;
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetNextEntryID()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_021
* DESCRIPTION:  Try to get the next entry ID.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated By Rakesh Dhanya(RBIN/EDI3) on April 23, 2007.
*				Updated By Tinoy Mathews(RBIN/EDI3) on May 7,2007
          Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/
tU32 u32KDSGetNextEntryID(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tEntry Entry1 = 0, Entry2 = 0;
  tU8 Sw_version[25] = "PLATFORM_XXXX";
  tU8   Sw_version_Entry_Length = 24;
  tU32 Production_Date = 0x07D90501 ; // 1-05-2009
  tU8 * prodate =0;
  tU8    Prod_Date_Entry_Length = 4;
  tU8   u8date[10] ;


  /*Open the KDS device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1; 
  }
  else
  {
    if(SetUpKDS(&hDevice))      /*KDS Setup*/
      u32Ret = 2;
    if(CheckKDS(&hDevice))      /*Check KDS*/
      u32Ret += 3 ;
    /*Make the KDS Entries*/
    Entry1 = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
    /* Write SW vesrion entry into KDS */
    if(WriteDataToKDS(&hDevice, Entry1, Sw_version_Entry_Length, Sw_version))
      u32Ret += 10;
    /* Write Production date into KDS */
    Entry2 = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_PROD_DATE);
    prodate = (tU8*)&Production_Date;
    u8date[0] = *prodate;
    u8date[1] = *(prodate+1);
    u8date[2] = *(prodate+2);
    u8date[3] = *(prodate+3);     
    if(WriteDataToKDS(&hDevice, Entry2, Prod_Date_Entry_Length, u8date))
      u32Ret += 20;
    if(CheckKDS(&hDevice))      /*Check KDS*/
      u32Ret += 100;
    /*Get Next id on an existing id*/
    if(Scenario_Existing_Entry(&hDevice,Entry1))
      u32Ret += 200;
    /*Revert KDS to clear state*/
    if(SetUpKDS(&hDevice))       /*KDS Setup*/
      u32Ret += 2000;
    if(CheckKDS(&hDevice))       /*Check KDS*/
      u32Ret += 5000;
    /*Close KDS handle*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
      u32Ret += 10000;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetNextEntryIDWithInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_022
* DESCRIPTION:  Try to get the next entry ID with Invalid parameter (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSGetNextEntryIDWithInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_GET_NEXT_ID,ZERO_PARAMETER) != OSAL_ERROR)
    {
      u32Ret = 3;
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
} 
/*****************************************************************************
* FUNCTION:		u32KDSDeleteEntryID()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_023
* DESCRIPTION:  Try to make the particular Entry ID as invalid.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11,2008
******************************************************************************/
tU32 u32KDSDeleteEntryID(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tU16 u16EntryId = 0;
  tsKDS_Info  rKDSInfo;
  tBool bAccessOption = 0;
  tChar Software_Version [25] = "PLATFORM_XXXX";
  tsKDSEntry rEntryInfo;
  tU32 EntryID = 0;
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(Before)%d",rKDSInfo.u32NumberOfActiveEntries);     
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of all entries(Before)%d",rKDSInfo.u32NumberOfEntries);
    }
    bAccessOption = 1;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    rEntryInfo.u16EntryLength = 24;
    rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
    rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
    (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Software_Version ); 
    if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
    u16EntryId =  rEntryInfo.u16Entry;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,(intptr_t)u16EntryId) == OSAL_ERROR)
    {
      u32Ret += 30;     
    }
    rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
    EntryID = rEntryInfo.u16Entry; 
    rEntryInfo.u16EntryLength = 24;
    rEntryInfo.au8EntryData[0] = 0;
    if(OSAL_s32IORead(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
    {
      u32Ret += 100 ;     
    }
    if(EntryID == rEntryInfo.u16Entry)
      u32Ret += 200 ;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
    {     
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO. of all entries(After)%d",rKDSInfo.u32NumberOfEntries);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSDeleteEntryIDWithInvalID()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_024
* DESCRIPTION:  Try to make the Non-Exist Entry ID as invalid (should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSDeleteEntryIDWithInvalID(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tS16 s16EntryId = -1;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,(intptr_t)s16EntryId) != OSAL_ERROR)
    {
      u32Ret = 3;     
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32KDSGetDevVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_025
* DESCRIPTION:  Try to get the device version number.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32 u32KDSGetDevVersion(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tS32 s32Version = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_VERSION,(intptr_t)&s32Version) == OSAL_ERROR)
    {
      u32Ret = 3;     
    }
    else
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Version No. : %d",s32Version);
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetDevVersionWithInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_026
* DESCRIPTION:  Try to get the device version number with invalid Parameter 
*				(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*				Updated By Rakesh Dhanya(RBIN/EDI3) on April 23, 2007.

******************************************************************************/
tU32 u32KDSGetDevVersionWithInvalParm(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_VERSION,ZERO_PARAMETER) != OSAL_ERROR)
    {
      u32Ret = 3;     
    }
    else
    {
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSInvalMacroToIOCTL()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_027
* DESCRIPTION:  Try to pass invalid macro to IOCTL function(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006 
*
******************************************************************************/
tU32  u32KDSInvalMacroToIOCTL(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tChar szBuffer [10];
  tS32 tIoctl_ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if((tIoctl_ret = OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_INVAL_FUNC,(intptr_t)szBuffer )) != (OSAL_ERROR))
    {
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Return Value :%d(%d)",tIoctl_ret,OSAL_ERROR);
      u32Ret  = 3;
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetDevVersionAfterDevclose
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_028
* DESCRIPTION:  Try to get the device version number after closing the device(should fail)
* HISTORY:		Created Kishore Kumar.R (RBIN/ECM1) DECEMBER 5 , 2006 
*
******************************************************************************/
tU32 u32KDSGetDevVersionAfterDevclose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tChar szVersionBuffer [5]={0};

  hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY);
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_VERSION,(intptr_t)szVersionBuffer) != OSAL_ERROR)
    {
      u32Ret += 4 ;     
    }
    else
    {    
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"Version No. : %S",szVersionBuffer);
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32KDSReWriteProtected()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_029
* DESCRIPTION:  Try to rewrite the write protected entry (should fail).
* HISTORY:		Created Kishore Kumar.R (RBIN/ECM1) DECEMBER 5 , 2006 
*				Updated by Rakesh Dhanya (RBIN/ECM1) on January 10, 2007.
******************************************************************************/
tU32 u32KDSReWriteProtected(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tU32 NumEntriesBefore = 0;
  tU32 EntryID = 0;
  tBool bAccessOption = 0;
  tChar Write_Buffer [] = "REWRITING_DATA\n";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 2;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 4 ;
    }
    else
    {
      rEntryInfo.u16EntryLength = 16;
      rEntryInfo.u16Entry =0x013 ;
      EntryID = rEntryInfo.u16Entry;
      rEntryInfo.u16EntryFlags=OEDT_KDS_M_KDS_ENTRY_FLAG_WRITE_PROTECTED;     
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO.of active Entries(Before)%d",rKDSInfo.u32NumberOfActiveEntries);
      }
      (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Write_Buffer ); 
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret += 8;
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      }
      /****try to rewrite***********/
    }

    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 16;
    }
    else
    {
      rEntryInfo.u16EntryLength = 16;
      rEntryInfo.u16Entry = (tU16)EntryID;
      rEntryInfo.u16EntryFlags=OEDT_KDS_M_KDS_ENTRY_FLAG_NONE;
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {      
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO.of active Entries(Before)%d",rKDSInfo.u32NumberOfActiveEntries);
        NumEntriesBefore = rKDSInfo.u32NumberOfActiveEntries; 
      }
      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData,Write_Buffer); 
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
      {
        u32Ret += 32;
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {       
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      }
      if(NumEntriesBefore !=  rKDSInfo.u32NumberOfActiveEntries)
      {
        u32Ret += 64;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 128;
    }
  } 
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32KDSWriteProtectedDel()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_030
* DESCRIPTION:  Try to delete the write protected entry (should fail).
* HISTORY:		Created by Rakesh Dhanya  (RBIN/ECM1) Jan 09 , 2007 
*
******************************************************************************/
tU32 u32KDSWriteProtectedDel(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tU32 u16EntryId = 0;
  tBool bAccessOption = 0;
  tChar Write_Buffer [] = "REWRITING_DATA\n";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {
      rEntryInfo.u16EntryLength = 16;
      rEntryInfo.u16Entry =0x013 ;
      rEntryInfo.u16EntryFlags=OEDT_KDS_M_KDS_ENTRY_FLAG_WRITE_PROTECTED;     
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {
        rEntryInfo.u16Entry = (tU16)(rKDSInfo.u32NumberOfEntries+1);       
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO.of active Entries(Before)%d",rKDSInfo.u32NumberOfActiveEntries);
      }
      u16EntryId =  rEntryInfo.u16Entry;

      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, Write_Buffer); 
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret = 30;
      }
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {       
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      }

      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,(intptr_t)u16EntryId) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
      rEntryInfo.u16EntryFlags=OEDT_KDS_M_KDS_ENTRY_FLAG_NONE;  
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }

  } 
  return u32Ret;
}
/************************************************************************************
 * FUNCTION:	u32KDSWriteEntryGreater()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_031
* DESCRIPTION:  Try to write data where Kds entry is greater than maximum length 
                possible(240 bytes)(should fail)
* HISTORY:		Created Kishore Kumar.R (RBIN/ECM1) DEC 13 , 2006 
*				Updated on jan 22, 2007.
*				Modified by Pradeep Chand C(RBIN/EDI3) Apr 24, 2007
                Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11, 2008

******************************************************************************/
tU32 u32KDSWriteEntryGreater(void)
{
#define MAX_LENGTH 300
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;
  tChar Software_Version [25] = "PLATFORM_XXXX";

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS,OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {
      rEntryInfo.u16EntryLength =MAX_LENGTH ;
      rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
      rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
      /* hbs2kor - Length of rEntryInfo.au8EntryData is fixed 240 bytes. 
            we can't fill this buffer with more than 240 bytes. This attempt gives exception*/

      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, Software_Version); 

      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,MAX_LENGTH)!= OSAL_ERROR)
      {
        u32Ret += 30;
      }
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {        
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32KDSReadcheck ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_032
* DESCRIPTION:  Get Next entry with ID of non Existing Entry in KDS (should fail).
*                (As per document (Diagnosis_KDS_Access_V18.pdf), if no next valid 
                 entry is found an entry ID of 0XFFFF must be returned with an error) 
* HISTORY:		Created by Rakesh Dhanya  (RBIN/ECM1) Jan 09 , 2007 
*				Updated By Rakesh Dhanya(RBIN/EDI3) on April 23, 2007.
          Updated By Tinoy Mathews(RBIN/EDI3) on May 7,2007
        Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11, 2008.
******************************************************************************/
tU32 u32KDSReadcheck(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;
  tS32 u16ReadEntryId = 0;
  tS32 u16GetNextEntryId = 0;
  
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret += 2;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 4;
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 8;      
    }
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret +=  16;
    }
    rEntryInfo.u16EntryLength = 24;
    rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret += 32;
    }
    /* Read the Entry Which is not present in the KDS */
    /* if the entry is not found an entry ID of 0XFFFF must be returned with an error*/
    if(OSAL_s32IORead(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
    {
      u32Ret += 64;     
    }

    u16ReadEntryId = rEntryInfo.u16Entry;
    if(u16ReadEntryId != 0xFFFF)
    {
      u32Ret += 128;      
    }
    /* Get Next entry to the Entry ID which is not present in KDS */
    /* if the entry is not found an entry ID of 0XFFFF must be returned with an error*/
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_GET_NEXT_ID,(intptr_t)&rEntryInfo) != OSAL_ERROR)
    {
      u32Ret += 256;
    }
    else
    {
      u16GetNextEntryId =  rEntryInfo.u16Entry;
      if(u16GetNextEntryId != 0xFFFF)
      {
        u32Ret += 512;
      }
    } 
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1024;
    }
  } 
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetRemainingSize ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_033
* DESCRIPTION:  Try to get the remaining size in KDS after storing entries.
* HISTORY:		Created by Rakesh Dhanya (RBIN/ECM1) on January 19,2007 
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11,2008
******************************************************************************/
tU32 u32KDSGetRemainingSize(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tsKDS_Info  rKDSInfo;
  volatile tU32 KDSSizeBeforeWrite=0;
  volatile tU32 KDSSizeAfterWrite=0;
  tEntry Entry1 = 0, Entry2 = 0, Entry3 = 0;
  tU32 Production_Date = 0x07D90501 ; // 1-05-2009
  tU8    Prod_Date_Entry_Length = 4;
  tU8 * dummy_prodate =0;
  tU8 u8date[10] ;
  tU8 Sw_version[25] = "PLATFORM_XXXX";
  tU8   Sw_version_Entry_Length = 24;
  tU8 Hw_version[15] = "MID_EU_DSB4" ;
  tU8 Hw_version_Entry_Length = 11;


  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 4;
    }
    /*Clear the KDS and Make sure there are no Entries */
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 8;      
    }
    /* After every Clear operation ensure that KDS initialization */
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret +=  16;
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret +=  32;
    }
    /* Check size before writing */
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_REMAIN,(intptr_t)&KDSSizeBeforeWrite) == OSAL_ERROR)
    {
      u32Ret +=  64;
    }
    /* Make entries into KDS  */
    Entry1 = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
    /* Write SW vesrion entry into KDS */
    if(WriteDataToKDS(&hDevice, Entry1, Sw_version_Entry_Length, Sw_version))
      u32Ret += 128;
    /* Write Production date into KDS */
    Entry2 = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_PROD_DATE);
    dummy_prodate = (tU8*)&Production_Date;
    u8date[0] = *dummy_prodate;
    u8date[1] = *(dummy_prodate+1);
    u8date[2] = *(dummy_prodate+2);
    u8date[3] = *(dummy_prodate+3);
    if(WriteDataToKDS(&hDevice, Entry2, Prod_Date_Entry_Length, u8date))
      u32Ret += 256;
    /* Write HW version into KDS*/
    Entry3 = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_HW_VERSION);
    if(WriteDataToKDS(&hDevice, Entry3, Hw_version_Entry_Length, Hw_version))
      u32Ret += 512;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret +=  1024;
    }
    /* Now Check the size which is( no of entries * sizeof(tsKDSEntry)) */   
    tS32 Vs32Ret=OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_REMAIN,(intptr_t)&KDSSizeAfterWrite);
    if(Vs32Ret == OSAL_ERROR)
    {
      u32Ret +=  2000;
    }
    else
    {   
      if(KDSSizeAfterWrite != (KDSSizeBeforeWrite - ((tU32)(3* sizeof(tsKDSEntry)))))
      {
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"The used size of KDS is not updated ");
        u32Ret +=  5000;  
      }
    }
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret += 10000;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSGetRemainingSizeInval ()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_034
* DESCRIPTION:  Try to get the remaining size in KDS with invalid parameter(should fail)
* HISTORY:	    Created by Rakesh Dhanya (RBIN/ECM1) on January 19,2007 
*
******************************************************************************/ 
tU32 u32KDSGetRemainingSizeInval(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_REMAIN,OSAL_NULL) != OSAL_ERROR)
    {
      u32Ret +=  32;
    }
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret += 512;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32KDSWriteWithoutWEnable()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_035
* DESCRIPTION:  Try to write to KDS without enabling the write flag (should fail)
* HISTORY:	    Created by Rakesh Dhanya (RBIN/EDI3) on Apr 24,2007 
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008.
******************************************************************************/ 
tU32 u32KDSWriteWithoutWEnable (void)
{
  OSAL_tIODescriptor hDevice = 0;
  tBool bAccessOption = 0;
  tU32 u32Ret = 0;
  tChar Software_Version [25] = "PLATFORM_XXXX";
  tsKDS_Info  rKDSInfo, rKDSInfo_2;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  memset(&rKDSInfo_2,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
    else
    {
      rEntryInfo.u16EntryLength = (tU16)(strlen(Software_Version));
      rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION); 
	  rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
      (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Software_Version ); 
      if(OSAL_s32IOWrite ( hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) != OSAL_ERROR)
      {
        u32Ret += 100;
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo_2) != OSAL_ERROR)
      {
        if(rKDSInfo.u32NumberOfActiveEntries != rKDSInfo_2.u32NumberOfActiveEntries)
        {
          u32Ret += 1000;
        }
      }
      else
      {
        u32Ret += 10000;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 4;
    }

  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSClearWithoutWEnable()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_036
* DESCRIPTION:  Try to Clear data without Write enabling.(should fail)
* HISTORY:	    Created by Pradeep Chand C (RBIN/EDI3) 24 Apr, 2007
******************************************************************************/
tU32 u32KDSClearWithoutWEnable(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tBool bAccessOption = 0;
  tU32 u32Ret = 0;
  tsKDS_Info  rKDSInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) != OSAL_ERROR)
      {
        u32Ret += 100;  

        if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
        {
          u32Ret +=  16;
        }
      }
      else
      {
        if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
        {
          if(rKDSInfo.u32NumberOfActiveEntries == 0)
          {
            u32Ret += 1000;
          }
        }
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 4;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSOverWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_037
* DESCRIPTION:  Try to write an entry already existing with the same ID
                some 'n' number of times
        As per the document : OSAL I/O Device KDS Function calls and Error 
        values, v0.6 Draft - 
        1. It is possible to write new data or overwrite already stored data.
        2. If you overwrites data, the new data is stored at the end of KDS 
        and afterwards the old one is 'deleted' (marked as invalid).
* HISTORY:	    Created by Pradeep Chand C(RBIN/EDI3) on April 24, 2007 
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11, 2008.
******************************************************************************/ 
tU32 u32KDSOverWrite(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0, u32Loop = 0;
  tBool bAccessOption = 0;
  tChar Software_Version [25] = "PLATFORM_XXXX";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret += 10 ;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret += 100;      
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret +=  1000;
      }
      /* Try to Overwrite the data of same ID for 1000 times */
      do
      {
        rEntryInfo.u16EntryLength = (tU16)(strlen(Software_Version));
        rEntryInfo.u16Entry = 1 ;
        rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
        (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Software_Version ); 
        if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
        {
          u32Ret += 10000;
          break;
        }
        u32Loop++;
      }while((u32Loop <= 1000)&& (u32Ret == 0));

      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) != OSAL_ERROR)
      {
        if(rKDSInfo.u32NumberOfActiveEntries != 1)
        {
          u32Ret +=100000;
        }
      }
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 100;      
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 3;
    }
  } 
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSWriteFull()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_038
* DESCRIPTION:  Try to write entries, some 'n' number 
        till the KDS memory is full.
        As per the document : OSAL I/O Device KDS Function calls and Error 
        values, v0.6 Draft - 
        1. It is possible to write new data or overwrite already stored data.
        2. If you overwrites data, the new data is stored at the end of KDS 
        and afterwards the old one is 'deleted' (marked as invalid).
        3. Once the KDS memory is full it will not be able to write and will
        return OSAL_C_S32_IOCTRL_KDS_FULL
* HISTORY:	    Created by Pradeep Chand C(RBIN/EDI3) on April 24, 2007 
*				Modified by Haribabu Sannapaneni (RBEI/ECM1) on March 11,2008
******************************************************************************/ 
tU32 u32KDSWriteFull(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0, u32Loop = 1;
  tBool bAccessOption = 0;
  tChar Write_Buffer [15] = "REWRITING_DATA";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2 ;
    }
    else
    {
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret += 3;      
      }

      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret +=  10;
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret +=  100;
      }

      /*  can be written maximum of 1000 entries to KDS */
      /* Each entry consumes 246 bytes = sizeof(tsKDSEntry) */
      do
      {
        rEntryInfo.u16EntryLength = (tU16)(strlen(Write_Buffer));
        rEntryInfo.u16Entry = (tU16)u32Loop;
		rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
        (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Write_Buffer ); 
        if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
        {
          if((OSAL_u32ErrorCode()!= (tU32)OSAL_C_S32_IOCTRL_KDS_FULL)||(OSAL_u32ErrorCode()== (tU32)OSAL_E_NOERROR))
          {
            u32Ret +=  1000;
          }
          if(((tS32)OSAL_u32ErrorCode()== OSAL_C_S32_IOCTRL_KDS_FULL))
            break;/* Exit from the loop if KDS is FULL */
        }
        u32Loop++;
      }while(((tS32)OSAL_u32ErrorCode()!= OSAL_C_S32_IOCTRL_KDS_FULL)&& (u32Ret == 0)&&(u32Loop <= 1010));
    }

    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret +=  2000;
    }

    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 5000;     
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10000;
    }
  } 
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32KDSWriteFullAndBackToFlash()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_038
* DESCRIPTION:  Try to write entries, some 'n' number 
                till the KDS memory is full.
                then save it into Flash
                the clear flash        
* HISTORY:	    Created by andrea B�ter 
******************************************************************************/
tU32 u32KDSWriteFullAndBackToFlash(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0, u32Loop = 1;
  tBool bAccessOption = 0;
  tChar Write_Buffer [15] = "REWRITING_DATA";
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;

  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2 ;
    }
    else
    {
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret += 3;      
      }
      if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INIT,ZERO_PARAMETER) == OSAL_ERROR)
      {
        u32Ret +=  10;
      }
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret +=  100;
      }
      /*  can be written maximum of 1000 entries to KDS */
      /* Each entry consumes 246 bytes = sizeof(tsKDSEntry) */
      do
      {
        rEntryInfo.u16EntryLength = (tU16)(strlen(Write_Buffer));
        rEntryInfo.u16Entry = (tU16)u32Loop;
		rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE ;
        (tVoid)OSAL_szStringCopy ( rEntryInfo.au8EntryData, Write_Buffer ); 
        if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
        {
          if((OSAL_u32ErrorCode()!= (tU32)OSAL_C_S32_IOCTRL_KDS_FULL)||(OSAL_u32ErrorCode()== (tU32)OSAL_E_NOERROR))
          {
            u32Ret +=  1000;
          }
          if(((tS32)OSAL_u32ErrorCode()== OSAL_C_S32_IOCTRL_KDS_FULL))
            break;/* Exit from the loop if KDS is FULL */
        }
        u32Loop++;
      }while(((tS32)OSAL_u32ErrorCode()!= OSAL_C_S32_IOCTRL_KDS_FULL)&& (u32Ret == 0)&&(u32Loop <= 1010));
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(intptr_t)&rKDSInfo) == OSAL_ERROR)
    {
      u32Ret +=  2000;
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_BACK,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 5000;     
    }
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CLEAR,ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret += 5000;     
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10000;
    }
  } 
  return u32Ret;
}
/********************************************************************
* FUNCTION:		u32KDSOpenMax()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_039
* DESCRIPTION:  Try to  Open the Kds device maximum number of times.( 253 times)
* HISTORY:	    Created by Kishore Kumar.R (RBIN/EDI3) 2 May, 2007
*********************************************************************/
tU32 u32KDSOpenMax(void)
{
  OSAL_tIODescriptor hDevice[255] = {0};
  tU32 u32Ret = 0;
  tU32 loopcount=0;

  for(loopcount=0;loopcount<=253;loopcount++)
  {
    hDevice[loopcount] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
    if(hDevice[loopcount] == OSAL_ERROR)
    {
      u32Ret = 1;
    }
  }
  hDevice[loopcount] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice[loopcount] != OSAL_ERROR)    //Opening  254 th time should fail
  {
    u32Ret = +10;
    if(OSAL_s32IOClose( hDevice[loopcount])== OSAL_ERROR)
    {
      u32Ret = +100;
    }
  }
  for(loopcount=0;loopcount<=253;loopcount++)
  {
    if(OSAL_s32IOClose( hDevice[loopcount])== OSAL_ERROR)
    {
      u32Ret =+ 1000;
    }
  }
  return u32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32KDSWriteBackToKDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_KDS_040
* DESCRIPTION:  Try to write all KDS entries from AVL tree to KDS_DATA.bin.
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on May 06,2008
******************************************************************************/
tU32 u32KDSWriteBackToFlash(void)
{
  OSAL_tIODescriptor hDevice = 0;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_BACK, ZERO_PARAMETER) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 5;
    }
  }
  return u32Ret;
}
