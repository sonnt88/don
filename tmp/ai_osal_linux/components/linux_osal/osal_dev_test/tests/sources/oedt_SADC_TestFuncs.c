/******************************************************************************
 * FILE         : oedt_SADC_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the individual test cases for the SADC
 *                device for GM-GE hardware.
 *
 * AUTHOR(s)    :  Haribabu Sannapaneni (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          |                             | Author & comments
 * --.--.--      | Initial revision            | ------------
 * 4, April,2008 | version 0.1                 | Haribabu Sannapaneni (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 *               |                             | Haribabu Sannapaneni (RBEI/ECM1)
 *               |                             | Added new test cases for Threshold
 * 2, July ,2008 | version 0.2                 | and EOC callbacks
 *-----------------------------------------------------------------------------
 *               |                             | Haribabu Sannapaneni (RBEI/ECM1)
 *               |                             | Test code modified(TU_OEDT_SADC_002,
 * 19,August,2008| version 0.3                 | TU_OEDT_ADC_005,TU_OEDT_ADC_006,
                                                 TU_OEDT_SADC_008,TU_OEDT_SADC_009,
*-----------------------------------------------------------------------------
 *               |                               | Haribabu Sannapaneni (RBEI/ECM1)
 *               |                            |Test case upadte based on test data
 *29,sept,2008   | version 0.4                | set update.
*-----------------------------------------------------------------------------
 *               |                               | Anoop Chandran(RBEI/ECF1)
 *               |                            |update the Test case  TU_OEDT_SADC_001
 *28,April,2009  | version 0.5                |
*-----------------------------------------------------------------------------
 *               | version 0.6                | Sainath Kalpuri (RBEI/ECF1)
 *               |                            | Tests related ADC4 have been commented
 * 20,April,2010 |                            | Condition check related return value
 *               |                            | of OSAL_s32IORead call has been modified
*******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_SADC_TestFuncs.h"
#include "osal_if.h"
#include <tk/tkernel.h>

static OSAL_tEventHandle evHandle = 0;
static OSAL_tEventHandle ThresholdevHandle1 = 0;
static OSAL_tEventHandle ThresholdevHandle2 = 0;
static OSAL_tEventHandle ThresholdevHandle3 = 0;

//#define HIGHSPEED_EOC_CALLBACK_EVENTPATTERN (Event1 | Event2)

#define LOWSPEED_EOC_CALLBACK_EVENTPATTERN Event3


enum ThresholdEvent
{
    CH4ThresholdEvent = (tU32) 0x01,
    CH5ThresholdEvent = (tU32)0x02,
    CH6ThresholdEvent = (tU32)0x04,
    CH7ThresholdEvent = (tU32)0x08

};

enum CallbackEvent
{
    Event1 =(tU32) 0x01,
    Event2 = (tU32)0x02,
    Event3 = (tU32)0x04

};
/* Global functions */


/*****************************************************************************
* FUNCTION:     OpenCloseSADCSubUnit()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:
* DESCRIPTION:  Opens and closes the specified SADC Sub unit.
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 OpenCloseSADCSubUnit(tS32 s32SubUnit)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;

    /* Channel Open */
    hFd = OSAL_IOOpen((tCString)s32SubUnit,enAccess);
    if(OSAL_ERROR != hFd)
    {
        /* Channel Close */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            u32Ret = 1;
        }
    }
    else
    {
        u32Ret = 2;
    }
    return u32Ret;

}

/*****************************************************************************
* FUNCTION:     u32SADCHighSpeedUnitOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_001
* DESCRIPTION:  1. Open SADC HighSpeed Unit
*               2. Close SADC HighSpeed Unit

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*              update by Anoop Chandran (RBEI/ECF1) on 28, April,2009
******************************************************************************/
tU32 u32SADCHighSpeedUnitOpenClose(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;


     /* Open and Close ADC High speed  unit 3 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_2);
    if(u32Status == 1)
    {
        u32Ret += 10;
    }
    else if(u32Status == 2)
    {
        u32Ret += 50;
    }

     /* Open and Close ADC High speed  unit 4 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_3);
    if(u32Status == 1)
    {
        u32Ret += 100;
    }
    else if(u32Status == 2)
    {
        u32Ret += 500;
    }

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCLowSpeedUnitOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_002
* DESCRIPTION:  1. Open SADC LowSpeed Unit
*               2. Close SADC LowSpeed Unit

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCLowSpeedUnitOpenClose(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;
#if 0  /* These channels are being used */
    /* Open and Close ADC Low speed  unit 1 */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_4);
    if (u32Status > 0)
    {
    u32Ret = 1;
    }



     /* Open and Close ADC Low speed  unit 2 */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_5);
    if (u32Status > 0)
    {
         u32Ret += 2;
    }


     /* Open and Close ADC Low speed  unit 3 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_6);
    if (u32Status > 0)
    {
     u32Ret += 5;
    }
#endif

     /* Open and Close ADC Low speed  unit 4 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_7);
    if (u32Status > 0)
    {
     u32Ret += 10;
    }

    /* Open and Close ADC Low speed  unit 5 */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_8);
    if (u32Status > 0)
    {
     u32Ret += 20;
    }

     /* Open and Close ADC Low speed  unit 6 */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_9);
    if (u32Status > 0)
    {
     u32Ret += 50;
    }

     /* Open and Close ADC Low speed  unit 7 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_10);
    if (u32Status > 0)
    {
     u32Ret += 100;
    }

     /* Open and Close ADC Low speed  unit 8 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_11);
    if (u32Status > 0)
    {
     u32Ret += 200;
    }

   return u32Ret;

}

/*****************************************************************************
* FUNCTION:     u32SADCChanReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_003
* DESCRIPTION:  1. Open SADC channel 2
*               2. Attempt to Re-open the SADC channel 2
*               3. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
******************************************************************************/
tU32 u32SADCChanReOpen(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tIODescriptor hFd1 = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;

    /* Open ADC channel 2 */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_2,enAccess);
    if(OSAL_ERROR != hFd)
    {
        /* Attempt to Re-open ADC channel 2 */
        hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_2,enAccess);
        if(OSAL_ERROR != hFd1)
        {
            /* If successful, indicate error */
            u32Ret = 1;
            /* Close the channel */
            if(OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
            {
                u32Ret += 2;
            }
        }

        /* Close the channel */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            u32Ret += 4;
        }
    }
    else
    {
        u32Ret = 10;
    }
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCChanCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_004
* DESCRIPTION:  1. Open SADC channel    2
*               2. Close SADC channel 2
*               3. Attempt to Close channel 2 again

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
******************************************************************************/
tU32 u32SADCChanCloseAlreadyClosed(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;

    /* Open ADC channel 2 */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_2,enAccess);
    if(OSAL_ERROR != hFd)
    {
        /* Close the channel */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            u32Ret = 1;
        }
        else
        {
            /* Attempt to Close the channel which is already closed */
            if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 2;
            }
        }
    }
    else
    {
        u32Ret = 4;
    }
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCGetVersion()
* PARAMETER:    Channel ID , No of Blocks
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_005
* DESCRIPTION:  1. Opens requested SADC Unit
*               2. Gets version
*               3. Closes ADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCGetVersion(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU32 u32Version = 0;
    /* Channel Open */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_7,
                      enAccess);
    if(OSAL_ERROR != hFd)
        {
            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_VERSION, (tS32)&u32Version ))
                {
                    u32Ret = 2;
                }
            
            if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
                {
                    u32Ret += 10;
                }
            
            
        }
    else
        {
            u32Ret = 1;
        }
    return  u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCGetAttributeData()
* PARAMETER:    Channel ID , No of Blocks
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_006
* DESCRIPTION:  1. Open requested SADC Unit
*               2. Gets Attribute data of SADC
                3. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCGetAttributeData(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_trAdcAttributeDataInfo Adc_Attributedata;
    tU32 u32Ret = 0;
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_7, enAccess);
    if(OSAL_ERROR != hFd)
        {
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_GET_ATTRIBUTE_DATA_INFO,(tS32)&Adc_Attributedata))
                               {
                                      u32Ret = 2;
                                     if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
                                        {
                                            u32Ret += 10;
                                        }


                                     return u32Ret;
                                }
           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 100;
            }

         }
         else
         {
          u32Ret = 1;
         }
 return u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32SADCHighSpeedRead()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested SADC Unit
*               2. Performs read operation
*               3. Close SADC channel

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCHighSpeedRead(tS32 s32SubUnit)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU16 * tU16ADCdata = NULL ;
    tPS8 *ps8ADCdata = NULL;
    tU32  NoOfBlocks  = 1;
    tU32  u32NoOfBlocks = 0;
    OSAL_trAdcAttributeDataInfo Adc_Attributedata;


        /* Channel Open */
        hFd = OSAL_IOOpen((tCString)s32SubUnit, enAccess);
        if(OSAL_ERROR != hFd)
        {
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,   OSAL_C_S32_IOCTRL_ADC_GET_ATTRIBUTE_DATA_INFO,(tS32)&Adc_Attributedata))
                               {
                                       u32Ret = 2;
                                       if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
                                        {
                                            u32Ret += 10;
                                        }

                                     return u32Ret;
                                }

            tU16ADCdata = (tU16 *)OSAL_pvMemoryAllocate((tU32)Adc_Attributedata
                                            .hs_blk_size* NoOfBlocks*2);
            if (tU16ADCdata != NULL)
            {
               ps8ADCdata = (tPS8 *)(tVoid *)tU16ADCdata;
               u32NoOfBlocks = (tU32)OSAL_s32IORead(hFd,(tPS8)ps8ADCdata, NoOfBlocks);
                if((OSAL_ERROR == (tS32)u32NoOfBlocks) || (NoOfBlocks  !=
                           u32NoOfBlocks))
                {
                    u32Ret += 100;
                }
                /* Get 12 bit ADC data */

                else
                {
                  tU16ADCdata = (tU16*)(tVoid*)ps8ADCdata;

                }
                        /* Channel Close */
                if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
                {
                    u32Ret += 1000;
                }

        }
        else
        {
         u32Ret += 10000;
        }



    }
    else
    {
        u32Ret = 1;
    }

    OSAL_vMemoryFree(tU16ADCdata);
      tU16ADCdata = NULL;
    ps8ADCdata = NULL;

    return u32Ret;


}

/*****************************************************************************
* FUNCTION:     u32SSADCHighSpeedUnitRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_007
* DESCRIPTION:  1. Opens SADC High Speed channels
*               2. Perform read operation
*               3. Close SADC High speed channels
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCHighSpeedUnitRead(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;



    u32Status = u32SADCHighSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_2);
    if( u32Status > 0)
     u32Ret += 1;


    u32Status = u32SADCHighSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_3);
    if( u32Status > 0)
     u32Ret += 10;

  return    u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32SADCLowSpeedRead()
* PARAMETER:    Channel ID , No of Blocks
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested SADC Unit
*               2. Perform read operation
*               3. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCLowSpeedRead(tS32 s32SubUnit )
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU16  tU16ADCdata = 0;
    tU32  NoOfBlocks  = 1, u32NoOfBlocks = 0;

        /* Channel Open */
        hFd = OSAL_IOOpen((tCString)s32SubUnit,
                            enAccess);
        if(OSAL_ERROR != hFd)
        {

              u32NoOfBlocks = (tU32)OSAL_s32IORead(hFd,(tPS8)&tU16ADCdata, NoOfBlocks) ;
            if((OSAL_ERROR == (tS32)u32NoOfBlocks) || (NoOfBlocks  !=
                       u32NoOfBlocks))
            {
                u32Ret = 2;
            }
            /* Get 12 bit ADC data */

            else
            {

               tU16ADCdata = tU16ADCdata & 0x0fff;
            }

                                /* Channel Close */
            if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 5;
            }

        }
        else
        {
         u32Ret = 1;
        }


return u32Ret;


}

/*****************************************************************************
* FUNCTION:     u32SADCLowSpeedUnitRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_008
* DESCRIPTION:  1. Open SADC Low speed channels
*               2. Perform read operation
*               3. Close SADC Low speed channels
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCLowSpeedUnitRead(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;

#if 0  /* This channel is being used by GYRO */
    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_4);
    if( u32Status > 0)
     u32Ret = 1;


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_5);
    if( u32Status > 0)
     u32Ret += 10;


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_6);
    if( u32Status > 0)
     u32Ret += 50;
#endif


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_7);
    if( u32Status > 0)
     u32Ret += 100;

    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_8);
    if( u32Status > 0)
     u32Ret += 200;


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_9);
    if( u32Status > 0)
     u32Ret += 500;


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_10);
    if( u32Status > 0)
     u32Ret += 1000;


    u32Status = u32SADCLowSpeedRead((tS32)OSAL_C_STRING_DEVICE_ADC_11);
    if( u32Status > 0)
     u32Ret += 2000;


return  u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCSetThreshold()
* PARAMETER:    Channel ID , No of Blocks
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_009
* DESCRIPTION:  1. Open requested SADC Unit
*               2. Sets threshold   (only channels CH4-CH5).
*               3. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCSetThreshold(tVoid)
{

    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU16 LowerComparedata = 10;
    tU16 UpperComparedata = 100;
    OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
    /* Set thershold for Channel 4 */
    /* Channel Open */

    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_7,
                      enAccess);
    if(OSAL_ERROR != hFd)
        {
            /* Set lower limit */
            padcsetthreshold.u16Threshold = LowerComparedata;
            padcsetthreshold.enComparison = enAdcThresholdLt;

            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
             u32Ret = 2;
            }

            /* Set Upper limit */
            padcsetthreshold.u16Threshold = UpperComparedata;
            padcsetthreshold.enComparison = enAdcThresholdGt;

            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
                u32Ret += 5;
            }

         if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 10;
            }
        }
        else
        {
        u32Ret = 1;
        }


  /* Set thershold for Channel 5 */
  /* Channel Open */
        hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_8,
                            enAccess);
        if(OSAL_ERROR != hFd)
        {
            /* Set lower limit */
            padcsetthreshold.u16Threshold = LowerComparedata;
            padcsetthreshold.enComparison = enAdcThresholdLt;

            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
             u32Ret += 20;
            }

            /* Set Upper limit */
            padcsetthreshold.u16Threshold = UpperComparedata;
            padcsetthreshold.enComparison = enAdcThresholdGt;

            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
             u32Ret += 50;
            }
         if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 100;
            }
        }
        else
        {
        u32Ret += 500;
        }

return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SADCSetRegisters()
* PARAMETER:    Channel ID , No of Blocks
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_010
* DESCRIPTION:  1. Open requested SADC Unit
*               2. Sets SADC Limit violation interrupt register, interrupt mask register
                   and Limit violation interrupt mask register.
*               3. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*
******************************************************************************/
tU32 u32SADCSetRegisters(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 limVioInt = 0x00000000;
//  tU32 IntMask  =  0x1fff1fff;
    tU32 limVioMsk = 0x1fff1fff; /* all off */
    tU32 u32Ret = 0;
        hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_5, enAccess);
        if(OSAL_ERROR != hFd)
        {

             /* Set Limit violation register */
              limVioInt = 0x00000011;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_REG, (tS32)&limVioInt ))
                               {
                                     u32Ret = 2;

                                }
#if 0
             /* Set Interrupt mask register */
              IntMask = 0x1fff1fff;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_STATES_INT_MSK, (tS32)&IntMask ))
                               {
                                     u32Ret += 10;

                                }

#endif
             /* Set Limit violation mask register */
              limVioInt = 0x1fff1fee;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_MSK, (tS32)&limVioMsk ))
                               {
                                     u32Ret += 100;

                                }


           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 1000;
            }

         }
         else
         {
          u32Ret = 1;
         }
 return u32Ret;

}

/*****************************************************************************
* FUNCTION:     u32SADCOpenCloseWithAlias()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_011
* DESCRIPTION:  1. Uses Alias to the channel
                2. Opens corresponding SADC channel
*               3. Closes corresponding SADC channel

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 23, May,2008
*
******************************************************************************/
tU32 u32SADCOpenCloseWithAlias(tVoid)
{
    tU32 u32Status = 0;
    tU32 u32Ret = 0;


     /* Open and Close Sensor ADC AMP_CLIP */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_AMP_CLIP);
    if (u32Status > 0)
    {
         u32Ret = 1;
    }


     /* Open and Close Sensor ADC DIAG_AUX_R */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_DIAG_AUX_R);
    if (u32Status > 0)
    {
     u32Ret += 2;
    }

     /* Open and Close Sensor ADC DIAG_AUX_L */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_DIAG_AUX_L);
    if (u32Status > 0)
    {
     u32Ret += 10;
    }

    /* Open and Close Sensor ADC Gps Antenna Sense */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_GPS_UANT);
    if (u32Status > 0)
    {
     u32Ret += 20;
    }

     /* Open and Close Sensor ADC GPS Temperature */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_GPS_UTMP);
    if (u32Status > 0)
    {
     u32Ret += 50;
    }

     /* Open and Close Sensor ADC GPS Voltage Sense */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_GPS_VOLTAGE_SENSE);
    if (u32Status > 0)
    {
     u32Ret += 100;
    }

     /* Open and Close Sensor ADC TEMP_SENSOR_1 */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_TEMP_SENSOR_1);
    if (u32Status > 0)
    {
     u32Ret += 200;
    }

    /* Open and Close Sensor ADC TEMP_SENSOR_2 */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_TEMP_SENSOR_2);
    if (u32Status > 0)
    {
     u32Ret += 500;
    }

     /* Open and Close Sensor DIAG_PHANTOM_ANT */
    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_DIAG_PHANTOM_ANT);
    if (u32Status > 0)
    {
     u32Ret += 1000;
    }

     /* Open and Close Sensor ADC PWM_AMP_DIAG */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_PWM_AMP_DIAG);
    if (u32Status > 0)
    {
     u32Ret += 2000;
    }

     /* Open and Close Sensor ADC PWM_DISPLAY_DIAG */

    u32Status = OpenCloseSADCSubUnit((tS32)OSAL_C_STRING_DEVICE_ADC_PWM_DISPLAY_DIAG);
    if (u32Status > 0)
    {
     u32Ret += 5000;
    }

   return u32Ret;

}


/* Threshold Callback function for CH4  */
static tVoid SADCCH4ThresholdCallback(tVoid)
{
    OSAL_s32EventPost(ThresholdevHandle1, (OSAL_tEventMask)CH4ThresholdEvent, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
* FUNCTION:     u32SADCSetThresholdCallbackForCH4(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_012
* DESCRIPTION:  1. Event creation
*               2. Open requested SADC Unit
*               3. Sets threshold and Callback for channel CH4
*               4. Wait for the event posted by Threshold callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*
******************************************************************************/
tU32 u32SADCSetThresholdCallbackForCH4(tVoid)
{

    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU16 LowerComparedata = 10;
    tU16 UpperComparedata = 100;
    OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
    OSAL_trAdcSetCallback padcsetcallback;
    OSAL_tEventMask gevMask1 = 0;
        /* Create Event for Thread 1*/
        if( OSAL_ERROR == OSAL_s32EventCreate("SADC_Threshold_CH4_CALLBACK",&ThresholdevHandle1 ) )
        {
            u32Ret = 1;
            return u32Ret;
        }

       /* Set thershold for Channel   */
        /* Channel Open */
        hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_4,
                            enAccess);
        if(OSAL_ERROR != hFd)
        {

           /* Set threshold Callback */
              padcsetcallback.enCallbackType = enAdcCallbackThreshold;
              padcsetcallback.pfnCallbackFunction = SADCCH4ThresholdCallback;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
                               {
                                      u32Ret = 3;

                                }
            if(u32Ret == 0)
            {
            /* Set lower limit */
            padcsetthreshold.u16Threshold = LowerComparedata;
            padcsetthreshold.enComparison = enAdcThresholdLt;
                padcsetthreshold.bEnable = TRUE;
            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
                 u32Ret = 5;
            }

            /* Set Upper limit */
            padcsetthreshold.u16Threshold = UpperComparedata;
            padcsetthreshold.enComparison = enAdcThresholdGt;
                padcsetthreshold.bEnable = TRUE;
            if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
            {
                                      u32Ret += 10;
                                }


               else
               {

                    if(OSAL_s32EventWait( ThresholdevHandle1,(tU32) CH4ThresholdEvent,
                                       OSAL_EN_EVENTMASK_OR,
                                       10000,
                                       &gevMask1 )== OSAL_ERROR)
                                       {
                                        u32Ret += 20;
                                       }
               }
                  /* Unregister the call back function */
              padcsetcallback.enCallbackType = enAdcCallbackThreshold;
              padcsetcallback.pfnCallbackFunction = 0;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
                               {
                                      u32Ret += 50;

                                }


             }
            /* Close Event */
            if( OSAL_ERROR == OSAL_s32EventClose( ThresholdevHandle1 ) )
            {
                u32Ret += 100;
            }
            /*Delete the Event */
            if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_Threshold_CH4_CALLBACK" ) )
            {
                u32Ret += 1000;
            }



         if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
            {
                u32Ret += 10000;
            }
        }
        else
        {
        u32Ret = 2;
        }


return u32Ret;
}

/* Threshold Callback function for CH5  */
static tVoid SADCCH5ThresholdCallback(tVoid)
{
    OSAL_s32EventPost(ThresholdevHandle2, (OSAL_tEventMask)CH5ThresholdEvent, OSAL_EN_EVENTMASK_OR);
}


/* Threshold Callback function for CH7  */
static tVoid SADCCH7ThresholdCallback(tVoid)
{
    OSAL_s32EventPost(ThresholdevHandle2, (OSAL_tEventMask)CH7ThresholdEvent, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
* FUNCTION:     u32SADCSetThresholdCallbackForCH5(tVoid)
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_013
* DESCRIPTION:  1. Event creation
*               2. Open requested SADC Unit
*               3. Sets threshold and Callback for channel CH5
*               4. Wait for the event posted by Threshold callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*
******************************************************************************/
tU32 u32SADCSetThresholdCallbackForCH5(tVoid)
{

   OSAL_tIODescriptor hFd = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tU16 LowerComparedata = 10;
   tU16 UpperComparedata = 100;
   OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
   OSAL_trAdcSetCallback padcsetcallback;
   OSAL_tEventMask gevMask1 = 0;
   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("SADC_Threshold_CH5_CALLBACK",&ThresholdevHandle2 ) )
   {
      u32Ret = 1;
      return u32Ret;
   }

   /* Set thershold for Channel   */
   /* Channel Open */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_5,
   enAccess);
   if(OSAL_ERROR != hFd)
   {

      /* Set Threshold Callback */
      padcsetcallback.enCallbackType = enAdcCallbackThreshold;
      padcsetcallback.pfnCallbackFunction = SADCCH5ThresholdCallback;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 3;

      }

      if(u32Ret == 0)
      {
         /* Set lower limit */
         padcsetthreshold.u16Threshold = LowerComparedata;
         padcsetthreshold.enComparison = enAdcThresholdLt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret = 5;
         }

         /* Set Upper limit */
         padcsetthreshold.u16Threshold = UpperComparedata;
         padcsetthreshold.enComparison = enAdcThresholdGt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret += 10;
         }
         else
         {
            if(OSAL_s32EventWait( ThresholdevHandle2,(tU32) CH5ThresholdEvent,
                     OSAL_EN_EVENTMASK_OR,
                     10000,
                     &gevMask1 )== OSAL_ERROR)
            {
               u32Ret += 20;
            }
         }
         /* Unregister the callback*/
         padcsetcallback.enCallbackType = enAdcCallbackThreshold;
         padcsetcallback.pfnCallbackFunction = 0;
         if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
         {
            u32Ret += 50;

         }

      }
      /* Close Event */
      if( OSAL_ERROR == OSAL_s32EventClose( ThresholdevHandle2 ) )
      {
         u32Ret += 100;
      }
      /*Delete the Event */
      if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_Threshold_CH5_CALLBACK" ) )
      {
         u32Ret += 1000;
      }



      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 10000;
      }
   }
   else
   {
      u32Ret = 2;
   }


   return u32Ret;

}



/*****************************************************************************
* FUNCTION:     u32SADCSetThresholdCallbackForCH7(tVoid)
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_013
* DESCRIPTION:  1. Event creation
*               2. Open requested SADC Unit
*               3. Sets threshold and Callback for channel CH7
*               4. Wait for the event posted by Threshold callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*
******************************************************************************/
tU32 u32SADCSetThresholdCallbackForCH7(tVoid)
{

   OSAL_tIODescriptor hFd = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tU16 LowerComparedata = 10;
   tU16 UpperComparedata = 100;
   OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
   OSAL_trAdcSetCallback padcsetcallback;
   OSAL_tEventMask gevMask1 = 0;
   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("SADC_Threshold_CH7_CALLBACK",&ThresholdevHandle2 ) )
   {
      u32Ret = 1;
      return u32Ret;
   }

   /* Set thershold for Channel   */
   /* Channel Open */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_7,
   enAccess);
   if(OSAL_ERROR != hFd)
   {

      /* Set Threshold Callback */
      padcsetcallback.enCallbackType = enAdcCallbackThreshold;
      padcsetcallback.pfnCallbackFunction = SADCCH7ThresholdCallback;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 3;

      }

      if(u32Ret == 0)
      {
         /* Set lower limit */
         padcsetthreshold.u16Threshold = LowerComparedata;
         padcsetthreshold.enComparison = enAdcThresholdLt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret = 5;
         }

         /* Set Upper limit */
         padcsetthreshold.u16Threshold = UpperComparedata;
         padcsetthreshold.enComparison = enAdcThresholdGt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret += 10;
         }
         else
         {
            if(OSAL_s32EventWait( ThresholdevHandle2,(tU32) CH7ThresholdEvent,
                     OSAL_EN_EVENTMASK_OR,
                     10000,
                     &gevMask1 )== OSAL_ERROR)
            {
               u32Ret += 20;
            }
         }
         /* Unregister the callback*/
         padcsetcallback.enCallbackType = enAdcCallbackThreshold;
         padcsetcallback.pfnCallbackFunction = 0;
         if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
         {
            u32Ret += 50;

         }

      }
      /* Close Event */
      if( OSAL_ERROR == OSAL_s32EventClose( ThresholdevHandle2 ) )
      {
         u32Ret += 100;
      }
      /*Delete the Event */
      if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_Threshold_CH7_CALLBACK" ) )
      {
         u32Ret += 1000;
      }



      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 10000;
      }
   }
   else
   {
      u32Ret = 2;
   }


   return u32Ret;

}


static tVoid SingleThresholdCallback(tVoid)
{
    OSAL_s32EventPost(ThresholdevHandle3, (OSAL_tEventMask)CH6ThresholdEvent, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
* FUNCTION:     u32SADCSetSingleThresholdCallbackCH5(tVoid)
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_SADC_016
* DESCRIPTION:  1. Event creation
*               2. Open requested SADC Unit
*               3. Sets threshold value and Single threshold
                  Callback for channel CH7
*               4. Wait for the event posted by Threshold callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*               Modified to channel 7.
******************************************************************************/
tU32 u32SADCSetSingleThresholdCallbackCH5(tVoid)
{

   OSAL_tIODescriptor hFd = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tU16 LowerComparedata = 5;
   tU16 UpperComparedata = 10;
   OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
   OSAL_trAdcSetCallback padcsetcallback;
   OSAL_tEventMask gevMask1 = 0;
   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("SADC_Threshold_CH6_CALLBACK",&ThresholdevHandle3 ) )
   {
      u32Ret = 1;
      return u32Ret;
   }

   /* Set thershold for Channel   */
   /* Channel Open */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_5,
   enAccess);
   if(OSAL_ERROR != hFd)
   {

      /* Set Single Threshold Callback */
      padcsetcallback.enCallbackType = enAdcCallbackSingleInterruptThreshold;
      padcsetcallback.pfnCallbackFunction = SingleThresholdCallback;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 3;

      }

      if(u32Ret == 0)
      {
         /* Set lower limit */
         padcsetthreshold.u16Threshold = LowerComparedata;
         padcsetthreshold.enComparison = enAdcThresholdLt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret = 5;
         }

         /* Set Upper limit */
         padcsetthreshold.u16Threshold = UpperComparedata;
         padcsetthreshold.enComparison = enAdcThresholdGt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret = 10;
         }
         else
         {

            if(OSAL_s32EventWait( ThresholdevHandle3,(tU32) CH6ThresholdEvent,
                     OSAL_EN_EVENTMASK_OR,
                     10000,
                     &gevMask1 )== OSAL_ERROR)
            {
               u32Ret += 20;
            }
         }

         /* Unregister the callback*/
         padcsetcallback.enCallbackType = enAdcCallbackSingleInterruptThreshold;
         padcsetcallback.pfnCallbackFunction = 0;
         if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
         {
            u32Ret += 50;

         }

      }

      /* Close Event */
      if( OSAL_ERROR == OSAL_s32EventClose( ThresholdevHandle3 ) )
      {
         u32Ret += 100;
      }
      /*Delete the Event */
      if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_Threshold_CH6_CALLBACK" ) )
      {
         u32Ret += 1000;
      }



      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 10000;
      }
   }
   else
   {
      u32Ret = 2;
   }


   return u32Ret;

}


static tVoid HighSpeedChannelcallback1(tVoid)
{
    OSAL_s32EventPost(evHandle, (OSAL_tEventMask)Event1, OSAL_EN_EVENTMASK_OR);
}

static tVoid HighSpeedChannelcallback2(tVoid)
{
    OSAL_s32EventPost(evHandle, (OSAL_tEventMask)Event2, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
* FUNCTION:     u32SADCHighSpeedEOCCallBack()
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_017
* DESCRIPTION:  1. Event creation
*               2. Opens requested SADC Unit
*               3. Sets EOC Callback for channel CH2 and CH3
*               4. Wait for the event posted by EOC callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*
******************************************************************************/
tU32 u32SADCHighSpeedEOCCallBack(tVoid)
{
   OSAL_tIODescriptor hFd1 = 0, hFd2 = 0 ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trAdcSetCallback padcsetcallback;
   //  OSAL_tEventMask gevMask1 = 0;
#if 0 /* commented because -EOC Callback registration for High speed channels
      is not supported. */
   /* See the below comment present in the dev_sadc.c */
   /* Ch0 - Ch3 are high speed channels and have DMA trasfer. Hence EOC(Interrupt)
   callback is not registered. It will increase burden on the system.  */

   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("SADC_HighSpeed_EOC_CALLBACK",&evHandle ) )
   {
      u32Ret = 1;
      return u32Ret;
   }

#endif
   /* Open SADC High Speed Channel */
   tBool IsOpenedADC2 = FALSE;
   hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_2, enAccess);
   if(OSAL_ERROR != hFd1)
   {
      IsOpenedADC2 = TRUE;
      padcsetcallback.enCallbackType = enAdcCallbackEndOfConversion;
      padcsetcallback.pfnCallbackFunction = HighSpeedChannelcallback1;
      if(OSAL_ERROR != OSAL_s32IOControl(hFd1,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 2;

      }


   }
   else
   {
      u32Ret = 3;
   }
   /* Open SADC High Speed Channel */
   tBool IsOpenedADC3 = FALSE;
   hFd2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_3, enAccess);
   if(OSAL_ERROR != hFd2)
   {
      IsOpenedADC3 = TRUE;
      padcsetcallback.enCallbackType = enAdcCallbackEndOfConversion;
      padcsetcallback.pfnCallbackFunction = HighSpeedChannelcallback2;
      if(OSAL_ERROR != OSAL_s32IOControl(hFd2,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret += 10;

      }


   }
   else
   {
      u32Ret += 5;

   }

#if 0
   if(OSAL_s32EventWait( evHandle, HIGHSPEED_EOC_CALLBACK_EVENTPATTERN,
            OSAL_EN_EVENTMASK_AND,
            10000,
            &gevMask1 )== OSAL_ERROR)
   {
      u32Ret += 100;
   }
#endif
   if(IsOpenedADC2 == TRUE)
   {
      if(OSAL_ERROR  == OSAL_s32IOClose (hFd1))
      {
         u32Ret += 1000;
      }

   }

   if(IsOpenedADC3 == TRUE)
   {
      if(OSAL_ERROR  == OSAL_s32IOClose (hFd2))
      {
         u32Ret += 10000;
      }
   }

#if 0
   /* Close Event */
   if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
   {
      u32Ret += 100000;
   }
   /*Delete the Event */
   if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_HighSpeed_EOC_CALLBACK" ) )
   {
      u32Ret += 1000000;
   }
#endif
   return u32Ret;
}


/* Low Speed Channels EOC callback functions */

static tVoid LowSpeedChannelcallback1(tVoid)
{
    OSAL_s32EventPost(evHandle, (OSAL_tEventMask)Event3, OSAL_EN_EVENTMASK_OR);
}


/*****************************************************************************
* FUNCTION:     u32SADCLowSpeedEOCCallBack()
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_018
* DESCRIPTION:  1. Event creation
*               2. Opens requested SADC Unit
*               3. Sets EOC Callback for channel CH5 and CH6
*               4. Wait for the event posted by EOC callback function.
*               5. Close SADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*
******************************************************************************/
tU32 u32SADCLowSpeedEOCCallBack(tVoid)
{
   OSAL_tIODescriptor hFd1 = 0 ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trAdcSetCallback padcsetcallback;
   OSAL_tEventMask gevMask1 = 0;
   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("SADC_LowSpeed_EOC_CALLBACK",&evHandle ) )
   {
      u32Ret = 1;
      return u32Ret;
   }


   /* Open SADC High Speed Channel */

   hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADC_5, enAccess);
   if(OSAL_ERROR != hFd1)
   {

      padcsetcallback.enCallbackType = enAdcCallbackEndOfConversion;
      padcsetcallback.pfnCallbackFunction = LowSpeedChannelcallback1;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd1,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 2;

      }
      if(OSAL_s32EventWait( evHandle, (OSAL_tEventMask)LOWSPEED_EOC_CALLBACK_EVENTPATTERN,
               OSAL_EN_EVENTMASK_OR,
               10000,
               &gevMask1 )== OSAL_ERROR)
      {
         u32Ret += 10;

      }
      /* Unregister the callback function */
      padcsetcallback.enCallbackType = enAdcCallbackEndOfConversion;
      padcsetcallback.pfnCallbackFunction = 0;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd1,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret += 100;

      }

      if(OSAL_ERROR  == OSAL_s32IOClose (hFd1))
      {
         u32Ret += 1000;
      }


   }
   else
   {
      u32Ret = 3;
   }

   /* Close Event */
   if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
   {
      u32Ret += 100000;
   }
   /*Delete the Event */
   if( OSAL_ERROR == OSAL_s32EventDelete( "SADC_LowSpeed_EOC_CALLBACK" ) )
   {
      u32Ret += 1000000;
   }
   evHandle = 0;
   return u32Ret;
}

/* end of file */
