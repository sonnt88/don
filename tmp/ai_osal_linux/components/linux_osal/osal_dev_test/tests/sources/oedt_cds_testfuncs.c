/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_memory_testfuncs.c
 *
 * CREATED:     2005-03-08
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *
 * DESCRIPTION:
 *      OEDT tests for the memory  management system
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 * HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
                Version - 1.1
 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#include "OsalConf.h"

//#include "osal.h" tnn was here
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include <extension/memory.h>
#include <extension/proctask.h>



#define CDS_MAX_ENTRY_SZ_RESULT  ((UB*) "The entry is 16 chars long :-)")
#define CDS_TEST_STRING_RESULT   ((UB*) "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
#define CDS_TEST_NR_DEC_RESULT   65000
#define CDS_TEST_NR_HEX_RESULT   0xFFFF

#define CDS_MAX_ENTRY_SZ         ((UB*) "CDS_MAX_ENTRY_SZ")
#define CDS_TEST_STRING          ((UB*) "CDS_TEST_STRING")
#define CDS_TEST_NR_DEC          ((UB*) "CDS_TEST_NR_DEC")
#define CDS_TEST_NR_HEX          ((UB*) "CDS_TEST_NR_HEX")
#define CDS_TEST_ARRAY_DEC       ((UB*) "CDS_TEST_ARRAY_DEC")
#define CDS_TEST_ARRAY_HEX       ((UB*) "CDS_TEST_ARRAY_HEX")
#define CDS_TEST_NO_EXIST        ((UB*) "CDS_TEST_NO_EXIST")

//#define CDS_TEST_ARRAY_DEC   65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  65000  
//#define CDS_TEST_ARRAY_HEX   0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 0xFFFF 
//CDS_TEST_ARRAY_STR    Hallo1 Hallo2 Hallo3 Hallo4 Hallo5 Hallo6 Hallo7 Hallo8 Hallo9 Hallo10 Hallo11 Hallo12 Hallo13 Hallo14 HalloLast


tU32 u32CdsTest1(void){
    tU32 u32_ret = 0;
    int n_res_array[16] = {0};
    UB  ch_res_array[256] = "";
    int i=0;
    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_ARRAY_DEC' could be read correctly */
    memset(n_res_array, 0, sizeof(n_res_array));
    if(tkse_get_cfn (CDS_TEST_ARRAY_DEC, n_res_array, 16) != 16){
        return 1;
    }
    for(i=0;i<16;i++){
        if(n_res_array[i] != 65000){
            return 2;
        }
    }

    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_ARRAY_HEX' could be read correctly */
    memset(n_res_array, 0, sizeof(n_res_array));
    if(tkse_get_cfn (CDS_TEST_ARRAY_HEX, n_res_array, 16) != 16){
        return 4;
    }

    for(i=0;i<16;i++){
        if(n_res_array[i] != 0xFFFF){
            return 5;
        }
    }

    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_ARRAY_DEC' could be read correctly */
    memset(n_res_array, 0, sizeof(n_res_array));
    if(tkse_get_cfn (CDS_TEST_NR_DEC, n_res_array, 16) != 1){
        return 7;
    }
    for(i=0;i<1;i++){
        if(n_res_array[i] != CDS_TEST_NR_DEC_RESULT){
            return 8;
        }
    }


    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_NR_HEX' could be read correctly */
    memset(n_res_array, 0, sizeof(n_res_array));
    if(tkse_get_cfn (CDS_TEST_NR_HEX, n_res_array, 16) != 1){
        return 10;
    }

    for(i=0;i<1;i++){
        if(n_res_array[i] != CDS_TEST_NR_HEX_RESULT){
            return 11;
        }
    }



    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_STRING_RESULT' could be read correctly */
    memset(ch_res_array, 0, sizeof(ch_res_array));
    if(tkse_get_cfs (CDS_TEST_STRING, ch_res_array, sizeof(ch_res_array)) != strlen(CDS_TEST_STRING_RESULT)){
        return 13;
    }
       
    if(strcmp(CDS_TEST_STRING_RESULT, ch_res_array) != 0){
        return 14;
    }

    /**************************************************************************/
    /* Check if the values from 'CDS_TEST_SZ_RESULT' could be read correctly */
    memset(ch_res_array, 0, sizeof(ch_res_array));
    if(tkse_get_cfs (CDS_MAX_ENTRY_SZ, ch_res_array, sizeof(ch_res_array)) != strlen(CDS_MAX_ENTRY_SZ_RESULT)){
        return 15;
    }
    
    if(strcmp(CDS_MAX_ENTRY_SZ_RESULT, ch_res_array) != 0){
        return 16;
    }
    
    
    if(tkse_get_cfs (CDS_TEST_NO_EXIST, ch_res_array, sizeof(ch_res_array)) != E_NOEXS){
        return 17;
    }

    if(tkse_get_cfn (CDS_TEST_NO_EXIST, n_res_array, 16) != E_NOEXS){
        return 17;
    }
    
    return u32_ret;

}
