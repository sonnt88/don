/******************************************************************************
 *FILE         : oedt_Illumination_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for the ILLUMINATION
 *               device	for GM-GE hardware.
 *              
 *AUTHOR(s)    :  Anoop Chandran (RBIN/EDI3)
 *HISTORY      : 
 					v 1.3, 04-02-2009 
					Added new test case
					TU_OEDT_ILLUMINATION_019
					April 17, 2008 update the test case
					TU_OEDT_ILLUMINATION_012,013
					Added new test case
					TU_OEDT_ILLUMINATION_015-018

 					03-oct-2007 Initial  Version v0.1 - Anoop Chandran
 *******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Illumination_TestFuncs.h"

/*****************************************************************************
* FUNCTION:		u32IlluminationDevOpenClose()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_001
* DESCRIPTION:  Open and Close the ILLUMINATION device
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 03-oct-2007
 ******************************************************************************/
tU32 u32IlluminationDevOpenClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
    else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32IlluminationDevOpenCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_002
* DESCRIPTION:  Attempt to Open and Close the ILLUMINATION device with invalid
*				parameter
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationDevOpenCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OSAL_C_STRING_ILLU_INVALID_ACCESS_MODE;

 	/* Open the device in writeonly mode with invalid device name */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR != hDevice )
    {
		u32Ret = 1;
    	/* If successfully opened, indicate error and close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
	}// end of if ( OSAL_ERROR != hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationDevMultipleOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_003
* DESCRIPTION:  Attempt to Close the ILLUMINATION device which has already 
*				been closed
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationDevMultipleOpen(tVoid)	
{
	OSAL_tIODescriptor hDeviceOne = 0, hDeviceTwo = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

 	/* Open the first device in writeonly mode */
 	hDeviceOne = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDeviceOne )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}// end of switch(s32Status)
    }
    else
	{
		/* Open the device with out colsing 
			the first device in writeonly mode */
	 	hDeviceTwo = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
	    if ( OSAL_ERROR != hDeviceTwo )
	    {
			/* check the error status returned */
			s32Status = (tS32) OSAL_u32ErrorCode();
			switch(s32Status)
			{
				case OSAL_E_ALREADYEXISTS:
					u32Ret += 10;
					break;
				case OSAL_E_UNKNOWN:
					u32Ret += 20;
					break;
				default:
					u32Ret += 30;
			}
	   		/* Close the second device */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )
			{
				u32Ret = 100;
			}// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceTwo ) )
	   	   
		 }//end of if ( OSAL_ERROR == hDeviceTwo )

		/* Close the first device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )
		{
			u32Ret += 500;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDeviceOne ) )

	}// end of if ( OSAL_ERROR == hDeviceOne )
   	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32IlluminationDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_004
* DESCRIPTION:  Attempt to Close the ILLUMINATION device which has already 
*				been closed
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationDevCloseAlreadyClosed(tVoid)	
{
	OSAL_tIODescriptor hDevice;
	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

 	/* Open the first device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
    else
	{
	   	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 5;
		}
		else
		{
			/* Try to close the device which allready Close  */
			if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret = 10;
			}
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
   	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationDevCloseInvalHandle()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_005
* DESCRIPTION:  Attempt to Open the ILLUMINATION device with null parameter
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationDevCloseInvalHandle(tVoid)  
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

 	/* Open the device in writeonly access mode */
 	hDevice = OSAL_IOOpen( ILLUMINATION_INVAL_PARAM , enAccess );
    if ( OSAL_ERROR != hDevice  )
    {
    	u32Ret = 1; 
    	/* If successfully opened, indicate an error and close the device */  
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}// end of if ( OSAL_ERROR != hDevice  )

	hDevice = ILLUMINATION_INVAL_PARAM ;
	if( OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
	{
			u32Ret += 10;
	}//endo of if( OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationGetVersion() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_006
* DESCRIPTION:  Get device version
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007 
******************************************************************************/
tU32 u32IlluminationGetVersion(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
		/* Get device version */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{   
			u32Ret = 5;
		}//end of if ( OSAL_ERROR == OSAL_s32IOControl())
				/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
	}//end of if ( OSAL_ERROR == hDevice )
 
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationGetVersionInvalParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_007
* DESCRIPTION:  Attempt to get device version with invalid parameters
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007 
******************************************************************************/
tU32 u32IlluminationGetVersionInvalParam(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
  	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
		/* Get device version */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,OSAL_NULL) )
		{   
			u32Ret = 5;
		}
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationrSetPWMInRange()    
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_008
* DESCRIPTION:  Set PWM duty cycle with h minimum, medium, max permissible
* 				value of duty cycle value
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007 
******************************************************************************/
tU32 u32IlluminationrSetPWMInRange( tVoid ) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION, enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch( s32Status )
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
	else
	{
		/* Set PWM duty cycle with value 0 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MINVAL ) )
		{
			u32Ret = 5;
		}

		/* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MIDVAL ) )
		{
			u32Ret += 10;
		}

		/* Set PWM duty cycle with value 65535 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MAXVAL ) )
		{
			u32Ret += 20;
		}

		/* Close the device */
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 50;
		}//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
 
/*****************************************************************************
* FUNCTION:		u32lluminationrSetPWMOutRange()    
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_009
* DESCRIPTION:  Set PWM duty cycle with invalid value
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007 
******************************************************************************/
tU32 u32lluminationrSetPWMOutRange(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
   	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else
	{
		/* Set PWM duty cycle with value -1*/	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, PWM_DUTYCYCLE_INVAL_MIN ) )
		{
			u32Ret = 5;
		}
		/* Set PWM duty cycle with value 65536 */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, PWM_DUTYCYCLE_INVAL_MAX ) )
		{
			u32Ret += 20;
		}
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 50;
		}// end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationGetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_010
* DESCRIPTION:  Get PWM duty cycle 
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in readonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		} //end of switch(s32Status)

    }
	else
	{
	
		/* Get PWM duty cycle value */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&u32DutyCycle ) )
		{
			u32Ret = 5;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )


	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32IlluminationGetPWMInvalParam()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_011
* DESCRIPTION:  Get PWM duty cycle with invalid parameters 
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32IlluminationGetPWMInvalParam(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else
	{
		/* Get PWM duty cycle value */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_GET, OSAL_NULL ) )
		{
			u32Ret = 5;
		}

		/* Close the device */
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32lluminationSetGetPWM()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_012
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
* 				is correctly set
* HISTORY:
			
				Updated by Anoop Chandran (RBEI/ECM1)April 17, 2008
				Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32lluminationSetGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{

	    /* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MIDVAL ))
		{
			u32Ret += 50;
		}
		u32DutyCycle = 0;
		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&u32DutyCycle ) )
		{
			u32Ret += 500;
		}

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}

	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32lluminationInvalidSetGetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_013
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
* 				is correctly set
* HISTORY:	
				
				Updated by Anoop Chandran (RBEI/ECM1)April 17, 2008	
				Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32lluminationInvalidSetGetPWM(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
	else
	{
	    /* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MIDVAL ))
		{
			u32Ret = 5;
		}
		/* Set PWM duty cycle with value -1 */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_SET, PWM_DUTYCYCLE_INVAL_MIN ))
		{
			u32Ret += 50;
		}// end of if ( OSAL_ERROR != OSAL_s32IOControl())

		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&u32DutyCycle ) )
		{
			u32Ret += 500;
		}

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}//end of if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )

		 
	}//end of if ( OSAL_ERROR == hDevice )


	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32lluminationInvalFuncParamToIOCtrl()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_014
* DESCRIPTION:  Attempt to perform IOCtrl opertion with invalid parameter
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32lluminationInvalFuncParamToIOCtrl(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
   /* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
    }
	else
	{
		/* IOCtrl operation with invalid parameter */	
		if (OSAL_ERROR != OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_INVAL_ILLU_FUNC,(tS32)&u32DutyCycle))
		{
			u32Ret = 5;
		}
	   		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32IlluminationrMultSetPWM()  
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_015
* DESCRIPTION:  Multiple setting of the PWM for illumination 
* HISTORY:	
				Created by Anoop Chandran (RBEI/ECM1)April 17, 2008
******************************************************************************/
tU32 u32IlluminationrMultSetPWM(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tS32 s32Count = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	
   /* Open the device in writeonly mode */
 	hDevice 
 	=
 	OSAL_IOOpen
 	(
 		OSAL_C_STRING_DEVICE_ILLUMINATION ,
 		enAccess 
 	);
	if ( OSAL_ERROR == hDevice )
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)
	}
	else
	{
		/* Multiple setting of the PWM for illumination with delay */	
		for (; s32Count <=100; ++s32Count)
		{
			s32Status 
			= 
			OSAL_s32IOControl
			(
				hDevice, 
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, 
				s32Count
			);
			if (s32Status == OSAL_ERROR)
			{
					u32Ret = 20;
			}
			OSAL_s32ThreadWait(100);
		}
		/* Multiple setting of the PWM for illumination with out delay */	
		for (s32Count = 0; s32Count <=100; ++s32Count)
		{
			s32Status 
			= 
			OSAL_s32IOControl
			(
				hDevice, 
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, 
				s32Count
			);
			if (s32Status == OSAL_ERROR)
			{
					u32Ret = 20;
			}
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 30;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

	}//end of if ( OSAL_ERROR == hDevice )
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32IlluminationIOControlsAfterClose() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_016
* DESCRIPTION:  	1) open the device
*						2) close the device
*						3) Get device version 
*						4)	Set PWM duty cycle
*						5)	Get the PWM duty cycle
* HISTORY:	  
				Created by Anoop Chandran (RBEI/ECM1)April 17, 2008
******************************************************************************/
tU32 u32IlluminationIOControlsAfterClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	tU32 u32DutyCycle = 0;
	/* Open the device in writeonly mode */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}//end of switch(s32Status)

    }
	else 
	{   
	  	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 10;
		}//end of if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )

		/* Get device version */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{   
			u32Ret += 50;
		}//end of if ( OSAL_ERROR == OSAL_s32IOControl())

		/* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
				OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MIDVAL ) )
		{
			u32Ret += 100;
		}//end of if ( OSAL_ERROR == OSAL_s32IOControl())

		/* Get the PWM duty cycle value */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&u32DutyCycle ) )
		{
			u32Ret += 900;
		}

	}//end of if ( OSAL_ERROR == hDevice )
 
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32lluminationGetPWMAfterClose()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_017
* DESCRIPTION:  	1) open the device
*						2)	Set PWM duty cycle
*						3) close the device
*						4) Get device version 
* HISTORY:	  
				Created by Anoop Chandran (RBEI/ECM1)April 17, 2008
******************************************************************************/
tU32 u32lluminationGetPWMAfterClose(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{

	    /* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_SET, ILLU_PWM_DUTYCYCLE_MIDVAL ))
		{
			u32Ret += 50;
		}
		u32DutyCycle = 0;

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 500;
		}
		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_ILLUMINATION_GET, (tS32)&u32DutyCycle ) )
		{
			u32Ret += 900;
		}
	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32lluminationSetGetPWMInval()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_018
* DESCRIPTION:  Set PWM duty cycle. Read back and verify if predetermined value
* 				is correctly set
* HISTORY:
			
				Updated by Anoop Chandran (RBEI/ECM1)April 17, 2008
				Created by Anoop Chandran (RBIN/EDI3) on 04-oct-2007
******************************************************************************/
tU32 u32lluminationSetGetPWMInval(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tU32 u32DutyCycle = 0;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{

	    /* Set PWM duty cycle with value 32767 */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_SET_PWM, ILLU_PWM_DUTYCYCLE_MIDVAL ))
		{
			u32Ret += 50;
		}
		u32DutyCycle = 0;
		/* Now, read PWM duty cycle value again */	
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_PWM_GET_PWM, (tS32)&u32DutyCycle ) )
		{
			u32Ret += 500;
		}

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 900;
		}

	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32lluminationSetBackLightLCD()      
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ILLUMINATION_019
* DESCRIPTION:  Set Back light and LCD
* HISTORY:
			
				Created by Anoop Chandran (RBIN/EDI3) on 04-02-2009
******************************************************************************/
tU32 u32lluminationSetBackLightLCD(tVoid) 
{
	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret 					= 0;
	tS32 s32Status 				= 0;
	OSAL_tenAccess enAccess 	= OSAL_EN_WRITEONLY;

	/* Open the device in writeonly mode */
   	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ILLUMINATION , enAccess );
    if ( OSAL_ERROR == hDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret += 10;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret += 20;
				break;
			default:
				u32Ret += 30;
		}//end of switch(s32Status)
    }
	else
	{

	    /* Set Back Light ON */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
		OSAL_C_S32_IOCTRL_ILLUMINATION_SET_BACKLIGHT, OSAL_C_S32_ILLUM_ON ))
		{
			u32Ret += 50;
		}
	    /* Set Back Light OFF */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
		OSAL_C_S32_IOCTRL_ILLUMINATION_SET_BACKLIGHT, OSAL_C_S32_ILLUM_OFF ))
		{
			u32Ret += 100;
		}
	    /* Set LCD ON */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
		OSAL_C_S32_IOCTRL_ILLUMINATION_SET_LCD, OSAL_C_S32_ILLUM_ON ) )
		{
			u32Ret += 500;
		}

	    /* Set LCD OFF */	
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
		OSAL_C_S32_IOCTRL_ILLUMINATION_SET_LCD, OSAL_C_S32_ILLUM_OFF ) )
		{
			u32Ret += 900;
		}

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose( hDevice ) )
		{
			u32Ret += 1300;
		}

	  
	}//end of if ( OSAL_ERROR == hDevice )

	return u32Ret;
}
/************** End of Test Code developed by RBIN *****************/  //   @Anoop


