/******************************************************************************
| FILE:			oedt_RAMdisk_TestFuncs.c
| PROJECT:      TE_Plattform_INT_XX  (RBIN Sub Project ID: 70705_T_OEDT)
| SW-COMPONENT: RAMDisk
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the main file for OEDT Test for RAMDisk
|               The corresponding test specs:  TU_OEDT _RAMDisk_TestSpec.xls
                                     version:  2.4
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				               | Author	& comments
| --.--.--      | Initial revision             | -------, -----
|20 April 2006  |  version 1.0                 |Haribabu Sannapaneni(RBIN/ECM1)
|------------------------------------------------ -----------------------------
|			    |	                           |
|26 April 2006  |  version 2.0                 |Haribabu Sannapaneni(RBIN/ECM1)
|   		    |                              | Updated after review comments
|______________________________________________________________________________
|			    |	                           |
|26 April 2006  |  version 2.1                 | Venkateswara N (RBIN/ECM1)
|   		    |                              | Test cases related to " File Create" are 
|				|							   | modified
|______________________________________________________________________________
|			    |	                           |
|08 Sep 2006    |  version 2.2                 | Anoop Krishnan (RBIN/ECM1)
|   		    |                              | Updated after review comments
|				|							   | 
|09 Sep 2006    |  version 2.3				   | Venkateswara N (RBIN/ECM1)
|				|							   | NULL checking condition is 
|				|							   | added before OSAL_vMemoryFree()
|				|							   | call	 	
______________________________________________________________________________
|			    |	                           |
|	 			|							   | Rakesh Dhanya (RBIN/ECM1)
| 06 Feb 2006   |	version 2.4				   | Inclusion of case number 080
|               |                              |Updated cases 077,078 and 079. 
_____________________________________________________________________________
|  				|							   | 
| 				|	version 2.5				   | Tinoy Mathews(RBIN/ECM1)
|				|							   | Removed the case 091,which contained
|				|							   | code to test the features:
| 05 March 2007	|							   | OSAL_C_S32_IOCTRL_FIOCHKDSK
|				|							   | OSAL_C_S32_IOCTRL_DISK_ABORT
|               |                              | OSAL_C_S32_IOCTRL_RAMDISK_RELOAD
|				|							   | which as per the reply for the MMS
|				|                              | ticket 109654,is not a valid test 
|               |                              | case for ADIT platform
|______________________________________________________________________________
|04 Apr 2007	|	ver 2.6					   | Pradeep Chand C(RBIN/EDI)
|				|							   | Updated test cases: 54, 58, 
|				|							   |		77, 78, 79, 80, 81, 87
|______________________________________________________________________________
| 20 Apr 2007   |  ver 2.7                     |Rakesh Dhanya (RBIN/ECM1)
|				|							   |Updated a few cases for usage 
|				|							   |of pointers to ensure 
|				|							   |better compilation of code.
|______________________________________________________________________________
|7 May, 2007    |  ver 2.8                     |Shilpa Bhat	(RBIN/EDI3)
|               |                              |Updated test case 081, 082
|______________________________________________________________________________
|19 Sept, 2007  | ver 2.9                      |Tinoy Mathews(RBIN/EDI3)
|				|							   |Updated case 073 based on
|               |                              |comments from 
|               |                              |Resch Carsten (CM-DI/PJ-CF32) 
|               |                              |and Anoop Chandran(RBIN/EDI3)
|_______________|______________________________|_______________________________
|21 Sept, 2007  | ver 3.0					   |Tinoy Mathews(RBIN/EDI3)
|				|							   |Updated cases using COMNFILE
|				|							   |,so that they have become 
|				|							   |independant,
|               |                              |Updated case 031
|_______________|______________________________|_______________________________
|17 Oct, 2007	| ver 3.1					   |Tinoy Mathews(RBIN/EDI3)
|				|							   |Removed duplicate cases to open
|				|							   |and close file with no extension,
|				|							   |Added two cases:
|				|							   |TU_OEDT_RAMDisk_089(Performance 
| 				|							   |test on Ramdisk)
|				|							   |TU_OEDT_RAMDisk_090(Multithreaded
|				|							   |write on to same file on Ramdisk)
|_______________|______________________________|_______________________________
|23  Oct, 2007	| ver 3.2					   |Pankaj Kumar(RBIN/EDI3)
|				|							   |Added new Test Cases for some IO 
												Controls:
|				|							   |Added three test cases:
|				|							   |TU_OEDT_RAMDisk_091(Renames of
|				|							   |the files|		
|				|							   |TU_OEDT_RAMDisk_092(Remove Files 
|				|							   |and Directories recursively)
|				|							   |TU_OEDT_RAMDisk_093(Copy the 
_____________________________________________________________________________
|23  Oct, 2007	| ver 3.3					   |Pankaj Kumar(RBIN/EDI3)
|				|							   |Updated cases:
|				|							   |TU_OEDT_RAMDisk_091,092,093
|_______________|______________________________|_____________________________
|19  Nov, 2007	| ver 3.4					   |Anoop Chandran(RBIN/EDI3)
|				|							   |Removed test cases which using 
|				|							   |invalid handle
|				|							   |TU_OEDT_RAMDisk_007,014,065
_______________________________________________________________________________
|10  Jan, 2008	| ver 3.5				       |Haribabu Sannapaneni(RBIN/ECM1)
|			    |							   |Added new test case:
|				|							   | 
|				|							   | File name with Unicode
|_______________|______________________________|_charecters.___________________
|22  Jan, 2008	| ver 3.6   			       |Anoop Chandran (RBIN/ECM1)
|			    |							   |Updated cases:
|				|							   |TU_OEDT_RAMDisk_036,079
|				|							   |from previous version ver 3.3
|______________________________________________________________________________
|14 Feb, 2008	| ver 3.7				       |Haribabu Sannapaneni(RBIN/ECM1)
|			    |							   |Rectified wrong Test code. 
|______________________________________________________________________________
|20 Oct, 2008	| ver 3.8				       |Test caes TU_OEDT_RAMDisk_025 and 082 are
|			    |						   	   |Update by Anoop Chandran(RBEI/ECM1) 
|_______________|______________________________|_______________________________	
|19 Mar, 2009	| ver 3.9				       |Lint Removal, Updated Generate_File_Name
|			    |						       |filename to RAMDisk_Generate_File_Name
|               |                              |Update by Shilpa Bhat(RBEI/ECF1) 
|_______________|______________________________|_______________________________	
|26 Mar, 2015   | ver 4.0                      | Lint removal                                             
|               |                              | update by Deepak Kumar(RBEI/ECF5)
|_______________________________________________________________________________
|12 June, 2015  | ver 4.1                      | Lint removal                                             
|               |                              | update by Balaji.V(RBEI/ECF5)
|_______________________________________________________________________________*/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! TRACE Calls from test functions are NOT ALLOWED !!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
#define vTtfisTrace(...)
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomar (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"

#ifndef CHECK_4_OSAL_ERROR
#define CHECK_4_OSAL_ERROR(Value,ErrCode) if((Value)==OSAL_ERROR) return ErrCode;
#endif

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define OEDT_RAMDISK_MAX_FILE_PATH_LEN		44


/*
   OSAL calls take as input "/dev/ramdisk/" pre-appended to the actual
   directory or file path. So the dimension of the character chain 
   which would contain the OSAL input path should accomodate for 
   the string length of "/dev/ramdisk/" which is 13.  The extra 1 is 
   to accomodate the string terminator.
*/





#define OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN 42
#define COMMON_FILE_ERROR                   100
#define HUGE_VALUE                          10000
#define MB_SIZE                             ((tU32)1048576)	
#define MAX_LEN                                 255
#define MAX_LEN_EXCEEDED                        256

/*Start of macro*/
#define DIR 								"File_Dir"
#define DIR_RECR1                 "File_Dir2" 
#define DIR1                                "File_Source"
#define _FILE12_RECR2         OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define _FILE11                             OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File11.txt"
#define _FILE12                             OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File12.txt"
#define _FILE13                             OSAL_C_STRING_DEVICE_RAMDISK"/"DIR1"/File13.txt"
#define _FILE14                             OSAL_C_STRING_DEVICE_RAMDISK"/"DIR1"/File14.txt"
/*End of macros*/

#define TEXTFILE1							OSAL_C_STRING_DEVICE_RAMDISK"/Textfile1.txt"
#define TEXTFILE2							OSAL_C_STRING_DEVICE_RAMDISK"/Textfile2.txt"
#define TEXTFILE3							OSAL_C_STRING_DEVICE_RAMDISK"/Textfile3.txt"
#define TEXTFILE							OSAL_C_STRING_DEVICE_RAMDISK"/Textfile.txt"
#define MP3FILE							    OSAL_C_STRING_DEVICE_RAMDISK"/MP3file.mp3"
#define ZIPFILE							    OSAL_C_STRING_DEVICE_RAMDISK"/ZIPfile.zip"
#define DATFILE								OSAL_C_STRING_DEVICE_RAMDISK"/DATfile.dat"
#define COMNFILE							OSAL_C_STRING_DEVICE_RAMDISK"/common.txt"
#define EVENT_NAME 							"RAMDISK_Simultaneous_Read_Write"
#define OEDT_RAMDISK_CYCLES								3
#define RAM_THREAD1                         0x0001
#define RAM_THREAD2                         0x0002
#define STACKSIZE                           4096
#define OEDT_RAMDISK_TEXTFILE3							OSAL_C_STRING_DEVICE_RAMDISK"/Textfile3.txt"


/*****************************************************************
| variable declaration (scope: local)
|----------------------------------------------------------------*/
   
OSAL_tSemHandle hSemAsyncRAMDisk;
OSAL_tSemHandle hSemAsyncWrRAMDisk;

/*Event handle*/
OSAL_tEventHandle hEventRAMDisk;

/*Device Handles to be used in threads*/
OSAL_tIODescriptor     hDeviceRAM = 0;

/*Global buffer parameters*/
tS8 *HugeBufferTr1 				   = NULL;
tS8 *HugeBufferTr2 				   = NULL;

/*Return Parameters*/
tU32 u32tr1;
tU32 u32tr2;

static tS8 as8RAMdiskMesgToWrite [1024] =
{
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d'
};


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

 

/************************************************************************
*FUNCTION:      vOEDTRAMDiskAsyncCallback 
* PARAMETER:    none
* RETURNVALUE:  "void" ( called Internally during Async Read )
* DESCRIPTION:  Call back function for Async read operation
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
 ************************************************************************/


tVoid vOEDTRAMDiskAsyncCallback (OSAL_trAsyncControl *prAsyncCtrl)
{
	tU32 u32RetVal;
	

	u32RetVal = OSAL_u32IOErrorAsync(prAsyncCtrl);


	if ( u32RetVal == OSAL_E_NOERROR )
	{
	    /* Release semaphore */

	    OSAL_s32SemaphorePost ( hSemAsyncRAMDisk );   
	}
	   /* 
	      If previous Async operation caused error then send
	      the error message.
	   */
} 


/************************************************************************
 *FUNCTION:      vOEDTRAMDiskAsyncWrCallback 
* PARAMETER:    none
* RETURNVALUE:  "void" ( called Internally during Async Write )
* DESCRIPTION:  Call back function for Async read operation
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*************************************************************************/


tVoid vOEDTRAMDiskAsyncWrCallback (OSAL_trAsyncControl *prAsyncCtrl)
{

   OSAL_u32IOErrorAsync(prAsyncCtrl);
   /* Release semaphore */
   OSAL_s32SemaphorePost ( hSemAsyncWrRAMDisk );   
}
  


/*****************************************************************************
* FUNCTION:		u32RAMDiskCreateCommonFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Creates data file on Ramdisk,Writes data into it.
* HISTORY:		Created by Tinoy Mathews (RBIN/EDI2)    21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskCreateCommonFile(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;	 
   	tCS8 DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Writing some test data to the data file";
	tU32 u32Ret = 0;    
	tU8 u8BytesToWrite = 40;
    tS32 s32BytesWritten = 0;

	/*Create and Open the common file*/
	if ( ((hFile = OSAL_IOCreate (COMNFILE, enAccess)) == OSAL_ERROR)  )
	{
		u32Ret = 1;
	}
	else
	{							
			/*Write data to file*/
			s32BytesWritten = OSAL_s32IOWrite (
		                               			hFile, DataToWrite,
												(tU32) u8BytesToWrite
											   );
			/*Check status of write*/
			if ( s32BytesWritten == OSAL_ERROR )
			{
				u32Ret += 3;
			}
			if( OSAL_s32IOClose ( hFile )== OSAL_ERROR )
			{
				u32Ret += 20;
			}
			
	}

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32RAMDiskRemoveCommonFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Creates data file on Ramdisk,Writes data into it.
* HISTORY:		Created by Tinoy Mathews (RBIN/EDI2)    21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskRemoveCommonFile(void)
{
	/*Declarations*/
	tU32 u32Ret;

	/*Remove the common File*/
	if(OSAL_s32IORemove(COMNFILE) == OSAL_ERROR)
		u32Ret = 1;
	else
		u32Ret = 0;
	
	/*Return Code*/
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		RAMDisk_Generate_File_Name()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Creates data file on Ramdisk,Writes data into it.
* HISTORY:		Created by Tinoy Mathews (RBIN/EDI2)    21 Sept, 2007
******************************************************************************/
tVoid RAMDisk_Generate_File_Name(tChar* ptrstr,tU32 len,tU32 limit)
{
	/*Declarations*/
	tChar ch = 'a';
    tU32 i;
	
	/*Generate filename*/
    for(i=len;i<limit-5;i++)
    {
    	/*Fill the string*/
        *(ptrstr+i)=ch;
        /*Goto next alphabet*/
        ch = ch+1; 
        /*Compare for limit*/
        if(ch == 'z'+1)
        {
        	/*Reset charater*/
            ch='a';
        }
     }
     /*Extension*/
     *(ptrstr+i)='.';
     *(ptrstr+i+1)='t';*(ptrstr+i+2)='x';*(ptrstr+i+3)='t';   
     /*String terminator*/
     *(ptrstr+i+4)='\0';
}

/*****************************************************************************
* FUNCTION:		WriteThread1()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Writes data to a file.
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) 15 Oct, 2007
******************************************************************************/
static void WriteThread1(tVoid *pvData )
{
	(tVoid)pvData;	//to satisfy lint
	/*Write some data to File1*/
	if( OSAL_ERROR == OSAL_s32IOWrite( hDeviceRAM,HugeBufferTr1,MB_SIZE ) )
	{
		u32tr1 += 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventRAMDisk,RAM_THREAD1,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}
/*****************************************************************************
* FUNCTION:		WriteThread2()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Writes data to a file.
* HISTORY:		Created by Anoop Chandran (RBIN/EDI3) 15 Oct, 2007
******************************************************************************/
static void WriteThread2(tVoid *pvData )
{
	(tVoid)pvData; //to satisfy lint
	/*Write some data to File1*/
	if( OSAL_ERROR == OSAL_s32IOWrite( hDeviceRAM,HugeBufferTr2,MB_SIZE ) )
	{
		u32tr2 += 2;
	}

	/*Post an event to the main thread*/
    OSAL_s32EventPost( hEventRAMDisk,RAM_THREAD2,OSAL_EN_EVENTMASK_OR );

	/*Exit thread*/
	OSAL_vThreadExit();
}


/*****************************************************************************
* FUNCTION:		OedtRamdiskCreateEvent()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Create an Event and Open the event field
* HISTORY:		Created by Tinoy Mathews (RBIN/EDI2)    15 Oct, 2007
******************************************************************************/
tU32 OedtRamdiskCreateEvent( OSAL_tEventHandle* hEventHandle )
{
	/*Definitions*/
	tU32 u32Ret = 0;

	/*Create the Event field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME , hEventHandle ) )
	{
		u32Ret = 3;
	}
	
    /*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		DeleteEvent()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Close the event and delete it
* HISTORY:		Created by Tinoy Mathews (RBIN/EDI2)    15 Oct, 2007
******************************************************************************/
tU32 DeleteEvent( const OSAL_tEventHandle * hEventHandle )
{
	/*Definitions*/
	tU32 u32Ret = 0;

	/*Close event*/
	if( OSAL_ERROR == OSAL_s32EventClose( *hEventHandle ) )
	{
		u32Ret += 400;
	}
	/*Delete event*/
	if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
	{
		u32Ret += 500;
	}

	/*Return error value*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskOpenDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDISK_001
* DESCRIPTION:  Opens the RAMDisk device
* HISTORY:		Created Hari Babu S (RBIN/ECM1)  Apr 21, 2006 
*
******************************************************************************/

tU32 u32RAMDiskOpenDevice(void)
{
	OSAL_tIODescriptor hDevice;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    
        
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}
      

   
/*****************************************************************************
* FUNCTION:		u32RAMDiskReOpenDev()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_002
* DESCRIPTION:  Try to open the device which is already opened
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskReOpenDev(void)
{
	OSAL_tIODescriptor hDevice1 = 0,hDevice2 = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    
        
    hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice1 == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
	    hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
		if( hDevice2 == OSAL_ERROR )
	    {
		   u32Ret = 10;
		}
		else
		{
			if( OSAL_s32IOClose ( hDevice2 )== OSAL_ERROR )
			{
				u32Ret = 100;
			}
		}
		if( OSAL_s32IOClose ( hDevice1 )== OSAL_ERROR )
		{
			u32Ret += 1000;
		}

	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskOpenCloseDevDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_003
* DESCRIPTION:  Try to open the device in different access modes
*				(Read only,Write only,Read write)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskOpenCloseDevDiffModes(void)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = (OSAL_tenAccess )0x0000;
	tU32 u32Ret = 0;
    
    /* In read only mode */
    enAccess = OSAL_EN_READONLY;
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}

	
	/* In read write mode */
	enAccess = OSAL_EN_READWRITE;
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 10;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}


	/* In write only mode */
	enAccess = OSAL_EN_WRITEONLY;
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 100;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}

	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskCloseDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_004
* DESCRIPTION:  Close the RAMDisk device
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskCloseDevice(void)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    
    
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskCloseDevInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_005
* DESCRIPTION:  Close the device with invalid parameters (should fail)  
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskCloseDevInvalParam(void)
{
	OSAL_tIODescriptor hDevice_inval = -1;
	tU32 u32Ret = 0;
        
    if( OSAL_s32IOClose ( hDevice_inval ) != OSAL_ERROR )
		{
			u32Ret = 1;
		}
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskOpenDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_006
* DESCRIPTION:  Open an existing directory
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskOpenDir(void)
{
	OSAL_tIODescriptor hDir ;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
    
	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskOpenNonExstngDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_007
* DESCRIPTION:  Open a non-existing directory(should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskOpenNonExstngDir(void)
{
	OSAL_tIODescriptor hDir ;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/Inval";
    

	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir != OSAL_ERROR )
	{
		u32Ret = 1;
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}

	}

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskOpenDirDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_008
* DESCRIPTION:  Try to open a directory in different access modes
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
******************************************************************************/

tU32 u32RAMDiskOpenDirDiffModes(void)
{
	OSAL_tIODescriptor hDir,hDir1 = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tPS16 Retval = NULL;
	OSAL_trIOCtrlDirent rDirent;
    tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/MYNEW1";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/MYNEW1";

	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	else
	{

	   /*	if( OSAL_szStringCopy ( rDirent.s8Name,szSubdirName ) == OSAL_ERROR )
		{
			  u32Ret = 2;
		}*/
		 Retval =(tPS16)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );
		 if(*Retval == OSAL_ERROR)
		{
			  u32Ret = 2;
		}
		
    	else
	    {
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{	
				/*In Read write mode*/
			    enAccess = OSAL_EN_READWRITE;
				hDir1 = OSAL_IOOpen ( szSubDirPath,enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret = 4;
				}
				else 
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret = 5;
					}
				}

				/*In Write only mode*/
			    enAccess = OSAL_EN_WRITEONLY;
				hDir1 = OSAL_IOOpen ( szSubDirPath,enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret += 10;
				}
				else 
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret += 20;
					}
				}


				/*In Read only mode*/
			    enAccess = OSAL_EN_READONLY;
				hDir1 = OSAL_IOOpen ( szSubDirPath,enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret += 100;
				}
				else 
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret += 200;
					}
				}
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 1000;
				}
	
			}
		}
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret += 10000;
		}
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskReOpenDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_009
* DESCRIPTION:  Try to open a DIR which is already opened 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskReOpenDir(void)
{
	OSAL_tIODescriptor hDir1,hDir2 = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
   
	hDir1 = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
		hDir2 = OSAL_IOOpen ( szDirPath,enAccess );
		if( hDir2 == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			if( OSAL_s32IOClose ( hDir2 ) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
		}
		if(	OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskCloseDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_010
* DESCRIPTION:  Close a directory,which is opened.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskCloseDir(void)
{
	OSAL_tIODescriptor hDir,hDir1 = 0;
	OSAL_trIOCtrlDirent rDirent;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tPS16 Retval = NULL;
    tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/MYNEW1";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/MYNEW1";

	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir == (OSAL_tIODescriptor)OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		Retval = (tPS16)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );
		if( *Retval == OSAL_ERROR )
		{
			  u32Ret = 2;
		}
    	else
	    {
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{	
				hDir1 = OSAL_IOOpen ( szSubDirPath,enAccess );
				if( hDir1 == OSAL_ERROR )
				{
					u32Ret = 4;
				}
				else 
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret = 5;
					}
				}
				if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
	
			}
		}
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret += 1000;
		}

			
	}
					
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskCloseInvalDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_011
* DESCRIPTION:  Try to close an invalid directory (should fail) 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskCloseInvalDir(void)
{
	OSAL_tIODescriptor hDir_Inval = -1;
   	tU32 u32Ret = 0;
    
 	if( OSAL_s32IOClose ( hDir_Inval ) != OSAL_ERROR )
	{
		u32Ret = 1;
	}
	
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32RAMDiskMkDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_012
* DESCRIPTION:  Create a directory with correct parameters.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskMkDir(void)
{
	OSAL_tIODescriptor hDir1 ;
    OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0;
	tPS16 Retval = NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/MYNEW1";

	hDir1 = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir1 == (OSAL_tIODescriptor)OSAL_ERROR )
	{
		u32Ret = 1;
	}

	else
	{
		Retval = (tPS16) OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );
		if( *Retval == OSAL_ERROR )
		{
			  u32Ret = 2;
		}
    	else
	    {
			if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{
				
				if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret = 4;
				}
			}
			

		}
		if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}

	
	}

	return u32Ret;
}
		


/*****************************************************************************
* FUNCTION:		u32RAMDiskMultiMkDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_013
* DESCRIPTION:  Create Multiple sub-directories
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskMultiMkDir(void)
{
    OSAL_tIODescriptor hDir1,hDir2 = 0,hDir3 = 0;
	OSAL_trIOCtrlDirent rDirent1,rDirent2,rDirent3;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirPath1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUB1DIR";
	tChar szSubdirName1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUB1DIR";
	tChar szSubdirName2 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUB2DIR";
	tChar szSubdirPath2 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUB1DIR/SUB2DIR";
	tChar szSubdirName3 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUB3DIR";	

    hDir1 = OSAL_IOOpen ( szDirPath,enAccess );
    if( hDir1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
			 
	    (tVoid)OSAL_szStringCopy ( rDirent1.s8Name,szSubdirName1 );	  
	    if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t First level Sub-Dir created" );

			(tVoid)OSAL_szStringCopy ( rDirent2.s8Name,szSubdirName2 );
			hDir2 = OSAL_IOOpen ( szSubdirPath1,enAccess );
			if( hDir2 == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{

				if ( OSAL_s32IOControl ( hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
												(tS32)&rDirent2) == OSAL_ERROR)
				{
					u32Ret = 4;
				}
				else
				{
					vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Second level Sub-Dir created" );
					(tVoid)OSAL_szStringCopy ( rDirent3.s8Name,szSubdirName3 );
					hDir3 = OSAL_IOOpen ( szSubdirPath2,enAccess );
					if( hDir3 == OSAL_ERROR )
					{
						u32Ret = 5;
					}
					else
					{

						if ( OSAL_s32IOControl 
										( hDir3,
											OSAL_C_S32_IOCTRL_FIOMKDIR,
														(tS32)&rDirent3) 
																== OSAL_ERROR )
						{
							u32Ret = 6;
						}
						else
						{
							vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Third level Sub-Dir created" );
							
							if ( OSAL_s32IOControl 
											( hDir3,
												OSAL_C_S32_IOCTRL_FIORMDIR,
														(tS32)&rDirent3)
															  == OSAL_ERROR )
							{
								u32Ret += 100;
							}
						}
						OSAL_s32IOClose ( hDir3 );

					}
					if ( OSAL_s32IOControl ( hDir2,
											  OSAL_C_S32_IOCTRL_FIORMDIR,
													 (tS32)&rDirent2)
													 		 == OSAL_ERROR )
					{
						u32Ret += 1000;
					}
				}
				OSAL_s32IOClose ( hDir2 );
			}	
			if ( OSAL_s32IOControl ( hDir1,
										OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent1) == OSAL_ERROR )
			{
				u32Ret += 10000;
			}
		}
		OSAL_s32IOClose ( hDir1 );

	}
	return u32Ret;
}




/*****************************************************************************
* FUNCTION:		u32RAMDiskMkDirMultiTimes() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_014
* DESCRIPTION:  Create a directory, which is already existing (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskMkDirMultiTimes(void)
{
	OSAL_tIODescriptor hDir;
    OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/NEWdir";
	
    hDir = OSAL_IOOpen ( szDirPath,enAccess ); /* open the root dir */
    if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );	  
   
    	if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
									  (tS32)&rDirent) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret = 3;
			}
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
										(tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
			
		}
		if ( OSAL_s32IOClose ( hDir )== OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}

	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskMkDirInvalName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_015
* DESCRIPTION:  Create a directory with invalid name (should fail) 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskMkDirInvalName(void)
{
	OSAL_tIODescriptor hDir;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK;
    OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/*@#**" ;

	hDir = OSAL_IOOpen ( szDirPath,enAccess );
    if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) != OSAL_ERROR )
		{
			u32Ret = 2;
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 10;
			}

		}
	}			
	
	return u32Ret;
}





/*****************************************************************************
* FUNCTION:		u32RAMDiskMkDirInvalPath()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_016
* DESCRIPTION:  Create a directory with invalid path (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskMkDirInvalPath(void)
{
	OSAL_tIODescriptor hDir;
    OSAL_trIOCtrlDirent rDirent;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/MYSUB1/MYSUB2/MYSUB3";
	
	hDir = OSAL_IOOpen ( szDirPath,enAccess );
    if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) != OSAL_ERROR )
		{
			u32Ret = 2;
			
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
	}

	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskRmDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_017
* DESCRIPTION:  Remove an empty existing directory
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskRmDir(void)
{
	OSAL_tIODescriptor hDir1;
    OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0;
	tPS16 Retval = NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/MYNEWDIR21";

	hDir1 = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir1 == (OSAL_tIODescriptor) OSAL_ERROR )
	{
		u32Ret = 1;
	}

	else
	{
		Retval = (tPS16)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );
		if( *Retval == OSAL_ERROR )
		{
			  u32Ret = 2;
		}
    	else
	    {
			if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{
				
				if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret = 4;
				}
			}
			

		}
		OSAL_s32IOClose ( hDir1 );

	}

	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskRmNonExstngDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_018
* DESCRIPTION:  Remove a non- existing directory (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/


tU32 u32RAMDiskRmNonExstngDir(void)
{
	OSAL_tIODescriptor hDir;
    OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0;
	tPS16 Retval = NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirNameInval [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/Inval";

	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir == (OSAL_tIODescriptor)OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		Retval = (tPS16)(OSAL_szStringCopy ( rDirent.s8Name,szSubdirNameInval ));
	    if( *Retval == OSAL_ERROR )
		{
			  u32Ret = 2;
		}
		else
		{
			if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
										(tS32)&rDirent) != OSAL_ERROR )
			{
				u32Ret = 3;
			}
		}
	}

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskRmNonEmptyDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_019
* DESCRIPTION:  Remove a non- empty sub-directory (should fail)  
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskRmNonEmptyDir(void)
{
	OSAL_tIODescriptor hDir1,hDir2 = 0;
    OSAL_trIOCtrlDirent rDirent1,rDirent2;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubdirName1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUBONE";
	tChar szSubdirPath1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUBONE";	
	tChar szSubdirName2 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUBONE/SUBTWO";
	

	hDir1 = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent1.s8Name,szSubdirName1 );	  
	   
	    if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			hDir2 = OSAL_IOOpen ( szSubdirPath1,enAccess );
			if( hDir2 == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{

				(tVoid)OSAL_szStringCopy ( rDirent2.s8Name,szSubdirName2 );	
				if ( OSAL_s32IOControl 
							( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
											(tS32)&rDirent2) == OSAL_ERROR )
				{
					u32Ret = 4;
				}
				else
			   	{			
			   		if ( OSAL_s32IOControl 
			   				( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
											(tS32)&rDirent1) != OSAL_ERROR )
					{
						u32Ret = 5;
					}
					
					
					if ( OSAL_s32IOControl
									 ( hDir1,
									 		OSAL_C_S32_IOCTRL_FIORMDIR,
											  (tS32)&rDirent2) == OSAL_ERROR )
					{
						u32Ret += 10;
					}
					
				}
			}
						
			if ( OSAL_s32IOControl ( hDir1,
										OSAL_C_S32_IOCTRL_FIORMDIR,
												(tS32)&rDirent1) == OSAL_ERROR )
							{
								u32Ret += 100;
							}
		}
		OSAL_s32IOClose ( hDir1 );

	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskRmFileUsingRmDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_020
* DESCRIPTION:  Test with which is not a directory (with files)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskRmFileUsingRmDir(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	OSAL_trIOCtrlDirent rDirent;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/FILEFIRST.txt";
	

	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile == (OSAL_tIODescriptor)OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		OSAL_pvMemorySet(rDirent.s8Name,'\0',OSAL_C_U32_MAX_PATHLENGTH);

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szFilePath );

		if ( OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIORMDIR,
									  (tS32)&rDirent) != OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
			{
				u32Ret += 2;
			}
			else
			{
				if(OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR)
				{
					u32Ret += 4;
				}
			}
		}
	}
	return u32Ret;

}




/*****************************************************************************
* FUNCTION:		u32RAMDiskGetDirInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_021
* DESCRIPTION:  Get DIR info with correct parameters
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskGetDirInfo(void)
{
	OSAL_tIODescriptor hDir1;
	OSAL_tenAccess enAccess;
    OSAL_trIOCtrlDirent rDirent1,rDirent2;
	OSAL_trIOCtrlDir rDirCtrl;
	tU32 u32Ret = 0;
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "TESTDIR1"	;
	tChar szSubdirNameroot [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
  	enAccess = OSAL_EN_READWRITE;


	(tVoid)OSAL_szStringCopy ( rDirent1.s8Name,szSubdirName );	  

	hDir1 = OSAL_IOOpen ( szSubdirNameroot,enAccess );
    if ( hDir1 == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if ( OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
		/* Create a sub-DIR under root */
		
		{
			u32Ret = 2;
		}
		else
		{
			/* Get DIR info for root DIR so that it displays the newly created Sub-dir */

			(tVoid)OSAL_szStringCopy ( rDirent2.s8Name,szSubdirNameroot );
			rDirCtrl.fd = hDir1;
			rDirCtrl.s32Cookie = 0;
			rDirCtrl.dirent = rDirent2;

			if( OSAL_s32IOControl (
	                                       hDir1,
	                                       OSAL_C_S32_IOCTRL_FIOREADDIR,
	                                       (tS32) &rDirCtrl
	                                    ) == OSAL_ERROR	)
			{
				u32Ret = 3;
			}
			else
			{
				vTtfisTrace(  OSAL_C_TRACELEVEL1,"DIR Info :\n\t %s \n"
									,(tString)rDirCtrl.dirent.s8Name );
				if ( OSAL_s32IOControl ( hDir1,
										 OSAL_C_S32_IOCTRL_FIORMDIR,
										 (tS32)&rDirent1) == OSAL_ERROR )
				{
					u32Ret = 4;
				}
				else
				{
					if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
					{
						u32Ret = 5;
					}
				}
			}
		}
	}

	return u32Ret;

}



/*****************************************************************************
* FUNCTION:		u32RAMDiskGetDirInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_022
* DESCRIPTION:  Get DIR info with in-correct parameters 
*              (Non -existing directory, invalid parameters) - (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskGetDirInfoInvalParam(void)
{
	OSAL_tIODescriptor hDir_inval = -1;
    OSAL_trIOCtrlDirent rDirent;
	OSAL_trIOCtrlDir rDirCtrl = {0};
	tU32 u32Ret = 0;
	tChar szSubdirNameInval [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/Inval";
		
	(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirNameInval );
	
	rDirCtrl.fd = hDir_inval;
	rDirCtrl.s32Cookie = 0;
	rDirCtrl.dirent = rDirent;
	
	if( OSAL_s32IOControl (hDir_inval,OSAL_C_S32_IOCTRL_FIOREADDIR,
	                                  (tS32) &rDirCtrl) != OSAL_ERROR	)
	{
		u32Ret = 1;
	}


  return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreate()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_023
* DESCRIPTION:  Create file  with correct parameters
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

	
tU32 u32RAMDiskFileCreate(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Testf1.txt";
	
	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}

		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_024
* DESCRIPTION:  Create file  with different parameters
*               (with different modes like Read only,Write only,Read write)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskFileCreateDiffModes(void)
{
	OSAL_tenAccess enAccess ;
	OSAL_tIODescriptor hFile ;
	tU32 u32Ret = 0;
	
	/* In Write only mode */
	enAccess = OSAL_EN_WRITEONLY;
	hFile = OSAL_IOCreate (TEXTFILE1, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else if( OSAL_s32IORemove ( TEXTFILE1 ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
	}
   
	/* In Read Only mode */
	
	enAccess = OSAL_EN_READONLY;
	hFile = OSAL_IOCreate (TEXTFILE2, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
		else if( OSAL_s32IORemove ( TEXTFILE2 ) == OSAL_ERROR )
		{
			u32Ret += 30;
		}
	}

	/* In Read-Write mode */
	
	enAccess = OSAL_EN_READWRITE;

	hFile = OSAL_IOCreate (TEXTFILE3, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 100;
	}
	else
	{
	        if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}  
	        else if( OSAL_s32IORemove ( TEXTFILE3 ) == OSAL_ERROR )
		{
			u32Ret += 300;
		}
	}
	return u32Ret;


}

/*****************************************************************************
* FUNCTION:		u32RAMDiskMultiFileCreation()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_025
* DESCRIPTION:  Create the existing file multiple times. (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Modified by Haribabu Sannapaneni (RBIN/ECM1) Feb 15, 2008
*				Modified by Anoop Chandran (RBIN/ECM1) Oct 20, 2008
******************************************************************************/

tU32 u32RAMDiskMultiFileCreation(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile1,hFile2 = 0;
	tU32 u32Ret = 0;
	tS32 s32BytesWritten = 0;
	tCS8 DataToWrite[24] = "Test_for_Writing data";
    tU8 u8BytesToWrite = 20; /* Number of bytes to write */
	tS32 s32BytesWritten1 = 0;
	tCS8 DataToWrite1[24] = "Overwrite_old_data";
    tU8 u8BytesToWrite1 = 20; /* Number of bytes to write */
	tS32 s32File_Size,s32File_Size1  = 0;
    

	hFile1 = OSAL_IOCreate (OEDT_RAMDISK_TEXTFILE3, enAccess);

	if ( hFile1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*Write 20 bytes of data to the created  file */
		s32BytesWritten = OSAL_s32IOWrite( hFile1, DataToWrite,(tS32) u8BytesToWrite);
		if ( (s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != u8BytesToWrite)  )
	 	{
	  		u32Ret = 3;
			if( OSAL_s32IOClose ( hFile1 ) != OSAL_ERROR )
			{
 				if( OSAL_s32IORemove ( OEDT_RAMDISK_TEXTFILE3 ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}

			return u32Ret; 
	 	}
		s32File_Size = OSALUTIL_s32FGetSize(hFile1);
		if (s32File_Size == OSAL_ERROR)
		{
			u32Ret = 60;
			if( OSAL_s32IOClose ( hFile1 ) != OSAL_ERROR )
			{
 				if( OSAL_s32IORemove ( OEDT_RAMDISK_TEXTFILE3 ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}

			return u32Ret; 
		}

        /*Changing the permission */
 		enAccess = OSAL_EN_READONLY;
		/*Recreating the same file */
		hFile2 = OSAL_IOCreate (OEDT_RAMDISK_TEXTFILE3, enAccess);

		if ( hFile2 == OSAL_ERROR )
		{
			u32Ret  = 5;
			if( OSAL_s32IOClose ( hFile1 ) != OSAL_ERROR )
			{
 				if( OSAL_s32IORemove ( OEDT_RAMDISK_TEXTFILE3 ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
			}
		}
		else
		{
        		/*Write 20 bytes of data to the second created  file to confirm the permission changed */
				s32BytesWritten1 = OSAL_s32IOWrite( hFile2, DataToWrite1,(tS32) u8BytesToWrite1);
				if (s32BytesWritten1 != OSAL_ERROR)
	 			{
	  				u32Ret = 20;
	 			}
				s32File_Size1 = OSALUTIL_s32FGetSize(hFile2);
				if (s32File_Size1 == OSAL_ERROR)
				{
					u32Ret +=50;
				}
				else
				{	
					if (s32File_Size != s32File_Size1)
					{
						u32Ret += 70;
					}
				}
				if( OSAL_s32IOClose ( hFile2 ) == OSAL_ERROR )
	    		{
			 		u32Ret += 30;
				}

		}
	    if( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
		{
			u32Ret += 3;
		}
		if( OSAL_s32IORemove ( OEDT_RAMDISK_TEXTFILE3 ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateInvalName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_026
* DESCRIPTION:  Create file with incorrect file name (should fail) 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskFileCreateInvalName(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/*/<>>.txt";

	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile != OSAL_ERROR )
	{
		u32Ret = 1;

		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 3;
		}
		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}

	}

	return u32Ret;
}
	

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateInvalPath()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_027
* DESCRIPTION:  Create file with invalid path(should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskFileCreateInvalPath(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/dummydir/file1.txt";

	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
		 if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 3;
		}

		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}

	}

	return u32Ret;
}
	


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateLongName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_028
* DESCRIPTION:  Create file with file name containing more than 8 characters
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21 Sept , 2007
                Modified by Haribabu Sannapaneni (RBIN/ECM1) Feb 15,2008
******************************************************************************/

tU32 u32RAMDiskFileCreateLongName(void)
{
	/*Declarations*/
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar file_long_name[255];
	tChar file_long_name_ex[256];
	tChar file_long_name_ex_[257];

	
	/*Initialize the allocated memory*/
	(tVoid)OSAL_pvMemorySet(file_long_name,'\0',MAX_LEN);
	/*Copy Ramdisk path*/
	(tVoid)OSAL_szStringCopy(file_long_name,OSAL_C_STRING_DEVICE_RAMDISK);
	/*Add '/' character*/
	(tVoid)OSAL_szStringConcat(file_long_name,"/");
	/*Call filename generator*/
	RAMDisk_Generate_File_Name(file_long_name,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_RAMDISK)+1,MAX_LEN);
	/*Create the file*/
	if ( (hFile = OSAL_IOCreate (file_long_name, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*Close the file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else 
		{
			/*Remove the file*/
			if( OSAL_s32IORemove ( file_long_name ) == OSAL_ERROR )
			{
				u32Ret = 2;
			}
		}
	}

   
	/*Initialize the allocated memory*/
	OSAL_pvMemorySet(file_long_name_ex,'\0',MAX_LEN_EXCEEDED);
	/*Copy Ramdisk path*/
	(tVoid)OSAL_szStringCopy(file_long_name_ex,OSAL_C_STRING_DEVICE_RAMDISK);
	/*Add '/' character*/
	(tVoid)OSAL_szStringConcat(file_long_name_ex,"/");
	/*Call filename generator*/
	RAMDisk_Generate_File_Name(file_long_name_ex,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_RAMDISK)+1,MAX_LEN_EXCEEDED);
	/*Create the file*/
	if ( (hFile = OSAL_IOCreate (file_long_name_ex, enAccess)) != OSAL_ERROR )
	{
		u32Ret += 10;
		/*Close the file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else 
		{
			/*Remove the file*/
			if( OSAL_s32IORemove ( file_long_name_ex ) == OSAL_ERROR )
			{
				u32Ret = 2;
			}
		}

	}

	/*Initialize the allocated memory*/
	OSAL_pvMemorySet(file_long_name_ex_,'\0',MAX_LEN_EXCEEDED+1);
	/*Copy Ramdisk path*/
	(tVoid)OSAL_szStringCopy(file_long_name_ex_,OSAL_C_STRING_DEVICE_RAMDISK);
	/*Add '/' character*/
	(tVoid)OSAL_szStringConcat(file_long_name_ex_,"/");
	/*Call filename generator*/
	RAMDisk_Generate_File_Name(file_long_name_ex_,OSAL_u32StringLength(OSAL_C_STRING_DEVICE_RAMDISK)+1,MAX_LEN_EXCEEDED+1);
	/*Create the file*/
	if ( (hFile = OSAL_IOCreate (file_long_name_ex_, enAccess)) != OSAL_ERROR )
	{
		u32Ret += 100;
		/*Close the file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 100;
		}
		else 
		{
			/*Remove the file*/
			if( OSAL_s32IORemove ( file_long_name_ex_ ) == OSAL_ERROR )
			{
				u32Ret += 200;
			}
		}

	}


	/*Return error code*/
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateDiffExtn()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_029
* DESCRIPTION:  Create file with different file extensions 
*				such as .mp3, .dat, .txt and so on
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileCreateDiffExtn(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	
	/* Create a .txt file*/
	
	
	hFile = OSAL_IOCreate (TEXTFILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
	   if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
	   {
			u32Ret = 3;
	   }
		else if( OSAL_s32IORemove ( TEXTFILE ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}

	/* Create a .mp3 file */
	
	hFile = OSAL_IOCreate (MP3FILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 30;
		}
		else if( OSAL_s32IORemove ( MP3FILE ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}

	
	/* Create a .dat file */
	
	hFile = OSAL_IOCreate (DATFILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 100;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 300;
		}
		else if( OSAL_s32IORemove ( DATFILE ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}


	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCrtOpnClseNoExtn()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_030
* DESCRIPTION:  Create a file without any file extension
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3)  15 Oct, 2007
******************************************************************************/
tU32 u32RAMDiskFileCrtOpnClseNoExtn(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Noextn";

	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 7;
		}
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 3;
			}
		}
		if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCreateInvalExtn()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_031
* DESCRIPTION:  Create files with Invalid extensions
*				(Ex:$&.@#*,111.***)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskFileCreateInvalExtn(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/first.*/><";

	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile != OSAL_ERROR )
	{
		u32Ret = 1;		
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 3;
		}
		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileDel()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_032
* DESCRIPTION:  Delete an existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileDel(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/one.txt";
   
	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) )
		{
			u32Ret = 3;
		}
		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileDelNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_033
* DESCRIPTION:  Delete a non-existing file (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileDelNonExstng(void)
{
   	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Dummy.txt";
  		
	if( OSAL_s32IORemove ( szFilePath ) != OSAL_ERROR )
	{
		u32Ret = 2;
	}

	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileDelDiffExtn()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_034
* DESCRIPTION:  Delete files with different file extensions 
*               such as .mp3, .dat, .txt and so on
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileDelDiffExtn(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	
	/* Delete a .txt file*/
	

	hFile = OSAL_IOCreate (TEXTFILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else if( OSAL_s32IORemove ( TEXTFILE ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}

	/* Delete a .mp3 file */
	
	hFile = OSAL_IOCreate (MP3FILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 30;
		}
		else if( OSAL_s32IORemove ( MP3FILE ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}

	
	/* Delete a .dat file */
	
	hFile = OSAL_IOCreate (DATFILE, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret += 100;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 300;
		}
		else if( OSAL_s32IORemove ( DATFILE ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}


	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileDelWithoutClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_035
* DESCRIPTION:  Try deleting the file without closing it (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Pradeep Chand C (RBIN/EDI) 	4 Apr, 2006
*				Updated by Anoop Chandran (RBIN/ECM1) 	22 Jan, 2008  
*				 OSAL_s32IORemove can't remove before closing the file or directory
******************************************************************************/


tU32 u32RAMDiskFileDelWithoutClose(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Test.txt";

	if ( (hFile =OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
			{
			

				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 10;
				}
				else
				{
					if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
					{
						u32Ret += 50;
					}
		   		}
			}
			else
			{
				u32Ret = 2;
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
						u32Ret += 100;
					}
				else
				{
					if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
					{
						u32Ret += 200;
		   		}
			}
				
			}
	}

	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileOpen() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_036
* DESCRIPTION:  Open an existing file 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileOpen(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/file1.txt";
   
	if ( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			hFile = OSAL_IOOpen ( szFilePath,enAccess );
			if( hFile == OSAL_ERROR )
			{
				u32Ret = 10;
				if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
				{
					u32Ret += 100;
				}

			}

			else
			{
				if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
				{
					u32Ret += 20;
				}
				else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
				{
					u32Ret += 30;
				}

			}
		}
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFilOpenNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_037
* DESCRIPTION:  Open a non-existing file (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFilOpenNonExstng(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Dummy.txt";

	
	hFile = OSAL_IOOpen ( szFilePath,enAccess );
	if( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
   
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
				
	}

	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileOpenInvalPath()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_038
* DESCRIPTION:  Open file with invalid path name (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileOpenInvalPath(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Dummydir/Dummy.txt";
	
	hFile = OSAL_IOOpen ( szFilePath,enAccess );
	if( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
   
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
				
	}

	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileOpenInvalName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_039
* DESCRIPTION:  Open file with invalid file name (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Modified the logic - Venkateswara.N 04 Sep, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileOpenInvalName(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Dummydir/**@.txt";
	
	
	hFile = OSAL_IOCreate (szFilePath, enAccess);
	if ( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
        OSAL_s32IOClose ( hFile ) ; /* Create will internally open file, hence, close file before opening it again*/
	    hFile = OSAL_IOOpen ( szFilePath,enAccess );
	    if( hFile != OSAL_ERROR )
	    {
			u32Ret = 2;   
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 20;
		   
				if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
				{
			u32Ret += 10;
		        }
			}
		
		}
		else
		{
			if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
			{
				u32Ret += 100;
			}
		} 

		
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileOpenDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_040
* DESCRIPTION:  Open file  with different file modes  
*               like Read only,Read write,Write only
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/


tU32 u32RAMDiskFileOpenDiffModes(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/Newfile.txt";

	if ( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		/* Open in Read Write mode */
		
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 30;
			}
		}
		
		/* Open in Read Only mode */

		enAccess = OSAL_EN_READONLY;
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 50;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 100;
			}
		}
		
		/* Open in Write only mode */
		
		enAccess = OSAL_EN_WRITEONLY;
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 500;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 1000;
			}
		}
		if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10000;
		}


	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileOpenDiffExtn()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_041
* DESCRIPTION:  Open file with different extensions such as .mp3, .dat and  so on
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileOpenDiffExtn(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;

	
	/* Open a .zip file */
	if ( (hFile = OSAL_IOCreate (ZIPFILE, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}

		hFile = OSAL_IOOpen ( ZIPFILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 3;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 7;
			}
		}			
		if( OSAL_s32IORemove ( ZIPFILE ) == OSAL_ERROR )
		{
			u32Ret += 11;
		}
		
	}

	
	/* Open a .mp3 file */
	
	if ( (hFile = OSAL_IOCreate (MP3FILE, enAccess)) == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
		hFile = OSAL_IOOpen ( MP3FILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 30;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 70;
			}
		}
		if( OSAL_s32IORemove ( MP3FILE ) == OSAL_ERROR )
		{
			u32Ret += 110;
		}
			
	}


	/* Open a .dat file */
	
	
  	if ( (hFile = OSAL_IOCreate (DATFILE, enAccess)) == OSAL_ERROR )
	{
		u32Ret += 100;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
		hFile = OSAL_IOOpen ( DATFILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 300;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 700;
			}
		}
		if( OSAL_s32IORemove ( DATFILE ) == OSAL_ERROR )
		{
			u32Ret += 1100;
		}
	}
	
	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_042
* DESCRIPTION:  Close an existing file 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileClose(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/TFILE.txt";
	

	if ( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 30;
			}
		}
		if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCloseNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_043
* DESCRIPTION:  Close a non-existing file / invalid file handle  (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileCloseNonExstng(void)
{
	OSAL_tIODescriptor hFile = -1;
	tU32 u32Ret = 0;

	
	if( OSAL_s32IOClose ( hFile ) != OSAL_ERROR )
	{
		u32Ret = 1;
	}
	return u32Ret;

}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileCloseDiffExtns()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_044
* DESCRIPTION:  Close files with different file extensions 
*				such as .mp3, .dat, .txt and so on 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskFileCloseDiffExtns(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;

	
	/* Close a .zip file */
	

	if ( (hFile = OSAL_IOCreate (ZIPFILE, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 7;
		}
		hFile = OSAL_IOOpen (ZIPFILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 3;
			}
		}
		if( OSAL_s32IORemove ( ZIPFILE ) == OSAL_ERROR )
		{
			u32Ret += 1000;
		}
    }

	
	/* Close a .mp3 file */
	
  	if ( ( (hFile = OSAL_IOCreate (MP3FILE, enAccess)) == OSAL_ERROR ) )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 70;
		}
		hFile = OSAL_IOOpen ( MP3FILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 20;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 30;
			}
		}				
		if( OSAL_s32IORemove ( MP3FILE ) == OSAL_ERROR )
		{
			u32Ret += 2000;
		}
	}


	/* Close a .dat file */
	
   	if ( ( (hFile = OSAL_IOCreate (DATFILE, enAccess) ) == OSAL_ERROR )	)
	{
		u32Ret += 100;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 700;
		}
		hFile = OSAL_IOOpen ( DATFILE,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 200;
		}

		else
		{
			if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 300;
			}
		}
		if( OSAL_s32IORemove ( DATFILE ) == OSAL_ERROR )
		{
			u32Ret += 3000;
		}
	}


	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_045
* DESCRIPTION:  Read data from a existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21st Sept,2007
******************************************************************************/  
tU32 u32RAMDiskFileRead(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;     	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check for the success of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Allocate memory dynamically*/
		ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);
		/*Check if allocation successful*/
		if ( ps8ReadBuffer == NULL )
		{
			u32Ret = 2;
		}
		else
		{
			/*Read from common file*/
			s32BytesRead = OSAL_s32IORead (
                                hFile,
                                ps8ReadBuffer,
                                (tU32) u32BytesToRead
                             );
			/*Check the status of read*/
			if ( s32BytesRead == OSAL_ERROR )
			{
				u32Ret = 3;	
    		}
			else
			{
				vTtfisTrace( OSAL_C_TRACELEVEL1,
	 							"\n\t Bytes read : %d",s32BytesRead );	
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	} 
	/*Free the allocated memory*/
	if( ps8ReadBuffer != NULL )
	{
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
    }
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileReadAccessCheck()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_046
* DESCRIPTION:  Try to read from the file which has no access rights / permissions. (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskFileReadAccessCheck(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_WRITEONLY;
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Open success check*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Allocate memory*/
		ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);
		/*Check for success*/
		if ( ps8ReadBuffer == NULL )
		{
			u32Ret = 2;
		}
		else
		{
			/*Read data from file*/
			s32BytesRead = OSAL_s32IORead (
                                hFile,
                                ps8ReadBuffer,
                                (tU32) u32BytesToRead
                             );
			/*Check if read is successful*/
			if ( s32BytesRead != OSAL_ERROR )
			{
				u32Ret = 3;	
    		}
		}
		/*Close the file handle*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
			
	}

	/*Free the allocated memory*/
	if( ps8ReadBuffer != NULL)			
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}

	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskFileReadSubDir()	   
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_047
* DESCRIPTION:  Read file from the different sub-directories
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskFileReadSubDir(void)
{	
	OSAL_tIODescriptor hDir,subDir=0,hFile = 0,hFile1 = 0;
    OSAL_trIOCtrlDirent rDirent;  
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tS8 *ps8ReadBuffer;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;
    tU8 u8BytesToWrite = 20; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/NEWSUB53";
	tChar szSubdirName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/NEWSUB53";
	tChar szFileName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/NEWSUB53/Myfile.txt";
	tCS8 DataToWrite[30] = "Writing data to Sub-dir";
	
	/*Initialize the Dirent structure*/
	OSAL_pvMemorySet(rDirent.s8Name,'\0',OSAL_C_U32_MAX_PATHLENGTH);
	
	hDir = OSAL_IOOpen ( szDirPath,enAccess );/* szSubDirPath */
	if( hDir ==OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent.s8Name,szSubdirName );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
		      subDir= OSAL_IOOpen ( szSubDirPath,enAccess );
			if( subDir != OSAL_ERROR )
			{

				if ( (hFile = OSAL_IOCreate ( szFileName,enAccess )) != OSAL_ERROR )
				{
					if( hFile != OSAL_ERROR )
					{
						s32BytesWritten = OSAL_s32IOWrite (
					                           hFile, DataToWrite,
													(tU32) u8BytesToWrite
														    );
						if ( s32BytesWritten == OSAL_ERROR )
						{
							u32Ret = 3;
						}
						OSAL_s32IOClose ( hFile );
					}
					hFile1 = OSAL_IOOpen ( szFileName,enAccess );
					if( hFile1 != OSAL_ERROR )
					{
						ps8ReadBuffer = (tS8 *)
										 OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
						s32BytesRead = OSAL_s32IORead (
					                                hFile1,
					                                ps8ReadBuffer,
					                                (tU32) u32BytesToRead
				                             			);
						if ( s32BytesRead == OSAL_ERROR )
						{
							u32Ret += 10;	
					    }
						else
						{
							vTtfisTrace( OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes read : %d",s32BytesRead );	
						}
							
						if( ps8ReadBuffer != NULL)
						{
							OSAL_vMemoryFree( ps8ReadBuffer );
							ps8ReadBuffer = NULL;
						}
						if ((OSAL_s32IOClose ( hFile1 ))!=OSAL_ERROR)
						{				
							OSAL_s32IORemove ( szFileName );
						}

					}
					
				}
				else
				{
				   u32Ret += 1111;	
				}
			}
		   if ((OSAL_s32IOClose ( subDir ))!=OSAL_ERROR)
		   {	 
		   		if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent) == OSAL_ERROR )
				{
					u32Ret += 1000;
				}
			}	
	   
		OSAL_s32IOClose ( hDir );
		}
	}
	return u32Ret;   
} 


/*****************************************************************************
* FUNCTION:	   u32RAMDiskLargeFileRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_048
* DESCRIPTION:  Try to read large data (1MB) from the file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Pradeep Chand C (RBIN/EDI)		4 Apr, 2007
******************************************************************************/

tU32 u32RAMDiskLargeFileRead(void)
{	
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
   	tChar DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Writedat";
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile1.txt";
   	tS8 *ps8ReadBuffer;     
   	tS32 s32BytesRead = 0;
	tU32 u32Slen = 0;
   	tS8 *ps8WriteBuffer;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	
	tU32 u32BlockSize = 0;
	tU32 u32BytesToRead = 0;

	u32Slen = strlen( DataToWrite );
    u32BlockSize = 1024*32*u32Slen;	/* 256 KB */;
    u32BytesToRead = u32BlockSize *4; /* Number of bytes to read - 1 MB */

	OEDT_HelperPrintf( TR_LEVEL_USER_1, " Allocating Memory\n");
	ps8WriteBuffer =(tS8 *)OSAL_pvMemoryAllocate (u32BlockSize + 1);
	if(ps8WriteBuffer == OSAL_NULL)
	{ 
		return 1;
	}

    OSAL_pvMemorySet(ps8WriteBuffer, '\0',u32BlockSize); 	
	/* Write 256 KB of data to a buffer */
 
   	for( loop_cnt = 0;loop_cnt < 1024*32 ;loop_cnt += u32Slen)	//@prc4kor
	{
		(tVoid)OSAL_szStringNConcat(ps8WriteBuffer,DataToWrite,u32Slen);
	}
	OEDT_HelperPrintf( TR_LEVEL_USER_1, " Buffer is filled\n");
   	if( (hFile = OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 2;
	}		
	else
	{
	   		for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
			{
				s32BytesWritten = OSAL_s32IOWrite (
	                               hFile, ps8WriteBuffer,(tU32) u32BlockSize);
				if ( s32BytesWritten == OSAL_ERROR )
				{
					u32Ret = 3;
					break;
				}			
			}
		if(OSAL_s32IOClose ( hFile )==OSAL_ERROR)
		{
			u32Ret += 5;
		}
	
		hFile = OSAL_IOOpen ( szFilePath,enAccess );
		if( hFile == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		else
		{
			ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);			
			s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,
	                            (tU32) u32BytesToRead );
			if ( (s32BytesRead == OSAL_ERROR) && (s32BytesRead != (tS32)u32BytesToRead) )
			{
				u32Ret += 100;	
			}
			if( ps8ReadBuffer != NULL)
			{ 
				OSAL_vMemoryFree( ps8ReadBuffer );
				ps8ReadBuffer = NULL;
			}
			if ( OSAL_s32IOClose ( hFile )!=OSAL_ERROR )
			 {
				OSAL_s32IORemove ( szFilePath );
			 }
			 else
			 {
				u32Ret += 1000;
			 }
		}
	}
	
	/* free memory */
	OSAL_vMemoryFree( ps8WriteBuffer );	
	ps8WriteBuffer = NULL;		

	return u32Ret;	

}
/*****************************************************************************
* FUNCTION:		u32RAMDiskFileWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_049
* DESCRIPTION:  Write data to an existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Logic is Modified - Venkateswara.N - 04 sep, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/


tU32 u32RAMDiskFileWrite(void)
{	
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;  
   	tCS8 DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Writing data";
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/WriteFile.txt";
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;	

	if( (hFile = OSAL_IOCreate (szFilePath, enAccess) )== OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{				
			s32BytesWritten = OSAL_s32IOWrite (
	                                hFile, DataToWrite,
												 (tU32) u8BytesToWrite
										    );
			if ( s32BytesWritten == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			if(OSAL_s32IOClose ( hFile )== OSAL_ERROR)
			{
				u32Ret += 20;
			}
			else if( OSAL_s32IORemove ( szFilePath ) )
			{
				u32Ret += 30;
			}
							
		}
	return u32Ret;	

}	



/*****************************************************************************
* FUNCTION:		u32RAMDiskFileWriteAccessCheck()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_050
* DESCRIPTION:  Try to write to a file which has no access rights / permissions   
*				(should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskFileWriteAccessCheck(void)
{	
	# define WRACCFILE OSAL_C_STRING_DEVICE_RAMDISK"/Accesschk.txt"
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;  
   	tCS8 DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Writing data";
	tU32 u32Ret = 0;
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	# if 0
	tString szFilePath;
	char Path[]  = OSAL_C_STRING_DEVICE_RAMDISK"/Accesschk.txt";
   	tU32 Len = strlen(OSAL_C_STRING_DEVICE_RAMDISK"/Accesschk.txt")+1;
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	szFilePath = (tString)OSAL_pvMemoryAllocate( OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1);
	OSAL_pvMemoryCopy( (tString)szFilePath ,Path,Len);
	# endif
	if( (hFile =OSAL_IOCreate (WRACCFILE, enAccess)) == OSAL_ERROR )
	{
		u32Ret = 1;
	}		
	else
	{

		    s32BytesWritten = OSAL_s32IOWrite (
                                hFile, DataToWrite,
											 (tU32) u8BytesToWrite
									    );
			if ( s32BytesWritten != OSAL_ERROR )
			{
				u32Ret = 3;
			}
			if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
			{
				u32Ret += 10;
			}

			else if ( OSAL_s32IORemove (WRACCFILE ) == OSAL_ERROR )
			{
				u32Ret += 100;
			}
			# if 0
			OSAL_vMemoryFree( szFilePath);
			if(szFilePath != NULL);
			{
				u32Ret += 1000;
			}
			# endif
	}
	    
	return u32Ret;	

}	

/*****************************************************************************
* FUNCTION:		u32RAMDiskLargeFileWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_051
* DESCRIPTION:  Try for large data like greater than 1 MB
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Pradeep Chand C (RBIN/EDI)	4 Apr, 2007
******************************************************************************/

tU32 u32RAMDiskLargeFileWrite(void)
{	
	OSAL_tIODescriptor hFile ;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
   	tChar DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Writedat";
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile2.txt";
	tU32 u32Slen = 0;
   	tS8 *ps8WriteBuffer = NULL;
	tU32 loop_cnt =0;
    tS32 s32BytesWritten = 0;	
	tU32 u32BlockSize = 0;

	u32Slen = strlen( DataToWrite );
    u32BlockSize = 1024*32*u32Slen; /* 256 KB */
  
	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BlockSize + 1);
	if(ps8WriteBuffer == OSAL_NULL)
	{ 
		return 1;
	}
	
	OSAL_pvMemorySet(ps8WriteBuffer, '\0',u32BlockSize); 
	/* Write 256 KB of data to a buffer */
	
	for( loop_cnt = 0;loop_cnt < 1024*32 ;loop_cnt += u32Slen)	//@prc4kor
	{
		(tVoid)OSAL_szStringNConcat(ps8WriteBuffer,DataToWrite,u32Slen);
	}
	
	if( (hFile =OSAL_IOCreate (szFilePath, enAccess))  == OSAL_ERROR )
	{
		u32Ret = 2;
	}		
	else
	{
	   		for( loop_cnt = 0;loop_cnt <= 3;loop_cnt++ )
			{
				s32BytesWritten = OSAL_s32IOWrite (
	                                hFile, ps8WriteBuffer,
												 (tU32) u32BlockSize
										    );
				if ( (s32BytesWritten == OSAL_ERROR) && (s32BytesWritten!=(tS32)u32BlockSize) )
				{
					u32Ret = 3;
					break;
				}
			}
    	  if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		  {
		  	u32Ret += 10;			
		  }
		  else
		  {
			if ( OSAL_s32IORemove ( szFilePath )==OSAL_ERROR )
			{
			  u32Ret += 20;
			}	
		  }
	}

	/* free memory */
	OSAL_vMemoryFree( ps8WriteBuffer );	
	ps8WriteBuffer = NULL;		

	return u32Ret;	

}	


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileWriteSubDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_052
* DESCRIPTION:  Write to file in the different sub-directories
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskFileWriteSubDir(void)
{	
	OSAL_tIODescriptor hDir,subDir1=0,subDir2 = 0,hFile = 0;
    OSAL_trIOCtrlDirent rDirent1,rDirent2;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    tU8 u8BytesToWrite = 20; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
	tChar szDirPath [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/";
	tChar szSubDirPath1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUB59";
	tChar szSubDirPath2 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUB59/SUBTWO";
	tChar szSubdirName1 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUB59";
	tChar szSubdirName2 [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = "/SUBTWO";

	tChar szSubDirFileName [OEDT_RAMDISK_MAX_TOTAL_DIR_PATH_LEN] = OSAL_C_STRING_DEVICE_RAMDISK"/SUB59/SUBTWO/Myfile.txt";
	tCS8 DataToWrite[30] = "Writing data to Sub-dir";


	hDir = OSAL_IOOpen ( szDirPath,enAccess );
	if( hDir ==OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{

		(tVoid)OSAL_szStringCopy ( rDirent1.s8Name,szSubdirName1 );	  
	   
	    if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent1) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
		    subDir1 = OSAL_IOOpen ( szSubDirPath1,enAccess ); 
			if( subDir1 != OSAL_ERROR )
			{
			   (tVoid)OSAL_szStringCopy ( rDirent2.s8Name,szSubdirName2 );
			   if ( OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
										  (tS32)&rDirent2) == OSAL_ERROR )
		        {
			       u32Ret = 3;
		        }
				else
				{
				  subDir2 = OSAL_IOOpen ( szSubDirPath2,enAccess ); 
					  if( subDir2 != OSAL_ERROR )
					  {
				  
						 if ( (hFile = OSAL_IOCreate ( szSubDirFileName,enAccess )) != OSAL_ERROR )
						 {							
						  	s32BytesWritten = OSAL_s32IOWrite (
							                           hFile, DataToWrite,
															(tU32) u8BytesToWrite
																    );
						  	if ( s32BytesWritten == OSAL_ERROR )
						  	{
						   		u32Ret = 4;
						  	}
							OSAL_s32IOClose ( hFile );
							OSAL_s32IORemove ( szSubDirFileName );
						 }
						 else
						 {
							u32Ret = 7;
						 }
									
					  }
					  OSAL_s32IOClose ( subDir2 );

			   			if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent2) == OSAL_ERROR )
							{
								u32Ret += 1000;
							}

			    }
			}
			OSAL_s32IOClose ( subDir1 );

			if ( OSAL_s32IOControl ( hDir,
											 OSAL_C_S32_IOCTRL_FIORMDIR,
											 (tS32)&rDirent1) == OSAL_ERROR )
					{
						u32Ret += 10000;
					}
		}
		
		OSAL_s32IOClose ( hDir );

	}
	return u32Ret;   
} 


/*****************************************************************************
* FUNCTION:		u32RAMDiskFileWritePredefined()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_053
* DESCRIPTION:  Write small data bytes like less than 100 Bytes and large data 
*				like greater than 1 MB of stored data to the file.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskFileWritePredefined(void)
{	
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;  
	tU32 u32Ret = 0;
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/WritePredef.txt";
    tU32 u32BytesToWrite = 1024; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
   	

	if( (hFile =OSAL_IOCreate (szFilePath, enAccess)) == OSAL_ERROR )
	{		
		u32Ret = 1;
	}	
	else
	{
		s32BytesWritten = OSAL_s32IOWrite (
	                                hFile, as8RAMdiskMesgToWrite,
												  u32BytesToWrite
										    );
			if ( s32BytesWritten == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			if( OSAL_s32IOClose ( hFile )== OSAL_ERROR )
			{
				u32Ret += 10;
			}

		
		else if( OSAL_s32IORemove ( szFilePath )==OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}		 
	return u32Ret;	

}	


/*****************************************************************************
* FUNCTION:		u32RAMDiskGetPosFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_054
* DESCRIPTION:  Get the position from BOF for an existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskGetPosFrmBOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile ;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 10;   	
	tU32 u32FilePos = 0;   
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	/*Open the commmon file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check for success of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Seek the file to desired position*/
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{

			/*Get position of file pointer from beginning of common file*/
			if( OSAL_s32IOControl (
            	                  hFile,
                	              OSAL_C_S32_IOCTRL_FIOWHERE,
                    	          (tS32)&u32FilePos
                        	   ) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{
				vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes between Current Position and BOF : %d"
	 																		,u32FilePos );	
			}
		}
		/*Close the common file*/	
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}			    
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return error code*/
    return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskGetPosFrmBOFNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_055
* DESCRIPTION:  Get the position from BOF for a non-existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskGetPosFrmBOFNonExstng(void)
{	
	OSAL_tIODescriptor hFile = -1;
	tU32 u32Ret = 0;  
   	tU32 u32FilePos = 0;   		
	if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOWHERE,
                              (tS32)&u32FilePos
                           ) != OSAL_ERROR )
	{
		u32Ret = 1;
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskGetPosFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_056
* DESCRIPTION:  Get the position from EOF for existing file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskGetPosFrmEOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
  	tU32 u32FilePos = 0;
  	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Call the control function*/
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&u32FilePos
                           ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes between Current Position and EOF : %d"
	 																		,u32FilePos );
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskGetPosFrmEOFNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_057
* DESCRIPTION:  Get the position from EOF for a non-existing file
*				(invalid file handle)  (should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskGetPosFrmEOFNonExstng(void)
{	
    OSAL_tIODescriptor hFile = -1;
	tU32 u32Ret = 0;  
   	tU32 u32FilePos = 0;   		
	if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&u32FilePos
                           ) != OSAL_ERROR )
	{
		u32Ret = 1;
	}

  return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskSetPosDiffOff()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_058
* DESCRIPTION:  Set file position for different offsets from different positions
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskSetPosDiffOff(void)
{	
	/*Declaration*/
	OSAL_tIODescriptor hFile,hFile1,hFile2;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
      	
   	tS32 s32PosToSet = 0;
   	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/* With +ve Offset */

		s32PosToSet = 10;
		/*Seek to positive offset*/
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) == OSAL_ERROR )
		{
			u32Ret = 10;
		}
		
		/* With -ve Offset */
		s32PosToSet = -30;
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) != OSAL_ERROR )
		{
			u32Ret += 100;
		}

		
	   if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
	   {
			u32Ret += 1000;
	   }
	}
	
	/*Open the common file*/
	hFile1 = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open is successful*/
	if ( hFile1 != OSAL_ERROR )
	{
	
		/* With -ve Offset from BOF */
		s32PosToSet = -7;
		/*Seek through the common file with neg offset from file beginning*/
		if( OSAL_s32IOControl (
	                              hFile1,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet
	                           ) != OSAL_ERROR )
		{
			u32Ret += 1000;
		}
		
  		if(OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR)
		{
			u32Ret += 1212;
		}
	}
	else
	{
		u32Ret += 1117;
	}

	/*Open the common file*/
	hFile2 = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open is successful*/
	if( hFile2 != OSAL_ERROR )
	{

		/* Set position at EOF */
		if( OSAL_s32IOControl (
                              hFile2,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&s32PosToSet
                           ) != OSAL_ERROR )

		{
			/*Seek within the common file*/
			if( OSAL_s32IOControl (
	                              hFile2,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet
	                           ) == OSAL_ERROR )
			{
				u32Ret += 10000;
			}
			else
			{

				/* Try Setting position beyond EOF */
				s32PosToSet = 10;
				if( OSAL_s32IOControl (
	             	                 hFile2,
	                	              OSAL_C_S32_IOCTRL_FIOSEEK,
	                    	          s32PosToSet
	                        	   ) == OSAL_ERROR )
				{
					u32Ret += 100000;
				}
			}
		}
		
	  	if(OSAL_s32IOClose ( hFile2 ) == OSAL_ERROR)
		{
			u32Ret += 4567;
		}
	}
	else
	{
		u32Ret += 3978;
	}
	/*Remove the common file*/	
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskSetPosNonExstng()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_059
* DESCRIPTION:  Set position for a non-existing file(invalid file handle)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*
******************************************************************************/

tU32 u32RAMDiskSetPosNonExstng(void)
{	
    OSAL_tIODescriptor hFile = -1;
	tU32 u32Ret = 0;  
   	tS32 s32PosToSet = 10;
   	   		
	if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) != OSAL_ERROR )
	{
		u32Ret = 1;
	}

  return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32RAMDiskReadFrmOffFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_060
* DESCRIPTION:  Read file with few offsets from BOF
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) 21 Sept, 2007
******************************************************************************/

tU32 u32RAMDiskReadFrmOffFrmBOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/		
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check for success of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/* Set position at an Offset from BOF */
		s32PosToSet = 5;
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			/*Allocate memory*/
			ps8ReadBuffer = 
							(tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
			/*Check for success of memory allocation*/
			if ( ps8ReadBuffer == NULL )
			{
				u32Ret = 3;
			}
			else
			{
				/*Read from file*/
				s32BytesRead = OSAL_s32IORead (
	                                hFile,
	                                ps8ReadBuffer,
	                                (tU32) u32BytesToRead
	                             );
				/*Check for success of read*/
				if ( s32BytesRead == OSAL_ERROR )
				{
					u32Ret = 4;	
	    		}
				else
				{
					vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes read: %d ",s32BytesRead);
				}
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Free the allocated memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}	
	/*Return error code*/
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskReadBeyondEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_061
* DESCRIPTION:  Try to read more no. of bytes than the file size 
*				(beyond EOF),(should fail)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 21, 2007
******************************************************************************/

tU32 u32RAMDiskReadBeyondEOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;		
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
   	tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	tS32 s32FileSz = 0;
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		
	   	/*Go to end of file*/
	   	if ( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&s32FileSz
                           ) != OSAL_ERROR )
		{

			 s32PosToSet = s32FileSz; 
			/* Set position at EOF */
			if( OSAL_s32IOControl (
            	                  hFile,
                	              OSAL_C_S32_IOCTRL_FIOSEEK,
                    	          s32PosToSet
                        	   ) == OSAL_ERROR )
			{
				u32Ret = 2;
			}
			else
			{

				/*Allocate memory*/
				ps8ReadBuffer = 
								(tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
				/*Check the status of memory allocation*/
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret = 3;
				}
				else
				{
					/*Read beyond EOF*/
					s32BytesRead = OSAL_s32IORead (
		                                hFile,
		                                ps8ReadBuffer,
		                                (tU32) u32BytesToRead
		                             );
					/*Check status of read*/
					if ( s32BytesRead > 0 )
					{
						u32Ret = 4;	
		    		}
				}
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Deallocate memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return error code*/
   	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskReadFrmOffFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_062
* DESCRIPTION:  Read file with few offsets from EOF
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskReadFrmOffFrmEOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;		
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 10;
	tU32 u32FilePos = 0;
   	tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	tS32 s32offFrmEOF = 10;  /* This is the offset from EOF */

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check for the status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		
		/*Store end of file*/
		if(OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&u32FilePos
                           ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}

		else
		{
			s32PosToSet = (tS32)u32FilePos - (tS32)s32offFrmEOF;
			/*Seek at an offset from end of file*/
			if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              (tS32)s32PosToSet
                           ) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
	   
			else
			{
				/*Allocate memory*/
				ps8ReadBuffer = 
								(tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
				/*Check status of allocation*/
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret = 4;
				}
				else
				{
					/*Read from the common file at an offset from EOF*/
					s32BytesRead = OSAL_s32IORead (
		                                hFile,
		                                ps8ReadBuffer,
		                                (tU32) u32BytesToRead
		                             );
					/*Check status of read*/
					if ( s32BytesRead == OSAL_ERROR )
					{
						u32Ret = 5;	
		    		}
					else
					{
						vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t Bytes read: %d ",s32BytesRead);
					}
				}
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Dealllocate memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
   	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskReadFrmEvryNFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_063
* DESCRIPTION:  Reads from every nth position from BOF
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept , 2007
				Updated by Balaji.V(RBEI/ECF5) for lint removal on 6 June,2015
******************************************************************************/

tU32 u32RAMDiskReadFrmEvryNFrmBOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 5; /* This is the number of bytes that will be read */
   	tS32 s32BytesRead = 0;
	tS32 s32OffsetFrmStart = 0;
	tS32 s32FileSz = 0;
	tS32 u32FilePos = 0;
	tS32 *temp = &s32FileSz; /* Temporary variable is taken for avoid lint warning */

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Store the end of file*/
		if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
      	(tS32)&s32FileSz)== OSAL_ERROR )
		{
			u32Ret = 2;
		}
		/* This is the value of 'N' */
		s32OffsetFrmStart = 4;   		
		/* Check if current file-position is within file boundaries */
		while( (*temp > u32FilePos) && (*temp > s32OffsetFrmStart) )  
		{

			/*Seek through the file*/
			if( OSAL_s32IOControl (
	                              hFile,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              u32FilePos
	                           ) == OSAL_ERROR )
			{
				u32Ret = 2;
				break;
			}
			else
			{

				/*Allocate memory*/
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
				/*Check if allocation successful*/
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret = 3;
					break;
				}
				else
				{
					/*Read from common file*/
					s32BytesRead = OSAL_s32IORead (
		                                hFile,
		                                ps8ReadBuffer,
		                                (tU32) u32BytesToRead
		                             );
					/*Check status of read*/
					if ( s32BytesRead == OSAL_ERROR )
					{
						u32Ret = 4;
						break;	
		    		}
				}
			}
			/*Skip with offset*/
			u32FilePos += s32OffsetFrmStart;
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Free memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskReadFrmEvryNFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_064
* DESCRIPTION:  Reads from every nth position from EOF
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
                Updated by Balaji.V(RBEI/ECF5) for lint removal on 6 June,2015
******************************************************************************/

tU32 u32RAMDiskReadFrmEvryNFrmEOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 4;/* Reads 4 bytes in every cycle */
   	tS32 s32BytesRead = 0;
	tS32 s32PosToSet = 0;
	tS32 s32OffSet = 5; /* This is the offset from EOF */
	tU32 u32FilePos = 0;
	tU32 u32FileSz = 0;
	tU32 *temp = &u32FileSz; /*Temporary variable is taken for avoid lint warning*/

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*Store end of file*/
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              (tS32)&u32FileSz
                           )== OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{

			s32PosToSet = (tS32)*temp - (tS32)s32OffSet;
			while( OSAL_s32IOControl (
		                          hFile,
		                          OSAL_C_S32_IOCTRL_FIOWHERE,
		                          (tS32)&u32FilePos
		                       ) != OSAL_ERROR && (s32PosToSet) > 0 )
				
			{					
				
			   	/*Seek through the common file*/
				if( OSAL_s32IOControl (
		                              hFile,
		                              OSAL_C_S32_IOCTRL_FIOSEEK,
		                              s32PosToSet
		                           ) == OSAL_ERROR )
				{
					u32Ret = 3;
					break;
				}
				else
				{
					/*Allocate memory*/
					ps8ReadBuffer = 
								(tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead+1 );
					/*Check if memory allocation successful*/
					if ( ps8ReadBuffer == NULL )
					{
						u32Ret = 4;
						break;
					}
					else
					{
						/*Read from common file*/
						s32BytesRead = OSAL_s32IORead (
			                                hFile,
			                                ps8ReadBuffer,
			                                (tU32) u32BytesToRead
			          		                );
						/*Check status of read*/
						if ( s32BytesRead == OSAL_ERROR )
						{
							u32Ret = 5;	
							break;
			    		}
					}
				}
				s32PosToSet -= s32OffSet; 
			}
		}
		/*Cloae the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Deallocate memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskWriteFrmOff()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_065
* DESCRIPTION:  Write data at an offset from BOF.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 21, 2007
******************************************************************************/
tU32 u32RAMDiskWriteFrmOff(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
   	tChar DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "New data written";
   	tS8 *ps8WriteBuffer = NULL;
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
  	tS32 s32PosToSet = 0;
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/	
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/* Set position at an Offset from BOF */
		s32PosToSet = 5;
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIOSEEK,
                              s32PosToSet
                           ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{

			/*Allocate memory*/
			ps8WriteBuffer = 
						(tS8 *) OSAL_pvMemoryAllocate ( u8BytesToWrite +1 );
			/*Check status of allocation*/
			if ( ps8WriteBuffer == NULL )
			{
				u32Ret = 3;
			}
			else
			{
				
				/*Copy bytes into buffer*/
				(tVoid) OSAL_szStringNCopy( ps8WriteBuffer,DataToWrite,u8BytesToWrite );

				/*Write to common file*/
				s32BytesWritten = OSAL_s32IOWrite (
	                                hFile,
	                                ps8WriteBuffer,
	                                (tS32) u8BytesToWrite
	                             );
				/*Check status of write*/
				if ( s32BytesWritten == OSAL_ERROR )
				{
					u32Ret = 5;	
				}
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Free memory*/
	if( ps8WriteBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8WriteBuffer );
		ps8WriteBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
	return COMMON_FILE_ERROR;
	}
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskWriteFrmEOF() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_066
* DESCRIPTION:  Try writing data,starting at the EOF
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 21,2007
******************************************************************************/
tU32 u32RAMDiskWriteFrmEOF(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;   	
   	tChar DataToWrite[OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = "Appending more data";
   	tS8 *ps8WriteBuffer = NULL;
    tU8 u8BytesToWrite = 25; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;
  	tS32 s32PosToSet = 0;
	tS32 s32FileSz = 0;
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check the status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/* Set position at EOF */
		if( OSAL_s32IOControl (
                              hFile,
                              OSAL_C_S32_IOCTRL_FIONREAD,
                              	(tS32)&s32FileSz
                           )== OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			/*Seek to end of file*/
			s32PosToSet = s32FileSz;
			if( OSAL_s32IOControl (
	                              hFile,
	                              OSAL_C_S32_IOCTRL_FIOSEEK,
	                              s32PosToSet
	                           ) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{

				/*Allocate memory*/
				ps8WriteBuffer = 
								(tS8 *) OSAL_pvMemoryAllocate ( u8BytesToWrite +1 );
				/*Check if allocation successful*/
				if ( ps8WriteBuffer == NULL )
				{
					u32Ret = 4;
				}
				else
				{
					/*Copy data to write inot buffer*/
					(tVoid) OSAL_szStringCopy ( ps8WriteBuffer,DataToWrite );

					/*Write data to file*/
					s32BytesWritten = OSAL_s32IOWrite (
		                                hFile,
		                                ps8WriteBuffer,
		                                (tS32) u8BytesToWrite
		                             );
					/*Check status of write*/
					if ( s32BytesWritten == OSAL_ERROR )
					{
						u32Ret = 6;	
					}
				}
			}
		}
		/*Close common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Deallocate memory*/
	if( ps8WriteBuffer != NULL)
	{
		OSAL_vMemoryFree( ps8WriteBuffer );
		ps8WriteBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskStringSearch()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_067
* DESCRIPTION:  Searches for a string in the file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskStringSearch(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile ;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 StrngCnt = 0;
	tU32 u32Ret = 0;   	
	tChar szSearchString [5] = "dat";
	tU32 u32StringLen = 3;
   	tS8 *ps8ReadBuffer = NULL;     
   	tU32 u32BytesToRead = 3; /* this must be equal to length of the search string */
   	tS32 s32BytesRead = 0;
	tU32 u32PosToSet = 0;
	tU32 u32FilePos = 0,s32FileSz = 0;
	
	/*Create common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/		
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check status of open*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{  	
		/*End of file*/
		if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,
      	(tS32)&s32FileSz)== OSAL_ERROR )
		{
			u32Ret = 2;
		}

		while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )  
		/*Check if current file-position is within file boundaries*/
		{

			if( s32FileSz < u32PosToSet)
				break;
			/*Seek through the common file*/
			if( OSAL_s32IOControl (
                              		hFile,
                              		OSAL_C_S32_IOCTRL_FIOSEEK,
                              		(tS32)u32PosToSet
                           		  ) == OSAL_ERROR )
			{
				u32Ret = 2;
				break;
			}
			else
			{
				/*Allocate memory dynamically*/
				ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1 );
				/*Check if memory allocation successful*/
				if ( ps8ReadBuffer == NULL )
				{
					u32Ret = 3;
					break;
				}
				else
				{
					/*Read from common file*/
					s32BytesRead = OSAL_s32IORead (
	                                hFile,
	                                ps8ReadBuffer,
	                                (tU32) u32BytesToRead
	                             );
					/*Check status of read*/
					if ( s32BytesRead == OSAL_ERROR )
					{
						u32Ret = 4;
						break;	
	    			}
					else
					{
						/*Do a string comparison*/
						if ( OSAL_s32StringNCompare ( 
							                          ps8ReadBuffer,
															  szSearchString,
															  u32StringLen
														  )
							== 0 )
					   	{
							/*Increment occurences*/
							StrngCnt++;
						}
					}											
				}
			}
			u32PosToSet++;
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Check for error code*/
   	if ( u32Ret == 0)
	{
		if( StrngCnt == 0 )
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t String not found" );
		}
		else
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t String found %u times",StrngCnt);
		}
	
	}
	/*Deallocate memory*/
	if( ps8ReadBuffer != NULL)
	{ 
		OSAL_vMemoryFree( ps8ReadBuffer );
		ps8ReadBuffer = NULL;
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RAMDiskGetFileCRC()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_068
* DESCRIPTION:  Get CRC value(Test for different file formats)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskGetFileCRC(void)
{	
	/*Declarations*/
    OSAL_tIODescriptor hFile;    
	tU32 u32Ret =0;
    tS32 s32FilePosFrmEnd = 0;
	tS32 s32FilePosFrmStart = 0;
    tS8 as8ReadBuffer [2];
	OSAL_tenAccess enAccess;
	enAccess = OSAL_EN_READONLY;

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
	hFile = OSAL_IOOpen ( COMNFILE,enAccess );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*Get position of file pointer currently*/
		if( OSAL_s32IOControl (
            	                  hFile,
                	              OSAL_C_S32_IOCTRL_FIOWHERE,
                    	          (tS32)&s32FilePosFrmStart
                        	   ) == OSAL_ERROR )
		{
			u32Ret = 2;
	   	}
	    else
   		{
			/*Go to end of file*/
			if ( OSAL_s32IOControl (
										hFile,
										OSAL_C_S32_IOCTRL_FIONREAD,
										(tS32)&s32FilePosFrmEnd
											) == OSAL_ERROR )
			{
				u32Ret = 3;
			}
			else
			{
				/*Seek through file*/
				if ( OSAL_s32IOControl ( hFile,
										OSAL_C_S32_IOCTRL_FIOSEEK,
										s32FilePosFrmStart + 
												s32FilePosFrmEnd - 2
												  ) == OSAL_ERROR )
				{
					u32Ret = 4;	
				}
				else
				{
					/*Read from file*/
					if ( OSAL_s32IORead (
											hFile,
											as8ReadBuffer,2
											) == OSAL_ERROR )
					{
						u32Ret = 5;
					}
					else
					{
						vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								"\n\t data read: %s ",as8ReadBuffer);
					}
				}
			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskReadAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_069
* DESCRIPTION:  Read asynchronously from the file
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated By Rakesh Dhanya (RBIN/ECM1) on 6 Feb, 2007.
*				Updated by Pradeep Chand C(RBIN/EDI) on 4 Apr, 2007.
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007.
******************************************************************************/
tU32 u32RAMDiskReadAsync(void)
{	
	/*Declarations*/
	OSAL_tIODescriptor hFile;
    OSAL_trAsyncControl rAsyncCtrl;   	
    tS8 *ps8ReadBuffer = NULL;
    tU32 u32Ret = 0;               
    tU32 u32BytesToRead = 10;
    tS32 s32BytesRead = 0;
    tS32 s32Result = 0;
   	tU32 u32OsalRetVal = 0;
	tChar szSemName []   = "SemAsyncRAMDisk";
   	tS32 s32RetVal       = OSAL_ERROR;
	
	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Open the common file*/
   	hFile = OSAL_IOOpen ( COMNFILE,OSAL_EN_READWRITE );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*Allocate memory*/
		ps8ReadBuffer = 
               (tS8 *)
               OSAL_pvMemoryAllocate (
                                     u32BytesToRead+1
                                     );
		/*Check if memory allocated successfully*/
   		if ( ps8ReadBuffer == NULL )
	   	{
	      	u32Ret = 2;
	   	}
		else
		{

	   		/*
	      		Create semaphore for event signalling used by async callback.
	   		*/
			s32RetVal = OSAL_s32SemaphoreCreate (
	                                          szSemName,
	                                          &hSemAsyncRAMDisk,
	                                          0
	                                       );

			/*Check if semaphore is already existent*/
		   	if(s32RetVal == OSAL_ERROR)
		    {
				u32Ret = 3;      
		    }
			else
			{
				/*Fill the asynchronous control structure*/
				rAsyncCtrl.id = hFile;
			    rAsyncCtrl.s32Offset = 0;
			    rAsyncCtrl.pvBuffer = ps8ReadBuffer;
			    rAsyncCtrl.u32Length = u32BytesToRead;
			    rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTRAMDiskAsyncCallback;
			    rAsyncCtrl.pvArg = &rAsyncCtrl;

			   /* 
			      If IOReadAsync returns OSAL_ERROR, Callback function is not called
			      so semaphore is not waited upon.
			   */
				if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			    {
			    	u32Ret = 4;     
				}
    			else
    			{
				      /*
				         Wait for 5 seconds for asynchronous read to get over.
				         If semaphore not posted by the callback within 5 seconds,
				         then cancel the asynchronous read.
				      */
					s32Result = OSAL_s32SemaphoreWait (
                                           hSemAsyncRAMDisk,
                                           5000
                                          );

      				if (
            				( s32Result != OSAL_OK )
            				&&
            				( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
           				)
      				{
         				s32Result = OSAL_s32IOCancelAsync (
                                              rAsyncCtrl.id,
                                              &rAsyncCtrl
                                          	 );
						u32Ret =  5;
        			
        			}
      				else
      				{

         				u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );
         				
         				if (!(u32OsalRetVal == OSAL_E_INPROGRESS||
               				u32OsalRetVal == OSAL_E_CANCELED))
						{
							s32BytesRead = OSAL_s32IOReturnAsync(&rAsyncCtrl);
							
							if( s32BytesRead != (tS32)u32BytesToRead)
            				{
            				u32Ret += 8;
            				}     	
						}
         	    	}
		    	}
			}		    	      
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}

	/* Free the memory allocated */
   	if(ps8ReadBuffer != NULL)
	{
		OSAL_vMemoryFree (ps8ReadBuffer);
		ps8ReadBuffer = NULL;
	}

	OSAL_s32SemaphoreClose ( hSemAsyncRAMDisk );
   	OSAL_s32SemaphoreDelete( szSemName );

	/*Remove common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
	/*Return error code*/
   	return u32Ret;
   
}

/*****************************************************************************
* FUNCTION:		u32RAMDiskLargeReadAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_070
* DESCRIPTION:  Read large data,asynchronously
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated By Rakesh Dhanya (RBIN/ECM1) on 6 Feb, 2007.
*				Updated by Pradeep Chand C(RBIN/EDI) on 4 Apr, 2007.
				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 21,2007
******************************************************************************/


tU32 u32RAMDiskLargeReadAsync(void)
{	
   	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile.txt";
  	OSAL_tIODescriptor hFile;
    OSAL_trAsyncControl rAsyncCtrl;  
    tS8 *ps8ReadBuffer = NULL;
    tU32 u32Ret = 0, u32LoopCount = 0;               
    tU32 u32BytesToRead = HUGE_VALUE;
    tS32 s32BytesRead = 0;
    tS32 s32Result = 0;
   	tU32 u32OsalRetVal = 0;
	tChar szSemName []   = "SemAsyncRAMDisk";
   	tS32 s32RetVal       = OSAL_ERROR;
	tCS8 sWriteData[ ] = "Datatowrite";
	tU32 u32WriteDataLen = strlen((tCString)sWriteData);
	
   	hFile = OSAL_IOCreate ( szFilePath,OSAL_EN_READWRITE );
	if( hFile == (OSAL_tIODescriptor)OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Writing data into the Largefile.txt*/	//@prc4kor
		 for(u32LoopCount = 0; u32LoopCount < HUGE_VALUE; u32LoopCount++)
		 {
			OSAL_s32IOWrite(hFile, sWriteData, u32WriteDataLen);
		 }
		/*End of write data*/

		ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate(u32BytesToRead);
   		if ( ps8ReadBuffer == NULL )
	   	{
	      	u32Ret = 2;
	   	}
		else
		{
	   		/*
	      		Create semaphore for event signalling used by async callback.
	   		*/
			s32RetVal = OSAL_s32SemaphoreCreate (
	                                          szSemName,
	                                          &hSemAsyncRAMDisk,
	                                          0
	                                       );

		   	if(s32RetVal == OSAL_ERROR)
		    {
				u32Ret = 3;      
		    }
			else
			{

				rAsyncCtrl.id = hFile;
			    rAsyncCtrl.s32Offset = 0;
			    rAsyncCtrl.pvBuffer = ps8ReadBuffer;
			    rAsyncCtrl.u32Length = u32BytesToRead;
			    rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTRAMDiskAsyncCallback;
			    rAsyncCtrl.pvArg = &rAsyncCtrl;

			   /* 
			      If IOReadAsync returns OSAL_ERROR, Callback function is not called
			      so semaphore is not waited upon.
			   */
				if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			    {
			    	u32Ret = 4;     
				}
    			else
    			{
				      /*
				         Wait for 50 seconds for asynchronous read to get over.
				         If semaphore not posted by the callback within 5 seconds,
				         then cancel the asynchronous read.
				      */
					s32Result = OSAL_s32SemaphoreWait (
                                           hSemAsyncRAMDisk,
                                           50000
                                          );

      				if (
            				( s32Result != OSAL_OK )
            				&&
            				( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
           				)
      				{
         				s32Result = OSAL_s32IOCancelAsync (
                                              rAsyncCtrl.id,
                                              &rAsyncCtrl
                                          	 );
        				u32Ret = 5;
        			}
      				else
      				{

         				u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );
         				if (!(u32OsalRetVal == OSAL_E_INPROGRESS
               				||u32OsalRetVal == OSAL_E_CANCELED))
         				{
            				s32BytesRead = OSAL_s32IOReturnAsync(&rAsyncCtrl);         				
            				if( s32BytesRead != (tS32)u32BytesToRead)
            				{
            				u32Ret += 8;
            				}     				
            			}
					}
				}
		    }      
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		else								//@prc4kor
		{
		  if(	OSAL_s32IORemove ( OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile.txt" ) == OSAL_ERROR )
		  {
			u32Ret += 20;
		  }
		}		
	}
	

	OSAL_s32SemaphoreClose ( hSemAsyncRAMDisk );
   	OSAL_s32SemaphoreDelete( szSemName );

	/*Free the memory allocated */
   	if(	ps8ReadBuffer != NULL)
	{
   		OSAL_vMemoryFree (ps8ReadBuffer);
		ps8ReadBuffer = NULL;
	}
   
   return u32Ret;
   
}

 


/*****************************************************************************
* FUNCTION:		u32RAMDiskWriteAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_071
* DESCRIPTION:  Write asynchronously to a file(Test for different file formats)
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				 Updated By Rakesh Dhanya (RBIN/ECM1) on 6 Feb, 2007.
*				Updated by Pradeep Chand C (RBIN/EDI) on 4 Apr, 2007.
				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
				Updated by Tinoy Mathews(RBIN/EDI3) on 21 Sept, 2007
******************************************************************************/
tU32 u32RAMDiskWriteAsync(void)
{
	/*Declarations*/
	OSAL_tIODescriptor hFile;
	OSAL_trAsyncControl rAsyncCtrl;  
	tU32 u32Ret = 0;   
   	                                                   
   	tU32 u32BytesToWrite = 10;
   	tS32 s32BytesWritten = 0;
   	tS32 s32Result;
   	tU32 u32OsalRetVal;
   	tS32 s32RetVal       = OSAL_ERROR;
	tChar szSemName []   = "SemAsyncWrRAMDisk";

	/*Create the common file*/
	if(u32RAMDiskCreateCommonFile())
	{
		return COMMON_FILE_ERROR;
	}

	/*Open the common file*/	
   	hFile = OSAL_IOOpen ( COMNFILE,OSAL_EN_READWRITE );
	/*Check if open successful*/
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
	   	/*
	      	Create semaphore for event signalling used by async callback.
	   	*/
	   	s32RetVal = OSAL_s32SemaphoreCreate (
	                                          szSemName,
	                                          &hSemAsyncWrRAMDisk,
	                                          0
	                                       );
		/*Check if semaphore already existing in the system*/
	   	if(s32RetVal == OSAL_ERROR)
	   	{
	    	u32Ret = 2;
	   	}
		else
		{
	
			/*Fill the asynchronous control structure*/
			rAsyncCtrl.s32Offset = 0;
			rAsyncCtrl.id = hFile;
			rAsyncCtrl.pvBuffer = as8RAMdiskMesgToWrite;
			rAsyncCtrl.u32Length = u32BytesToWrite;
			rAsyncCtrl.pCallBack = (OSAL_tpfCallback) vOEDTRAMDiskAsyncWrCallback;
			rAsyncCtrl.pvArg = &rAsyncCtrl;

				/* 
				If IOWriteAsync returns OSAL_ERROR, Callback function is not called
				so semaphore is not waited upon.
			*/
			if ( OSAL_s32IOWriteAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			{
			   u32Ret = 3;
			}
			else
			{
	         	/*
		            Wait for 5 seconds for asynchronous write to get over.
		            If semaphore not posted by the callback within 5 seconds,
		            then cancel the asynchronous write.
		         */
		        s32Result = OSAL_s32SemaphoreWait (
		                                              hSemAsyncWrRAMDisk,
		                                              5000
		                                           );

	         	if (
	               	( s32Result != OSAL_OK )
	               	&&
	               	(OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
	            	)
	         	{
	            	s32Result = OSAL_s32IOCancelAsync (
	                                                 	rAsyncCtrl.id,
	                                                 	&rAsyncCtrl
	                                              	);
	                u32Ret = 4;
	         	}
	         	else
	         	{

	            	u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );
	            	if (!(u32OsalRetVal == OSAL_E_INPROGRESS||
	                  	u32OsalRetVal == OSAL_E_CANCELED))
	            	{
	                	s32BytesWritten = OSAL_s32IOReturnAsync (&rAsyncCtrl);
						if(	s32BytesWritten != (tS32)u32BytesToWrite)
						{
						      u32Ret += 8;
						}
	            	}


				   /*Send a msg to the Console IO */
				   vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								 "Total number of bytes written : %d",
	 											s32BytesWritten);
				   	   
				}

			}
		}
		/*Close the common file*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
   	}
   
	OSAL_s32SemaphoreClose ( hSemAsyncWrRAMDisk );
   	OSAL_s32SemaphoreDelete( szSemName );   

	/*Remove the common file*/
	if(u32RAMDiskRemoveCommonFile())
	{
		return COMMON_FILE_ERROR;
	}
  	/*Return error code*/
  	return u32Ret;

} 

/*****************************************************************************
* FUNCTION:		u32RAMDiskLargeWriteAsync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RAMDisk_072
* DESCRIPTION:  Try writng large data(1MB), asynchronously
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
*				Updated By Rakesh Dhanya (RBIN/ECM1) on 6 Feb, 2007.
*				Updated by Pradeep Chand (RBIN/EDI) on 4 Apr, 2007.
				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007

******************************************************************************/

tU32 u32RAMDiskLargeWriteAsync(void)
{
/* This function is not currently implemented */	
 // tU32 u32Ret = 0;
  //return u32Ret;
    tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile1.txt";

    OSAL_tIODescriptor hFile;
	OSAL_trAsyncControl rAsyncCtrl;  
	tS8 *ps8WriteBuffer = NULL; 
  tU32 u32Ret = 0;
   	                                                   
   	tU32 u32BytesToWrite = MB_SIZE;	  //1 Mb
   	tS32 s32BytesWritten = 0;
   	tS32 s32Result = 0;
   	tU32 u32OsalRetVal = 0;
   	tS32 s32RetVal       = OSAL_ERROR;
	tChar szSemName []   = "SemAsyncLrgWrRAMDisk";

		
    hFile = OSAL_IOCreate ( szFilePath,OSAL_EN_READWRITE );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
	   	ps8WriteBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BytesToWrite+1);

   		if ( ps8WriteBuffer == NULL )
	   	{
	      	u32Ret = 2;
	   	}
		else
		{

            OSAL_pvMemorySet(ps8WriteBuffer,'a',u32BytesToWrite);
	   	/*
	      	Create semaphore for event signalling used by async callback.
	   	*/
	   	s32RetVal = OSAL_s32SemaphoreCreate (
	                                          szSemName,
	                                          &hSemAsyncWrRAMDisk,
	                                          0
	                                       );

	   	if(s32RetVal == OSAL_ERROR)
	   	{
	    	u32Ret = 2;
	   	}
		else
		{

			rAsyncCtrl.s32Offset = 0;
			rAsyncCtrl.id = hFile;
			rAsyncCtrl.pvBuffer = ps8WriteBuffer;
			rAsyncCtrl.u32Length = u32BytesToWrite;
			rAsyncCtrl.pCallBack = (OSAL_tpfCallback) vOEDTRAMDiskAsyncWrCallback;
			rAsyncCtrl.pvArg = &rAsyncCtrl;

				/* 
				If IOWriteAsync returns OSAL_ERROR, Callback function is not called
				so semaphore is not waited upon.
			*/
			if ( OSAL_s32IOWriteAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			{
			   u32Ret = 3;
			}
			else
			{
	         	/*
		            Wait for 50 seconds for asynchronous write to get over.
		            If semaphore not posted by the callback within 5 seconds,
		            then cancel the asynchronous write.
		         */
		        s32Result = OSAL_s32SemaphoreWait (
		                                              hSemAsyncWrRAMDisk,
		                                              50000
		                                           );

	         	if (
	               	( s32Result != OSAL_OK )
	               	&&
	               	(OSAL_u32ErrorCode() == OSAL_E_TIMEOUT )
	            	)
	         	{
	            	s32Result = OSAL_s32IOCancelAsync (
	                                                 	rAsyncCtrl.id,
	                                                 	&rAsyncCtrl
	                                              	);
	                u32Ret = 4;
	         	}
	         	else
	         	{

	            	u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );
	            		if (!(u32OsalRetVal == OSAL_E_INPROGRESS|| 
	            				u32OsalRetVal == OSAL_E_CANCELED))
	            	{
	                	s32BytesWritten = OSAL_s32IOReturnAsync (&rAsyncCtrl);
						
						if(	s32BytesWritten != (tS32)u32BytesToWrite)
						{
						      u32Ret += 8;
						}

	            	}

				   /*Send a msg to the Console IO */
				   vTtfisTrace(OSAL_C_TRACELEVEL1,
	 								 "Total number of bytes written : %d",
	 											s32BytesWritten);
				}
			}
		}

					}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		else
		{
		  if(	OSAL_s32IORemove ( OSAL_C_STRING_DEVICE_RAMDISK"/LargeFile1.txt" ) == OSAL_ERROR )
		  {
			u32Ret += 20;
		  }
		}	
   	}
   
	/* Free the memory allocated */
   	if(	ps8WriteBuffer != NULL)
	{
   		OSAL_vMemoryFree (ps8WriteBuffer);
		ps8WriteBuffer = NULL;
	}

	OSAL_s32SemaphoreClose ( hSemAsyncWrRAMDisk );
   	OSAL_s32SemaphoreDelete( szSemName );   
  	

  return u32Ret;

}



/* Following test functions are taken from the Paramount Product */
/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskMultipleHandles()
* AUTHOR: tnn
* CALLED: YES
* tests multiple handles to the same file
* TEST CASE:    TU_OEDT_RAMDisk_073
* HISTORY:		Modified by  Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
				Updated by Shilpa Bhat (EDI3/RBIN) 7 May, 2007
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskMultipleHandles(void)
{
  tU32 u32Ret = 0;
  OSAL_tIODescriptor hFd1;
  OSAL_tIODescriptor hFd2;

  hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile_82.txt", OSAL_EN_READONLY);
  hFd2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_RAMDISK"/testfile_82.txt", OSAL_EN_READONLY);
  if(hFd1 != OSAL_ERROR) 
  {
    OSAL_s32IOClose(hFd1);
  }
  else
  {
	u32Ret = 2;
  }
  
  if(hFd2 != OSAL_ERROR) 
  {
    OSAL_s32IOClose(hFd2);
  }
  else
  {
	u32Ret += 20;
  }
   
   if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile_82.txt") == OSAL_ERROR)
  {
    u32Ret += 100;
  }
  
  return u32Ret;

 
}

/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskMultipleFileCreation ()
* AUTHOR: tnn
* CALLED: YES
* tests multiple consecutive file creations
* TEST CASE:    TU_OEDT_RAMDisk_074
* HISTORY:		Modified by  Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskMultipleFileCreation (void)
{
  tU32 u32Ret     = 0;
  tU32 u32MaxLoop = 10;
  tU32 u32Cnt=0;
  OSAL_tIODescriptor hFd1;

  while((u32Cnt < u32MaxLoop) && (u32Ret == 0))
  {
    hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile.txt", OSAL_EN_READWRITE);
    if(hFd1 == OSAL_ERROR)
    {
      u32Ret = 1;
    }
    else
    {
      if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
      {
        u32Ret = 2;
      }
      else
      {
        if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile.txt"))
        {
          u32Ret = 3;
        }
      }
    }

    u32Cnt++;
  }	  
  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskWriteOddBuffer()
* AUTHOR: tnn
* CALLED: YES
* tests writing of an odd buffer to ramdisk and reads it again
* TEST CASE:    TU_OEDT_RAMDisk_075
* HISTORY:		Modified by  Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
               Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
               Updated by Deepak Kumar (RBEI/ECF5) on 26 Mar, 2015
**********************************************************************FunctionHeaderEnd***/
/* durch 4 teilbar!! */
#define OEDT_RAMDISK_TEST_BUFFER_SIZE 16

tU32 u32RAMDiskWriteOddBuffer(void)
{
  tU32 u32Ret = 0;
  OSAL_tIODescriptor hFd1;
  tString pBuffer = OSAL_NULL;
  tString pBuffer2= OSAL_NULL;
  size_t sizeCnt = 0;
  size_t sizeBuf = OEDT_RAMDISK_TEST_BUFFER_SIZE;

  hFd1  = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile", OSAL_EN_READWRITE);
  CHECK_4_OSAL_ERROR(hFd1, 1);

  pBuffer = (tString)OSAL_pvMemoryAllocate(sizeBuf*sizeof(tS8));
  if(pBuffer != NULL)
  {
	  for(sizeCnt = 0; sizeCnt < (sizeBuf - 1); sizeCnt += 4)
	  {
		if (sizeCnt+3 < sizeBuf)
		{
		  pBuffer[sizeCnt]     = 0xde;
  	      pBuffer[sizeCnt + 1] = 0xad;
	      pBuffer[sizeCnt + 2] = 0xbe;
	      pBuffer[sizeCnt + 3] = 0xef;
		}
	  }
	  //pBuffer++;
	  
	  if(OSAL_s32IOWrite(hFd1, (tPCS8)(pBuffer+1), (tU32)(sizeBuf - 1)) != (tS32)(sizeBuf - 1))  //fix for lint
	  {
	    u32Ret = 2;
	  }
	  else
	  {
	    if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)0) == OSAL_ERROR)
	    {
	      u32Ret = 3;
	    }
	    else
	    {
	      pBuffer2 = (tString)OSAL_pvMemoryAllocate(sizeBuf - 1);
	      if(pBuffer2 != NULL)
		  {
		      if(OSAL_s32IORead(hFd1, (tPS8)pBuffer2, (tU32)(sizeBuf - 1)) != (tS32)(sizeBuf - 1))
		      {
		        u32Ret = 4;
		      }
		      else
		      {
		        if(strncmp((tCString)(pBuffer+1),(tCString) pBuffer2, (sizeBuf - 1)) != 0)    //fix for lint    
		        {
		          u32Ret = 5;
		        }
		      }
		   }
	    }
	  }

	  if(pBuffer2 != NULL)
	  {
	    OSAL_vMemoryFree((tString)pBuffer2);
	  }

	  //pBuffer--;
   	  OSAL_vMemoryFree((tString)pBuffer);
  }

  if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
  {
    u32Ret += 10;
  }

  else if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile") == OSAL_ERROR)
  {
    u32Ret += 100;
  }

  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskFilePtrTest()
* AUTHOR: tnn
* CALLED: NO
* tests the file access pointers (only in case of 2 handles from the same file)
* -> not in the standard test set
* TEST CASE:    TU_OEDT_RAMDisk_076
* HISTORY:	   Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
               Updated by Balaji.V(RBEI/ECF5) for lint removal on 6 June,2015
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskFilePtrTest(void)
{
  OSAL_tIODescriptor hFd1;
  OSAL_tIODescriptor hFd2;
  tU32 iPos  = 0;
  tU32 bRet = 0;
  tU32 *temp = &iPos; /*Temporary variable is taken for avoid lint warning*/

  hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile3", OSAL_EN_READWRITE);
 
  OSAL_s32IOWrite(hFd1, as8RAMdiskMesgToWrite, 64);
  OSAL_s32IOClose(hFd1);

  hFd2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_RAMDISK"/testfile3", OSAL_EN_READWRITE);
  OSAL_s32IOControl(hFd2, OSAL_C_S32_IOCTRL_FIOWHERE, (tS32)&iPos);
  if(*temp != 0) bRet = 1;
  if(OSAL_s32IOClose(hFd2)!=OSAL_ERROR)
  {
  	OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile3");
  }

    return bRet;
}



/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskWriteWithInvalidSize()
* AUTHOR: tnn
* CALLED: YES
* writing with invalid size
* TEST CASE:    TU_OEDT_RAMDisk_077
* HISTORY:		Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
				Updated by Rakesh Dhanya(RBIN/EDI3) on 20 April, 2007
                Updated by Balaji.V(RBEI/ECF5) for lint removal on 6 June,2015
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskWriteWithInvalidSize(void)
{
  OSAL_tIODescriptor hFd1;
  tS32  iCnt;
  tU32  u32Ret = 0;
  tU32  iPos   = 0;
  tCS8 DatatoWrite[]= "abcdefg";
  tU32 *temp = &iPos; /*Temporary variable is taken for avoid lint warning*/

  hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile", OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    u32Ret = 1;
	return u32Ret;
  }
  else
  {
    iCnt = OSAL_s32IOWrite(hFd1, DatatoWrite, ((tU32)(-1)));
    if(iCnt != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOWHERE, (tS32)&iPos);
      if(*temp != 0)
      {
        u32Ret = 3;
      }
    }
  }

  if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
  {
    u32Ret += 10;
  }

  else if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile") == OSAL_ERROR)
  {
    u32Ret += 100;
  }

  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskWriteInvalidBuffer ()
* AUTHOR: tnn
* CALLED: NO
* writing with invalid buffer
* TEST CASE:    TU_OEDT_RAMDisk_078
* HISTORY:	  Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
			  Updated by Pradeep Chand C (RBIN/EDI)			4 Apr, 2007
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskWriteInvalidBuffer (void)
{
  OSAL_tIODescriptor hFd1;
  tU32 u32Ret = 0;
  tS32 iCnt = 0;
  tS8* pBuffer = OSAL_NULL;

  hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile", OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    u32Ret = 1;
	return u32Ret;
  }
  else
  {
    iCnt = OSAL_s32IOWrite(hFd1, pBuffer, (tS32)3);
    if(iCnt != OSAL_ERROR)
    {
      u32Ret = 2;
    }
  }
  
  if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
  {
    u32Ret += 10;
  }

  else if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile") == OSAL_ERROR)
  {
    u32Ret += 100;
  }

  return u32Ret;
}


/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskWriteStepByStep()
* AUTHOR: tnn
* CALLED: YES
* write large amount of data stepwise to ramdisk
* TEST CASE:    TU_OEDT_RAMDisk_079
* HISTORY:	   Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006

**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskWriteStepByStep(void)
{
  OSAL_tIODescriptor hFd1;
  tU32 u32Ret = 0;
  tS32 iCnt = 0;
  tU32 iStepWidth = 262144; /* 256 kB */ /* DURCH 4 TEILBAR!!! */
  tString pBuffer = OSAL_NULL;
  tU32 iFileIdx = 0;
  tU32 iFullSize = 1048576; /* 1MB */ /*16777216;*/ /* 16MB */
  tU32 iTmp = 0;

 pBuffer = (tString)OSAL_pvMemoryAllocate(iStepWidth*sizeof(tS8));
 if ( pBuffer == NULL)
 {
  u32Ret = 1;
  return  u32Ret;
 }

  for(iTmp = 0; iTmp < iStepWidth; iTmp += 4)
  {
    if (iTmp+3 < iStepWidth)
    {
      pBuffer[iTmp]     =  0xde;
      pBuffer[iTmp + 1] =  0xad;
      pBuffer[iTmp + 2] =  0xbe;
      pBuffer[iTmp + 3] =  0xef;
    }
  }

  hFd1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_RAMDISK"/testfile", OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    u32Ret = 2;
  }
  else
  {
    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret = 3;
    }
    else
    {
      iFileIdx = 0;
      while((iFileIdx < iFullSize) && (u32Ret == 0))
      {
        hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_RAMDISK"/testfile", OSAL_EN_READWRITE);
        if(hFd1 == OSAL_ERROR)
        {
          u32Ret = 4;
        }
        else
        {
          if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)iFileIdx) == OSAL_ERROR)
          {
            u32Ret = 5;
          }
          else
          {
            iCnt = OSAL_s32IOWrite(hFd1, (tPCS8)pBuffer, iStepWidth);
            if(iCnt != (tS32)iStepWidth)
            {
              u32Ret = 6;
            }
            else
            {
              if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
              {
                u32Ret = 7;
              }
            }
          }
        }

        iFileIdx += iStepWidth;
      }
    }
  }

  OSAL_vMemoryFree((tString)pBuffer);
  pBuffer = NULL;
 
  if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/testfile") == OSAL_ERROR)
  {
    u32Ret += 100;
  }
  
  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskGetFreeSpace ()
* AUTHOR: tnn
* CALLED: YES
* test posibility to get free space
* TEST CASE:    TU_OEDT_RAMDisk_080
* HISTORY:	   Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006
**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskGetFreeSpace (void)
{
  OSAL_tIODescriptor      hFd1;
  OSAL_trIOCtrlDeviceSize rSize;
  tS32                    s32Ret = 0;
  tU32                    u32Ret = 0;
  
  hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_RAMDISK, OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    u32Ret = 1;
	return u32Ret;
  }
  else
  {
    rSize.u32High = 0;
    rSize.u32Low  = 0;

    s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOFREESIZE, (tS32)&rSize);
    if(((rSize.u32Low == 0) && (rSize.u32High == 0)) || (s32Ret == OSAL_ERROR))
    {
      u32Ret = 2;
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}
/*********************************************************************FunctionHeaderBegin***
* u32RAMDiskGetTotalSpace()
* AUTHOR: tnn
* CALLED: YES
* test posibility to get total space
* TEST CASE:    TU_OEDT_RAMDisk_081
* HISTORY:		Modified by Haribabu Sannapaneni (RBIN/ECM1)    20 Apr, 2006

**********************************************************************FunctionHeaderEnd***/
tU32 u32RAMDiskGetTotalSpace(void)
{
  OSAL_tIODescriptor      hFd1;
  OSAL_trIOCtrlDeviceSize rSize;
  tS32                    s32Ret = 0;
  tU32                    u32Ret = 0;
  
  hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_RAMDISK, OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    u32Ret = 1;
	return u32Ret;
  }
  else
  {
    rSize.u32High = 0;
    rSize.u32Low  = 0;

    s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOTOTALSIZE, (tS32)&rSize);
    if(((rSize.u32Low == 0) && (rSize.u32High == 0)) || (s32Ret == OSAL_ERROR))
    {
      u32Ret = 2;
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}

/*****************************************************************************/
/* FUNCTION    :  u32TestRamdiskPerformance()							     */
/* PARAMETER   :  None  						 							 */
/* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error */
/* DESCRIPTION :  Finds bytes written per second 							 */
/* TEST CASE   :  TU_OEDT_RAMDisk_082									     */
/* HISTORY     :  Created by Tinoy Mathews(RBIN/EDI3)	on Oct 11,2007		 */
/* 			   :  Updated by Anoop Chandran(RBIN/EDI3)on Oct 20,2008		 */
/*****************************************************************************/
tU32 u32TestRamdiskPerformance(tVoid)
{
	OSAL_tIODescriptor hFileHandle   = 0;
   	tU32 u32Ret                      = 0;
	tS8* HugeBuffer                  = NULL;
	tU8  u8Counter                   = 0;
	OSAL_tMSecond EffectiveStartTime = 0;
 	OSAL_tMSecond StartTime 		 = 0;
	OSAL_tMSecond EffectiveEndTime   = 0;
 	OSAL_tMSecond EndTime 		     = 0;
	OSAL_tMSecond OneMBWriteTime     = 0;
	OSAL_tMSecond EffectiveWriteTime = 0;
	OSAL_tMSecond OneMBReadTime      = 0;
	OSAL_tMSecond EffectiveReadTime  = 0;
	tFloat afWriteSpeed		         = 0;
	tFloat afReadSpeed	            = 0;
	tFloat afFileWriteSpeed     = 0;
	tFloat afFileReadSpeed      = 0;

	/*Create File handle*/
	hFileHandle = OSAL_IOCreate( OSAL_C_STRING_DEVICE_RAMDISK"/Performance.txt", OSAL_EN_READWRITE );
	if( OSAL_ERROR == hFileHandle )
	{
		u32Ret = 1;
	}
	else
	{
		/*Allocate memory dynamically*/
		HugeBuffer = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBuffer )
		{
			u32Ret = 2;
		}
		else
		{
			/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBuffer,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBuffer,'d',MB_SIZE);

			/*Write Time Performance Measurement*/
			EffectiveStartTime = OSAL_ClockGetElapsedTime();
			while( u8Counter < OEDT_RAMDISK_CYCLES)
			{
			  StartTime = OSAL_ClockGetElapsedTime();
			  if( OSAL_ERROR == OSAL_s32IOWrite( hFileHandle , HugeBuffer, MB_SIZE ) )
			  {
				u32Ret += 20;
				break; /* Exit from the loop , If writing fails */
			  }
			  EndTime  = OSAL_ClockGetElapsedTime();
			  /*Get 1 MB Write Time*/
			  OneMBWriteTime = EndTime - StartTime;
			  /*Calculate Write Speed for 1 MB*/
				if(0 != OneMBWriteTime)
				{
					afWriteSpeed  = (((tFloat)MB_SIZE*(tFloat)1000)/((tFloat)OneMBWriteTime *(tFloat) 1024*(tFloat)1024) );
					OEDT_HelperPrintf
					( 
						TR_LEVEL_USER_1, 
						"The data rate for 1MB Write is	:	%f MB/S \t",
						afWriteSpeed
					);

				}
			  /*Update Counter*/
			  u8Counter++;
			  
			
			}
			EffectiveEndTime   = OSAL_ClockGetElapsedTime();
			if ( EffectiveEndTime >	EffectiveStartTime)
			{
			/*Time for all 1 MB Write three times*/
			EffectiveWriteTime = EffectiveEndTime - EffectiveStartTime;
			}
			else 
			{
			EffectiveWriteTime = ( 0xFFFFFFFF - EffectiveStartTime ) + EffectiveEndTime+1;

			}
			/*THree MB Write Speed*/
			if(0 != EffectiveWriteTime)
			{
			
				afFileWriteSpeed = (((tFloat)MB_SIZE * (tFloat)OEDT_RAMDISK_CYCLES )*(tFloat)1000/((tFloat)EffectiveWriteTime*(tFloat)1024*(tFloat)1024) );
				OEDT_HelperPrintf
				( 
					TR_LEVEL_USER_1, 
					"The Average 3MB Data rate for Write Operation is		:%f MB/s \t",
					afFileWriteSpeed 
				);
				
			}

			/*Reset Paramters*/
			OSAL_pvMemorySet( HugeBuffer,'\0',(MB_SIZE+1) );
			EffectiveStartTime = 0;
			EffectiveEndTime   = 0;
			StartTime          = 0;
			EndTime            = 0;
			u8Counter          = 0;

			
			
			/*Read Time Performance Measurement*/
			EffectiveStartTime = OSAL_ClockGetElapsedTime();
			while( u8Counter < OEDT_RAMDISK_CYCLES )
			{
				StartTime = OSAL_ClockGetElapsedTime();
			    if( OSAL_ERROR == OSAL_s32IORead( hFileHandle, HugeBuffer, MB_SIZE ) )
				{
					u32Ret += 30;
					break; /* Exit from the loop , If read fails */
				}
				EndTime   = OSAL_ClockGetElapsedTime();
				/*Get 1 MB Read Time*/
				
				 OneMBReadTime =	 EndTime - StartTime;

			    	/*Calculate the read speed*/

				if(0 != OneMBReadTime)
				{
					afReadSpeed  = (((tFloat)MB_SIZE * (tFloat)1000) /((tFloat)OneMBReadTime*(tFloat) 1024*(tFloat)1024) );
					OEDT_HelperPrintf
					( 
						TR_LEVEL_USER_1, 
						"The data rate for 1MB Read is	:	%f MB/S \t",
						afReadSpeed
					);

				}
				/*Update Counter*/
			  	u8Counter++;
			}
			EffectiveEndTime = OSAL_ClockGetElapsedTime();
			/*Time for 1 MB Read Three times*/
			if(	EffectiveEndTime > EffectiveStartTime)
			{
			  EffectiveReadTime = EffectiveEndTime - EffectiveStartTime;
			}
			else
			{
			 EffectiveReadTime = ( 0xFFFFFFFF - EffectiveStartTime ) + EffectiveEndTime+1;

			}
			/*Three MB Read speed*/
			//u32FiveMBFileReadSpeed = ( (MB_SIZE*3)/ (tU32)EffectiveReadTime );
			if(0 != EffectiveReadTime)
			{
			
				afFileReadSpeed = ((((tFloat)MB_SIZE *(tFloat) OEDT_RAMDISK_CYCLES)*(tFloat)1000/((tFloat)EffectiveReadTime*(tFloat)1024*(tFloat)1024) ) );
				OEDT_HelperPrintf
				( 
					TR_LEVEL_USER_1, 
					"The Average 3MB Data rate for Read Operation is		:%f MB/s \t",
					afFileReadSpeed 
				);
				
			}
			
			/*Free the dynamically allocated memory*/
			OSAL_vMemoryFree( HugeBuffer );
			HugeBuffer = OSAL_NULL;
		
		}
		/*Close the file handle*/
		if( OSAL_ERROR == OSAL_s32IOClose( hFileHandle ) )
		{	
			u32Ret += 1000;
		}
		if( OSAL_ERROR == OSAL_s32IORemove( OSAL_C_STRING_DEVICE_RAMDISK"/Performance.txt" ) )
		{
			u32Ret += 2000;
		}
	}

   	/*Return the error value*/
	return u32Ret;
}




/*****************************************************************************
* FUNCTION	  :	 u32RAMDiskWriteFile2Threads()
* PARAMETER   :  none
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :  TU_OEDT_RAMDisk_083
* DESCRIPTION :  Writes on the same file using two threads, programmatically
				 given the same time.
* HISTORY     :	 Created by Tinoy Mathews(RBIN/EDI3)  Oct 16,2007
*				 Modified by Haribabu Sannapaneni ( RBIN/ECM1) Feb 14, 2008.
*******************************************************************************/
tU32  u32RAMDiskWriteFile2Threads(tVoid)
{
	/*Definitions*/
	tU32 u32Ret                    	   = 0;

	OSAL_tEventMask EM                 = 0;

   	/*Thread attributes*/
	OSAL_trThreadAttribute tr1_atr     ={OSAL_NULL};
	OSAL_trThreadAttribute tr2_atr	   ={OSAL_NULL};

	/*Initialize thread return code*/
	u32tr1 = 0;
	u32tr2 = 0;

	/*Initialize the event handle*/
	hEventRAMDisk = 0;

	/*Create file in Ramdisk - file Opened*/
	if( ( hDeviceRAM = OSAL_IOCreate( OSAL_C_STRING_DEVICE_RAMDISK"/File.txt" , OSAL_EN_READWRITE ) )
	    == OSAL_ERROR )
	{
		u32Ret = 1;
	}

	if( !u32Ret )
	{
   		/*Allocate memory dynamically for thread 1*/
		HugeBufferTr1 = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr1 )
		{
			u32Ret += 1;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr1,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr1,'d',MB_SIZE);
		}
		/*Fill thread Attributes*/
		 tr1_atr.szName 	    = "RAMThread1";
		 tr1_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		 tr1_atr.s32StackSize = STACKSIZE;
		 tr1_atr.pfEntry = WriteThread1;
		 tr1_atr.pvArg        = NULL;

		
		/*Allocate memory dynamically for thread 2*/
		HugeBufferTr2 = (tS8*)OSAL_pvMemoryAllocate( (tU32)( sizeof( tS8 )*( MB_SIZE+1 ) ) );
		if( OSAL_NULL == HugeBufferTr2 )
		{
			u32Ret += 1;
		}
		else
		{	/*Fill the buffer with some value*/
			OSAL_pvMemorySet( HugeBufferTr2,'\0',(MB_SIZE+1) );
			OSAL_pvMemorySet( HugeBufferTr2,'e',MB_SIZE);
		}
		/*Fill thread Attributes*/
		tr2_atr.szName 	    = "RAMThread2";
		tr2_atr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr2_atr.s32StackSize = STACKSIZE;
		tr2_atr.pfEntry = WriteThread2;
		tr2_atr.pvArg        = NULL;

		 /*Create Event and Open event*/
		u32Ret += OedtRamdiskCreateEvent( &hEventRAMDisk );
		 
		 	
		 /*Create the first thread*/
		 if( OSAL_ERROR == (OSAL_ThreadSpawn( (OSAL_trThreadAttribute *)&tr1_atr ) ) )
		 {
		 	u32Ret += 5;
		 }
		 /*Create the first thread*/
		 if( OSAL_ERROR == (OSAL_ThreadSpawn( (OSAL_trThreadAttribute *)&tr2_atr ) ) )
		 {
		 	u32Ret += 5;
		 }
		 /*Wait for threads to Post status*/
		 if( OSAL_ERROR == OSAL_s32EventWait( hEventRAMDisk, RAM_THREAD1|RAM_THREAD2,
		                 	   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM ) )
		 {
		 	u32Ret += 2000;
		 }

#if 0  /* commented because, trying to delete the threads which are not existing is wrong*/
		 /*Delete the two threads*/
		 if( OSAL_ERROR == OSAL_s32ThreadDelete( tr1 ) )
		 {
			u32Ret += 250;
		 }
		 if( OSAL_ERROR == OSAL_s32ThreadDelete( tr2 ) )
		 {
			u32Ret += 450;
		 }
#endif
		 /*Delete event*/
		 u32Ret += DeleteEvent( &hEventRAMDisk );
		
		/*Close the file*/
		if( OSAL_ERROR == OSAL_s32IOClose( hDeviceRAM ) )
		{
			u32Ret += 600;
		}
		/*Remove the fil from Ramdisk*/
		if( OSAL_ERROR == OSAL_s32IORemove( OSAL_C_STRING_DEVICE_RAMDISK"/file.txt" ) )
		{
			u32Ret += 800;
		}

	   	/*Free the dynamically allocated memory*/
		if(HugeBufferTr1 != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr1 );
			HugeBufferTr1 = OSAL_NULL;
		}
		if(HugeBufferTr2 != NULL)
		{
			OSAL_vMemoryFree( HugeBufferTr2 );
			HugeBufferTr2 = OSAL_NULL;
		}
		
		/*Include also the thread error code*/
		u32Ret += (u32tr1+u32tr2);
	}

	/*Return the error value*/
	return u32Ret;
}
/******************************************************************************
* FUNCTION	  :	 u32RAMDiskRenameFile()
* PARAMETER   :  none
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :  TU_OEDT_RAMDisk_084
* DESCRIPTION :  Renames of the files (rename of directories are not allowed)
* HISTORY     :	 Created by Pankaj Kumar (RBIN/EDI3)  Oct 23,2007
*******************************************************************************/

tU32 u32RAMDiskRenameFile(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile, hDev=0;
	tU32 u32Ret = 0;    
	tChar* arrFileName[2]= {OSAL_NULL};

    /*Create the file in readwrite mode */
 	hFile = OSAL_IOCreate (OSAL_C_STRING_DEVICE_RAMDISK"/File1.txt", enAccess);
	OSAL_s32IOWrite (hFile,(tPCS8) "This is a Test File",19);
	OSAL_s32IOClose(hFile);

    if (hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
   		arrFileName[0] = (tChar*)"File1.txt";        /* old name*/ 
   		arrFileName[1] = (tChar*)"File2.txt";       /* new name */ 
 	    
	 	hDev = OSAL_IOOpen (OSAL_C_STRING_DEVICE_RAMDISK,OSAL_EN_READWRITE);
        if (hDev == OSAL_ERROR )
		{
		   u32Ret = 2;
		}
    	else 
		{
    	  if(OSAL_ERROR == OSAL_s32IOControl(hDev, OSAL_C_S32_IOCTRL_FIORENAME,(tS32)arrFileName ))
		  {
			u32Ret += 3;
 		    /*If rename fails, close  the  */
		    if (OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/File1.txt"))
			{
				u32Ret +=4;
			}
		  }
		  else
		  {
				/*Delete the old  file */
				if (OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/File2.txt"))
				{
					u32Ret +=5;
				}
		  }/*Sucess of close of the old file*/ 
						
 	      /*Close the new  file */
		  if (OSAL_ERROR == OSAL_s32IOClose(hDev))
		  {
				u32Ret += 6;
		  }
 
		}/*	Sucess of  IO Control function*/
	}

 	return u32Ret;
  }	   
	   
/******************************************************************************
* FUNCTION	  :	 u32RAMDiskRemoveDir()
* PARAMETER   :  none
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :  TU_OEDT_RAMDisk_085
* DESCRIPTION :  Remove Files  and Directories recursively
* HISTORY     :	 Created by Pankaj Kumar (RBIN/EDI3)  Oct 23,2007
*******************************************************************************/
tU32 u32RAMDiskRemoveDir(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hDevice;
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
    tU32 u32Ret = 0;    
	
	/*Open the device in read write mode */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
	if(hDevice == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
    	/*Create the directory in the device */
	OSALUTIL_s32CreateDir(hDevice, DIR );

	 /*Create some files in read write mode */
    hFile1 = OSAL_IOCreate(_FILE11 , enAccess);
    if (hFile1 == OSAL_ERROR )
    {
		u32Ret = 1;
	}
	else 
	{
        OSALUTIL_s32CreateDir(hDevice, DIR"/"DIR_RECR1 );
		 /*Create another file */
   		hFile2 = OSAL_IOCreate (_FILE12_RECR2 ,enAccess);
   		if (hFile2 == OSAL_ERROR)
		{
			u32Ret +=2;
		}
		else 
		{
         	OSAL_s32IOClose(hFile1);
				OSAL_s32IOClose(hFile2);
            
    		if(OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,
    											(tS32) DIR )) 
			{
				u32Ret += 3;
				OSAL_s32IORemove(_FILE11);
				OSAL_s32IORemove(_FILE12);
				OSALUTIL_s32RemoveDir(hDevice,OSAL_C_STRING_DEVICE_RAMDISK"/"DIR );
            OSALUTIL_s32RemoveDir(hDevice,OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/"DIR_RECR1  );
				OSAL_s32IOClose(hDevice);
			}
			else
			{
			   
						/*Remove the first file in the dir*/
						if (OSAL_ERROR !=  OSAL_s32IORemove(_FILE11))
						{
							u32Ret += 6;
							OSAL_s32IOClose(hDevice);
						}
						else
						{
							/*Remove the  second file in the dir*/
				        	if (OSAL_ERROR !=  OSAL_s32IORemove(_FILE12))					
							{
								u32Ret += 7;
								OSAL_s32IOClose(hDevice);

							}
							else
							{
								/*Check for directory removal*/
     							if (OSAL_ERROR != OSALUTIL_s32RemoveDir(hDevice,OSAL_C_STRING_DEVICE_RAMDISK"/"DIR ))
								{
									u32Ret += 8;
									OSAL_s32IOClose(hDevice);
								}
								else
								{
									/*Close the device */
									if (OSAL_ERROR == OSAL_s32IOClose(hDevice))
									{
										u32Ret += 9;
									}
							 	}
							}
						}
					}
			   
		 }
	 }
	}
 	return u32Ret;
}



/******************************************************************************
* FUNCTION	  :	 u32RAMDiskCopyDir()
* PARAMETER   :  none
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :  TU_OEDT_RAMDisk_086
* DESCRIPTION :  Copy the directories recursively
* HISTORY     :	 Created by Pankaj Kumar (RBIN/EDI3)  Oct 23,2007
                 Modified by Haribabu Sannapaneni (RBIN/ECM1) Feb 15,2008.

*******************************************************************************/
tU32 u32RAMDiskCopyDir(void)
{
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hDevice ;
    OSAL_tIODescriptor hFile1 = 0;
    OSAL_tIODescriptor hFile2 = 0;
	tS32 hDir1 = 0;
	tS32 hDir2 = 0;
	tU32 u32Ret = 0;    
	tChar* arrFileName[2]={OSAL_NULL};

    /*Open the device in read write mode */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RAMDISK, enAccess );
	if (OSAL_ERROR == hDevice)
	{
		u32Ret =1;
	}
	else
	{
		/*Create the source directory  in readwrite mode */
    	hDir1 = OSALUTIL_s32CreateDir (hDevice,"/"DIR1);
		if (hDir1 == OSAL_ERROR)
		{
			u32Ret += 2;
			OSAL_s32IOClose(hDevice);
		}
		else
		{
			/*Create the destination directory  in readwrite mode */
	        hDir2 =OSALUTIL_s32CreateDir (hDevice,"/"DIR); 
			if (hDir2 == OSAL_ERROR)
			{
				u32Ret += 3;
				OSAL_s32IOClose(hDevice);
			}
            else 
			{	    /*Create the first file in source directory */
   			   		hFile1 = OSAL_IOCreate(_FILE13 , enAccess);
					if (hFile1 == OSAL_ERROR )
    				{
						u32Ret += 4;
						OSALUTIL_s32RemoveDir(hDevice,DIR1);
						OSAL_s32IOClose(hDevice);
					}
					else 
					{
	    				/*Create second file in source directory*/
   						hFile2 = OSAL_IOCreate (_FILE14 ,enAccess);
   						if (hFile2 == OSAL_ERROR)
						{
							u32Ret += 5;
							OSAL_s32IOClose(hFile1);
							OSAL_s32IORemove(_FILE13);
							OSALUTIL_s32RemoveDir(hDevice,DIR1);
							OSAL_s32IOClose(hDevice);

						}
						else 
						{
							arrFileName[0] = (tChar*)OSAL_C_STRING_DEVICE_RAMDISK"/"DIR1 ;        /* Source     Directory */ 
   							arrFileName[1] = (tChar*)OSAL_C_STRING_DEVICE_RAMDISK"/"DIR ;        /* Destination Directory  */ 
    
    		    			if(OSAL_ERROR == OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOCOPYDIR,
    		    										(tS32)arrFileName ))
							{
								u32Ret += 6;
								OSAL_s32IOClose(hFile1);
								OSAL_s32IOClose(hFile2);
								OSAL_s32IORemove(_FILE13);
								OSAL_s32IORemove(_FILE14);
								OSALUTIL_s32RemoveDir(hDevice,DIR1);
								OSAL_s32IOClose(hDevice);
							}
							else  
							{	
							    /*Close the first file in source directory*/ 
		       					if  (OSAL_ERROR == OSAL_s32IOClose(hFile1))
								{	
									u32Ret += 7;
									OSAL_s32IOClose(hDevice);
								}
								else
								{
								 	/*Close the second file in source directory*/ 
		       						if (OSAL_ERROR == OSAL_s32IOClose(hFile2))
		       						{
										u32Ret += 8;
										OSAL_s32IOClose(hDevice);
		    						}
									else 
									{	
								
							   			/*Remove the first file in the source directory*/ 
										if(OSAL_ERROR ==  OSAL_s32IORemove(_FILE13))
										{
											u32Ret += 9;
									   		OSAL_s32IOClose(hDevice);
										}
										else
										{
											/*Remove the second file in the source directory*/ 
											if  (OSAL_ERROR ==  OSAL_s32IORemove(_FILE14))					
											{
								   				u32Ret += 10;
												OSAL_s32IOClose(hDevice);
											}
											else
											{
									   			/* Remove the first copied file in the destination directory*/

												if(OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File13.txt"))
												{
													u32Ret += 11;
													OSAL_s32IOClose(hDevice);
												}
								        		else
												{
													/*Remove the second copied file in the destination  directory*/
										 			if(OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File14.txt"))
													{
														u32Ret += 12;
														OSAL_s32IOClose(hDevice);
													}
													else 
													{
														/*Remove the source directory */
														if (OSAL_ERROR == OSALUTIL_s32RemoveDir(hDevice,DIR1))
														{
															u32Ret += 13;
															OSAL_s32IOClose(hDevice);
														}

														else
														{
												    		/*Remove the destination directory*/
															if  (OSAL_ERROR == OSALUTIL_s32RemoveDir(hDevice,DIR))
															{
																u32Ret += 14;
																OSAL_s32IOClose(hDevice);
															}
															else
													   		{
																if  (OSAL_ERROR == OSAL_s32IOClose(hDevice))
																{
																u32Ret += 15;
																}	
																													
															}/*Sucess of removal of destination directory*/

														}/*Sucess of removal of source directory*/

													}/*Sucess of removal of second file in destination*/

												}/*Sucess of removal of first file in destination*/		

		 									}/*Sucess of removal of second file in source*/

										}/*Sucess of removal of first file in source*/				

									}/*Sucess of closure of second file in source*/

								}/*Sucess of closure of first file in source*/

							}/*Sucess of IO Control function*/

						}/*Sucess of creation of second file in source*/

					}/*Sucess of creation of first file in source*/

				}/*Sucess of creation of destination directory*/

 		 	}/*	Sucess of creation of source directory*/

	 	 }/*Sucess of open device*/

	 return u32Ret;
 }

/******************************************************************************
* FUNCTION	  :	 u32RAMDiskFileUnicodeName()
* PARAMETER   :  none
* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :  TU_OEDT_RAMDisk_87
* DESCRIPTION :  Unicode characters for file name 
* HISTORY     :	 Created by Haribabu Sannapaneni (RBIN/EDI3) Jan 10, 2008
*******************************************************************************/   

tU32 u32RAMDiskFileUnicodeName(void)
{
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_tIODescriptor hFile;
	tU32 u32Ret = 0;
	/*Creation of file with chinese characters*/
	tChar szFilePath [OEDT_RAMDISK_MAX_FILE_PATH_LEN + 1] = OSAL_C_STRING_DEVICE_RAMDISK"/��汪�.txt";


	hFile = OSAL_IOCreate (szFilePath, enAccess);

	if ( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 3;
		}

		else if( OSAL_s32IORemove ( szFilePath ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;

}



