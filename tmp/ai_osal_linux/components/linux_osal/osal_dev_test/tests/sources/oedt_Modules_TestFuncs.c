/******************************************************************************
 * FILE         : oedt_Modules_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : Linux Kernel Module Check
 *           
 * AUTHOR(s)    : Martin Langer (CM-AI/PJ-CF33)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     	    | Author & comments
 * 16 Nov, 2011   | Initial revision    | Martin Langer (CM-AI/PJ-C33)
 * 4 Feb, 2016    | lint_fix            | Deepak Kumar(RBEI/ECF5)
 ------------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Modules_TestFuncs.h"
#include "oedt_helper_funcs.h"


tU32 u32ModuleLoadCheck(void)
{
    tString OEDT_PROC_MODULES = "/dev/root/proc/modules";
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
    OSAL_tIODescriptor hFile;
    tS8 *ps8ReadBuffer = OSAL_NULL;
    tS8 *ps8ReadChar = OSAL_NULL;
    tBool enableComparing = FALSE;
    tBool enableLineSkip = FALSE;
    tS32 lenMax = 10000;
    tS32 idx = 0;
    tS32 widx = 0;
    tS32 modidx = 0;
    tU32 u32RetVal = 0;
    tU32 u32status = 0;

	struct module_info kernel_module[] = {
        {"nls_utf8",          FALSE},
        {"nls_cp437",         FALSE},
        {"hfsplus",           FALSE},
        {"hfs",               FALSE},
        {"ntfs",              FALSE},
        {"vfat",              FALSE},
        {"fat",               FALSE},
        {"sd_mod",            FALSE},
        {"usb_storage",       FALSE},
        {"scsi_mod",          FALSE},
        {"evdev",             FALSE},
        {"snd_usb_audio",     FALSE},
        {"snd_hwdep",         FALSE},
        {"snd_usb_lib",       FALSE},
        {"snd_rawmidi",       FALSE},
        {"usbhid",            FALSE},
        {"loop",              FALSE},
        {"nemid_pmon",        FALSE},
        {"axi64dmac8",        FALSE},
        {"vid_vmp2Driver",    FALSE},
        {"hwmon",             FALSE},
        {"unifi_sdio",        FALSE},
        {"firmware_class",    FALSE},
        {"dc_svg",            FALSE},
        {"pvrsrvkm",          FALSE},
        {"svg_layer",         FALSE},
        {"svg_video",         FALSE},
        {"svg_resource",      FALSE},
        {"svg_common",        FALSE},
        {"rfs",               FALSE},
		{NULL, 0}
	};
  
  
 	hFile = OSAL_IOOpen ( OEDT_PROC_MODULES, enAccess );
    if( hFile == OSAL_ERROR )
    {
        return 1;
	}

    ps8ReadBuffer = (tS8*) OSAL_pvMemoryAllocate( lenMax );
    if ( OSAL_NULL == ps8ReadBuffer)
    {
        return 2;
	}

    ps8ReadChar = (tS8*) OSAL_pvMemoryAllocate( 1 );
    if ( OSAL_NULL == ps8ReadChar)
    {
        OSAL_vMemoryFree( ps8ReadBuffer );
        return 3;
	}
  
  
    for (idx=0; idx<lenMax; idx++)
    {
        if (OSAL_ERROR == OSAL_s32IORead( hFile, ps8ReadChar, 1))
        {
            u32status = OSAL_u32ErrorCode();
            u32status &= ~OSAL_C_COMPONENT;
            u32status &= ~OSAL_C_ERROR_TYPE;			
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IORead at pos %i returns error code %i", idx, u32status );
            OSAL_vMemoryFree( ps8ReadBuffer );
            OSAL_vMemoryFree( ps8ReadChar );
            return 4;
        }
    
        switch (ps8ReadChar[0]) 
        {
            case ' ':
                ps8ReadBuffer[widx] = '\0';      
                widx = 0;
                enableComparing = TRUE;
                break;
      
            case '\n':
                enableLineSkip = FALSE;
                break;
      
            default:
                if (enableLineSkip == FALSE)
                {
                    ps8ReadBuffer[widx] = ps8ReadChar[0];
                    widx++;
                }
                enableComparing = FALSE;
                break;
        }
    
        if (enableComparing == FALSE || ps8ReadBuffer[0] == '\0')
        {
            continue;
        }
     
        for (modidx=0; kernel_module[modidx].module_name!=NULL; modidx++)
        {
            if(OSAL_s32StringCompare((tCString)ps8ReadBuffer,(tCString) kernel_module[modidx].module_name) == 0)
            {
                OEDT_HelperPrintf(TR_LEVEL_USER_4, "INFO: success found string %s = %s", (char*) ps8ReadBuffer, kernel_module[modidx].module_name );
                kernel_module[modidx].module_loaded = TRUE;
            }
        }
    
        enableLineSkip = TRUE;
   
        if ((tS32)ps8ReadBuffer[idx] == EOF)    
        {
            break;
        }
    }

    for (modidx=0; kernel_module[modidx].module_name!=NULL; modidx++)
    {
        if( kernel_module[modidx].module_loaded == FALSE)
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "INFO: kernel module '%s' not loaded", kernel_module[modidx].module_name );
            u32RetVal += 100;
        }
    }
  
    OSAL_vMemoryFree( ps8ReadBuffer );
    OSAL_vMemoryFree( ps8ReadChar );

    if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
	{
        u32status = OSAL_u32ErrorCode();
        u32status &= ~OSAL_C_COMPONENT;
        u32status &= ~OSAL_C_ERROR_TYPE;			
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOClose failed with error code %i", u32status );
		u32RetVal += 5;
	}
  
    return u32RetVal;
}

