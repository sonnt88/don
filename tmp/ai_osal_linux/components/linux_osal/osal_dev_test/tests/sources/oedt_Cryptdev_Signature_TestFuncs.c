
/***************************************************************
*
* FILE:
*     oedt_Cryptdev_Signature_TestFuncs.c
*
* REVISION:
*     1.1
*
* AUTHOR:
*     (c) 2005, RBEI, ECF1 Jeryn M.
*
* CREATED:
*     29/12/2010 - Jeryn M.
*
* DESCRIPTION:
*      This file contains signature verification testcases for
*      crypt devices.
*
* NOTES:
*
* MODIFIED:
* 29-12-2010 | jev1kor    | Initial Version
* 05-04-2011 | Anooj Gopi | Added test cases multiple crypt devices
* 07-01-2013 | Selvan Krishnan | Added test cases for validtaing "/dev/media/UUID" fetaure
* 12-03-2014 | Swathi Bolar    | (INNAVPF-2544)Added test cases to update data to KDS
* 10-02-2015 | Kranthi Kiran   | To fix the Lint warnings (CFG3-1026)
****************************************************************/
#include "oedt_Cryptdev_Signature_TestFuncs.h"
#include <math.h>
#include <string.h>


/****************************************************************
| defines and macros (scope: module-local)
|****************************************************************/
//#define ENABLE_PRM_REGISTRATION
#define  TEST_THREAD_STACK_SIZE           (8*1024)
#define  MAX_NUM_THREAD                     32

#ifndef HIWORD
  #define HIWORD( dwValue )   ((tU16)((((tU32)( dwValue )) >> 16) & 0xFFFF))
#endif

/****************************************************************
| variable declaration (scope: local)
|****************************************************************/

typedef struct
{
   tS32              s32RetVal;/* Return value */
   OSAL_tEventHandle hMultiThreadEvent;/* Event handle */
   tCString coszDevName; /* Device Name */
   tCString coszCertPath; /* Certificate path */
   OSAL_tEventMask eventFlag; /* Event flag need to be sent by thread on exit */
}trThreadArg;

typedef struct
{
   tString szThreadName; /* Thread Name */
   OSAL_tpfThreadEntry pfThreadEntry; /* Thread entry */
   tCString coszDevName; /* Device Name */
   tCString coszCertPath; /* Certificate path */
}trFDCMultiThreadArgs;


static tPChar 	pcSDcardUUID = OSAL_NULL;
static tPChar 	pcUSBUUID = OSAL_NULL;

static tBool bIsMedia_Change_Notified = FALSE;
static tBool bIsData_Media_Found = FALSE;
static tU8 CBcnt;


tU32 u32_IOCTRL_TriggerSignVerify(tCString coszDevicename, tCString coszCertpath);

tU32 u32_IOCTRL_SignVerifyStatus(tCString Devicename);
tU32 u32_IOCTRL_SignFileType(tCString coszDevicename, tCString coszCertpath);
tU32 u32_InvalidCertPath_TriggerSignVerify( tCString Devicename);
tU32 u32_InvalidCertPath_GetSignFileType( tCString Devicename );

static tVoid vMulti_Thread_SignVerify( tPVoid pvThreadArgs );
static tVoid vMulti_Thread_SignType( tPVoid pvThreadArgs );
static tVoid vMulti_Thread_VerifyStat( tPVoid pvThreadArgs );

static tU32 u32CreateThread( tS32 s32ThreadCount,const trFDCMultiThreadArgs *trthread_arg);
static void vPostEvent(  OSAL_tEventHandle hMultiThreadEvent, OSAL_tEventMask u32EventPostVal );
static tU32 u32WriteVINtoKDS(tCString VIN_DATA);
static tU32 u32WriteDIDtoKDS(tCString DID_DATA);

/* Test Cases */
/************************************************************
* FUNCTION NAME: u32_IOCTRL_TriggerSignVerify()
* DESCRIPTION  : Testing of IOControl to trigger Signature
*              : verification.
* ARGUMENTS    : coszDevName - device Name
                           coszCertpath   -Certificate path
* RETURN       : tU32 -> 0 = Success
*                   :            >0 = Failure
* HISTORY      : 
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  17.11.2010 |   jev1kor  | Initial version
*  17.10.2011 |  svs1cob  | Passing device name and certificate path as argument 
                                       to make it generic for crypt and noncrypt devices
*************************************************************/
tU32 u32_IOCTRL_TriggerSignVerify(tCString coszDevicename, tCString coszCertpath)
{
   OSAL_tIODescriptor          hHandle;
   tU32                        u32RetVal = 0;
   OSAL_trTrigSigVerifyArg  rTrigSigVerifyArg;
   
   /* Initialize the argument for IO control */
   rTrigSigVerifyArg.enStatus = OSAL_EN_SIGN_STATUS_UNKNOWN;
   rTrigSigVerifyArg.coszCertPath = coszCertpath;
   

   if( ( hHandle = OSAL_IOOpen( coszDevicename, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
		
      OEDT_HelperPrintf( (tU8)OSAL_C_TRACELEVEL1, "Device \"%s\" Open Failed",coszDevicename );
   }

   else 
   {
		OEDT_HelperPrintf( (tU8)OSAL_C_TRACELEVEL1, " Device \"%s\" Open Passed",coszDevicename );
      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY, (tS32)&rTrigSigVerifyArg) != OSAL_OK )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device enstatus=%d",rTrigSigVerifyArg.enStatus );
         u32RetVal += 2;
      }
      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
      }
   }
   if( rTrigSigVerifyArg.enStatus == OSAL_EN_SIGN_VERIFY_PASSED )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification result = PASSED",coszDevicename );
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification result = FAILED",coszDevicename );
      u32RetVal +=4;
   }
   
   return u32RetVal;
}

/************************************************************
* FUNCTION NAME: u32_IOCTRL_SignVerifyStatus()
* DESCRIPTION  : Testing of IOControl to get Signature
*              : verification status.
* ARGUMENTS    : coszDevName - device Name
* RETURN       : tU32 -> 0 = Success
*              :                 >0 = Failure
* HISTORY      : 
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  17.11.2010 |   jev1kor  | Initial version
*  17.10.2011 |  svs1cob  | Passing device name as argument  to make it
                                       generic for crypt and noncrypt devices
*************************************************************/

tU32 u32_IOCTRL_SignVerifyStatus(tCString Devicename)
{
   OSAL_tIODescriptor       hHandle;
   tU32                    u32RetVal = 0;
   OSAL_tenSignVerifyStatus enStatus  = OSAL_EN_SIGN_STATUS_UNKNOWN;
   
   if( ( hHandle = OSAL_IOOpen( Devicename, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
		
      OEDT_HelperPrintf( (tU8)OSAL_C_TRACELEVEL1, "Device \"%s\" Open Failed",Devicename );
   }
   else 
   {

      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOCRYPT_VERIFY_STATUS, (tS32)(&enStatus) ) != OSAL_OK )
      {
         u32RetVal += 2;
      }
      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
      }
   }
   //PQM_authorized_multi_588: Disable the lint info 774:Boolean within 'if' always evaluates to False 
   //lint -e774 
   if( enStatus == OSAL_EN_SIGN_VERIFY_PASSED )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification status = PASSED",Devicename );
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification status = IN-PROGRESS/FAILED",Devicename );
      
   }
   
   return u32RetVal;
}

/************************************************************
* FUNCTION NAME: u32_IOCTRL_SignFileType()
* DESCRIPTION  : Testing of IOControl to get Signature
*                        : file type.
* ARGUMENTS    : coszDevName - device Name
                           coszCertpath   -Certificate path
* RETURN       : tU32 -> 0 = Success
*                   :            >0 = Failure
* HISTORY      : 
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  17.11.2010 |   jev1kor  | Initial version
*  17.10.2011 |  svs1cob  | Passing device name and certificate path as argument 
                                       to make it generic for crypt and noncrypt devices
*  12.03.2014 |   sow4kor  | Added support for DID (INNAVPF-2544)
*************************************************************/
tU32 u32_IOCTRL_SignFileType(tCString coszDevicename, tCString coszCertpath)
{
   OSAL_tIODescriptor    hHandle;
   tU32                  u32RetVal = 0;
   OSAL_trGetSigTypeArg  rTrigSigVerifyArg;
   rTrigSigVerifyArg.coszCertPath = coszCertpath;
   if( ( hHandle = OSAL_IOOpen( coszDevicename, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;		
      OEDT_HelperPrintf( (tU8)OSAL_C_TRACELEVEL1, "Device \"%s\" Open Failed",coszDevicename );
		
   }
   else
   {
      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE, (tS32)&rTrigSigVerifyArg ) != OSAL_OK )
      {
         u32RetVal += 2;
      }
      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
      }
   }
   
   if(  rTrigSigVerifyArg.enSigType == OSAL_EN_SIGN_XML_VIN )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, 
      "Device \"%s\" Signature Certificate type = XML Certificate using VIN",coszDevicename );
   }
   else if( rTrigSigVerifyArg.enSigType == OSAL_EN_SIGN_XML_CID )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, 
      "Device \"%s\" Signature Certificate type = XML Certificate using CID",coszDevicename );
   }
      else if( rTrigSigVerifyArg.enSigType == OSAL_EN_SIGN_XML_DID )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, 
      "Device \"%s\" Signature Certificate type = XML Certificate using DID",coszDevicename );
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, 
      "Signature Certificate type = Unknown!" );
   }
   
   return u32RetVal;
}

/************************************************************
* FUNCTION NAME: u32_InvalidCertPath_TriggerSignVerify()
* DESCRIPTION  : Testing of IOControl to trigger Signature
*              : verification.
* ARGUMENTS    : coszDevName - device Name
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  05.04.2011 |   Anooj  | Initial version
*************************************************************/
tU32 u32_InvalidCertPath_TriggerSignVerify( tCString Devicename)
{
   OSAL_tIODescriptor       hHandle;
   tU32                     u32RetVal = 0;
   OSAL_trTrigSigVerifyArg  rTrigSigVerifyArg;
   tS32                     s32OSALRet = 0;

   /* Initialize the argument for IO control */
   rTrigSigVerifyArg.enStatus = OSAL_EN_SIGN_STATUS_UNKNOWN;
   rTrigSigVerifyArg.coszCertPath = OEDTTEST_C_STRING_CERT_INVALID_PATH_CRYPTCARD;

   if( ( hHandle = OSAL_IOOpen( Devicename, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
   }
   /* Trigger sign verify need to return IO error */
   else if( (s32OSALRet = OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY,
               (tS32)(&rTrigSigVerifyArg) )) != OSAL_ERROR )
   {
      u32RetVal = 2;
   }
   else if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
   {
      u32RetVal = 3;
   }

   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "OSAL_s32IOControl return value %d ", s32OSALRet);

   if( rTrigSigVerifyArg.enStatus == OSAL_EN_SIGN_VERIFY_PASSED )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification result = PASSED (Not Expected) ",Devicename );
      u32RetVal += 4;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification result = FAILED (Expected) ",Devicename );
   }

   return u32RetVal;
}

/************************************************************
* FUNCTION NAME: u32_InvalidCertPath_GetSignFileType()
* DESCRIPTION  : Testing of IOControl to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  05.04.2011 |   Anooj  | Initial version
*  12.03.2014 |   sow4kor| Added support for DID (INNAVPF-2544)
*************************************************************/
tU32 u32_InvalidCertPath_GetSignFileType( tCString Devicename )
{
   OSAL_tIODescriptor    hHandle;
   tU32                  u32RetVal = 0;
   OSAL_trGetSigTypeArg  rGetSigTypeArg;

   /* Initialize the argument for IO control */
   rGetSigTypeArg.enSigType = OSAL_EN_SIGN_TYPE_UNKNOWN;
   rGetSigTypeArg.coszCertPath = OEDTTEST_C_STRING_CERT_INVALID_PATH_CRYPTCARD;

   if( ( hHandle = OSAL_IOOpen( Devicename, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
   }
   else if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE, (tS32)(&rGetSigTypeArg) ) != OSAL_ERROR )
   {
      u32RetVal = 2;
   }
   else if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
   {
      u32RetVal = 3;
   }

   if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_VIN )
   {
      u32RetVal += 4;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device \"%s\"Signature Certificate type = XML Certificate using VIN",Devicename );
   }
   else if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_CID )
   {
      u32RetVal += 5;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device \"%s\"Signature Certificate type = XML Certificate using CID",Devicename );
   }
    else if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_DID )
   {
      u32RetVal += 5;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device \"%s\"Signature Certificate type = XML Certificate using CID",Devicename );
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Signature Certificate type = Unknown (Expected) " );
   }

   return u32RetVal;
}
/************************************************************
* FUNCTION NAME: u32Cryptcard_IOCTRL_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of cryptcard to trigger 
*                       :signature verify
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_001
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptcard_IOCTRL_TriggerSignVerify(tVoid)
{

   tU32 u32Ret;

   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD, OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD);

   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Cryptcard_IOCTRL_SignVerifyStatus()
* DESCRIPTION  : Testing IOControl of cryptcard to get  
*                       :signature verify status.
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_002
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptcard_IOCTRL_SignVerifyStatus(tVoid)
{

   tU32 u32Ret ;

   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard_IOCTRL_SignFileType()
* DESCRIPTION  : Testing IOControl of cryptcard to get  
*                       :  the type of sign file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_003
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1) | Initial version
*************************************************************/
tU32 u32Cryptcard_IOCTRL_SignFileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD,OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard_InvalidCertPath_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of cryptcard to trigger sign verify 
*				by passing invalid path
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_004
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1) | Initial version
*************************************************************/
tU32 u32Cryptcard_InvalidCertPath_TriggerSignVerify(tVoid)
{
   tU32 u32Ret;
   
   u32Ret=u32_InvalidCertPath_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD);
   
   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Cryptcard_InvalidCertPath_GetSignFileType()
* DESCRIPTION  : Testing IOControl of cryptcard to get sign file type
*				by passing invalid path
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_005
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1) | Initial version
*************************************************************/
tU32 u32Cryptcard_InvalidCertPath_GetSignFileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_InvalidCertPath_GetSignFileType((tCString)OEDTTEST_C_STRING_DEVICE_CRYPTCARD);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard2_IOCTRL_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of cryptcard2 to trigger 
*                       :signature verify
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_006
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptcard2_IOCTRL_TriggerSignVerify(tVoid)
{

   tU32 u32Ret;  
   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD2, OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2);
   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Cryptcard2_IOCTRL_SignVerifyStatus()
* DESCRIPTION  : Testing IOControl of cryptcard2 to get the the status of
*              :  Signature verification.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_007
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptcard2_IOCTRL_SignVerifyStatus(tVoid)
{

   tU32 u32Ret;

   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard2_IOCTRL_SignFileType()
* DESCRIPTION  : Testing IOControl of cryptcard2 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_008
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |   Sudharsanan  | Initial version
*************************************************************/
tU32 u32Cryptcard2_IOCTRL_SignFileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTCARD2,OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptnav_IOCTRL_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of cryptnav to trigger 
*                       :signature verify
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_009
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptnav_IOCTRL_TriggerSignVerify(tVoid)
{

   tU32 u32Ret;
   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAV, OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV);
   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Cryptnav_IOCTRL_SignVerifyStatus()
* DESCRIPTION  : Testing  IOControl of cryptnav get the status of
*              :  Signature verification.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_010
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptnav_IOCTRL_SignVerifyStatus(tVoid)
{

   tU32 u32Ret;

   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAV);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptnav_IOCTRL_SignFileType()
* DESCRIPTION  : Testing IOControl of cryptnav to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_011
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Cryptnav_IOCTRL_SignFileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAV,OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev1_IOCTRL_SignVerify()
* DESCRIPTION  : Testing IOControl of Non crypt dev1 to trigger 
*                       :signature verify
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_012
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev1_IOCTRL_SignVerify(tVoid)
{

   tU32 u32Ret; 
   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CARD, OEDTTEST_C_STRING_CERT_PATH_CARD);
   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev1_IOCTRL_VerifyStatus()
* DESCRIPTION  : Testing IOControl of dev1 to get the status of
*              :  Signature verification.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_013
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev1_IOCTRL_VerifyStatus(tVoid)
{

   tU32 u32Ret;

   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CARD);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev1_IOCTRL_FileType()
* DESCRIPTION  : Testing IOControl of dev1 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_014
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev1_IOCTRL_FileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CARD,OEDTTEST_C_STRING_CERT_PATH_CARD);
   

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev2_IOCTRL_SignVerify()
* DESCRIPTION  : Testing IOControl of dev2 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_015
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev2_IOCTRL_SignVerify(tVoid)
{

   tU32 u32Ret;
   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CARD2, OEDTTEST_C_STRING_CERT_PATH_CARD2);
   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev2_IOCTRL_VerifyStatus()
* DESCRIPTION  : Testing IOControl of dev2 to get the status of
*              :  Signature verification.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_016
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev2_IOCTRL_VerifyStatus(tVoid)
{

   tU32 u32Ret;

   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CARD2);

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev2_IOCTRL_FileType()
* DESCRIPTION  : Testing IOControl of dev2 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_017
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev2_IOCTRL_FileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CARD2,OEDTTEST_C_STRING_CERT_PATH_CARD2);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev3_IOCTRL_SignVerify()
* DESCRIPTION  : Testing IOControl of dev3 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_018
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev3_IOCTRL_SignVerify(tVoid)
{

   tU32 u32Ret;
   u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT, OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT);


   return u32Ret;
}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev3_IOCTRL_VerifyStatus()
* DESCRIPTION  : Testing IOControl of dev3 to get the status of
*              :  Signature verification.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_019
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev3_IOCTRL_VerifyStatus(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignVerifyStatus((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT);
   

   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Noncrypt_dev3_IOCTRL_FileType()
* DESCRIPTION  : Testing IOControl of dev3 to get the type of
*              : Signature certificate file.
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_020
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  11.10.2011 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Noncrypt_dev3_IOCTRL_FileType(tVoid)
{

   tU32 u32Ret;
   
   u32Ret=u32_IOCTRL_SignFileType((tCString) OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT,OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT);
   
   return u32Ret;

}


/*****************************************************************************
* FUNCTION     : vPostEvent()
* PARAMETER    : u32EventPostVal - "Event Post" Value. Gets caught by "Event Wait".
* RETURNVALUE  : Void
* DESCRIPTION  : For the DualThreadTests, each of the threads must post an event
*              : at the end of the test case.
* HISTORY      : 28.Dec.2010 | Initial Version           | Jeryn Mathew (RBEI/ECF1)
*                    :19.Oct.2011  |Event handle changed |Sudharsanan Sivagnanam (RBEI/ECF1)
******************************************************************************/
static void vPostEvent(  OSAL_tEventHandle hMultiThreadEvent, OSAL_tEventMask u32EventPostVal )
{
   if( hMultiThreadEvent != 0 )
   {
      if( OSAL_OK != OSAL_s32EventPost( hMultiThreadEvent,
               u32EventPostVal,
               OSAL_EN_EVENTMASK_OR ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event post Error! ErrorCode : %u", OSAL_u32ErrorCode() );
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event post Passed!" );
      }
   }
}


/*****************************************************************************
* FUNCTION     :  vMultidev_Thread_SignVerify()
* PARAMETER    :  Pointer to Return value
* RETURNVALUE  :  None
* DESCRIPTION  :  Thread to verify signature for specific device
* HISTORY      :  Created by Anooj Gopi (RBEI/ECF1) on Apr 05, 2011
******************************************************************************/
static tVoid vMulti_Thread_SignVerify( tPVoid pvThreadArgs )
{
   OSAL_tIODescriptor       hHandle;
   tU32                     u32RetVal = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;
   OSAL_trTrigSigVerifyArg  rTrigSigVerifyArg;

   /* Initialize the argument for IO control */
   rTrigSigVerifyArg.enStatus = OSAL_EN_SIGN_STATUS_UNKNOWN;
   rTrigSigVerifyArg.coszCertPath = prArgs->coszCertPath;
   if( ( hHandle = OSAL_IOOpen( prArgs->coszDevName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
   }
   else
   {
      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY, (tS32)(&rTrigSigVerifyArg) ) != OSAL_OK )
      {
         u32RetVal += 2;
      }

      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" - Close is failed", prArgs->coszDevName);
      }
   }

   if( rTrigSigVerifyArg.enStatus == OSAL_EN_SIGN_VERIFY_PASSED )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" - Signature Verification result = PASSED", prArgs->coszDevName);
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" - Signature Verification result = FAILED", prArgs->coszDevName );
      u32RetVal += 4;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Trigger Sign Verify Return value = %d" ,
      prArgs->coszDevName, rTrigSigVerifyArg.enStatus);
      
   }

   prArgs->s32RetVal = (tS32)u32RetVal;
   /* Send the signal to main test thread */
   vPostEvent( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}

/*****************************************************************************
* FUNCTION     :  vMultidev_Thread_MultiDev_SignType()
* PARAMETER    :  Pointer to Return value
* RETURNVALUE  :  None
* DESCRIPTION  :  Thread to get signature type for specific device.
* HISTORY      :  Created by Anooj Gopi (RBEI/ECF1) on Apr 05, 2011
                  Added support for DID by Swathi B(INNAVPF-2544) on Mar 12, 2014
******************************************************************************/
static tVoid vMulti_Thread_SignType( tPVoid pvThreadArgs )
{
   OSAL_tIODescriptor       hHandle;
   tU32                     u32RetVal = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;
   OSAL_trGetSigTypeArg  rGetSigTypeArg;

   /* Initialize the argument for IO control */
   rGetSigTypeArg.enSigType = OSAL_EN_SIGN_TYPE_UNKNOWN;
   rGetSigTypeArg.coszCertPath = prArgs->coszCertPath;
   
   if( ( hHandle = OSAL_IOOpen( prArgs->coszDevName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal += 1;
   }
   else
   {
      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE, (tS32)(&rGetSigTypeArg) ) != OSAL_OK )
      {
         u32RetVal += 2;
      }

      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
      }
   }

   if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_VIN )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      " Device: \"%s\" Signature Certificate type = XML Certificate using VIN", prArgs->coszDevName );
   }
   else if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_CID )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device: \"%s\" Signature Certificate type = XML Certificate using CID", prArgs->coszDevName );
   }
      else if( rGetSigTypeArg.enSigType == OSAL_EN_SIGN_XML_DID )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device: \"%s\" Signature Certificate type = XML Certificate using CID", prArgs->coszDevName );
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,
      "Device: \"%s\" Signature Certificate type = Unknown!", prArgs->coszDevName );
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device %s Get Signature type Return value = %d" ,
      prArgs->coszDevName, rGetSigTypeArg.enSigType);
   }
   prArgs->s32RetVal = (tS32)u32RetVal;
   /* Send the signal to main test thread */
   vPostEvent( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}

/*****************************************************************************
* FUNCTION     :  vMultidev_Thread_MultiDev_VerifyStat()
* PARAMETER    :  Pointer to Return value
* RETURNVALUE  :  None
* DESCRIPTION  :  Thread to get verify status for specific device.
* HISTORY      :  Created by Anooj Gopi (RBEI/ECF1) on Apr 05, 2011
******************************************************************************/
static tVoid vMulti_Thread_VerifyStat( tPVoid pvThreadArgs )
{
   OSAL_tIODescriptor       hHandle;
   tU32                     u32RetVal = 0;
   OSAL_tenSignVerifyStatus enStatus  = OSAL_EN_SIGN_STATUS_UNKNOWN;
   trThreadArg               *prArgs   = (trThreadArg *)pvThreadArgs;

   if( ( hHandle = OSAL_IOOpen( prArgs->coszDevName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      u32RetVal = 1;
   }
   else 
   {
      if( OSAL_s32IOControl( hHandle, OSAL_C_S32_IOCTRL_FIOCRYPT_VERIFY_STATUS, (tS32)(&enStatus) ) != OSAL_OK )
      {
         u32RetVal += 2;
      }

      if( OSAL_s32IOClose( hHandle ) != OSAL_OK )
      {
         u32RetVal += 3;
      }
   }
   //PQM_authorized_multi_588: Disable the lint info 774:Boolean within 'if' always evaluates to False 
   //lint -e774 
   if( enStatus == OSAL_EN_SIGN_VERIFY_PASSED )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification status = PASSED", prArgs->coszDevName);
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Signature Verification status = IN-PROGRESS/FAILED",
      prArgs->coszDevName);
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Device \"%s\" Trigger Sign Verify Return value = %d" ,
      prArgs->coszDevName, enStatus);
   }
   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1," vMulti_Thread_VerifyStat:return value=%d ", u32RetVal );
   prArgs->s32RetVal = (tS32)u32RetVal;
   /* Send the signal to main test thread */
   vPostEvent( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}
/*****************************************************************************
* FUNCTION     : u32TriThreadTest( )
* PARAMETER    : pu8ThreadName1 - name of first thread
*              : pu8ThreadName2 - name of second thread
*              : pu8ThreadName2 - name of Third thread
*              : Thread1        - Entry Function name of first thread
*              : Thread2        - Entry Function name of second thread
*              : coszEventName   - Name of the event (for EventHandle)
* RETURNVALUE  : u32Ret - Error Code : 0 for success, < 0 for all errors
* TEST CASE    :
* DESCRIPTION  : The Generic function where 3 threads are opened for all
*              : each device supported by fd_crypt.
* HISTORY      : 05.Apr.2011| Initial Version             | Anooj Gopi (RBEI/ECF1)
*                    : 19.Oct.2011|Changed as generic     |Sudharsanan Sivagnanam (RBEI/ECF1)
*                         |function to create multi thread
*                   : Lints removed (CFG3-1026)             | Kranthi Kiran
******************************************************************************/
static tU32 u32CreateThread( tS32 s32ThreadCount,const trFDCMultiThreadArgs *prMultiThreadArgs)
{
   tS32                   s32Ret          = 0;
   OSAL_tEventMask        ResultMask      = 0;
   tS32                   s32Event_Mask;
   tU32                   u32ThrdCnt;
   tS32                   s32SpawedThreadCount = 0;

   OSAL_tEventHandle      hMultiThreadEvent;
   tCString               coszEventName="Multithread_Test_SignVerify";
   trThreadArg            rThrArg[MAX_NUM_THREAD] ={0};
   OSAL_trThreadAttribute rThreadAttr;
   OSAL_tThreadID         s32ThrdID[MAX_NUM_THREAD]={0};

   if( OSAL_OK == OSAL_s32EventCreate( coszEventName, &hMultiThreadEvent) )
   {
      
      for( u32ThrdCnt = 0; u32ThrdCnt < (tU32)s32ThreadCount; u32ThrdCnt++)
      {
         rThrArg[u32ThrdCnt].coszDevName       = prMultiThreadArgs[u32ThrdCnt].coszDevName;
         rThrArg[u32ThrdCnt].coszCertPath      = prMultiThreadArgs[u32ThrdCnt].coszCertPath;
         rThrArg[u32ThrdCnt].eventFlag         = ((tU32)1<<u32ThrdCnt);
         rThrArg[u32ThrdCnt].hMultiThreadEvent = hMultiThreadEvent;
      }

      /*thread stuff*/
      
      for(u32ThrdCnt=0;u32ThrdCnt<(tU32)s32ThreadCount;u32ThrdCnt++)
      {
         
         rThreadAttr.szName = prMultiThreadArgs[u32ThrdCnt].szThreadName;
         rThreadAttr.pfEntry = prMultiThreadArgs[u32ThrdCnt].pfThreadEntry;
         rThreadAttr.pvArg   = (tPVoid)&rThrArg[u32ThrdCnt];
         rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
         rThreadAttr.s32StackSize = TEST_THREAD_STACK_SIZE;

         s32ThrdID[u32ThrdCnt] = OSAL_ThreadSpawn( &rThreadAttr );
         if( s32ThrdID[u32ThrdCnt] == OSAL_ERROR )
         {
            // thread creation failed
            s32Ret += 100;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Thread spwn failed");
            break;
         }
         else
         {
            s32SpawedThreadCount++;     
         }
      }
      
      if(s32Ret==0)
      {
         
         s32Event_Mask = (tS32)pow(2.0,(double) s32ThreadCount) - 1;
         
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event_mask_value=%d",s32Event_Mask);
         
         if( OSAL_s32EventWait( hMultiThreadEvent, (OSAL_tEventMask)s32Event_Mask,
                  OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER,
                  &ResultMask ) != OSAL_OK )
         {
            s32Ret += 200;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "u32TriThreadTest: Wait for Events over !!!" );
            OSAL_s32EventPost( hMultiThreadEvent, ~ResultMask, OSAL_EN_EVENTMASK_AND);
         }
      }

      //close the event
      if( OSAL_ERROR == OSAL_s32EventClose( hMultiThreadEvent ))
      {
         s32Ret += 300;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event close failed" );
      }
      
      //delete the event
      if( OSAL_ERROR == OSAL_s32EventDelete( coszEventName ) )
      {
         s32Ret += 400;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event delete failed" );
      }

      
      
      for(u32ThrdCnt=0;u32ThrdCnt < (tU32)s32ThreadCount;u32ThrdCnt++)
      {
         
         if(rThrArg[u32ThrdCnt].s32RetVal != 0)
         {
            s32Ret = rThrArg[u32ThrdCnt].s32RetVal + s32Ret;
         }
      }
      
      //delete the Threads
      for(u32ThrdCnt= 0; u32ThrdCnt< (tU32)s32SpawedThreadCount;u32ThrdCnt++)
      {
         OSAL_s32ThreadDelete( s32ThrdID[u32ThrdCnt]);
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "thread[%d] is deleted",u32ThrdCnt );
      }
      
   }

   else
   {
      //event creation failed
      s32Ret += 500;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event create failed" );
   }
   
   return (tU32)s32Ret;
}
/************************************************************
* FUNCTION NAME: u32MultiThread_Signverify()
* DESCRIPTION  : Testing of IOControl of cryptcard to trigger sign 
*  			    :verify from multiple thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_021
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*  10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)           
*************************************************************/
tU32 u32MultiThread_Signverify( tVoid )
{
   
   tU32 u32Ret;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
    tPVoid       coszDevicename = NULL;
    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            OSAL_szStringCopy(coszDevicename, "/dev/media/");
            coszDevicename = OSAL_szStringConcat( coszDevicename, pcSDcardUUID );

            rMultiThreadArgs[0].szThreadName         = "TriggSignVerify1";
            rMultiThreadArgs[0].pfThreadEntry        = vMulti_Thread_SignVerify;
            rMultiThreadArgs[0].coszDevName          = coszDevicename;
            rMultiThreadArgs[0].coszCertPath         = OEDTTEST_C_STRING_CERT_PATH_CARD;

            rMultiThreadArgs[1].szThreadName     = "TriggSignVerify2";
            rMultiThreadArgs[1].pfThreadEntry    = vMulti_Thread_SignVerify;
            rMultiThreadArgs[1].coszDevName      = coszDevicename;
            rMultiThreadArgs[1].coszCertPath     = OEDTTEST_C_STRING_CERT_PATH_CARD;

            rMultiThreadArgs[2].szThreadName     = "TriggSignVerify3";
            rMultiThreadArgs[2].pfThreadEntry    = vMulti_Thread_SignVerify;
            rMultiThreadArgs[2].coszDevName      = coszDevicename;
            rMultiThreadArgs[2].coszCertPath     = OEDTTEST_C_STRING_CERT_PATH_CARD;
            
            u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
            
            OSAL_vMemoryFree (coszDevicename);  
            }
        else
        {
            u32Ret +=20;

        }
            
    }
    else
    {
        u32Ret +=10;
    }

#else
   rMultiThreadArgs[0].szThreadName             = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry            = vMulti_Thread_SignVerify;
   rMultiThreadArgs[0].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName             = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry            = vMulti_Thread_SignVerify;
   rMultiThreadArgs[1].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[1].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[2].szThreadName             = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry            = vMulti_Thread_SignVerify;
   rMultiThreadArgs[2].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[2].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);  
    
#endif /* */

    return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32MultiThread_SignverifyStatus()
* DESCRIPTION  : : Testing of IOControl of cryptcard to get sign 
*  			    :verify status from multiple thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_022
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32MultiThread_SignverifyStatus( tVoid )
{
   
   tU32 u32Ret;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
   tPVoid    coszDevicename = NULL;
   u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            OSAL_szStringCopy(coszDevicename, "/dev/media/");
            coszDevicename = OSAL_szStringConcat( coszDevicename, pcSDcardUUID );
            
            rMultiThreadArgs[0].szThreadName    = "TriggSignVerify1";
            rMultiThreadArgs[0].pfThreadEntry   = vMulti_Thread_VerifyStat;
            rMultiThreadArgs[0].coszDevName     = coszDevicename;
            rMultiThreadArgs[0].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;
            
            rMultiThreadArgs[1].szThreadName    = "TriggSignVerify2";
            rMultiThreadArgs[1].pfThreadEntry   = vMulti_Thread_VerifyStat;
            rMultiThreadArgs[1].coszDevName     = coszDevicename;
            rMultiThreadArgs[1].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;
            
            rMultiThreadArgs[2].szThreadName    = "TriggSignVerify3";
            rMultiThreadArgs[2].pfThreadEntry   = vMulti_Thread_VerifyStat;
            rMultiThreadArgs[2].coszDevName     = coszDevicename;
            rMultiThreadArgs[2].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;
  
            u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);

            OSAL_vMemoryFree (coszDevicename);
        }
        else
        {
            u32Ret +=20;
        }
    }
    else
    {
        u32Ret +=10;
    }
#else
   rMultiThreadArgs[0].szThreadName             = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry            = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[0].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName             = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry            = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[1].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[1].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[2].szThreadName             = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry            = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[2].coszDevName              = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[2].coszCertPath             = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
    
#endif /* */
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32MultiThread_SignverifyStatus()
* DESCRIPTION  : : Testing of IOControl of cryptcard to get sign 
*  			    :file type from multiple thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_023
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/

tU32 u32MultiThread_SignType( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
   tPVoid     coszDevicename = NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            OSAL_szStringCopy(coszDevicename, "/dev/media/");
            coszDevicename = OSAL_szStringConcat( coszDevicename, pcSDcardUUID );

            rMultiThreadArgs[0].szThreadName        = "TriggSignVerify1";
            rMultiThreadArgs[0].pfThreadEntry       = vMulti_Thread_SignType;
            rMultiThreadArgs[0].coszDevName         = coszDevicename;
            rMultiThreadArgs[0].coszCertPath        = OEDTTEST_C_STRING_CERT_PATH_CARD;
    
            rMultiThreadArgs[1].szThreadName        = "TriggSignVerify2";
            rMultiThreadArgs[1].pfThreadEntry       = vMulti_Thread_SignType;
            rMultiThreadArgs[1].coszDevName         = coszDevicename;
            rMultiThreadArgs[1].coszCertPath        = OEDTTEST_C_STRING_CERT_PATH_CARD;
            
            rMultiThreadArgs[2].szThreadName        = "TriggSignVerify3";
            rMultiThreadArgs[2].pfThreadEntry       = vMulti_Thread_SignType;
            rMultiThreadArgs[2].coszDevName         = coszDevicename;
            rMultiThreadArgs[2].coszCertPath        = OEDTTEST_C_STRING_CERT_PATH_CARD;
            
            u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
        
            OSAL_vMemoryFree (coszDevicename);
        }
        else
        {
            u32Ret +=20;
        }
    }
    else
    {
        u32Ret +=10;
    }
#else
    
   rMultiThreadArgs[0].szThreadName                 = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry                = vMulti_Thread_SignType;
   rMultiThreadArgs[0].coszDevName                  = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath                 = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName                 = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry                = vMulti_Thread_SignType;
   rMultiThreadArgs[1].coszDevName                  = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[1].coszCertPath                 = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[2].szThreadName                 = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry                = vMulti_Thread_SignType;
   rMultiThreadArgs[2].coszDevName                  = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[2].coszCertPath                 = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);

 #endif /*  */  
 
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard_Multifunction()
* DESCRIPTION  : : Testing of different IOControl function of cryptcard  
*  			    : from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_024
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/

tU32 u32Cryptcard_Multifunction( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
   
   rMultiThreadArgs[0].szThreadName = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[0].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[1].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[1].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[2].szThreadName = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[2].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[2].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Cryptcard_Multifunction()
* DESCRIPTION  : : Testing of different IOControl function of card  
*  			    : from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_025
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*************************************************************/
tU32 u32Card_Multifunction( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
   
   rMultiThreadArgs[0].szThreadName = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[0].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[0].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[1].szThreadName = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[1].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[1].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[2].szThreadName = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[2].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[2].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Multidev_Signverify()
* DESCRIPTION  : : Testing of IOControl  to trigger sign verify of different device  
*  			    : from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_026
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32Multidev_Signverify( tVoid )
{    
   
   tU32 u32Ret;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
    tPVoid       SDcardDevicename = NULL;	
    tPVoid       USBDevicename = NULL;
    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            SDcardDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            OSAL_szStringCopy(SDcardDevicename, "/dev/media/");
            SDcardDevicename = OSAL_szStringConcat( SDcardDevicename, pcSDcardUUID );

            rMultiThreadArgs[0].szThreadName        = "TriggSignVerify1";
            rMultiThreadArgs[0].pfThreadEntry       = vMulti_Thread_SignVerify;
            rMultiThreadArgs[0].coszDevName         = SDcardDevicename;
            rMultiThreadArgs[0].coszCertPath        = OEDTTEST_C_STRING_CERT_PATH_CARD;
        

            u32Ret= u32USB_RegisterForNotification();

            if ( OSAL_OK == u32Ret )
            {
                if ( NULL != pcUSBUUID )
                {
                    USBDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
                    OSAL_szStringCopy(USBDevicename, "/dev/media/");
                    USBDevicename = OSAL_szStringConcat( USBDevicename, pcUSBUUID );
                    rMultiThreadArgs[1].szThreadName    = "TriggSignVerify2";
                    rMultiThreadArgs[1].pfThreadEntry   = vMulti_Thread_SignVerify;
                    rMultiThreadArgs[1].coszDevName     = USBDevicename;
                    rMultiThreadArgs[1].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_USB;

                    
                    u32Ret = u32CreateThread(2,(trFDCMultiThreadArgs *) rMultiThreadArgs);
                    if ( OSAL_OK != u32Ret )
                    {
                        u32Ret += 100;
                    }
                    OSAL_vMemoryFree (USBDevicename);
                }
                else
                {
                    u32Ret += 200;
                }
            }
            else
            {
                u32Ret += 10;
            }
            OSAL_vMemoryFree (SDcardDevicename);
        }
        else
        {
            u32Ret +=500;
        }
    }
    else
    {
        u32Ret +=1000;
    }
            
#else
   
   rMultiThreadArgs[0].szThreadName  = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[0].coszDevName   = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName  = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[1].coszDevName   = OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
   rMultiThreadArgs[1].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2;
   
   rMultiThreadArgs[2].szThreadName  = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[2].coszDevName   = OEDTTEST_C_STRING_DEVICE_CRYPTNAV;
   rMultiThreadArgs[2].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV;

   rMultiThreadArgs[3].szThreadName  = "TriggSignVerify4";
   rMultiThreadArgs[3].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[3].coszDevName   = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[3].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[4].szThreadName  = "TriggSignVerify5";
   rMultiThreadArgs[4].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[4].coszDevName   = OEDTTEST_C_STRING_DEVICE_CARD2;
   rMultiThreadArgs[4].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CARD2;
   
   rMultiThreadArgs[5].szThreadName  = "TriggSignVerify6";
   rMultiThreadArgs[5].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[5].coszDevName   = OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT;
   rMultiThreadArgs[5].coszCertPath  = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT;
   
   u32Ret = u32CreateThread(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);

#endif /* Enable_Gen3_Crypt_Support*/ 
 
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Multidev_SignverifyStatus()
* DESCRIPTION  : : Testing of IOControl  to get sign verify status of different device  
*  			    : from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_027
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32Multidev_SignverifyStatus( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
    tPVoid       SDcardDevicename = NULL;	
    tPVoid       USBDevicename = NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            SDcardDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            OSAL_szStringCopy(SDcardDevicename, "/dev/media/");
            SDcardDevicename = OSAL_szStringConcat( SDcardDevicename, pcSDcardUUID );

            rMultiThreadArgs[0].szThreadName    = "SignVerifyStatus1";
            rMultiThreadArgs[0].pfThreadEntry   = vMulti_Thread_VerifyStat;
            rMultiThreadArgs[0].coszDevName     = SDcardDevicename;
            rMultiThreadArgs[0].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;
        

            u32Ret= u32USB_RegisterForNotification();

            if ( OSAL_OK == u32Ret )
            {
                if ( NULL != pcUSBUUID )
                {
                    USBDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
                    OSAL_szStringCopy(USBDevicename, "/dev/media/");
                    USBDevicename = OSAL_szStringConcat( USBDevicename, pcUSBUUID );

                    rMultiThreadArgs[1].szThreadName    = "SignVerifyStatus2";
                    rMultiThreadArgs[1].pfThreadEntry   = vMulti_Thread_VerifyStat;
                    rMultiThreadArgs[1].coszDevName     = USBDevicename;
                    rMultiThreadArgs[1].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_USB;


                    u32Ret = u32CreateThread(2,(trFDCMultiThreadArgs *) rMultiThreadArgs);
                    if ( OSAL_OK != u32Ret )
                    {
                        u32Ret += 100;
                    }
                    OSAL_vMemoryFree (USBDevicename);
                }
                else
                {
                    u32Ret += 200;
                }
            }
            else
            {
                u32Ret += 10;
            }
            OSAL_vMemoryFree (SDcardDevicename);
        }
        else
        {
            u32Ret +=500;
        }
    }
    else
    {
        u32Ret +=1000;
    }
                
#else
   
   rMultiThreadArgs[0].szThreadName = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[0].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[1].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
   rMultiThreadArgs[1].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2;
   
   rMultiThreadArgs[2].szThreadName = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[2].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAV;
   rMultiThreadArgs[2].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV;

   rMultiThreadArgs[3].szThreadName = "TriggSignVerify4";
   rMultiThreadArgs[3].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[3].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[3].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[4].szThreadName = "TriggSignVerify5";
   rMultiThreadArgs[4].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[4].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD2;
   rMultiThreadArgs[4].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD2;
   
   rMultiThreadArgs[5].szThreadName = "TriggSignVerify6";
   rMultiThreadArgs[5].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[5].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT;
   rMultiThreadArgs[5].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT;
   
   u32Ret = u32CreateThread(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
 #endif /* Enable_Gen3_Crypt_Support*/ 
 
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Multidev_SignfileType()
* DESCRIPTION  : : Testing of IOControl  to get sign file type of different 
*  			    :device from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_028
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32Multidev_SignfileType( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
    tPVoid      SDcardDevicename = NULL;
    tPVoid      USBDevicename = NULL;


    u32Ret= u32SDcard_RegisterForNotifiction();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            SDcardDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            OSAL_szStringCopy(SDcardDevicename, "/dev/media/");
            SDcardDevicename = OSAL_szStringConcat( SDcardDevicename, pcSDcardUUID );

            rMultiThreadArgs[0].szThreadName 	= "SignFileType1";
            rMultiThreadArgs[0].pfThreadEntry	= vMulti_Thread_SignType;
            rMultiThreadArgs[0].coszDevName		= SDcardDevicename;
            rMultiThreadArgs[0].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_CARD;

            u32Ret= u32USB_RegisterForNotification();

            if ( OSAL_OK == u32Ret )
            {
                if ( NULL != pcUSBUUID )
                {
                    USBDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
                    OSAL_szStringCopy(USBDevicename, "/dev/media/");
                    USBDevicename = OSAL_szStringConcat( USBDevicename, pcUSBUUID );

                    rMultiThreadArgs[1].szThreadName 	= "SignFileType2";
                    rMultiThreadArgs[1].pfThreadEntry	= vMulti_Thread_SignType;
                    rMultiThreadArgs[1].coszDevName		= USBDevicename;
                    rMultiThreadArgs[1].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_USB;

                    
                    u32Ret = u32CreateThread(2,(trFDCMultiThreadArgs *) rMultiThreadArgs);
                    if ( OSAL_OK != u32Ret )
                    {
                        u32Ret += 100;
                    }
                    OSAL_vMemoryFree (USBDevicename);	
                    
                }
                else
                {
                    u32Ret += 200;
                }
            }
            else
            {
                u32Ret += 10;
            }
            OSAL_vMemoryFree (SDcardDevicename);
        }
        else
        {
            u32Ret +=500;
        }
    }
    else
    {
        u32Ret +=1000;
    }
            
#else

   rMultiThreadArgs[0].szThreadName = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[0].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[1].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
   rMultiThreadArgs[1].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2;
   
   rMultiThreadArgs[2].szThreadName = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[2].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAV;
   rMultiThreadArgs[2].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV;

   rMultiThreadArgs[3].szThreadName = "TriggSignVerify4";
   rMultiThreadArgs[3].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[3].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[3].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[4].szThreadName = "TriggSignVerify5";
   rMultiThreadArgs[4].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[4].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD2;
   rMultiThreadArgs[4].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD2;
   
   rMultiThreadArgs[5].szThreadName = "TriggSignVerify6";
   rMultiThreadArgs[5].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[5].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT;
   rMultiThreadArgs[5].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT;
   
   u32Ret = u32CreateThread(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);

#endif /* Enable_Gen3_Crypt_Support*/
   
   return u32Ret;

}
/************************************************************
* FUNCTION NAME: u32Multidev_Multifunction()
* DESCRIPTION  : : Testing of different IOControl functions of different 
*  			    :device from different thread          
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_029
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   19.10.2011 |   Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*   10.02.2015 | Kranthi Kiran                         |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32Multidev_Multifunction( tVoid )
{ 
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
        tPVoid       SDcardDevicename = NULL;	
        tPVoid       USBDevicename = NULL;

        u32Ret= u32SDcard_RegisterForNotification();

        if ( OSAL_OK == u32Ret )
        {
            if ( NULL != pcSDcardUUID )
            {
                SDcardDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
                OSAL_szStringCopy(SDcardDevicename, "/dev/media/");
                SDcardDevicename = OSAL_szStringConcat( SDcardDevicename, pcSDcardUUID );
                rMultiThreadArgs[0].szThreadName    = "SDcardTriggSignVerify";
                rMultiThreadArgs[0].pfThreadEntry   = vMulti_Thread_SignVerify;
                rMultiThreadArgs[0].coszDevName      = SDcardDevicename;
                rMultiThreadArgs[0].coszCertPath     = OEDTTEST_C_STRING_CERT_PATH_CARD;

                rMultiThreadArgs[1].szThreadName 	= "SDcardSignVerifyStatus";
                rMultiThreadArgs[1].pfThreadEntry	= vMulti_Thread_VerifyStat;
                rMultiThreadArgs[1].coszDevName 		= SDcardDevicename;
                rMultiThreadArgs[1].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_CARD;
                rMultiThreadArgs[2].szThreadName 	= "SDcardSignFileType1";
                rMultiThreadArgs[2].pfThreadEntry	= vMulti_Thread_SignType;
                rMultiThreadArgs[2].coszDevName		= SDcardDevicename;
                rMultiThreadArgs[2].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_CARD;

                u32Ret= u32USB_RegisterForNotification();
                if ( OSAL_OK == u32Ret )
                {
                    if ( NULL != pcUSBUUID )
                    {
                        USBDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
                        OSAL_szStringCopy(USBDevicename, "/dev/media/");
                        USBDevicename = OSAL_szStringConcat( USBDevicename, pcUSBUUID );
                        rMultiThreadArgs[3].szThreadName		= "USBTriggSignVerify";
                        rMultiThreadArgs[3].pfThreadEntry 	= vMulti_Thread_SignVerify;
                        rMultiThreadArgs[3].coszDevName		= USBDevicename;
                        rMultiThreadArgs[3].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_USB;
 
                        rMultiThreadArgs[4].szThreadName 	= "USBSignVerifyStatus";
                        rMultiThreadArgs[4].pfThreadEntry	= vMulti_Thread_VerifyStat;
                        rMultiThreadArgs[4].coszDevName 		= USBDevicename;
                        rMultiThreadArgs[4].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_USB;

                        rMultiThreadArgs[5].szThreadName 	= "USBSignFileType1";
                        rMultiThreadArgs[5].pfThreadEntry	= vMulti_Thread_SignType;
                        rMultiThreadArgs[5].coszDevName		= USBDevicename;
                        rMultiThreadArgs[5].coszCertPath 	= OEDTTEST_C_STRING_CERT_PATH_USB;
                        
                        u32Ret = u32CreateThread(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
                        if ( OSAL_OK != u32Ret )
                        {
                            u32Ret += 100;
                        }
                        OSAL_vMemoryFree (USBDevicename);						
                    }
                    else
                    {
                        u32Ret += 200;
                    }
                }
                else
                {
                    u32Ret += 10;
                }
                OSAL_vMemoryFree (SDcardDevicename);
            }
            else
            {
                u32Ret +=500;
            }
        }
        else
        {
            u32Ret +=1000;
        }
        
#else   
   rMultiThreadArgs[0].szThreadName = "TriggSignVerify1";
   rMultiThreadArgs[0].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[0].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD;
   rMultiThreadArgs[0].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD;
   
   rMultiThreadArgs[1].szThreadName = "TriggSignVerify2";
   rMultiThreadArgs[1].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[1].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAV;
   rMultiThreadArgs[1].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV;
   
   rMultiThreadArgs[2].szThreadName = "TriggSignVerify3";
   rMultiThreadArgs[2].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[2].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD2;
   rMultiThreadArgs[2].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD2;

   rMultiThreadArgs[3].szThreadName = "TriggSignVerify4";
   rMultiThreadArgs[3].pfThreadEntry = vMulti_Thread_SignVerify;
   rMultiThreadArgs[3].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT;
   rMultiThreadArgs[3].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT;
   
   rMultiThreadArgs[4].szThreadName = "TriggSignVerify5";
   rMultiThreadArgs[4].pfThreadEntry = vMulti_Thread_VerifyStat;
   rMultiThreadArgs[4].coszDevName = OEDTTEST_C_STRING_DEVICE_CARD;
   rMultiThreadArgs[4].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CARD;
   
   rMultiThreadArgs[5].szThreadName = "TriggSignVerify6";
   rMultiThreadArgs[5].pfThreadEntry = vMulti_Thread_SignType;
   rMultiThreadArgs[5].coszDevName = OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
   rMultiThreadArgs[5].coszCertPath = OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2;
   
   u32Ret = u32CreateThread(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
#endif /*Enable_Gen3_Crypt_Support */

   return u32Ret;

}



/************************************************************
* FUNCTION NAME: u32SDCard_IOCTRL_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of SD card to trigger 
*                       :signature verify
* ARGUMENTS    : None
* RETURN        : tU32 -> 0 = Success
*                      :        >0 = Failure
* TEST CASE     :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_001
* HISTORY       :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 | Selvan Krishnan(RBEI/ECF5)  | Initial version
*  10.02.2015 | Kranthi Kiran               |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32SDCard_IOCTRL_TriggerSignVerify(tVoid)
{

   tU32             u32Ret;
   tPVoid   coszDevicename= NULL;
   tPVoid   coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    
    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID );		
                u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) coszDevicename_temp, OEDTTEST_C_STRING_CERT_PATH_CARD);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                                " TriggerSignVerify is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret = 2;
            }
        }
        
    }
    else
    {
        u32Ret +=10;
    }


   return u32Ret;
}



/*****************************************************************************
* FUNCTION		:	u32SDcard_RegisterForNotification( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Register to PRM to get 'SD-Card inserted' event
* HISTORY		:	For GEN3, the navigation data is accessed through /dev/media/UUID(SDCard).
   
* Additional Info:  1. Ensure that no navi data is available in sd card.
                    2.Select OEDT_DATASET_SELECT 1 
                    3.Call the test function by OEDT_TEST_SELECTIVE X X 2 2 
                    (X is the class ID for File System and 2 is the test ID)
                    with all testcases relevant to File System enabled in 
                    oedt_AutoTester_Testdescription.c  
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 | Selvan Krishnan(RBEI/ECF5)  | Initial version
*  12.03.2014 | Swathi Bolar(RBEI/ECF5)     | Updated the test case 
******************************************************************************/
tU32 u32SDcard_RegisterForNotification (tVoid )
{
   OSAL_tIODescriptor 		hDevice;
   OSAL_tenAccess 			enAccess = OSAL_EN_READONLY;
   tU32 							u32Ret = 0;
   OSAL_trNotifyDataExt2 	rNotifyData;

   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 10;
		
   }
   else
   {

	  /* Different App Id is required for registration, because
            registation fails with same appid unless unregister
            the earlier one */
      rNotifyData.u16AppID 				= OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName 			= "/dev/media";
      rNotifyData.u16NotificationType	= (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      rNotifyData.pCallbackExt2 			=(OSALCALLBACKFUNCEXT2)vData_Media_SDCARD;

      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 20;
      }
      else
      {
      	CBcnt=0;/* Reinitialize the CBcnt*/
         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 40;
         }
      }
      if((bIsMedia_Change_Notified == FALSE)
         || (bIsData_Media_Found == FALSE))
      {
         u32Ret += 80;
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 160;
      }
   }

   bIsMedia_Change_Notified = FALSE;
   bIsData_Media_Found = FALSE;
   
   return u32Ret;
}


/************************************************************
* FUNCTION NAME	: vData_Media_SDCARD()
* DESCRIPTION  		: Callback function for getting device status & UUID
*                      
* ARGUMENTS   		: Notification status, UUID
* RETURN       		: None
* TEST CASE     		: TU_OEDT_CRYPTDEV_SIGN_VERIFY_001
* HISTORY       		:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 | Selvan Krishnan(RBEI/ECF5)  | Initial version
*************************************************************/

tVoid vData_Media_SDCARD (tPCU32 pu32MediaChangeInfo, tU8 au8String[])
{
   //OSAL_trNotifyDataExt2 	rNotifyData; // to fix the lint it is commented(it is not used again)
     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
         "SDCARD CB executed : %s !!!\n", au8String);

	
   if(pu32MediaChangeInfo != NULL)
   {
	  tU16 u16NotiType;
		CBcnt++; 
		if ( CBcnt == 1)
		{
			pcSDcardUUID = (tPChar)au8String;
		}
		else if( CBcnt == 2)
		{
			pcUSBUUID	 = pcSDcardUUID;
			pcSDcardUUID = (tPChar)au8String;
		}
	  u16NotiType = HIWORD(*pu32MediaChangeInfo);
	  if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
	  {
         bIsMedia_Change_Notified = TRUE;
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got noti media change eve !!!\n");

		 if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA )
		 {
            bIsData_Media_Found = TRUE;
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got data media eve !!!\n");
		 }
	  }
	  else
	  {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got no event !!!\n");
	  }
   }
}



/************************************************************
* FUNCTION NAME	: u32SDCard_IOCTRL_SignVerifyStatus()
* DESCRIPTION		: Testing IOControl of cryptcard to get  
*						   signature verify status.
* ARGUMENTS 		: None
* RETURN 		 		: tU32 -> 0 = Success
*						:		  >0 = Failure
* TEST CASE 			: TU_OEDT_CRYPTDEV_SIGN_VERIFY_002
* HISTORY		 	:
*		DATE	  |	AUTHOR	|	  COMMENTS
*------------------------------------------------------------
*	21.11.2012 |  Selvan Krishnan (RBEI/ECF5)  | Initial version
*   10.02.2015 | Kranthi Kiran (RBEI/ECF5)     |   Lints removed (CFG3-1026)  
*************************************************************/
tU32 u32SDCard_IOCTRL_SignVerifyStatus(tVoid)
{
    tU32             u32Ret;
    tPVoid          coszDevicename = NULL;
    tPVoid          coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();
    
    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1 );
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID );		 
                u32Ret = u32_IOCTRL_SignVerifyStatus((tCString) coszDevicename_temp);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " SignVerifyStatus is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret = 2;
            }
        }  
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;

}


/************************************************************
* FUNCTION NAME	: u32SDCard_IOCTRL_SignFileType()
* DESCRIPTION  		: Testing IOControl of cryptcard to get  
*                       		   the type of sign file.
* ARGUMENTS    		: None
* RETURN       		: tU32 -> 0 = Success
*              			:        >0 = Failure
* TEST CASE    		:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_003
* HISTORY      		:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |   Selvan Krishnan (RBEI/ECF5) | Initial version
*  10.02.2015 | Kranthi Kiran (RBEI/ECF5)     |   Lints removed (CFG3-1026) 
*************************************************************/
tU32 u32SDCard_IOCTRL_SignFileType(tVoid)
{

    tU32             u32Ret;
    tPVoid      coszDevicename = NULL;
    tPVoid      coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID ); 
                u32Ret = u32_IOCTRL_SignFileType((tCString) coszDevicename_temp, OEDTTEST_C_STRING_CERT_PATH_CARD);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " SignFileType is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret=2;
            }
        }
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;

}


/************************************************************
* FUNCTION NAME: u32SDCard_InvalidCertPath_TriggerSignVerify()
* DESCRIPTION  : Testing IOControl of cryptcard to trigger sign verify 
*				by passing invalid path
* ARGUMENTS    : None
* RETURN       : tU32 -> 0 = Success
*              :        >0 = Failure
* TEST CASE    :   TU_OEDT_CRYPTDEV_SIGN_VERIFY_004
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |   Selvan Krishnan (RBEI/ECF1) | Initial version
*  10.02.2015 | Kranthi Kiran (RBEI/ECF5)     |   Lints removed (CFG3-1026) 
*************************************************************/
tU32 u32SDCard_InvalidCertPath_TriggerSignVerify(tVoid)
{
    tU32             u32Ret;
    tPVoid          coszDevicename = NULL;
    tPVoid          coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID );	 
                u32Ret=u32_InvalidCertPath_TriggerSignVerify((tCString) coszDevicename_temp);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " InvalidCertPath_TriggerSignVerify is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret=2;
            }
        } 
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;
 
}


/************************************************************
* FUNCTION NAME		: u32SDCard_InvalidCertPath_GetSignFileType()
* DESCRIPTION  			: Testing IOControl of cryptcard to get sign file type
*								by passing invalid path
* ARGUMENTS    			: None
* RETURN      				: tU32 -> 0 = Success
*              				:        >0 = Failure
* TEST CASE    			:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_005
* HISTORY      			:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |   Selvan Krishnan(RBEI/ECF5) | Initial version
*  10.02.2015 | Kranthi Kiran(RBEI/ECF5)     |   Lints removed (CFG3-1026)
*************************************************************/
tU32 u32SDCard_InvalidCertPath_GetSignFileType(tVoid)
{
    tU32             u32Ret;
    tPVoid      coszDevicename = NULL;
    tPVoid      coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID );	 
                u32Ret=u32_InvalidCertPath_GetSignFileType((tCString) coszDevicename_temp);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " InvalidCertPath_GetSignFileType is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            } 
            else 
            {
                u32Ret = 2;
            }
        }
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;

}


/************************************************************
* FUNCTION NAME		: u32USB_IOCTRL_TriggerSignVerify()
* DESCRIPTION			: Testing IOControl of cryptcard2 to trigger 
*                       			  signature verify
* ARGUMENTS    			: None
* RETURN        			: tU32 -> 0 = Success
*                     			:        >0 = Failure
* TEST CASE    			:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_006
* HISTORY      			:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |  Sudharsanan Sivagnanam (RBEI/ECF1)  | Initial version
*  10.02.2015 | Kranthi Kiran(RBEI/ECF5)             |  Lints removed (CFG3-1026)          
*************************************************************/
tU32 u32USB_IOCTRL_TriggerSignVerify(tVoid)
{

    tU32            u32Ret;
    tPVoid  coszDevicename= NULL;
    tPVoid  coszDevicename_temp= NULL;

    u32Ret= u32USB_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcUSBUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/") + 1 );
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcUSBUUID );		
                u32Ret = u32_IOCTRL_TriggerSignVerify((tCString) coszDevicename_temp, OEDTTEST_C_STRING_CERT_PATH_USB);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " TriggerSignVerify is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret = 2;
            }
        }
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;
}



/*****************************************************************************
* FUNCTION		:	u32USB_RegisterForNotification( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Register to PRM to get 'SD-Card inserted' event
* HISTORY		:	For GEN3, the navigation data is accessed through /dev/media/UUID(USB).
   
* Additional Info:  1. Ensure that no navi data is available in sd card.
                    2.Select OEDT_DATASET_SELECT 1 
                    3.Call the test function by OEDT_TEST_SELECTIVE X X 2 2 
                    (X is the class ID for File System and 2 is the test ID)
                    with all testcases relevant to File System enabled in 
                    oedt_AutoTester_Testdescription.c  
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 | Selvan Krishnan(RBEI/ECF5)  | Initial version                  
*  12.03.2012 | Swathi Bolar(RBEI/ECF5)     | Updated the test case to obtain PRM notification
*  04.03.2015 | Kranthi Kiran (RBEI/ECF5)   | To fix compiler warning
******************************************************************************/
tU32 u32USB_RegisterForNotification (tVoid )
{
   OSAL_tIODescriptor 		hDevice;
   OSAL_tenAccess 			enAccess = OSAL_EN_READONLY;
   tU32 							u32Ret = 0;
   OSAL_trNotifyDataExt2 	rNotifyData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 10;		
   }
   else
   {

	  /* Different App Id is required for registration, because
            registation fails with same appid unless unregister
            the earlier one */
      rNotifyData.u16AppID 				= OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName 			= "/dev/media";
      rNotifyData.u16NotificationType 	= (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      /* to fix compiler error typecast was done */ 
      rNotifyData.pCallbackExt2			= (OSALCALLBACKFUNCEXT2)vData_Media_USB; 

      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 20;
      }
      else
      {
      	CBcnt=0;/* Reinitialize the CBcnt*/
         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 40;
         }
      }
		if((bIsMedia_Change_Notified == FALSE)
			|| (bIsData_Media_Found == FALSE))
      {
         u32Ret += 80;
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 160;
      }
   }

	bIsMedia_Change_Notified = FALSE;
	bIsData_Media_Found = FALSE;
   
   return u32Ret;
}


/************************************************************
* FUNCTION NAME	: vData_Media_USB()
* DESCRIPTION  		: Callback function for getting device status & UUID
*                      
* ARGUMENTS   		: Notification status, UUID
* RETURN       		: None
* HISTORY       		:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 | Selvan Krishnan(RBEI/ECF5)  | Initial version
*************************************************************/

tVoid vData_Media_USB (tPCU32 pu32MediaChangeInfo, tU8 au8String[])
{
	//OSAL_trNotifyDataExt2	rNotifyData;	//lint fix

	
	OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
         "USB CB executed : %s !!!\n", au8String);
   if(pu32MediaChangeInfo != NULL)
   {
	  tU16 u16NotiType ;
		CBcnt++; 
		if ( CBcnt == 1)
		{
			pcUSBUUID = (tPChar)au8String;
		}
		else if( CBcnt == 2)
		{		
			pcSDcardUUID = (tPChar)au8String;
		}
	  u16NotiType = HIWORD(*pu32MediaChangeInfo);
	  if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
	  {
			bIsMedia_Change_Notified = TRUE;
			OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got noti media change eve !!!\n");

		 if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA )
		 {
				bIsData_Media_Found = TRUE;
				OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got data media eve !!!\n");
		 }
	  }
	  else
	  {
			OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
						"got no event !!!\n");
	  }
   }
}




/************************************************************
* FUNCTION NAME			: u32USB_IOCTRL_SignVerifyStatus()
* DESCRIPTION 				: Testing IOControl of cryptcard2 to get the the status of
*             						  Signature verification.
* ARGUMENTS   				: None
* RETURN      					: tU32 -> 0 = Success
*             						:        >0 = Failure
* TEST CASE   				:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_007
* HISTORY      :
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |  Selvan Krishnan (RBEI/ECF5)  | Initial version
*  10.02.2015 | Kranthi Kiran                 |  Lints removed (CFG3-1026)
*************************************************************/
tU32 u32USB_IOCTRL_SignVerifyStatus(tVoid)
{

    tU32            u32Ret;
    tPVoid      coszDevicename= NULL;
    tPVoid      coszDevicename_temp= NULL;

    u32Ret= u32USB_RegisterForNotification();
    
    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcUSBUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            if(coszDevicename!= NULL)
            {
                (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
                coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcUSBUUID ); 	
                u32Ret = u32_IOCTRL_SignVerifyStatus((tCString) coszDevicename_temp);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                                " SignVerifyStatus is failed!!!\n");
                }
                OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret =2;
            }
        }
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;
}


/************************************************************
* FUNCTION NAME		: u32USB_IOCTRL_SignFileType()
* DESCRIPTION  			: Testing IOControl of cryptcard2 to get the type of
*              				: Signature certificate file.
* ARGUMENTS   			: None
* RETURN       			: tU32 -> 0 = Success
*              				:        >0 = Failure
* TEST CASE    			:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_008
* HISTORY      			:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*  21.11.2012 |   Selvan Krishnan  | Initial version
*  10.02.2015 |   Kranthi Kiran    |  Lints removed (CFG3-1026)
*************************************************************/
tU32 u32USB_IOCTRL_SignFileType(tVoid)
{
    tU32            u32Ret;
    tPVoid  coszDevicename= NULL;
    tPVoid  coszDevicename_temp= NULL;

    u32Ret= u32USB_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcUSBUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcUSBUUID )+ OSAL_u32StringLength("/dev/media/") + 1);
            if(coszDevicename!= NULL)
            {
            (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
            coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcUSBUUID ); 
            u32Ret = u32_IOCTRL_SignFileType((tCString) coszDevicename_temp,OEDTTEST_C_STRING_CERT_PATH_USB);
                if ( OSAL_OK != u32Ret)
                {
                    OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                            " SignFileType is failed!!!\n");
                }
            OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
            u32Ret=2;
            }
        }
    }
    else
    {
        u32Ret +=10;
    }


    return u32Ret;
}



/************************************************************
* FUNCTION NAME		: u32SDcard_Multifunction()
* DESCRIPTION  			: Testing of different IOControl function of cryptcard  
*  			   				: from different thread          
* ARGUMENTS    			: None
* RETURN       			: tU32 -> 0 = Success
*             					:        >0 = Failure
* TEST CASE    			:   TU_OEDT_CRYPTDEV_SIGN_VERIFY_024
* HISTORY      			:
*     DATE    |   AUTHOR   |    COMMENTS
*------------------------------------------------------------
*   21.11.2012 |   Selvan Krishnan (RBEI/ECF5)  | Initial version
*   10.02.2015 |   Kranthi Kiran                |  Lints removed (CFG3-1026)
*************************************************************/

tU32 u32SDcard_Multifunction( tVoid )
{
   
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[3];
    tPVoid             coszDevicename= NULL;
    tPVoid             coszDevicename_temp= NULL;

    u32Ret= u32SDcard_RegisterForNotification();

    if ( OSAL_OK == u32Ret )
    {
        if ( NULL != pcSDcardUUID )
        {
            coszDevicename = OSAL_pvMemoryAllocate (OSAL_u32StringLength(pcSDcardUUID )+ OSAL_u32StringLength("/dev/media/")+ 1);
            if(coszDevicename!= NULL)
            {
            (void)OSAL_szStringCopy(coszDevicename, "/dev/media/");
            coszDevicename_temp = OSAL_szStringConcat(coszDevicename, pcSDcardUUID );				
   
            rMultiThreadArgs[0].szThreadName    = "TriggSignVerify1";
            rMultiThreadArgs[0].pfThreadEntry   = vMulti_Thread_SignVerify;
            rMultiThreadArgs[0].coszDevName     = coszDevicename_temp;
            rMultiThreadArgs[0].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;

            rMultiThreadArgs[1].szThreadName    = "SignVerifyStatus";
            rMultiThreadArgs[1].pfThreadEntry   = vMulti_Thread_VerifyStat;
            rMultiThreadArgs[1].coszDevName     = coszDevicename_temp;
            rMultiThreadArgs[1].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;

            rMultiThreadArgs[2].szThreadName    = "SignFileType";
            rMultiThreadArgs[2].pfThreadEntry   = vMulti_Thread_SignType;
            rMultiThreadArgs[2].coszDevName     = coszDevicename_temp;
            rMultiThreadArgs[2].coszCertPath    = OEDTTEST_C_STRING_CERT_PATH_CARD;

            u32Ret = u32CreateThread(3,(trFDCMultiThreadArgs *) rMultiThreadArgs);
            OSAL_vMemoryFree (coszDevicename);
            }
            else
            {
                u32Ret=2;
            }
        }
        else
        {
            u32Ret +=10;
        }
    }
    else
    {
        u32Ret +=20;
    }


   return u32Ret;

}

/*****************************************************************************
* FUNCTION:     u32WriteVINtoKDS()
* PARAMETER:    VIN_DATA - VIN Data has to passed
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to writeVIN data to the VIN  Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012 
******************************************************************************/
static tU32 u32WriteVINtoKDS(tCString VIN_DATA)
{
  tU32 u32Ret = 0;
#ifdef VIN_IN_KDS
  OSAL_tIODescriptor hDevice ;
  tBool bAccessOption = 0;
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;
  tU32 u32NumberOfActiveEntries = 0;
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(tS32)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      rEntryInfo.u16EntryLength = VIN_ENTRY_LEN;
      rEntryInfo.u16Entry = VIN_ENTRY;
      rEntryInfo.u16EntryFlags = ((tU16)0x0000) ;
      /* delete entry, if exist */ 
      OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,rEntryInfo.u16Entry);
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(tS32)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret = 3;
      }
      else
      {
        u32NumberOfActiveEntries = rKDSInfo.u32NumberOfActiveEntries;
      }
      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, VIN_DATA); 
      /* write entry*/
      if(OSAL_s32IOWrite ( hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(tS32)&rKDSInfo) != OSAL_ERROR)
      {       
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
        if((u32NumberOfActiveEntries+1) != rKDSInfo.u32NumberOfActiveEntries)
          u32Ret += 100;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
#else
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(VIN_DATA);
#endif
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32WriteDIDtoKDS()
* PARAMETER:    DID_DATA - DID Value has to passed 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to write DID data to the DID  Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012  
******************************************************************************/
static tU32 u32WriteDIDtoKDS(tCString DID_DATA)
{
  OSAL_tIODescriptor hDevice;
  tU32 u32Ret = 0;
  tBool bAccessOption = 0;
  tsKDS_Info  rKDSInfo;
  tsKDSEntry rEntryInfo;
  tU32 u32NumberOfActiveEntries = 0;
  #ifdef DID_IN_KDS
  //lint: initialize rKDSInfo
  memset(&rKDSInfo,0,sizeof(tsKDS_Info));
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    bAccessOption = 1;
    if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(tS32)bAccessOption) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      rEntryInfo.u16EntryLength = DID_ENTRY_LEN;
      rEntryInfo.u16Entry = DID_ENTRY;
      rEntryInfo.u16EntryFlags = ((tU16)0x0000) ;
      /* delete entry, if exist */ 
      OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,rEntryInfo.u16Entry);
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(tS32)&rKDSInfo) == OSAL_ERROR)
      {
        u32Ret = 3;
      }
      else
      {
        u32NumberOfActiveEntries = rKDSInfo.u32NumberOfActiveEntries;
      }
      (tVoid)OSAL_szStringCopy(rEntryInfo.au8EntryData, DID_DATA); 
      /* write entry*/
      if(OSAL_s32IOWrite ( hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo)) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
      /* check KDS and get active entries*/
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_CHECK,(tS32)&rKDSInfo) != OSAL_ERROR)
      {       
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"NO .of active Entries(After)%d",rKDSInfo.u32NumberOfActiveEntries);
        if((u32NumberOfActiveEntries+1) != rKDSInfo.u32NumberOfActiveEntries)
          u32Ret += 100;
      }
    }
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
  #endif
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32WriteVINValuetoKDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to write VIN data to the VIN Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012 
******************************************************************************/
tU32 u32WriteVINValuetoKDS(void)
{
    tU32 u32Ret;
    u32Ret = u32WriteVINtoKDS(OEDTTEST_C_STRING_VALID_VIN_DATA);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32WriteDIDValuetoKDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to write DID data to the DID Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012 
******************************************************************************/
tU32 u32WriteDIDValuetoKDS(void)
{
    tU32 u32Ret;
    u32Ret = u32WriteDIDtoKDS(OEDTTEST_C_STRING_VALID_DID_DATA);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32WriteErrorVINtoKDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to write error data to the VIN Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012 
******************************************************************************/
tU32 u32WriteErrorVINtoKDS(void)
{
    tU32 u32Ret;
    u32Ret = u32WriteVINtoKDS(OEDTTEST_C_STRING_INVALID_VIN_DATA);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32WriteErrorDIDtoKDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Try to write error data to the DID Entry ID.
* HISTORY:      Created Swathi.Bolar (RBIN/ECF5) March 12, 2012 
******************************************************************************/
tU32 u32WriteErrorDIDtoKDS(void)
{
    tU32 u32Ret;
    u32Ret = u32WriteDIDtoKDS(OEDTTEST_C_STRING_INVALID_DID_DATA);
    return u32Ret;
}

