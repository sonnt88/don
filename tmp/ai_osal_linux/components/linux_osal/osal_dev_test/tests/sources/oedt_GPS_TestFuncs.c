/******************************************************************************
 *FILE         : oedt_GPS_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk
 *
 *DESCRIPTION  : This file implements the individual test cases for the GPS
 *               device in OEDT mode
 *
 *
 *
 *AUTHOR       : Ravindran P (CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2006 Blaupunkt GmbH
 *
 *HISTORY      :  Initial version
                 5.09.06 -  Ravindran P (CM-DI/EAP) - changed  hardcoded path of the device to macro
                 04.06.07 - Ravindran P (CM-DI/EAP) - added test for front end toggle
                 13.06.07 - Ravindran P (CM-DI/EAP) - added test for Production test
                 31.08.07 - Ravindran P (RBIN/EDI3) - added tests for Critical voltage condition
                 18.10.07 - Ravindran P (RBIN/EDI3) - added function enGpsReadRecord
                 24.03.08 - Ravindran P (RBEI/ECM1) - u32Check2DFixAfterSetHints
                 23.03.09 - Ravindran P (RBEI/ECF1) - Compiler/LINT warning removal
                 30.04.09 - Ravindran P (RBEI/ECF1) - Compiler/LINT INFos removal
                 10.07.10 - Sainath K   (RBEI/ECF1) - u32GpsInterfaceCheck has been modified
                                                      to work with Gen2 platform and also FFD device
                                                      access has been changed from FFS1 to FFS2
                 23.05.12 - Jaikumar D  (RBEI/ECF5) - modified the macro REGRESSION_TEST_TIMEOUT for LSIM
                                                    - Added test case u32GpsDeviceOpenClosetest
                 31.05.12 - Jaikumar D  (RBEI/ECF5) - Added test case u32GpsDeviceAntennaStatustest

 *
 *****************************************************************************/


#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "osioctrl_gps.h"


/*regression test time out not needed as it is supported by the framework*/
#if defined(LSIM) || defined(GEN3X86)
#define REGRESSION_TEST_TIMEOUT     60    /* Decreased the regression test timeout for GPS support in LSIM */
#else
#define REGRESSION_TEST_TIMEOUT     28800 /*Regression test Time Out.Value in secs. for 8 hours.Max value imposed by the OEDT framework*/
#endif

#define DEFAULT_CLK_DRIFT           (0)
#define GPS_SEARCH_MASK             0x1f  /*Mask for consistency check for received GPS message*/
#define GPS_SV_PROD_TEST            17    /*Visible satellite used for production test.Would change as per testing location*/
#define CVM_WAIT_TIME               10    /*Wait time in secs from CVM state to normal state*/
#define WAIT_MSEC                   1000  /*Wait time in msec */


/*Specific error codes with values assigned corresponding to the array value in TRT file*/
#define     OSAL_C_S32_GPS_ERROR_UNKNOWN                                36
#define     OSAL_DEVICE_OPEN_ERROR                                      37
#define     OSAL_GPS_START_ERROR                                        38
#define     OSAL_GPS_IOREAD_ERROR                                       39
#define     OSAL_GPS_IOCTRL_READ_RECORD_ERROR                           40
#define     OSAL_C_S32_IOCTRL_VERSION_ERROR                             41
#define     OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR                 42
#define     OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR           43
#define     OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA_ERROR                44
#define     OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT_ERROR          45
#define     OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT_ERROR                  46
#define     OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME_ERROR                 47
#define     OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_ERROR                      48
#define     OSAL_C_S32_IOCTL_GPS_SET_ANTENNA_ERROR                      49
#define     OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE_ERROR         50
#define     OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS_ERROR                51
#define     OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_ERROR                52
#define     OSAL_GPS_STOP_ERROR                                         53
#define     OSAL_DEVICE_CLOSE_ERROR                                     54
#define     OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS_ERROR                55
#define     OSAL_C_S32_IOCTL_GPS_READ_RECORD_ERROR                      56
#define     OSAL_C_S32_FLASH_ACCESS_ERROR                               57
#define     OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR                       58
#define     OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR          59
#define     OSAL_C_S32_SET_PARAM_PROD_TEST_ERROR                        60
#define     OSAL_C_S32_GET_RESULTS_PROD_TEST_ERROR                      61
#define     OSAL_C_S32_SET_INITIAL_POSITION_ERROR                       62
#define     OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR                     63
#define     OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR                     64
#define     OSAL_CVM_CRITICAL_LOW_VOLTAGE_START_ERROR                   65
#define     OSAL_CVM_CRITICAL_LOW_VOLTAGE_END_ERROR                     66
#define     OSAL_C_S32_IOCTL_GPS_SET_HINTS_ERROR                        67



#define     OSAL_GPS_IOCTRL_TIME_OUT_ERROR                              35


/*Front end tes related defines*/

#define OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_0000  0x00
#define OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_0101  0x14
#define OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_1010  0x0A
#define OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_1111  0x1E
#define OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111     0x1E

#define OSAL_FFS_DEVICE     OSAL_C_STRING_DEVICE_FFS2



typedef struct {
    OSAL_tIODescriptor GPSIOHandle;
    tBool bResult;
    tU8 GPSErrNo;

} OSAL_GPSResult;



tU32  u32TimerCnt_oedt;
/*OSAL_trGPSInternalData  rGPSPersistentData_oedt;  */
OSAL_trGPSTimeUTC rGPS_UTCTime_oedt;

/*Entry for time out in seconds as per the Test ID*/
tU16 aGpsTimeOut_oedt[]= { 5  ,5 ,300,90 ,120,300,120,300,300,60};


OSAL_trGPSTimeUTC trGPSTimeUTCDummy = {2006,7,16,12,37,32,32};

 tU8 aGPSErr_oedt[] = {
                    OSAL_C_S32_GPS_ERROR_UNKNOWN    ,                           //  0
                    OSAL_DEVICE_OPEN_ERROR,                                     //  1
                    OSAL_GPS_START_ERROR,                                       //  2
                    OSAL_GPS_IOREAD_ERROR,                                      //  3
                    OSAL_GPS_IOCTRL_READ_RECORD_ERROR ,                         /// 4
                    OSAL_C_S32_IOCTRL_VERSION_ERROR,                            //  5
                    OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR ,               //  6
                    OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR,          //  7
                    OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA_ERROR,               //  8
                    OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT_ERROR,         //  9
                    OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT_ERROR,                 //  10
                    OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME_ERROR,                //  11
                    OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_ERROR ,                    //  12
                    OSAL_C_S32_IOCTL_GPS_SET_ANTENNA_ERROR,                     //  13
                    OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE_ERROR,        //  14
                    OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS_ERROR,               //  15
                    OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_ERROR,               //  16
                    OSAL_GPS_STOP_ERROR ,                                       //  17
                    OSAL_DEVICE_CLOSE_ERROR,                                    //  18
                    OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS_ERROR,               //  19
                    OSAL_GPS_IOCTRL_TIME_OUT_ERROR,                             //  20
                    OSAL_C_S32_IOCTL_GPS_READ_RECORD_ERROR,                     //  21
                    OSAL_C_S32_FLASH_ACCESS_ERROR,                              //  22
                    OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR,                      //  23
                    OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR,         //  24
                    OSAL_C_S32_SET_PARAM_PROD_TEST_ERROR,                       //  25
                    OSAL_C_S32_GET_RESULTS_PROD_TEST_ERROR,                     //  26
                    OSAL_C_S32_SET_INITIAL_POSITION_ERROR,                      //  27
                    OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR,                    //  28
                    OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR,                    //  29
                    OSAL_CVM_CRITICAL_LOW_VOLTAGE_START_ERROR,                  //  30
                    OSAL_CVM_CRITICAL_LOW_VOLTAGE_END_ERROR,                    //  31
                    };


OSAL_tIODescriptor hDeviceRegression;

static OSAL_tTimerHandle hGPSStartTimer_oedt(tVoid);
static tVoid GPSStopTimer_oedt(OSAL_tTimerHandle phTimer);
#ifdef TEST_OSAL_TIMER
static tBool OSAL_GPSTestTimer(tVoid);
#endif

static tVoid v_OedtTimerCallBack(tPVoid pDummyArg);

static OSAL_trGPSRecordFields GPSSetAllRecordFields_oedt(OSAL_trGPSRecordFields sGPSRecordFields);
static OSAL_GPSResult PreparationsForColdStart_oedt(tVoid);
static OSAL_GPSResult PreparationsForWarmStart_oedt(tVoid);
static OSAL_GPSResult enGpsReadRecord(OSAL_tIODescriptor hDevice,OSAL_trGPSReadRecordHeaderArgs* rGPSReadRecordHeaderArgs);

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function decalaration (scope: extern)
|----------------------------------------------------------------*/




/*****************************************************************
| Global variables
|----------------------------------------------------------------*/



/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/************************************************************************
 *FUNCTION     :  u32GpsBasic_OpenReadDevice
 *DESCRIPTION  :  Test case 1 of the SW Test specification for drv_gps Edition 1.1.This does not need GPS antenna active
                   1.Gets a device handle for this device.
                   2.Starts the GPS device
                   3.Reads one GPS data record Header
                   4.Retrive the data associated with the header
                   5.Stop the GPS device
                   6.Close the device

 *PARAMETER    :   Nil
 *
 *
 *RETURNVALUE  :  tU32(0 - success or Error code - failure)
 *
 *Test Number  :  TU_OEDT_GPS_00
 *HISTORY         26.04.06  Ravindran P (CM-DI/ESA)
 *                19.10.07  Ravindran P(RBIN/EDI3)
                   -enGpsReadRecord added as replacement to read IOControls
 *
 ************************************************************************/

tU32  u32GpsBasic_OpenReadDevice( tVoid )
{
    OSAL_tIODescriptor hDevice = 0;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};

    tU32 u32RetVal = 0;
    OSAL_GPSResult enGpsResult ;
    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

   /********************Step1-Open the device*************************************************/


    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );


    if ( hDevice == OSAL_ERROR)
    {
         return(OSAL_DEVICE_OPEN_ERROR);
    }


   /************Step2:Start the GPS device****************************************************/

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0 ) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/

         return(OSAL_GPS_START_ERROR);

    }

    /*Read a GPS record*/
    enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
    if(FALSE == enGpsResult.bResult)
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
        return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
    }


    /**************Step 4:Stop the device**************************************************/
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
    == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return(OSAL_GPS_STOP_ERROR);

    }

    /**************Step 5:Close the device **************************************************/
    if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
    {


         u32RetVal = OSAL_DEVICE_CLOSE_ERROR;

    }
    else
    {

        u32RetVal = 0;

    }


    return(u32RetVal);


}



/************************************************************************
 *FUNCTION     :  u32GpsInterfaceCheck
 *DESCRIPTION  :  Test case 2 of the SW Test specification for drv_gps Edition 1.1.This does not need GPS antenna active
                   1.Opens the device
                   2.Gets the version
                   3.Sets the start Up Mode
                   4.Gets the size of storage of persistent data
                   5.Set the default clock drift of the oscillator
                   6.Get the default clock drift of the oscillator
                   7.Set the current time in UTC
                   8.Start the GPS calculations
                   9.Get the status of the Antenna
                   10.Set the status of the Antenna to ON
                   11.Get the current Clock Drift
                   12.Read data Record Header
                   13.Get the No of Records
                   14.Get the Record field Information
                   15.Set the desired fields in the record
                   16.Retrieve the data associated with the data record header
                   17.Stop the GPS device
                   18.Get the Persistent data for storage
                   19.Close the Device

 *PARAMETER    :  Nil

 *
 *
 *RETURNVALUE  :  tU32(0 - success or Error code - failure)
 *
 *Test Number  :  TU_OEDT_GPS_01
 *HISTORY      :  26.04.06  Ravindran P (CM-DI/ESA)
 *             :  10.07.10  Sainath Kalpuri (RBEI/ECF1)
                  Time details (Year, Month, Date, Hour etc..) have been
                  updated to valid data instead zero to make it run on Gen2
 *
 *Initial Revision.
 ************************************************************************/

tU32 u32GpsInterfaceCheck( tVoid )
{
    OSAL_tIODescriptor  hDevice = 0;
    OSAL_trGPSRecordHeader  rGpsHeader = {0,0,0,0,
        {{0,0,0}}
    };

    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs= {NULL,0};
    OSAL_trGPSRecordFields rGPSRecordFields = {0,0};
    OSAL_trGPSRecordFields rTmpGPSRecordFields ={0,0};
    OSAL_trGPSInternalData          rGPSInternalData = {NULL,0};
    OSAL_tenGPSStartupMode          enGPSStartupMode =  GPS_STARTUP_COLD;
    tU32 au32Record[(OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE + 3) / 4] = {0};
    tS32    s32GetInternalDataSizeArg = 0;
    tS32    s32GetClockDrift = 0;
    tS32    s32GetNoRecordsAvailableArg = 0;
    tBool    bGetAntennaArg = FALSE;
    tBool   bSetAntennaArg = FALSE;
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;


    tU32 u32Version = 0;
    OSAL_trGPSTimeUTC trGPSTimeUTC ={0,0,0,0,0,0,0};

    tU32 u32RetVal = 0;


    /********************Step1-Open the device*************************************************/


    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );


    if ( hDevice == OSAL_ERROR)
    {

        return(OSAL_DEVICE_OPEN_ERROR);

    }

    /***************************************************************/
    /************Step2:Start get Version****************************************************/



    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTRL_VERSION_ERROR );

    }

    /***************************************************************/
    /************Step3:Set Start Up Mode****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR );

    }

    /***************************************************************/
    /************Step4:Get the size of internal data****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE,(tS32)&s32GetInternalDataSizeArg ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR );

    }

    /***************************************************************/

    /************Step5:Restore the size of internal data****************************************************/


    /*OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA - commented as check sum for persistent data would be wrong in Fd GPS driver* /

    recGPSInternalData.u32BufSize =s32GetInternalDataSizeArg;
    recGPSInternalData.pau8Buf =(tU8 *)OSAL_pvMemoryAllocate(recGPSInternalData.u32BufSize);
    OSAL_pvMemorySet(recGPSInternalData.pau8Buf,0x00,recGPSInternalData.u32BufSize);

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA,(tS32) &recGPSInternalData) == OSAL_ERROR )
    {
        OSAL_vMemoryFree((tPVoid)recGPSInternalData.pau8Buf);
        OSAL_s32IOClose( hDevice );
        return ( OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA_ERROR );

    }
    OSAL_vMemoryFree((tPVoid)recGPSInternalData.pau8Buf);*/


    /************Step6:Set the clock drift ****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT,(tS32)DEFAULT_CLK_DRIFT) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT_ERROR );

    }



    /***************************************************************/

    /************Step7:Get the clock drift ****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT,(tS32)&s32GetClockDrift)  == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT_ERROR );

    }

    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR );
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR );
    }




    /***************************************************************/

    /************Step8:Set the current time ****************************************************/
    trGPSTimeUTC.u16Year = 2010;
    trGPSTimeUTC.u16Month =  7;
    trGPSTimeUTC.u16Day =  8;
    trGPSTimeUTC.u16Hour = 14;
    trGPSTimeUTC.u16Minute =  13;
    trGPSTimeUTC.u16Second = 35;
    trGPSTimeUTC.u16Millisecond = 0;

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME,(tS32)&trGPSTimeUTC) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME_ERROR );

    }


    /***************************************************************/

    /************Step 9:Start the device ****************************************************/

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32) 0 ) == OSAL_ERROR )
    {

        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_GPS_START_ERROR );

    }

    /************Step 10:Get the antenna status ****************************************************/

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_GET_ANTENNA,(tS32)&bGetAntennaArg ) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/

        return ( OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_ERROR );


    }

    /***************************************************************/

    /************Step 11:Set the antenna status ****************************************************/

    bSetAntennaArg = TRUE;//Antenna ON

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_SET_ANTENNA,(tS32)bSetAntennaArg ) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_C_S32_IOCTL_GPS_SET_ANTENNA_ERROR );


    }

    /***************************************************************/

    /************Step12:Get the clock drift ****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT,(tS32)&s32GetClockDrift) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT_ERROR );

    }

    /*Get XO Coeff - for clock drift*/
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR );
    }



    /**************Step13:Get No of records available**************************************************/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE,(tS32)&s32GetNoRecordsAvailableArg ) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

        return ( OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE_ERROR );

    }
    /**************Step14:Do a IO Read**************************************************/

    if ( OSAL_s32IORead (hDevice,(tS8 *)&rGpsHeader,sizeof(rGpsHeader) )
            == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_GPS_IOREAD_ERROR );

    }

    /***************************************************************/


    /**************Step15:Get Records field**************************************************/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS,(tS32)&rGPSRecordFields) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS_ERROR  );

    }

    /***************************************************************/

    /**************Step16:Set Records field**************************************************/

    rGPSRecordFields = GPSSetAllRecordFields_oedt(rTmpGPSRecordFields);

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS,(tS32)&rGPSRecordFields ) == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS_ERROR  );


    }

    /***************************************************************/
    /**************Step 17:Retrive the data using read record command**************************************************/
    rReadRecordHeaderArgs.pBuffer = &au32Record[0];
    rReadRecordHeaderArgs.u32RecordId = rGpsHeader.u32RecordId;

    if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_READ_RECORD,(tS32)&rReadRecordHeaderArgs )
            == OSAL_ERROR )
    {
        OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_GPS_IOCTRL_READ_RECORD_ERROR  );

    }

    /**************Step 18:Stop the device**************************************************/
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_GPS_STOP_ERROR  );

    }

    /**************Step 19:Get internal data**************************************************/
    rGPSInternalData.u32BufSize =(tU32)s32GetInternalDataSizeArg;
    rGPSInternalData.pau8Buf =(tU8 *)OSAL_pvMemoryAllocate(rGPSInternalData.u32BufSize);
    OSAL_pvMemorySet((tPVoid)rGPSInternalData.pau8Buf,0x00,rGPSInternalData.u32BufSize);
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA,(tS32) &rGPSInternalData )
            == OSAL_ERROR )
    {
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


        return ( OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_ERROR  );

    }
    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);

    /**************Step 20:Close the device **************************************************/
    if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
    {


        u32RetVal = OSAL_DEVICE_CLOSE_ERROR;


    }
    else
    {


        u32RetVal = 0;

    }


    return(u32RetVal);



}

/************************************************************************
 *FUNCTION     :  u32GpsRegressionTest
 *DESCRIPTION  :  Test case 2 of the SW Test specification for drv_gps Edition 1.1.This  needs GPS antenna active
                  This can be run for till 7 days.Intermediate exit can be done through Hard Reset


 *PARAMETER    :  Nil

 *RETURNVALUE  :  tU32(0 - success or Error code - failure)
 *
 *Test Number  :  TU_OEDT_GPS_02
 *HISTORY      :  26.04.06  Ravindran P (CM-DI/ESA)
 *
 *
 *Initial Revision.
 ************************************************************************/

tU32  u32GpsRegressionTest( tVoid )
{

    OSAL_tIODescriptor  hDevice = 0;
    OSAL_trGPSRecordHeader rGpsHeader = {0,0,0,0,
                                         {{0,0,0}}
                                        };

    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
    OSAL_trGPSRecordFields rGPSRecordFields = {0,0};
    OSAL_trGPSRecordFields rTmpGPSRecordFields ={0,0};
    tU32  au32Record[(OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE + 3) / 4] = {0};
    tS32    s32GetNoRecordsAvailableArg = 0;
    tBool    bGetAntennaArg = FALSE;
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;
    OSAL_GPSResult GPSRetVal;
    tU32 u32RetVal = 0;
    OSAL_tTimerHandle phTimer = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;


    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {

        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {
        hDevice = GPSRetVal.GPSIOHandle;
        phTimer = hGPSStartTimer_oedt();



        for(;;)
        {
        /************Step 6:Get the antenna status ****************************************************/

            if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_GET_ANTENNA,(tS32)&bGetAntennaArg ) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

                return ( (tBool) OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_ERROR );

            }


            /* Commented - as functionality replaced by XO co-efficients
            if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT,(tS32)&s32GetClockDrift) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );
                OSAL_s32IOClose( hDevice );
                return ( OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT_ERROR );

            } */

            /*XO compensation data - For Clock drift*/

            if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR );
            }



        /************Step 8:Do a IO read ****************************************************/
        /*This call copies the the valid GPS information to the global variable which is used by the OSAL_s32IORead.It also updates the record ID*/

            if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE,(tS32)&s32GetNoRecordsAvailableArg ) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE_ERROR );

            }

            if ( OSAL_s32IORead (hDevice,(tS8 *)&rGpsHeader,sizeof(rGpsHeader) )
            == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_GPS_IOREAD_ERROR );

            }



        /**************Step10:Get Records field**************************************************/

            if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS,(tS32)&rGPSRecordFields) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS_ERROR );

            }

        /**************Step11:Set Records field**************************************************/

            rGPSRecordFields = GPSSetAllRecordFields_oedt(rTmpGPSRecordFields);

            if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS,(tS32)&rGPSRecordFields ) == OSAL_ERROR )
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS );

            }

        /**************Step 12:Retrive the data using read record command**************************************************/
            rReadRecordHeaderArgs.pBuffer = &au32Record[0];
            rReadRecordHeaderArgs.u32RecordId = rGpsHeader.u32RecordId;   /*This assures that the Record ID is the latest*/

            if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_READ_RECORD,(tS32)&rReadRecordHeaderArgs )
            == OSAL_ERROR )
            {
                  OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                  OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                  return ( OSAL_C_S32_IOCTL_GPS_READ_RECORD_ERROR );

            }


            if( u32TimerCnt_oedt >= REGRESSION_TEST_TIMEOUT )

            {
                break;

            }
             else
            {

                continue;
            }



        }



        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
        == OSAL_ERROR )
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
            return ( OSAL_GPS_STOP_ERROR );

        }


        /**************Close the device**************************************************/

        if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
        {

              u32RetVal = OSAL_DEVICE_CLOSE_ERROR;


        }
        else
        {

             u32RetVal = 0;

        }

        GPSStopTimer_oedt(phTimer);
    }



    return( u32RetVal );


}

/************************************************************************
 *FUNCTION:    v_OedtTimerCallBack
 *DESCRIPTION:  Call back functio of the timer used for Time out


 *PARAMETER: Void Pointer

 *RETURNVALUE: Nil
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/

static tVoid v_OedtTimerCallBack( tPVoid pvDummyArg)
{
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvDummyArg);
    u32TimerCnt_oedt ++;

}



#ifdef TEST_OSAL_TIMER
static tBool OSAL_GPSTestTimer(tVoid)
{
    /*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;

    tPVoid pvarg = NULL;
    OSAL_tMSecond TimerCount = 1000 ;
    OSAL_tMSecond Timerinterval =1000;


    tBool bRetVal= TRUE;
    tU32 u32TimerCnt_oedt = 0;



    u32TimerCnt_oedt = 0;
    if(OSAL_s32TimerCreate((OSAL_tpfCallback)vTimerCallBack,pvarg,&phTimer) == OSAL_ERROR)
    {
        bRetVal= FALSE;
    }
    else
        {
        if(OSAL_s32TimerSetTime(phTimer,TimerCount,Timerinterval) == OSAL_ERROR)
        {
                bRetVal= FALSE;
        }


    }

    while(TRUE)
    {

         if(u32TimerCnt_oedt >= 300)

                break;

         else
                continue;

    }

    u32TimerCnt_oedt= 0;

    OSAL_s32TimerSetTime(phTimer, 0, 0);  /*Stop the timer*/
    OSAL_s32TimerDelete( phTimer );/*Delete the timer*/

    return(bRetVal);



}

#endif

/************************************************************************
 *FUNCTION     : u32GpsDurationTimeFix
 *DESCRIPTION  : Test case 4 of the SW Test specification for drv_gps Edition 1.1.This  requires GPS Receipt
                   1.Starts the Device in the cold boot
                   2.Reads the Device continuously until Time Fix happens
                   3.Checks the time duration for the time fix is within the specified limits

 *PARAMETER    : Nil

 *
 *
 *RETURNVALUE  : tU32(0 - success or Error code - failure)
 *
 *Test Number  : TU_OEDT_GPS_03
 *HISTORY      :   26.04.06  Ravindran P (CM-DI/ESA)
                   19.10.07  Ravindran P(RBIN/EDI3)
                   -enGpsReadRecord added as replacement to read IOControls
 *
 *Initial Revision.
 ************************************************************************/
tU32 u32GpsDurationTimeFix( tVoid )
{

    OSAL_tIODescriptor hDevice = 0;
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
    tU32 u32RetVal = 0;
    tU8 u8TestId = 3;

    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    /*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {

        OSAL_s32IOClose( hDevice );
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {
        hDevice = GPSRetVal.GPSIOHandle;
        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }

            /**********Check for valid Data**************************************************/
            /*ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;*/

            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));
            if(rGPSMeasuredPosition.trTime.enTimeValidity== GPS_TIME_VALID)
            {
                break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }

        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
                == OSAL_ERROR )
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


            return ( OSAL_GPS_STOP_ERROR );

        }


        /**************Close the device**************************************************/

        if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
        {


            return ( OSAL_DEVICE_CLOSE_ERROR );

        }
        else
        {



            if(bTimeOut == FALSE)
            {
                u32RetVal = 0;

            }
            else
            {


                u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;

            }

        }



        GPSStopTimer_oedt(phTimer);


        return(u32RetVal);

    }

}

/************************************************************************
 *FUNCTION:    u32GpsDuration2DFix_ColdStart
 *DESCRIPTION: Test case 5 of the SW Test specification for drv_gps Edition 1.1.This  requires GPS Receipt
                1.Starts the Device in the cold boot
                2.Reads the Device continuously until 2D Fix happens
                3.Checks the time duration for the 2D fix is within the specified limits

 *PARAMETER:   Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
               19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *
 *Initial Revision.
 ************************************************************************/


tU32 u32GpsDuration2DFix_ColdStart( tVoid )
{

    OSAL_tIODescriptor hDevice = 0;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;

/*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;
    tU32 u32RetVal = 0;
    tU8 u8TestId = 4;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;


    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {

        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {

            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }

            /**************Check for valid Data**************************************************/

            /*ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;*/
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));
            if( (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_2D) ||
                (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D)   )

            {

                break;

            }
            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                GPSStopTimer_oedt(phTimer);
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_GPS_STOP_ERROR );

           }

        GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

         if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
           {

                return ( OSAL_DEVICE_CLOSE_ERROR );

           }
           else
           {



                if(bTimeOut == FALSE)
                {
                    u32RetVal = 0;

                }
                else
                {
                    u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;

                }

           }
           return(u32RetVal);

        }


}
/************************************************************************
 *FUNCTION:    u32GpsDuration3DFix_ColdStart
 *DESCRIPTION: Test case 6 of the SW Test specification for drv_gps Edition 1.1.This  requires GPS Receipt
                1.Starts the Device in the cold boot
                2.Reads the Device continuously until 3D Fix happens
                3.Checks the time duration for the 3D fix is within the specified limits

 *PARAMETER: Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
                19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *
 *
 ************************************************************************/

tU32  u32GpsDuration3DFix_ColdStart( tVoid )
{

    OSAL_tIODescriptor hDevice = 0;


    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};

/*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;
    tU32 u32RetVal = 0;
    tU8 u8TestId = 5;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;


    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {

            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }


           /*************Check for valid Data**************************************************/
            /*ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;*/
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));

            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

            {

                break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                 GPSStopTimer_oedt(phTimer);
                 OSAL_s32IOClose( hDevice );/*Open pass.Hence close */


                return ( OSAL_GPS_STOP_ERROR );

           }

        GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

         if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
           {


                return ( OSAL_DEVICE_CLOSE_ERROR );

           }
           else
           {



                if(bTimeOut == FALSE)
                {

                    u32RetVal = 0;

                }
                else
                {

                    u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;


                }

           }
        return(u32RetVal);

        }

}

/************************************************************************
 *FUNCTION:    u32GpsDuration2DFix_WarmStart
 *DESCRIPTION: Test case 7 of the SW Test specification for drv_gps Edition 1.1.This  requires GPS Receipt
                Important Note:This function should be called only after bGpsGetPersistentDataForWamBoot command is sent from the TTfis
                1.Starts the Device in the Warm boot
                2.Reads the Device continuously until 2D Fix happens
                3.Checks the time duration for the 2D fix is within the specified limits

 *PARAMETER:   Nil

 *
 *
 *RETURNVALUE: tU32,(0- Success or Error code).
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
               19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *
 *Initial Revision.
 ************************************************************************/

tU32  u32GpsDuration2DFix_WarmStart(tVoid)
{
    OSAL_tIODescriptor  hDevice = 0;
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
/*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;

    tU32 u32RetVal = 0;

    tU8 u8TestId = 6;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;



    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForWarmStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {

        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();



        for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }




            /**************Check for valid Data******************************************/


            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));

            if( (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_2D) ||
                (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D)   )

            {
               break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                 GPSStopTimer_oedt(phTimer);
                 OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                 return ( OSAL_GPS_STOP_ERROR );

           }

        GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

         if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
           {


                return ( OSAL_DEVICE_CLOSE_ERROR );

           }
           else
           {



                if(bTimeOut == FALSE)
                {

                    u32RetVal = 0;

                }
                else
                {

                    u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;

                }

           }
        return(u32RetVal);

        }


}

/************************************************************************
 *FUNCTION:    u32GpsDuration3DFix_WarmStart
 *DESCRIPTION: Test case 8 of the SW Test specification for drv_gps Edition 1.1.This  requires GPS Receipt
                Important Note:This function should be called only after bGpsGetPersistentDataForWamBoot command is sent from the TTfis
                1.Starts the Device in the Warm boot
                2.Reads the Device continuously until 3D Fix happens
                3.Checks the time duration for the 3D fix is within the specified limits

 *PARAMETER:    Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
                19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *
 *
  ************************************************************************/

tU32 u32GpsDuration3DFix_WarmStart( tVoid )
{
    OSAL_tIODescriptor  hDevice = 0;
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};

    /*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;

    tU32 u32RetVal = 0;
    tU8 u8TestId = 7;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;

    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForWarmStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {

            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }


            /************Check for valid Data**********************************************/


            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));

            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

            {
               break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                 GPSStopTimer_oedt(phTimer);
                 OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                 return ( OSAL_GPS_STOP_ERROR );

           }

        GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

         if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
           {


                return ( OSAL_DEVICE_CLOSE_ERROR );

           }
           else
           {



                if(bTimeOut == FALSE)
                {

                    u32RetVal = 0;

                }
                else
                {

                    u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;


                }

           }

        return(u32RetVal);

        }

}

/************************************************************************
 *FUNCTION:    u32GpsGetPersistentDataForWamBoot
 *DESCRIPTION: This gets  the internal data and the UTC time and stores on a Global variableThis  requires GPS Receipt
                     Important Notes   :a.This function should be called before any Command for testing time duration in warm boot is sent from the TTFis
                                                 b.This is a temporary command until FFS(Non Volatile memory) becomes stable in the platform.
                1.Starts the Device in the Cold boot
                2.Reads the Device continuously until Time Fix happens
                3.Reads the internal data and UTC time and puts it in Global variable

 *PARAMETER:    Nil


 *Global Variables:NIL

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     26.04.06  Ravindran P (CM-DI/ESA)
                19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *             10.07.10 Sainath Kalpuri (RBEI/ECF1)
                - OSAL_C_STRING_DEVICE_FFS2 device has been changed to OSAL_FFS_DEVICE
                  so that based on the availability any FFS device can be used
 *
 *
 ************************************************************************/

tU32 u32GpsGetPersistentDataForWamBoot( tVoid )
{

    OSAL_tIODescriptor hDevice = 0;
    OSAL_trGPSInternalData rGPSInternalData = {NULL,0};
    tS32    s32GetInternalDataSizeArg = 0;
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};

    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;

    /*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    tU32 u32RetVal = 0;
    tU8 u8TestId = 8;

    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0;
    tS32 s32BytesWritten = 0;

    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {

        OSAL_s32IOClose( hDevice );
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {
        hDevice = GPSRetVal.GPSIOHandle;
        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }



            /**************Step 9:Check for valid Data**************************************************/
            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));


            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )
            {
                break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device and then get the internal data**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
                == OSAL_ERROR )
        {
            GPSStopTimer_oedt(phTimer);
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return ( OSAL_GPS_STOP_ERROR );

        }

        if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE,(tS32)&s32GetInternalDataSizeArg ) == OSAL_ERROR )
        {
            GPSStopTimer_oedt(phTimer);
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close*/
            return ( OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR );

        }


        rGPSInternalData.u32BufSize =(tU32)s32GetInternalDataSizeArg;
        rGPSInternalData.pau8Buf =(tU8 *)OSAL_pvMemoryAllocate(rGPSInternalData.u32BufSize);
        OSAL_pvMemorySet((tPVoid)rGPSInternalData.pau8Buf,0x00,rGPSInternalData.u32BufSize);
        if(FALSE ==bTimeOut)
        {
            if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA,(tS32) &rGPSInternalData )
                    == OSAL_ERROR )
            {
                OSAL_vMemoryFree(rGPSInternalData.pau8Buf);
                GPSStopTimer_oedt(phTimer);
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return ( OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_ERROR );

            }

            /*Access flash for Persistent Data */
            hFile = OSAL_IOOpen (OSAL_FFS_DEVICE"/PersistentData.dat",enAccess);
            if( hFile == OSAL_ERROR )
            {
                if( OSAL_IOCreate (OSAL_FFS_DEVICE"/PersistentData.dat", enAccess) == OSAL_ERROR )
                {
                    GPSStopTimer_oedt(phTimer);
                    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                    OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                    return(OSAL_C_S32_FLASH_ACCESS_ERROR);

                }
                hFile = OSAL_IOOpen (OSAL_FFS_DEVICE"/PersistentData.dat",enAccess);
                if( hFile == OSAL_ERROR )
                {
                    GPSStopTimer_oedt(phTimer);
                    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                    OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
                    OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                    return(OSAL_C_S32_FLASH_ACCESS_ERROR);

                }
            }

            s32BytesWritten = OSAL_s32IOWrite (hFile,
            (tS8*)rGPSInternalData.pau8Buf,
            rGPSInternalData.u32BufSize);
            if ( s32BytesWritten != (tS32)rGPSInternalData.u32BufSize )
            {
                GPSStopTimer_oedt(phTimer);
                OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                OSAL_s32IOClose ( hFile );
                OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return(OSAL_C_S32_FLASH_ACCESS_ERROR+2);
            }


            OSAL_s32IOClose ( hFile );
            /*Access flash for UTC time */
            hFile = OSAL_IOOpen (OSAL_FFS_DEVICE"/UtcTime.dat",enAccess);
            if( hFile == OSAL_ERROR )
            {
                if( OSAL_IOCreate (OSAL_FFS_DEVICE"/UtcTime.dat", enAccess) == OSAL_ERROR )
                {
                    GPSStopTimer_oedt(phTimer);
                    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                    OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                    return(OSAL_C_S32_FLASH_ACCESS_ERROR);
                }
                hFile = OSAL_IOOpen (OSAL_FFS_DEVICE"/UtcTime.dat",enAccess);
                if( hFile == OSAL_ERROR )
                {
                    GPSStopTimer_oedt(phTimer);
                    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                    OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
                    OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                    return(OSAL_C_S32_FLASH_ACCESS_ERROR);

                }
            }

            s32BytesWritten = OSAL_s32IOWrite (hFile,
            (tS8*)&rGPSMeasuredPosition.trTime.uTime.trUTCTime,
            (tS32)(sizeof(OSAL_trGPSTimeUTC)));
            if ( s32BytesWritten != (tS32)(sizeof(OSAL_trGPSTimeUTC)) )
            {
                GPSStopTimer_oedt(phTimer);
                OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
                OSAL_s32IOClose ( hFile );
                OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                return(OSAL_C_S32_FLASH_ACCESS_ERROR+2);
            }

            OSAL_s32IOClose ( hFile );


        }

        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);

        GPSStopTimer_oedt(phTimer);
        /**************Close the device**************************************************/

        if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
        {

            return ( OSAL_DEVICE_CLOSE_ERROR );

        }
        else
        {


            if(bTimeOut == FALSE)
            {

                u32RetVal = 0;

            }
            else
            {

                u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;

            }

        }
        return(u32RetVal);

    }
}

/************************************************************************
 *FUNCTION:    u32GpsGetPinToggleFrontendTest
 *DESCRIPTION: 1.Opens the device
               2.Tests the Front end with a specific set bit pattern set by the SPI connectivity
                 and check the data pattern read by the data port at the front end

 *PARAMETER:None
 *

 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     04.06.07  Ravindran P (CM-DI/ESA)

 *
 *
 ************************************************************************/
tU32  u32GpsGetPinToggleFrontendTest( tVoid )
{
    OSAL_tIODescriptor hDevice = 0;
    tU32 u32PinToggleInitialValue = 0;

    tU32 u32RetVal = 0;
    OSAL_trGPSFrontEndTest rGPSFrontEndTest = {0,0};

    /********************Step1-Open the device*************************************************/
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {

         return(OSAL_DEVICE_OPEN_ERROR);

    }
    /********************Step2-set different patterns*************************************************/
    rGPSFrontEndTest.u8DataPattern = 4;
    rGPSFrontEndTest.u32PinToggle = u32PinToggleInitialValue;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST,
    (tS32)&rGPSFrontEndTest ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR );

    }
    else
    {
        if (OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_0000 !=
       ( rGPSFrontEndTest.u32PinToggle& OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111))
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return (OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR);
        }
    }

    rGPSFrontEndTest.u8DataPattern = 5;
    rGPSFrontEndTest.u32PinToggle = u32PinToggleInitialValue;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST,
    (tS32)&rGPSFrontEndTest ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR );

    }
    else
    {
        if (OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_0101 !=
       ( rGPSFrontEndTest.u32PinToggle& OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111))
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return (OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR);
        }
    }

    rGPSFrontEndTest.u8DataPattern = 6;
    rGPSFrontEndTest.u32PinToggle = u32PinToggleInitialValue;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST,
    (tS32)&rGPSFrontEndTest ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR );

    }
    else
    {
        if (OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_1010 !=
       ( rGPSFrontEndTest.u32PinToggle& OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111))
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return (OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR);
        }
    }

    rGPSFrontEndTest.u8DataPattern = 7;
    rGPSFrontEndTest.u32PinToggle = u32PinToggleInitialValue;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST,
    (tS32)&rGPSFrontEndTest ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR );

    }
    else
    {
        if (OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_1111 !=
       ( rGPSFrontEndTest.u32PinToggle& OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111))
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return (OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR);
        }
    }

    rGPSFrontEndTest.u8DataPattern = 4;
    rGPSFrontEndTest.u32PinToggle = u32PinToggleInitialValue;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST,
    (tS32)&rGPSFrontEndTest ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_PIN_TOGGLE_FRONT_END_ERROR );

    }
    else
    {
        if (OEDTGPS_PINTOGGLE_FRONTEND_PATTERN_0000 !=
       ( rGPSFrontEndTest.u32PinToggle& OEDTGPS_PINTOGGLE_FRONTEND_MASK_1111))
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return (OSAL_C_S32_PIN_TOGGLE_FRONT_END_READ_PATTERN_ERROR);
        }
    }

    /**************Step 3:Close the device **************************************************/
    if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
    {
        u32RetVal = OSAL_DEVICE_CLOSE_ERROR;
    }
    else
    {
         u32RetVal = 0;
    }
    return(u32RetVal);
}
/************************************************************************
 *FUNCTION:    u32GpsProductionTest
 *DESCRIPTION: This  requires GPS Receipt
                1.OPens the device
                1.Sets the approx.Geographical position and production test set up parameters
                2.Starts the Device in Factory mode and waits till production test is completed or time out
                3.Stops and closes the device

 *PARAMETER:  Nil

 *
 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     13.06.07  Ravindran P (CM-DI/EAP)
 *
 *Initial Revision.
 ************************************************************************/

tU32  u32GpsProductionTest( tVoid )
{
    OSAL_tIODescriptor hDevice = 0;
    OSAL_trGPSExtTestmodeStartupParams rGPSExtTestmodeStartupParams = {0,0};
    OSAL_trGPSExtTestmodeResults rGPSExtTestmodeResults ={ {0},0,0,0,0,0,0,0,0,0,0 };
    OSAL_trGPSIntitialPositionInDegLLA rGPSIntitialPositionInDegLLA ={0,0,0,0,0,0,0};
    OSAL_tenGPSStartupMode enGPSStartupMode =  GPS_STARTUP_FACTORY;
    tU32 u32RetVal = 0;
    OSAL_tTimerHandle phTimer = 0;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {

         return(OSAL_DEVICE_OPEN_ERROR);

    }

    /*12� 58� North of the Equator 77� 35�East of Greenwich - Geographical position of Bangalore!!*/

    rGPSIntitialPositionInDegLLA.s16LatDeg = 12;
    rGPSIntitialPositionInDegLLA.s16LatMin = 58;
    rGPSIntitialPositionInDegLLA.s16LatSec = 0;
    rGPSIntitialPositionInDegLLA.s16LonDeg = 77;
    rGPSIntitialPositionInDegLLA.s16LonMin = 35;
    rGPSIntitialPositionInDegLLA.s16LonSec = 0;
    rGPSIntitialPositionInDegLLA.s16Alt = 1200;/*Arbitrary altitude value in meters.Should not exceed 12000*/

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_POSITION,
    (tS32)&rGPSIntitialPositionInDegLLA ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_SET_INITIAL_POSITION_ERROR );

    }

    /*Set parameters for production test*/
    rGPSExtTestmodeStartupParams.u16GpsSearchMask = GPS_SEARCH_MASK;
    rGPSExtTestmodeStartupParams.u8Satellite = GPS_SV_PROD_TEST;

    if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_SETUP_EXTTESTMODE,
    (tS32)&rGPSExtTestmodeStartupParams ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_C_S32_SET_PARAM_PROD_TEST_ERROR );

    }

    /*Start the device in factory mode*/
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR);
    }

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32) 0 ) == OSAL_ERROR )
    {

         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return ( OSAL_GPS_START_ERROR );

    }

    /*Wait till rGPSExtTestmodeResults.u8GpsTestResult becomes TRUE or time out*/
    rGPSExtTestmodeResults.u8GpsTestResult = FALSE;
    phTimer = hGPSStartTimer_oedt();
    while( (rGPSExtTestmodeResults.u8GpsTestResult != TRUE) && (u32TimerCnt_oedt <= aGpsTimeOut_oedt[9]))
    {
        if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_EXTTESTMODE_RESULTS,
        (tS32)&rGPSExtTestmodeResults ) == OSAL_ERROR )
        {
            OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return ( OSAL_C_S32_GET_RESULTS_PROD_TEST_ERROR );
        }
    }
    GPSStopTimer_oedt(phTimer);


    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return ( OSAL_GPS_STOP_ERROR );

    }

    if(rGPSExtTestmodeResults.u8GpsTestResult != TRUE)
   {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_GPS_IOCTRL_TIME_OUT_ERROR );
   }

    if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
    {
        u32RetVal = OSAL_DEVICE_CLOSE_ERROR;
    }
    else
    {
         u32RetVal = 0;
    }
    return(u32RetVal);

}



/************************************************************************
 *FUNCTION:    hGPSStartTimer_oedt
 *DESCRIPTION: 1.starts the Periodic timer with 1 sec resolution

 *PARAMETER:None
 *

 *RETURNVALUE: Handle to the timer
 *
 *HISTORY:     04.04.06  Ravindran P (CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/
static OSAL_tTimerHandle hGPSStartTimer_oedt(tVoid)
{
    OSAL_tTimerHandle phTimer = 0;
    u32TimerCnt_oedt = 0;

    tPVoid pvarg = NULL;
    OSAL_tMSecond TimerCount = 1000 ;
    OSAL_tMSecond Timerinterval =1000;

    if(OSAL_s32TimerCreate((OSAL_tpfCallback)v_OedtTimerCallBack,pvarg,&phTimer) != OSAL_ERROR)
    {

        if(OSAL_s32TimerSetTime(phTimer,TimerCount,Timerinterval) != OSAL_ERROR)
        {
            return(phTimer);
        }
        else
        {
            return((OSAL_tTimerHandle)NULL);
        }
    }
    else
    {
        return((OSAL_tTimerHandle)NULL);
    }


}

/************************************************************************
 *FUNCTION:   GPSStopTimer_oedt
 *DESCRIPTION: 1.Stops the Periodic timer with 1 sec resolution

 *PARAMETER:None
 *

 *RETURNVALUE: Handle to the timer
 *
 *HISTORY:     04.04.06  Ravindran P (CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/




static tVoid GPSStopTimer_oedt(OSAL_tTimerHandle phTimer)
{
    u32TimerCnt_oedt= 0;
    OSAL_s32TimerSetTime(phTimer, 0, 0);  /*Stop the timer*/
    OSAL_s32TimerDelete( phTimer );/*Delete the timer*/

}

/************************************************************************
 *FUNCTION:    bGpsSeAllRecordFields
 *DESCRIPTION: Sets the Desired record fields

 *PARAMETER:
 *             OSAL_trGPSRecordFields sGPSRecordFields

 *RETURNVALUE: OSAL_trGPSRecordFields sGPSRecordFields
 *
 *HISTORY:     06.039.06  Ravindran P (CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/


static OSAL_trGPSRecordFields GPSSetAllRecordFields_oedt(OSAL_trGPSRecordFields sGPSRecordFields)
{
    sGPSRecordFields.u32FieldSpecifiers = 0;
    sGPSRecordFields.u32FieldTypes= 0;

    sGPSRecordFields.u32FieldSpecifiers|= OSAL_C_S32_GPS_FIELD_SPEC_TIME_UTC;
    sGPSRecordFields.u32FieldSpecifiers |= OSAL_C_S32_GPS_FIELD_SPEC_POS_LLA;
    sGPSRecordFields.u32FieldSpecifiers |= OSAL_C_S32_GPS_FIELD_SPEC_VEL_NED;


    sGPSRecordFields.u32FieldTypes|= OSAL_C_S32_GPS_FIELD_TYPE_MEASURED_POSITION;
    sGPSRecordFields.u32FieldTypes |= OSAL_C_S32_GPS_FIELD_TYPE_TRACKING_DATA;
    sGPSRecordFields.u32FieldTypes |= OSAL_C_S32_GPS_FIELD_TYPE_VISIBLE_LIST;
    sGPSRecordFields.u32FieldTypes|= OSAL_C_S32_GPS_FIELD_TYPE_SATELLITE_HEALTH;

    return(sGPSRecordFields);

}

/************************************************************************
 *FUNCTION:    PreparationsForColdStart_oedt
 *DESCRIPTION: 1.Performs the necessary steps before a IOread in Cold Boot starts

 *PARAMETER:
 *             odt_trTestFuncParm *prTestFuncParm

 *RETURNVALUE: OSAL_GPSResult.This contains the
                1.Handle of the opened device
                2.The Error No which gets mapped to the corresponding error in the array aGPSErr
 *
 *HISTORY:     06.039.06  Ravindran P (CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/


static OSAL_GPSResult PreparationsForColdStart_oedt( tVoid )
{
    OSAL_tIODescriptor  hDevice = 0;

    OSAL_tenGPSStartupMode enGPSStartupMode =  GPS_STARTUP_COLD;
    tS32 s32RetVal = 0;
    tU32 u32Version = 0;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;

  /********************Step1-Open the device*************************************************/


       hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );


       if ( hDevice == OSAL_ERROR)
       {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo =  1;
        return(GPSRetVal);
       }




  /***************************************************************/
  /************Step2:Start get Version****************************************************/

     if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version ) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 5;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }

  /***************************************************************/
  /************Step3:Set Start Up Mode****************************************************/


     if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 6;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }

    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         GPSRetVal.bResult = FALSE;
         GPSRetVal.GPSErrNo= 28;
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(GPSRetVal);
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         GPSRetVal.bResult = FALSE;
         GPSRetVal.GPSErrNo= 29;
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(GPSRetVal);
    }


     /************Step 5:Start the device ****************************************************/
    s32RetVal = OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0);

    if ( s32RetVal != OSAL_OK)
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         GPSRetVal.bResult = FALSE;
         GPSRetVal.GPSErrNo=2;
      }
    else
    {
        GPSRetVal.bResult = TRUE;
        GPSRetVal.GPSIOHandle = hDevice;
    }

    return(GPSRetVal);

}

/************************************************************************
 *FUNCTION:    PreparationsForWarmStart_oedt
 *DESCRIPTION: 1.Performs the necessary steps before a IOread in Warm Boot starts

 *PARAMETER:
 *             odt_trTestFuncParm *prTestFuncParm

 *RETURNVALUE: OSAL_GPSResult.This contains the
                1.Handle of the opened device
                2.The Error No which gets mapped to the corresponding error in the array aGPSErr
 *
 *HISTORY:     06.03t.06  Ravindran P (CM-DI/ESA)
 *             10.07.10 Sainath Kalpuri (RBEI/ECF1)
                - OSAL_C_STRING_DEVICE_FFS2 device has been changed to OSAL_FFS_DEVICE
                  so that based on the availability any FFS device can be used

 *
 *Initial Revision.
 ************************************************************************/



static OSAL_GPSResult PreparationsForWarmStart_oedt( tVoid )
{
    OSAL_tIODescriptor  hDevice = 0;

    OSAL_tenGPSStartupMode  enGPSStartupMode =  GPS_STARTUP_WARM;
    tS32  s32RetVal = 0;
    tU32 u32Version = 0;
    OSAL_GPSResult GPSRetVal;
    tS32    s32GetInternalDataSizeArg = 0;
    OSAL_trGPSInternalData  rGPSInternalData  = {NULL,0};
    OSAL_trGPSTimeUTC trGPSTimeUTC ={0,0,0,0,0,0,0};
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0;
    tS32 s32BytesRead = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0 ;
    /********************Step1-Open the device*************************************************/

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );


    if ( hDevice == OSAL_ERROR)
    {

        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo =  1;
        return(GPSRetVal);
    }


    /************Step2:Start get Version****************************************************/

    if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version ) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 5;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }


    /************Step3:Set Start Up Mode****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 6;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }
    /*************Step 4:Get internal data size*******/
    if ( (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE,(tS32)&s32GetInternalDataSizeArg ) == OSAL_ERROR) )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo =  7;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);

    }

    /*Copy the Persistent data to a local buffer*/
    rGPSInternalData.u32BufSize =(tU32)s32GetInternalDataSizeArg;
    rGPSInternalData.pau8Buf =(tU8 *)OSAL_pvMemoryAllocate(rGPSInternalData.u32BufSize);
    OSAL_pvMemorySet((tPVoid)rGPSInternalData.pau8Buf,0x00,rGPSInternalData.u32BufSize);
    /*Flash access for persistent data*/
    hFile = OSAL_IOOpen ( OSAL_FFS_DEVICE"/PersistentData.dat",enAccess );
    if( hFile == OSAL_ERROR )
    {
        OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 22;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }
    else
    {
        s32BytesRead = OSAL_s32IORead (
        hFile,
        (tS8*)rGPSInternalData.pau8Buf,
        rGPSInternalData.u32BufSize );
        if ( s32BytesRead != (tS32)(rGPSInternalData.u32BufSize) )
        {
            OSAL_s32IOClose ( hFile );
            OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
            OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
            GPSRetVal.bResult = FALSE;
            GPSRetVal.GPSErrNo= 22;
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return(GPSRetVal);
        }
        OSAL_s32IOClose ( hFile );

    }
    /*Flash access for UTC time*/
    hFile = OSAL_IOOpen ( OSAL_FFS_DEVICE"/UtcTime.dat",enAccess );
    if( hFile == OSAL_ERROR )
    {
        OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 22;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }
    else
    {
        s32BytesRead = OSAL_s32IORead (
        hFile,
        (tS8*)&trGPSTimeUTC,
        (tS32)(sizeof(OSAL_trGPSTimeUTC)) );
        if ( s32BytesRead != (tS32)(sizeof(OSAL_trGPSTimeUTC)) )
        {
            OSAL_s32IOClose ( hFile );
            OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
            OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
            GPSRetVal.bResult = FALSE;
            GPSRetVal.GPSErrNo= 22;
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return(GPSRetVal);
        }
        OSAL_s32IOClose ( hFile );

    }

    /**************Set internal data **************************************************/
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA,(tS32) &rGPSInternalData ) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo =  8;
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }

    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
    /*Get valid internal data-over*/

    /************Set the Default clock drift ****************************************************/


    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 28;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo= 29;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }


    /*Using ptrGPSMeasuredPosition from read record collect the time details in a specific structure nad pass on to the GPS GPS_SET_CURRENT_TIME*/

    /************Set the current time ****************************************************/

    /*Ravi-Time can be from RTC.UTC Time from flash is used for simulation of warm boot.TBD-source of current time*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME,(tS32)&trGPSTimeUTC ) == OSAL_ERROR )
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo = 11;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(GPSRetVal);
    }

    /************Start the device ****************************************************/
    s32RetVal = OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0);

    if ( s32RetVal != OSAL_OK)
    {
        GPSRetVal.bResult = FALSE;
        GPSRetVal.GPSErrNo=2;
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

    }
    else
    {
        GPSRetVal.bResult = TRUE;
        GPSRetVal.GPSIOHandle = hDevice;
    }

    return(GPSRetVal);

}

/************************************************************************
 *FUNCTION:    u32TestCriticalVoltageColdStart
 *DESCRIPTION: Test the IO controls for critical voltage
                1.Starts the Device in the cold boot
                2.Reads the Device continuously until 3D Fix happens or time out
                3.Start the low voltage condition
                4.Wait for 10sec
                5.End the low voltage condition
                6.Check if 3D fix happens as before

 *PARAMETER: Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     31.08.07  Ravindran P(RBIN/EDI3)
               19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *
 *Initial Revision.
 ************************************************************************/
tU32  u32TestCriticalVoltageColdStart( tVoid )
{
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tenGPSStartupMode  enGPSStartupMode =  GPS_STARTUP_COLD;
    tU8 u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP;
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};

    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    OSAL_GPSResult enGpsResult;

    tU8 u8WaitCnt = 0;

/*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    tU32 u32RetVal = 0;
    tU8 u8TestId = 5;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;


    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForColdStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }
         /**************Check for valid Data**********************************************/


            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));
            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

            {

                break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                 GPSStopTimer_oedt(phTimer);
                 OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                 return ( OSAL_GPS_STOP_ERROR );

           }
        /*Stop the timer */
        GPSStopTimer_oedt(phTimer);

        if(bTimeOut == TRUE)
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return ( OSAL_GPS_IOCTRL_TIME_OUT_ERROR );
        }

    }

        /*Set Critical voltage condition*/
    u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_START;
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE, (tS32)&u8CvmState )
        == OSAL_ERROR )
       {
             OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
             return ( OSAL_CVM_CRITICAL_LOW_VOLTAGE_START_ERROR );

       }
    /*Wait for 10 secs*/
    for(u8WaitCnt=0;u8WaitCnt<CVM_WAIT_TIME;u8WaitCnt++)
        OSAL_s32ThreadWait(WAIT_MSEC);
    /*Get back to normal condition */
    u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_END;
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE, (tS32)&u8CvmState )
        == OSAL_ERROR )
       {
             OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
             return ( OSAL_CVM_CRITICAL_LOW_VOLTAGE_END_ERROR );

       }
    /*Check if 3D fix happens again after change of states*/
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR);
    }

    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR );
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR );
    }


     /*Start the device*/
    if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0 )== OSAL_ERROR)
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_GPS_START_ERROR);
    }


    phTimer = hGPSStartTimer_oedt();

    for(;;)
    {


        /*Read a GPS record*/
        enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
        if(FALSE == enGpsResult.bResult)
        {
            OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
            return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
        }

        /**************Check for valid Data**************************************************/

        //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
        OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));

        if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

        {

            break;

        }

        else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

        {
            continue;

        }
        else
        {

            bTimeOut = TRUE;
            break;

        }
    }
    /**************Stop the device**************************************************/
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
    == OSAL_ERROR )
    {
         GPSStopTimer_oedt(phTimer);
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return ( OSAL_GPS_STOP_ERROR );

    }

    /*Stop the timer */
    GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

     if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
     {

            return ( OSAL_DEVICE_CLOSE_ERROR );

     }
     else
     {

            if(bTimeOut == FALSE)
            {

                u32RetVal = 0;

            }
            else
            {

                u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;


            }

     }
     return(u32RetVal);

}

/************************************************************************
 *FUNCTION:    u32TestCriticalVoltageWarmStart
 *DESCRIPTION: Test the IO controls for critical voltage
                1.Starts the Device in the Warm boot
                2.Reads the Device continuously until 3D Fix happens or time out
                3.Start the low voltage condition
                4.Wait for 10sec
                5.End the low voltage condition
                6.Check if 3D fix in warm boot happens as before

 *PARAMETER: Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:     03.09.07  Ravindran P(RBIN/EDI3)
               19.10.07  Ravindran P(RBIN/EDI3)
                -enGpsReadRecord added as replacement to read IOControls
 *             10.07.10 Sainath Kalpuri (RBEI/ECF1)
                - OSAL_C_STRING_DEVICE_FFS2 device has been changed to OSAL_FFS_DEVICE
                  so that based on the availability any FFS device can be used
 *
 ************************************************************************/

tU32 u32TestCriticalVoltageWarmStart( tVoid )
{
    OSAL_tIODescriptor   hDevice = 0;
    OSAL_tenGPSStartupMode          enGPSStartupMode =  GPS_STARTUP_WARM;
    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {NULL,0};
    tBool bTimeOut = FALSE;
    OSAL_GPSResult GPSRetVal;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    tU8 u8WaitCnt = 0;
    tU8 u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_AT_STARTUP;
    tS32    s32GetInternalDataSizeArg = 0;
    OSAL_trGPSInternalData rGPSInternalData = {NULL,0};
    OSAL_trGPSTimeUTC trGPSTimeUTC ={0,0,0,0,0,0,0};
    OSAL_trGPSXoCompensationData rGPSXoCompensationData ={0,0,0,0,0,0,0} ;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0;
    tS32 s32BytesRead = 0;

    /*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;
    OSAL_GPSResult enGpsResult;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    GPSRetVal.bResult = TRUE;
    GPSRetVal.GPSIOHandle = hDevice;
    GPSRetVal.GPSErrNo = 0;




    tU32 u32RetVal = 0;

    tU8 u8TestId = 7;


    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    GPSRetVal = PreparationsForWarmStart_oedt();

    if(GPSRetVal.bResult == FALSE)
    {
        return ( aGPSErr_oedt[GPSRetVal.GPSErrNo] );

    }
    else
    {

        hDevice = GPSRetVal.GPSIOHandle;

        phTimer = hGPSStartTimer_oedt();

        for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }
            /*************Check for valid Data**************************************************/


            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));
            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

            {
               break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }


        /**************Stop the device**************************************************/
        if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
            == OSAL_ERROR )
           {
                 GPSStopTimer_oedt(phTimer);
                 OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
                 return ( OSAL_GPS_STOP_ERROR );

           }

        GPSStopTimer_oedt(phTimer);

        if(bTimeOut == TRUE)
        {
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            return ( OSAL_GPS_IOCTRL_TIME_OUT_ERROR );
        }

    }

    /*Set Critical voltage condition*/
    u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_START;
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE, (tS32)&u8CvmState )
        == OSAL_ERROR )
       {
             OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
             return ( OSAL_CVM_CRITICAL_LOW_VOLTAGE_START_ERROR );

       }
    /*Wait for 10 secs*/
    for(u8WaitCnt=0;u8WaitCnt<CVM_WAIT_TIME;u8WaitCnt++)
        OSAL_s32ThreadWait(WAIT_MSEC);
    /*Get back to normal condition */
    u8CvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_END;
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE, (tS32)&u8CvmState )
        == OSAL_ERROR )
       {
             OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
             return ( OSAL_CVM_CRITICAL_LOW_VOLTAGE_END_ERROR );

       }

    /************Set Start Up Mode****************************************************/

    enGPSStartupMode =  GPS_STARTUP_WARM;
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR);
    }
/*************Step 4:Get internal data size*******/
    if ( (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE,(tS32)&s32GetInternalDataSizeArg ) == OSAL_ERROR) )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR);

    }

    /*Copy the Persistent data to a local buffer*/
    rGPSInternalData.u32BufSize =(tU32)s32GetInternalDataSizeArg;
    rGPSInternalData.pau8Buf =(tU8 *)OSAL_pvMemoryAllocate(rGPSInternalData.u32BufSize);
    OSAL_pvMemorySet((tPVoid)rGPSInternalData.pau8Buf,0x00,rGPSInternalData.u32BufSize);
   /*Flash access for persistent data*/
    hFile = OSAL_IOOpen ( OSAL_FFS_DEVICE"/PersistentData.dat",enAccess );
    if( hFile == OSAL_ERROR )
    {
        OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        return(OSAL_C_S32_FLASH_ACCESS_ERROR);
    }
    else
    {
        s32BytesRead = OSAL_s32IORead (
                            hFile,
                           (tS8*)rGPSInternalData.pau8Buf,
                            rGPSInternalData.u32BufSize );
        if ( s32BytesRead != (tS32)(rGPSInternalData.u32BufSize) )
        {
            OSAL_s32IOClose ( hFile );
            OSAL_s32IORemove ( OSAL_FFS_DEVICE"/PersistentData.dat" );
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
            return(OSAL_C_S32_FLASH_ACCESS_ERROR);
        }
        OSAL_s32IOClose ( hFile );

    }
  /*Flash access for UTC time*/
    hFile = OSAL_IOOpen ( OSAL_FFS_DEVICE"/UtcTime.dat",enAccess );
    if( hFile == OSAL_ERROR )
    {
        OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        return(OSAL_C_S32_FLASH_ACCESS_ERROR);
    }
    else
    {
        s32BytesRead = OSAL_s32IORead (
                            hFile,
                            (tS8*)&trGPSTimeUTC,
                            (tS32)(sizeof(OSAL_trGPSTimeUTC)) );
        if ( s32BytesRead != (tS32)(sizeof(OSAL_trGPSTimeUTC)) )
        {
            OSAL_s32IOClose ( hFile );
            OSAL_s32IORemove ( OSAL_FFS_DEVICE"/UtcTime.dat" );
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
            OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
            return(OSAL_C_S32_FLASH_ACCESS_ERROR);
        }
        OSAL_s32IOClose ( hFile );

    }

    /**************Set internal data **************************************************/
    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA,(tS32) &rGPSInternalData ) == OSAL_ERROR )
    {
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
        return(OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA_ERROR);
    }

    OSAL_vMemoryFree((tPVoid)rGPSInternalData.pau8Buf);
    /*Get valid internal data-over*/



    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR);
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR);
    }


     /*Using ptrGPSMeasuredPosition from read record collect the time details in a specific structure nad pass on to the GPS GPS_SET_CURRENT_TIME*/

     /************Set the current time ****************************************************/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME,(tS32)&trGPSTimeUTC ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME_ERROR);
    }

     /************Start the device ****************************************************/
    if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0 )== OSAL_ERROR)
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_GPS_START_ERROR);
    }

    phTimer = hGPSStartTimer_oedt();

    for(;;)
        {


            /*Read a GPS record*/
            enGpsResult = enGpsReadRecord(hDevice,& rReadRecordHeaderArgs);
            if(FALSE == enGpsResult.bResult)
            {
                OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
                OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
                return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
            }


            /*************Check for valid Data**********************************************/


            //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
            OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));

            if( rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D )

            {
               break;

            }

            else if(u32TimerCnt_oedt <= aGpsTimeOut_oedt[u8TestId])

            {
                continue;

            }
            else
            {

                bTimeOut = TRUE;
                break;

            }

        }

    /**************Stop the device**************************************************/
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
    == OSAL_ERROR )
    {
         GPSStopTimer_oedt(phTimer);
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return ( OSAL_GPS_STOP_ERROR );

    }

    /*Stop the timer */
    GPSStopTimer_oedt(phTimer);

    /**************Close the device**************************************************/

     if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
     {

            return ( OSAL_DEVICE_CLOSE_ERROR );

     }
     else
     {

            if(bTimeOut == FALSE)
            {

                u32RetVal = 0;

            }
            else
            {

                u32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;


            }

     }
     return(u32RetVal);


}

/************************************************************************
 *FUNCTION:    u32Check2DFixAfterSetHints
 *DESCRIPTION:  Set the geographical position as hints and then
                check for a 2D fix in cold start

 *PARAMETER:   Nil

 *
 *RETURNVALUE: tU32(0 - success or Error code - failure)
 *
 *HISTORY:  24.03.08  Ravindran P(RBEI/ECM1)
 *
 *Initial Revision.
 ************************************************************************/


tU32 u32Check2DFixAfterSetHints( tVoid )
{

    OSAL_trGPSReadRecordHeaderArgs rReadRecordHeaderArgs = {0};
    tBool bTimeOut = FALSE;
    OSAL_trGPSMeasuredPosition rGPSMeasuredPosition;
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tenGPSStartupMode  enGPSStartupMode =  GPS_STARTUP_COLD;
    OSAL_trGPSXoCompensationData rGPSXoCompensationData = {0};
    OSAL_trGPSHints rGPSHints;

    tS32 s32RetVal = 0;
    //tU8 u8TestId = 4;/*index in array of delays*/
    OSAL_GPSResult enGpsResult;

/*Timer related declarations*/
    OSAL_tTimerHandle phTimer = 0;

    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    OSAL_pvMemorySet((tPVoid)&rGPSMeasuredPosition,0,sizeof(OSAL_trGPSMeasuredPosition));
    /*******************Open the device*************************************************/


    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );


    if ( hDevice == OSAL_ERROR)
    {
        return( OSAL_DEVICE_OPEN_ERROR );
    }
    /*Set Start Up Mode*/


    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE,(tS32)enGPSStartupMode) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR);
    }

    /*Geogpraphical position of bangalore.Only dlat,dlon,dalt updated in fd_gps*/
    rGPSHints.rForwardPosition.enDatum = GPS_DATUM_WGS84;
    rGPSHints.rForwardPosition.dAlt =  920;    /*in m*/
    rGPSHints.rForwardPosition.dLat =  0.2263; /*in radian*/
    rGPSHints.rForwardPosition.dLon =  1.3536;  /*in radian*/
    rGPSHints.rForwardPosition.dAltMSL = 0.0;   /*as used in vdsensor*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_HINTS,(tS32)&rGPSHints ) == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return( OSAL_C_S32_IOCTL_GPS_SET_HINTS_ERROR );
    }

    /*XO compensation data - For Clock drift*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF_ERROR);
    }

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF,(tS32)&rGPSXoCompensationData) == OSAL_ERROR )
    {
         OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
         return(OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF_ERROR);
    }


     /************Start the device ****************************************************/
    s32RetVal = OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTL_GPS_START_DEVICE,(tS32)0);

    if ( s32RetVal != OSAL_OK)
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return(OSAL_GPS_START_ERROR);
    }

    phTimer = hGPSStartTimer_oedt();
    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;


    for(;;)
    {


        /*Read a GPS record*/
        enGpsResult = enGpsReadRecord(hDevice,&rReadRecordHeaderArgs);
        if(FALSE == enGpsResult.bResult)
        {
            OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
            OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
            return ( aGPSErr_oedt[enGpsResult.GPSErrNo] );
        }

        /**************Check for valid Data**************************************************/

        //ptrGPSMeasuredPosition =(OSAL_trGPSMeasuredPosition*)rReadRecordHeaderArgs.pBuffer;
        OSAL_pvMemoryCopy(&rGPSMeasuredPosition,rReadRecordHeaderArgs.pBuffer,sizeof(OSAL_trGPSMeasuredPosition));
        if( (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_2D) ||
            (rGPSMeasuredPosition.trSolutionMode.enSolutionType== GPS_SOLTYPE_LSQ_3D)   )

        {

            break;

        }
        else if(u32TimerCnt_oedt <= 600/*aGpsTimeOut_oedt[u8TestId]*/)
        {
            continue;

        }
        else
        {

            bTimeOut = TRUE;
            break;

        }

    }

    GPSStopTimer_oedt(phTimer);

    /**************Stop the device**************************************************/
    if( OSAL_s32IOControl( hDevice,OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 )
        == OSAL_ERROR )
    {
        OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
        return ( OSAL_GPS_STOP_ERROR );

    }



    /**************Close the device**************************************************/

    if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
    {

        return ( OSAL_DEVICE_CLOSE_ERROR );

    }
    else
    {
        if(bTimeOut == FALSE)
        {
            s32RetVal = 0;

        }
        else
        {
            s32RetVal = OSAL_GPS_IOCTRL_TIME_OUT_ERROR;

        }

    }
    return(tU32)(s32RetVal);

}




/************************************************************************
 *FUNCTION:    enGpsReadRecord
 *DESCRIPTION: Calls necessary IOControls to Read a record

 *PARAMETER: OSAL_tIODescriptor hDevice,
             OSAL_trGPSReadRecordHeaderArgs* prGPSReadRecordHeaderArgs

 *
 *RETURNVALUE: OSAL_GPSResult
 *
 *HISTORY:     19.10.07  Ravindran P(RBIN/EDI3)
 *             Initial version

 ************************************************************************/

static OSAL_GPSResult enGpsReadRecord(OSAL_tIODescriptor hDevice,
                            OSAL_trGPSReadRecordHeaderArgs* prGPSReadRecordHeaderArgs)
{

    OSAL_trGPSRecordHeader  rGpsHeader = {0,0,0,0,
                                          {{0,0,0}}
                                          };
    OSAL_trGPSRecordFields rGPSRecordFields = {0,0};
    OSAL_trGPSRecordFields rTmpGPSRecordFields = {0,0};
    tS32  s32GetNoRecordsAvailableArg = 0;
    OSAL_GPSResult enGpsResult;
    tU32  au32Record[(OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE + 3) / 4] = {0};
    enGpsResult.bResult = TRUE;
    enGpsResult.GPSIOHandle = hDevice;
    enGpsResult.GPSErrNo = 0;

    /*This call copies the the valid GPS information to the global variable which is used by the OSAL_s32IORead.It also updates the record ID*/

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE,
        (tS32)&s32GetNoRecordsAvailableArg ) == OSAL_ERROR )
    {
        enGpsResult.bResult = FALSE;
        enGpsResult.GPSErrNo= 14;
        return(enGpsResult);

    }

    if ( OSAL_s32IORead (hDevice,(tS8 *)&rGpsHeader,sizeof(rGpsHeader) )
    == OSAL_ERROR )
    {
        enGpsResult.bResult = FALSE;
        enGpsResult.GPSErrNo= 3;
        return(enGpsResult);

    }


    /************Set Desired Record Fields ****************************************************/

    rGPSRecordFields = GPSSetAllRecordFields_oedt(rTmpGPSRecordFields);

    if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS,
    (tS32)&rGPSRecordFields ) == OSAL_ERROR )
    {
        enGpsResult.bResult = FALSE;
        enGpsResult.GPSErrNo= 19;
        return(enGpsResult);
    }

    /*******Retrive the data using read record command**************************************************/

    prGPSReadRecordHeaderArgs->pBuffer = au32Record;

    prGPSReadRecordHeaderArgs->u32RecordId = rGpsHeader.u32RecordId;   /*This assures that the Record ID is the latest*/

    if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_READ_RECORD,
    (tS32)prGPSReadRecordHeaderArgs )== OSAL_ERROR )
    {
        enGpsResult.bResult = FALSE;
        enGpsResult.GPSErrNo= 19;
        return(enGpsResult);
    }

    return enGpsResult;
}


/********************************************************************************
 *FUNCTION     :  u32GpsDeviceOpenClosetest
 *DESCRIPTION  :  1. Open GPS device. If open success, again try to open the same device.
 *                   check for the error
 *
 *PARAMETER    :  Nil
 *RETURNVALUE  :  tU32(0 - success or Error code - failure)
 *
 *Test Number  :  TU_OEDT_GPS_04
 *HISTORY      :  23/05/2012 Jaikumar Dhanapal (RBEI/ECF5)
 *             Initial version

 ********************************************************************************/

tU32 u32GpsDeviceOpenClosetest(tVoid)
{
   OSAL_tIODescriptor hDevice_1 = 0;
   OSAL_tIODescriptor hDevice_2 = 0;
   tU32 u32RetVal = 0;

/*********************** Try to open an already opened device*********************/
   hDevice_1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE ); //opening the device first time
   if ( hDevice_1 == OSAL_ERROR)
   {
      u32RetVal =2;
   }
   else
   {
      hDevice_2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE ); /* Opening the GPS device which is already opened */
      if ( hDevice_2 == OSAL_ERROR) //checking whether the handle is valid or not
      {
         u32RetVal = 0;
      }
      else
      {
         OSAL_s32IOClose( hDevice_2 );/*Open pass at second time open.Hence close and delete the node*/

         u32RetVal+=4;
      }
      OSAL_s32IOClose( hDevice_1 );/*Open pass at first time open.Hence close and delete the node*/
   }
   return u32RetVal;
}

/************************************************************************
 *FUNCTION     :  u32GpsDeviceAntennaStatustest
 *DESCRIPTION  :  1. Get the Current Antenna status of the GPS device.
 *                2. Set a new Antenna status to the GPS device.
 *                3. Get the Current Antenna status.
 *                4. Check with the Antenna status which is set and which is retrieved. *
 *PARAMETER    :  Nil
 *RETURNVALUE  :  tU32(0 - success or Error code - failure)
 *
 *Test Number  :  TU_OEDT_GPS_05
 *HISTORY      :  31/05/2012 Jaikumar Dhanapal (RBEI/ECF5)
 *                Initial version

 ************************************************************************/
tU32 u32GpsDeviceAntennaStatustest(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   tBool bSetAntennaArg = TRUE;//Antenna ON
   tBool bGetAntennaArg = TRUE;
   tU32 u32RetVal = 0;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPS, OSAL_EN_READWRITE );

   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal += 2;
   }

   if ((u32RetVal == 0) && (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_START_DEVICE, (tS32) 0 ) == OSAL_ERROR ))
   {
      OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
      u32RetVal += 4;
   }

   if ((u32RetVal == 0) && (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_ANTENNA, (tS32)&bGetAntennaArg ) == OSAL_ERROR ))
   {
      OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
      OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
      u32RetVal += 8;
   }

   bSetAntennaArg = !bGetAntennaArg; /* Setting the opposite antenna status */

   if ((u32RetVal == 0) && (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_SET_ANTENNA, (tS32)bSetAntennaArg ) == OSAL_ERROR ))
   {
      OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
      OSAL_s32IOClose( hDevice );/*Open pass.Hence close */
      u32RetVal += 16;
   }

   if ((u32RetVal == 0) && (OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTL_GPS_GET_ANTENNA, (tS32)&bGetAntennaArg ) == OSAL_ERROR ))
   {
      OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
      OSAL_s32IOClose( hDevice );/*Open pass.Hence close and delete the node*/
      u32RetVal += 32;
   }

   if ((u32RetVal == 0) && (bGetAntennaArg != bSetAntennaArg))
   {
      u32RetVal += 64;
   }

   OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTL_GPS_STOP_DEVICE, (tS32)0 );/*Start pass.Hence stop*/
   OSAL_s32IOClose( hDevice );/*Open pass.Hence close */

   return u32RetVal;
}
