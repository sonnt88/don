/******************************************************************************
| FILE:         oedt_ComTest.cpp
| PROJECT:      GMGE
| SW-COMPONENT: OEDT FWK
|------------------------------------------------------------------------------
| DESCRIPTION:  This file contains part of the implementation for the OEDT 
|               communcation test. Especially the exported function for the 
|               test framework
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          |                       | Author   & comments
| --.--.--      | Initial revision        | -------, -----
| 17 Oct 2006   |  version 1.0            | Matthias Weise, initial version
|------------------------------------------------------------------------------
*******************************************************************************/

/******************************************************************************
 * Includes                                                                   *
 ******************************************************************************/
/*------  standard includes -------*/
#include "OsalConf.h"
extern void OSAL_tk_get_tim(SYSTIM* time);

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"

#define SYSTEM_S_IMPORT_INTERFACE_EXH
#include <stl_pif.h>

#include "ostrace.h"

// CAL includes
#include <adit_typedef.h>


// Mocca includes 
//#include "MoccaClient.h"
//#include "ADStream.h"
//#include "MoccaFramework.h"


#include "oedt_CCAMessageLoop.h"
#include "oedt_ComTest.h"
#include "oedt_helper_funcs.h"

/******************************************************************************
 * Forward declarations                                                       *
 ******************************************************************************/
static void vOsal1(void);
static void vOsal2(void);
static void vMocca1(void);
static void vMocca2(void);
static void vCal1(void);
static void vCal2(void);

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/
// Definition for Event that used to signal the end of a sub-test
OSAL_tEventMask    hEvMask     = 0x00000001;
OSAL_tEventMask    hEvResult   = 0x00000000;
OSAL_tEventHandle  hEvPerfTest = 0;

// Number of messages to send in one test. Has to be an ascending sequence ! (NO_OF_MSGCOUNT)
const tU32 u32NrOfMsg[]   = { 1000, 10000, 50000 };
// Sizes of messages to send in one test. Has to be an ascending sequence ! (NO_OF_MSGSIZES, MAX_MSG_LENGTH)
const tU32 u32SizeOfMsg[] = { 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };

// Global variable indicating if the reliable or simple mode shall be used for CAL send
bool useReliable = false;

/******************************************************************************
 * Enums                                                                      *
 ******************************************************************************/
enum ResultCodes 
{
	ComTest_OK,
	ComTest_ResourceError
};

/******************************************************************************
 *                                                                            *
 *                           OSAL Message Queue Test                          *
 *                                                                            *
 ******************************************************************************/
// OSAL Text mailbox name definition
static tCString szName1 = "MQ_Test1";
static tCString szName2 = "MQ_Test2";

/*****************************************************************************
 * FUNCTION:     u32ComPerfTest_OSALMailboxPerf(void)
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * DESCRIPTION:  Test the performaance of the OSAL mailboxes 
 * HISTORY:      Initial Version
 ******************************************************************************/   
tU32 u32ComPerfTest_OSALMailboxPerf(void)
{
	// Init event that is used to wait for the end of subtest 	
  if(OSAL_s32EventCreate("Perf_Test_Event", &hEvPerfTest) == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creating End of Test Event failed");
		return ComTest_ResourceError;
	}
	// Create first test thread
	OSAL_trThreadAttribute attr;
	attr.szName = "OSAL1";
	attr.u32Priority = THREAD_PRITORITY;
	attr.s32StackSize = THREAD_STACKSIZE;
	attr.pfEntry = (OSAL_tpfThreadEntry) vOsal1;
	attr.pvArg = NULL;
	OSAL_tThreadID ThreadID1 = OSAL_ThreadSpawn(&attr);
	if (ThreadID1 == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Spawning OSAL1 thread failed");
		return ComTest_ResourceError;
	}

  // Wait for completion of OSAL performance test
  OSAL_s32EventWait(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hEvResult);

	// Cleanup event
	if (OSAL_OK != OSAL_s32EventClose(hEvPerfTest))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing End of Test Event failed");
		return ComTest_ResourceError;
	}
	if (OSAL_OK != OSAL_s32EventDelete("Perf_Test_Event"))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Deleting End of Test Event failed");
		return ComTest_ResourceError;
	}

	return ComTest_OK;
}

/*****************************************************************************
 * FUNCTION:     vOsal1(void)
 * PARAMETER:    none
 * RETURNVALUE:  none
 * DESCRIPTION:  This is the thread function for the first thread for the OSAL
 *               message queue test. It sets up the message queues and starts 
 *               second thread. It starts the sending cycle and cleans up 
 *               aferwards and notifies the main function of the end of the test.
 * HISTORY:      Initial Version
 ******************************************************************************/   
static void vOsal1(void)
{
	tS32 RetValue;
	tU32 u32MaxMessages       = MAX_MSG;
	tU32 u32MaxLength         = MAX_MSG_LENGTH;
	OSAL_tenAccess enAccess   = OSAL_EN_READWRITE ;
	OSAL_tMQueueHandle Handle1;
	OSAL_tMQueueHandle Handle2;
	tU8 InMsg[MAX_MSG_LENGTH];
	tU8 OutMsg[MAX_MSG_LENGTH];
	tU32 dwCountRead     = 0;
	tU32 dwErrCountRead  = 0;
	tU32 dwCountWrite    = 0;
	tU32 dwErrCountWrite = 0;
	OSAL_tMSecond mSecStart;
	OSAL_tMSecond mSecStop;
	
	// prepare messages
	for(tU32 i=0; i < MAX_MSG_LENGTH; i++) 
	{
		InMsg[i] = 0;
		OutMsg[i] = i;
	}

	tU32 Prio;
	// prepare connection
	RetValue = OSAL_s32MessageQueueCreate(szName1,
	                                      u32MaxMessages,
	                                      u32MaxLength,
	                                      enAccess,
	                                      &Handle1);
  if(RetValue != OSAL_OK) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"First OSAL Message Queue creation failed"); 
		OSAL_s32MessageQueuePost(hEvPerfTest, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO);
		return;
	}
	RetValue = OSAL_s32MessageQueueCreate(szName2,
	                                      u32MaxMessages,
	                                      u32MaxLength,
	                                      enAccess,
	                                      &Handle2);
  if(RetValue != OSAL_OK) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Second OSAL Message Queue creation failed");
		OSAL_s32MessageQueuePost(hEvPerfTest, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO);		
		return;
	}

	// create second thread for echoing messages
	OSAL_tThreadID ThreadID2;
	OSAL_trThreadAttribute  attr;
	attr.szName = "OSAL2";
	attr.u32Priority = THREAD_PRITORITY;
	attr.s32StackSize = THREAD_STACKSIZE;
	attr.pfEntry = (OSAL_tpfThreadEntry) vOsal2;
	attr.pvArg = NULL;
	ThreadID2 = OSAL_ThreadSpawn(&attr);
	if (ThreadID2 == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creation of second OSAL Message Queue Thread failed");
		OSAL_s32MessageQueuePost(hEvPerfTest, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO);		
		return;
	}

	// wait for start (sync)
	while(InMsg[0] != START_MSG) 
	{
		OSAL_s32ThreadWait(1000);
		OSAL_s32MessageQueueWait(Handle1, (tPU8)InMsg, u32MaxLength, &Prio,	(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER);
	}
	for(tU8 currSize = 0; currSize < NO_OF_MSGSIZES; currSize++)
	{
		for(tU8 currNr = 0; currNr < NO_OF_MSGCOUNT; currNr++) 
		{
			// store start time
			mSecStart = OSAL_ClockGetElapsedTime();

			// start message loop
			for(tU32 i = 0;i<u32NrOfMsg[currNr];i++) 
			{
				// send message
				if(OSAL_s32MessageQueuePost(Handle2, (tPCU8)OutMsg, u32SizeOfMsg[currSize], MSG_PRIO) == OSAL_OK) 
				{
					dwCountWrite++;
				}
				else 
				{
					dwErrCountWrite++;
				}
	
				// receive reply
				if(OSAL_s32MessageQueueWait(Handle1, (tPU8)InMsg, u32MaxLength, &Prio, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) > 0)
				{
					dwCountRead++;
				}
				else
				{
					dwErrCountRead++;
				}
			}
			// store end time
			mSecStop = OSAL_ClockGetElapsedTime();

			OEDT_HelperPrintf(TR_LEVEL_USER_1, "OSAL Messung %u Messages with Size %u in %lu msec",
			                  (unsigned int) u32NrOfMsg[currNr],
			                  (unsigned int) u32SizeOfMsg[currSize],
			                  mSecStop-mSecStart);
		}
	}
	
	// prepare shutdown
	OutMsg[0] = END_MSG;
	if (OSAL_OK != OSAL_s32MessageQueuePost(Handle2, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Sending of the final OSAL MQ Test message failed");
	}
	OSAL_s32ThreadWait(5000);
	if (OSAL_OK != OSAL_s32EventPost(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Posting OSAL MQ Test end event failed");
	}
	if (OSAL_OK != OSAL_s32MessageQueueClose(Handle1))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing first OSAL message queue failed");
	}
	if (OSAL_OK != OSAL_s32MessageQueueClose(Handle2))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing second OSAL message queue failed");
	}
	if (OSAL_OK != OSAL_s32MessageQueueDelete(szName1))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Deleting first OSAL message queue failed");
	}
	if (OSAL_OK != OSAL_s32MessageQueueDelete(szName2))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Deleting first OSAL message queue failed");
	}
}

/*****************************************************************************
 * FUNCTION:     vOsal2(void)
 * PARAMETER:    none
 * RETURNVALUE:  none
 * DESCRIPTION:  This is the thread function for the second thread for the OSAL
 *               message queue test. It opens the message queues and sends the 
 *               receivd mesasages back.
 * HISTORY:      Initial Version
 ******************************************************************************/   
static void vOsal2(void)
{
	tS32 RetValue;
	tU32 u32MaxLength         = MAX_MSG_LENGTH;
	OSAL_tenAccess enAccess   = OSAL_EN_READWRITE ;
	OSAL_tMQueueHandle Handle1;
	OSAL_tMQueueHandle Handle2;
	tU32 Prio;
	tU8 InMsg[MAX_MSG_LENGTH];
	tU8 OutMsg[MAX_MSG_LENGTH];
	tU32 dwCountRead = 0, dwErrCountRead = 0;
	tU32 dwCountWrite = 0, dwErrCountWrite = 0;
	int i;
	
	// prepare messages
	for(i= 0;i < MAX_MSG_LENGTH;i++) 
	{
		InMsg[i] = 0;
		OutMsg[i] = i;
	}

	// prepare connection
	RetValue = OSAL_s32MessageQueueOpen(szName1, enAccess, &Handle1);
	if(RetValue != OSAL_OK) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Opening first OSAL Message Queue failed");
		return;
	}

	RetValue = OSAL_s32MessageQueueOpen(szName2, enAccess, &Handle2);
	if(RetValue != OSAL_OK) {
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Opening first OSAL Message Queue failed");
		return;
	}

	// start measurement (sync)
	OutMsg[0] = START_MSG;
	while(OSAL_s32MessageQueuePost(Handle1, (tPCU8)OutMsg, MAX_MSG_LENGTH, 2) == OSAL_ERROR) 
	{
		OSAL_s32ThreadWait(5000);
	}

	// start message loop
	char bRun = TRUE;
	while(bRun)
	{
		// receive message
		if(OSAL_s32MessageQueueWait(Handle2, (tPU8)InMsg, u32MaxLength, &Prio, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) > 0) 
		{
			dwCountRead++;
			if(InMsg[0] == END_MSG) 
			{
				bRun = FALSE;
				break;
			}
		}
		else 
		{
			dwErrCountRead++;
		}
	
		// send reply
		if(OSAL_s32MessageQueuePost(Handle1, (tPCU8)OutMsg, MAX_MSG_LENGTH, 2) == OSAL_OK) 
		{
			dwCountWrite++;
		}
		else
		{
			dwErrCountWrite++;
		}
	}
	
	if (OSAL_OK != OSAL_s32MessageQueueClose(Handle1))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing first OSAL message queue failed");
	}
	if (OSAL_OK != OSAL_s32MessageQueueClose(Handle2))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing second OSAL message queue failed");
	}
}


/******************************************************************************
 *                                                                            *
 *                                 Mocca Test                                 *
 *                                                                            *
 ******************************************************************************/

/*****************************************************************************
 * FUNCTION:     u32ComPerfTest_MoccaPerf(void)
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * DESCRIPTION:  Test the performaance of the OSAL mailboxes 
 * HISTORY:      Initial Version
 ******************************************************************************/   
tU32 u32ComPerfTest_MoccaPerf(void)
{
	// Init event that is used to wait for the end of subtest 	
  if(OSAL_s32EventCreate("Perf_Test_Event", &hEvPerfTest) == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creating End of Test Event failed");
		return ComTest_ResourceError;
	}
	OSAL_trThreadAttribute attr;
	attr.szName = "MOCCA1";
	attr.u32Priority = THREAD_PRITORITY;
	attr.s32StackSize = THREAD_STACKSIZE;
	attr.pfEntry = (OSAL_tpfThreadEntry) vMocca1;
	attr.pvArg = NULL;
	OSAL_tThreadID ThreadID1 = OSAL_ThreadSpawn(&attr);
	if (ThreadID1 == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning MOCCA thread failed");
		return ComTest_ResourceError;
	}
	
	// wait for completion of MOCCA performance test
	OSAL_s32EventWait(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,&hEvResult);
	
	// Cleanup event
	if (OSAL_OK != OSAL_s32EventClose(hEvPerfTest))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing End of Test Event failed");
		return ComTest_ResourceError;
	}
	if (OSAL_OK != OSAL_s32EventDelete("Perf_Test_Event"))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Deleting End of Test Event failed");
		return ComTest_ResourceError;
	}
	
	return ComTest_OK;
}

/*****************************************************************************
 * FUNCTION:     vMocca1(void)
 * PARAMETER:    none
 * RETURNVALUE:  none
 * DESCRIPTION:  This is the thread function for the first thread for the Mocca
 *               test. It initializes Mocca and starts second thread. It starts 
 *               the sending cycle and cleans up aferwards and notifies the main 
 *               function of the end of the test.
 * HISTORY:      Initial Version
 ******************************************************************************/   
static void vMocca1(void)
{
#if MOCCA_AVAILABLE
	MoccaClientHandle_t hConnection;
	ui8 InMsg[MAX_MSG_LENGTH];
	ui8 OutMsg[MAX_MSG_LENGTH];
	ui32 dwMsg;
	ui32 dwCountRead     = 0;
	ui32 dwErrCountRead  = 0;
	ui32 dwCountWrite    = 0;
	ui32 dwErrCountWrite = 0;
	OSAL_tMSecond mSecStart;
	OSAL_tMSecond mSecStop;
	ui8 cEventChannel = 18 ;
	mocca::CMessage msg;
	mocca::CMessageEvent msgEvent;
	ui8 group;
	ui16 bufLength = MAX_MSG_LENGTH;
	
	// prepare messages
	for(int i= 0;i < MAX_MSG_LENGTH;i++) 
	{
		InMsg[i] = 0;
		OutMsg[i] = i;
	}
	
	OutMsg[0] = 0;  // Message Type
	
	if(MoccaClientInitialize()) 
	{
		if(MoccaClientConnect(&hConnection)) 
		{
			if (MoccaClientEventChannelsJoin(hConnection, &cEventChannel, 1)) 
			{
				// create second thread for echoing messages
				OSAL_tThreadID ThreadID2;
				OSAL_trThreadAttribute  attr;
				attr.szName = "MOCCA2";
				attr.u32Priority = THREAD_PRITORITY;
				attr.s32StackSize = THREAD_STACKSIZE;
				attr.pfEntry = (OSAL_tpfThreadEntry) vMocca2;
				attr.pvArg = NULL;
				ThreadID2 = OSAL_ThreadSpawn(&attr);
				if (ThreadID2 == OSAL_ERROR) 
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creation of second Mocca Thread failed");
					OSAL_s32MessageQueuePost(hEvPerfTest, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO);		
					return;
				}

				// wait for start (sync)
				while(InMsg[0] != START_MSG) 
				{
					OSAL_s32ThreadWait(1000);
					if(MoccaClientMessageRead(hConnection, InMsg, MAX_MSG_LENGTH, &dwMsg)) 
					{
						if(msg.DeSerialize(InMsg, static_cast<ui16>(dwMsg))) 
						{
							if(msg.IsEventMessage()) 
							{
								msgEvent.FromMessage(msg); // get CEventMessage from CMessage
								group = msgEvent.GetGroup();
								if(group == 18) 
								{
								}
							}
						}
					}
				}

				// prepare outgoing message
				mocca::CMessageEvent eMsg;
				if(!eMsg.Serialize(OutMsg, sizeof(OutMsg), bufLength)) 
				{
					OEDT_HelperPrintf(TR_LEVEL_FATAL,"Serialization of message failed");
					OSAL_s32MessageQueuePost(hEvPerfTest, (tPCU8)OutMsg, MAX_MSG_LENGTH, MSG_PRIO);		
					return;
				}
	
				for(tU8 currSize = 0; currSize < NO_OF_MSGSIZES; currSize++)
				{
					for(tU8 currNr = 0; currNr < NO_OF_MSGCOUNT; currNr++) 
					{
						// store start time
						mSecStart = OSAL_ClockGetElapsedTime();
						// start message loop
						for(tU32 i=0; i<u32NrOfMsg[currNr]; i++) 
						{
							// send message
							if(MoccaClientMessageWrite(hConnection, OutMsg, u32SizeOfMsg[currSize])) 
							{
								dwCountWrite++;
							}
							else 
							{
								dwErrCountWrite++;
							}
							// receive reply
							if(MoccaClientMessageRead(hConnection, InMsg, u32SizeOfMsg[currSize], &dwMsg)) 
							{
								dwCountRead++;
							}
							else 
							{
								dwErrCountRead++;
							}
						}
						// store end time
						mSecStop = OSAL_ClockGetElapsedTime();
						OEDT_HelperPrintf(TR_LEVEL_USER_1, "MOCCA Messung %d Messages with Size %d in %d msec",
						                  u32NrOfMsg[currNr],
						                  u32SizeOfMsg[currSize],
						                  mSecStop-mSecStart);
					}
				}
				// prepare shutdown
				OutMsg[0] = END_MSG;
				MoccaClientMessageWrite(hConnection, OutMsg, MAX_MSG_LENGTH);
				MoccaClientDisconnect(hConnection);
			}
			MoccaClientTerminate();
		}
	}
	OSAL_s32ThreadWait(5000);
	OSAL_s32EventPost(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR);
#endif
	OSAL_s32EventPost(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR);
}

/*****************************************************************************
 * FUNCTION:     vMocca2(void)
 * PARAMETER:    none
 * RETURNVALUE:  none
 * DESCRIPTION:  This is the thread function for the second thread for the MOCCA
 *               tests. It receives the messages and echoes them back.
 * HISTORY:      Initial Version
 ******************************************************************************/   
static void vMocca2(void)
{
#if MOCCA_AVAILABLE
	MoccaClientHandle_t hConnection;
	ui8 InMsg[MAX_MSG_LENGTH];
	ui8 OutMsg[MAX_MSG_LENGTH];
	ui32 dwMsg;
	ui32 dwCountRead     = 0;
	ui32 dwErrCountRead  = 0;
	ui32 dwCountWrite    = 0;
	ui32 dwErrCountWrite = 0;
	OutMsg[0] = 18; // SPM channel
	OutMsg[1] = 0;  // Message Type
	ui8 cEventChannel = 18 ;
	mocca::CMessage msg;
	mocca::CMessageEvent msgEvent;
	
	// prepare messages
	for(int i= 0;i < MAX_MSG_LENGTH;i++) 
	{
		InMsg[i] = 0;
		OutMsg[i] = i;
	}
	
	if(MoccaClientConnect(&hConnection))
	{
		if(MoccaClientEventChannelsJoin(hConnection, &cEventChannel, 1)) 
		{
			// start measurement (sync)
			OutMsg[0] = START_MSG;  // message Type
			while(!MoccaClientMessageWrite(hConnection, OutMsg, MAX_MSG_LENGTH)) 
			{
				tkse_slp_tsk(1000);
			}
			// start message loop
			OutMsg[0] = 0;  // message Type
			bool bRun = true;
			while(bRun) 
			{
				// receive message
				if(MoccaClientMessageRead(hConnection, InMsg, MAX_MSG_LENGTH, &dwMsg)) 
				{
					dwCountRead ++;
					if(END_MSG == InMsg[0]) 
					{
						bRun = false;
						break;
					}
				}
				else 
				{
					dwErrCountRead ++;
				}
				// send reply
				if(MoccaClientMessageWrite(hConnection, OutMsg, MAX_MSG_LENGTH)) 
				{
					dwCountWrite++;
				}
				else 
				{
					dwErrCountWrite++;
				}
			}
		}
		MoccaClientDisconnect(hConnection);
	}
	MoccaClientTerminate();
#endif
}


/******************************************************************************
 *                                                                            *
 *                                  CCA Test                                  *
 *                                                                            *
 ******************************************************************************/
#if 0 /* commented for Gen2 */
/*****************************************************************************
 * FUNCTION:     u32ComPerfTest_CCAPerf(void)
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * DESCRIPTION:  Test the performaance of the OSAL mailboxes 
 * HISTORY:      Initial Version
 ******************************************************************************/   
tU32 u32ComPerfTest_CCAPerf(void)
{
	// Init event that is used to wait for the end of subtest 	
  if(OSAL_s32EventCreate("Perf_Test_Event", &hEvPerfTest) == OSAL_ERROR) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creating End of Test Event failed");
		return ComTest_ResourceError;
	}
	// Init SCD (per process)
	if (! scd_init()) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "SCD inir failed");
		return ComTest_ResourceError;
	}
	// Init Exception handling (per process) 
	if (! exh_bInitExceptionHandling())
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "exception handling init failed");
		return ComTest_ResourceError;
	}
	// Create and start the CCA tasks
	CCAMessageLoop *task1 = new CCAMessageLoop("CCA1", CCA_C_U16_APP_CT_MEASURE, CCA_C_U16_APP_CT_ECHO, CCA_C_U16_SRV_COMTEST); 
	CCAMessageLoop *task2 = new CCAMessageLoop("CCA2", CCA_C_U16_APP_CT_ECHO, CCA_C_U16_APP_CT_MEASURE, CCA_C_U16_SRV_COMTEST);
	if ( ! task1->vStart())
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "Starting CCA Task 1 failed");
		return ComTest_ResourceError;
	}
	if (! task2->vStart())
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "Starting CCA Task 2 failed");
		return ComTest_ResourceError;
	}
	// wait for completion of CCA performance test
	OSAL_s32EventWait(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hEvResult);
	scd_exit();
	exh_vFreeExceptionHandling();
	task1->vDeinitInstance();
	task2->vDeinitInstance();
	delete task1;
	delete task2;

	// Cleanup event
	if (OSAL_OK != OSAL_s32EventClose(hEvPerfTest))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Closing End of Test Event failed");
		return ComTest_ResourceError;
	}
	if (OSAL_OK != OSAL_s32EventDelete("Perf_Test_Event"))
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Deleting End of Test Event failed");
		return ComTest_ResourceError;
	}
	
	return ComTest_OK;
}
#endif

