/******************************************************************************
 *FILE         : oedt_Auxclock_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the  test cases for the auxclock 
 *               device.Ref:SW Test specification 0.1 draft
 *               
 *AUTHOR       :Ravindran P(CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 06.04.06 .Initial version
 				23.03.09 - Ravindran P (RBEI/ECF1) - Compiler/LINT warning removal
 *				
 *****************************************************************************/


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define	AUXCLOCK_DEVICE_OPEN_ERROR							35
#define	AUXCLOCK_GET_RESOLUTION_ERROR						36
#define	AUXCLOCK_GET_CYCLETIME_ERROR						37
#define	AUXCLOCK_FREQUENCY_CYCLETIME_PRODUCT_ERROR			38
#define	AUXCLOCK_IOREAD_BEFORE_DELAY_ERROR					39
#define	AUXCLOCK_IOREAD_AFTER_DELAY_ERROR					40
#define	AUXCLOCK_TIME_VALIDITY_ERROR						41
#define	AUXCLOCK_DEVICE_CLOSE_ERROR							42
#define	AUXCLOCK_DEVICE_UNKNOWN_ERROR						43
#define	AUXCLOCK_GETVERSION_ERROR							44
#define	AUXCLOCK_POSIXIATE_ERROR							45
#define	AUXCLOCK_OSAL_ERROR									46
#define	POSIX_TIME_INCREMENT_ERROR							47
#define	POSIX_TIME_DECREMENT_ERROR						    48																								


tU8 aAuxClockErr_oedt[] = {
					AUXCLOCK_DEVICE_OPEN_ERROR	,							//  0
					AUXCLOCK_GET_RESOLUTION_ERROR	,  						//	1	
					AUXCLOCK_GET_CYCLETIME_ERROR,                           //	2
					AUXCLOCK_FREQUENCY_CYCLETIME_PRODUCT_ERROR,             //	3
					AUXCLOCK_IOREAD_BEFORE_DELAY_ERROR ,                    /// 4
					AUXCLOCK_IOREAD_AFTER_DELAY_ERROR,                      //  5
					AUXCLOCK_TIME_VALIDITY_ERROR ,              		    //  6        			
					AUXCLOCK_DEVICE_CLOSE_ERROR	,    		                //  7
					AUXCLOCK_DEVICE_UNKNOWN_ERROR,							//  8
					AUXCLOCK_GETVERSION_ERROR,								//  9
					AUXCLOCK_POSIXIATE_ERROR,								//  10
					AUXCLOCK_OSAL_ERROR,  									//  11
					POSIX_TIME_INCREMENT_ERROR,								//  12
					POSIX_TIME_DECREMENT_ERROR								//  13
					};

#define	SCALE_FACTOR_SECS_MUL_BY_100	10000000         /*To get seconds in Multiples of 100 so check for +/- 1% is possible*/
#define	SECS_LO_LIMIT						99
#define	SECS_HI_LIMIT						101

/*****************************************************************
|Function definitions 	(scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/



/*****************************************************************
| function implementation (scope: module-local)
*****************************************************************/




/*****************************************************************************
* FUNCTION:		tu32AuxclockOpenCloseDev()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Opens and closes the specified AUXCLK.
* HISTORY:		Created by Martin Langer (CM-AI/PJ-CF33) on  2011-09-13
*               
******************************************************************************/
tU32 tu32AuxclockOpenCloseDev(void)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tU32 u32Status = 0;

	/* Channel Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_AUXILIARY_CLOCK, enAccess);
	if(OSAL_ERROR != hFd)
	{
		/* Channel Close */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Status = OSAL_u32ErrorCode();
			u32Status &= ~OSAL_C_COMPONENT;
			u32Status &= ~OSAL_C_ERROR_TYPE;			
			u32Ret += 100 + u32Status;
		}
	}
	else
	{
		/* check the error status returned */
		u32Status = OSAL_u32ErrorCode();
		u32Status &= ~OSAL_C_COMPONENT;
		u32Status &= ~OSAL_C_ERROR_TYPE;			
		u32Ret += 200 + u32Status;
	}
	return u32Ret;	

}

/************************************************************************
 *FUNCTION:    tu32AuxclockCheckTimeStampValiditySeq 
 *DESCRIPTION: Checks the complete functionality of the AuxClock
 				1.Opens the device
 				2.Gets the clock resolution in hertz
 				3.Gets the cycle time in Nano seconds.
 				4.Check whehter the product of product of cycle time and frequency is +/- 1% of 1 sec.
 				5.Check the clock frequency is +/- 1% of Actual value
 				6.Close the Device
 *PARAMETER:   Nil
 *RETURNVALUE: Error code

 *
 *HISTORY:     13.04.06(Ravindran P-CM-DI/ESA)
 *
 *Initial Revision.
 ************************************************************************/


tU32 tu32AuxclockCheckTimeStampValiditySeq( void  )
{
	OSAL_tIODescriptor hDevice = 0;
	OSAL_trIOCtrlAuxClockTicks rIOAuxData1 ={0,0};
	OSAL_trIOCtrlAuxClockTicks rIOAuxData2 ={0,0};
	
	tBool bErrorOccured = FALSE;
	tU8   u8ErrorNo = 0;
	tU32 u32ResInHertz = 0;
	tU32  u32ResInNano = 0;
	tU32 u32TimePeriod = 1000;
	tU32 u32FreqLoLimit = 0;
	tU32 u32FreqHiLimit = 0;
	/* tU32 u32ErrorPercentage = 1; */         /*As per the specifications*/
	tU32 u32RetVal = 0;

	

	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_AUXILIARY_CLOCK, OSAL_EN_READWRITE );
   	if ( hDevice == OSAL_ERROR)
   	{
   		bErrorOccured = TRUE;
		u8ErrorNo = 0;
      		
	}
	
	if(FALSE == bErrorOccured)
	{
		if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION,(tS32)&u32ResInHertz) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 1;
			OSAL_s32IOClose ( hDevice );				
		}
		      	
	}

	if(FALSE == bErrorOccured)
	{
		if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME,(tS32)&u32ResInNano) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 2;
			OSAL_s32IOClose ( hDevice );
			
		}
		      	
	}

	/*Check whehter the product of product of cycle time and frequency is +/- 1% of 1 sec. */
	if(FALSE == bErrorOccured)
	{
		if (( ((u32ResInHertz*u32ResInNano)/(SCALE_FACTOR_SECS_MUL_BY_100) )< SECS_LO_LIMIT)  || (((u32ResInHertz*u32ResInNano)/(SCALE_FACTOR_SECS_MUL_BY_100) )> SECS_HI_LIMIT) )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 3;
			OSAL_s32IOClose ( hDevice );
				
		}
	  	
	}

	if(FALSE == bErrorOccured)
	{
		if (  OSAL_s32IORead (hDevice,(tS8 *)&rIOAuxData1,sizeof(rIOAuxData1) )== OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 4;
			OSAL_s32IOClose ( hDevice );
				
		}
      	
	}

	if(FALSE == bErrorOccured)
	{
		 if( OSAL_s32ThreadWait(u32TimePeriod) == OSAL_ERROR)
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 8;
			OSAL_s32IOClose ( hDevice );
				
		}
      	
	}

	if(FALSE == bErrorOccured)
	{
		if (  OSAL_s32IORead (hDevice,(tS8 *)&rIOAuxData2,sizeof(rIOAuxData2) )== OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 5;
			OSAL_s32IOClose ( hDevice );
				
		}
      	
	}

         /*Check the clock frequency is +/- 1% of Actual value*/
    u32FreqLoLimit =u32ResInHertz - (u32ResInHertz/100);  /*For a error percentage of +/- 1% of Resolution .Resolution = 1000*/
	u32FreqHiLimit =u32ResInHertz + (u32ResInHertz/100); 	 
   		 
	if(FALSE == bErrorOccured)
	{
				
		if( rIOAuxData2.u32High == rIOAuxData1.u32High)  /*The condition can fail approx after 50days.Hence Else condition is not considered*/
		{
			
			if ( ((rIOAuxData2.u32Low- rIOAuxData1.u32Low) <(u32FreqLoLimit)) || ((rIOAuxData2.u32Low- rIOAuxData1.u32Low) >(u32FreqHiLimit ))  )
			{
				bErrorOccured = TRUE;
				u8ErrorNo = 6;
				OSAL_s32IOClose ( hDevice );
					
			}
		}
		
      	
	}

	if(FALSE == bErrorOccured )
	{

		if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 7;
		}
		
	}
   

	if( FALSE == bErrorOccured )
	{ 
	
		u32RetVal = 0;			
	
	}
	else
	{
	
		u32RetVal = aAuxClockErr_oedt[u8ErrorNo] ;
	
	}
	return ( u32RetVal );
		
}
#if 0//since the iocontrol is removed, the test function is not required
/************************************************************************
 *FUNCTION:    tu32AuxclockPosixTimeCheck
 *DESCRIPTION: 1.Converts the AuxClock time to POSIX time
			   2.Additionally checks all the IOControl interfaces
 				
 *PARAMETER:   Nil
 *             
 *             
 *
 *RETURNVALUE: Error code
 *
 *HISTORY:     16.10.06(Ravindran P-CM-DI/EAP)
 *
 *Initial Revision.
 ************************************************************************/

tU32 tu32AuxclockPosixTimeCheck(void)
{
	OSAL_tIODescriptor hDevice = 0;
	OSAL_trIOCtrlAuxClockTicksToPosix rIOCtrlAuxClockTicksToPosix = {0,0,0,0}; 
	OSAL_trIOCtrlAuxClockTicks rIOAuxData ={0,0};
		
	tBool bErrorOccured = FALSE;
	tU8   u8ErrorNo = 0;
	tU32 u32ResInHertz = 0;
	tU32  u32ResInNano = 0;
	tS32 s32AuxClockVer = 0;
	OSAL_trTimeDate rTime={0,0,0,0,0,0,0,0,0};
   	tU32 u32RetVal = 0;
	tS32 s32Ret = 0;
	tU32 u32PosixTime1 = 0;
	tU32 u32PosixTime2 = 0;
	tU32 u32PosixTime3 = 0; 

	

	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_AUXILIARY_CLOCK, OSAL_EN_READWRITE );
   	if ( hDevice == OSAL_ERROR)
   	{
   		bErrorOccured = TRUE;
		u8ErrorNo = 0;
      		
	}
	rTime.s32Second = 30;         
	rTime.s32Minute = 30;
	rTime.s32Hour = 12;
	rTime.s32Day = 15;
	rTime.s32Month = 6;
	rTime.s32Year = 106;
	rTime.s32Weekday = 0;
	rTime.s32Yearday = 0;
	rTime.s32Daylightsaving = 0;
  
	s32Ret = OSAL_s32ClockSetTime( &rTime );
	if( s32Ret != OSAL_OK )
	{
		OSAL_s32IOClose( hDevice );
		return AUXCLOCK_OSAL_ERROR;
	}

		
	if(FALSE == bErrorOccured)
	{
		if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32AuxClockVer) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 9;
			OSAL_s32IOClose ( hDevice );
		}	
	}
		
	if(FALSE == bErrorOccured)
	{
		if ( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION,(tS32)&u32ResInHertz) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 1;
				
		}
		      	
	}

	if(FALSE == bErrorOccured)
	{
		if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME,(tS32)&u32ResInNano) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 2;
			OSAL_s32IOClose ( hDevice );
			
		}
	  	
	}
	

	if(FALSE == bErrorOccured)
	{
		if (  OSAL_s32IORead (hDevice,(tS8 *)&rIOAuxData,sizeof(rIOAuxData) )== OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 5;
			OSAL_s32IOClose ( hDevice );
				
		}
      	
	}
	
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksHigh = rIOAuxData.u32High;
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksLow = rIOAuxData.u32Low;
																		  
	if(FALSE == bErrorOccured) /*Convert Auxclock time to posix time*/
	{
		if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_CONVERTTICKSTOPOSIX,(tS32)&rIOCtrlAuxClockTicksToPosix) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 10;
			OSAL_s32IOClose ( hDevice );
		}
	}

	u32PosixTime1 = rIOCtrlAuxClockTicksToPosix.u32PosixTime;
   /*Decrease time */
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksHigh = rIOAuxData.u32High;
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksLow = rIOAuxData.u32Low-5000;/*Roll over need not be taken care as it is only change in time*/
																		  
	if(FALSE == bErrorOccured)  /*Convert Auxclock time to posix time*/
	{
		if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_CONVERTTICKSTOPOSIX,(tS32)&rIOCtrlAuxClockTicksToPosix) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 10;
			OSAL_s32IOClose ( hDevice );
			
		}
	}

	u32PosixTime2 = rIOCtrlAuxClockTicksToPosix.u32PosixTime;

	/*Increase time */
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksHigh = rIOAuxData.u32High;
	rIOCtrlAuxClockTicksToPosix.u32AuxClockTicksLow = rIOAuxData.u32Low+5000;/*Roll over need not be taken care as it is only change in time*/
																		  
	if(FALSE == bErrorOccured)	/*Convert Auxclock time to posix time*/
	{
		if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_CONVERTTICKSTOPOSIX,(tS32)&rIOCtrlAuxClockTicksToPosix) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 10;
			OSAL_s32IOClose ( hDevice );
			
		}
	}

	u32PosixTime3 = rIOCtrlAuxClockTicksToPosix.u32PosixTime;

	if(u32PosixTime1 == u32PosixTime2)
	{
		bErrorOccured = TRUE;
		u8ErrorNo = 13;
		OSAL_s32IOClose ( hDevice );	
	}
	else if(u32PosixTime1 == u32PosixTime3)
	{
		bErrorOccured = TRUE;
		u8ErrorNo = 12;
		OSAL_s32IOClose ( hDevice );	
	}


	if(FALSE == bErrorOccured )
	{
	 	if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			bErrorOccured = TRUE;
			u8ErrorNo = 7;

		}
	}

	if( FALSE == bErrorOccured )
	{ 
		u32RetVal = 0;			
	}
	else
	{
	
		u32RetVal = aAuxClockErr_oedt[u8ErrorNo] ;
	
	}
	return ( u32RetVal );
}
#endif

