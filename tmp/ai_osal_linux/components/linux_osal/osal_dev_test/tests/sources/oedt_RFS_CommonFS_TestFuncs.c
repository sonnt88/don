/******************************************************************************
 * FILE				:	oedt_RFS_CommonFS_TestFuncs.c 
 *
 * SW-COMPONENT	:	OEDT_FrmWrk 
 *
 * DESCRIPTION		:	This file implements the file system test cases for the RFS
 *                          
 * AUTHOR(s)		:	pwaechtler
 *
 * HISTORY			: copied from FFS2 files
 * ------------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_RFS_CommonFS_TestFuncs.h"

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpenClosedevice( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_001
* DESCRIPTION	:	Opens and closes RFS device
******************************************************************************/
tU32 u32RFS_CommonFSOpenClosedevice(tVoid)
{ 
	return u32FSOpenClosedevice(OEDTTEST_C_STRING_DEVICE_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpendevInvalParm( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_002
* DESCRIPTION	:	Try to Open RFS device with invalid parameters
******************************************************************************/
tU32 u32RFS_CommonFSOpendevInvalParm(tVoid)
{
	return u32FSOpendevInvalParm(OEDTTEST_C_STRING_DEVICE_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReOpendev( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_003
* DESCRIPTION	:	Try to Re-Open RFS device
******************************************************************************/
tU32 u32RFS_CommonFSReOpendev(tVoid)
{
	return u32FSReOpendev(OEDTTEST_C_STRING_DEVICE_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpendevDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_004
* DESCRIPTION	:	RFS device in different modes
******************************************************************************/
tU32 u32RFS_CommonFSOpendevDiffModes(tVoid)
{
	return u32FSOpendevDiffModes(OEDTTEST_C_STRING_DEVICE_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSClosedevAlreadyClosed( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_005
* DESCRIPTION	:	Close RFS device which is already closed
******************************************************************************/
tU32  u32RFS_CommonFSClosedevAlreadyClosed(tVoid)
{
	return u32FSClosedevAlreadyClosed(OEDTTEST_C_STRING_DEVICE_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpenClosedir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_006
* DESCRIPTION	:	Try to Open and closes a directory
******************************************************************************/
tU32  u32RFS_CommonFSOpenClosedir(tVoid)
{
	return u32FSOpenClosedir(OEDT_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpendirInvalid( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_007
* DESCRIPTION	:	Try to Open invalid directory
******************************************************************************/
tU32  u32RFS_CommonFSOpendirInvalid(tVoid)
{
	tPS8 dev_name[2] = {
					(tPS8)OEDT_C_STRING_RFS_NONEXST,
					(tPS8)OEDT_C_STRING_DEVICE_RFS_TESTDIR
					};
	return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateDelDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_008
* DESCRIPTION	:	Try create and delete directory
******************************************************************************/
tU32  u32RFS_CommonFSCreateDelDir(tVoid)
{
	return u32FSCreateDelDir(OEDT_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateDelSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_009
* DESCRIPTION	:	Try create and delete sub directory
******************************************************************************/
tU32 u32RFS_CommonFSCreateDelSubDir(tVoid)
{
	return u32FSCreateDelSubDir(OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateDirInvalName( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_010
* DESCRIPTION	:	Try create and delete directory with invalid path
******************************************************************************/
tU32 u32RFS_CommonFSCreateDirInvalName(tVoid)
{
	tPS8 dev_name[2] = {
						(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
						(tPS8)OEDT_C_STRING_RFS_DIR_INV_NAME
						};
	return u32FSCreateDirInvalName(dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSRmNonExstngDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_011
* DESCRIPTION	:	Try to remove non exsisting directory
******************************************************************************/
tU32 u32RFS_CommonFSRmNonExstngDir(tVoid)
{
	return u32FSRmNonExstngDir(OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSRmDirUsingIOCTRL( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_012
* DESCRIPTION	:	Delete directory with which is not a directory (with files) 
*						using IOCTRL
******************************************************************************/
tU32 u32RFS_CommonFSRmDirUsingIOCTRL(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)OEDT_C_STRING_RFS_FILE1
							};
	return u32FSRmDirUsingIOCTRL( dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetDirInfo( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_013
* DESCRIPTION	:	Get directory information
******************************************************************************/
tU32 u32RFS_CommonFSGetDirInfo(tVoid)
{
	return u32FSGetDirInfo(OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpenDirDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_014
* DESCRIPTION	:	Try to open directory in different modes
******************************************************************************/
tU32 u32RFS_CommonFSOpenDirDiffModes(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)SUBDIR_PATH_RFS
							};
	return u32FSOpenDirDiffModes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReOpenDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_015
* DESCRIPTION	:	Try to reopen directory
******************************************************************************/
tU32 u32RFS_CommonFSReOpenDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDT_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)OEDT_C_STRING_RFS_DIR1
							};
	return u32FSReOpenDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSDirParallelAccess( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_016
* DESCRIPTION	:	When directory is accessed by another thread try to rename it
*						delete it
******************************************************************************/
tU32 u32RFS_CommonFSDirParallelAccess(tVoid)
{
	return u32FSDirParallelAccess(OEDT_C_STRING_DEVICE_RFS_TESTDIR); 
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateDirMultiTimes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_017
* DESCRIPTION	:	Try to Create directory with similar name 
******************************************************************************/
tU32 u32RFS_CommonFSCreateDirMultiTimes(tVoid)
{
	tPS8 dev_name[3] = {
								(tPS8)OEDT_C_STRING_DEVICE_RFS_TESTDIR,
								(tPS8)SUBDIR_PATH_RFS,
								(tPS8)SUBDIR_PATH2_RFS
								};
	return u32FSCreateDirMultiTimes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateRemDirInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_018
* DESCRIPTION	:	Create/ remove directory with Invalid path
******************************************************************************/
tU32 u32RFS_CommonFSCreateRemDirInvalPath(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)OEDT_C_STRING_RFS_DIR_INV_PATH
							};
	return u32FSCreateRemDirInvalPath(dev_name);
}



/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateRmNonEmptyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_019
* DESCRIPTION	:	Try to remove non Empty directory
******************************************************************************/
tU32 u32RFS_CommonFSCreateRmNonEmptyDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)SUBDIR_PATH_RFS
							};
	return u32FSCreateRmNonEmptyDir(dev_name, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCopyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_020
* DESCRIPTION	:	Copy the files in source directory to destination directory
******************************************************************************/
tU32 u32RFS_CommonFSCopyDir(tVoid)
{
	tPS8 dev_name[3] = {
									(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
									(tPS8)FILE13_RFS,
									(tPS8)FILE14_RFS
									};
	return u32FSCopyDir(dev_name,TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSMultiCreateDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_021
* DESCRIPTION	:	Create multiple sub directories
******************************************************************************/
tU32 u32RFS_CommonFSMultiCreateDir(tVoid)
{
	return u32FSMultiCreateDir(OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_022
* DESCRIPTION	:	Attempt to create sub-directory within a directory opened
* 						in READONLY modey
******************************************************************************/
tU32 u32RFS_CommonFSCreateSubDir(tVoid)
{
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
								(tPS8)SUBDIR_PATH_RFS
								};

	return u32FSCreateSubDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSDelInvDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_023
* DESCRIPTION	:	Delete invalid Directory
******************************************************************************/
tU32  u32RFS_CommonFSDelInvDir(tVoid)
{
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
								(tPS8)OEDT_C_STRING_RFS_DIR_INV_NAME
								};
	return u32FSDelInvDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCopyDirRec( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_024
* DESCRIPTION	:	Copy directories recursively
******************************************************************************/
tU32  u32RFS_CommonFSCopyDirRec(tVoid)
{
	tPS8 dev_name[3] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)FILE13_RFS,
							(tPS8)FILE14_RFS
							};
	return u32FSCopyDirRec(dev_name);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSRemoveDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_025
* DESCRIPTION	:	Remove directories recursively
******************************************************************************/
tU32 u32RFS_CommonFSRemoveDir(tVoid)
{
	tPS8 dev_name[4] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR,
							(tPS8)FILE11_RFS,
							(tPS8)FILE12_RFS,
							(tPS8)FILE12_RECR2_RFS
							};
	return u32FSRemoveDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateDel( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_026
* DESCRIPTION	:  Create/ Delete file  with correct parameters
******************************************************************************/
tU32 u32RFS_CommonFSFileCreateDel(tVoid)
{
	return u32FSFileCreateDel(CREAT_FILE_PATH_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_027
* DESCRIPTION	:	Open /close File
******************************************************************************/
tU32 u32RFS_CommonFSFileOpenClose(tVoid)
{
	return u32FSFileOpenClose(OSAL_TEXT_FILE_FIRST_RFS, TRUE);
}	


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileOpenInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_028
* DESCRIPTION	:  Open file with invalid path name (should fail)
******************************************************************************/
tU32 u32RFS_CommonFSFileOpenInvalPath(tVoid)
{
	return u32FSFileOpenInvalPath(OEDT_C_STRING_FILE_INVPATH_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileOpenInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_029
* DESCRIPTION	:	Open a file with invalid parameters (should fail), 
******************************************************************************/
tU32 u32RFS_CommonFSFileOpenInvalParam(tVoid)
{
	return u32FSFileOpenInvalParam(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReOpen( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_030
* DESCRIPTION	:  Try to open and close the file which is already opened
******************************************************************************/
tU32  u32RFS_CommonFSFileReOpen(tVoid)
{
	return u32FSFileReOpen(OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_031
* DESCRIPTION	:	Read data from already exsisting file
******************************************************************************/
tU32 u32RFS_CommonFSFileRead(tVoid)
{	
	return u32FSFileRead(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileWrite( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_032
* DESCRIPTION	:  Write data to an existing file
******************************************************************************/
tU32 u32RFS_CommonFSFileWrite(tVoid)
{	
	return u32FSFileWrite(OEDT_RFS_WRITEFILE, TRUE);	
}	


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetPosFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_033
* DESCRIPTION	:  Get File Position from begining of file
******************************************************************************/
tU32 u32RFS_CommonFSGetPosFrmBOF(tVoid)
{
	return u32FSGetPosFrmBOF(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetPosFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_034
* DESCRIPTION	:	Get File Position from end of file
******************************************************************************/
tU32 u32RFS_CommonFSGetPosFrmEOF(tVoid)
{
	return u32FSGetPosFrmEOF(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_035
* DESCRIPTION	:	Read with a negative offset from BOF
******************************************************************************/
tU32   u32RFS_CommonFSFileReadNegOffsetFrmBOF()
{
	return u32FSFileReadNegOffsetFrmBOF(OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_036
* DESCRIPTION	:	Try to read more no. of bytes than the file size (beyond EOF)
******************************************************************************/
tU32 u32RFS_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
	return u32FSFileReadOffsetBeyondEOF(OEDT_RFS_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_037
* DESCRIPTION	:  Read from offset from Beginning of file
******************************************************************************/
tU32 u32RFS_CommonFSFileReadOffsetFrmBOF(tVoid)
{	
	return u32FSFileReadOffsetFrmBOF(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_038
* DESCRIPTION	:	Read file with few offsets from EOF
******************************************************************************/
tU32 u32RFS_CommonFSFileReadOffsetFrmEOF(tVoid)
{	
	return u32FSFileReadOffsetFrmEOF(OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_039
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
******************************************************************************/
tU32 u32RFS_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmBOF(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_040
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*						(This is from EOF)		
******************************************************************************/
tU32 u32RFS_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmEOF(OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetFileCRC( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_041
* DESCRIPTION	:	Get CRC value
******************************************************************************/
tU32 u32RFS_CommonFSGetFileCRC(tVoid)
{
	return u32FSGetFileCRC(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_042
* DESCRIPTION	:	Read data asyncronously from a file
******************************************************************************/  
tU32 u32RFS_CommonFSReadAsync(tVoid)
{	
	return u32FSReadAsync(OEDT_RFS_WRITEFILE, TRUE);

}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSLargeReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_043
* DESCRIPTION	:	Read data asyncronously from a large file
******************************************************************************/  
tU32 u32RFS_CommonFSLargeReadAsync(tVoid)
{	
	return u32FSLargeReadAsync(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteAsync()
* PARAMETER		:	none
* RETURNVALUE	:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_044
* DESCRIPTION	:	Write data asyncronously to a file
******************************************************************************/  
tU32 u32RFS_CommonFSWriteAsync(tVoid)
{	
	return u32FSWriteAsync(OEDT_RFS_WRITEFILE, TRUE);
}

	
/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSLargeWriteAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_045
* DESCRIPTION	:	Write data asyncronously to a large file
******************************************************************************/  
tU32 u32RFS_CommonFSLargeWriteAsync(tVoid)
{	
	return u32FSLargeWriteAsync(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteOddBuffer()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_046
* DESCRIPTION	:	Write data to an odd buffer
******************************************************************************/  
tU32 u32RFS_CommonFSWriteOddBuffer(tVoid)
{
	return u32FSWriteOddBuffer(OEDT_RFS_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteFileWithInvalidSize()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_FS_047
* DESCRIPTION	:  Write data to a file with invalid size
******************************************************************************/  
tU32 u32RFS_CommonFSWriteFileWithInvalidSize(tVoid)
{
	return u32FSWriteFileWithInvalidSize ( OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteFileInvalidBuffer()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_048
* DESCRIPTION	:	Write data to a file with invalid buffer
******************************************************************************/  
tU32 u32RFS_CommonFSWriteFileInvalidBuffer(tVoid)
{
	return u32FSWriteFileInvalidBuffer (OEDT_RFS_WRITEFILE, TRUE );
}
/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteFileStepByStep()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_049
* DESCRIPTION	:	Write data to a file stepwise
******************************************************************************/  
tU32 u32RFS_CommonFSWriteFileStepByStep(tVoid)
{
	return u32FSWriteFileStepByStep (OEDT_RFS_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetFreeSpace()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_050
* DESCRIPTION	:	Get free space of device
******************************************************************************/  
tU32 u32RFS_CommonFSGetFreeSpace (tVoid)
{
	return u32FSGetFreeSpace(OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSGetTotalSpace()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_051
* DESCRIPTION	:	Get total space of device
******************************************************************************/  
tU32 u32RFS_CommonFSGetTotalSpace (tVoid)
{
	return u32FSGetTotalSpace (OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}
	
/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileOpenCloseNonExstng()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_052
* DESCRIPTION	:	Open a non existing file 
******************************************************************************/  
tU32 u32RFS_CommonFSFileOpenCloseNonExstng(tVoid)
{
	return u32FSFileOpenCloseNonExstng (OEDT_C_STRING_FILE_INVPATH_RFS);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileDelWithoutClose()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_053
* DESCRIPTION	:	Delete a file without closing 
******************************************************************************/  
tU32 u32RFS_CommonFSFileDelWithoutClose(tVoid)
{
	return u32FSFileDelWithoutClose (OEDT_RFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSSetFilePosDiffOff()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_054
* DESCRIPTION	:	Set file position to different offsets
******************************************************************************/  
tU32 u32RFS_CommonFSSetFilePosDiffOff(tVoid)
{
	return u32FSSetFilePosDiffOff (OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSCreateFileMultiTimes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_055
* DESCRIPTION	:	create file multiple times
******************************************************************************/  
tU32 u32RFS_CommonFSCreateFileMultiTimes(tVoid)
{
	return u32FSCreateFileMultiTimes (OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateUnicodeName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_056
* DESCRIPTION	:	create file with unicode characters
******************************************************************************/  
tU32 u32RFS_CommonFSFileCreateUnicodeName(tVoid)
{
	return u32FSFileCreateUnicodeName (OEDT_RFS_CFS_UNICODEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateInvalName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_057
* DESCRIPTION	:	create file with invalid characters
******************************************************************************/  
tU32 u32RFS_CommonFSFileCreateInvalName(tVoid)
{
	return u32FSFileCreateInvalName(OEDT_RFS_CFS_INVALCHAR_FILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateLongName()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_058
* DESCRIPTION	:	create file with long file name
******************************************************************************/  
tU32 u32RFS_CommonFSFileCreateLongName(tVoid)
{
	return u32FSFileCreateLongName (OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateDiffModes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_059
* DESCRIPTION	:	Create file with different access modes
******************************************************************************/  
tU32 u32RFS_CommonFSFileCreateDiffModes(tVoid)
{
	return u32FSFileCreateDiffModes (OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileCreateInvalPath()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_060
* DESCRIPTION	:	Create file with invalid path
******************************************************************************/  
tU32 u32RFS_CommonFSFileCreateInvalPath(tVoid)
{
	return u32FSFileCreateInvalPath (OEDT_RFS_CFS_INVALIDFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSStringSearch()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_061
* DESCRIPTION	:	Search for string 
******************************************************************************/  
tU32 u32RFS_CommonFSStringSearch(tVoid)
{
	return u32FSStringSearch (OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileOpenDiffModes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_062
* DESCRIPTION	:	Create file with different access modes
******************************************************************************/  
tU32 u32RFS_CommonFSFileOpenDiffModes(tVoid)
{
	return u32FSFileOpenDiffModes (OEDT_RFS_WRITEFILE, TRUE	);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadAccessCheck()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_063
* DESCRIPTION	:	Try to read from the file which has no access rights 
******************************************************************************/
tU32 u32RFS_CommonFSFileReadAccessCheck(tVoid)
{	
	return u32FSFileReadAccessCheck  (OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileWriteAccessCheck()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_064
* DESCRIPTION	:	Try to write from the file which has no access rights 
******************************************************************************/
tU32 u32RFS_CommonFSFileWriteAccessCheck(tVoid)
{	 
	return u32FSFileWriteAccessCheck (OEDT_RFS_WRITEFILE, TRUE);	
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileReadSubDir()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_065
* DESCRIPTION	:  Read file present in sub-directory
******************************************************************************/
tU32 u32RFS_CommonFSFileReadSubDir(tVoid)
{	
	return u32FSFileReadSubDir (OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
} 


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFileWriteSubDir()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_066
* DESCRIPTION	:	Write to file present in sub-directory
******************************************************************************/
tU32 u32RFS_CommonFSFileWriteSubDir(tVoid)
{	
	return u32FSFileWriteSubDir (OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
} 


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof1KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_067
* DESCRIPTION	:	Read Write time measure for 1KB file
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1KbFile(tVoid)
{	
	return u32FSTimeMeasureof1KbFile (OEDT_RFS_WRITEFILE);
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof10KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_068
* DESCRIPTION	:	Read Write time measure for 10KB file
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof10KbFile(tVoid)
{	
	return u32FSTimeMeasureof10KbFile (OEDT_RFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof100KbFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_069
* DESCRIPTION	:	Read Write time measure for 10KB file
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof100KbFile(tVoid)
{	
	return u32FSTimeMeasureof100KbFile (OEDT_RFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof1MBFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_070
* DESCRIPTION	:	Read Write time measure for 1MB file
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1MBFile(tVoid)
{	
	return u32FSTimeMeasureof1MBFile (OEDT_RFS_WRITEFILE);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof1KbFilefor1000times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_071
* DESCRIPTION	:	Read write time measure for 1KB file 10000 times
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid)
{	
	return u32FSTimeMeasureof1KbFilefor1000times (OEDT_RFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof10KbFilefor1000times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_072
* DESCRIPTION	:	Read write time measure for 10KB file 1000 times
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid)
{	
	return u32FSTimeMeasureof10KbFilefor1000times( OEDT_RFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof100KbFilefor100times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_073
* DESCRIPTION	:	Read write time measure for 100KB file 100 times
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid)
{	
	return u32FSTimeMeasureof100KbFilefor100times( OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadWriteTimeMeasureof1MBFilefor16times()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_074
* DESCRIPTION	:	Read write time measure for 1MB file 16 times
******************************************************************************/  
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid)
{	
	return u32FSTimeMeasureof1MBFilefor16times(
					OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, 
					OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSRenameFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_075
* DESCRIPTION	:	Rename file
******************************************************************************/  
tU32 u32RFS_CommonFSRenameFile(tVoid)
{
	return u32FSRenameFile( OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteFrmBOF()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_076
* DESCRIPTION	:	Write to file from BOF
******************************************************************************/  
tU32 u32RFS_CommonFSWriteFrmBOF(tVoid)
{	
	return u32FSWriteFrmBOF( OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteFrmEOF()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_077
* DESCRIPTION	:	Write to file from EOF
******************************************************************************/  
tU32 u32RFS_CommonFSWriteFrmEOF(tVoid)
{	
	return u32FSWriteFrmEOF ( OEDT_RFS_WRITEFILE, TRUE	);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSLargeFileRead()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_078
* DESCRIPTION	:	Read large file
******************************************************************************/  
tU32 u32RFS_CommonFSLargeFileRead(tVoid)
{	
	return u32FSLargeFileRead ( OEDT_RFS_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSLargeFileWrite()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_079
* DESCRIPTION	:	Write large data to file
******************************************************************************/  
tU32 u32RFS_CommonFSLargeFileWrite(tVoid)
{	
	return u32FSLargeFileWrite ( OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSOpenCloseMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_080
* DESCRIPTION	:	Open and close file from multiple threads
******************************************************************************/  
tU32 u32RFS_CommonFSOpenCloseMultiThread(tVoid)
{	
	return u32FSOpenCloseMultiThread ( OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_081
* DESCRIPTION	:	write to file from multiple threads
******************************************************************************/  
tU32 u32RFS_CommonFSWriteMultiThread(tVoid)
{	
	return u32FSWriteMultiThread ( OEDT_RFS_WRITEFILE );
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSWriteReadMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_082
* DESCRIPTION	:	write and read to file from multiple threads
******************************************************************************/  
tU32 u32RFS_CommonFSWriteReadMultiThread(tVoid)
{	
	return u32FSWriteReadMultiThread ( OEDT_RFS_WRITEFILE	);
}


/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSReadMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_083
* DESCRIPTION	:	Read from file from multiple threads
******************************************************************************/  
tU32 u32RFS_CommonFSReadMultiThread(tVoid)
{	
	return u32FSReadMultiThread ( OEDT_RFS_WRITEFILE );
}

/*****************************************************************************
* FUNCTION		:	u32RFS_CommonFSFormat()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_RFS_CFS_084
* DESCRIPTION	:	Format FS
******************************************************************************/  
tU32 u32RFS_CommonFSFormat(tVoid)
{	/* the RFS cannot be formatted this way */
	return 0; //u32FSFormat (OEDTTEST_C_STRING_DEVICE_RFS);
}

/*****************************************************************************
* FUNCTION:		u32RFS_CommonFSFileGetExt()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RFS_CFS_085
* DESCRIPTION:  Read file/directory contents 
******************************************************************************/
tU32 u32RFS_CommonFSFileGetExt(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt( OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RFS_CommonFSFileGetExt2()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_RFS_CFS_086
* DESCRIPTION:  Read file/directory contents with 2 IOCTL 
******************************************************************************/
tU32 u32RFS_CommonFSFileGetExt2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt2( OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RFS_CommonFSSaveNowIOCTRL_SyncWrite()
* PARAMETER:    RFS device 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with sync write
******************************************************************************/ 
tU32 u32RFS_CommonFSSaveNowIOCTRL_SyncWrite(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FFSCommonFSSaveNowIOCTRL_SyncWrite(
	                               (tPS8)OEDT_RFS_CFS_IOCTRL_SAVENOW_FILE_SYNC
	                               ); 
	return u32Ret; 
}

/*****************************************************************************
* FUNCTION:		u32RFS_CommonFSSaveNowIOCTRL_AsynWrite()
* PARAMETER:    RFS device 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with async write
******************************************************************************/
tU32 u32RFS_CommonFSSaveNowIOCTRL_AsynWrite(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FFSCommonFSSaveNowIOCTRL_AsynWrite(
	                               (tPS8)OEDT_RFS_CFS_IOCTRL_SAVENOW_FILE_ASYNC
	                               ); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileOpenCloseMultipleTime()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files accenting order.
* TEST CASE:    TU_OEDT_RFS_CFS_089
******************************************************************************/
tU32 u32RFS_CommonFileOpenCloseMultipleTime(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTime
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1100,											 //file count
		1,											    //dir count
		(tS8*)OEDT_TEST_FILE								 //file name
	); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileOpenCloseMultipleTimeRandom1()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files random order.
        Random order means 0 - 750 and 1500 - 750.
* TEST CASE:    TU_OEDT_RFS_CFS_090
******************************************************************************/
tU32 u32RFS_CommonFileOpenCloseMultipleTimeRandom1(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTimeRandom1
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1500,											 //file count
		1,											    //dir count
		(tS8*)OEDT_TEST_FILE								 //file name
	); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileOpenCloseMultipleTimeRandom2()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files accenting order.
        Random order means 0 - 750 and 750 - 1500.
* TEST CASE:    TU_OEDT_RFS_CFS_091
******************************************************************************/
tU32 u32RFS_CommonFileOpenCloseMultipleTimeRandom2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTimeRandom2
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1500,											 //file count
		1,												 //dir count
		(tS8*)OEDT_TEST_FILE								 //file name			    
	); 
	return u32Ret; 
}

/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileLongFileOpenCloseMultipleTime()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files accenting order.
* TEST CASE:    TU_OEDT_RFS_CFS_092
******************************************************************************/
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTime(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTime
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1100,											 //file count
		1,											    //dir count
		(tS8*)OEDT_TEST_LONG_FILE						 //file name
	); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom1()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files random order.
        Random order means 0 - 750 and 1500 - 750.
* TEST CASE:    TU_OEDT_RFS_CFS_093
******************************************************************************/
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom1(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTimeRandom1
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1500,											 //file count
		1,											    //dir count
		(tS8*)OEDT_TEST_LONG_FILE						 //file name
	); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom2()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files accenting order.
        Random order means 0 - 750 and 750 - 1500.
* TEST CASE:    TU_OEDT_RFS_CFS_094
******************************************************************************/
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTimeRandom2
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1500,											 //file count
		1,												 //dir count
		(tS8*)OEDT_TEST_LONG_FILE						 //file name					    
	); 
	return u32Ret; 
}
/*****************************************************************************
* FUNCTION:		u32RFS_CommonFileOpenCloseMultipleTimeMultDir()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
                This test has the test data of three chain directory contains 
                1500 files with file name length 14 and opens these files 
                accenting order.
* TEST CASE:    TU_OEDT_RFS_CFS_095
******************************************************************************/
tU32 u32RFS_CommonFileOpenCloseMultipleTimeMultDir(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseMultipleTime
	(
	   (tPS8)OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR, //device name + working dir
		TRUE,											 //create files
		1100,											 //file count
		3,											    //dir count
		(tS8*)OEDT_TEST_FILE								 //file name
	); 
	return u32Ret; 
}

/* EOF */

