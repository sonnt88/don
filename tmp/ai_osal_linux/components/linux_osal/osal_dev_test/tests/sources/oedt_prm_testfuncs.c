/******************************************************************************
 * FILE         : oedt_PRM_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the PRM test cases for the CDROM/DVD, 
 *                SDCard 
 *                          
 * AUTHOR(s)    :	Sriranjan U (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     	          | Author & comments
 * --.--.--       | Initial revision    | ------------
 * 17 Dec, 2008   | version 1.0         | Sriranjan U (RBEI/ECM1)
 * 11 Mar, 2009   | version 1.1         | Sainath Kalpuri (RBEI/ECF1)
                                        | Two new OEDTs "u32PRM_NaviDataMedia_event"
										            and "u32PRM_DataMedia_event" have been added 
 * 18 Mar, 2009   | version 1.2         | Sainath Kalpuri (RBEI/ECF1)
 										            New OEDT "u32PRM_NaviDataMedia_event_TEST" 
                                          has been added
 * 25 Mar, 2009   | version 1.3         | Sainath Kalpuri (RBEI/ECF1)
                                          Removed Lint and compiler warnings
 * 14 Oct, 2011   | version 1.4         | Anooj Gopi (RBEI/ECF1)
                                          Added Tests for /dev/media devices
 * 24 Jan, 2013   | version 1.5         | Suresh Dhandapani (RBEI/ECF5)
                                          Added Tests for LSIM /dev/usbpower
 * 02 Jun, 2014   | version 1.5         | Gokulkrishnan N (RBEI/ECF5)
                                          Added Tests for Gen3 /dev/usbpower
 * 5 August,2014  |version 1.8          | Padma Kudari(RBEI/ECF5)
                                          Modified OEDTs to fix the ticket CFG3-697.
 * 24 April, 2015 |version 1.9          | Deepak Kumar
                                          Added missing OEDTS for PRM
 * 7th May,2015   | version 1.10        | Amit Bhardwaj ( RBEI/ECF5 )
                                          Added Oedt in regression dataset for SD card register for PRM notification. 
 * 13th Nov 2015  | Version 1.11        | Vinutha Eshwar Shantha (RBEI/ECF5)
                                          Lint fix for CFG3-1515
 ----------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_prm_testfuncs.h"
#include "oedt_helper_funcs.h"
#include "oedt_testing_macros.h"

#include <unistd.h>
//#include <device/disk.h>

#define OSAL_DEV_NAME_MAX_LEN ( 80 )

tU32 RetNotiInsetEject = 0;
tU32 oedt_prm_status = 0;
tBool bIsCallback_Called = FALSE;
tBool bIsError = FALSE;
tBool bIsDeviceReady = FALSE;
tBool bIsNaviData_Media_Found = FALSE;
tBool bISSdcard_inserted = FALSE;
tBool bIsMedia_Change_Notified = FALSE;
tBool bIsData_Media_Found = FALSE;
tU8 szDevname[OSAL_DEV_NAME_MAX_LEN];
#ifndef OSAL_C_TRACELEVEL1
#define OSAL_C_TRACELEVEL1  TR_LEVEL_FATAL
#endif

#ifndef HIWORD
  #define HIWORD( dwValue )   ((tU16)((((tU32)( dwValue )) >> 16) & 0xFFFF))
#endif

#define OEDT_STRING_LENGTH       (20)
tU32 Errorcode=0;
char NavPathBuffer1[256];
#define COUNT 10
tS32 callback_execution_count=0;

#if defined (LSIM) || defined (OSAL_GEN3_SIM)

tU8 u8UVCount;
tU8 u8OCRegFlg;
tU8 u8UndefRegFlg;
static OSAL_tEventHandle  hoedt_usbpwr_rEventHnd;

extern tVoid vUSBPWRCallbackHandler(tPVoid pvBuffer );
static tVoid vUSBPWR_OC_Callback( tPCU32 pu32ModeChangeInfo, tVoid* pVoid );
static tVoid vUSBPWR_UV_Callback( tPCU32 pu32ModeChangeInfo, tVoid* pVoid );
static tVoid vUSBPWR_UNDEF_Callback( tPCU32 pu32ModeChangeInfo, tVoid* pVoid );
extern OEDT_TESTSTATE OEDT_CREATE_TESTSTATE(tVoid);

#define OEDT_USB_SIG_UNDEF          0x62
#define EVENT_WAIT_TIMEOUT_USBPWR   10000         //10 sec time out value
#define OEDT_USBPWR_EV_MASK         0x0007FFFF
#define OEDT_USBPWR_OCACT_MASK      0x00001000
#define OEDT_USBPWR_OCINACT_MASK    0x00002000
#define OEDT_USBPWR_OCINACT1_MASK   0x00008000
#define OEDT_USBPWR_UVACT_MASK      0x00003000
#define OEDT_USBPWR_UVINACT_MASK    0x00004000
#define OEDT_USBPWR_UVALLACT_MASK   0x00005000
#define OEDT_USBPWR_UVALLINACT_MASK 0x00006000
#define OEDT_USBPWR_UNDEF_MASK      0x00007000
#define OEDT_USB_PORTS              2
#define OEDT_SIG_TRUE               2
#define OEDT_SIG_FALSE              1
#define OEDT_SIG_UNDEF              0
#define OEDT_UV_MAX_CMD             4
#define OEDT_OC_MAX_CMD             2

/*****Error Flags************************/
#define OEDT_ERROR_EVT_CREATE       101
#define OEDT_ERROR_OSAL_OPEN        108
#define OEDT_ERROR_EVT_TIMEOUT      102
#define OEDT_ERROR_EVT_CLR          104
#define OEDT_ERROR_WRNG_CMD         106
#define OEDT_ERROR_WRNG_CMD_EVT_CLR 105
#define OEDT_ERROR_REG_NOT          107

typedef struct
{
   tU8  u8PortNr;
   tU8  u8OC;
   tU8  u8UV;
   tU8  u8PPON;

   tU32 u32OCStartTime;
   tU32 u32OCEndTime;
   tU32 u32UVStartTime;
   tU32 u32UVEndTime;
   tU32 u32PPONStartTime;
   tU32 u32PPONEndTime;
}OEDTUsbPortState;

/************************************************************************
 * FUNCTION:      u32USB_PWR_OC_test
 * DESCRIPTION:   Opens dev/prm and Registers the callback to dev/usbpower
 *                and waits for OverCurrent event to be notified from the
 *                registered callback.Then Unregisters the callback and closes
 *                dev/prm.
 * 			       			     
 *
 * PARAMETER:     Nil
 *             
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 u32USB_PWR_OC_test(void)
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32FunRetVal = OSAL_OK;
   OEDT_TESTSTATE rstate;
   OSAL_tEventMask roedt_ResultMask;
   OSAL_trNotifyDataExt2 rNotifyData;
   tS32 s32Mask;
   tU8 u8Count;
   tU8 u8CountPort;
   tU8 u8CountPort_init;
   tU8 u8LastCountEvnt;
   tU8 u8CountEvnt;
   OSAL_tIODescriptor hDevice;
   tU8 u8Buffer[4];
   /*This flag is to set callback not to be executed,beacause while registering itself
     callback will be called from driver. In order for not to process callback notitfication during registration*/
   u8OCRegFlg = FALSE;

   rstate = OEDT_CREATE_TESTSTATE();

   /*Initialize the driver fields to zero*/
   u8Buffer[2] = OEDT_USB_SIG_UNDEF;
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }

   if(OSAL_OK != OSAL_s32EventCreate("oedt_usbpowerEv1", &hoedt_usbpwr_rEventHnd))
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Event creation for USB_Power");
      s32RetVal = OEDT_ERROR_EVT_CREATE;
   }
   else
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
      if(!rstate.error_code)
      {
         rNotifyData.pu32Data            = 0;
         rNotifyData.ResourceName        = "/dev/usbpower" ;
         rNotifyData.u16AppID            = OSAL_C_U16_MP3CD_APPID ;
         rNotifyData.u8Status            = 0;
         rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_LOW_POW;
         rNotifyData.pCallbackExt2       = (OSALCALLBACKFUNCEXT2)vUSBPWR_OC_Callback;
         /*Registers the callback function*/
         s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
         OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
         if(!rstate.error_code)
         {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
            u8OCRegFlg = TRUE;/*This flag is to set callback to be executed after registration*/
            /*loop to run Active and Inactive*/
            for(u8Count = 0; u8Count < OEDT_OC_MAX_CMD; u8Count++)
            {
               if(!u8Count)
               {
                  u8LastCountEvnt = 1;/*event will wait for one time in active*/
               }
               else
               {
                  u8LastCountEvnt = 2;/*event will wait for two time in inactive, because driver gives two times for this notification*/
               }
               /*loop to run for all usb ports*/
               for(u8CountPort = 1; u8CountPort <= OEDT_USB_PORTS; u8CountPort++)
               {
                  if(!u8Count)
                  {
                     s32Mask = OEDT_USBPWR_OCACT_MASK;
                     s32Mask = s32Mask | u8CountPort;
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Overcurrent active command with port:%d",u8CountPort);
                  }
                  else
                  {
                     s32Mask = OEDT_USBPWR_OCINACT1_MASK;
                     s32Mask = s32Mask | u8CountPort;
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Overcurrent Inactive command with port:%d",u8CountPort);
                  }
                  for(u8CountEvnt = 0; u8CountEvnt < u8LastCountEvnt; u8CountEvnt++)
                  {
                     if(OSAL_OK != OSAL_s32EventWait(hoedt_usbpwr_rEventHnd, OEDT_USBPWR_EV_MASK,
                                                   OSAL_EN_EVENTMASK_OR,
                                                   EVENT_WAIT_TIMEOUT_USBPWR,
                                                   &roedt_ResultMask))
                     {
                        /*If Timeout, then do deinitialization*/
                        s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                        OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                        if(rstate.error_code)
                        {
                           OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                        }
                        OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                        OSAL_s32EventDelete("oedt_usbpowerEv1");
                        OSAL_s32IOClose(hDevice);
                        s32RetVal = OEDT_ERROR_EVT_TIMEOUT;
                        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Overcurrent event TimeOut for USB_Power");

                        break;
                     }
                     else
                     {
                        if((tS32)roedt_ResultMask == s32Mask)
                        {
                           /*clear the event flag*/
                           if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                         ~roedt_ResultMask,
                                                         OSAL_EN_EVENTMASK_AND))
                           {
                              /*if clear event fails then perform deinitialization operation*/
                              s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                              OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                              if(rstate.error_code)
                              {
                                 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                              }
                              OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                              OSAL_s32EventDelete("oedt_usbpowerEv1");
                              OSAL_s32IOClose(hDevice);
                              s32RetVal = OEDT_ERROR_EVT_CLR;
                              break;
                           }
                           else
                           {
                              if(!u8Count)
                              {
                                 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: callback received for overcurrent Active in PORT:%d",u8CountPort);
                              }
                              else if(!u8CountEvnt)
                              {
                                 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: callback received for overcurrent INACTIVE PPON in PORT:%d",u8CountPort);
                              }
                              else  if(u8CountEvnt == 1)
                              {
                                 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: callback received for overcurrent INACTIVE OC in PORT:%d",u8CountPort);
                              }
                              /*Assign the overcurrent inactive mask to wait in second time of event*/
                              if((!u8CountEvnt) && u8Count)
                              {
                                 s32Mask = OEDT_USBPWR_OCINACT_MASK;
                                 s32Mask = s32Mask | u8CountPort;
                              }
                           }
                        }
                        else
                        {
                           /*mask doesn't match*/
                           OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: wrong command you have entered");
                           if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                         ~roedt_ResultMask,
                                                         OSAL_EN_EVENTMASK_AND))
                           {
                              s32RetVal = OEDT_ERROR_WRNG_CMD_EVT_CLR;
                           }
                           else
                           {
                              s32RetVal = OEDT_ERROR_WRNG_CMD;
                           }
                           /*if mask doesn't match then perform deinitialization operation*/
                           s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                           OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                           if(rstate.error_code)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                           }
                           OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                           OSAL_s32EventDelete("oedt_usbpowerEv1");
                           OSAL_s32IOClose(hDevice);
                           break;

                        }
                     }
                  }
                  if(s32RetVal == OEDT_ERROR_EVT_TIMEOUT || s32RetVal == OEDT_ERROR_EVT_CLR || s32RetVal == OEDT_ERROR_WRNG_CMD_EVT_CLR  || s32RetVal == OEDT_ERROR_WRNG_CMD)
                  {
                     break;
                  }
               }
               if(s32RetVal == OEDT_ERROR_EVT_TIMEOUT || s32RetVal == OEDT_ERROR_EVT_CLR || s32RetVal == OEDT_ERROR_WRNG_CMD_EVT_CLR  || s32RetVal == OEDT_ERROR_WRNG_CMD)
               {
                  break;
               }
            }
            /*check whether already deinitialization is performed*/
            if(s32RetVal != OEDT_ERROR_EVT_TIMEOUT && s32RetVal != OEDT_ERROR_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD_EVT_CLR  && s32RetVal != OEDT_ERROR_WRNG_CMD)
            {
               /*perform deinitialization operation*/
               s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
               OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
               if(rstate.error_code)
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               else
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
               OSAL_s32EventDelete("oedt_usbpowerEv1");
               OSAL_s32IOClose(hDevice);
            }
         }
         else
         {
            s32RetVal = OEDT_ERROR_REG_NOT;
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
         }

      }
      else
      {
         s32RetVal = OEDT_ERROR_OSAL_OPEN;
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
      }
   }
   /*Initialize the driver fields to zero in order to make sure that it wont affect test case*/
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }
   return (tU32)s32RetVal;
}


/************************************************************************
 * FUNCTION:      u32USB_PWR_UV_test
 * DESCRIPTION:   Opens dev/prm and Registers the callback to dev/usbpower
 *                and waits for event to be notified for undervoltage.
 *                Then Unregisters the callback and closes dev/prm.
 *
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 u32USB_PWR_UV_test(void)
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32FunRetVal = OSAL_OK;
   OEDT_TESTSTATE rstate;
   OSAL_tEventMask roedt_ResultMask;
   OSAL_trNotifyDataExt2 rNotifyData;
   tS32 s32Mask = 0;
   tU8 u8CountPort;
   tU8 u8CountPort_init;
   OSAL_tIODescriptor hDevice;
   tU8 u8Buffer[4];


   u8UVCount = 0;
   rstate = OEDT_CREATE_TESTSTATE();

   /*Initialize the driver fields to zero*/
   u8Buffer[2] = OEDT_USB_SIG_UNDEF;
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }

   if(OSAL_OK != OSAL_s32EventCreate("oedt_usbpowerEv1", &hoedt_usbpwr_rEventHnd))
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Event creation for USB_Power");
      s32RetVal = OEDT_ERROR_EVT_CREATE;
   }
   else
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
      if(!rstate.error_code)
      {
         rNotifyData.pu32Data            = 0;
         rNotifyData.ResourceName        = "/dev/usbpower" ;
         rNotifyData.u16AppID            = OSAL_C_U16_MP3CD_APPID ;
         rNotifyData.u8Status            = 0;
         rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_LOW_POW;
         rNotifyData.pCallbackExt2       = (OSALCALLBACKFUNCEXT2)vUSBPWR_UV_Callback;
         /*Register for callback notification*/
         s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
         OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
         if(!rstate.error_code)
         {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
            /*loop to run active,inactive,allactive and allinactive*/
            for(u8UVCount = 1; u8UVCount <= OEDT_UV_MAX_CMD; u8UVCount++)
            {  /*cleck for all active*/
               if(u8UVCount == 3)
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Undervoltage ALL active command");
               }
               else if(u8UVCount == 4)/*check for all inactive*/
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Undervoltage ALL inactive command");
               }
               /*loop for all ports*/
               for(u8CountPort = 1; u8CountPort <= OEDT_USB_PORTS; u8CountPort++)
               {
                  if(u8UVCount == 1)
                  {
                     s32Mask = OEDT_USBPWR_UVACT_MASK;
                     s32Mask = s32Mask | u8CountPort;
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Undervoltage active command with port:%d",u8CountPort);
                  }
                  else if(u8UVCount == 2)
                  {
                     s32Mask = OEDT_USBPWR_UVINACT_MASK;
                     s32Mask = s32Mask | u8CountPort;
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Undervoltage Inactive command with port:%d",u8CountPort);
                  }
                  else if(u8UVCount == 3)
                  {
                     s32Mask = OEDT_USBPWR_UVALLACT_MASK;
                     s32Mask = s32Mask | u8CountPort;
                  }
                  else if(u8UVCount == 4)
                  {
                     s32Mask = OEDT_USBPWR_UVALLINACT_MASK;
                     s32Mask = s32Mask | u8CountPort;
                  }
                  if(OSAL_OK != OSAL_s32EventWait(hoedt_usbpwr_rEventHnd, OEDT_USBPWR_EV_MASK,
                                                 OSAL_EN_EVENTMASK_OR,
                                                 EVENT_WAIT_TIMEOUT_USBPWR,
                                                 &roedt_ResultMask))
                  {
                     /*If Timeout, then do deinitialization operation*/
                     s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                     OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                     if(rstate.error_code)
                     {
                        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                     }
                     OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                     OSAL_s32EventDelete("oedt_usbpowerEv1");
                     OSAL_s32IOClose(hDevice);
                     s32RetVal = OEDT_ERROR_EVT_TIMEOUT;
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: undervoltage event TimeOut for USB_Power");
                     break;

                  }
                  else
                  {
                     /*check whether event mask matches*/
                     if((tS32)roedt_ResultMask == s32Mask)
                     {  /*clear the event mask*/
                        if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                      ~roedt_ResultMask,
                                                      OSAL_EN_EVENTMASK_AND))
                        {
                           /*if event clear fails then perform deinitialization operation*/
                           s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                           OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                           if(rstate.error_code)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                           }
                           OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                           OSAL_s32EventDelete("oedt_usbpowerEv1");
                           OSAL_s32IOClose(hDevice);
                           s32RetVal = OEDT_ERROR_EVT_CLR;
                           break;
                        }
                        else
                        {
                           if(u8UVCount == 1)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: UV active callback received for undervoltage in PORT:%d",u8CountPort);
                           }
                           else if(u8UVCount == 2)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: UV inactive callback received for undervoltage in PORT:%d",u8CountPort);
                           }
                           else if(u8UVCount == 3)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: UV ALL active callback received for undervoltage in PORT:%d",u8CountPort);
                           }
                           else if(u8UVCount == 4)
                           {
                              OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: UV ALL inactive callback received for undervoltage in PORT:%d",u8CountPort);
                           }
                        }
                     }
                     else
                     {
                        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: wrong command:You have entered");
                        if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                      ~roedt_ResultMask,
                                                      OSAL_EN_EVENTMASK_AND))
                        {
                           s32RetVal = OEDT_ERROR_WRNG_CMD_EVT_CLR;
                        }
                        else
                        {
                           s32RetVal = OEDT_ERROR_WRNG_CMD;
                        }
                        /*if mask doesn't match then perform deinitialization operation*/
                        s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                        OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                        if(rstate.error_code)
                        {
                           OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                        }
                        OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                        OSAL_s32EventDelete("oedt_usbpowerEv1");
                        OSAL_s32IOClose(hDevice);
                        break;
                     }
                  }
               }
               /*Exit from the loop if there is an error*/
               if(s32RetVal == OEDT_ERROR_EVT_TIMEOUT || s32RetVal == OEDT_ERROR_EVT_CLR || s32RetVal == OEDT_ERROR_WRNG_CMD_EVT_CLR || s32RetVal == OEDT_ERROR_WRNG_CMD)
               {
                  break;
               }
            }
            /*check whether already deinitialization is performed*/
            if(s32RetVal != OEDT_ERROR_EVT_TIMEOUT && s32RetVal != OEDT_ERROR_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD)
            {
               /*perform deinitialization operation*/
               s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
               OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
               if(rstate.error_code)
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
               OSAL_s32EventDelete("oedt_usbpowerEv1");
               OSAL_s32IOClose(hDevice);
            }
         }
         else
         {
            s32RetVal = OEDT_ERROR_REG_NOT;
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
         }

      }
      else
      {
         s32RetVal = OEDT_ERROR_OSAL_OPEN;
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
      }
   }
   /*Initialize the driver fields to zero in order to make sure that it wont affect test case*/
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }
   return (tU32)s32RetVal;
}

/************************************************************************
 * FUNCTION:      u32USB_PWR_UNDEF_test
 * DESCRIPTION:   Opens dev/prm and Registers the callback to dev/usbpower
 *                and waits for event to be notified for undefined signal.
 *                Then Unregisters the callback and closes dev/prm.
 *
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 u32USB_PWR_UNDEF_test(void)
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32FunRetVal = OSAL_OK;
   OEDT_TESTSTATE rstate;
   OSAL_tEventMask roedt_ResultMask;
   OSAL_trNotifyDataExt2 rNotifyData;
   tS32 s32Mask;
   tU8 u8CountPort;
   tU8 u8CountPort_init;
   OSAL_tIODescriptor hDevice;
   tU8 u8Buffer[4];
   /*This flag is to set callback not to be executed,beacause while registering itself
        callback will be called from driver. In order for not to process callback notitfication during registration*/
   u8UndefRegFlg = FALSE;
   rstate = OEDT_CREATE_TESTSTATE();

   /*Initialize the driver fields to zero*/
   u8Buffer[2] = OEDT_USB_SIG_UNDEF;
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }


   if(OSAL_OK != OSAL_s32EventCreate("oedt_usbpowerEv1", &hoedt_usbpwr_rEventHnd))
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Event creation for USB_Power");
      s32RetVal = OEDT_ERROR_EVT_CREATE;
   }
   else
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );
      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
      if(!rstate.error_code)
      {
         rNotifyData.pu32Data            = 0;
         rNotifyData.ResourceName        = "/dev/usbpower" ;
         rNotifyData.u16AppID            = OSAL_C_U16_MP3CD_APPID ;
         rNotifyData.u8Status            = 0;
         rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_LOW_POW;
         rNotifyData.pCallbackExt2       = (OSALCALLBACKFUNCEXT2)vUSBPWR_UNDEF_Callback;
         /*Register for callback notification*/
         s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
         OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
         if(!rstate.error_code)
         {
            u8UndefRegFlg = TRUE;/*This flag is to set callback to be executed after registration*/
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
            /*loop for all ports*/
            for(u8CountPort = 1; u8CountPort <= OEDT_USB_PORTS; u8CountPort++)
            {
               s32Mask = OEDT_USBPWR_UNDEF_MASK;
               s32Mask = s32Mask | u8CountPort;

               OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"Please enter the Undefined signal command with port:%d",u8CountPort);


               if(OSAL_OK != OSAL_s32EventWait(hoedt_usbpwr_rEventHnd, OEDT_USBPWR_EV_MASK,
                                              OSAL_EN_EVENTMASK_OR,
                                              EVENT_WAIT_TIMEOUT_USBPWR,
                                              &roedt_ResultMask))
               {
                  /*If Timeout, then do deinitialization operation*/
                  s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                  OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                  if(rstate.error_code)
                  {
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                  }
                  OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                  OSAL_s32EventDelete("oedt_usbpowerEv1");
                  OSAL_s32IOClose(hDevice);
                  s32RetVal = OEDT_ERROR_EVT_TIMEOUT;
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Undefined signal event TimeOut for USB_Power");
                  break;
               }
               else
               {
                  /*check for event pattern matches*/
                  if((tS32)roedt_ResultMask == s32Mask)
                  {  /*clear the event pattern*/
                     if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                   ~roedt_ResultMask,
                                                   OSAL_EN_EVENTMASK_AND))
                     {
                        /*if clear event fails then perform deinitialization operation*/
                        s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                        OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                        if(rstate.error_code)
                        {
                           OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                        }
                        OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                        OSAL_s32EventDelete("oedt_usbpowerEv1");
                        OSAL_s32IOClose(hDevice);
                        s32RetVal = OEDT_ERROR_EVT_CLR;
                        break;
                     }
                     else
                     {
                        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: callback received for Undefined signal for the PORT:%d",u8CountPort);
                     }
                  }
                  else
                  {
                     OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: wrong command:You have entered");
                     if(OSAL_OK != OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                                                   ~roedt_ResultMask,
                                                   OSAL_EN_EVENTMASK_AND))
                     {
                        s32RetVal = OEDT_ERROR_WRNG_CMD_EVT_CLR;
                     }
                     else
                     {
                        s32RetVal = OEDT_ERROR_WRNG_CMD;
                     }
                     /*if mask doesn't match then perform deinitialization operation*/
                     s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
                     OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
                     if(rstate.error_code)
                     {
                        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
                     }
                     OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
                     OSAL_s32EventDelete("oedt_usbpowerEv1");
                     OSAL_s32IOClose(hDevice);
                     break;
                  }
               }
            }
            /*Initialize the driver fields to zero in order to make sure that it wont affect test case*/
            if(s32RetVal != OEDT_ERROR_EVT_TIMEOUT && s32RetVal != OEDT_ERROR_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD)
            {
               /*perform deinitialization operation*/
               s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
               OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
               if(rstate.error_code)
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
               OSAL_s32EventDelete("oedt_usbpowerEv1");
               OSAL_s32IOClose(hDevice);
            }
         }
         else
         {
            s32RetVal = OEDT_ERROR_REG_NOT;
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
         }

      }
      else
      {
         s32RetVal = OEDT_ERROR_OSAL_OPEN;
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
      }
   }
   /*Initialize the driver fields to zero*/
   for(u8CountPort_init = 1; u8CountPort_init <= OEDT_USB_PORTS; u8CountPort_init++)
   {
      u8Buffer[3] = u8CountPort_init;
      vUSBPWRCallbackHandler((tPVoid)u8Buffer);
   }
   return (tU32)s32RetVal;
}


/************************************************************************
 * FUNCTION:      vUSBPWR_OC_Callback
 * DESCRIPTION:   This is overcurrent callback which gets registered
 *                and gives overcurrent event notification
 * 			       			     
 *
 * PARAMETER:     pVoid - this contains the information of signals
 *                pu32ModeChangeInfo - contains notification and medium type
 *             
 * RETURNVALUE:   void.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *                
 * ************************************************************************/

static tVoid vUSBPWR_OC_Callback(tPCU32 pu32ModeChangeInfo, tVoid* pVoid)
{

   OEDTUsbPortState *pPortState = (OEDTUsbPortState*)pVoid;
   tS32 s32Mask;

   /*check for whether event notification should be performed or not*/
   if(u8OCRegFlg)
   {/*check whether overcurrent active event should be notified*/
      if(pPortState->u8OC == (tU8)OEDT_SIG_TRUE && pPortState->u8PPON == (tU8)OEDT_SIG_FALSE)
      {
         s32Mask = OEDT_USBPWR_OCACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      /*check whether overcurrent inactive event one should be notified*/
      else if(pPortState->u8OC == (tU8)OEDT_SIG_TRUE && pPortState->u8PPON == (tU8)OEDT_SIG_TRUE)
      {
         s32Mask = OEDT_USBPWR_OCINACT1_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      /*check whether overcurrent inactive event two should be notified*/
      else if(pPortState->u8OC == (tU8)OEDT_SIG_FALSE && pPortState->u8PPON == (tU8)OEDT_SIG_TRUE)
      {
         /*It is to ensure that event is in sync(i.e.,event wait is executed and ready for the events to be occured before posting the event)
           because the driver gives callback notification immediately after previous notification*/
         OSAL_s32ThreadWait(100);
         s32Mask = OEDT_USBPWR_OCINACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      else
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback 0x%08x",*pu32ModeChangeInfo);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback 0x%08x",*pu32ModeChangeInfo);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
   }

}

/************************************************************************
 * FUNCTION:      vUSBPWR_UV_Callback
 * DESCRIPTION:   This is undervoltage callback which gets registered
 *                and gives undervoltage event notification
 *
 *
 * PARAMETER:     pVoid - this contains the information of signals
 *                pu32ModeChangeInfo - contains notification and medium type
 *
 * RETURNVALUE:   void.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/

static tVoid vUSBPWR_UV_Callback(tPCU32 pu32ModeChangeInfo, tVoid* pVoid)
{

   OEDTUsbPortState *pPortState = (OEDTUsbPortState*)pVoid;
   tS32 s32Mask;

   /*check undervoltage active or inactive event should be notified*/
   if(u8UVCount == 1 || u8UVCount == 2)
   {
      if(pPortState->u8UV == (tU8)OEDT_SIG_TRUE)/*check if undervoltage is active*/
      {
         /*It is to ensure that event is in sync(i.e.,event wait is executed and ready for the events to be occured before posting the event)
           because the driver gives callback notification immediately after previous notification*/
         OSAL_s32ThreadWait(100);
         s32Mask = OEDT_USBPWR_UVACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      else if(pPortState->u8UV == (tU8)OEDT_SIG_FALSE)/*check if undervoltage is inactive*/
      {
         /*It is to ensure that event is in sync(i.e.,event wait is executed and ready for the events to be occured before posting the event)
           because the driver gives callback notification immediately after previous notification*/
         OSAL_s32ThreadWait(100);
         s32Mask = OEDT_USBPWR_UVINACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      else
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_UV_Callback 0x%08x",*pu32ModeChangeInfo);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
      }
   }
   /*check undervoltage all active or all inactive event should be notified*/
   else if(u8UVCount == 3 || u8UVCount == 4)
   {
      if(pPortState->u8UV == (tU8)OEDT_SIG_TRUE)/*check if undervoltage is active*/
      {
         /*It is to ensure that event is in sync(i.e.,event wait is executed and ready for the events to be occured before posting the event)
           because the driver gives callback notification immediately after previous notification*/
         OSAL_s32ThreadWait(100);
         s32Mask = OEDT_USBPWR_UVALLACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      else if(pPortState->u8UV == (tU8)OEDT_SIG_FALSE)/*check if undervoltage is inactive*/
      {
         /*It is to ensure that event is in sync(i.e.,event wait is executed and ready for the events to be occured before posting the event)
           because the driver gives callback notification immediately after previous notification*/
         OSAL_s32ThreadWait(100);
         s32Mask = OEDT_USBPWR_UVALLINACT_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
      else
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_Callback 0x%08x",*pu32ModeChangeInfo);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_UV_Callback 0x%08x",*pu32ModeChangeInfo);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
   }

}


/************************************************************************
 * FUNCTION:      vUSBPWR_UNDEF_Callback
 * DESCRIPTION:   This is undef signal callback which gets registered
 *                and gives undefined signal event notification
 *
 *
 * PARAMETER:     pVoid - this contains the information of signals
 *                pu32ModeChangeInfo - contains notification and medum type
 *
 * RETURNVALUE:   void.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/

static tVoid vUSBPWR_UNDEF_Callback(tPCU32 pu32ModeChangeInfo, tVoid* pVoid)
{

   OEDTUsbPortState *pPortState = (OEDTUsbPortState*)pVoid;
   tS32 s32Mask;
   /*check for whether event notification should be performed or not*/
   if(u8UndefRegFlg)
   {
      if(pPortState->u8UV == (tU8)OEDT_SIG_UNDEF && pPortState->u8UV == (tU8)OEDT_SIG_UNDEF && pPortState->u8PPON == (tU8)OEDT_SIG_UNDEF)
      {
         s32Mask = OEDT_USBPWR_UNDEF_MASK;
         s32Mask = s32Mask | pPortState->u8PortNr;
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)s32Mask,
                           OSAL_EN_EVENTMASK_OR);
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_UNDEF_Callback 0x%08x",*pu32ModeChangeInfo);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"                                                           ");
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PortNr: %d  ",pPortState->u8PortNr);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8OC:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8OC,  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8UV:     %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8UV,  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"  rPortState->u8PPON:   %d  [StartTime=%d   EndTime=%d]  ",pPortState->u8PPON,pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
   }

}

//#endif

#else /*No LSIM; H/W testing*/

tU8 u8OCRegFlg;
static OSAL_tEventHandle  hoedt_usbpwr_rEventHnd;

static tVoid vUSBPWR_OC_Callback( tPCU32 pu32ModeChangeInfo, tVoid* pVoid );
extern OEDT_TESTSTATE OEDT_CREATE_TESTSTATE(tVoid);

#define OEDT_USB_SIG_UNDEF          0x62
#define EVENT_WAIT_TIMEOUT_USBPWR   10000         //10 sec time out value
#define OEDT_USBPWR_EV_MASK         0x0007FFFF
#define OEDT_USBPWR_OCACT_MASK      0x00001000
#define OEDT_USBPWR_OCINACT_MASK    0x00002000
#define OEDT_USBPWR_OCINACT1_MASK   0x00008000
#define OEDT_USBPWR_UNDEF_MASK      0x00007000
#define OEDT_SIG_TRUE               2
#define OEDT_SIG_FALSE              1
#define OEDT_SIG_UNDEF              0
#define OEDT_OC_MAX_CMD             2

/*****Error Flags************************/
#define OEDT_ERROR_EVT_CREATE       101
#define OEDT_ERROR_OSAL_OPEN        108
#define OEDT_ERROR_EVT_TIMEOUT      102
#define OEDT_ERROR_EVT_CLR          104
#define OEDT_ERROR_WRNG_CMD         106
#define OEDT_ERROR_WRNG_CMD_EVT_CLR 105
#define OEDT_ERROR_REG_NOT          107

typedef struct
{
   tU8  u8PortNr;
   tU8  u8OC;
   tU8  u8UV;
   tU8  u8PPON;

   tU32 u32OCStartTime;
   tU32 u32OCEndTime;
   tU32 u32UVStartTime;
   tU32 u32UVEndTime;
   tU32 u32PPONStartTime;
   tU32 u32PPONEndTime;
}OEDTUsbPortState;

/************************************************************************
 * FUNCTION:      u32USB_PWR_OC_test
 * DESCRIPTION:   Opens dev/prm and Registers the callback to dev/usbpower
 *                and waits for OverCurrent event to be notified from the
 *                registered callback.Then Unregisters the callback and closes
 *                dev/prm.
 *
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.13  Suresh Dhandapani (RBEI/ECF5)
 *
 * ************************************************************************/
tU32 u32USB_PWR_OC_test(void)
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32FunRetVal = OSAL_OK;
   OEDT_TESTSTATE rstate;
   OSAL_tEventMask roedt_ResultMask = 0;
   OSAL_tEventMask oedt_LastEvMask = 0;
   OSAL_trNotifyDataExt2 rNotifyData;
   tU8 u8Count;
   OSAL_tIODescriptor hDevice;

   /*This flag is to set callback not to be executed,beacause while registering itself
     callback will be called from driver. In order for not to process callback notitfication during registration*/
   u8OCRegFlg = FALSE;

   rstate = OEDT_CREATE_TESTSTATE();

   if(OSAL_OK != OSAL_s32EventCreate("oedt_usbpowerEv1", &hoedt_usbpwr_rEventHnd))
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Event creation for USB_Power");
      s32RetVal = OEDT_ERROR_EVT_CREATE;
   }
   else
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

      OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
      if(!rstate.error_code)
      {
         rNotifyData.pu32Data            = 0;
         rNotifyData.ResourceName        = "/dev/usbpower" ;
         rNotifyData.u16AppID            = OSAL_C_U16_MP3CD_APPID ;
         rNotifyData.u8Status            = 0;
         rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_LOW_POW;
         rNotifyData.pCallbackExt2       = (OSALCALLBACKFUNCEXT2)vUSBPWR_OC_Callback;
         /*Registers the callback function*/
         s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
         OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
         if(!rstate.error_code)
         {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
            u8OCRegFlg = TRUE;/*This flag is to set callback to be executed after registration*/

            for(u8Count = 0; u8Count < 2; u8Count++) //one OC Start + one OC END event
			{
				if(OSAL_OK != OSAL_s32EventWait(hoedt_usbpwr_rEventHnd, OEDT_USBPWR_EV_MASK,
										   OSAL_EN_EVENTMASK_OR,
										   EVENT_WAIT_TIMEOUT_USBPWR,
										   &roedt_ResultMask))
				{
					/*If Timeout, then do deinitialization*/
					s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
					OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
					if(rstate.error_code)
					{
						OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
					}
					OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
					OSAL_s32EventDelete("oedt_usbpowerEv1");
					OSAL_s32IOClose(hDevice);
					s32RetVal = OEDT_ERROR_EVT_TIMEOUT;
					OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: Overcurrent event TimeOut for USB_Power");

					break;
				}
				else
				{
					if(oedt_LastEvMask == roedt_ResultMask)
					{
						OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR!: Unexpected Event Notification (0x%08X)(repeated)",oedt_LastEvMask);
						s32RetVal = OEDT_ERROR_WRNG_CMD_EVT_CLR;
					}
					else
					{
						oedt_LastEvMask = roedt_ResultMask;
						OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"SUCCESS: callback received for overcurrent %s(0x%08X)",((roedt_ResultMask>>12)-1)?"INACTIVE":"ACTIVE", roedt_ResultMask);
						if(OSAL_OK != OSAL_s32EventPost( hoedt_usbpwr_rEventHnd,~(roedt_ResultMask),OSAL_EN_EVENTMASK_AND ))
							OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, "OSAL_s32EventPost failed %u", __LINE__);
					}
				}
				if(s32RetVal == OEDT_ERROR_EVT_TIMEOUT || s32RetVal == OEDT_ERROR_EVT_CLR || s32RetVal == OEDT_ERROR_WRNG_CMD_EVT_CLR  || s32RetVal == OEDT_ERROR_WRNG_CMD)
				{
					break;
				}
			}
               /*check whether already deinitialization is performed*/
            if(s32RetVal != OEDT_ERROR_EVT_TIMEOUT && s32RetVal != OEDT_ERROR_EVT_CLR && s32RetVal != OEDT_ERROR_WRNG_CMD_EVT_CLR  && s32RetVal != OEDT_ERROR_WRNG_CMD)
            {
               /*perform deinitialization operation*/
               s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2, (tS32)&rNotifyData);
               OEDT_EXPECT_DIFFERS(OSAL_ERROR, s32FunRetVal, &rstate);
               if(rstate.error_code)
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               else
               {
                  OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2 for USB_Power");
               }
               OSAL_s32EventClose(hoedt_usbpwr_rEventHnd);
               OSAL_s32EventDelete("oedt_usbpowerEv1");
               OSAL_s32IOClose(hDevice);
            }
         }
         else
         {
            s32RetVal = OEDT_ERROR_REG_NOT;
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_C_S32_IOCTRL_REG_NOTIFICATION for USB_Power");
         }

      }
      else
      {
         s32RetVal = OEDT_ERROR_OSAL_OPEN;
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
      }
   }
   return (tU32)s32RetVal;
}

/************************************************************************
 * FUNCTION:      vUSBPWR_OC_Callback
 * DESCRIPTION:   This is overcurrent callback which gets registered
 *                and gives overcurrent event notification
 *
 *
 * PARAMETER:     pVoid - this contains the information of signals
 *                pu32ModeChangeInfo - contains notification and medium type
 *
 * RETURNVALUE:   void.
 * HISTORY:       24.01.14
 *                13.11.2015 Vinutha Eshwar Shantha(RBEI/ECF5) 
 *                           Lint fix for CFG3-1515
 * ************************************************************************/

static tVoid vUSBPWR_OC_Callback(tPCU32 pu32ModeChangeInfo, tVoid* pVoid)
{

   OEDTUsbPortState *pPortState = (OEDTUsbPortState *)pVoid;
   tU8 *u8states[] = {(tU8 *)"UNDEF", (tU8 *)"INACTIVE", (tU8 *)"ACTIVE"};
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ENTER vUSBPWR_OC_Callback 0x%08x",*pu32ModeChangeInfo);
   if(pPortState)
   {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\n");
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\t > \trPortState->u8PortNr: \t%-10d  ",pPortState->u8PortNr);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\t > \trPortState->u8OC:     \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8OC],  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\t > \trPortState->u8UV:     \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8UV],  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\t > \trPortState->u8PPON:   \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8PPON],pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\n");
   }

   /*check for whether event notification should be performed or not*/
   if(u8OCRegFlg)
   {
      //Lint fix for Warning 613: Possible use of null pointer 'pPortState' in left argument to operator '->'
      if((pPortState !=NULL) && (pPortState->u8OC == (tU8)OEDT_SIG_TRUE))
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback EVPOST: 0x%08x",(tU32)OEDT_USBPWR_OCACT_MASK);
         OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
                           (tU32)OEDT_USBPWR_OCACT_MASK,
                           OSAL_EN_EVENTMASK_OR);
      }
      //Lint fix for Warning 613: Possible use of null pointer 'pPortState' in left argument to operator '->'
      else if((pPortState !=NULL) && (pPortState->u8OC == (tU8)OEDT_SIG_FALSE))
      {
		 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback EVPOST: 0x%08x",(tU32)OEDT_USBPWR_OCINACT_MASK);
		 OSAL_s32EventPost(hoedt_usbpwr_rEventHnd,
						   (tU32)OEDT_USBPWR_OCINACT_MASK,
						   OSAL_EN_EVENTMASK_OR);
      }
      else
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback UnDefined ev OC state 0x%08x",*pu32ModeChangeInfo);

   }
   else
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"vUSBPWR_OC_Callback IGNORED u8OCRegFlg not set 0x%08x",*pu32ModeChangeInfo);


}

/************************************************************************
 * FUNCTION:      u32USB_PWR_CTRL_test
 * DESCRIPTION:   Opens dev/prm and toggles the power state and verifies
 * 					the change in state of the port power in the USB Ports
 * 					in sequence.
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.14
 *                13.11.2015 Vinutha Eshwar Shantha(RBEI/ECF5) 
 *                           Lint fix for CFG3-1515
 * ************************************************************************/
tU32 u32USB_PWR_CTRL_test(void)
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32FunRetVal = OSAL_OK;
   OEDT_TESTSTATE rstate;
   tS32 s32Mask = 1;
   tU8 u8Count;
   tS32 PortCount = -1;
   OEDTUsbPortState rPortState = {0};
   OSAL_tIODescriptor hDevice;
   tU32 u32StableCheck = 5;

   struct usb_port_control
   {
   	tU32 PortMask;
   	tU32 ValueMask;
   };

	struct usb_port_control cmask[2] = {{1,0}, //OFF
										{1,1}};//ON
	struct usb_port_control cmask_temp;



   rstate = OEDT_CREATE_TESTSTATE();


	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

	OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
	if(!rstate.error_code)
	{
		s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT, (tS32)&PortCount);
      
      //Supress lint info 774: Boolean within 'if' always evaluates to False 
     //lint -e774 
		if((s32FunRetVal == OSAL_OK)&&(PortCount > 0))
		{
			OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTCOUNT of %d",PortCount);
         
         //Supress Warning info 681: Loop is not entered
         //lint -e681
			for(u8Count = 1; u8Count <=PortCount; u8Count++)
			{

				rPortState.u8PortNr = u8Count;

				OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nPRM_USBPOWER_CTRL of PORT %d: START\n",u8Count);

				OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);
				sleep(u32StableCheck);

					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (tS32)&rPortState);
					 if(s32FunRetVal == OSAL_OK)
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTSTATE INITIAL PORTSTATE of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);

				 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);
				 sleep(u32StableCheck);

				 if(rPortState.u8PPON == 0x2) //IF PP ON true, switch OFF port else power ON first
					 s32Mask = 0; //OFF
				 else
					 s32Mask = 1;//ON

				 cmask_temp.PortMask = cmask[s32Mask].PortMask << (u8Count-1);
				 cmask_temp.ValueMask = cmask[s32Mask].ValueMask << (u8Count-1);

					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER, (tS32)&cmask_temp);
					 if(s32FunRetVal == OSAL_E_NOERROR)
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_SET_PORTPOWER of %d to %s",u8Count,s32Mask?"ON":"OFF");
					 else
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_SET_PORTPOWER of %d ERROR:0x%X",u8Count,s32FunRetVal );

				 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);
				 sleep(u32StableCheck);

				 rPortState.u8PortNr = u8Count;
					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (tS32)&rPortState);
					 if(s32FunRetVal == OSAL_OK)
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTSTATE of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);

					 s32Mask = (s32Mask+1)%2; /*Toggle State*/
					 cmask_temp.PortMask = cmask[s32Mask].PortMask << (u8Count-1);
					 cmask_temp.ValueMask = cmask[s32Mask].ValueMask << (u8Count-1);

				 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);
				 sleep(u32StableCheck);

					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER, (tS32)&cmask_temp);
					 if(s32FunRetVal == OSAL_E_NOERROR)
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_SET_PORTPOWER of %d  to %s  ",u8Count, s32Mask?"ON":"OFF");
					 else
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_SET_PORTPOWER of %d ERROR:0x%X",u8Count,s32FunRetVal );

				 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);
				 sleep(u32StableCheck);

				 rPortState.u8PortNr = u8Count;
				 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (tS32)&rPortState);

					 if(s32FunRetVal == OSAL_OK)
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTSTATE  of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);


					 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nPRM_USBPOWER_CTRL of PORT %d: END\n",u8Count);
			}
		}
		else
			 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTCOUNT with %d , ERR:0x%X",PortCount ,s32FunRetVal);
		OSAL_s32IOClose( hDevice );
	}
	else
	{
	 s32RetVal = OEDT_ERROR_OSAL_OPEN;
	 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
	}

   return (tU32)s32RetVal;
}

/************************************************************************
 * FUNCTION:      u32USB_PWR_CTRL_Portstate
 * DESCRIPTION:   Opens /dev/prm , gets port count and port state  
 * 					in the USB Ports in sequence.
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.14
 *                13.11.2015 Vinutha Eshwar Shantha(RBEI/ECF5) 
 *                           Lint fix for CFG3-1515
 * ************************************************************************/
tU32 u32USB_PWR_CTRL_Portstate(void)
{
	tS32 s32RetVal = OSAL_OK;
	tS32 s32FunRetVal = OSAL_OK;
	OEDT_TESTSTATE rstate;
	tU8 u8Count;
	tS32 PortCount = -1;
	OEDTUsbPortState rPortState = {0};
	OSAL_tIODescriptor hDevice;

	struct usb_port_control
	{
	tU32 PortMask;
	tU32 ValueMask;
	};

	rstate = OEDT_CREATE_TESTSTATE();

	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

	OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
	if(!rstate.error_code)
	{
		s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT, (tS32)&PortCount);
      
       //Supress lint info 774: Boolean within 'if' always evaluates to False 
      //lint -e774 
		if((s32FunRetVal == OSAL_OK)&&(PortCount > 0))
		{
			OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTCOUNT of %d",PortCount);
         
         //Supress Warning info 681: Loop is not entered
         //lint -e681
			for(u8Count = 1; u8Count <=PortCount; u8Count++)
			{

				rPortState.u8PortNr = u8Count;

				 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (tS32)&rPortState);
				 if(s32FunRetVal == OSAL_OK)
					 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTSTATE PORT %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
				 else
					 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);
			}
		}
		else
			 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTCOUNT with %d , ERR:0x%X",PortCount ,s32FunRetVal);
		OSAL_s32IOClose( hDevice );
	}
	else
	{
	 s32RetVal = OEDT_ERROR_OSAL_OPEN;
	 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
	}

	return (tU32)s32RetVal;
}

/************************************************************************
 * FUNCTION:      u32USB_PWR_CTRL_PPC
 * DESCRIPTION:   Opens /dev/prm and changes the power in USB port to ON/OFF
 * 					according to the port number and state passed
 *
 * PARAMETER:     Port Number and required power state ( ON/OFF )
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.14
 *                13.11.2015 Vinutha Eshwar Shantha(RBEI/ECF5) 
 *                           Lint fix for CFG3-1515
 * ************************************************************************/
static tU32 u32USB_PWR_CTRL_PPC(tU8 u8Count, tBool bState)
{
	   tS32 s32RetVal = OSAL_OK;
	   tS32 s32FunRetVal = OSAL_OK;
	   OEDT_TESTSTATE rstate;
	   tS32 s32Mask = 1;
	   tS32 PortCount = -1;
	   OEDTUsbPortState rPortState = {0};
	   OSAL_tIODescriptor hDevice;
	   tU32 u32StableCheck = 5;

	   struct usb_port_control
	   {
	   	tU32 PortMask;
	   	tU32 ValueMask;
	   };

		struct usb_port_control cmask[2] = {{1,0}, //OFF
											{1,1}};//ON
		struct usb_port_control cmask_temp;

	   rstate = OEDT_CREATE_TESTSTATE();

		hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );

		OEDT_EXPECT_DIFFERS(OSAL_ERROR, hDevice, &rstate);
		if(!rstate.error_code)
		{
			s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT, (tS32)&PortCount);
         
          //Supress lint info 774: Boolean within 'if' always evaluates to False 
         //lint -e774 
			if((s32FunRetVal == OSAL_OK)&&(PortCount > 0))
			{
				OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTCOUNT of %d",PortCount);
				if((u8Count > 0) && (u8Count <= PortCount))
				{

					rPortState.u8PortNr = u8Count;

						 s32Mask = bState; //0=OFF, 1=ON

					 cmask_temp.PortMask = cmask[s32Mask].PortMask << (u8Count-1);
					 cmask_temp.ValueMask = cmask[s32Mask].ValueMask << (u8Count-1);

						 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER, (tS32)&cmask_temp);
						 if(s32FunRetVal == OSAL_E_NOERROR)
							 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_SET_PORTPOWER of %d to %s",u8Count,s32Mask?"ON":"OFF");
						 else
							 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_SET_PORTPOWER of %d ERROR:0x%X",u8Count,s32FunRetVal );

					 sleep(u32StableCheck);
					 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"\nsleep(%u)\n", u32StableCheck);


					 rPortState.u8PortNr = u8Count;
						 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (tS32)&rPortState);
						 if(s32FunRetVal == OSAL_OK)
							 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OK: PRM_USBPOWER_GET_PORTSTATE of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
						 else
							 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);

						 if(bState+1 == rPortState.u8PPON)//Dirty hack
							 s32RetVal = OSAL_OK;
						 else
							 s32RetVal = OSAL_ERROR;


				}
				else
				{
					OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER wrong PortNo :%d , ERR:0x%X",u8Count ,s32FunRetVal);
				}


			}
			else
				 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: PRM_USBPOWER_GET_PORTCOUNT with %d , ERR:0x%X",PortCount ,s32FunRetVal);
			OSAL_s32IOClose( hDevice );
		}
		else
		{
		 s32RetVal = OEDT_ERROR_OSAL_OPEN;
		 OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"ERROR: OSAL_IOOpen for USB_Power");
		}

	   return (tU32)s32RetVal;
}
/************************************************************************
 * FUNCTION:      u32USB_PWR_CTRL_P**
 * DESCRIPTION:   Wrapper for u32USB_PWR_CTRL_PPC for different ports and states
 *
 * PARAMETER:     Nil
 *
 * RETURNVALUE:   tU32 => OEDT error code.
 * HISTORY:       24.01.14
 *
 * ************************************************************************/
tU32 u32USB_PWR_CTRL_P1OFF(void)
{
	return u32USB_PWR_CTRL_PPC(1, 0);
}
tU32 u32USB_PWR_CTRL_P1ON(void)
{
	return u32USB_PWR_CTRL_PPC(1, 1);
}
tU32 u32USB_PWR_CTRL_P2OFF(void)
{
	return u32USB_PWR_CTRL_PPC(2, 0);
}
tU32 u32USB_PWR_CTRL_P2ON(void)
{
	return u32USB_PWR_CTRL_PPC(2, 1);
}

#endif
/************************************************************************
 * FUNCTION:      u32PRM_DrvOpenClose 
 * DESCRIPTION:   1.Opens the device.
 * 			      2.Close the device. 			     
 *
 * PARAMETER:     Nil
 *             
 * RETURNVALUE:   Open and Close PRM Device
 * HISTORY:       22.09.11  Martin Langer (CM-AI/PJ-CF33)
 *                Oct 14,2011 Anooj Gopi(RBEI/ECF1)
 *                   Modified to fix multiple function return.
 * ************************************************************************/
tU32 u32PRM_DrvOpenClose(void)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32Error = 0;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32Error = OSAL_u32ErrorCode();
   }
   else if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      u32Error = OSAL_u32ErrorCode();
      u32Error += 1000;
   }

   return u32Error;
} // End of u32PRM_DrvOpenClose()



/************************************************************************
* FUNCTION		:	vInsertNotiHandlerCDVD ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CD/DVD
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 17, 2008 
************************************************************************/
tVoid vInsertNotiHandlerCDVD(const tU32* pu32MediaChangeInfo)
{
   /*	OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "CD Handler %x!!!\n",*pu32MediaChangeInfo ); */
   if(pu32MediaChangeInfo != NULL)
   {
       /*Check for the type of notification. For this extract the high word
       & based on the notification type call the corresponding notification
       handler*/
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_DEVICE_READY)
      {
         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DEVICE_READY)
         {
            oedt_prm_status = 1;
         }
         else if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DEVICE_NOT_READY)
         {
            oedt_prm_status = 2;
         }
         else
         {
            RetNotiInsetEject +=201;
         }
      }
      else if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA
                              && oedt_prm_status == 1 )
         {
            oedt_prm_status = 0;
         }
         else if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_MEDIA_EJECTED
                              && oedt_prm_status == 2 )
         {
            oedt_prm_status = 0;
         }
         else
         {
            RetNotiInsetEject +=202;
         }
      }
      else
      {
         RetNotiInsetEject +=203;
      }
   }
   if (RetNotiInsetEject == 500 )
      RetNotiInsetEject = 0;
} /*  End of vInsertNotiHandlerCDVD */


/************************************************************************
* FUNCTION		:	vInsertNotiHandlerCARD ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CARD
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 17, 2008 
************************************************************************/
tVoid vInsertNotiHandlerCARD(const tU32* pu32MediaChangeInfo)
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "In InsertNotihandler !!!\n");

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA )
         {
            oedt_prm_status = 0;
         }
         else if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_UNKNOWN_MEDIA )
         {
            oedt_prm_status = 0;
         }
         else
         {
            RetNotiInsetEject +=202;
         }
      }
      else
      {
         RetNotiInsetEject +=203;
      }
   }
   if (RetNotiInsetEject == 500)
      RetNotiInsetEject = 0;
   if (RetNotiInsetEject == 300)
      RetNotiInsetEject = 15;
} /*  End of vInsertNotiHandlerCARD */

/************************************************************************
* FUNCTION		:	vNaviData_Media_HandlerCARD ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CARD
                    to check the OSAL_C_U16_DATA_MEDIA_NAVI event
* HISTORY		:	Created by Sainath Kalpuri (RBEI/ECM1) on Mar 10, 2009 
************************************************************************/
tVoid vNaviData_Media_HandlerCARD(const tU32* pu32MediaChangeInfo)
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
               "In Navidatahandler !!!\n");

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         bIsMedia_Change_Notified = TRUE;
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "got noti media change eve !!!\n");

         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA_NAVI )
         {
            bIsNaviData_Media_Found = TRUE;
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                     "got navi data media eve !!!\n");

         }
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "got no event !!!\n");
      }
   }
}

/************************************************************************
* FUNCTION   	:	vNaviData_Media_HandlerCARD_devcard ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CARD
                    to check the OSAL_C_U16_DATA_MEDIA_NAVI event
* HISTORY		:	Created by Sainath Kalpuri (RBEI/ECM1) on Mar 18, 2009 
************************************************************************/
tVoid vNaviData_Media_HandlerCARD_devcard(const tU32* pu32MediaChangeInfo)
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
               "In devcardNavidatahandler !!!\n");

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "devcard: got noti media change eve !!!\n");

         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA_NAVI )
         {
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                     "got devnavi data media eve !!!\n");

         }
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "got no event !!!\n");
      }
   }
}


/************************************************************************
* FUNCTION		:	vNaviData_Media_HandlerCARD_crypt ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CARD
                    to check the OSAL_C_U16_DATA_MEDIA_NAVI event
* HISTORY		:	Created by Sainath Kalpuri (RBEI/ECM1) on Mar 18, 2009 
************************************************************************/
tVoid vNaviData_Media_HandlerCARD_crypt(const tU32* pu32MediaChangeInfo)
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "In CyrpNavidatahandler !!!\n");

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "cyp: got noti media change eve !!!\n");

         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA_NAVI )
         {
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "got cryptnavi data media eve !!!\n");
         }
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "got no event !!!\n");
      }
   }
}

/************************************************************************
* FUNCTION		:	vData_Media_HandlerCARD ()
* PARAMETER		:	Notification status, UUID
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for CARD
                    to check the OSAL_C_U16_DATA_MEDIA event
* HISTORY		:	Created by Sainath Kalpuri (RBEI/ECM1) on Mar 10, 2009
*                 Anooj Gopi (RBEI/ECF1) Oct 17, 2011
*                   Modified to support /dev/media EXT2 notification
************************************************************************/
tVoid vData_Media_HandlerCARD(tPU32 pu32MediaChangeInfo, tU8 au8String[])
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
         "In InsertNotihandler Device : %s !!!\n", au8String);

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         bIsMedia_Change_Notified = TRUE;
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "got noti media change eve !!!\n");

         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA )
         {
            bIsData_Media_Found = TRUE;
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "got data media eve !!!\n");
         }
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "got no event !!!\n");
      }
   }
}


/*****************************************************************************
* FUNCTION		:	u32PRM_CDVD_inserted_event( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_PRM_002
* DESCRIPTION	:	Register to PRM to get 'CD/DVD inserted' event
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 17, 2008  
******************************************************************************/
tU32 u32PRM_CDVD_inserted_event(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tU32 u32Ret = 0;
   OSAL_trNotifyData rNotifyData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      if( OSAL_s32IOControl ( hDevice , OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA,
                                       0 ) == OSAL_ERROR )
      {
         u32Ret += 2;
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "No media present inside");
      }
      else
      {
         OSAL_s32ThreadWait(3000);
         rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
         rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL;
         rNotifyData.u16NotificationType =
               (OSAL_C_U16_NOTI_MEDIA_CHANGE | OSAL_C_U16_NOTI_DEVICE_READY );
         rNotifyData.pCallback = (OSALCALLBACKFUNC)vInsertNotiHandlerCDVD;
         if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION,
                           (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 3;
         }
         else
         {
            if( OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR,
                                    0) == OSAL_ERROR )
            {
               u32Ret += 4;
               OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Inserting media failed");
            }
            RetNotiInsetEject = 500;
            OSAL_s32ThreadWait(8000);
            if( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA,
                                       0 ) == OSAL_ERROR )
            {
               u32Ret += 6;
               OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "No media present inside");
            }

            OSAL_s32ThreadWait(5000);
            if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                                 (tS32)&rNotifyData) == OSAL_ERROR)
            {
               u32Ret += 5;
            }
         }
      }
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 6;
      }
   }
   return (u32Ret + RetNotiInsetEject);
}


/*****************************************************************************
* FUNCTION     :  u32PRM_CARD_inserted_event( )
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_PRM_003
* DESCRIPTION  :  Register to PRM to get 'SD-Card inserted' event
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 17, 2008
*                 Anooj Gopi (RBEI/ECF1) on Oct 17, 2011 :-
*                  Modified to fix device close error
******************************************************************************/
tU32 u32PRM_CARD_inserted_event(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trNotifyData rNotifyData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CARD, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please eject the card now !!!\n");
      OSAL_s32ThreadWait(5000);
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = OSAL_C_STRING_DEVICE_CARD;
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE );
      rNotifyData.pCallback = (OSALCALLBACKFUNC)vInsertNotiHandlerCARD;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 2;
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please insert the card now !!!\n");
         RetNotiInsetEject = 500;
         OSAL_s32ThreadWait(15000);
         if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,
                                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 10;
         }
         if (RetNotiInsetEject == 500)
            u32Ret += 500;
         RetNotiInsetEject = 300;
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please eject and insert the card again !!!\n");
         OSAL_s32ThreadWait(15000);
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 100;
      }
   }

   if (RetNotiInsetEject == 300)
      RetNotiInsetEject = 0;
   return (u32Ret + RetNotiInsetEject);
}

/*****************************************************************************
* FUNCTION		:	u32PRM_NaviDataMedia_event( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Register to PRM to get 'SD-Card inserted' event
* HISTORY		:	Created by Sainath (RBEI/ECF1) on Mar 10, 2009 
                    Modified the  app id and card insertion/removal
                    sequence trases by Sainath (RBEI/ECF1) on Mar 18,2009
                    Modified to fix multiple function return and device close error
                    by Anooj Gopi(RBEI/ECF1) on Oct 14,2011.
* Additional Info:  1. This test can be done using "Crypt Enabled" and
                       using "Crypt Disabled"
                    2. To test with Crypt Enable, enable the crypt feature and
                       ensure that you have a navidata under cryptnav folder and
                       run the steps from 4 onwards.
                    3. To test with Crypt Disable, disable the crypt feature and 
                       ensure that navidata is under root directory of the card and 
                       run the steps from 4 onwards
                    4.Select OEDT_DATASET_SELECT 1 
                    5.Call the test function by OEDT_TEST_SELECTIVE X X 1 1
                     (X is the class ID for File System and 2 is the test ID) 
                     with all testcases relevant to File System enabled in 
                     oedt_AutoTester_Testdescription.c 
******************************************************************************/
tU32 u32PRM_NaviDataMedia_event(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trNotifyData rNotifyData;

   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                      "If card is not inserted, insert it now !!!\n");
   OSAL_s32ThreadWait(15000);

   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CRYPTCARD, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 10;
   }
   else
   {
      /*   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please eject the card now !!!\n");
      OSAL_s32ThreadWait(5000); */
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = OSAL_C_STRING_DEVICE_CARD;
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);

      rNotifyData.pCallback = (OSALCALLBACKFUNC)vNaviData_Media_HandlerCARD;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 20;
      }
      else
      {
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please eject the card now !!!\n");
         OSAL_s32ThreadWait(5000);
            OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please insert the card now !!!\n");
         OSAL_s32ThreadWait(10000);


         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 40;
         }
      }
      if((bIsMedia_Change_Notified == FALSE)
         || (bIsNaviData_Media_Found == FALSE))
      {
         u32Ret += 80;
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 160;
      }
   }

   bIsMedia_Change_Notified = FALSE;
   bIsNaviData_Media_Found = FALSE;
   return u32Ret;
}

/*****************************************************************************
* FUNCTION		:	u32PRM_NaviDataMedia_event_TEST( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	This OEDT tests the capability of registering for notification
                    for devcard and crypt card using different app ID
* HISTORY		:	Created by Sainath (RBEI/ECF1) on Mar 18, 2009 
                  Modified to fix multiple function return and device close error
                  by Anooj Gopi(RBEI/ECF1) on Oct 14,2011.
* Additional Info:  1. This test can be done using "Crypt Enabled" 
                    2. To test with Crypt Enable, enable the crypt feature and
                       ensure that you have a navidata under cryptnav folder, and
                       also ensure that you have normal dev card having a navi data on
                       root directory and run the steps from 4 onwards
                    4.Select OEDT_DATASET_SELECT 1 
                    5.Call the test function by OEDT_TEST_SELECTIVE X X 1 1
                     (X is the class ID for File System and 2 is the test ID) 
                     with all testcases relevant to File System enabled in 
                     oedt_AutoTester_Testdescription.c 
******************************************************************************/
tU32 u32PRM_NaviDataMedia_event_TEST(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hDevice_crypt = 0;

   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trNotifyData rNotifyData;
   OSAL_trNotifyData rNotifyData_crypt;

   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 10;
   }
   else
   {
      OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please eject the card now !!!\n");
      OSAL_s32ThreadWait(10000);
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = OSAL_C_STRING_DEVICE_CARD;
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);

      rNotifyData.pCallback = (OSALCALLBACKFUNC)vNaviData_Media_HandlerCARD_devcard;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 20;
      }
      OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please insert the card now !!!\n");
      OSAL_s32ThreadWait(10000);

      hDevice_crypt = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CRYPTCARD, enAccess );
      if( hDevice_crypt == OSAL_ERROR )
      {
         u32Ret += 40;
      }
      else
      {
         /* Different App Id is required for registration, because
            registation fails with same appid unless unregister
            the earlier one */
         rNotifyData_crypt.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
         rNotifyData_crypt.ResourceName = OSAL_C_STRING_DEVICE_CRYPTCARD;
         rNotifyData_crypt.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);

         rNotifyData_crypt.pCallback = (OSALCALLBACKFUNC)vNaviData_Media_HandlerCARD_crypt;
         if(OSAL_s32IOControl (hDevice_crypt, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData_crypt) == OSAL_ERROR)
         {
            u32Ret += 80;
         }

         OSAL_s32ThreadWait(10000);
         if(OSAL_s32IOControl(hDevice_crypt, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                       (tS32)&rNotifyData_crypt) == OSAL_ERROR)
         {
            u32Ret += 160;
         }

         if( OSAL_s32IOClose ( hDevice_crypt ) == OSAL_ERROR )
         {
            u32Ret += 320;
         }
      }

      if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                       (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 640;
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 1280;
      }
   }

   return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32PRM_DataMedia_event( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Register to PRM to get 'SD-Card inserted' event
* HISTORY		:	Created by Sainath (RBEI/ECF1) on Mar 10, 2009
               :  Modified the  app id by sak9kor on Mar 18, 2009
               :  Modified to use EXT2 IO control for data media devices
                  by Anooj Gopi(RBEI/ECF1) on Oct 14,2011.
* Additional Info:  1. Ensure that no navi data is available in sd card.
                    2.Select OEDT_DATASET_SELECT 1 
                    3.Call the test function by OEDT_TEST_SELECTIVE X X 2 2 
                    (X is the class ID for File System and 2 is the test ID)
                    with all testcases relevant to File System enabled in 
                    oedt_AutoTester_Testdescription.c  
******************************************************************************/
tU32 u32PRM_DataMedia_event(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trNotifyDataExt2 rNotifyData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 10;
   }
   else
   {
      OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                        "Please eject the card now !!!\n");
      OSAL_s32ThreadWait(5000);
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = "/dev/media";
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      rNotifyData.pCallbackExt2 = vData_Media_HandlerCARD;

      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 20;
      }
      else
      {
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                            "Please insert the card now !!!\n");
         OSAL_s32ThreadWait(15000);
         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 40;
         }
      }
      if((bIsMedia_Change_Notified == FALSE)
         || (bIsData_Media_Found == FALSE))
      {
         u32Ret += 80;
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 160;
      }
   }

   bIsMedia_Change_Notified = FALSE;
   bIsData_Media_Found = FALSE;
   return u32Ret;
}

/************************************************************************
* FUNCTION     :  vInsertNotiHandlerDevMedia ()
* PARAMETER    :  Notification status and UUID
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for insert/eject event for /dev/media
* HISTORY      :  Created by Anooj Gopi (RBEI/ECF1) on Oct 17, 2011
************************************************************************/
tVoid vInsertNotiHandlerDevMedia( tPU32 pu32MediaChangeInfo, tU8 au8String[] )
{
   OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
         "In InsertNotihandler Device : %s !!!\n", au8String);

   if(pu32MediaChangeInfo != NULL)
   {
      tU16 u16NotiType = 0;
      u16NotiType = HIWORD(*pu32MediaChangeInfo);
      if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
      {
         if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA ||
             (tU16) *pu32MediaChangeInfo == OSAL_C_U16_UNKNOWN_MEDIA )
         {
            oedt_prm_status = 0;
            OSAL_s32PrintFormat(szDevname, "/dev/media/%s", au8String);
         }
         else
         {
            RetNotiInsetEject +=202;
         }
      }
      else
      {
         RetNotiInsetEject +=203;
      }
   }
   if (RetNotiInsetEject == 500)
      RetNotiInsetEject = 0;
   if (RetNotiInsetEject == 300)
      RetNotiInsetEject = 15;
}

/*****************************************************************************
* FUNCTION     :  u32PRM_DevMedia_event
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Register to PRM to get /dev/media device events
* HISTORY      :  Created by Anooj Gopi (RBEI/ECF1) on Oct 17, 2011
******************************************************************************/
tU32 u32PRM_DevMedia_event(tVoid)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tIODescriptor hUSBDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   OSAL_trNotifyDataExt2 rNotifyData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      u32Ret += 1;
   }
   else
   {
      OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please eject the card now !!!\n");
      OSAL_s32ThreadWait(5000);
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = "/dev/media";
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE );
      rNotifyData.pCallbackExt2 = (OSALCALLBACKFUNCEXT2)vInsertNotiHandlerDevMedia;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         u32Ret += 2;
      }
      else
      {
         szDevname[0] = '\0';
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please insert the card now !!!\n");
         RetNotiInsetEject = 500;
         OSAL_s32ThreadWait(15000);
         if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                                          (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32Ret += 3;
         }
         if (RetNotiInsetEject == 500)
         {
            u32Ret += 500;
         }
         else
         {
            /* Notification is been called.. Lets try opening the device */
            if(szDevname[0] != '\0')
            {
               hUSBDevice = OSAL_IOOpen( szDevname, OSAL_EN_READONLY );
               if( hUSBDevice == OSAL_ERROR )
               {
                  u32Ret += 4;
               }
               else if (OSAL_s32IOClose ( hUSBDevice ) == OSAL_ERROR)
               {
                  u32Ret += 5;
               }
            }
            else
            {
               u32Ret += 6;
            }
         }
         RetNotiInsetEject = 300;
         OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Please eject and insert the card again !!!\n");
         OSAL_s32ThreadWait(15000);
      }

      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32Ret += 100;
      }
   }

   if (RetNotiInsetEject == 300)
      RetNotiInsetEject = 0;
   return (u32Ret + RetNotiInsetEject);
}

tU32 u32Data;
tVoid vNotifyHandler(const tU32* pu32Param)
{
        if (*pu32Param  == WRITE_ERRMEM_START)
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"WRITE_ERRMEM_START event received");
        }
        else if (*pu32Param  == WRITE_ERRMEM_SUCCESS)
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"WRITE_ERRMEM_SUCCESS event received");
        }
        else if (*pu32Param  == ERASE_ERRMEM_START)
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"ERASE_ERRMEM_START event received");
        }
        else if (*pu32Param  == ERROR_MOUNT_FAILED)
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"ERROR_MOUNT_FAILED event received");
        }
        else if (*pu32Param  == WRITE_ERRMEM_FAILED)
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"WRITE_ERRMEM_FAILED event received");
        }
        else 
        {
         OEDT_HelperPrintf(TR_LEVEL_USER_1,"unknown event received");
        }        
}

tU32 u32PRM_CallbackHandler(void)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_trNotifyDataSystem rNotifyData;
   tU32 u32Ret = 0;

   hDevice = OSAL_IOOpen( "/dev/prm", OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
         OEDT_HelperPrintf(TR_LEVEL_FATAL,"ERROR: OSAL_IOOpen() failed with error '%i'", OSAL_u32ErrorCode());
         u32Ret+=1;
   }
   else
   {
        rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
        rNotifyData.pCallback = (OSALCALLBACKFUNC)vNotifyHandler;
        rNotifyData.pu32Data  = &u32Data;

        if(OSAL_s32IOControl (hDevice, 
                              OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO ,
                              (tS32)&rNotifyData) == OSAL_ERROR)
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL,"ERROR: OSAL_s32IOControl() failed with error '%i'", OSAL_u32ErrorCode());
            u32Ret+=2;
        }

	   if(OSAL_s32IOClose (hDevice) == OSAL_ERROR)
	   {
			OEDT_HelperPrintf(TR_LEVEL_FATAL,"ERROR: OSAL_s32IOClose() failed with error '%i'", OSAL_u32ErrorCode());
			u32Ret+=3;
	   }
   }
   return u32Ret;
}

/************************************************************************
* FUNCTION		:	vData_Media_HandlerCARD ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for getting status
*               helper function for u32PRM_GetStatus()
* HISTORY		  :	Created by Martin Langer (CM-AI/PJ-CF33)
************************************************************************/
tVoid u32PRM_Callback_GetDeviceReadyStatus(const tU32* pu32MediaChangeInfo)
{
  bIsCallback_Called = TRUE;
  bIsDeviceReady = FALSE;
  bIsError = FALSE;
  
  OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG: callback is called");

	if(pu32MediaChangeInfo != NULL)
  {
   	tU16 u16NotiType = 0;
   	u16NotiType = HIWORD(*pu32MediaChangeInfo);
    
    if (u16NotiType  == OSAL_C_U16_NOTI_DEVICE_READY)
    {
      bIsDeviceReady = TRUE;
		} else {
      bIsError = TRUE;
      OEDT_HelperPrintf( TR_LEVEL_FATAL, "ERROR: got neither status 'device is ready', value is '%i'", u16NotiType );
    }
    
	}
}


/*****************************************************************************
* FUNCTION		:	u32PRM_GetStatus( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Register to PRM and check status
*               https://hi-cmts.apps.intranet.bosch.com:8443/browse/CF3PF-600
* HISTORY		  :	Created by Martin Langer (CM-AI/PJ-CF33)
******************************************************************************/
tU32 u32PRM_GetStatus(tVoid)
{
  OSAL_tIODescriptor hDevice;
  tU32 u32Error = 0;
	OSAL_trNotifyData rNotifyData;

  bIsCallback_Called = FALSE;
  bIsDeviceReady = FALSE;
  bIsError = FALSE;

  hDevice = OSAL_IOOpen( "/dev/prm", OSAL_EN_READWRITE );
  if ( hDevice == OSAL_ERROR)
 	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_IOOpen() failed with error '%i'", OSAL_u32ErrorCode() );
		return 1;
	}

	rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
	rNotifyData.ResourceName = "/dev/cryptcard"; 
	rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DEVICE_READY;
	rNotifyData.pCallback = (OSALCALLBACKFUNC)u32PRM_Callback_GetDeviceReadyStatus;
  
	if ( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION, (tS32)&rNotifyData ) == OSAL_ERROR )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(REG) failed with error '%i'", OSAL_u32ErrorCode() );
		u32Error += 2;
	}
	else
	{
    OSAL_s32ThreadWait(50);       
     
    if ( bIsCallback_Called == FALSE )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: Callback is called to get the init status for the first time");
      u32Error += 4;
    } else {
      if (bIsError)
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got an error from callback");
        u32Error += 8;
      }
    }
      
		if ( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION, (tS32)&rNotifyData ) == OSAL_ERROR )
		{
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOControl(UNREG) failed with error '%i'", OSAL_u32ErrorCode() );
			u32Error += 16;
		}
	}
  
	if ( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32IOClose() failed with error '%i'", OSAL_u32ErrorCode() );
    u32Error += 32;
	}
	
	return u32Error;
}
/*****************************************************************************
* FUNCTION:        vData_Media_HandlerCardState() 
* PARAMETER:       Notification status, UUID
* RETURNVALUE:     None
* TEST CASE:       TU_OEDT_CARD_CFS_090
* DESCRIPTION:     Callback function for getting device status & UUID and get cardstate
* HISTORY:         Created by pmh5kor, August 5,2014
******************************************************************************/

tVoid vData_Media_HandlerCardState(tPU32 pu32MediaChangeInfo, tU8 au8String[])
{
    
    (void)pu32MediaChangeInfo; //lint
    OSAL_trIOCtrlCardState cardstate={0};
    OSAL_tIODescriptor hFd;
    tS32 ercd = 0;
    if(pu32MediaChangeInfo != NULL)
    {
        tU16 u16NotiType = HIWORD(*pu32MediaChangeInfo);
        if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
        {
            if ((tU16) *pu32MediaChangeInfo != OSAL_C_U16_MEDIA_EJECTED )
            {
                
                OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "media available !!! Device is %s \n",au8String);
                strncpy(cardstate.szDevicName,au8String,OEDT_STRING_LENGTH);
                hFd = OSAL_IOOpen( ( tCString )"/dev/media", OSAL_EN_READWRITE );
                if ( hFd == OSAL_ERROR )
                {
                    Errorcode += 2;
                }
                else
                {
                    if 
                    ( ( tU32 )OSAL_ERROR != ( tU32 )OSAL_s32IOControl
                    ( hFd,OSAL_C_S32_IOCTRL_CARD_STATE,(tS32)&cardstate) )
                    {
                        bISSdcard_inserted = TRUE;
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," bHW_WriteProtected   %d\n", 
                            cardstate.bHW_WriteProtected);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," bSW_WriteProtected   %d\n", 
                            cardstate.bSW_WriteProtected);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"(bMounted             %d)\n", 
                            cardstate.bMounted);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"(u16UncleanUnmountCnt %d)\n", 
                            cardstate.u16UncleanUnmountCnt);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"(u16UncleanWriteCnt   %d)\n", 
                            cardstate.u16UncleanWriteCnt);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"(u16UncleanReadCnt    %d)\n", 
                            cardstate.u16UncleanReadCnt);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u8ManufactureId      0x%02X\n", 
                            (tU32)cardstate.u8ManufactureId);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u32SerialNumber      0x%08X\n", 
                            (tU32)cardstate.u32SerialNumber);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u8SDCardSpecVersion  %d\n", 
                                cardstate.u8SDCardSpecVersion);
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u8CIDRegister        ");
                        for(ercd=0; ercd<16; ercd++)
                        {
                            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"%02X ", cardstate.u8CIDRegister[ercd]);
                        }
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"\n");
                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u64CardSize High    0x%08lX\n", 
                        (tU32)(cardstate.u64CardSize >> 32));

                        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1," u64CardSize Low     0x%08lX\n", 
                        (tU32)(cardstate.u64CardSize &0xFFFFFFFF));

                        
                    }
                    else
                    {
                        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                            "Card State Iocntrl failed %d\n",OSAL_u32ErrorCode( ));
                        Errorcode += 8;
                    }
                    
                    if( OSAL_s32IOClose( hFd ) == OSAL_ERROR )
                     {
                        Errorcode += 4;
                     }
                }
            }
            else
            {
                Errorcode += 10;
            }
        }
        else
        {
            Errorcode += 12; 
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "got no event !!!\n");
        }
    }

}    

/*****************************************************************************
* FUNCTION:         u32CARD_RegisterforprmNotification()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:        TU_OEDT_CARD_CFS_091
* DESCRIPTION:      Register for PRM NOTIFICATION to get the UUID
* HISTORY:          Created by pmh5kor, August 5,2014
******************************************************************************/

tU32 u32CARD_RegisterforprmNotification(tVoid)
{
    OSAL_tIODescriptor hDevice;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    OSAL_trNotifyDataExt2 rNotifyData;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
    if( hDevice == OSAL_ERROR )
    {
        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "/dev/prm open failed\n");
        u32Ret = 1;
    }
    else
    {
        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "Please eject the connected CARD/USB if any !!!\n");
        OSAL_s32ThreadWait(15000);
        rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
        rNotifyData.ResourceName = "/dev/media";
        rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
        rNotifyData.pCallbackExt2 = vData_Media_HandlerCardState;
        if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
            (tS32)&rNotifyData) == OSAL_ERROR)
        {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "Registration for notification failed\n");
                u32Ret += 3;
        }
        else
        {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "Registration for notification successful\n");
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "Please insert the card now !!!\n");
            OSAL_s32ThreadWait(15000);

            if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                (tS32)&rNotifyData) == OSAL_ERROR)
            {
                OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                    "UnRegistration for notification failed\n");
                u32Ret += 5;
            }
        }
        
        if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            u32Ret += 7;
        }
    }
   
    u32Ret += Errorcode;
    if((Errorcode!=0)|| (u32Ret!=0 ))
    {
        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "OEDT FAILED WITH Errorcode=%d \n u32Ret=%d",Errorcode,u32Ret);
        Errorcode=0;
    }
 
    return u32Ret;
   
}

/************************************************************************
* FUNCTION     :  vHandler ()
* PARAMETER    :  Dummy Callback parameter for registration purpose.
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for PRM:termination of prm process registration 
* HISTORY      :  Created by dku6kor, Mar 31,2015
************************************************************************/

tVoid vHandler(const tU32* pu32Param)
{
   OEDT_HelperPrintf((tS32)TR_LEVEL_USER_1,"Process with ID %d terminated for value %d", getpid(),*pu32Param);
}

/*************************************************************************
* FUNCTION:         u32PRM_IOCTRL_REG_SYSTEM_INFO()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error       
* DESCRIPTION:      To get info about termination of prm process registration
* HISTORY:          Created by dku6kor, Mar 31,2015
*************************************************************************/

tU32 u32PRM_IOCTRL_REG_SYSTEM_INFO(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;
   OSAL_trNotifyDataSystem rNotifyData;
   tU32 pu32Param;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, enAccess);
   if(hDevice == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      rNotifyData.pCallback =(OSALCALLBACKFUNC)vHandler;
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.pu32Data = &pu32Param;
      if(OSAL_s32IOControl (hDevice,  OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_STAT_INFO,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      { 
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "PRM OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_STAT_INFO failed",u32ErrorCode);
         u32Ret += 3;
      }
      else
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "Registration for notification is successful\n");
         
         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_SYSTEM_STAT_INFO,
                              (tS32)&rNotifyData) == OSAL_ERROR)
         {
            u32ErrorCode = OSAL_u32ErrorCode();
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                              "UnRegistration for notification failed\n",u32ErrorCode);
            u32Ret += 5;
         }
      }
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                       "UnRegistration for notification successful\n");
      if(OSAL_s32IOClose (hDevice) == OSAL_ERROR)
      {  
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OSAL_s32IOClose FAILED",u32ErrorCode);
         u32Ret += 7;
      }
   }
   return u32Ret;
}

/*************************************************************************
* FUNCTION:         u32PRM_TEST_max_registry()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error       
* DESCRIPTION:      To check for the max entry for registrations possible
* HISTORY:          Created by dku6kor, Apr,20,2015
*************************************************************************/

tU32 u32PRM_TEST_max_registry(tVoid)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;
   OSAL_trNotifyDataSystem rNotifyData;
   tU32 pu32Param;
   tS32 i = 0;
   tS32 j=0;
   tS32 registration_count=0;
   
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM,enAccess);
   if(hDevice == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                        "/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      rNotifyData.pCallback =(OSALCALLBACKFUNC)vHandler;
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.pu32Data = &pu32Param;
      for(i=0;i<COUNT;i++)
      {
         if(OSAL_s32IOControl (hDevice,  OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_STAT_INFO,(tS32)&rNotifyData)== OSAL_ERROR)
         {
            u32ErrorCode = OSAL_u32ErrorCode();      /* Set error code */
            if(u32ErrorCode == OSAL_E_BUSY&&(i>=5))
            {
               OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                 "registration reached its maximum limit\n");
               break;
            }
            else
            {
               OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                                 "Registration failed due to error %d",u32ErrorCode);
               u32Ret += 3;
            } 
         }
         else
         {
            registration_count++;
         }
      }
         
      for(j=0;j<=registration_count-1;j++)
      {
         if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_SYSTEM_STAT_INFO,
                              (tS32)&rNotifyData) == OSAL_ERROR)
         {  
            u32ErrorCode = OSAL_u32ErrorCode();
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                              "UnRegistration for notification failed\n",u32ErrorCode);
            u32Ret += 5;
         }
      }
      if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OSAL_s32IOClose FAILED",u32ErrorCode);
         u32Ret += 7;
      }
   }
   return u32Ret;
}

/*************************************************************************
* FUNCTION:         u32PRM_IOCTRL_TRIG_ERRMEM_TTF()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error        
* DESCRIPTION:      Trace out error memory in TTfis
* HISTORY:          Created by dku6kor, Apr 10,2015
*************************************************************************/

tU32 u32PRM_IOCTRL_TRIG_ERRMEM_TTF(tVoid)
{  
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;

   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, enAccess);
   if(hDevice == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      tS32 s32Dummy;
      if (OSAL_ERROR==OSAL_s32IOControl(hDevice , OSAL_C_S32_IOCTRL_PRM_TRIGGER_ERRMEM_TTFIS, (tS32)&s32Dummy))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, "OSAL_s32IOControl FAILED",u32ErrorCode);
         u32Ret += 3;
      }
      if (OSAL_OK!=OSAL_s32IOClose(hDevice))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OSAL_s32IOClose FAILED",u32ErrorCode);
         u32Ret += 5;
      }
   }  
   return u32Ret;
}

/*************************************************************************
* FUNCTION:         u32PRM_IOCTRL_TRIG_DEL_ERRMEM()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error      
* DESCRIPTION:      delete the error memory traces
* HISTORY:          Created by dku6kor, Apr 6,2015
*************************************************************************/

tU32 u32PRM_IOCTRL_TRIG_DEL_ERRMEM (tVoid)
{  
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;
   
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, enAccess);
   if( hDevice == OSAL_ERROR )
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      tS32 s32Dummy;
      if (OSAL_ERROR==OSAL_s32IOControl(hDevice , OSAL_C_S32_IOCTRL_PRM_TRIGGER_DEL_ERRMEM,(tS32)&s32Dummy))
      { 
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, "OSAL_s32IOControl OSAL_C_S32_IOCTRL_PRM_TRIGGER_DEL_ERRMEM=0x%0x FAILED",u32ErrorCode);
         u32Ret += 3;
      }
      if (OSAL_OK!=OSAL_s32IOClose(hDevice))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OSAL_s32IOClose(0x%x) FAILED",u32ErrorCode);
         u32Ret += 5;
      }
   }  
   return u32Ret;
}

/************************************************************************
* FUNCTION     :  vMedia_Mount_SDCardState ()
* PARAMETER    :  notification for media change.
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for PRM:to get the new mount point state.
* HISTORY      :  Created by dku6kor, Apr 6,2015
************************************************************************/   

tVoid vMedia_Mount_SDCardState(tPU32 pu32MediaChangeInfo,tU8 au8String[])
{
   (void)pu32MediaChangeInfo; //lint fix
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                     "media available !!! Device is %s \n",au8String);
   strncpy(NavPathBuffer1,au8String,strlen(au8String));
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                     "Navpathbuffer Device is %s \n",NavPathBuffer1);   
}

/************************************************************************
* FUNCTION     :  Media_Createfile()
* PARAMETER    :  path for file and access mode
* RETURNVALUE  :  tU32
* DESCRIPTION  :  call back function for PRM:to check the access mode.
* HISTORY      :  Created by dku6kor, Apr 27,2015
************************************************************************/   

tU32 Media_Createfile(const char* au8String)
{  
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tIODescriptor hFile;
   tU32 u32Ret = 0;
   char FileBuffer[256]= {0};

   if ( au8String == OSAL_NULL )
   {
      u32Ret = 1;
      return u32Ret;
   }
   strncpy(&FileBuffer[0],au8String,strlen(au8String));
   strncat(&FileBuffer[0],"/txt1.txt",strlen("/txt1.txt"));
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                     "path for the file is %s \n",FileBuffer);
   hFile = OSAL_IOCreate ((tCString)FileBuffer, OSAL_EN_READWRITE);
   if (hFile == OSAL_ERROR)
   { 
      u32ErrorCode = OSAL_u32ErrorCode();
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, "file creation failed !!!\n",u32ErrorCode);
      u32Ret += 3;
   }
   else 
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, "file created successfully\n");
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      { 
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"file closed failed\n",u32ErrorCode);
         u32Ret += 5;
      }
      if ( OSAL_s32IORemove (( tCString )FileBuffer) == OSAL_ERROR )
      { 
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"file removal failed\n",u32ErrorCode);
         u32Ret += 7;
      }  
   }
   return u32Ret;
}

/*************************************************************************
* FUNCTION:         u32PRM_IOCTRL_REMOUNT()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error       
* DESCRIPTION:      To check the remount mode of Sd card.
* HISTORY:          Created by dku6kor, Apr 13,2015
*************************************************************************/

tU32 u32PRM_IOCTRL_REMOUNT(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;
   OSAL_trRemountData rRemountData;
   OSAL_trNotifyDataExt2 rNotifyData;
   char FileBuffer[256]= {0};
   tU32 file_check=0;
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
   if( hDevice == OSAL_ERROR )
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                        "/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData.ResourceName = "/dev/media";
      rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      rNotifyData.pCallbackExt2 = vMedia_Mount_SDCardState;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
                           (tS32)&rNotifyData) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "Registration for notification failed\n");
         u32Ret += 3;
      }
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                        "Registration for notification successful\n");
      strncpy(FileBuffer,"/dev/media/",strlen("/dev/media/"));
      strncat(FileBuffer,NavPathBuffer1,strlen(NavPathBuffer1));
      rRemountData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;            // ID of the application               
      rRemountData.szPath   = FileBuffer;
      rRemountData.szOption = "rw";
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REMOUNT,
                           (tS32)&rRemountData) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "PRM OSAL_C_S32_IOCTRL_PRM_REMOUNT failed",u32ErrorCode);
         u32Ret += 5;
      }
      file_check=Media_Createfile(rRemountData.szPath);
      if(file_check!=0)
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "PRM OSAL_C_S32_IOCTRL_PRM_REMOUNT failed\n");
      u32Ret += 7;
      }
      rRemountData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;                           
      rRemountData.szPath   = FileBuffer;
      rRemountData.szOption = "ro";
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REMOUNT,
                           (tS32)&rRemountData) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "PRM OSAL_C_S32_IOCTRL_PRM_REMOUNT failed",u32ErrorCode);
         u32Ret += 9;
      }
      file_check=Media_Createfile(rRemountData.szPath);
      if(file_check!=0)
      {
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                        "file cant be created in read only mode !!!");
         if(file_check!=3)
         {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "PRM OSAL_C_S32_IOCTRL_PRM_REMOUNT failed !!!\n");
            u32Ret += 11;
         }
      }
      if (OSAL_OK!=OSAL_s32IOClose(hDevice))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                        "OSAL_s32IOClose(0x%x) FAILED",u32ErrorCode);
         u32Ret += 13;
      }
   }   
   return u32Ret;
}

/************************************************************************
* FUNCTION     :  vHandler1()
* PARAMETER    :  Dummy Callback parameter for registration purpose.
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for PRM:termination of prm process registration 
* HISTORY      :  Created by dku6kor, Apr 27,2015
************************************************************************/

tVoid vHandler1(const tU32* pu32MediaChangeInfo)
{
   (void)pu32MediaChangeInfo;
   callback_execution_count++;
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                     "the value for symbol in vHandler1 is %d",callback_execution_count);
}

/************************************************************************
* FUNCTION     :  vHandler2()
* PARAMETER    :  Dummy Callback parameter for registration purpose.
* RETURNVALUE  :  "void"
* DESCRIPTION  :  call back function for PRM:check for the callback function trigger purpose
* HISTORY      :  Created by dku6kor, Apr 27,2015
************************************************************************/

tVoid vHandler2(const tU32* pu32Param)
{   
   (void)pu32Param;
   callback_execution_count++;
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1, 
                    "the value for symbol in vHandler2 is %d ",callback_execution_count);
}

/*************************************************************************
* FUNCTION:         u32PRM_IOCTRL_ACTIVATE_SIGNAL()
* PARAMETER:        none
* RETURNVALUE:      tU32, "0" on success  or "non-zero" value in case of error        
* DESCRIPTION:      To activate the signal for other devices after cryptnav registration. 
* HISTORY:          Created by dku6kor, Apr 13,2015
*************************************************************************/

tU32 u32PRM_IOCTRL_ACTIVATE_SIGNAL(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hDevice;
   OSAL_trNotifyData rNotifyData_crypt;
   OSAL_trNotifyData rNotifyData_card;
   tU32 u32Ret = 0;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM,enAccess);
   if(hDevice == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"/dev/prm open failed\n");
      u32Ret = 1;
   }
   else
   {
      tS32 s32Dummy;
      rNotifyData_crypt.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
      rNotifyData_crypt.ResourceName = OSAL_C_STRING_DEVICE_CRYPTCARD;
      rNotifyData_crypt.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      rNotifyData_crypt.pCallback = (OSALCALLBACKFUNC)vHandler1;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData_crypt) == OSAL_ERROR)
      {
         u32Ret += 3;
      }
      rNotifyData_card.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
      rNotifyData_card.ResourceName = OSAL_C_STRING_DEVICE_CRYPTCARD;
      rNotifyData_card.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
      rNotifyData_card.pCallback = (OSALCALLBACKFUNC)vHandler2;
      if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
                           (tS32)&rNotifyData_card) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "OSAL_s32IOControl OSAL_C_S32_IOCTRL_REG_NOTIFICATION  =0x%0x FAILED",u32ErrorCode);
         u32Ret += 5;
      }
      OSAL_s32ThreadWait(5000);
      if (OSAL_ERROR==OSAL_s32IOControl(hDevice , OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL ,(tS32)&s32Dummy))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "OSAL_s32IOControl OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL =0x%0x FAILED",u32ErrorCode);
         u32Ret += 7;
      }
      OSAL_s32ThreadWait(5000);
      if(callback_execution_count!=4)
      {  
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "OSAL_s32IOControl OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL =0x%0x FAILED",u32ErrorCode);
         u32Ret += 9;  
      }
      if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                           (tS32)&rNotifyData_crypt) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "OSAL_s32IOControl OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION  =0x%0x FAILED",u32ErrorCode);
         u32Ret += 11;
      }
      if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION,
                           (tS32)&rNotifyData_card) == OSAL_ERROR)
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                           "OSAL_s32IOControl OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION  =0x%0x FAILED",u32ErrorCode);
         u32Ret += 13;
      }
      if (OSAL_OK!=OSAL_s32IOClose(hDevice))
      {
         u32ErrorCode = OSAL_u32ErrorCode();
         OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"OSAL_s32IOClose(0x%x) FAILED", u32ErrorCode);
         u32Ret += 15;
      }
   }  
   OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,"the final value after calling callback fun is %d",callback_execution_count);
   callback_execution_count=0;
   return u32Ret;
}




/*****************************************************************************
* FUNCTION:          u32SDCARD_RegisterforprmNotification()
* PARAMETER:         none
* RETURNVALUE:       tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:         
* DESCRIPTION:       Register for PRM NOTIFICATION to get the UUID
* HISTORY:           Created by Ahm5kor, April 03,2015
******************************************************************************/

tU32 u32SDCARD_RegisterforprmNotification(tVoid)
{
    OSAL_tIODescriptor hDevice;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    OSAL_trNotifyDataExt2 rNotifyData;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, enAccess );
    if( hDevice == OSAL_ERROR )
    {
        OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "/dev/prm open failed\n");
        u32Ret = 1;
    }
    else
    {
        rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
        rNotifyData.ResourceName = "/dev/media";
        rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE);
        rNotifyData.pCallbackExt2 = vData_Media_HandlerCardState;
        if(OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2,
            (tS32)&rNotifyData) == OSAL_ERROR)
        {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "Registration for notification failed\n");
                u32Ret += 3;
        }
        else
        {
            OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                "Registration for notification successful\n");
            if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2,
                (tS32)&rNotifyData) == OSAL_ERROR)
            {
                OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
                    "UnRegistration for notification failed\n");
                u32Ret += 5;
            }
        }
        if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            u32Ret += 7;
        }
    }
   /* if (bISSdcard_inserted == FALSE)
    {
       u32Ret = 10;
       u32Ret += Errorcode;
       OEDT_HelperPrintf((tU8)OSAL_C_TRACELEVEL1,
            "OEDT FAILED WITH u32Ret=%d",u32Ret);
       bISSdcard_inserted = FALSE;
    }*/
  
    Errorcode=0;
    bISSdcard_inserted = FALSE;
    return u32Ret;
}
/* EOF */
