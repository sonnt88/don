/******************************************************************************
 *FILE         : oedt_Cdrom_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for the CDROM 
 *               device	for the low variant hardware of GM-GE.
 *              
 *AUTHOR(s)    :  Rakesh Dhanya (RBIN/EDI3)
 *HISTORY      :  28-may-2007 Initial  Version
 *                31-may-2007 ver 1.0 - review comments incorporated
 *				  						Rakesh Dhanya (RBIN/EDI3)
 *	          08 June-2007 ver 1.1 - updates made based on review
										 comments given by Mr Resch Carsten.
										 Rakesh Dhanya (RBIN/EDI3)
 *		  02 July-2007 ver 1.2 - updates made based on review 
 *                                       comments given by PQC.
 *		  06.03.08 ver 1.3  CF32- Lars Tracht    renamed to oedt_Cdrom_TestFuncs.c							 
 *        30.07.2008 ver 1.4 - Shilpa Bhat (BRBEI/ECM1)
 *        Removed cases u32CDROMClosedevInvalParm, u32CDROMCloseDirInvalParm,
 *        u32CDROMGetInfoInvalParm, u32CDROMFileCloseInvParm , 
 *		  u32CDROMGetVersionNumberAfterDevClose, u32CDROMGetVolumeLabelAfterDevClose
 *        u32CDROMOpendevNullParm, u32CDROMOpenDirWithNULL, u32CDROMFileOpenWithNULLParam,
 *        u32CDROMDirReClose, u32CDROMSetReadErrorStatergyNormal,
 *        u32CDROMSetReadErrorStatergyStreaming
 *        Added cases u32CDROMReadAsync, u32CDROMFileReadInvaildCount
 *        29.09.2008 ver 1.5 - Shilpa Bhat (BRBEI/ECM1)
 *        Removed testcases u32CDROMGetDirInfo, u32CDROMFirstFileRead, u32CDROMLastFileRead,
 *        u32CDROMFirstMP3Read, u32CDROMGetVolumeLabel, u32CDROMGetVolumeLabelInvalParm , 
 *        u32CDROMGetMediaInfo, u32CDROMGetMediaInfoInvalParam
 *        
 *        24.03.2009 ver 1.6 - Shilpa Bhat (RBEI/ECF1)
 *        Lint Removal
 *
 *
 ----------------------------------------------------------------------------- */


#define vTtfisTrace(...)
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_Cdrom_TestFuncs.h"
#include "../oedt_helper_funcs.h"

/* These macros define invalid parameters to be used in the test code */
#define OSAL_C_STRING_DEVICE_INVAL  	"/dev/Invalid"
#define OSAL_C_S32_IOCTRL_INVAL_FUNC_CDROM	-1000

/* The following macros are used for read operations in various test cases */
#define BYTES_TO_READ 20
#define MAX_LASER_JUMPS 100
#define BYTES_TO_READ_N 5
#define SKIP_COUNT 20
#define BYTES_TO_READ_TPUT 1048576
#define ZERO_PARAMETER_CDROM 0
#define INVALID_PARAMETER_CDROM -1
#define SIZE_OF_DATA_FILE 5242880
#define SIZE_OF_MP3_FILE 4585140
#define BYTES_TO_READ_37 12
#define FIRST_POSITION_TO_SET 2531400
#define SECOND_POSITION_TO_SET 5241800
#define NEGATIVE_POS_TO_SET -75
#define INVAL_BYTES_TO_READ -100
#define ONE_MB 1048575
#define BYTES_TO_READ_47 12
#define OFFSET_FROM_END 1080
#define OFFSET_BEYOND_END 559

/* Defines the invalid access mode that is to be used in test cases */
#define OSAL_INVALID_ACCESS_MODE -100
/* Directory name of a valid directory within the CDROM */
#define OSAL_C_STRING_CD_DIR1 "/dev/cdrom/TestCD/DreamTheatre"

/* These are paths of various files and directories within the CDROM */
#define OSAL_C_STRING_FILE_INVPATH "/dev/cdrom/TestCD/Creed/Weathered/Weathered/ 04-Creed-signs.mp3"
#define FILE_TO_REMOVE "/dev/cdrom/TestCD/DreamTheatre/Dream Theater - Awake -03- Innocence faded.MP3"										
#define OSAL_TEXT_FILE_FIRST "/dev/cdrom/TestCD/00_first_folder/00_cd_test_firstfile.dat"
#define OSAL_PK_FILE "/dev/cdrom/TestCD/Buena Vista Social C/Buena Vista Social Club/01_Buena Vista Social Club_Chan Chan.pk"
#define OSAL_MP3_FILE "/dev/cdrom/TestCD/Creed/Weathered/01-Creed-bullets.mp3"
#define OSAL_JPG_FILE "/dev/cdrom/TestCD/Creed/My own prison/Creed - My Own_Prison - back.jpg"
#define OSAL_M3U_FILE "/dev/cdrom/TestCD/Creed/Weathered/all.m3u"
#define SUBDIR_CREAT_PATH "/dev/cdrom/TestCD/Creed/Weathered"
#define CREAT_FILE_PATH  "/dev/cdrom/TestCD/Creed/Weathered/NewFile"
#define CREAT_DIR_PATH "/dev/cdrom/TestCD/Creed/Weathered/NewDir"
#define SUBDIR_DEL_PATH "/dev/cdrom/TestCD/Buena Vista Social C"
#define DEL_DIR_PATH "/dev/cdrom/TestCD/Buena Vista Social C/Buena Vista Social Club"
#define OSAL_FILE_LAST "/dev/cdrom/TestCD/ZZ_last/last_00/last_01/last_02/last_03/last_04/last_06/99_cd_test_lastfile.dat"
#define OSAL_LONG_FILE_NAME "/dev/cdrom/TestCD/long_name_test_long_name_test_long_name_test_long_name_test.txt"
#define OSAL_C_STRING_CD_DIR_CRE_PAR "/dev/cdrom/TestCD/Creed/Weathered"

/* These are the global parameters that will be used by the read dir function */
tU32 GlobDirCount = 0;
tU32 GlobFileCount = 0;
tU32 GlobDirRet = 0;
tU32 CountVal = 0;
tPS8 ReadBuffer = NULL;
OSAL_tSemHandle hSemAsyncCDROM;

/************************************************************************
*FUNCTION:      vOEDTCDROMAsyncCallback 
* PARAMETER:    none
* RETURNVALUE:  "void"
* DESCRIPTION:  Call back function for Async read operation
* HISTORY:		Created Shilpa Bhat(RBIN/ECM1)on July 29, 2008
 ************************************************************************/
tVoid vOEDTCDROMAsyncCallback (OSAL_trAsyncControl *prAsyncCtrl)
{
	tU32 u32RetVal = 0;
	u32RetVal = OSAL_u32IOErrorAsync(prAsyncCtrl);
	if ( u32RetVal == OSAL_E_NOERROR )
	{
	    OSAL_s32SemaphorePost ( hSemAsyncCDROM );   
	}
} 

		
/*****************************************************************************
* FUNCTION:		u32CDROMOpenClosedevice( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_001
* DESCRIPTION:  Opens the CDROM device
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*				Function name changed as closedevice close has been 
				removed. Both features are being checked for in thos case.
				Updated by Rakesh Dhanya(RBIN/EDI3) on June 5, 2007.
******************************************************************************/
 tU32 u32CDROMOpenClosedevice(void)
 {
 
 	OSAL_tIODescriptor hDevice = 0;
 	tU32 u32Ret = 0;

 	/* Open the device */
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM , OSAL_EN_READONLY );
    	if ( OSAL_ERROR == hDevice )
    	{
    		u32Ret = 1;
    	}
    	else
		{
			/* Close the device */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
			{
				u32Ret = 2;
			}
		}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpendevInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_002
* DESCRIPTION:  Try to Open the CDROM device with invalid parameters
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32 u32CDROMOpendevInvalParm(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
    
    /* Try to open the device with an invalid device name*/
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_INVAL, OSAL_EN_READONLY );
    if ( OSAL_ERROR != hDevice  )
    {
    	u32Ret = 1; 
    	/* If successfully opened, indicate an error and close the device */  
		if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 2;
		}
	}

	/* Try to open the device with an invalid access mode */
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM , 
									(OSAL_tenAccess)OSAL_INVALID_ACCESS_MODE );
	if (  OSAL_ERROR != hDevice )
	{
		u32Ret += 4;
		/* If successfully opened, indicate an error and close the device */
		if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpendevDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_003
* DESCRIPTION:  Try to Open the CDROM device in different access modes
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32 u32CDROMOpendevDiffModes(void)
{
  	OSAL_tIODescriptor hDevice = 0;
  	tU32 u32Ret = 0;

	/* Open the file in READONLY mode */
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if(OSAL_ERROR == hDevice  )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice )  )
		{
			u32Ret += 2;
		}
	}

	/* Open the file in READWRITE mode */
	/* Should not be able to open the device in this mode */
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READWRITE );
	if(OSAL_ERROR != hDevice  )
	{
		u32Ret += 4;
		/* If open with this mode is successfull, indicate error and close 
			the device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice )  )
		{
			u32Ret += 8;
		}
	}

	/* Open the file in WRITEONLY mode */
	/* Should not be able to open the device in this mode */
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_WRITEONLY );
	if(OSAL_ERROR != hDevice  )
	{
		u32Ret += 16;
		/* If open with this mode is successfull, indicate error and close 
			the device */
		if( OSAL_ERROR  ==  OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret += 32;
		}
	}
return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMReOpendev( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_004
* DESCRIPTION:  Try to Re-Open the CDROM device 
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
 tU32 u32CDROMReOpendev(void)
 {
 	OSAL_tIODescriptor hDevice1 = 0;
 	OSAL_tIODescriptor hDevice2 = 0;
 	tU32 u32Ret = 0;

	/* Open the device */
	hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
    if (OSAL_ERROR == hDevice1  )
    {
    	u32Ret = 1;
    }
	else
	{
		/* Re-open the device, should be successfull */
		hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
		if (OSAL_ERROR == hDevice2  )
		{
			u32Ret += 2;
		}
		else
		{	/* If open for the second time is successfull, close the second 
				device handle */
			if( OSAL_ERROR == OSAL_s32IOClose ( hDevice2 ) )
			{
				u32Ret += 4;
			}
		}
		/* Now close the first device handle */
		if( OSAL_ERROR ==  OSAL_s32IOClose ( hDevice1 ) )
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMClosedevAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_005
* DESCRIPTION:  Close the CDROM device which is already closed
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
 tU32  u32CDROMClosedevAlreadyClosed(void)
 {
 	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
    
	/* Open the device */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
    if (OSAL_ERROR ==  hDevice )
    {
    	u32Ret = 1;
    }
    else
	{
		/* Close the device with the device handle obtained in open */
		if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
		{
			u32Ret = 2;
		}
		else
		{
			/* Re Closing , Try to close with the same handle.
			Must not be possible */
			if( OSAL_ERROR != OSAL_s32IOClose ( hDevice )  )
			{
				u32Ret = 20;
			}
		}
			
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpenClosedir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_006
* DESCRIPTION:  Open a directory in the CDROM device
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*				Function name changed as closedir close has been 
				removed. Both features are being checked for in thos case.
				Updated by Rakesh Dhanya(RBIN/EDI3) on June 5, 2007.
******************************************************************************/
tU32  u32CDROMOpenClosedir(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
    tU32 u32Ret = 0;
    
	/* Open the directory */
    hDir = OSALUTIL_prOpenDir ( OSAL_C_STRING_CD_DIR1);
	if( OSAL_NULL  == hDir) 
	{
		u32Ret = 1;
	}
	else 
	{
		/* Close the directory */
		if( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir ) )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpendirInvalid( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_007
* DESCRIPTION:  Open a directory in the CDROM device with invalid parameters
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*				Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008
******************************************************************************/
tU32  u32CDROMOpendirInvalid(void)
{

	OSAL_tIODescriptor hDir1 = 0;
   	tU32 u32Ret = 0;
   
	/* Try to open a valid directory with an invalid access mode */
	hDir1 = OSAL_IOOpen ( OSAL_C_STRING_CD_DIR1,(OSAL_tenAccess)OSAL_INVALID_ACCESS_MODE );
	{
		if(OSAL_ERROR != hDir1)
		{
			u32Ret += 4;
		
			/* If open is successful, indicate an error and close the directory */
			if(OSAL_ERROR  == OSAL_s32IOClose( hDir1 ) )
			{
				u32Ret += 8;
			}
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpendirDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_008
* DESCRIPTION:  Open a directory in the CDROM device in different modes
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32 u32CDROMOpendirDiffModes(void)
{
   	OSAL_tIODescriptor hDir = 0;
	tU32 u32Ret = 0;
	
	/* Open the device in READONLY mode */	
	hDir = OSAL_IOOpen ( OSAL_C_STRING_CD_DIR1,OSAL_EN_READONLY );
	if(OSAL_ERROR  == hDir )
	{
		u32Ret += 1;
	}
	else
	{
		/* Close the device */
		if(OSAL_ERROR  ==  OSAL_s32IOClose(hDir))
		{
			u32Ret += 2;
		}
	}

	/* Open the device in READWRITE Mode */
	hDir = OSAL_IOOpen ( OSAL_C_STRING_CD_DIR1,OSAL_EN_READWRITE );
	if(OSAL_ERROR != hDir  )
	{
		u32Ret += 4;

		/* The directory should not be opened in the READWRITE Mode.
		   If it does get opened, indicate an error and close it */
		if(OSAL_ERROR == OSAL_s32IOClose(hDir) )
		{
			u32Ret += 8;
		}
	}

	/* Open the device in WRITEONLY Mode */
	hDir = OSAL_IOOpen ( OSAL_C_STRING_CD_DIR1,OSAL_EN_WRITEONLY );
	if(OSAL_ERROR != hDir  )
	{
		u32Ret += 16;

		/* The directory should not be opened in the WRITEONLY Mode.
		   If it does get opened, indicate an error and close it */
		if(OSAL_ERROR == OSAL_s32IOClose(hDir)  )
		{
			u32Ret += 32;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMOpendirwithFile( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_009
* DESCRIPTION:  Open a directory with a file name parameter
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32 u32CDROMOpendirwithFile(void)
{
	OSAL_trIOCtrlDir* hDir = 0;
	tU32 u32Ret = 0;

	/* Open the directory with a file name parameter, should fail */
	hDir = OSALUTIL_prOpenDir ( OSAL_TEXT_FILE_FIRST);
	if(OSAL_NULL != hDir) 
	{
		u32Ret += 1;
		/* If the directory is successfully opened, indicate error and close
		the directory */
		if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir ) )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMReOpenDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_010
* DESCRIPTION:  Open a directory in the CDROM device that is already opened
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32 u32CDROMReOpenDir(void)
{
	OSAL_trIOCtrlDir* hDir1 = NULL,*hDir2=NULL;
    tU32 u32Ret = 0;

	/* Open the directory */
  	hDir1 = OSALUTIL_prOpenDir ( OSAL_C_STRING_CD_DIR1);
	if( OSAL_NULL  == hDir1) 
	{
		u32Ret += 1;
	}
	else
	{
		/* Open the same directory again */ 
		hDir2 = OSALUTIL_prOpenDir ( OSAL_C_STRING_CD_DIR1);
		if( OSAL_NULL  == hDir2) 
		{
			u32Ret += 2;
		}
		else
		{
			/* After opening twice, close the first handle */
			if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir2 ) )
			{
				u32Ret += 4;
			}
		}
		/* Close the first handle */
		if(OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir1 ) )
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMCreateDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_011
* DESCRIPTION:  Try to Create a new directory
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 25,2007 
*
******************************************************************************/
tU32  u32CDROMCreateDir(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	tU32 u32Ret = 0;
	tS32 DirRet = 0;
	
	/* Open the parent directory */
	hDir = OSALUTIL_prOpenDir(SUBDIR_CREAT_PATH);
	if (OSAL_NULL == hDir)
	{
	   u32Ret += 1;	
	}
	else
	{
		/* Attempt to Create a Directory, though the parent directory is opened 
		in read only mode */
		DirRet = OSALUTIL_s32CreateDir(hDir->fd,
										CREAT_DIR_PATH);
		if(OSAL_ERROR != DirRet)
		{
			u32Ret += 2;
			/* If the directory is created, indicate an error and 
			remove the directory */
			if(OSAL_ERROR == OSALUTIL_s32RemoveDir(hDir->fd
													,CREAT_DIR_PATH))
			{
				u32Ret += 4;
			}
		}
		/* Close the parent directory */
		if( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir ) )
			{
				u32Ret += 8;
			}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMDelDir( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_012
* DESCRIPTION:  Try to Delete an existing directory
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
*
******************************************************************************/
tU32 u32CDROMDelDir(void)
{ 	
 	OSAL_trIOCtrlDir* hDir = OSAL_NULL;
	tU32 u32Ret = 0;
		/* Open the parent directory */
	hDir = OSALUTIL_prOpenDir(SUBDIR_DEL_PATH);
	if (OSAL_NULL == hDir)
	{
	   u32Ret += 1;	
	}
	else
	{
		/* Attempt to remove an existing sub directory */
		if(OSAL_ERROR != OSALUTIL_s32RemoveDir(hDir->fd,
												DEL_DIR_PATH))
		{
			u32Ret += 2;
		}
		/* Close the parent directory */
		if( OSAL_ERROR  == OSALUTIL_s32CloseDir ( hDir ) )
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileOpenClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_013
* DESCRIPTION:  Try to Open a file
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007
*				Function name changed as closefile close has been 
				removed. Both features are being checked for in thos case.
				Updated by Rakesh Dhanya(RBIN/EDI3) on June 5, 2007.
 ******************************************************************************/
tU32 u32CDROMFileOpenClose(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	/* Open the file */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	
	if(OSAL_ERROR == hFile)
	{
		u32Ret += 1;
	}
	else
	{
		/* Close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile) )
		{
			u32Ret += 2;
		}
	}
	
	return u32Ret;
}	
/*****************************************************************************
* FUNCTION:	    u32CDROMFileOpenInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_014
* DESCRIPTION:  Try to Open a file with invalid parameters
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008
******************************************************************************/
tU32 u32CDROMFileOpenInvalParam(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	/* Open with invalid access mode */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,(OSAL_tenAccess)OSAL_INVALID_ACCESS_MODE);
	if(OSAL_ERROR != hFile)
	{
		u32Ret += 4;
		/* If open is successfull, indicate an error and close the file */
		if(OSAL_ERROR == OSAL_s32IOClose (hFile) )
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileOpenInvalPath( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_015
* DESCRIPTION:  Try to Open a file with invalid path
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileOpenInvalPath(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	/* Open the file with invalid path */
	hFile = OSAL_IOOpen ( OSAL_C_STRING_FILE_INVPATH,OSAL_EN_READONLY );
	if(  OSAL_ERROR != hFile  )
	{
		u32Ret += 1;
		/* If Open is successfull, indicate an error and close the file */
		if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileOpenDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_016
* DESCRIPTION:  Try to Open a file with in different modes
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32  u32CDROMFileOpenDiffModes(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	/* Opening in READ ONLY Mode */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 1;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 2;
		}
	}

	/* Opening in WRITE only mode (should fail)*/
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_WRITEONLY );
	if(  OSAL_ERROR != hFile )
	{
		u32Ret += 4;
		/* If open is successfull, indicate error and close the file */
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 8;
		}
	}

	/* Opening in READ-WRITE  mode (should fail) */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READWRITE );
	if(OSAL_ERROR != hFile )
	{
		u32Ret += 16;

		/* If open is successfull, indicate error and close the file */
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 32;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileOpenCloseDiffExtns( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_017
* DESCRIPTION:  Try to Open a file with  different extensions (.dat,.mp3,.jpg,
				.m3u,.pk)
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
*				Function name changed as closefile close has been 
				removed. Both features are being checked for in thos case.
				Updated by Rakesh Dhanya(RBIN/EDI3) on June 5, 2007.
******************************************************************************/
tU32 u32CDROMFileOpenCloseDiffExtns(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	/* Open a .dat file */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 1;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 2;
		}
	}

	/* open a .pk file */

	hFile = OSAL_IOOpen(OSAL_PK_FILE,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 4;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 8;
		}
	}

	/* Open  a .mp3 file */

	hFile = OSAL_IOOpen(OSAL_MP3_FILE,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 16;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 32;
		}
	}

	/* Open a .jpg file */

	hFile = OSAL_IOOpen(OSAL_JPG_FILE,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 64;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 128;
		}
	}

	/* Open a .m3u file */
	hFile = OSAL_IOOpen(OSAL_M3U_FILE,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 256;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 512;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_018
* DESCRIPTION:  Try to Open a file which is already opened  
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32  u32CDROMFileReOpen(void)
{
	OSAL_tIODescriptor hFile1 = 0,hFile2 = 0;
	tU32 u32Ret = 0;
	
	/* Open the file for the first time */
	hFile1 = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile1 )
	{
		u32Ret = 1;
	}
	else
	{
		/* Re Opening The File, should be possible */
		hFile2 = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
		if(OSAL_ERROR == hFile2 )
		{
			u32Ret += 2;
		}
		else
		{
			/* close the second handle of the file */
			if( OSAL_ERROR == OSAL_s32IOClose ( hFile2 ) )
			{
				u32Ret += 4;
			}
		}
		/* Close the first handle of the file */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hFile1 ) )
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileReClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_019
* DESCRIPTION:  Try to Close a file that is already closed
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileReClose()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 1;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 2;
		}
		else
		{
			if(OSAL_ERROR != OSAL_s32IOClose ( hFile )  )
			{
				u32Ret += 4;
			}
		}
 	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileCreate( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_020
* DESCRIPTION:  Try to Create a file on the CDROM device
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileCreate(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0;

	/* Open a directory first and then attempt to create a file within it */
	hDir = OSALUTIL_prOpenDir(OSAL_C_STRING_CD_DIR_CRE_PAR);
	if(OSAL_NULL == hDir)
	{
		u32Ret += 1;
	}
	else
	{
		/* Attempt to create a file within this directory
		   Should fail	 */
		hFile = OSAL_IOCreate (CREAT_FILE_PATH, enAccess);

		if (OSAL_ERROR != hFile )
		{
			u32Ret += 2;
			/* If the file is successfully created	indicate an error 
				and close the file before removing it */
			if(OSAL_ERROR == OSAL_s32IOClose ( hFile ))
			{
				u32Ret += 4;
			}
			/* Now remove the file */
			else if( OSAL_ERROR == OSAL_s32IORemove ( CREAT_FILE_PATH ))
			{
				u32Ret += 8;
			}
		}
		/* Close the directory */
		if (OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileRemove( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_021
* DESCRIPTION:  Try to remove a file on the CDROM device
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileRemove(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
   
	tU32 u32Ret = 0;



   /* Open a parent directory first  */

	hDir = OSALUTIL_prOpenDir(OSAL_C_STRING_CD_DIR1);
	if(OSAL_NULL == hDir)
	{
		u32Ret += 1;
	}
	else
	{
		/* Try to remove an existing file on the CDROM. it should not be
		posible to remove, as permissions will be read only*/
		if(OSAL_ERROR != OSAL_s32IORemove (FILE_TO_REMOVE))
		{
			u32Ret += 2;
		}

		/* Close the directory */
		if (OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_022
* DESCRIPTION:  Retreives the number of bytes between current position and BOF.
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya on June 05, 2007
******************************************************************************/
tU32 u32CDROMFileGetPosFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32FilePos = 0;
	
	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/*	OSAL_C_S32_IOCTRL_FIOWHERE -- Gives the file pos 
			from beginning of the file*/
		if( OSAL_ERROR == OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
								(tS32)&s32FilePos)  )
		{
			u32Ret += 2;
		}
		else
		{
			/* Check if a non negative value has been returned */
			if(s32FilePos < 0)
			{
				u32Ret += 4;
			}
		}
		/* Close the file */
	  	if( OSAL_ERROR ==  OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 8;
		}
	}

	/* Check for MP3 file */

	/* Open the file of .mp3 format */
	hFile = OSAL_IOOpen ( OSAL_MP3_FILE,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 16;
	}
	else
	{
		/*	OSAL_C_S32_IOCTRL_FIOWHERE -- Gives the file pos 
			from beginning of the file*/
		if(OSAL_ERROR == OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
								(tS32)&s32FilePos) )
		{
			u32Ret += 32;
		}
		else
		{
			/* Check if a non negative value has been returned */

			if(s32FilePos < 0)
			{
				u32Ret += 64;
			}
		}

		/* Close the file */
	  	if( OSAL_ERROR == OSAL_s32IOClose ( hFile ) )
		{
			u32Ret += 128;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmBOFInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_023
* DESCRIPTION:  Retreives the number of bytes between current position and BOF.
				with the use of invalid parameters.
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008
******************************************************************************/
tU32 u32CDROMFileGetPosFrmBOFInvalParm()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;

	/* Open the file, this valid handle can be used */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	
	if(OSAL_ERROR == hFile)
	{
		u32Ret += 2;
	}
	else
	{
		/* Attempt to get file position with invalid macro */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_NULL,
							(tS32)&u32FilePos)  )
		{
			u32Ret += 4;
		}

		/* Attempt to get file position by passing invalid parameter 
		to the control macro */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
								OSAL_NULL  ) )
		{
			u32Ret += 8;
		}
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}	
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmBOFAfterClosing( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_024
* DESCRIPTION:  Retreives the number of bytes between current position and BOF.
				after closing the file
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileGetPosFrmBOFAfterClosing(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 2;
		}
		else
		{
			/* Now attempt to get file position. Should fail */
			if( OSAL_ERROR != OSAL_s32IOControl(hFile,
				OSAL_C_S32_IOCTRL_FIOWHERE, (tS32)&u32FilePos)  )
			{
				u32Ret += 4;
			}
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_025
* DESCRIPTION:  Retreives the number of bytes between current position and EOF.
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya on June 05, 2007
******************************************************************************/
tU32 u32CDROMFileGetPosFrmEOF(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32FilePos = 0;
	
	/* open a file (.dat) on the CDROM device */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if(OSAL_ERROR == hFile )
	{
		u32Ret = 1;
	}
	else
	{
		/* OSAL_C_S32_IOCTRL_FIONREAD -- Gives the file pos 
		from the end of file */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
								(tS32)&s32FilePos)  )
		{
			u32Ret = 2;
		}
		/* Check if the returned value is within the bounds of the file size */
		if((s32FilePos <ZERO_PARAMETER_CDROM)||(s32FilePos > SIZE_OF_DATA_FILE ))
		{
			u32Ret += 4;
		}
		
		if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 8;
		}
	}

	/* Try the same with a .MP3 file */

	/* open a file (.mp3) on the CDROM device */
	hFile = OSAL_IOOpen ( OSAL_MP3_FILE,OSAL_EN_READONLY );
	if(OSAL_ERROR == hFile )
	{
		u32Ret += 20;
	}
	else
	{
		/* OSAL_C_S32_IOCTRL_FIONREAD -- Gives the file pos 
		from the end of file */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
								(tS32)&s32FilePos)  )
		{
			u32Ret += 50;
		}
		/* Check if the returned value is within the bounds of the file size */

		if((s32FilePos <ZERO_PARAMETER_CDROM)||(s32FilePos > SIZE_OF_MP3_FILE ))
		{
			u32Ret += 100;
		}

		if(OSAL_ERROR == OSAL_s32IOClose ( hFile )  )
		{
			u32Ret += 200;
		}
	}

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmEOFInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_026
* DESCRIPTION:  Retreives the number of bytes between current position and EOF.
				with the use of invalid parameters.
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007
*               Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008 
******************************************************************************/
tU32 u32CDROMFileGetPosFrmEOFInvalParm()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;

	/* Open the file, this valid handle can be used */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if(OSAL_ERROR == hFile)
	{
		u32Ret += 2;
	}
	else
	{
		/* Attempt to get file position with invalid macro */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_NULL,
							(tS32)&u32FilePos)  )
		{
			u32Ret += 4;
		}

		/* Attempt to get file position by passing invalid parameter 
		to the control macro */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
								OSAL_NULL  ) )
		{
			u32Ret += 8;
		}
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}	
/*****************************************************************************
* FUNCTION:	    u32CDROMFileGetPosFrmEOFAfterClosing( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_027
* DESCRIPTION:  Retreives the number of bytes between current position and EOF
				after closing the file
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileGetPosFrmEOFAfterClosing(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 2;
		}
		else
		{
			/* Now attempt to get file position. Should fail */
			if( OSAL_ERROR != OSAL_s32IOControl(hFile,
					OSAL_C_S32_IOCTRL_FIONREAD,(tS32)&u32FilePos)  )
			{
				u32Ret += 4;
			}
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileSetPosFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_028
* DESCRIPTION:  Sets position for different offsets from different positions.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007
******************************************************************************/
tU32 u32CDROMFileSetPosFrmBOF()
{
   
	
	static char DataArr[BYTES_TO_READ_37] =   {0x5F,0x03,0x00,0x00,
									   0x60,0x03,0x00,0x80,
									   0x61,0x03,0x00,0x00};

	static char DataArr2[BYTES_TO_READ_37] =  {0x12,0xA8,0x09,0x80,
									   0x13,0xA8,0x09,0x00,
									   0x14,0xA8,0x09,0x80};

	static char DataArr3[BYTES_TO_READ_37] =  {0xF2,0xFE,0x13,0x80,
									   0xF3,0xFE,0x13,0x00,
									   0xF4,0xFE,0x13,0x80};		
									   	
   	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0; 
	tS32 ReadRet = 0;
	tU32 s32PosToSet=0;
	tS8 ReadData[BYTES_TO_READ_37];
	

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 3452;
		/* Seek to the byte position specified */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 2;
		}		
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,BYTES_TO_READ_37);
		/* The read number is important. Compare if expected number 
		is read */
		if( (OSAL_ERROR == ReadRet) || ( BYTES_TO_READ_37 != ReadRet) )
		{
			u32Ret += 4; 
		}
		else
		{
		 /* Compare the read bytes with the contents that is previously
		copied into a buffer */
		if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr,
								   BYTES_TO_READ_37))
		{
			u32Ret += 8;
		}	
		}	


		s32PosToSet = FIRST_POSITION_TO_SET;
		/* Seek to the byte position specified */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 16;
		}		
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,(tU32)BYTES_TO_READ_37);
		/* The read number is important. Compare if expected number 
		is read */
		if( (OSAL_ERROR == ReadRet) || ( BYTES_TO_READ_37 != ReadRet) )
		{
			u32Ret += 32; 
		}
		else
		{
		/* Compare the read bytes with the contents that is previously
		copied into a buffer */
		if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr2,
								   BYTES_TO_READ_37))
		{
			u32Ret += 64;
		}	
		}	

		s32PosToSet = SECOND_POSITION_TO_SET;
		/* Seek to the byte position specified */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 128;
		}		
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,BYTES_TO_READ_37);
		/* The read number is important. Compare if expected number 
		is read */
		if( (OSAL_ERROR == ReadRet) || ( BYTES_TO_READ_37 != ReadRet) )
		{
			u32Ret += 256; 
		}
		else
		{
		/* Compare the read bytes with the contents that is previously
		copied into a buffer */
		if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr3,
								   BYTES_TO_READ_37))
		{
			u32Ret += 512;
		}	
		}	

		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 1024;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileSetPosFrmBOFInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_029
* DESCRIPTION:  Sets position for different offsets with invalid parameters.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007
*               Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008 
******************************************************************************/
tU32  u32CDROMFileSetPosFrmBOFInvalParam(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;
	tS32 s32PosToSet = 0;

	/* Open the file, this valid handle can be used */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if(OSAL_ERROR == hFile)
	{
		u32Ret += 2;
	}
	else
	{
		/* Attempt to get file position with invalid macro */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_NULL,
							(tS32)u32FilePos)  )
		{
			u32Ret += 4;
		}

		s32PosToSet = NEGATIVE_POS_TO_SET;
		/* Attempt to get file position by passing invalid parameter 
		to the control macro, i.e. a negative offset */
		if( OSAL_ERROR != OSAL_s32IOControl( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
								s32PosToSet  ) )
		{
			u32Ret += 8;
		}
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	 }
  	
   	return u32Ret;
}	
/*****************************************************************************
* FUNCTION:	    u32CDROMFileSetPosFrmBOFAfterClosing( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_030
* DESCRIPTION:  Sets position for different offsets after closing the file.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileSetPosFrmBOFAfterClosing()
{	
	
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* close the file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 2;
		}
		else
		{
			/* Now attempt to set file position. Should fail */
			if( OSAL_ERROR != OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
								(tS32)&u32FilePos)  )
			{
				u32Ret += 4;
			}
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadInJumps( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_031
* DESCRIPTION:  Read Contents of the first and last file alternatively.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32  u32CDROMFileReadInJumps(void)
{
	   
	OSAL_tIODescriptor hFile1=0;
	OSAL_tIODescriptor hFile2=0;
   	static char DataArr[BYTES_TO_READ] = {0x00,0x00,0x00,0x80,
									   0x01,0x00,0x00,0x00,
									   0x02,0x00,0x00,0x80,
									   0x03,0x00,0x00,0x00,
									   0x04,0x00,0x00,0x80};
	tU32 u32Ret = 0;
	tU32 LoopCount = 0;
	tS8 ReadData[BYTES_TO_READ];
	tS32 ReadRet = 0;
	tU32 s32PosToSet = 0;

	/* The laser jump operation will read the contents of the first file and  
	contents of the last file successively for a 100 times in a loop */
	for(LoopCount =ZERO_PARAMETER_CDROM ; LoopCount < MAX_LASER_JUMPS; LoopCount++)
	{
		hFile1 = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
		if( OSAL_ERROR == hFile1)
		{
			u32Ret = 1;
		}
		else
		{
			s32PosToSet = ZERO_PARAMETER_CDROM;
			/* Seek to the byte position specified */
			if(OSAL_s32IOControl ( hFile1,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret += 2;
			}		
			/* Read the specified number of bytes into an array */
			ReadRet = OSAL_s32IORead(hFile1,ReadData,BYTES_TO_READ);
			/* The read number is important. Compare if expected number 
			is read */
			if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ != ReadRet ) )			{
				u32Ret += 4; 
			}
			else
			{
			/* Compare the read bytes with the contents that is previously
			copied into a buffer */
			if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr,
								   BYTES_TO_READ))
			{
				u32Ret += 8;
			}	
			}	
			if(OSAL_ERROR == OSAL_s32IOClose(hFile1))
			{
				u32Ret += 16;
			}
		}

		/* Repeat the same procedure for the last data file on the CD */
		hFile2 = OSAL_IOOpen (OSAL_FILE_LAST,OSAL_EN_READONLY );
		if( OSAL_ERROR == hFile2)
		{
			u32Ret += 32;
		}
		else
		{
			s32PosToSet = ZERO_PARAMETER_CDROM;
			/* Seek to the byte position specified */
			if(OSAL_s32IOControl ( hFile2,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret += 64;
			}		
			/* Read the specified number of bytes into an array */
			ReadRet = OSAL_s32IORead(hFile2,ReadData,BYTES_TO_READ);
			/* The read number is important. Compare if expected number 
			is read */
			if( (OSAL_ERROR == ReadRet) || (ReadRet != BYTES_TO_READ) )
			{
				u32Ret += 128; 
			}
			else
			{
			/* Compare the read bytes with the contents that is previously
			copied into a buffer */
			if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr,
								   BYTES_TO_READ))
			{
				u32Ret += 256;
			}	
			}	
			if(OSAL_ERROR == OSAL_s32IOClose(hFile2))
			{
				u32Ret += 512;
			}
		}
	}
	return u32Ret;
}
	
	 
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadWithInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_032
* DESCRIPTION:  Read Contents of the  file with invalid parameters.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007
				Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008
******************************************************************************/
tU32 u32CDROMFileReadWithInvalParm(void)
{ 	
 	OSAL_tIODescriptor hFile = INVALID_PARAMETER_CDROM;
	tU32 u32Ret = 0;
    tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32  s32BytesRead = 0;
	tU32 ZeroArray[BYTES_TO_READ] = {0};

	/*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_FILE_LAST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret += 2;
	}
	else
	{
		tS32 s32BytesToRead = INVAL_BYTES_TO_READ;
		OSAL_pvMemorySet(ps8ReadBuffer,ZERO_PARAMETER_CDROM,BYTES_TO_READ);
		/* Read with invalid number of bytes */
		s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead);
		if((OSAL_ERROR !=  s32BytesRead)||(OSAL_s32StringNCompare(ps8ReadBuffer,
											(tCString)ZeroArray,BYTES_TO_READ)))
										
														
		{
			u32Ret += 4;
		}

		else
		{
			 s32BytesToRead	= BYTES_TO_READ;
			/* Read with invalid buffer */
			s32BytesRead = OSAL_s32IORead( hFile,OSAL_NULL,BYTES_TO_READ );
			if(OSAL_ERROR !=  s32BytesRead)
			{
				u32Ret += 8;
			}
		}
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	
	/* free the dynamically allocated memory */

	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadLargeData( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_033
* DESCRIPTION:  Read Contents upto size of 1MB.				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMFileReadLargeData(void)
{
	OSAL_tIODescriptor hFile=0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;

    tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = ONE_MB; 
	tS32 ReadRet=0;

	/*Dynamically Allocate Memory for the read buffer */

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);

	/* Open the file */
	hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
			s32PosToSet = ZERO_PARAMETER_CDROM;
			/* Seek to the byte position specified */
			if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret += 64;
			}		
			/* Read the specified number of bytes into an array */
			ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,(tU32)s32BytesToRead);
			/* The read number is important. Compare if expected number 
			is read */
			if( (OSAL_ERROR == ReadRet) || (ReadRet != s32BytesToRead) )
			{
				u32Ret += 128; 
			}
			/* Close the file */	
			if(OSAL_ERROR == OSAL_s32IOClose(hFile))
			{
				u32Ret += 512;
			}
	}
	/* free the dynamically allocated memory */

	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}

	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadNegOffsetFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_034
* DESCRIPTION:  Try to Read Contents with negative offset from BOF				
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32   u32CDROMFileReadNegOffsetFrmBOF()
{
	OSAL_tIODescriptor hFile=0;
	tU32 u32Ret = 0;
	tS32 ReadRet =0;
	tS32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_FILE_LAST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret += 2;
	}

	else
	{
		s32PosToSet = NEGATIVE_POS_TO_SET;
		/* Seek to the byte position specified */
		if(OSAL_ERROR  != OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							s32PosToSet) )
		{
		  	/*Dynamically Allocate Memory for the read buffer */
			ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);

			/* Read the specified number of bytes into an array
			If a negative offset can be set,*/
			ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ);
			/* The read number is important. Compare if expected number 
			is read */
			if(OSAL_ERROR != ReadRet)
			{
				u32Ret += 4; 
			}
		}
		/* If the seek to a negative offset is not possible, just close
		the file and exit */					
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadOffsetFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_035
* DESCRIPTION:  Reads file with few offsets from EOF			
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
tU32 u32CDROMFileReadOffsetFrmEOF(void)
{
	
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 CurFilePos=0;
	tS32 s32FileOff_frm_end = OFFSET_FROM_END;
	tS32 s32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesRead = 0;
	static char DataArr[BYTES_TO_READ_47] =  {0xF2,0xFE,0x13,0x80,
									   0xF3,0xFE,0x13,0x00,
									   0xF4,0xFE,0x13,0x80};

  /*Dynamically Allocate Memory for the read buffer */

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_47);

	/* Open the file */
	hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* Retreive the number of bytes between current position and EOF */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
											(tS32)&s32FilePos))
		{
			u32Ret += 2 ;
		}
		/* Check if the returned value is inside the bounds of the file size */
		if((s32FilePos <ZERO_PARAMETER_CDROM)||(s32FilePos > SIZE_OF_DATA_FILE) )
		{
			u32Ret += 4;
		}

		else
		{
			/* Now retreive the current file position */
			if(OSAL_ERROR == OSAL_s32IOControl (hFile,
					OSAL_C_S32_IOCTRL_FIOWHERE,(tS32)&CurFilePos))
			{
				u32Ret += 4 ;
			}
			/* Check if returned value is non negative */
			if(CurFilePos <ZERO_PARAMETER_CDROM)
			{
				u32Ret += 8;
			}
			else
			{
				/* This will point to the position from where read has to
				begin, i.e. at the specified offset from the end */
				s32PosToSet =  s32FilePos+CurFilePos-s32FileOff_frm_end;
				if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
						OSAL_C_S32_IOCTRL_FIOSEEK,(tS32)s32PosToSet))
				{
					u32Ret += 8;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead( hFile,
											ps8ReadBuffer,BYTES_TO_READ_47 );
					/* The read number of bytes is important, hence verify */
					if((OSAL_ERROR == s32BytesRead) || 
										(BYTES_TO_READ_47 != s32BytesRead  ))
					{
						u32Ret += 16;
					}
					if(OSAL_s32StringNCompare((tCString)ps8ReadBuffer,
								(tCString)DataArr,BYTES_TO_READ_47))
					{
						u32Ret += 32;
					}				
				}
			}
		 }
		/* Close the file */
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 64;
		}
	}

	/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadOffsetBeyondEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_036
* DESCRIPTION:  Reads file by setting offset beyond EOF		
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05,2007
******************************************************************************/
tU32 u32CDROMFileReadOffsetBeyondEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0; 
	tS32 CurFilePos=0;
	tS32 s32FileOff_frm_end = OFFSET_BEYOND_END;
	tS32 s32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);
   /* Open the file */
	hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* Retreive the number of bytes between current position and EOF */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
											(tS32)&s32FilePos))
		{
			u32Ret += 2 ;
		}
		/* Check if the returned value is inside the bounds of the file size */
		if((s32FilePos <ZERO_PARAMETER_CDROM)||(s32FilePos > SIZE_OF_DATA_FILE) )
		{
			u32Ret += 4;
		}

		else
		{
			/* Now retreive the current file position */
			if(OSAL_ERROR == OSAL_s32IOControl (hFile,
					OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
			{
				u32Ret += 4 ;
			}
			/* Check if returned value is non negative */
			if(CurFilePos <ZERO_PARAMETER_CDROM)
			{
				u32Ret += 8;
			}
			else
			{
				/* This will point to the position from where read has to
				begin, i.e. at the specified offset from the end */
				s32PosToSet =  s32FilePos+CurFilePos+s32FileOff_frm_end;
				if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
						OSAL_C_S32_IOCTRL_FIOSEEK,	(tS32)s32PosToSet))
				{
					u32Ret += 8;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead( hFile,
									ps8ReadBuffer,BYTES_TO_READ );
					/* The read number of bytes is important, hence verify */
#ifdef TSIM_OSAL
					if((s32BytesRead != 0))
					{
						u32Ret += 16;
					}
#else /*#ifdef TSIM_OSAL*/
					if((OSAL_ERROR != s32BytesRead))
					{
						u32Ret += 16;
					}
#endif /*#ifdef TSIM_OSAL*/
				}
			}
		}
		/* Close the file */
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 32;
		}
	}
		/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadEveryNthByteFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_037
* DESCRIPTION:  Reads file by skipping certain offsets at specified intervals.		
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
tU32 u32CDROMFileReadEveryNthByteFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 CurFilePos = 0;
	tS32 u32FilePos = 0;
	tS32 FileSize = 0;
	tS32 s32BytesRead = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;

	  /*Dynamically Allocate Memory for the read buffer */

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);

  	/* Open the file */
	hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* Retreive the number of bytes between current position and EOF */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
											(tS32)&u32FilePos))
		{
			u32Ret += 2 ;
		}
		/* Check if the returned value is inside the bounds of the file size */
		if((u32FilePos <ZERO_PARAMETER_CDROM)||(u32FilePos >SIZE_OF_DATA_FILE))
		{
			u32Ret += 4;
		}

		else
		{
			/* Now retreive the current file position */
			if(OSAL_ERROR == OSAL_s32IOControl (hFile,
						OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
			{
				u32Ret += 4 ;
			}
			/* Check if returned value is non negative */
			if(CurFilePos < ZERO_PARAMETER_CDROM)
			{
				u32Ret += 8;
			}
			else
			{
				/* Get the file size, this will be necessary to ensure that 
				while reading we do not go beyond the file size */
				FileSize = (u32FilePos+CurFilePos)/4;

				/* Start off with an offset of 0 */
				s32PosToSet =  0;
				
				/* Make sure that the position being set is less than the 
					file size */
//			   	while(s32PosToSet< FileSize-5)
				while(s32PosToSet<= FileSize-(SKIP_COUNT+BYTES_TO_READ_N))
				{
					if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
							OSAL_C_S32_IOCTRL_FIOSEEK,	(tS32)s32PosToSet))
					{
						u32Ret += 8;
					
					}
					else
					{
						/* Now read 5 bytes from that offset */
						s32BytesRead = OSAL_s32IORead( hFile,
										ps8ReadBuffer,BYTES_TO_READ_N);
						/* The number of bytes that are read is important, 
						hence verify the count */
				   		if(s32BytesRead != BYTES_TO_READ_N)
				   		{
				   			u32Ret += 16;
							break;
				   		}
						else
						{
							/* Now move over to the new offset */
					  		s32PosToSet += SKIP_COUNT;
						}
					}
				} /* End of while */
			}
		}
		/*Close the file */
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 64;
		}
	}
			/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadEveryNthByteFrmEOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_038
* DESCRIPTION:  Reads file by skipping certain offsets at specified intervals.
				(This is from EOF)		
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
tU32 u32CDROMFileReadEveryNthByteFrmEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 CurFilePos=0;
	tS32 u32FilePos=0;
	tS32 s32BytesRead=0;
	tS8 *ps8ReadBuffer = OSAL_NULL;

	  /*Dynamically Allocate Memory for the read buffer */

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);

	/* Open the file */
	hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* Retreive the number of bytes between current position and EOF */
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
											(tS32)&u32FilePos))
		{
			u32Ret += 2 ;
		}
		/* Check if the returned value is inside the bounds of the file size */
		if((u32FilePos <ZERO_PARAMETER_CDROM)||(u32FilePos > SIZE_OF_DATA_FILE) )
		{
			u32Ret += 4;
		}

		else
		{
			/* Now retreive the current file position */
			if(OSAL_ERROR == OSAL_s32IOControl (hFile,
					OSAL_C_S32_IOCTRL_FIOWHERE,	(tS32)&CurFilePos))
			{
				u32Ret += 4 ;
			}
			/* Check if returned value is non negative */

			if(CurFilePos <ZERO_PARAMETER_CDROM)
			{
				u32Ret += 8;
			}
			else
			{
			   
				s32PosToSet =  (u32FilePos+CurFilePos-SKIP_COUNT)/4;
								
				 
				while(s32PosToSet>0)
				{
					if(OSAL_ERROR == OSAL_s32IOControl ( hFile,
						OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)s32PosToSet))
					{
						u32Ret += 8;
					
					}
					else
					{
						/* Now read 5 bytes from that offset */
						s32BytesRead = OSAL_s32IORead( hFile,
								ps8ReadBuffer,BYTES_TO_READ_N);
						/* The number of bytes that are read is important, 
							hence verify the count */
				   		if(s32BytesRead != BYTES_TO_READ_N)
				   		{
				   			u32Ret += 16;
							break;
				   		}
						else
						{	
							s32PosToSet = s32PosToSet - SKIP_COUNT;
						}
					}
				}/* End of while */
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 64;
		}
	}
				/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadLongFileName( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_039
* DESCRIPTION:  Read the contents of the file which has a long file name
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
 tU32 u32CDROMFileReadLongFileName(void)
 {

	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
   	static char DataArr[BYTES_TO_READ] = {0x6C,0x6F,0x6E,0x67,
									   0x5F,0x6E,0x61,0x6D,
									   0x65,0x5F,0x74,0x65,
									   0x73,0x74,0x5F,0x6C,
									   0x6F,0x6E,0x67,0x5F};
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;

     /*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ);

	/* Open the file */
	hFile = OSAL_IOOpen (OSAL_LONG_FILE_NAME,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 00;
		/* Seek to the byte position specified */
		if(OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
		{
			u32Ret += 2;
		}		
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ);
		/* The read number is important. Compare if expected number 
		is read */
		if( (OSAL_ERROR == ReadRet) || ( BYTES_TO_READ != ReadRet) )
		{
			u32Ret += 4; 
		}
		/* Compare the read bytes with the contents that is previously
		copied into a buffer */
		if(OSAL_s32StringNCompare((tCString)ps8ReadBuffer,(tCString)DataArr,
								   BYTES_TO_READ))
		{
			u32Ret += 8;
		}	
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	/* free the dynamically allocated memory */
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;

	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileThroughPutFirstFile( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_040
* DESCRIPTION:  Calculate the rate of data transfer for a read operation 
				(for the first file of the CD)
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
tU32  u32CDROMFileThroughPutFirstFile(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 Read_ThroughPut=0;
   	OSAL_tMSecond StartTime=0;
   	OSAL_tMSecond EndTime = 0;
   	OSAL_tMSecond ReadTime = 0;
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	
	/*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_TPUT);

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 0;
		/* Seek to the byte position specified */
		if(OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
		{
			u32Ret += 2;
		}	
	   /* Read the time before the start of the read operation */	
	   StartTime = OSAL_ClockGetElapsedTime();
	   	
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ_TPUT);
#ifdef TSIM_OSAL
                /* reading is tooo fast on TSIM */
                /* So delay to make the time measurement meaningful*/
                OSAL_s32ThreadWait(10);
#endif
		/* Read the time before the end of the read operation */	
	  	EndTime = OSAL_ClockGetElapsedTime();
	  	
	  	/* The read number is important. Compare if expected number 
		is read. But get the time before this as the time calculation in
		critical only for the read operation, not the verification */
	
		if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_TPUT != ReadRet ) )
		{
			u32Ret += 4; 
		}
		else
		{
		    /* Check if end time greater than start time */
			if(EndTime > StartTime)
			{
                            /* Calculate the time required for read in millisec */
                            ReadTime =   (EndTime - StartTime); 
                        }else{
                        ReadTime =   (0xFFFFFFFF-StartTime) +EndTime+1;
                    }
				/* Throughput needs to be expressed in kB/s */
                    /* check for ReadTime != 0 */
                    if(ReadTime != 0){
                        /* now we have bytes/ms which is == kB/s */ 
                        Read_ThroughPut = (BYTES_TO_READ_TPUT) / (ReadTime);                                                              
                    }else{
                        Read_ThroughPut = 0;
                    }
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32CDROMFileThroughPutFirstFile: %d kB/s  (%d Bytes read) ",
                                      Read_ThroughPut, 
                                      BYTES_TO_READ_TPUT);
                    /* If the throughput < 300, indicate an error condition */			
                    if(	Read_ThroughPut < 300 )
				{
					u32Ret += 8;
				}
			}
		/* Close the file */	
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	/* free the dynamically allocated memory */
	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileThroughPutLastFile( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_041
* DESCRIPTION:  Calculate the rate of data transfer for a read operation 
				(for the last file on the CD)
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
 tU32  u32CDROMFileThroughPutLastFile(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 Read_ThroughPut = 0;
   	OSAL_tMSecond StartTime = 0;
   	OSAL_tMSecond EndTime = 0;
   	OSAL_tMSecond ReadTime = 0;

	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	
	/*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_TPUT);

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen ( OSAL_FILE_LAST,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 0;
		/* Seek to the byte position specified */
		if( OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 2;
		}	
	   /* Read the time before the start of the read operation */	
	   StartTime = OSAL_ClockGetElapsedTime();
	   	
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ_TPUT);
#ifdef TSIM_OSAL
                /* reading is tooo fast on TSIM */
                /* So delay to make the time measurement meaningful*/
                OSAL_s32ThreadWait(10);
#endif
		/* Read the time before the end of the read operation */	
	  	EndTime = OSAL_ClockGetElapsedTime();
	  	
	  	/* The read number is important. Compare if expected number 
		is read. But get the time before this as the time calculation in
		critical only for the read operation, not the verification */
	
		if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_TPUT != ReadRet ) )

		{
			u32Ret += 4; 
		}
	   else
		{
                    /* Check if end time greater than start time */
			if(EndTime > StartTime)
			{
                            /* Calculate the time required for read in millisec */
                            ReadTime =   (EndTime - StartTime); 
                        }else{
                        ReadTime =   (0xFFFFFFFF-StartTime) +EndTime+1;
                    }
				/* Throughput needs to be expressed in kB/s */
                    /* check for ReadTime != 0 */
                    if(ReadTime != 0){
                        /* now we have bytes/ms which is == kB/s */ 
                        Read_ThroughPut = (BYTES_TO_READ_TPUT) / (ReadTime);                                                              
                    }else{
                        Read_ThroughPut = 0;
                    }

                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32CDROMFileThroughPutLastFile: %d kB/s  (%d Bytes read) ",
                                      Read_ThroughPut, 
                                      BYTES_TO_READ_TPUT);
                    /* If the throughput < 300, indicate an error condition */			
                    if(	Read_ThroughPut < 300 )
				{
					u32Ret += 8;
				}
			}
		/* Close the file */	
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	/* free the dynamically allocated memory */
	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMFileThroughPutMP3File( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_042
* DESCRIPTION:  Calculate the rate of data transfer for a read operation 
				(for a file of MP3 format)
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
				Updated by Rakesh Dhanya (RBIN/EDI3) on June 05, 2007

******************************************************************************/
 tU32  u32CDROMFileThroughPutMP3File(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0,Read_ThroughPut=0;
   	OSAL_tMSecond StartTime = 0; 
   	OSAL_tMSecond EndTime = 0; 
   	OSAL_tMSecond ReadTime = 0;
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	
	/*Dynamically Allocate Memory for the read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_TPUT);

	/* Open the file of .dat format */
	hFile = OSAL_IOOpen (OSAL_MP3_FILE,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 0;
		/* Seek to the byte position specified */
		if( OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
		{
			u32Ret += 2;
		}	
	   /* Read the time before the start of the read operation */	
	   StartTime = OSAL_ClockGetElapsedTime();
	   	
		/* Read the specified number of bytes into an array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ_TPUT);
#ifdef TSIM_OSAL
                /* reading is tooo fast on TSIM */
                /* So delay to make the time measurement meaningful*/
                OSAL_s32ThreadWait(10);
#endif
		/* Read the time before the end of the read operation */	
	  	EndTime = OSAL_ClockGetElapsedTime();
	  	
	  	/* The read number is important. Compare if expected number 
		is read. But get the time before this as the time calculation in
		critical only for the read operation, not the verification */
	
		if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_TPUT != ReadRet ) )

		{
			u32Ret += 4; 
		}
		else
		{
                    /* Check if end time greater than start time */
			if(EndTime > StartTime)
			{
                            /* Calculate the time required for read in millisec */
                            /* Resch: Time si alrteady in ms */
                            ReadTime =   (EndTime - StartTime); // /1000;
                        }else{
                        ReadTime =   (0xFFFFFFFF-StartTime) +EndTime+1;
                    }
				/* Throughput needs to be expressed in kB/s */
                    /* check for ReadTime != 0 */
                    if(ReadTime != 0){
                        /* now we have bytes/ms which is == kB/s */ 
                        Read_ThroughPut = (BYTES_TO_READ_TPUT) / (ReadTime);                                                              
                    }else{
                        Read_ThroughPut = 0;
                    }
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32CDROMFileThroughPutMP3File: %d kB/s  (%d Bytes read) ",
                                      Read_ThroughPut, 
                                      BYTES_TO_READ_TPUT);
                    /* If the throughput < 300, indicate an error condition */			
                    if(	Read_ThroughPut < 300 )
				{
					u32Ret += 8;
				}
			}
		/* Close the file */	
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 16;
		}
	}
	/* free the dynamically allocated memory */
	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	   	u32CDROMGetVersionNumber( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_043
* DESCRIPTION:   Gets the version
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32  u32CDROMGetVersionNumber(void)
{
   	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tChar szVersionBuffer [5]={0};
    
	/* Open the CDROM device */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( OSAL_ERROR == hDevice)
    {
    	u32Ret = 1;
    }
    else
	{
		/* Get the version and store it into a buffer */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hDevice,
					OSAL_C_S32_IOCTRL_VERSION,
						 (tS32)szVersionBuffer ))
		{
			u32Ret	+= 2;
		}
		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	   	u32CDROMGetVersionNumberInvalParm( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_044
* DESCRIPTION:   Try to Get the version	with invalid parameters
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007
*               Modified by Shilpa Bhat (RBEI/ECM1) on 30 July, 2008 
******************************************************************************/
tU32 u32CDROMGetVersionNumberInvalParm(void)
{
	OSAL_tIODescriptor hDevice = INVALID_PARAMETER_CDROM;
    tU32 u32Ret = 0;

    
	/* Open the CDROM device */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( OSAL_ERROR == hDevice)
    {
    	u32Ret += 2;
    }
    else
	{
		/* Try to Get the version with an invalid buffer */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hDevice,
						OSAL_C_S32_IOCTRL_VERSION,
						 						OSAL_NULL ))
		{
			u32Ret	+= 4;
		}
		/* Close the device */
	   	if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 8;
		}

	}
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	   	u32CDROMInvalFuncParm2IOCTL( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_045
* DESCRIPTION:   Check the functionality of the IOCTL macro with invalid
				 parameters
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007

******************************************************************************/
tU32 u32CDROMInvalFuncParm2IOCTL(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	OSAL_trMediaInfo MediaInfo;
	tS32 tIoctl_ret = 0;
    
	/* Open the CDROM device */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( OSAL_ERROR == hDevice)
    {
    	u32Ret = 1;
    }
    else
	{
		/* Try a control operation with an invalid macro */
		tIoctl_ret = OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_INVAL_FUNC_CDROM,
						 (tS32)&MediaInfo );
		if( OSAL_ERROR != tIoctl_ret)
		{
		  	u32Ret	= 3;
		}
		/* Close the CDROM device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMInvalidMedia()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_046
* DESCRIPTION:  Test all above cases with Invalid media
* HISTORY:	    Created Rakesh Dhnaya (RBIN/EDI3) May 31, 2007
*				Updated by Rakesh Dhanya(RBIN/EDI3) on June 05, 2007   
******************************************************************************/

tU32 u32CDROMInvalidMedia(void)
{
	tS32 tFunc_ret = 0;

	tFunc_ret = (tS32)u32CDROMOpenClosedevice();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	" u32CDROMOpendevice-%s(%d)(INV MEDIA)",tFunc_ret == 0?
				"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendevInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendevInvalParm-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendevDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendevDiffModes-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMReOpendev();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReOpendev-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	
   	tFunc_ret = (tS32)u32CDROMClosedevAlreadyClosed();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMClosedevAlreadyClosed-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpenClosedir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendir-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);


	tFunc_ret = (tS32)u32CDROMOpendirInvalid() ;
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendirInvalid-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendirDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"ReOpenDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMOpendirwithFile( );
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendirDiffModes-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret); 

	
	tFunc_ret = (tS32)u32CDROMReOpenDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReOpenDir-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret); 

	tFunc_ret = (tS32)u32CDROMCreateDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMCreateDir-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMDelDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMDelDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenClose();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpen-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenInvalParam();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenInvalParam-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenInvalPath();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenInvalPath-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenDiffModes-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret  = (tS32)u32CDROMFileOpenCloseDiffExtns();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenDiffExtns-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret =  (tS32)u32CDROMFileReOpen()	;
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReOpen-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReClose();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReClose-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileCreate();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileCreate-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileRemove();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileRemove-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	#if 0 /* This case genrates a CPU exception */
	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOFInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOFInvalParm-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	 #endif

 
	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOFAfterClosing-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	#if 0 /* This case genrates a CPU exception */

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOFInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOFInvalParmt-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	#endif


	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOFAfterClosing-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOFInvalParam();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOFInvalParam-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOFAfterClosing-%s(%d)(INV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadInJumps();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadInJumps-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadWithInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadWithInvalParm-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	
	tFunc_ret = (tS32)u32CDROMFileReadLargeData();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadLargeData-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadNegOffsetFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadNegOffsetFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);	

	tFunc_ret = (tS32)u32CDROMFileReadOffsetFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadOffsetFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMFileReadOffsetBeyondEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadOffsetBeyondEOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadEveryNthByteFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadEveryNthByteFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadEveryNthByteFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadEveryNthByteFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	
	tFunc_ret = (tS32)u32CDROMFileReadLongFileName();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadLongFileName-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutFirstFile();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutFirstFile-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutLastFile();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutLastFile-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutMP3File();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutMP3File-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMGetVersionNumber();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMGetVersionNumber-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	
	tFunc_ret = (tS32)u32CDROMGetVersionNumberInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMGetVersionNumberInvalParm-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMInvalFuncParm2IOCTL();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMInvalFuncParm2IOCTL-%s(%d)(INV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMReadAsync();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReadAsync-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);
	
	return (tU32)tFunc_ret;
}
/*****************************************************************************
* FUNCTION:	    u32CDROMReversedMedia()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_047
* DESCRIPTION:  Test all above cases with Reverse media
* HISTORY:		Created Rakesh Dhnaya (RBIN/EDI3) May 31, 2007  
*				Updated by Rakesh Dhanya(RBIN/EDI3) on June 05, 2007
******************************************************************************/

tU32 u32CDROMReversedMedia(void)
{
	tS32 tFunc_ret = 0;

	tFunc_ret = (tS32)u32CDROMOpenClosedevice();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	" u32CDROMOpendevice-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendevInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendevInvalParm-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendevDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendevDiffModes-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMReOpendev();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReOpendev-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

   	tFunc_ret = (tS32)u32CDROMClosedevAlreadyClosed();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMClosedevAlreadyClosed-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpenClosedir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendir-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);


	tFunc_ret = (tS32)u32CDROMOpendirInvalid() ;
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendirInvalid-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMOpendirDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"ReOpenDir-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMOpendirwithFile( );
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMOpendirDiffModes-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret); 

	
	tFunc_ret = (tS32)u32CDROMReOpenDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReOpenDir-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret); 

	tFunc_ret = (tS32)u32CDROMCreateDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMCreateDir-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMDelDir();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMDelDir-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenClose();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpen-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenInvalParam();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenInvalParam-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenInvalPath();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenInvalPath-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileOpenDiffModes();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenDiffModes-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret  = (tS32)u32CDROMFileOpenCloseDiffExtns();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileOpenDiffExtns-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret =  (tS32)u32CDROMFileReOpen()	;
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReOpen-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
 
	tFunc_ret = (tS32)u32CDROMFileReClose();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReClose-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileCreate();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileCreate-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileRemove();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileRemove-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOF-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	#if 0 /* This case genrates a CPU exception */
	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOFInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOFInvalParm-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	 #endif

 
	tFunc_ret = (tS32)u32CDROMFileGetPosFrmBOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmBOFAfterClosing-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOF-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	#if 0 /* This case genrates a CPU exception */

	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOFInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOFInvalParmt-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);
	#endif


	tFunc_ret = (tS32)u32CDROMFileGetPosFrmEOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileGetPosFrmEOFAfterClosing-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOF-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOFInvalParam();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOFInvalParam-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileSetPosFrmBOFAfterClosing();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileSetPosFrmBOFAfterClosing-%s(%d)(REV MEDIA)",tFunc_ret != 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadInJumps();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadInJumps-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadWithInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadWithInvalParm-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	
	tFunc_ret = (tS32)u32CDROMFileReadLargeData();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadLargeData-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadNegOffsetFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadNegOffsetFrmBOF-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);	

	tFunc_ret = (tS32)u32CDROMFileReadOffsetFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadOffsetFrmEOF-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMFileReadOffsetBeyondEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadOffsetBeyondEOF-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadEveryNthByteFrmBOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadEveryNthByteFrmBOF-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileReadEveryNthByteFrmEOF();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadEveryNthByteFrmEOF-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	
	tFunc_ret = (tS32)u32CDROMFileReadLongFileName();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileReadLongFileName-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutFirstFile();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutFirstFile-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutLastFile();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutLastFile-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMFileThroughPutMP3File();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMFileThroughPutMP3File-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMGetVersionNumber();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMGetVersionNumber-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);

	
	tFunc_ret = (tS32)u32CDROMGetVersionNumberInvalParm();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMGetVersionNumberInvalParm-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMInvalFuncParm2IOCTL();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMInvalFuncParm2IOCTL-%s(%d)(REV MEDIA)",tFunc_ret == 0?
			"PASSED":"FAILED",tFunc_ret);
	
    tFunc_ret = (tS32)u32CDROMReadAsync();
	vTtfisTrace(OSAL_C_TRACELEVEL1,
	"u32CDROMReadAsync-%s(%d)(REV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	return (tU32)tFunc_ret;
}

/*****************************************************************************
* FUNCTION:	   	u32CDROMReadAsync( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_056
* DESCRIPTION:  Read asynchronously from the file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on July 29, 2008
******************************************************************************/
tU32 u32CDROMReadAsync(tVoid)
{
	OSAL_tIODescriptor hFile = 0;	
	OSAL_trAsyncControl rAsyncCtrl = {0};   	
    tS8 *ps8ReadBuffer = NULL;
    tU32 u32Ret = 0;               
    tU32 u32BytesToRead = 10;
    tS32 s32BytesRead = 0;
    tS32 s32Result = 0;
   	tU32 u32OsalRetVal = 0;
	tChar szSemName[]   = "SemAsyncCDROMRead";
   	tS32 s32RetVal = OSAL_ERROR;
	
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if(OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/*Allocate memory*/
		ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BytesToRead+1);
		/*Check if memory allocated successfully*/
   		if ( ps8ReadBuffer == NULL )
	   	{
	      	u32Ret = 2;
	   	}
		else
		{
	   		/*Create semaphore for event signalling used by async callback*/
			s32RetVal = OSAL_s32SemaphoreCreate (szSemName,&hSemAsyncCDROM,0);
			/*Check if semaphore is already existent*/
		   	if(s32RetVal == OSAL_ERROR)
		    {
				u32Ret = 3;      
		    }
			else
			{
				/*asynchronous control structure*/
				rAsyncCtrl.id = hFile;
			    rAsyncCtrl.s32Offset = 0;
			    rAsyncCtrl.pvBuffer = ps8ReadBuffer;
			    rAsyncCtrl.u32Length = u32BytesToRead;
			    rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTCDROMAsyncCallback;
			    rAsyncCtrl.pvArg = &rAsyncCtrl;

			   	if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			    {
			    	u32Ret += 10;     
				}
    			else
    			{
				      s32Result = OSAL_s32SemaphoreWait (hSemAsyncCDROM, 5000);
					  if (( s32Result != OSAL_OK )&&( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT ))
      				  {
         				s32Result = OSAL_s32IOCancelAsync (rAsyncCtrl.id,&rAsyncCtrl);
						u32Ret += 20; 			
        			  }
      				  else
      				  {
					  	u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );        				
         				if (!(u32OsalRetVal == OSAL_E_INPROGRESS||u32OsalRetVal == OSAL_E_CANCELED))
						{
							s32BytesRead = OSAL_s32IOReturnAsync(&rAsyncCtrl);				
							if( s32BytesRead != (tS32)u32BytesToRead)
            				{
            					u32Ret += 100;
            				}     	
						}
         	    	}
		    	}
			}		    	      
   			if(ps8ReadBuffer != NULL)
			{
   				OSAL_vMemoryFree (ps8ReadBuffer);
				ps8ReadBuffer = NULL;
			} 
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}

	OSAL_s32SemaphoreClose ( hSemAsyncCDROM);
   	OSAL_s32SemaphoreDelete( szSemName );

   	return u32Ret;   
}


/*****************************************************************************
* FUNCTION:	    u32CDROMFileReadInvalidCount()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_048
* DESCRIPTION:  Try to Read Contents with invalid read count				
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) 
******************************************************************************/
tU32 u32CDROMFileReadInvalidCount(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 ReadRet = 0;
	tS8 *ReadBuf = 0;
	tS32 s32BytesToRead = 10;

	/* File Open */
	hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST,OSAL_EN_READONLY);
	if(OSAL_ERROR != hFile)
	{
		/* Allocate memory for read buffer */
		ReadBuf = (tS8 *)OSAL_pvMemoryAllocate((tU32)s32BytesToRead);
		/* Set byte position specified */
		s32BytesToRead = -10;
		/* Read specified number of bytes into read buffer */	
		ReadRet = OSAL_s32IORead(hFile,ReadBuf,(tU32)s32BytesToRead);
		if(OSAL_ERROR != ReadRet)
		{
			u32Ret += 2;
		}
		/* File Close */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 4;
		}			
	}
	else
	{
		u32Ret += 1;
	}
	/* Free Memory */
	if(OSAL_NULL != ReadBuf)
	{
		OSAL_vMemoryFree(ReadBuf);
		ReadBuf = OSAL_NULL;
	}

	return u32Ret;
}

/* EOF */

