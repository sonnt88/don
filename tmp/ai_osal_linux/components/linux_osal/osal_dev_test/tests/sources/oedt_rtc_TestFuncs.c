/******************************************************************************
*FILE         : oedt_rtc_TestFuncs.c
*
*SW-COMPONENT : OEDT_FrmWrk 
*
*DESCRIPTION  : This file implements the  test cases for the rtc 
*               device.
*               
*AUTHOR       :Ravindran P(CM-DI/EAP)
*
*COPYRIGHT    : (c) 2006 Blaupunkt Werke GmbH
*
*HISTORY      : 11.10.06 .Initial version
            23.03.07 - added test cases for dev_rt cbased on the External RTC driver
            25.04.07 - added test cases for offset recalculation when GPS time is set
            23.10.07 -  RTC_MAX_YRS_PRODUCTION value chnaged to 102
                     OR mechanism for IORead
            15.11.07 -  tu32RTCMulOpenMax,tu32RTCMulOpenClose test cases added
            26.06.07 - Added Test Case modifications for Internal RTC(tny1kor)
            26.05.10 - Added Test Cases and case modifications for
                              Internal RTC on Gen2( tny1kor )
            17/02/2015 - Lint Fix (CFG3-1023) - Kranthi Kiran Kongnati(RBEI/ECF5)
            
*******************************************************************************/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osioctrl.h"
#include "oedt_rtc_TestFuncs.h"




/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

static OSAL_trTimeDate rRTCDateTimeRef[394] = 
{
   {0 ,0 ,0 ,1 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {1 ,1 ,1 ,2 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {3 ,3 ,2 ,3 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {5 ,5 ,3 ,4 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {7 ,7 ,4 ,5 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {9 ,9 ,5 ,6 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {11,11,6 ,7 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {13,13,7,8 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {15,15,8 ,9 ,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {17,17,9 ,10,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {19,19,10,11,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {21,21,11,12,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,12,13,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,13,14,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,14,15,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,15,16,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,16,17,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,17,18,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,18,19,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,19,20,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,20,21,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,21,22,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,22,23,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,23,24,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,0 ,25,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,1 ,26,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,2 ,27,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,3 ,28,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,4 ,29,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,5 ,30,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,6 ,31,1 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,7,1 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0}, 
   {6 ,6 ,8 ,2 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,9 ,3 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,10,4 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,11,5 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,12,6 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,13,7 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,14,8 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,15,9 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,16,10,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,17,11,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,18,12,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,19,13,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,20,14,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,21,15,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,22,16,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,23,17,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,0 ,18,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {40,40,1 ,19,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,2 ,20,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {44,44,3 ,21,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {46,46,4 ,22,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {48,48,5 ,23,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {50,50,6 ,24,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {52,52,7 ,25,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {54,54,8 ,26,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {56,56,9 ,27,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {58,58,10,28,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {0 ,0 ,11,1 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {1 ,1 ,12,2 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {3 ,3 ,13,3 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {5 ,5 ,14,4 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {7 ,7 ,15,5 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {9 ,9 ,16,6 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {11,11,17,7 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {13,13,18,8 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {15,15,19,9 ,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {17,17,20,10,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {19,19,21,11,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {21,21,22,12,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,23,13,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,0 ,14,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,1 ,15,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,2 ,16,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,3 ,17,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,4 ,18,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,5 ,19,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,6 ,20,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,7 ,21,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,8 ,22,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,9 ,23,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,10,24,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,11,25,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,12,26,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,13,27,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,14,28,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,15,29,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,16,30,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,17,31,3 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,18,1 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,19,2 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,20,3 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,21,4 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,22,5 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,23,6 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,0 ,7 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,1 ,8 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,2 ,9 ,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,3 ,10,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,4 ,11,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,5 ,12,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,6 ,13,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,7,14,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,8 ,15,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,9 ,16,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,10,17,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,11,18,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},   
   {40,40,12,19,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,13,20,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {44,44,14,21,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {46,46,15,22,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {48,48,16,23,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {50,50,17,24,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {52,52,18,25,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {54,54,19,26,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {56,56,20,27,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {58,58,21,28,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {0 ,0 ,22,29,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {1 ,1 ,23,30,4 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {3 ,3 ,0 ,1 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {5 ,5 ,1 ,2 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {7 ,7 ,2 ,3 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {9 ,9 ,3 ,4 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {11,11,4 ,5 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {13,13,5 ,6 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {15,15,6 ,7 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {17,17,7,8 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {19,19,8 ,9 ,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {21,21,9 ,10,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,10,11,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,11,12,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,12,13,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,13,14,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,14,15,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,15,16,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,16,17,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,17,18,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,18,19,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,19,20,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,20,21,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,21,22,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,22,23,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,23,24,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,0 ,25,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,1 ,26,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,2 ,27,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,3 ,28,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,4 ,29,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,5 ,30,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,6 ,31,5 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,7,1 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,8 ,2 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,9 ,3 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,10,4 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,11,5 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,12,6 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,13,7 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,14,8 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,15,9 ,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,16,10,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,17,11,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,18,12,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,19,13,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,20,14,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,21,15,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,22,16,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {40,40,23,17,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,0 ,18,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {44,44,1 ,19,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {46,46,2 ,20,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {48,48,3 ,21,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {50,50,4 ,22,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {52,52,5 ,23,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {54,54,6 ,24,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {56,56,7,25,6  ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {58,58,8 ,26,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {0 ,0 ,9 ,27,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {1 ,1 ,10,28,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {3 ,3 ,11,29,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {5 ,5 ,12,30,6 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {7 ,7 ,13,1 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {9 ,9 ,14,2 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {11,11,15,3 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {13,13,16,4 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {15,15,17,5 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {17,17,18,6 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {19,19,19,7 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {21,21,20,8 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,21,9 ,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,22,10,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,23,11,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,0 ,12,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,1 ,13,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,2 ,14,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,3 ,15,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,4 ,16,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,5 ,17,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,6 ,18,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,7,19,7  ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,8 ,20,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,9 ,21,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,10,22,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,11,23,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,12,24,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,13,25,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,14,26,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,15,27,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,16,28,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,17,29,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,18,30,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,19,31,7 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,20,1 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,21,2 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,22,3 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,23,4 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,0 ,5 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,1 ,6 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,2 ,7 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,3 ,8 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,4 ,9 ,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,5 ,10,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,6 ,11,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,7,12,8  ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,8 ,13,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,9 ,14,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {40,40,10,15,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,11,16,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {44,44,12,17,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {46,46,13,18,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {48,48,14,19,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {50,50,15,20,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {52,52,16,21,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {54,54,17,22,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {56,56,18,23,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {58,58,19,24,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {7 ,7 ,20,25,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {9 ,9 ,21,26,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {11,11,22,27,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {13,13,23,28,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {15,15,0 ,29,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {17,17,1 ,30,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {19,19,2 ,31,8 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {21,21,3 ,1 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,4 ,2 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,5 ,3 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,6 ,4 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,7,5 ,9  ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,8 ,6 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,9 ,7 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,10,8 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,11,9 ,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,12,10,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,13,11,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,14,12,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,15,13,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,16,14,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,17,15,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,18,16,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,19,17,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,20,18,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,21,19,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,22,20,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,23,21,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,0 ,22,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,1 ,23,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,2 ,24,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,3 ,25,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,4 ,26,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,5 ,27,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,6 ,28,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,7,29,9  ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,8 ,30,9 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,9 ,1 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {26,26,10,2 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {28,28,11,3 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {30,30,12,4 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {32,32,13,5 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {34,34,14,6 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {36,36,15,7 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {38,38,16,8 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {40,40,17,9 ,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {42,42,18,10,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {44,44,19,11,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {46,46,20,12,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {48,48,21,13,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {50,50,22,14,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {52,52,23,15,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {54,54,0 ,16,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {56,56,1 ,17,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {58,58,2 ,18,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},  
   {21,21,3 ,19,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {23,23,4 ,20,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {25,25,5 ,21,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {27,27,6 ,22,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {29,29,7,23,10 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {31,31,8 ,24,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {33,33,9 ,25,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {35,35,10,26,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {37,37,11,27,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {39,39,12,28,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {41,41,13,29,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {43,43,14,30,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {45,45,15,31,10,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,16,1 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,17,2 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,18,3 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,19,4 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,20,5 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,21,6 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,22,7 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,23,8 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,0 ,9 ,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,1 ,10,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,2 ,11,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,3 ,12,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,4 ,13,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,5 ,14,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,6 ,15,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,7,16,11 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,8 ,17,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,9 ,18,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,10,19,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,11,20,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,12,21,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,13,22,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,14,23,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,15,24,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,16,25,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {40,40,17,26,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,18,27,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {44,44,19,28,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {46,46,20,29,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {48,48,21,30,11,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {50,50,22,1 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {52,52,23,2 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {54,54,0 ,3 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {56,56,1 ,4 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {58,58,2 ,5 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {47,47,3 ,6 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {49,49,4 ,7 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {51,51,5 ,8 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {53,53,6 ,9 ,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {57,57,7 ,10,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {59,59,8 ,11,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {2 ,2 ,9 ,12,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {4 ,4 ,10,13,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {6 ,6 ,11,14,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {8 ,8 ,12,15,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {10,10,13,16,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {12,12,14,17,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {14,14,15,18,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {16,16,16,19,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {18,18,17,20,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {20,20,18,21,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {22,22,19,22,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {24,24,20,23,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {26,26,21,24,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {28,28,22,25,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {30,30,23,26,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {32,32,0 ,27,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {34,34,1 ,28,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {36,36,2 ,29,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {38,38,3 ,30,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {40,40,4 ,31,12,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   {42,42,5 ,1 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {44,44,6 ,2 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {46,46,7 ,3 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {48,48,8 ,4 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {50,50,9 ,5 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {52,52,10,6 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {54,54,11,7 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {56,56,12,8 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {58,58,13,9 ,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {26,26,14,10,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {28,28,15,11,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {30,30,16,12,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {32,32,17,13,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {34,34,18,14,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {36,36,19,15,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {38,38,20,16,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {40,40,21,17,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {42,42,22,18,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {44,44,23,19,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {46,46,0 ,20,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {48,48,1 ,21,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {50,50,2 ,22,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {52,52,3 ,23,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {54,54,4 ,24,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {56,56,5 ,25,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {58,58,6 ,26,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {47,47,7 ,27,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {49,49,8 ,28,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},
   {51,51,9 ,29,2 ,RTC_OSAL_OFFSET_DEFAULT_YEAR+1,0, 0, 0},

};

typedef struct
{
   tString szThreadName; /* Thread Name */
   OSAL_tpfThreadEntry pfThreadEntry; /* Thread entry */
}trFDCMultiThreadArgs;

typedef struct
{
   tS32              s32RetVal;/* Return value */
   OSAL_tEventHandle hMultiThreadEvent;/* Event handle */
   OSAL_tEventMask eventFlag; /* Event flag need to be sent by thread on exit */
}trThreadArg;

static tVoid tu32Gen2RTCOpenClose(  tPVoid pvThreadArgs);

static tVoid tu32Gen2RTCGetVersion(  tPVoid pvThreadArgs);
static tVoid tu32Gen2RTCRead(  tPVoid pvThreadArgs);
static void vGen2RTCEventpost(  OSAL_tEventHandle hMultiThreadEvent, OSAL_tEventMask u32EventPostVal );
static tU32 tu32Gen2RTCMultiThreadcreate(tS32 s32ThreadCount, const trFDCMultiThreadArgs *prMultiThreadArgs);

/*****************************************************************
|Function definitions (scope: module-local)
|----------------------------------------------------------------*/

/************************************************************************
*FUNCTION:    tu32Gen2RTCMulOpenClose
*DESCRIPTION: Check if continuous open and close is supported.
         any no of open and close has to be supported irrespective 
                     of max no of open supported by dev_rtc 
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     
*   20.05.10, Tinoy Mathews( RBEI/ECF1 )- Initial version
************************************************************************/ 
tU32 tu32Gen2RTCMulOpenClose( void )
{
   OSAL_tIODescriptor hDevice = 0;
   tU8 u8Index  ;
   tU32 u32RetVal  = 0;

   for( u8Index = 0;u8Index < 50;u8Index++ )
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
      if ( hDevice == OSAL_ERROR)
      {
         return( RTC_DEVICE_OPEN_ERROR + u8Index + 100 );/*to know the index of failure*/
      }
      
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         return(RTC_DEVICE_CLOSE_ERROR + u8Index + 100); /*to know the index of failure*/
      }
   }

   return( u32RetVal ); 
}

/************************************************************************
*FUNCTION:    tu32Gen2RTCMultipleRead
*DESCRIPTION:  
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     
*  20.05.10, Tinoy Mathews( RBEI/ECF1 )- Initial version
*
*17/02/2015 - Lint Fix (CFG3-1023) -             Kranthi Kiran Kongnati(RBEI/ECF5)
************************************************************************/
tU32 tu32Gen2RTCMultipleRead( void )
{
   OSAL_tIODescriptor hDevice ;
   OSAL_trRtcRead rRTCRead,rRTCReadNoTime;
   tU32 u32RetVal = 0;

   OSAL_pvMemorySet( ( tPVoid )&rRTCRead,0,sizeof( OSAL_trRtcRead ) );
   OSAL_pvMemorySet( ( tPVoid )&rRTCReadNoTime,0,sizeof( OSAL_trRtcRead ) );


   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"OSAL_IOOpen:failed");
      return(RTC_DEVICE_OPEN_ERROR);
     
   }
   
   /*Multiple times read by IORead*/
   rRTCRead.RegisterForRtcTime.updateRate = 0;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   if( OSAL_s32IORead( hDevice,(tS8 *)&rRTCRead,sizeof(rRTCRead) ) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"OSAL_IOOpen:failed");
      return( RTC_DEVICE_READ_ERROR );
      
   } 
   
   rRTCRead.RegisterForRtcTime.updateRate = 0;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   if( OSAL_s32IORead( hDevice,(tS8 *)&rRTCRead,sizeof(rRTCRead) ) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return(RTC_DEVICE_READ_ERROR + 100);
   }
   
   rRTCRead.RegisterForRtcTime.updateRate = 0;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   /*Should return error if the time to be queried is not specified*/
   if( OSAL_s32IORead (hDevice,(tS8 *)&rRTCReadNoTime,sizeof( rRTCReadNoTime ) )
         != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return(RTC_DEVICE_ILLEGAL_UPDATE_RATE_ERROR + 1000);
   }
   
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return(RTC_DEVICE_CLOSE_ERROR);
   }
   
   return ( u32RetVal );
}

/************************************************************************
*FUNCTION:    tu32Gen2RTCGpsTimeSetReadStressTest
*DESCRIPTION: Sets the GPS Time 'n' number of times 
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCSetGpsTimeRegressionSet( void )
{
   OSAL_tIODescriptor hDevice  ;
   tBool bErrorOccured         = FALSE;
   tU32 u32RetVal              = 0;
   tU8  u8ErrorNo              = 0;
   tU32 u32Count               = 100;

   
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u8ErrorNo = 0;
   }

   if( FALSE == bErrorOccured )
   {
      while( u32Count-- )
      {
         if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeRef[2] ) == OSAL_ERROR )
         {
            bErrorOccured = TRUE;
            OSAL_s32IOClose ( hDevice );
            u8ErrorNo = 3;
         }
      }
   }
   
   if(FALSE == bErrorOccured )
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         bErrorOccured = TRUE;
         u8ErrorNo = 1;

      }
   }
   
   if( FALSE == bErrorOccured )
   { 
      u32RetVal = 0;
   }
   else
   {
      
      u32RetVal = aRtcErr_oedt[u8ErrorNo] ;
      
   }
   
   return ( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCSetGpsTimeIllegalRegressionSet
*DESCRIPTION: Tries to set illegal times into the Internal RTC 
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCSetGpsTimeIllegalRegressionSet( void )
{
   OSAL_tIODescriptor hDevice  ;  
   tU32 u32RetVal              = 0;

   tU32 u32SetCount            = 5;
   
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /*As GPS time is set ,offset to RDS and BASE is recalculated*/
   while( u32SetCount-- )
   {
      if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCIllegalTimes[u32SetCount] ) != OSAL_ERROR )
      {
         OSAL_s32IOClose ( hDevice );
         return( RTC_DEVICE_SET_GPS_TIME_ERROR);
      }
   }
   
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }
   
   return( u32RetVal );
}

/************************************************************************
*FUNCTION:    tu32Gen2RTCNonExistentTime
*DESCRIPTION: Tries to read BASE and RDS Time from dev rtc
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCNonExistentTime( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   OSAL_trRtcRead rRTCRead;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* IORead for BASE Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_BASE_TIME;
   
   /* Read BASE Time */
   if( OSAL_s32IORead( hDevice,(tS8 *)&rRTCRead,sizeof(rRTCRead) ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   
   /* IORead for RDS Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_RDS_TIME;
   
   /* Read RDS Time */
   if( OSAL_s32IORead( hDevice,(tS8 *)&rRTCRead,sizeof(rRTCRead) ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }

   /* Return error code */
   return( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCReadVersion
*DESCRIPTION: Read the version information from dev rtc
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCReadVersion( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   tS32 s32Version             = 0;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_RTC_VERSION,(tS32)&s32Version) 
         == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return(RTC_DEVICE_GET_VERSION_ERROR);
   } 
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }

   /* Return error code */
   return( u32RetVal ); 
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCReadVersionInval
*DESCRIPTION: Read the version information from dev rtc
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCReadVersionInval( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   tS32 s32Version             = 0;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }


   /* Try to get version with a nonexistent handle */
   if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_VERSION,(tS32)&s32Version ) 
         != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return(RTC_DEVICE_GET_VERSION_ERROR);
   }
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }

   /* Try to get version with a NULL arguement */
   if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_VERSION, (tS32)0 ) 
         != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return(RTC_DEVICE_GET_VERSION_ERROR);
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }
   
   /* Return error code */
   return( u32RetVal ); 
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCReadInval
*DESCRIPTION: Read time from dev rtc passing invalid parameters
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCReadInval( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   OSAL_trRtcRead rRTCRead;
   tU32 u32InvalidByteSize     = sizeof( OSAL_trRtcRead )-1;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* IORead for GPS Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   /* Read GPS Time with an invalid of bytes*/
   if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, u32InvalidByteSize ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   /* Read GPS Time with a NULL buffer*/
   if( OSAL_s32IORead( hDevice,( tS8 * )NULL, sizeof( OSAL_trRtcRead ) ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }
   
   /* Read GPS Time with a non existent handle*/
   if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, sizeof( OSAL_trRtcRead ) ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }

   /* Return error code */
   return( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCReadWithoutOpen
*DESCRIPTION: Read without open from dev rtc
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCReadWithoutOpen( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  = 0;
   tU32 u32RetVal              = 0;
   OSAL_trRtcRead rRTCRead;
   
   /* IORead for GPS Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   /* Read GPS Time with a non existent handle*/
   if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, sizeof( OSAL_trRtcRead ) ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }

   /* Return error code */
   return( u32RetVal );   
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCReadExceedMax
*DESCRIPTION: Read time from dev rtc exceeding the maximum update 
*             rate
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCReadExceedMax( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   OSAL_trRtcRead rRTCRead;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* IORead for GPS Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = RTC_MAX_UPDATE_RATE+2;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   /* Read GPS Time with a non existent handle*/
   if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, sizeof( OSAL_trRtcRead ) ) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }  
   
   /* Return error code */
   return( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCSetGPSTimeNoOpen
*DESCRIPTION: Set GPS time without an open on dev rtc 
*             rate
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCSetGPSTimeNoOpen( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice  = 0;
   tU32 u32RetVal              = 0;
   
   /* Try to set the GPS Time before an open */
   if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps ) != OSAL_ERROR )
   {
      return( RTC_DEVICE_SET_GPS_TIME_ERROR);
   }
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }
   
   /* Try to set the GPS Time using a non existent handle */
   if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps ) != OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_SET_GPS_TIME_ERROR);
   }
   
   /* Return error code */
   return( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCGpsTimeStateValid
*DESCRIPTION: Check the valid state of GPS Time.
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCGpsTimeStateValid( void )
{
   /* Declarations */
   OSAL_tIODescriptor hDevice ;
   tU32 u32RetVal             = 0;
   OSAL_trRtcRead rRTCRead;
   
   /* IORead for GPS Time */
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;
   
   /* Open the device */
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      return( RTC_DEVICE_OPEN_ERROR );
   }
   
   /* Try to set the GPS Time */
   if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps ) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_SET_GPS_TIME_ERROR);
   }
   
   /* Read GPS Time */
   if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, sizeof( OSAL_trRtcRead ) ) != OSAL_ERROR )
   {
      if( OSAL_RTC_VALID != rRTCRead.RtcTimes.gpsUtcTimeState )
      {
         OSAL_s32IOClose( hDevice );
         return( RTC_DEVICE_TIME_VALID_ERROR );
      }
   }
   else
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_READ_ERROR );
   }
   
   /* Close the device */
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   } 
   
   /* Return error code */
   return( u32RetVal );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCOpenClose
*DESCRIPTION: To open/close the RTC
*
*PARAMETER:   
*             
*             
*
*RETURNVALUE: 
*
*HISTORY:   Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
static tVoid tu32Gen2RTCOpenClose(  tPVoid pvThreadArgs)
{
   OSAL_tIODescriptor hDevice ;
   tU32 u32RetVal  = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );

   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal =  RTC_DEVICE_OPEN_ERROR ;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Device Open faile");
   }

   else
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32RetVal = RTC_DEVICE_CLOSE_ERROR ;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Device close faile");
      }
   }

   prArgs->s32RetVal = (tS32)u32RetVal;

   vGen2RTCEventpost( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCIOSetGpsTime
*DESCRIPTION: To Set the GPS time
*
*PARAMETER:   tPVoid pvThreadArgs
*             
*RETURNVALUE: void
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
static tVoid tu32Gen2RTCIOSetGpsTime(  tPVoid pvThreadArgs)
{
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );

   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = RTC_DEVICE_OPEN_ERROR;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Device Open faile");
   }

   else if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      u32RetVal = RTC_DEVICE_SET_GPS_TIME_ERROR ;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Device io control faile");
   }

   else
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32RetVal = RTC_DEVICE_CLOSE_ERROR ;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Device close faile");
      }
   }

   prArgs->s32RetVal = (tS32)u32RetVal;

   vGen2RTCEventpost( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCGetVersion
*DESCRIPTION: To get the driver version of RTC
*
*PARAMETER:   tPVoid pvThreadArgs            
*
*RETURNVALUE: void
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
static tVoid tu32Gen2RTCGetVersion(  tPVoid pvThreadArgs)
{
   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   tS32 s32Version             = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );

   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = RTC_DEVICE_OPEN_ERROR;
   }

   else if( OSAL_s32IOControl( hDevice, OSAL_C_S32_IOCTRL_RTC_VERSION,
                             (tS32)&s32Version )== OSAL_ERROR )
         
   {
      OSAL_s32IOClose ( hDevice );
      u32RetVal = RTC_DEVICE_SET_GPS_TIME_ERROR ;
   }

   else
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32RetVal = RTC_DEVICE_CLOSE_ERROR ;
      }
   }

   prArgs->s32RetVal = (tS32)u32RetVal;

   vGen2RTCEventpost( prArgs->hMultiThreadEvent, prArgs->eventFlag );
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCRead
*DESCRIPTION: To read RTC
*
*PARAMETER:    tPVoid pvThreadArgs
*             
*RETURNVALUE: void
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
static tVoid tu32Gen2RTCRead(  tPVoid pvThreadArgs)
{

   OSAL_tIODescriptor hDevice  ;
   tU32 u32RetVal              = 0;
   trThreadArg                 *prArgs   = (trThreadArg *)pvThreadArgs;
   OSAL_trRtcRead rRTCRead;

   /* IORead for GPS Time*/
   rRTCRead.RegisterForRtcTime.updateRate        = 1;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;


   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );

   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = RTC_DEVICE_OPEN_ERROR;
   }

   else if( OSAL_s32IORead( hDevice,( tS8 * )&rRTCRead, sizeof( OSAL_trRtcRead ) ) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      u32RetVal = RTC_DEVICE_READ_ERROR ;
   }

   else
   {
      if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32RetVal = RTC_DEVICE_CLOSE_ERROR ;
      }
   }

   prArgs->s32RetVal = (tS32)u32RetVal;

   vGen2RTCEventpost( prArgs->hMultiThreadEvent, prArgs->eventFlag );

}
/************************************************************************
*FUNCTION:    vGen2RTCEventpost

*DESCRIPTION: 
*    
*PARAMETER:   OSAL_tEventHandle hMultiThreadEvent, OSAL_tEventMask u32EventPostVal 
*             
*RETURNVALUE: void
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
static void vGen2RTCEventpost(  OSAL_tEventHandle hMultiThreadEvent, OSAL_tEventMask u32EventPostVal )
{
   if( hMultiThreadEvent != 0 )
   {
      if( OSAL_OK != OSAL_s32EventPost( hMultiThreadEvent,
                                          u32EventPostVal,
                                          OSAL_EN_EVENTMASK_OR ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event post Error! ErrorCode : %u", OSAL_u32ErrorCode() );
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "Event post Passed!" );
      }
   }
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCMultiThreadcreate
*DESCRIPTION: 
*    
*PARAMETER: tS32 s32ThreadCount, trFDCMultiThreadArgs *prMultiThreadArgs  
*             
*RETURNVALUE: s32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
*17/02/2015 - Lint Fix (CFG3-1023) -             Kranthi Kiran Kongnati(RBEI/ECF5)
************************************************************************/
static tU32 tu32Gen2RTCMultiThreadcreate(tS32 s32ThreadCount,const trFDCMultiThreadArgs *prMultiThreadArgs)
{
   tS32                   s32Ret          = 0;
   OSAL_tEventMask        ResultMask      = 0;
   tS32                   s32Event_Mask;
   tU32  u32ThrdCnt;
   tS32  s32SpawedThreadCount = 0;
   OSAL_tEventHandle      hMultiThreadEvent;
   tCString               coszEventName="Multithread_Test_RTC";
   trThreadArg            rThrArg[10] ={0};
   OSAL_trThreadAttribute rThreadAttr;
   OSAL_tThreadID  s32ThrdID[10]={0};

   if( OSAL_OK == OSAL_s32EventCreate( coszEventName, &hMultiThreadEvent) )
   {

      for( u32ThrdCnt = 0; u32ThrdCnt <(tU32)s32ThreadCount; u32ThrdCnt++)
      {
         rThrArg[u32ThrdCnt].eventFlag         = ((tU32)1<<u32ThrdCnt);
         rThrArg[u32ThrdCnt].hMultiThreadEvent = hMultiThreadEvent;
      }

      /*thread stuff*/

      for(u32ThrdCnt=0;u32ThrdCnt< (tU32)s32ThreadCount;u32ThrdCnt++)
      {

         rThreadAttr.szName = prMultiThreadArgs[u32ThrdCnt].szThreadName;
         rThreadAttr.pfEntry = prMultiThreadArgs[u32ThrdCnt].pfThreadEntry;
         rThreadAttr.pvArg   = (tPVoid)&rThrArg[u32ThrdCnt];
         rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
         rThreadAttr.s32StackSize = TEST_THREAD_STACK_SIZE;

         s32ThrdID[u32ThrdCnt] = OSAL_ThreadSpawn( &rThreadAttr );
         if( s32ThrdID[u32ThrdCnt] == OSAL_ERROR )
         {
            // thread creation failed
            s32Ret += 100;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Thread spwn failed Name:%s",prMultiThreadArgs[u32ThrdCnt].szThreadName);
            OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL,"Thread spwn failed no:%d",u32ThrdCnt);
            break;
         }
         else
         {
            s32SpawedThreadCount++;
         }
      }

      if(s32Ret==0)
      {

         s32Event_Mask = (tS32)pow((double)2,(double)s32ThreadCount) - 1;

         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "Event_mask_value=%d",s32Event_Mask);

         if( OSAL_s32EventWait( hMultiThreadEvent, (OSAL_tEventMask)s32Event_Mask,
                  OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER,
                  &ResultMask ) != OSAL_OK )
         {
            s32Ret += 200;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "u32TriThreadTest: Wait for Events over !!!" );
            OSAL_s32EventPost( hMultiThreadEvent, ~ResultMask, OSAL_EN_EVENTMASK_AND);
         }
      }

      //close the event
      if( OSAL_ERROR == OSAL_s32EventClose( hMultiThreadEvent ))
      {
         s32Ret += 300;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "Event close failed" );
      }

      //delete the event
      if( OSAL_ERROR == OSAL_s32EventDelete( coszEventName ) )
      {
         s32Ret += 400;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "Event delete failed" );
      }



      for(u32ThrdCnt=0;u32ThrdCnt<(tU32)s32ThreadCount;u32ThrdCnt++)
      {

         if(rThrArg[u32ThrdCnt].s32RetVal != 0)
         {
            s32Ret = rThrArg[u32ThrdCnt].s32RetVal + s32Ret;
         }
      }

      //delete the Threads
      for(u32ThrdCnt= 0; u32ThrdCnt<(tU32)s32SpawedThreadCount;u32ThrdCnt++)
      {
         OSAL_s32ThreadDelete( s32ThrdID[u32ThrdCnt]);
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4, "thread[%d] is deleted",u32ThrdCnt );
      }

   }
   else
   {
      //event creation failed
      s32Ret += 500;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "Event create failed" );
   }

   return (tU32)s32Ret;

}
/************************************************************************
*FUNCTION:    u32MultiThread_OpenClose
*DESCRIPTION: 
*    
*PARAMETER:   Nil
*             
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32MultiThread_OpenClose( tVoid )
{

   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

   rMultiThreadArgs[0].szThreadName = "tu32Gen2RTCOpenClose1";
   rMultiThreadArgs[0].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[1].szThreadName = "tu32Gen2RTCOpenClose2";
   rMultiThreadArgs[1].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[2].szThreadName = "tu32Gen2RTCOpenClose3";
   rMultiThreadArgs[2].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[3].szThreadName = "tu32Gen2RTCOpenClose4";
   rMultiThreadArgs[3].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[4].szThreadName = "tu32Gen2RTCOpenClose5";
   rMultiThreadArgs[4].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[5].szThreadName = "tu32Gen2RTCOpenClose6";
   rMultiThreadArgs[5].pfThreadEntry = tu32Gen2RTCOpenClose;

   u32Ret = tu32Gen2RTCMultiThreadcreate(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   return u32Ret;
}
/************************************************************************
*FUNCTION:    u32MultiThread_SetGpsTime
*DESCRIPTION: 
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32MultiThread_SetGpsTime( tVoid )
{

   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

   rMultiThreadArgs[0].szThreadName = "tu32Gen2RTCOpenClose1";
   rMultiThreadArgs[0].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[1].szThreadName = "tu32Gen2RTCOpenClose2";
   rMultiThreadArgs[1].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[2].szThreadName = "tu32Gen2RTCOpenClose3";
   rMultiThreadArgs[2].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[3].szThreadName = "tu32Gen2RTCOpenClose4";
   rMultiThreadArgs[3].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[4].szThreadName = "tu32Gen2RTCOpenClose5";
   rMultiThreadArgs[4].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[5].szThreadName = "tu32Gen2RTCOpenClose6";
   rMultiThreadArgs[5].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   u32Ret = tu32Gen2RTCMultiThreadcreate(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   return u32Ret;
}
/************************************************************************
*FUNCTION:    u32MultiThread_GetVersion
*DESCRIPTION: 
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32MultiThread_GetVersion( tVoid )
{

   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

   rMultiThreadArgs[0].szThreadName = "tu32Gen2RTCGetVersion1";
   rMultiThreadArgs[0].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[1].szThreadName = "tu32Gen2RTCGetVersion2";
   rMultiThreadArgs[1].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[2].szThreadName = "tu32Gen2RTCGetVersion3";
   rMultiThreadArgs[2].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[3].szThreadName = "tu32Gen2RTCGetVersion4";
   rMultiThreadArgs[3].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[4].szThreadName = "tu32Gen2RTCGetVersion5";
   rMultiThreadArgs[4].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[5].szThreadName = "tu32Gen2RTCGetVersion6";
   rMultiThreadArgs[5].pfThreadEntry = tu32Gen2RTCGetVersion;
   u32Ret = tu32Gen2RTCMultiThreadcreate(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);

   return u32Ret;
}
/************************************************************************
*FUNCTION:    u32MultiThread_RtcIORead
*DESCRIPTION: 
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32MultiThread_RtcIORead( tVoid )
{
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[6];

   rMultiThreadArgs[0].szThreadName = "tu32Gen2RTCRead1";
   rMultiThreadArgs[0].pfThreadEntry = tu32Gen2RTCRead;

   rMultiThreadArgs[1].szThreadName = "tu32Gen2RTCRead2";
   rMultiThreadArgs[1].pfThreadEntry = tu32Gen2RTCRead;

   rMultiThreadArgs[2].szThreadName = "tu32Gen2RTCRead3";
   rMultiThreadArgs[2].pfThreadEntry = tu32Gen2RTCRead;

   rMultiThreadArgs[3].szThreadName = "tu32Gen2RTCRead4";
   rMultiThreadArgs[3].pfThreadEntry = tu32Gen2RTCRead;

   rMultiThreadArgs[4].szThreadName = "tu32Gen2RTCRead5";
   rMultiThreadArgs[4].pfThreadEntry = tu32Gen2RTCRead;

   rMultiThreadArgs[5].szThreadName = "tu32Gen2RTCRead6";
   rMultiThreadArgs[5].pfThreadEntry = tu32Gen2RTCRead;

   u32Ret = tu32Gen2RTCMultiThreadcreate(6,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   return u32Ret;
}
/************************************************************************
*FUNCTION:    u32MultiThread_MultiFunctionTest
*DESCRIPTION: To test the multi function of multi thread RTC
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32MultiThread_MultiFunctionTest( tVoid )
{
   tU32 u32Ret ;
   trFDCMultiThreadArgs rMultiThreadArgs[4];

   rMultiThreadArgs[0].szThreadName = "tu32Gen2RTCOpenClose";
   rMultiThreadArgs[0].pfThreadEntry = tu32Gen2RTCOpenClose;

   rMultiThreadArgs[1].szThreadName = "tu32Gen2RTCIOSetGpsTime";
   rMultiThreadArgs[1].pfThreadEntry = tu32Gen2RTCIOSetGpsTime;

   rMultiThreadArgs[2].szThreadName = "tu32Gen2RTCGetVersion";
   rMultiThreadArgs[2].pfThreadEntry = tu32Gen2RTCGetVersion;

   rMultiThreadArgs[3].szThreadName = "tu32Gen2RTCRead4";
   rMultiThreadArgs[3].pfThreadEntry = tu32Gen2RTCRead;

   u32Ret = tu32Gen2RTCMultiThreadcreate(4,(trFDCMultiThreadArgs *) rMultiThreadArgs);
   return u32Ret;
}
#if 0
/************************************************************************
*FUNCTION:    u32RTCMultiProcessTest
*DESCRIPTION: To test the multi process test of RTC
*    
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: u32Ret
*
*HISTORY:     Initial Revision -- 23/11/2012 - Sudharsanan Sivagnanam (RBEI/ECF5)
*
************************************************************************/
tU32 u32RTCMultiProcessTest(tVoid)
{
   OSAL_tMQueueHandle mqHandleR = OSAL_C_INVALID_HANDLE;
   OSAL_trProcessAttribute prAtr = {OSAL_NULL};
   OSAL_tProcessID prID_1 = INVALID_PID;
   OSAL_tProcessID prID_2 = INVALID_PID;
   tU32 u32Ret = 0;

   // Create or open MQ for both processes to send their responses
   if (OSAL_s32MessageQueueCreate(MQ_RTC_MP_RESPONSE_NAME,
            MQ_RTC_MP_CONTROL_MAX_MSG, MQ_RTC_MP_CONTROL_MAX_LEN,
            OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR)
   {
      if (OSAL_s32MessageQueueOpen(MQ_RTC_MP_RESPONSE_NAME,
               OSAL_EN_READONLY, &mqHandleR) == OSAL_ERROR)
      {
         return 1000;
      }
   }

   prAtr.szAppName      = "/opt/bosch/processes/procrtcp1_out.out";
   prAtr.szName         = "procrtcp1_out.out";
   prAtr.u32Priority    = PRIORITY;
   
   prID_1 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_1)
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "proc1 spawn failed: %lu",
      OSAL_u32ErrorCode());
      return 1001;
   }

   prAtr.szAppName      = "/opt/bosch/processes/procrtcp2_out.out";
   prAtr.szName         = "procrtcp2_out.out";
   prAtr.u32Priority    = PRIORITY;
   prID_2 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_2)
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "proc2 spawn failed: %lu",
      OSAL_u32ErrorCode());
      return 1002;
   }

   // Wait for processes to respond

   u32Ret += GetProcessResponse(mqHandleR);
   u32Ret += GetProcessResponse(mqHandleR);

   return u32Ret;
}
#endif

