/******************************************************************************
| FILE:         oedt_RegistryTest.cpp
| PROJECT:      GMGE
| SW-COMPONENT: OEDT_FrmWrk
|------------------------------------------------------------------------------
| DESCRIPTION:  This file contains part of the implementation for the OEDT 
|               registry test. Especially the exported function for the test 
|				framework
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          |                         | Author   & comments
| --.--.--      | Initial revision        | -------, -----
| 30 May 2011   |  version 1.0            | Martin Langer, initial version
| 17 Feb 2015   |  version 1.1            | Chandra Sekaran Mariappan (RBEI/ECF5)
|               |                         | Modified the functions 
|               |                         | u32RegBlockCreateAndRemove() and 
|               |                         | u32RegUseInvalidKey() for lint fix  
|------------------------------------------------------------------------------
*******************************************************************************/

/******************************************************************************
 * Includes                                                                   *
 ******************************************************************************/
#include "oedt_RegistryTest.h"
#include "oedt_helper_funcs.h"

/******************************************************************************
 * Forward declarations                                                       *
 ******************************************************************************/

 /******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Enums                                                                      *
 ******************************************************************************/

/******************************************************************************
 *                                                                            *
 *                      Registry Block Helper Funcs                           *
 *                                                                            *
 ******************************************************************************/
tBool bRemoveSoftwareBlock(const tChar* szKeyName)
{
    tS8 as8Name[256];
    OSAL_trIOCtrlDir      rDir;
    OSAL_trIOCtrlDir      rDirV;
    OSAL_tIODescriptor    fd;
    OSAL_trIOCtrlRegistry rReg;

    fd = OSAL_IOOpen(szKeyName, OSAL_EN_READWRITE);

    if (fd != OSAL_ERROR)
    {
        rDir.fd = fd;
        rDir.s32Cookie = 0;

        // dive recursive in the leafes
        while (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGREADDIR, (tS32)&rDir) != OSAL_ERROR)
        {
            rDir.s32Cookie = 0;
            OSAL_szStringCopy(   (tString)as8Name, szKeyName);
            OSAL_szStringConcat( (tString)as8Name, "/");
            OSAL_szStringConcat( (tString)as8Name, (tString)rDir.dirent.s8Name);

            if (!bRemoveSoftwareBlock( (tString)as8Name))
            {
                return FALSE;
            }
        }

        rDirV.fd = fd;
        rDirV.s32Cookie = 0;

        while (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGENUMVALUE, (tS32)&rDirV) != OSAL_ERROR)
        {
            rReg.pcos8Name = rDirV.dirent.s8Name;
            rDirV.s32Cookie = 0;
            if (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&rReg) != OSAL_OK)
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL,"bRemoveSoftwareBlock, can't remove Value: '%s', error code '%i'", rReg.pcos8Name, OSAL_u32ErrorCode());
            }
            else
            {
                OEDT_HelperPrintf(TR_LEVEL_USER_4,"bRemoveSoftwareBlock, Removed Value: '%s'", rReg.pcos8Name);			
            }
        }

        if (OSAL_s32IOClose(fd) != OSAL_OK)
        {
          OEDT_HelperPrintf(TR_LEVEL_FATAL,"bRemoveSoftwareBlock, can't close handle, error code '%i'", OSAL_u32ErrorCode());
        }

        if (OSAL_s32IORemove(szKeyName) != OSAL_OK)
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL,"bRemoveSoftwareBlock, can't remove Key: '%s', error code '%i'", szKeyName, OSAL_u32ErrorCode());
            return FALSE;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_4,"bRemoveSoftwareBlock, Removed Key: '%s'", szKeyName);			
        }

    }

    return TRUE;
}
  
/******************************************************************************
 *                                                                            *
 *                      Registry Block Writing Test                           *
 *                                                                            *
 ******************************************************************************/
/*****************************************************************************
 * FUNCTION:     u32RegBlockCreateAndRemove(void)
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * DESCRIPTION:  block writing to the registry and removing whole block afterwards
 * HISTORY:      Initial Version
 *               17-Feb-2015 v1.0 - Chandra Sekaran Mariappan (RBEI/ECF5)
 *               Removed lint and compiler warnings
 ******************************************************************************/   
tU32 u32RegBlockCreateAndRemove(void)
{
	OSAL_tIODescriptor hRegDev = 0;
	OSAL_tIODescriptor hRegKey = 0;
	OSAL_tIODescriptor hRegKeySub1 = 0;
	OSAL_tIODescriptor hRegKeySub2 = 0;
    OSAL_trIOCtrlRegistry hReg;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tS8 as8RegKeyBasePath [256];
	tS8 as8RegKeyPath [256];
	tU32 u32Ret = 0;
	tU8 u8NumData= 42;	
	/* The variables au8Datavalue1, au8Datavalue2 and au8Datavalue3 are declared as local */
    /* Initialise the variables before usage	*/
	tU8 au8Datavalue1[MAXVALUE] = "WriteCheck1";
	tU8 au8Datavalue2[MAXVALUE] = "WriteCheck2";
	tU8 au8Datavalue3[MAXVALUE] = "WriteCheck3";

	OSAL_szStringCopy(   (tString)as8RegKeyBasePath, "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/blocktest");
	OSAL_szStringCopy(   (tString)as8RegKeyPath, as8RegKeyBasePath);

	hRegDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDev) {
		return 1;
	}
	
	/* Create a Key */		
  	hRegKey = OSAL_IOCreate((tChar*)as8RegKeyBasePath, enAccess);
	if(OSAL_ERROR == hRegKey) { 
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_IOCreate() failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 2; 
	}

	hReg.pcos8Name =  (tPCS8) "my_value_string";
	hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
	hReg.ps8Value  =  au8Datavalue1;
	hReg.u32Size   =  11;
	if (OSAL_ERROR == OSAL_s32IOControl( hRegKey, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg)) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 4;
  }
	
	hReg.pcos8Name =  (tPCS8) "my_value_s32";
	hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
	hReg.ps8Value  =  &u8NumData;
	hReg.u32Size   =  2;
	if (OSAL_ERROR == OSAL_s32IOControl( hRegKey, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg)) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 8;
  }

	/* Create a SubKey */		
    OSAL_szStringConcat( (tString)as8RegKeyPath, "/subdirtest");
	hRegKeySub1 = OSAL_IOCreate((tChar*)as8RegKeyPath, enAccess);
	if(OSAL_ERROR == hRegKeySub1) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_IOCreate() failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 16;
	}
	
	hReg.pcos8Name =  (tPCS8) "my_subvalue_string";
	hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
	hReg.ps8Value  =  au8Datavalue2;
	hReg.u32Size   =  11;
	if (OSAL_ERROR == OSAL_s32IOControl( hRegKeySub1, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg)) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 32;
  }

	hReg.pcos8Name =  (tPCS8) "my_subvalue_s32";
	hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
	hReg.ps8Value  =  &u8NumData;
	hReg.u32Size   =  2;
	if (OSAL_ERROR == OSAL_s32IOControl( hRegKeySub1, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg)) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 64;
  }

	/* Create a SubSubKey */		
    OSAL_szStringConcat( (tString)as8RegKeyPath, "/subsubdirdata");
	hRegKeySub2 = OSAL_IOCreate((tChar*)as8RegKeyPath, enAccess);
	if(OSAL_ERROR == hRegKeySub2) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_IOCreate() failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 128;
	}

	hReg.pcos8Name =  (tPCS8) "my_subsubvalue_string";
	hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
	hReg.ps8Value  =  au8Datavalue3;
	hReg.u32Size   =  11;
	if (OSAL_ERROR == OSAL_s32IOControl( hRegKeySub2, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg)) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 256;
  }

  if(hRegKeySub2 != OSAL_ERROR){
    if(OSAL_ERROR  == OSAL_s32IOClose (hRegKeySub2)){
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOClose(hRegKeySub2) failed with error code '%i'", OSAL_u32ErrorCode());
      u32Ret += 512;
    }
  }else{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"hRegKeySub2 is broken");
    u32Ret += 1024;
  }

  if(hRegKeySub1 != OSAL_ERROR){
    if(OSAL_ERROR  == OSAL_s32IOClose (hRegKeySub1)){
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOClose(hRegKeySub1) failed with error code '%i'", OSAL_u32ErrorCode());
      u32Ret += 2048;
    }
  }else{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"hRegKeySub1 is broken");
    u32Ret += 4096;
  }
        
  if(hRegKey != OSAL_ERROR){
    if(OSAL_ERROR  == OSAL_s32IOClose (hRegKey)){
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOClose(hRegKey) failed with error code '%i'", OSAL_u32ErrorCode());
      u32Ret += 10000;
    }
  }else{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"hRegKey is broken");
    u32Ret += 20000;
  }
        
	if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDev ) )	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"OSAL_s32IOClose(hRegDev) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 40000;
	}
		
	if (bRemoveSoftwareBlock((tChar*)as8RegKeyBasePath) == FALSE) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"Subfunction bRemoveSoftwareBlock() failed");
		u32Ret += 80000;
	}
	
	return u32Ret;
}


/******************************************************************************
 *                                                                            *
 *                   Writing And Removing with Invalid Key                    *
 *                                                                            *
 ******************************************************************************/
/*****************************************************************************
 * FUNCTION:     u32RegUseInvalidKey(void)
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * DESCRIPTION:  writing and removing to the registry with an invalid key
 * HISTORY:      Initial Version
 *               17-Feb-2015 v1.0 - Chandra Sekaran Mariappan (RBEI/ECF5)
 *               Removed lint and compiler warnings
 ******************************************************************************/   

tU32 u32RegUseInvalidKey(void)
{
	OSAL_tIODescriptor hRegDev = 0;
	OSAL_tIODescriptor hRegKey = 0;
	OSAL_tIODescriptor hRegKey2 = 0;
	OSAL_trIOCtrlRegistry hReg;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tS8 as8RegKeyBasePath [256];
	tS8 as8RegKeyPath [256];
	tU32 u32Ret = 0;
    tU32 errCode = 0;
	/* The variable au8Datavalue1 is declared as local */
    /* Initialise the variable before usage */
	tU8 au8Datavalue1[MAXVALUE] = "WriteCheck1";
  
	OSAL_szStringCopy(   (tString)as8RegKeyBasePath, "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/blocktest");
	OSAL_szStringCopy(   (tString)as8RegKeyPath, as8RegKeyBasePath);

	hReg.pcos8Name =  (tPCS8) "my_value_string";
	hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
	hReg.ps8Value  =  au8Datavalue1;
	hReg.u32Size   =  11;

	hRegDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDev) {
		return 1;
	}
	
	// Create a Key
 	hRegKey = OSAL_IOCreate((tChar*)as8RegKeyBasePath, enAccess);
	if( hRegKey == OSAL_ERROR ) { 
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_IOCreate() failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 2; 
	}

	if ( OSAL_s32IOControl( hRegKey, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg) == OSAL_ERROR ) {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOControl(REGSET) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret += 4;
  }
	
  if (OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOControl(REGREMOVE) can't remove value: '%s', error code '%i'", hReg.pcos8Name, OSAL_u32ErrorCode());
		u32Ret += 8;     
  }
  
  if (OSAL_s32IORemove((tChar*)as8RegKeyBasePath) == OSAL_OK)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: Oopps, removing Key: '%s' wasn't expected", (tChar*)as8RegKeyBasePath);
    u32Ret += 16;
  }
  
	// Try to create the same Key again
 	hRegKey2 = OSAL_IOCreate((tChar*)as8RegKeyBasePath, enAccess);
	if(hRegKey2 == OSAL_OK) { 
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_IOCreate() doesn't fail");
		u32Ret += 32; 
	} else {
    // use invalid hRegKey2
    if ( OSAL_s32IOControl( hRegKey2, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&hReg) == OSAL_OK ) {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOControl(REGSET) doesn't fail");
      u32Ret += 100;
    } else {
      errCode = OSAL_u32ErrorCode();
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32RegCreateAndRemoveBlock: got error code '%i' from OSAL_s32IOControl(REGSET)", errCode);
      if ( (errCode != (errCode|OSAL_C_COMPONENT|OSAL_C_ERROR_TYPE)) || ((tS32)errCode == OSAL_ERROR) )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: got wrong error code '%i' from OSAL_s32IOControl(REGSET)", errCode);
        u32Ret += 200;
      }  
    }
	
    // use invalid hRegKey2
    if ( OSAL_s32IOControl(hRegKey2, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_OK )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOControl(REGREMOVE) doesn't fail removing '%s'", hReg.pcos8Name);
      u32Ret += 400;     
    } else {
      errCode = OSAL_u32ErrorCode();
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32RegCreateAndRemoveBlock: got error code '%i' from OSAL_s32IOControl(REGREMOVE)", errCode);
      if ( (errCode != (errCode|OSAL_C_COMPONENT|OSAL_C_ERROR_TYPE)) || ((tS32)errCode == OSAL_ERROR) )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: got wrong error code '%i' from OSAL_s32IOControl(REGREMOVE)", errCode);
        u32Ret += 1000;
      }  
    }
    
    // use invalid hRegKey2
    if(OSAL_s32IOClose ( hRegKey2 ) == OSAL_OK)	{
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOClose(hRegKey2) doesn't fail");
      u32Ret +=2000;
    }
  }
  
	if(OSAL_ERROR  == OSAL_s32IOClose ( hRegKey ) )	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOClose(hRegKey) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret +=4000;
	}
  
	if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDev ) )	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: OSAL_s32IOClose(hRegDev) failed with error code '%i'", OSAL_u32ErrorCode());
		u32Ret +=10000;
	}
  
  if (OSAL_s32IORemove((tChar*)as8RegKeyBasePath) != OSAL_OK)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"u32RegCreateAndRemoveBlock: can't remove Key: '%s', error code '%i'", (tChar*)as8RegKeyBasePath, OSAL_u32ErrorCode());
    u32Ret += 20000;
  }
  
  return u32Ret;
}

/************************************************************************
|end of file oedt_RegistryTest.cpp
|-----------------------------------------------------------------------*/
