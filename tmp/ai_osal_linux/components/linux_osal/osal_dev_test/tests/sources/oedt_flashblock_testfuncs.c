/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        oedt_flashblock_TestFuncs.c
 *
 *  O(E)DT test for osal driver dev_flashblock
 *
**********************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

#include  "dev_flashblock.h"
#include  "oedt_flashblock_testfuncs.h"

/* TODO delete, if interface complete available*/
#define OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD    "/dev/flashblock_ffd"     
#define OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS    "/dev/flashblock_kds"   


static   tPS8    vpBufferOrgDataFFD;
static   tPS8    vpBufferOrgDataKDS;
/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tU32 OEDT_u32FlashblockWriteRead(tCString szName);
static tU32 OEDT_u32FlashblockGetSize(tCString szName);
static tU32 OEDT_u32FlashblockErase(tCString szName);
static tU32 OEDT_u32FlashblockSaveActualData(tCString szName);
static tU32 OEDT_u32FlashblockRestoreActualData(tCString szName);


/*****************************************************************************
* FUNCTION:		u32FlashblockOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open close flashblock
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockOpenClose(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*open the driver for ffd */
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=1;   
  }
  else
  { /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status=2;
    }
  }
  /*open the driver for kds */
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status|=4;  
  }
  else
  { /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status|=8;
    }
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockOpenWithInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockOpenWithInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*open the driver for ffd */
  VhDevice = OSAL_IOOpen(NULL,OSAL_EN_READWRITE);
  if(VhDevice != OSAL_ERROR)
  {
    Vs32Status=1;
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockCloseWithInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  close with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockCloseWithInvalidParam(void)
{
  tS32                    Vs32Status = 0;

  /*close with invalid parameter*/
  if(OSAL_ERROR != OSAL_s32IOClose(0))
  {
    Vs32Status |=2;
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockOpenCloseSeveralTimes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open close several times
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockOpenCloseSeveralTimes(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_tIODescriptor      VhDeviceKDS = OSAL_ERROR;

  /*open the driver for ffd */
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vs32Status=0x01;
  }
  /*open the driver for kds */
  VhDeviceKDS = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS,OSAL_EN_READWRITE);
  if(VhDeviceKDS == OSAL_ERROR)
  {
    Vs32Status|=0x02;
  }
  /*open again FFD*/
  if(OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE)!=OSAL_ERROR)
  {
    Vs32Status|=0x04;
  }
  /*open again KDS*/
  if(OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS,OSAL_EN_READWRITE)!=OSAL_ERROR)
  {
    Vs32Status|=0x08;
  }
  /*close the device ffd*/
  if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD))
  {
    Vs32Status|=0x10;
  }
  /*close the device ffd*/
  if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceKDS))
  {
    Vs32Status|=0x20;
  }
  /*close the device ffd again*/
  if(OSAL_ERROR !=   OSAL_s32IOClose(VhDeviceFFD))
  {
    Vs32Status|=0x40;
  }
  /*close the device ffd again*/
  if(OSAL_ERROR !=   OSAL_s32IOClose(VhDeviceKDS))
  {
    Vs32Status|=0x80;
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockWriteRead_FFD()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  write read FFD data
* HISTORY:		Created Andrea B�ter (TMS)  04.07.2010 
******************************************************************************/
tU32 u32FlashblockWriteRead_FFD(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockWriteRead(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockWriteRead_KDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  write read KDS data
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockWriteRead_KDS(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockWriteRead(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS);
  return(Vs32Status);
}

/*****************************************************************************
* FUNCTION:		u32FlashblockWriteReadInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  read write data with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockWriteReadInvalidParam(void)
{
  tS32                    Vs32Status= 0;
  tU32                    Vu32Size=100;
  tPS8                    VpBuffer=OSAL_pvMemoryAllocate(Vu32Size);
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  if(Vs32Status!=0)
  {
    /*open the driver  */
    VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /*write data*/
      if(OSAL_s32IOWrite(0,(tPCS8)VpBuffer,Vu32Size)!=OSAL_ERROR)
      {
        Vs32Status |= 2;
      }
      if(OSAL_s32IOWrite(VhDevice,NULL,Vu32Size)!=OSAL_ERROR)
      {
        Vs32Status |= 3;
      }
      if(OSAL_s32IOWrite(VhDevice,VpBuffer,0xffff)!=OSAL_ERROR)
      {
        Vs32Status |= 4;
      }
      /* read  data */
      if(OSAL_s32IORead(0,VpBuffer,Vu32Size)==OSAL_ERROR)
      {
        Vs32Status|=5;
      }
      if(OSAL_s32IORead(VhDevice,NULL,Vu32Size)==OSAL_ERROR)
      {
        Vs32Status|=6;
      }
      if(OSAL_s32IORead(VhDevice,VpBuffer,0xffff)==OSAL_ERROR)
      {
        Vs32Status|=7;
      }
      /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=8;
      }
    }
    /*restore data*/
    Vs32Status=OEDT_u32FlashblockRestoreActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  }
  /* memory free */
  OSAL_vMemoryFree(VpBuffer);
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockWriteReadIfClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  write read data if driver close
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockWriteReadIfClose(void)
{
  tS32                    Vs32Status= 0;
  tU32                    Vu32Size=100;
  tPS8                    VpBuffer=OSAL_pvMemoryAllocate(Vu32Size);
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  if(Vs32Status!=0)
  {
    /*open the driver  */
    VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=2;
      }
      /*write data*/
      if(OSAL_s32IOWrite(VhDevice,(tPCS8)VpBuffer,Vu32Size)!=OSAL_ERROR)
      {
        Vs32Status |= 4;
      }
      /* read  data */
      if(OSAL_s32IORead(VhDevice,VpBuffer,Vu32Size)==OSAL_ERROR)
      {
        Vs32Status|=8;
      }
    }
    /*restore data*/
    Vs32Status=OEDT_u32FlashblockRestoreActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  }
  /* memory free */
  OSAL_vMemoryFree(VpBuffer);
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockGetSizeData_FFD()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get size if das for FFD
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockGetSizeData_FFD(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockGetSize(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockGetSizeData_KDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get size if data for KDS
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockGetSizeData_KDS(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockGetSize(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS);
  return(Vs32Status);
}

/*****************************************************************************
* FUNCTION:		u32FlashblockGetSizeDataInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get data size with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockGetSizeDataInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;
  /*open the driver  */
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=1;
  }
  else
  { /* get data size*/
    if(OSAL_s32IOControl(0,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)!=OSAL_ERROR)
    {
      Vs32Status=2;
    }
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA+100,(tS32)&Vu32Size)!=OSAL_ERROR)
    {
      Vs32Status|=3;
    }
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)NULL)!=OSAL_ERROR)
    {
      Vs32Status|=4;
    }
    /* close driver*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status|=5;
    }
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockGetSizeIfClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get data  size if driver close
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockGetSizeIfClose(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;
  /*open the driver  */
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=1;
  }
  else
  { /* close driver*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status=2;
    }
    /* get data size*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)!=OSAL_ERROR)
    {
      Vs32Status|=3;
    }
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockErase_FFD()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  erase FFD 
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockErase_FFD(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockErase(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockErase_KDS()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  erase KDS
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockErase_KDS(void)
{
  tS32    Vs32Status = 0;
  Vs32Status=OEDT_u32FlashblockErase(OSAL_C_STRING_DEVICE_FLASHBLOCK_KDS);
  return(Vs32Status);
}

/*****************************************************************************
* FUNCTION:		u32FlashblockEraseInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  erase block with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockEraseInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  if(Vs32Status!=0)
  {/*open the driver  */
    VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /* erase flash*/
      if(OSAL_s32IOControl(0,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_ERASE,0)!=OSAL_ERROR)
      {
        Vs32Status=2;
      }
      /* erase flash*/
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_ERASE+100,0)!=OSAL_ERROR)
      {
        Vs32Status|=3;
      }
      /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=5;
      }
    }
    Vs32Status=OEDT_u32FlashblockRestoreActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		u32FlashblockEraseIfClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  erase block if close
* HISTORY:		Created Andrea B�ter (TMS)  05.07.2010 
******************************************************************************/
tU32 u32FlashblockEraseIfClose(void)
{ 
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  if(Vs32Status!=0)
  {
    /*open the driver  */
    VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status=2;
      }
      /* erase flash*/
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_ERASE,0)!=OSAL_ERROR)
      {
        Vs32Status|=3;
      }
    }
    Vs32Status=OEDT_u32FlashblockRestoreActualData(OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD);
  }
  return(Vs32Status);
}

/*------------------------------------------------------------------------------*/
/* private function                                                             */
/*------------------------------------------------------------------------------*/
/*****************************************************************************
* FUNCTION:		static tU32 OEDT_u32FlashblockWriteRead(tCString szName)
* PARAMETER:    szName: Name of the device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  read and write data to the dev_flashblock
* HISTORY:		Created Andrea B�ter (TMS)  06.07.2010 
******************************************************************************/
static tU32 OEDT_u32FlashblockWriteRead(tCString szName)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(szName);
  if(Vs32Status!=0)
  {/*open the driver  */
    VhDevice = OSAL_IOOpen(szName,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /* get data size*/
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)==OSAL_ERROR)
      {
        Vs32Status=2;
      }
      else
      {
        tPS8  VpBufferNewDataWrite=OSAL_pvMemoryAllocate(Vu32Size);
        tPS8  VpBufferNewDataRead=OSAL_pvMemoryAllocate(Vu32Size);
        if((VpBufferNewDataRead==NULL)||(VpBufferNewDataWrite==NULL))
        {
          Vs32Status=4;
        }
        else
        {   /* write new data */
          memset(VpBufferNewDataWrite,0x55,Vu32Size);
          if(OSAL_s32IOWrite(VhDevice,VpBufferNewDataWrite,Vu32Size)==OSAL_ERROR)
          {
            Vs32Status = 8;
          }
          /* read new data */
          if(OSAL_s32IORead(VhDevice,VpBufferNewDataRead,Vu32Size)==OSAL_ERROR)
          {
            Vs32Status|=10;
          }
          /* compare data*/
          if(memcmp(VpBufferNewDataRead,VpBufferNewDataWrite,Vu32Size)!=0)
          {
            Vs32Status|=20;
          }
        }
        OSAL_vMemoryFree(VpBufferNewDataWrite);
        OSAL_vMemoryFree(VpBufferNewDataRead);
      }                
      /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=40;
      }
    }
    Vs32Status=OEDT_u32FlashblockRestoreActualData(szName);
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		static tU32 OEDT_u32FlashblockGetSize(tCString szName)
* PARAMETER:    szName: Name of the device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get size if data
* HISTORY:		Created Andrea B�ter (TMS)  06.07.2010 
******************************************************************************/
static tU32 OEDT_u32FlashblockGetSize(tCString szName)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;
  /*open the driver  */
  VhDevice = OSAL_IOOpen(szName,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=1;
  }
  else
  { /* get data size*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)==OSAL_ERROR)
    {
      Vs32Status=2;
    }
    /* close driver*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status|=3;
    }
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		static tU32 OEDT_u32FlashblockErase(tCString szName)
* PARAMETER:    szName: Name of the device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  erase flash
* HISTORY:		Created Andrea B�ter (TMS)  06.07.2010 
******************************************************************************/
static tU32 OEDT_u32FlashblockErase(tCString szName)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;

  /*save original data*/
  Vs32Status=OEDT_u32FlashblockSaveActualData(szName);
  if(Vs32Status!=0)
  {
    /*open the driver  */
    VhDevice = OSAL_IOOpen(szName,OSAL_EN_READWRITE);
    if(VhDevice == OSAL_ERROR)
    {
      Vs32Status=1;
    }
    else
    { /* erase flash*/
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_ERASE,0)==OSAL_ERROR)
      {
        Vs32Status=2;
      }
      /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=3;
      }
    }
    Vs32Status=OEDT_u32FlashblockRestoreActualData(szName);
  }
  return(Vs32Status);
}

/*****************************************************************************
* FUNCTION:		static tU32 OEDT_u32FlashblockSaveActualData(tCString szName)
* PARAMETER:    szName: Name of the device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  save actual data
* HISTORY:		Created Andrea B�ter (TMS)  06.07.2010 
******************************************************************************/
static tU32 OEDT_u32FlashblockSaveActualData(tCString szName)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;
  tPS8                    VpBufferOrgData;

  if(szName==OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD)
  {
    VpBufferOrgData=vpBufferOrgDataFFD;
  }
  else
  {
    VpBufferOrgData=vpBufferOrgDataKDS;
  }

  /*open the driver  */
  VhDevice = OSAL_IOOpen(szName,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=1000;
  }
  else
  { /* get data size*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)==OSAL_ERROR)
    {
      Vs32Status=2000;
    }
    else
    {/* allocate buffer*/
      VpBufferOrgData = OSAL_pvMemoryAllocate(Vu32Size);
      if(VpBufferOrgData==NULL)
      {
        Vs32Status=3000;
      }
      else
      {/* read actula data and save data into buffer*/
        if(OSAL_s32IORead(VhDevice,VpBufferOrgData,Vu32Size)==OSAL_ERROR)
        {
          Vs32Status=4000;
        }
      }
    }
    /* close driver*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
    {
      Vs32Status|=5000;
    }
  }
  return(Vs32Status);
}
/*****************************************************************************
* FUNCTION:		static tU32 OEDT_u32FlashblockSaveActualData(tCString szName)
* PARAMETER:    szName: Name of the device
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  save actual data
* HISTORY:		Created Andrea B�ter (TMS)  06.07.2010 
******************************************************************************/
static tU32 OEDT_u32FlashblockRestoreActualData(tCString szName)
{
  tS32                    Vs32Status = 0;
  OSAL_tIODescriptor      VhDevice = OSAL_ERROR;
  tU32                    Vu32Size;
  tPS8                    VpBufferOrgData;

  if(szName==OSAL_C_STRING_DEVICE_FLASHBLOCK_FFD)
  {
    VpBufferOrgData=vpBufferOrgDataFFD;
  }
  else
  {
    VpBufferOrgData=vpBufferOrgDataKDS;
  }
  /*open the driver  */
  VhDevice = OSAL_IOOpen(szName,OSAL_EN_READWRITE);
  if(VhDevice == OSAL_ERROR)
  {
    Vs32Status=6000;
  }
  else
  { /* get data size*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_DEV_FLASHBLOCK_GET_SIZE_DATA,(tS32)&Vu32Size)==OSAL_ERROR)
    {
      Vs32Status=7000;
    }
    else
    {
      tPS8  VpBufferNewDataRead=OSAL_pvMemoryAllocate(Vu32Size);
      /* restore data*/
      if(OSAL_s32IOWrite(VhDevice,VpBufferOrgData,Vu32Size)==OSAL_ERROR)
      {
        Vs32Status=8000;
      }
      if(VpBufferNewDataRead==NULL)
      {/*read data */        
        if(OSAL_s32IORead(VhDevice,VpBufferNewDataRead,Vu32Size)==OSAL_ERROR)
        {
          Vs32Status|=9000;
        }
        /* compare data*/
        if(memcmp(VpBufferNewDataRead,VpBufferOrgData,Vu32Size)!=0)
        {
          Vs32Status|=10000;
        }
      }
      /* deallocate buffer*/
      OSAL_vMemoryFree(VpBufferOrgData);
      /* close driver*/
      if(OSAL_ERROR ==   OSAL_s32IOClose(VhDevice))
      {
        Vs32Status|=11000;
      }
    }    
  }
  return(Vs32Status);
}
