/******************************************************************************
 * FILE				:	oedt_CDROM_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT	:	OEDT_FrmWrk 
 *
 * DESCRIPTION		:	This file implements the file system test cases for the CDROM  
 *                          
 * AUTHOR(s)		:	Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			|							|	Author & comments
 * --.--.--			|	Initial revision		|	------------
 * 01 Dec, 2008		|	version 1.0				 |	Sriranjan U (RBEI/ECM1)
 * 01 Dec, 2008		|	version 1.1				 |	anc3kor
 *							|	TU_OEDT_CDROM_CFS_004 |
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010     | version 1.2	    	    |  Updated the Device name 
 *				    |						    |  Anoop Chandran (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 02 June, 2010   | version 1.3     |  Added new OEDT u32CDROM_CommonFSCopyDirToFFS1()
 *                |                  |  Anoop Chandran
 * ------------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_CDROM_CommonFS_TestFuncs.h"

/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpenClosedevice( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_001
* DESCRIPTION	:	Opens and closes CDROM device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSOpenClosedevice(tVoid)
{ 
	return u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpendevInvalParm( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_002
* DESCRIPTION	:	Try to Open CDROM device with invalid parameters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32CDROM_CommonFSOpendevInvalParm(tVoid)
{
	return u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSReOpendev( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_003
* DESCRIPTION	:	Try to Re-Open CDROM device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32CDROM_CommonFSReOpendev(tVoid)
{
	return u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM); 
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpendevDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_004
* DESCRIPTION	:	Try to Re-Open CDROM device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*						update the error handling, anc3kor,14-04-2009
******************************************************************************/
tU32 u32CDROM_CommonFSOpendevDiffModes(tVoid)
{
	tU32 u32Ret = 0;
	u32Ret = u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM);
	if(u32Ret == 20)
	{
		u32Ret = 0;
	}
	return u32Ret ;
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSClosedevAlreadyClosed( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_005
* DESCRIPTION	:	Try to Re-Open CDROM device
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CDROM_CommonFSClosedevAlreadyClosed(tVoid)
{
	return u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpenClosedir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_006
* DESCRIPTION	:	Try to Open and closes a directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CDROM_CommonFSOpenClosedir(tVoid)
{
	return u32FSOpenClosedir((const tPS8)OEDT_C_STRING_CDROM_DIR1);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpendirInvalid( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_007
* DESCRIPTION	:	Try to Open invalid directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CDROM_CommonFSOpendirInvalid(tVoid)
{
	tPS8 dev_name[2] = {
					(tPS8)OEDT_C_STRING_CDROM_NONEXST,
					(tPS8)OEDT_C_STRING_CDROM_DIR1
					};
	return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSCreateDelDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_008
* DESCRIPTION	:	Try create and delete directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32CDROM_CommonFSCreateDelDir(tVoid)
{
	tU32 u32Ret = 0; 
	u32Ret = u32FSCreateDelDir((const tPS8)OEDT_C_STRING_DEVICE_CDROM_ROOT);
	// Dir Creation should fail
	if (u32Ret == 3 || u32Ret == 1)
	{
		u32Ret = 0;
	}
	return u32Ret; 
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSGetDirInfo( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_009
* DESCRIPTION	:	Get directory information
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSGetDirInfo(tVoid)
{
	return u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpenDirDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_010
* DESCRIPTION	:	Try to open directory in different modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSOpenDirDiffModes(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_CDROM,
							(tPS8)OEDT_C_STRING_CDROM_DIR1
							};
	return u32FSOpenDirDiffModes(dev_name, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSReOpenDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_011
* DESCRIPTION	:	Try to reopen directory
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSReOpenDir(tVoid)
{
	tPS8 dev_name[2] = {
							(tPS8)OEDT_C_STRING_DEVICE_CDROM_ROOT,
							(tPS8)OEDT_C_STRING_CDROM_DIR1
							};
	return u32FSReOpenDir(dev_name, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileCreateDel( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_012
* DESCRIPTION	:  Create/ Delete file  with correct parameters
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileCreateDel(tVoid)
{
	tU32 u32Ret = 0; 
	u32Ret = u32FSFileCreateDel((const tPS8)CDROM_TEXT_FILE);
	// File Creation should fail
	if (u32Ret == 1)
	{
		u32Ret = 0;
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_013
* DESCRIPTION	:	Open /close File
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileOpenClose(tVoid)
{
	return u32FSFileOpenClose((const tPS8)CDROM_TEXT_FILE, FALSE);
}	


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileOpenInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_014
* DESCRIPTION	:  Open file with invalid path name (should fail)
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileOpenInvalPath(tVoid)
{
	return u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_CDROM);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileOpenInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_015
* DESCRIPTION	:	Open a file with invalid parameters (should fail), 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileOpenInvalParam(tVoid)
{
	return u32FSFileOpenInvalParam((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReOpen( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_016
* DESCRIPTION	:  Try to open and close the file which is already opened
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32  u32CDROM_CommonFSFileReOpen(tVoid)
{
	return u32FSFileReOpen((const tPS8)CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_017
* DESCRIPTION	:	Read data from already exsisting file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileRead(tVoid)
{	
	return u32FSFileRead((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSGetPosFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_018
* DESCRIPTION	:  Get File Position from begining of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSGetPosFrmBOF(tVoid)
{
	return u32FSGetPosFrmBOF((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSGetPosFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_019
* DESCRIPTION	:	Get File Position from end of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSGetPosFrmEOF(tVoid)
{
	return u32FSGetPosFrmEOF((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_020
* DESCRIPTION	:	Read with a negative offset from BOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32   u32CDROM_CommonFSFileReadNegOffsetFrmBOF()
{
	return u32FSFileReadNegOffsetFrmBOF((const tPS8)CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_021
* DESCRIPTION	:	Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
	return u32FSFileReadOffsetBeyondEOF((const tPS8)CDROM_TEXT_FILE, FALSE );
}

/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_022
* DESCRIPTION	:  Read from offset from Beginning of file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileReadOffsetFrmBOF(tVoid)
{	
	return u32FSFileReadOffsetFrmBOF((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_023
* DESCRIPTION	:	Read file with few offsets from EOF
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileReadOffsetFrmEOF(tVoid)
{	
	return u32FSFileReadOffsetFrmEOF((const tPS8)CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_024
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmBOF((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_025
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*						(This is from EOF)		
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
	return u32FSFileReadEveryNthByteFrmEOF((const tPS8)CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSGetFileCRC( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_026
* DESCRIPTION	:	Get CRC value
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32CDROM_CommonFSGetFileCRC(tVoid)
{
	return u32FSGetFileCRC((const tPS8)CDROM_TEXT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_027
* DESCRIPTION	:	Read data asyncronously from a file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32CDROM_CommonFSReadAsync(tVoid)
{	
	return u32FSReadAsync((const tPS8)CDROM_TEXT_FILE, FALSE);

}

/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSLargeReadAsync()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_028
* DESCRIPTION	:	Read data asyncronously from a large file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSLargeReadAsync(tVoid)
{	
	return u32FSLargeReadAsync((const tPS8)CDROM_TEXT_FILE, FALSE);
}

	
/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileOpenCloseNonExstng()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_029
* DESCRIPTION	:	Open a non existing file 
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32CDROM_CommonFSFileOpenCloseNonExstng(tVoid)
{
	return u32FSFileOpenCloseNonExstng ((const tPS8)OEDTTEST_C_STRING_DEVICE_CDROM"/Dummy.txt");
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSSetFilePosDiffOff()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_030
* DESCRIPTION	:	Set file position to different offsets
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSSetFilePosDiffOff(tVoid)
{
	return u32FSSetFilePosDiffOff ((const tPS8)CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileOpenDiffModes()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_031
* DESCRIPTION	:	Create file with different access modes
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSFileOpenDiffModes(tVoid)
{
	tU32 u32Ret = 0; 
	u32Ret = u32FSFileOpenDiffModes ((const tPS8)CDROM_TEXT_FILE, FALSE);

	if (u32Ret == 50)
	{
		u32Ret = 0;
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileThroughPutFirstFile()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_032
* DESCRIPTION	:  Calculate throughput for first CDROM file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 03, 2008 
******************************************************************************/
tU32 u32CDROM_CommonFSFileThroughPutFirstFile (tVoid)
{
	return u32FSFileThroughPutFirstFile((const tPS8)CDROM_TEXT_FILE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileThroughPutSecondFile ()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_033
* DESCRIPTION	:  Calculate throughput for last CDROM file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 03, 2008 
******************************************************************************/
tU32 u32CDROM_CommonFSFileThroughPutSecondFile (tVoid)
{
	return u32FSFileThroughPutSecondFile((const tPS8)CDROM_TEXT_LAST);
}

/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSFileThroughPutMP3File()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_034
* DESCRIPTION	:  Calculate throughput for CDROM MP3 file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 03, 2008 
******************************************************************************/
tU32 u32CDROM_CommonFSFileThroughPutMP3File(tVoid)
{
	return u32FSFileThroughPutMP3File((const tPS8)CDROM_MP3_FILE);
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSLargeFileRead()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_035
* DESCRIPTION	:	Read large file
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSLargeFileRead(tVoid)
{	
	return u32FSLargeFileRead ((const tPS8) CDROM_TEXT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSOpenCloseMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_036
* DESCRIPTION	:	Open and close file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSOpenCloseMultiThread(tVoid)
{	
	return u32FSOpenCloseMultiThread ((const tPS8) CDROM_TEXT_FILE );
}


/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSReadMultiThread()
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_CDROM_CFS_037
* DESCRIPTION	:	Read from file from multiple threads
* HISTORY		:	Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32CDROM_CommonFSReadMultiThread(tVoid)
{	
	return u32FSReadMultiThread ((const tPS8) CDROM_TEXT_FILE );
}
/*****************************************************************************
* FUNCTION		:	u32CDROM_CommonFSCopyDirToFFS1( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_FFS1_CFS_038
* DESCRIPTION	:	Copy the files in source directory in FFS1 to 
                  destination directory in USBMS
* HISTORY		:	Created by anc2hi. 02 June,2010 
******************************************************************************/
tU32 u32CDROM_CommonFSCopyDirToFFS1(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
									(tPS8)OEDT_COPY_DIR_PATH,
									(tPS8)OSAL_C_STRING_DEVICE_FFS1
									};

  	u32Ret = u32FSCopyDirTest(dev_name,FALSE);
	return u32Ret;
}
/* EOF */

