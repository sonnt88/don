/*!
 * \file odt_drv_cdctrl_random_read.c
 *
 * \brief OEDT test for drv_cdctrl
 *
 * \author CM-DI/ESP Schepers
 *
 * \par COPYRIGHT (c) 2006 Blaupunkt GmbH
 *
 * \date 2006-08-23
 */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

/** basic trace function */
extern tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length);

#define REQUEST_MIN (0x08*2)           /* we want minimal 16kByte and max 48kByte per request */
#define REQUEST_MAX (0x18*2)
#define MAX_NR_REQUEST (REQUEST_MAX - REQUEST_MIN)   
#define MAX_NR_TESTS (1)           /* number of tests sequences */
#define MAX_NR_BLOCK (512+256)     /* 512*2K = 1 Megabyte, we want 1,5 Megabyte */
#define MAX_MB_CD    (600)         /* assume the maximum Megabytes on a CD */

/*
 * variable for complete read test being incremented after each read test
 * each time the complete read is called we do MAX_NR_TESTS turns of 10 sek read and return
 * to read a whole CD/DVD an endurance test has to be startet, each one starting at
 * incremented lba_static_start
 */
static tU32 lba_static_start = 0;

/*
 * variables commonly used in tests
 */
struct CdCtrlTestVar {
  char*              devicename;
  OSAL_tIODescriptor hDevice;
  OSAL_tMSecond      msT0, msT1, msDiff;
  OSAL_trReadRawInfo ReadInfo;
  OSAL_trDiskType    DiskType;
  OSAL_trDvdInfo     DvdInfo;
  OSAL_tenIOCtrlMediaDevice DeviceInfo;
  tU32               nr_blocks, nr_reads;
  char               trace_buffer[256];
  tU32               max_lba;
};

/*
 * struct for values from real route calculation as example and performance test
 * first phase ended by 0,0 (first calculation), finalised by second 0,0 (Routennachberechnung)
 */
struct CdCtrlAccessPattern {
  tU32 lba;
  tU32 nr_blk;
};

/*
 * access pattern for route from HI/460 to Center Stuttgart/Bad Cannstatt
 */
static struct CdCtrlAccessPattern hi_stgt[] =
  {
    {0x26ee8, 0x8},
    {0x5e178, 0x8},
    {0x135138, 0x8},
    {0x135558, 0x18},
    {0x141bd0, 0x18},
    {0x141b98, 0x10},
    {0x141bc8, 0x8},
    {0x141be8, 0x8},
    {0x121768, 0x8},
    {0x121770, 0x10},
    {0x121838, 0x8},
    {0x121840, 0x10},
    {0x121850, 0x8},
    {0x141af0, 0x18},
    {0x141b90, 0x8},
    {0x141ba8, 0x8},
    {0x141bf0, 0x10},
    {0x1aa970, 0x8},
    {0x1aa978, 0x8},
    {0x1218a8, 0x18},
    {0x121930, 0x18},
    {0x135540, 0x18},
    {0x1aa980, 0x1},
    {0x13cfc8, 0x8},
    {0x13d0b0, 0x10},
    {0x141cf0, 0x8},
    {0x121a70, 0x13},
    {0x141b40, 0x18},
    {0x141a50, 0x10},
    {0x1418e0, 0x8},
    {0x1418e8, 0x8},
    {0x1218e8, 0x10},
    {0x1218f8, 0x10},
    {0x121978, 0x18},
    {0x121a60, 0x10},
    {0x121960, 0x18},
    {0x121908, 0x18},
    {0x121990, 0x10},
    {0x121948, 0x10},
    {0x121958, 0x8},
    {0x1219b8, 0x18},
    {0x1219d0, 0x18},
    {0, 0},
    {0x78, 0x8},
    {0x80, 0x8},
    {0x88, 0x1},
    {0x1a58c0, 0x8},
    {0x1a83c0, 0x18},
    {0x1a83d8, 0x10},
    {0x1a83e8, 0x10},
    {0x1a8b08, 0x18},
    {0x1a8b20, 0x18},
    {0x1a8da8, 0x18},
    {0x1a8f38, 0x18},
    {0x1a8f50, 0x8},
    {0x141a50, 0x10},
    {0x1418e0, 0x10},
    {0x88, 0x8},
    {0x90, 0x8},
    {0x98, 0x8},
    {0xa0, 0x8},
    {0xa8, 0x8},
    {0xb0, 0x6},
    {0x6f7a0, 0x6},
    {0x6f338, 0x8},
    {0x13ce60, 0x8},
    {0x13ce78, 0x8},
    {0x6f2f0, 0x2},
    {0x141af0, 0x18},
    {0x6f340, 0x1},
    {0x141d50, 0x10},
    {0x6f340, 0x4},
    {0x141d88, 0x8},
    {0x13d0b0, 0x10},
    {0x141950, 0x18},
    {0x141b40, 0x18},
    {0x13ce68, 0x8},
    {0x141570, 0x8},
    {0x141588, 0x10},
    {0x13cd18, 0x8},
    {0x13cdf8, 0x10},
    {0x141850, 0x8},
    {0x141858, 0x8},
    {0x1218f0, 0x18},
    {0x6f0b8, 0x8},
    {0x1218e8, 0x8},
    {0x121908, 0x18},
    {0x121978, 0x18},
    {0x1219b8, 0x18},
    {0x1219d0, 0x18},
    {0x1219e8, 0x18},
    {0x121a48, 0x8},
    {0x121a50, 0x10},
    {0x121a60, 0x8},
    {0x121930, 0x18},
    {0x121948, 0x10},
    {0x121958, 0x10},
    {0x121968, 0x10},
    {0x121990, 0x10},
    {0x121870, 0x10},
    {0x121a68, 0x10},
    {0x121838, 0x8},
    {0x121840, 0x10},
    {0x121850, 0x8},
    {0x121880, 0x18},
    {0x121a78, 0xb},
    {0x70, 0x8},
    {0x135138, 0x8},
    {0x135140, 0x8},
    {0x1351a0, 0x18},
    {0x135430, 0x18},
    {0x6f280, 0x2},
    {0x134950, 0x8},
    {0x134cd0, 0x10},
    {0x134db8, 0x18},
    {0x6f268, 0x8},
    {0x6f278, 0x7},
    {0x135540, 0x18},
    {0x135558, 0x18},
    {0x1355a8, 0x18},
    {0x134a98, 0x10},
    {0x6f270, 0x8},
    {0x134a80, 0x18},
    {0x134958, 0x10},
    {0x134aa8, 0x10},
    {0x134c00, 0x10},
    {0x134c40, 0x10},
    {0x134d20, 0x10},
    {0x13a240, 0x8},
    {0x13a248, 0x8},
    {0x6f2c8, 0x8},
    {0x13a2f8, 0x8},
    {0x13a7c8, 0x10},
    {0x135570, 0x10},
    {0x141be0, 0x10},
    {0x141ba8, 0x18},
    {0x121900, 0x8},
    {0x121908, 0x10},
    {0x121918, 0x8},
    {0x6f0b8, 0x8},
    {0x121920, 0x18},
    {0x121998, 0x18},
    {0x1219b0, 0x10},
    {0x1219c8, 0x8},
    {0x1219d0, 0x10},
    {0x1219e0, 0x8},
    {0, 0},
    {0, 0}
};

struct FileList {
  char* filename;
  tU32 offset;
  tU32 size;
};

static struct FileList hi_stgt_files[] =
  {
    {"/dev/cd0/DATA/RNW/NAV02019.DAT",	225280,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2154496, 49152},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	780288,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	665600,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	763904,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	829440,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01776.DAT",	124928,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	6144,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	415744,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	432128,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	464896,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	321536,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	649216,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	698368,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	845824,	 32768},
    {"/dev/cd0/DATA/RNW/EXT01736.DAT",	4096,	 16384},
    {"/dev/cd0/DATA/RNW/EXT01777.DAT",	14336,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	645120,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	923648,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2105344, 49152},
    {"/dev/cd0/DATA/RNW/EXT01777.DAT",	30720,	  2048},
    {"/dev/cd0/DATA/RNW/NAV02105.DAT",	272384,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02106.DAT",	471040,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02160.DAT",	296960,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1579008, 38912},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	485376,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02158.DAT",	751616,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02157.DAT",	286720,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02158.DAT",	 14336,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	776192,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	808960,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1071104, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1546240, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1021952, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	841728,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1120256, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	972800,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1005568, 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1202176, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1251328, 49152},
    {NULL, 0, 0},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	0,	 16384},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	22544384, 49152},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	22593536, 32768},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	22626304, 32768},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	26361856, 49152},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	26411008, 49152},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	27738112, 49152},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	28557312, 49152},
    {"/dev/cd0/DATA/RNW/NAV_ROOT.DAT",	28606464, 16384},
    {"/dev/cd0/DATA/RNW/NAV02158.DAT",	751616,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02157.DAT",	286720,	 32768},
    {"/dev/cd0/DATA/RNW/AEX/AEX03024.DAT",	0,	 12288},
    {"/dev/cd0/DATA/RNW/AEX/AEX02150.DAT",	0,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02103.DAT",	667648,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02104.DAT",	36864,	 16384},
    {"/dev/cd0/DATA/RNW/AEX/AEX02103.DAT",	0,	 4096},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	321536,	 49152},
    {"/dev/cd0/DATA/RNW/AEX/AEX02159.DAT",	0,	 2048},
    {"/dev/cd0/DATA/RNW/NAV02161.DAT",	190464,	 32768},
    {"/dev/cd0/DATA/RNW/AEX/AEX02159.DAT",	0,	 8192},
    {"/dev/cd0/DATA/RNW/NAV02161.DAT",	305152,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02106.DAT",	471040,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02158.DAT",	227328,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	485376,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02104.DAT",	4096,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02155.DAT",	299008,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02156.DAT",	47104,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02101.DAT",	1638400, 16384},
    {"/dev/cd0/DATA/RNW/NAV02103.DAT",	454656,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02156.DAT",	1505280, 16384},
    {"/dev/cd0/DATA/RNW/NAV02157.DAT",	8192,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	792576,	 49152},
    {"/dev/cd0/DATA/RNW/AEX/AEX01767.DAT",	0,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	776192,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	841728,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1071104, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1202176, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1251328, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1300480, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1497088, 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1513472, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1546240, 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	923648,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	972800,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1005568, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1038336, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1120256, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	530432,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1562624, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	415744,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	432128,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	464896,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	563200,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1595392, 22528},
    {"/dev/cd0/DATA/RNW/NAV02019.DAT",	225280,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	8192,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	204800,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	1548288, 49152},
    {"/dev/cd0/DATA/RNW/AEX/AEX02020.DAT",	0,	  4096},
    {"/dev/cd0/DATA/RNW/NAV02016.DAT",	468992,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	1830912, 32768},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	2306048, 49152},
    {"/dev/cd0/DATA/RNW/AEX/AEX02013.DAT",	0,	 16384},
    {"/dev/cd0/DATA/RNW/AEX/AEX02017.DAT",	24576,	 14336},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2105344, 49152},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2154496, 49152},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2318336, 49152},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	667648,	 32768},
    {"/dev/cd0/DATA/RNW/AEX/AEX02017.DAT",	8192,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	618496,	 49152},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	12288,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	700416,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	1404928, 32768},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	1536000, 32768},
    {"/dev/cd0/DATA/RNW/NAV02017.DAT",	1994752, 32768},
    {"/dev/cd0/DATA/RNW/NAV02077.DAT",	380928,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02078.DAT",	12288,	 16384},
    {"/dev/cd0/DATA/RNW/AEX/AEX02063.DAT",	6144,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02078.DAT",	372736,	 16384},
    {"/dev/cd0/DATA/RNW/NAV02079.DAT",	2510848, 32768},
    {"/dev/cd0/DATA/RNW/NAV02020.DAT",	2203648, 32768},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	813056,	 32768},
    {"/dev/cd0/DATA/RNW/NAV02159.DAT",	698368,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	825344,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	841728,	 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	874496,	 16384},
    {"/dev/cd0/DATA/RNW/AEX/AEX01767.DAT",	0,	 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	890880,	 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1136640, 49152},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1185792, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1234944, 16384},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1251328, 32768},
    {"/dev/cd0/DATA/RNW/NAV01777.DAT",	1284096, 16384},
    {NULL, 0, 0},
    {NULL, 0, 0}
  };


enum {
  CD_CTRL_NO_ERR = 0,
  CD_CTRL_INITIALISE_ERR,
  CD_CTRL_CLEANUP_ERR,
  CD_CTRL_IO_CTRL_ERR,
  CD_CTRL_DATA_READ_ERR
};


static void ClearTraceBuffer( struct CdCtrlTestVar* vars )
{
  tU32 i;

  /*
   * clear trace bufffer
   */
  for( i=0; i<256; i++)
    {
      vars->trace_buffer[i] = 0;
    }
}


/*
 * common function to set up the test variables
 */
static tU32 InitialiseCdCtrlTest( struct CdCtrlTestVar* vars )
{

  ClearTraceBuffer(vars);
  
  vars->ReadInfo.ps8Buffer = OSAL_pvMemoryAllocate((REQUEST_MAX+1) * 1024);
  
  if ( vars->ReadInfo.ps8Buffer == OSAL_NULL )
    {
      /* no memory for readbuffer */
      return 1;
    }


  /*
   * real random will initialize the random generator with
   * clock time (which will be random depending upon test start)
   * otherwise a "reproducible" random generation is used
   */
#define REAL_RANDOM
#ifdef REAL_RANDOM
  OSAL_vRandomSeed( OSAL_ClockGetElapsedTime() );
#else
  OSAL_vRandomSeed( 220869 );
#endif
  
  vars->hDevice = OSAL_IOOpen( vars->devicename, OSAL_EN_READWRITE );
  if( vars->hDevice == OSAL_ERROR )
    {
      /* Device could not be opened. */
      return 2;
    }

  vars->max_lba = 0;


#if 0
   // hard coded values to speed up test
   vars->DvdInfo.u32StartSector = 196608;
   vars->DvdInfo.u32EndSector = 1681023;
   vars->max_lba = 1484415;
   vars->nr_blocks = 64;
   vars->nr_reads = 8256;
   return 0;
#endif


  /*
   * check if a dvd or cd drive is used
   */
  if ( OSAL_s32IOControl( vars->hDevice,
			  OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO,
			  (tS32) &vars->DeviceInfo )
       == OSAL_OK )
    {
      if (  OSAL_EN_MEDIADEV_CD == vars->DeviceInfo )
	{
	  vars->max_lba = (MAX_MB_CD * 1024)/2;
	}
      /*
       * get the disc type and calculate number of blocks accordingly
       * DVD: get the number of LBA from the disk
       */
      else if ( (OSAL_EN_MEDIADEV_DVD == vars->DeviceInfo ) &&
		(OSAL_s32IOControl( vars->hDevice,
				    OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE,
				    (tS32) &vars->DiskType )
		 == OSAL_OK ))
	{
	  switch( vars->DiskType.u8DiskType ) 
	    {
	    case ATAPI_C_U8_DISK_TYPE_DVD:
	      if ( OSAL_s32IOControl( vars->hDevice,
				      OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO,
				      (tS32) &vars->DvdInfo )
		   == OSAL_OK )
		{
		  vars->max_lba = vars->DvdInfo.u32EndSector - vars->DvdInfo.u32StartSector;
		}
	      else
		{
		  return 3;
		}
	      break;
	    case ATAPI_C_U8_DISK_TYPE_CD:
	    default:
	      vars->max_lba = (MAX_MB_CD * 1024)/2;
	      break;
	    }
	}
      else
	{
	  return 4;
	}
    }
  else
    {
      /*
       * cd drive type could not be recoginsed
       */
#if 0
      return 5;
#else
      /*
       * workaround:
       * due to a bug in cd ctrl for cd-drives the cd-drives
       * return OSAL_ERROR_NOT_SUPPORTED when asked for the drive
       * type. Assume it is a CD.
       */
      vars->max_lba = (MAX_MB_CD * 1024)/2;
#endif
    }

  if ( vars->max_lba == 0 )
    {
      return 6;
    }
  else 
    {
      return 0;
    }
}

/*
 * common cleanup functions
 */
static tU32 CleanupCdCtrlTest( struct CdCtrlTestVar* vars )
{
  OSAL_vMemoryFree(vars->ReadInfo.ps8Buffer);

  if( OSAL_s32IOClose( vars->hDevice ) != OSAL_OK )
    {
      /* Device could not be closed. */
      return 1;
    }
  
  return 0;
}

/*
 * random read test (somewhat) simulating route calculation
 */
static tU32 u32TestCdCtrlRandomRead(char* devicename )
{
  struct CdCtrlTestVar test;
  tS32 s32Ret;
  tU32 i;

  test.devicename = devicename;

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      return CD_CTRL_INITIALISE_ERR;
    }
  
  for( i=0; i<MAX_NR_TESTS; i++ )
    {
      test.nr_blocks = 0;
      test.nr_reads = 0;
      test.msT0 = OSAL_ClockGetElapsedTime();
      do
	{
	  test.ReadInfo.u32NumBlocks = ((((tU32)OSAL_s32Random()) % MAX_NR_REQUEST)+REQUEST_MIN)/2;
	  test.ReadInfo.u32LBAAddress = ((tU32) OSAL_s32Random()) % test.max_lba;
	  /*
	   * verify that we can read engough at the end of the disk without running over max_lba
	   */
	  if (test.ReadInfo.u32LBAAddress > (test.max_lba - test.ReadInfo.u32NumBlocks) )
	    {
	      test.ReadInfo.u32LBAAddress = test.max_lba - test.ReadInfo.u32NumBlocks;
	    }
	  
	  s32Ret = OSAL_s32IOControl( test.hDevice,
				      OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				      (tS32) &test.ReadInfo );
	  if ( OSAL_OK != s32Ret )
	    {
	      ClearTraceBuffer(&test);
	      sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		      test.ReadInfo.u32LBAAddress,
		      test.ReadInfo.u32NumBlocks,
		      test.max_lba);
	      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	      CleanupCdCtrlTest(&test);
	      return CD_CTRL_IO_CTRL_ERR;
	    }
	  test.nr_blocks += test.ReadInfo.u32NumBlocks;
	  test.nr_reads += 1;
	} 
      while ( test.nr_blocks <  MAX_NR_BLOCK ); 
      test.msT1 = OSAL_ClockGetElapsedTime();
      test.msDiff = test.msT1- test.msT0;
      ClearTraceBuffer(&test);
      sprintf(test.trace_buffer, "%d msec for %d 2K blocks with %d random reads = %d KByte/sek ", 
	      test.msDiff, 
	      test.nr_blocks, 
	      test.nr_reads, 
	      ((test.nr_blocks * 2 ) * 1000) / test.msDiff);
      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
      oedt_vSetExtInfo(test.trace_buffer); 
    }

  if ( CleanupCdCtrlTest( &test ) != 0 )
    {
      return CD_CTRL_CLEANUP_ERR;
    }
  
  return CD_CTRL_NO_ERR;
}

/*
 * random read test (somewhat) simulating route calculation
 */
static tU32 u32TestCdCtrlRandomReadWithVerify(char* devicename )
{
  struct CdCtrlTestVar test;
  tS32 s32Ret;
  tU32 i, j;
  tU32 test_result;

  OSAL_trReadRawInfo ReadInfo2;

  test_result = CD_CTRL_NO_ERR;

  ReadInfo2.ps8Buffer = OSAL_pvMemoryAllocate((REQUEST_MAX+1) * 1024);
  if ( ReadInfo2.ps8Buffer == OSAL_NULL )
    {
      /* no memory for readbuffer */
      return CD_CTRL_INITIALISE_ERR;
    }

  test.devicename = devicename;

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
      return CD_CTRL_INITIALISE_ERR;
    }
  
  for( i=0; i<MAX_NR_TESTS; i++ )
    {
      test.nr_blocks = 0;
      test.nr_reads = 0;
      test.msT0 = OSAL_ClockGetElapsedTime();


      /*
       * read a random block to verify later on
       */

      test.ReadInfo.u32NumBlocks = ((((tU32)OSAL_s32Random()) % MAX_NR_REQUEST)+REQUEST_MIN)/2;
      test.ReadInfo.u32LBAAddress = ((tU32) OSAL_s32Random()) % test.max_lba;
      /*
       * verify that we can read engough at the end of the disk without running over max_lba
       */
      if (test.ReadInfo.u32LBAAddress > (test.max_lba - test.ReadInfo.u32NumBlocks) )
	{
	  test.ReadInfo.u32LBAAddress = test.max_lba - test.ReadInfo.u32NumBlocks;
	}
      
      s32Ret = OSAL_s32IOControl( test.hDevice,
				  OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				  (tS32) &test.ReadInfo );
      if ( OSAL_OK != s32Ret )
	{
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		  test.ReadInfo.u32LBAAddress,
		  test.ReadInfo.u32NumBlocks,
		  test.max_lba);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  CleanupCdCtrlTest(&test);
	  OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
	  return CD_CTRL_IO_CTRL_ERR;
	}


      /*
       * now read a random block in between to flush all caches
       */
      ReadInfo2.u32NumBlocks = ((((tU32)OSAL_s32Random()) % MAX_NR_REQUEST)+REQUEST_MIN)/2;
      ReadInfo2.u32LBAAddress = ((tU32) OSAL_s32Random()) % test.max_lba;
      /*
       * verify that we can read engough at the end of the disk without running over max_lba
       */
      if (ReadInfo2.u32LBAAddress > (test.max_lba - ReadInfo2.u32NumBlocks) )
	{
	  ReadInfo2.u32LBAAddress = test.max_lba - ReadInfo2.u32NumBlocks;
	}

      s32Ret = OSAL_s32IOControl( test.hDevice,
				  OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				  (tS32) &ReadInfo2 );
      if ( OSAL_OK != s32Ret )
	{
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		  ReadInfo2.u32LBAAddress,
		  ReadInfo2.u32NumBlocks,
		  test.max_lba);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  CleanupCdCtrlTest(&test);
	  OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
	  return CD_CTRL_IO_CTRL_ERR;
	}


      /*
       * now read the first random block again
       */
      ReadInfo2.u32NumBlocks = test.ReadInfo.u32NumBlocks;
      ReadInfo2.u32LBAAddress = test.ReadInfo.u32LBAAddress;
      s32Ret = OSAL_s32IOControl( test.hDevice,
				  OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				  (tS32) &ReadInfo2 );
      if ( OSAL_OK != s32Ret )
	{
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		  ReadInfo2.u32LBAAddress,
		  ReadInfo2.u32NumBlocks,
		  test.max_lba);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  CleanupCdCtrlTest(&test);
	  OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
	  return CD_CTRL_IO_CTRL_ERR;
	}

      /*
       * now compare the blocks which have been read
       */
      for ( i=0; i < ReadInfo2.u32NumBlocks; i++ )
	{
	  for ( j=0; j < 2048; j++ )
	    {
	      if ( ReadInfo2.ps8Buffer[j+(i*2048)] != test.ReadInfo.ps8Buffer[j+(i*2048)] )
		{
		  ClearTraceBuffer(&test);
		  sprintf(test.trace_buffer, "Byte differ @%d with lba %d, value1=%d value2=%d",
			  j+(i*2048), 
			  ReadInfo2.u32LBAAddress, 
			  ReadInfo2.ps8Buffer[j+(i*2048)], 
			  test.ReadInfo.ps8Buffer[j+(i*2048)]);
		  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
		  test_result = CD_CTRL_DATA_READ_ERR;
		}
	    }
	}

      test.msT1 = OSAL_ClockGetElapsedTime();
      test.msDiff = test.msT1- test.msT0;
      ClearTraceBuffer(&test);
      sprintf(test.trace_buffer, "%d msec for compare of lba=%d with blocks=%d", test.msDiff, test.ReadInfo.u32LBAAddress, test.ReadInfo.u32NumBlocks );
      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
    }

  if ( CleanupCdCtrlTest( &test ) != 0 )
    {
      OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
      return CD_CTRL_CLEANUP_ERR;
    }
  
  OSAL_vMemoryFree(ReadInfo2.ps8Buffer);
  return test_result;
}



/*
 * read test (using pre-recorded CdCtrlAccessPattern) simulating route calculation
 */
static tU32 u32TestCdCtrlRecordedRead(char* devicename )
{
  struct CdCtrlTestVar test;
  tS32 s32Ret;
  tU32 i;
  tU32 rc_phase;     /* we have to phases, each terminated by nr_blk=0, which are counted here */

  test.devicename = devicename;

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      return CD_CTRL_INITIALISE_ERR;
    }
  
  for( i=0; i<MAX_NR_TESTS; i++ )
    {
      rc_phase = 0;
      test.nr_blocks = 0;
      test.nr_reads = 0;
      test.msT0 = OSAL_ClockGetElapsedTime();
      do
	{
	  test.ReadInfo.u32LBAAddress = hi_stgt[test.nr_reads].lba;
	  test.ReadInfo.u32NumBlocks = hi_stgt[test.nr_reads].nr_blk;
#if 0
	  /*
	   * test to read more data to see effect on time
	   */
	  if ( hi_stgt[test.nr_reads].lba != 0 )
	    {
	      test.ReadInfo.u32NumBlocks = 0x18;
	    }
	  else
	    {
	      test.ReadInfo.u32NumBlocks = 0;
	    }
	  sprintf(test.trace_buffer, "read LBA=%d nr_block=%d      ",
		  test.ReadInfo.u32LBAAddress,
		  test.ReadInfo.u32NumBlocks);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
#endif 
	  /*
	   * verify that we can read engough at the end of the disk without running over max_lba
	   */
	  if (test.ReadInfo.u32LBAAddress > (test.max_lba - test.ReadInfo.u32NumBlocks) )
	    {
	      test.ReadInfo.u32LBAAddress = test.max_lba - test.ReadInfo.u32NumBlocks;
	    }
	  
	  if ( test.ReadInfo.u32NumBlocks > 0 ) 
	    {
	      s32Ret = OSAL_s32IOControl( test.hDevice,
					  OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
					  (tS32) &test.ReadInfo );
	      if ( OSAL_OK != s32Ret )
		{
		  ClearTraceBuffer(&test);
		  sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
			  test.ReadInfo.u32LBAAddress,
			  test.ReadInfo.u32NumBlocks,
			  test.max_lba);
		  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
		  CleanupCdCtrlTest(&test);
		  return CD_CTRL_IO_CTRL_ERR;
		}
	    }
	  else 
	    {
	      test.msT1 = OSAL_ClockGetElapsedTime();
	      test.msDiff = test.msT1- test.msT0;
	      ClearTraceBuffer(&test);
	      sprintf(test.trace_buffer, "%d msec for %d 2K blocks with %d recorded reads = %d KByte/sek ", 
		      test.msDiff, 
		      test.nr_blocks, 
		      test.nr_reads, 
		      ((test.nr_blocks * 2 ) * 1000) / test.msDiff);
	      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	      rc_phase++;
	    }
	  test.nr_blocks += test.ReadInfo.u32NumBlocks;
	  test.nr_reads += 1;
	} 
      while ( rc_phase <  2 ); 
    }

  if ( CleanupCdCtrlTest( &test ) != 0 )
    {
      return CD_CTRL_CLEANUP_ERR;
    }
  
  return CD_CTRL_NO_ERR;
}

/*
 * read test (using pre-recorded CdCtrlAccessPattern) simulating route calculation
 */
tU32 u32TestFilesystemRecordedRead(void)
{
  struct CdCtrlTestVar test;
  tU32 rc_phase;     /* we have two phases, each terminated by filename=NULL, which are counted here */
  int filenr;
  int i;
  tU32 nr_bytes;
  OSAL_tIODescriptor fd = OSAL_ERROR;
  
  test.msT0 = OSAL_ClockGetElapsedTime();
  filenr = 0;
  rc_phase = 0;
  nr_bytes = 0;

  test.devicename = "/dev/cdctrl0";

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", test.devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      return CD_CTRL_INITIALISE_ERR;
    }

  do
    {
      if ( hi_stgt_files[filenr].filename != NULL )
	{
#ifdef OPTIMIZE_OPEN_CLOSE
	  if ( filenr > 0 ) 
	    {
	      if ( ( hi_stgt_files[filenr-1].filename == NULL ) ||
		   ( strcmp(hi_stgt_files[filenr].filename, hi_stgt_files[filenr-1].filename) != 0 )
		   )
		{
		  OSAL_s32IOClose( fd );
		  fd = OSAL_IOOpen(hi_stgt_files[filenr].filename, OSAL_EN_READONLY);
		}
	    }
#else
	  if ( filenr > 0 )
	    {
	      OSAL_s32IOClose( fd );
	      fd = OSAL_IOOpen(hi_stgt_files[filenr].filename, OSAL_EN_READONLY);
	    }
#endif
	  else
	    {
	      fd = OSAL_IOOpen(hi_stgt_files[filenr].filename, OSAL_EN_READONLY);
	    }
	  if ( OSAL_ERROR == fd )
	    {
	      ClearTraceBuffer(&test);
	      sprintf(test.trace_buffer, "open failed file=%s ", 
		      hi_stgt_files[filenr].filename);
	      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	      CleanupCdCtrlTest(&test);
	      return CD_CTRL_IO_CTRL_ERR;
	    }
	  if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_FIOSEEK, hi_stgt_files[filenr].offset) == OSAL_ERROR)
	    {
	      ClearTraceBuffer(&test);
	      sprintf(test.trace_buffer, "seek failed file=%s pos=%d ", 
		      hi_stgt_files[filenr].filename,
		      hi_stgt_files[filenr].offset);
	      LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	      OSAL_s32IOClose( fd );
	      CleanupCdCtrlTest(&test);
	      return CD_CTRL_IO_CTRL_ERR;
	    }
	  nr_bytes += OSAL_s32IORead(fd, test.ReadInfo.ps8Buffer, hi_stgt_files[filenr].size - 1);
	}
	else
	{
	  test.msT1 = OSAL_ClockGetElapsedTime();
	  test.msDiff = test.msT1- test.msT0;
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "%d msec for %d files with %d bytes = %d KByte/sek ", 
		  test.msDiff, 
		  filenr, 
		  nr_bytes, 
		  ((nr_bytes/1024) * 1000) / test.msDiff);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  rc_phase++;
	}
      filenr++;
    } while (rc_phase < 2);

  OSAL_s32IOClose( fd );
  
  if ( CleanupCdCtrlTest( &test ) != 0 )
    {
      return CD_CTRL_CLEANUP_ERR;
    }

  
  return CD_CTRL_NO_ERR;
}


/*
 * sequential read test starting at random position
 */
static tU32 u32TestCdCtrlSequentialRead( char* devicename )
{
   struct CdCtrlTestVar test;
   tS32 s32Ret;
   tU32 i;

   test.devicename = devicename;

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      return CD_CTRL_INITIALISE_ERR;
    }
	   
   for( i=0; i<MAX_NR_TESTS; i++ )
     {
       test.ReadInfo.u32LBAAddress = ((tU32) OSAL_s32Random()) % test.max_lba;
       /*
	* verify that we can read engough at the end of the disk without running over max_lba
	*/
       if (test.ReadInfo.u32LBAAddress > (test.max_lba - MAX_NR_BLOCK*2) )
	 {
	   test.ReadInfo.u32LBAAddress = test.max_lba - MAX_NR_BLOCK*2;
	 }

       test.nr_blocks = 0;
       test.nr_reads = 0;
       
       test.msT0 = OSAL_ClockGetElapsedTime();
       do
	 {
	   test.ReadInfo.u32NumBlocks = ((((tU32)OSAL_s32Random()) % MAX_NR_REQUEST)+REQUEST_MIN)/2;
	   s32Ret = OSAL_s32IOControl( test.hDevice,
				       OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				       (tS32) &test.ReadInfo );
	   if ( OSAL_OK != s32Ret )
	     {
	       ClearTraceBuffer(&test);
	       sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		       test.ReadInfo.u32LBAAddress,
		       test.ReadInfo.u32NumBlocks,
		       test.max_lba);
	       LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	       CleanupCdCtrlTest(&test);
	       return CD_CTRL_IO_CTRL_ERR;
	     }
	   test.nr_reads += 1;
	   test.nr_blocks += test.ReadInfo.u32NumBlocks;
	   test.ReadInfo.u32LBAAddress += test.ReadInfo.u32NumBlocks;
	 } 
       while ( test.nr_blocks <  MAX_NR_BLOCK ); 
       test.msT1 = OSAL_ClockGetElapsedTime();
       test.msDiff = test.msT1- test.msT0;
       ClearTraceBuffer(&test);
       sprintf(test.trace_buffer, "%d msec for %d 2K blocks with %d seq. reads from LBA %d = %d KByte/sek ", 
	       test.msDiff, 
	       test.nr_blocks, 
	       test.nr_reads, 
	       test.ReadInfo.u32LBAAddress - test.nr_blocks, 
	       ((test.nr_blocks * 2 ) * 1000) / test.msDiff);
       LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
       oedt_vSetExtInfo(test.trace_buffer); 
     }

   if ( CleanupCdCtrlTest( &test ) != 0 )
     {
       return CD_CTRL_CLEANUP_ERR;
     }

   return CD_CTRL_NO_ERR;
}

/*
 * read test trying to read the complete CD/DVD (if called several times)
 */
static tU32 u32TestCdCtrlCompleteRead(char* devicename )
{
  struct CdCtrlTestVar test;
  tS32 s32Ret;
  tU32 i;

  test.devicename = devicename;

  i = InitialiseCdCtrlTest(&test);
  sprintf(test.trace_buffer, "%s initialised with return %d", devicename, i);
  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_USER_3, test.trace_buffer, strlen(test.trace_buffer) + 2 );
  if ( i > 0 )
    {
      if ( i > 1 )
	{
	  CleanupCdCtrlTest(&test);
	}
      return CD_CTRL_INITIALISE_ERR;
    }
  
  test.nr_blocks = 0;
  test.nr_reads = 0;
  test.msT0 = OSAL_ClockGetElapsedTime();
  test.ReadInfo.u32NumBlocks = (tU32) REQUEST_MAX/2;
  test.ReadInfo.u32LBAAddress = lba_static_start;
  i = 0;
  do 
    {
      if (test.ReadInfo.u32LBAAddress > (test.max_lba - test.ReadInfo.u32NumBlocks) )
	{
	  test.ReadInfo.u32LBAAddress = test.max_lba - test.ReadInfo.u32NumBlocks;
	  lba_static_start = 0;
	}
      
      s32Ret = OSAL_s32IOControl( test.hDevice,
				  OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,
				  (tS32) &test.ReadInfo );
      if ( OSAL_OK != s32Ret )
	{
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "read failed LBA=%d nr_block=%d max_lba=%d ", 
		  test.ReadInfo.u32LBAAddress,
		  test.ReadInfo.u32NumBlocks,
		  test.max_lba);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_FATAL, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  CleanupCdCtrlTest(&test);
	  return CD_CTRL_IO_CTRL_ERR;
	}
      test.nr_blocks += test.ReadInfo.u32NumBlocks;
      test.nr_reads += 1;
      test.msT1 = OSAL_ClockGetElapsedTime();
      test.msDiff = test.msT1- test.msT0;
      /*
       * cyclic (10 sek) print of transfer rate
       */
      if ( test.msDiff > 10000 )
	{
	  i++;
	  ClearTraceBuffer(&test);
	  sprintf(test.trace_buffer, "%d msec for %d 2K blocks with %d conseq. reads from LBA %d = %d KByte/sek ", 
		  test.msDiff, 
		  test.nr_blocks, 
		  test.nr_reads, 
		  test.ReadInfo.u32LBAAddress,
		  ((test.nr_blocks * 2 ) * 1000) / test.msDiff);
	  LLD_vTrace(TR_COMP_OSALTEST, (tS16) TR_LEVEL_COMPONENT, test.trace_buffer, strlen(test.trace_buffer) + 2 );
	  test.nr_blocks = 0;
	  test.nr_reads = 0;
	  test.msT0 = OSAL_ClockGetElapsedTime();
	}

      test.ReadInfo.u32LBAAddress += test.ReadInfo.u32NumBlocks;
    }
  while ( i < MAX_NR_TESTS );

  lba_static_start = test.ReadInfo.u32LBAAddress;

  if ( CleanupCdCtrlTest( &test ) != 0 )
    {
      return CD_CTRL_CLEANUP_ERR;
    }
  
  return CD_CTRL_NO_ERR;
}

/*
 * todo:
 * data read verification: compare two discs
 * random read REQUEST_MAX blocks from cdctrl0, compare it to the same blocks from cdctrl1
 */


/*
 * wrapper functions for oedt for the devices in Ford HSRNS
 */
tU32 u32TestCdCtrl0RandomRead(void)
{
  return u32TestCdCtrlRandomRead("/dev/cdctrl");
}

tU32 u32TestCdCtrl1RandomRead(void)
{
  return u32TestCdCtrlRandomRead("/dev/cdctrl1");
}

tU32 u32TestCdCtrl0RecordedRead(void)
{
  return u32TestCdCtrlRecordedRead("/dev/cdctrl");
}

tU32 u32TestCdCtrl0SequentialRead(void)
{
  return u32TestCdCtrlSequentialRead("/dev/cdctrl");
}

tU32 u32TestCdCtrl1SequentialRead(void)
{
  return u32TestCdCtrlSequentialRead("/dev/cdctrl1");
}

tU32 u32TestCdCtrl0CompleteRead(void)
{
  return u32TestCdCtrlCompleteRead("/dev/cdctrl");
}

tU32 u32TestCdCtrl1CompleteRead(void)
{
  return u32TestCdCtrlCompleteRead("/dev/cdctrl1");
}

tU32 u32TestCdCtrl0RandomReadWithVerify(void)
{
  return u32TestCdCtrlRandomReadWithVerify("/dev/cdctrl") ;
}

tU32 u32TestCdCtrl1RandomReadWithVerify(void)
{
  return u32TestCdCtrlRandomReadWithVerify("/dev/cdctrl1") ;
}
