
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 


//#define NUCLEUS_CORE_S_IMPORT_INTERFACE_NUCLEUS
//#include "nucleus_core_pif.h"
extern void os_assertion_failed(const char *file, unsigned long line);
#define OS_ASSERT(_INT_) \
        ((_INT_) ? (tVoid)(0) : (os_assertion_failed(__FILE__, (__LINE__)&(~0x80000000))) )



#define MAX_NO_OF_JOBS 25

#define DIRECTORY_TO_READ "/dev/cdrom/data/rnw"
#define BYTES_TO_READ (1024*24)

#define READ_TASK_WAIT_MS   135
#define CANCEL_TASK_WAIT_MS 350
#define WRITE_TASK_WAIT_MS  50

#define IPN_DATASIZE 250

typedef struct tWriteData_
{
   OSAL_tSemHandle *semaphore;
   const char *deviceName;

   OSAL_trAsyncControl *poActualJob;
} tWriteData;


OSAL_trAsyncControl m_poAsyncControl[MAX_NO_OF_JOBS] = {0};
tBool               m_bAsyncUsed[MAX_NO_OF_JOBS] = {0};
tU32 m_noOfDirEntries = 0;
OSAL_tSemHandle m_hSid;

OSAL_tSemHandle m_hWriteTask1;
OSAL_tSemHandle m_hWriteTask2;


tC8 **dirEntries;


void vWaitRandom(int msecMax)
{
   tDouble max = (tDouble) msecMax;
   tS32 tmp = OSAL_s32Random();
   tDouble rnd = (tDouble) tmp;

   rnd = (rnd / RAND_MAX) * max + 0.5;

   OSAL_s32ThreadWait((OSAL_tMSecond) rnd);
}

void vLock()
{
   OSAL_s32SemaphoreWait(m_hSid, OSAL_C_TIMEOUT_FOREVER);
}

void vUnlock()
{
   OSAL_s32SemaphorePost(m_hSid);
}

tU32 u32GetNoOfUsedEntries()
{
   int i;

   tU32 noOfUsedEntries = 0;

   for (i=0; i<MAX_NO_OF_JOBS; ++i)
   {
      if (m_bAsyncUsed[i] == TRUE)
      {
         ++noOfUsedEntries;
      }
   }
   return noOfUsedEntries;
}

tU32 u32GetNoOfFreeEntries()
{
   return MAX_NO_OF_JOBS - u32GetNoOfUsedEntries();
}

tBool bCountDirEntries()
{
   OSAL_trIOCtrlDir* pDir;
   OSAL_trIOCtrlDirent* pEntry;
   tBool success = FALSE;
   m_noOfDirEntries = 0;
   pDir = OSALUTIL_prOpenDir (DIRECTORY_TO_READ);

   pEntry = OSALUTIL_prReadDir(pDir);
   
   while (pEntry != NULL)
   {
      pEntry = OSALUTIL_prReadDir(pDir);
      ++m_noOfDirEntries;
   }

   OSALUTIL_s32CloseDir (pDir);

   dirEntries = malloc (sizeof(tC8*) * m_noOfDirEntries);

   if (dirEntries != NULL)
   {
      tU32 i;
      pDir = OSALUTIL_prOpenDir (DIRECTORY_TO_READ);

      for(i=0; i<m_noOfDirEntries; ++i)
      {
         tU32 length;
         pEntry = OSALUTIL_prReadDir(pDir);
         length = OSAL_u32StringLength(pEntry->s8Name);
         dirEntries[i] = malloc (sizeof(tC8) * length + 1);
         (void) OSAL_szStringCopy (dirEntries[i], pEntry->s8Name);
         *((dirEntries[i])+length) = 0;
      }
   
      OSALUTIL_s32CloseDir (pDir);

      success = TRUE;
   }

   return success;
}

tBool bReadDirEntry(tU32 no, tC8 *fileName)
{
   tBool success = FALSE;

   if (no < m_noOfDirEntries)
   {
      (void) OSAL_szStringCopy (fileName, dirEntries[no]);
      success = TRUE;
   }

   return success;
}


void vShuffleThreadPrio()
{
   tU8 newPrio;
   tDouble rnd = (tDouble) OSAL_s32Random();     // 0-32767

   rnd = (rnd / RAND_MAX) * 70;
   newPrio = (tU8) rnd+180;

   OSAL_s32ThreadPriority (OSAL_ThreadWhoAmI(), newPrio);
}


OSAL_trAsyncControl* pGetFreeJobEntry()
{
   int i;
   OSAL_trAsyncControl* freeJob = NULL;

   for (i=0; i<MAX_NO_OF_JOBS; ++i)
   {
      if (m_bAsyncUsed[i] == FALSE)
      {
         freeJob = &m_poAsyncControl[i];
         m_bAsyncUsed[i] = TRUE;
         break;
      }
   }

   return freeJob;
}


void vFreeJobEntry (const OSAL_trAsyncControl* block)
{
   int i;

   for (i=0; i<MAX_NO_OF_JOBS; ++i)
   {
      if (&m_poAsyncControl[i] == block)
      {
         m_bAsyncUsed[i] = FALSE;         
         break;
      }
   }
}


void writeCallback(void *arg)
{
   tWriteData *config;
   config = (tWriteData *) arg;

   if (config->poActualJob->s32Status == OSAL_ERROR)
   {
      vWaitRandom(WRITE_TASK_WAIT_MS);
   }

   //jobentry schliessen
   vLock();
   config->poActualJob->pvBuffer = NULL;
   vFreeJobEntry (config->poActualJob);
   vUnlock();

   //Semaphore posten
   OSAL_s32SemaphorePost(*config->semaphore);

   return;
}


void readCallback(void *arg)
{
   OSAL_trAsyncControl* job = (OSAL_trAsyncControl*) arg;
   //file schliessen
   OSAL_s32IOClose(job->id);
   //speicher freigeben
   free(job->pvBuffer);
   job->pvBuffer = NULL;
   //jobentry schliessen
   vLock();
   vFreeJobEntry (job);
   vUnlock();

   return;
}


OSAL_tIODescriptor openFile (const char *filename)
{
   OSAL_tIODescriptor fd = -1;
   char tmp[1024] = {0};

   OSAL_s32PrintFormat (tmp, "%s/%s", DIRECTORY_TO_READ, filename);

   fd = OSAL_IOOpen (tmp, OSAL_EN_READONLY);

   return fd;
}


void vCreateReadJob(const char *filename)
{
   OSAL_trAsyncControl* job;

   vLock();

   if (u32GetNoOfFreeEntries() > 2)
   {
      job = pGetFreeJobEntry();

      vUnlock();

      if (job != NULL)
      {
         OSAL_tIODescriptor fd = openFile(filename);
         
         if (fd != OSAL_ERROR)
         {
            tS32 s32RetVal;

            job->id = fd;
            job->s32Offset = 0;
            OS_ASSERT(job->pvBuffer == NULL);
            job->pvBuffer = malloc(BYTES_TO_READ);
            job->u32Length = BYTES_TO_READ;
            job->pCallBack = readCallback;
            job->pvArg = job;

            s32RetVal = OSAL_s32IOReadAsync (job);
            if (s32RetVal != OSAL_OK)
            {
               readCallback(job);
            }
         }
      }
   }
   else
   {
      vUnlock();
   }
}


tU32 u32SelectRandomEntry (tU32 noOfEntries)
{
   tDouble rnd = (tDouble) OSAL_s32Random();
   tDouble d = noOfEntries;

   if (noOfEntries == 0)
      return 0;
   if (noOfEntries == 1)
      return 1;

   d = (rnd / RAND_MAX) * d + 0.5;

   return (tU32) d;
}


void readTask(void *arg)
{
   tC8 fileName[OSAL_C_U32_MAX_PATHLENGTH];
   tU32 entry;
   (void) arg; // not necessary in this function

   while (1)  /*lint !e716     Yes, this is an endless loop... */
   {
      vShuffleThreadPrio();
      entry = u32SelectRandomEntry(m_noOfDirEntries);

      memset (fileName, 0, sizeof(fileName));
      if (bReadDirEntry (entry, fileName) == TRUE)
      {
         vCreateReadJob(fileName);
      }

      vWaitRandom(READ_TASK_WAIT_MS);
   }
}   /*lint !e818*/



OSAL_trAsyncControl* poGetUsedEntry(tU32 number)
{
   OSAL_trAsyncControl* entry = NULL;
   tU32 noOfUsedEntries = 0;
   int i;

   for (i=0; i<MAX_NO_OF_JOBS; ++i)
   {
      if (m_bAsyncUsed[i] == TRUE)
      {
         ++noOfUsedEntries;
      }

      if (noOfUsedEntries == number)
      {
         entry = &m_poAsyncControl[i];
         break;
      }
   }

   return entry;
}


void cancelTask(void* arg)
{
   tU32 noOfUsedEntries;
   OSAL_trAsyncControl *poAIO;

   (void) arg;  // not necessary in this function

   while (1)  /*lint !e716     Yes, this is an endless loop... */
   {
      vShuffleThreadPrio();
      vWaitRandom(CANCEL_TASK_WAIT_MS);

      vLock();
      noOfUsedEntries = u32GetNoOfUsedEntries();
      if (noOfUsedEntries > 0)
      {
         poAIO = poGetUsedEntry(u32SelectRandomEntry(noOfUsedEntries));
         
         vUnlock();
         
         if (poAIO != NULL)
         {
            if (OSAL_s32IOCancelAsync( poAIO->id, 
                                       poAIO ) == OSAL_C_S32_AIO_CANCELED )
            {
               poAIO->pCallBack(poAIO->pvArg);
            }
         }
      }
      else
      {
         vUnlock();
      }
      
   }
}  /*lint !e818*/



void writeTask (void *arg)
{
   tWriteData *config;
   OSAL_tIODescriptor fd = -1;

   config = (tWriteData *) arg;

   fd = OSAL_IOOpen(config->deviceName, OSAL_EN_WRITEONLY);

   if (fd != -1)
   {
      tU8 data[IPN_DATASIZE];
      int i;
      for (i=0; i<IPN_DATASIZE; ++i)
      {
         data[i] = (tU8) i;
      }

      while (1)  /*lint !e716     Yes, this is an endless loop... */
      {
         OSAL_trAsyncControl *job;

         vShuffleThreadPrio();

         vLock();
         job = pGetFreeJobEntry();
         vUnlock();

         if (job != NULL)
         {
            tS32 s32RetVal;

            config->poActualJob = job;

            job->id = fd;
            job->s32Offset = 0;
            job->pvBuffer = data;
            job->u32Length = IPN_DATASIZE;
            job->pCallBack = writeCallback;
            job->pvArg = config;

            s32RetVal = OSAL_s32IOWriteAsync(job);
            
            if (s32RetVal != OSAL_OK)
            {
               writeCallback(config);
            }

            OSAL_s32SemaphoreWait (*config->semaphore, OSAL_C_TIMEOUT_FOREVER);
         }
         else
         {
            vWaitRandom(WRITE_TASK_WAIT_MS);
         }
      }
   }

   OSAL_s32IOClose(fd);
}




tU32 u32OEDT_OSAL_AsyncRead_StressTest( void )
{
   tU32 i;
   //OSAL_tThreadID readTaskId1;
   //OSAL_tThreadID readTaskId2;
   //OSAL_tThreadID readTaskId3;
   OSAL_trThreadAttribute readTaskAttribute1;
   OSAL_trThreadAttribute readTaskAttribute2;
   OSAL_trThreadAttribute readTaskAttribute3;

   //OSAL_tThreadID cancelTaskId;
   OSAL_trThreadAttribute cancelTaskAttribute;

   //OSAL_tThreadID writeTaskId1;
   //OSAL_tThreadID writeTaskId2;
   OSAL_trThreadAttribute writeTaskAttribute1;
   OSAL_trThreadAttribute writeTaskAttribute2;

   tWriteData writeDataTask1;
   tWriteData writeDataTask2;


   OSAL_vRandomSeed(42);
   if (bCountDirEntries() == TRUE)
   {
      OSAL_s32SemaphoreCreate ("u32OEDT_OSAL_AsyncRead_StressTest", &m_hSid, 1);
      OSAL_s32SemaphoreCreate ("WriteTask1", &m_hWriteTask1, 0);
      OSAL_s32SemaphoreCreate ("WriteTask2", &m_hWriteTask2, 0);

      readTaskAttribute1.szName = "READ_T1";
      readTaskAttribute1.u32Priority = 200;
      readTaskAttribute1.s32StackSize = 4096;
      readTaskAttribute1.pfEntry = readTask;
      readTaskAttribute1.pvArg = NULL;

      readTaskAttribute2.szName = "READ_T2";
      readTaskAttribute2.u32Priority = 200;
      readTaskAttribute2.s32StackSize = 4096;
      readTaskAttribute2.pfEntry = readTask;
      readTaskAttribute2.pvArg = NULL;

      readTaskAttribute3.szName = "READ_T3";
      readTaskAttribute3.u32Priority = 200;
      readTaskAttribute3.s32StackSize = 4096;
      readTaskAttribute3.pfEntry = readTask;
      readTaskAttribute3.pvArg = NULL;

      cancelTaskAttribute.szName = "CANCEL_TASK";
      cancelTaskAttribute.u32Priority = 200;
      cancelTaskAttribute.s32StackSize = 4096;
      cancelTaskAttribute.pfEntry = cancelTask;
      cancelTaskAttribute.pvArg = NULL;

      writeDataTask1.semaphore = &m_hWriteTask1;
      writeDataTask1.deviceName = "/dev/fgs/map";

      writeDataTask2.semaphore = &m_hWriteTask2;
      writeDataTask2.deviceName = "/dev/fgs/map_carsor";

      writeTaskAttribute1.szName = "WRITE_T1";
      writeTaskAttribute1.u32Priority = 200;
      writeTaskAttribute1.s32StackSize = 4096;
      writeTaskAttribute1.pfEntry = writeTask;
      writeTaskAttribute1.pvArg = &writeDataTask1;

      writeTaskAttribute2.szName = "WRITE_T2";
      writeTaskAttribute2.u32Priority = 200;
      writeTaskAttribute2.s32StackSize = 4096;
      writeTaskAttribute2.pfEntry = writeTask;
      writeTaskAttribute2.pvArg = &writeDataTask2;


      /*readTaskId1 =*/(void) OSAL_ThreadSpawn(&readTaskAttribute1);
      /*readTaskId2 =*/(void) OSAL_ThreadSpawn(&readTaskAttribute2);
      /*readTaskId3 =*/(void) OSAL_ThreadSpawn(&readTaskAttribute3);

      /*cancelTaskId =*/(void) OSAL_ThreadSpawn(&cancelTaskAttribute);

      /*writeTaskId1 =*/(void) OSAL_ThreadSpawn(&writeTaskAttribute1);
      /*writeTaskId2 =*/(void) OSAL_ThreadSpawn(&writeTaskAttribute2);

      // wait 4ever...
      OSAL_s32ThreadWait(0xF0000000);

      for (i=0; i<m_noOfDirEntries; ++i)
      {
         free(dirEntries[i]);
      }

      free (dirEntries);
      OSAL_s32SemaphoreClose(m_hSid);
      OSAL_s32SemaphoreDelete("u32OEDT_OSAL_AsyncRead_StressTest");
      OSAL_s32SemaphoreClose(m_hWriteTask1);
      OSAL_s32SemaphoreDelete("WriteTask1");
      OSAL_s32SemaphoreClose(m_hWriteTask2);
      OSAL_s32SemaphoreDelete("WriteTask2");

      return 0;
   }
   else
   {
      return 1;
   }
   //OSAL_s32ThreadPriority (OSAL_ThreadWhoAmI(), prio);

/*   OSAL_tIODescriptor fd = OSAL_IOOpen ("/dev/", OSAL_tenAccess);

   OSAL_s32IOReadAsync (OSAL_trAsyncControl *prAIO);
   OSAL_s32IOWriteAsync (OSAL_trAsyncControl *prAIO);
   OSAL_s32IOCancelAsync(OSAL_tIODescriptor fd, OSAL_trAsyncControl *prAIO);

   OSAL_s32IOClose (fd);

   tU32 rnd = (tU32) OSAL_s32Random();     // 0-32767*/

}
