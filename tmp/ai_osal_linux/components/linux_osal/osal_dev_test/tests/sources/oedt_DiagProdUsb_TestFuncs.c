/**********************************************************FileHeaderBegin******
 *
 * FILE:        odt_DiagProdUsb_TestFuncs.c
 *
 * CREATED:     2005-12-06
 *
 * AUTHOR:
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2005 Technik & Marketing Service GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/

 
/*****************************************************************
| includes: 
|   1)system- and project- includes
|   2)needed interfaces from external components
|...3)internal and external interfaces from this component
|----------------------------------------------------------------*/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 


/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/



/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/



/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

static tCString strDiagProdUsb_EventName_test = "DiagProdUsbTest1";
static OSAL_tEventHandle DiagProdUsb_handleEvent_Test = OSAL_C_INVALID_HANDLE;

static tU8 DiagProdUsb_arRequestMsg[255];


/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/



/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/



/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DiagProdUsb_vTestOnReceiveInd
 * CREATED     : 2006-01-30
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : void DiagProdUsb_vTestOnReceiveInd(tPCS8 ps8Buffer,tU32
 *      u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               none
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

void DiagProdUsb_vTestOnReceiveInd(tPCS8 ps8Buffer, tU32 u32nbytes)
{
  tU32 i;
  for (i = 0; i < u32nbytes; i++)
  {
    DiagProdUsb_arRequestMsg[i] = ps8Buffer[i];
  }
  OSAL_s32EventPost(DiagProdUsb_handleEvent_Test, 1, OSAL_EN_EVENTMASK_OR);
}


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DiagProdUsb_vTestOnReceiveCon
 * CREATED     : 2006-01-30
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : void DiagProdUsb_vTestOnReceiveCon(tU32 u32res)
 * ARGUMENTS   : 
 *               tU32 u32res
 * RETURN VALUE: 
 *               none
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

void DiagProdUsb_vTestOnReceiveCon(tU32 u32res)
{
  if (u32res != 0)
  {
    OSAL_s32EventPost(DiagProdUsb_handleEvent_Test, 2, OSAL_EN_EVENTMASK_OR);
  }
}


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : u32DiagProdUsbTest
 * CREATED     : 2005-12-06
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tU32 u32DiagProdUsbTest(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tU32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tU32 u32DiagProdUsbTest(tVoid)
{
  tU32 u32Result = 0;
  OSAL_tIODescriptor fd;
  OSAL_trDiagProdProxyCallback rCallback;
  OSAL_tEventMask rResultEventMask;

  fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_PROD, OSAL_EN_READWRITE);
  if (fd == OSAL_ERROR)
  {
    return 1;
  }

  if(OSAL_C_INVALID_HANDLE == DiagProdUsb_handleEvent_Test)
  {
      if(OSAL_OK != OSAL_s32EventCreate(strDiagProdUsb_EventName_test, &DiagProdUsb_handleEvent_Test))
      {
          return 1;
      }
  }
  else
  {
      if(OSAL_OK != OSAL_s32EventOpen(strDiagProdUsb_EventName_test, &DiagProdUsb_handleEvent_Test))
      {
          return 1;
      }
  }

  rCallback.m_pOnReceiveCon = DiagProdUsb_vTestOnReceiveCon;
  rCallback.m_pOnReceiveInd = DiagProdUsb_vTestOnReceiveInd;
  if (OSAL_OK != OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 
    (tS32) &rCallback))
  {
    u32Result = 3; 
  }
  else if (OSAL_OK == (tS32) OSAL_s32EventWait(
    /*OSAL_tEventHandle*/ DiagProdUsb_handleEvent_Test, 
    /*OSAL_tEventMask*/   1, 
    /*OSAL_tenEventMaskFlag*/ OSAL_EN_EVENTMASK_OR,
    /*OSAL_tMSecond*/ 		 10000 /*msec*/,
    /*OSAL_tEventMask*/ &rResultEventMask))
  {
    if (rResultEventMask & 1)
    {
      /* request message received */

      /* Clear event */
      if (OSAL_OK == OSAL_s32EventPost(DiagProdUsb_handleEvent_Test,
                         ~1, OSAL_EN_EVENTMASK_AND))
      {
        /* Send a response message (echo the request message) */

        tU8 u8Response = DiagProdUsb_arRequestMsg[0] + 1;

        if (OSAL_ERROR == OSAL_s32IOWrite(fd, (tPCS8)&u8Response, 1))
        {
          u32Result = 4; 
        }
        else 
        {     
          if (OSAL_OK != (tS32) OSAL_s32EventWait(
            /*OSAL_tEventHandle*/ DiagProdUsb_handleEvent_Test, 
            /*OSAL_tEventMask*/   2, 
            /*OSAL_tenEventMaskFlag*/ OSAL_EN_EVENTMASK_OR,
            /*OSAL_tMSecond*/ 		 5000 /*msec*/,
            /*OSAL_tEventMask*/ &rResultEventMask))
          {
            /* Timeout on sending the response */
            u32Result = 5; 
          }
          else 
          {
            if (rResultEventMask & 2)
            {
              /* Clear event */
              if( OSAL_OK == OSAL_s32EventPost(DiagProdUsb_handleEvent_Test,
                                 ~2, OSAL_EN_EVENTMASK_AND))
              {
                /* response successfully sent */
              }
            }
            else
            {
              /* Timeout on sending the response */
              u32Result = 6; 
            }
          }
        }
      }
    } /* (rResultEventMask & 1) */
  } /* OSAL_OK == (tS32) OSAL_s32EventWait */
  else
  {
    /* No request message received */
  }

  if (OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_CALLBACK_REG, 0))
  {
    u32Result = 7; 
  }

  if (OSAL_OK != OSAL_s32IOClose(fd))
  {
    return 2;
  }

  return u32Result;
}

/* End of File odt_DiagProdUsb_TestFuncs.c                                  */

