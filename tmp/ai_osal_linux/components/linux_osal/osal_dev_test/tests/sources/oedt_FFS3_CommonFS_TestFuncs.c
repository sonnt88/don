/******************************************************************************
 * FILE               : oedt_FFS3_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT :   OEDT_FrmWrk 
 *
 * DESCRIPTION  :   This file implements the file system test cases for the FFS3  
 *                          
 * AUTHOR(s)        :   Sriranjan (RBEI/ECM1)
 *
 * HISTORY          :
 *-----------------------------------------------------------------------------
 *  Date              |                         |   Author & comments
 * --.--.--         |   Initial revision|   ------------
 * 17 Jan, 2012 |   version 1.0         |   Martin Langer CM-AI/PJ-CF33
 *-----------------------------------------------------------------------------
 * 10-02-2015   | Lint Fix (CFG3-1027)  |  Kranthi Kiran (RBEI/ECF5)
 * 26-03-2015   | Lint Fix (CFG3-993)   |  Deepak Kumar (RBEI/ECF5)
 * 05 Feb,2016  | OEDT Fix (CF3PF-3742) |  Chakitha Saraswathi (RBEI/ECF5)
 *------------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_testing_macros.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_FFS3_CommonFS_TestFuncs.h"
#include "oedt_teststate.h"
extern OEDT_TESTSTATE OEDT_CREATE_TESTSTATE(tVoid);                             //lint fix
/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenClosedevice( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_001
* DESCRIPTION   :   Opens and closes FFS3 device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSOpenClosedevice(tVoid)
{ 
    return u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpendevInvalParm( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_002
* DESCRIPTION   :   Try to Open FFS3 device with invalid parameters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32FFS3_CommonFSOpendevInvalParm(tVoid)
{
    return u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReOpendev( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_003
* DESCRIPTION   :   Try to Re-Open FFS3 device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSReOpendev(tVoid)
{
    return u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpendevDiffModes( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_004
* DESCRIPTION   :   FFS3 device in different modes
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSOpendevDiffModes(tVoid)
{
    return u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSClosedevAlreadyClosed( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_005
* DESCRIPTION   :   Close FFS3 device which is already closed
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSClosedevAlreadyClosed(tVoid)
{
    return u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenClosedir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_006
* DESCRIPTION   :   Try to Open and closes a directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSOpenClosedir(tVoid)
{
    return u32FSOpenClosedir((const tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpendirInvalid( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_007
* DESCRIPTION   :   Try to Open invalid directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSOpendirInvalid(tVoid)
{
    tPS8 dev_name[2] ;
	dev_name[0]= (tPS8)OEDT_C_STRING_FFS3_NONEXST;
	dev_name[1]=(tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
	return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDelDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_008
* DESCRIPTION   :   Try create and delete directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSCreateDelDir(tVoid)
{
    return u32FSCreateDelDir((const tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDelSubDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_009
* DESCRIPTION   :   Try create and delete sub directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSCreateDelSubDir(tVoid)
{
    return u32FSCreateDelSubDir((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDirInvalName( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_010
* DESCRIPTION   :   Try create and delete directory with invalid path
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSCreateDirInvalName(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)OEDT_C_STRING_FFS3_DIR_INV_NAME;

    return u32FSCreateDirInvalName(dev_name );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRmNonExstngDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_011
* DESCRIPTION   :   Try to remove non exsisting directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSRmNonExstngDir(tVoid)
{
    return u32FSRmNonExstngDir((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRmDirUsingIOCTRL( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_012
* DESCRIPTION   :   Delete directory with which is not a directory (with files) 
*                       using IOCTRL
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSRmDirUsingIOCTRL(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)OEDT_C_STRING_FFS3_FILE1;

    return u32FSRmDirUsingIOCTRL( dev_name );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetDirInfo( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_013
* DESCRIPTION   :   Get directory information
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSGetDirInfo(tVoid)
{
    return u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenDirDiffModes( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_014
* DESCRIPTION   :   Try to open directory in different modes
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSOpenDirDiffModes(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)SUBDIR_PATH_FFS3;

    return u32FSOpenDirDiffModes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReOpenDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_015
* DESCRIPTION   :   Try to reopen directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSReOpenDir(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]= (tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
    dev_name[1]= (tPS8)OEDT_C_STRING_FFS3_DIR1;
    
    return u32FSReOpenDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSDirParallelAccess( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_016
* DESCRIPTION   :   When directory is accessed by another thread try to rename it
*                       delete it
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSDirParallelAccess(tVoid)
{
    return u32FSDirParallelAccess((const tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT); 
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDirMultiTimes( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_017
* DESCRIPTION   :   Try to Create directory with similar name 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSCreateDirMultiTimes(tVoid)
{
    tPS8 dev_name[3];
    dev_name[0]=(tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
    dev_name[1]=(tPS8)SUBDIR_PATH_FFS3;
    dev_name[2]=(tPS8)SUBDIR_PATH2_FFS3;
    
    return u32FSCreateDirMultiTimes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateRemDirInvalPath( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_018
* DESCRIPTION   :   Create/ remove directory with Invalid path
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSCreateRemDirInvalPath(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]= (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]= (tPS8)OEDT_C_STRING_FFS3_DIR_INV_PATH;						
    
    return u32FSCreateRemDirInvalPath(dev_name);
}



/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateRmNonEmptyDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_019
* DESCRIPTION   :   Try to remove non Empty directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSCreateRmNonEmptyDir(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)SUBDIR_PATH_FFS3;
    
    return u32FSCreateRmNonEmptyDir(dev_name, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCopyDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_020
* DESCRIPTION   :   Copy the files in source directory to destination directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSCopyDir(tVoid)
{
    tPS8 dev_name[3];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)FILE13_FFS3;
    dev_name[2]=(tPS8)FILE14_FFS3;
    
    return u32FSCopyDir(dev_name,TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSMultiCreateDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_021
* DESCRIPTION   :   Create multiple sub directories
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSMultiCreateDir(tVoid)
{
    return u32FSMultiCreateDir((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateSubDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_022
* DESCRIPTION   :   Attempt to create sub-directory within a directory opened
*                       in READONLY modey
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSCreateSubDir(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0] = (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]	= (tPS8)SUBDIR_PATH_FFS3;
    
    return u32FSCreateSubDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSDelInvDir ( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_023
* DESCRIPTION   :   Delete invalid Directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSDelInvDir(tVoid)
{
    tPS8 dev_name[2];
    dev_name[0] = (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]	= (tPS8)OEDT_C_STRING_FFS3_DIR_INV_NAME;
    
    return u32FSDelInvDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCopyDirRec( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_024
* DESCRIPTION   :   Copy directories recursively
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32FFS3_CommonFSCopyDirRec(tVoid)
{
    tPS8 dev_name[3];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)FILE13_FFS3;
    dev_name[2]=(tPS8)FILE14_FFS3;

    return u32FSCopyDirRec(dev_name);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRemoveDir ( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_025
* DESCRIPTION   :   Remove directories recursively
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSRemoveDir(tVoid)
{
    tPS8 dev_name[4] ;
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)FILE11_FFS3;
    dev_name[2]=(tPS8)FILE12_FFS3;
    dev_name[3]=(tPS8)FILE12_RECR2_FFS3;
    
    return u32FSRemoveDir(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateDel( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_026
* DESCRIPTION   :  Create/ Delete file  with correct parameters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileCreateDel(tVoid)
{
    return u32FSFileCreateDel((const tPS8)CREAT_FILE_PATH_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenClose( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_027
* DESCRIPTION   :   Open /close File
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileOpenClose(tVoid)
{
    return u32FSFileOpenClose((const tPS8)OSAL_TEXT_FILE_FIRST_FFS3, TRUE);
}   


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenInvalPath( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_028
* DESCRIPTION   :  Open file with invalid path name (should fail)
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileOpenInvalPath(tVoid)
{
    return u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenInvalParam( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_029
* DESCRIPTION   :   Open a file with invalid parameters (should fail), 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileOpenInvalParam(tVoid)
{
    return u32FSFileOpenInvalParam((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReOpen( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_030
* DESCRIPTION   :  Try to open and close the file which is already opened
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32  u32FFS3_CommonFSFileReOpen(tVoid)
{
    return u32FSFileReOpen((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileRead( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_031
* DESCRIPTION   :   Read data from already exsisting file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileRead(tVoid)
{   
    return u32FSFileRead((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileWrite( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_032
* DESCRIPTION   :  Write data to an existing file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileWrite(tVoid)
{   
    return u32FSFileWrite((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);   
}   


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetPosFrmBOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_033
* DESCRIPTION   :  Get File Position from begining of file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSGetPosFrmBOF(tVoid)
{
    return u32FSGetPosFrmBOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetPosFrmEOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_034
* DESCRIPTION   :   Get File Position from end of file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSGetPosFrmEOF(tVoid)
{
    return u32FSGetPosFrmEOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_035
* DESCRIPTION   :   Read with a negative offset from BOF
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32   u32FFS3_CommonFSFileReadNegOffsetFrmBOF()
{
    return u32FSFileReadNegOffsetFrmBOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_036
* DESCRIPTION   :   Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
    return u32FSFileReadOffsetBeyondEOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_037
* DESCRIPTION   :  Read from offset from Beginning of file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadOffsetFrmBOF(tVoid)
{   
    return u32FSFileReadOffsetFrmBOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_038
* DESCRIPTION   :   Read file with few offsets from EOF
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadOffsetFrmEOF(tVoid)
{   
    return u32FSFileReadOffsetFrmEOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_039
* DESCRIPTION   :   Reads file by skipping certain offsets at specified intervals.
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
    return u32FSFileReadEveryNthByteFrmBOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_040
* DESCRIPTION   :   Reads file by skipping certain offsets at specified intervals.
*                       (This is from EOF)      
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
    return u32FSFileReadEveryNthByteFrmEOF((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetFileCRC( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_041
* DESCRIPTION   :   Get CRC value
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32FFS3_CommonFSGetFileCRC(tVoid)
{
    return u32FSGetFileCRC((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadAsync()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_042
* DESCRIPTION   :   Read data asyncronously from a file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32FFS3_CommonFSReadAsync(tVoid)
{   
    return u32FSReadAsync((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);

}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSLargeReadAsync()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_043
* DESCRIPTION   :   Read data asyncronously from a large file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSLargeReadAsync(tVoid)
{   
    return u32FSLargeReadAsync((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteAsync()
* PARAMETER     :   none
* RETURNVALUE   :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_044
* DESCRIPTION   :   Write data asyncronously to a file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteAsync(tVoid)
{   
    return u32FSWriteAsync((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}

    
/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSLargeWriteAsync()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_045
* DESCRIPTION   :   Write data asyncronously to a large file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSLargeWriteAsync(tVoid)
{   
    return u32FSLargeWriteAsync((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteOddBuffer()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_046
* DESCRIPTION   :   Write data to an odd buffer
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteOddBuffer(tVoid)
{
    return u32FSWriteOddBuffer((const tPS8)OEDT_FFS3_WRITEFILE, TRUE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteFileWithInvalidSize()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FS_047
* DESCRIPTION   :  Write data to a file with invalid size
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteFileWithInvalidSize(tVoid)
{
    return u32FSWriteFileWithInvalidSize ((const tPS8) OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteFileInvalidBuffer()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_048
* DESCRIPTION   :   Write data to a file with invalid buffer
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteFileInvalidBuffer(tVoid)
{
    return u32FSWriteFileInvalidBuffer ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}
/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteFileStepByStep()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_049
* DESCRIPTION   :   Write data to a file stepwise
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteFileStepByStep(tVoid)
{
    return u32FSWriteFileStepByStep ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetFreeSpace()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_050
* DESCRIPTION   :   Get free space of device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSGetFreeSpace (tVoid)
{
    return u32FSGetFreeSpace((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetTotalSpace()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_051
* DESCRIPTION   :   Get total space of device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSGetTotalSpace (tVoid)
{
    return u32FSGetTotalSpace ((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}
    
/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenCloseNonExstng()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_052
* DESCRIPTION   :   Open a non existing file 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32FFS3_CommonFSFileOpenCloseNonExstng(tVoid)
{
    return u32FSFileOpenCloseNonExstng ((const tPS8)OEDT_C_STRING_FILE_INVPATH_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileDelWithoutClose()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_053
* DESCRIPTION   :   Delete a file without closing 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileDelWithoutClose(tVoid)
{
    return u32FSFileDelWithoutClose ((const tPS8)OEDT_FFS3_WRITEFILE);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSSetFilePosDiffOff()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_054
* DESCRIPTION   :   Set file position to different offsets
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSSetFilePosDiffOff(tVoid)
{
    return u32FSSetFilePosDiffOff ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateFileMultiTimes()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_055
* DESCRIPTION   :   create file multiple times
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSCreateFileMultiTimes(tVoid)
{
    return u32FSCreateFileMultiTimes ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateUnicodeName()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_056
* DESCRIPTION   :   create file with unicode characters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileCreateUnicodeName(tVoid)
{
    return u32FSFileCreateUnicodeName ((const tPS8)OEDT_FFS3_CFS_UNICODEFILE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateInvalName()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_057
* DESCRIPTION   :   create file with invalid characters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileCreateInvalName(tVoid)
{
    return u32FSFileCreateInvalName((const tPS8)OEDT_FFS3_CFS_INVALCHAR_FILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateLongName()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_058
* DESCRIPTION   :   create file with long file name
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileCreateLongName(tVoid)
{
    return u32FSFileCreateLongName ((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateDiffModes()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_059
* DESCRIPTION   :   Create file with different access modes
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileCreateDiffModes(tVoid)
{
    return u32FSFileCreateDiffModes ((const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateInvalPath()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_060
* DESCRIPTION   :   Create file with invalid path
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/  
tU32 u32FFS3_CommonFSFileCreateInvalPath(tVoid)
{
    return u32FSFileCreateInvalPath ((const tPS8)OEDT_FFS3_CFS_INVALIDFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSStringSearch()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_061
* DESCRIPTION   :   Search for string 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSStringSearch(tVoid)
{
    return u32FSStringSearch ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenDiffModes()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_062
* DESCRIPTION   :   Create file with different access modes
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFileOpenDiffModes(tVoid)
{
    return u32FSFileOpenDiffModes ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE    );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadAccessCheck()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_063
* DESCRIPTION   :   Try to read from the file which has no access rights 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadAccessCheck(tVoid)
{   
    return u32FSFileReadAccessCheck  ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileWriteAccessCheck()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_064
* DESCRIPTION   :   Try to write from the file which has no access rights 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSFileWriteAccessCheck(tVoid)
{    
    return u32FSFileWriteAccessCheck ((const tPS8)OEDT_FFS3_WRITEFILE, TRUE );  
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadSubDir()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_065
* DESCRIPTION   :  Read file present in sub-directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSFileReadSubDir(tVoid)
{   
    return u32FSFileReadSubDir ((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
} 


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileWriteSubDir()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_066
* DESCRIPTION   :   Write to file present in sub-directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32FFS3_CommonFSFileWriteSubDir(tVoid)
{   
    return u32FSFileWriteSubDir ((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3 );
} 


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_067
* DESCRIPTION   :   Read Write time measure for 1KB file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile(tVoid)
{   
    return u32FSTimeMeasureof1KbFile ((const tPS8) OEDT_FFS3_WRITEFILE  );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_068
* DESCRIPTION   :   Read Write time measure for 10KB file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile(tVoid)
{   
    return u32FSTimeMeasureof10KbFile ((const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_069
* DESCRIPTION   :   Read Write time measure for 10KB file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile(tVoid)
{   
    return u32FSTimeMeasureof100KbFile ((const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_070
* DESCRIPTION   :   Read Write time measure for 1MB file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile(tVoid)
{   
    return u32FSTimeMeasureof1MBFile ((const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_071
* DESCRIPTION   :   Read write time measure for 1KB file 10000 times
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid)
{   
    return u32FSTimeMeasureof1KbFilefor1000times ((const tPS8)OEDT_FFS3_WRITEFILE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_072
* DESCRIPTION   :   Read write time measure for 10KB file 1000 times
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid)
{   
    return u32FSTimeMeasureof10KbFilefor1000times( (const tPS8)OEDT_FFS3_WRITEFILE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_073
* DESCRIPTION   :   Read write time measure for 100KB file 100 times
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid)
{   
    return u32FSTimeMeasureof100KbFilefor100times((const tPS8) OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_074
* DESCRIPTION   :   Read write time measure for 1MB file 16 times
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid)
{   
    return u32FSTimeMeasureof1MBFilefor16times(
                    (const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, 
                    (const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRenameFile()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_075
* DESCRIPTION   :   Rename file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSRenameFile(tVoid)
{
    return u32FSRenameFile((const tPS8) OEDTTEST_C_STRING_DEVICE_FFS3);
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteFrmBOF()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_076
* DESCRIPTION   :   Write to file from BOF
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteFrmBOF(tVoid)
{   
    return u32FSWriteFrmBOF((const tPS8) OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteFrmEOF()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_077
* DESCRIPTION   :   Write to file from EOF
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteFrmEOF(tVoid)
{   
    return u32FSWriteFrmEOF ( (const tPS8)OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSLargeFileRead()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_078
* DESCRIPTION   :   Read large file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSLargeFileRead(tVoid)
{   
    return u32FSLargeFileRead ((const tPS8) OEDT_FFS3_WRITEFILE, TRUE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSLargeFileWrite()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_079
* DESCRIPTION   :   Write large data to file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSLargeFileWrite(tVoid)
{   
    return u32FSLargeFileWrite ((const tPS8) OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenCloseMultiThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_080
* DESCRIPTION   :   Open and close file from multiple threads
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSOpenCloseMultiThread(tVoid)
{   
    return u32FSOpenCloseMultiThread ((const tPS8) OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteMultiThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_081
* DESCRIPTION   :   write to file from multiple threads
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteMultiThread(tVoid)
{   
    return u32FSWriteMultiThread ( (const tPS8)OEDT_FFS3_WRITEFILE );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteReadMultiThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_082
* DESCRIPTION   :   write and read to file from multiple threads
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteReadMultiThread(tVoid)
{   
    return u32FSWriteReadMultiThread ((const tPS8) OEDT_FFS3_WRITEFILE  );
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadMultiThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_083
* DESCRIPTION   :   Read from file from multiple threads
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSReadMultiThread(tVoid)
{   
    return u32FSReadMultiThread ((const tPS8) OEDT_FFS3_WRITEFILE );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFormat()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_084
* DESCRIPTION   :   Format FS
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/  
tU32 u32FFS3_CommonFSFormat(tVoid)
{   
    return u32FSFormat ((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3 );
}

/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFSFileGetExt()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FFS3_CFS_085
* DESCRIPTION:  Read file/directory contents 
* HISTORY:      Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32FFS3_CommonFSFileGetExt(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileGetExt( (const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3 );
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFSFileGetExt2()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FFS3_CFS_086
* DESCRIPTION:  Read file/directory contents with 2 IOCTL 
* HISTORY:      Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32FFS3_CommonFSFileGetExt2(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileGetExt2( (const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3 );
    return u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite()
* PARAMETER:    FFS3 device 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with sync write
* HISTORY:      Created by Vijay D (RBEI/ECF1) on 25 Mar, 2009
******************************************************************************/ 
tU32 u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite(tVoid)
{
    tU32 u32Ret; // u32Ret to identify the error

    u32Ret = u32FFSCommonFSSaveNowIOCTRL_SyncWrite(
                                   (tPS8)OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_SYNC
                                   ); 
    return u32Ret; 
}

/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite()
* PARAMETER:    FFS3 device 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Tests the OSAL_C_S32_IOCTRL_FFS_SAVENOW with async write
* HISTORY:      Created by Vijay D (RBEI/ECF1) on 25 Mar, 2009
******************************************************************************/
tU32 u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite(tVoid)
{
    tU32 u32Ret; // u32Ret to identify the error

    u32Ret = u32FFSCommonFSSaveNowIOCTRL_AsynWrite(
                                   (tPS8)OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_ASYNC
                                   ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileOpenCloseMultipleTime()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files accenting order.
* TEST CASE:    TU_OEDT_FFS3_CFS_089
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileOpenCloseMultipleTime(tVoid)
{
    tU32 u32Ret; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTime
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1100,                                            //file count
        1,                                              //dir count
        (tS8*)OEDT_TEST_FILE                                 //file name
    ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileOpenCloseMultipleTimeRandom1()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files random order.
        Random order means 0 - 750 and 1500 - 750.
* TEST CASE:    TU_OEDT_FFS3_CFS_090
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeRandom1(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTimeRandom1
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1500,                                            //file count
        1,                                              //dir count
        (tS8*)OEDT_TEST_FILE                                 //file name
    ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileOpenCloseMultipleTimeRandom2()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 14 and 
        opens these files accenting order.
        Random order means 0 - 750 and 750 - 1500.
* TEST CASE:    TU_OEDT_FFS3_CFS_091
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeRandom2(tVoid)
{
    tU32 u32Ret; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTimeRandom2
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1500,                                            //file count
        1,                                               //dir count
        (tS8*)OEDT_TEST_FILE                                 //file name                
    ); 
    return u32Ret; 
}

/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileLongFileOpenCloseMultipleTime()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files accenting order.
* TEST CASE:    TU_OEDT_FFS3_CFS_092
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTime(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTime
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1100,                                            //file count
        1,                                              //dir count
        (tS8*)OEDT_TEST_LONG_FILE                        //file name
    ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files random order.
        Random order means 0 - 750 and 1500 - 750.
* TEST CASE:    TU_OEDT_FFS3_CFS_093
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTimeRandom1
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1500,                                            //file count
        1,                                              //dir count
        (tS8*)OEDT_TEST_LONG_FILE                        //file name
    ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
        This test has the test data of 1500 files with file name length 40 and 
        opens these files accenting order.
        Random order means 0 - 750 and 750 - 1500.
* TEST CASE:    TU_OEDT_FFS3_CFS_094
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTimeRandom2
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1500,                                            //file count
        1,                                               //dir count
        (tS8*)OEDT_TEST_LONG_FILE                        //file name                        
    ); 
    return u32Ret; 
}
/*****************************************************************************
* FUNCTION:     u32FFS3_CommonFileOpenCloseMultipleTimeMultDir()
* PARAMETER:    none 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Test case is used to measure the time taken for open the file. 
                This test has the test data of three chain directory contains 
                1500 files with file name length 14 and opens these files 
                accenting order.
* TEST CASE:    TU_OEDT_FFS3_CFS_095
* HISTORY:    Created by Anoop Chandran on 16 Mar, 2010
******************************************************************************/
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeMultDir(tVoid)
{
    tU32 u32Ret ; // u32Ret to identify the error

    u32Ret = u32FSFileOpenCloseMultipleTime
    (
       (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, //device name
        TRUE,                                            //creae files
        1100,                                            //file count
        3,                                              //dir count
        (tS8*)OEDT_TEST_FILE                                 //file name
    ); 
    return u32Ret; 
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetFileSize()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSGetFileSize(tVoid)
{
    return u32FSGetFileSize((tS8*)OEDT_C_STRING_FFS3_FILE1);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadDirValidate()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSReadDirValidate(tVoid)
{
    return u32FSReadDirValidate((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRmRecursiveCancel()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSRmRecursiveCancel(tVoid)
{
    return u32FSRmRecursiveCancel((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateManyThreadDeleteMain()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSCreateManyThreadDeleteMain(tVoid)
{
    return u32FSCreateManyThreadDeleteMain((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateManyDeleteInThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSCreateManyDeleteInThread(tVoid)
{
    return u32FSCreateManyDeleteInThread((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenManyCloseInThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSOpenManyCloseInThread(tVoid)
{
    return u32FSOpenManyCloseInThread((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenInThreadCloseMain()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSOpenInThreadCloseMain(tVoid)
{
    return u32FSOpenInThreadCloseMain((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteMainReadInThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteMainReadInThread(tVoid)
{
    return u32FSWriteMainReadInThread((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSWriteThreadReadInMain()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSWriteThreadReadInMain(tVoid)
{
    return u32FSWriteThreadReadInMain((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileAccInDiffThread()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSFileAccInDiffThread(tVoid)
{
    return u32FSFileAccInDiffThread((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadDirExtPartByPart()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSReadDirExtPartByPart(tVoid)
{
    return u32FSReadDirExtPartByPart((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadDirExt2PartByPart()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSReadDirExt2PartByPart(tVoid)
{
    return u32FSReadDirExt2PartByPart((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCopyDirTest()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* HISTORY       :   05 Jan 2016 :  CF3PF-3742 - As source & destination specified
                    in previous versions were same, dev_name[1] is changed to 
                    OEDTTEST_C_STRING_DEVICE_DYNAMIC.
******************************************************************************/  
tU32 u32FFS3_CommonFSCopyDirTest(tVoid)
{
    tU32 u32Ret;
    tPS8 dev_name[2] ;
    dev_name[0]= (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]= (tPS8)OEDTTEST_C_STRING_DEVICE_DYNAMIC;

    u32Ret = u32FSCopyDirTest(dev_name,TRUE);
    return u32Ret;
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReadWriteHugeData()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSReadWriteHugeData(tVoid)
{
    return u32FSReadWriteHugeData((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, 0x55);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRW_Performance_Diff_BlockSize()
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
******************************************************************************/  
tU32 u32FFS3_CommonFSRW_Performance_Diff_BlockSize(tVoid)
{
    return u32FSRW_Performance_Diff_BlockSize((const tPS8)OEDT_C_STRING_FFS3_FILE1);
}

/*****************************************************************************
* FUNCTION      : FFS3_CommonFSStressTest()
* DESCRIPTION   : Execute vFSStressTest() on device FFS3.
* PARAMETER     : None.
* RETURNVALUE   : OEDT_TESTSTATE teststate.
* HISTORY       : 30.08.2012 - CM-AI/PJ-CF33-Kalms - Initial version.
******************************************************************************/
 tU32 FFS3_CommonFSStressTest(tVoid)
{
  OEDT_TESTSTATE teststate = OEDT_CREATE_TESTSTATE();

  vFSStressTest((const tPS8)OEDTTEST_C_STRING_DEVICE_FFS3, &teststate);

  return teststate.error_code;
}

/* EOF */
