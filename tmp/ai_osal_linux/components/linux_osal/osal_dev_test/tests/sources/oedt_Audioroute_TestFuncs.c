/********************************************************************************
FILE         : oedt_Audioroute_TestFuncs.c
*
*SW-COMPONENT : OEDT_FrmWrk
*
*DESCRIPTION  : This file implements the  test cases for validating the audio routes in
                Gen3 Projects. The tests are run on Base SW Platform.
                The alsa device names have been taken from asound.project.conf file.
                New test cases will be added in case any new alsa device is added to asound.project.conf file.
*
*AUTHOR       : Kranthi Kiran (RBEI/ECF5)
*
*HISTORY:      
*------------------------------------------------------------------------
* Date        |       Version       | Author & comments
*------------ |---------------------|--------------------------------------
* 19 Aug,2015 |    Initial Version 1.0 | Kranthi Kiran(RBEI/ECF5)
*---------------------------------------------------------------------------------
* 02 Mar,2016 |                     - The tests are made available for LSIM as well.
*             |                     - To support with the PC sound cards modifications have been done.
*                                   |  Kranthi Kiran (RBEI/ECF5)
**********************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#define OEDT_AUDIOROUTE_PRINTF_INFO(_X_) printf _X_
#include <stddef.h>
#include "oedt_Audioroute_TestFuncs.h"
#include <alsa/asoundlib.h>
#include "osal_if.h"
#include "osproc.h"


extern tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec);


/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*
********************************************************************************
* FUNCTION        : vWriteToAlsa 
* PARAMETER       : tPVoid: A pointer of type OsalAlsaParameter is passed
* RETURNVALUE     : No return value
* DESCRIPTION     : This function writes audio data from a local file to Alsa                        
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
* 19 Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 |                     | To support with the PC sound card, wait time out is increased.
*             |                     - Kranthi Kiran (RBEI/ECF5)
**********************************************************************************/
static void vWriteToAlsa(tPVoid pvData )
{
   
       OsalAlsaParameter * TestDevice = (OsalAlsaParameter*) pvData;
       char * sDeviceName = TestDevice->DeviceName;
       unsigned int nChannels = (unsigned int)TestDevice->Channels;
       char * FilePath = TestDevice->FilePath;
       unsigned int nSampleRate = TestDevice->SampleRate;
       tU32 * pu32RetValue = &(TestDevice->RetValue); 
       unsigned int size;
       int pFile;
       char *Buffer;
       int iDirection = 0,err,iRet,sRet,fRet;
       snd_pcm_t* pAlsaPCMPtr;
       snd_pcm_uframes_t uiPeriodSize,framesBufferSize,framesPeriodSize,uAvailMin,uThreshStop,uThreshStart;
       snd_pcm_hw_params_t *hw_params;
       snd_pcm_sw_params_t *sw_params;
       OSAL_tMSecond startMS; // start time in milliseconds
       OSAL_tMSecond diffMS;
       tBool bExit = FALSE;
       *pu32RetValue = OSAL_OK;
   
       err = snd_pcm_open(&pAlsaPCMPtr,sDeviceName,SND_PCM_STREAM_PLAYBACK,SND_PCM_NONBLOCK);
       if(err < 0 )
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:unable to open pcm device (%s): %s\n",__FUNCTION__,sDeviceName,snd_strerror(err)));
           pAlsaPCMPtr = NULL;
           (void)pAlsaPCMPtr;// to fix lint
           *pu32RetValue = (tU32)OSAL_ERROR;
           return;
       }
       OEDT_AUDIOROUTE_PRINTF_INFO(("%s: The Device %s is opened with Playback Stream \n",__FUNCTION__,sDeviceName)); 
       
       /*Allocate hardware parameters object */	   
       snd_pcm_hw_params_alloca(&hw_params);/*lint !e717*/
       
       /*Check for hw_params to be valid */
       if (hw_params == NULL)
       {
          *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
       
       /* set default hw parameters */
       err = snd_pcm_hw_params_any( pAlsaPCMPtr, hw_params);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set default params (%s)\n",
                                  __FUNCTION__,
                                  snd_strerror (err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
   
       /* set access to SND_PCM_ACCESS_RW_INTERLEAVED */
       err = snd_pcm_hw_params_set_access(pAlsaPCMPtr,hw_params,SND_PCM_ACCESS_RW_INTERLEAVED );
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set access (%s)\n", __FUNCTION__,snd_strerror (err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
   
       /* set format SND_PCM_FORMAT_S16_LE */
       err = snd_pcm_hw_params_set_format(pAlsaPCMPtr,hw_params,SND_PCM_FORMAT_S16_LE);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set format (%s)\n", __FUNCTION__,snd_strerror (err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
   
       /* Set number of Channels */
       err = snd_pcm_hw_params_set_channels(pAlsaPCMPtr,hw_params,nChannels);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set Channels %u (%s)\n", __FUNCTION__,nChannels, snd_strerror (err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
   
       /* set sampling rate */
       err = snd_pcm_hw_params_set_rate_near(pAlsaPCMPtr,hw_params,&nSampleRate,&iDirection);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set Rate %u (%s)\n", __FUNCTION__,nSampleRate, snd_strerror (err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
       OEDT_AUDIOROUTE_PRINTF_INFO(("Sample Rate set is : (%u) \n",nSampleRate));
       iDirection = 0; //to set to nearest value
   
       /* set period size */
       framesPeriodSize = (nSampleRate * 16 /1000); //try to set period time as 16 ms

       err = snd_pcm_hw_params_set_period_size_near(pAlsaPCMPtr,hw_params,&framesPeriodSize,&iDirection);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s: ERROR: cannot set period size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }

       framesBufferSize = (framesPeriodSize * 2); //number of periods per buffer set to 2
       err = snd_pcm_hw_params_set_buffer_size_near(pAlsaPCMPtr,hw_params,&framesBufferSize);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s: ERROR: cannot set Buffer size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
       
       /*Write the hardware parameters to the PCM device */
       err = snd_pcm_hw_params(pAlsaPCMPtr,hw_params);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:unable to set hw parameters: %s \n",__FUNCTION__,snd_strerror(err) ));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
   
       iDirection = 0; //to set to nearest value
       err = snd_pcm_hw_params_get_period_size(hw_params, &uiPeriodSize, &iDirection);
       OEDT_AUDIOROUTE_PRINTF_INFO(("%s:The Period Size is (%u frames)\n ",__FUNCTION__,(unsigned int)uiPeriodSize));
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: cannot get periods",__FUNCTION__));
       }
   
       /*Get the default Software Parameters for the PCM device */

       snd_pcm_sw_params_alloca(&sw_params);/*lint !e717*/
       /*Check for sw_params to be valid */
       if (sw_params == NULL)
       {
          *pu32RetValue = (tU32)OSAL_ERROR;
          snd_pcm_close(pAlsaPCMPtr);
          return;
       }
       err = snd_pcm_sw_params_current(pAlsaPCMPtr, sw_params);
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to determine "
                                  "current sw_params for playback: %s \n",
                                  __FUNCTION__,
                                  snd_strerror(err)));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
       err = snd_pcm_sw_params_get_avail_min(sw_params,&uAvailMin);
       OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of    avail min is (%u frames ) \n",__FUNCTION__,(unsigned int)uAvailMin));
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get avail min ",__FUNCTION__));
       }
       err = snd_pcm_sw_params_get_start_threshold(sw_params, &uThreshStart);
       OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of  start_treshold is (%u frames )\n",__FUNCTION__,(unsigned int)uThreshStart));
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get start_treshold ",__FUNCTION__));
       }
       err = snd_pcm_sw_params_get_stop_threshold(sw_params, &uThreshStop);
       OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of  stop_treshold is (%u frames ) \n",__FUNCTION__,(unsigned int)uThreshStop));
       if(err < 0)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get stop_treshold \n",__FUNCTION__));
       }
   
       pFile = open(FilePath,O_RDONLY);
       if(pFile == -1)
       {
           OEDT_AUDIOROUTE_PRINTF_INFO(("ERROR : The file cannot be opened \n"));
           *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
       else
       {
           /* Use a buffer large enough to hold one period */
           size = (uiPeriodSize * 2 * nChannels); // 2 bytes for each sample
           Buffer = (char *)malloc(size);
   
           /* loop for 20 seconds */
   
           startMS = OSAL_ClockGetElapsedTime();
         
           do{
           
              #ifdef GEN3X86
              /* In order to support with PC Sound cards, the wait time has been increased to 1000*/
              iRet = snd_pcm_wait(pAlsaPCMPtr,1000);
              #else
              iRet = snd_pcm_wait(pAlsaPCMPtr,100);
              #endif

              if(iRet == 0)
                {
                    OEDT_AUDIOROUTE_PRINTF_INFO(("Time out ERROR \n"));
                    *pu32RetValue = (tU32)OSAL_ERROR;
                    bExit = TRUE;
                }
              else if(iRet < 0)
                {
                  iRet = iHandleXRUN(pAlsaPCMPtr, iRet);
                  if(iRet < 0)
                  {
                    OEDT_AUDIOROUTE_PRINTF_INFO(("XRUN Could not be recovered \n"));
                    bExit = TRUE;
                  }
                }
            
            if(iRet > 0)
            {
                fRet = read(pFile,Buffer,size);
                if(fRet == 0)
                    {
                        OEDT_AUDIOROUTE_PRINTF_INFO(("End of the file reached \n"));
                        bExit = TRUE;
                    }
                
                else if (fRet != (int)size)
                    {
                        OEDT_AUDIOROUTE_PRINTF_INFO(("Short Read : Read %d bytes \n",fRet));
                    }
                sRet = snd_pcm_writei(pAlsaPCMPtr,Buffer,uiPeriodSize);
                if(sRet >= 0)
                    {
                        OEDT_AUDIOROUTE_PRINTF_INFO(("Bytes written to alsa is %d\n",sRet));
                    }
                
               else if (sRet < 0) 
                {
                    iRet = iHandleXRUN(pAlsaPCMPtr,sRet);
                    if(iRet < 0)
                    {
                        OEDT_AUDIOROUTE_PRINTF_INFO(("error from writei: %s\n",snd_strerror(iRet)));
                        *pu32RetValue = (tU32)OSAL_ERROR;
                        bExit = TRUE;
                    }
                }
           }
        diffMS = OSAL_ClockGetElapsedTime() - startMS;
        if(diffMS > 10000)//10 sec time
            {
                OEDT_AUDIOROUTE_PRINTF_INFO(("The time 10sec is over:) \n"));
                bExit = TRUE;
            }
         }while(!bExit);
         close(pFile);
         snd_pcm_drain(pAlsaPCMPtr);
         snd_pcm_close(pAlsaPCMPtr);
         free(Buffer);  
       }

   }
/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*
********************************************************************************
* FUNCTION        : vReadFromAlsa 
* PARAMETER       : tPVoid: A pointer of type OsalAlsaParameter is passed
* RETURNVALUE     : No return value
* DESCRIPTION     : This function reads audio data from ALsa and writes the data
                             to a local file.
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
* 19 Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 |                     | To support with the PC sound card, wait time out is increased.
*             |                     - Kranthi Kiran (RBEI/ECF5)
**********************************************************************************/
static void vReadFromAlsa(tPVoid pvData)
{
	OsalAlsaParameter * TestDevice = (OsalAlsaParameter*) pvData;
	char * sDeviceName = TestDevice->DeviceName;
	unsigned int nChannels = (unsigned int)TestDevice->Channels;
	char * FilePath = TestDevice->FilePath;
	unsigned int nSampleRate = TestDevice->SampleRate;
	tU32 * pu32RetValue = &(TestDevice->RetValue); 
    unsigned int size;
	int pFile;
    char *Buffer;
    size_t nBytesReadFromAlsa = 0;
    int iDirection = 0,err,iRet=0,sRet,fRet;
    snd_pcm_t* pAlsaPCMPtr;
    snd_pcm_uframes_t uiPeriodSize,framesBufferSize,framesPeriodSize,uAvailMin,uThreshStop,uThreshStart;
    snd_pcm_hw_params_t *hw_params;
    snd_pcm_sw_params_t *sw_params;
    OSAL_tMSecond startMS; // start time in milliseconds
    OSAL_tMSecond diffMS;
    tBool bExit = FALSE;
    *pu32RetValue = OSAL_OK;

    err = snd_pcm_open(&pAlsaPCMPtr,sDeviceName,SND_PCM_STREAM_CAPTURE,SND_PCM_NONBLOCK);
    if(err < 0 )
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:unable to open pcm device (%s): %s\n",__FUNCTION__,sDeviceName,snd_strerror(err)));
        pAlsaPCMPtr = NULL;
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }
    OEDT_AUDIOROUTE_PRINTF_INFO(("%s: The Device %s is opened with Capture Stream \n",__FUNCTION__,sDeviceName)); 

    /*Allocate hardware parameters object */
    snd_pcm_hw_params_alloca(&hw_params);/*lint !e717*/
    
    /*Check for hw_params to be valid */
    if (hw_params == NULL)
    {
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /* set default hw parameters */
    err = snd_pcm_hw_params_any( pAlsaPCMPtr, hw_params);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set default params (%s)\n",
                               __FUNCTION__,
                               snd_strerror (err)));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /* set access to SND_PCM_ACCESS_RW_INTERLEAVED */
    err = snd_pcm_hw_params_set_access(pAlsaPCMPtr,hw_params,SND_PCM_ACCESS_RW_INTERLEAVED );
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set access (%s)\n", __FUNCTION__,snd_strerror (err)));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /* set format SND_PCM_FORMAT_S16_LE */
    err = snd_pcm_hw_params_set_format(pAlsaPCMPtr,hw_params,SND_PCM_FORMAT_S16_LE);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set format (%s)\n", __FUNCTION__,snd_strerror (err)));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /* Set number of Channels */
    err = snd_pcm_hw_params_set_channels(pAlsaPCMPtr,hw_params,nChannels);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set Channels %u (%s)\n", __FUNCTION__,nChannels, snd_strerror (err)));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /* set sampling rate */
    err = snd_pcm_hw_params_set_rate_near(pAlsaPCMPtr,hw_params,&nSampleRate,&iDirection);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:cannot set Rate %u (%s)\n", __FUNCTION__,nSampleRate, snd_strerror (err)));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }
    iDirection = 0; //to set to nearest value

    /* set period size */
    framesPeriodSize = (nSampleRate*16/1000);//to set period time as 16 ms
    err = snd_pcm_hw_params_set_period_size_near(pAlsaPCMPtr,hw_params,&framesPeriodSize,&iDirection);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: ERROR: cannot set period size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /*set buffer size */
    framesBufferSize = (framesPeriodSize * 2); //number of periods per buffer set to 2
    err = snd_pcm_hw_params_set_buffer_size_near(pAlsaPCMPtr,hw_params,&framesBufferSize);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: ERROR: cannot set Buffer size(%u) \n ", __FUNCTION__,(unsigned int)framesPeriodSize));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    /*Write the hardware parameters to the PCM device */
    err = snd_pcm_hw_params(pAlsaPCMPtr,hw_params);
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s:unable to set hw parameters: %s \n",__FUNCTION__,snd_strerror(err) ));
        *pu32RetValue = (tU32)OSAL_ERROR; 
        snd_pcm_close(pAlsaPCMPtr);
        return;
    }

    iDirection = 0; //to set to nearest value
    err = snd_pcm_hw_params_get_period_size(hw_params, &uiPeriodSize, &iDirection);
    OEDT_AUDIOROUTE_PRINTF_INFO(("%s:The Period Size is (%u frames)\n ",__FUNCTION__,(unsigned int)uiPeriodSize));
    if(err < 0)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: cannot get period size",__FUNCTION__));
    } 

    /*Get the default Software Parameters for the PCM device */
   
        snd_pcm_sw_params_alloca(&sw_params);/*lint !e717*/ 
        /*Check for sw_params to be valid */
        if (sw_params == NULL)
       {
          *pu32RetValue = (tU32)OSAL_ERROR;
           snd_pcm_close(pAlsaPCMPtr);
           return;
       }
        err = snd_pcm_sw_params_current(pAlsaPCMPtr, sw_params);
        if(err < 0)
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to determine "
                                   "current sw_params for playback: %s \n",
                                   __FUNCTION__,
                                   snd_strerror(err)));
             *pu32RetValue = (tU32)OSAL_ERROR;
             snd_pcm_close(pAlsaPCMPtr);
             return;
        }
        err = snd_pcm_sw_params_get_avail_min(sw_params,&uAvailMin);
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of	avail min is (%u frames ) \n",__FUNCTION__,(unsigned int)uAvailMin));
        if(err < 0)
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get avail min ",__FUNCTION__));
        }
        err = snd_pcm_sw_params_get_start_threshold(sw_params, &uThreshStart);
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of  start_treshold is (%u frames )\n",__FUNCTION__,(unsigned int)uThreshStart));
        if(err < 0)
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get start_treshold ",__FUNCTION__));
        }
        err = snd_pcm_sw_params_get_stop_threshold(sw_params, &uThreshStop);
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: INFO: The value of  stop_treshold is (%u frames ) \n",__FUNCTION__,(unsigned int)uThreshStop));
        if(err < 0)
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s:Unable to get stop_treshold \n",__FUNCTION__));
        }

    pFile = open(FilePath,O_APPEND | O_RDWR | O_CREAT);
    if(pFile == -1)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("ERROR : The file cannot be opened \n"));
        *pu32RetValue = (tU32)OSAL_ERROR;
        snd_pcm_close(pAlsaPCMPtr);
        return;    
    }
    else
    {
        /* Use a buffer large enough to hold one period */
        size = (uiPeriodSize * 2 * nChannels); // 2 bytes per sample and nChannels
        Buffer = (char *)malloc(size);

        /* loop for 20 seconds */
        startMS = OSAL_ClockGetElapsedTime();
       do
        {
         sRet = snd_pcm_readi(pAlsaPCMPtr,Buffer,uiPeriodSize);
         if(sRet >= 0)
         {
             nBytesReadFromAlsa = (size_t)snd_pcm_frames_to_bytes(pAlsaPCMPtr,sRet);
             OEDT_AUDIOROUTE_PRINTF_INFO(("Bytes Read From Alsa %u\n",nBytesReadFromAlsa));
             fRet = (int)write(pFile,Buffer,size);
             if (fRet != (int)size)
                {
                    OEDT_AUDIOROUTE_PRINTF_INFO(("short write: wrote %d bytes\n", fRet));
                }
         }
         else
         {
             iRet = iHandleXRUN(pAlsaPCMPtr, iRet);
             if(iRet < 0)
             {
                OEDT_AUDIOROUTE_PRINTF_INFO(("XRUN Could not be recovered \n"));
                *pu32RetValue = (tU32)OSAL_ERROR;
                bExit = TRUE;
                
             }
             else
             {
                #ifdef GEN3X86
                iRet = snd_pcm_wait(pAlsaPCMPtr,1000);
                #else
                iRet = snd_pcm_wait(pAlsaPCMPtr,100);
                #endif
                if(iRet == 0)
                     {
                         OEDT_AUDIOROUTE_PRINTF_INFO(("Time out ERROR \n"));
                         *pu32RetValue = (tU32)OSAL_ERROR;
                         bExit = TRUE;
                     }
             }

          }
          diffMS = OSAL_ClockGetElapsedTime() - startMS;
          if(diffMS > 10000)//10 sec time
            {
                OEDT_AUDIOROUTE_PRINTF_INFO(("The time 10sec is over:) \n"));
                bExit = TRUE;
            }

      }while(!bExit);
      close(pFile);
      snd_pcm_drain(pAlsaPCMPtr);
      snd_pcm_close(pAlsaPCMPtr);
      free(Buffer);
    }
}
/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*
***************************************************************************
*  FUNCTION:      iHandleXRUN
*  @brief         try to recover XRUN
*  PARAMETERS :    iRet - ALSA error from previous call
*                  AlsaPCM - Handle of the Alsa PCM device
*   RETURN :       new ALSA error code after revocering
*****************************************************************************
**------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* ----------------------------------------------------------------------- */

static int iHandleXRUN(snd_pcm_t* AlsaPCM,int iRet)
{
    if(iRet >= 0)
    return iRet;

    // no error, return previous error code
    OEDT_AUDIOROUTE_PRINTF_INFO(("XRUN Occured. Try to handle the return value %d ",iRet));
    if(iRet == -EAGAIN)
    {
        usleep(2000); // avoid a bug in ALSAlib 1.0.25
        return 0;
    }
    iRet = snd_pcm_recover(AlsaPCM,iRet,0);
    if(iRet == 0)
    {
        iRet = snd_pcm_start(AlsaPCM);
        if(iRet == 0)
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s: Successfully Restarted from XRUN ",__FUNCTION__));   
        }
        else
        {
            OEDT_AUDIOROUTE_PRINTF_INFO(("%s: snd_pcm_start error	",__FUNCTION__)); 
        }
    }
    else 
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("%s: snd_pcm_recover error [%d]",__FUNCTION__, iRet));
    } 
    return iRet;
}

tU32 OEDT_AUDIO_ROUTE_TEST_00(void)
{
    OEDT_AUDIOROUTE_PRINTF_INFO((" To run these OEDTs use /etc/asound.audioroutetesting_XXX.conf file  \n"));
	OEDT_AUDIOROUTE_PRINTF_INFO((" In /usr/share/alsa/alsa.conf replace the file path '/etc/asound.conf' with '/etc/asound.audioroutetesting_XXX.conf'\n"));
    OEDT_AUDIOROUTE_PRINTF_INFO((" For SUZUKI    : /etc/asound.audioroutetesting_SUZUKI.conf file \n"));
    OEDT_AUDIOROUTE_PRINTF_INFO((" For AIVI      : /etc/asound.audioroutetesting_AIVI.conf file \n"));
    OEDT_AUDIOROUTE_PRINTF_INFO((" For GMMY16/17 : /etc/asound.audioroutetesting_GMMY16.conf file \n"));
    OEDT_AUDIOROUTE_PRINTF_INFO((" For LSIM      : /etc/asound.audioroutetesting_LSIM.conf file \n"));

}


/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_01
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevEnt1Out",
                  which is a Playback device with channels 2
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
* Playback Alsa device: AdevEnt1Out
* definition stream: stereo, 16 bits, fs: 8-96ksps
* definition src : HW ASRC , clockOut: 48ksps , clockIn: 8-96 ksps
* definition Sink : adr3_ESAI_TDM channel Tx1/Tx5
* definition binding 
*******************************************************************************/

tU32 OEDT_AUDIO_ROUTE_TEST_01(void)
{
    OsalAlsaParameter EntertainmentDevice;
    EntertainmentDevice.DeviceName = OEDT_PCM_DEVICE_AdevEnt1Out;
    EntertainmentDevice.Channels = STEREO;
    EntertainmentDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    EntertainmentDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    vWriteToAlsa(&EntertainmentDevice);
    return EntertainmentDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_02
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAudioSampler1Out", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
 * Playback Alsa device: AdevAudioSampler1Out
* definition stream: mono, 16 bits, fs: 8-48ksps
* definition src : SW SRC , clockOut: 48ksps , clockIn: 8-48ksps 
* definition Senk : adr3_ESAI_TDM channel Tx2
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_02(void)
{
    OsalAlsaParameter AudioSampler1OutDevice;
    AudioSampler1OutDevice.DeviceName = OEDT_PCM_DEVICE_AdevAudioSampler1Out;
    #ifdef GEN3X86
    AudioSampler1OutDevice.Channels = STEREO;
    AudioSampler1OutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    AudioSampler1OutDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    #else
    AudioSampler1OutDevice.Channels = MONO;
    AudioSampler1OutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE;
    AudioSampler1OutDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    vWriteToAlsa(&AudioSampler1OutDevice);
	#endif
    return AudioSampler1OutDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_03
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAudioSampler2Out", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | To comply with the device definition on LSIM the changes are done.
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
* Playback Alsa device: AdevAudioSampler2Out
* definition stream: mono, 16 bits, fs: 8-48ksps
* definition src : SW SRC , clockOut: 48ksps , clockIn: 8-48ksps
* definition Senk : adr3_ESAI_TDM channel Tx3
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_03(void)
{
    OsalAlsaParameter AudioSampler2OutDevice;
    AudioSampler2OutDevice.DeviceName = OEDT_PCM_DEVICE_AdevAudioSampler2Out;
    #ifdef GEN3X86
    AudioSampler2OutDevice.Channels = STEREO;
    AudioSampler2OutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    AudioSampler2OutDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    #else
    AudioSampler2OutDevice.Channels = MONO;
    AudioSampler2OutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE ;
    AudioSampler2OutDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    #endif
    vWriteToAlsa(&AudioSampler2OutDevice);
    return AudioSampler2OutDevice.RetValue;
}
 
/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_04
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAcousticoutSpeech", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | To comply with the device definition on LSIM the changes are done.
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
* Playback Alsa device: AdevAcousticoutSpeech
* definition stream: mono, 16 bits, fs: OEDT_SAMPLERATE_MONOksps
* definition src : HW src , clockOut: 48ksps , clockIn: OEDT_SAMPLERATE_MONOksps
* definition Senk : adr3_ESAI_TDM channel Tx6
******************************************************************************/

tU32 OEDT_AUDIO_ROUTE_TEST_04(void)
{
    OsalAlsaParameter AcousticoutSpeechDevice;
    AcousticoutSpeechDevice.DeviceName = OEDT_PCM_DEVICE_AdevAcousticoutSpeech;
    #ifdef GEN3X86
    AcousticoutSpeechDevice.Channels = STEREO;
    AcousticoutSpeechDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    AcousticoutSpeechDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    #else
    AcousticoutSpeechDevice.Channels = MONO;
    AcousticoutSpeechDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE ;
    AcousticoutSpeechDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    #endif
    vWriteToAlsa(&AcousticoutSpeechDevice);
    return AcousticoutSpeechDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_05
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAcousticinSpeech", 
                  which is a Capture device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
* Capture Alsa device: AdevAcousticinSpeech
* definition stream: mono, 16 bits, fs: 16ksps
* definition src : SW SRC , clockOut: 16ksps , clockIn: 48ksps
* definition Senk : adr3_ESAI_TDM channel Rcv6
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_05(void)
{
    OsalAlsaParameter AcousticinSpeechDevice;
    AcousticinSpeechDevice.DeviceName = OEDT_PCM_DEVICE_AdevAcousticinSpeech;
    AcousticinSpeechDevice.Channels = MONO;
    AcousticinSpeechDevice.FilePath = OEDT_FILEPATH_CAPTURE_DEVICE;
    AcousticinSpeechDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    vReadFromAlsa(&AcousticinSpeechDevice);
    return AcousticinSpeechDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_06
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevVoiceOut", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | To comply with the device definition on LSIM the changes are done.
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
* Playback Alsa device: AdevVoiceOut
* definition stream: mono, 16 bits, fs: 8/16ksps
* definition src : SW src , clockOut: 48ksps , clockIn: 8/16ksps
* definition Senk : adr3_ESAI_TDM channel Tx2
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_06(void)
{
    OsalAlsaParameter VoiceOutDevice;
    VoiceOutDevice.DeviceName = OEDT_PCM_DEVICE_AdevVoiceOut;
    #ifdef GEN3X86
    VoiceOutDevice.Channels = STEREO;
    VoiceOutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    VoiceOutDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    #else
    VoiceOutDevice.Channels = MONO;
    VoiceOutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE ;
    VoiceOutDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    #endif
    vWriteToAlsa(&VoiceOutDevice);
    return VoiceOutDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_07
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevMicro1In", 
                  which is a Capture device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevMicro1In
 definition stream: 1 channel , 16 bits, fs: 8/16ksps
 definition src : HW src , clockOut: 8/16ksps , clockIn: 48ksps
 definition Senk : adr3_ESAI_TDM Rcv5 
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_07(void)
{
    OsalAlsaParameter Micro1InDevice;
    Micro1InDevice.DeviceName = OEDT_PCM_DEVICE_AdevMicro1In;
    Micro1InDevice.Channels = MONO;
    Micro1InDevice.FilePath = OEDT_FILEPATH_CAPTURE_DEVICE;
    Micro1InDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    vReadFromAlsa(&Micro1InDevice);
    return Micro1InDevice.RetValue;

}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_08
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAudioInfoMonoOut", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | To comply with the device definition on LSIM the changes are done.
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevAudioInfoMonoOut
 definition stream: 1 channel , 16 bits, fs: 8/16ksps
 definition src : SW src , clockOut: 48ksps , clockIn: 8-96ksps
 definition Senk : adr3_ESAI_TDM Tx6 
******************************************************************************/
tU32 OEDT_AUDIO_ROUTE_TEST_08(void)
{
    OsalAlsaParameter AudioInfoMonoOutDevice;
    AudioInfoMonoOutDevice.DeviceName = OEDT_PCM_DEVICE_AdevAudioInfoMonoOut;
    #ifdef GEN3X86
    AudioInfoMonoOutDevice.Channels = STEREO;
    AudioInfoMonoOutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    AudioInfoMonoOutDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    #else
    AudioInfoMonoOutDevice.Channels = MONO;
    AudioInfoMonoOutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE ;
    AudioInfoMonoOutDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    #endif
    vWriteToAlsa(&AudioInfoMonoOutDevice);
    return AudioInfoMonoOutDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_09
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevAudioSampleStereo", 
                  which is a Playback device with channels 2
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevAudioSampleStereo
 definition stream: 2 channel , 16 bits, fs: 8/16ksps
 definition src : SW src , clockOut: 48ksps , clockIn: 8-96ksps
 definition Senk : adr3_ESAI_TDM Tx6, adr3_ESAI_TDM Tx2 
******************************************************************************/

tU32 OEDT_AUDIO_ROUTE_TEST_09(void)
{
    OsalAlsaParameter AudioSampleStereoDevice;
    AudioSampleStereoDevice.DeviceName = OEDT_PCM_DEVICE_AdevAudioSampleStereo;
    AudioSampleStereoDevice.Channels = STEREO;
    AudioSampleStereoDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    AudioSampleStereoDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    vWriteToAlsa(&AudioSampleStereoDevice);
    return AudioSampleStereoDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_10
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevBTAudio", 
                  which is a Playback device with channels 2
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | The device definition on LSIM is not valid. Hence this test is excluded
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevBTAudio
 definition stream: stereo, 16 bits, fs: 8-96ksps
 definition src : HW ASRC , clockOut: 48ksps , clockIn: 8-96 ksps
 definition Senk : adr3_ESAI_TDM channel Tx1/Tx5
 definition binding 
******************************************************************************/
#ifndef GEN3X86
tU32 OEDT_AUDIO_ROUTE_TEST_10(void)
{
    OsalAlsaParameter BTAudioDevice;
    BTAudioDevice.DeviceName = OEDT_PCM_DEVICE_AdevBTAudio;
    BTAudioDevice.Channels = STEREO;
    BTAudioDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    BTAudioDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    vWriteToAlsa(&BTAudioDevice);
    return BTAudioDevice.RetValue;
}
#endif

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_11
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevMPOut", 
                  which is a Playback device with channels 2
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevMPOut
 definition stream: stereo, 16 bits, fs: 8-96ksps
 definition src : HW ASRC , clockOut: 48ksps , clockIn: 8-96 ksps
 definition Sink : adr3_ESAI_TDM channel Tx1/Tx5
******************************************************************************/

tU32 OEDT_AUDIO_ROUTE_TEST_11(void)
{
    OsalAlsaParameter MPOutDevice;
    MPOutDevice.DeviceName = OEDT_PCM_DEVICE_AdevMPOut;
    MPOutDevice.Channels = STEREO;
    MPOutDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    MPOutDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    vWriteToAlsa(&MPOutDevice);
    return MPOutDevice.RetValue;
}

/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_12
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevMicro12AmpRef", 
                  which is a Capture device with channels 3
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | The device definition on LSIM is not valid. Hence this test is excluded
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
 Capture Alsa device: AdevMicro12AmpRef
 definition stream: 3 channel , 16 bits, fs: 8/16ksps
 definition src : SW SRC , clockOut: 8/16ksps , clockIn: 48ksps
 definition Senk : adr3_ESAI_TDM Rcv6/Rcv2/Rcv1 
******************************************************************************/
#ifndef GEN3X86
 tU32 OEDT_AUDIO_ROUTE_TEST_12(void)
{
    OsalAlsaParameter Micro12AmpRefDevice;
    Micro12AmpRefDevice.DeviceName = OEDT_PCM_DEVICE_AdevMicro12AmpRef;
    Micro12AmpRefDevice.Channels = MULTICHANNEL;
    Micro12AmpRefDevice.FilePath = OEDT_FILEPATH_CAPTURE_DEVICE;
    Micro12AmpRefDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    vReadFromAlsa(&Micro12AmpRefDevice);
    return Micro12AmpRefDevice.RetValue;

}
#endif
/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_13
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio route for the device "AdevEsaiClockOnOff", 
                  which is a Playback device with channels 1
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | The device definition on LSIM is not valid. Hence this test is excluded
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************
******************************************************************************
 Playback Alsa device: AdevEsaiClockOnOff
 definition stream: 1 channel , 16 bits, fs: 8/16ksps
 definition src : clockOut: 48ksps , clockIn: 
 definition Senk : adr3_ESAI_TDM Tx, adr3_ESAI_TDM Tx 
******************************************************************************/
#ifndef GEN3X86
tU32 OEDT_AUDIO_ROUTE_TEST_13(void)
{
    OsalAlsaParameter EsaiClockOnOffDevice;
    EsaiClockOnOffDevice.DeviceName = OEDT_PCM_DEVICE_AdevEsaiClockOnOff;
    EsaiClockOnOffDevice.Channels = MONO;
    EsaiClockOnOffDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE;
    EsaiClockOnOffDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    vWriteToAlsa(&EsaiClockOnOffDevice);
    return EsaiClockOnOffDevice.RetValue;
}
#endif
 
/*****************************************************************************
* FUNCTION		: tU32 OEDT_AUDIO_ROUTE_TEST_14
* PARAMETER		: None
* RETURNVALUE	: 0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	: To test the audio routes in Parallel Use case
                  where "AdevAcousticoutSpeech" and "AdevEnt1Out" are used
                  simultaneously
* HISTORY		:  
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19Aug,2015 | Initialversion 1.0  | KRANTHI KIRAN (RBEI/ECF5)
* -----------------------------------------------------------------------
* 02 Mar,2016 | To comply with the device definitions on LSIM the changes are done.
*             - Kranthi Kiran (RBEI/ECF5)
******************************************************************************/

tU32 OEDT_AUDIO_ROUTE_TEST_14(void)
{
    tU32 u32RetValue;
    OSAL_trThreadAttribute rThreadAttr;
    OSAL_tThreadID threadID;
    OsalAlsaParameter SpeechDevice,EntertainmentDevice;
    SpeechDevice.DeviceName = OEDT_PCM_DEVICE_AdevAcousticoutSpeech;
    EntertainmentDevice.DeviceName = OEDT_PCM_DEVICE_AdevEnt1Out;
    #ifdef GEN3X86
    SpeechDevice.Channels = STEREO;
    SpeechDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    SpeechDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE;
    #else
    SpeechDevice.Channels = MONO;
    SpeechDevice.SampleRate = OEDT_SAMPLERATE_MONO;
    SpeechDevice.FilePath = OEDT_FILEPATH_PLAYBACK_MONO_DEVICE;
    #endif
    EntertainmentDevice.Channels = STEREO;
    EntertainmentDevice.SampleRate = OEDT_SAMPLERATE_STEREO;
    EntertainmentDevice.FilePath = OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE ;


    /* Spawn a thread to play the second device */
    rThreadAttr.szName = "Entertainment Playback Thread";
    rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThreadAttr.pfEntry = vWriteToAlsa;
    rThreadAttr.pvArg = &EntertainmentDevice;
    rThreadAttr.s32StackSize = 65536;
    threadID = OSAL_ThreadSpawn(&rThreadAttr);
    if(OSAL_ERROR == threadID)
    {
        OEDT_AUDIOROUTE_PRINTF_INFO(("Entertainment Playback Thread Creation Failed ! Thread ID : %d \n",threadID));
        u32RetValue = (tU32)OSAL_ERROR;
    }
    else
    {
        vWriteToAlsa(&SpeechDevice);
        if(threadID != OSAL_ERROR)
            {
                OSAL_s32ThreadJoin(threadID, 1000);
                threadID = OSAL_ERROR;/*To invalidate the thread*/
                (void)threadID;
            }
        u32RetValue = SpeechDevice.RetValue | EntertainmentDevice.RetValue;
    }
    return u32RetValue;
}

#ifdef __cplusplus
}
#endif
