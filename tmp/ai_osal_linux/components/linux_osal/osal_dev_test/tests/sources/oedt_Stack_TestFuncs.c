/******************************************************************************
 *FILE         : oedt_Stack_TestFuncs.c
 *
 *SW-COMPONENT : ODT_FrmWrk
 *
 *DESCRIPTION  : This file implements test cases to check task
 *               stack overflow handling (> 64kB). 
 *
 *AUTHOR       : Dirk Behme
 *
 *COPYRIGHT    : (c) 2007 Blaupunkt Werke GmbH
 *
 *HISTORY      : 05.10.07  Rev. 1.0 CM-DI/PJ-CF31 - Dirk Behme
 *               Initial Revision.
 *      		Ported by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
                for ADIT platform
				Version - 1.1
 *****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "TEngine_osal.h"


#define OEDT_TEST_STACKSIZE 1024
static unsigned int stacktest_helper_finished = 0, stacktest_main_finished = 0;
#define HELPER_TEST_STACK (1024 * 3)

/*****************************************************************************
* FUNCTION:	    stacktest_helper_thread(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  None
* TEST CASE:    
* DESCRIPTION:  1. Helper Thread of the Test.
*                 
*               2. This thread tries to fill it's Stack 
*               3. The assumption is that Main thread might overwrites the stack
                   of Helper thread. Generally it won't happen.   
*				   When the main thread stack usage gets exceeded the limit, TLB 
                   exception occurs. 
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tVoid stacktest_helper_thread(tVoid)
{
      /* This 2k array on stack should be overwritten by other task */
      char test_array[HELPER_TEST_STACK];
      unsigned int i;
      
      for(i = 0; i < HELPER_TEST_STACK; i++)
          test_array[i] = 0xDB;
      
      stacktest_helper_finished = 1;
      
      (void) test_array; /* Make lint happy */
      
      for (;;)
      {
	  OSAL_s32ThreadWait (100);
      }; /*lint !e527  */
}

#define MAIN_TEST_ADDITIONAL_STACK (1024 * 191)
/*****************************************************************************
* FUNCTION:	    allocate_more_stack(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  None
* TEST CASE:    
* DESCRIPTION:  1. Tries to fill the Task Stack more than it's limit.
*                 
*               2.  Note: 
*					1. UsrStkSz only applies to sub-tasks 
*					(i.e. tasks created by tkse_cre_tsk).
*					2. The stack size is fixed for all kinds of 
*					   tasks and cannot be changed.
*					3. There is no special check of Stack Overflow 
*					   and that the exception is the normal TLB exception.
*					4.Maximum User Stack size is 252KB, if it gets exceeded
*					  TLB exception will occur
*                  
*
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tVoid allocate_more_stack(tVoid)
{
      char test_array1[MAIN_TEST_ADDITIONAL_STACK]= {0};
      
      
      /* System should do exception while next array is allocated, so
         signal success already here */
      stacktest_main_finished = 1;
	  /* Try to Use the Stack Space more than 252 KB */
      char test_array2[1024]= {0};
      
      (void) test_array1; /* Make lint happy */
	  (void) test_array2; /* Make lint happy */
}

#define MAIN_TEST_STACK (1024 * 60)
/*****************************************************************************
* FUNCTION:	    stacktest_main_thread(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  None
* TEST CASE:    
* DESCRIPTION:  1. Main Thread of the Test.
*                 
*               2. This thread tries to make the Stack Overflow. 
*                  
*
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/

tVoid stacktest_main_thread(tVoid)
{

      char test_array[MAIN_TEST_STACK];
      unsigned int i;
      
      /* wait until helper thread has filled his stack */
      while(!stacktest_helper_finished) {};
      
      for(i = 0; i < MAIN_TEST_STACK; i++)
          test_array[i] = i & 0xFF;
      
      allocate_more_stack();
      
      (void) test_array; /* Make lint happy */
      
      for (;;)
      {
	   OSAL_s32ThreadWait (100);
      }; /*lint !e527  */
}

/*****************************************************************************
* FUNCTION:	    vStackOverFlowTest(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  None
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                 
*               2. This test tries to make the Stack Overflow. 
*               3. Creates two Threads.   
*
* HISTORY:		Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tU32 vStackOverFlowTest(tVoid)
{
    OSAL_tThreadID r_stackMainThread, r_stackHelperThread;
    OSAL_trThreadAttribute rMainAttr, rHelperAttr;
    tC8 szMainThreadName[OSAL_C_U32_MAX_NAMELENGTH]   = "STKTST";
    tC8 szHelperThreadName[OSAL_C_U32_MAX_NAMELENGTH] = "STKHLP";
    
    rMainAttr.szName = szMainThreadName;
    rMainAttr.pfEntry = (OSAL_tpfThreadEntry) stacktest_main_thread;
    rMainAttr.s32StackSize = OEDT_TEST_STACKSIZE; 
    rMainAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rMainAttr.pvArg = (tPVoid *)NULL;
    
    rHelperAttr.szName = szHelperThreadName;
    rHelperAttr.pfEntry = (OSAL_tpfThreadEntry) stacktest_helper_thread;
    rHelperAttr.s32StackSize = OEDT_TEST_STACKSIZE; 
    rHelperAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rHelperAttr.pvArg = (tPVoid *)NULL;

    r_stackHelperThread = OSAL_ThreadSpawn(&rHelperAttr);
    if (OSAL_ERROR == r_stackHelperThread)
    {
       return 1;
    }
    
    r_stackMainThread = OSAL_ThreadSpawn(&rMainAttr);
    if (OSAL_ERROR == r_stackMainThread)
    {
       OSAL_s32ThreadDelete(r_stackHelperThread);
       stacktest_helper_finished = 0;
       stacktest_main_finished = 0;
       return 3;
    }
    
    if ( OSAL_ERROR == OSAL_s32ThreadWait ((OSAL_tMSecond) 1000) )
    {
       OSAL_s32ThreadDelete(r_stackHelperThread);
       OSAL_s32ThreadDelete(r_stackMainThread);
       stacktest_helper_finished = 0;
       stacktest_main_finished = 0; 
       return 5;
    }
    
    if((stacktest_helper_finished != 1) || (stacktest_main_finished != 1))
    {
       OSAL_s32ThreadDelete(r_stackHelperThread);
       OSAL_s32ThreadDelete(r_stackMainThread);
       stacktest_helper_finished = 0;
       stacktest_main_finished = 0; 
       return 6;
    }
    
    stacktest_helper_finished = 0;
    stacktest_main_finished = 0;
    
    if ( OSAL_ERROR == OSAL_s32ThreadDelete(r_stackMainThread))
    {
       OSAL_s32ThreadDelete(r_stackHelperThread);
       return 7;
    }
    
    if ( OSAL_ERROR == OSAL_s32ThreadDelete(r_stackHelperThread))
    {
       return 8;
    }

    return 0;
}


/* End of oedt_Stack_TestFuncs.c */
