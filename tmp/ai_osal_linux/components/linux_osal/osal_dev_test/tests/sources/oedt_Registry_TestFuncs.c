
/******************************************************************************
 *FILE         : oedt_Registry_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk(osal_dev_test) 
 *
 *DESCRIPTION  : This file implements the individual test cases for the Registry
 *               device	for GM-GE hardware.
 *               Corresponding Test spec: TU_OEDT_Registry_TestSpec.xls (draft v0.1)
 *
 *AUTHOR(s)    :  Pankaj Kumar(RBIN/EDI3)

 *COPYRIGHT    : (c) 2007 Robert Bosch India Limited (RBIN)

 *HISTORY      :  10-Oct-2007 v0.1 - Pankaj Kumar (RBIN/EDI3) - Initial Version
 *                16-Oct-2007 v0.2 - Pankaj Kumar (RBIN/EDI3)
 *                28-Mar-208	v0.3 - Shilpa Bhat (RBEI/ECM1) - Added new testcases;
 *                Removed testcases u32RegOpenCreateSearch, u32RegCreateCheck, 
 *                u32RegReadValue, u32RegNumValue, u32RegSetDeleteValue
 *                30.05.2008 v0.4 - Shilpa Bhat (RBEI/ECM1) - Added a new case
 *                29-Sept-2008 v0.5 - Shilpa Bhat (RBEI/ECM1)
 *                Removed testcases u32RegGetAllSubkeyNames, u32RegGetValueNames,
 *                u32RegGetValueInfo, u32RegGetVersionInvalParam
 *                25-Mar-2009 v0.6 - Sainath Kalpuri (RBEI/ECF1)
                  Removed lint and compiler warnings
 *                28-April-2009 v0.7 - Anoop Chandran (RBEI/ECF1)
                  update the test caes TU_OEDT_REGISTRY_020
 *                17-Feb-2015 v0.8 - Chandra Sekaran Mariappan (RBEI/ECF5)				  
 *                Modified for lint fix by including oedt_helper_funcs.h
*******************************************************************************/

#include "oedt_Registry_TestFuncs.h"
#include "oedt_helper_funcs.h"



/*****************************************************************************
* FUNCTION:		 u32RegDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_001
* DESCRIPTION:  Open and Close the Registry device
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
******************************************************************************/
tU32 u32RegDevOpenClose(void)
{ 
   OSAL_tIODescriptor hRegDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device in read-write mode */
 	hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY , enAccess );
	if ( OSAL_ERROR == hRegDevice )
	{
		/* Error status returned */
    	s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
        	case OSAL_E_DOESNOTEXIST:
				 u32Ret = 1;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret = 2;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret = 3;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret = 4;
				 break;
		    default:
				 u32Ret = 5;
		}
    }
    else
	 {
	 	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
			u32Ret += 7;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegDevOpenCloseDiffModes() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_002
* DESCRIPTION:  Open and Close the Registry device in different modes 
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
******************************************************************************/

tU32 u32RegDevOpenCloseDiffModes(tVoid) 
{
	OSAL_tIODescriptor hRegDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

 	/* Open the device in readwrite  mode */
 	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
   if ( OSAL_ERROR == hRegDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret = 1;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret = 2;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret = 3;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret = 4;
				 break;
		    default:
				 u32Ret = 5;
		}
   }
   else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
			u32Ret += 6;
		}
	}

	enAccess = OSAL_EN_READONLY;

 	/* Open the device in  read only mode */
 	hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
   if ( OSAL_ERROR == hRegDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret += 10;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret += 20;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret += 30;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret += 40;
				 break;
		    default:
				 u32Ret += 50;
		}    
	}
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
			u32Ret += 60;
		}
	}
	enAccess = OSAL_EN_WRITEONLY;

 	/* Open the device in  write  only mode */
 	hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
   if ( OSAL_ERROR == hRegDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret += 70;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret += 80;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret += 90;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret += 100;
				 break;
		    default:
				 u32Ret += 110;
		 }    
	}
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
	    	u32Ret += 600;
		}
	}
	return u32Ret;

}
   
/*****************************************************************************
* FUNCTION:		 u32RegDevOpenCloseInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_003																  
* DESCRIPTION:  Open and Close the Registry  device with Invalid Parameter
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
******************************************************************************/
tU32 u32RegDevOpenCloseInvalParam(void)
{
	OSAL_tIODescriptor hRegDevice = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_REGISTRY_INVALID_ACCESS_MODE;

 	/* Open the device with invalid access mode */
 	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
   if ( OSAL_ERROR !=hRegDevice )
   {
    	u32Ret = 1;
    	/* If successfully opened, indicate error and close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
			u32Ret += 2;
		}
	}
	else
	{

     	/* Close device with invalid parameter */
   	    hRegDevice = (OSAL_tIODescriptor)REG_INVAL_PARAM;
	 	if(OSAL_ERROR  != OSAL_s32IOClose ( hRegDevice ) )
   	{
   		u32Ret += 4;
   	}
	}

   return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegCreateKey()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_004																  
* DESCRIPTION:  Create the Key in Registry device
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 31 Mar, 2008
******************************************************************************/
tU32 u32RegCreateKey()
{
 	 tU32  u32Ret = 0;
    OSAL_tIODescriptor  hRegDevice1 = 0;
    OSAL_tIODescriptor  hRegDevice2 = 0;
    tS32 s32Status = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /* Open  the device in readwrite mode */
    hRegDevice1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
    if(OSAL_ERROR == hRegDevice1)
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret = 1;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret = 2;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret = 3;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret = 4;
				 break;
		    default:
				 u32Ret = 5;
	 	}    
	 }
    else
    {
  	 	enAccess = (OSAL_tenAccess)OSAL_EN_READWRITE;
  		/*  Create the Key in readwrite mode*/
	  	hRegDevice2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
  		if (OSAL_ERROR == hRegDevice2)
    	{
     		s32Status = (tS32) OSAL_u32ErrorCode();
			switch(s32Status)
			{
				 case OSAL_E_DOESNOTEXIST:
					 u32Ret += 10;
					 break;
			    case OSAL_E_INVALIDVALUE:
				     u32Ret += 20;
					 break;    
	          case OSAL_E_NOACCESS:      	
					 u32Ret += 30;
					 break;
	          case OSAL_E_UNKNOWN:
				   	 u32Ret += 40;
					 break;
			    default:
					 u32Ret += 50;
		    } 
	  	}
      else
	   {
	   	/* Close the key */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice2 ) )
			{
		    	u32Ret += 60;
			}
			else
			{
	        	/*Remove the Key*/
	     		if (OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
		    	{
		    	u32Ret +=65;
		    	}
		   }
      }

   	/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice1 ) )
		{
	    	u32Ret += 100;
		}
	}
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegOpenCloseKey()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_005																  
* DESCRIPTION:  Open the Key in Registry  device
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 31 Mar, 2008
******************************************************************************/
tU32 u32RegOpenCloseKey(void)
{
 	tU32  u32Ret = 0;	
  	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
  	tS32 s32Status = 0;
  	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Open the registry device */
   hRegDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_REGISTRY,enAccess);
   if (OSAL_ERROR == hRegDevice)
   {
      s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
         case OSAL_E_DOESNOTEXIST:
			   u32Ret = 1;
				break;
		   case OSAL_E_INVALIDVALUE:
				u32Ret = 2;
				break;
		   case OSAL_E_NOACCESS:      	
				u32Ret = 3;
				break;
		   case OSAL_E_UNKNOWN:
			   u32Ret = 4;
				break;
           default:
			   u32Ret = 5;      
	   }
   }
	else 
   {
	  	/*  Create the Key in readwrite mode*/
  		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
	  	if (OSAL_ERROR == hRegKey)
      {
      	u32Ret += 6;
      } 	
    	else
		{
		  	if (OSAL_ERROR == OSAL_s32IOClose ( hRegKey ) )
			{
				u32Ret += 10;
			}
			else
			{
				hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
				if(OSAL_ERROR == hRegKey)
				{
					u32Ret += 20;
				}
				else
				{
					if (OSAL_ERROR == OSAL_s32IOClose ( hRegKey ) )
					{
						u32Ret += 50;
					}
	        		/*Remove the Key*/
		     		if (OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			    	{
			   	 	u32Ret +=60;
		   	 	}

				}
			}
		}

   	/* Close registry device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
		{
	    	u32Ret += 100;
		}
	}

   return u32Ret;

}

/*****************************************************************************
* FUNCTION:		 u32RegGetVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_006																  
* DESCRIPTION:  Get the version  of the Registry device   
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
******************************************************************************/
tU32 u32RegGetVersion(void)
{
 	OSAL_tIODescriptor hRegDevice = 0;
 	tU32 u32Ret = 0;
	tS32 s32Status = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	
	/* Open the device in readonly mode */
 	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess ); 
    if ( OSAL_ERROR == hRegDevice )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
		   case OSAL_E_DOESNOTEXIST:
			     u32Ret = 1;
				 break;
		   case OSAL_E_INVALIDVALUE:
				u32Ret = 2;
				break;
		   case OSAL_E_NOACCESS:      	
				u32Ret = 3;
				break;
		   case OSAL_E_UNKNOWN:
			   	u32Ret = 4;
				break;
           default:
			    u32Ret = 5;		 }

     }

    else
    {
    
    	/* Get device version */	
	  	if ( OSAL_ERROR == OSAL_s32IOControl ( hRegDevice,OSAL_C_S32_IOCTRL_VERSION,
										 (tS32)&s32VersionInfo) )
      {
			u32Ret += 6;
	   }

	  	/* Close the device */
	  	if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
	   {
			u32Ret += 7;
	   }
	 }
	 return u32Ret;

}

/*****************************************************************************
* FUNCTION:		 u32RegSetGetValue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_007																  
* DESCRIPTION:  Set the registry value 
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 1 Apr, 2008
******************************************************************************/
tU32 u32RegSetGetValue(void)
 {
 	tU32 u32Ret = 0;
    tS32 s32Status = 0;
	OSAL_tIODescriptor 	 hRegDevice1 = 0;
	OSAL_tIODescriptor 	 hRegDevice2 = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlRegistry hReg ;
  tS8   pBuffer[30];

	/* Open the device in readwrite  mode */
    hRegDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
    if ( OSAL_ERROR == hRegDevice1 )
    {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		 {
			case OSAL_E_DOESNOTEXIST:
                 u32Ret = 1;
				 break;
            case OSAL_E_INVALIDVALUE:
                 u32Ret = 2;
				 break;
            case OSAL_E_NOACCESS:
                 u32Ret = 3;
				 break;
            case OSAL_E_UNKNOWN:
                 u32Ret = 4;
				 break;
              default:
				u32Ret  = 5;
		 }
	} 
	 
	else
	{
		/*Create the Key*/
	 	hRegDevice2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
	 	if ( OSAL_ERROR == hRegDevice2 )
      {
	 		/* check the error status returned */
			s32Status = (tS32) OSAL_u32ErrorCode();
			switch(s32Status)
			{
				case OSAL_E_DOESNOTEXIST:
                 u32Ret += 10;
				break;
            case OSAL_E_INVALIDVALUE:
                 u32Ret += 20;
				break;
            case OSAL_E_NOACCESS:
                 u32Ret += 30;
				break;
            case OSAL_E_UNKNOWN:
                 u32Ret += 40;
				 break;
            default:
				u32Ret  += 50;
			}
		} 
		else 
		{
			/*Set the value */
			hReg.pcos8Name =  (void*)"REGISTRY";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.u32Size   =  13;
			hReg.ps8Value  =  (void*)"FunctionCheck";

			if (OSAL_ERROR ==OSAL_s32IOControl( hRegDevice2 ,
                 OSAL_C_S32_IOCTRL_REGSETVALUE, 
                           (tS32)&hReg))
                           
         {
        		u32Ret += 6;
         }                             
         else
		   {
				hReg.pcos8Name =  (void*)"REGISTRY";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  13;
				hReg.ps8Value  =  (tPU8)pBuffer;
				
				
				if (OSAL_ERROR ==OSAL_s32IOControl( hRegDevice2 ,
                 OSAL_C_S32_IOCTRL_REGGETVALUE, 
                           (tS32)&hReg))
				{
					u32Ret +=7;
				}
			}
			/*Set the value */
			hReg.pcos8Name =  (void*)"REGISTRY";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.u32Size   =  0;
			hReg.ps8Value  =  NULL;
		   
		   if(OSAL_s32IOControl(hRegDevice2, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
		   {
		   	u32Ret = 10;
		   }
			else
			{
				/* Close the key */
				if (OSAL_ERROR == OSAL_s32IOClose(hRegDevice2))
				{
					u32Ret +=50;
				}
						
				/* Remove the key in the registry*/
				if (OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
				{
				  	u32Ret +=100;
				}
			}
		}
	      			
		/*Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice1 ) )
  		{
 				u32Ret += 500;
		}
   }
   return u32Ret;
}


/*****************************************************************************
* FUNCTION:		 u32RegWriteValue() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_008																  
* DESCRIPTION:  Write the registry value 
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*              Modified by Shilpa Bhat (RBEI/ECM1) on 3 Apr, 2008
******************************************************************************/

tU32 u32RegWriteValue(void)
 {
  	OSAL_tIODescriptor 	 hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
    tU32 u32Ret = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlRegistryValue sValue;


	/* Open the device in readwrite  mode */
    hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
    if ( OSAL_ERROR == hRegDevice )
    {
		u32Ret = 1;
	 } 
	 
	else
	{
		/*Create the Key*/
	 	hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
	 	if ( OSAL_ERROR == hRegKey)
      {
			u32Ret = 2;	
		}	
		else
		{
			   /* Write the value */
			   OSAL_pvMemoryCopy(sValue.s8Name, (const void*)"REG", 3);
			   OSAL_pvMemoryCopy(sValue.s8Value, (const void*)"WriteValue", 10);
				sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
		   
		    	if (OSAL_ERROR ==OSAL_s32IOControl( hRegKey ,
		                 OSAL_C_S32_IOCTRL_REGWRITEVALUE, 
		                           (tS32)&sValue)) 

		       {

		       		u32Ret = 3;

		       }

			   OSAL_pvMemoryCopy(sValue.s8Name, (const void*)"REG", 3);
			   OSAL_pvMemoryCopy(sValue.s8Value, (const void*)"0", 0);
				sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
			   if(OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGDELETEVALUE, (tS32)&sValue) == OSAL_ERROR)
			   {
			   	u32Ret += 4;
			   }
				else
				{
					/* Close the key */
					if (OSAL_ERROR == OSAL_s32IOClose(hRegKey))
					{
						u32Ret +=5;
					}
							
					/* Remove the key in the registry*/
					if (OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
					{
					  	u32Ret +=10;
					}		
				}
			}
	      /*Close the device */
			if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
			{
		    	u32Ret += 50;
			}
   	}

   return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32RegSetQueryValue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_009																  
* DESCRIPTION:  Set the registry  value and query the registry value 
* HISTORY:		Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*              Modified by Shilpa Bhat (RBEI/ECM1) on 4 Apr, 2008
******************************************************************************/

tU32 u32RegSetQueryValue(void)
 {
    tU32 u32Ret = 0;
	OSAL_tIODescriptor 	 hRegDevice = 0;
	OSAL_tIODescriptor 	 hRegKey = 0;
    OSAL_trIOCtrlRegistry hReg;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tS8   pBuffer[30];

	/* Open the device in readwrite  mode */
    hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
    if ( OSAL_ERROR == hRegDevice )
    {
		u32Ret =1;
	 } 
	 else
    {
		/*Create the Key*/
	 	hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
	 	if ( OSAL_ERROR == hRegKey)
      {
			u32Ret = 2;	
		}	
		else
		{
	    /* Write the value */
	   	hReg.pcos8Name =  (void*)"REG1";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.u32Size   =  10;
			hReg.ps8Value  =  (void*)"WriteCheck";

   		/* Set the value*/
   		if (OSAL_ERROR ==OSAL_s32IOControl( hRegKey ,
                 OSAL_C_S32_IOCTRL_REGSETVALUE, 
                           (tS32)&hReg)) 
        {

       		u32Ret = 3;

        }
        else
	     {
		      hReg.pcos8Name =  (void*)"REG1";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  10;
				hReg.ps8Value  =  (tPU8)pBuffer;

		    /* Query  the  value*/ 
	    	 if (OSAL_ERROR == OSAL_s32IOControl( hRegKey ,
                 OSAL_C_S32_IOCTRL_REGQUERYVALUE, 
                           (tS32)&hReg))
          {                    	
				u32Ret = 4;
	     	 }
	     }	
	     hReg.pcos8Name =  (void*)"REG1";
		  hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
		  hReg.u32Size   =  0;
		  hReg.ps8Value  =  NULL;

   	  if (OSAL_ERROR ==OSAL_s32IOControl( hRegKey ,
                 OSAL_C_S32_IOCTRL_REGREMOVEVALUE, 
                           (tS32)&hReg)) 

        {

       		u32Ret = 5;

        }
		  
	     /* Close key */
     	  if (OSAL_ERROR == OSAL_s32IOClose(hRegKey))
	     {
   	  	u32Ret += 7;
  		  }
		  /*Remove the key in the registry */
		  if(OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
		  {
		 		u32Ret += 8;
    	  }
		}
      /*Close the device */
 		if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
  		{
			u32Ret += 100;
 		}
	}
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegRemoveValue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_010															  
* DESCRIPTION:  Remove the registry value 
* HISTORY:		 Created by Pankaj Kumar (RBIN/EDI3) on 11 Oct,2007 
*               Modified by Shilpa Bhat (RBEI/ECM1) on 2 Apr, 2008
******************************************************************************/
tU32 u32RegRemoveValue(void)
 {
    tU32 u32Ret = 0;
	OSAL_tIODescriptor 	 hRegDevice = 0;
	OSAL_tIODescriptor 	 hRegKey = 0;
	OSAL_trIOCtrlRegistry hReg;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	  
	/* Open the device in readwrite  mode */
    hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, enAccess );
    if ( OSAL_ERROR == hRegDevice )
    {
		u32Ret = 1;
    } 
	 else
    {
		/*Create the Key*/
	 	hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
	 	if ( OSAL_ERROR == hRegKey)
      {
			u32Ret = 2;	
		}	
		else
		{
			hReg.pcos8Name =  (void*)"REG123";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.u32Size   =  10;
			hReg.ps8Value  =  (void*)"WriteCheck";

        if (OSAL_ERROR ==OSAL_s32IOControl( hRegKey ,
                 OSAL_C_S32_IOCTRL_REGSETVALUE, 
                           (tS32)&hReg)) 

       	{
       		u32Ret = 3;

       	}
			else
			{
	   		
				hReg.pcos8Name =  (void*)"REG123";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  0;
				hReg.ps8Value  =  NULL;			
				
				if (OSAL_ERROR ==OSAL_s32IOControl( hRegKey ,
	                 OSAL_C_S32_IOCTRL_REGREMOVEVALUE, 
	                           (tS32)&hReg))
				{
					u32Ret +=7;
				}
				else
		    	{
			 		if (OSAL_ERROR != OSAL_s32IOControl( hRegKey ,
	                 OSAL_C_S32_IOCTRL_REGQUERYVALUE, 
	                           (tS32)&hReg))
               {
         			u32Ret +=8;
         	  	}
					/* Close the key */
					if (OSAL_ERROR == OSAL_s32IOClose(hRegKey))
					{
						u32Ret +=9;
					}
							
					/* Remove the key in the registry*/
					if (OSAL_ERROR ==  OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
					{
					  	u32Ret +=10;
					}
				}
	      }
	 	}
		/* Close Device */
		if (OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret +=100;
		}
	}
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegDevReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_011															  
* DESCRIPTION:  Re-open an already opened registry device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 28 Mar, 2008
******************************************************************************/
tU32 u32RegDevReOpen(void)
{
	OSAL_tIODescriptor hRegDevice1 = 0;	
	OSAL_tIODescriptor hRegDevice2 = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open Registry Device */
	hRegDevice1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
	if(OSAL_ERROR == hRegDevice1)
	{
		u32Ret = 1;	
	}
	else
	{
		/* Re-open Registry Device */
		hRegDevice2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
		if(OSAL_ERROR == hRegDevice2)
		{
			u32Ret = 2;
		}
		else
		{
			/* Close the re-opened handle */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice2))
			{
				u32Ret = 3;
			}
		}
		/* Close the device */
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice1))
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_012
* DESCRIPTION:  Close an already closed registry device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 28 Mar, 2008
******************************************************************************/
tU32 u32RegDevCloseAlreadyClosed(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open Registry Device */
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Close Device */
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret = 2;
		}
		/* Close device already closed */
		if(OSAL_ERROR != OSAL_s32IOClose(hRegDevice))
		{
			u32Ret = 3;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegCreateMultipleValues()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_013
* DESCRIPTION:  Create Multiple values for a Key
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 28 Mar, 2008
******************************************************************************/
tU32 u32RegCreateMultipleValues(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	OSAL_trIOCtrlRegistry hReg;
	tU32 u32Ret = 0;
	tU32 u32len = 11;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tS8   pBuffer[30];

	/* Open Registry Device */
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Create a Key */		
	  	hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Set value 1 */
			hReg.pcos8Name =  (void*)"VAL1";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.ps8Value  =  (void*)"WriteCheck1";
			hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);

			if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
             OSAL_C_S32_IOCTRL_REGSETVALUE, 
             (tS32)&hReg))                 
         {
	        	u32Ret = 3;
         }
         else
			{
				hReg.pcos8Name =  (void*)"VAL1";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  sizeof(pBuffer);
				hReg.ps8Value  =  (tPU8)pBuffer;
				
				if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
                OSAL_C_S32_IOCTRL_REGGETVALUE, 
                (tS32)&hReg))
				{
					u32Ret = 4;
				}
				else
				{
					if(OSAL_ERROR == OSAL_s32StringNCompare((tS8 *)hReg.ps8Value,(tS8 *)"WriteCheck1",u32len))
					{
						u32Ret = 5;
					}
				}
			}   
		   		  
		   /* Set value 2 */
		   hReg.pcos8Name =  (void*)"VAL2";
		   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
		   hReg.ps8Value  =  (void*)"WriteCheck2";
		   hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);

		   if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
	          OSAL_C_S32_IOCTRL_REGSETVALUE, 
	          (tS32)&hReg))                 
	      {
	       	u32Ret += 10;
	      }
	      else
		   {
				hReg.pcos8Name =  (void*)"VAL2";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  sizeof(pBuffer);
				hReg.ps8Value  =  (tPU8)pBuffer;
				
				if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
	             OSAL_C_S32_IOCTRL_REGGETVALUE, 
	             (tS32)&hReg))
		  	   {
					u32Ret += 20;
			   }
			   else
			   {
					if(OSAL_ERROR == OSAL_s32StringNCompare((tS8 *)hReg.ps8Value,(tS8 *)"WriteCheck2",u32len))
					{
						u32Ret += 30;
		  			}
			   }
			}	  

			hReg.pcos8Name =  (void*)"VAL1";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.ps8Value  =  NULL;
			hReg.u32Size   =  0;

		   if(OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
		   {
		   	u32Ret += 100;
		   }
			
			hReg.pcos8Name =  (void*)"VAL2";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.ps8Value  =  NULL;
			hReg.u32Size   =  0;

		   if(OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
		   {
		   	u32Ret += 500;
		   }		
			
			/* Close Key */
		  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret += 1000;
			}
    		
    		/* Remove Key */
    		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 5000;
			}			
		}

		/* Close Registry device*/
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 10000;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegCreateMultipleKeys()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_014
* DESCRIPTION:  Create Multiple Keys in a common root 
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 31 Mar, 2008
******************************************************************************/
tU32 u32RegCreateMultipleKeys(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey1 = 0;
	OSAL_tIODescriptor hRegKey2 = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open Registry Device */
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Create a Key */		
	  	hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey1)
		{
			u32Ret = 2;
		}
		else
		{
			/* Create another Key */		
		  	hRegKey2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY1, enAccess);
			if(OSAL_ERROR == hRegKey2)
			{
				u32Ret = 3;
			}
			else
			{
				/* Close Key */
			  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey2))
				{
					u32Ret = 4;
				}
	    		
	    		/* Remove Key */
	    		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY1))
				{
					u32Ret += 5;
				}			
			}
			/* Close Key */
		  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey1))
			{
				u32Ret += 10;
			}
    		
    		/* Remove Key */
    		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 20;
			}			
		}
		/* Close Registry device*/
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 50;
		}
	}
	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		 u32RegReOpenKey()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_015
* DESCRIPTION:  Re-open an already opened key
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 31 Mar, 2008
******************************************************************************/
tU32 u32RegReOpenKey(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey1 = 0;
	OSAL_tIODescriptor hRegKey2 = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open Registry Device */
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Create a Key and hence open */		
	  	hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey1)
		{
			u32Ret = 2;
		}
		else
		{
			/* Re-open key */
			hRegKey2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,enAccess);
			if(OSAL_ERROR == hRegKey2)
			{
				u32Ret += 3;
			}
			else
			{
				/* Close Key */
			  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey2))
				{
					u32Ret += 10;
				}
			}
			/* Close Key */
		  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey1))
			{
				u32Ret += 50;
			}
    		
    		/* Remove Key */
    		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 100;
			}			
		}

		/* Close Registry device*/
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 500;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegCloseKeyAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_016
* DESCRIPTION:  Close an already closed key 
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 31 Mar, 2008
******************************************************************************/
tU32 u32RegCloseKeyAlreadyClosed(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Close Key */
		  	if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret = 3;
			}
    		
			/* Attempt to close key which is already closed */
			if(OSAL_ERROR != OSAL_s32IOClose(hRegKey))
			{
				u32Ret += 5;
			}

    		/* Remove Key */
    		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 10;
			}			
		}

		/* Close Registry device*/
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 50;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegDeleteKeyWithValues()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_017
* DESCRIPTION:  Delete keys with active values
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 2 Apr, 2008
******************************************************************************/
tU32 u32RegDeleteKeyWithValues(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlRegistry hReg;
	
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Set value 1 */
			hReg.pcos8Name =  (void*)"VAL1";
			hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
			hReg.ps8Value  =  (void*)"WriteCheck1";
			hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);

			if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
             OSAL_C_S32_IOCTRL_REGSETVALUE, 
             (tS32)&hReg))                 
         {
	        	u32Ret = 3;
         }

    		/* Delete key with valid values */
	 		if(OSAL_ERROR != OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 5;
			}
			else
			{
				/*Set the value */
				hReg.pcos8Name =  (void*)"VAL1";
				hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
				hReg.u32Size   =  0;
				hReg.ps8Value  =  NULL;
			   
			   if(OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
			   {
			   	u32Ret += 10;
			   }
				
				/* Close key */	
				if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
				{
					u32Ret += 50;
				}
					
	    		/* Delete key with valid values */
		 		if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
				{
					u32Ret += 100;
				}	
			}	
		}

		/* Close device */	
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 500;
		}
	}		
						
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		 u32OpenKeyDiffAccessModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_018
* DESCRIPTION:  Open key with different access modes
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 2 Apr, 2008
******************************************************************************/
tU32 u32RegOpenKeyDiffAccessModes(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Close key opened by default */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret = 3;
			}

			enAccess = OSAL_EN_READWRITE;
			/* Open key in READWRITE mode */
			hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
			if(OSAL_ERROR == hRegKey)
			{
				u32Ret = 4;
			}
			/* Close key */
			else
			{
				if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
				{
					u32Ret = 5;
				}
			}

			enAccess = OSAL_EN_READONLY;
			/* Open key in READWRITE mode */
			hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
			if(OSAL_ERROR == hRegKey)
			{
				u32Ret = 40;
			}
			/* Close key */
			else
			{
				if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
				{
					u32Ret = 50;
				}
			}

			enAccess = OSAL_EN_WRITEONLY;
			/* Open key in READWRITE mode */
			hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
			if(OSAL_ERROR == hRegKey)
			{
				u32Ret = 400;
			}
			/* Close key */
			else
			{
				if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
				{
					u32Ret = 500;
				}
			}

	    	/* Delete key */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 1000;
			}
		}
		
		/* Close device */	
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret += 500;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegWriteNumericValue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_019
* DESCRIPTION:  Create key with numeric value
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 2 Apr, 2008
******************************************************************************/
tU32 u32RegWriteNumericValue(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	tU32 u32Ret = 0;
    OSAL_trIOCtrlRegistry hReg;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU8 u8NumData= 12;
	
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Set value */
			hReg.pcos8Name =  (void*)"VALUE";
			hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
			hReg.ps8Value  =  &u8NumData;
			hReg.u32Size   =  2;

			if (OSAL_ERROR == OSAL_s32IOControl( hRegKey,
             OSAL_C_S32_IOCTRL_REGSETVALUE, 
             (tS32)&hReg))                 
         {
	        	u32Ret = 3;
         }
			hReg.pcos8Name =  (void*)"VALUE";
			hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
			hReg.u32Size   =  0;
			hReg.ps8Value  =  NULL;
		   
		   if(OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&hReg) == OSAL_ERROR)
		   {
		   	u32Ret += 10;
		   }

			/* Close Key */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret += 50;
			}
	    	/* Delete key */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 100;
			}
		}
		/* Close Key */
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret = 1000;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32RegDelKeyAlreadyRemoved()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_020
* DESCRIPTION:  Delete a key which is already removed
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 2 Apr, 2008
* 		 			update by Anoop Chandran (RBEI/ECF1) on 28 Apr, 2009
******************************************************************************/
tU32 u32RegDelKeyAlreadyRemoved(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			/* Close Key */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret += 10;
			}
	    	/* Delete key */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 50;
			}

	    	/* Attempt to delete a key already deleted. 
	    	 Diver comment says that, remove will success for no existing Key  */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 100;
			}
		}
		/* Close Key */
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret = 500;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		 u32RegDelKeyStructure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_021
* DESCRIPTION:  Delete a key with active sub-keys
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 2 Apr, 2008
******************************************************************************/
tU32 u32RegDelKeyStructure(tVoid)
{
	OSAL_tIODescriptor hRegDevice = 0;
	OSAL_tIODescriptor hRegKey = 0;
	OSAL_tIODescriptor hRegKey1 = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, enAccess);	
	if(OSAL_ERROR == hRegDevice)
	{
		u32Ret = 1;
	}
	else
	{
		hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess);
		if(OSAL_ERROR == hRegKey)
		{
			u32Ret = 2;
		}
		else
		{
			hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY"/Test1", enAccess);
			if(OSAL_ERROR == hRegKey1)
			{
				u32Ret = 3;
			}
		}

		/* Delete key */
	 	if(OSAL_ERROR != OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
		{
			u32Ret += 5;
		}
	  	else
	  	{
	  		/* Close Key */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegKey1))
			{
				u32Ret += 10;
			}
			/* Delete key */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY"/Test1"))
			{
				u32Ret += 50;
			}
			/* Close Key */
			if(OSAL_ERROR == OSAL_s32IOClose(hRegKey))
			{
				u32Ret = 100;
			}
			/* Delete key */
		 	if(OSAL_ERROR == OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY))
			{
				u32Ret += 500;
			}
		}

		/* Close Key */
		if(OSAL_ERROR == OSAL_s32IOClose(hRegDevice))
		{
			u32Ret = 1000;
		}
	}		
	return u32Ret;
} 

/*****************************************************************************
* FUNCTION:		 u32REGISTRYBasic1Test()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_022
* DESCRIPTION:  1. open device registry
					 2. create key 'tralala'
					 3. search the dir for 'tralala'
* HISTORY:		 Created by tnn
******************************************************************************/
tU32 u32REGISTRYBasic1Test(void)
{
  tU32  u32Ret = 0;
  tS32  s32Ret = 0;
  tBool bContinue = TRUE;
  OSAL_tIODescriptor  hFd1;
  OSAL_tIODescriptor  hFd2;
  OSAL_trIOCtrlDir 	  sDirCtrl;

  hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE);
  if( OSAL_ERROR == hFd1)
  {
    return 1;
  }

  hFd2 = OSAL_IOCreate("/dev/registry/Bosch", OSAL_EN_READWRITE);
  if(OSAL_ERROR == hFd2)
  {
    u32Ret = 2;
  }
  else
  {
    sDirCtrl.fd        = hFd1;
    sDirCtrl.s32Cookie = 0;

    do
    {
      s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOREADDIR, (tS32)&sDirCtrl);

      if(s32Ret == OSAL_OK)
      {
        if(strncmp((const char*)sDirCtrl.dirent.s8Name, "Bosch", 5) == 0)
        {
          bContinue = FALSE;
        }
      }
      else
      {
        if(s32Ret == OSAL_ERROR)
        {
          u32Ret = 3;
        }
        else
        {
          u32Ret = 4;
        }

        bContinue = FALSE;
      }
    }
    while(bContinue == TRUE);

    OSAL_s32IOClose(hFd2);
    OSAL_s32IORemove("/dev/registry/Bosch");
  }

  OSAL_s32IOClose(hFd1);

  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		 u32REGISTRYBasic2Test()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_023
* DESCRIPTION:  1. create key 'tralala'
					 2. create subkey 'tralala/Test' with the value 'abcdefghijklmnopqrstuvwxyz'
					 3. check whether 'Test' was created
					 4. check content of 'Test'
* HISTORY:		 Created by tnn
******************************************************************************/
tU32 u32REGISTRYBasic2Test(void)
{
  tU32  u32Ret = 0;
  tS32  s32Ret = 0;
  tBool bContinue = TRUE;
  OSAL_tIODescriptor    hFd1;
  OSAL_trIOCtrlRegistry sIOCtrlRegistry;
  OSAL_trIOCtrlDir      sValueInfo;
  tS8   pBuffer[30];

  hFd1 = OSAL_IOCreate("/dev/registry/tralala", OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }

  memset(pBuffer, 0, 30);

  sIOCtrlRegistry.pcos8Name = (void*)"Test";
  sIOCtrlRegistry.u32Size   = 26;
  sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
  sIOCtrlRegistry.ps8Value  = (void*)"abcdefghijklmnopqrstuvwxyz";

  if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32)&sIOCtrlRegistry) == OSAL_ERROR)
  {
    u32Ret = 2;
  }
  else
  {
    sValueInfo.fd        = hFd1;
    sValueInfo.s32Cookie = 0;

    do
    {
      s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGENUMVALUE, (tS32)&sValueInfo);
      if(s32Ret != OSAL_OK)
      {
        u32Ret = 3;
        bContinue = FALSE;
      }
      else
      {
        if(strncmp((const char*)sValueInfo.dirent.s8Name, "Test", 4) == 0)
        {
          bContinue = FALSE;
        }
      }
    }
    while((sValueInfo.dirent.s8Name != OSAL_NULL) && (bContinue == TRUE));    

    sIOCtrlRegistry.pcos8Name = (void*)"Test";
    sIOCtrlRegistry.u32Size   = 30;
    sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
    sIOCtrlRegistry.ps8Value  = (tPU8)pBuffer;

    if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&sIOCtrlRegistry) == OSAL_ERROR)
    {
      u32Ret = 4;
    }
    else
    {
      if(strncmp((const char*)pBuffer, "abcdefghijklmnopqrstuvwxyz", 26) != 0)
      {
        u32Ret = 5;
      }
    }
  }

  sIOCtrlRegistry.pcos8Name = (void*)"Test";
  sIOCtrlRegistry.u32Size   = 0;
  sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
  sIOCtrlRegistry.ps8Value  = NULL;

  if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (tS32)&sIOCtrlRegistry) == OSAL_ERROR)
  {
    u32Ret = 6;
  }
  else
  {
    OSAL_s32IOClose(hFd1);

    if(OSAL_s32IORemove("/dev/registry/tralala") == OSAL_ERROR)
    {
      u32Ret = 7;
    }
  }

  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32REGISTRYBasic3Test()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_024
* DESCRIPTION:  tests basic key operations using iocontrols IOCTRL_REGWRITEVALUE + IOCTRL_REGDELETEVALUE 
                + OSAL_C_S32_IOCTRL_REGREADVALUE
                1. create key 'blabla'
                4. check whether 'Test' was created ???????????????????????????
                4. check content of 'Test'
* HISTORY:		 Created by tnn
******************************************************************************/
tU32 u32REGISTRYBasic3Test(void)
{
  tU32  u32Ret = 0;
  OSAL_tIODescriptor         hFd1;
  OSAL_trIOCtrlRegistryValue sValue;
  tS8                        pBuffer[9];

  hFd1 = OSAL_IOCreate("/dev/registry/blabla", OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }

  sValue.s32Type = OSAL_C_S32_VALUE_STRING;
  memcpy(sValue.s8Name,  (const void*)"blub", 5);
  memcpy(sValue.s8Value, (const void*)"zyxwvuts", 9);

  memset(pBuffer, 0, 9);

  if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGWRITEVALUE, (tS32)&sValue) == OSAL_ERROR)
  {
    u32Ret = 2;
  }
  else
  {
    sValue.s32Type = OSAL_C_S32_VALUE_STRING;
    memcpy(sValue.s8Name, (const void*)"blub", 5);
    memset(sValue.s8Value, 0, sizeof(sValue.s8Value));

    if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREADVALUE, (tS32)&sValue) == OSAL_ERROR)
    {
      u32Ret = 4;
    }
    else
    {
      if(strncmp((const char*)sValue.s8Value, "zyxwvuts", 8) != 0)
      {
        u32Ret = 5;
      }
    }
  }

  sValue.s32Type = OSAL_C_S32_VALUE_STRING;
  memcpy(sValue.s8Name, (const void*)"blub", 5);
  memset(sValue.s8Value, 0, sizeof(sValue.s8Value));

  if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGDELETEVALUE, (tS32)&sValue) == OSAL_ERROR)
  {
    u32Ret += 10;
  }
  
  if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
  {
    u32Ret += 100;
  }

  if(OSAL_s32IORemove("/dev/registry/blabla") == OSAL_ERROR)
  {
    u32Ret += 1000;
  }
  
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32RegSearchKey()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_025
* DESCRIPTION:  1. open device registry
				2. create key 
				3. search the dir for key
* HISTORY:		Created by Shilpa Bhat (RBEI/ECM1) on 30.05.2008
******************************************************************************/
tU32 u32RegSearchKey(void)
{
  tU32  u32Ret = 0;
  tS32  s32Ret = 0;
  tBool bContinue = TRUE;
  OSAL_tIODescriptor  hFd1 = 0;
  OSAL_tIODescriptor  hFd2 = 0;
  OSAL_trIOCtrlDir sDirCtrl = {0};

  hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE);
  if( OSAL_ERROR == hFd1)
  {
    return 1;
  }

  hFd2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY"/Bosch", OSAL_EN_READWRITE);
  if(OSAL_ERROR == hFd2)
  {
    u32Ret = 2;
  }
  else
  {
    sDirCtrl.fd = hFd1;
    sDirCtrl.s32Cookie = 0;

    do
    {
      s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREADDIR, (tS32)&sDirCtrl);

      if(s32Ret == OSAL_OK)
      {
        if(strncmp((const char*)sDirCtrl.dirent.s8Name, "Bosch", 5) == 0)
        {
          bContinue = FALSE;
        }
      }
      else
      {
        if(s32Ret == OSAL_ERROR)
        {
          u32Ret = 3;
        }
        else
        {
          u32Ret = 4;
        }

        bContinue = FALSE;
      }
    }
    while(bContinue == TRUE);

    OSAL_s32IOClose(hFd2);
    OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY"/Bosch");
  }

  OSAL_s32IOClose(hFd1);

  return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32RegOpenAsDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_REGISTRY_026
* DESCRIPTION:  1. open reg device
				2. create key
				3. open key (like an directory)
				5. close key (like an directory)
				6. close key	
				7. remove key	
				8. close device	
* HISTORY:		Created by Martin Langer (Fa. ESE, CM-AI/PJ-CF33) on 15.04.2011
******************************************************************************/
tU32 u32RegOpenAsDir(void)
{
  tU32  u32Ret = 0;
  OSAL_tIODescriptor  	hRegDevice = 0;
  OSAL_tIODescriptor  	hRegKey = 0;
  OSAL_tenAccess  		enAccess = OSAL_EN_READWRITE;
  OSAL_trIOCtrlDir*  	pDir = NULL;

  /* Open the device in read-write mode */
  hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY , enAccess );
  if (OSAL_ERROR == hRegDevice)
  {
	OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: open registry device failed");
	u32Ret += 2;
  }
  else
  {
    enAccess = (OSAL_tenAccess)OSAL_EN_READWRITE;
    /* Create the Key in readwrite mode */
	hRegKey = OSAL_IOCreate( OSAL_C_STRING_DEVICE_REGISTRY_KEY, enAccess );
  	if (OSAL_ERROR == hRegKey)
    {
	  OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: create registry key failed");
	  u32Ret += 4;
	}
	else
	{
	  /* Open reg like an directory */
	  pDir = OSALUTIL_prOpenDir( OSAL_C_STRING_DEVICE_REGISTRY_KEY );
  	  if(OSAL_NULL == pDir)
	  {
		OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: open registry key in directory mode failed");
		u32Ret += 10;
	  } 
	  else
	  {

		/* close reg directory */
		if (OSAL_ERROR == OSALUTIL_s32CloseDir( pDir ))
		{
		  OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: close registry directory failed");
		  u32Ret += 20;
		}
		else
		{
		  pDir = OSAL_NULL;
		}
	  }

	  /* Close the key */
	  if(OSAL_ERROR  == OSAL_s32IOClose ( hRegKey ))
	  {
		OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: close registry key failed");
		u32Ret += 40;
	  }

	  /* Remove the key */
	  if (OSAL_ERROR == OSAL_s32IORemove( OSAL_C_STRING_DEVICE_REGISTRY_KEY ))
	  {
		OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: remove registry key failed");
		u32Ret += 100;
	  }
	}
		
	/* Close the device */
	if(OSAL_ERROR  == OSAL_s32IOClose ( hRegDevice ) )
	{
  	  OEDT_HelperPrintf(TR_LEVEL_ERRORS,"ERROR: close registry device failed");
	  u32Ret += 300;
  	}
  }
  
  return u32Ret;  
}  


/* End of Test Code */

