/******************************************************************************
 * FILE         : oedt_CARD_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the USB  
 *                device for GM-GE hardware.
 *              
 * AUTHOR(s)    :  	Anoop Chandran (RBEI/ECM1) 10/10/2008
 * HISTORY		:		Taken form Paramount platform 
                   23.03.09 - Ravindran P (RBEI/ECF1) - Compiler/LINT warning removal
				   04.05.09 - Jeryn M (RBEI/ECF1)     - Some Lint warnings
******************************FileHeaderEnd**********************************/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "../oedt_helper_funcs.h"

#include "oedt_card_TestFuncs.h"

/*#define  NUCLEUS_CORE_S_IMPORT_INTERFACE_B3_REPLACEMENTS
#include "nucleus_core_pif.h"

#define NUCLEUS_DRIVER_S_IMPORT_INTERFACE_DEV_COMMON
#define NUCLEUS_DRIVER_S_IMPORT_INTERFACE_LLD
#include "nucleus_driver_pif.h"*/


/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define EVENT_WAIT_TIMEOUT (110000)
#define EVENT_POST_THREAD_1 (0x00000001) 
#define EVENT_POST_THREAD_2 (0x00000002)
#define EVENT_WAIT (0x00000003)
#define NO_IOP (8)
#define THREAD_STACK_SIZE (1024)

#define ADIT_PLATFORM

#define EVENT_TIMEOUT (5000) /*10secs*/
#define EVENT_ASREAD_ALLDONE (0x000003FF)
#define ASYNC_READ_ITERATIONS (10)
#define NUM_OF_FILESIZES      (15) //2K - 1M
/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/
typedef struct  
{
   tU8 u8Id;
   OSAL_tMSecond cbStartTime_rd;
   OSAL_tMSecond cbStartTime_wr;
   OSAL_tMSecond cbStopTime_rd;
   OSAL_tMSecond cbStopTime_wr;
   OSAL_tMSecond cbTotalTime_rd;
   OSAL_tMSecond cbTotalTime_wr;

   OSAL_trAsyncControl *acArg;
}CBARGS;
/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
OSAL_tSemHandle semchkcallback, semfmtcallback;
tS8 s8ChkDskStatus = 0;
tS8 s8FormatStatus = 0;
OSAL_tEventHandle parallel_write, parallel_read, parallel_open,parallel_close,parallel_access,parallel_rw,Read_Format;
OSAL_tEventMask ResultMask;

tS8 *ps8UserBuf,*ps8ReadBuf_1, *ps8ReadBuf_2;
tS32 s32ParallelRetval;

OSAL_tIODescriptor filehandle1,filehandle2;
/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/
static OSAL_tEventHandle hAsyncReadEvent;
static tU8               *u8AsyncWriteBuf = NULL;
static tU8               *u8AsyncReadBuf  = NULL;
static OSAL_tIODescriptor Asyncfilehandle;
static OSAL_tIODescriptor Asyncfilewritehandle;
static CBARGS cbArg[ASYNC_READ_ITERATIONS];
/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/
/*function declaration*/
static tVoid Parallel_Write_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_Write_Thread_2(tPVoid pvThread2Arg);
static tVoid Parallel_Read_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_Read_Thread_2(tPVoid pvThread2Arg);
static tVoid Parallel_Open_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_Open_Thread_2(tPVoid pvThread2Arg);
static tVoid Parallel_Close_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_Close_Thread_2(tPVoid pvThread2Arg);
static tVoid Parallel_Access_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_Access_Thread_2(tPVoid pvThread2Arg);
static tVoid Parallel_ReadWrite_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_ReadWrite_Thread_2(tPVoid pvThread2Arg);
static tS32  s32PrepareFiles(tPCU8 pu8Buffer);
static tU32  u32FreeResources(tS32 s32ErrCode);
static tVoid vPerfAsyncReadCallback (CBARGS *prArgs);
static tVoid vPerfAsyncWriteCallback (CBARGS *prArgs);


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/
/*****************************************************************************
* FUNCTION:		u32CardOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_001 
* DESCRIPTION:  open and Close
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

tS32 u32CardOpenClose(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle;
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			s32Retval = 2;
		}
		
	}
	else
	{
		s32Retval = 1;
	}
	
	return s32Retval;
}
	
	
	
	
	

/*****************************************************************************
* FUNCTION:		u32CardFileCreateRemove()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_002 
* DESCRIPTION:  File create & remove
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

tS32 u32CardFileCreateRemove(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle,filehandle;
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		filehandle = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/oedt_test.txt", OSAL_EN_READWRITE);
		if(filehandle != OSAL_ERROR)
		{
			if(OSAL_s32IOClose(filehandle) == OSAL_OK)
			{
				if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt_test.txt") != OSAL_OK)
				{
					//file remove error
					s32Retval = 4;
				}
				
			}
			else
			{
				//file close error
				s32Retval = 5;
			}
			
		}
		else
		{
			// file create error
			s32Retval = 3;
		}
		
		//close the devie
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			s32Retval = 2;
		}
		
	}
	else
	{
		//device cannnot be openend
		s32Retval = 1;
		
	} 
	
	return s32Retval;
}
	
/*****************************************************************************
* FUNCTION:		u32CardFileOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_003 
* DESCRIPTION: 
*					File open & close
*					Test file 'oedt.txt' should be available in the card
*
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

tS32 u32CardFileOpenClose(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle = 0,filehandle = 0, filehandle1 = 0; /*lint !e578 */ /* same name desired*/  
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READONLY);
		
		if(filehandle1 == OSAL_ERROR)
		{
			filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
		}

		if(OSAL_ERROR != filehandle1)
		{
			if(OSAL_s32IOClose(filehandle1) == OSAL_OK)
			{
				filehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
				if(filehandle != OSAL_ERROR)
				{
					
					if(OSAL_s32IOClose(filehandle) != OSAL_OK)
					{
						//file remove error
						s32Retval = 5;
					}
				}
				else
				{
					// file create error
					s32Retval = 4;
				}
			}
			else
			{
				//file close after creation failed
				s32Retval = 3;
			}
			//delete the file created

			if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt.txt") != OSAL_OK)
			{
				s32Retval = 6;
			}

		}
		else
		{
			// file creation failed	
			s32Retval = 2;	
		}
			
		//close the devie
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			s32Retval = 7;
		}
		
	}
	else
	{
		//device cannnot be openend
		s32Retval = 1;
		
	} 
	
	return s32Retval;
}

	

/*****************************************************************************
* FUNCTION:		u32CardFileMultipleOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_004 
* DESCRIPTION: 
*						File multi open 
*						Test file 'oedt.txt' should be available in the card
*
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

tS32 u32CardFileMultipleOpen(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle = 0,filehandle1 = 0,filehandle2 = 0; /*lint !e578 */ /* same name desired*/
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		
		filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READONLY);
		
		if(filehandle2 == OSAL_ERROR)
		{
			filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
		}

		if(OSAL_ERROR != filehandle2)
		{
			if(OSAL_s32IOClose(filehandle2) == OSAL_OK)
			{
				filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
				if(filehandle1 != OSAL_ERROR)
				{
					/*rav8kor - comented as open should get handle to close it later */
					/*if(OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE) != OSAL_ERROR)
					{
						//file opened second time successfully
						s32Retval = 5;
					} */
					
					
					if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
					{
						//file remove error
						s32Retval = 6;
					}
					
				}
				else
				{
					// file open error
					s32Retval = 4;
				}
			}
			else
			{
				//file close failed after creating
				s32Retval =	3;
			}
			//delete the file created
			if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt.txt") != OSAL_OK)
			{
				s32Retval = 7;
			}

		}
		else
		{
			// file creation failed
			s32Retval = 2;	
		}	
		//close the devie
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			s32Retval = 8;
		}
	}
	else
	{
		//device cannnot be openend
		s32Retval = 1;
		
	} 
	
	return s32Retval;
}
	
	
	
/*****************************************************************************
* FUNCTION:		u32CardFileWriteRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CARD_005 
* DESCRIPTION: 
*					File read write
*					Test file 'oedt.txt' should be available in the card
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

tS32 u32CardFileWriteRead(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle,filehandle1;  /*lint !e578 */ /* same name desired*/
	tU8 *u8WriteBuf = NULL, *u8ReadBuf = NULL;
	tU32 u32Index;
	
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		
		filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
		
		if(filehandle1 == OSAL_ERROR)
		{
			filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
		}
		
		if(filehandle1 != OSAL_ERROR)
		{
			u8WriteBuf = (tU8*)OSAL_pvMemoryAllocate(1024*1024);
			
			for(u32Index=0;u32Index<(1024*1024);u32Index++)
			{
				*(u8WriteBuf+u32Index) = (u32Index%256);
			}
			
			//write in to the file
			if(OSAL_s32IOWrite(filehandle1,(tPCS8)u8WriteBuf,(1024*1024)) == OSAL_ERROR)
			{
				s32Retval = 11;
			}
			
			
			if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
			{
				//file remove error
				s32Retval = 10;
			}
			
			
			// read from the file
			filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
			if(filehandle1 != OSAL_ERROR)
			{
				u8ReadBuf = (tU8*)OSAL_pvMemoryAllocate(1024*1024);
				
				//write in to the file
				if(OSAL_s32IORead(filehandle1,(tPS8)u8ReadBuf,(1024*1024)) == OSAL_ERROR)
				{
					s32Retval = 9;
				}
				
				
				if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
				{
					//file remove error
					s32Retval = 8;
				}
				
				// compare the written and read bytes
				
				for(u32Index=0;u32Index<(1024*1024);u32Index++)
				{
					if((*(u8WriteBuf+u32Index)) !=(*(u8ReadBuf+u32Index)))
					{
						s32Retval =7;
						break;
					}
				}
				
				
			}
			else
			{
				// opening the file for read failed
				s32Retval = 6;
				
			}
			
			OSAL_vMemoryFree(u8WriteBuf);
			OSAL_vMemoryFree(u8ReadBuf);
			
		    /*remove oedt.txt file*/
			if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt.txt") != OSAL_OK)
			{
				if(s32Retval == 0)
				{
					//file remove error
					s32Retval = 3;
				}
			}
		}
		else
		{
			// file create error
			s32Retval = 5;
		}
		
		
		//close the devie
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			s32Retval = 2;
		}
		
		
	}
	else
	{
		//device cannnot be openend
		s32Retval = 1;
		
	} 
	
	return s32Retval;
}

 
/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelOpen
*     
*
* DESCRIPTION: This test tries to open 2 diferent file from 2
*              different threads at the same time 
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
* TEST CASE:    TU_OEDT_CARD_006 
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/

tS32 u32CardFileParallelOpen(void)
{
	OSAL_tIODescriptor devicehandle;
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Open_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Open_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	/*thread stuff*/
	
	/*create events*/
	if(OSAL_OK == OSAL_s32EventCreate("Parallel_Open", &parallel_open))
	{
		devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
		if(devicehandle != OSAL_ERROR)
		{
			/*create and activate thread1 */
			TID_1 = OSAL_ThreadSpawn(&rThAttr1);
			if(OSAL_ERROR != TID_1)
			{
				/*create thread2*/
				TID_2 = OSAL_ThreadSpawn(&rThAttr2);
				if(OSAL_ERROR != TID_2)
				{
					//wait for 2 events signalling the end of read operation
					if(OSAL_OK != OSAL_s32EventWait(parallel_open, EVENT_WAIT,
						OSAL_EN_EVENTMASK_AND, EVENT_WAIT_TIMEOUT, 
						&ResultMask))
					{
						if(s32ParallelRetval == 0)
						{
							//wait event failed
							s32ParallelRetval = 110;
						}
					}
					if(s32ParallelRetval == 0)
					{
						//close the file opened
						if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
							|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
						{															
							
								s32ParallelRetval = 111;
						}
						
						//remove the file created	
						if((OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
							|| (OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK))
						{
							if(s32ParallelRetval == 0)
							{
								s32ParallelRetval = 112;
							}
						}
					}		
				}
				else
				{
					//thread2 creation failed
					s32ParallelRetval = 106;
					
					if (OSAL_s32ThreadDelete(TID_1) != OSAL_OK)
					{
						//error closing file1 
						s32ParallelRetval = 107;
					}
					
				}
			}
			else
			{
				//thread1 creation failed
				s32ParallelRetval = 104;	
			}
			
			//close the devie
			if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
			{
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 105;
				}
			}
			
		}
		else
		{
			//device open failed
			s32ParallelRetval = 101;	
		}
		//close the event
		if(OSAL_ERROR == OSAL_s32EventClose(parallel_open))
		{
			//event close error
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 102;
			}	
		}
		//delete the event
		if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Open"))
		{
			//event delete error
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 103;
			}
		}
		
	}
	else
	{
		//Event creation failed
		s32ParallelRetval = 100;
	}
	return s32ParallelRetval; 
}

/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelClose
*     
*
* DESCRIPTION:  This test tries to close 2 diferent file from 2
*               different threads at the same time 
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
* TEST CASE:    TU_OEDT_CARD_007 
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/
tS32 u32CardFileParallelClose(void)
{
	OSAL_tIODescriptor devicehandle;
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Close_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Close_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	/*thread stuff*/
	
	/*create events*/
	if(OSAL_OK == OSAL_s32EventCreate("Parallel_Close", &parallel_close))
	{
		devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
		if(devicehandle != OSAL_ERROR)
		{
			filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
			
			if(filehandle1 == OSAL_ERROR)
			{	
				filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt",
					OSAL_EN_READWRITE);
			}
			if(filehandle1 != OSAL_ERROR)
			{
				filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", 
					OSAL_EN_READWRITE);
				
				if(filehandle2 == OSAL_ERROR)
				{	
					filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
						OSAL_EN_READWRITE);
				}
				if(filehandle2 != OSAL_ERROR)
				{
					
					/*create and activate thread1 */
					TID_1 = OSAL_ThreadSpawn(&rThAttr1);
					if(OSAL_ERROR != TID_1)
					{
						/*create thread2*/
						TID_2 = OSAL_ThreadSpawn(&rThAttr2);
						if(OSAL_ERROR != TID_2)
						{
						/*wait for 2 events signalling the end 
							of read operation*/
							if(OSAL_OK != OSAL_s32EventWait(parallel_close,
								EVENT_WAIT,
								OSAL_EN_EVENTMASK_AND,
								EVENT_WAIT_TIMEOUT,
								&ResultMask))
							{
								if(s32ParallelRetval == 0)
								{
									//wait event failed
									s32ParallelRetval = 215;
								}
							}
							
							//remove the file created	
							if((OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt")
								!= OSAL_OK) 
								|| (OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt")
								!= OSAL_OK))
							{
								if(s32ParallelRetval == 0)
								{
									s32ParallelRetval = 216;
								}
							}						
						}
						else
						{
							//thread2 creation failed
							s32ParallelRetval = 210;
							
							if (OSAL_s32ThreadDelete(TID_1) != OSAL_OK)
							{
							   	//error closing file1 
								s32ParallelRetval = 211;
								
							}
							
							if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
								||(OSAL_s32IOClose(filehandle2) != OSAL_OK))
							{
							   	s32ParallelRetval = 212;
							}
						}
					}
					else
					{
						//thread1 creation failed
						s32ParallelRetval = 208;
						
						if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
							||(OSAL_s32IOClose(filehandle2) != OSAL_OK))
						{
							 s32ParallelRetval = 209;
						}
					}
					
				}
				else
				{
					//file2 open failed
					s32ParallelRetval = 206;
					
					if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
					{
					   	s32ParallelRetval = 207;
					}
				}
			}
			else
			{
				//File1 open failed
				s32ParallelRetval = 204;	
			}
			
			//close the devie
			if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
			{
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 205;
				}
			}
		}
		else
		{
			//device open failed
			s32ParallelRetval = 201;	
		}
		//close the event
		if(OSAL_ERROR == OSAL_s32EventClose(parallel_close))
		{
			//event close error
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 202;
			}	
		}
		//delete the event
		if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Close"))
		{
			//event delete error
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 203;
			}
		}
	}
	else
	{
		//Event creation failed
		s32ParallelRetval = 200;
	}

	return s32ParallelRetval; 
}

/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelRead
*     
*
* DESCRIPTION:  This test tries to Read from 2 different file from 2
*               different threads at the same time 
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
* TEST CASE:    TU_OEDT_CARD_008 
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/

tS32 u32CardFileParallelRead(void)
{
	OSAL_tIODescriptor devicehandle;
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	/**************************************************/
	tU32 u32Cnt = 1;
	tU32 u32Cnt_Int=0;
	tU32 u32Cnt_dat = 65; /*Ascii value of "A"*/
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Read_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Read_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	/*thread stuff*/
	
	ps8UserBuf = (tS8*)OSAL_pvMemoryAllocate(1024*1024*sizeof(tS8));
	if(OSAL_NULL != ps8UserBuf)
	{
		for(;u32Cnt <=2048;u32Cnt++)
		{
			for(;u32Cnt_Int<(512*u32Cnt);u32Cnt_Int++)
			{
				ps8UserBuf[u32Cnt_Int] = (tS8)u32Cnt_dat;
			}
			
			++u32Cnt_dat;
			if(91 == u32Cnt_dat)
			{
				u32Cnt_dat = 65;	
			}  	
		}
		
		/*create events*/
		if(OSAL_OK == OSAL_s32EventCreate("Parallel_Read", &parallel_read))
		{
			devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
			if(devicehandle != OSAL_ERROR)
			{
				filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", 
					OSAL_EN_READWRITE);
				
				if(filehandle1 == OSAL_ERROR)
				{	
					filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", 
						OSAL_EN_READWRITE);
				}
				
				if(filehandle1 != OSAL_ERROR)
				{
					filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", 
						OSAL_EN_READWRITE);
					
					if(filehandle2 == OSAL_ERROR)
					{	
						filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
							OSAL_EN_READWRITE);
					}
					
					if(filehandle2 != OSAL_ERROR)
					{   
						//write in to the file
						if(OSAL_s32IOWrite(filehandle1,ps8UserBuf,
							(1048576)) != OSAL_ERROR)
						{
							if(OSAL_s32IOWrite(filehandle2,ps8UserBuf,
								(1048576)) != OSAL_ERROR)
							{
								if((OSAL_s32IOClose(filehandle1) == OSAL_OK)
									&& (OSAL_s32IOClose(filehandle2) == OSAL_OK))
								{ 
									filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt",
										OSAL_EN_READWRITE);
									if(filehandle1 != OSAL_ERROR)
									{
										
										filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
											OSAL_EN_READWRITE);
										if(filehandle2 != OSAL_ERROR)
										{
											
											/*create and activate thread1 */
											TID_1 = OSAL_ThreadSpawn(&rThAttr1);
											if(OSAL_ERROR != TID_1)
											{
												/*create thread2*/
												TID_2 = OSAL_ThreadSpawn(&rThAttr2);
												if(OSAL_ERROR != TID_2)
												{
													
													//wait for 2 events signalling the end of read operation
													if(OSAL_OK != OSAL_s32EventWait(parallel_read, EVENT_WAIT,
														OSAL_EN_EVENTMASK_AND, EVENT_WAIT_TIMEOUT, 
														&ResultMask))
													{
														//wait event failed
														s32ParallelRetval = 425;
													}
												}
												else
												{
													s32ParallelRetval = 419;
													
													if (OSAL_s32ThreadDelete(TID_1)
														!= OSAL_OK)
													{
													   	 //error closing file1 
														 s32ParallelRetval = 420;
											    	}
												}
											}
											else
											{
												//thread 1 creationn failed
												s32ParallelRetval = 418;
											}
											
											//close the file opened
											if((OSAL_s32IOClose(filehandle1)
												!= OSAL_OK)
												|| (OSAL_s32IOClose(filehandle2)
												!= OSAL_OK))
											{																	
												if(s32ParallelRetval == 0)
												{
													s32ParallelRetval = 223;
												}
											}
										}
										else
										{
											// error opening file2
											s32ParallelRetval = 416;
											
											if(OSAL_s32IOClose(filehandle1) 
												!= OSAL_OK)
											{
												s32ParallelRetval = 417;	
											}
										}
									}
									else
									{
										//file1 open failed
										s32ParallelRetval = 415; 
									}
								}
								else
								{
									//file close for write failed
									s32ParallelRetval = 414;
									
								}
							}
							else
							{
								// error opening file1
								if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
									|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
								{
								    s32ParallelRetval = 413;	
								}							
							}
						}
						else
						{
							//write to file1 failed
							s32ParallelRetval = 410;
							
							//close file1 that has opened
							if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
								|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
							{
								s32ParallelRetval = 411;	
							}
						}

						/*Remove the files created*/
						if((OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK) || 
						   (OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK))
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 412;
							}
						}
					}
					else
					{
						//error opening file2
						s32ParallelRetval = 407;
						
						//close file1 that has opened
						if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
						{
							s32ParallelRetval = 408;
						}
						
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
						{
							//file remove error
							s32ParallelRetval = 409;
						}
					}
				}
				else
				{
					//error opening file1
					s32ParallelRetval = 405;	
				}
				//close the devie
				if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
				{
					if(s32ParallelRetval == 0)
					{
						s32ParallelRetval = 406;
					}
				}
			}
			else
			{
				//Error opening the device
				s32ParallelRetval = 402;
			}
			//close the event
			if(OSAL_ERROR == OSAL_s32EventClose(parallel_read))
			{
				//event close error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 403;
				}	
			}
			//delete the event
			if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Read"))
			{
				//event delete error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 404;
				}
			}
		}
		else
		{
			s32ParallelRetval = 401;
		}

		OSAL_vMemoryFree(ps8UserBuf);
	}
	else
	{
		s32ParallelRetval = 400;
	}

	return s32ParallelRetval;
}


/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelWrite
*     
*
* DESCRIPTION:  This test tries to Write from 2 different file from 2
*               different threads at the same time 
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
*
* TEST CASE:    TU_OEDT_CARD_009 
*
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/
tS32 u32CardFileParallelWrite(void)
{
	OSAL_tIODescriptor devicehandle;
	tU32 u32Count;
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	/**************************************************/
	tU32 u32Cnt = 1;
	tU32 u32Cnt_Int=0;
	tU32 u32Cnt_dat = 65; /*Ascii value of "A"*/
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Write_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Write_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	/*thread stuff*/
	
	ps8UserBuf = (tS8*)OSAL_pvMemoryAllocate(1024*1024*sizeof(tS8));
	if(OSAL_NULL != ps8UserBuf)
	{
		for(;u32Cnt <=2048;u32Cnt++)
		{
			for(;u32Cnt_Int<(512*u32Cnt);u32Cnt_Int++)
			{
				ps8UserBuf[u32Cnt_Int] = (tS8)u32Cnt_dat;
			}
			
			++u32Cnt_dat;
			if(91 == u32Cnt_dat)
			{
				u32Cnt_dat = 65;	
			}  	
		}
		
		/*create events*/
		if(OSAL_OK == OSAL_s32EventCreate("Parallel_Write", &parallel_write))
		{
			devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
			if(devicehandle != OSAL_ERROR)
			{
				filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt",
					OSAL_EN_READWRITE);
				
				if(filehandle1 == OSAL_ERROR)
				{	
					filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt",
						OSAL_EN_READWRITE);
				}
				
				if(filehandle1 != OSAL_ERROR)
				{
					filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
						OSAL_EN_READWRITE);
					
					if(filehandle2 == OSAL_ERROR)
					{	
						filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
							OSAL_EN_READWRITE);
					}
					
					if(filehandle2 != OSAL_ERROR)
					{   
						/*create and activate thread1 */
						TID_1 = OSAL_ThreadSpawn(&rThAttr1);
						if(OSAL_ERROR != TID_1)
						{
							/*create thread2*/
							TID_2 = OSAL_ThreadSpawn(&rThAttr2);
							if(OSAL_ERROR != TID_2)
							{
							/*wait for 2 events signalling the end of write
								operation*/
								if(OSAL_OK == OSAL_s32EventWait(parallel_write, 
									EVENT_WAIT,
									OSAL_EN_EVENTMASK_AND,
									EVENT_WAIT_TIMEOUT, 
									&ResultMask))
								{
									if((OSAL_s32IOClose(filehandle1) == OSAL_OK)
										&& (OSAL_s32IOClose(filehandle2) == OSAL_OK))
									{						
										// read from the file
										filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", 
											OSAL_EN_READWRITE);
										if(filehandle1 != OSAL_ERROR)
										{
											ps8ReadBuf_1 = (tS8*)OSAL_pvMemoryAllocate(1048576);
											if(ps8ReadBuf_1 != OSAL_NULL )
											{
												//read from the file
												if(OSAL_s32IORead(filehandle1,
													ps8ReadBuf_1,
													1048576)
													!= OSAL_ERROR)
												{
													
													// compare the written and read bytes
													for(u32Count = 0;u32Count <1048576;u32Count++)
													{
														if(ps8ReadBuf_1[u32Count] != ps8UserBuf[u32Count])
														{
															//write to file corrupted
															s32ParallelRetval = 324;
															break;
														}  
													}
													
													if(s32ParallelRetval == 0)
													{
														filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
														if(filehandle2 != OSAL_ERROR)
														{
															ps8ReadBuf_2 = (tS8*)OSAL_pvMemoryAllocate(1048576);
															if(ps8ReadBuf_2 != OSAL_NULL )
															{
																//read from to the file
																if(OSAL_s32IORead(filehandle2,ps8ReadBuf_2,1048576) != OSAL_ERROR)
																{
																	// compare the written and read bytes
																	for(u32Count = 0;u32Count <1048576;u32Count++)
																	{
																		if(ps8ReadBuf_2[u32Count] != ps8UserBuf[u32Count])
																		{
																			s32ParallelRetval = 328;
																			break;
																		}  
																	}
																	//close the file opened
																	if(OSAL_s32IOClose(filehandle2) != OSAL_OK)
																	{
																		if(s32ParallelRetval == 0)
																		{
																			s32ParallelRetval = 329;
																		}
																	}
																	OSAL_vMemoryFree(ps8ReadBuf_2);
																}
																else
																{
																	//error reading from file2
																	s32ParallelRetval = 327;
																}
															}
															else
															{
																//error allocating memory
																s32ParallelRetval = 326;
															}
														}	
														else
														{
															// opening the file2 for read failed
															s32ParallelRetval = 325;
														}
														
													}
												}
												else
												{
													//read from file1 failed
													s32ParallelRetval = 322;
												}
												
												if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
												{
													if(s32ParallelRetval == 0)
													{
														//file remove error
														s32ParallelRetval = 323;
													}
												}
												OSAL_vMemoryFree(ps8ReadBuf_1);
												// read from the file
												
											}
											else
											{
												//memory allocation for read failed
												s32ParallelRetval = 321;
											}
										}
										else
										{
											//error opening file1 for read
											s32ParallelRetval = 320;
										}															
									}
									else
									{
										//error closing files that was opened for writing
										s32ParallelRetval = 319;	
									}	
								}	
								else
								{
									//event wait failed
									s32ParallelRetval = 318;
								}
							}
							else
							{
								//thread2 creation failed
								s32ParallelRetval = 313;
								
								if (OSAL_s32ThreadDelete(TID_1) != OSAL_OK)
								{
									//error closing file1 
									s32ParallelRetval = 314;
								}
								
								if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
									|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
								{
								   	//error closing file1 
									s32ParallelRetval = 315;
								}
							}
						}
						else
						{
							//thread1 creation failed
							s32ParallelRetval = 310;
							
							if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
								|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
							{
								//error closing file1 
								s32ParallelRetval = 311;
							}
						}

						if((OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)|| 
						   (OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK))
						{					
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 312;
							}
						} 
					}	
					else
					{
						//error opening 2nd file
						s32ParallelRetval = 307;
						
						//close file1 that has opened
						if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
						{
							s32ParallelRetval = 308;	
						}
						
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
						{
						   //file remove error
						   s32ParallelRetval = 309;
						}
					}
				}
				else
				{
					//error opening 1st file
					s32ParallelRetval = 305;
				}

				//close the devie
				if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
				{
					if(s32ParallelRetval == 0)
					{
						s32ParallelRetval = 306;
					}
				}	
			}
			else
			{
				//device cannnot be openend
				s32ParallelRetval = 302;	
			}

			//close the event
			if(OSAL_ERROR == OSAL_s32EventClose(parallel_write))
			{
				//event close error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 303;
				}	
			}

			//delete the event
			if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Write"))
			{
				//event delete error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 304;
				}
			}
		}
		else
		{
			//error creating events
			s32ParallelRetval = 301;
		}

		OSAL_vMemoryFree(ps8UserBuf);	
	}
	else
	{
		s32ParallelRetval = 300; //Null buffer
	}

	return s32ParallelRetval;
}

/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelReadWrite
*     
*
* DESCRIPTION:  This test tries to Write into a file a thread
*               and reads from another file from another thread
*               at the same time 
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
*
* TEST CASE:    TU_OEDT_CARD_010  
*
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/
tS32 u32CardFileParallelReadWrite(void)
{
	tU32 u32Count;
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	OSAL_tIODescriptor devicehandle; 
	/**************************************************/
	tU32 u32Cnt = 1;
	tU32 u32Cnt_Int=0;
	tU32 u32Cnt_dat = 65; /*Ascii value of "A"*/
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_ReadWrite_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_ReadWrite_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	/*thread stuff*/
	ps8UserBuf = (tS8*)OSAL_pvMemoryAllocate(1024*1024*sizeof(tS8));
	if(ps8UserBuf != OSAL_NULL)
	{
		for(;u32Cnt <=2048;u32Cnt++)
		{
			for(;u32Cnt_Int<(512*u32Cnt);u32Cnt_Int++)
			{
				ps8UserBuf[u32Cnt_Int] = (tS8)u32Cnt_dat;
			}
			
			++u32Cnt_dat;
			if(91 == u32Cnt_dat)
			{
				u32Cnt_dat = 65;	
			}  	
			
		}
		if(OSAL_OK == OSAL_s32EventCreate("Parallel_RW", &parallel_rw))
		{
			devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
			if(devicehandle != OSAL_ERROR)
			{
				filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", 
					OSAL_EN_READWRITE);
				
				if(filehandle1 == OSAL_ERROR)
				{	
					filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", 
						OSAL_EN_READWRITE);
				}
				
				if(filehandle1 != OSAL_ERROR)
				{
					filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", 
						OSAL_EN_READWRITE);
					
					if(filehandle2 == OSAL_ERROR)
					{	
						filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt", 
							OSAL_EN_READWRITE);
					}
					
					if(filehandle2 != OSAL_ERROR)
					{
						if(OSAL_s32IOWrite(filehandle2,ps8UserBuf,
							(1024*1024)) != OSAL_ERROR)
						{
							if(OSAL_s32IOClose(filehandle2) == OSAL_OK)
							{
								if(OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt",
									OSAL_EN_READWRITE) != OSAL_ERROR)
								{
									/*create and activate thread1 */
									TID_1 = OSAL_ThreadSpawn(&rThAttr1);
									if(OSAL_ERROR != TID_1)
									{
										/*create thread2*/
										TID_2 = OSAL_ThreadSpawn(&rThAttr2);
										if(OSAL_ERROR != TID_2)
										{
											if(OSAL_OK == OSAL_s32EventWait(parallel_rw,
												EVENT_WAIT,
												OSAL_EN_EVENTMASK_AND, 
												EVENT_WAIT_TIMEOUT, 
												&ResultMask))
											{
												//close file1
												if((OSAL_s32IOClose(filehandle1)
													!= OSAL_OK)|| 
													(OSAL_s32IOClose(filehandle2)
													!= OSAL_OK))
												{
													if(s32ParallelRetval == 0)
													{
														s32ParallelRetval = 526;
													}
												}
												
												// read from the file
												filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
												if(filehandle1 != OSAL_ERROR)
												{
													ps8ReadBuf_1 = (tS8*)OSAL_pvMemoryAllocate(1048576);
													
													if(ps8ReadBuf_1 != OSAL_NULL )
													{
														//read from to the file
														if(OSAL_s32IORead(filehandle1,ps8ReadBuf_1,1048576) != OSAL_ERROR)
														{
															if(OSAL_s32IOClose(filehandle1) == OSAL_OK)
															{
																//check for errors in read and writing
																for(u32Count = 0;u32Count <1048576;u32Count++)
																{
																	if(ps8ReadBuf_1[u32Count] != ps8UserBuf[u32Count])
																	{
																		//write to file corrupted
																		s32ParallelRetval = 531;
																		break;
																	}
																	
																	if(ps8ReadBuf_2[u32Count] != ps8UserBuf[u32Count])
																	{
																		//write to file corrupted
																		s32ParallelRetval = 532;
																		break;
																	}	  
																}
															}
															else
															{
																// close operation for file1 failed
																s32ParallelRetval = 530;
															}		
														}
														else
														{
															//read operation for file1 failed
															s32ParallelRetval = 529;
														}
														
														OSAL_vMemoryFree(ps8ReadBuf_1);	
														
													}
													else
													{
														//error allocating memory
														s32ParallelRetval = 528;	
													}
												}
												else
												{
													//error opening file1
													s32ParallelRetval = 527;
												}
												
												OSAL_vMemoryFree(ps8ReadBuf_2);
											}
											else
											{
												//event wait failed
												s32ParallelRetval = 525;
											}	
										}
										else
										{
											//thread2 creation and activation failed
											s32ParallelRetval = 519;
											
											if (OSAL_s32ThreadDelete(TID_1)
												!= OSAL_OK)
											{
												//error closing file1 
												s32ParallelRetval = 520;
											}
											
											//close file1
											if((OSAL_s32IOClose(filehandle1)
												!= OSAL_OK)
												||(OSAL_s32IOClose(filehandle2)
												!= OSAL_OK))
											{
												s32ParallelRetval = 521;
											}
										}
									}
									else
									{
										//thread1 creation and activation failed
										s32ParallelRetval = 517;	
										
										//close file1
										if((OSAL_s32IOClose(filehandle1) 
											!= OSAL_OK)
											||(OSAL_s32IOClose(filehandle2)
											!= OSAL_OK))
											
										{
											s32ParallelRetval = 518;
										}
									}
								}
								else
								{
									//open failed for reading
									s32ParallelRetval = 515;
									
									//close file1
									if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
									{
										s32ParallelRetval = 516;
									}
								}			
							}
							else
							{
								//file2 close failed
								s32ParallelRetval = 513;
								
								//close file1
								if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
								{
									s32ParallelRetval = 514;
								}
							}		
						}
						else
						{
							//write operation failed
							s32ParallelRetval = 510;
							
							//close file1
							if((OSAL_s32IOClose(filehandle1) != OSAL_OK)
								|| (OSAL_s32IOClose(filehandle2) != OSAL_OK))
							{
								s32ParallelRetval = 511;
							}
						}

						if((OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK) || 
						   (OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK))
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 512;
							}
						}	
					}
					else
					{
						//error creation of file2
						s32ParallelRetval = 507;
						
						//close file1
						if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
						{
						   s32ParallelRetval = 508;
						}
						
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
						{
							 //file remove error
							 s32ParallelRetval = 509;
						}	
					}
				}
				else
				{
					//file1 creation failed
					s32ParallelRetval = 505;
					
				}

				//close the devie
				if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
				{
					if(s32ParallelRetval == 0)
					{
						s32ParallelRetval = 506;
					}
				}	 
			}
			else
			{
				//device handle creation failed
				s32ParallelRetval = 502;
			}

			//close the event
			if(OSAL_ERROR == OSAL_s32EventClose(parallel_rw))
			{
				//event close error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 503;
				}	
			}

			//delete the event
			if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_RW"))
			{
				//event delete error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 504;
				}
			}
		}
		else
		{
			//event creation failed
			s32ParallelRetval = 501;
		}

		//free the memory allocated
		OSAL_vMemoryFree(ps8UserBuf);
	}
	else
	{
		//memory alocation failed
		s32ParallelRetval = 500;
	}

	return s32ParallelRetval; 		
}

/*****************************************************************************
*
* FUNCTION:	u32CardFileParallelAccess
*     
*
* DESCRIPTION:	 This test tries to open , close and do the IOP from 2 
*               different threds at the same time 
*
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
*
* TEST CASE:    TU_OEDT_CARD_011 
*
* HISTORY:		Taken form Paramount platform 
*
*****************************************************************************/
tS32 u32CardFileParallelAccess(void)
{
	OSAL_trThreadAttribute rThAttr1, rThAttr2;
	OSAL_tThreadID TID_1, TID_2;
	/**************************************************/
	tU32 u32Cnt = 1;
	tU32 u32Cnt_Int=0;
	tU32 u32Cnt_dat = 65; /*Ascii value of "A"*/
	s32ParallelRetval = 0;
	
	/*thread stuff*/
	rThAttr1.szName  = "Thread_1";
	rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_Access_Thread_1;
	rThAttr1.pvArg   =   (tPVoid)NULL;
	rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr1.s32StackSize = THREAD_STACK_SIZE;
	
	rThAttr2.szName  = "Thread_2";
	rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_Access_Thread_2;
	rThAttr2.pvArg   =   (tPVoid)NULL;
	rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	rThAttr2.s32StackSize = THREAD_STACK_SIZE;
	
	/*thread stuff*/
	ps8UserBuf = (tS8*)OSAL_pvMemoryAllocate(1024*1024*sizeof(tS8));
	if(ps8UserBuf != OSAL_NULL)
	{
		for(;u32Cnt <=2048;u32Cnt++)
		{
			for(;u32Cnt_Int<(512*u32Cnt);u32Cnt_Int++)
			{
				ps8UserBuf[u32Cnt_Int] = (tS8)u32Cnt_dat;
			}
			
			++u32Cnt_dat;
			if(91 == u32Cnt_dat)
			{
				u32Cnt_dat = 65;	
			}  	
			
		}
		if(OSAL_OK == OSAL_s32EventCreate("Parallel_Access", &parallel_access))
		{
			/*create and activate thread1 */
			TID_1 = OSAL_ThreadSpawn(&rThAttr1);
			if(OSAL_ERROR != TID_1)
			{
				/*create thread2*/
				TID_2 = OSAL_ThreadSpawn(&rThAttr2);
				if(OSAL_ERROR != TID_2)
				{
					//wait for 2 events signalling the end of read operation
					if(OSAL_OK != OSAL_s32EventWait(parallel_access, EVENT_WAIT,
						OSAL_EN_EVENTMASK_AND, EVENT_WAIT_TIMEOUT, 
						&ResultMask))
					{
						if(s32ParallelRetval == 0)
						{
							//wait event failed
							s32ParallelRetval = 633;
						}
					}
				}
				else
				{
					//thread2 creation failed
					s32ParallelRetval = 605;
					
					//delete the thread1 created
					if (OSAL_s32ThreadDelete(TID_1) != OSAL_OK)
					{
						//error closing file1 
						s32ParallelRetval = 606;
					} 
				}
			}
			else
			{
				// thread1 creation failed
				s32ParallelRetval = 602;
			}
			
			//close the event
			if(OSAL_ERROR == OSAL_s32EventClose(parallel_access))
			{
				//event close error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 603;
				}	
			}
			//delete the event
			if(OSAL_ERROR == OSAL_s32EventDelete("Parallel_Access"))
			{
				//event delete error
				if(s32ParallelRetval == 0)
				{
					s32ParallelRetval = 604;
				}
			}
		}	 
		else
		{
			//event creation failed
			s32ParallelRetval = 601;
		}
		
		//free the memory allocated
		OSAL_vMemoryFree(ps8UserBuf);
	}
	else
	{	//error allocating memory 
		s32ParallelRetval = 600;
	}
	
	return s32ParallelRetval; 
}


static tVoid Parallel_Access_Thread_1(tPVoid pvThread2Arg)
{
	
	OSAL_tIODescriptor devicehandle1,filehandle1; /*lint !e578 */ /* same name desired*/
	tU32 u32Index;
	tS8 *ps8ReadBuf_1; /*lint !e578 */ /* same name desired*/
	
	(tVoid)pvThread2Arg;
	
	devicehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle1 != OSAL_ERROR)
	{
		
		filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
		
		if(filehandle1 == OSAL_ERROR)
		{
			filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
		}
		
		if(filehandle1 != OSAL_ERROR)
		{			
			//write in to the file
			if(OSAL_s32IOWrite(filehandle1,ps8UserBuf,(1024*1024))
				!= OSAL_ERROR)
			{
				
				if(OSAL_s32IOClose(filehandle1) == OSAL_OK)
				{
					// read from the file
					filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
					if(filehandle1 != OSAL_ERROR)
					{
						ps8ReadBuf_1 = (tS8*)OSAL_pvMemoryAllocate(1024*1024);
						
						//Read from the file
						if(OSAL_s32IORead(filehandle1,ps8ReadBuf_1,(1024*1024))
							!= OSAL_ERROR)
						{
							
							
							// compare the written and read bytes
							
							for(u32Index=0;u32Index<(1024*1024);u32Index++)
							{
								if((*(ps8UserBuf+u32Index)) !=(*(ps8ReadBuf_1+u32Index)))
								{
									s32ParallelRetval =619;
									break;
								}
							}
							
						}
						else
						{
							//read operation failed
							s32ParallelRetval = 616;
						}
						
						if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 617;
							}
						}			
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 618;
							}
						}
						
						OSAL_vMemoryFree(ps8ReadBuf_1);
					}
					else
					{
						// opening the file for read failed
						s32ParallelRetval = 614;
						
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
						{
						    //file remove error
							s32ParallelRetval = 615;
						}
					}
				}
				else
				{
					//error closing the file1
					s32ParallelRetval = 613; 
				}
			}
			else
			{
				//write failed
				s32ParallelRetval = 610;
				
				if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
				{
					//file remove error
					s32ParallelRetval = 611;
				}
				
				if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/rbin.txt") != OSAL_OK)
				{
					//file remove error
					s32ParallelRetval = 612;
				}
			}
		}
		else
		{
			// file create error
			s32ParallelRetval = 608;
		}

		//close the devie
		if(OSAL_s32IOClose(devicehandle1) != OSAL_OK)
		{
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 609;
			}
		}	
	}
	else
	{
		//device cannnot be openend
		s32ParallelRetval = 607;
	}

	//post event for thread1 completion  
	OSAL_s32EventPost(parallel_access, EVENT_POST_THREAD_1,
					  OSAL_EN_EVENTMASK_OR);

}

static tVoid Parallel_Access_Thread_2(tPVoid pvThread2Arg)
{
	
	OSAL_tIODescriptor devicehandle2,filehandle2; /*lint !e578 */ /* same name desired*/
	tU32 u32Index;
	tS8 *ps8ReadBuf_2;  /*lint !e578 */ /* same name desired*/
	
	(tVoid)pvThread2Arg;
	
	devicehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle2 != OSAL_ERROR)
	{
		
		filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
		
		if(filehandle2 == OSAL_ERROR)
		{
			filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
		}
		
		if(filehandle2 != OSAL_ERROR)
		{			
			//write in to the file
			if(OSAL_s32IOWrite(filehandle2,ps8UserBuf,(1024*1024)) 
				!= OSAL_ERROR)
			{
				if(OSAL_s32IOClose(filehandle2) == OSAL_OK)
				{
					// read from the file
					filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
					if(filehandle2 != OSAL_ERROR)
					{
						ps8ReadBuf_2 = (tS8*)OSAL_pvMemoryAllocate(1024*1024);
						
						//Read from the file
						if(OSAL_s32IORead(filehandle2,ps8ReadBuf_2,(1024*1024))
							!= OSAL_ERROR)
						{
							// compare the written and read bytes
							
							for(u32Index=0;u32Index<(1024*1024);u32Index++)
							{
								if((*(ps8UserBuf+u32Index)) !=(*(ps8ReadBuf_2+u32Index)))
								{
									s32ParallelRetval =632;
									break;
								}
							}
							
						}
						else
						{
							//read operation failed
							s32ParallelRetval = 629;
						}
						
						if(OSAL_s32IOClose(filehandle2) != OSAL_OK)
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 630;
							}
						}			
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK)
						{
							if(s32ParallelRetval == 0)
							{
								//file remove error
								s32ParallelRetval = 631;
							}
						}
						
						OSAL_vMemoryFree(ps8ReadBuf_2);
					}
					else
					{
						// opening the file for read failed
						s32ParallelRetval = 627;
						
						if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK)
						{
							//file remove error
							s32ParallelRetval = 628;
						}	
					}
				}
				else
				{
					//error closing the file1
					s32ParallelRetval = 626;
				}
				
			}
			else
			{
				//write failed
				s32ParallelRetval = 623;
				
				if(OSAL_s32IOClose(filehandle2) != OSAL_OK)
				{
				   	//file remove error
					s32ParallelRetval = 624;
				}
				
				if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/bp.txt") != OSAL_OK)
				{
					//file remove error
					s32ParallelRetval = 625;
				}
			}
		}
		else
		{
			// file create error
			s32ParallelRetval = 621;
		}
		
		//close the devie
		if(OSAL_s32IOClose(devicehandle2) != OSAL_OK)
		{
			if(s32ParallelRetval == 0)
			{
				s32ParallelRetval = 622;
			}
		}	
	}
	else
	{
		//device cannnot be openend
		s32ParallelRetval = 620;	
	} 

	//post event for thread2 completion  
	OSAL_s32EventPost(parallel_access, EVENT_POST_THREAD_2,
					  OSAL_EN_EVENTMASK_OR);

}

/*****************************************************************************
* FUNCTION:		Parallel_Write_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/
static tVoid Parallel_Write_Thread_1(tPVoid pvThread1Arg)
{
	
	(tVoid)pvThread1Arg;
	
	//write in to the file
	if(OSAL_s32IOWrite(filehandle1,ps8UserBuf,(1048576)) == OSAL_ERROR)
	{
		s32ParallelRetval = 316;
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_write, EVENT_POST_THREAD_1,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Write_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_Write_Thread_2(tPVoid pvThread2Arg)
{
	
	(tVoid)pvThread2Arg;
	
	//write in to the file
	if(OSAL_s32IOWrite(filehandle2,ps8UserBuf,(1048576)) == OSAL_ERROR)
	{
		s32ParallelRetval = 317;
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_write, EVENT_POST_THREAD_2,
		OSAL_EN_EVENTMASK_OR);
	
}

/*****************************************************************************
* FUNCTION:		Parallel_Read_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_Read_Thread_1(tPVoid pvThread1Arg)
{
	tU32 u32Thr1ReadCnt;
	(tVoid)pvThread1Arg;
	
	ps8ReadBuf_1 = (tS8*)OSAL_pvMemoryAllocate(1048576);
	if(ps8ReadBuf_1 != OSAL_NULL )
	{
		//read from to the file
		if(OSAL_s32IORead(filehandle1,ps8ReadBuf_1,1048576) != OSAL_ERROR)
		{
			
			// compare the written and read bytes
			for(u32Thr1ReadCnt = 0;u32Thr1ReadCnt <1048576;u32Thr1ReadCnt++)
			{
				if(ps8ReadBuf_1[u32Thr1ReadCnt] != ps8UserBuf[u32Thr1ReadCnt])
				{
					//write to file corrupted
					s32ParallelRetval = 422;
					break;
				}  
			}
		}
		OSAL_vMemoryFree(ps8ReadBuf_1);
	}
	else
	{
		s32ParallelRetval = 421;
	}	
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_read, EVENT_POST_THREAD_1,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Read_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/
static tVoid Parallel_Read_Thread_2(tPVoid pvThread2Arg)
{
	tU32 u32Thr2ReadCnt;
	(tVoid)pvThread2Arg;
	
	ps8ReadBuf_2 = (tS8*)OSAL_pvMemoryAllocate(1048576);
	if(ps8ReadBuf_2 != OSAL_NULL )
	{
		//read from to the file
		if(OSAL_s32IORead(filehandle2,ps8ReadBuf_2,1048576) != OSAL_ERROR)
		{
			// compare the written and read bytes
			for(u32Thr2ReadCnt = 0;u32Thr2ReadCnt <1048576;u32Thr2ReadCnt++)
			{
				if(ps8ReadBuf_2[u32Thr2ReadCnt] != ps8UserBuf[u32Thr2ReadCnt])
				{
					s32ParallelRetval = 424;
					break;
				}  
			}
		}
		OSAL_vMemoryFree(ps8ReadBuf_2);
	}
	else
	{
		s32ParallelRetval = 423;	
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_read, EVENT_POST_THREAD_2,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Open_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_Open_Thread_1(tPVoid pvThread1Arg)
{

	(tVoid)pvThread1Arg;
	filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
	if(filehandle1 == OSAL_ERROR)
	{	
		filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/rbin.txt", OSAL_EN_READWRITE);
	}
	if(filehandle1 == OSAL_ERROR)
	{
		if(s32ParallelRetval == 0)
		{
			s32ParallelRetval = 108;
		}
	}

	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_open, EVENT_POST_THREAD_1,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Open_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_Open_Thread_2(tPVoid pvThread2Arg)
{
	(tVoid)pvThread2Arg;

	filehandle2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
	if(filehandle2 == OSAL_ERROR)
	{	
		filehandle2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/bp.txt", OSAL_EN_READWRITE);
	}
	if(filehandle2 == OSAL_ERROR)
	{
		if(s32ParallelRetval == 0)
		{
			s32ParallelRetval = 109;
		}
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_open, EVENT_POST_THREAD_2,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Close_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_Close_Thread_1(tPVoid pvThread1Arg)
{
	(tVoid)pvThread1Arg;
	
	if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
	{
		s32ParallelRetval = 213;
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_close, EVENT_POST_THREAD_1,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_Close_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/
static tVoid Parallel_Close_Thread_2(tPVoid pvThread2Arg)
{
	(tVoid)pvThread2Arg;
	
	if(OSAL_s32IOClose(filehandle2) != OSAL_OK)
	{
		s32ParallelRetval = 214;
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_close, EVENT_POST_THREAD_2,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_ReadWrite_Thread_1()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_ReadWrite_Thread_1(tPVoid pvThread1Arg)
{
	(tVoid)pvThread1Arg;
	
	//write in to the file
	if(OSAL_s32IOWrite(filehandle1,ps8UserBuf,(1048576)) == OSAL_ERROR)
	{
		s32ParallelRetval = 522;
	}
	
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_rw, EVENT_POST_THREAD_1,
		OSAL_EN_EVENTMASK_OR);
	
}
/*****************************************************************************
* FUNCTION:		Parallel_ReadWrite_Thread_2()
* PARAMETER:    tPVoid
* RETURNVALUE:  None
* DESCRIPTION:  
* HISTORY:		Taken form Paramount platform 
******************************************************************************/

static tVoid Parallel_ReadWrite_Thread_2(tPVoid pvThread2Arg)
{
	(tVoid)pvThread2Arg;
	
	ps8ReadBuf_2 = (tS8*)OSAL_pvMemoryAllocate(1048576);
	
	if(ps8ReadBuf_2 != OSAL_NULL )
	{
		//read from to the file
		if(OSAL_s32IORead(filehandle2,ps8ReadBuf_2,1048576) == OSAL_ERROR)
		{
			s32ParallelRetval = 524;	
		}
	}
	else
	{
		s32ParallelRetval = 523;	
	}
	
		
	//post event for thread 2 completion  
	OSAL_s32EventPost(parallel_rw, EVENT_POST_THREAD_2,
		OSAL_EN_EVENTMASK_OR);
	
	
}

/*****************************************************************************
*
* FUNCTION:	u32CardFileReadWrite_Performance
*     
*
* DESCRIPTION:  This test tries to Write and read different sizes of files 
*               and stores the time taken and calculates the data rate
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
* TEST CASE:    TU_OEDT_CARD_012 
* HISTORY:		Taken form Paramount platform 
*				04.05.09 | Jeryn M (RBEI/ECF1) | Lint warnings removed 
*****************************************************************************/
tS32 u32CardFileReadWrite_Performance(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle,filehandle1;  /*lint !e578 */ /* same name desired*/
	tU8 *u8WriteBuf = NULL, *u8ReadBuf = NULL;
	tU32 u32Index, u32Count,u32NumBytes = 0;
	tFloat flAvgWrite_DataRate =0, flAvgRead_DataRate =0;
	tFloat aflData_Rate_Wr[8]= {0},aflData_Rate_Rd[8]={0};
	OSAL_tMSecond starttime_wr = 0, endtime_wr =0, totaltime_wr=0;
	OSAL_tMSecond starttime_rd = 0, endtime_rd =0, totaltime_rd=0;
	
	u8WriteBuf = (tU8*)OSAL_pvMemoryAllocate(1024*1024);
	u8ReadBuf = (tU8*)OSAL_pvMemoryAllocate(1024*1024);
	
	if(u8WriteBuf != OSAL_NULL && u8ReadBuf != OSAL_NULL)
	{
		for(u32Index=0;u32Index<(1024*1024);u32Index++)
		{
			*(u8WriteBuf+u32Index) = (u32Index%256);
		}
		
		devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
		if(devicehandle != OSAL_ERROR)
		{
			for(u32Count = 0;u32Count < 8;u32Count++)
			{
				
				switch(u32Count)
				{
				case 0:  u32NumBytes = 2048;   //2k
					break;
				case 1:  u32NumBytes = 10240;  //10k
					break;
				case 2:  u32NumBytes = 32768;  //32k
					break;
				case 3:  u32NumBytes = 51200;  //50k
					break;
				case 4:  u32NumBytes = 102400; //100k
					break;
				case 5:  u32NumBytes = 307200; //300k
					break;
				case 6:  u32NumBytes = 512000; //500k
					break;
				case 7:  u32NumBytes = 1048576;//1M
					break;
					
				default: break;
				}
				
				/*Initialize the time variables before write operation*/
				starttime_wr = 0;
				endtime_wr = 0;
				totaltime_wr = 0;
				
				filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt", OSAL_EN_READWRITE);
				
				if(filehandle1 == OSAL_ERROR)
				{
					filehandle1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD"/oedt.txt",
						OSAL_EN_READWRITE);
				}
				
				if(filehandle1 != OSAL_ERROR)
				{
					
					starttime_wr = OSAL_ClockGetElapsedTime();
					//write in to the file
					if(OSAL_s32IOWrite(filehandle1,(tPCS8)u8WriteBuf,u32NumBytes)
						== OSAL_ERROR)
					{
						s32Retval = 704;
					}
					
					endtime_wr = OSAL_ClockGetElapsedTime();
					totaltime_wr = endtime_wr - starttime_wr;
					/*calculating the data rate for read operation(MB/S)
					(converting msec to sec by multiplying with 1000 and 
					converting bytes to Megabytes by multiplying with 
					1024*1024 */
					aflData_Rate_Wr[u32Count] = (tFloat)(u32NumBytes*(tFloat)1000)/(totaltime_wr*(tFloat)(1024*1024));
					
					if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
					{
						//file remove error
						s32Retval = 705;
					}
					
					/*Initialize the time variables before read operation*/
					starttime_rd = 0;
					endtime_rd = 0;
					totaltime_rd = 0;
					// read from the file
					filehandle1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD"/oedt.txt",
						OSAL_EN_READWRITE);
					if(filehandle1 != OSAL_ERROR)
					{
						
						starttime_rd = OSAL_ClockGetElapsedTime();
						//Read from the file
						if(OSAL_s32IORead(filehandle1,(tPS8)u8ReadBuf,u32NumBytes) 
							== OSAL_ERROR)
						{
							s32Retval = 707;
						}
						endtime_rd = OSAL_ClockGetElapsedTime();
						totaltime_rd = endtime_rd - starttime_rd;
						
						/*calculating the data rate for read operation(MB/S)
						(converting msec to sec by multiplying with 1000 and 
						converting bytes to Megabytes by multiplying with 
						1024*1024 */
						aflData_Rate_Rd[u32Count] = (tFloat)(u32NumBytes*(tFloat)1000)/(totaltime_rd*(tFloat)(1024*1024));
						
						if(OSAL_s32IOClose(filehandle1) != OSAL_OK)
						{
							//file remove error
							s32Retval = 708;
						}
						
						// compare the written and read bytes
						
						for(u32Index=0;u32Index < u32NumBytes;u32Index++)
						{
							if((*(u8WriteBuf+u32Index)) !=(*(u8ReadBuf+u32Index)))
							{
								s32Retval =709;
								break;
							}
						}
					}
					else
					{
						// opening the file for read failed
						s32Retval = 706;
						
					}				
				}
				else
				{
					// file create error
					s32Retval = 703;
				}
			}	

			if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt.txt") != OSAL_OK)
			{
				if(s32Retval == 0)
				{
					//file remove error
					s32Retval = 710;
				}
			}

			//close the devie
			if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
			{
				if(s32Retval == 0)
				{
					s32Retval = 702;
				}
			}								
		}
		else
		{
			//device cannnot be openend
			s32Retval = 701;
			
		} 

		OSAL_vMemoryFree(u8WriteBuf);
		OSAL_vMemoryFree(u8ReadBuf);
	}
	else
	{
		//error buffer allocation failed
		s32Retval = 700;
	}

	if(s32Retval == 0)
	{
		OEDT_HelperPrintf( TR_LEVEL_USER_1,"The Data Rate for Read and Write operations are:\n" );
		for(u32Count = 0;u32Count < 8;u32Count++)
		{
			switch(u32Count)
			{
			case 0:  u32NumBytes = 2;   //2k
				break;
			case 1:  u32NumBytes = 10;  //10k
				break;
			case 2:  u32NumBytes = 32;  //32k
				break;
			case 3:  u32NumBytes = 50;  //50k
				break;
			case 4:  u32NumBytes = 100; //100k
				break;
			case 5:  u32NumBytes = 300; //300k
				break;
			case 6:  u32NumBytes = 500; //500k
				break;
			case 7:  u32NumBytes = 1024;//1M
				break;
				
			default: break;
			}
			
			flAvgWrite_DataRate = 	flAvgWrite_DataRate + aflData_Rate_Wr[u32Count];
			flAvgRead_DataRate = flAvgRead_DataRate + aflData_Rate_Rd[u32Count];
			OEDT_HelperPrintf
			( 
				TR_LEVEL_USER_1, 
				"The data rate for %dK Read is	:	%f MB/S \t",
				u32NumBytes, 
				aflData_Rate_Rd[u32Count]
			);
			OEDT_HelperPrintf
			( 
				TR_LEVEL_USER_1, 
				"The data rate for %dK Write is	:	%f MB/S \n",
				u32NumBytes, 
				aflData_Rate_Wr[u32Count]
			);
		}
		//calculating the average write and read data rates
		flAvgWrite_DataRate = 	flAvgWrite_DataRate / NO_IOP;
		flAvgRead_DataRate = 	flAvgRead_DataRate / NO_IOP;
		OEDT_HelperPrintf
		( 
			TR_LEVEL_USER_1, 
			"The Average Data rate for Read Operation is		:%f MB/s \t",
			flAvgRead_DataRate 
		);
		OEDT_HelperPrintf
		( 
			TR_LEVEL_USER_1, 
			"The Average Data rate for Write Operation is	:%f MB/s \n",
			flAvgWrite_DataRate 
		);
	}

	return s32Retval;
}
	
/*****************************************************************************
* FUNCTION:		u32CardMultCreate()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:   
* DESCRIPTION:  Remove without create
* HISTORY:		Taken form Paramount platform 
******************************************************************************/
tS32 u32CardRemoveWOCreate(void)
{
	tS32 s32Retval = 0;
	OSAL_tIODescriptor devicehandle;
	
	devicehandle = OSAL_IOCreate(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		
		if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD) != OSAL_OK)
		{
			s32Retval = 2;
		}
		else
		{
			// once again try to remove the device
			if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD) != OSAL_ERROR)
			{
				// can remove once again
				s32Retval = 3;
			}
			
		}
		
	}
	else
	{
		s32Retval = 1;
	}
	
	return s32Retval;
}
	
/*****************************************************************************
* FUNCTION:		u32CardIOCtrl()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION: card specific IO control function 
* HISTORY:		Taken form Paramount platform 
         10/12/08-Ravindran P - test case activated  
******************************************************************************/

tU32 u32CardIOCtrl(void)
{
	tU32 u32Retval = 0;
	OSAL_tIODescriptor devicehandle;
	OSAL_trIOCtrlCardState rIOCtrlCard;
	
	
	devicehandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CARD, OSAL_EN_READWRITE);
	if(devicehandle != OSAL_ERROR)
	{
		
		if(OSAL_s32IOControl(devicehandle,OSAL_C_S32_IOCTRL_CARD_STATE,(tS32)&rIOCtrlCard) == OSAL_OK)
		{
		#ifndef ADIT_PLATFORM	
			if(OSAL_s32IOControl(devicehandle,OSAL_C_S32_IOCTRL_CARD_GETMEDIAINFO,(tS32)&rMediaInfo) != OSAL_OK)
			{
				// error in getting the media info
				u32Retval = 6;
			}
		#endif
					
		}
		else
		{
			// error in getting the card state
			u32Retval = 5;
		}
		
		
		//close the device
		if(OSAL_s32IOClose(devicehandle) != OSAL_OK)
		{
			u32Retval = 2;
		}
		
	}
	else
	{
		u32Retval = 1;
	}
				
	return u32Retval;
}
		
/*****************************************************************************
*
* FUNCTION:	u32CardCheckdisk     
*
* DESCRIPTION:  This function is to the the checkdisk feature
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
*
* TEST CASE:    
*  
History:Created by Ravindran P (RBEI/ECM1)

*****************************************************************************/
tU32 u32CardCheckdisk(void)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tU32 u32Ret = 0;

        
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CARD, enAccess );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if ( OSAL_s32IOControl ( hDevice ,OSAL_C_S32_IOCTRL_FIOCHKDSK,
									  (tS32)NULL) == OSAL_ERROR )
		{
			u32Ret += 2;
		}

		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;

}

/************************************************************************
* FUNCTION                 :  vPerfAsyncReadCallback 
* PARAMETER               :  prArgs -> Structure to contain the IDs of different Callback Async Reads
* RETURNVALUE          :  "void"
* DESCRIPTION          :  call back function for Async read operation in Performance Analysis
* HISTORY                   : Created Jeryn Mathew(RBIN/ECF1) on 19 Feb, 2009
 ************************************************************************/
tVoid vPerfAsyncReadCallback ( CBARGS *prArgs )
{
   OSAL_tEventMask tMask;
   /*Start Timer - jev1kor*/
   prArgs->cbStopTime_rd  = OSAL_ClockGetElapsedTime();
   prArgs->cbTotalTime_rd = prArgs->cbStopTime_rd - prArgs->cbStartTime_rd;
   
   switch (prArgs->u8Id)
   {
   case 0  : tMask = 1;   break;
   case 1  : tMask = 2;   break;
   case 2  : tMask = 4;   break;
   case 3  : tMask = 8;   break;
   case 4  : tMask = 16;  break;
   case 5  : tMask = 32;  break;
   case 6  : tMask = 64;  break;
   case 7  : tMask = 128; break;
   case 8  : tMask = 256; break;
   case 9  : tMask = 512; break;
   default : tMask = 0;   break;
   }
   
   OSAL_s32EventPost (hAsyncReadEvent, tMask, OSAL_EN_EVENTMASK_OR);
   OEDT_HelperPrintf
   ( 
      TR_LEVEL_USER_4, 
      "Read Event Posted : %d",
      prArgs->u8Id
   );
} 

/************************************************************************
* FUNCTION                 :  vPerfAsyncWriteCallback 
* PARAMETER               :  prArgs -> Structure to contain the IDs of different Callback Async Reads
* RETURNVALUE          :  "void"
* DESCRIPTION          :  call back function for Async write operation in Performance Analysis
* HISTORY                   : Created Jeryn Mathew(RBIN/ECF1) on 19 Feb, 2009
 ************************************************************************/
tVoid vPerfAsyncWriteCallback ( CBARGS *prArgs )
{
   OSAL_tEventMask tMask;
   /*Start Timer - jev1kor*/
   prArgs->cbStopTime_wr  = OSAL_ClockGetElapsedTime();
   prArgs->cbTotalTime_wr = prArgs->cbStopTime_wr - prArgs->cbStartTime_wr;
   
   switch (prArgs->u8Id)
   {
   case 0  : tMask = 1;   break;
   case 1  : tMask = 2;   break;
   case 2  : tMask = 4;   break;
   case 3  : tMask = 8;   break;
   case 4  : tMask = 16;  break;
   case 5  : tMask = 32;  break;
   case 6  : tMask = 64;  break;
   case 7  : tMask = 128; break;
   case 8  : tMask = 256; break;
   case 9  : tMask = 512; break;
   default : tMask = 0;   break;
   }
   
   OSAL_s32EventPost (hAsyncReadEvent, tMask, OSAL_EN_EVENTMASK_OR);
   OEDT_HelperPrintf
   ( 
      TR_LEVEL_USER_4, 
      "Write Event Posted : %d",
      prArgs->u8Id
   );
} 

/************************************************************************
* FUNCTION             :  u32FreeResources 
* PARAMETER            :  ErrCode -> Error cod
* RETURNVALUE          :  ErrCode -> Error code entered by calling function.
* DESCRIPTION          :  function to release resources in ASYNC READ Performance OEDT
* HISTORY              : Created Jeryn Mathew(RBIN/ECF1) on 19 Feb, 2009
						 Lint removed. Jeryn Mathew (RBEI/ECF1) on 04-05-09
 ************************************************************************/
tU32 u32FreeResources(tS32 s32ErrCode)
{
   tU8 u8SwitchCode = 0;
   //Fall-through style of switch cases   
   switch (s32ErrCode)
   {
      case 700 : /* Event Create Failed. Already exists. */
      case 701 : /* Buffer allocation failed */
                  u8SwitchCode = 1;
                  break;
      case 702 : /* Memset in Write Buffer failed. */
      case 703 : /* File Create error during 1MB write */
                  u8SwitchCode = 2;
                  break;
      case 704 : /* File Write Failed */
                  u8SwitchCode = 3;
                  break;
      case 705 : /* File close error */
      case 706 : /* File open for Async Read failed */
	   case 712 : /* Event Wait error */ 
      case 720 : /* Async read error : index 0 */
      case 721 : /* Async read error : index 1 */
      case 722 : /* Async read error : index 2 */
      case 723 : /* Async read error : index 3 */
      case 724 : /* Async read error : index 4 */
      case 725 : /* Async read error : index 5 */
      case 726 : /* Async read error : index 6 */
      case 727 : /* Async read error : index 7 */
      case 728 : /* Async read error : index 8 */
      case 729 : /* Async read error : index 9 */
      case 750 : /* Read Async return Value doesn't match */
                  u8SwitchCode = 3;
                  break;
      case 707 : /* Event Wait error */
                  u8SwitchCode = 3;
                  break;
      case 708 : /* File Close error after the Async Read*/
      case 709 : /* Comparison of Read and Write Buffers failed */
      case 710 : /* File Remove error */
      case 711 : /* File Remove error */
                  u8SwitchCode = 3;
                  break;
      default  :  break;
   }
      
   switch (u8SwitchCode)
   {
      /*rav8kor - lint supressed as the style of coding is intentional*/
      /*lint -save -e616 -e825*/      
      case 3:  /* Handle File Close */
               OSAL_s32IOClose (Asyncfilehandle);
               OSAL_s32IOClose (Asyncfilewritehandle);
               OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt_async_perf.txt");
               OSAL_s32IORemove(OSAL_C_STRING_DEVICE_CARD"/oedt_async_write_perf.txt");
               /* fallthrough */
      case 2:  if (u8AsyncWriteBuf != OSAL_NULL)/*Handle Memory Clearing*/
               {
                  OSAL_vMemoryFree (u8AsyncWriteBuf);
                  u8AsyncWriteBuf = OSAL_NULL;
               }
               if (u8AsyncReadBuf != OSAL_NULL)
               {
                  OSAL_vMemoryFree (u8AsyncReadBuf);
                  u8AsyncReadBuf = OSAL_NULL;
               }       
               /* fallthrough */
      case 1:  OSAL_s32EventClose  (hAsyncReadEvent); /* Handle Event Closure */
               OSAL_s32EventDelete ("AsyncReadCallback");
               break;
     /*lint -restore */
      default  :  break;
   }
   
   return (tU32)s32ErrCode;
  
}

/************************************************************************
* FUNCTION     :  s32PrepareFiles 
* PARAMETER    :  pu8Buffer -> Buffer containing data to write into file 
* RETURNVALUE  :  ErrCode -> Error code 
* DESCRIPTION  : function to create the file for OEDT
* HISTORY      : Created Jeryn Mathew(RBIN/ECF1) on 19 Feb, 2009
 ************************************************************************/
static tS32 s32PrepareFiles(tPCU8 pu8Buffer)
{
   tS32 s32Retval = 0;
   
   //Create Files
   if ( (Asyncfilehandle = OSAL_IOCreate 
                    (
                      OSAL_C_STRING_DEVICE_CARD"/oedt_async_perf.txt",
                      OSAL_EN_READWRITE
                    ) ) == OSAL_ERROR)
   {
      // file Create error
         s32Retval = 703;
   }
   else if ( OSAL_s32IOWrite 
           ( 
              Asyncfilehandle, 
              (tPCS8)pu8Buffer, 
              (1024*1024)
           ) == OSAL_ERROR)
   {
      //file write failed
         s32Retval = 704;
   }
   else if ( OSAL_s32IOClose (Asyncfilehandle) != OSAL_OK )
   {
      //file remove error
         s32Retval = 705;
   }

   return s32Retval;
}

/*****************************************************************************
*
* FUNCTION:	u32CardPerformanceAsyncRead     
*
* DESCRIPTION:  This function is to conduct Performance analysis of Async
*              Read Operations for SD-Cards.
*              10 Async Read Calls are queued to test the performance analysis.
*
* PARAMETERS:
*  None   
*
* RETURNVALUE:
*	SUCCESS - Success
*	FAIL 	- Error code
*
* TEST CASE:    
*  History : Created by Jeryn Mathew (RBEI/ECF1)
			 Lint removed. Jeryn Mathew (RBEI/ECF1) on 04-05-09
*****************************************************************************/
tU32 u32CardPerformanceAsyncIO(void)
{
   tS32                 s32Retval            = 0;
   tS32                 s32CompareVal        = 0;
   tU8                  u8Loop               = 0;
   tU32                 u32Count             = 0;
   tU32                 u32NumBytes          = 0;
   tFloat               flAvgRead_DataRate   = 0;
   tFloat               flAvgWrite_DataRate   = 0;
   tFloat               aflData_Rate_Rd[NUM_OF_FILESIZES]  = {0};
   tFloat               aflData_Rate_Wr[NUM_OF_FILESIZES]  = {0};
   OSAL_trAsyncControl  rAsyncCtrl[ASYNC_READ_ITERATIONS];
   OSAL_tEventMask      rResultMask;
   OSAL_tIODescriptor   AsyncPerfFile;
   OSAL_tMSecond        tReadDelay           = 0;
   
   s32Retval = OSAL_s32EventCreate ("AsyncReadCallback", &hAsyncReadEvent);
   /*Check if Event is already existent*/
   if(s32Retval == OSAL_ERROR)
   {
      s32Retval = 700;      
   }

   if ((u8AsyncWriteBuf = (tU8*)OSAL_pvMemoryAllocate(1024*1024)) == OSAL_NULL || 
       (u8AsyncReadBuf  = (tU8*)OSAL_pvMemoryAllocate(1024*1024)) == OSAL_NULL)
   {
      //error buffer allocation failed
      s32Retval = 701;
   }
   else
   {
      /* Prepare the 1MB File*/
      if (OSAL_pvMemorySet (u8AsyncWriteBuf, 'a', (1024*1024) ) == OSAL_NULL)
      {
         //error in Memory Set for Write Buffer
         s32Retval = 702;
      }
      else 
      {
         s32Retval = s32PrepareFiles(u8AsyncWriteBuf);
      }
      
      //Conduct a check for RetVal
      if (s32Retval >= 700)
      {
         //Free the resources
         return u32FreeResources(s32Retval);
      }
      
      //Iterate through all filesizes.
      for(u32Count = 0;u32Count < NUM_OF_FILESIZES;u32Count++)
      {
         switch(u32Count)
         {
            case 0:  u32NumBytes = 2048;   //2k
                  tReadDelay  = 50;
               break;
            case 1:  u32NumBytes = 4096;   //4k
               break;
            case 2:  u32NumBytes = 8192;   //8k
               break;
            case 3:  u32NumBytes = 10240;  //10k
               break;
            case 4:  u32NumBytes = 16384;  //16k
               break;
            case 5:  u32NumBytes = 32768;  //32k
               break;
            case 6:  u32NumBytes = 51200;  //50k
               break;
            case 7:  u32NumBytes = 65536;  //64k
               break;
            case 8:  u32NumBytes = 102400; //100k
               break;
            case 9:  u32NumBytes = 131072;  //128k
               break;
            case 10:  u32NumBytes = 262144;  //256k
               break;
            case 11:  u32NumBytes = 307200; //300k
               break;
            case 12:  u32NumBytes = 512000; //500k
                     tReadDelay  = 75;
               break;
            case 13:  u32NumBytes = 524288;  //512k
               break;
            case 14:  u32NumBytes = 1048576; //1M
                     tReadDelay  = 200;
               break;
            case 15:  u32NumBytes = 20977664;  //2M
               break;
            case 16:  u32NumBytes = 31466496;  //3M
               break;
            default: break;
         }

         //Open the file for Async reads
         if ((Asyncfilehandle = OSAL_IOOpen
                                 (
                                   OSAL_C_STRING_DEVICE_CARD"/oedt_async_perf.txt",
                                   OSAL_EN_READONLY
                                 ) ) == OSAL_ERROR)
         {
            // opening the file for read failed
            s32Retval = 706;
            break;
         }
       //Open the file for Async reads
         if ((Asyncfilewritehandle = OSAL_IOCreate
                                     (
                                       OSAL_C_STRING_DEVICE_CARD"/oedt_async_write_perf.txt",
                                       OSAL_EN_READWRITE
                                     ) ) == OSAL_ERROR)
         {
            // opening the file for read failed
            s32Retval = 707;
            break;
         }
                 
         //Print the % completion
         if (u32Count == NUM_OF_FILESIZES-1)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_1, 
               "100 percent Complete!\n"
            );
         }
         else if (u32Count == 12)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_1, 
               "75 percent Completed..."
            );
         }
         else if (u32Count == 8)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_1, 
               "50 percent Completed..."
            );
         }
         else if (u32Count == 3)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_1, 
               "25 percent Completed..."
            );
         }
         // Prepare the Iterations for Async Operations
         for (u8Loop = 0; u8Loop < ASYNC_READ_ITERATIONS; u8Loop++)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_4, 
               "Begin Asyn Structure : %d",
               u8Loop
            );
            //Prepare the Async Structure
            /* Fill up Private structure */
            cbArg[u8Loop].u8Id            = u8Loop;
            //Lint is supressed because the following line is intentional.
            /*lint -save -e789*/
            cbArg[u8Loop].acArg           = (rAsyncCtrl + u8Loop);
            /*lint -restore*/
            cbArg[u8Loop].cbStartTime_rd  = 0;
            cbArg[u8Loop].cbStopTime_rd   = 0;
            cbArg[u8Loop].cbTotalTime_rd  = 0;
            /*Fill the asynchronous control structure*/
            rAsyncCtrl[u8Loop].id         = Asyncfilehandle;
            rAsyncCtrl[u8Loop].s32Offset  = 0;
            rAsyncCtrl[u8Loop].pvBuffer   = u8AsyncReadBuf;
            rAsyncCtrl[u8Loop].u32Length  = u32NumBytes;
            rAsyncCtrl[u8Loop].pCallBack  = (OSAL_tpfCallback)vPerfAsyncReadCallback;
            /*rAsyncCtrl.pvArg            = &rAsyncCtrl;*//*jev1kor*/
            rAsyncCtrl[u8Loop].pvArg      = (cbArg + u8Loop);
            
            //Start Timer
            cbArg[u8Loop].cbStartTime_rd  = OSAL_ClockGetElapsedTime();
            //Begin Read
            if ( OSAL_s32IOReadAsync ( (rAsyncCtrl+u8Loop) ) == OSAL_ERROR )
            {
               s32Retval = 720 + u8Loop;
               OEDT_HelperPrintf
               ( 
                  TR_LEVEL_USER_4, 
                  "Async Read Error!"
               );
               break;
            }
            
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_4, 
               "Async Read No: %d, Filesize : %d",
               u8Loop,
               (u32NumBytes/1024)
            );
            
            //Force a delay before the next read request is given.
            OSAL_s32ThreadWait(tReadDelay);            
         }
         
         //Conduct a check for RetVal
         if (s32Retval >= 700)
         {
            //Free Resources
            return u32FreeResources(s32Retval);
         }
         
         /* Wait Indefinitely for the event*/
         OEDT_HelperPrintf
         ( 
            TR_LEVEL_USER_4, 
            "Waiting for Events"
         );
         
         if ( OSAL_s32EventWait 
               (
                  hAsyncReadEvent,
                  (OSAL_tEventMask)EVENT_ASREAD_ALLDONE,
                  OSAL_EN_EVENTMASK_AND,
                  EVENT_TIMEOUT/*OSAL_C_TIMEOUT_FOREVER*/,
                  &rResultMask
               ) == OSAL_ERROR )
         {
            //Error case
            s32Retval = 712;
            break;
         }
         else
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_4, "Event Wait Complete" );
            OSAL_s32EventPost
            (
               hAsyncReadEvent,
               ~rResultMask /*0*/,
               OSAL_EN_EVENTMASK_AND
            );            
         }//end of else case
         
         OEDT_HelperPrintf
         ( 
            TR_LEVEL_USER_4, 
            "Closing File"
         );

         //Close the file 
         if ( OSAL_s32IOClose (Asyncfilehandle) != OSAL_OK )
         {
            //file close error
            s32Retval = 708;
            //Free Resources
            break;
         }

		 // Prepare the Iterations for Async Operations
         for (u8Loop = 0; u8Loop < ASYNC_READ_ITERATIONS; u8Loop++)
         {
            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_4, 
               "Begin Asyn Structure : %d",
               u8Loop
            );
            //Prepare the Async Structure
            /* Fill up Private structure */
            cbArg[u8Loop].u8Id            = u8Loop;
            //Lint is supressed because the following line is intentional.
            /*lint -save -e789*/
            cbArg[u8Loop].acArg           = (rAsyncCtrl + u8Loop);
            /*lint -restore*/
            cbArg[u8Loop].cbStartTime_wr  = 0;
            cbArg[u8Loop].cbStopTime_wr   = 0;
            cbArg[u8Loop].cbTotalTime_wr  = 0;
            /*Fill the asynchronous control structure*/
            rAsyncCtrl[u8Loop].id         = Asyncfilewritehandle;
            rAsyncCtrl[u8Loop].s32Offset  = 0;
            rAsyncCtrl[u8Loop].pvBuffer   = u8AsyncWriteBuf;
            rAsyncCtrl[u8Loop].u32Length  = u32NumBytes;
            rAsyncCtrl[u8Loop].pCallBack  = (OSAL_tpfCallback)vPerfAsyncWriteCallback;
            /*rAsyncCtrl.pvArg            = &rAsyncCtrl;*//*jev1kor*/
            rAsyncCtrl[u8Loop].pvArg      = (cbArg + u8Loop);
            
            //Start Timer
            cbArg[u8Loop].cbStartTime_wr  = OSAL_ClockGetElapsedTime();
            //Begin Write
            if ( OSAL_s32IOWriteAsync ( (rAsyncCtrl+u8Loop) ) == OSAL_ERROR )
            {
               s32Retval = 721 + u8Loop;
               OEDT_HelperPrintf
               ( 
                  TR_LEVEL_USER_4, 
                  "Async Write Error!"
               );
               break;
            }

            OEDT_HelperPrintf
            ( 
               TR_LEVEL_USER_4, 
               "Async Write No: %d, Filesize : %d",
               u8Loop,
               (u32NumBytes/1024)
            );

            //Force a delay before the next read request is given.
			OSAL_s32ThreadWait(275+tReadDelay);
         }
         
         //Conduct a check for RetVal
         if (s32Retval >= 700)
         {
            //Free Resources
            return u32FreeResources(s32Retval);
         }
         
         /* Wait Indefinitely for the event*/
         OEDT_HelperPrintf
         ( 
            TR_LEVEL_USER_4, 
            "Waiting for Events"
         );
         
         if ( OSAL_s32EventWait 
               (
                  hAsyncReadEvent,
                  (OSAL_tEventMask)EVENT_ASREAD_ALLDONE,
                  OSAL_EN_EVENTMASK_AND,
                  EVENT_TIMEOUT/*OSAL_C_TIMEOUT_FOREVER*/,
                  &rResultMask
               ) == OSAL_ERROR )
         {
            //Error case
            s32Retval = 712;
            break;
         }
         else
         {
            OEDT_HelperPrintf( TR_LEVEL_USER_4, "Event Wait Complete" );
            OSAL_s32EventPost
            (
               hAsyncReadEvent,
               ~rResultMask /*0*/,
               OSAL_EN_EVENTMASK_AND
            );            
         }//end of else case
		 if ( OSAL_s32IOClose (Asyncfilewritehandle) != OSAL_OK )
         {
            //file close error
            s32Retval = 709;
            //Free Resources
            break;
         }
         
         // compare the written and read bytes
         if ((s32CompareVal = OSAL_s32MemoryCompare 
                              (
                                 u8AsyncWriteBuf, 
                                 u8AsyncReadBuf, 
                                 u32NumBytes
                              ) ) != 0)
         {
            OEDT_HelperPrintf 
            ( 
               TR_LEVEL_USER_4, 
               "s32CompareVal = %d", 
               s32CompareVal 
            );
            s32Retval = 709;
            break;
         }
         //Obtaining Average Data rate of one filesize read
         for (u8Loop = 0; u8Loop<ASYNC_READ_ITERATIONS; u8Loop++)
         {
            aflData_Rate_Rd[u32Count] += (tFloat)(u32NumBytes*(tFloat)1000)/(tFloat)(cbArg[u8Loop].cbTotalTime_rd*(tFloat)(1024*1024));
            aflData_Rate_Wr[u32Count] += (tFloat)(u32NumBytes*(tFloat)1000)/(tFloat)(cbArg[u8Loop].cbTotalTime_wr*(tFloat)(1024*1024));
         }
         aflData_Rate_Rd[u32Count] = aflData_Rate_Rd[u32Count]/ASYNC_READ_ITERATIONS;
         aflData_Rate_Wr[u32Count] = aflData_Rate_Wr[u32Count]/ASYNC_READ_ITERATIONS;
      }//end of main for-loop
      
      if ( OSAL_s32IORemove 
            (
               OSAL_C_STRING_DEVICE_CARD"/oedt_async_perf.txt"
            ) != OSAL_OK)
      {
         //file remove error
         s32Retval = 710;
      }
      if ( OSAL_s32IORemove 
            (
               OSAL_C_STRING_DEVICE_CARD"/oedt_async_write_perf.txt"
            ) != OSAL_OK)
      {
         //file remove error
         s32Retval = 711;
      }
   }
   
   //Conduct a check for RetVal
   if (s32Retval >= 700)
   {
      return u32FreeResources(s32Retval);
   }
   
   //Print the Final Speeds
   if(s32Retval == 0)
   {
      
      tBool bCreateFile = TRUE;
      //Clear Resources that is not required
      //Memory and Event must be removed
      (tVoid)u32FreeResources (702);
      
      //For Logging results in CSV form. It will be stored in SD-Card
      if ( (AsyncPerfFile = OSAL_IOCreate 
                              (
                                 OSAL_C_STRING_DEVICE_CARD"/asyncperflog.csv",
                                 OSAL_EN_READWRITE
                              ) ) == OSAL_ERROR)
      {
         // file Create error
         s32Retval = 703;
         bCreateFile = FALSE;
      }
      if (bCreateFile)
      {
         OSALUTIL_s32FPrintf (AsyncPerfFile, "S.No.,DataSize,ReadTime,ReadDataRate,WriteTime,WriteDataRate\n");
      }     

      OEDT_HelperPrintf
      ( 
         TR_LEVEL_USER_1,
         "The Data Rate for Read and Write operations are :\n" 
      );
      
      for(u32Count = 0;u32Count < NUM_OF_FILESIZES;u32Count++)
      {
         switch(u32Count)
         {
            case 0:  u32NumBytes = 2048;   //2k
               break;
            case 1:  u32NumBytes = 4096;   //4k
               break;
            case 2:  u32NumBytes = 8192;   //8k
               break;
            case 3:  u32NumBytes = 10240;  //10k
               break;
            case 4:  u32NumBytes = 16384;  //16k
               break;
            case 5:  u32NumBytes = 32768;  //32k
               break;
            case 6:  u32NumBytes = 51200;  //50k
               break;
            case 7:  u32NumBytes = 65536;  //64k
               break;
            case 8:  u32NumBytes = 102400; //100k
               break;
            case 9:  u32NumBytes = 131072;  //128k
               break;
            case 10:  u32NumBytes = 262144;  //256k
               break;
            case 11:  u32NumBytes = 307200; //300k
               break;
            case 12:  u32NumBytes = 512000; //500k
               break;
            case 13:  u32NumBytes = 524288;  //512k
               break;
            case 14:  u32NumBytes = 1048576; //1M
               break;
            default: break;
         }
         
         flAvgRead_DataRate = flAvgRead_DataRate + aflData_Rate_Rd[u32Count];
         flAvgWrite_DataRate = flAvgWrite_DataRate + aflData_Rate_Wr[u32Count];
         OEDT_HelperPrintf
         ( 
            TR_LEVEL_USER_1, 
            "The data rate for %dK Read is	:	%f MB/S \t",
            (u32NumBytes/1024), 
            aflData_Rate_Rd[u32Count]
         );
         OEDT_HelperPrintf
         ( 
            TR_LEVEL_USER_1, 
            "The data rate for %dK Write is	:	%f MB/S \n",
            (u32NumBytes/1024), 
            aflData_Rate_Wr[u32Count]
         );
         
         //For Logging results in CSV form. It will be stored in SD-Card
         if (bCreateFile)
         {
            OSALUTIL_s32FPrintf
            (
               AsyncPerfFile, "%d,%d,%f,%f,%f,%f\n",
               (u32Count+1),
               (u32NumBytes/1024),
               (tFloat)((tFloat)(u32NumBytes*(tFloat)1000)/(tFloat)(aflData_Rate_Rd[u32Count]*(tFloat)(1024*1024))),
               aflData_Rate_Rd[u32Count],
               (tFloat)((tFloat)(u32NumBytes*(tFloat)1000)/(tFloat)(aflData_Rate_Wr[u32Count]*(tFloat)(1024*1024))),
               aflData_Rate_Wr[u32Count]
            );
         }
      }
      //calculating the average write and read data rates
      flAvgRead_DataRate  = flAvgRead_DataRate / NUM_OF_FILESIZES;
      flAvgWrite_DataRate = flAvgWrite_DataRate / NUM_OF_FILESIZES;
      OEDT_HelperPrintf
      ( 
         TR_LEVEL_USER_1, 
         "The Average Data rate for Read Operation is  : %f MB/s \t",
         flAvgRead_DataRate 
      );
      OEDT_HelperPrintf
      ( 
         TR_LEVEL_USER_1, 
         "The Average Data rate for Write Operation is : %f MB/s \n",
         flAvgWrite_DataRate 
      );
     
      if (bCreateFile)
      {
         OSALUTIL_s32FPrintf
         (
            AsyncPerfFile, "Average,,,%f,,%f\n",
            flAvgRead_DataRate,
            flAvgWrite_DataRate
         );
         
         //For Logging results in CSV form. It will be stored in SD-Card
         OSAL_s32IOClose(AsyncPerfFile);
      }
   }
   
   return (tU32)s32Retval;
}

