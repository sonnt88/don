/******************************************************************************
 *FILE         : oedt_Odo_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the  test cases for the odometer 
 *               device.Ref:SW Test specification dev_odo
 *               
 *AUTHOR       : Ravindran P(CM-DI/EAP)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 07.06.06 - Initial version

                 04.09.06 - Ravindran P(CM-DI/EAP)- changes as per dev_odo GM GE
                            vTtfisTrace-calls removed.

                 31.10.07 - Ravindran P(RBIN/EDI3)OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE	and 
                            OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE IOControls removed except from 
                            interface checkSeq test function
                            tu32OdoSetTimeoutAndReadSeq renamed to tu32OdoReadAndFlushSeq
                            Block read added to test case  tu32OdoBasicReadSeq and

                 31.10.07 - Ravindran P(RBIN/EDI3)-Function renamed to tu32OdoReadAndFlushSeq

                 02.11.07 - Ravindran P(RBIN/EDI3)-tu32OdoCheckReadLargeNoOfRecords
                            Test Function added

                 05.11.07 - Ravindran P(RBIN/EDI3)tu32OdoCheckReadMoreThanBufferLength
                            Test Function added

                 24.11.14 - Updated the oedts to test Gen3 environment

                 11.12.15 - (RBEI/ECF5) - Shivasharnappa Mothpalli 
                            Removed un used IOCTRL calls (Fix for CFG3-1622).
                            OSAL_C_S32_IOCTRL_ODOMETER_RESET,
                            OSAL_C_S32_IOCTRL_ODOMETER_INIT,
                            OSAL_C_S32_IOCTRL_ODOMETER_FLUSH,
                            OSAL_C_S32_IOCTRL_ODOMETER_GETDIRECTION,
                            OSAL_C_S32_IOCTRL_ODOMETER_GET_WHEELCOUNTER,
                            OSAL_C_S32_IOCTRL_ODOMETER_SETDIRECTION,
                            OSAL_C_S32_IOCTRL_ODOMETER_ACTIVATE_OIC,
                            OSAL_C_S32_IOCTRL_ODOMETER_INACTIVATE_OIC,
                            OSAL_C_S32_IOCTRL_ODOMETER_INIT_OVC,
                            OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE,
                            OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE,
                            OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION,
 *
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"


/* Macros specific for Stub test */
/* Uncomment definition below until #endif during stub tests */

//#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE_ODO
//#define SENSOR_PROXY_TEST_STUB_ACTIVE_ODO
//#endif


//#define CHECK_Odo
//#define UNDERSTAND
/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define BLOCK_READ_COUNT                                30
#define READ_COUNT_MORE_THAN_BUFFER_LENGTH              520
#define WAIT_IN_MS_FOR_NEXT_READ                        100
#define MAX_LOOP_CNT_FOR_GETCNT                         100
#define NO_RECORDS_ADD_ILLEGAL                          200


/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#define  ODO_DEVICE_OPEN_ERROR               35
#define  ODO_IOREAD_ERROR                    36 
#define  ODO_GET_VERSION_ERROR               37
#define  ODO_GETCNT_ERROR                    38
#define  ODO_SETTIMEOUT_VALUE_ERROR          39
#define  ODO_GETTIMEOUT_VALUE_ERROR          40
#define  ODO_GET_RESOUTION_ERROR             41
#define  ODO_CYCLE_TIME_VALUE_MISMATCH_ERROR            42
#define  ODO_CLOSE_ERROR                     43
#define  ODO_NOT_SUPPORTED_ERROR             44
#define  ODO_BLOCK_READ_ERROR                45
#define  ODO_UNKNOWN_ERROR                   46
#define  ODO_INVALID_RECORD                  47
#define  ODO_REOPEN_ERROR                    48
#define  ODO_RECLOSE_ERROR                   49
#define  ODO_READNULL_ERROR                  50

#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE_ODO     //to solve lint
#define  ODO_STUB_READ_VALUE_MISMATCH        51
#endif


#define  ODO_NO_OF_ENTRIES              10





#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE_ODO

#define  ODO_DIRE_READ_NO_OF_ENTRIES    20

/*SCC sends the following direction values to IMX
  
  Direction from SCC
  00  -- DATA INVALID
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- UNKNOWN direction

  Handling of direction is done in the following way in IMX

  Direction data in IMX
  00  -- UNKNOWN direction
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- DATA INVALID
*/
  
#define  OEDT_STUB_ODO_DIRE_MASK        (0x03)

#define  OEDT_STUB_ODO_DIRE_UNKW        0
#define  OEDT_STUB_ODO_DIRE_FORW        1
#define  OEDT_STUB_ODO_DIRE_REVE        2

#endif

tU8 aOdoErr_oedt[] = {
                        ODO_DEVICE_OPEN_ERROR,       /*0*/
                        ODO_IOREAD_ERROR,            /*1*/
                        ODO_GET_VERSION_ERROR,       /*2*/
                        ODO_GETCNT_ERROR,            /*3*/
                        ODO_SETTIMEOUT_VALUE_ERROR,  /*4*/
                        ODO_GETTIMEOUT_VALUE_ERROR,  /*5*/
                        ODO_GET_RESOUTION_ERROR,     /*6*/
                        ODO_CYCLE_TIME_VALUE_MISMATCH_ERROR,    /*7*/
                        ODO_CLOSE_ERROR,             /*8*/
                        ODO_NOT_SUPPORTED_ERROR,     /*9*/  
                        ODO_BLOCK_READ_ERROR,        /*10*/
                        ODO_UNKNOWN_ERROR,           /*11*/
                        ODO_INVALID_RECORD,          /*12*/
                        ODO_REOPEN_ERROR,            /*13*/
                        ODO_RECLOSE_ERROR,           /*14*/
                        ODO_READNULL_ERROR,          /*15*/
                    };


#define OEDT_ODO_TEST_PASS      0
#define NO_OF_REC_FOR_RETRY 3
#define  ODO_DEFAULT_TIME_INTERVAL_FOR_JAC     50000000
#define  ODO_DEFAULT_TIME_INTERVAL_FOR_SUZUKI  70000000
#define  ODO_DEFAULT_TIME_INTERVAL_FOR_GM      100000000
#define ODO_IOCTRL_GET_CYCLE_TIME_ERROR 51






/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/



/*****************************************************************
| function declaration (scope: module-local)
|----------------------------------------------------------------*/


/*tVoid vOdoTimerCallBack_oedt(tPVoid pDummyArg);
OSAL_tTimerHandle hOdoStartTimer_oedt();
void OdoStopTimer_oedt(OSAL_tTimerHandle phTimer);*/


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
* FUNCTION    : u32OdoGetCycleTime()
* DESCRIPTION : Reads the cycle time from Odo .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 24.11.15  Shivasharnappa Mothpalli(RBEI/ECF5)
******************************************************************/
tU32 u32OdoGetCycleTime(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = OEDT_ODO_TEST_PASS;
   tU32 OdoGetCyleTime = 0;

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ODOMETER,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = ODO_DEVICE_OPEN_ERROR;
   }
   else
   {
      if (  OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME,(tS32)&OdoGetCyleTime) == OSAL_ERROR) 
      {
         u32RetVal = ODO_IOCTRL_GET_CYCLE_TIME_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Odometer get cycle time failed error no  : %d",OSAL_u32ErrorCode() );
      }
      //PQM_authorized_multi_550
      else if ( ( OdoGetCyleTime != ODO_DEFAULT_TIME_INTERVAL_FOR_JAC ) &&
                 ( OdoGetCyleTime != ODO_DEFAULT_TIME_INTERVAL_FOR_SUZUKI ) && ( OdoGetCyleTime != ODO_DEFAULT_TIME_INTERVAL_FOR_GM ) )//lint !e774
      {
         u32RetVal = ODO_CYCLE_TIME_VALUE_MISMATCH_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Mismatch in Odometer cycle time , OdoGetCyleTime: %d",OdoGetCyleTime );
      }
      else 
      {
         u32RetVal = OEDT_ODO_TEST_PASS;
      }
   }

   if ( (hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = ODO_CLOSE_ERROR;
   }

   return u32RetVal;
}


/************************************************************************
 *FUNCTION:    u32OdoOpenDev 
 *DESCRIPTION: 1.Opens the device.
               2.Close the device.  
 
 *PARAMETER:   Nil
 *             
 *RETURNVALUE: Open and Close ODO Device
 *HISTORY:     05.09.11  Martin Langer (CM-AI/CF-33)
 *
 * ************************************************************************/
tU32 u32OdoOpenDev(void)
{
    OSAL_tIODescriptor hDevice;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tBool bErrorOccured = FALSE;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    }

    OSAL_s32ThreadWait(10);
    if (FALSE == bErrorOccured)
    {
        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;
        }
    }
    if( FALSE == bErrorOccured )
    {
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");	
    }
    else
    {
        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
        }
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST FAILED err No: %d",u8ErrorNo);
    }

    return ( u32RetVal );

} // End of u32OdoOpenDev()


/************************************************************************
 *FUNCTION:    tu32OdoBasicReadSeq 
 *DESCRIPTION:  1.Opens the device.
                2.Gets the version
                3.Reads 30 data records from the device.
                4.Close the device.
 
 *PARAMETER:    Nil
 *             
 *RETURNVALUE:  Error Number or NULL to indicate that test is passed
 *HISTORY:      07.06.06  RBIN(CM-DI/EAP)- Ravindran P -Initial Revision.
                31.10.07 -Ravindran P(RBIN/EDI3)
                -Block read functionality added
 *
 * ************************************************************************/
tU32 tu32OdoBasicReadSeq(void)
{
    OSAL_tIODescriptor hDevice ;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tU32 u32Version = 0;
    tU32 count=0;
    OSAL_trIOCtrlOdometerData rOdoData;
    OSAL_trIOCtrlOdometerData arOdoDataBlockRead[BLOCK_READ_COUNT]={0};
    tS32 NoOfRecsForRead = 30; 
    tU8 u8Index;
//  tU8 u8VersionHi,u8VersionLo;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;


    (tVoid)u32Version;  // To fix lint warning

   #ifdef CHECK_THREAD_ODO
        OSAL_tThreadID ThreadID;
        ThreadID = OSAL_ThreadWhoAmI();
        vTtfisTrace(OSAL_C_TRACELEVEL1,
        "Thread No \t: %d",(tS32)ThreadID);
   #endif

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
        
    }

    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            OSAL_s32IOClose ( hDevice );
        }
//      else
//      {
//          u8VersionHi = (tU8)((tU32)((u32Version & 0x0000FF00)>> 8));	 /*Bit handling for getting the version no.*/
//          u8VersionLo = (tU8)(u32Version& 0x000000FF);
//      }
    }
    /*Sequential read*/
    if(FALSE == bErrorOccured )
    {
        for(u8Index = 0;u8Index < NoOfRecsForRead; u8Index++)
        {
            if(OSAL_s32IORead (hDevice,(tS8 *)&rOdoData,sizeof(rOdoData)) == OSAL_ERROR)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 1;
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else if(rOdoData.u16ErrorCounter)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 12;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                "OSAL_s32IORead passed but the record is invalid:");
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else
            {
                OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                " Direction %u Err cnt %u TIME stamp %u Wheel Cnt %u Status %u",
                rOdoData.enDirection,
                rOdoData.u16ErrorCounter,
                rOdoData.u32TimeStamp,
                rOdoData.u32WheelCounter,
                rOdoData.enOdometerStatus);
            }

        }
    }
    /*Block read*/
    if(FALSE == bErrorOccured )
    {
        NoOfRecsForRead=OSAL_s32IORead (hDevice,(tS8*)arOdoDataBlockRead,
        sizeof(arOdoDataBlockRead));
        if(NoOfRecsForRead == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 10;
            OSAL_s32IOClose ( hDevice );
        }
        else
        {
            count = ((tU32)NoOfRecsForRead)/(sizeof(OSAL_trIOCtrlOdometerData));
            for(u8Index = 0;u8Index < count; u8Index++)
            {
                if(arOdoDataBlockRead[u8Index].u16ErrorCounter)
                {
                    bErrorOccured = TRUE;
                    u8ErrorNo = 12; 
                    OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                    "OSAL_s32IORead passed but the record is invalid");
                    OSAL_s32IOClose ( hDevice );
                    break;
                }
                else
                {
                    OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                    " Direction %u Err cnt %u TIME stamp %u Wheel Cnt %u Status %u",
                    arOdoDataBlockRead[u8Index].enDirection,
                    arOdoDataBlockRead[u8Index].u16ErrorCounter,
                    arOdoDataBlockRead[u8Index].u32TimeStamp,
                    arOdoDataBlockRead[u8Index].u32WheelCounter,
                    arOdoDataBlockRead[u8Index].enOdometerStatus);

                }
            }

        }
    }

    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }

    }
 
    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {
        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST FAILED Err No: %d",u8ErrorNo);
        }

    }

    return ( u32RetVal );


} // End of tu32OdoBasicReadSeq()


/************************************************************************
 *FUNCTION     : tu32OdoInterfaceCheckSeq 
 
 *DESCRIPTION  : 1.Opens the device and get the version.
                 2.Gets the count of No. of records in the buffer
                 3.Sets the Time out value(waiting time while the buffer is read when it is empty) 
                 4.Get the time out value
                 5.Gets the resolution and cycle time of the Odo
                 6.Close the device.
 
 *PARAMETER    : Nil
 
 *RETURNVALUE  : Error Number or NULL to indicate that test is passed
 
 *HISTORY      : 07.06.06  RBIN(CM-DI/EAP)- Ravindran P -initial version
                 05.09.06  RBIN(CM-DI/EAP)- Ravindran P -updated for not supported IO control interfaces
                 11.12.15  (RBEI/ECF5) - Shivasharnappa Mothpalli 
                           Removed un used IOCTRL calls (Fix for CFG3-1622).
                           OSAL_C_S32_IOCTRL_ODOMETER_RESET,
                           OSAL_C_S32_IOCTRL_ODOMETER_INIT,
                           OSAL_C_S32_IOCTRL_ODOMETER_FLUSH,
                           OSAL_C_S32_IOCTRL_ODOMETER_GETDIRECTION,
                           OSAL_C_S32_IOCTRL_ODOMETER_GET_WHEELCOUNTER,
                           OSAL_C_S32_IOCTRL_ODOMETER_SETDIRECTION,
                           OSAL_C_S32_IOCTRL_ODOMETER_ACTIVATE_OIC,
                           OSAL_C_S32_IOCTRL_ODOMETER_INACTIVATE_OIC,
                           OSAL_C_S32_IOCTRL_ODOMETER_INIT_OVC,
                           OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE,
                           OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE,
                           OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION,

  ************************************************************************/

tU32 tu32OdoInterfaceCheckSeq(void)
{
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tU32 u32Version = 0;
    tU8 u8VersionHi,u8VersionLo;
    tS32 s32GetCnt = 0;
    //tS32 s32GetDirection = OSAL_EN_RFS_UNKNOWN;
    //tS32 s32SetDirection = OSAL_EN_RFS_FORWARD;
    //tS32 s32WheelCounter = 0;
    tS32 s32GetCycleTime =0;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;


    (tVoid)s32GetCycleTime;  // To fix lint warning
    (tVoid)s32GetCnt;  //To fix lint warning
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
        
    }

    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            OSAL_s32IOClose ( hDevice );
        }
        else
        {
            u8VersionHi = (tU8)((tU32)((u32Version & 0x0000FF00)>> 8));  /*lint !e845 PQM_authorized_multi_534*/  /*Bit handling for getting the version no.*/
            u8VersionLo = (tU8)(u32Version& 0x000000FF);  /*lint !e845 PQM_authorized_multi_534*/

            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"u8VersionHi= %c,u8VersionLo=%c ",
            u8VersionHi,u8VersionLo);
        }
    }


    if( FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
        (tS32)&s32GetCnt) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
            OSAL_s32IOClose ( hDevice );

        }
    }


   if( FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME,
        (tS32)&s32GetCycleTime) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 7;
            OSAL_s32IOClose ( hDevice );
        }

    }

    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }

    }
   
    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {
        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            " TEST FAILED Err No: %d",u8ErrorNo);
        }
    }

    return ( u32RetVal );
}

/************************************************************************
 *FUNCTION:    tu32OdoReadAndFlushSeq
 *DESCRIPTION:  1.Opens the device and get the version.
                2.Set the Time out to a invalid value
                3.Get the record count in the buffer
                4.Read all the records in the buffer
                5.Get the record count in the buffer
                6.Flush the buffer
                7.wait for sufficient records in the buffer  for block read
                8.Do a block read and flush the device
                9.check if the buffer gets filled with records again
                10.close the device


 *PARAMETER:     Nil
 *
 *RETURNVALUE:  Error Number or NULL to indicate that test is passed
 *HISTORY:      08.06.06  RBIN(CM-DI/EAP)- Ravindran P.-initial version
                05.09.06  RBIN(CM-DI/EAP)- Ravindran P -updated for not supported IO control interfaces
                31.10.07 -Ravindran P(RBIN/EDI3)OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE   and 
                OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE IOControls removed 
                - Block read followed by flush implemented
 *
 *
 ************************************************************************/
tU32 tu32OdoReadAndFlushSeq(void)
{
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tU32 u32Version = 0;
    tU32 NoofRec=0;
    tS32 bytesread=0;
    tU32 count=0;
    OSAL_trIOCtrlOdometerData rOdoData;
    OSAL_trIOCtrlOdometerData arOdoDataBlockRead[BLOCK_READ_COUNT]={0};
    tS32 s32Index;
//  tU8 u8VersionHi,u8VersionLo;
    tS32 s32GetCnt = 0;
    tU8 u8LoopCnt = 0;

    tU32 u32RetVal = OEDT_ODO_TEST_PASS;

    (tVoid)u32Version;  // To fix lint warning
    (tVoid)s32GetCnt;  // To fix lint warning

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;

    }

    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            OSAL_s32IOClose ( hDevice );
        }
//      else
//      {
//          u8VersionHi = (tU8)((tU32)((u32Version & 0x0000FF00)>> 8)); /*Bit handling for getting the version no.*/
//          u8VersionLo = (tU8)(u32Version& 0x000000FF);
//      }
    }


    if( FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
        (tS32)&s32GetCnt) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
            OSAL_s32IOClose ( hDevice );

        }

    }
    /*Sequential read*/
    if( FALSE == bErrorOccured )
    {
        for(s32Index = 0;s32Index < s32GetCnt; s32Index++) /*lint !e681 PQM_authorized_533*/
        {
            if(OSAL_s32IORead (hDevice,(tS8 *)&rOdoData,sizeof(rOdoData)) == OSAL_ERROR)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 1;
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else if(rOdoData.u16ErrorCounter)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 12;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                "OSAL_s32IORead passed but the record is invalid:");
                OSAL_s32IOClose ( hDevice );
                break;
            }

        }

    }
    /*check if the buffer has records */
    if( FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
        (tS32)&s32GetCnt) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
            OSAL_s32IOClose ( hDevice );

        }

    }

    /*wait for sufficient no of records for block read*/
    if( FALSE == bErrorOccured )
    {
        s32GetCnt = 0;
        while(s32GetCnt < BLOCK_READ_COUNT ) 
        {
            OSAL_s32ThreadWait(WAIT_IN_MS_FOR_NEXT_READ);

            if( FALSE == bErrorOccured )
            {
                if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
                (tS32)&s32GetCnt) == OSAL_ERROR )
                {
                    bErrorOccured = TRUE;
                    u8ErrorNo = 3;
                    OSAL_s32IOClose ( hDevice );
                }

            }
            u8LoopCnt++;
            if(u8LoopCnt > MAX_LOOP_CNT_FOR_GETCNT)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 11;
                OSAL_s32IOClose ( hDevice );
                break;
            }
        }
    }
    /*perform a block read*/
    if(FALSE == bErrorOccured )
    {
        bytesread = OSAL_s32IORead (hDevice,(tS8*)arOdoDataBlockRead,
        sizeof(arOdoDataBlockRead));
        if( bytesread == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 10;
            OSAL_s32IOClose ( hDevice );
        }
        else
        {
            NoofRec = ((tU32)bytesread)/(sizeof(OSAL_trIOCtrlOdometerData));
            for(count=0 ; count < NoofRec ; count++ )
            {
                if(arOdoDataBlockRead[count].u16ErrorCounter)
                {
                    bErrorOccured = TRUE;
                    u8ErrorNo = 12;
                    OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                    "OSAL_s32IORead passed but the record is invalid");
                    OSAL_s32IOClose ( hDevice );
                    break;
                }
            }
        }
    }
    if(FALSE == bErrorOccured)
    {
        /*wait for atleast 1 record */
        s32GetCnt = 0;
        while(s32GetCnt < 1 ) 
        {
            OSAL_s32ThreadWait(WAIT_IN_MS_FOR_NEXT_READ);

            if( FALSE == bErrorOccured )
            {
                if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
                (tS32)&s32GetCnt) == OSAL_ERROR )
                {
                    bErrorOccured = TRUE;
                    u8ErrorNo = 3;
                    OSAL_s32IOClose ( hDevice );
                }
            }
            u8LoopCnt++;
            if(u8LoopCnt > MAX_LOOP_CNT_FOR_GETCNT)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 11;
                OSAL_s32IOClose ( hDevice );
                break;
            }
        }
    }
    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }

    }

    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {

        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            " TEST FAILED Err No: %d",u8ErrorNo);
        }

    }

    return ( u32RetVal );

}
/************************************************************************
 *FUNCTION:    tu32OdoCheckReadLargeNoOfRecords
 *DESCRIPTION:  1.Opens the device and get the version.
                2.Flush the device
                3.Get the record count in the buffer
                4.Block Read > records count(from previous step)in the buffer
                5.close the device


 *PARAMETER:     Nil
 *
 *RETURNVALUE:  tU32(0 - pass,Error code- failure)
 *HISTORY:      2.11.07 -Ravindran P(RBIN/EDI3)
                -Initial version
 *
 *
 ************************************************************************/

tU32 tu32OdoCheckReadLargeNoOfRecords(void)
{
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tU8* pau8OdoReadBuffer;
    tU32 u32Index;
    tU32 NoOfRecsForRead ;
    tU32 count=0;
    tS32 s32bytesread;
    tS32 s32GetCnt = 0;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;
    OSAL_trIOCtrlOdometerData  *rData;



    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    }

    if( FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ODOMETER_GETCNT,
        (tS32)&s32GetCnt) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
            OSAL_s32IOClose ( hDevice );
        }
    }

    /*Read records > No of records in OSAL_C_S32_IOCTRL_ODOMETER_GETCNT to check 
    write/read consistency*/

    NoOfRecsForRead = ((tU32)s32GetCnt)+NO_RECORDS_ADD_ILLEGAL;

    pau8OdoReadBuffer =(tU8 *)OSAL_pvMemoryAllocate(NoOfRecsForRead*sizeof(OSAL_trIOCtrlOdometerData));
    if(NULL == pau8OdoReadBuffer)
    {
        OSAL_s32IOClose ( hDevice );
        return(ODO_UNKNOWN_ERROR);
    }
    OSAL_pvMemorySet((tPVoid)pau8OdoReadBuffer,0x00,
    (NoOfRecsForRead*sizeof(OSAL_trIOCtrlOdometerData)) );

    /*perform a block read*/
    if(FALSE == bErrorOccured )
    {
        s32bytesread =OSAL_s32IORead (hDevice,(tS8*)pau8OdoReadBuffer,
        (NoOfRecsForRead*sizeof(OSAL_trIOCtrlOdometerData)));
        if(s32bytesread == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 10;
            OSAL_s32IOClose ( hDevice );
        }
        else
        {
            rData = (OSAL_trIOCtrlOdometerData*)pau8OdoReadBuffer;
            count =  ((tU32)s32bytesread)/(sizeof(OSAL_trIOCtrlOdometerData));

            for(u32Index=0;u32Index<count;u32Index++)
            {
                if(rData[u32Index].u16ErrorCounter)
                {
                    bErrorOccured = TRUE;
                    u8ErrorNo = 12;
                    OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                    "OSAL_s32IORead passed but the record is invalid");
                    OSAL_s32IOClose ( hDevice );
                    break;
                }
            }
        }
    }
    OSAL_vMemoryFree((tPVoid)pau8OdoReadBuffer);

    if(FALSE == bErrorOccured )
    { 
        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }
    }

    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {

        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            " TEST FAILED Err No: %d",u8ErrorNo);

        }

    }

    return ( u32RetVal );
}
/************************************************************************
 *FUNCTION:    tu32OdoCheckReadMoreThanBufferLength
 *DESCRIPTION:  1.Opens the device and get the version.
                2.No of records Block Read > Buffer length 
                3.close the device

 
 *PARAMETER:    Nil
 *
 *RETURNVALUE:  tU32(0 - pass,Error code- failure)
 *HISTORY:      5.11.07 -Ravindran P(RBIN/EDI3)
                -Initial version
 *
 *
 ************************************************************************/
tU32 tu32OdoCheckReadMoreThanBufferLength(void)
{
    OSAL_tIODescriptor hDevice;
    tU32 u32Index;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    OSAL_trIOCtrlOdometerData arOdoDataBlockRead[READ_COUNT_MORE_THAN_BUFFER_LENGTH]= {0};
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;

    }

    /*Block read > buffer length to check Buffer rollover*/
    if(FALSE == bErrorOccured )
    {
        OSAL_s32ThreadWait(5000);
        if(OSAL_s32IORead (hDevice,(tS8*)arOdoDataBlockRead,
        sizeof(arOdoDataBlockRead)) == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 10;
            OSAL_s32IOClose ( hDevice );
        }

    }

    if(FALSE == bErrorOccured )
    {
        for(u32Index = 0;(u32Index < READ_COUNT_MORE_THAN_BUFFER_LENGTH) ; u32Index++)
        {
            if(arOdoDataBlockRead[u32Index].u16ErrorCounter)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 12;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                "OSAL_s32IORead passed but the record is invalid");
                OSAL_s32IOClose ( hDevice );
                break;
            }
        }
    }

    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }

    }

    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {

        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            "TEST FAILED Err No: %d",u8ErrorNo);

        }
    }
    return ( u32RetVal );

}

/*****************************************************************************
* FUNCTION:     tu32OdoReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open Device 
*               2. Attempt to open the ODOMETER device which has already been
*                  opened (should fail)
*               3. Close Device

* HISTORY:      21.03.2013 Niyatha S Rao(RBEI)

*****************************************************************************/
tU32 tu32OdoReOpen(tVoid)    
{
    OSAL_tIODescriptor hFd1;
    OSAL_tIODescriptor hFd2;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tBool bErrorOccured = FALSE;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /* Open the device */
    hFd1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER , enAccess );
    if ( OSAL_ERROR == hFd1)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    } 

    if(FALSE == bErrorOccured)
    {
        /* Attempt to Re-open the device 
        Mutiple open is not supported. So it should
        fail*/
        OSAL_s32ThreadWait(10);

        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"Retry OPENING the device again");

        hFd2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER , enAccess );
        if ( OSAL_ERROR != hFd2)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 13;

            /* If successful, Close the device */  
            if( OSAL_ERROR  == OSAL_s32IOClose ( hFd2 ) )
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 8;
            }
        }
        OSAL_s32ThreadWait(10);

        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," Closing the device");	

        /* Close the device */  
        if( OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;
        }
    }

    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {

        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            "TEST FAILED Err No: %d",u8ErrorNo);

        }
    }
    return ( u32RetVal );

}


/*****************************************************************************
* FUNCTION:     tu32OdoCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ACC_002
* DESCRIPTION:  1. Open Device 
*               2. Close Device    
*               3. Attempt to Close the ACC device which has already been 
*                  closed (should fail)
*
* HISTORY:   21.03.2013 Niyatha S Rao (RBEI)   
*****************************************************************************/
tU32 tu32OdoCloseAlreadyClosed(tVoid)
{
    OSAL_tIODescriptor hFd ;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;
    tBool bErrorOccured = FALSE;


    /* Open the device */
    hFd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER , enAccess );
    if ( OSAL_ERROR == hFd)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    } 

    if(FALSE == bErrorOccured)
    {
        OSAL_s32ThreadWait(10);
        /* Close the device */

        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," Closing the device");
        if( OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;
        }
    }

    if(FALSE == bErrorOccured)
    {
        OSAL_s32ThreadWait(10);
        /* Close the device which is already closed, if successful indicate
        error*/
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," Closing the device");
        if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 14;
        }
        else
        {
            /*success*/
        }
    }


    if( FALSE == bErrorOccured )
    { 
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {

        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            "TEST FAILED Err No: %d",u8ErrorNo);

        }
    }
    return ( u32RetVal );

}

/************************************************************************
*FUNCTION:    tu32OdoReadpassingnullbuffer
*DESCRIPTION:   1.Opens the device.
                2.Reads the data by passing buffer as NULL
                3.Close the device.  
*PARAMETER:     None
*
*
*RETURNVALUE: Error Number or NULL to indicate that test is passed
*
*HISTORY:  21.03.2013  Niyatha S Rao (RBEI)   
************************************************************************/
tU32 tu32OdoReadpassingnullbuffer(tVoid)
{
   
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo =  ODO_UNKNOWN_ERROR;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;
 
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    }

    if(FALSE == bErrorOccured )
    {
        if(OSAL_s32IORead (hDevice,OSAL_NULL,sizeof(OSAL_trIOCtrlOdometerData)) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 15;
            (tVoid)OSAL_s32IOClose ( hDevice );
        }
    }
    /*Close the device*/
    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;
        }

    }

    if( FALSE == bErrorOccured )
    {
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");		  
    }
    else
    {
        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
            OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
            "TEST FAILED Err No: %d",u8ErrorNo);
        }
    }
    return ( u32RetVal );
}


/************************************************************************
 *FUNCTION:    tu32OdoBasicRead 
 *DESCRIPTION:  1.Opens the device.
                2.Gets the version
                3.Reads ODO_NO_OF_ENTRIES data records from the device.
                4.Close the device.
 
 *PARAMETER:    Nil
 *
 *RETURNVALUE:  Error Number or NULL to indicate that test is passed
 *HISTORY:      19.03.2013 - Niyatha S Rao -Initial Revision.
 *
 * ************************************************************************/
tU32 tu32OdoBasicRead(void)
{
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = ODO_UNKNOWN_ERROR;
    tU32 u32Version=0;
    OSAL_trIOCtrlOdometerData rOdoData;
    //OSAL_trIOCtrlOdometerData arOdoDataBlockRead[BLOCK_READ_COUNT];
    tU32 NoOfRecsForRead = ODO_NO_OF_ENTRIES; 
    tU8 u8Index;
    tU32 u32RetVal = OEDT_ODO_TEST_PASS;

    (tVoid)u32Version;  // To fix lint warning

    #ifdef CHECK_THREAD_ODO
        OSAL_tThreadID ThreadID;
        ThreadID = OSAL_ThreadWhoAmI();
        vTtfisTrace(OSAL_C_TRACELEVEL1, "Thread No \t: %d",(tS32)ThreadID);
    #endif

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 0;
    }

    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            OSAL_s32IOClose ( hDevice );
        }
    }
    /*Sequential read*/
    if(FALSE == bErrorOccured )
    {
        for(u8Index = 0;u8Index < NoOfRecsForRead; u8Index++)
        {
            OSAL_s32ThreadWait(1000);
            if(OSAL_s32IORead (hDevice,(tS8 *)&rOdoData,sizeof(rOdoData)) == OSAL_ERROR)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 1;
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else if(rOdoData.u16ErrorCounter)
            {
                bErrorOccured = TRUE;
                u8ErrorNo = 12;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                "OSAL_s32IORead Passed but the record is invalid");
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else
            {
                OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                " Direction %u Err cnt %u TIME stamp %u Wheel Cnt %u Status %u",
                rOdoData.enDirection,
                rOdoData.u16ErrorCounter,
                rOdoData.u32TimeStamp,
                rOdoData.u32WheelCounter,
                rOdoData.enOdometerStatus);
            }

        }
    }

    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 8;

        }

    }


    if( FALSE == bErrorOccured )
    {
        u32RetVal = OEDT_ODO_TEST_PASS;
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");	
    }
    else
    {
        if(u8ErrorNo<sizeof(aOdoErr_oedt))
        {
            u32RetVal = aOdoErr_oedt[u8ErrorNo] ;
        }
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST FAILED err No: %d",u8ErrorNo);

    }

    return ( u32RetVal );


} // End of tu32OdoBasicRead()

/************************************************************************
 *FUNCTION:    tu32OdoTimeoutRead 
 *
 *DESCRIPTION:  1.Opens the device.
 *              2.Reads ODO data record from the device.
 *              3.Check if it is a timeout record.
 *              4.Validate the timeout record.
 *              Imp: Full project autosar must be flashed on the target
 *
 *PARAMETER:    Nil
 *
 *RETURNVALUE:  Error Number or 0 to indicate that test is passed
 *
 *HISTORY:      14.07.2015 - Srinivas Prakash Anvekar -Initial Revision.
 *
 * ************************************************************************/
tU32 tu32OdoTimeoutRead(void)
{
   OSAL_tIODescriptor hDevice;
   tBool bIsTimeRec = FALSE;
   tU32 u32ErrorCode = 0;
   tU8 u8Index = 0;
   OSAL_trIOCtrlOdometerData rOdoData;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
   if( hDevice == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL , " tu32OdoTimeoutRead: Odo device open failed ");
      u32ErrorCode += 1;
   }

   else
   {
      for( u8Index = 1; ( u8Index <= NO_OF_REC_FOR_RETRY ) && ( 0 == u32ErrorCode ) && ( FALSE == bIsTimeRec ); u8Index++ )
      {
         
         if( OSAL_ERROR == OSAL_s32IORead( hDevice, (tS8 *)&rOdoData, sizeof(rOdoData)) )
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL , " tu32OdoTimeoutRead: Odo device Read failed ");
            u32ErrorCode += 2;
         }
         else
         {
            if( rOdoData.u16ErrorCounter )
            {
               OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4 , " tu32OdoTimeoutRead: Timeout Record Rcvd ");
               bIsTimeRec = TRUE;
            }
            else
            {
               
               OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS , " tu32OdoTimeoutRead: No timeout Error Record Rcvd ");
               bIsTimeRec = FALSE;
               if ( NO_OF_REC_FOR_RETRY == u8Index )
               {
                  u32ErrorCode += 4;
               }
            }
         }
         
      }
      
      if( ( 0 == u32ErrorCode ) )
      {
         if( ( rOdoData.enDirection == OSAL_EN_RFS_UNKNOWN ) 
           && ( rOdoData.enOdometerStatus == ODOMSTATE_DISCONNECTED ) )
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4 , " tu32OdoTimeoutRead: Values for Timeout"
                              " Record Rcvd are as expected");
            
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_4 , " tu32OdoTimeoutRead: TimeStamp : %lu"
               " Dir : %lu Stat : %d ErrCnt : %d ", rOdoData.u32TimeStamp, rOdoData.enDirection,
               rOdoData.enOdometerStatus, rOdoData.u16ErrorCounter );
         }
         else
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS , " tu32OdoTimeoutRead: Unexpected values "
                              " for timeout record Rcvd ");
            u32ErrorCode += 8;
         }
      }
      
      
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32ErrorCode += 16;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, " Device Closing Failed at "
                 "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
      }
      
   }

   return ( u32ErrorCode );
      
}




/************************************************************************
 *FUNCTION     :    tu32OdoStubTestReadDirection 
 *DESCRIPTION:  1.Opens the device.
                         2.Reads ODO_DIRE_READ_NO_OF_ENTRIES data records from the stub.
                         3.Closes the device.
 
 *PARAMETER:    Nil
 *
 *RETURNVALUE:    OEDT_ODO_TEST_PASS   -   On successful execution of the test
 *                         Error Number                  -   On failure
 *HISTORY:      
 *
 * ************************************************************************/
tU32 tu32OdoStubTestReadDirection(void)
{

    tU32 u32RetVal = OEDT_ODO_TEST_PASS;

#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE_ODO
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU32 u32Version=0;
    OSAL_trIOCtrlOdometerData rOdoData;
    tU8 u8NoOfRecsForRead = ODO_DIRE_READ_NO_OF_ENTRIES;
    tU8 u8Index;

    /* The array expected_dir[] should match the array given in the stub sensor_proxy_chk_for_odo_direction.c */
    tU8 expected_dir[ ODO_DIRE_READ_NO_OF_ENTRIES ] = {
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_UNKW,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_UNKW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_UNKW,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_UNKW,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_UNKW,
                                                      OEDT_STUB_ODO_DIRE_REVE,
                                                      OEDT_STUB_ODO_DIRE_FORW,
                                                      OEDT_STUB_ODO_DIRE_UNKW
                                                     };

    (tVoid)u32Version;  // To fix lint warning

    #ifdef CHECK_THREAD_ODO
        OSAL_tThreadID ThreadID;
        ThreadID = OSAL_ThreadWhoAmI();
        vTtfisTrace(OSAL_C_TRACELEVEL1, "Thread No \t: %d",(tS32)ThreadID);
    #endif

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u32RetVal = ODO_DEVICE_OPEN_ERROR;
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
        "OSAL_s32IOOpen Failed for ODO device");
    }

    /*Sequential read*/
    if(FALSE == bErrorOccured )
    {
        for(u8Index = 0; u8Index < u8NoOfRecsForRead; u8Index++)
        {
            OSAL_s32ThreadWait(1000);
            if(OSAL_s32IORead (hDevice,(tS8 *)&rOdoData,sizeof(rOdoData)) == OSAL_ERROR)
            {
                bErrorOccured = TRUE;
                u32RetVal= ODO_IOREAD_ERROR;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                "OSAL_s32IORead Failed for ODO. Closing the device now");
                OSAL_s32IOClose ( hDevice );
                break;
            }
            else
            {
                if( expected_dir[u8Index] == ( (tU8)rOdoData.enDirection & OEDT_STUB_ODO_DIRE_MASK ) )
                {
                   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                   "Direction received from stub matches the expected direction."
                   "Direction : %u Err cnt %u TIME stamp %u Wheel Cnt %u Status %u",
                   rOdoData.enDirection,
                   rOdoData.u16ErrorCounter,
                   rOdoData.u32TimeStamp,
                   rOdoData.u32WheelCounter,
                   rOdoData.enOdometerStatus);
                }
                else
                {
                   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,
                   "Direction received from Stub does NOT match the expected direction"
                   "Direction received: %u Err cnt %u TIME stamp %u Wheel Cnt %u Status %u",
                   rOdoData.enDirection,
                   rOdoData.u16ErrorCounter,
                   rOdoData.u32TimeStamp,
                   rOdoData.u32WheelCounter,
                   rOdoData.enOdometerStatus);
                   u32RetVal = ODO_STUB_READ_VALUE_MISMATCH;
                   break;
                }
            }
        }
    }

    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u32RetVal = ODO_CLOSE_ERROR;
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
            "OSAL_s32IOClose Failed for ODO device");
        }
    }

    if(( u32RetVal == OEDT_ODO_TEST_PASS ) && (FALSE == bErrorOccured)) // (FALSE == bErrorOccured) is added to solve lint
    {
       OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST PASSED");
    }
    else
    {
       OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4," TEST FAILED err No: %d", u32RetVal);
    }
#endif

#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE_ODO
   OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"OEDT is valid only when stub is active ");
#endif

    return ( u32RetVal );
} // End of tu32OdoBasicRead()


//EOF
