/******************************************************************************
 * FILE         : oedt_Trace_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the Trace 
 *                device for GM-GE hardware.
 *              
 * AUTHOR(s)    : Shilpa Bhat (RBIN/EDI3)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          | 		     		           | Author & comments
 * --.--.--      | Initial revision            | ------------
 * 20 Nov,2007   | version 0.1                 | Shilpa Bhat(RBIN/EDI3)
 *				 |                             | on 20 Nov, 2007
 * ----------------------------------------------------------------------------
 * 22 Nov,2007   | version 0.2                 | Shilpa Bhat (RBIN/EDI3)
 *               |                             | Updated based on review comments 
 * ----------------------------------------------------------------------------
 * 03 Dec,2007   | version 0.3                 | Shilpa Bhat (RBIN/EDI3)
 *				 |                             | Added testcases TU_OEDT_Trace_017 
 *               |                             | to 23. Updated cases TU_OEDT_Trace
 *				 |                             | _011 and TU_OEDT_Trace_016
 --------------------------------------------------------------------------------
 * 2 Apr,2009    | version 0.4                 | Shilpa Bhat (RBIN/ECF1)
 *               |                             | Lint warning removal
 --------------------------------------------------------------------------------
 * 20 Mar,2015   | version 0.5                 | Deepak Kumar(RBEI/ECF5)
 *                                             | Lint warning removal
 *******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_Trace_TestFuncs.h"
#include "oedt_helper_funcs.h"
#include "oedt_Configuration.h"

/*****************************************************************************
* FUNCTION:		RegCallback()
* DESCRIPTION:  Gets called whenever callback is registered. 
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/      
tVoid RegCallback(tVoid)
{
	OEDT_HelperPrintf(TR_LEVEL_USER_1,"\n\t Trace Registered"); 
					//trace out message to TTFis	
}

/*****************************************************************************
* FUNCTION:		UnRegCallback()
* DESCRIPTION:  Gets called whenever callback is unregistered. 
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/      
tVoid UnRegCallback(tVoid)
{
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
			"\n\t Trace Unregistered" ); //trace out message to TTFis
}

/*****************************************************************************
* FUNCTION:		u32TraceDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_001
* DESCRIPTION:  1. Open Device 
*				2. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/
tU32 u32TraceDevOpenClose(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device in readwrite mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevOpenCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_002
* DESCRIPTION:  1. Attempt to Open Device with invalid parameter (should fail)
*				2. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/
tU32 u32TraceDevOpenCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OSAL_C_STRING_OEDTTRACE_INVALID_ACCESS_MODE;

	/* Open the device with invalid access mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR != hFd )
    {
		u32Ret = 1;
		/* If successful, indicate error and Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 3;
		}
	}
  	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_003
* DESCRIPTION:  1. Open Device 
*				2. Close Device	
*				3. Attempt to Close the Trace device which has already been 
*               closed (should fail)
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/
tU32 u32TraceDevCloseAlreadyClosed(tVoid)	
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device */
	hFd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TRACE , enAccess );
	if ( OSAL_ERROR == hFd)
	{
		u32Ret = 1;
	} 
	/* Close the device */  
	if( OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
	{
		u32Ret += 2;
	}
	
	/* Close the device which is already closed, if successful indicate error*/
	if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
	{
		u32Ret += 10;
	}
			
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_004
* DESCRIPTION:  1. Open Device 
*				2. Open the Trace device which has already been opened 
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/
tU32 u32TraceDevReOpen(tVoid)	
{
	OSAL_tIODescriptor hFd1 = 0;
	OSAL_tIODescriptor hFd2 = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device */
		hFd1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TRACE , enAccess );
	if ( OSAL_ERROR == hFd1)
	{
		u32Ret = 1;
	} 
	else
	{
		/* Re-Open the device */
 		hFd2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_TRACE , enAccess );
    	if ( OSAL_ERROR == hFd2)
    	{
    		u32Ret += 2;
		}
		else 
		{
			/* Close the device */  
			if( OSAL_ERROR  == OSAL_s32IOClose ( hFd2 ) )
			{
				u32Ret += 20;
			}
		}

		/* Close the device */  
		if( OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
		{
			u32Ret += 30;
		}
	}
    				
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevOpenCloseDiffModes() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_005
* DESCRIPTION:  1. Open the Trace device in different modes - OSAL_EN_READONLY, 
*				OSAL_EN_WRITEONLY, OSAL_EN_READWRITE
*				2. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 20 Nov, 2007 
******************************************************************************/	
tU32 u32TraceDevOpenCloseDiffModes(tVoid) 
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device in readwrite mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 2;
		}
	}
	
	enAccess = OSAL_EN_READONLY;
	/* Open the device in readonly mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret += 10;
    }
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 20;
		}
	}
	
	enAccess = OSAL_EN_WRITEONLY;
	/* Open the device in writeonly mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret += 100;
    }
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 200;
		}
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevOpenCloseInvalAccessMode() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_006
* DESCRIPTION:  1. Attempt to Open the Trace device with invalid access mode
*				(should fail)
*				2. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
******************************************************************************/	
tU32 u32TraceDevOpenCloseInvalAccessMode(tVoid) 
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OSAL_C_STRING_OEDTTRACE_INVALID_ACCESS_MODE;

	/* Open the device with invalid access mode */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR != hFd )
    {
		u32Ret = 1; 
    	/* If successfully opened, indicate an error and close the device */  
		if( OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 2;
		}
    }
	return u32Ret;
}

/* NOTE: The case below is not complete as there is no information as to what the 
         source of read is - 22 Nov, 2007 */
/*****************************************************************************
* FUNCTION:		u32TraceDevRead() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_007
* DESCRIPTION:  1. Open the Trace device 
*				2. Perform read operation 
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
*               Updated by Shilpa Bhat (RBIN/EDI3) on 22 Nov, 2007
*               Updated by Deepak Kumar(RBEI/ECF5) on 20 Mar, 2015
******************************************************************************/	
tU32 u32TraceDevRead(tVoid) 
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	tS8 *ps8ReadBuffer = NULL;
	tS32 s32BytesRead = 0;
	tU32 u32BytesToRead = 10;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/*Allocate memory dynamically*/
		ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( u32BytesToRead +1);
		/*Check if allocation successful*/
		if ( ps8ReadBuffer == NULL )
		{
			u32Ret += 2;
		}
		else
		{
			/* Perform read operation */
			s32BytesRead = OSAL_s32IORead(hFd,ps8ReadBuffer,(tU32)u32BytesToRead);
			if(OSAL_ERROR == s32BytesRead)
			{
				u32Ret += 4;
			}
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 10;
		}
	}
   if(ps8ReadBuffer != OSAL_NULL)   //condition added to free the buffer(lint-fix)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
	}
			
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceDevWrite() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_008
* DESCRIPTION:  1. Open the Trace device 
*				2. Perform write operation 
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
*               Updated by Shilpa Bhat (RBIN/EDI3) on 22 Nov, 2007
******************************************************************************/	
tU32 u32TraceDevWrite(tVoid) 
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	tS8 DataToWrite[OEDT_TRACE_MAX_LENGTH + 1] = "Test Data";   
	tU32 u32BytesToWrite = BYTES_TO_WRITE;
    tS32 s32BytesWritten = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		s32BytesWritten = OSAL_s32IOWrite(hFd,DataToWrite,
							(tU32)u32BytesToWrite);
		if(OSAL_ERROR == s32BytesWritten)
		{
			u32Ret += 2;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}
			
	return u32Ret;
}

#if 0   //not implemented in driver, hence commented - 22 Nov, 2007
/*****************************************************************************
* FUNCTION:		u32TraceGetVersion() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_009
* DESCRIPTION:  1. Open the Trace device 
*				2. Get device version
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
******************************************************************************/
tU32 u32TraceGetVersion(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Get device version */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{
			u32Ret += 2;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}
#endif

// NOTE: Functionality not tested - 22 Nov, 2007
/*****************************************************************************
* FUNCTION:		u32TraceCheckActive() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_010
* DESCRIPTION:  1. Open the Trace device 
*				2. Check if Trace is active
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
******************************************************************************/
tU32 u32TraceCheckActive(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlActivTrace ChkAct;
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		ChkAct.bIsActive = 1; 
		ChkAct.enTraceClass = (TR_tenTraceClass)0;			   //TR_CLASS_TRACE
		ChkAct.enTraceLevel = (TR_tenTraceLevel)4;			   //TR_LEVEL_COMPONENT[WARNING]
		/* Check if Trace is active */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_ISACTIVE,(tS32)&ChkAct) )
		{
			u32Ret += 2;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
			"\n\t Trace is active"); //trace out message to TTFis 
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/* NOTE: Functionality not tested - 22 Nov, 2007 */
/*****************************************************************************
* FUNCTION:		u32TraceCallbackRegUnreg() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_011
* DESCRIPTION:  1. Open the Trace device 
*				3. Register callback
*				4. Unregister callback
*				5. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
				Updated by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007 
******************************************************************************/
tU32 u32TraceCallbackRegUnreg(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlLaunchChannel CbData; 

	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		CbData.enTraceChannel = _OEDT_TTFIS_CHANNEL; 
		CbData.pCallback = (OSAL_tpfCallback)RegCallback;
		
		/* Register callback */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_CALLBACK_REG,(tS32)&CbData) )
		{
			u32Ret += 2;
		}
		else
		{
			CbData.enTraceChannel = _OEDT_TTFIS_CHANNEL; 
			CbData.pCallback = (OSAL_tpfCallback)UnRegCallback;
			/* Unregister callback */
			if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
						OSAL_C_S32_IOCTRL_CALLBACK_UNREG,(tS32)&CbData) )
			{
				u32Ret += 4;
			}
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 10;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceSendBinaryData() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_012
* DESCRIPTION:  1. Open the Trace device 
*				2. Send Binary Data
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
*               Updated by Shilpa Bhat (RBIN/EDI3) on 22 Nov, 2007 
******************************************************************************/
tU32 u32TraceSendBinaryData(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlBinaryData BinData;
//	tS8 ps8Buffer* = "Test Data";    
	tS8 ps8Buffer[10] = {0,1,0,1,0,1,0,1,0,1};    
		
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
	    BinData.u32CompId = 0;
		BinData.u8SockId = 0;
		BinData.u32BufferLen = 10;
//		BinData.puchDataBuffer = *ps8Buffer;
		BinData.puchDataBuffer = (tUChar*)ps8Buffer;
		/* Send Binary Data */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_SEND_BINARY_DATA,(tS32)&BinData) )
		{
			u32Ret += 2;
		}
   
   		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

#if 0   //not implemented in driver, hence commented - 22 Nov, 2007 
/*****************************************************************************
* FUNCTION:		u32TraceGetOutputChan() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_013
* DESCRIPTION:  1. Open the Trace device 
*				2. Get Channel currectly set (usb or uart interface)
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007 
******************************************************************************/
tU32 u32TraceGetOutputChan(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32ChanSel = 0;
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Get output channel set */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_GET_OUTPUTCHANNEL,(tS32)u32ChanSel) )
		{
			u32Ret += 2;
		}
		
		if(OUTPUT_VIA_ADITUSBIO == ChanSel)
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
			"\n\t Output set to USB channel"); //trace out message to TTFis 
		}
		
		if(OUTPUT_VIA_ADITSIO == ChanSel)
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
			"\n\t Output set to UART channel"); //trace out message to TTFis		
		}	

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceSetOutputChanUSB() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_014
* DESCRIPTION:  1. Open the Trace device 
*				2. Set Channel to USB
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
*               Updated by Shilpa Bhat (RBIN/EDI3) on 22 Nov, 2007
******************************************************************************/
tU32 u32TraceSetOutputChanUSB(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32ChanSel = 0;

	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		u32ChanSel = OUTPUT_VIA_ADITUSBIO;
		/* Set output channel to USB */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_SET_OUTPUTCHANNEL,(tS32)&u32ChanSel) )
		{
			u32Ret += 2;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
				"\n\t Output set to USB"); //trace out message to TTFis 		   	
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32TraceSetOutputChanUART() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_015
* DESCRIPTION:  1. Open the Trace device 
*				2. Set Channel to UART
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
*               Updated by Shilpa Bhat (RBIN/EDI3) on 22 Nov, 2007
******************************************************************************/
tU32 u32TraceSetOutputChanUART(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32ChanSel = 0;

	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		u32ChanSel = OUTPUT_VIA_ADITSIO;
		/* Set output channel to UART */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_SET_OUTPUTCHANNEL,(tS32)&u32ChanSel) )
		{
			u32Ret += 2;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
				"\n\t Output set to UART channel"); //trace out message to TTFis 
		}
	
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}
#endif

/* NOTE: Functionality not tested. Test case in intital stages, created with 
limited available information */
/*****************************************************************************
* FUNCTION:		u32TraceTripFuncCheck() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_016
* DESCRIPTION:  1. Open the Trace device 
*				2. Init Trip record
*				3. Replay Trip file
				4. Stop replay
				5. Get next trip data
*				6. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
*               Updated by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007
******************************************************************************/
tU32 u32TraceTripFuncCheck(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trIOCtrlInitTripData TripChk;
	tU32 u32Count = 0;	
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Create file in RAMDisk for Trip File Rec */
		if ( (OSAL_IOCreate (TRIP_FILE_REC, enAccess) == OSAL_ERROR)  )
		{
			u32Ret = 2;
		}
		
		TripChk.strFileName = TRIP_FILE_REC;
		/* Record trip data */
		if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_INIT_TRIP_DATA_REC,(tS32)&TripChk) )
		{
			u32Ret += 4;
		}
		else
		{
			/* Create another file in RAMDisk for Trip File Replay */
			if ( (OSAL_IOCreate (TRIP_FILE_REPLAY, enAccess) == OSAL_ERROR)  )
			{
				u32Ret = 10;
			}

			TripChk.strFileName = TRIP_FILE_REPLAY;
			TripChk.u32Interval = 10;
			TripChk.u8Mode = 0;
			
			/* Replay trip data */
			if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_INIT_TRIP_DATA_REPLAY,(tS32)&TripChk) )
			{
				u32Ret += 20;
			}
			else
			{
				/* Stop replay */
				if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_STOP_TRIP_DATA_REPLAY,(tS32)&u32Count) )
				{
					u32Ret += 100;
				}
				else
				{
					u32Count = 1;
					/* Get next trip data*/
					if ( OSAL_ERROR == OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_GET_NEXT_TRIP_DATA,(tS32)&u32Count) )
					{
						u32Ret += 200;
					}
				}
			}
     
     		/* Remove file created in RAMDisk */			
			if(OSAL_s32IORemove ( TRIP_FILE_REPLAY ) == OSAL_ERROR)
		   	{
		   		u32Ret += 500;
		   	}
		}

		/* Remove file created in RAMDisk */
		if(OSAL_s32IORemove ( TRIP_FILE_REC ) == OSAL_ERROR)
	   	{
	   		u32Ret += 1000;
	   	}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 10000;
		}
	}
	return u32Ret;					
}

#if 0 // does not compile - mal4hi, 2011-09-22
/*****************************************************************************
* FUNCTION:		u32TraceInvalIOCtrlParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_017
* DESCRIPTION:  1. Open the Trace device 
*				2. Attempt to perform IOCtrl operation with invalid parameter
*				3. Close Device	
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 21 Nov, 2007
******************************************************************************/
tU32 u32TraceInvalIOCtrlParam(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32ChanSel = 0;

	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		u32ChanSel = OUTPUT_VIA_ADITUSBIO;
		/* Get output channel set with invalid parameter */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hFd,
					OSAL_C_S32_OEDTTRACE_IOCTRL_INVAL_FUNC,(tS32)&u32ChanSel) )
		{
			u32Ret += 2;
		}
		
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}
#endif

#if 0   //not implemented in driver, hence commented - 03 Dec, 2007
/*****************************************************************************
* FUNCTION:	    u32TraceGetVersionNumberInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_Trace_018
* DESCRIPTION:  1. Open the Trace device 
*				2. Attempt to get the version number with invalid parameter
*				3. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007
******************************************************************************/
tU32 u32TraceGetVersionNumberInvalParam(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Get device version */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_VERSION,OSAL_NULL) )
		{
			u32Ret += 2;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	   	u32TraceGetVersionNumberAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_019
* DESCRIPTION:  1. Open the Trace device 
*				2. Attempt to get device version number after device close
*				3. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007
******************************************************************************/
tU32 u32TraceGetVersionNumberAfterDevClose(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	tS32 s32VersionInfo = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 2;
		}

		/* Attempt to get device version after device close */
		if ( OSAL_ERROR != OSAL_s32IOControl ( hFd,
					OSAL_C_S32_IOCTRL_VERSION,(tS32)&s32VersionInfo) )
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}
#endif

/*****************************************************************************
* FUNCTION:	    u32TraceDeviceOpenMultipleTimes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_020
* DESCRIPTION:  1. Open the Trace device maximum number of times
*				2. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007
******************************************************************************/
tU32 u32TraceDeviceOpenMultipleTimes(tVoid)
{
	static OSAL_tIODescriptor hFile[MULTIPLE_DEV_OPEN];
	tU32 u32Ret = 0;
	tU32 u32CountOpen = 0;
	tS32 u32CountClose = 0;
//	tU32 iCount = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

  	/* Attempt to open the device max number of times; on reaching the max count,
	   exit loop */	
	do{
	    if(OSAL_ERROR == (hFile[u32CountOpen] = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, 
	    											enAccess)))
	    {
			u32Ret = (u32CountOpen*10) + 1;		
			break;
		}
		else
		{
			u32CountOpen++;
		}
	}while(u32CountOpen < MULTIPLE_DEV_OPEN);

	/* Close the open device handles; if any has been opened successfully */
	if(u32CountOpen > 0)
	{
		u32CountClose-=u32CountClose; 
		do
		{
			if(	OSAL_ERROR == OSAL_s32IOClose ( hFile[u32CountClose] ))	
			{
				u32Ret += 10;
			}
			else
				--u32CountClose;				
		}while(u32CountClose >= 0);
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32TraceWriteInvalSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_021
* DESCRIPTION:  1. Open trace device 
*               2. Attempt to perform write operation with invalid size
*               3. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007 
******************************************************************************/
tU32 u32TraceWriteInvalSize(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
	tS8 DataToWrite[OEDT_TRACE_MAX_LENGTH + 1] = "Test Data";

    tS32 s32BytesWritten = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Attempt to perform write operation with invalid size */
		s32BytesWritten = OSAL_s32IOWrite(hFd, DataToWrite, (tU32)TRACE_INVAL_PARAM);
		if(OSAL_ERROR != s32BytesWritten)
		{
			u32Ret += 2;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}
			
	return u32Ret;  
}
 
/*****************************************************************************
* FUNCTION:	    u32TraceWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_Trace_022
* DESCRIPTION:  1. Open trace device 
*               2. Attempt to perform write operation with invalid buffer
*               3. Close Device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) on 03 Dec, 2007 
******************************************************************************/
tU32 u32TraceWriteInvalBuffer(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
 	tU32 u32Ret = 0;
    tS32 s32BytesWritten = 0;
	tU32 u32BytesToWrite = BYTES_TO_WRITE;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  
	/* Open the device */
 	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,enAccess);
	if ( OSAL_ERROR == hFd )
    {
		u32Ret = 1;
    }
	else
	{
		/* Attempt to perform write operation with invalid buffer */
		s32BytesWritten = OSAL_s32IOWrite(hFd, OSAL_NULL,
							(tU32)u32BytesToWrite);
		if(OSAL_ERROR != s32BytesWritten)
		{
			u32Ret += 2;
		}

		/* Close the device */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}
			
	return u32Ret;  
}

/************************ End of file ********************************/


