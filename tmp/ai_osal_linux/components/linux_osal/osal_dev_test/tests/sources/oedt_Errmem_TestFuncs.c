/******************************************************************************
| FILE:			oedt_Errmem_TestFuncs.c
| PROJECT:      TE_Plattform_INT_XX  (RBIN Sub Project ID: 70705_T_OEDT)
| SW-COMPONENT: OEDT FWK
|------------------------------------------------------------------------------
| DESCRIPTION:  This file implements the individual test cases for ERRMEM 
                device
|               The corresponding test specs:  TU_OEDT_ERRMEM_TestSpec.xls
                                               version:  2.5
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
|16 June 2006   |  version 1.0            |Narasimha Prasad Palasani(RBIN/ECM1)
|------------------------------------------------------------------------------
|			    |	                      |
|23 June 2006   |  version 2.0            |Narasimha Prasad Palasani(RBIN/ECM1)
|   		    |                         |Updated after review comments
|______________________________________________________________________________
|				|						  |
|28 Nov  2006	|  version 2.1            |Rakesh Dhanya (RBIN/ECM1)
|				| 						  |Updated cases number 8,9,11,12,15
|				|						  |16,18,21.
|				|						  |Inclusion of case number 23.
______________________________________________________________________________
|				|						  |
|04 Dec 2006	| version 2.2			  |Rakesh Dhanya (RBIN/ECM1)
|				|						  |Updated cases numbers 3,8,11,18
|				|						  |21,29.
|				|						  |Inclusion of case numbers 12,13,14
| 				|						  |15,22,23.
______________________________________________________________________________
|				|						  |
|11 Jan 2007	|  Version 2.3			  |Pradeep Chand C (RBIN/ECM1)
|				|						  |Updated Test cases: 
|				|						  |TU_OEDT_ERRMEM_003, 008, 011, 013,  
|				|						  |014, 017, 018, 021
|				|						  |Inclusion of case number 029, and 
|				|						  |function	 u32ErrMemCheckFatal( )
______________________________________________________________________________
|				|						  |
|19 Jan 2007	|  Version 2.4			  |Pradeep Chand C (RBIN/ECM1)
|				|						  |Updated Test Case: 003
*****************************************************************************
____________________________________________________________________
|				|						  |
| 7  Feb 2007	|  Version 2.5			  |Kishore Kumar.R (RBIN/ECM1)
|				|						  |Added new test case  
|				|						  | u32vErrmemReadTest()
*****************************************************************************

______________________________________________________________________________
|				|						  |
|25 April 2007	| version 2.6 			  |Rakesh Dhanya (RBIN/ECM1)
|				|						  |Updated cases numbers 
|				|						  |TU_OEDT_ERRMEM_013,029
|				|						  |
| 				|						  |
*****************************************************************************/  
/*****************************************************************************
|25 June 2007	| version 2.7 			  |Rakesh Dhanya (RBIN/ECM1)
|				|						  |Updated cases numbers 
|				|						  |	TU_OEDT_ERRMEM_001
|				|						  |to 030 and included cases 031-041,
|				|						  |based on inputs from DTS discussion
*****************************************************************************/  
/*****************************************************************************
|27 sep  2007	| version 2.8 			  |Anoop Chandran (RBIN/EDI3)
|				|						  |Updated cases numbers 
|				|						  |	TU_OEDT_ERRMEM_012
|				|						  | 27 september 2007
*****************************************************************************/
/*****************************************************************************
|18 oct  2007	| version 2.9 			  |Anoop Chandran (RBIN/EDI3)
|				|						  |Updated cases numbers 
|				|						  |	Remove the test cases :
|				|						  |TU_OEDT_ERRMEM_001-007,010,012,
|				|						  |016-019,024,027,029 from version2.8
|				|						  | 
*****************************************************************************
|18 oct  2007	| version 3.0	  |Anoop Chandran (RBIN/ECM1)
|				|						  |Added new new function ERRMEM_format.
|				|						  | Update the test cases
|				|						  |TU_OEDT_ERRMEM_002,006,009,
|				|						  |013,019,024
*****************************************************************************
|18 oct  2007	| version 3.1	  |Anoop Chandran (RBIN/ECM1)
|				|						  |Added new new function ERRMEM_format.
|				|						  | Update the test cases
|				|						  |TU_OEDT_ERRMEM_013
*****************************************************************************
|25 Mar  2009	| version 3.2	          |Sainath Kalpuri (RBEI/ECF1)
|				|						  |Removed lint and compiler warnings
|				|						  | 
|				|						  |
*****************************************************************************
|22 Apr  2009	| version 3.3	          |Sainath Kalpuri (RBEI/ECF1)
|				|						  |Removed lint and compiler warnings
|				|						  | 
|				|						  |
*****************************************************************************
|30 Apr 2009	| version 3.4    |Sriranjan U (RBEI/ECF1)
|				|						  |Added TC to regression dataset 
*****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Errmem_TestFuncs.h"
#include "oedt_helper_funcs.h"

#define ERRMEM_KERNEL // Added this macro for testing.
#ifdef ERRMEM_KERNEL

//#define vTtfisTrace(...)
//#define  OEDT_ERRMEM_MAP 0xFFFFFFFF


/*****************************************************************
| function implementation 
|----------------------------------------------------------------*/

	/* Normal Entry Data */
	static const char* Entry_text_normal = "Normal error: 0x 12345678 \0";

	/* Fatal Entry Data */
	static const char* Entry_text_fatal = "0x 87654321 \0";

	/* Info Data Entry */ 
	static const char* Entry_text_info = "A NEW ENTRY HAS BEEN UPDATED INTO THE ERROR MEMORY  \0";

	/*Undefined Entry Data */ //To be included, after clarifcation */
	static const char* Entry_text_undefined	="1 \0";

	/* Static counters to ensure clients are notified about 
	update to ERROR MEMORY */
	/*sak9kor- callback_info_counter is commented because it is being used
	           in dead code */
	//static tU32 callback_info_counter = 0;
	static tU32 callback_infoallcounter = 0;
	static tU32 callback_normal_counter = 0;
	

	trErrmemEntry glob_entry;
	trErrmemEntry *entry = &glob_entry; 
	/* Entry Structure that is used for write */
	
#ifdef USE_IN_KERNEL_MODE

	/* invalid version of ERROR MEMORY, to be used for testing */
	static const tU8 dev_errmem_inval_paramount_version[4] = 
	{ (DEV_ERRMEM_INVAL_PARAMOUNT_VERSION & 0xff),
  	((DEV_ERRMEM_INVAL_PARAMOUNT_VERSION / 0x100) & 0xff),
  	((DEV_ERRMEM_INVAL_PARAMOUNT_VERSION / 0x10000) & 0xff),
  	((DEV_ERRMEM_INVAL_PARAMOUNT_VERSION / 0x1000000) & 0xff)
	};

	//static const tU8 err_mem_erase_magic_invalid[4]={'#','#','#','#'};
	/* Invalid value for Magic entry (indicating block status), 
	to be used for testing */
	static const tU8 err_mem_magic_block_invalid[4] = 
	{ (ERR_MEM_MAGIC_BLOCK_INVALID & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_INVALID >> 8) & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_INVALID >> 16) & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_INVALID >> 24) & 0xff)
	};

	const tU8 err_mem_magic_block_valid[4] = 
	{ (ERR_MEM_MAGIC_BLOCK_VALID & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_VALID >> 8) & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_VALID >> 16) & 0xff),
  	((ERR_MEM_MAGIC_BLOCK_VALID >> 24) & 0xff)
	};

	const tU8 dev_errmem_paramount_version[4] = 
	{ (DEV_ERRMEM_PARAMOUNT_VERSION & 0xff),
  	((DEV_ERRMEM_PARAMOUNT_VERSION / 0x100) & 0xff),
  	((DEV_ERRMEM_PARAMOUNT_VERSION / 0x10000) & 0xff),
  	((DEV_ERRMEM_PARAMOUNT_VERSION / 0x1000000) & 0xff)
	};


/*****************************************************************************
* FUNCTION:		EMSwOff(void)
* PARAMETER:    None
* RETURNVALUE:  None
* DESCRIPTION:  This function is a new thread of execution that is started of
				to access dev EM, called by a particular test case.
* HISTORY:		Created by Rakesh Dhanya on 25 June 2007
******************************************************************************/
void EMSwOff(void)
{
	FATAL_M_ASSERT(ZERO_PARAMETER);
	tk_ext_tsk();

}

static tU32 ThreadRetVal  = 0;

/*****************************************************************************
* FUNCTION:		EMPllTh(void)
* PARAMETER:    None
* RETURNVALUE:  None
* DESCRIPTION:  This function is a new thread of execution that is started of
				dev EM, called by a particular test case.
* HISTORY:		Created by Rakesh Dhanya on 25 June 2007
******************************************************************************/
void EMPllTh(void)
{
	trErrmemEntry *entry_local = NULL;
	tU32 WriteResult = 0; 
	T_RTSK pk_rtsk;
	struct ref_param  *ThreadStruct = NULL;
	
	/*Get the task refernce information*/
	if(tk_ref_tsk(tk_get_tid(),&pk_rtsk) == E_OK)
	{
		ThreadStruct	= (pk_rtsk.exinf);
	}
	else
	{
	   ThreadRetVal++;
	}	
	   
	if(ThreadStruct != OSAL_NULL)
	{
	

	/* Set up the entry structure */
	entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE);
	if(entry_local!=NULL)
	{
		/* Write the entry into EM. Since this attempts to write to the 
		EM using a separate thread of execution, when already the device 
		is being accessed by another thread, the write should not happen and 
		must be blocked. If the write is successful, indicate an error */
			tk_sig_tev(ThreadStruct->ParTsk,1);

			WriteResult = (tU32)OSAL_s32IOWrite(ThreadStruct->hDevPtr, 
						  (tPCS8)(&entry_local), sizeof(glob_entry));

			if(OSAL_ERROR != (tS32)WriteResult)
			{
				(void )(*(ThreadStruct->RetVal)++);
			}

	}

	/* the parent is waiting for a signal from this thread. This call will
	send the specified event type. In the RefPrmForTsk structure, the task
	ID has been filled */
//	tk_sig_tev(ThreadStruct->ParTsk,3);
	}
	tk_ext_tsk();
}

#endif

/*****************************************************************************
* FUNCTION		:	ERR_MEM_BLOCK_ADDR_TEST( )
* PARAMETER		:	BlockNumber whose address needs to be calculated.
* RETURNVALUE	:	Block Address of the specified block number
* DESCRIPTION	:	This function calucates the block address of the block whose 
*						number is given as an input to this function
* HISTORY		:	Created by Rakesh Dhanya on 25 June 2007
******************************************************************************/
tU32 ERR_MEM_BLOCK_ADDR_TEST(tU32 BlockAddr)
{
	return (ERR_MEM_FLASH_START_OFFSET + (BlockAddr)*ERR_MEM_FLASH_BLOCK_SIZE);
}


/*****************************************************************************
* FUNCTION		:	ErrmemWriteSetup( )
* PARAMETER		:	Error number to be written
* RETURNVALUE	:	Pointer to the ERRMEM entry structure on success, NULL 
*						otherwise
* DESCRIPTION	:	Does a setup (fill the ERRMEM entry structure before writing)
* HISTORY		:	Created by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
trErrmemEntry* ErrmemWriteSetup(entrytype type)
{
	/* Entry ID will be randomly taken */
	entry->u16Entry = (tU16)OSAL_s32Random();
	OSAL_pvMemorySet(entry->au8EntryData,0x0,ERRMEM_MAX_ENTRY_LENGTH);
	switch (type)
	{
		case NORMAL_ENTRY_TYPE:	  /* Setup for Normal Entry */
			/* Length of the data string to be written*/
			entry->u16EntryLength = (tU16)strlen(Entry_text_normal); 
		   /* Copy data into the buffer */
		   OSAL_pvMemoryCopy(entry->au8EntryData,
		   					Entry_text_normal,entry->u16EntryLength);

		   /* Specify the type of entry */
		   entry->eEntryType = eErrmemEntryNormal;
		   break;
		   
		case FATAL_ENTRY_TYPE:	  /*Setup for Fatal Entry */
 		    /* Length of the data string to be written*/
			entry->u16EntryLength = (tU16)strlen(Entry_text_fatal);

		   /* Copy data into the buffer */
		   OSAL_pvMemoryCopy(entry->au8EntryData,
		   					Entry_text_fatal,entry->u16EntryLength);

		   /* Specify the type of entry */
		   entry->eEntryType = eErrmemEntryFatal;
		   break;

		case INFO_ENTRY_TYPE:	  /*Setup for Info Entry */
		    /* Length of the data string to be written*/
			entry->u16EntryLength = (tU16)strlen(Entry_text_info);

		   /* Copy data into the buffer */
		   OSAL_pvMemoryCopy(entry->au8EntryData,
		   					Entry_text_info,entry->u16EntryLength);

		   /* Specify the type of entry */
		   entry->eEntryType = eErrmemEntryInfo;
		   break;

		case UNDEFINED_ENTRY_TYPE:
		    /* Length of the data string to be written*/
			entry->u16EntryLength = (tU16)strlen(Entry_text_undefined);

		   /* Copy data into the buffer */
		   OSAL_pvMemoryCopy(entry->au8EntryData,
		   					Entry_text_undefined,entry->u16EntryLength);

		  /* Specify the type of entry */
		  entry->eEntryType = eErrmemEntryUndefined;
		  break;

		default :
			entry =(trErrmemEntry*) OSAL_NULL;			
	}
	return (entry); 
}
		  
		  
/*****************************************************************************
* FUNCTION		:	ErrmemCompressWrite( )
* PARAMETER		:	Error number to be written
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Writing Entries Before Compression (Called by Mem Compress 
*						case)  
* HISTORY		:	Created by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 ErrmemCompressWrite(tU32 number, const OSAL_tIODescriptor *hDevice)
{
	static const char* Entry_text_1 = "0x55\0";
	static const char* Entry_text_2 = "0x65\0";
	static const char* Entry_text_3 = "0x75\0";
	static const char* Entry_text_4 = "0x85\0";
	/* These are the 4 entry texts that are written, based on the error type */
	trErrmemEntry entry_local = {0};
	tU32 FuncRet=0,WriteResult = 0;
	tU32 type = number % 4;
	/* Use a random ID returned by the function */
	entry_local.u16Entry = (tU16)OSAL_s32Random();  
  
	/*Entry Type Normal */
	entry_local.eEntryType = eErrmemEntryNormal; 
	/* Account for the 14 byte header */
	entry_local.u16EntryLength = (tU16)strlen(Entry_text_1);
	/* Based on type of error, copy the contents into the buffer */	
	switch(type)
	{
		case 0:
			strcpy((tString)(entry_local.au8EntryData), Entry_text_1);
			break;
		case 1:
			strcpy((tString)(entry_local.au8EntryData), Entry_text_2);
			break;
		case 2:
			strcpy((tString)(entry_local.au8EntryData), Entry_text_3);
			break;
		case 3:
			strcpy((tString)(entry_local.au8EntryData), Entry_text_4);
			break;
		default:
			FuncRet += 4;
	}
	/* 	 Now write into the Error Memory */
	if (!FuncRet) 
	{
		WriteResult = (tU32)OSAL_s32IOWrite(*hDevice, 
					  (tPCS8)(&entry_local), sizeof(entry_local));
		/* Check the return value of OSAL_WRITE */
		if(( OSAL_ERROR == (tS32)WriteResult)|| 
	    		(WriteResult != entry_local.u16EntryLength ))
		{
			FuncRet += 8;
		}
	}
	return((tU32)FuncRet);
}


/*****************************************************************************
* FUNCTION		:	callback_normal( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
*						a normal error has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter)  
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_normal(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryNormal)
	{
		/* The data that was written must be available to be read in the callback 
		function.Hence just compare the  data with what was written earlier. Only 
		if these comparisons are successful increment the callback counter which is 
		an indication to the test case that the callback works fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,Entry_text_normal,
								sizeof(Entry_text_normal)))
		{	
			callback_normal_counter++;
		}
	}
}


/*****************************************************************************
* FUNCTION		:	callback_info1( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
* 						a info has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter. This is for the 
*						first registered client) 
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_info1(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryInfo)
	{
		/* The data that was written must be available to be read in the 
		callback function.Hence just compare the  data with what was written 
		earlier. Only if these comparisons are successful increment the 
		callback counter which is an indication to the test case that the 
		callback works fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,
			Entry_text_info,sizeof(Entry_text_info)))
    	{	
    		callback_infoallcounter++;
		}
	}
}


/*****************************************************************************
* FUNCTION		:	callback_info2( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
*						a info has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter. This is for the 
*						second registered client) 
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_info2(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryInfo)
	{
		/* The data that was written must be available to be read in the callback
		function.Hence just compare the  data with what was written earlier. 
		Only if these comparisons are successful increment the callback counter
		which is an indication to the test case that the callback works fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,Entry_text_info,
								sizeof(Entry_text_info)))
		{	
			callback_infoallcounter++;
		}
	}
}


/*****************************************************************************
* FUNCTION		:	callback_info3( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
*						a info has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter. This is for the 
*						third registered client) 
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_info3(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryInfo)
	{
		/* The data that was written must be available to be read in the 
		callback function.Hence just compare the  data with what was written 
		earlier. Only if these comparisons are successful increment the callback 
		counter which is an indication to the test case that the callback works 
		fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,Entry_text_info,
								sizeof(Entry_text_info)))
    	{	
    		callback_infoallcounter++;
  		}
	}
}


/*****************************************************************************
* FUNCTION		:	callback_info4( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
*						a info has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter. This is for the 
*						fourth registered client) 
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_info4(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryInfo)
	{
		/* The data that was written must be available to be read in the 
		callback function.Hence just compare the  data with what was written 
		earlier. Only if these comparisons are successful increment the callback 
		counter which is an indication to the test case that the callback works 
		fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,Entry_text_info,
								sizeof(Entry_text_info)))
    	{	
    		callback_infoallcounter++;
  		}
	}
}


/*****************************************************************************
* FUNCTION		:	callback_info5( )
* PARAMETER		:	NONE
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Callback function registered by client for notification after 
*						a info has been written into ERROR MEMORY
*						(When a notification is to be issued, this function is called.
*						It updates the value of a static counter. This is for the 
*						fifth registered client) 
* HISTORY		:	Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
static void callback_info5(tPVoid pvCookie,const trErrmemEntry* ptrErrmemEntry)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvCookie);
	if (ptrErrmemEntry->eEntryType == eErrmemEntryInfo)
	{
		/* The data that was written must be available to be read in the callback 
		function.Hence just compare the  data with what was written earlier. 
		Only if these comparisons are successful increment the callback counter 
		which is an indication to the test case that the callback works fine */

		if(!OSAL_s32StringNCompare(ptrErrmemEntry->au8EntryData,Entry_text_info,
								sizeof(Entry_text_info)))
    	{	
    		callback_infoallcounter++;
  		}
	}
}


/*****************************************************************
|  Implementations of OEDT ERRMEM functions
|----------------------------------------------------------------*/
 /* Open and close in the driver is only a dummy implementation. So the result
of this test case is irrelevant. This is as per the information received to 
RBIN, whicg reads "The Open and Close function in the error Memory has only the
 reason that the ErrMem has to fullfil the OSAL Specification. So for 
 re-opening no check is done.". Hence all cases pertaining to open and 
 close will not be tested - Rakesh Dhanya */

/****************************************************************************
* FUNCTION		:	u32ErrMemClear( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_001
* DESCRIPTION	:	Try to erase contents of  ERRMEM   
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006.
*						Updated by Pradeep Chand C (RBIN/ECM1) Jan 11, 2006 - 
*								Logic updated
*						Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
tU32 u32ErrMemClear(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	trErrmemInfo info ={0};

	/* Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );

	if (  OSAL_ERROR  == hDevice)
	{
		u32Ret += 1;
	}
	else
	{
		/* Clear all contents from the dev EM space */
		if(  OSAL_ERROR == OSAL_s32IOControl ( hDevice, 
				OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, ERRMEM_CLEAR_MAGIC_VALUE))
		{
			u32Ret += 2;
		}
		else
		{ 
			/* Check if all the contents are removed */
			if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
					OSAL_C_S32_IOCTRL_ERRMEM_CHECK, (tS32) (&info) ))
			{
				u32Ret += 4 ;
			}
			else 
			{
				/*check if the entries are actually cleared */
				if((info.u32CountOfEntries != ZERO_PARAMETER) || 
				(info.u32CountOfFatals != ZERO_PARAMETER) || 
				(info.u32NumberOfEntries !=ZERO_PARAMETER) || 
				(info.u32NumberOfFatals !=ZERO_PARAMETER))
  				{
					u32Ret += 8;
				}
				else
				{
					/* The Vtty window must be open to get this display */
					/*vTtfisTrace( OSAL_C_TRACELEVEL1,
					" NumberOfEntries : %d \n ",info.u32NumberOfEntries );*/
					OEDT_HelperPrintf(TR_LEVEL_USER_1, "NumberOfEntries : %d \n ",
		                      info.u32NumberOfEntries);
					OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                      "UsedSize : %d \n",
		                      info.u32UsedSize);

				}
			}
		}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemClearInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_002
* DESCRIPTION	:	Try to erase contents of  ERRMEM with incorrect parameters   
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Anoop Chandran 29 Feb 2008
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemClearInvalParam(void)
{
	OSAL_tIODescriptor hDevice = ERRMEM_INVALID_PARAM;
	OSAL_tIODescriptor hDevice1 = 0;
	trErrmemInfo info ={0};
	trErrmemEntry *entry_local = OSAL_NULL; /*Pointer to entry structure */
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	
	/* with invalid device handle */
	if( OSAL_ERROR != OSAL_s32IOControl(hDevice,
	OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,ERRMEM_CLEAR_MAGIC_VALUE))
	{
		u32Ret = 1;
  	}
	/* Open the device, this handle will be used */
	hDevice1 = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
	if (OSAL_ERROR == hDevice1)
	{
		u32Ret += 2;
	}
	else
	{
		entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE); 
		if ( NULL != entry_local)
		{
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite (hDevice1, (tPCS8)(&glob_entry),
					 sizeof(glob_entry));
			if(OSAL_ERROR == (tS32)WriteResult)
			{
				u32Ret += 5;
			}
		}
		/* with invalid parameter */
   	if( OSAL_ERROR != OSAL_s32IOControl(hDevice1,
		OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,ERRMEM_INVALID_PARAM))
		{
			u32Ret += 4;
		}
		/* Check if all the contents are removed */
		if( OSAL_ERROR == OSAL_s32IOControl ( hDevice1 ,  
				OSAL_C_S32_IOCTRL_ERRMEM_CHECK, (tS32) (&info)))
		{
			u32Ret += 10 ;
		}
		else 
		{
			/*check if the entries are actually cleared */
			if((info.u32CountOfEntries == ZERO_PARAMETER) || 
			(info.u32NumberOfEntries ==ZERO_PARAMETER) ) 
			{
				u32Ret += 8;
			}
			else
			{
				/* The Vtty window must be open to get this display */
				/*vTtfisTrace( OSAL_C_TRACELEVEL1,
				" NumberOfEntries : %d \n ",info.u32NumberOfEntries );*/
				OEDT_HelperPrintf(TR_LEVEL_USER_1, "NumberOfEntries : %d \n",
		                      info.u32NumberOfEntries);

				/*vTtfisTrace( OSAL_C_TRACELEVEL1,
				"UsedSize : %d \n ",info.u32UsedSize);*/
				OEDT_HelperPrintf(TR_LEVEL_USER_1, "UsedSize : %d \n ",
		                      info.u32UsedSize);
			}
		}
		/* format the sectors */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice1, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE))
	  	{
			u32Ret += 7000 ;
	  	}
		/* Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice1 ))
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemCheckNormal( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_003
* DESCRIPTION	:	Try to read the contents of  ERRMEM after writing normal 
*						entries to errmem.
* HISTORY		:	Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006. 
*						Updated by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007 
*						- Fatal Entries check removed 
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemCheckNormal(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	tU32 EntriesBeforeCheck = 0;
	tU32 EntriesAfterCheck = 0;
	tU32 UsedSizeBeforeCheck = 0;
	tU32 UsedSizeAfterCheck = 0;
	trErrmemInfo info = {0};
	trErrmemEntry *entry_local = OSAL_NULL; /*Pointer to entry structure */

	 /*Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Call an EM check to retreive the number of entries and used
		size in the Flash sector before a write and check is made */
		if(  OSAL_ERROR == OSAL_s32IOControl (hDevice ,
						OSAL_C_S32_IOCTRL_ERRMEM_CHECK , (tS32) (&info)))
		{
			u32Ret += 2;
		}
		else
		{
			/*Get the size and count before an entry is written */
			UsedSizeBeforeCheck = info.u32UsedSize;
			EntriesBeforeCheck = info.u32NumberOfEntries;

			entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE); 
			/*Set up the parameters for write */	

			if ( NULL != entry_local)
			{
				/*Writing an entry*/
				WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPCS8)(&glob_entry), 
								sizeof(glob_entry));

				/* Check for a successful write */
				if((WriteResult != sizeof(trErrmemEntry) )||( OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/ 
				{
					u32Ret += 4;
				}
				else 
				{
					if(OSAL_ERROR  == OSAL_s32IOControl (hDevice, 
							OSAL_C_S32_IOCTRL_ERRMEM_CHECK, (tS32) (&info)))
					{
						u32Ret += 32;
					}
					else
					{
						/* Get the size and count after writing an entry */
						UsedSizeAfterCheck = info.u32UsedSize;
						EntriesAfterCheck = info.u32NumberOfEntries;
						/* Validate the used size  */
						if (UsedSizeAfterCheck != ( UsedSizeBeforeCheck + (
									entry_local->u16EntryLength + 
									ERR_MEM_ENTRY_DATA_START_OFFSET ))) 
						{
							u32Ret += 64;
						}
						if (EntriesAfterCheck != EntriesBeforeCheck+1)  
						/*Validate the number of entries */
						{	
							u32Ret += 128;
						}
					}
				}
			}
		}
		/* format the sectors */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE))
	  	{
			u32Ret += 7000 ;
	  	}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		/*Close the device */
		{
			u32Ret += 256 ;
		}
	} 
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemWrite( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_003
* DESCRIPTION	:	Try to read the contents of  ERRMEM after writing normal 
*						entries to errmem.
* HISTORY		:	Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006. 
*						Updated by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007 
*						- Fatal Entries check removed 
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemWrite(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	trErrmemEntry *entry_local = OSAL_NULL; /*Pointer to entry structure */

  /*Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice )
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got error value '%i' from OSAL_IOOpen()", OSAL_u32ErrorCode());
		u32Ret = 1;
	}
	else
	{
		/*Set up the parameters for write */	
		entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE); 

		if ( NULL != entry_local )
		{
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite( hDevice, (tPCS8)(&glob_entry), sizeof(glob_entry) );

			/* Check for a successful write */
			if((WriteResult != sizeof(trErrmemEntry) )||(OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/ 
			{
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got error value '%i' from OSAL_s32IOWrite()", OSAL_u32ErrorCode());
				u32Ret += 4;
			}
        
		}
		
		/*Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		{
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got error value '%i' from OSAL_s32IOClose()", OSAL_u32ErrorCode());
			u32Ret += 8 ;
		}
	}
  
	return u32Ret;
}


#if 0
/*This case will not be tested. Because it's going to check the 
Excess Entry Data. In c programming there no boundary check for 
static array and here try to do it. This test case not going to 
check the feature of the device.*/

 /*****************************************************************************
* FUNCTION:		u32ErrMemCheckMaxEntrylength( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_004
* DESCRIPTION:  Try to check if the maximum length of an entry is 128 bytes.
                (as per specification in 
                OSAL_Devices_ErrMem_V0005.pdf document)  
* HISTORY:	    Created by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006. 
*				Updated by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007
*				- Logic updated
*               Updated by Rakesh Dhanya (RBIN/EDI3) April 25, 2007.
*				Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
  tU32 u32ErrMemCheckMaxEntrylength(void)
{
	OSAL_tIODescriptor hDevice = 0;	
 	tU32 u32Ret = 0;

	tChar Entry_text [ERR_MEM_LONG_ENTRY_SIZE] = {0};	
	/* Maximum size is 128 bytes with header. 
	So attempt to write with 116 data bytes + 14 header bytes */

	trErrmemEntry  entry;
		
	/* Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE );

	if (OSAL_ERROR == hDevice)
    {
    	u32Ret = 1;
    }
	else
	{ 	   
		/* Setup the entry structure for writing */
		/* Entry ID is randomly generated */
    	entry.u16Entry = OSAL_s32Random() ;
	
		/* Entry type will be Normal */
  		entry.eEntryType = eErrmemEntryNormal;

   		entry.u16EntryLength = ERR_MEM_LONG_ENTRY_SIZE;	 
   		/* Account for the 14 byte header */

		/* Fill up entry Data */
   		OSAL_pvMemorySet(Entry_text,'x',ERR_MEM_LONG_ENTRY_SIZE-1);
		
	    strcpy((char *)entry.au8EntryData, Entry_text);
  
		/* Attempt to Write the entry, should fail */
		if(OSAL_ERROR != OSAL_s32IOWrite(hDevice, (tPCS8)(&entry), 
										sizeof(entry)))
  		{
        	u32Ret += 2 ;
		}
		
		 
		/*Close the device */
 	   	if( OSAL_ERROR == OSAL_s32IOClose ( hDevice))
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}
#endif
/*This case will not be tested, until a definite reply is received to the list 
of open points sent. The document specifies "There is actually a cache for 20 
entries defined in EM for this usage. If this cached is filled up, the write 
access will stop the caller". The deriver code has a queue whose size is 
initialized to size of 8 entries. However, here it is seen that even upto 100 
entries can be written.

NOTE: This has been raised as an open point in the mail sent to Mr Ulrich. 
No reply has been obtained thus far. 
Due to this uncertainity, this case will remain not tested 	 */
/*****************************************************************************
* FUNCTION		:	u32ErrMemCheckMaxEntries( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_005
* DESCRIPTION	:	Check if it is possible to write more than 20 errors into 
*						ERRMEM in none blocking mode. The max is defined as 20 in
*						(as per specification in OSAL_Devices_ErrMem_V0005.pdf document.) 
* HISTORY		:	Created by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006.
*						Modified by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007.
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemCheckMaxEntries(void)
{
	OSAL_tIODescriptor hDevice = 0;	
 	tU32 u32Ret = 0;
	tU32 LoopStart = 0;
	
	trErrmemEntry  entry_local ={0};
	tU32 WriteResult[MAX_ENTRIES_TO_WRITE]={0};

	
	 /* Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE );

	if ( OSAL_ERROR == hDevice)
	{
		u32Ret += 1;
	}
	else
	{
		/* Clear all contents of the ERROR MEMORY */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice, 
		    OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, ERRMEM_CLEAR_MAGIC_VALUE))
		{
			u32Ret += 2;
		}
	  	/* Write entries one after another in a loop */
	  	for(LoopStart=0;LoopStart<MAX_ENTRIES_TO_WRITE;LoopStart++ )
		{
			/* Setup the entry structure */
			entry_local.u16Entry = (tU16)OSAL_s32Random() ;

			/* Entry type will be Normal */
			entry_local.eEntryType = eErrmemEntryNormal;

			entry_local.u16EntryLength = ERRMEM_MAX_ENTRY_LENGTH;	 

			/* Fill up entry Data */
			OSAL_pvMemorySet((tChar *)entry_local.au8EntryData,'x',ERRMEM_MAX_ENTRY_LENGTH-1);

			/* Write the entry */
			WriteResult[LoopStart] = (tU32)(OSAL_s32IOWrite(hDevice, 
							(tPCS8)(&glob_entry), sizeof(glob_entry)));
			/* Incase the write fails, set the error code */
			if(( (tS32)(WriteResult[LoopStart]) == (tS32)OSAL_ERROR)|| 
					(WriteResult[LoopStart]!= sizeof(trErrmemEntry)))
			{
				u32Ret += 4;
				break;
			}
		}
		/* Clear all contents of the ERROR MEMORY */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,
					ERRMEM_CLEAR_MAGIC_VALUE))
		{
			u32Ret += 2;
		}
		/* Close the device */   
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemCheckInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_006
* DESCRIPTION	:	Try to read contents of ERRMEM with incorrect parameters  
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Anoop Chandran 29 Feb 2008
******************************************************************************/
tU32 u32ErrMemCheckInvalParam(void)
{
	OSAL_tIODescriptor hDevice1 = 0;
	tU32 u32Ret = 0;
	/* open the device */
	hDevice1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
	if (OSAL_ERROR == hDevice1)
	{
		u32Ret += 2;
	}
	else
	{
		/* with invalid parameter */
		if( OSAL_ERROR != OSAL_s32IOControl(hDevice1,
				OSAL_C_S32_IOCTRL_ERRMEM_CHECK, ERRMEM_INVALID_PARAM))
		{
			u32Ret += 4;
		}
		/*Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice1 ))
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemRegUnregCallback( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_007
* DESCRIPTION	:	Callback function to register and unregister ERRMEM    
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006.
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemRegUnregCallback(void)
{    
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	
	trErrmemEntry *entry_local = NULL;
	callback_normal_counter = 0;
	OSAL_trErrmemInfoReg callback_info = {0};
	
	/*Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE);

	if (OSAL_ERROR == hDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Clear the ErrMem first. This will be useful because, in the callback
		function we would be saved from the trouble of comparing the entry_local ID. 
		With this setup, only the written data can be compared with what is 
		obtained in the callback function */
		if(OSAL_ERROR == OSAL_s32IOControl (hDevice,
					OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,ERRMEM_CLEAR_MAGIC_VALUE))
		{
			u32Ret += 2;
		}
		else
		{
			/* Setup the parameters for the callback structure. The cookie 
			is a standard value. The function pointer is given the address
			of the callback function that needs to be called on the arrival of 
			a new entry */
	    	callback_info.pCallback = callback_normal;
 	 		callback_info.pvCookie = (tPVoid)COOKIE_VALUE;

			/* Register the callback function for notification */
			if(OSAL_ERROR == OSAL_s32IOControl( hDevice, 
						OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,(tS32)(&callback_info)))
			{
				u32Ret +=4;
			}
			else
			{
				/* Set up the entry structure for the write */
				entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE);

				if(NULL != entry_local)
				{
					/* Write a new entry */
					WriteResult = (tU32)OSAL_s32IOWrite(hDevice,(tPCS8)(&glob_entry),
												sizeof(glob_entry));
					if((WriteResult != sizeof(trErrmemEntry))||(OSAL_ERROR == (tS32)WriteResult )) /*lint !e774 PQM_authorized_475*/ 
  					{

						u32Ret += 8 ;
 					}
					else 
					{
						OSAL_s32ThreadWait(100);
						/* if counter is not updated, the client has not 
						been notified about the new entry as this is the only 
						function where the specific callback function
						is registered for notification*/
						if(	callback_normal_counter != 1)
						{
							u32Ret += 16;	   
						}
					}
				}
				/* Unregister the callback function */
				if(OSAL_ERROR == OSAL_s32IOControl( hDevice , 
						OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER,(tS32)&callback_info))
				{
					u32Ret += 32;
				}
			}
		}
		/* format the sectors */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE))
	  	{
			u32Ret += 7000 ;
	  	}
		/*Close the device */					
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 64;
		}	
	}
	return u32Ret;
}

#if 0
/* There is a lack of clarity on how a fata entry can be written, both with
and without using OS resources. As per the document to write a fatal entry,
"To store an Fatal Error, fill up the ERRMEM-Entry structure with the data to 
be stored. This structure is copied in internal RAM. Afterwards the function 
can force an Reset of the full device without any warning."
Also parameters required for the LLD function which achieves this is 
described as
PARAMETER:
- tr_EM_Entry rEM_Entry: Error to be stored in internal RAM
- tBool bReset: If TRUE, an Reset is done after saving the fatal error in RAM.
However, in the implementation in dev_errmem.c, the second parameter is left 
unused OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(bReset) (line 1812), dev_errrmem.c. 
 Due to this inconsistency of data and lack of clarity this case will not be 
 tested by RBIN, until a clarification is received. Note that this point is 
 raised in the Open points list sent to Mr Ulrich, reply to which has not yet 
 reached RBIN - Rakesh Dhanya, 21 June 2007 */
 /*****************************************************************************
* FUNCTION:		u32ErrMemRegUnregCallbackwithFatal( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_008
* DESCRIPTION:  Callback function to register and unregister ERRMEM with 
                entry type as FATAL, should fail.   
* HISTORY:		Created by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006.
				Updated by Pradeep Chand C (RBIN/ECM1) Jan 11, 2006.
				-Logic Updated.
******************************************************************************/
 tU32 u32ErrMemRegUnregCallbackwithFatal(void)
{    
   	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
	trErrmemEntry entry;

	OSAL_trErrmemInfoReg callback_info;
	static const char* Entry_text_fatal =  
						"1234567890abcdefghijklmnopqrstuvwxyz";
  
   	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE);

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
	    if( OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_CLEAR ,
		    ERRMEM_CLEAR_MAGIC_VALUE ) == OSAL_ERROR )
	    {
	    	u32Ret += 2;
		}

	    if(OSAL_s32IOControl(hDevice,
		OSAL_C_S32_IOCTRL_ERRMEM_CHECK,(tS32) (&entry))== OSAL_ERROR)
		{
			u32Ret += 4;
  		}

	   	callback_info_counter = 0;
 		callback_info.pCallback = callback_info1;
 	 	callback_info.pvCookie = (tPVoid)0x12345678;

		if(OSAL_s32IOControl ( hDevice , 
		OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,(tS32)&callback_info) == OSAL_ERROR)
		{
			u32Ret +=8 ;
		}
		
        entry.u16Entry = eErrmemEntryFatal;
  		entry.eEntryType = eErrmemEntryFatal;
 		entry.u16EntryLength = strlen(Entry_text_fatal)+1;
	  		strcpy((char *)entry.au8EntryData, Entry_text_fatal);

       

	    if(OSAL_s32IOWrite(hDevice, (tPCS8)(&entry), sizeof(entry)) <= 0)
  		{
        	u32Ret += 16 ;
 		}
		else 
		{
			 OSAL_s32ThreadWait(100);

			if (callback_info_counter != 0)
	    	{
	    		u32Ret += 32;
	    	}
		 }
			 if( OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_CLEAR ,
		    ERRMEM_CLEAR_MAGIC_VALUE ) == OSAL_ERROR )
	    {
	    	u32Ret += 1024;
		}
	
		
			if( OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER ,
		     (tS32)&callback_info) == OSAL_ERROR )
		   	{
		   		u32Ret += 2048;
			}
		
		   						
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 4096;
		}	
	}
  	return u32Ret;
}
#endif

/*****************************************************************************
* FUNCTION		:	u32ErrMemRegUnregMaxCallback( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_009
* DESCRIPTIO	:	Checking if registration of callback can be done for 
*						maximum number of times.(Maximum is defined as 5 in 
*						OSAL_Devices_ErrMem_V0005.pdf document.      
* HISTORY		:	Created by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006. 
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Anoop Chandran 03 March 2008
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemRegUnregMaxCallback(void)
{    
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	tU32 LoopStart = 0;
	tU32 LoopStart_Unreg = 0;

	OSAL_trErrmemInfoReg callback_info[C_MAX_NOTIFIERS_ARRAY_SIZE];
	trErrmemEntry *entry_local = NULL;														
	callback_infoallcounter = 0;														
   
	OSAL_pvMemorySet(callback_info,0,sizeof(OSAL_trErrmemInfoReg));
  	/*Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE);

	if ( hDevice == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Clear the ErrMem first. This will be useful because, in the callback
		function we would be saved from the trouble of comparing the entry ID. 
		With this setup, only the written data can be compared with what is 
		obtained in the callback function */
		if(OSAL_ERROR == OSAL_s32IOControl ( hDevice, 
				OSAL_C_S32_IOCTRL_ERRMEM_CLEAR , ERRMEM_CLEAR_MAGIC_VALUE ))
		{
			u32Ret += 2;
		}
		/* In a loop, just set up the callback structure for all instances,
		and with specific callback functions, register for a callback */
 		for(LoopStart=0;LoopStart<C_MAX_NOTIFIERS;LoopStart++)
		{
			if(u32Ret)
			{
				break;
			}
			switch(LoopStart)
			{
				case 0:
					callback_info[LoopStart].pCallback = callback_info1;
		 	 		callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
					if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
							OSAL_C_S32_IOCTRL_ERRMEM_REGISTER, 
							(tS32)&callback_info[LoopStart]))
					{
						u32Ret += 2;
					}
					break;
				case 1:
					callback_info[LoopStart].pCallback = callback_info2;
		 	 		callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
					if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
						OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,
						(tS32)&callback_info[LoopStart]))
					{
						u32Ret += 4;
					}
					break;
				case 2:			 
					callback_info[LoopStart].pCallback = callback_info3;
		 	 		callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
					if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
						OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,
						(tS32)&callback_info[LoopStart]))
					{
						u32Ret += 8;
					}
					break;
				case 3:			 
					callback_info[LoopStart].pCallback = callback_info4;
		 	 		callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
					if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
						OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,
						(tS32)&callback_info[LoopStart]))
					{
						u32Ret += 16;
					}
					break;
				case 4:			 
					callback_info[LoopStart].pCallback = callback_info5;
		 	 		callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
					if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
							OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,
							(tS32)&callback_info[LoopStart]))
					{
						u32Ret += 32;
					}
					break;
				default :
					break;
			}
		}
		/* A Maximum of 5 clients can register for a notification. It should 
		not be possible to register a sixth client. Hence the negative 
		condition is checked for here */
		callback_info[LoopStart].pCallback = callback_normal;
 	 	callback_info[LoopStart].pvCookie = (tPVoid)COOKIE_VALUE;
		if(OSAL_ERROR != OSAL_s32IOControl( hDevice , 
			OSAL_C_S32_IOCTRL_ERRMEM_REGISTER ,
			(tS32)&callback_info[LoopStart]))
		{
		   u32Ret += 64;		   
		   if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
					OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER ,
					(tS32)&callback_info[LoopStart]))
			{
			   u32Ret += 100;
			}
		}	
		/* If registration of all callback functions are complete, write a new 
		entry and check if the application that has registered for the callback 
		function has been notified.Else skip this code snippet and go ahead 
		with unregistration of all the callbacks that 
		are registered till now */
		if(!u32Ret)
		{
			/* Set up the entry structure for the write */
			entry_local = ErrmemWriteSetup(INFO_ENTRY_TYPE);
						if(NULL != entry_local)
			{
				/* Write a new entry */
				WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPCS8)(&glob_entry), 
      						sizeof(glob_entry));
			    			if((WriteResult != sizeof(trErrmemEntry))||(OSAL_ERROR == (tS32)WriteResult )) /*lint !e774 PQM_authorized_475*/ 
				{
					u32Ret += 80 ;

				}
				else 
				{
					OSAL_s32ThreadWait(5*THREAD_WAIT_TIME);
					/* if counter is not updated, the clients have not been 
					notified about the new entry as this is the only function 
					where the specific callback functions have registered for 
					notification. Since 5 clients have registered, the 
					counter value must be updated to 5*/
					if(callback_infoallcounter !=  1 )
					{
						u32Ret += 160;	   
					}
				}
			}
		}
		/* Unregister the callback functions */
		for(LoopStart_Unreg=0;LoopStart_Unreg < C_MAX_NOTIFIERS;LoopStart_Unreg++)
		{
			if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
				OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER ,
				(tS32)&callback_info[LoopStart_Unreg]))
			{
				u32Ret += 64;
				break;
			}
		} 
		/* format the sectors */
		if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE))
	  	{
			u32Ret += 7000 ;
	  	}
		/*Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 128;
		}	
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemRegUnregCallbackInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_010
* DESCRIPTION	:	Registering and unregistering of ERRMEM with invalid param    
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
tU32 u32ErrMemRegUnregCallbackInvalParam(void)
{	
	OSAL_tIODescriptor hDevice1 = 0;
	tU32 u32Ret = 0;
	/* Open the device. This handle can be used in later invalid situations */
	hDevice1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
  	if ( OSAL_ERROR == hDevice1)
	{
		u32Ret += 4;
	}
	else
	{
		/*  With invalid parameter  */
		if( OSAL_ERROR != OSAL_s32IOControl(hDevice1,
				OSAL_C_S32_IOCTRL_ERRMEM_REGISTER,ERRMEM_INVALID_PARAM))
		{
			u32Ret += 20;

			/* The registration should not be possible. If possible,
			indicate an error and unregister that callback function */
			if( OSAL_ERROR == OSAL_s32IOControl(hDevice1,
					OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER,ERRMEM_INVALID_PARAM))
			{
				u32Ret += 100;
  			}
  		}
		/*Close the device */
		if(OSAL_ERROR == OSAL_s32IOClose ( hDevice1 ))
		{
			u32Ret += 1000;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemDevOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION	:	Try to open and close err mem device  
* HISTORY		:	Created by Martin Langer (CM-AI/PJ-CF33) 
******************************************************************************/
tU32 u32ErrMemDevOpenClose(void)
{
	OSAL_tIODescriptor hDevice1 = 0;
	tU32 u32Ret = 0;
	/* open the device */
	hDevice1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE);
	if (OSAL_ERROR == hDevice1)
	{
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got error value '%i' from OSAL_IOOpen()", OSAL_u32ErrorCode());
		u32Ret += 1;
	}
	else
	{
		/*Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice1 ))
		{
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: got error value '%i' from OSAL_s32IOClose()", OSAL_u32ErrorCode());
			u32Ret += 2;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemVer( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_011
* DESCRIPTION	:	Get Version    
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
* 						Updated by Rakesh Dhanya on 25 June 2007
******************************************************************************/
tU32 u32ErrMemVer(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32VersionInfo = 0;
	/* Open the device.*/
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret = 1;
	}
	else
	{
		/* Get the version information */
		if(  OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
				OSAL_C_S32_IOCTRL_VERSION ,(tS32)&s32VersionInfo))
		{
			u32Ret = 2;
		}
		else
		{
			if(DEV_ERRMEM_PARAMOUNT_VERSION != (tU32)s32VersionInfo )/*lint !e774 PQM_authorized_475*/
			{
				u32Ret+= 2;
			} 
		}
		/*Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 10;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemVerInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_012
* DESCRIPTION	:	Get Version with incorrect parameters     
* HISTORY		:	Created Narasimha Prasad Palasani (RBIN/ECM1)  June 16,2006 
*						Updated by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Anoop Chandran (RBIN/EDI3) 26 september 2007
*						Updated by Anoop Chandran (RBIN/EDI3) 27 september 2007
*							based on review comment	
******************************************************************************/
tU32 u32ErrMemVerInvalParam(void)
{
	OSAL_tIODescriptor hDevice1 = 0;
	tU32 u32Ret = 0;    
	hDevice1 = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE ) ;
	if (OSAL_ERROR == hDevice1)
	{
		u32Ret += 2;
	}
	else
	{
		/* with invalid parameter */	
		if( OSAL_ERROR != OSAL_s32IOControl(hDevice1,
				OSAL_C_S32_IOCTRL_VERSION,ERRMEM_INVALID_PARAM))
		{
			u32Ret += 4;
		}
		/*Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice1 ))
		{
			u32Ret += 8;
		}
	}
	return u32Ret;
}

   /* The following case has been re-used to strengthen the testing activity
  In this particular case, once the device is opened, all
  features are checked for before the closure.
  The error values are changed from actual code snippet, to check for
  failure of more than one feature - Rakesh Dhanya*/ 
/*****************************************************************************
* FUNCTION		:	vErrorMemTest( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_013
* DESCRIPTION	:	To check all errmem associated functionalities 
*						Returns 0 in case of no errors, or error value if there is.
* HISTORY		:	Reused by Rakesh Dhanya (RBIN/ECM1) Nov 28,2006.
*						Updated by Rakesh Dhanya (RBIN/ECM1) Dec 04,2006. 
*						Updated by Pradeep Chand (RBIN/ECM1) Jan 11, 2007
*						Updated by Rakesh Dhanya on 25 June 2007
*						Updated by Anoop Chandran (RBIN/EDI3) 26 september 2007
*						Updated by Anoop Chandran (RBIN/ECM1) 03 March 2008
* 						Updated by Anoop Chandran 25/07/2008
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 vErrorMemTest(tVoid)
{
	OSAL_tIODescriptor fd = 0;
  	tU32 u32Ret = 0;
  	tS32 Result = 0;
  	tS32 WriteResult = 0;
  	tS32 ReadResult = 0;
  	tU32 LoopStart = 0;
  	trErrmemInfo info = {0};
  	trErrmemEntry *entry_rdwr = OSAL_NULL;
  	OSAL_trErrmemInfoReg callback_info = {0};
	callback_infoallcounter = 0;
  	/* open the device */
  	fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE);
  	if(OSAL_ERROR == fd)
  	{
  		u32Ret += 1 ;
  	}
	else
	{
	  	/* format the sectors */
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  								ERRMEM_CLEAR_MAGIC_VALUE);
	  	if( OSAL_OK != Result)
	  	{
			u32Ret += 2 ;
	  	}

	  	/* check the number of error entries made */
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_CHECK, 
	  								(tS32) (&info));
	  	if( OSAL_OK != Result)
	  	{
			u32Ret += 4 ;
	  	}
	  	/* check if the numbers are really cleared */
	  	if((info.u32CountOfEntries != ZERO_PARAMETER) || 
	  				(info.u32CountOfFatals !=ZERO_PARAMETER))
	  	{
			u32Ret += 8 ;
	  	}
	 	/* Set up the entry structure for the write */
		entry_rdwr = ErrmemWriteSetup(NORMAL_ENTRY_TYPE);
		if(NULL != entry_rdwr)
		{
			/* Write a new entry */
			WriteResult = OSAL_s32IOWrite(fd, (tPCS8)(entry_rdwr),
	      									sizeof(trErrmemEntry));
			if((tU32)WriteResult != sizeof(trErrmemEntry)||(OSAL_ERROR == WriteResult ))
			{
				u32Ret += 16 ;
	 		}
		}

	  	/* read the entry again */
		OSAL_pvMemorySet((tChar *)entry->au8EntryData,'\0',ERRMEM_MAX_ENTRY_LENGTH-1);
	  	ReadResult = OSAL_s32IORead(fd, (tPS8)(entry_rdwr), 
	  								sizeof(trErrmemEntry));
	  	if((tU32)ReadResult != sizeof(trErrmemEntry)|| (OSAL_ERROR == ReadResult))
	  	{
			u32Ret += 32 ;
	  	}
		/* set a callback */
	  	callback_info.pCallback = callback_info1;
	  	callback_info.pvCookie = (tPVoid)COOKIE_VALUE;
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_REGISTER,
	  								(tS32)&callback_info);
	  	if(OSAL_OK != Result)
	  	{
			u32Ret += 128;
	  	}

	  	/* Set up the entry structure for the write */
		entry_rdwr = ErrmemWriteSetup(INFO_ENTRY_TYPE);
		if(OSAL_NULL != entry_rdwr)
		{
			/* Write a new entry */
			WriteResult = OSAL_s32IOWrite(fd, (tPCS8)(&glob_entry),
	      									sizeof(glob_entry));

			if((tU32)WriteResult != sizeof(glob_entry)||(OSAL_ERROR == WriteResult )) 
			{
				u32Ret += 256 ;
			}
		}
		OSAL_s32ThreadWait(THREAD_WAIT_TIME);
	 
		/* check the callback */
	  	if (callback_infoallcounter != 1)
	  	{
			u32Ret += 512;
	  	}

		/* Set up the entry structure for the write */
		entry_rdwr = ErrmemWriteSetup(FATAL_ENTRY_TYPE);

		if(NULL != entry_rdwr)
		{
			/* Write a new entry */
			WriteResult = OSAL_s32IOWrite(fd, (tPCS8)(&glob_entry),
	      									sizeof(glob_entry));

			if((tU32)WriteResult != sizeof(glob_entry)|| (OSAL_ERROR == ReadResult))
			{
				u32Ret += 1024 ;
			}
		}

	  	OSAL_s32ThreadWait(THREAD_WAIT_TIME);

	  	/* check the callback */
	  	if (callback_infoallcounter!= 1)
	  	{
			u32Ret += 2048 ;
	  	}
   	/* unregister the callback */
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER, 
	  								(tS32)&callback_info); 
	  	if( OSAL_OK != Result)
	  	{
			u32Ret += 4096 ;
	  	}

	  	/* format the sectors */
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  								ERRMEM_CLEAR_MAGIC_VALUE);
	  	if( OSAL_OK != Result)
	  	{
	  		u32Ret += 2 ;
	  	}
	  	/* write 100 entries */
	   for(LoopStart=0; LoopStart < MAX_ENTRIES_TO_WRITE; ++LoopStart)
	  	{
			/* Set up the entry structure for the write */
			entry_rdwr = ErrmemWriteSetup(INFO_ENTRY_TYPE);
			if(NULL != entry_rdwr)
			{
				/* Write a new entry */
				WriteResult = OSAL_s32IOWrite(fd, (tPCS8)(entry_rdwr),
	      										sizeof(glob_entry));

		    	if((tU32)WriteResult !=sizeof(glob_entry)|| (OSAL_ERROR == ReadResult))
	  			{
					u32Ret += 1000 ;
					break;
	 			}
			}
	  	}

	  	OSAL_s32ThreadWait(THREAD_WAIT_TIME);
		/* check the number of error entries made */
	  	Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_CHECK, 
	  							   (tS32) (&info));
	  	if( OSAL_OK != Result)
	  	{
			u32Ret += 5000 ;
	  	}
	  	/* check if all entries are really written */
	  	if((info.u32CountOfEntries != MAX_ENTRIES_TO_WRITE))
	  	{
			u32Ret += 6000 ;
	  	}

	  	/* format the sectors */
		Result = OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE);
	  	if( OSAL_OK != Result)
	  	{
			u32Ret += 7000 ;
	  	}

	  	/* close the device */
	  	if(OSAL_ERROR == OSAL_s32IOClose (fd))
		{
			u32Ret += 8000 ;
		}
	}
	return u32Ret;
}

/*Start of FATAL entries TEST*/
#if 0
/* There is a lack of clarity on how a fata entry can be written, both with
and without using OS resources. As per the document to write a fatal entry,
"To store an Fatal Error, fill up the ERRMEM-Entry structure with the data to 
be stored. This structure is copied in internal RAM. Afterwards the function 
can force an Reset of the full device without any warning."
Also parameters required for the LLD function which achieves this is described 
as
PARAMETER:
- tr_EM_Entry rEM_Entry: Error to be stored in internal RAM
- tBool bReset: If TRUE, an Reset is done after saving the fatal error in RAM.
However, in the implementation in dev_errmem.c, the second parameter is left 
unused
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(bReset) (line 1812), dev_errrmem.c. 
 Due to this inconsistency of data and lack of clarity this case will not be 
 tested
 by RBIN, until a clarification is received. Note that this point is raised in 
 the Open points list sent to Mr Ulrich, reply to which has not yet reached
 RBIN - Rakesh Dhanya, 21 June 2007 */


#if ERRMEM_FATAL_TEST
/*****************************************************************************
* FUNCTION:		u32ErrMemWriteFatal( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_014
* DESCRIPTION:  Try to write a FATAL entry into ERRMEM 
* CONDITIONS:	1.  CAN BE CARRIED OUT ONLY IN KERNEL MODE. For this purpose the 
*					macro ERRMEM_FATAL_TEST has to be made 1, for user mode it 
					has 
*					to be made 0.
*				2.	The system has to be restarted/reset after the test case has
*					executed successfully.
*				3.	Though the log file taken from trace might show the test has 
*					PASSED, but it has to be verified manually by the tester 
*					by executing the u32ErrMemCheckFatal() after restarting the 
*					system.		
* HISTORY:	   Created by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007
******************************************************************************/
 tU32   u32ErrMemWriteFatal(void)
 {
    OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
   	trErrmemEntry  entry;

    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );

	if ( hDevice == OSAL_ERROR )
	{
    	u32Ret = 1;
    }
   	else
	{ 
		if( OSAL_s32IOControl ( hDevice , OSAL_C_S32_IOCTRL_ERRMEM_CLEAR ,
		    ERRMEM_CLEAR_MAGIC_VALUE ) == OSAL_ERROR )
	    {
	    	u32Ret += 2;
		}
   
    	entry.u16Entry = 0x1234;  		
    	entry.eEntryType = eErrmemEntryFatal;
 		entry.u16EntryLength = 0;
		entry.au8EntryData[0] = 0; 		
		
		/* FATAL ERROR STORED IN INTERNAL RAM if fEM_WriteFatal() 
			returns EM_Result_Ok (0)
		   Otherwise ERROR : not stored in RAM */

		if( fEM_WriteFatal(entry,FALSE) != 0 )
		{
			u32Ret += 8 ;
		}
	    
	    if( OSAL_s32IOClose ( hDevice)  == OSAL_ERROR )
		{
			u32Ret += 128;
		}

	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32ErrMemCheckFatal( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to get information on the contents of  ERRMEM after writing
*				FATAL entries. This function has to be called to verify whether
*				the test for writing fatal entries has been successfull or not.
* CONDITIONS:	Function has to be called externally from e-binder.
* OUTPUT:		The output of the function can be viewed only in VTTY console 
				in
*				E-Binder.
* HISTORY:		Created by Pradeep Chand C (RBIN/ECM1) Jan 11, 2007  
******************************************************************************/

tU32 u32ErrMemCheckFatal(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
    trErrmemInfo info;
	
   	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );

	if ( hDevice == OSAL_ERROR )
	{
    	u32Ret = 1;
    }
    else
	{	  		
		if( OSAL_s32IOControl ( hDevice , 
			OSAL_C_S32_IOCTRL_ERRMEM_CHECK , (tS32) (&info) ) == OSAL_ERROR )
	    {
	    	u32Ret = 2;
	    }
		else
		{
			if(!(info.u32NumberOfEntries == 1 && info.u32NumberOfFatals == 1))
			{
				u32Ret = 3;
				printf("The number of entries is %d and fatal entries is : %d\n", 
						info.u32NumberOfEntries, info.u32NumberOfFatals);
			}
		}
		if( OSAL_s32IOControl ( hDevice , 
		   	OSAL_C_S32_IOCTRL_ERRMEM_CLEAR , 
		   	ERRMEM_CLEAR_MAGIC_VALUE ) == OSAL_ERROR )
	    {
	    	u32Ret += 10;
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 100 ;
		}
	} 	  	
	/*This is for the tester to view the output of the test in VTTY (e-binder)*/

	printf("This is to check whether the test case u32ErrMemWriteFatal() has PASSED OR FAILED\n");
	printf("Return value is : %d\n", u32Ret);
	printf("If u32Ret is 0 Test case u32ErrMemWriteFatal() has PASSED\n");
	printf("Otherwise this test countered some ERRORS or the u32ErrMemWriteFatal() test FAILED\n");
	printf("\n");
	printf("\n");
	/*End of output in VTTY*/

	return u32Ret;
}

#endif //End of FATAL entries TEST

#endif/* End of comment */
/*****************************************************************************
* FUNCTION		:	u32ErrUnregWithoutReg( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_015
* DESCRIPTION	:	Try to unregister a callback without registering for it
* HISTORY		:	Created by Rakesh Dhanya on June 25, 2007 
******************************************************************************/
tU32 u32ErrUnregWithoutReg(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	OSAL_trErrmemInfoReg callback_info = {0};
	/* Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (OSAL_ERROR == hDevice)
	{
		u32Ret += 1;
	}
	else
	{
 		callback_info.pCallback = callback_info1;
 	 	callback_info.pvCookie = (tPVoid)0x12345678;
		
		/* Unregister a callback function which is not yet registered. It should
		fail to unregister. If unregistration is successful, indicate an error */
		if(OSAL_ERROR != OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_UNREGISTER ,
		     (tS32)(&callback_info)))
		{
			u32Ret += 2;
		}

		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 4 ;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32ErrMemGetReadAndWrTime( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_016
* DESCRIPTION	:	Calculate the time required to read and write an entry at any
*							random location
* HISTORY		:	Created by Rakesh Dhanya on June 25, 2007   
*						Updated by Sriranjan U 6 May 2009
******************************************************************************/
tU32 u32ErrMemGetReadAndWrTime(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult=0;
	tU32 ReadResult=0;
	trErrmemEntry * entry_local = OSAL_NULL;
	OSAL_tMSecond WrStartTime=0; 
	OSAL_tMSecond WrEndTime =0;
	OSAL_tMSecond WriteTime = 0;
	OSAL_tMSecond RdStartTime=0;
	OSAL_tMSecond RdEndTime =0;
	OSAL_tMSecond ReadTime = 0;

    /* Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (OSAL_ERROR == hDevice)
	{
		u32Ret += 1;
	}
	else
	{
		/* Set up the entry structure before writing */
		entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE);

		/* If the structure is filled up, write the data and calculate the 
		time */
		if(NULL != entry_local)
		{
			/* Start the timer before a write operation */
		 	WrStartTime = OSAL_ClockGetElapsedTime();

			/* Perform the write operation */
			WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPCS8)(&glob_entry),
						                  sizeof(glob_entry));

			/* Get the elapsed time for write */
		 	WrEndTime = OSAL_ClockGetElapsedTime();

			/* If the specified number of bytes are not written indicate an 
			error,
			else calculate the time required to write an entry */
			if((OSAL_ERROR == (tS32)WriteResult)|| (WriteResult !=
			sizeof(trErrmemEntry)))
			{
				u32Ret += 2;
			}
			else
			{
				/* The counter could have wrapped around, ensure that the 
				endtime is 
				greater than the start time */
				if(WrEndTime > WrStartTime )
				{
					/* Calculate the write time */
					WriteTime = WrEndTime-WrStartTime;
				}
				else
				{
					WriteTime = ( 0xFFFFFFFF - WrStartTime ) + WrEndTime+1;
				}
			}

			/* Start the timer before a Read operation */
			RdStartTime = OSAL_ClockGetElapsedTime();

			/* Perform the Read operation */
			ReadResult = (tU32)OSAL_s32IORead(hDevice,(tPS8)entry_local,
							sizeof(trErrmemEntry));

			/* Get the elapsed time for read */
			RdEndTime = OSAL_ClockGetElapsedTime();

			/* If the specified number of bytes are not read indicate an error,
			else calculate the time required to read an entry */
			if( (OSAL_ERROR == (tS32)ReadResult) || (ReadResult !=  
				sizeof(trErrmemEntry)))
			{
			   u32Ret += 4;
			}
			else
			{
				/* The counter could have wrapped around, ensure that the 
				endtime is 
				greater than the start time */
				if(RdEndTime > RdStartTime )
				{
					/* Calculate the read time */
					ReadTime = RdEndTime-RdStartTime;
				}
				else
				{
					ReadTime = ( 0xFFFFFFFF - RdStartTime ) + RdEndTime+1;
				}
			}
			OEDT_HelperPrintf(TR_LEVEL_USER_1, "Write time : %d ms\n Read time : %d ms\n",
		                      WriteTime, ReadTime );
			/* format the sectors */
			if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  	 					ERRMEM_CLEAR_MAGIC_VALUE))
	  		{
				u32Ret += 7000 ;
	  		}
			/* Close the device */
			if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
			{
				u32Ret += 8 ;
			}
		}
		/* If the entry structure is not yet set up indicate an error */
		else
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}

#ifdef USE_IN_KERNEL_MODE
/*****************************************************************************
* FUNCTION:		u32ErrMemGetBlkInfoInvalid( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_017
* DESCRIPTION:  Try to get the info about a non-ERRMEM block
* HISTORY:	   Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/
tU32 u32ErrMemGetBlkInfoInvalid(void)
{
	tU8 header[ERR_MEM_END_HEADER_OFFSET];
	tU32 u32Ret = 0;

	/* Try to get information about a NON-ERRMEM Block. Only Block Numbers 0-2 
	are valid for Dev ERROR MEM. SO Block Number 5 will correspond to a 
	non ERRMEM block
	in the NOR Flash */
	 if ( OSAL_E_IOERROR != WRAPPER_FLASH_Read((tPU8)header,  
	 						ERR_MEM_BLOCK_ADDR_TEST(INVALID_ERRMEM_BLOCK),
                         	ERR_MEM_END_HEADER_OFFSET))
	{
		u32Ret += 1;
		/* Since the block does not correspond to an ERRMEM block, it should 
		not have 
		all the ERRMEM specific version number and valid block offsets. If it 
		does have such
		constants defined in the block, just indicate an eror */
		/* check if the block is formatted */
    	if ( ERR_MEM_MAGIC_BLOCK_VALID == ERR_MEM_GET_LONG_ENTRY(header))
    	{
			u32Ret += 2;
      	/* check version number. If ERRMEM version found, indicate an error */
      		if ( DEV_ERRMEM_PARAMOUNT_VERSION == ERR_MEM_GET_LONG_ENTRY(header+
      											ERR_MEM_VERSION_NUMBER_OFFSET))
      		{
				u32Ret += 4;
        	}
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32ErrMemEraseWithInvalBlock( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_018
* DESCRIPTION:  Use the Errmem Logical Flash ID, but to erase give the 
				address of a device other than errmem but within NAND/NOR FLASH
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/
tU32 u32ErrMemEraseWithInvalBlock(void)
{
	tU32 BlockAddr = 0;
	tBool EraseSuccess = FALSE;
	tU32 u32Ret = 0;

	/* This call will calculate the block address of the 
	block number that is given as an input to the function */
	BlockAddr = ERR_MEM_BLOCK_ADDR_TEST(INVALID_ERRMEM_BLOCK);

	/* This is a call to a function in the LLD. This will be 
	essential as the calls made through OSAL will not target
	any specific block within the FLASH address space */
	EraseSuccess = LFM_FlashEraseBlock(IDERRMEM,BlockAddr);
	
	/* Using the dev_errmem ID, it should not be possible to erase
	any block outside the space of dev errmem. If the erase is
	successful, indicate an error */
	if(EraseSuccess != FALSE)
	{
		u32Ret += 1;
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32ErrMemWriteWithInvalEntryHeader( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_019
* DESCRIPTION:  Write an entry with an invalid header.
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007  
*						Updated by Anoop Chandran (RBIN/ECM1) 03 March 2008
******************************************************************************/
tU32 u32ErrMemWriteWithInvalEntryHeader(void)
{
 	trErrmemEntry write_entry = {0};
 	trErrmemEntry read_entry = {0};
	tU32 Write_Hdr_Res = 0; 
	tU32 Write_Ent_Res = 0;
	tU32 offset = 0;
	tU32 Read_Res = 0;
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;

	/* The ERROR Memory will be cleared before writing an entry with an invalid 
	header. After that, the invalid entry will be written into block 0 0f 
	ERROR MEMORY */
	tU32 block_number = 0;

	/* The invalid elements are first filled into this array */
	tU16 invalid_header[ERR_MEM_ENTRY_DATA_START_OFFSET] = {0};

	/* Account for the 14 byte header, to write the entry with the header */
	write_entry.u16EntryLength = (tU16)strlen(Entry_text_normal); 

	/* Copy the data into the entry buffer */
	OSAL_pvMemoryCopy(write_entry.au8EntryData,Entry_text_normal,
						write_entry.u16EntryLength);

	/* Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice)
	{
    	u32Ret = 1;
    }
	else
	{
		/* Clear the ERROR MEMORY */
		if(  OSAL_ERROR == OSAL_s32IOControl(hDevice, 
					OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, ERRMEM_CLEAR_MAGIC_VALUE))
		{
			u32Ret += 2;
		}
  		else
		{
   			/* The invalid parameters along with	invalid parameter for the 
   			bytes that indicate that the entry is
   			valid. In the header, the first 2 bytes indicate that the entry is 
   			valid (Default values are 0x1969
   			for valid entry and 0xFFFF to indicate free entry). Here an invalid 
   			value 
   			is filled */

  			invalid_header[0] =  ERR_MEM_MAGIC_INVALID_ENTRY & 0x00FF ;
  			invalid_header[1] =  ((ERR_MEM_MAGIC_INVALID_ENTRY & 0xFF00)>> 8);
  			invalid_header[2] =  write_entry.u16EntryLength;
  			invalid_header[3] =  0x00;
  			invalid_header[4] =  0x01;
  			invalid_header[5] =  0x00;
  			invalid_header[6] =  0x00;
  			invalid_header[7] =  0x00;
  			invalid_header[8] =  0xFF;
  			invalid_header[9] =  0xFF;
  			invalid_header[10] = 0xFF;
  			invalid_header[11] = 0xFF;
  			invalid_header[12] = 0xFF;
  			invalid_header[13] = 0xFF;
  			/* offset will be set to 4. After ERRMEM Clear, a magic entry is 
  			written into the ERROR MEMORY,
	  		indicating that the block is an ERROR MEMORY block */
  			offset = 4;

  			/* Write the invalid header into dev_errmem, by calling a driver 
  			function (Wrapper_Flash_Write directly) */
  			Write_Hdr_Res =  WRAPPER_FLASH_Write((tPU8)invalid_header,
                             ERR_MEM_BLOCK_ADDR_TEST(block_number) + offset,
                             ERR_MEM_ENTRY_DATA_START_OFFSET);

  			/* If invalid entry header is successfully written , try to write 
  			the entry, 
  			but still indicate an error */
  			if (OSAL_E_IOERROR == Write_Hdr_Res) //anoop
  			{
				u32Ret += 4;

				/* Write the entry whose data is valid, but the header is 
				invalid */
				Write_Ent_Res =  WRAPPER_FLASH_Write((tPU8)
									(write_entry.au8EntryData),
                              ERR_MEM_BLOCK_ADDR_TEST(block_number) + offset 
                              + ERR_MEM_ENTRY_DATA_START_OFFSET,
                              write_entry.u16EntryLength);

				/* If writing the entry data is not successful, indicate an 
				error */
				if (OSAL_E_IOERROR == Write_Ent_Res)
				{
					u32Ret += 8;
				}
				else
				{
					/* Try to read the entry with the invalid header */
					read_entry.u16Entry = 0x00;
					read_entry.eEntryType =  eErrmemEntryNormal;

					/* Issue a read call with the details of the invalid entry 
					*/
					Read_Res = (tU32)OSAL_s32IORead(hDevice, (tPS8)(&read_entry), 
								sizeof(read_entry));
					if( OSAL_ERROR != (tS32)Read_Res)  
					{
						u32Ret += 16;
					}
				#if 0
					/*  this piece of code is comment because
					 OSAL_s32IORead  will not update read_entry  
					 acrroding to Entry ID*/

					else
					{
						/* If data with the invalid header was stored and 
						then successfuly read, indicate error */
						if (!(OSAL_s32StringNCompare(Entry_text_normal,
										read_entry.au8EntryData,
									   	sizeof(read_entry.au8EntryData))))
						{
							u32Ret += 32;
						}
					}
				#endif
				}

				/* Clear the ERROR MEMORY - Remove all invalid data just 
				written */
				if(  OSAL_ERROR == OSAL_s32IOControl(hDevice, 
									OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
									ERRMEM_CLEAR_MAGIC_VALUE))
				{
					u32Ret += 64;

				}
  			}
		}
		/* Close the device */
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 128 ;
		}
	}
return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32ErrMemWriteInvalHeaderToErrMem( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_020
* DESCRIPTION:  Write invalid header into the ERROR MEMORY.
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/
tU32 u32ErrMemWriteInvalHeaderToErrMem(void)
{   
   OSAL_tIODescriptor hDevice = 0;
   trErrmemEntry  *entry_local = OSAL_NULL;
   tU32 WriteResult = 0;
   //tU32 ReadResult = 0;
	
   tU32 u32Ret =0;

   /* The ERROR Memory will be cleared before writing an entry with an invalid 
   header. After that, the invalid entry will be written into block 0 0f 
   ERROR MEMORY */
	tU32 block_number = 0;

   /* Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice)
	{
    	u32Ret = 1;
    }
	else
	{
	  /* Erase the contents of the block by a direct call to erase in driver 
	  function,Using ERRMEM_CLEAR macro will erase the block but will write a 
	  valid ERROR MEMORY
	  header.*/
	  if ( OSAL_E_IOERROR != WRAPPER_FLASH_TEST_BlockErase
	  						(ERR_MEM_FLASH_START_OFFSET / ERR_MEM_FLASH_BLOCK_SIZE +  
	  						block_number))
	  {
			/* Now write an invalid version number into the ERROR MEMORY 
			header */
	   		if ( OSAL_E_IOERROR != WRAPPER_FLASH_Write(
	   								(tPU8)dev_errmem_inval_paramount_version,
                            		ERR_MEM_BLOCK_ADDR_TEST(block_number) + 
					ERR_MEM_VERSION_NUMBER_OFFSET, sizeof(tU32)))
    		{
				/* Write an invalid block valid number */
     			 if ( OSAL_E_IOERROR != WRAPPER_FLASH_Write(
     			 						(tPU8)err_mem_magic_block_invalid,
							ERR_MEM_BLOCK_ADDR_TEST(block_number), sizeof(tU32)))
                 {	
					/* Set up the entry structure */
					 entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE);
					 if(NULL != entry_local)
					 {
						/* Write the entry */
					 	WriteResult =(tU32) OSAL_s32IOWrite(hDevice, 
					 				(tPCS8)(&glob_entry),sizeof(glob_entry));

						if( OSAL_ERROR == (tS32)WriteResult)
						{
							u32Ret += 2;
							/* Clear the ERROR MEMORY - Remove all invalid data 
							just written */
							if( OSAL_ERROR ==  OSAL_s32IOControl(hDevice, 
												OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
												ERRMEM_CLEAR_MAGIC_VALUE) )
							{
								u32Ret += 4;
							}
						}
					 }
				 }
				 else
				 {
				    /* Error while writing invalid magic value */
					u32Ret += 8;
				 }
			 }
			 else
			 {
				/* Error while writing invalid version */
				u32Ret += 16;
			 }
		}
		else
		{
			/* Error while Erasing the Flash */
			u32Ret += 32;
		}
		/* Close the device */
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 64 ;
		}
	}
	return u32Ret;
}
#ifdef TEST_KDS_FEATURE_IN_KERNEL_MODE
/*This test case will going to test the KDS feature*/
/*****************************************************************************
* FUNCTION:		u32ErrMemWriteInvalWithinNORFLASH( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_021
* DESCRIPTION:  Write using the ID of ERRMEM, but using the address
				space of dev_kds. This address though invalid for dev_errmem
				is still part of the NOR Flash.
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/	
tU32 u32ErrMemWriteInvalWithinNORFLASH(void)
{ 
	tU32 u32Ret = 0;
	tU8 CharToWrite = 'K';
 	
 	tU32 U32Bytes = 100;
	tPU8 WriteBuff[100] = {0};
	tS32 WriteSuccess = FALSE;
	OSAL_tIODescriptor hDevice = 0;
	tBool bAccessOption = 0;

   
	/* Set the memory area with the specific character */
	OSAL_pvMemorySet(WriteBuff,CharToWrite,U32Bytes-1);
	
	/* Use an lld call directly, as using the OSAL calls, we will not be able to
	specify the address space of another device within the NOR Flash. The write 
	should fail, but in case if it does happen, just open the KDS device and clear
	 all data.
	This will be essential in order not to affect the working of dev_kds */
	WriteSuccess = WRAPPER_FLASH_Write((tPU8)WriteBuff,
                              ERR_MEM_BLOCK_ADDR_TEST(INVALID_ERRMEM_BLOCK),
                              sizeof(WriteBuff));


	
	if( OSAL_E_IOERROR != WriteSuccess )
	{
		u32Ret += 1;

		/* Open the KDS device */
		hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
    	if ( OSAL_ERROR == hDevice)
    	{
    		u32Ret += 2;
    	}
    	else
		{
			/* Set the access flag for write and enable the write */
			bAccessOption = 1;
			if(OSAL_ERROR == OSAL_s32IOControl (hDevice,
                              OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,
                              (tS32)bAccessOption) )
			{
				u32Ret += 4;
			}
			else
			{
				/* Clear the KDS device. this will ensure that if any data was 
				written
				by this test case into the address space of dev_KDS, it will be 
				erased */
				if(OSAL_ERROR == OSAL_s32IOControl (
                              hDevice,
                              OSAL_C_S32_IOCTRL_KDS_CLEAR,
                              ZERO_PARAMETER)  )
				{
					u32Ret += 8;			
				}
				else
				{
					/* After clearing the KDS device, make sure to call an init. 
					This 
					will ensure all the initial data structures will be written 
					again into
					the KDS dev address space */
					if(OSAL_ERROR == OSAL_s32IOControl (
                              hDevice,
                              OSAL_C_S32_IOCTRL_KDS_INIT,
                              ZERO_PARAMETER))
					{
						u32Ret += 16;			
					}
				}

			}
			/* Close the KDS device */
			if(OSAL_ERROR ==  OSAL_s32IOClose ( hDevice ))
			{
				u32Ret += 32;
			}

		  }
	}
	return u32Ret;
}
#endif
/*****************************************************************************
* FUNCTION:		u32ErrMemReadInvalWithinNORFLASH( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_022
* DESCRIPTION:  Read using the ID of ERRMEM, but using the address
				space of dev_kds. This address though invalid for dev_errmem
				is still part of the NOR Flash.
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/	
tU32 u32ErrMemReadInvalWithinNORFLASH(void)
{
	tU32 u32Ret = 0;
	tS32 ReadSuccess = 0;
	
 	tPU8 ReadBuff[100] = {0};
 	tU32 U32Bytes = 100;
	tChar CompareArr[100] = {0};
	
	/* Use an lld call directly, as using the OSAL calls, we will not be ableto
	specify the address space of another device within the N0R Flash*/
	ReadSuccess = WRAPPER_FLASH_Read((tPU8)ReadBuff,
                              ERR_MEM_BLOCK_ADDR_TEST(INVALID_ERRMEM_BLOCK),
                              sizeof(ReadBuff));

	/* Reading data musrt not be possible. If anything is read, indicate an 
	error */
	if((OSAL_E_IOERROR != (tU32)ReadSuccess)||(OSAL_s32StringNCompare((tCString)ReadBuff,
						(tCString)CompareArr,U32Bytes)))
	{
		u32Ret += 1;
	}
	return u32Ret;
}

static tS32 ERRMEM_format(tU32 block_number);

/*****************************************************************************
* FUNCTION:		u32ErrMemMultiThreadAccess( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_023
* DESCRIPTION:  Access the ERRMEM from 2 different threads
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007   
******************************************************************************/	
tU32 u32ErrMemMultiThreadAccess(void)
{
	T_CTSK    Pltsk;               /* Task Controlblock */
    ID tskid;				  /* task ID of the new task */
	OSAL_tIODescriptor hDevice = 0;
	tU32 BlockNumber = 0;
	tU32 u32Ret = 0;
	//INT Int_WTRet = 0;
	struct ref_param RefPrmForTsk;
	const tU8 err_mem_erase_magic[4] = {0,0,0,0};

   /* Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice )
	{
    	u32Ret = 1;
    }
	else
	{
		/* Here in this loop, format block 0 and write the version and other 
		info. 
		This is the initial step in the clear process. Normally after this, all
		other ERRMEM blocks are 'Killed'. Before doing this start off a new
		thread and try to issue a write command in that new thread of execution.
		So here even though the clear/format is incomplete, it is tested if a 
		new thread that will be spawned off will be able to access the EM and
		write an entry into the ERRMEM */
		for(BlockNumber = 0; BlockNumber < 3; BlockNumber++)
		{
			if(BlockNumber == 0)
			{
				ERRMEM_format(0);
				#if 0
				/* This call will format block 0 */
			  	if (OSAL_E_IOERROR != WRAPPER_FLASH_TEST_BlockErase
			  							(BlockNumber)) 
							
				{

					/* Write the version and other parameters */
					if ( OSAL_E_IOERROR != WRAPPER_FLASH_Write(
											(tPU8)dev_errmem_paramount_version,
                            				ERR_MEM_BLOCK_ADDR_TEST(BlockNumber) + 
                            				ERR_MEM_VERSION_NUMBER_OFFSET,
                            				sizeof(tU32)))
    				{
      					if (OSAL_E_IOERROR == WRAPPER_FLASH_Write(
      										(tPU8)err_mem_magic_block_valid,
                              				ERR_MEM_BLOCK_ADDR_TEST(BlockNumber),
                             				 sizeof(tU32)))
						{
							u32Ret+= 2;
						}
					}

					else
					{
						u32Ret+= 4;
					}
				}
				else
				{
					u32Ret+=  8;
				}
			#endif

			}
			else
			{
				/* For blocks 1 and 2, they have to be killed. Before doing 
				that spawn
				a new thread and try to write an entry into the EM */
			  	if(BlockNumber == 1)
				{
					RefPrmForTsk.RetVal = &ThreadRetVal;
					RefPrmForTsk.ParTsk	= tk_get_tid ( );
					RefPrmForTsk.hDevPtr = hDevice;
					/* Setup the parameters for the new task */
					Pltsk.exinf   = &RefPrmForTsk;       /* extended info */
    		  		Pltsk.tskatr  = TA_HLNG | TA_RNG0 | TA_FPU | TA_DSNAME;	
	   				Pltsk.task    = (FP)EMPllTh;     /* entry point */
    				Pltsk.itskpri = THREAD_PRIORITY;         /* priority */
    				Pltsk.stksz   = THREAD_STACK_SIZE; 	  /* StackSize */	
					memcpy(&Pltsk.dsname[0],"EMPllTh",8);  
					/* Name of task in Data Segment */                  

					/* Create the new task based on the above parameters */
    				tskid = tk_cre_tsk( &Pltsk );       

    				if (tskid < E_OK) 	   
    				/* prove success, if not indicate an error */
					{
						u32Ret += 16;
					}
					else
					{
      					/* start task */
      					if(E_OK != tk_sta_tsk(tskid, 0))              
	  					{
							/* If the task has not started off, indicate an 
							error */
							u32Ret += 32;
	  					}
					}
					//Int_WTRet = tk_wai_tev(1,TMO_FEVR);
					 tk_wai_tev(1,TMO_FEVR);

					/* kill Block Number 1 */
        			(tVoid)WRAPPER_FLASH_Write( (tPU8)err_mem_erase_magic,
                                    ERR_MEM_BLOCK_ADDR_TEST(BlockNumber) + 
                                    ERR_MEM_MAGIC_FULL_OFFSET,
                                    sizeof(tU32));
				}
			
				else
				{
						/* Only before killing block 1, a new thread is spwaned to 
						access the EM in parallel. Otherwise just kill all the 
						other blocks */
        			(tVoid)WRAPPER_FLASH_Write( (tPU8)err_mem_erase_magic,
                                    ERR_MEM_BLOCK_ADDR_TEST(BlockNumber) + 
                                    ERR_MEM_MAGIC_FULL_OFFSET,
                                    sizeof(tU32));
				}
			}
		}
		/* Before Accessing the value that was updated by the spawned thread, 
		check if the child
		(spawned) thread is complete. This would be essential as the two 
		threads will be 
		executing parallely and it may so happen that the spawned thread 
		has not yet updated
		the variable that is checked here */
//		Int_WTRet = tk_wai_tev(3,TMO_FEVR);
		if(ThreadRetVal)
		{
			u32Ret += 64;
		}
		/* Close the ERRMEM device */
		if(OSAL_ERROR ==  OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 128;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32ErrMemHdrChkAfterInit( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Simulate the startup procedure. Uninit and then init the flash
				and	then check the headers.
* TEST CASE:    TU_OEDT_ERRMEM_024
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007  
                Updated by Anoop Chandran (RBIN/ECM1) 03 March 2008
******************************************************************************/
tU32 u32ErrMemHdrChkAfterInit(void)
{ 	
	tBool FlashOptnRet = FALSE;
 	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 BlockNumber = 0;
	tU8 ReadBuffer[ERRMEM_HEADER_MAGIC_LENGTH] = {0};
 	/* Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice )
	{
    	u32Ret = 1;
    }
	else
	{
		/* First Clear the EM and remove all it's contents */
		if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_CLEAR ,
		    ERRMEM_CLEAR_MAGIC_VALUE ))
	    {
	    	u32Ret += 2;
		}
		else
		{
			/* Just uninitialise the flash, it will be necessary,
			Later the flash can be initialized. This will be 
			necessary as during the start up procedure, the 
			flash will be initialized. This test case checks if the 
			EM blocks have valid headers, which should be set up during
			an initialization procedure */

			FlashOptnRet = LFM_FlashUninit();
			/* If uninitialization of the flash is not successful,
			indicate an error */
			if(!FlashOptnRet)
			{
				u32Ret += 2;
			}
			else
			{
				/* Initialise the flash again, and check for EM 
				headers in the three EM blocks */
			  	FlashOptnRet = LFM_FlashInit();	
				if(!FlashOptnRet)
				{
					/* If initialization of the flash is not successful,
					indicate an error */

					u32Ret += 4;
				}
				else
				{
					/* From the 3 EM blocks, read the first four bytes. They 
					correspond
					to the block headers */
					for(BlockNumber = 0;BlockNumber < NUMBER_OF_ERRMEM_BLOCKS;
													BlockNumber++)
					{
						if (ERRMEM_format(BlockNumber) == (tS32)OSAL_E_IOERROR)
					 	{
					 	 	u32Ret += 5;

					 	}

					 	if (OSAL_E_IOERROR != WRAPPER_FLASH_Read
					 						((tPU8)ReadBuffer,
					 						ERR_MEM_BLOCK_ADDR_TEST(BlockNumber),
					 						sizeof(ReadBuffer)))
					 	{
//						#if 0
						/*  this piece of code is comment because
					 	OSAL_s32IORead  will not update read_entry  
						 acrroding to Entry ID*/

							/* Compare the read contents to see if the contents 
							are valid, i.e.
							if it corresponds to a valid EM header */
					 		if(OSAL_s32StringNCompare(ReadBuffer,
					 						err_mem_magic_block_valid,
					 						ERRMEM_HEADER_MAGIC_LENGTH))
					 		{
					 			u32Ret += 8;
					 		}
//						#endif
					 	}
					 	else
					 	{
					 		u32Ret += 16;
					 	}
					}
				}
			}
		}	
		/* Close the ERRMEM device */
		if(OSAL_ERROR ==  OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}


/* NOTE:- THIS TEST CASE IS TO BE TESTED MANUALLY AND MUST NOT BE RUN USING THE
TTFIS CONSOLE */
/*****************************************************************************
* FUNCTION:		u32ErrMemSwitchOffWhileClear( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ERRMEM_025
* DESCRIPTION:  Reset Software during a clear process 
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007  
******************************************************************************/	
tU32 u32ErrMemSwitchOffWhileClear(void)
{
	T_CTSK    Pltsk;               /* Task Controlblock */
    ID tskid;				  /* task ID of the new task */
	tU8 WriteBuffer[ERRMEM_MAX_ENTRY_LENGTH] = {0};
	OSAL_tIODescriptor hDevice = 0;
	tU32 LengthWritten = 0;
	tU32 u32Ret = 0;
   /* Open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice )
	{
    	u32Ret = 1;
    }
	else
	{
		/* First Clear the EM and remove all it's contents */
		if( OSAL_ERROR == OSAL_s32IOControl ( hDevice , 
		    OSAL_C_S32_IOCTRL_ERRMEM_CLEAR ,
		    ERRMEM_CLEAR_MAGIC_VALUE ))
	    	{
	    		u32Ret += 2;
			}
		else
		{
			   WriteBuffer[0] = 0x69;	
			    /* Bytes 1  and 2 indicate that the block is an EM block */
			   WriteBuffer[1] = 0x19;	 
			   /* This value is 0x1969 as configured by driver */
			   WriteBuffer[2] =	0x80;	 
			   /* Bytes 3 and 4 refer to the length of the entry */
			   WriteBuffer[3] = 0x00;	 
			   /* Max Length is 128, so indicate length as 0x80 */
			   WriteBuffer[4] = 0x00;	 
			   /* Bytes 5 and 6 correspond to entry type */
			   WriteBuffer[5] = 0x80;	 
			   /* This value will be 0x8000, as defined in lld_errmem.h */ 
			   WriteBuffer[6] = 0x00;	 
			   /* Bytes 7 and 8 correspond to Entry ID	*/
			   WriteBuffer[7] = 0x00;	 
			   /* This value(Entry ID) will be hardcoded to 0 */
			   WriteBuffer[8] = 0x00;	 
			   /* Bytes 9 - 15 correspond to the time */
			   WriteBuffer[9] = 0x22;	 
			   /* This has been selected at random */
			   WriteBuffer[10] = 0x08;	 
			   /* Day 18, Month 06, Year 2007 */
			   WriteBuffer[11] = 0x12;	 
			   /* Hour = 08, Minute = 34, Seconds = 0 */
			   WriteBuffer[12] = 0x06;
			   WriteBuffer[13] = 0x07;
			   WriteBuffer[14] = 0x14;
			   WriteBuffer[15] = '\0';

			   /* An entry is being written into block 1 of the dev ErrMem 
			   space. Initially
			   the flash sector corresponding to this device is erased. So the
			    header as well
			   as the entry to be written is filled into the write buffer */
			   /* After the header details the buffer is filled with data, 
			   the data 
			   corresponds to be of the info type (EM info type entry. The 
			   extra elements
			   of the array are zero padded */	
			   (tVoid)OSAL_szStringNConcat(WriteBuffer,Entry_text_info,
			   						strlen(Entry_text_info));
			   LengthWritten = (15+strlen(Entry_text_info));
			   OSAL_pvMemorySet(WriteBuffer+LengthWritten,ZERO_PARAMETER,
			   					ERRMEM_MAX_ENTRY_LENGTH-LengthWritten);
				/* Write an entry into block 1 of dev EM space */
		   			(tVoid)WRAPPER_FLASH_Write( (tPU8)WriteBuffer,
                                    ERR_MEM_BLOCK_ADDR_TEST(BLOCK_ONE) + 
					ERR_MEM_MAGIC_FULL_OFFSET, sizeof(WriteBuffer));

			/* An entry is first written into block 1.
			Here in this loop, format block 0 and write the version and other 
			info. 
			This is the initial step in the clear process. Normally after this, 
			all other ERRMEM blocks are 'Killed'. Before doing this start off a 
			new thread, which will cause the application software to be reset. 
			After this reset the one entry that was written will be tried to be 
			read.This must  be possible. So in effect it will be checked if 
			the previous stable contents are retained in the Dev Errmem Flash
			Address Sector. So in effect the format will be incomplete and an
			attempt is made to access the EM */

	   		/* This call will format block 0 */
			if (OSAL_E_IOERROR != WRAPPER_FLASH_TEST_BlockErase(0)) 
			/* Generally after formatting block 0, all other blocks will be 
			killed. Before that being done an application software reset is 
			forced 
			here */
			{

				/* Setup the parameters for the new task */
				Pltsk.exinf   = NULL;                /* extended info */
    			Pltsk.tskatr  = TA_HLNG | TA_RNG0 | TA_FPU | TA_DSNAME;;	
	   			Pltsk.task    = (FP)EMSwOff;     /* entry point */
    			Pltsk.itskpri = THREAD_PRIORITY + 10;    /* priority */
    			Pltsk.stksz   = THREAD_STACK_SIZE; 	  /* StackSize */	
				memcpy(&Pltsk.dsname[0],"EMSwOff",8);  
				/* Name of task in Data Segment */ 
				/* Create the new task based on the above parameters */
    			tskid = tk_cre_tsk( &Pltsk );       

    			if (tskid < E_OK) 	   
    			/* prove success, if not indicate an error */
				{
					u32Ret += 16;
				}
				else
				{
      				/* start task */
      				if(E_OK != tk_sta_tsk(tskid, 0))              
	  				{
						/* If the task has not started off, indicate an error */
						u32Ret += 32;
	  				}
				}
			}
		}
	  		/* Close the ERRMEM device */
			if(OSAL_ERROR ==  OSAL_s32IOClose ( hDevice ))
			{
				u32Ret += 128;
			}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		CheckERRMEMAfterReset( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Read the contents of EM after a switch off in betwen a 
				clear operation. This case is to be executed manually
				after the completion of the run of the previous case. 
* HISTORY:		Created by Rakesh Dhanya on June 25, 2007  
******************************************************************************/
tU32 CheckERRMEMAfterReset(void)
{
   	tU8 ReadBuffer[ERRMEM_MAX_ENTRY_LENGTH]={0};
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	/* Open the dev EM */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if ( OSAL_ERROR == hDevice )
	{
    	u32Ret = 1;
    }
	else
	{
		/* Read the entry from block 1 of dev EM address space */
		if (OSAL_E_IOERROR != WRAPPER_FLASH_Read((tPU8)ReadBuffer,  
					ERR_MEM_BLOCK_ADDR_TEST(BLOCK_ONE),sizeof(ReadBuffer)))
        {
			/* check if the contents of the read buffer is NULL */
			if(NULL != ReadBuffer)
			{
				/* check for the ocurance of the particular string in the 
				ReadBuffer */
        		if(NULL == strstr((const char *)ReadBuffer,Entry_text_info))
        		{
        			u32Ret += 2;
				}
			}
			else
			{
				u32Ret += 4;
			}
		}
		else
		{
			u32Ret += 8;
		}
		/* Close the ERRMEM device */
		if(OSAL_ERROR ==  OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 16;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		ERRMEM_format( )
* PARAMETER:    block_number : number of error memory block to format
* RETURNVALUE:  OSAL_E_NOERROR or OSAL_E_IOERROR
* DESCRIPTION:  format the memory block 
* HISTORY:		Taken from the driver code
******************************************************************************/	
static tS32 ERRMEM_format(tU32 block_number)
{
    tS32 ret;
    ret = OSAL_E_IOERROR;
	if (WRAPPER_FLASH_BlockErase(ERR_MEM_FLASH_START_OFFSET / 
				ERR_MEM_FLASH_BLOCK_SIZE +  block_number) != OSAL_E_IOERROR)
    {
        if (WRAPPER_FLASH_Write((tPU8)dev_errmem_paramount_version,
                                ERR_MEM_BLOCK_ADDR(block_number) + ERR_MEM_VERSION_NUMBER_OFFSET,
                                sizeof(tU32)) != OSAL_E_IOERROR)
        {
            if (WRAPPER_FLASH_Write((tPU8)err_mem_magic_block_valid,
					ERR_MEM_BLOCK_ADDR(block_number), sizeof(tU32)) != OSAL_E_IOERROR)
            {
                ret = OSAL_E_NOERROR;
            }
        }
    }
    return ret;
}
#endif

/*****************************************************************************
* FUNCTION		:	u32ErrMemCheckCorrectWriteRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_ERRMEM_026
* DESCRIPTION	:	Try to read the contents of ERRMEM after writing  
*						entries to errmem. see that write / read values matches.
* HISTORY		:	Created by Sriranjan U (RBEI/ECF1) May 04, 2009.
******************************************************************************/
tU32 u32ErrMemCheckCorrectWriteRead(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tU32 WriteResult = 0;
	tU32 ReadResult = 0;
	trErrmemEntry read_local = {0};
	trErrmemEntry temp_local = {0};
	trErrmemEntry *entry_local = OSAL_NULL; /*Pointer to entry structure */
	OSAL_pvMemorySet(glob_entry.au8EntryData,0x0,ERRMEM_MAX_ENTRY_LENGTH);
	 /*Open the device */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret += 1;
	}
	else
	{
		entry_local = ErrmemWriteSetup(NORMAL_ENTRY_TYPE); 
		/*Set up the parameters for write */
		if ( NULL != entry_local)
		{
				
			OSAL_pvMemoryCopy(&temp_local,entry_local,sizeof(trErrmemEntry));
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPCS8)(&temp_local), 
							sizeof(temp_local));
			/* Check for a successful write */
			if((WriteResult != sizeof(trErrmemEntry) )||( OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/ 
			{
				u32Ret += 4;
			}
			else 
			{
				/*Reading an entry*/
				ReadResult = (tU32)OSAL_s32IORead(hDevice, (tPS8)(&read_local), 
							sizeof(read_local));
				if ((ReadResult != sizeof(trErrmemEntry) )||(OSAL_ERROR != (tS32)ReadResult))/*lint !e774 PQM_authorized_475*/ 
				{
					if (read_local.u16Entry != temp_local.u16Entry ||
						read_local.eEntryType != temp_local.eEntryType || 
						read_local.u16EntryLength != temp_local.u16EntryLength ||
						OSAL_s32StringCompare(read_local.au8EntryData ,temp_local.au8EntryData) != 0)
					{
						u32Ret += 40;
					}
					if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	  						ERRMEM_CLEAR_MAGIC_VALUE))
	  				{
						u32Ret += 70 ; 
	  				}
				}
			}
		}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		/*Close the device */
		{
			u32Ret += 99 ;
		}
	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret += 100;
	}
	else
	{
		OSAL_pvMemorySet(&read_local,0x0,sizeof(trErrmemEntry));
		entry_local = ErrmemWriteSetup(FATAL_ENTRY_TYPE); 
		
		/*Set up the parameters for write */	
		if ( NULL != entry_local)
		{
			OSAL_pvMemoryCopy(&temp_local,entry_local,sizeof(trErrmemEntry));
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPS8)(&temp_local), 
							sizeof(temp_local));
			/* Check for a successful write */
			if((WriteResult != sizeof(trErrmemEntry) )||( OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/  
			{
				u32Ret += 400;
			}
			else 
			{
				/*Reading an entry*/
				ReadResult = (tU32)OSAL_s32IORead(hDevice, (tPS8)(&read_local), 
								sizeof(read_local));
				if (OSAL_ERROR != (tS32)ReadResult)
				{
					if (read_local.u16Entry != temp_local.u16Entry ||
						read_local.eEntryType != temp_local.eEntryType || 
						read_local.u16EntryLength != temp_local.u16EntryLength ||
						OSAL_s32StringCompare(read_local.au8EntryData ,temp_local.au8EntryData) != 0)
					{
						u32Ret += 410;
					}
				}
				if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
	 	 					ERRMEM_CLEAR_MAGIC_VALUE))
				{
					u32Ret += 710 ;
				}
			}
		}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		/*Close the device */
		{
			u32Ret += 256 ;
		}
	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret += 1000;
	}
	else
	{
		OSAL_pvMemorySet(&read_local,0x0,sizeof(trErrmemEntry));
		entry_local = ErrmemWriteSetup(INFO_ENTRY_TYPE); 
		
		/*Set up the parameters for write */	
		if ( NULL != entry_local)
		{
			OSAL_pvMemoryCopy(&temp_local,entry_local,sizeof(trErrmemEntry));
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPS8)(&temp_local), 
							sizeof(temp_local));
			/* Check for a successful write */
			if((WriteResult != sizeof(trErrmemEntry) )||( OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/ 
			{
				u32Ret += 4000;
			}
			else 
			{
				/*Reading an entry*/
				ReadResult = (tU32)OSAL_s32IORead(hDevice, (tPS8)(&read_local), 
							sizeof(read_local));
				if ((ReadResult != sizeof(trErrmemEntry) )||OSAL_ERROR != (tS32)ReadResult)/*lint !e774 PQM_authorized_475*/ 
				{
					if (read_local.u16Entry != temp_local.u16Entry ||
						read_local.eEntryType != temp_local.eEntryType || 
						read_local.u16EntryLength != temp_local.u16EntryLength ||
						OSAL_s32StringCompare(read_local.au8EntryData ,temp_local.au8EntryData) != 0)
					{
						u32Ret += 4300;
					}
				}
				if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
						ERRMEM_CLEAR_MAGIC_VALUE))
				{
					u32Ret += 4990 ;
				}
			}
		}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		/*Close the device */
		{
			u32Ret += 2560 ;
		}
	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
	if (  OSAL_ERROR == hDevice)
	{
		u32Ret += 5000;
	}
	else
	{
		OSAL_pvMemorySet(&read_local,0x0,sizeof(trErrmemEntry));
		entry_local = ErrmemWriteSetup(UNDEFINED_ENTRY_TYPE); 
		/*Set up the parameters for write */	
		if ( NULL != entry_local)
		{
			OSAL_pvMemoryCopy(&temp_local,entry_local,sizeof(trErrmemEntry));
			/*Writing an entry*/
			WriteResult = (tU32)OSAL_s32IOWrite(hDevice, (tPCS8)(&temp_local), 
							sizeof(temp_local));
			/* Check for a successful write */
			if((WriteResult != sizeof(trErrmemEntry) )||( OSAL_ERROR == (tS32)WriteResult)) /*lint !e774 PQM_authorized_475*/ 
			{
				u32Ret += 8000;
			}
			else 
			{
				/*Reading an entry*/
				ReadResult = (tU32)OSAL_s32IORead(hDevice, (tPS8)(&read_local), 
							sizeof(read_local));
				if ((ReadResult != sizeof(trErrmemEntry) )||(OSAL_ERROR != (tS32)ReadResult))/*lint !e774 PQM_authorized_475*/ 
				{
					if (read_local.u16Entry != temp_local.u16Entry ||
						read_local.eEntryType != temp_local.eEntryType || 
						read_local.u16EntryLength != temp_local.u16EntryLength ||
						OSAL_s32StringCompare(read_local.au8EntryData ,temp_local.au8EntryData) != 0)
					{
						u32Ret += 8300;
					}
				}
				if( OSAL_ERROR == OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, 
						ERRMEM_CLEAR_MAGIC_VALUE))
				{
					u32Ret += 7003 ;
				}
			}
		}
		if(  OSAL_ERROR == OSAL_s32IOClose ( hDevice ))	   
		/*Close the device */
		{
			u32Ret += 9560 ;
		}
	}
	return u32Ret;
}

#endif

/*EOF*/

