/******************************************************************************
 * Copyright (C) Blaupunkt GmbH, [year]
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ************************************************************************/

/************************************************************************/
/*! \file  oedt_System_TestFuncs.c
 *\brief    oedt system stet-cases

 *\author		CM-DI/PJ-GM32 - Resch

 *\par Copyright: 
 *(c) *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH

 *\par History:
       14/04/09       Lint Removal - Shilpa Bhat (RBEI/ECF1)  

 **********************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|----------------------------------------------------------------*/

/*------  standard includes -------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 


/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
#define OEDT_SYS_TEST_NUM_CONCURRENT_THREADS                ((tInt) 140)
/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/
#ifdef TSIM_OSAL
#define OEDT_SYS_NUM_THREADS_TO_SPAWN 10
#else /*#ifdef TSIM_OSAL*/
#define OEDT_SYS_NUM_THREADS_TO_SPAWN 1000
#endif /*#ifdef TSIM_OSAL*/


/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
static tInt n_system_test_started_threads_cnt = 0;
/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/

void oedt_system_test_empty_thread(const tU32 *p_arg){
    
    (tVoid)p_arg;
    OSAL_vThreadExit();
}



void oedt_system_test_concurrent_thread(const tU32 *p_arg){
    
	(tVoid)p_arg;
    n_system_test_started_threads_cnt++;
    while(n_system_test_started_threads_cnt < OEDT_SYS_TEST_NUM_CONCURRENT_THREADS){
        OSAL_s32ThreadWait(50);
    }
    return;
}

void oedt_system_test_empty_thread_no_exit(const tU32 *p_arg){
    
    (tVoid)p_arg;
    return;
}


void oedt_system_test_empty_thread_no_terminate(const tU32 *p_arg){
    
    (tVoid)p_arg;
	for(;;){
        OSAL_s32ThreadWait(5);
    }
}





/************************************************************************/
/*! 
 *\fn       tU32 u32_OEDT_SystemTest1_ThreadSpawn(void){
 *\brief    Spawns several threads
 *
 *\param none
 *
 *\return     0  -->  on success
 *\           >0 --> on error
 *
 *\par History:  
 * [history]
 **********************************************************************/
tU32 u32_OEDT_SystemTest1_ThreadSpawn(void){
    tU32 u32_ret = 0;
    tChar sz_name[128] ="oedt_test_thread";
    tU16 i = 0;
    OSAL_trThreadAttribute tr_thread_attr; 
    OSAL_tThreadID osal_thread_id;
    tS32 s32_osal_ret;
    tU16 u16_wait_cnt = 0;
    OSAL_trThreadControlBlock tr_thread_ctrl;
    
    for(i=0;i<OEDT_SYS_NUM_THREADS_TO_SPAWN;i++){
        
        tr_thread_attr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        tr_thread_attr.s32StackSize = 2048;
        tr_thread_attr.szName       = sz_name;
        tr_thread_attr.pfEntry      = (OSAL_tpfThreadEntry) oedt_system_test_empty_thread;      /* Startroutine */
        tr_thread_attr.pvArg        = NULL;                        /* Argument der Startroutine */
        
        osal_thread_id = OSAL_ThreadSpawn(&tr_thread_attr);
        if(osal_thread_id == OSAL_ERROR){
            
            u32_ret += 10;
            
        }else{
            s32_osal_ret = OSAL_s32ThreadControlBlock(osal_thread_id,
                                                      &tr_thread_ctrl);
            u16_wait_cnt = 0;
            while((s32_osal_ret != OSAL_ERROR) &&
                  (u16_wait_cnt < 50)){
                
                OSAL_s32ThreadWait(50);
                s32_osal_ret = OSAL_s32ThreadControlBlock(osal_thread_id,
                                                          &tr_thread_ctrl);
                
            }
            
            OSAL_s32ThreadDelete(osal_thread_id);
            
        }
    }
    
    return( u32_ret);
}



/************************************************************************/
/*! 
 *\fn       u32_OEDT_SystemTest2_ThreadSpawnWithoutDelete
 *\brief    Spawns several threads
 *
 *\param none
 *
 *\return     0  -->  on success
 *\           >0 --> on error
 *
 *\par History:  
 * [history]
 **********************************************************************/
tU32 u32_OEDT_SystemTest2_ThreadSpawnWithoutDelete(void){
    
    tU32 u32_ret = 0;
    tChar sz_name[128] ="oedt_test_thread";
    tU16 i = 0;
    OSAL_trThreadAttribute tr_thread_attr; 
    OSAL_tThreadID osal_thread_id;
    
    for(i=0;i<OEDT_SYS_NUM_THREADS_TO_SPAWN;i++){

        tr_thread_attr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        tr_thread_attr.s32StackSize = 2048;
        tr_thread_attr.szName       = sz_name;
        tr_thread_attr.pfEntry      = (OSAL_tpfThreadEntry) oedt_system_test_empty_thread;      /* Startroutine */
        tr_thread_attr.pvArg        = NULL;                        /* Argument der Startroutine */
        
        osal_thread_id = OSAL_ThreadSpawn(&tr_thread_attr);
        if(osal_thread_id == OSAL_ERROR){
            
            u32_ret += 10;
            
        }else{
            OSAL_s32ThreadWait(50);            
        }
    }
    
    return( u32_ret);

}

/************************************************************************/
/*! 
 *\fn       u32_OEDT_SystemTest3_ThreadSpawnWithoutExit
 *\brief    Spawns several threads
 *
 *\param none
 *
 *\return     0  -->  on success
 *\           >0 --> on error
 *
 *\par History:  
 * [history]
 **********************************************************************/
tU32 u32_OEDT_SystemTest3_ThreadSpawnWithoutExit(void){
    tU32 u32_ret = 0;
    tChar sz_name[128] ="oedt_test_thread";
    tU16 i = 0;
    OSAL_trThreadAttribute tr_thread_attr; 
    OSAL_tThreadID osal_thread_id;
    tS32 s32_osal_ret;
    tU16 u16_wait_cnt = 0;
    OSAL_trThreadControlBlock tr_thread_ctrl;
    
    for(i=0;i<OEDT_SYS_NUM_THREADS_TO_SPAWN;i++){
        
        tr_thread_attr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        tr_thread_attr.s32StackSize = 2048;
        tr_thread_attr.szName       = sz_name;
        tr_thread_attr.pfEntry      = (OSAL_tpfThreadEntry) oedt_system_test_empty_thread_no_exit;      /* Startroutine */
        tr_thread_attr.pvArg        = NULL;                        /* Argument der Startroutine */
        
        osal_thread_id = OSAL_ThreadSpawn(&tr_thread_attr);
        if(osal_thread_id == OSAL_ERROR){
            
            u32_ret += 10;
            
        }else{
            s32_osal_ret = OSAL_s32ThreadControlBlock(osal_thread_id,
                                                      &tr_thread_ctrl);
            u16_wait_cnt = 0;
            while((s32_osal_ret != OSAL_ERROR) &&
                  (u16_wait_cnt < 50)){
                
                OSAL_s32ThreadWait(50);
                s32_osal_ret = OSAL_s32ThreadControlBlock(osal_thread_id,
                                                          &tr_thread_ctrl);
                
            }
            
            OSAL_s32ThreadDelete(osal_thread_id);
            
        }
    }
    
    return( u32_ret);
}

/************************************************************************/
/*! 
 *\fn       u32_OEDT_SystemTest4_KillRunningThreads
 *\brief    Spawns several threads
 *
 *\param none
 *
 *\return     0  -->  on success
 *\           >0 --> on error
 *
 *\par History:  
 * [history]
 **********************************************************************/
tU32 u32_OEDT_SystemTest4_KillRunningThreads(void){

    tU32 u32_ret = 0;
    tChar sz_name[128] ="oedt_test_thread";
    tU16 i = 0;
    OSAL_trThreadAttribute tr_thread_attr; 
    OSAL_tThreadID osal_thread_id;
    
    for(i=0;i<OEDT_SYS_NUM_THREADS_TO_SPAWN;i++){
        
        tr_thread_attr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        tr_thread_attr.s32StackSize = 2048;
        tr_thread_attr.szName       = sz_name;
        tr_thread_attr.pfEntry      = (OSAL_tpfThreadEntry) oedt_system_test_empty_thread_no_terminate;      /* Startroutine */
                                      
        tr_thread_attr.pvArg        = NULL;                        /* Argument der Startroutine */
        
        osal_thread_id = OSAL_ThreadSpawn(&tr_thread_attr);
        if(osal_thread_id == OSAL_ERROR){
            
            u32_ret += 10;
            
        }else{
            OSAL_s32ThreadWait(50);
            OSAL_s32ThreadDelete(osal_thread_id);
        }
    }
    
    return( u32_ret);
}


/************************************************************************/
/*! 
 *\fn       u32_OEDT_SystemTest5_StartMultipleThreads
 *\brief    Spawns several threads
 *
 *\param none
 *
 *\return     0  -->  on success
 *\           >0 --> on error
 *
 *\par History:  
 * [history]
 **********************************************************************/
static int test5_boundary = 140;
tU32 u32_OEDT_SystemTest5_StartMultipleThreads(void){
    tU32 u32_ret = 0;
    tChar sz_base_name[128] = "oedt_test_thread";
    tChar sz_name[128]      = "";
    tU16 i = 0;
    tU16 j = 0;
    OSAL_trThreadAttribute tr_thread_attr; 
    OSAL_tThreadID osal_thread_id[OEDT_SYS_TEST_NUM_CONCURRENT_THREADS];
    
    for(i=0;i < 5;i++){
        n_system_test_started_threads_cnt = 0;
        for(j=0;j<OEDT_SYS_TEST_NUM_CONCURRENT_THREADS;j++){

            if(j==test5_boundary){
                j++;
                j--;
            }

            OSALUTIL_s32SaveNPrintFormat(&sz_name[0],
                                         sizeof(sz_name), 
                                         "%s%d",
                                         sz_base_name, j);

            tr_thread_attr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
            tr_thread_attr.s32StackSize = 2048;
            tr_thread_attr.szName       = sz_name;
            tr_thread_attr.pfEntry      = (OSAL_tpfThreadEntry) oedt_system_test_concurrent_thread;      /* Startroutine */
            
            tr_thread_attr.pvArg        = NULL;                        /* Argument der Startroutine */
            
            osal_thread_id[j] = OSAL_ThreadSpawn(&tr_thread_attr);
        
            if(osal_thread_id[j] == OSAL_ERROR){
                
                u32_ret += 10;
            }
        }
        OSAL_s32ThreadWait(100);        
        for(j=0;j<OEDT_SYS_TEST_NUM_CONCURRENT_THREADS;j++){
            OSAL_s32ThreadDelete(osal_thread_id[j]);
        }
        
    }
    
    return( u32_ret);
    
}

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/

/*! @}*/
