/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file includes the individual test cases ( OEDT testcases) for DataPool.
 * @addtogroup   OEDT
 * @{
 */
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DP_S_IMPORT_INTERFACE_BASE
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_if.h"
#include "dp_generic_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "DataPoolSCC.h"
#include "pdd.h" 
#include "pdd_config_nor_user.h" 

#ifdef DP_U32_POOL_ID_PDDDPTEST
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_effd_if.h"
#endif

#ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_spm_if.h"
#endif

#ifdef __cplusplus
  extern "C" {
#endif


#include "oedt_DataPool_TestFuncs.h"
#include "../../ai_osal_linux/components/devices/pdd/test/config/pddDpTestStructure.h"

/*********************************************************************************/
/*                         define                                                */
/*********************************************************************************/
#define DP_EARLY_CONFIG_DISPLAY_RESOLTUTION_STRING   "800x480@60Hz"
	  
#define DP_EARLY_CONFIG_DISPLAY_BC_MODE              (tS32) 1
#define DP_EARLY_CONFIG_DISPLAY_LF_MODE              (tS32) 1

#define DP_FILE_BACKUP_NOR_PDDDPTESTLOCATIONRAWNOR5   "/var/opt/bosch/persistent/pdd/backup/nor/PddDpTestLocationRawNor5.tmp"
#define DP_ELEMENT_TR_BANKKDS_SENSOR_CONFIG_GYRO_ITEM_TAG_NOINIT_CONFIG_STRING   "TrBankKDSSensorConfigGyroItemTagNoInitConfigTString"

//for message queue
#define MQ_DP_MP_CONTROL_MAX_MSG     200
#define MQ_DP_MP_CONTROL_MAX_LEN     sizeof(TPoolMsg)

//for test process
#define PRIORITY             200
#define INVALID_PID          ((tS32)-100)

#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#define DP_TESTVALUE_TU32  0x22222222
#define DP_TESTVALUE_TU16  0x2222
#define DP_TESTVALUE_TU8   0x22
#define DP_TESTVALUE_FLAG  FALSE
#define DP_TESTVALUE_STR   "alles anders"
#define DP_TESTVALUE_STR_4 "user 4 anders"

#define DP_ACCESS_ID_SPM           0
#define DP_ACCESS_ID_END_USER_APP  (tU16)0x00D8  //CCA_C_U16_APP_FC_USERMANAGER
#endif

#define DP_MAX_TEST_PROCESS   6

/*********************************************************************************/
/*                         macro                                                 */
/*********************************************************************************/

/*********************************************************************************/
/*                         static variable                                       */
/*********************************************************************************/
tBool                   VbInit=FALSE;
tBool                   VbStartProcess=FALSE;
tBool                   VbSetElementDone=FALSE;
OSAL_tMQueueHandle      VmqHandleOEDTProcess[DP_MAX_TEST_PROCESS];
OSAL_tMQueueHandle      VmqHandleTestProcess[DP_MAX_TEST_PROCESS];
tBool                   vbTestCallback;
OSAL_tProcessID         vprID[DP_MAX_TEST_PROCESS];

/*********************************************************************************/
/*                        declaration static function                            */
/*********************************************************************************/
static tU32 u32KDSClearEntry(tU16 Pu16Entry);
static tU32 u32DPSetUserDefault_CurrentUserDifferent(tU8 Pu8CurrentUserChange);
static tU32 u32DPSetGetTestArrayGreat(tU32 Pu32Size);

/*********************************************************************************/
/*                        static function                                        */
/*********************************************************************************/
static void vDPInit(void)
{
  if(VbInit==FALSE)
  {
    tU8    Vu8Inc;
    ET_TRACE_OPEN; 
    #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE 
    vSetInitCallback_EFFD();
    #endif
    #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
    u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
    u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
    #endif
    for (Vu8Inc=0;Vu8Inc<DP_MAX_TEST_PROCESS;Vu8Inc++)
	  { /*set id to invalid*/
      vprID[Vu8Inc]=INVALID_PID;
    }
	  VbInit=TRUE;

  }
}
static tU32 u32DPProcessTest(ECmdTypes PeCmd,const char* PpElementName,const char* PpPoolName, tU8 PubEndUser)
{   
   tU32                     Vu32Ret = 0;
   TPoolMsg                 VtMsg;
   OSAL_trProcessAttribute	VprAtr 	  = {OSAL_NULL};
   tU8                      Vu8Process=0;  //testprocess1

   //check, which process should start
   if(PeCmd>eCmdProcess5End)  
      Vu8Process=5;                        //testprocess6
   else if(PeCmd>eCmdProcess4End)  
      Vu8Process=4;                        //testprocess5
   else if(PeCmd>eCmdProcess2End)  
     Vu8Process=2;                         //testprocess3
   else if(PeCmd>eCmdProcess1End)  
     Vu8Process=1;                         //testprocess2

   //create message queue and start all processes
   if(VbStartProcess==FALSE)
   { 
     tU8    Vu8Inc;
     for (Vu8Inc=0;Vu8Inc<DP_MAX_TEST_PROCESS;Vu8Inc++)
	   { // Create and open MQ for both processes to send their responses    
	     tCString VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS_NAME;
	     tCString VstrNameTestProcess=MQ_DP_TEST_PROCESS_NAME;
	     if(Vu8Inc==1)
	     {
	       VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS2_NAME;
         VstrNameTestProcess=MQ_DP_TEST_PROCESS2_NAME;
	     }
       if(Vu8Inc==2)
       {
         VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS3_NAME;
         VstrNameTestProcess=MQ_DP_TEST_PROCESS3_NAME;
       }
       if(Vu8Inc==3)
       {
         VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS4_NAME;
         VstrNameTestProcess=MQ_DP_TEST_PROCESS4_NAME;
       }
       if(Vu8Inc==4)
       {
         VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS5_NAME;
         VstrNameTestProcess=MQ_DP_TEST_PROCESS5_NAME;
       }
       if(Vu8Inc==5)
       {
         VstrNameOEDTProcess=MQ_DP_OEDT_PROCESS6_NAME;
         VstrNameTestProcess=MQ_DP_TEST_PROCESS6_NAME;
       }
       if (OSAL_s32MessageQueueCreate(VstrNameOEDTProcess,MQ_DP_MP_CONTROL_MAX_MSG, MQ_DP_MP_CONTROL_MAX_LEN,OSAL_EN_READWRITE, &VmqHandleOEDTProcess[Vu8Inc]) == OSAL_ERROR)
       {
         if (OSAL_s32MessageQueueOpen(VstrNameOEDTProcess,OSAL_EN_READWRITE, &VmqHandleOEDTProcess[Vu8Inc]) == OSAL_ERROR)
         {
           Vu32Ret=1000;
         }
       }
	     if (OSAL_s32MessageQueueCreate(VstrNameTestProcess,MQ_DP_MP_CONTROL_MAX_MSG, MQ_DP_MP_CONTROL_MAX_LEN,OSAL_EN_READWRITE, &VmqHandleTestProcess[Vu8Inc]) == OSAL_ERROR)
       {
         if (OSAL_s32MessageQueueOpen(VstrNameTestProcess,OSAL_EN_READWRITE, &VmqHandleTestProcess[Vu8Inc]) == OSAL_ERROR)
         {
           Vu32Ret=1000;
         }
       }
     }
     //start process
     if(Vu32Ret==0)
     { 
	     VprAtr.szCommandLine  = NULL;
	     VprAtr.u32Priority    = PRIORITY;
	     //start process 1
	     VprAtr.szName="procdatapool1_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool1_out.out";       
       vprID[0] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[0])
       {
         Vu32Ret=2000;
       }	  
	     //start process 2
	     VprAtr.szName="procdatapool2_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool2_out.out";     
       vprID[1] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[1])
       {
         Vu32Ret|=4000;
       }
       //start process 3
	     VprAtr.szName="procdatapool3_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool3_out.out";     
       vprID[2] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[2])
       {
         Vu32Ret|=4000;
       }      
       //start process 4
	     VprAtr.szName="procdatapool4_name_greater_32bytes_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool4_name_greater_32bytes_out.out";     
       vprID[3] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[3])
       {
         Vu32Ret|=4000;
       }
       //start process 5
	     VprAtr.szName="procdatapool5_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool5_out.out";     
       vprID[4] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[4])
       {
         Vu32Ret|=4000;
       }	    
       //start process 6
	     VprAtr.szName="procdatapool6_out.out";
	     VprAtr.szAppName="/opt/bosch/base/bin/procdatapool6_out.out";     
       vprID[5] = OSAL_ProcessSpawn(&VprAtr);
       if (OSAL_ERROR == vprID[5])
       {
         Vu32Ret|=4000;
       }
	     //check error
	     if(Vu32Ret==0)
	     {
	       VbStartProcess=TRUE;  
		     sleep(5);
       }
     }   
   }
   //post message
   if(Vu32Ret==0)
   {     
	   VtMsg.eCmd=PeCmd;
	   if(PpElementName!=NULL)
       OSAL_szStringCopy(VtMsg.u.tShared.strElement,PpElementName);
     if(PpPoolName!=NULL)
       OSAL_szStringCopy(VtMsg.u.tShared.strPool,PpPoolName);
	   if(Vu8Process==1)
	   {
	     VtMsg.u.u8EndUser=PubEndUser;
	   }
	   //fprintf(stderr, "u32DPProcessTest() post message %d \n",PeCmd);
     if (OSAL_s32MessageQueuePost(VmqHandleTestProcess[Vu8Process], (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
        Vu32Ret = 5000;
     }	
   }
   //wait answer
   if(Vu32Ret==0)
   {
     tU32 u32Prio;
     if(OSAL_s32MessageQueueWait(VmqHandleOEDTProcess[Vu8Process], (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	   {
       Vu32Ret = 6000;
	   }
	   else
	   {
	     if(VtMsg.eCmd==eCmdResponse)
       {
	       sleep(2); 
	       Vu32Ret |= VtMsg.u.u32ErrorCode;		
	       //fprintf(stderr, "u32DPProcessTest() wait done %d \n",VtMsg.eCmd);		 
	     }
	     else
	     {
	       Vu32Ret=8000;
	       //fprintf(stderr, "u32DPProcessTest() wrong message %d \n",VtMsg.eCmd);
	     }
     }
   }
   return(Vu32Ret);
}

#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
//set end user
static tU32 u32DPSetEndUser(tU8 Pu8EndUser)
{
  tU32 Vu32ErrorCode=0;
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100000000;
  }
  else
  { /*set end user*/
	  if(DP_s32SetEndUser(Pu8EndUser,DP_ACCESS_ID_END_USER_APP)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200000000;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=400000000;
	  }
  }
  return(Vu32ErrorCode);
}

//set end user
static tS32 s32DPCopyEndUser(tU8 Pu8EndUserFrom,tU8 Pu8EndUserTo)
{
  tS32 Vs32ErrorCode=DP_S32_NO_ERR;
  /* lock*/
  Vs32ErrorCode=DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000);
  if(Vs32ErrorCode==DP_S32_NO_ERR)  
  { /*set end user*/
	  Vs32ErrorCode=DP_s32CopyEndUser(Pu8EndUserFrom,Pu8EndUserTo,DP_ACCESS_ID_END_USER_APP,5000);
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000);
  }
  return(Vs32ErrorCode);
}
#endif

/****************************************************************************************************/
/*             test for end user feature banking                                                    */
/****************************************************************************************************/
#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
//helper function for load bank to current user
static tU32 u32OEDTDPLoadBankToCurrentEndUser(tU8 Vu8BankLoad)
{
  tU32                 Vu32ErrorCode=0;
  /* load bank to current user*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000000;
  }
  else
  { 
	  if(DP_s32LoadBankToCurrentEndUser(Vu8BankLoad,DP_ACCESS_ID_END_USER_APP,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20000000;
	  }	
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40000000;
    }
  }  
  return(Vu32ErrorCode);
}
//helper function for save current to bank
static tU32 u32OEDTDPSaveCurrentEndUserToBank(tU8 Vu8Bank)
{
  tU32                 Vu32ErrorCode=0;
  /* load bank to current user*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100000000;
  }
  else
  { 
	  if(DP_s32SaveCurrentEndUserToBank(Vu8Bank,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200000000;
	  }
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=400000000;
    }
  }  
  return(Vu32ErrorCode);
}

//helper function for set bank to default
static tU32 u32OEDTDP_s32SetBankDefault(tU8 Vu8Bank)
{
  tU32                 Vu32ErrorCode=0;
  /* load bank to current user*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=1000000;
  }
  else
  { 
	  if(DP_s32SetBankDefault(Vu8Bank,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2000000;
	  }	
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=4000000;
    }
  }  
  return(Vu32ErrorCode);
}
#endif
#endif

#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
//helper function for set all users and banks 
static tU32 u32OEDTDP_s32SetAllUsersAndBanks(void)
{
  tU32    Vu32ErrorCode=0;
  tU8     Vu8Bank;
  tU8     Vu8User;
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tU16    Vu16Value=DP_TESTVALUE_TU16;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankTrTestEndUserBankArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedtU32    VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedString  VmyTestElementSharedStr;
  //for all user (4)
  for (Vu8User=0;Vu8User<4;Vu8User++)
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8User);
	  //for all banks
	  for(Vu8Bank=0;Vu8Bank<4;Vu8Bank++)
	  {
	    if(Vu32ErrorCode==0)
      {
	      if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=1;
        }
	      if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=2;
	      }
	      if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=4;
	      }
	      if(VmyTestElementSharedU32.s32SetData(Vu32Value)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=8;
	      }
	      if(VmyTestElementSharedArray.s32SetData(&Vu16Value,1)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=10;
	      }
	      memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
	      if(VmyTestElementSharedStr.s32SetData(VStr)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=20;
	      }
		    Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(Vu8Bank);
      }
    }
  }
  return(Vu32ErrorCode);
}
//helper function check default value bank 2 
static tU32 u32OEDTDP_s32CheckDefaultBank2(void)
{
  tU32    Vu32ErrorCode=0;
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tU16    Vu16Value=DP_TESTVALUE_TU16;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankTrTestEndUserBankArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedtU32    VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedString  VmyTestElementSharedStr;
  /*get default value and compare*/
  if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
  {
    Vu32ErrorCode|=40; 
  }
  else if(Vu32Value!=2)
  {
    Vu32ErrorCode|=80;
  }
  if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100;
  }
  else if(VbValue!=TRUE)
  {
    Vu32ErrorCode|=200;
  }
  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=800;
  }
  else if(Vu8Value!=12)
  {
    Vu32ErrorCode|=1000;
  }
  if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=2000;
  }
  else if(Vu32Value!=0x33333333)
  {
    Vu32ErrorCode|=4000;
  }
  if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=1) 
  {
    Vu32ErrorCode|=8000;
  }
  else if(Vu16Value!=0x5555)
  {
    Vu32ErrorCode|=4000;
  }
  if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000;
  }
  else if(memcmp(VStr,"test3",strlen("test3"))!=0)
  {
    Vu32ErrorCode|=20000;
  }
  return(Vu32ErrorCode);
}
#endif
#endif
static tU32 u32KDSClearEntry(tU16 Pu16Entry)
{
  OSAL_tIODescriptor VhDevice = 0;
  tU32               Vu32Ret = 0;
  tBool              VbAccessOption = 0;
  tsKDSEntry         VrEntry;

  memset(&VrEntry,0,sizeof(tsKDSEntry));
  VhDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret = 1000;
  }
  else
  {
    VbAccessOption = 1;
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)VbAccessOption) == OSAL_ERROR)
    {
      Vu32Ret = 2000;
    }
    else
    {
      VrEntry.u16Entry = Pu16Entry;
      /* delete entry, if exist */ 
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY,VrEntry.u16Entry)== OSAL_ERROR)
      {//read entry
        memset(VrEntry.au8EntryData,0, sizeof(VrEntry.au8EntryData));
        if(OSAL_s32IORead(VhDevice,(tS8 *)&VrEntry,(tS32)sizeof(VrEntry)) != OSAL_ERROR)
        {
          Vu32Ret = 4000;      
        }
      }
    }
    if(OSAL_s32IOClose(VhDevice ) == OSAL_ERROR)
    {
      Vu32Ret |= 8000;
    }
  }
  return(Vu32Ret);
}
static tU32 u32KDSWriteEntry(tU16 Pu16Entry,tU8* PubBuffer,tU16 Pub16len)
{
  OSAL_tIODescriptor VhDevice = 0;
  tU32               Vu32Ret = 0;
  tBool              VbAccessOption = 0;
  tsKDSEntry         VrEntry;

  memset(&VrEntry,0,sizeof(tsKDSEntry));
  VhDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret = 1000;
  }
  else
  {
    VbAccessOption = 1;
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)VbAccessOption) == OSAL_ERROR)
    {
      Vu32Ret = 2000;
    }
    else
    {
      VrEntry.u16Entry = Pu16Entry;   
      VrEntry.u16EntryLength = Pub16len;
      memcpy(VrEntry.au8EntryData,PubBuffer,Pub16len);
      if(OSAL_s32IOWrite(VhDevice,(tS8 *)&VrEntry,(tS32)sizeof(VrEntry)) == OSAL_ERROR)
      {
        Vu32Ret = 3000;
      }
    }
    if(OSAL_s32IOClose(VhDevice ) == OSAL_ERROR)
    {
      Vu32Ret |= 8000;
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetElementWithoutClassAccess()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.03.2014
******************************************************************************/
tU32 u32DPSetGetElementWithoutClassAccess(void)
{
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_TESTPOOL 
  dp_tclBaseElement myDpElem("testAcc0066ImmStr", (tS32)0x434e095fL);
  tChar                                            VstrStringRead[32];
  memset(VstrStringRead, 0, 32);
  strncpy(VstrStringRead,"test",32);
  (tVoid)myDpElem.bFillData((tVoid*)VstrStringRead, (tU32)(strlen(VstrStringRead)));
  if (DP_s32SetElement(DP_U32_POOL_ID_TESTPOOL, &myDpElem, 0x4711)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=10;
  }
  else
  {
    memset(VstrStringRead, 0, 32);
    if (DP_s32GetElement(DP_U32_POOL_ID_TESTPOOL, &myDpElem, 0x4711)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=100;
    }
    else
    {
      /*check value*/
      tS32 s32Len = (tS32)myDpElem.u32GetDataLength();
      if(s32Len!=strlen("test"))
      {
        Vu32ErrorCode=(tU32)s32Len;
      }
      else
      {
        if(memcmp(myDpElem.pvGetData(),"test",s32Len)!=0)
        {
          Vu32ErrorCode=1000;
        }
      }
    }
  }
   #endif 
   #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR1
   if (Vu32ErrorCode==0)
   {
     tU8  Vu8Array[10];  
     tU32 Vu32ArraySize=10;
     memset(Vu8Array,0x55,10);
     dp_tclBaseElement myDpElem2("TrTestRawNor11", (tS32)0x9a924e69L);
     (tVoid)myDpElem2.bFillData((tVoid*)Vu8Array, sizeof(tU8)*Vu32ArraySize);
     if(DP_s32SetElement(DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR1, &myDpElem2, DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR1 >> 16)!=DP_S32_NO_ERR)
     {
       Vu32ErrorCode=2000;
     }
     //read   
     else if (DP_s32GetElement(DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR1, &myDpElem2, DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR1 >> 16)!=DP_S32_NO_ERR)
     {
       Vu32ErrorCode=2100;
     }
     else
     {
       /*check value*/
       if (myDpElem2.u32GetDataLength()!=Vu32ArraySize)
       {
         Vu32ErrorCode=2200;
       }
       else
       {
         if(memcmp(myDpElem2.pvGetData(),&Vu8Array[0],Vu32ArraySize)!=0)
         {
           Vu32ErrorCode=2300;
         }
       }
     }
   }
  #endif

  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefaultValueTrue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.03.2014
******************************************************************************/
tU32 u32DPGetDefaultValueTrue(void)
{
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  //first delete file from file system
  pdd_delete_data_stream("effd/PddDpTestLocationFile",PDD_LOCATION_FS);
  {
    tBool                                               VbTest;
    dp_tclPddDpTestLocationFileTrTestFileFlag           VElement;
    if(VElement.s32GetData(VbTest)==DP_S32_NO_ERR)
    {
      if(VbSetElementDone==FALSE) //array not set in other test
	    {
        if(VbTest==FALSE)
	      {
	        Vu32ErrorCode=20;
	      }
	    }
    }
    else
    {
      Vu32ErrorCode=10;
    }
  }
  #endif  
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefaultValueAndState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   5.02.2015
******************************************************************************/
tU32 u32DPGetDefaultValueAndState(void)
{
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE  
  //first delete file from file system
  pdd_delete_data_stream("effd/PddDpTestLocationFile",PDD_LOCATION_FS);
  { //read value 
	  tBool                                               VbTest;
	  tU32                                                Vu32Value;
	  char                                                VStr[100];
    dp_tclPddDpTestLocationFileTrTestFileFlag           VElement;
	  dp_tclPddDpTestLocationFileTrTestFile1              VElementArray;
	  dp_tclPddDpTestLocationFileTrTestArrayNoInit        VElementArrayNoInit;
	  dp_tclPddDpTestLocationFileTrTestFileString         VMyStrElement;

	  //----------- type element ----------------------
	  if(VElement.s32GetData(VbTest)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  else
	  { 
	    if(VbSetElementDone==FALSE) //array not set in other test
	    { //check default value
	      if(VbTest==FALSE)
	      {
	        Vu32ErrorCode|=4;
	      }
	      else
	      { //check element state
	        if(VElement.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		      {
		        Vu32ErrorCode|=8;
		      }
        }
      }
    }
	  //-------------- string element -------------------------
	  if (VMyStrElement.s32GetData((tString)VStr,sizeof(VStr)) <= DP_S32_NO_ERR) 
	  {
      Vu32ErrorCode|=10;
    }
	  else
	  {
	    if(VbSetElementDone==FALSE) //array not set in other test
	    {
	      if(memcmp(VStr,"test's forever",strlen(VStr))!=0)
	      {
	        Vu32ErrorCode|=20;
	      }
	      else
	      { 
	        if(VbSetElementDone==FALSE) //array not set in other test
	        { //check element state
	          if(VMyStrElement.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		        {
		          Vu32ErrorCode|=40;
		        }
          }
        }
      }
    }
	  // ------------------ array element -----------------------
	  if (VElementArray.s32GetData(&Vu32Value,1)!=1) //return size default 1
    {
      Vu32ErrorCode|=100;
    }
	  else
	  {
	    if(VbSetElementDone==FALSE) //array not set in other test
	    { //check default value
	      if(Vu32Value!=0)
	      {
	        Vu32ErrorCode|=200;
	      } 
	      else
	      { //check element state
	        if(VElementArray.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		      {
		        Vu32ErrorCode|=400;
          }
        }
      }
    }
	  // ------------------ array element -----------------------
	  tU8 Vu8Value[20];
	  if (VElementArrayNoInit.s32GetData(&Vu8Value[0],20)!=0) //return size default 0 
    {
      Vu32ErrorCode|=1000;
    }
	  if(VElementArray.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
	  {
	    Vu32ErrorCode|=2000;
	  }
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetChangedValueAndState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   06.02.2015
******************************************************************************/
tU32 u32DPGetChangedValueAndState(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  tBool                                               VbTest;
	tU32                                                Vu32Value;
	char                                                VStr[100];
  dp_tclPddDpTestLocationFileTrTestFileFlag           VElement;
	dp_tclPddDpTestLocationFileTrTestFile1              VElementArray;
	dp_tclPddDpTestLocationFileTrTestFileString         VMyStrElement;

	//----------- type element ----------------------
	VbTest=FALSE;
	if(VElement.s32SetData(VbTest)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=2;
	}
	else
	{ //get value
	  if(VElement.s32GetData(VbTest)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4;
	  }
	  else
	  { //check element state
	    if(VElement.u8GetElementStatus()!=DP_U8_ELEM_STATUS_VALID)
		  {
		    Vu32ErrorCode=VElement.u8GetElementStatus();
		  }
    }
	}
	//-------------- string element -------------------------
	memcpy(VStr,"test string",strlen("test string")+1);
	if (VMyStrElement.s32SetData(VStr) != DP_S32_NO_ERR) 
	{
    Vu32ErrorCode|=10;
	}	
	else
	{//get value
	  if (VMyStrElement.s32GetData((tString)VStr,sizeof(VStr)) <= DP_S32_NO_ERR) 
	  {
	    Vu32ErrorCode|=20;
	  }
	  else
	  { //check element state
	    if(VMyStrElement.u8GetElementStatus()!=DP_U8_ELEM_STATUS_VALID)
		  {
		    Vu32ErrorCode|=40;
      }
    }
	}
	// ------------------ array element -----------------------
	Vu32Value=1234;
	if (VElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR) //return size default 1
  {
    Vu32ErrorCode|=100;
  }
	else
	{//getvalue
	  if (VElementArray.s32GetData(&Vu32Value,1)!=1) //return size default 1
	  {
	    Vu32ErrorCode|=200;
	  } 
	  else
	  { //check element state
	    if(VElementArray.u8GetElementStatus()!=DP_U8_ELEM_STATUS_VALID)
	    {
		    Vu32ErrorCode|=400;
      }
    }
	}
  VbSetElementDone=TRUE; //set for other test to indicates that element are load and set  
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetConfig()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32DPSetConfig(void)
{
  tU32 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    tU32 Vu32AccessId=0;  /*SPM*/
    tU32 Vu32PoolId=DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE;
    tU32 Vu32Timeout=0x3000;

    if (DP_s32SetConfig(Vu32PoolId, Vu32AccessId,Vu32Timeout)<0)
    {
      Vu32ErrorCode=20;
    }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPGetSetCodeDataAndSetConfig()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32DPGetSetCodeDataAndSetConfig(void)
{
  tU32                                       Vu32ErrorCode=0;
  vDPInit(); 
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    tU32                                       Vu32AccessId=0;  /*SPM*/
    tU32                                       Vu32PoolId=DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE;
    tU32                                       Vu32Timeout=3000;
    tU32                                       Vu32Value;
    dp_tclPddDpTestLocationFileTrTestFile1     VoElements;
 
    if (VoElements.s32GetData(&Vu32Value,1)<DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
    else
    {
      Vu32Value+=1;
      if (DP_S32_NO_ERR != VoElements.s32SetData(&Vu32Value,1))
      {
        Vu32ErrorCode|=20;
      }
      else
      {
        if (DP_s32SetConfig(Vu32PoolId, Vu32AccessId,Vu32Timeout)<0)
        {
          Vu32ErrorCode|=40;
	      }
        else 
	      {
	        Vu32Value+=1;
	        if (DP_S32_NO_ERR != VoElements.s32SetData(&Vu32Value,1))
          {
            Vu32ErrorCode|=80;
          }
	        else if (DP_s32SetConfig(Vu32PoolId, Vu32AccessId,Vu32Timeout)<0)
          {
            Vu32ErrorCode|=100;
	        }
		      VbSetElementDone=TRUE; //set for other test to indicates that element are load and set 
        }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPAddNotificationAndSetConfig()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  14.12.2015
******************************************************************************/
static tVoid datapool_callback(void)
{
  //fprintf(stderr, "test: datapool_callback()\n");  
  vbTestCallback=1;
}

tU32 u32DPAddNotificationAndSetConfig(void)
{
  tU32                                       Vu32ErrorCode=0;
  vDPInit(); 
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE  
  tU32                                       Vu32AccessId=0;  /*SPM*/
  tU32                                       Vu32PoolId=DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE;
  tU32                                       Vu32Timeout=3000;
  tU32                                       Vu32Value=0x12345; 
  dp_tclPddDpTestLocationFileTrTestFile1     VoElements;

  //get data
  if (VoElements.s32GetData(&Vu32Value,1)<DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  //add notification
  if(VoElements.bAddNotification((tVoid*)&datapool_callback,123)!=TRUE) //lint !e611: interface
  {
    Vu32ErrorCode=2;
  }
  else 
  { 
    vbTestCallback=0;
    Vu32Value+=1;
    if (DP_S32_NO_ERR != VoElements.s32SetData(&Vu32Value,1))
    {
      Vu32ErrorCode=4;
    }
    else 
    {
      sleep(1);
      if (DP_s32SetConfig(Vu32PoolId, Vu32AccessId,Vu32Timeout)<0)
      {
        Vu32ErrorCode=8;
	    }
      if(vbTestCallback==0)
      {
        Vu32ErrorCode=10;
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPGetSetCodeDataAndSetConfig()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32DPSetPoolV850(void)
{
  tU32                                           Vu32ErrorCode=0;
  vDPInit(); 
#ifdef DP_U32_POOL_ID_PDDDPTEST
  {
    /*------ test only works if the version of the pool matches  23.02.2016; therefore write data to the pool with the version  */
    TDataPoolScc_PddDpTest  VStructDataWrite;
	  if(pdd_write_data_stream(PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),DATAPOOL_SCC_VERSION_POOL_PDDDPTEST)>=0)
    {      
      TDataPoolScc_PddDpTest                         VStructData;
      TDataPoolScc_PddDpTest                         VStructDataRead;
      dp_tclPddDpTestPddDpTestElement1               VoElements1;
      dp_tclPddDpTestPddDpTestElement2               VoElements2; 
      {
	      memset(&VStructData,0,sizeof(VStructData));
	      memset(&VStructData.PddDpTestElement1.PddDpTestElement1[0],0x55,4);
	      memset(&VStructData.PddDpTestElement2.PddDpTestElement2[0],0xAA,4);
	      if (VoElements1.s32SetData(&VStructData.PddDpTestElement1.PddDpTestElement1[0],4)<DP_S32_NO_ERR)
        {
          Vu32ErrorCode=20;
        }
	      /*read data and compare*/
	      if (VoElements1.s32GetData(&VStructDataRead.PddDpTestElement1.PddDpTestElement1[0],4)<DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=80;	
        }
	      else
	      {/*compare*/
          if(memcmp(&VStructData.PddDpTestElement1.PddDpTestElement1[0], &VStructDataRead.PddDpTestElement1.PddDpTestElement1[0],4)!=0)
	        {
	          Vu32ErrorCode|=100;	
	        }
        }
	      if (VoElements2.s32SetData(&VStructData.PddDpTestElement2.PddDpTestElement2[0],4)<DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=200;
        }
	      /*read data and compare*/
	      if (VoElements2.s32GetData(&VStructDataRead.PddDpTestElement2.PddDpTestElement2[0],4)<DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=800;	
        }
	      else
	      {/*compare*/
          if(memcmp(&VStructData.PddDpTestElement2.PddDpTestElement2[0], &VStructDataRead.PddDpTestElement2.PddDpTestElement2[0],4)!=0)
	        {
	          Vu32ErrorCode|=1000;	
	        }
        }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPSetGetTestValueU8()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.03.2014
******************************************************************************/
tU32 u32DPSetGetTestValueU8(void)
{
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    tU8                                                 Vu8Value;
    dp_tclPddDpTestLocationFileTrTestFiletU8            VElement;
    
    Vu8Value=5;
    if(VElement.s32SetData(Vu8Value)==DP_S32_NO_ERR)
    {
      Vu8Value=0;
      if(VElement.s32GetData(Vu8Value)==DP_S32_NO_ERR)
	    {
	      if(Vu8Value!=5)
	      {
		      Vu32ErrorCode=(tU32)Vu8Value;
        }
      }
	    else
	    {
	      Vu32ErrorCode=20;
	    }
    }
    else
    {
      Vu32ErrorCode=10;
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetTestArrayGreat4k()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.05.2016
******************************************************************************/
tU32 u32DPSetGetTestArrayGreat4k(void)
{  
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  Vu32ErrorCode=u32DPSetGetTestArrayGreat(4000);  
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetTestArray2Great2k()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.05.2016
******************************************************************************/
tU32 u32DPSetGetTestArrayGreat2k(void)
{  
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  Vu32ErrorCode=u32DPSetGetTestArrayGreat(2000);  
  return(Vu32ErrorCode);
}

static tU32 u32DPSetGetTestArrayGreat(tU32 Pu32Size)
{
   tU32                                                Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    dp_tclPddDpTestLocationFileTrTestFile2            VElement;
    tU8*                                              Vu8Buffer;
    tU8*                                              Vu8BufferRead;
    tU32                                              Vu32SizeRead=Pu32Size*2;
    
    Vu8Buffer=(tU8*) malloc(Pu32Size);
    Vu8BufferRead=(tU8*) malloc(Vu32SizeRead);
    if((Vu8Buffer==NULL)||(Vu8BufferRead==NULL))
    {
      Vu32ErrorCode=10;
    }
    else
    {
      memset(Vu8Buffer,0x3e,Pu32Size);
      if(VElement.s32SetData(Vu8Buffer,Pu32Size)==DP_S32_NO_ERR)
      {      
        tS32 Vs32Size=VElement.s32GetData(Vu8BufferRead,Vu32SizeRead);
        if(Vs32Size>=DP_S32_NO_ERR)
	      {
	        if(Vs32Size!=(tS32)Pu32Size)
          {
            Vu32ErrorCode=20;
          }
          else
          {
            if(memcmp(Vu8Buffer,Vu8BufferRead,Pu32Size)!=0)
            {
              Vu32ErrorCode=40;
            }
          }
        }
	      else
	      {
	        Vu32ErrorCode=60;
	      }
      }
      else
      {
        Vu32ErrorCode=80;
      }
      free(Vu8Buffer);
      free(Vu8BufferRead);
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetTestStructureArray()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.03.2014
******************************************************************************/
tU32 u32DPSetGetTestStructureArray(void)
{
  tU32                                                Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    tsStructureString                                   VtsStructSet[2];
    tsStructureString                                   VtsStructGet[10];
    dp_tclPddDpTestLocationFileTrTestStructure          VElement;
    
    memset(VtsStructSet,0,sizeof(VtsStructSet));
    memcpy(&VtsStructSet[0].name1[0],"name1",strlen("name1"));
    memcpy(&VtsStructSet[0].name2[0],"name2long",strlen("name2long"));
     
    if(VElement.s32SetData(VtsStructSet,1)==DP_S32_NO_ERR)
    {      
      tS32 Vs32Size=VElement.s32GetData(VtsStructGet,10);
      if(Vs32Size>=DP_S32_NO_ERR)
	    {
	      if(Vs32Size!=1)
        {
          Vu32ErrorCode=10;
        }
        else
        {
          if(strncmp(VtsStructGet[0].name1,VtsStructSet[0].name1,MAX_NAME1_LEN)!=0)
          {
            Vu32ErrorCode|=20;
          }
          if(strncmp(VtsStructGet[0].name2,VtsStructSet[0].name2,MAX_NAME2_LEN)!=0)
          {
            Vu32ErrorCode|=40;
          }
        }
      }
	    else
	    {
	      Vu32ErrorCode=60;
	    }
    }
    else
    {
      Vu32ErrorCode=80;
    }
  }
  #endif
  return(Vu32ErrorCode);
}


/*****************************************************************************
* FUNCTION:		u32DPSetToDefaultTestValueU8()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   11.09.2014
******************************************************************************/
tU32 u32DPSetToDefaultTestValueU8(void)
{
  tU32                                            Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  {
    tU32                                          Vu32AccessId=0;  /*SPM*/
    tU32                                          Vu32Timeout=8000;
	  tU8                                           Vu8Value;
    dp_tclPddDpTestLocationFileTrTestFiletU8      VElement;

    //set to default
    DP_s32SetDefault(Vu32AccessId,DP_DEFSET_USER,Vu32Timeout);

    //get value
    if(VElement.s32GetData(Vu8Value)==DP_S32_NO_ERR)
	  { /*value default?*/
	    if(Vu8Value!=9)
	    {
		    Vu32ErrorCode=(tU32)Vu8Value;
	    }
	  }
	  else
	  {
	    Vu32ErrorCode=20;
	  }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEarlyConfigDisplaySet()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPEarlyConfigDisplaySet(void)
{
  tU32                                      Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTEST
  dp_tclEarlyConfigDisplayTrResolution      VoTrResolution;
  dp_tclEarlyConfigDisplayTrBackwardComp    VoTrBC;
  dp_tclEarlyConfigDisplayTrLowFrequency    VoTrLF;
  char                                      Vstr[100];

  //set resolution string
  memcpy(Vstr,DP_EARLY_CONFIG_DISPLAY_RESOLTUTION_STRING,strlen(DP_EARLY_CONFIG_DISPLAY_RESOLTUTION_STRING)+1);
  if(VoTrResolution.s32SetData(Vstr)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //set backward compatibility
  if(VoTrBC.s32SetData(DP_EARLY_CONFIG_DISPLAY_BC_MODE)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  //set low frequency mode
  if(VoTrLF.s32SetData(DP_EARLY_CONFIG_DISPLAY_LF_MODE)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEarlyConfigDisplaySet()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPEarlyConfigDisplayGet(void)
{
  tU32                                      Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTEST
  dp_tclEarlyConfigDisplayTrResolution      VoTrResolution;
  dp_tclEarlyConfigDisplayTrBackwardComp    VoTrBC;
  dp_tclEarlyConfigDisplayTrLowFrequency    VoTrLF;
  tS32                                      Vs32BCModeRead=-2;
  tS32                                      Vs32LFModeRead=-2;
  tChar                                     VstrResolutionString[32];                                  
 
  // get backward compatibility
  if(VoTrBC.s32GetData(Vs32BCModeRead)==DP_S32_NO_ERR)
  {
	if(Vs32BCModeRead!=DP_EARLY_CONFIG_DISPLAY_BC_MODE)
	{
	  Vu32ErrorCode|=10;
	}
  }
  else
  {
    Vu32ErrorCode|=20;
  }
   // low frequency mode
  if(VoTrLF.s32GetData(Vs32LFModeRead)==DP_S32_NO_ERR)
  {
    if(Vs32LFModeRead!=DP_EARLY_CONFIG_DISPLAY_LF_MODE)
    {
      Vu32ErrorCode|=40;
    }
  }
  else
  {
    Vu32ErrorCode|=80;
  }
  // get resoltion string
  if(VoTrResolution.s32GetData(VstrResolutionString,sizeof(VstrResolutionString)) >= DP_S32_NO_ERR)
  {
    if(strcmp(VstrResolutionString,DP_EARLY_CONFIG_DISPLAY_RESOLTUTION_STRING)!=0)
    {
      Vu32ErrorCode|=100;
    }
  }
  else
  {
    Vu32ErrorCode|=200;
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEarlyConfigDisplaySet()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPEarlyConfigCSCSetGet(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTEST
  #ifdef DP_U32_POOL_ID_EARLYCONFIGCSC
  dp_tclSrvIf                                   VdpSrvIf;
  dp_tclEarlyConfigCSCTrCurrentProfile          VrMyElemCurrentProfile;
  dp_tclEarlyConfigCSCTrHueValue                VrMyElemHueValue;
  dp_tclEarlyConfigCSCTrContrastValue           VrMyElemContrastValue;
  dp_tclEarlyConfigCSCTrSaturationValue         VrMyElemSaturationValue;
  dp_tclEarlyConfigCSCTrBrightnessValue         VrMyElemBrightnessValue;
  dp_tclEarlyConfigCSCTrHueOffsetValue          VrMyElemHueOffsetValue;
  dp_tclEarlyConfigCSCTrSaturationOffsetValue   VrMyElemSaturationOffsetValue;
  dp_tclEarlyConfigCSCTrBrightnessOffsetValue   VrMyElemBrightnessOffsetValue;
  dp_tclEarlyConfigCSCTrGammaFactor             VrGammaFactor;
  tF64                                          Vt64Value=1.23456+(tF64) (rand() % 256); //get random number
  tF64                                          Vt64ValueRead=0;
  tS16                                          Vs16Value=1234;
  tS16                                          Vs16ValueRead=0;
  tU8                                           Vu8Profil=3;
  tU8                                           Vu8ProfilRead=0;

  if(VrGammaFactor.s32SetData(Vt64Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  if(VrMyElemCurrentProfile.s32SetData(Vu8Profil)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=2;
  }
  if(VrMyElemHueValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=4;
  }
  if(VrMyElemContrastValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=8;
  }
  if(VrMyElemSaturationValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  if(VrMyElemBrightnessValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=12;
  }
  if(VrMyElemHueOffsetValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=14;
  }
  if(VrMyElemSaturationOffsetValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=18;
  }
  if(VrMyElemBrightnessOffsetValue.s32SetData(Vs16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  {/*save pool*/
    if(VdpSrvIf.s32StorePoolNow(DP_U32_POOL_ID_EARLYCONFIGCSC)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=200;
    }
    else
    {/*get value*/
      if(VrGammaFactor.s32GetData(Vt64ValueRead)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=400;
      }
      else if(Vt64ValueRead!=Vt64Value)
      {
        Vu32ErrorCode|=800;
      }
      if(VrMyElemCurrentProfile.s32GetData(Vu8ProfilRead)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=2000;
      }
      else if(Vu8ProfilRead!=Vu8Profil)
      {
        Vu32ErrorCode|=4000;
      }
      if(VrMyElemSaturationValue.s32GetData(Vs16ValueRead)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=2000;
      }
      else if(Vs16ValueRead!=Vs16Value)
      {
        Vu32ErrorCode|=4000;
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEarlyConfigDisplaySet()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetDefaultValueCheckIfStored(void)
{
  tU32  Vu32ErrorCode=0;
  tU32  Vu32DefaultValue=2222;
  vDPInit();
#ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONRAWNOR5
  dp_tclPddDpTestLocationRawNor5TrTestRawNor53 VoTrElement53;
  if(VoTrElement53.s32SetData(Vu32DefaultValue)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //read pool from backup file 
  intptr_t ViFile = open(DP_FILE_BACKUP_NOR_PDDDPTESTLOCATIONRAWNOR5, O_RDONLY);
  /* check file handle */
  if(ViFile < 0)
  { /*  error*/
    Vu32ErrorCode|=20;
  }
  else
  {
    struct stat VFileStat;
    if(fstat(ViFile,&VFileStat) < 0)
	  { /* error*/
        Vu32ErrorCode|=40;
	  }
    else
    { /* create buffer*/
       void* VvpBuffer=malloc(VFileStat.st_size);
       if(VvpBuffer==NULL)
       {
         Vu32ErrorCode|=80;
       }
       else
       {/*read file */
         tS32 Vs32Result = read(ViFile,VvpBuffer,VFileStat.st_size);		
         if(Vs32Result<0)
         {
           Vu32ErrorCode|=100;
         }
         else
         { /*search string in buffer*/
          void* VPos=memchr(VvpBuffer,'T',VFileStat.st_size);
          void* VPosOld=VvpBuffer;
          tU32  Vu32Size=(tU32)VFileStat.st_size;
          while(VPos!=NULL)
          {
            if(memcmp(VPos,"TrTestRawNor53",strlen("TrTestRawNor53"))!=0)
	          {
	            Vu32ErrorCode|=200;
	          }
	          else
	          {
              break;
	          }
	          Vu32Size-=((tU32)VPosOld-(tU32)VPos);
	          VPosOld=VPos;
            VPos = memchr(VPos,'T',Vu32Size);
          };
          if(VPos==NULL)
          {
            Vu32ErrorCode|=300;
          }
          free(VvpBuffer);
        }/*end else*/
      }/*end else*/
    }/*end else*/
	  if (close(ViFile)<0) 
    {
      Vu32ErrorCode|=400;
    }
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetSRAMAccess()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetGetSRAMAccess(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  dp_tclSpmDpInternDataUBatDropDetected oData;
  tBool tDataWrite=FALSE;
  tBool tDataRead=TRUE;
  oData.vSetData(tDataWrite);
  if(oData.u8GetData(tDataRead)==DP_U8_ELEM_STATUS_INVALID)
  {
    Vu32ErrorCode=10;
  }
  else if(tDataWrite!=tDataRead)
  {
    Vu32ErrorCode=20;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetSRAMAccessWriteAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   22.09.2015
******************************************************************************/
tU32 u32DPSetGetSRAMAccessWriteAll(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  dp_tclSpmDpInternDataSystemState             oDataSystemState;
  dp_tclSpmDpInternDataUBatDropDetected        oDataUBatDropDetected;
  dp_tclSpmDpInternDataIgnitionOffCount        oDataIgnitionOffCount;
  dp_tclSpmDpInternDataLastOsalElapsedTime     oDataLastOsalElapsedTime;
  dp_tclSpmDpInternDataLastVoltageLevel        oDataLastVoltageLevel;
  dp_tclSpmDpInternDataLastUbatSense           oDataLastUbatSense;
  dp_tclSpmDpInternDataLastWakeupReason        oDataLastWakeupReason;
  dp_tclSpmDpInternDataResetSetBySpm           oDataResetSetBySpm;
  dp_tclSpmDpInternDataContinuousResets        oDataContinuousResets;

  tU32  Vu32SystemState=0xaaaaaaaa;
  tBool VbUBatDropDetected=TRUE;
  tU32  Vu32IgnitionOffCount=0xbbbbbbbb;
  tU32  Vu32LastOsalElapsedTime=0xffffffff;
  tU32  Vu32LastVoltageLevel=0xcccccccc;
  tU16  Vu16LastUbatSense=0x2222;
  tU16  Vu16LastWakeupReason=0x6666;
  tU8   Vu8ResetSetBySpm=0x88;
  tU32  Vu32ContinuousResets=0x11111111;
  tBool VbRead;
  tU8   Vu8Read;
  tU32  Vu32Read;
  tU16  Vu16Read;

  //set all data
  oDataSystemState.vSetData(Vu32SystemState);
  oDataUBatDropDetected.vSetData(VbUBatDropDetected);
  oDataIgnitionOffCount.vSetData(Vu32IgnitionOffCount);
  oDataLastOsalElapsedTime.vSetData(Vu32LastOsalElapsedTime);
  oDataLastVoltageLevel.vSetData(Vu32LastVoltageLevel);
  oDataLastUbatSense.vSetData(Vu16LastUbatSense);
  oDataLastWakeupReason.vSetData(Vu16LastWakeupReason);
  oDataResetSetBySpm.vSetData(Vu8ResetSetBySpm);
  oDataContinuousResets.vSetData(Vu32ContinuousResets);

  //read all data
  //-- Vu32SystemState
  Vu32Read=0;
  oDataSystemState.u8GetData(Vu32Read);
  if(Vu32Read!=Vu32SystemState)
  {
    Vu32ErrorCode|=1;
  }
  //--VbUBatDropDetected
  VbRead=FALSE;
  oDataUBatDropDetected.u8GetData(VbRead);
  if(VbRead!=VbUBatDropDetected)
  {
    Vu32ErrorCode|=2;
  }
  //--Vu32IgnitionOffCount
  Vu32Read=0;
  oDataIgnitionOffCount.u8GetData(Vu32Read);
  if(Vu32Read!=Vu32IgnitionOffCount)
  {
    Vu32ErrorCode|=4;
  }
  //--Vu32LastOsalElapsedTime
  Vu32Read=0;
  oDataLastOsalElapsedTime.u8GetData(Vu32Read);
  if(Vu32Read!=Vu32LastOsalElapsedTime)
  {
    Vu32ErrorCode|=8;
  }
  //--Vu32LastVoltageLevel
  Vu32Read=0;
  oDataLastVoltageLevel.u8GetData(Vu32Read);
  if(Vu32Read!=Vu32LastVoltageLevel)
  {
    Vu32ErrorCode|=10;
  }
  //--Vu16LastUbatSense
  Vu16Read=0;
  oDataLastUbatSense.u8GetData(Vu16Read);
  if(Vu16Read!=Vu16LastUbatSense)
  {
    Vu32ErrorCode|=12;
  }
  //--Vu16LastWakeupReason
  Vu16Read=0;
  oDataLastWakeupReason.u8GetData(Vu16Read);
  if(Vu16Read!=Vu16LastWakeupReason)
  {
    Vu32ErrorCode|=14;
  }
  //--Vu8ResetSetBySpm
  Vu8Read=0;
  oDataResetSetBySpm.u8GetData(Vu8Read);
  if(Vu8Read!=Vu8ResetSetBySpm)
  {
    Vu32ErrorCode|=18;
  }
  //--Vu32ContinuousResets
  Vu32Read=0;
  oDataContinuousResets.u8GetData(Vu32Read);
  if(Vu32Read!=Vu32ContinuousResets)
  {
    Vu32ErrorCode|=20;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetSRAMAccessMultiThread()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   22.09.2015
******************************************************************************/
tU32 u32DPSetGetSRAMAccessMultiThread(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  Vu32ErrorCode=u32DPProcessTest(eCmdSramAccessMultiThreads,NULL,NULL,0);
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecordNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecordNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantCodingRecord VmyElementRecord;
  tU8                                                      Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  tU8                                                      Vu8RecordDefault[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  tS32                                                     VS32Size;
  memset(&Vu8Record,0,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  memset(&Vu8RecordDefault,0xaa,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  if(Vu32ErrorCode==0)
  {//read record
    VS32Size=VmyElementRecord.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
    if(VS32Size!=DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)
    {
      Vu32ErrorCode|=1;
    }
    else
    {/*check if record default */
      if(memcmp(&Vu8Record[0],&Vu8RecordDefault[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
      {
        Vu32ErrorCode|=2;
      }
      else
      {/*check status*/
        if(VmyElementRecord.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		    {
		      Vu32ErrorCode|=4;
		    }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecordNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecordNoInitNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantCodingRecord_NoInit                  VmyElementRecord;
  tU8                                                                              Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  tS32                                                                             VS32Size;
  memset(&Vu8Record,0,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);  
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  if(Vu32ErrorCode==0)
  {//read record
    VS32Size=VmyElementRecord.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
    if(VS32Size!=0)
    {
      Vu32ErrorCode|=1;
    }
    else
    {/*check status*/
      if(VmyElementRecord.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=2;
		  }      
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantItemGNSS   VmyElementItem;
  tU8                                                    Vu8Value;
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(Vu8Value!=55)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemNoInitNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemNoInitNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantItemGNSS_NoInit   VmyElementItem;
  tU8                                                           Vu8Value;
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(Vu8Value!=0)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSWrongItemNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSWrongItemNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementNoKDSVariantItemWrong       VmyElementItem;
  tU8                                                           Vu8Value;
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(Vu8Value!=22)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtU32NoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtU32NoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemAdcRangeMinTu32       VmyElementItem;
  tU32                                                                        Vu32Value;
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
  if(VmyElementItem.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(Vu32Value!=0x12345678)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtU16NoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtU16NoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemMinorVersionTu16      VmyElementItem;
  tU16                                                                        Vu16Value;
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
  if(VmyElementItem.s32GetData(Vu16Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(Vu16Value!=0x1234)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtStringNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtStringNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemTagTString      VmyElementItem;
  tChar                                                                 Vstr[10];
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
  if(VmyElementItem.s32GetData(&Vstr[0],sizeof(Vstr))!=(strlen("test")+1))
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(strcmp("test",&Vstr[0])!=0)
    {
      Vu32ErrorCode=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32ErrorCode|=4;
		  }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSSensorConfigGyroItemTagNoInitTString      VmyElementItem;
  tChar                                                                          Vstr[10];
  Vu32ErrorCode=u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
  if(VmyElementItem.s32GetData(&Vstr[0],sizeof(Vstr))!=0)
  {
    Vu32ErrorCode=1;
  }
  else
  {/*check status*/
    if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		{
		  Vu32ErrorCode|=4;
		}
  }
  #endif
  return(Vu32ErrorCode);
}


/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecord()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecord(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSRecord,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecordNoInit()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecordNoInit(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSRecordNoInit,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItem()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItem(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSItemGNSS,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemNoInit()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemNoInit(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSItemGNSSNoInit,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSWrongItem()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSWrongItem(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSWrongItem,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtU32()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtU32(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSu32,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtU16()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtU16(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSu16,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtString()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtString(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSstr,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemtStringNoInit()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemtStringNoInit(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  if(Vu32ErrorCode==0)
  {
    Vu32ErrorCode=u32DPProcessTest(eCmdGetDefKDSstrNoInit,NULL,NULL,0);
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSBankElementsNoKdsEntryStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSBankElementsNoKdsEntryStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST_BANK
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec                                         VmyEleCodingRec;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec_NoInitBank                              VmyEleCodingRec_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec_NoInitBankConfig                        VmyEleCodingRec_NoInitBankConfig;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS                                          VmyEleItemGNSS;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS_NoInitBank                               VmyEleItemGNSS_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS_NoInitBankConfig                         VmyEleItemGNSS_NoInitBankConfig;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemAdcRangeMinTu32                         VmyEleItemAdcRangeMinTu32;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemMinorVersionTu16_NoInitBank             VmyEleItemMinorVersionTu16_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemTagTString                              VmyEleItemTagTString;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSSensorConfigGyroItemTagNoInitTString                     VmyEleItemTagNoInitTString;
  tU8    Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  tChar  Vstr[10];
  tU8    Vu8Value=0;
  tU16   Vu16Value=0;
  tU32   Vu32Value=0;

  Vu32ErrorCode|=u32KDSClearEntry(DP_U16_KDSADR_CMVARIANTCODING);
  Vu32ErrorCode|=u32KDSClearEntry(DP_U16_KDSADR_SENSORCONFIGGYRO);
  if(Vu32ErrorCode==0)
  { /* load bank 2 to current user*/
    Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(2);     
    if(Vu32ErrorCode==0)
    {
      if(VmyEleCodingRec.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=6)     
      {
        Vu32ErrorCode|=1;        
      }
      if(VmyEleCodingRec_NoInitBank.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
      {
        Vu32ErrorCode|=2;
      }
      if(VmyEleCodingRec_NoInitBankConfig.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
      {
        Vu32ErrorCode|=4;
      }
      if(VmyEleItemGNSS.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=11)
        {
          Vu32ErrorCode|=8;
        }
      }
      if(VmyEleItemGNSS_NoInitBank.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=0)
        {
          Vu32ErrorCode|=10;
        }
      }
      if(VmyEleItemGNSS_NoInitBankConfig.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=0)
        {
          Vu32ErrorCode|=12;
        }
      }
      if(VmyEleItemAdcRangeMinTu32.s32GetData(Vu32Value)==DP_S32_NO_ERR)
      {
        if(Vu32Value!=0x3456789a)
        {
          Vu32ErrorCode|=14;
        }
      }
      if(VmyEleItemMinorVersionTu16_NoInitBank.s32GetData(Vu16Value)==DP_S32_NO_ERR)
      {
        if(Vu16Value!=0)
        {
          Vu32ErrorCode|=18;
        }
      }
      if(VmyEleItemTagTString.s32GetData(&Vstr[0],sizeof(Vstr))<= 0)
      {
        Vu32ErrorCode|=20;
      }     
      else
      {
        if(strcmp("test2",&Vstr[0])!=0)
        {
          Vu32ErrorCode|=22;
        }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSBankElementsStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   04.12.2015
******************************************************************************/
tU32 u32DPGetDefKDSBankElementsStored(void)
{
  tU32  Vu32ErrorCode=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST_BANK
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec                                         VmyEleCodingRec;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec_NoInitBank                              VmyEleCodingRec_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantCodingRec_NoInitBankConfig                        VmyEleCodingRec_NoInitBankConfig;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS                                          VmyEleItemGNSS;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS_NoInitBank                               VmyEleItemGNSS_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSVariantItemGNSS_NoInitBankConfig                         VmyEleItemGNSS_NoInitBankConfig;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemAdcRangeMinTu32                         VmyEleItemAdcRangeMinTu32;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemMinorVersionTu16_NoInitBank             VmyEleItemMinorVersionTu16_NoInitBank;
  dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemTagTString                              VmyEleItemTagTString;
  dp_tclPddDpTestKdsDefaultBankTrBankKDSSensorConfigGyroItemTagNoInitTString                     VmyEleItemTagNoInitTString;

  tU8    Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  tChar  Vstr[10];
  tU8    Vu8Value;
  tU16   Vu16Value;
  tU32   Vu32Value;

  pdd_delete_data_stream("effd/PddDpTestKdsDefaultBank",PDD_LOCATION_FS);
  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_CMVARIANTCODING,Vu8RecordKdsDefault,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(Vu32ErrorCode==0)
  { /* load bank 2 to current user*/
    Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(2);  
    if(Vu32ErrorCode==0)
    {
      if(VmyEleCodingRec.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)     
      {
        Vu32ErrorCode|=1;
      }
      if(VmyEleCodingRec_NoInitBank.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)
      {
        Vu32ErrorCode|=2;
      }
      if(VmyEleCodingRec_NoInitBankConfig.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
      {
        Vu32ErrorCode|=4;
      }
      if(VmyEleItemGNSS.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=3)
        {
          Vu32ErrorCode|=8;
        }
      }
      if(VmyEleItemGNSS_NoInitBank.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=3)
        {
          Vu32ErrorCode|=10;
        }
      }
      if(VmyEleItemGNSS_NoInitBankConfig.s32GetData(Vu8Value)==DP_S32_NO_ERR)
      {
        if(Vu8Value!=3)
        {
          Vu32ErrorCode|=12;
        }
      }
      if(VmyEleItemAdcRangeMinTu32.s32GetData(Vu32Value)==DP_S32_NO_ERR)
      {
        if(Vu32Value!=0x4a4a4a4a)
        {
          Vu32ErrorCode|=14;
        }
      }
      if(VmyEleItemMinorVersionTu16_NoInitBank.s32GetData(Vu16Value)==DP_S32_NO_ERR)
      {
        if(Vu16Value!=0x4a4a)
        {
          Vu32ErrorCode|=14;
        }
      }
      if(VmyEleItemTagTString.s32GetData(&Vstr[0],sizeof(Vstr))<=0)
      {
        Vu32ErrorCode|=18;
      }
      else
      {
        if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
        {
          Vu32ErrorCode|=20;
        }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetDefKDSToDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   25.11.2015
******************************************************************************/
tU32 u32DPSetDefKDSToDefault(void)
{
  tU32  Vu32ErrorCode=0;  
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST_BANK
  //end process 3 
  Vu32ErrorCode=u32DPProcessTest(eCmdProcess3End,NULL,NULL,0);
  if(Vu32ErrorCode==0)
  { 
    vprID[2]=INVALID_PID;
    //change values   
    dp_tclPddDpTestKdsDefaultBankTrBankSensorConfigGyroItemTagTString                              VmyEleItemTagTString;
    tChar  Vstr[10];
    if(VmyEleItemTagTString.s32SetData("test")!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
    else
    {
      //Set to default
      if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }
      else
      { /*set default user*/
	      if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=80;
	      }	
	      if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=100;
        }
      }
      //default lesen
      if(VmyEleItemTagTString.s32GetData(&Vstr[0],sizeof(Vstr))<=0)
      {
        Vu32ErrorCode|=18;
      }
      else
      {
        if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
        {
          Vu32ErrorCode|=20;
        }
      }
    }
  }
  //Dateien löschen 
  pdd_delete_data_stream("effd/PddDpTestKdsDefaultBank",PDD_LOCATION_FS);
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPLoadDefaultBankToUserAndStore()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   06.04.2016
******************************************************************************/
tU32 u32DPLoadDefaultBankToUserAndStore(void)
{
  tU32  Vu32ErrorCode=0;  
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST_BANK
  dp_tclPddDpTestKdsDefaultBankTrBankKDSSensorConfigGyroItemTagNoInitConfigTString               VmyEleItemTagNoInitConfigTString;

  memset(Vu8RecordKdsDefaultSensor,0x4a,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32KDSWriteEntry(DP_U16_KDSADR_SENSORCONFIGGYRO,Vu8RecordKdsDefaultSensor,DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE);
  Vu32ErrorCode|=u32DPSetEndUser(2);
  if(Vu32ErrorCode==0)
  { //lock
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
    else
    { //set end user to default
      if(DP_s32SetEndUserDefault(2,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=20;
	    }	 
      //load bank 2 to current 
	    if(DP_s32LoadBankToCurrentEndUser(2,DP_ACCESS_ID_END_USER_APP,8000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=40;
	    }
	    //unlock  DP_U32_LOCK_MODE_END_USER
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode|=80;
	    }
    }//end lock user manager
    //check default value
    tChar  Vstr[10];
    if(VmyEleItemTagNoInitConfigTString.s32GetData(&Vstr[0],sizeof(Vstr))<=0)
    {
      Vu32ErrorCode|=100;
    }
    else
    {
      if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
      {
        Vu32ErrorCode|=200;
      }
    }
    if(Vu32ErrorCode==0)
    { //call store now
      if(DP_s32StoreNow(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=400;
      }
      //get file from PDD  
      tS32 Vs32Size=pdd_get_data_stream_size("effd/PddDpTestKdsDefaultBank02",PDD_LOCATION_FS);
      if(Vs32Size<=0)
      {
        Vu32ErrorCode|=600;
      }
      else
      {
        tU8* Vu8pBuf=(tU8*)malloc(Vs32Size);
        if(Vu8pBuf==NULL)
        {
           Vu32ErrorCode|=800;
        }
        else
        {
          tU8 VubInfo;
          Vs32Size=pdd_read_datastream("effd/PddDpTestKdsDefaultBank02",PDD_LOCATION_FS,Vu8pBuf, Vs32Size,1,&VubInfo);
          if(Vs32Size<=0)
          {
            Vu32ErrorCode|=1000;
          }
          else
          { //call helper function to get the element value
            if(pdd_helper_get_element_from_stream(DP_ELEMENT_TR_BANKKDS_SENSOR_CONFIG_GYRO_ITEM_TAG_NOINIT_CONFIG_STRING,Vu8pBuf,Vs32Size,(void*)&Vstr[0],sizeof(Vstr),00)!=0)
            {
               Vu32ErrorCode|=2000;
            }            
            else
            {
              if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
              {
                Vu32ErrorCode|=3000;
              }
            }
          }
          free(Vu8pBuf);
        }
      }
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUndefElementCheckParameter()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetUndefElementCheckParameter(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="User1";
  const char*          VcPoolName="Pool1";
  const char*          VcElementName="TrElement1";
  unsigned char        VucBuf[30];
  if(DP_s32SetUndefElement(NULL,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=10;
  }
  if(DP_s32SetUndefElement(VcUserName,NULL,VcElementName,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=20;
  }
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,NULL,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=40;
  }
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,NULL,sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=80;
  }
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],0)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=100;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetUndefElementCheckParameter()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPGetUndefElementCheckParameter(void)
{
  tU32                 Vu32ErrorCode=0;  
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="User1";
  const char*          VcPoolName="Pool1";
  const char*          VcElementName="TrElement1";
  unsigned char        VucBuf[30];
  if(DP_s32GetUndefElement(NULL,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=10;
  }
  if(DP_s32GetUndefElement(VcUserName,NULL,VcElementName,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=20;
  }
  if(DP_s32GetUndefElement(VcUserName,VcPoolName,NULL,&VucBuf[0],sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=40;
  }
  if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,NULL,sizeof(VucBuf))!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=80;
  }
  if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],0)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=100;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementCheckParameter()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPInitUndefElementCheckParameter(void)
{
  tU32                 Vu32ErrorCode=0;
  
  vDPInit();
 #ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="User1";
  const char*          VcPoolName="Pool1";
  dp_tclBaseElement    VmyDpElem("TrElement1");
  if(DP_s32InitUndefElement(NULL,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=10;
  }
  if(DP_s32InitUndefElement(VcUserName,NULL,eDpLocation_FILE_SYSTEM,&VmyDpElem)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=20;
  }
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_RAW_NOR,&VmyDpElem)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=40;
  }
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,NULL)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=80;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUndefElementCheckParameter()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPDeleteUndefElementCheckParameter(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="User1";
  const char*          VcPoolName="Pool1";
  const char*          VcElementName="TrElement1";
  if(DP_s32DeleteUndefElement(NULL,VcPoolName,VcElementName)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=10;
  }
  if(DP_s32DeleteUndefElement(VcUserName,NULL,VcElementName)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=20;
  }
  if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,NULL)!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=40;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUndefElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetUndefElement(void)
{
  tU32                 Vu32ErrorCode=0;
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson111";
  const char*          VcPoolName="Pool22";
  const char*          VcElementName="TestElement111";
  unsigned char        VucBuf[30];
  vDPInit();
  memset(VucBuf,0x55,sizeof(VucBuf));
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))< DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPGetUndefElementUnknown()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPGetUndefElementUnknown(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson2";
  const char*          VcPoolName="Pool23";
  const char*          VcElementName="TestElement2";
  unsigned char        VucBuf[30];
  if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))!= DP_S32_ERR_NO_POOL)
  {
    Vu32ErrorCode|=10;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPDeleteUndefElementUnknown()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPDeleteUndefElementUnknown(void)
{
  tU32  Vu32ErrorCode=0;
  vDPInit();  
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson3";
  const char*          VcPoolName="Pool24";
  const char*          VcElementName="TrElement3";
  if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,VcElementName)!=DP_S32_ERR_NO_POOL)
  {
    Vu32ErrorCode|=10;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetGetUndefElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetGetUndefElement(void)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  const char*          VcPoolName="Pool25";
  const char*          VcElementName="TestElement4";
  unsigned char        VucBuf[30];
  unsigned char        VucBufRead[30];
  vDPInit();
  memset(VucBuf,0x2A,sizeof(VucBuf));
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))< DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {/*get element */
	  memset(VucBufRead,0x00,sizeof(VucBuf));
    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBufRead[0],sizeof(VucBufRead))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  {/*compare buffer*/
	    if(memcmp(VucBufRead,VucBuf,sizeof(VucBuf))!=0)
	    {
	      Vu32ErrorCode|=40;
	    }
	  }
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetDeleteGetUndefElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPSetDeleteGetUndefElement(void)
{
  tU32                 Vu32ErrorCode=0; 
  vDPInit();
  #ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson5";
  const char*          VcPoolName="Pool26";
  const char*          VcElementName="TestElement7";
  unsigned char        VucBuf[30];
  unsigned char        VucBufRead[30];
  memset(VucBuf,0xAA,sizeof(VucBuf));
  if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBuf[0],sizeof(VucBuf))< DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {/*delete element*/
    if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,VcElementName)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { /*get element */
	    memset(VucBufRead,0x00,sizeof(VucBuf));
	    tS32 VS32ErrorCode=DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&VucBufRead[0],sizeof(VucBufRead));
      if(VS32ErrorCode>=0)
      {
        Vu32ErrorCode|=VS32ErrorCode;
      }
	  }
  }
#endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPInitGetUndefElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPInitGetUndefElement(void)
{
   tU32                 Vu32ErrorCode=0;
   
   vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
   const char*          VcPoolName="Pool33";
   const char*          VcUserName="User33";    
   unsigned char        VucBufRead[30];
   dp_tclBaseElement    VmyDpElem(
	    "TestElement8",			                   //pcElementName 
	    (tU8)0x01,					               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      FALSE,						               //bVarSize
      0x0006,									   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    

   if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem)!=DP_S32_NO_ERR)
   {
     Vu32ErrorCode|=10;
   }
   else
   {
     if(DP_s32GetUndefElement(VcUserName,VcPoolName,"TestElement8",&VucBufRead[0],sizeof(VucBufRead))>=DP_S32_NO_ERR)
     {
        Vu32ErrorCode|=20;
     }
	   if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,"TestElement8")!=DP_S32_NO_ERR)
     {
       Vu32ErrorCode|=40;
     }
   }
#endif
   return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitSetGetUndefElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPInitSetGetUndefElement(void)
{
   tU32                 Vu32ErrorCode=0;
 #ifdef DP_FEATURE_UNDEF_ELEMENT_D
   const char*          VcPoolName="Pool44";
   const char*          VcUserName="User44";    
   unsigned char        VucBuf[30];
   unsigned char        VucBufRead[30];
  
   vDPInit();

     dp_tclBaseElement    VmyDpElem(
	    "TestElement9",			                   //pcElementName 
	    (tU8)0x01,					               //u8Version
	    sizeof(VucBuf),	   		                   //u16ElementLength
      FALSE,						               //bVarSize
      0x0006,									   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode   

   if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem)!=DP_S32_NO_ERR)
   {
     Vu32ErrorCode|=10;
   }
   else
   {
     memset(VucBuf,0x33,sizeof(VucBuf));
     if(DP_s32SetUndefElement(VcUserName,VcPoolName,"TestElement9",&VucBuf[0],sizeof(VucBuf))< DP_S32_NO_ERR)
     {
       Vu32ErrorCode|=20;
     }
     else
	   {
       if(DP_s32GetUndefElement(VcUserName,VcPoolName,"TestElement9",&VucBufRead[0],sizeof(VucBufRead))<DP_S32_NO_ERR)
       {
          Vu32ErrorCode|=40;
       }
	     else
	     { /*compare buffer*/
	       if(memcmp(VucBufRead,VucBuf,sizeof(VucBuf))!=0)
	       {
	         Vu32ErrorCode|=80;
	       }
	     }
	   } 
   }
#endif
   return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInit20UndefElements()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.05.2014
******************************************************************************/
tU32 u32DPInit20UndefElements(void)
{
   tU32                 Vu32ErrorCode=0;
 
   vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
   const char*          VcPoolName="Pool55";
   const char*          VcUserName="User55";    
   tU8                  VubInc;
   tChar                VstrElementName[200];
   for(VubInc=0;VubInc<20;VubInc++)
   {//change name of element
      memset(VstrElementName,0x00,sizeof(VstrElementName));
      snprintf(VstrElementName,sizeof(VstrElementName),"Element_%d",VubInc);
      dp_tclBaseElement    VmyDpElem(
	    VstrElementName,			                   //pcElementName 
	    VubInc,	    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                   //bVarSize
      0x0006,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
	    if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM_SECURE,&VmyDpElem)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=VubInc;
	      break;
      }
   }
#endif
   return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementWriteNewProperty()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.05.2014
******************************************************************************/
tU32 u32DPInitUndefElementNewProperty(void)
{
  tU32                 Vu32ErrorCode=0;
  
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="Pool66";
  const char*          VcUserName="User66";    
  dp_tclBaseElement    VmyDpElem1(
	    "TEST_ELEMENT",			                   //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                   //bVarSize
      0x0006,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    

  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //change property
  dp_tclBaseElement    VmyDpElem2(
	    "TEST_ELEMENT",			                   //pcElementName 
	    (tU8) 0x02,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      FALSE,	    			                   //bVarSize
      0x0006,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem2)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementWriteSetDefaultProperty()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPInitUndefElementSetDefaultProperty(void)
{
  tU32                 Vu32ErrorCode=0;
   
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="Pool77";
  const char*          VcUserName="User77";    
  dp_tclBaseElement    VmyDpElem1(
	    "TEST_ELEMENT",			                   //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                   //bVarSize
      0x0006,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode   

  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //default property
  dp_tclBaseElement    VmyDpElem2(
	    "TEST_ELEMENT",			                   //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      FALSE,	    			                   //bVarSize
      0x0006,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem2)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementWriteSetDefaultProperty()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPWriteInitUndefElement(void)
{
  tU32                 Vu32ErrorCode=0;
 
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="Pool88";
  const char*          VcUserName="User88"; 
  tU32                 Vu32Write=0x55555555;
  dp_tclBaseElement    VmyDpElem1(
	    "TEST_ELEMENT88",			                       //pcElementName 
	    (tU8) 0x01,    				                       //u8Version
	    sizeof(tU32),					                       //u16ElementLength
      TRUE,						                             //bVarSize
      0x0006,							 		                     //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                             //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,"TEST_ELEMENT88",(tU8*)&Vu32Write,sizeof(tU32)) < DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementWriteSetDefaultProperty()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPReadInitUndefElementAfterReboot(void)
{
  tU32                 Vu32ErrorCode=0;
  
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  tU32                 Vu32Read=0xffffffff;
  tU32                 Vu32Write=0x55555555;
  const char*          VcPoolName="Pool88";
  const char*          VcUserName="User88";
  tS32 Vs32ReturnCode=DP_s32GetUndefElement(VcUserName,VcPoolName,"TEST_ELEMENT88",(tU8*)&Vu32Read,sizeof(tU32));
  if(Vs32ReturnCode!=sizeof(tU32))
  {
    Vu32ErrorCode=20;
  }
  else
  {
    if(Vu32Read!=Vu32Write)
	  {
	    Vu32ErrorCode=Vu32Read;
	  }
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitSetGetUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPInitSetGetUndefElementShared(void)
{
  tU32                 Vu32ErrorCode=0;
  
  vDPInit();
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="PoolTestShared";
  const char*          VcUserName="User3"; 
  tU8                  Vu8Write=0x11;
  tU8                  Vu8Read=0;
  dp_tclBaseElement    VmyDpElem1(
	    "TEST_ELEMENT99",			                             //pcElementName 
	    (tU8) 0x01,    				                             //u8Version
	    sizeof(tU8),					                             //u16ElementLength
      TRUE,						                                   //bVarSize
      0x0666,							 		                           //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent,       //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,      //tStoringType
      0,							                                   //u32StoringIntervall
      DP_DEFSET_USER);                                   //u32DefSetMode    
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,"TEST_ELEMENT99",&Vu8Write,sizeof(tU8))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { 
	    if(DP_s32GetUndefElement(VcUserName,VcPoolName,"TEST_ELEMENT99",&Vu8Read,sizeof(tU8))< DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }
      else
      {
        if(Vu8Read!=Vu8Write)
	      {
	        Vu32ErrorCode=(tU32) Vu8Read;
	      }
      }
    }
  }
#endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPOtherProcessInitUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   17.06.2014
******************************************************************************/
tU32 u32DPOtherProcessInitUndefElementShared(void)
{
 tU32                 Vu32ErrorCode=0;
 
 vDPInit();
 #ifdef DP_FEATURE_UNDEF_ELEMENT
 const char*          VcPoolName="PoolTestSharedInit";
 const char*          VcUserName="User5"; 
 const char*          VcElementName="TEST_ELEMENT78"; 

 dp_tclBaseElement    VmyDpElem1(
	    VcElementName,	                             //pcElementName 
	    (tU8) 0x01,    				                       //u8Version
	    sizeof(tU8),					                       //u16ElementLength
      TRUE,						                             //bVarSize
      0x0666,							 		                     //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                             //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  //set and get element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //start process to delete
  if(Vu32ErrorCode==0)
  {
	  Vu32ErrorCode=u32DPProcessTest(eCmdInit,VcElementName,VcPoolName,0);
  }
 #endif
 return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPInitProcessDeleteUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPOtherProcessDeleteUndefElementShared(void)
{
 tU32                 Vu32ErrorCode=0;
 
 vDPInit();
 #ifdef DP_FEATURE_UNDEF_ELEMENT
 const char*          VcPoolName="PoolTestSharedDelete";
 const char*          VcUserName="User5"; 
 tU8                  Vu8Write=0xAA;
 tU8                  Vu8Read=0;
 const char*          VcElementName="TEST_ELEMENT123"; 

 dp_tclBaseElement    VmyDpElem1(
	    VcElementName,	                             //pcElementName 
	    (tU8) 0x01,    				                       //u8Version
	    sizeof(tU8),					                       //u16ElementLength
      TRUE,						                             //bVarSize
      0x0666,							 		                     //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                             //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  //set and get element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Write,sizeof(tU8))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { 
	    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Read,sizeof(tU8))< DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }
      else
      {
        if(Vu8Read!=Vu8Write)
	      {
	        Vu32ErrorCode=(tU32) Vu8Read;
	      }
      }
	  }
  }
  //start process to delete
  if(Vu32ErrorCode==0)
  {
	  Vu32ErrorCode=u32DPProcessTest(eCmdDelete,VcElementName,VcPoolName,0);
  }
  //get element=> error
  if(Vu32ErrorCode==0)
  {
    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Read,sizeof(tU8))>= DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=80;
    }
  }
 #endif
 return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPInitProcessDeleteUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPOtherProcessSetUndefElementShared(void)
{
 tU32                 Vu32ErrorCode=0;
 
 vDPInit();
 #ifdef DP_FEATURE_UNDEF_ELEMENT
 const char*          VcPoolName="PoolTestSharedSet";
 const char*          VcUserName="User5"; 
 tU8                  Vu8Write=0x51;
 tU8                  Vu8Read=0;
 const char*          VcElementName="TEST_ELEMENT89"; 

 dp_tclBaseElement    VmyDpElem1(
	    VcElementName,	                          //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                   //bVarSize
      0x0666,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  //set and get element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Write,sizeof(tU8))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { 
	    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Read,sizeof(tU8))< DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }
      else
      {
        if(Vu8Read!=Vu8Write)
	      {
	       Vu32ErrorCode=(tU32) Vu8Read;
	      }
      }
    }
  }
  //start process to set
  if(Vu32ErrorCode==0)
  {
	  Vu32ErrorCode=u32DPProcessTest(eCmdSet,VcElementName,VcPoolName,0);
  }
  //get element read!=write
  if(Vu32ErrorCode==0)
  {
    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,&Vu8Read,sizeof(tU8))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=80;
    }
	  else
	  {
	    if(Vu8Read==Vu8Write)
	    {
	      Vu32ErrorCode =(tU32) Vu8Read;
	    }
    }
  }
 #endif
 return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPOtherProcessGetUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPOtherProcessGetUndefElementShared(void)
{
 tU32                 Vu32ErrorCode=0;
 
 vDPInit();
 #ifdef DP_FEATURE_UNDEF_ELEMENT
 const char*          VcPoolName="PoolTestSharedGet";
 const char*          VcUserName="User5"; 
 tU32                 Vu32Write=DP_TEST_VALUE_WRITE_GET_UNDEF_ELEMENT_SHARED;
 tU32                 Vu32Read=0;
 const char*          VcElementName="TEST_ELEMENT11"; 

 dp_tclBaseElement    VmyDpElem1(
	    VcElementName,	                          //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                   //bVarSize
      0x0666,							 		   //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							               //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  //set and get element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,(tU8*)&Vu32Write,sizeof(tU32))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { 
	    if(DP_s32GetUndefElement(VcUserName,VcPoolName,VcElementName,(tU8*)&Vu32Read,sizeof(tU32))< DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }
      else
      {
        if(Vu32Read!=Vu32Write)
	      {
	        Vu32ErrorCode=(tU32) Vu32Read;
	      }
      }
    }
  }
  //start process to set
  if(Vu32ErrorCode==0)
  {
	  Vu32ErrorCode=u32DPProcessTest(eCmdGet,VcElementName,VcPoolName,0);
  }
 #endif
 return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPOtherProcessAccessDelUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPOtherProcessAccessDelUndefElementShared(void)
{
  tU32                 Vu32ErrorCode=0;
 
  vDPInit();
  #ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="PoolTestSharedAccessDelete";
  const char*          VcUserName="User5"; 
  tU32                 Vu32Write=0x67891234;
  const char*          VcElementName="TEST_ELEMENT987"; 
  dp_tclBaseElement    VmyDpElem1(
	    VcElementName,    	                 //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                     //bVarSize
      0x0666,							 		             //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                     //u32StoringIntervall
      DP_DEFSET_USER);                     //u32DefSetMode    
  //set and get element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  {
    if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,(tU8*)&Vu32Write,sizeof(tU32))< DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  else
	  { 
	    if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,VcElementName)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40;
      }     
    }
  }
  //start process to set
  if(Vu32ErrorCode==0)
  {
	  Vu32ErrorCode=u32DPProcessTest(eCmdAccessDeleteElement,VcElementName,VcPoolName,0);
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPOtherProcessInitDelUndefElementShared()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   21.05.2014
******************************************************************************/
tU32 u32DPOtherProcessInitDelUndefElementShared(void)
{
  tU32                 Vu32ErrorCode=0;
 
  vDPInit();
  #ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcPoolName="PoolTestSharedInitDelete";
  const char*          VcUserName="User5"; 
  tU32                 Vu32Write=0x67891234;
  const char*          VcElementName="TEST_ELEMENT222"; 


 dp_tclBaseElement    VmyDpElem1(
	    VcElementName,	                     //pcElementName 
	    (tU8) 0x01,    				               //u8Version
	    sizeof(tU8),					               //u16ElementLength
      TRUE,						                     //bVarSize
      0x0666,							 		             //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                     //u32StoringIntervall
      DP_DEFSET_USER);                     //u32DefSetMode    
  //init and set element
  if(DP_s32InitUndefElement(VcUserName,VcPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else if(DP_s32SetUndefElement(VcUserName,VcPoolName,VcElementName,(tU8*)&Vu32Write,sizeof(tU32))< DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  else if(DP_s32DeleteUndefElement(VcUserName,VcPoolName,VcElementName)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }     
  //start process 
  Vu32ErrorCode|=u32DPProcessTest(eCmdInitDeleteElement,VcElementName,VcPoolName,0);
 #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserLockUnlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserLockUnlock(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE

  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  {
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode=2;
	  }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSetGetElementEndUserIfLocked()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserSetGetElementEndUserIfLocked(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  /*lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=1;
  }
  else
  { /*try to set end user element*/
    dp_tclPddDpTestEndUserTestEndUserFlag VMyElement;
	  if(VMyElement.s32SetData(TRUE)!=DP_S32_ERR_ACCESS_POOL_LOCKED)
	  {
	    Vu32ErrorCode|=2;;
	  }
	  tBool VbTest;
	  if(VMyElement.s32GetData(VbTest)!=DP_S32_ERR_ACCESS_POOL_LOCKED)
	  {
	    Vu32ErrorCode|=4;;
	  }
	  /*unlock*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=8;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserSetGetElementEndUserAfterUnLocked()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserSetGetElementEndUserAfterUnLocked(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  /*lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  { 
    dp_tclPddDpTestEndUserTestEndUserFlag VMyElement;
	  /*unlock*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=2;
	  }
	  /*try to set end user element*/
	  if(VMyElement.s32SetData(FALSE)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4;;
	  }
	  tBool VbTest;
	  if(VMyElement.s32GetData(VbTest)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=8;;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSetGetElementNoEndUserIfLocked()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserSetGetElementNoEndUserIfLocked(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  /*lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=1;
  }
  else
  { /*try to set element*/
    dp_tclPddDpTestLocationFileTrTestFileFlag           VElement;
	  if(VElement.s32SetData(FALSE)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;;
	  }
	  tBool VbTest;
	  if(VElement.s32GetData(VbTest)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4;;
	  }
	  /*unlock*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=8;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserLock2ModesUnlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserLock2ModesUnlock(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tBool                                  VbTest;
  dp_tclPddDpTestEndUserTestEndUserFlag  VMyElement;
  /*lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode=1;
  }
  else
  { /*lock spi mode DP_U32_LOCK_MODE_SETDEF  => ERROR*/
	  if(DP_s32Lock(0,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_ERR_ALREADY_LOCKED)
	  {
	    Vu32ErrorCode=10;
	  }
	  /*unlock spi mode DP_U32_LOCK_MODE_SETDEF => ERROR*/
	  if(DP_s32Unlock(0,5000)!=DP_S32_ERR_ALREADY_LOCKED)
	  {
	    Vu32ErrorCode|=20;
	  }
    /*set end user element*/
	  if(VMyElement.s32SetData(FALSE)!=DP_S32_ERR_ACCESS_POOL_LOCKED)	
	  {	 
	    Vu32ErrorCode|=40;
	  }
	  /*get end user element*/
	  if(VMyElement.s32GetData(VbTest)!=DP_S32_ERR_ACCESS_POOL_LOCKED)
	  {
	    Vu32ErrorCode|=80;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=100;
	  }
	  if(VMyElement.s32SetData(TRUE)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200;
	  }
	  /*get end user element*/
	  if(VMyElement.s32GetData(VbTest)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=400;;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSetEndUserReadNewDefaultElement()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserSetEndUserReadNewDefaultElement(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUserSharedString  VMyStrElement;
  char                                           VStr[100];
  
  /************* set end user 0 ******************/
  Vu32ErrorCode|=u32DPSetEndUser(0);
  /************* set end user string    ******************/
  memcpy(VStr,"all test",strlen("all test")+1);
  if (VMyStrElement.s32SetData(VStr)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  { /*get element and compare */	
    if (VMyStrElement.s32GetData((tString)VStr,sizeof(VStr)) <= DP_S32_NO_ERR) 
	  {
       Vu32ErrorCode|=20;
	  }
	  else if(memcmp(VStr,"all test",strlen(VStr))!=0)
	  {
	    Vu32ErrorCode|=40;
	  }
  }
  /************* set end user 2 ******************/
  Vu32ErrorCode|=u32DPSetEndUser(2);
  /************* get end user string => default ******************/
  if (VMyStrElement.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR) 
  {
    Vu32ErrorCode|=1000;
  }
  else if(memcmp(VStr,"test's forever",strlen(VStr))!=0)
  {
    Vu32ErrorCode|=2000;
  }
  /************* set end user 0 ******************/
  Vu32ErrorCode|=u32DPSetEndUser(0);  
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSaveNewEndUser1AndSetAllElements()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserSaveNewEndUser1AndSetAllElements(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  /************* set end user 1 ******************/
   Vu32ErrorCode|=u32DPSetEndUser(1);  
  /************* change all elements for end user(1) ******************/
  {
    tU32    Vu32Value=DP_TESTVALUE_TU32;
	  tU16    Vu16Value=DP_TESTVALUE_TU16;
	  tBool   VbValue=DP_TESTVALUE_FLAG;
	  tU8     Vu8Value=DP_TESTVALUE_TU8;
	  char    VStr[100];
    dp_tclPddDpTestEndUserTrTestEndUserArray       VmyTestElementArray;
	  dp_tclPddDpTestEndUserTestEndUserFlag          VmyTestElementFlag;
	  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
	  dp_tclPddDpTestEndUserTestEndUserShared        VmyTestElementSharedU32;
	  dp_tclPddDpTestEndUserTestEndUserSharedArray   VmyTestElementSharedArray;
	  dp_tclPddDpTestEndUserTestEndUserSharedString  VmyTestElementSharedStr;
	  if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=100;
	  }
	  if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200;
	  }
	  if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=400;
	  }
	  if(VmyTestElementSharedU32.s32SetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=800;
	  }
	  if(VmyTestElementSharedArray.s32SetData(&Vu16Value,1)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1000;
	  }
	  memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
	  if(VmyTestElementSharedStr.s32SetData(VStr)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2000;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   20.01.2015
******************************************************************************/
tU32 u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU32    Vu32Value;
  tU16    Vu16Value;
  tBool   VbValue;
  tU8     Vu8Value;
  char    VStr[100];
  tU8     Vu8CurrentEndUser;
  dp_tclPddDpTestEndUserTrTestEndUserArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserTestEndUserFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserTestEndUserShared        VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserTestEndUserSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserTestEndUserSharedString  VmyTestElementSharedStr;
  /************* get mode end user 1 ******************/
  if(DP_s32GetEndUser(Vu8CurrentEndUser)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=1;
  }
  else
  {//check end user
    if(Vu8CurrentEndUser!=1)
    {
	    Vu32ErrorCode|=2;
	  }
  }
  /************* check all elements for end user (1)******************/
  {
	  if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
	  {
	    Vu32ErrorCode|=10;
	  }
	  else if(Vu32Value!=DP_TESTVALUE_TU32)
	  {
	    Vu32ErrorCode|=20;
	  }
	  if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=40;
	  }
	  else if(VbValue!=DP_TESTVALUE_FLAG)
	  {
	    Vu32ErrorCode|=80;
	  }
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=100;
	  }
	  else if(Vu8Value!=DP_TESTVALUE_TU8)
	  {
	    Vu32ErrorCode|=200;
	  }
	  if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=400;
	  }
	  else if(Vu32Value!=DP_TESTVALUE_TU32)
	  {
	    Vu32ErrorCode|=800;
	  }
	  if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=1)
	  {
	    Vu32ErrorCode|=1000;
	  }
	  else if(Vu16Value!=DP_TESTVALUE_TU16)
    {
	    Vu32ErrorCode|=2000;
	  }
	  if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4000;
	  }
	  else if(memcmp(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR))!=0)
	  {
	    Vu32ErrorCode|=8000;
	  }
  }
  /************* set all elements for user(1) to default******************/
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000;
  }
  else
  { /*set end user*/
    if(DP_s32SetEndUserDefault(1,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20000;
    }
    /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40000;
    }
  }
  /************* read all default elements  for user(1) ******************/
  {
	  if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
	  {
	    Vu32ErrorCode|=80000;
	  }	
	  if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200000;
	  }
	  else if(VbValue!=TRUE)
	  {
	    Vu32ErrorCode|=400000;
	  }
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=800000;
	  }
	  else if(Vu8Value!=9)
	  {
	    Vu32ErrorCode|=1000000;
	  }
	  if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2000000;
	  }
	  else if(Vu32Value!=0x12345678)
	  {
	    Vu32ErrorCode|=4000000;
	  }
	  if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=1)
	  {
	    Vu32ErrorCode|=8000000;
	  }
	  else if(Vu16Value!=0x1223)
    {
	    Vu32ErrorCode|=10000000;
	  }
	  if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20000000;
	  }
	  else if(memcmp(VStr,"test's forever",strlen("test's forever"))!=0)
	  {
	     Vu32ErrorCode|=40000000;
	  }
  }
  /************* s32SetEndUser(0) ******************/
  Vu32ErrorCode|=u32DPSetEndUser(0);    
#endif
#endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserSetNoCurrentUserToDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.01.2015
******************************************************************************/
tU32 u32DPEndUserSetNoCurrentUserToDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  char    VStrUser0[100];
  char    VStrUser4[100];
  dp_tclPddDpTestEndUserTestEndUserSharedString  VmyTestElementSharedStr;
  /************* s32SetEndUser(4) ******************/
  Vu32ErrorCode|=u32DPSetEndUser(4);    
  /*************store element end user 4  ******************/
  memcpy(VStrUser0,DP_TESTVALUE_STR_4,strlen(DP_TESTVALUE_STR_4)+1);
  if(VmyTestElementSharedStr.s32SetData(VStrUser0)!=DP_S32_NO_ERR)
  {
	  Vu32ErrorCode|=2;
  }
  /************* get element and compare  ******************/
  if(VmyTestElementSharedStr.s32GetData(VStrUser4,sizeof(VStrUser4))<=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=4;
  }
  else if(memcmp(VStrUser4,DP_TESTVALUE_STR_4,strlen(DP_TESTVALUE_STR_4))!=0)
  {
    Vu32ErrorCode|=8;
  }
  /************* set end user 0 ******************/
  Vu32ErrorCode|=u32DPSetEndUser(0);    
  /************* get element end user 0 and compare differ from end user 4 ******************/
  if(VmyTestElementSharedStr.s32GetData(VStrUser0,sizeof(VStrUser0))<=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else if(memcmp(VStrUser4,VStrUser0,strlen(DP_TESTVALUE_STR_4))==0)
  {
    Vu32ErrorCode|=20;
  }
  /************* set to default end user 4  => file should be delete ******************/   
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  else
  { /*set end user*/
	  if(DP_s32SetEndUserDefault(4,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=80;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=100;
	  }
  }
  /************* get element for end user 4 => default value ******************/
  if(VmyTestElementSharedStr.s32GetDataEndUser(4,VStrUser4,sizeof(VStrUser4))<=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=200;
  }
  else if(memcmp(VStrUser4,"test's forever",strlen("test's forever"))!=0)
  {
    Vu32ErrorCode|=400;
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserSetAllPoolsToFactoryDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.01.2015
******************************************************************************/
tU32 u32DPEndUserSetAllPoolsToFactoryDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU8                                            Vu8EndUser=0;
  tU8                                            Vu8Value=0;
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;

  /************* do until end user 10: write value  ******************/   
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //store element
    if(VmyTestElementU8.s32SetData(Vu8EndUser)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1;
	  }
	  //get element and compare
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  else if(Vu8Value!=Vu8EndUser)
	  {
	    Vu32ErrorCode|=4;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
  /************* set to factory default  ******************/   
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  else
  { /*set end user*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_TEF,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=80;
	  }
    /*******unlock  DP_U32_LOCK_MODE_END_USER******/
    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=100;
    }
  }
  /************* do until end user 10: read default  ******************/ 
  Vu8EndUser=0;
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //get element and compare
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }
	  else if(Vu8Value!=9)
	  {
	    Vu32ErrorCode|=40;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
#endif
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSetAllPoolsToUserDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.01.2015
******************************************************************************/
tU32 u32DPEndUserSetAllPoolsToUserDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU8                                            Vu8EndUser=0;
  tU8                                            Vu8Value=0;
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;

  /************* do until end user 10: write value  ******************/   
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //store element
    if(VmyTestElementU8.s32SetData(Vu8EndUser+10)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1;
	  }
	  //get element and compare
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  else if(Vu8Value!=Vu8EndUser+10)
	  {
	    Vu32ErrorCode|=4;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
  /************* set to factory default  ******************/   
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  else
  { /*set to default*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=80;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=100;
	  }
  }
  /************* do until end user 10: read default  ******************/ 
  Vu8EndUser=0;
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //get element and compare
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }
	  else if(Vu8Value!=9)
	  {
	    Vu32ErrorCode|=40;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
  /************* do until end user 10: set new value ******************/ 
  Vu8EndUser=0;
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //store element
    if(VmyTestElementU8.s32SetData(Vu8EndUser)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=100;
  	}
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
#endif
#endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserGetNoCurrentEndUserValue()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   27.01.2015
******************************************************************************/
tU32 u32DPEndUserGetNoCurrentEndUserValue(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU8                                            Vu8EndUser=0;
  tU32                                           Vu32Value=0;
  dp_tclPddDpTestEndUserTestEndUserShared        VmyTestElementSharedU32;
  /************* do until end user 10: write value  ******************/   
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //store element
    if(VmyTestElementSharedU32.s32SetData((tU32)(Vu8EndUser+100))!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1;
	  }
	  //get element and compare
	  if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  else if(Vu32Value!=(tU32)(Vu8EndUser+100))
	  {
	    Vu32ErrorCode|=4;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<10);
  /************* do until read end user not current  ******************/  
  Vu8EndUser=0;
  do
  {
    if(VmyTestElementSharedU32.s32GetDataEndUser(Vu8EndUser,Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=8;
	  }
	  else if(Vu32Value!=(tU32)(Vu8EndUser+100))
	  {
	    Vu32ErrorCode|=10;
	  }
    Vu8EndUser++;
  }while(Vu8EndUser<10);
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserOtherProcessSetEndUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   10.02.2015
******************************************************************************/
tU32 u32DPEndUserOtherProcessSetEndUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet=0xaa;
  tU8                                            Vu8ValueGet;

  //set end user 0
  Vu32ErrorCode=u32DPProcessTest(eCmdSetUserWithLock,NULL,NULL,0);
  //set element
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //set end user 1
  Vu32ErrorCode|=u32DPProcessTest(eCmdSetUserWithLock,NULL,NULL,1);
  //get element
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  else
  {
    if(Vu8ValueGet==Vu8ValueSet)
	  {
	    Vu32ErrorCode|=40;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserOtherProcessLockSetEndUserUnlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   10.02.2015
******************************************************************************/
tU32 u32DPEndUserOtherProcessLockSetEndUserUnlock(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet=0xaa;
  tU8                                            Vu8ValueGet;
  //lock all end user pools
  Vu32ErrorCode=u32DPProcessTest(eCmdLock,NULL,NULL,0);
  //set element
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)==DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  //get element
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)==DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  //set end user
  Vu32ErrorCode=u32DPProcessTest(eCmdSetUser,NULL,NULL,5);
  //set element
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)==DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  //get element
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)==DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=80;
  }
  //unlock all end user pools
  Vu32ErrorCode=u32DPProcessTest(eCmdUnlock,NULL,NULL,0);
  //set element
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100;
  }
  //get element
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=200;
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserCopyToCurrentUserNotAllowed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   01.08.2016
******************************************************************************/
tU32 u32DPEndUserCopyToCurrentUserNotAllowed(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet=0xba;

  /*set end user 1*/
  Vu32ErrorCode|=u32DPSetEndUser(1);
  if(Vu32ErrorCode==0)
  { /*store element*/
    if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
    if(s32DPCopyEndUser(0,1)!=DP_S32_ERR_NOT_ALLOWED)
    {
       Vu32ErrorCode|=20;
    }
  } 
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserCopyFromCurrentUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   01.08.2016
******************************************************************************/
tU32 u32DPEndUserCopyFromCurrentUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet=22;
  tU8                                            Vu8ValueGet;

  /*set end user 3*/
  Vu32ErrorCode|=u32DPSetEndUser(3);
  /*store element*/
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  /*set end user 2*/
  Vu32ErrorCode|=u32DPSetEndUser(2);
  /*store element*/
  Vu8ValueSet=88;
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  /*copy current end user to 3*/
  if(s32DPCopyEndUser(2,3)!=DP_S32_NO_ERR)
  {
     Vu32ErrorCode|=40;
  }
  /*set end user 3*/
  Vu32ErrorCode|=u32DPSetEndUser(3);
  /*get element and compare*/
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=80;
  }
  else
  {
     if(Vu8ValueGet!=Vu8ValueSet)
     {
       Vu32ErrorCode|=100;
     }
  }                      
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserCopyFromFileNotCurrentUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   01.08.2016
******************************************************************************/
tU32 u32DPEndUserCopyFromFileNotCurrentUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet=11;
  tU8                                            Vu8ValueGet;

  /*set end user 0*/
  Vu32ErrorCode|=u32DPSetEndUser(0);
  /*store element*/
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  /*set end user 1*/
  Vu32ErrorCode|=u32DPSetEndUser(1);
  /*store element*/
  Vu8ValueSet=22;
  if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=20;
  }
  /*copy 0 to 1  => error*/
  if(s32DPCopyEndUser(0,1)==DP_S32_NO_ERR)
  {
     Vu32ErrorCode|=40;
  }
  /*copy 0 to 2 */
  if(s32DPCopyEndUser(0,2)!=DP_S32_NO_ERR)
  {
     Vu32ErrorCode|=80;
  }
  /*set end user 2*/
  Vu32ErrorCode|=u32DPSetEndUser(2);
  /*get element and compare*/
  if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=80;
  }
  else
  {
     if(Vu8ValueGet!=11 /*value of end user 1*/)
     {
       Vu32ErrorCode|=100;
     }
  }                      
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserCopySwitchEndUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   01.08.2016
******************************************************************************/
tU32 u32DPEndUserCopySwitchEndUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet;
  tU8                                            Vu8ValueGet;
  tU8                                            VubInc;

  /*set value for end user 0 to 9*/
  for(VubInc=0;VubInc<10;VubInc++)
  { /*set end user VubInc*/
    Vu8ValueSet=50;
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    /*store element*/
    Vu8ValueSet+=VubInc;
    if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=(10+VubInc);
    }
  }
  /*set end user 10*/
  Vu32ErrorCode|=u32DPSetEndUser(10);
  /*copy user 9 -> 0 8 -> 1 ....*/
  tU8 Pu8UserFrom=9;
  for(VubInc=0;VubInc<10;VubInc++)
  {  
    if(s32DPCopyEndUser(Pu8UserFrom,VubInc)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=(40+VubInc);
    }
    Pu8UserFrom--;
  }
  /*set end user and read value value*/
  for(VubInc=0;VubInc<10;VubInc++)
  { /*set end user VubInc*/
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    /*read element*/
    if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=(80+VubInc);
    }
    else
    {
      if(VubInc<5)
        Vu8ValueSet=59-VubInc;
      else
        Vu8ValueSet=50+VubInc; 
      if(Vu8ValueGet!=Vu8ValueSet)
      {
         Vu32ErrorCode|=(200+VubInc);
         fprintf(stderr, "Vu8ValueSet: %d, Vu8ValueGet: %d\n",Vu8ValueSet,Vu8ValueGet);
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserCopySetDefaultCopyReadDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   01.08.2016
******************************************************************************/
tU32 u32DPEndUserCopySetDefaultCopyReadDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU8                                            VubInc;
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  /*set all pools to default*/
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  { /*set default user*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }	
	  if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40;
    }
  }   
  if(Vu32ErrorCode==0)
  { /*set end user 10*/
    Vu32ErrorCode|=u32DPSetEndUser(10);
  }
  if(Vu32ErrorCode==0)
  { 
    /*copy user 9 -> 0 8 -> 1 ....*/
    tS32 Vs32ErrorCode=DP_S32_NO_ERR;
    tU8  Vu8UserFrom=9;
    for(VubInc=0;VubInc<10;VubInc++)
    {  
      Vs32ErrorCode=s32DPCopyEndUser(Vu8UserFrom,VubInc);
      if(Vs32ErrorCode!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=(80+VubInc);
      }
      Vu8UserFrom--;
    }
  }  
  if(Vu32ErrorCode==0)
  { /*read default values */
    tU8                                            Vu8ValueGet; 
    for(VubInc=0;VubInc<10;VubInc++)
    {  
      Vu32ErrorCode|=u32DPSetEndUser(VubInc);
      /*read element*/
      if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=(400+VubInc);
      }
      else if(Vu8ValueGet!=9)
      {
        Vu32ErrorCode|=(800+VubInc);
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankLoadBankToCurrentEndUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   30.05.2015
******************************************************************************/
tU32 u32DPEndUserBankLoadBankToCurrentEndUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();

  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tU16    Vu16Value=DP_TESTVALUE_TU16;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankTrTestEndUserBankArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedtU32    VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedString  VmyTestElementSharedStr;
  /* load bank 2 to current user*/
  Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(2);  
  /* change values differ from default*/
  if(Vu32ErrorCode==0)
  {
    if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=1;
    }
	  if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4;
	  }
	  if(VmyTestElementSharedU32.s32SetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=8;
	  }
	  if(VmyTestElementSharedArray.s32SetData(&Vu16Value,1)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=10;
	  }
	  memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
	  if(VmyTestElementSharedStr.s32SetData(VStr)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }
    /* load bank 3 to current user*/
    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(3);  
	  /*get default value and compare*/
	  if(Vu32ErrorCode==0)
	  {
	    if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
	    {
	      Vu32ErrorCode|=40;
	    }
	    else if(Vu32Value!=3)
	    {
	      Vu32ErrorCode|=80;
	    }
	    if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=100;
	    }
	    else if(VbValue!=FALSE)
	    {
	      Vu32ErrorCode|=200;
	    }
	    if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=800;
	    }
	    else if(Vu8Value!=13)
	    {
	      Vu32ErrorCode|=1000;
	    }
	    if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=2000;
	    }
	    else if(Vu32Value!=0x44444444)
	    {
	      Vu32ErrorCode|=4000;
	    }
	    if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=0)  //for bank 3 no value default value given
	    {
	      Vu32ErrorCode|=8000;
	    }	
	    if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=10000;
	    }
	    else if(memcmp(VStr,"test4",strlen("test4"))!=0)
	    {
	      Vu32ErrorCode|=20000;
	    }
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSaveCurrentEndUserToBank()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   30.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSaveCurrentEndUserToBank(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tU16    Vu16Value=DP_TESTVALUE_TU16;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankTrTestEndUserBankArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedtU32    VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedString  VmyTestElementSharedStr;
  /*-- load bank 2 to current user*/
  Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(2);  
  /*-- change values differ from default*/
  if(Vu32ErrorCode==0)
  {
    if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
    {
     Vu32ErrorCode|=1;
    }
    if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=2;
    }
    if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=4;
    }
    if(VmyTestElementSharedU32.s32SetData(Vu32Value)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=8;
    }
    if(VmyTestElementSharedArray.s32SetData(&Vu16Value,1)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
	  memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
	  if(VmyTestElementSharedStr.s32SetData(VStr)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }
  }
  /*-- save current value to bank 1 */
  Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(1);
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetValueSaveToBankLoadSavedBank()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   30.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSetValueSaveToBankLoadSavedBank(void)
{//=> after save bank; load other bank; compare vaules before set bank and load bank 
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tU16    Vu16Value=DP_TESTVALUE_TU16;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankTrTestEndUserBankArray       VmyTestElementArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankFlag          VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedtU32    VmyTestElementSharedU32;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedArray   VmyTestElementSharedArray;
  dp_tclPddDpTestEndUserBankTestEndUserBankSharedString  VmyTestElementSharedStr;
  /*-- load bank 2 to current user*/
  Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(2);  
  /*-- change values differ from default*/
  if(Vu32ErrorCode==0)
  {
    if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
    {
     Vu32ErrorCode|=1;
    }
	  if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4;
	  }
	  if(VmyTestElementSharedU32.s32SetData(Vu32Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=8;
	  }
	  if(VmyTestElementSharedArray.s32SetData(&Vu16Value,1)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=10;
	  }
    memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
    if(VmyTestElementSharedStr.s32SetData(VStr)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
	  /*-- save current value to bank 0 */
	  Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(0);
	  /*-- load bank 3 */
	  if(Vu32ErrorCode==0)
	  {
	    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(3);  
	    /* -- check default value bank 3*/
	    if(Vu32ErrorCode==0)
	    { 
	      if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
	      {
	        Vu32ErrorCode|=40;
	      }
	      else if(Vu32Value!=3)
	      {
	        Vu32ErrorCode|=80;
	      }
	      if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=100;
	      }
	      else if(VbValue!=FALSE)
	      {
	        Vu32ErrorCode|=200;
	      }
	      if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=400;
	      }
	      else if(Vu8Value!=13)
	      {
	        Vu32ErrorCode|=800;
	      }
	      if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=1000;
	      }
	      else if(Vu32Value!=0x44444444)
	      {
	        Vu32ErrorCode|=2000;
	      }
	      if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=0)  //for bank 3 no value default value given
	      {
	        Vu32ErrorCode|=4000;
	      }	
	      if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=8000;
	      }
	      else if(memcmp(VStr,"test4",strlen("test4"))!=0)
	      {
	        Vu32ErrorCode|=10000;
	      }
        /*-- load bank 0*/
	      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(0);  
	      /*check value */
		    if(Vu32ErrorCode==0)
	      {
	        if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
	        {
	          Vu32ErrorCode|=20000;
	        }
	        else if(Vu32Value!=DP_TESTVALUE_TU32)
	        {
	          Vu32ErrorCode|=40000;
	        }
	        if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
	        {
	          Vu32ErrorCode|=80000;
	        }
	        else if(VbValue!=DP_TESTVALUE_FLAG)
	        {
	          Vu32ErrorCode|=100000;
	        }
	        if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	        {
	          Vu32ErrorCode|=200000;
	        }
	        else if(Vu8Value!=DP_TESTVALUE_TU8)
	        {
	          Vu32ErrorCode|=400000;
	        }
	        if(VmyTestElementSharedU32.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
	        {
	          Vu32ErrorCode|=800000;
	        }
	        else if(Vu32Value!=DP_TESTVALUE_TU32)
	        {
	          Vu32ErrorCode|=1000000;
	        }
	        if(VmyTestElementSharedArray.s32GetData(&Vu16Value,1)!=1)  //for bank 3 no value default value given
	        {
	          Vu32ErrorCode|=2000000;
	        }
		      else if(Vu16Value!=DP_TESTVALUE_TU16)
		      {
		        Vu32ErrorCode|=4000000;
		      }
	        if(VmyTestElementSharedStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
	        {
	          Vu32ErrorCode|=8000000;
	        }
	        else if(memcmp(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR))!=0)
	        {
	          Vu32ErrorCode|=80000000;
	        }
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankGetBank()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   30.05.2015
******************************************************************************/
tU32 u32DPEndUserBankGetBank(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU8 Vu8BankLoad;
  tU8 Vu8BankGet;
  //load for each bank
  for(Vu8BankLoad=0;Vu8BankLoad<4;Vu8BankLoad++)
  {//load bank
    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(Vu8BankLoad);
    if(Vu32ErrorCode!=0)
      break;
    //save bank
    Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(Vu8BankLoad+1);
    if(Vu32ErrorCode!=0)
      break;
    //get bank
    if(DP_s32GetBank(Vu8BankGet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=Vu8BankLoad+10;
	    break;
    }
    else
    {//check bank    
      if(Vu8BankGet!=Vu8BankLoad)
	    {
	      Vu32ErrorCode=Vu8BankLoad+20;
	      break;
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetEndGreatUserLoadBank() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.02.2016
******************************************************************************/
tU32 u32DPEndUserBankSetEndGreatUserLoadBank(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU8 Vu8BankGet=0;

  //set end user greater 20
  Vu32ErrorCode|=u32DPSetEndUser(20);
  if(Vu32ErrorCode==0)
  { //get end user
    tU8 Vu8CurrentEndUser=0xff;
    if(DP_s32GetEndUser(Vu8CurrentEndUser)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10;
    }
    else
    {//check end user
      if(Vu8CurrentEndUser!=20)
      {
        Vu32ErrorCode|=20;
      }
    }
    if(Vu32ErrorCode==0)
    { //load bank 3 
      Vu32ErrorCode=u32OEDTDPLoadBankToCurrentEndUser(3);  
      //get bank
      if(DP_s32GetBank(Vu8BankGet)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode=100;
      }
      else
      {
        if(Vu8BankGet!=3)
        {
          Vu32ErrorCode=200;
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetBankDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   30.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSetBankDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanks();
  if(Vu32ErrorCode==0)
  {// set bank 2 to default
    Vu32ErrorCode|=u32OEDTDP_s32SetBankDefault(2);
	  if(Vu32ErrorCode==0)
    {// load bank 2
	    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	    if(Vu32ErrorCode==0)
      { 
	      Vu32ErrorCode|=u32OEDTDP_s32CheckDefaultBank2();
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetAllBanksDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSetAllBanksDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  tU8 Vu8Bank;
  //set all banks and user
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanks();
  if(Vu32ErrorCode==0)
  { //set end user 2
	  Vu32ErrorCode|=u32DPSetEndUser(2);
    //for all banks of the current end user
    for(Vu8Bank=0;Vu8Bank<4;Vu8Bank++)
	  {//set to default
	    if(Vu32ErrorCode==0)
	    {
	      Vu32ErrorCode|=u32OEDTDP_s32SetBankDefault(Vu8Bank);
      }
    }
    if(Vu32ErrorCode==0)
	  { //load bank 2 and check default value
	    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	    if(Vu32ErrorCode==0)
      {
	      Vu32ErrorCode|=u32OEDTDP_s32CheckDefaultBank2();
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetEndUserDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSetEndUserDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  //set all banks and user
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanks();
  if(Vu32ErrorCode==0)
  { //set end user 1
	  Vu32ErrorCode|=u32DPSetEndUser(1);
	  //set end user 2 to default
    /* lock*/
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10000;
    }
    else
    { /*set end user*/
	    if(DP_s32SetEndUserDefault(2,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=20000;
	    }	 
	    /*unlock  DP_U32_LOCK_MODE_END_USER*/
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode|=40000;
	    }
    }
    if(Vu32ErrorCode==0)
	  { //set end user 2
	    Vu32ErrorCode|=u32DPSetEndUser(2);
	    if(Vu32ErrorCode==0)
	    {//load bank 2 and check default value
	      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	      if(Vu32ErrorCode==0)
        {
	        Vu32ErrorCode|=u32OEDTDP_s32CheckDefaultBank2();
	      }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetUserDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPEndUserBankSetUserDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  //set all banks and user
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanks();
  if(Vu32ErrorCode==0)
  {/*set all user pools to default*/
    if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40;
    }
    else
    { /*set default user*/      
	    if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=80;
	    }
	    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=100;
      }
    }
	  //check default value
	  if(Vu32ErrorCode==0)
	  { //set end user 2
	    Vu32ErrorCode|=u32DPSetEndUser(2);
	    if(Vu32ErrorCode==0)
	    {//load bank 2 and check default value
	      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	      if(Vu32ErrorCode==0)
        {
	        Vu32ErrorCode|=u32OEDTDP_s32CheckDefaultBank2();
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSwitchbackBankAction()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   18.05.2016
******************************************************************************/
tU32 u32DPEndUserSwitchbackBankAction(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  tU8                                                    Vu8Value;
  // set end user 1
  Vu32ErrorCode|=u32DPSetEndUser(1);
  // set element
  Vu8Value=222;
  if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=1;
	}
  if(Vu32ErrorCode==0)
  {// set end user 2
    Vu32ErrorCode|=u32DPSetEndUser(2);  
    // save bank
    Vu32ErrorCode=u32OEDTDPSaveCurrentEndUserToBank(0);
    // load bank
    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
    if(Vu32ErrorCode==0)
    {// set end user 1
      Vu32ErrorCode|=u32DPSetEndUser(1);
      if(Vu32ErrorCode==0)
      {// get element and check
        Vu8Value=0;
        if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=800;
        }
        else if(Vu8Value!=222)
        {
          Vu32ErrorCode|=1000;
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSwitchbackUserDefault()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   18.05.2016
******************************************************************************/
tU32 u32DPEndUserSwitchbackUserDefault(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  tU8                                                    Vu8Value;
  // set end user 1
  Vu32ErrorCode|=u32DPSetEndUser(3);
  // set element
  Vu8Value=111;
  if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=1;
	}
  if(Vu32ErrorCode==0)
  {// set end user 2
    Vu32ErrorCode|=u32DPSetEndUser(2);  
    // set end user 2 to default   
    /* lock*/
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10000;
    }
    else
    { /*set end user*/
	    if(DP_s32SetEndUserDefault(2,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=20000;
	    }	 
	    /*unlock  DP_U32_LOCK_MODE_END_USER*/
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode|=40000;
	    }
    }
    if(Vu32ErrorCode==0)
    {// set end user 1
      Vu32ErrorCode|=u32DPSetEndUser(3);
      if(Vu32ErrorCode==0)
      {// get element and check
        Vu8Value=0;
        if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=800;
        }
        else if(Vu8Value!=111)
        {
          Vu32ErrorCode|=1000;
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankLoadSwitchUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   26.05.2016
******************************************************************************/
tU32 u32DPEndUserBankLoadSwitchUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  // set user default
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  else
  { /*set default user*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=80;
	  }	
	  if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=100;
    }
  }

  // TestClass::add(new TC_setUserX(1));
  Vu32ErrorCode|=u32DPSetEndUser(1);
  // TestClass::add(new TC_setBankX_setbankDPY(1,101));
  Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1);
  if(VmyTestElementU8.s32SetData(101)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=1;
	}
  // TestClass::add(new TC_saveBankX(1));
  Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(1);
  // TestClass::add(new TC_setBankX_setbankDPY(2,102));
  Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
  if(VmyTestElementU8.s32SetData(102)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=1;
	}
  // TestClass::add(new TC_saveBankX(2));
  Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(2);
  // TestClass::add(new TC_setBankX(1));
  Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1);
 
  if(Vu32ErrorCode==0)
  {
   // TestClass::add(new TC_setUserX(2));
   Vu32ErrorCode|=u32DPSetEndUser(2);
   // TestClass::add(new TC_setBankX_setbankDPY(1,201));
   Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1);
   if(VmyTestElementU8.s32SetData(201)!=DP_S32_NO_ERR)
	 {
	   Vu32ErrorCode|=1;
	 }
   // TestClass::add(new TC_saveBankX(1));
   Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(1);;
   // TestClass::add(new TC_setBankX_setbankDPY(2,202));
   Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
   if(VmyTestElementU8.s32SetData(202)!=DP_S32_NO_ERR)
	 {
	   Vu32ErrorCode|=1;
	 }
   // TestClass::add(new TC_saveBankX(2));
   Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(2);
   // TestClass::add(new TC_setBankX(1));
   Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1); 
  }

  // check
  if(Vu32ErrorCode==0)
  {
    tU8 Vu8Value;
    tU8 Vu8Bank=0;
    // TestClass::add(new TC_setUserX(1));
    Vu32ErrorCode|=u32DPSetEndUser(1);
    // TestClass::add(new TC_is_BankIDX(1));
    DP_s32GetBank(Vu8Bank);
    if(Vu8Bank!=1)
    {
      Vu32ErrorCode|=200;
    }
    // TestClass::add(new TC_is_BankDPX(101));
    if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=8;
    }
    else if(Vu8Value!=101)
    {
      Vu32ErrorCode|=10;
    }
    // TestClass::add(new TC_setUserX(2));
    Vu32ErrorCode|=u32DPSetEndUser(2);
    // TestClass::add(new TC_is_BankIDX(1));
    Vu8Bank=0;
    DP_s32GetBank(Vu8Bank);
    if(Vu8Bank!=1)
    {
      Vu32ErrorCode|=200;
    }
    // TestClass::add(new TC_is_BankDPX(201));
    if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=20;
    }
    else if(Vu8Value!=201)
    {
      Vu32ErrorCode|=40;
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserBankSetBankDefaultGetSwitchEndUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   26.05.2016
******************************************************************************/
tU32 u32DPEndUserBankSetBankDefaultGetSwitchEndUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
  Vu32ErrorCode|=u32DPSetEndUser(2);
  //load bank 1 and check default value
	Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1);
  if (Vu32ErrorCode==0)
  {//change value to 201
    if(VmyTestElementU8.s32SetData(201)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1;
	  }
    else
    {
      Vu32ErrorCode|=u32DPSetEndUser(1);
      //load bank 1 
      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(1);
      // set bank to default
      Vu32ErrorCode|=u32OEDTDP_s32SetBankDefault(1);
      Vu32ErrorCode|=u32DPSetEndUser(2);
      if(Vu32ErrorCode==0)
      {  //read value 201
        tU8 Vu8Value;
        if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode=22;
        }
        else
        {
          if(Vu8Value!=201)
          {
            Vu32ErrorCode=33;
          }
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/****************************************************************************************************/
/*                     test default for bank and user                                               */
/****************************************************************************************************/
/*****************************************************************************
* FUNCTION:		u32DPSetDefault_TefUser()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   16.12.2015
******************************************************************************/
tU32 u32DPSetDefault_TefUser(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanks();
  /************* set to user default  ******************/   
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40000;
  }
  else
  { /*set to default*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=80000;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=100000;
	  }
  }
  //get actual bank
  tU8 Vu8Bank=0xff;
  if(DP_s32GetBank(Vu8Bank)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=200000;
  }
  else
  {//check bank
    if(Vu8Bank!=0)
    {
	    Vu32ErrorCode|=300000;
	  }
  }
  //get actual end user
  tU8 Vu8CurrentEndUser=0xff;
  if(DP_s32GetEndUser(Vu8CurrentEndUser)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=400000;
  }
  else
  {//check end user
    if(Vu8CurrentEndUser!=0)
    {
	    Vu32ErrorCode|=800000;
	  }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetDefault_EndUserCurrent()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   16.12.2015
******************************************************************************/
tU32 u32DPSetDefault_EndUserCurrent(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK 
  Vu32ErrorCode=u32OEDTDP_s32SetAllUsersAndBanks();
  if(Vu32ErrorCode==0)
  {// load bank 2
	  Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
    if(Vu32ErrorCode==0)
    {//get actual end user
      tU8 Vu8CurrentEndUser=0xff;
      if(DP_s32GetEndUser(Vu8CurrentEndUser)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=400000;
      }     
      /************* set actual end user to default  ******************/   
      /* lock*/
      if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=40000;
      }
      else
      { /*set to default*/
	      if(DP_s32SetEndUserDefault(Vu8CurrentEndUser,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=20000;
	      }	 
	      /*unlock  DP_U32_LOCK_MODE_END_USER*/
        if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
	      {
          Vu32ErrorCode|=100000;
	      }
      }       
      //get actual bank
      tU8 Vu8Bank=0xff;
      if(DP_s32GetBank(Vu8Bank)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=200000;
      }
      else
      {//check bank
        if(Vu8Bank!=0)
        {
	        Vu32ErrorCode|=300000;
	      }
      }
      //get actual value
      tU8 Vu8Value;
      dp_tclPddDpTestEndUserBankTestEndUserBanktU8           VmyTestElementU8;
      if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=800;
      }
      else if(Vu8Value!=9)
      {
        Vu32ErrorCode|=1000;
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPSetDefault_EndUserNotCurrent()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   02.02.2016
******************************************************************************/
tU32 u32DPSetDefault_EndUserNotCurrent(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  tU8                                            Vu8EndUser=0;
  tU8                                            Vu8Value=0;
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;

  /************* do until end user 4: write value  ******************/   
  do
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8EndUser);   
	  //store element
    if(VmyTestElementU8.s32SetData(Vu8EndUser+10)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=1;
	  }
	  //get element and compare
	  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=2;
	  }
	  else if(Vu8Value!=Vu8EndUser+10)
	  {
	    Vu32ErrorCode|=4;
	  }
	  Vu8EndUser++;
  }while(Vu8EndUser<4);
  /************* set end user 2 to default  ******************/   
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=40;
  }
  else
  { /*set to default*/
	  if(DP_s32SetEndUserDefault(2,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)	
	  {
	    Vu32ErrorCode|=80;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=100;
	  }
  }
  /************* read default value from user 2 ******************/ 
  //set end user
  Vu32ErrorCode|=u32DPSetEndUser(2);   
	//get element and compare
	if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=200;
	}
	else if(Vu8Value!=9)
	{
	  Vu32ErrorCode|=400;
	}
#endif
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUserDefault_CurrentUser1()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   10.05.2016
******************************************************************************/
tU32 u32DPSetUserDefault_CurrentUser1(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  Vu32ErrorCode=u32DPSetUserDefault_CurrentUserDifferent(1);
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUserDefault_CurrentUser3()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   10.05.2016
******************************************************************************/
tU32 u32DPSetUserDefault_CurrentUser3(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  Vu32ErrorCode=u32DPSetUserDefault_CurrentUserDifferent(3);
  #endif
  #endif
  return(Vu32ErrorCode);
}

static tU32 u32DPSetUserDefault_CurrentUserDifferent(tU8 Pu8CurrentUserChange)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8Value;
  // set to user default
  if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10;
  }
  else
  { /*set default user*/
	  if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20;
	  }	
	  if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40;
    }
  }   
  // set element end user 0
  if(VmyTestElementU8.s32SetData(33)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=60;
	}
  // set element end user 0
  if(VmyTestElementU8.s32SetData(11)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=80;
	}
  // set end user 1
  Vu32ErrorCode|=u32DPSetEndUser(Pu8CurrentUserChange); 
  // set element end user 1
  if(VmyTestElementU8.s32SetData(44)!=DP_S32_NO_ERR)
	{
	  Vu32ErrorCode|=100;
	}
  if(Vu32ErrorCode==0)
  {
    // get element
    if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200;
	  }
	  else if(Vu8Value!=44)
	  {
	    Vu32ErrorCode|=400;
    }
    // set to user default
    if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=800;
    }
    else
    { /*set default user*/
	    if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=1000;
	    }	
	    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=2000;
      }
    }   
    // get element end user 0
    if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=4000;
	  }
	  else if(Vu8Value!=9)
	  {
	    Vu32ErrorCode|=8000;
    }
    // set end user 1
    Vu32ErrorCode|=u32DPSetEndUser(1); 
    // get element end user 1
    if(VmyTestElementU8.s32GetData(Pu8CurrentUserChange)!=DP_S32_NO_ERR)
	  {     
	    Vu32ErrorCode|=10000;
	  }
	  else if(Vu8Value!=9)
	  {
	    Vu32ErrorCode|=20000;
    }
  }
  #endif
  #endif

  return(Vu32ErrorCode);
}
/****************************************************************************************************/
/*                     test default for elements with secure                                        */
/****************************************************************************************************/
#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANKSECURE 
//helper function for set all users and banks 
static tU32 u32OEDTDP_s32SetAllUsersAndBanksForPoolSecure(void)
{
  tU32    Vu32ErrorCode=0;
  tU8     Vu8Bank;
  tU8     Vu8User;
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankSecureTestArrayNoSecure     VmyTestElementArray;
  dp_tclPddDpTestEndUserBankSecureTestFlagSecure        VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankSecureTesttU8NoSecure       VmyTestElementU8;
  dp_tclPddDpTestEndUserBankSecureTestStringSecure      VmyTestElementStr;
  //for all user (4)
  for (Vu8User=0;Vu8User<4;Vu8User++)
  { //set end user
    Vu32ErrorCode|=u32DPSetEndUser(Vu8User);
	  //for all banks
	  for(Vu8Bank=0;Vu8Bank<4;Vu8Bank++)
	  {
	    if(Vu32ErrorCode==0)
      {
	      if(VmyTestElementArray.s32SetData(&Vu32Value,1)!=DP_S32_NO_ERR)
        {
          Vu32ErrorCode|=1;
        }
	      if(VmyTestElementFlag.s32SetData(VbValue)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=2;
	      }
	      if(VmyTestElementU8.s32SetData(Vu8Value)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=4;
	      }	    
	      memcpy(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR)+1);
	      if(VmyTestElementStr.s32SetData(VStr)!=DP_S32_NO_ERR)
	      {
	        Vu32ErrorCode|=20;
	      }
		    Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(Vu8Bank);
	    }
	  }
  }
  return(Vu32ErrorCode);
}
//helper function for check value secure 
static tU32 u32OEDTDP_s32CheckValueBankSecure2(void)
{
  tU32    Vu32ErrorCode=0;
  tU32    Vu32Value=DP_TESTVALUE_TU32;
  tBool   VbValue=DP_TESTVALUE_FLAG;
  tU8     Vu8Value=DP_TESTVALUE_TU8;
  char    VStr[100];
  dp_tclPddDpTestEndUserBankSecureTestArrayNoSecure     VmyTestElementArray;
  dp_tclPddDpTestEndUserBankSecureTestFlagSecure        VmyTestElementFlag;
  dp_tclPddDpTestEndUserBankSecureTesttU8NoSecure       VmyTestElementU8;
  dp_tclPddDpTestEndUserBankSecureTestStringSecure      VmyTestElementStr;
  /*get value and compare*/
  if(VmyTestElementArray.s32GetData(&Vu32Value,1)!=1)
  {
    Vu32ErrorCode|=40; 
  }
  else if(Vu32Value!=2)  //default
  {
    Vu32ErrorCode|=80;
  }
  if(VmyTestElementFlag.s32GetData(VbValue)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100;
  }
  else if(VbValue!=DP_TESTVALUE_FLAG)  //set value
  {
    Vu32ErrorCode|=200;
  }
  if(VmyTestElementU8.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=800;
  }
  else if(Vu8Value!=12) //default
  {
    Vu32ErrorCode|=1000;
  }
  if(VmyTestElementStr.s32GetData(VStr,sizeof(VStr))<=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000;
  }
  else if(memcmp(VStr,DP_TESTVALUE_STR,strlen(DP_TESTVALUE_STR))!=0)
  {
    Vu32ErrorCode|=20000;
  }
  return(Vu32ErrorCode);
}
#endif
#endif
/*****************************************************************************
* FUNCTION:		u32DPSetBankDefaultWithElemSecure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPSetBankDefaultWithElemSecure(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANKSECURE 
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanksForPoolSecure();
  if(Vu32ErrorCode==0)
  {// set bank 2 to default
    Vu32ErrorCode|=u32OEDTDP_s32SetBankDefault(2);
	  if(Vu32ErrorCode==0)
    {// load bank 2
	    Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	    if(Vu32ErrorCode==0)
	    {
	      Vu32ErrorCode|=u32OEDTDP_s32CheckValueBankSecure2();
	    }
	  }
  }	 
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetEndUserDefaultWithElemSecure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPSetEndUserDefaultWithElemSecure(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANKSECURE 
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanksForPoolSecure();
  if(Vu32ErrorCode==0)
  { //set end user 1
	  Vu32ErrorCode|=u32DPSetEndUser(1);
	  //set end user 2 to default
    /* lock*/
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=10000;
    }
    else
    { /*set end user*/
	    if(DP_s32SetEndUserDefault(2,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=20000;
	    }	 
	    /*unlock  DP_U32_LOCK_MODE_END_USER*/
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode|=40000;
	    }
    }
    if(Vu32ErrorCode==0)
	  { //set end user 2
	    Vu32ErrorCode|=u32DPSetEndUser(2);
	    if(Vu32ErrorCode==0)
	    {// load bank 2
	      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	      if(Vu32ErrorCode==0)
	      {
	        Vu32ErrorCode|=u32OEDTDP_s32CheckValueBankSecure2();
	      }
	    }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetUserDefaultWithElemSecure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.05.2015
******************************************************************************/
tU32 u32DPSetUserDefaultWithElemSecure(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANKSECURE 
  //set all banks and user
  Vu32ErrorCode|=u32OEDTDP_s32SetAllUsersAndBanksForPoolSecure();
  if(Vu32ErrorCode==0)
  {/*set all user pools to default*/
    if(DP_s32Lock(DP_ACCESS_ID_SPM,DP_U32_LOCK_MODE_SETDEF,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40;
    }
    else
    { /*set default user*/
	    if(DP_s32SetDefault(DP_ACCESS_ID_SPM,DP_DEFSET_USER,8000)!=DP_S32_NO_ERR)
	    {
	      Vu32ErrorCode|=80;
	    }	
	    if(DP_s32Unlock(DP_ACCESS_ID_SPM,5000)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=100;
      }
    }   
	  //check default value
	  if(Vu32ErrorCode==0)
	  { //set end user 2
	    Vu32ErrorCode|=u32DPSetEndUser(2);
	    if(Vu32ErrorCode==0)
	    {//load bank 2 and check default value
	      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(2);
	      if(Vu32ErrorCode==0)
        {
	        Vu32ErrorCode|=u32OEDTDP_s32CheckValueBankSecure2();
	      }
	    }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/****************************************************************************************************/
/*             test for kds interface over Datapool DP_KDS_INTERFACE_2                              */
/****************************************************************************************************/
#ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
//helper function for set KDS value
static tU32 u32DPWriteVehicalInformationToKds(tU8* Ppu8Buffer)
{
  tU32 Vu32ErrorCode=0;
  OSAL_tIODescriptor tIoKdsHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS,OSAL_EN_READWRITE);
  if ( OSAL_ERROR == tIoKdsHandle) 
  {
    Vu32ErrorCode=10000;
  }
  else
  {
    tBool bAccessOption = 1;
    tsKDSEntry sKDSEntryData;
    sKDSEntryData.u16Entry = DP_U16_KDSADR_SWUPD_VARIANTINFO;
    sKDSEntryData.u16EntryLength = DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE;
    sKDSEntryData.u16EntryFlags=M_KDS_ENTRY_FLAG_NONE;
    memcpy(&sKDSEntryData.au8EntryData[0],Ppu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
    if(OSAL_s32IOControl(tIoKdsHandle,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(intptr_t)bAccessOption) == OSAL_ERROR)
    {
       Vu32ErrorCode=20000;
    }
    else if(OSAL_s32IOWrite(tIoKdsHandle,(tPS8)&sKDSEntryData,(tS32)sizeof(sKDSEntryData))==OSAL_ERROR)
	  {
	    Vu32ErrorCode=40000;
	  }
    (tVoid)OSAL_s32IOClose(tIoKdsHandle);
  }  
  return(Vu32ErrorCode);
}
#endif
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemWrongParamString()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemWrongParamString(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
#ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8Buffer[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};
  tS32 Vs32ReturnCode=DP_s32GetConfigItem("Wrong1","Wrong2",Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT)
  {
    Vu32ErrorCode=1;
  }
  Vs32ReturnCode=DP_s32GetConfigItem(NULL,NULL,Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  if(Vs32ReturnCode!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode|=2;
  }
  Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","Wrong2",Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE,FALSE);
  if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT)
  {
    Vu32ErrorCode|=4;
  }
#endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemBufferNull()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemBufferNull(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tS32 Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",NULL,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  if(Vs32ReturnCode!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32ErrorCode=1;
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemWrongSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemWrongSize(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8Buffer[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};
  //set infomation
  memset(Vu8Buffer,0x22,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  Vu32ErrorCode=u32DPWriteVehicalInformationToKds(Vu8Buffer);
  if(Vu32ErrorCode==0)
  {
    tS32 Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",Vu8Buffer,0);
    if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT_READ)
    {
      Vu32ErrorCode=1;
    }
    Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",Vu8Buffer,1);
    if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT_READ)
    {
      Vu32ErrorCode|=2;
    }
    Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",Vu8Buffer,4);
    if(Vs32ReturnCode!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=4;
    }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemCompleteWriteGetCmp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemCompleteWriteGetCmp(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8BufferWrite[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE];
  tU8   Vu8BufferGet[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};

  memset(Vu8BufferWrite,0x65,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  //set infomation 
  Vu32ErrorCode=u32DPWriteVehicalInformationToKds(Vu8BufferWrite);
  if(Vu32ErrorCode==0)
  {
    tS32 Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","",Vu8BufferGet,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
    if(Vs32ReturnCode!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=1;
    }
	  else
	  {//compare value
	    if(memcmp(Vu8BufferGet,Vu8BufferWrite,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE)!=0)  
	    {
	       Vu32ErrorCode=2;
	    }
	  }
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemArrayWriteGetCmp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemArrayWriteGetCmp(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8BufferWrite[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE];
  tU8   Vu8BufferGet[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};

  memset(Vu8BufferWrite,0x5a,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  //set infomation 
  Vu32ErrorCode=u32DPWriteVehicalInformationToKds(Vu8BufferWrite);
  if(Vu32ErrorCode==0)
  {
    tS32 Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",Vu8BufferGet,4);
    if(Vs32ReturnCode!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=1;
    }
	  else
	  {//compare value
	    if(memcmp(Vu8BufferGet,Vu8BufferWrite,4)!=0)  
	    {
	     Vu32ErrorCode=Vu8BufferGet[1];
	    }
	  }
  }
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemByteWriteGetCmp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   28.04.2015
******************************************************************************/
tU32 u32DPs32GetConfigItemByteWriteGetCmp(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8BufferWrite[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE];
  tU8   Vu8BufferGet[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};
  //set infomation 
  memset(Vu8BufferWrite,0x22,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  Vu32ErrorCode=u32DPWriteVehicalInformationToKds(Vu8BufferWrite);
  if(Vu32ErrorCode==0)
  {
    tS32 Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","ConfigurationVersion",Vu8BufferGet,1);
    if(Vs32ReturnCode!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=1;
    }
	  else
	  {//compare value
	    if(Vu8BufferGet[0]!=0x22)  
	    {
	      Vu32ErrorCode=Vu8BufferGet[0];
	    }
	  }   
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetConfigItemFromTestProcessProcdatapool5()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   08.06.2016
******************************************************************************/
tU32 u32DPs32GetConfigItemFromTestProcessProcdatapool5(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  Vu32ErrorCode=u32DPProcessTest(eCmdGetConfigItemWithoutDatapool,NULL,NULL,0);
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32GetRegistryFromTestProcessProcdatapool6()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   08.06.2016
******************************************************************************/
tU32 u32DPs32GetRegistryFromTestProcessProcdatapool6(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  Vu32ErrorCode=u32DPProcessTest(eCmdGetRegistryWithoutDatapool,NULL,NULL,0);
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32StorePoolNow()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.06.2015
******************************************************************************/
tU32 u32DPs32StorePoolNow(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  dp_tclSrvIf VdpSrvIf;
  dp_tclPddDpTestLocationFileTrTestFiletU8            VElement;
  tU32                                                Vu32PoolId=DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE;
  tU8                                                 Vu8Test=0x66;
  if(VElement.s32SetData(Vu8Test)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=1;
  }
  if(VdpSrvIf.s32StorePoolNow(Vu32PoolId)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=2;
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPs32StorePoolNowWrongAccess()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.06.2015
******************************************************************************/
tU32 u32DPs32StorePoolNowWrongAccess(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_PDDDPTESTLOCATIONFILE
  dp_tclSrvIf VdpSrvIf;
  tU32        Vu32WrongPoolId=0x12345678;
  if(VdpSrvIf.s32StorePoolNow(Vu32WrongPoolId)!=DP_S32_ERR_ACCESS_RIGHT)
  {
    Vu32ErrorCode=1;
  }
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPProcessExitWithOsalExitFunction()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.06.2015
******************************************************************************/
tU32 u32DPProcessExitWithOsalExitFunction(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  //exit process with osal function
  Vu32ErrorCode=u32DPProcessTest(eCmdProcess1End,NULL,NULL,0);
  if(Vu32ErrorCode==0)
  { //lock unlock
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=1;
    }
    else
    {
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode=2;
	    }
    }
#if 0
	  //start process 1
	  {
	    OSAL_trProcessAttribute	VprAtr 	  = {OSAL_NULL};
	    VprAtr.szCommandLine  = NULL;
	    VprAtr.u32Priority    = PRIORITY;
	    VprAtr.szName="procdatapool1_out.out";
	    VprAtr.szAppName="/opt/bosch/base/bin/procdatapool1_out.out";     
      vprID[0] = OSAL_ProcessSpawn(&VprAtr);
	    sleep(5);
	  }
#endif
  }
  #endif
  return(Vu32ErrorCode);
}

#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
static tU32 u32PrepareProcess3ForTestEndUserNotStored(tU8 Pu8EndUserNotStored)
{
  tU32                 Vu32ErrorCode=0;
  
  //exit process 2 with osal function only if ID valid
  if (vprID[2]!=INVALID_PID)
  {
    Vu32ErrorCode|=u32DPProcessTest(eCmdProcess3End,NULL,NULL,0); 
  }
  if(Vu32ErrorCode==0)
  { //delete folder "/pdd/datapool/effd"
    system("rm -r /var/opt/bosch/dynamic/pdd/datapool/effd");
    system("rm -r /var/opt/bosch/dynamic/pdd/backup/datapool/effd");
	  sleep(1);
    //mount
    system("mount -o rw,remount /");
    sleep(1);   
    //write file datapool.conf with DP_END_USER_NOT_STORED = 1
    intptr_t    ViFile = open("/etc/datapool/datapool.conf", O_WRONLY | O_CREAT | O_TRUNC ,PDD_ACCESS_RIGTHS);
    if(ViFile < 0)
    {
      Vu32ErrorCode|=errno;
    }
    else
    {
      char  VcFile[150];
      memset(VcFile,0,sizeof(VcFile));
      snprintf(VcFile,sizeof(VcFile),"DP_END_USER_NOT_STORED = %d\n",Pu8EndUserNotStored);
      if((write(ViFile, VcFile, strlen(VcFile)))<=0)
      {
        Vu32ErrorCode|=40;
      }
      close(ViFile);
    }
    system("sync");
    sleep(1);    
  }  
  if((Vu32ErrorCode==0)&&(VbStartProcess==TRUE))
	{ //start process 3 manually
	  OSAL_trProcessAttribute	VprAtr 	  = {OSAL_NULL};
	  VprAtr.szCommandLine  = NULL;
	  VprAtr.u32Priority    = PRIORITY;
	  VprAtr.szName="procdatapool3_out.out";
	  VprAtr.szAppName="/opt/bosch/base/bin/procdatapool3_out.out";     
    vprID[2] = OSAL_ProcessSpawn(&VprAtr);
	  sleep(5);
	}
  return(Vu32ErrorCode);
}
static tVoid vRestoreProcess3ForTestEndUserNotStored(tVoid)
{
  /*delete file*/
  system("rm /etc/datapool/datapool.conf");
  sleep(1);
  //mount read only
  system("mount -o remount,ro /");
  sleep(1);
  //exit process 2 with osal function only if ID valid
  if (vprID[2]!=INVALID_PID)
  {
    if(u32DPProcessTest(eCmdProcess3End,NULL,NULL,0)==0)
    {
       vprID[2]=INVALID_PID;
    }
  }
}
#endif 
#endif
/*****************************************************************************
* FUNCTION:		u32DPChangeConfigSetUserDpEndUserNotStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   03.08.2016
******************************************************************************/
tU32 u32DPChangeConfigSetUserDpEndUserNotStored(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  Vu32ErrorCode=u32PrepareProcess3ForTestEndUserNotStored(1);
  if(Vu32ErrorCode==0)
  {  //set command to store all end user pools and check user stored
    Vu32ErrorCode|=u32DPProcessTest(eCmdConfigSetUserDpEndUserNotStored,NULL,NULL,0); 
  } 
  vRestoreProcess3ForTestEndUserNotStored();
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPChangeConfigSetBankDpEndUserNotStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.08.2016
******************************************************************************/
tU32 u32DPChangeConfigSetBankDpEndUserNotStored(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  Vu32ErrorCode=u32PrepareProcess3ForTestEndUserNotStored(0);
  if(Vu32ErrorCode==0)
  {  //set command to store all end user pools and check user stored
    Vu32ErrorCode|=u32DPProcessTest(eCmdConfigSetBankDpEndUserNotStored,NULL,NULL,0); 
  } 
  vRestoreProcess3ForTestEndUserNotStored();
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPChangeConfigSetDefaultDpEndUserNotStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.08.2016
******************************************************************************/
tU32 u32DPChangeConfigSetDefaultDpEndUserNotStored(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  Vu32ErrorCode=u32PrepareProcess3ForTestEndUserNotStored(2);
  if(Vu32ErrorCode==0)
  {  //set command to store all end user pools and check user stored
    Vu32ErrorCode|=u32DPProcessTest(eCmdConfigSetDefaultDpEndUserNotStored,NULL,NULL,0); 
  } 
  vRestoreProcess3ForTestEndUserNotStored();
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPChangeConfigCopyUserDpEndUserNotStored()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.08.2016
******************************************************************************/
tU32 u32DPChangeConfigCopyUserDpEndUserNotStored(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  Vu32ErrorCode=u32PrepareProcess3ForTestEndUserNotStored(3);
  if(Vu32ErrorCode==0)
  {  //set command to store all end user pools and check user stored
    Vu32ErrorCode|=u32DPProcessTest(eCmdConfigCopyUserDpEndUserNotStored,NULL,NULL,0); 
  } 
  vRestoreProcess3ForTestEndUserNotStored();
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPProcessExitWithoutOsalExitFunction()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created   05.06.2015
******************************************************************************/
tU32 u32DPProcessExitWithoutOsalExitFunction(void)
{
  tU32                 Vu32ErrorCode=0;
  vDPInit();
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE
  //exit process 2 without osal function
  Vu32ErrorCode=u32DPProcessTest(eCmdProcess2End,NULL,NULL,1);  
  if(Vu32ErrorCode==0)
  { //lock unlock
    if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode=1;
    }
    else
    {
      if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	    {
        Vu32ErrorCode=2;
	    }
    }
    //note: the start of this process leads to a reset, because this osal process exit without function call to osal:
    //      the datapool resources for osal already exist; The datapool wants to create this resources again, which fails
  }
  #endif
  return(Vu32ErrorCode);
}

#ifdef __cplusplus
}
#endif
