/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_priority_sealing.c
 *
 * CREATED:     2005-11-19
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *              Matthias Thoemel / Blaupunkt
 *
 * DESCRIPTION:
 *      OEDT tests for priority inversion problem and solution
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: Blaupunkt
 *HISTORY      :Initial Revision.
 *      		Ported by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
                for ADIT platform
				Version - 1.1
			   :Removed lint and compiler warnings by Sainath Kalpuri(RBEI/ECF1)
			    on 22 Apr, 2009
				Version - 1.2

 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#include "OsalConf.h"

//#include "osal.h" tnn was here
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "TEngine_osal.h"
#include  "extension/device.h"
#include "oedt_helper_funcs.h"

/* Struct to hold the status of the Test */
typedef struct {
  volatile tBool low_task_start_flag;
  volatile tBool low_task_get_flag;
  volatile tBool tasks_started;
  volatile tBool high_task_get_flag;
  volatile tBool low_task_release_flag;
  volatile tBool priority_took_back_flag;

  OSAL_tMtxHandle mtxHandle_1;
  OSAL_tMtxHandle mtxHandle_2;

  volatile tBool mid_task_finished;
  volatile tBool high_task_finished;
  volatile tBool more_high_task_finished;
} OEDT_PrioCeilingTest_t;

/* testpriorities: all are in the same priority group 15 to make test more difficult */
#define LOW_PRIORITY 123
#define MID_PRIORITY 122
#define HIGH_PRIORITY 121
#define MORE_HIGH_PRIORITY 120

#undef TEST_ONLY
#ifdef TEST_ONLY
#define NO_OF_TESTS 1 /* number of concurrent tests (each test has 4 tasks) */
#else
#define NO_OF_TESTS 20 /* number of concurrent tests (each test has 4 tasks) */
#endif
/*****************************************************************************
* FUNCTION:	    low_task(VOID *ptr)
* PARAMETER:    VOID *ptr -> pointer to  OEDT_PrioCeilingTest_t structure.
* RETURNVALUE:  tVoid
* TEST CASE:    
* DESCRIPTION:  1. Aquires the Mutex first, which is also requested by 
*                  High priority tasks.
*               2. Waits until all the Task are created. Mid, High and More
*                  high priority tasks.
*               3. With priority inheritance this task priority is raised to the
*    			   the priority of mutex owner.
*				4. Once the Low_task releases(Unlock) the Mutex it becomes low 
*				   priority task again and gets no more CPU Time.
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/

static tVoid low_task(VOID *ptr)
{
  OEDT_PrioCeilingTest_t *testData = (OEDT_PrioCeilingTest_t *)ptr;
    
  testData->low_task_start_flag = TRUE;   /* low task is started, sync flag to release sem from HISR */

  OSAL_s32MutexLock( testData->mtxHandle_1,OSAL_C_TIMEOUT_FOREVER );
  OSAL_s32MutexLock( testData->mtxHandle_2,OSAL_C_TIMEOUT_FOREVER );

  testData->low_task_get_flag = TRUE;   /* get flag = sync flag to start other tasks */
  
  while(!testData->tasks_started) OSAL_s32ThreadWait(100);

  OSAL_s32MutexUnLock( testData->mtxHandle_2 ); 
  OSAL_s32MutexUnLock( testData->mtxHandle_1 );

  /* should _NOT_ be reached (mid is higher than low after release) */
  testData->low_task_release_flag = TRUE; 

      for (;;)
      {
	   
      }; /*lint !e527  */

}

/*****************************************************************************
* FUNCTION:	    mid_task(VOID *ptr)
* PARAMETER:    VOID *ptr -> pointer to  OEDT_PrioCeilingTest_t structure.
* RETURNVALUE:  tVoid
* TEST CASE:    
* DESCRIPTION:  1. This task has Mid priority.
*                  
*               2. Waits until the High priority task gets Mutex.
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
static tVoid mid_task(VOID *ptr)
{
  OEDT_PrioCeilingTest_t *testData = (OEDT_PrioCeilingTest_t *)ptr;

  (void)(ptr);

  /* busy waiting for high task to get Mutex. 
     Temporarily low task gets CPU time while raised to High priority */
  while(!(testData->high_task_get_flag));

  /* should be reached (high task got the flag, and low_task becomes lower 
         priority task after release)*/
  testData->priority_took_back_flag = 1; 
  
  testData->mid_task_finished = 1;
  
      for (;;)
      {
	   /* The use of for loop is, Low task should not get CPU Time */
      }; /*lint !e527  */
}

/*****************************************************************************
* FUNCTION:	    high_task(VOID *ptr)
* PARAMETER:    VOID *ptr -> pointer to  OEDT_PrioCeilingTest_t structure.
* RETURNVALUE:  tVoid
* TEST CASE:    
* DESCRIPTION:  1. Originaly this task has high priority.
*                  
*               2. Waits until Low_task releases the Mutex.
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/

static tVoid high_task(VOID *ptr)
{
  OEDT_PrioCeilingTest_t *testData = (OEDT_PrioCeilingTest_t *)ptr;

  (void)(ptr);

  OSAL_s32MutexLock( testData->mtxHandle_1,OSAL_C_TIMEOUT_FOREVER );
  OSAL_s32MutexLock( testData->mtxHandle_2,OSAL_C_TIMEOUT_FOREVER );

  /* should be reached (low task is temporary higher than mid, so Mutex can be released) */
  testData->high_task_get_flag = TRUE; 

  OSAL_s32MutexUnLock( testData->mtxHandle_2 );
  OSAL_s32MutexUnLock( testData->mtxHandle_1 );

  testData->high_task_finished = 1;

  for (;;)
  {
    OSAL_s32ThreadWait (100);
  }; /*lint !e527  */
}

/*****************************************************************************
* FUNCTION:	    more_high_task(VOID *ptr)
* PARAMETER:    VOID *ptr -> pointer to  OEDT_PrioCeilingTest_t structure.
* RETURNVALUE:  tVoid
* TEST CASE:    
* DESCRIPTION:  1. Originaly this task has More high priority, due to priority 
                   ceiling this task is forced to priority level below to Low_task
                   untill it holds the Mutex.
*                  
*               2. Waits untill Low_task releases the Mutex.
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/

static tVoid more_high_task(VOID *ptr)
{
  OEDT_PrioCeilingTest_t *testData = (OEDT_PrioCeilingTest_t *)ptr;
  
  (void)(ptr);
  
  OSAL_s32MutexLock( testData->mtxHandle_1,OSAL_C_TIMEOUT_FOREVER );

  OSAL_s32ThreadWait(20);

  OSAL_s32MutexUnLock( testData->mtxHandle_1 );

  testData->more_high_task_finished = 1;


      for (;;)
      {
	   OSAL_s32ThreadWait (100);
      }; /*lint !e527  */

}


/* Test Status structure */
typedef struct {
  tU32 testResult;
  tU32 testEnd;
  tU32 testCount;
} oedt_CeilingTest_t;
#define MUTEX_NAME1               "Mutex1"
#define MUTEX_NAME2               "Mutex2"
//#define MUTEX_PAYLOAD             0
/*****************************************************************************
* FUNCTION:	    oedt_one_priority_ceiling(void *argv)
* PARAMETER:    VOID *argv -> pointer to  oedt_CeilingTest_t structure.
* RETURNVALUE:  tVoid
* TEST CASE:    
* DESCRIPTION:  1. priority ceiling test task
*                  
*               2. starts 4 concurrent tasks in a defined sequence.
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/

static tVoid oedt_one_priority_ceiling(void *argv)
{
  oedt_CeilingTest_t *ceilingTest = (oedt_CeilingTest_t *)argv;
  OSAL_tThreadID tLowThrID = 0;
  OSAL_tThreadID tMidThrID = 0;
  OSAL_tThreadID tHighThrID1 = 0;
  OSAL_tThreadID tMoreHighThrID = 0;

  OSAL_trThreadAttribute low_priority_task;
  OSAL_trThreadAttribute mid_priority_task;
  OSAL_trThreadAttribute high_priority_task;
  OSAL_trThreadAttribute more_high_priority_task;
  tChar Thread_Name[9];
  tChar Test_Mutex1[8];
  tChar Test_Mutex2[8];
  OEDT_PrioCeilingTest_t testData;
  tU32 waitCounter;
  tU32 emergencyAbort;
  OSAL_trThreadControlBlock prTcb;
  tS32 ErrVal = 0;
 /* reset testdata */
  memset(&testData, 0, sizeof(testData));
  ceilingTest->testResult = 0;


	if(NULL == OSAL_szStringCopy(Test_Mutex1, MUTEX_NAME1))
	{
	/* Just to avoid lint warning */
	}
	Test_Mutex1[sizeof(Test_Mutex1)-2] = '0'+ (tU8)ceilingTest->testCount; 
    Test_Mutex1[sizeof(Test_Mutex1)-1] = '\0'; 
	/* Create 1. Mutex. Option TA_INHERIT is set inside OSAL function */
    if( OSAL_ERROR == OSAL_s32MutexCreate( Test_Mutex1,&testData.mtxHandle_1,0) )
	{
		return;
	}

	if(NULL ==OSAL_szStringCopy(Test_Mutex2, MUTEX_NAME2))
	{
	/* Just to avoid lint warning */
	}
	Test_Mutex2[sizeof(Test_Mutex2)-2] = '0'+ (tU8)ceilingTest->testCount; 
	Test_Mutex2[sizeof(Test_Mutex2)-1] = '\0'; 

	/* Create 2. Mutex. Option TA_INHERIT is set inside OSAL function */
	if( OSAL_ERROR == OSAL_s32MutexCreate( Test_Mutex2,&testData.mtxHandle_2,0) )
	{
		return;
	}


        /* create low prio task. This task too tries to obtain the Mutex */
		low_priority_task.u32Priority  = LOW_PRIORITY;					
		low_priority_task.pvArg        = &testData;
		low_priority_task.s32StackSize = 2048;	
		if(NULL ==OSAL_szStringCopy(Thread_Name, "Low_Task"))
		{
		 /* Just to avoid lint warning */
		}

		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)ceilingTest->testCount;
		low_priority_task.szName =  Thread_Name;
		low_priority_task.pfEntry      = (OSAL_tpfThreadEntry)low_task;
		
		/*Spawn the Thread 1 */
		tLowThrID = OSAL_ThreadSpawn( &low_priority_task );
		if(tLowThrID == OSAL_ERROR)
  		{
			return;
		}

		/* dispatch low prio task to obtain the Mutex. */
		while(!testData.low_task_start_flag) OSAL_s32ThreadWait(100);

		/* dispatch low prio task. When low_task_get_flag is set, the low prio task has obtained the Mutex */
		while(!testData.low_task_get_flag) OSAL_s32ThreadWait(100);

 		/* start the mid task, it will wait for the get_flag */
		mid_priority_task.u32Priority  = MID_PRIORITY;					
		mid_priority_task.pvArg        = &testData;
		mid_priority_task.s32StackSize = 2048;	
		if(NULL ==OSAL_szStringCopy(Thread_Name, "Mid_Task"))
		{
		/* Just to avoid lint warning */
		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)ceilingTest->testCount;
		mid_priority_task.szName =  Thread_Name;
		mid_priority_task.pfEntry      = (OSAL_tpfThreadEntry)mid_task;
									   
		
		/*Spawn the Thread 1 */
		tMidThrID = OSAL_ThreadSpawn( &mid_priority_task );
		if(tMidThrID == OSAL_ERROR)
  		{
		   return;
		}

  		OSAL_s32ThreadWait(40);
  
  		/* start the high priority task, it wants to obtain the Mutex */
		high_priority_task.u32Priority  = HIGH_PRIORITY;					
		high_priority_task.pvArg        = &testData;
		high_priority_task.s32StackSize = 2048;	
		if(NULL == OSAL_szStringCopy(Thread_Name, "HighTask"))
		{
		 /* Just to avoid lint warning */
		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)ceilingTest->testCount;
		high_priority_task.szName =  Thread_Name;
		high_priority_task.pfEntry      = (OSAL_tpfThreadEntry)high_task;
									   
		
		/*Spawn the Thread 1 */
		tHighThrID1 = OSAL_ThreadSpawn( &high_priority_task );
		if(tHighThrID1 == OSAL_ERROR)
  		{
		   	return;
		}
  		OSAL_s32ThreadWait(40);


  		/* start the more high task, it wants to obtain the Mutex */
		more_high_priority_task.u32Priority  = MORE_HIGH_PRIORITY;					
		more_high_priority_task.pvArg        = &testData;
		more_high_priority_task.s32StackSize = 2048;	
		if(NULL == OSAL_szStringCopy(Thread_Name, "MHigTask"))
		{
		 /* Just to avoid lint warning */
		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)ceilingTest->testCount;
		more_high_priority_task.szName =  Thread_Name;
		more_high_priority_task.pfEntry      = (OSAL_tpfThreadEntry)more_high_task;

		
		/*Spawn the Thread 1 */
		tMoreHighThrID = OSAL_ThreadSpawn( &more_high_priority_task );
		if(tMoreHighThrID == OSAL_ERROR)
  		{
		   return;
		}
  		OSAL_s32ThreadWait(40);

		/* signal: all tasks are started now */
		testData.tasks_started = 1;

		/* wait for end of test */
		waitCounter = 0;
		emergencyAbort = 4000000; /* ms */

		while(!testData.high_task_finished || !testData.more_high_task_finished || !testData.mid_task_finished)
		 {
		  OSAL_s32ThreadWait(100);
		  waitCounter++;
		  if (waitCounter > emergencyAbort  / 100 /* sleeptime */)
		   {
		    ceilingTest->testResult |= 2;
		    break;  
		  }
		}

		/* check the priority of low task */
		{
			/* get the task infos */	
			ErrVal = OSAL_s32ThreadControlBlock(tLowThrID, &prTcb);
			/* check  the priority */
			if ((ErrVal == OSAL_ERROR) || (prTcb.u32Priority != LOW_PRIORITY))
			{
				ceilingTest->testResult = 1;
			}
		}

  /* terminate and delete the tasks */
  OSAL_s32ThreadDelete(tMoreHighThrID);
  
  OSAL_s32ThreadDelete(tHighThrID1);
 
  OSAL_s32ThreadDelete(tLowThrID);
  OSAL_s32ThreadDelete(tMidThrID);

 /*Close the Mutex*/
  OSAL_s32MutexClose( testData.mtxHandle_1 );
 /*Delete the Mutex*/
  OSAL_s32MutexDelete( Test_Mutex1 );
  
  /*Close the Mutex*/
  OSAL_s32MutexClose( testData.mtxHandle_2 );
 /*Delete the Mutex*/
  OSAL_s32MutexDelete( Test_Mutex2 );

  /* check the test flags if the semaphore operations has been done successfully */
  if (!ceilingTest->testResult)
   {
	    if(!(testData.high_task_get_flag && testData.priority_took_back_flag && (!testData.low_task_release_flag))) 
	    {
	      if (testData.high_task_get_flag != TRUE)
	       {
	    	  ceilingTest->testResult |= 4;
	       }
	      if (testData.priority_took_back_flag != TRUE)
	       {
	    	  ceilingTest->testResult |= 8;
	       }
	      if (testData.low_task_release_flag != FALSE)
	       {
	    	  ceilingTest->testResult |= 16;
	       }
	    }
   }
	
	/* end of test */
	ceilingTest->testEnd = 1;
      for (;;)
      {
	   OSAL_s32ThreadWait (100);
      }; /*lint !e527  */

}



#define Priority_Ceiling_Thread_Name "Thread-n"  /* Last character 'n' will be replaced by thread Number */
/*****************************************************************************
* FUNCTION:	    oedt_priority_ceiling(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                  
*               2. Test is for priority inversion problem and solution.
                   Runs the test for NO_OF_TESTS times
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tU32 oedt_priority_ceiling(void)
{
  OSAL_tThreadID tIDM[NO_OF_TESTS]	  = {0};
  OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
  oedt_CeilingTest_t ceilingTest[NO_OF_TESTS];
  tU32 testResult;
  //tU32 startTime;
  //tU32 stopTime;
  tChar Thread_Name[9];
  tU32 i = 0;
  tU32 status = 0;
  tU32 ActualNoofTests = 0;
  tU32 u32Ret = 0;
  memset(ceilingTest, 0, sizeof(ceilingTest));
  /* get start time */
  //startTime = OSAL_ClockGetElapsedTime();
  

  /* start ceiling test */
  for(i = 0; i<NO_OF_TESTS; i++)
  {
  
    /* create test tasks. The test tasks create concurrent tasks to test the ceiling itself 
       &ceilingTest[i] is passed as VOID *argv
     */
	/* Initialize Thread attributes */
		trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;					
		ceilingTest[i].testCount = i;
		trAtr.pvArg        = &ceilingTest[i];
		trAtr.s32StackSize = 2048;	
		if(NULL ==OSAL_szStringCopy(Thread_Name, Priority_Ceiling_Thread_Name))
		{
		 /* Just to avoid lint warning */

		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)i;
		trAtr.szName = Thread_Name;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)oedt_one_priority_ceiling;
        
        /*Spawn the Thread 1 */
		tIDM[i] = OSAL_ThreadSpawn( &trAtr );
		if(tIDM[i] == OSAL_ERROR)
  		{
			break; 
		}

  }
  /* This variable is used in case at some point thread creation fails,
   we can have the Actual number of tests */
  ActualNoofTests = i-1;

  /* wait for end of all tests */
  for(;;)
   {
    status = 0;
    for(i=0; i<=ActualNoofTests; i++)
     {
      if (!ceilingTest[i].testEnd)
       {
        status = 1;
        break;
      }
    }
    if (!status) break;
    OSAL_s32ThreadWait(100);
  }      
 

  /* decreate the tests tasks */
  testResult = 0;
  for(i=0; i<=ActualNoofTests; i++) 
  {
    OSAL_s32ThreadDelete(tIDM[i]);
 	testResult = ceilingTest[i].testResult;
      if (testResult)
       {
        u32Ret |= ((tU32)1 << i);
      }
   
  }
  
  /* get end time */
 // stopTime = OSAL_ClockGetElapsedTime();
 
 
  /* return result */
  return u32Ret;
}


typedef struct {
  
  OSAL_tThreadID runningTask;
  tU32 runningTaskEnd;
  OSAL_tThreadID changingTask;
  tU32 changingTaskEnd;
  tU32 testResult;
} oedt_PrioTest_t;

/*****************************************************************************
* FUNCTION:	    oedt_prio_test_running_task(tVoid *argv)
* PARAMETER:    void *argv -> Pointer to the oedt_PrioTest_t structure.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Thread whoes priority is to be changed/
*                  
*               
        
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
static tVoid oedt_prio_test_running_task(tVoid *argv)
{
    oedt_PrioTest_t *prioTest = (oedt_PrioTest_t *)argv;
    
    while(prioTest->runningTaskEnd == 0)
     {
      OSAL_s32ThreadWait(1);
    }
    
    prioTest->runningTaskEnd = 2;

      for (;;)
      {
	   OSAL_s32ThreadWait (100);
      }; /*lint !e527  */

}

static unsigned long PrioTestNormRandValue(unsigned long randValue, int norm)
{
	unsigned long result;
	result = 	(( ((unsigned long)(randValue)>>16) * ((unsigned long)norm)) / (RAND_MAX>>16));
	if (result > (unsigned long)norm) result = (unsigned long)norm;
	return result;
}


/*****************************************************************************
* FUNCTION:	    oedt_prio_test_changing_task(tVoid *argv)
* PARAMETER:    void *argv -> Pointer to the oedt_PrioTest_t structure.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. The thread to change the priority of the other thread.
*                  
*               
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
static tVoid oedt_prio_test_changing_task(tVoid *argv)
{
    oedt_PrioTest_t *prioTest = (oedt_PrioTest_t *)argv;
    tU32 prio;
    

    /* do until end of test */
    while(prioTest->changingTaskEnd == 0)
     {
      
      /* calculate new priority */
      prio = PrioTestNormRandValue((unsigned long)rand(), OSAL_C_U32_THREAD_PRIORITY_NORMAL) + OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	  if(OSAL_ERROR == OSAL_s32ThreadPriority(prioTest->runningTask, prio))
	  {
	   break;
	  }

       /* do something else */
        OSAL_s32ThreadWait(1);
  
    }
      /* Revert back to Original Priority */
      prio = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  OSAL_s32ThreadPriority(prioTest->runningTask, prio);

      /* signal the end of this task */
      prioTest->changingTaskEnd = 2;
      for (;;)
      {
	   OSAL_s32ThreadWait (100);
      }; /*lint !e527  */

}

#define NO_OF_PRIO_TEST_TASKS 20
oedt_PrioTest_t prioTest[NO_OF_PRIO_TEST_TASKS];
#define Priority_Change_Running_Thread_Name "RunThread-n"  /* Last character 'n' will be replaced by thread Number */
#define Priority_Change_Changing_Thread_Name "ChngeThrd-n"  /* Last character 'n' will be replaced by thread Number */
/*****************************************************************************
* FUNCTION:	    oedt_prio_change_test(tVoid)
* PARAMETER:    tVoid
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  1. Test function, called form oedt framework
*                  
*               2. Tests the changing of the priority of the Thread.
                   Runs the test for NO_OF_PRIO_TEST_TASKS times
        
*                  
*
* HISTORY:		Created by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
*               
******************************************************************************/
tS32 oedt_prio_change_test(void)
{
  OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
  tU32 retry;
  tU32 i, iEnd;
  tS32 result = 0;
  tChar Thread_Name[12];
  /* init */
  memset(&prioTest[0], 0, sizeof(prioTest));
  
  /* loop over al subtasks */
  for(i=0; i<NO_OF_PRIO_TEST_TASKS; i++)
   {
  
    /* create the running task */
    prioTest[i].runningTaskEnd = 0;
    	/* Initialize Thread attributes */
		trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;					
		trAtr.pvArg        = &prioTest[i];
		trAtr.s32StackSize = 1024;	
		if(NULL == OSAL_szStringCopy(Thread_Name, Priority_Change_Running_Thread_Name))
		{
		 /* Just to avoid lint warning */
		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)i;
		trAtr.szName = Thread_Name;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)oedt_prio_test_running_task;
        
        /*Spawn the Thread 1 */
		prioTest[i].runningTask = OSAL_ThreadSpawn( &trAtr );
		if(prioTest[i].runningTask == OSAL_ERROR)
  		{
			break; 
		}
    
     /* create the changing task */

    	prioTest[i].changingTaskEnd = 0;
    	/* Initialize Thread attributes */
		trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;					
		trAtr.pvArg        = &prioTest[i];
		trAtr.s32StackSize = 1024;	
		if(NULL == OSAL_szStringCopy(Thread_Name, Priority_Change_Changing_Thread_Name))
		{
		/* Just to avoid lint warning */
		}
		Thread_Name[sizeof(Thread_Name)-1] = '0'+ (tU8)i;
		trAtr.szName = Thread_Name;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)oedt_prio_test_changing_task;
        
        /*Spawn the Thread 1 */
		prioTest[i].changingTask = OSAL_ThreadSpawn( &trAtr );
		if(prioTest[i].changingTask == OSAL_ERROR)
  		{
			break; 
		}


  }
  
  /* wait for end */
  OSAL_s32ThreadWait(10000);
  

  /* get end of last init */
  iEnd = i;
  
  /* loop over al subtasks */
  for(i=0; i<iEnd; i++)
   {
  
    /* stop the test */
    prioTest[i].changingTaskEnd = 1;
    retry = 2000;
    while(prioTest[i].changingTaskEnd == 1 && retry)
     {
      OSAL_s32ThreadWait(10);
      retry--;
    }
    if (retry == 0) prioTest[i].testResult |= 20000;
    
    prioTest[i].runningTaskEnd = 1;
    retry = 2000;
    while(prioTest[i].runningTaskEnd == 1 && retry)
     {
      OSAL_s32ThreadWait(10);
      retry--;
    }
    if (retry == 0) prioTest[i].testResult |= 20001;
    
    OSAL_s32ThreadDelete(prioTest[i].runningTask);    
    OSAL_s32ThreadDelete(prioTest[i].changingTask);    
  
    /* collect result */
    result |= (int) prioTest[i].testResult;
  }
  
  /* return result */
  return result;
}








