/******************************************************************************
 * FILE         : oedt_UART_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the UART 
 *                device for GM-GE hardware.
 *              
 * AUTHOR(s)    :  Haribabu Sannapaneni (RBEI/ECM1) 
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          | 		     		           | Author & comments
 * --.--.--      | Initial revision            | ------------
 *22,August,2008 | version 1.0                 | Haribabu Sannapaneni (RBEI/ECM1) 
 *04,May,2009    | Compiler & Lint warnings    | Jeryn Mathew (RBEI/ECF1)
 *               | removed                     |
 *-----------------------------------------------------------------------------
*******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_UART_TestFuncs.h"
#include "osal_if.h"
#include <tk/tkernel.h>

/* Defines and Macros*/
#define UART_SEND_TIMEOUT 5000 /* Send time out  ms*/
#define UART_RECEIVE_TIMEOUT 5000 /* Receive time out ms*/
#define UART_BREAK_TIME  1     /* Break Transmission for n ms*/
#define UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN (tU32)9
#define UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN 11
#define UART_XM_CTRL_POWER_REQ_LEN (tU32)11
#define UART_XM_CTRL_POWER_RESPONSE_LEN 5

/* Global variables */ 
/* Is Powered up the Xm_Tuner */
tBool XM_TunerOn = FALSE;

/*****************************************************************************
* FUNCTION:		UartSwitchOnXm_Tuner()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Power ups the Xm_Tuner
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
static tU32 UartSwitchOnXm_Tuner()
{
   	OSAL_tIODescriptor devID = 0;
    OSAL_trGPIOData pinData;
	tU32 u32Ret = 0;

	   /* Create GPIO */
	    if(OSAL_ERROR != OSAL_IOCreate (OSAL_C_STRING_DEVICE_GPIO, OSAL_EN_READWRITE))
		{
			devID = OSAL_IOOpen (OSAL_C_STRING_DEVICE_GPIO, OSAL_EN_READWRITE);
			if(OSAL_ERROR != devID)
			{
				/* Both pins have to be set to 1 */
				pinData.unData.bState = TRUE;

				/* Switch the Shutdown pin to 1 (pin 28 SHDN on XM-Module) */
				pinData.tId = (OSAL_tGPIODevID) JV_IRQ_GPIO1_3;
				if(OSAL_ERROR == OSAL_s32IOControl (devID, OSAL_C_32_IOCTRL_GPIO_SET_STATE, (tS32) &pinData))
				{
				        u32Ret = 3;
						if(OSAL_ERROR == OSAL_s32IOClose(devID))
						{
						     u32Ret += 10;
						}

						 return u32Ret;

				}
				else
				{
					/* Switch the Reset pin to 1 (pin 18 RESET_M on XM-Module) */
					pinData.tId = (OSAL_tGPIODevID) JV_IRQ_GPIO1_12;
					if(OSAL_ERROR == OSAL_s32IOControl (devID, OSAL_C_32_IOCTRL_GPIO_SET_STATE, (tS32) &pinData))
					{
						u32Ret = 5;
						if(OSAL_ERROR == OSAL_s32IOClose(devID))
						{
						     u32Ret += 10;
						}
						return u32Ret;
					}
					else
					{
						 XM_TunerOn = TRUE;
					}

				}

				if(OSAL_ERROR == OSAL_s32IOClose(devID))
				{
				     u32Ret = 10;
				}

			}
			else
			{
				OSAL_s32IORemove (OSAL_C_STRING_DEVICE_GPIO);
			    return u32Ret = 2;
			} 

		}
		else
		{
			u32Ret = 1; 
		}
return u32Ret;
}


/*****************************************************************************
* FUNCTION:		OpenCloseUARTChannel()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Opens and closes the specified UART Channel.
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 OpenCloseUARTChannel(tCString DevName)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	if((!strcmp( DevName,OSAL_C_STRING_DEVICE_UART_1 )) && (XM_TunerOn == FALSE))
	{
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }
	}
	/* Channel Open */
	hFd = OSAL_IOOpen(DevName,enAccess);
	if(OSAL_ERROR != hFd)
	{
		/* Channel Close */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret = 10;	
		}
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret;	

}

/*****************************************************************************
* FUNCTION:		u32UARTChannelOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_001
* DESCRIPTION:  1. Open UART Channel
*				2. Close UART Channel

* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTChannelOpenClose(tVoid)
{
   	tU32 u32Status = 0;
	tU32 u32Ret = 0;

    /* Open and Close UART channel 1 */
 	u32Status = OpenCloseUARTChannel(OSAL_C_STRING_DEVICE_UART_0);
	if(u32Status != 0)
	{
		u32Ret = 1;
	}

    /* Open and Close UART channel 2 */
 	u32Status = OpenCloseUARTChannel(OSAL_C_STRING_DEVICE_UART_1);
	if(u32Status != 0)
	{
		u32Ret += 2;
	}

    /* Open and Close UART channel 3 */

 	u32Status = OpenCloseUARTChannel(OSAL_C_STRING_DEVICE_UART_2);
	if(u32Status != 0)
	{
		u32Ret += 3;
	}

#if 0	/* Channel 4(uartd) is in use by Trace component */
    /* Open and Close UART channel 4 */

 	u32Status = OpenCloseUARTChannel(OSAL_C_STRING_DEVICE_UART_3);
	if(u32Status != 0)
	{
		u32Ret += 10;
	}

#endif
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32UARTChannelReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_002
* DESCRIPTION:  1. Open UART channel 0 
*				2. Attempt to Re-open the UART channel 0
*               3. Close UART channel 0
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
******************************************************************************/
tU32 u32UARTChannelReOpen(tVoid)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tIODescriptor hFd1 = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open UART channel 1 */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_UART_0,enAccess);
	if(OSAL_ERROR != hFd)
	{
		/* Attempt to Re-open UART channel 1 */
		hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_UART_0,enAccess);
		if(OSAL_ERROR != hFd1)
		{
			/* If successful, indicate error */
			u32Ret = 1;
			/* Close the channel */
		    if(OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
			{
				u32Ret += 2;
			}
		}
	   
		/* Close the channel */
	    if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret += 4;
		}
	}
	else
	{
		u32Ret = 10;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32UARTChanCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_003
* DESCRIPTION:  1. Open UART channel 0
*               2. Close UART channel 0
*               3. Attempt to Close UART channel 0 again

* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
******************************************************************************/
tU32 u32UARTChanCloseAlreadyClosed(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open UART channel 1 */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_UART_0,enAccess);
	if(OSAL_ERROR != hFd)
	{
		/* Close the channel */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret = 1;
		}
		else
		{
			/* Attempt to Close the channel which is already closed */
		    if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 2;
			}
		}
	}
	else
	{
		u32Ret = 4;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32UARTChannelOpenCloseWithAlias()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_004
* DESCRIPTION:  Opens and closes the specified UART Channel using alias.
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTChannelOpenCloseWithAlias(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	if(XM_TunerOn == FALSE)
	{
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }
	}

	/* Channel Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_XM_CTRL,enAccess);
	if(OSAL_ERROR != hFd)
	{
		/* Channel Close */
		if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
		{
			u32Ret = 10;	
		}
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret;	

}


/*****************************************************************************
* FUNCTION:	    u32UARTGetVersion()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_005
* DESCRIPTION:  1. Opens UART Channel 0
*               2. Gets version
*               3. Closes UART Channel 0

* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTGetVersion(tVoid)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tU32 u32Version = 0;
      /* Channel Open */
		hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_UART_0,  
                            enAccess);
		if(OSAL_ERROR != hFd)
	   {
			if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_VERSION, (tS32)&u32Version ))
			{
			 u32Ret = 2;
			}

           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 10;	
			}		


	   }
	   else
	   {
		u32Ret = 1;
	   }
	   return  u32Ret;
}

/*****************************************************************************
* FUNCTION:	    SetAndGetUARTRsModeSettings()
* PARAMETER:    Channel ID 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested UART Channel
*               2. Set and Get the Communication Mode Settings
                3. Close UART channel
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
static tU32 SetAndGetUARTRsModeSettings(tCString DevName)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_trRsMode trSetRsMode, trGetRsMode;
	tU32 u32Ret = 0;

	if((!strcmp( DevName,OSAL_C_STRING_DEVICE_UART_1)) && (XM_TunerOn == FALSE))
	{
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }

	}

		hFd = OSAL_IOOpen(DevName, enAccess);
		if(OSAL_ERROR != hFd)
		{
			/* Fill the OSAL_trRsMode structure */
			trSetRsMode.u32stopbits = 0; 
			trSetRsMode.u32baud = 9600;  
			trSetRsMode.u32datalen = 3;  
			trSetRsMode.u32parity = 0;   

              /* Set Communication Mode Settings */
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSMODE,(tS32)&trSetRsMode))
                               {
                                      u32Ret = 3;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
                                     return u32Ret;
                                }

              /* Get Communication Mode Settings */
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_GET_RSMODE,(tS32)&trGetRsMode))
                               {
                                      u32Ret = 5;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
	

                                     return u32Ret;
                                }

           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 100;	
			}		
			 /* Compare Communication Mode Settings */
			 if((trSetRsMode.u32stopbits != trGetRsMode.u32stopbits) || (trSetRsMode.u32baud != trGetRsMode.u32baud) || (trSetRsMode.u32datalen != trGetRsMode.u32datalen) || (trSetRsMode.u32parity != trGetRsMode.u32parity) )
			 {
			   u32Ret += 1000;
			 }

	
		 }
		 else
		 {
		  u32Ret = 2;
		 }

 return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32UARTSetAndGetRsModeSettings()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_006
* DESCRIPTION:  Sets and Gets the Communication Mode Settings.
*               - Baud Rate, STOP bits, DATA length and Parity
                
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTSetAndGetRsModeSettings(tVoid)
{
   	tU32 u32Status = 0;
	tU32 u32Ret = 0;

    /* Test UART channel 1 */
 	u32Status = SetAndGetUARTRsModeSettings(OSAL_C_STRING_DEVICE_UART_0);
	if(u32Status != 0)
	{
		u32Ret = 1;
	}

    /* Test UART channel 2 */
 	u32Status = SetAndGetUARTRsModeSettings(OSAL_C_STRING_DEVICE_UART_1);
	if(u32Status != 0)
	{
		u32Ret += 2;
	}

    /* Test UART channel 3 */

 	u32Status = SetAndGetUARTRsModeSettings(OSAL_C_STRING_DEVICE_UART_2);
	if(u32Status != 0)
	{
		u32Ret += 10;
	}
#if 0 /* Channel 4(uartd) is in use by Trace component */
    /* Test UART channel 4 */
 	u32Status = SetAndGetUARTRsModeSettings(OSAL_C_STRING_DEVICE_UART_3);
	if(u32Status != 0)
	{
		u32Ret += 100;
	}
#endif
return u32Ret;

}

/*****************************************************************************
* FUNCTION:	    SetAndGetUARTRsFlowControl()
* PARAMETER:    Channel ID 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested UART Channel
*               2. Sets and Gets FLOW Control information
                3. Close UART Channel
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
static tU32 SetAndGetUARTRsFlowControl(tCString DevName)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_trRsFlow trSetRsFlow, trGetRsFlow;
	tU32 u32Ret = 0;
		/* Power On the XM_Tuner */
		if((!strcmp(DevName,OSAL_C_STRING_DEVICE_UART_1) ) && (XM_TunerOn == FALSE))
		{
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }

		}

		hFd = OSAL_IOOpen(DevName, enAccess);
		if(OSAL_ERROR != hFd)
		{
			/* Fill the OSAL_trRsFlow structure */
			trSetRsFlow.u32csflow = 0;
			trSetRsFlow.u32rcvxoff = 0;
			trSetRsFlow.u32rsflow =  0;
			trSetRsFlow.u32rxflow =  0;
			trSetRsFlow.u32sxflow =  0;
			trSetRsFlow.u32xonany =  0;


              /* Set Flow Control Settings */
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSFLOW,(tS32)&trSetRsFlow))
                               {
                                      u32Ret = 3;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
                                     return u32Ret;
                                }
              /* Get Flow Control Settings */
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_GET_RSFLOW,(tS32)&trGetRsFlow))
                               {
                                      u32Ret = 5;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
                                      return u32Ret;
                                }

           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 100;	
			}		
	
		 }
		 else
		 {
		   u32Ret = 2;
		 }
 return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32UARTSetAndGetRsFlowControl()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_007
* DESCRIPTION:  Sets and Gets FLOW Control information
*
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTSetAndGetRsFlowControl(tVoid)
{
   	tU32 u32Status = 0;
	tU32 u32Ret = 0;

    /* Test UART channel 1 */
 	u32Status = SetAndGetUARTRsFlowControl(OSAL_C_STRING_DEVICE_UART_0);
	if(u32Status != 0)
	{
		u32Ret = 1;
	}

    /* Test UART channel 2 */
 	u32Status = SetAndGetUARTRsFlowControl(OSAL_C_STRING_DEVICE_UART_1);
	if(u32Status != 0)
	{
		u32Ret += 2;
	}

    /* Test UART channel 3 */

 	u32Status = SetAndGetUARTRsFlowControl(OSAL_C_STRING_DEVICE_UART_2);
	if(u32Status != 0)
	{
		u32Ret += 10;
	}
#if 0	 /* Channel 4(uartd) is in use by Trace component */
    /* Test UART channel 4 */
 	u32Status = SetAndGetUARTRsFlowControl(OSAL_C_STRING_DEVICE_UART_3);
	if(u32Status != 0)
	{
		u32Ret += 100;
	}
#endif
return u32Ret;

}

/*****************************************************************************
* FUNCTION:	    UARTSetSendAndReceiveTimeOut()
* PARAMETER:    Channel ID 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested UART Unit
*               2. Sets the Send Timeout and Receive Timeout
                3. Close UART channel
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
static tU32 UARTSetSendAndReceiveTimeOut(tCString DevName)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    tU32 u32SendTimeOut = 0;
    tU32 u32ReceiveTimeOut = 0;

		/* Power On the XM_Tuner */
		if((!strcmp(DevName,OSAL_C_STRING_DEVICE_UART_1)) && (XM_TunerOn == FALSE))
		{
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }
		}

		hFd = OSAL_IOOpen(DevName, enAccess);
		if(OSAL_ERROR != hFd)
		{

              /* Set Send TimeOut */
              u32SendTimeOut = UART_SEND_TIMEOUT;    
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSSNDTMO,(tS32)&u32SendTimeOut))
                               {
                                      u32Ret = 3;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
                                     return u32Ret;
                                }
			  /* Set Receive TimeOut */
			  u32ReceiveTimeOut = UART_RECEIVE_TIMEOUT;
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSRCVTMO,(tS32)&u32ReceiveTimeOut))
                               {
                                      u32Ret += 5;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
                                     return u32Ret;
                                }

           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 100;	
			}		
	
		 }
		 else
		 {
		  return u32Ret = 2;
		 }

 return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32UARTSetSendAndReceiveTimeOut()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_008
* DESCRIPTION:  Set Send and Receive Timeout for UART channels
*               
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*               
******************************************************************************/
tU32 u32UARTSetSendAndReceiveTimeOut(tVoid)
{
   	tU32 u32Status = 0;
	tU32 u32Ret = 0;

    /* Test UART channel 1 */
 	u32Status = UARTSetSendAndReceiveTimeOut(OSAL_C_STRING_DEVICE_UART_0);
	if(u32Status != 0)
	{
		u32Ret = 1;
	}

    /* Test UART channel 2 */
 	u32Status = UARTSetSendAndReceiveTimeOut(OSAL_C_STRING_DEVICE_UART_1);
	if(u32Status != 0)
	{
		u32Ret += 2;
	}
    /* Test UART channel 3 */

 	u32Status = UARTSetSendAndReceiveTimeOut(OSAL_C_STRING_DEVICE_UART_2);
	if(u32Status != 0)
	{
		u32Ret += 10;
	}
#if 0  /* Channel 4(uartd) is in use by Trace component */
    /* Test UART channel 4 */
 	u32Status = UARTSetSendAndReceiveTimeOut(OSAL_C_STRING_DEVICE_UART_3);
	if(u32Status != 0)
	{
		u32Ret += 100;
	}
#endif
return u32Ret;

}

/*****************************************************************************
* FUNCTION:	    UARTBreakTransmission()
* PARAMETER:    Channel ID 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested UART Unit
*               2. Send BREAK Transmission for n ms time
                3. Close UART channel
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
static tU32 UARTBreakTransmission(tCString DevName)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    tU32 u32BreakTime = 0;

		/* Power On the XM_Tuner */
		if((!strcmp(DevName,OSAL_C_STRING_DEVICE_UART_1)) && (XM_TunerOn == FALSE))
		{
		   u32Ret = UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }

		}

		hFd = OSAL_IOOpen(DevName, enAccess);
		if(OSAL_ERROR != hFd)
		{

              /* Set Send TimeOut */
              u32BreakTime = UART_BREAK_TIME;    
              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSBREAK,(tS32)&u32BreakTime))
                               {
                                      u32Ret = 3;
									 if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
		                            	{
			                            	u32Ret += 10;	
		                             	}		
	
							          return u32Ret;
                                }

           if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
			{
				u32Ret += 100;	
			}		
	
		 }
		 else
		 {
		  return u32Ret = 2;
		 }

 return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32UARTBreakTransmission()
* PARAMETER:    Channel ID 
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_009
* DESCRIPTION:  Send Break Transmission Signal
*               
* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*               
******************************************************************************/
tU32 u32UARTBreakTransmission(tVoid)
{
   	tU32 u32Status = 0;
	tU32 u32Ret = 0;

    /* Test UART channel 1 */
 	u32Status = UARTBreakTransmission(OSAL_C_STRING_DEVICE_UART_0);
	if(u32Status != 0)
	{
		u32Ret = 1;
	}

    /* Test UART channel 2 */
 	u32Status = UARTBreakTransmission(OSAL_C_STRING_DEVICE_UART_1);
	if(u32Status != 0)
	{
		u32Ret += 3;
	}
    /* Test UART channel 3 */

 	u32Status = UARTBreakTransmission(OSAL_C_STRING_DEVICE_UART_2);
	if(u32Status != 0)
	{
		u32Ret += 10;
	}
#if 0	  /* Channel 4(uartd) is in use by Trace component */
    /* Test UART channel 4 */
 	u32Status = UARTBreakTransmission(OSAL_C_STRING_DEVICE_UART_3);
	if(u32Status != 0)
	{
		u32Ret += 100;
	}
#endif
return u32Ret;

}

/*****************************************************************************
* FUNCTION:	    u32UARTChannle1WriteRead()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_010
* DESCRIPTION:  1. Open UART Channel 1
*               2. Performs write and read operation
*               3. Close UART channel

* HISTORY: Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*                    Lint removed by Jeryn Mathew (RBEI/ECF1) on 04 May, 2009
******************************************************************************/
tU32 u32UARTChannle1WriteRead(tVoid)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	tPS8 *ps8UARTReadData = NULL;
	tU32  u32BytesWritten  = 0;
	tU32  u32NoOfBytesRead = 0;
    /* Power up Request */
    tU8 u8PowerRequest[] = {0x5a,0xa5,0x0,0x5,0x0,0x10,0x10,0x10,0x10,0x1,0x44};
    /* Expected Response for Power up Request */
    tU8   u8XmTunerResponse[] = {0x5a,0xa5,0x0,0x1b,0x80};

	   if(XM_TunerOn != TRUE)
	   {
			/* Power On the XM_Tuner */
		   u32Ret =	UartSwitchOnXm_Tuner();
		   if(u32Ret != 0)
		   {
		    return u32Ret = 1;
		   }
	   }
		 /* Delay for XM_Tuner to be available for Communication afetr Power UP */
		 OSAL_s32ThreadWait(300);/*300ms*/

    	 /* Channel 1 is connected to Xm Tuner */
		/* Open Channel  */
		hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_XM_CTRL, enAccess);
		if(OSAL_ERROR != hFd)
		{
				/* Write Power Up Request to XM Tuner */
				u32BytesWritten = (tU32)OSAL_s32IOWrite(hFd,
				(tPS8)u8PowerRequest, (tU32)UART_XM_CTRL_POWER_REQ_LEN);
				if((OSAL_ERROR == (tS32)u32BytesWritten) && (u32BytesWritten != (tU32)UART_XM_CTRL_POWER_REQ_LEN))
				{
					u32Ret = 3;
					if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
					{
						u32Ret += 10;	
					}

				   return u32Ret;
				}

	            ps8UARTReadData = (tPS8 *)OSAL_pvMemoryAllocate(UART_XM_CTRL_POWER_RESPONSE_LEN);

			   	/* Read the Response to Power request */
                u32NoOfBytesRead = (tU32)OSAL_s32IORead(hFd,(tPS8) ps8UARTReadData, UART_XM_CTRL_POWER_RESPONSE_LEN);
				if((OSAL_ERROR == (tS32)u32NoOfBytesRead) && (u32NoOfBytesRead  != UART_XM_CTRL_POWER_RESPONSE_LEN))
				{
					u32Ret = 100;
					if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
					{
						u32Ret += 10;	
					}

				   return u32Ret;
				}

				OSAL_vMemoryFree(ps8UARTReadData);

				/* Check the response */
				if(OSAL_ERROR == OSAL_s32StringNCompare(ps8UARTReadData, (tPS8 *)u8XmTunerResponse, (sizeof(u8XmTunerResponse)-1)))
				{
				    u32Ret += 500;
				}

						/* Channel Close */
				if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
				{
					u32Ret += 1000;	
				}

    } 
	else
	{
		u32Ret = 2;
	}

return u32Ret;	
}

/*****************************************************************************
* FUNCTION:	    u32UARTChannleWriteReadWithDiffBaud()
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_UART_011
* DESCRIPTION:  1. Open UART Channel 1
*               2. Send Baudrate change request 
*				3. Read response
* 				2. Set Baud rate
*               3. Perform write 
*               5. Then Read operation
*               6. Close UART channel 1

* HISTORY:		Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
*                    Lint removed by Jeryn Mathew (RBEI/ECF1) on 04 May, 2009
*               
******************************************************************************/
tU32 u32UARTChannleWriteReadWithDiffBaud(tVoid)
{
   	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    OSAL_trRsMode trSetRsMode, trGetRsMode;
	tU32 u32Ret = 0;
	tPS8 *ps8UARTReadData = NULL;
	tU32  u32BytesWritten  = 0;
	tU32  u32NoOfBytesRead = 0;
	tU8 u8BaudRateReq[UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN] = {0x5a,0xa5,0x0,0x3,0x70,0x20,0x03,0x01,0x95};   
    tU8 u8PowerRequest[] = {0x5a,0xa5,0x0,0x5,0x0,0x10,0x10,0x10,0x10,0x1,0x44};
    /* Expected Response for Power up Request */
    tU8   u8XmTunerResponse[] = {0x5a,0xa5,0x0,0x1b,0x80};

   if(XM_TunerOn != TRUE)
   {
		/* Power On the XM_Tuner */
		u32Ret =	UartSwitchOnXm_Tuner();
		if(u32Ret != 0)
		{
		return u32Ret = 1;
		}

   }
		 OSAL_s32ThreadWait(300);/*300ms*/
    	 /* Channel 1 is connected to Xm Tuner */
		/* Open Channel  */
		hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_UART_1, enAccess);
		if(OSAL_ERROR != hFd)
		{
	            

			     	/* Send Baud rate change request command */
				 	u32BytesWritten = (tU32)OSAL_s32IOWrite(hFd,
					(tPS8)u8BaudRateReq, (tU32)UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN);
					if((OSAL_ERROR == (tS32)u32BytesWritten) && (u32BytesWritten != UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN))
					{
					   OSAL_s32IOClose ( hFd );
					   return u32Ret = 3;
					}

		            ps8UARTReadData = (tPS8 *)OSAL_pvMemoryAllocate(UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN);

				   	/* Read the Response to Baud rate change request */
	                u32NoOfBytesRead = (tU32)OSAL_s32IORead(hFd,(tPS8)ps8UARTReadData, UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN);
					if((OSAL_ERROR == (tS32)u32NoOfBytesRead) && (u32NoOfBytesRead  !=      
		                       UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN))
					{
					 OSAL_s32IOClose ( hFd );
					 return	u32Ret = 5;
					}

					OSAL_vMemoryFree(ps8UARTReadData);

				/* Fill the OSAL_trRsMode structure */
				trSetRsMode.u32stopbits = 0; 
				trSetRsMode.u32baud = 115200;
				trSetRsMode.u32datalen = 3;  
				trSetRsMode.u32parity = 0;   

					/* Set Communication Mode Settings */
					if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSMODE,(tS32)&trSetRsMode))
					{
						u32Ret = 10;
						if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
						{
						u32Ret += 100;	
						}		
						return u32Ret;
					}
					/* Wait for baud rate changes to reflect */
					OSAL_s32ThreadWait(300);/*300ms*/
					/* Write Power Up Request to XM Tuner */
					u32BytesWritten = (tU32)OSAL_s32IOWrite(hFd,
					(tPS8)u8PowerRequest,(tU32)UART_XM_CTRL_POWER_REQ_LEN);
					if((OSAL_ERROR == (tS32)u32BytesWritten) && (u32BytesWritten != UART_XM_CTRL_POWER_REQ_LEN))
					{
					   OSAL_s32IOClose ( hFd );
					   return u32Ret = 1000;
					}

		            ps8UARTReadData = (tPS8 *)OSAL_pvMemoryAllocate(UART_XM_CTRL_POWER_RESPONSE_LEN);

				   	/* Read the Response to Power request */
	                u32NoOfBytesRead = (tU32)OSAL_s32IORead(hFd, (tPS8)ps8UARTReadData, UART_XM_CTRL_POWER_RESPONSE_LEN);
					if((OSAL_ERROR == (tS32)u32NoOfBytesRead) && (u32NoOfBytesRead  !=      
		                       UART_XM_CTRL_POWER_RESPONSE_LEN))
					{
					 OSAL_s32IOClose ( hFd );
					 return	u32Ret = 2000;
					}

					OSAL_vMemoryFree(ps8UARTReadData);

	              /* Get Communication Mode Settings */
	              if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_GET_RSMODE,(tS32)&trGetRsMode))
	                               {
		                            	u32Ret = 5000;	
                             
	                                }

					/* Check the response */
					if(OSAL_ERROR == OSAL_s32StringNCompare(ps8UARTReadData, (tPS8 *)u8XmTunerResponse, (sizeof(u8XmTunerResponse)-1)))
					{
					 u32Ret += 10000;
					}
					/* Check Baud Rate*/
					if(trSetRsMode.u32baud != trGetRsMode.u32baud)
					{
					  u32Ret += 20000;
					}
					  /* Revert Baud Rate back to 9600 */
	                u8BaudRateReq[6] = 0x00;   
			     	u8BaudRateReq[7] = 0x01;
					u8BaudRateReq[8] = 0x92;
			     	/* Send Baud rate change request command */
				 	u32BytesWritten = (tU32)OSAL_s32IOWrite(hFd,
					(tPS8)u8BaudRateReq, (tU32)UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN);
					if((OSAL_ERROR == (tS32)u32BytesWritten) && (u32BytesWritten != UART_XM_CTRL_BAUD_RATE_CHANGE_REQ_LEN))
					{
					   OSAL_s32IOClose ( hFd );
					   return u32Ret = 50000;
					}

		            ps8UARTReadData = (tPS8 *)OSAL_pvMemoryAllocate(UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN);

				   	/* Read the Response to Baud rate change request */
	                u32NoOfBytesRead = (tU32)OSAL_s32IORead(hFd,(tPS8)ps8UARTReadData, UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN);
					if((OSAL_ERROR == (tS32)u32NoOfBytesRead) && (u32NoOfBytesRead  !=      
		                       UART_XM_CTRL_BAUD_RATE_CHANGE_RESPONSE_LEN))
					{
					 OSAL_s32IOClose ( hFd );
					 return	u32Ret = 100000;
					}

					OSAL_vMemoryFree(ps8UARTReadData);

				/* Fill the OSAL_trRsMode structure */
				trSetRsMode.u32stopbits = 0; 
				trSetRsMode.u32baud = 9600;
				trSetRsMode.u32datalen = 3;  
				trSetRsMode.u32parity = 0;   

					/* Set Communication Mode Settings */
					if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_SIO_SET_RSMODE,(tS32)&trSetRsMode))
					{
						u32Ret = 200000;
						if(OSAL_ERROR  == OSAL_s32IOClose (hFd))
						{
						u32Ret += 100;	
						}		
						return u32Ret;
					}
					/* Wait for baud rate changes to reflect */
					OSAL_s32ThreadWait(300);/*300ms*/

					/* Channel Close */
					if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
					{
						u32Ret += 1000000;	
					}

    } 
	else
	{
		u32Ret = 2;
	}

	return u32Ret;	

}



/* Need to develop Read and Write test cases for Channel 0,2 and 3 */
/* end of file */

