/************************************************************************
| FILE:         oedt_InputInc_TestFuncs.c
| PROJECT:      platform
| SW-COMPONENT: OEDT_FrmWrk
|------------------------------------------------------------------------------
| DESCRIPTION:  Regression tests for INPUT_INC driver.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 16.05.13  | Initial version            | tma2hi
| 27.06.13  | Add support for protocol v2| tma2hi
| 19.06.14  | Add multi-touch type B test| tma2hi
| 17.02.15  | Lint fix for CFG3-1015     | Shashikant Suguni
| 11.05.15  | Bug fix for CFG3-1212      | Shahida Mohammmed Ashraf
| 27.05.15  | Bug fix for CFG3-1234      | Shahida Mohammmed Ashraf
| 11.05.16  | Bug fix for CFG3-1708      | Dharmender Suresh Chander
|*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_InputInc_TestFuncs.h"
#include "oedt_helper_funcs.h"

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include "linux/input.h"
#include "dgram_service.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"

#define DEBUG

#ifdef DEBUG
#define PR_DBG(fmt, args...)\
      OEDT_HelperPrintf(TR_LEVEL_USER_4, "DEBUG inc_test_input: %s: " fmt, __func__, ## args)
#else
#define PR_DBG(fmt, args...)
#endif

#define PR_ERR(fmt, args...) \
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "inc_test_input: %s: " fmt, __func__, ## args)

#define EXPCT_EQ(val1, val2, ret, fmt, args...) \
   if (val1 != val2) { \
      PR_ERR(fmt, ## args); \
      return ret; }

#define EXPCT_NE(val1, val2, ret, fmt, args...) \
   if (val1 == val2) { \
      PR_ERR(fmt, ## args); \
      return ret; }

#define EXPCT_LT(val1, val2, ret, fmt, args...) \
   if (val1 >= val2) { \
      PR_ERR(fmt, ## args); \
      return ret; }

#define EXPCT_GT(val1, val2, ret, fmt, args...) \
   if (val1 <= val2) { \
      PR_ERR(fmt, ## args); \
      return ret; }


#define SYSTEM_INIT_COMMAND            "modprobe dev-fake"
#define COMPONENT_INIT_COMMAND         "rmmod input-inc; modprobe input-inc of_id=rbcm,input-inc-test"

#define INPUT_INC_STATUS_ACTIVE        0x01
#define INPUT_INC_PROTOCOL_VERSION_CUR 2
#define INPUT_INC_PROTOCOL_VERSION_MIN 1

#define BUF_SIZE                       1024
#define MAX_INPUT_DEVICES              16
#define INPUT_DEVICE_NAME              "input_fake"
#define POLL_TIMEOUT_MS                400

#define KEY_MAX_VANILLA                0x2ff /* KEY_MAX of vanilla 2.6.35 */
#define KEY_MAX_EXTENDED               0x3ff /* Bosch-specific KEY_MAX */

/* Event Pattern to identify the fake device opening by rx thread */
#define INPUT_INC_FAKE_DEVICES_OPENED              0x01  

struct thread_arg {
   int test_id;
};

/*
 * test function declaration
 */
#define NUMBER_OF_TESTS 7

int test_multiple_devices_configure_devices(void);
int test_multiple_devices_generate_events(int);
int test_multiple_devices_assert_events(uint8_t, int, struct input_event *);

int test_many_events_configure_devices(void);
int test_many_events_generate_events(int);
int test_many_events_assert_events(uint8_t, int, struct input_event *);

int test_many_codes_configure_devices(void);
int test_many_codes_generate_events(int);
int test_many_codes_assert_events(uint8_t, int, struct input_event *);

int test_extended_keys_configure_devices(void);
int test_extended_keys_generate_events(int);
int test_extended_keys_assert_events(uint8_t, int, struct input_event *);

int test_multitouch_type_a_configure_devices(void);
int test_multitouch_type_a_generate_events(int);
int test_multitouch_type_a_assert_events(uint8_t, int, struct input_event *);

int test_multitouch_type_b_configure_devices(void);
int test_multitouch_type_b_generate_events(int);
int test_multitouch_type_b_assert_events(uint8_t, int, struct input_event *);

int test_nonfatal_protocol_violations_configure_devices(void);
int test_nonfatal_protocol_violations_generate_events(int);
int test_nonfatal_protocol_violations_assert_events(uint8_t, int, struct input_event *);
/*Function declaration*/
static tU32 check_input_inc_fake_device_opened(void);

int (*test_funcs_configure_devices[NUMBER_OF_TESTS])(void) =
      {
            test_multiple_devices_configure_devices,
            test_many_events_configure_devices,
            test_many_codes_configure_devices,
            test_extended_keys_configure_devices,
            test_multitouch_type_a_configure_devices,
            test_multitouch_type_b_configure_devices,
            test_nonfatal_protocol_violations_configure_devices
      };

int (*test_funcs_generate_events[NUMBER_OF_TESTS])(int) =
      {
            test_multiple_devices_generate_events,
            test_many_events_generate_events,
            test_many_codes_generate_events,
            test_extended_keys_generate_events,
            test_multitouch_type_a_generate_events,
            test_multitouch_type_b_generate_events,
            test_nonfatal_protocol_violations_generate_events
      };

int (*test_funcs_assert_events[NUMBER_OF_TESTS])(uint8_t, int,
      struct input_event *) =
      {
            test_multiple_devices_assert_events,
            test_many_events_assert_events,
            test_many_codes_assert_events,
            test_extended_keys_assert_events,
            test_multitouch_type_a_assert_events,
            test_multitouch_type_b_assert_events,
            test_nonfatal_protocol_violations_assert_events
      };

int sockfd;
sk_dgram *dgram;
/*Event used for synchronizing main function and rx thread.
This event is posted from rx thread once the fake devices are opened
successfully and is ready to receive generated events.*/
OSAL_tEventHandle  hReadytoRead= 0;
int system_call(char* cmd)
{
   int ret, status;

   ret = system(cmd);
   if (ret == -1)
      PR_DBG("system(%s) failed: %d\n", cmd, ret);
   //EXPCT_NE(ret, -1, 1, "system(%s) failed: %d\n", cmd, ret);

   status = WEXITSTATUS(ret);
   if (status != 0)
      PR_DBG("system(%s) exit status: %d\n", cmd, status);
   //EXPCT_EQ(status, 0, 2, "system(%s) exit status: %d\n", cmd, status);

   return 0;
}

int system_init()
{
   PR_DBG("enter\n");

   return system_call(SYSTEM_INIT_COMMAND);
}

int component_init()
{
   PR_DBG("enter\n");

   return system_call(COMPONENT_INIT_COMMAND);
}

int inc_init()
{
   int ret;
   int errsv;
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;

   PR_DBG("enter\n");

   local = gethostbyname("fake1-local");
   errsv = errno;
   EXPCT_NE(local, NULL, 1, "gethostbyname(fake1-local) failed: %d\n", errsv);

   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
   local_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

   remote = gethostbyname("fake1");
   errsv = errno;
   EXPCT_NE(remote, NULL, 2, "gethostbyname(fake1) failed: %d\n", errsv);

   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   remote_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

   sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
   errsv = errno;
   EXPCT_NE(sockfd, -1, 3, "create socket failed: %d\n", errsv);

   dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
   EXPCT_NE(dgram, NULL, 4, "dgram_init failed\n");
//lint -e64
   ret = bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   errsv = errno;
   EXPCT_EQ(ret, 0, 5, "bind failed: %d\n", errsv);

   ret = connect(sockfd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
   errsv = errno;
   EXPCT_EQ(ret, 0, 6, "connect failed: %d\n", errsv);

   return 0;
}

int inc_exit()
{
   int ret, errsv;

   ret = dgram_exit(dgram);
   errsv = errno;
   EXPCT_EQ(ret, 0, 1, "dgram_exit failed: %d\n", errsv);
   if(OSAL_ERROR == OSAL_s32EventClose(hReadytoRead))
   {
     PR_ERR("OSAL_s32EventClose Failed\n");
   }
   if(OSAL_ERROR ==  OSAL_s32EventDelete("Fake Device Ready Notification"))
   {
     PR_ERR("OSAL_s32EventDelete Failed\n");
   }
   close(sockfd);

   return 0;
}

int inc_receive(uint8_t *buf)
{
   int len, errsv;

   do {
      len = dgram_recv(dgram, buf, sizeof(buf));
   } while(len < 0 && errno == EAGAIN);

   errsv = errno;
   EXPCT_GT(len, 0, len, "dgram_recv failed: len=%d errno=%d\n", len, errsv);

   return len;
}

int inc_send(uint8_t *buf, int len)
{
   int ret = dgram_send(dgram, buf, len);
   /* For syncing the transmitter and receiver speed */
   usleep(10);
   int errsv = errno;

   EXPCT_NE(ret, -1, 1, "dgram_send failed: %d\n", errsv)
   return 0;
}

int inc_comm_init(int protocol_version)
{
   uint8_t recvbuf[BUF_SIZE];
   uint8_t sendbuf[3] = {SCC_INPUT_DEVICE_R_COMPONENT_STATUS_MSGID,
         INPUT_INC_STATUS_ACTIVE, protocol_version};
   int len;

   PR_DBG("enter\n");
   if(OSAL_s32EventCreate("Fake Device Ready Notification", &hReadytoRead) == OSAL_ERROR)
   {
     PR_ERR("Creation of event failed\n");
     return 1;
   }
   len = inc_receive(recvbuf);
   if (len < 0)
      return 1;

   EXPCT_EQ(recvbuf[0], SCC_INPUT_DEVICE_C_COMPONENT_STATUS_MSGID, 2,
         "expected SCC_INPUT_DEVICE_C_COMPONENT_STATUS, got 0x%02X", recvbuf[0]);
   EXPCT_EQ(len, 3, 3,
         "invalid msg size: 0x%02X %d\n", recvbuf[0], len);
   EXPCT_EQ(recvbuf[1], INPUT_INC_STATUS_ACTIVE, 4,
         "invalid status: %d\n", recvbuf[1]);
   EXPCT_LT(recvbuf[2], (INPUT_INC_PROTOCOL_VERSION_CUR + 1), 5,
         "invalid protocol version: %d\n", recvbuf[2]);
   EXPCT_GT(recvbuf[2], (INPUT_INC_PROTOCOL_VERSION_MIN - 1), 5,
         "invalid protocol version: %d\n", recvbuf[2]);/*lint !e778, authorized LINT-deactivation #<72>*/ 

   return inc_send(sendbuf, sizeof(sendbuf));
}

int config_start(uint8_t num_devices)
{
   uint8_t recvbuf[BUF_SIZE];
   uint8_t sendbuf[2] = {SCC_INPUT_DEVICE_R_CONFIG_START_MSGID, num_devices};
   int len;

   len = inc_receive(recvbuf);
   if (len < 0)
      return 1;

   EXPCT_EQ(recvbuf[0], SCC_INPUT_DEVICE_C_CONFIG_START_MSGID, 2,
         "expected SCC_INPUT_DEVICE_C_CONFIG_START_MSGID, got 0x%02X", recvbuf[0]);
   EXPCT_EQ(len, 1, 3,
         "invalid msg size: 0x%02X %d\n", recvbuf[0], len);

   return inc_send(sendbuf, sizeof(sendbuf));
}

int config_type(uint8_t devid, uint16_t type, int ncodes, ...)
{
   int err;
   uint8_t sendbuf[BUF_SIZE];
   uint16_t code;
   int pos = 0;
   va_list code_list;

   *(uint8_t *)  (sendbuf + pos) = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID; pos++;
   *(uint8_t *)  (sendbuf + pos) = devid; pos++;
   *(uint16_t *) (sendbuf + pos) = type; pos += 2;
   *(uint8_t *)  (sendbuf + pos) = (uint8_t) ncodes; pos++;
   va_start(code_list, (long) ncodes);// Fix for lint CFG3-1015

   for (code = 0; code < (uint16_t) ncodes; code++)
   {
      *(uint16_t *) (sendbuf + pos) = (uint16_t) va_arg(code_list, int);
      pos += 2;
   }

   va_end(code_list);

   err = inc_send(sendbuf, pos);
   EXPCT_EQ(err, 0, 2, "send CONFIG_TYPE failed");

   return err;
}

int config_abs(uint8_t devid, uint16_t code,
      int32_t min, int32_t max, int32_t fuzz, int32_t flat)
{
   int err;
   uint8_t sendbuf[BUF_SIZE];
   int pos = 0;

   *(uint8_t *)  (sendbuf + pos) = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID; pos++;
   *(uint8_t *)  (sendbuf + pos) = devid; pos++;
   *(uint16_t *) (sendbuf + pos) = code; pos += 2;
   *(int32_t *)  (sendbuf + pos) = min; pos += 4;
   *(int32_t *)  (sendbuf + pos) = max; pos += 4;
   *(int32_t *)  (sendbuf + pos) = fuzz; pos += 4;
   *(int32_t *)  (sendbuf + pos) = flat; pos += 4;

   err = inc_send(sendbuf, pos);
   EXPCT_EQ(err, 0, 2, "send CONFIG_ABS failed");

   return err;
}

int config_mt_slots(uint8_t devid, uint8_t num_slots)
{
   int err;
   uint8_t sendbuf[BUF_SIZE];
   int pos = 0;

   *(uint8_t *)  (sendbuf + pos) = SCC_INPUT_DEVICE_R_CONFIG_MT_SLOTS_MSGID; pos++;
   *(uint8_t *)  (sendbuf + pos) = devid; pos++;
   *(uint8_t *)  (sendbuf + pos) = num_slots; pos++;

   err = inc_send(sendbuf, pos);
   EXPCT_EQ(err, 0, 2, "send CONFIG_MT_SLOTS failed");

   return err;
}

int config_end()
{
   uint8_t sendbuf[1] = {SCC_INPUT_DEVICE_R_CONFIG_END_MSGID};

   return inc_send(sendbuf, sizeof(sendbuf));
}

int send_event(int protocol_version, uint8_t devid, long nparams, ...)
{
   int err;
   uint8_t sendbuf[BUF_SIZE];
   uint16_t event;
   int pos = 0;
   uint8_t nevents = nparams / 3;
   va_list event_list;

   EXPCT_EQ((nparams % 3), 0, 3, "wrong number of event parameters %d\n",
         nparams);

   if (protocol_version == INPUT_INC_PROTOCOL_VERSION_CUR) {
      *(uint8_t *)  (sendbuf + pos) = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID + devid * 2; pos++;
   } else {
      *(uint8_t *)  (sendbuf + pos) = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID; pos++;
      *(uint8_t *)  (sendbuf + pos) = devid; pos++;
   }
   *(uint8_t *)  (sendbuf + pos) = nevents; pos++;
   va_start(event_list, nparams);

   for (event = 0; event < (uint16_t) nevents; event++)
   {
      *(uint16_t *) (sendbuf + pos) = (uint16_t) va_arg(event_list, long);
      pos += 2;
      *(uint16_t *) (sendbuf + pos) = (uint16_t) va_arg(event_list, long);
      pos += 2;
      *(uint32_t *) (sendbuf + pos) = (uint32_t) va_arg(event_list, long);
      pos += 4;
   }

   va_end(event_list);

   err = inc_send(sendbuf, pos);
   EXPCT_EQ(err, 0, 4, "send EVENT failed");

   return err;
}
/*************************************************************************
| function     : check_input_inc_fake_device_opened
|-------------------------------------------------------------------------
| parameters   : NULL
|
| returnvalue  : 0 /1
|-------------------------------------------------------------------------
| description  : This module is invoked from main function after
|                configuration is over.Check if the fake device is opened
|                in the rx thread
|-------------------------------------------------------------------------
|  History:
|  Date            Modification              Author
|  27/5/15         Initial Version           Shahida Mohammed Ashraf
**************************************************************************/
static tU32 check_input_inc_fake_device_opened(void)
{
  tS32 s32EventWaitRetVal;
  tU32 u32EventResultMask = 0;
  tU32 err = 1;
  /* wait some time to make sure input devices have been opened by rx_thread*/
  s32EventWaitRetVal = OSAL_s32EventWait(hReadytoRead, INPUT_INC_FAKE_DEVICES_OPENED,
	                                  OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &u32EventResultMask);
  /*When Eventwait fails*/
  if( s32EventWaitRetVal != OSAL_OK )
  {
    PR_ERR( "Notify Thread(): -Event wait fail- ");
  }
  else
  {
    /* Clear the events */
    if(OSAL_ERROR == OSAL_s32EventPost(hReadytoRead, ~u32EventResultMask, OSAL_EN_EVENTMASK_AND ))
    {
	  PR_ERR("Notify Thread (): - Clear Event fail ");
    }
    err = 0;
  }
 return err;
}
void *rx_thread(void *arg)
{
   int err, i, fd, len, rd;
   int ndevices = 0;
   char path[20];
   struct pollfd pollfdset[MAX_INPUT_DEVICES];
   uint8_t devid[MAX_INPUT_DEVICES];
   int nevents[MAX_INPUT_DEVICES];
   char dev_name[256] = "Unknown";
   unsigned short id[4];
   struct thread_arg *thread_arg = (struct thread_arg *) arg;
   struct input_event event;
   int *res = malloc(sizeof(int));

   if (res == NULL) {
      int errsv = errno;
      PR_ERR("memory allocation failed: %d\n", errsv);
      return NULL;
   }
   *res = 0;

   PR_DBG("test id: %d\n", thread_arg->test_id);
   err = component_init();
   if (err != 0)
   {
     inc_exit();//Bug fix for CFG3-1212
     *res = 7;
     return res;
   }

   for (i = 0; i < MAX_INPUT_DEVICES; i++)
   {
      err = snprintf(path, 20, "/dev/input/event%d", i);
      if (err <= 0) {
         PR_ERR("create path failed\n");
         *res = 2;
         return res;
      }

      pollfdset[i].events = 0;
      nevents[i] = 0;
      devid[i] = 0;

      fd = open(path, O_RDONLY);
      if (fd == -1) {
         pollfdset[i].fd = 0;
         continue;
      }

      err = ioctl(fd, EVIOCGNAME(sizeof(dev_name)), dev_name);
      if (err == -1) {
         int errsv = errno;
         PR_ERR("ioctl EVIOCGNAME failed: %d\n", errsv);
         *res = 3;
         return res;
      }

      PR_DBG("device %d name: %s\n", i, dev_name);

      /* only use input_fake devices */
      if (strcmp(dev_name, INPUT_DEVICE_NAME) != 0) {
         close(fd);
         pollfdset[i].fd = 0;
         continue;
      }

      err = ioctl(fd, EVIOCGID, id);
      if (err == -1) {
         int errsv = errno;
         PR_ERR("ioctl EVIOCGVERSION failed: %d\n", errsv);
         *res = 4;
         return res;
      }
      devid[i] = id[ID_VERSION];

      pollfdset[i].fd = fd;
      pollfdset[i].events = POLLIN;
      ndevices++;
   }

   for (i = 0; i < MAX_INPUT_DEVICES; i++)
      if (pollfdset[i].fd > 0) {
         PR_DBG("using device %d id %d\n", i, devid[i]);
      }

   /* Notification to the main function that the fake device is
    opened and is ready to receive events*/
   (tVoid)OSAL_s32EventPost(hReadytoRead, INPUT_INC_FAKE_DEVICES_OPENED,\
		   OSAL_EN_EVENTMASK_OR);

   while (1)
   {
      len = poll(pollfdset, MAX_INPUT_DEVICES, POLL_TIMEOUT_MS);
      if (len < 0)
      {
         int errsv = errno;
         PR_ERR("poll failed: %d\n", errsv);
         *res = 5;
         break;
      }
      if (len == 0)
      {
         PR_DBG("poll timeout\n");
         break;
      }

      for (i = 0; i < MAX_INPUT_DEVICES; i++)
      {
         if (pollfdset[i].fd == 0)
            continue;

         if (pollfdset[i].revents & POLLIN) {
            PR_DBG("POLLIN on device %d\n", i);
            pollfdset[i].revents = 0;

            rd = read(pollfdset[i].fd, &event, sizeof(struct input_event));
            if (rd < (int) sizeof(struct input_event)) {
               PR_ERR("expected %d bytes, got %d\n",
                     (int) sizeof(struct input_event), rd);
               *res = 6;
               return res;
            }
            nevents[i]++;

            err = test_funcs_assert_events[thread_arg->test_id](devid[i],
                  nevents[i], &event);
            if (err) {
               *res = 10 * err;
               return res;
            }
         }
      }
   }

   for (i = 0; i < MAX_INPUT_DEVICES; i++)
      if (pollfdset[i].fd > 0)
         close(pollfdset[i].fd);

   return res;
}

int init_rx_thread(pthread_t *ptid, struct thread_arg *thread_arg)
{
   int err;

   err = pthread_create(ptid, NULL, rx_thread, thread_arg);
   EXPCT_NE(err, -1, 8, "pthread_create failed\n");

   return 0;
}

int exit_rx_thread(pthread_t tid)
{
   int err;
   void *res;

   err = pthread_join(tid, &res);
   EXPCT_NE(err, -1, 9, "pthread_join failed\n");

   err = *(int *) res;
   EXPCT_EQ(err, 0, (10*err), "rx_thread returned err %d\n", err);

   /* free memory allocated by thread */
   free(res);

   return 0;
}

int run_test(int test_id, int protocol_version)
{
   int err;
   pthread_t tid;
   struct thread_arg *thr_arg;


   if (test_id < 0 || test_id >= NUMBER_OF_TESTS) {
      PR_ERR("unsupported test id: %d\n", test_id);
      return -1;
   }

   err = system_init();
   if (err != 0)
      return err;

   /* wait some time to make sure /etc/hosts is written
    * and network is reachable
    */
   usleep(1000000);

   err = inc_init();
   if (err != 0)
      return 1 * err;

   thr_arg = malloc(sizeof(struct thread_arg));
   if (thr_arg == NULL)
   {
     int errsv = errno;
     inc_exit();//Bug fix for CFG3-1212
     PR_ERR("memory allocation failed: %d\n", errsv);
     return -1;
   }
   thr_arg->test_id = test_id;

   err = init_rx_thread(&tid, thr_arg);
   if (err != 0)
   {
     inc_exit();//Bug fix for CFG3-1212
     free(thr_arg);
     return 10000 * err;
   }
   err = inc_comm_init(protocol_version);
   if (err != 0)
   {
	 inc_exit();//Bug fix for CFG3-1212
	 free(thr_arg);//Bug fix for CFG3-1234
     return 100 * err;
   }

   err = test_funcs_configure_devices[test_id]();
   if (err != 0)
   {
     inc_exit();//Bug fix for CFG3-1212
	 free(thr_arg);//Bug fix for CFG3-1234
     return 1000 * err;
   }
   /* Check if the fake device is opened in the rx thread.*/
   err = check_input_inc_fake_device_opened();//Bug fix for 1234
   if (err != 0)
   {
     inc_exit();//Bug fix CFG3-1212
     free(thr_arg);
     return 10 * err;
    }

   err = test_funcs_generate_events[test_id](protocol_version);
   if (err != 0)
   {
     inc_exit();//Bug fix for CFG3-1212
     free(thr_arg);
     return 100000 * err;
   }

   err = exit_rx_thread(tid);
   if (err != 0)
   {
     inc_exit();//Bug fix for CFG3-1212
     free(thr_arg);
     return 1000000 * err;
   }

   inc_exit();

   free(thr_arg);
   return 0;
}

#if 0
int main()
{
   int err, i;

   PR_DBG("enter\n");

   for (i = 0; i < NUMBER_OF_TESTS; i++)
   {
      err = run_test(i, 1);
      if (err)
         PR_ERR("test id %d failed: %d\n", i, err);
      else
         PR_ERR("test id %d succeeded\n", i);
   }
}
#endif

/***************************************************************
 * test 0: multiple_devices
 ***************************************************************/
int test_multiple_devices_configure_devices(void)
{
   int err = 0;

   err = config_start(4);
   if (err)
      return err;

   err = config_type(0, EV_KEY, 9,
         KEY_ENTER, KEY_MENU, KEY_BACK,
         KEY_POWER, KEY_PHONE, KEY_COMPUTER,
         KEY_MEDIA, KEY_RADIO, KEY_OPTION);
   if (err)
      return err;
   err = config_type(1, EV_KEY, 5,
         KEY_PREVIOUS, KEY_NEXT, KEY_CHANNEL,
         KEY_POWER, KEY_MUTE);
   if (err)
      return err;
   err = config_type(2, EV_REL, 3,
         REL_X, REL_Y, REL_Z);
   if (err)
      return err;
   err = config_type(3, EV_ABS, 2,
         ABS_X, ABS_Y);
   if (err)
      return err;
   err = config_type(3, EV_KEY, 1,
         BTN_TOUCH);
   if (err)
      return err;
   err = config_abs(3, ABS_X, 0, 1024, 0, 0);
   if (err)
      return err;
   err = config_abs(3, ABS_Y, 0, 768, 0, 0);
   if (err)
      return err;

   return config_end();
}

int test_multiple_devices_generate_events(int protocol_version)
{
   int err = 0;

   err = send_event(protocol_version, 0, 1*3,
         EV_KEY, KEY_POWER, 1);
   if (err) return err;

   err = send_event(protocol_version, 1, 1*3,
         EV_KEY, KEY_POWER, 1);
   if (err) return err;

   err = send_event(protocol_version, 0, 2*3,
         EV_KEY, KEY_RADIO, 1,
         EV_KEY, KEY_POWER, 0);
   if (err) return err;

   err = send_event(protocol_version, 3, 3*3,
         EV_ABS, ABS_X, 222,
         EV_ABS, ABS_Y, 333,
         EV_KEY, BTN_TOUCH, 1);
   if (err) return err;

   err = send_event(protocol_version, 2, 1*3,
         EV_REL, REL_Y, 2);
   if (err) return err;

   err = send_event(protocol_version, 0, 3*3,
         EV_KEY, KEY_RADIO, 0,
         EV_SYN, SYN_REPORT, 0,
         EV_KEY, KEY_BACK, 1);
   if (err) return err;

   err = send_event(protocol_version, 1, 1*3,
         EV_KEY, KEY_POWER, 0);
   if (err) return err;

   err = send_event(protocol_version, 3, 3*3,
         EV_ABS, ABS_X, 555,
         EV_ABS, ABS_Y, 111,
         EV_KEY, BTN_TOUCH, 0);
   if (err) return err;

   err = send_event(protocol_version, 2, 1*3,
         EV_REL, REL_Y, -5);

   return err;
}

int test_multiple_devices_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   switch (dev_id)
   {
   case 0: /* device id */
      switch (event_num)
      {
      case 1: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_POWER,  3, "expected code %d, got %d\n",  KEY_POWER,  event->code);
         EXPCT_EQ(event->value, 1,          4, "expected value %d, got %d\n", 1,          event->value);
         break;
      case 2: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 3: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_RADIO,  3, "expected code %d, got %d\n",  KEY_RADIO,  event->code);
         EXPCT_EQ(event->value, 1,          4, "expected value %d, got %d\n", 1,          event->value);
         break;
      case 4: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_POWER,  3, "expected code %d, got %d\n",  KEY_POWER,  event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 5: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 6: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_RADIO,  3, "expected code %d, got %d\n",  KEY_RADIO,  event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 7: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 8: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_BACK,   3, "expected code %d, got %d\n",  KEY_BACK,   event->code);
         EXPCT_EQ(event->value, 1,          4, "expected value %d, got %d\n", 1,          event->value);
         break;
      case 9: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      default:
         PR_ERR("unexpected event number: %d\n", event_num);
         return 8;
      }
      break;
   case 1: /* device id */
      switch (event_num)
      {
      case 1: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_POWER,  3, "expected code %d, got %d\n",  KEY_POWER,  event->code);
         EXPCT_EQ(event->value, 1,          4, "expected value %d, got %d\n", 1,          event->value);
         break;
      case 2: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 3: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  KEY_POWER,  3, "expected code %d, got %d\n",  KEY_POWER,  event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 4: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      default:
         PR_ERR("unexpected event number: %d\n", event_num);
         return 8;
      }
      break;
   case 2: /* device id */
      switch (event_num)
      {
      case 1: /* event number */
         EXPCT_EQ(event->type,  EV_REL,     2, "expected type %d, got %d\n",  EV_REL,     event->type);
         EXPCT_EQ(event->code,  REL_Y,      3, "expected code %d, got %d\n",  REL_Y,      event->code);
         EXPCT_EQ(event->value, 2,          4, "expected value %d, got %d\n", 2,          event->value);
         break;
      case 2: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 3: /* event number */
         EXPCT_EQ(event->type,  EV_REL,     2, "expected type %d, got %d\n",  EV_REL,     event->type);
         EXPCT_EQ(event->code,  REL_Y,      3, "expected code %d, got %d\n",  REL_Y,      event->code);
         EXPCT_EQ(event->value, -5,         4, "expected value %d, got %d\n", -5,         event->value);
         break;
      case 4: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      default:
         PR_ERR("unexpected event number: %d\n", event_num);
         return 8;
      }
      break;
   case 3: /* device id */
      switch (event_num)
      {
      case 1: /* event number */
         EXPCT_EQ(event->type,  EV_ABS,     2, "expected type %d, got %d\n",  EV_ABS,     event->type);
         EXPCT_EQ(event->code,  ABS_X,      3, "expected code %d, got %d\n",  ABS_X,      event->code);
         EXPCT_EQ(event->value, 222,        4, "expected value %d, got %d\n", 222,        event->value);
         break;
      case 2: /* event number */
         EXPCT_EQ(event->type,  EV_ABS,     2, "expected type %d, got %d\n",  EV_ABS,     event->type);
         EXPCT_EQ(event->code,  ABS_Y,      3, "expected code %d, got %d\n",  ABS_Y,      event->code);
         EXPCT_EQ(event->value, 333,        4, "expected value %d, got %d\n", 333,        event->value);
         break;
      case 3: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  BTN_TOUCH,  3, "expected code %d, got %d\n",  BTN_TOUCH,  event->code);
         EXPCT_EQ(event->value, 1,          4, "expected value %d, got %d\n", 1,          event->value);
         break;
      case 4: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 5: /* event number */
         EXPCT_EQ(event->type,  EV_ABS,     2, "expected type %d, got %d\n",  EV_ABS,     event->type);
         EXPCT_EQ(event->code,  ABS_X,      3, "expected code %d, got %d\n",  ABS_X,      event->code);
         EXPCT_EQ(event->value, 555,        4, "expected value %d, got %d\n", 555,        event->value);
         break;
      case 6: /* event number */
         EXPCT_EQ(event->type,  EV_ABS,     2, "expected type %d, got %d\n",  EV_ABS,     event->type);
         EXPCT_EQ(event->code,  ABS_Y,      3, "expected code %d, got %d\n",  ABS_Y,      event->code);
         EXPCT_EQ(event->value, 111,        4, "expected value %d, got %d\n", 111,        event->value);
         break;
      case 7: /* event number */
         EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
         EXPCT_EQ(event->code,  BTN_TOUCH,  3, "expected code %d, got %d\n",  BTN_TOUCH,  event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      case 8: /* event number */
         EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
         EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
         EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
         break;
      default:
         PR_ERR("unexpected event number: %d\n", event_num);
         return 8;
      }
      break;
   default:
      PR_ERR("unexpected device id: %d\n", dev_id);
      return 9;
   }

   return err;
}

/***************************************************************
 * test 1: many_events
 ***************************************************************/
int test_many_events_configure_devices(void)
{
   int err = 0;

   PR_DBG("enter\n");

   err = config_start(1);
   if (err)
      return err;

   err = config_type(0, EV_REL, 1, REL_X);
   if (err)
      return err;

   return config_end();
}

int test_many_events_generate_events(int protocol_version)
{
   int err = 0, val;

   PR_DBG("enter\n");

   for (val = 1; val < 1000; val++)
   {
      err = send_event(protocol_version, 0, 1*3, EV_REL, REL_X, val);
      if (err)
         break;

      /* leave some time between events to prevent buffer overrun
       * in event queue (SYN_DROPPED) */
      usleep(10000);
   }

   return err;
}

int test_many_events_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0, value;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   EXPCT_EQ(dev_id, 0, 5, "expected device id %d, got %d\n", 0, dev_id);

   if (event_num % 2) {
      value = ((event_num+1)/2);
      EXPCT_EQ(event->type,  EV_REL,     2, "expected type %d, got %d\n",  EV_REL,     event->type);
      EXPCT_EQ(event->code,  REL_X,      3, "expected code %d, got %d\n",  REL_X,      event->code);
      EXPCT_EQ(event->value, value,      4, "expected value %d, got %d\n", value,      event->value);
   } else {
      EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
      EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
      EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
   }

   return err;
}

/***************************************************************
 * test 2: many_codes
 ***************************************************************/
int test_many_codes_configure_devices(void)
{
   int err = 0, code;

   PR_DBG("enter\n");

   err = config_start(1);
   if (err)
      return err;

   for (code = KEY_ESC; code <= KEY_MAX_VANILLA; code++)
   {
      err = config_type(0, EV_KEY, 1, code);
      if (err)
         return err;
   }

   return config_end();
}

int test_many_codes_generate_events(int protocol_version)
{
   int err = 0, code;

   PR_DBG("enter\n");

   for (code = KEY_ESC; code <= KEY_MAX_VANILLA; code++)
     {
      if (code == KEY_SYSRQ)
         err = send_event(protocol_version, 0, 1*3, EV_KEY, KEY_ESC, 0);
      else
         err = send_event(protocol_version, 0, 1*3, EV_KEY, code, 1);
      if (err)
         break;

      /* leave some time between events to prevent buffer overrun
       * in event queue (SYN_DROPPED) */
      usleep(20000);
   }

   return err;
}

int test_many_codes_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0, code, value = 1;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   EXPCT_EQ(dev_id, 0, 5, "expected device id %d, got %d\n", 0, dev_id);

   if (event_num % 2) {
      code = (event_num + 1) / 2;
      if (code == KEY_SYSRQ) {
         code = KEY_ESC;
         value = 0;
      }
      EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
      EXPCT_EQ(event->code,  code,       3, "expected code %d, got %d\n",  code,       event->code);
      EXPCT_EQ(event->value, value,      4, "expected value %d, got %d\n", value,      event->value);
   } else {
      EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
      EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
      EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
   }

   return err;
}

/***************************************************************
 * test 3: extended_keys
 ***************************************************************/
int test_extended_keys_configure_devices(void)
{
   int err = 0, code;

   PR_DBG("enter\n");

   err = config_start(1);
   if (err)
      return err;

   for (code = KEY_MAX_VANILLA + 1; code <= KEY_MAX_EXTENDED; code++)
   {
      err = config_type(0, EV_KEY, 1, code);
      if (err)
         return err;
   }

   return config_end();
}

int test_extended_keys_generate_events(int protocol_version)
{
   int err = 0, code;

   PR_DBG("enter\n");

   for (code = KEY_MAX_VANILLA + 1; code <= KEY_MAX_EXTENDED; code++)
     {
      err = send_event(protocol_version, 0, 1*3, EV_KEY, code, 1);
      if (err)
         break;

      /* leave some time between events to prevent buffer overrun
       * in event queue (SYN_DROPPED) */
      usleep(10000);
   }

   return err;
}

int test_extended_keys_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0, code, value = 1;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   EXPCT_EQ(dev_id, 0, 5, "expected device id %d, got %d\n", 0, dev_id);

   if (event_num % 2) {
      code = KEY_MAX_VANILLA + (event_num + 1) /2;
      EXPCT_EQ(event->type,  EV_KEY,     2, "expected type %d, got %d\n",  EV_KEY,     event->type);
      EXPCT_EQ(event->code,  code,       3, "expected code %d, got %d\n",  code,       event->code);
      EXPCT_EQ(event->value, value,      4, "expected value %d, got %d\n", value,      event->value);
   } else {
      EXPCT_EQ(event->type,  EV_SYN,     2, "expected type %d, got %d\n",  EV_SYN,     event->type);
      EXPCT_EQ(event->code,  SYN_REPORT, 3, "expected code %d, got %d\n",  SYN_REPORT, event->code);
      EXPCT_EQ(event->value, 0,          4, "expected value %d, got %d\n", 0,          event->value);
   }

   return err;
}

/***************************************************************
 * test 4: multitouch_a (multi-touch protocol for type A devices)
 ***************************************************************/
int test_multitouch_type_a_configure_devices(void)
{
   int err = 0;

   PR_DBG("enter\n");

   err = config_start(1);
   if (err)
      return err;

   err = config_type(0, EV_ABS, 0);
   if (err)
      return err;

   err = config_abs(0, ABS_MT_POSITION_X, 0, 1024, 0, 0);
   if (err)
      return err;

   err = config_abs(0, ABS_MT_POSITION_Y, 0, 1024, 0, 0);
   if (err)
      return err;

   return config_end();
}

int test_multitouch_type_a_generate_events(int protocol_version)
{
   int err = 0;

   PR_DBG("enter\n");

   /* two-contact touch */
   err = send_event(protocol_version, 0, 6*3,
         EV_ABS, ABS_MT_POSITION_X, 100,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_SYN, SYN_MT_REPORT, 0,
         EV_ABS, ABS_MT_POSITION_X, 800,
         EV_ABS, ABS_MT_POSITION_Y, 600,
         EV_SYN, SYN_MT_REPORT, 0);
   if (err) return err;

   /* lifting first contact */
   err = send_event(protocol_version, 0, 3*3,
         EV_ABS, ABS_MT_POSITION_X, 850,
         EV_ABS, ABS_MT_POSITION_Y, 650,
         EV_SYN, SYN_MT_REPORT, 0);
   if (err) return err;

   /* lifting second contact */
   err = send_event(protocol_version, 0, 1*3,
         EV_SYN, SYN_MT_REPORT, 0);
   if (err) return err;

   return err;
}

int test_multitouch_type_a_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   EXPCT_EQ(dev_id, 0, 5, "expected device id %d, got %d\n", 0, dev_id);

   switch (event_num)
   {
   case 1: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 100,                4, "expected value %d, got %d\n", 100,                event->value);
      break;
   case 2: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 200,                4, "expected value %d, got %d\n", 200,                event->value);
       break;
   case 3: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_MT_REPORT,      3, "expected code %d, got %d\n",  SYN_MT_REPORT,      event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 4: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 800,                4, "expected value %d, got %d\n", 800,                event->value);
      break;
   case 5: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 600,                4, "expected value %d, got %d\n", 600,                event->value);
       break;
   case 6: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_MT_REPORT,      3, "expected code %d, got %d\n",  SYN_MT_REPORT,      event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 7: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 8: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 850,                4, "expected value %d, got %d\n", 850,                event->value);
      break;
   case 9: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 650,                4, "expected value %d, got %d\n", 650,                event->value);
       break;
   case 10: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_MT_REPORT,      3, "expected code %d, got %d\n",  SYN_MT_REPORT,      event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 11: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 12: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_MT_REPORT,      3, "expected code %d, got %d\n",  SYN_MT_REPORT,      event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   case 13: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
       break;
   default:
      PR_ERR("unexpected event number: %d\n", event_num);
      return 8;
   }

   return err;
}

/***************************************************************
 * test 5: multitouch_b (multi-touch protocol for type B devices)
 ***************************************************************/
int test_multitouch_type_b_configure_devices(void)
{
   int err = 0;

   PR_DBG("enter\n");

   err = config_start(1);
   if (err)
      return err;

   err = config_type(0, EV_ABS, 2, ABS_MT_POSITION_X, ABS_MT_POSITION_Y);
   if (err)
      return err;

   err = config_mt_slots(0, 4);

   err = config_abs(0, ABS_MT_POSITION_X, 0, 1024, 0, 0);
   if (err)
      return err;

   err = config_abs(0, ABS_MT_POSITION_Y, 0, 1024, 0, 0);
   if (err)
      return err;

   return config_end();
}

int test_multitouch_type_b_generate_events(int protocol_version)
{
   int err = 0;

   PR_DBG("enter\n");

   /* four-finger touch */
   err = send_event(protocol_version, 0, 12*3,
         EV_ABS, ABS_MT_SLOT, 0 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 100,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 1 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 50,
         EV_ABS, ABS_MT_POSITION_Y, 50,
         EV_ABS, ABS_MT_SLOT, 2 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 400,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 3 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 111,
         EV_ABS, ABS_MT_POSITION_Y, 111);
   if (err) return err;

   /* raise 3rd finger */
   err = send_event(protocol_version, 0, 10*3,
         EV_ABS, ABS_MT_SLOT, 0 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 100,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 1 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 50,
         EV_ABS, ABS_MT_POSITION_Y, 50,
         EV_ABS, ABS_MT_SLOT, 2 | 0 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_SLOT, 3 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 111,
         EV_ABS, ABS_MT_POSITION_Y, 111);
   if (err) return err;

   /* move first finger in X direction only, omit 3rd finger */
   err = send_event(protocol_version, 0, 9*3,
         EV_ABS, ABS_MT_SLOT, 0 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 150,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 1 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 50,
         EV_ABS, ABS_MT_POSITION_Y, 50,
         EV_ABS, ABS_MT_SLOT, 3 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 111,
         EV_ABS, ABS_MT_POSITION_Y, 111);
   if (err) return err;

   /* move 2nd finger in X and Y direction */
   err = send_event(protocol_version, 0, 9*3,
         EV_ABS, ABS_MT_SLOT, 0 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 150,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 1 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 70,
         EV_ABS, ABS_MT_POSITION_Y, 30,
         EV_ABS, ABS_MT_SLOT, 3 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 111,
         EV_ABS, ABS_MT_POSITION_Y, 111);
   if (err) return err;

   /* no change */
   err = send_event(protocol_version, 0, 9*3,
         EV_ABS, ABS_MT_SLOT, 0 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 150,
         EV_ABS, ABS_MT_POSITION_Y, 200,
         EV_ABS, ABS_MT_SLOT, 1 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 70,
         EV_ABS, ABS_MT_POSITION_Y, 30,
         EV_ABS, ABS_MT_SLOT, 3 | 1 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_POSITION_X, 111,
         EV_ABS, ABS_MT_POSITION_Y, 111);
   if (err) return err;

   /* raise the remaining fingers */
   err = send_event(protocol_version, 0, 3*3,
         EV_ABS, ABS_MT_SLOT, 0 | 0 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_SLOT, 1 | 0 << 8 | 0 << 16, // slot / state / tool
         EV_ABS, ABS_MT_SLOT, 3 | 0 << 8 | 0 << 16);// slot / state / tool

   return err;
}

int test_multitouch_type_b_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0;

   PR_DBG("devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);

   EXPCT_EQ(dev_id, 0, 5, "expected device id %d, got %d\n", 0, dev_id);

   switch (event_num)
   {
   case 1: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      break;
   case 2: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 100,                4, "expected value %d, got %d\n", 100,                event->value);
      break;
   case 3: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 200,                4, "expected value %d, got %d\n", 200,                event->value);
      break;
   case 4: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 1,                  4, "expected value %d, got %d\n", 1,                  event->value);
      break;
   case 5: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      break;
   case 6: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 50,                 4, "expected value %d, got %d\n", 50,                 event->value);
      break;
   case 7: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 50,                 4, "expected value %d, got %d\n", 50,                 event->value);
      break;
   case 8: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 2,                  4, "expected value %d, got %d\n", 2,                  event->value);
      break;
   case 9: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      break;
   case 10: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 400,                4, "expected value %d, got %d\n", 400,                event->value);
      break;
   case 11: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 200,                4, "expected value %d, got %d\n", 200,                event->value);
      break;
   case 12: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 3,                  4, "expected value %d, got %d\n", 3,                  event->value);
      break;
   case 13: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      break;
   case 14: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 111,                4, "expected value %d, got %d\n", 111,                event->value);
      break;
   case 15: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 111,                4, "expected value %d, got %d\n", 111,                event->value);
      break;
   case 16: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 17: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 2,                  4, "expected value %d, got %d\n", 2,                  event->value);
      break;
   case 18: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      EXPCT_EQ(event->value, -1,                 4, "expected value %d, got %d\n", -1,                 event->value);
      break;
   case 19: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 20: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 21: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 150,                4, "expected value %d, got %d\n", 150,                event->value);
      break;
   case 22: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 23: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 1,                  4, "expected value %d, got %d\n", 1,                  event->value);
      break;
   case 24: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_X,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_X,  event->code);
      EXPCT_EQ(event->value, 70,                 4, "expected value %d, got %d\n", 70,                 event->value);
      break;
   case 25: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_POSITION_Y,  3, "expected code %d, got %d\n",  ABS_MT_POSITION_Y,  event->code);
      EXPCT_EQ(event->value, 30,                 4, "expected value %d, got %d\n", 30,                 event->value);
      break;
   case 26: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 27: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   case 28: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      EXPCT_EQ(event->value, -1,                 4, "expected value %d, got %d\n", -1,                 event->value);
      break;
   case 29: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 1,                  4, "expected value %d, got %d\n", 1,                  event->value);
      break;
   case 30: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      EXPCT_EQ(event->value, -1,                 4, "expected value %d, got %d\n", -1,                 event->value);
      break;
   case 31: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_SLOT,        3, "expected code %d, got %d\n",  ABS_MT_SLOT,        event->code);
      EXPCT_EQ(event->value, 3,                  4, "expected value %d, got %d\n", 3,                  event->value);
      break;
   case 32: /* event number */
      EXPCT_EQ(event->type,  EV_ABS,             2, "expected type %d, got %d\n",  EV_ABS,             event->type);
      EXPCT_EQ(event->code,  ABS_MT_TRACKING_ID, 3, "expected code %d, got %d\n",  ABS_MT_TRACKING_ID, event->code);
      EXPCT_EQ(event->value, -1,                 4, "expected value %d, got %d\n", -1,                 event->value);
      break;
   case 33: /* event number */
      EXPCT_EQ(event->type,  EV_SYN,             2, "expected type %d, got %d\n",  EV_SYN,             event->type);
      EXPCT_EQ(event->code,  SYN_REPORT,         3, "expected code %d, got %d\n",  SYN_REPORT,         event->code);
      EXPCT_EQ(event->value, 0,                  4, "expected value %d, got %d\n", 0,                  event->value);
      break;
   default:
      PR_ERR("unexpected event number: %d\n", event_num);
      return 8;
   }

   return err;
}

/***************************************************************
 * test 6: nonfatal_protocol_violations
 ***************************************************************/
int test_nonfatal_protocol_violations_configure_devices(void)
{
   int err = 0;

   PR_DBG("enter\n");

   /* num devices exceeds max */
   err = config_start(55);
   if (err)
      return err;

   err = config_type(0, EV_KEY, 9,
         KEY_ENTER, KEY_MENU, KEY_BACK,
         KEY_POWER, KEY_PHONE, KEY_COMPUTER,
         KEY_MEDIA, KEY_RADIO, KEY_OPTION);
   if (err)
      return err;

   return config_end();
}

int test_nonfatal_protocol_violations_generate_events(int protocol_version)
{
   int err = 0;
   uint8_t sendbuf[BUF_SIZE];

   PR_DBG("enter\n");

   /* invalid device id */
   err = send_event(protocol_version, 66, 1*3,
         EV_KEY, KEY_POWER, 1);
   if (err) return err;

   /* invalid msgsize */
   if (protocol_version == INPUT_INC_PROTOCOL_VERSION_CUR) {
	   sendbuf[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
	   sendbuf[1] = 5;
   } else {
	   sendbuf[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
	   sendbuf[1] = 0;
	   sendbuf[2] = 5;
   }
   err = inc_send(sendbuf, 3);
   if (err) return err;

   /* msg size mismatch */
   if (protocol_version == INPUT_INC_PROTOCOL_VERSION_CUR) {
	   sendbuf[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
	   sendbuf[1] = 5;
   } else {
	   sendbuf[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
	   sendbuf[1] = 0;
	   sendbuf[2] = 5;
   }
   err = inc_send(sendbuf, 12);
   if (err) return err;

   /* invalid type */
   err = send_event(protocol_version, 0, 1*3,
         66, 0, 0);
   if (err) return err;

   /* invalid code */
   err = send_event(protocol_version, 0, 1*3,
         EV_SYN, 77, 0);
   if (err) return err;

   return err;
}

int test_nonfatal_protocol_violations_assert_events(uint8_t dev_id, int event_num,
      struct input_event *event)
{
   int err = 0;

   PR_ERR("expect nothing, got devid %d event_num %d type %d code %d value %d\n",
         dev_id, event_num, event->type, event->code, event->value);
   return err;
}

/*
 * OSAL wrappers
 */
tU32 u32InputIncMultipleDevicesV1(tVoid)
{
   return run_test(0, 1);
}

tU32 u32InputIncMultipleDevicesV2(tVoid)
{
   return run_test(0, 2);
}

tU32 u32InputIncManyEventsV1(tVoid)
{
   return run_test(1, 1);
}

tU32 u32InputIncManyEventsV2(tVoid)
{
   return run_test(1, 2);
}

tU32 u32InputIncManyCodesV1(tVoid)
{
   return run_test(2, 1);
}

tU32 u32InputIncManyCodesV2(tVoid)
{
   return run_test(2, 2);
}

tU32 u32InputIncExtendedKeysV1(tVoid)
{
   return run_test(3, 1);
}

tU32 u32InputIncExtendedKeysV2(tVoid)
{
   return run_test(3, 2);
}

tU32 u32InputIncMultiTouchTypeAV1(tVoid)
{
   return run_test(4, 1);
}

tU32 u32InputIncMultiTouchTypeAV2(tVoid)
{
   return run_test(4, 2);
}

tU32 u32InputIncMultiTouchTypeBV1(tVoid)
{
   return run_test(5, 1);
}

tU32 u32InputIncMultiTouchTypeBV2(tVoid)
{
   return run_test(5, 2);
}

tU32 u32InputIncNonFatalProtoViolationsV1(tVoid)
{
   return run_test(6, 1);
}

tU32 u32InputIncNonFatalProtoViolationsV2(tVoid)
{
   return run_test(6, 2);
}

