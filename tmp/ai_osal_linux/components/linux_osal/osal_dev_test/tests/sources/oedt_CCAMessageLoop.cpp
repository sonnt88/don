/*=============================================================================
  =======                         INCLUDES                              =======
  =============================================================================*/

/*------  project includes -------*/
#include "oedt_CCAMessageLoop.h"

/*------  standard includes -------*/
#define REG_S_IMPORT_INTERFACE_GENERIC
#include "reg_if.h"

#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"

#define SYSTEM_S_IMPORT_INTERFACE_EXH
#include <stl_pif.h>

#include "ail_trace.h"

#include "oedt_helper_funcs.h"

/*=============================================================================
  =======                       VARIABLE DECLARATIONS                   =======
  =============================================================================*/
const tChar* CCAMessageLoop::smszProcessName = "PROCOEDT";

/*=============================================================================
  =======                       IMPLEMENTATIOM              						 ======
  =============================================================================*/
CCAMessageLoop::CCAMessageLoop(const tChar szTaskName[9], tU16 u16AppID, tU16 u16PartnerAppID, tU16 u16ServiceID)
{
	// Set values for this CCA thread
	(tVoid)OSAL_szStringNCopy(mszTaskName, szTaskName, 8);
	mszTaskName[8]      = 0;
	u16AppId            = u16AppID;
	mu16PartnerAppID    = u16PartnerAppID;
	mu16ServiceID       = u16ServiceID;
	// Set default values
	mu32MaxMailboxSize  = MAX_MSG_LENGTH + AMT_C_U32_BASEMSG_ABSMSGSIZE + AMT_C_U32_SVCDATA_RELMSGSIZE;
	mu32ThreadPriority  = THREAD_PRITORITY;
	mu32ThreadStacksize = THREAD_STACKSIZE;
	mu32MaxMessages     = 5;
	mbPartnerRegistered = FALSE;
	// Initialize buffer
	memset(mpcMessageBuffer, 42, MAX_MSG_LENGTH);
	// Setting up registry for CCA thread initialization
	bSetupRegistry();
}

/*virtual*/  CCAMessageLoop::~CCAMessageLoop()
{
}

tBool CCAMessageLoop::bSetupRegistryBase(tVoid)const
{
	reg_tclRegKey oNewKey;
	// Create base structures for all proceses
	if (! oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE))
	{
		if (! oNewKey.bCreate(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE))
			return FALSE;
	}
	if (! oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_SOFTWARE))
	{
		if (! oNewKey.bCreate(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_SOFTWARE))
			return FALSE;
	}
	if (! oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_BLAUPUNKT))
	{
		if (! oNewKey.bCreate(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_BLAUPUNKT))
			return FALSE;
	}
	if (! oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_PROCESS))
	{
		if (! oNewKey.bCreate(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_PROCESS))
			return FALSE;
	}
	if (! oNewKey.bRelOpen(smszProcessName))
	{
		oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_PROCESS);
		if (! oNewKey.bRelCreate(smszProcessName))
			return FALSE;
	}

	return TRUE;
}

tBool CCAMessageLoop::bSetupRegistry(tVoid)
{
	if (! bSetupRegistryBase())
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creating registry base structure for CCA failed");
		return FALSE;		
	}
	
	reg_tclRegKey oNewKey;
	tBool bRegCrSuc = TRUE;
	// Set path to test process
	bRegCrSuc = bRegCrSuc && oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/" REGKEY_PROCESS);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(smszProcessName);
  // Create the registry entries for this CCA task 
	if (! oNewKey.bRelCreate(mszTaskName))
	{
		// Check if entry already existed
		if (OSAL_u32ErrorCode() == OSAL_E_ALREADYEXISTS)
		{
			// Entry already exists from former test -> reuse registry structure
			return TRUE;
		}
		else
		{
			// Diferenz Problem 
			return FALSE;
		}
	}
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32( REGVALUE_APPID, u16AppId);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32( REGVALUE_MAXMCOUNT, mu32MaxMessages);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32( REGVALUE_MAXMSIZE, mu32MaxMailboxSize);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelCreate(REGKEY_AIF_CONFIG);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32( REGVALUE_MSG_COUNT_INTERMEDIATE_MBX, mu32MaxMessages);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32( REGVALUE_WATCHDOG_INTERVALL, mu32WatchdogInterval);
	bRegCrSuc = bRegCrSuc && oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/"  REGKEY_PROCESS);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(smszProcessName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(mszTaskName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelCreate(REGKEY_APP_CONFIG);
	bRegCrSuc = bRegCrSuc && oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/"  REGKEY_PROCESS);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(smszProcessName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(mszTaskName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelCreate(REGKEY_APP_THREAD);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32("AE_PRIO", mu32ThreadPriority);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32("AE_STCK", mu32ThreadStacksize);
	bRegCrSuc = bRegCrSuc && oNewKey.bOpen(OSAL_C_STRING_DEVICE_REGISTRY "/" REGSTRING_LOCAL_MACHINE "/"  REGKEY_PROCESS);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(smszProcessName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelOpen(mszTaskName);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelCreate(REGKEY_SERVICES);
	bRegCrSuc = bRegCrSuc && oNewKey.bRelCreate(mszTaskName);
	bRegCrSuc = bRegCrSuc && oNewKey.bSetU32(REGVALUE_SERVICEID, mu16ServiceID);
	// Check if at least one error occured
	if (! bRegCrSuc) 
	{
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Creating registry keys for CCA failed");
		return FALSE;		
	}
	else 
		return TRUE;
}

tBool CCAMessageLoop::vStart(tVoid) 
{
	return bInitInstance(AMT_C_U16_APPID_INVALID, u16AppId);
}

tVoid CCAMessageLoop::vAppEntry ()
{
	// Register partner
	ail_tclServiceReference partnerReference(mu16PartnerAppID, mu16ServiceID, 0, 0);
	partnerReference.vSetServiceState(AMT_C_U8_SVCSTATE_AVAILABLE);
	poServiceReferenceList->vAdd(partnerReference);

	// Check which application is started
	if (mu16PartnerAppID < u16AppId)
	{
		// This is the second thread, so send start message
		OSAL_tMQueueHandle oPartnerQueueHandle = scd_OpenQueue(mu16PartnerAppID);
		bSendTestMessage(1, CCA_C_U16_FCT_START);
		scd_s32CloseQueue(oPartnerQueueHandle);
	}
	// *************************************************************************
	// ****    Code copied from ail_tclAppInterfaceRestricted::vAppEntry    ****
	// *************************************************************************
	tBool                   bBlocked = TRUE;
	amt_tclBaseMessage      oMsgObject;
	tU32                    u32Prio;


	tBool bExhContextCreated = exh_bCreateExceptionContext( u32EntryJumpStackSize
														 , u32EntryDestroyStackSize
														 );
	if( bExhContextCreated)
	{
		ail_vTraceMsg( TR_LEVEL_USER_1, "Application %d: Hello CCAMessageLoop-world!", u16AppId );
		
		_BP_TRY_BEGIN
		{		
			/* application is ready to receive a command from the supervisor */
			enInterfaceState = AIL_EN_N_APPLICATION_INITIALIZED;
		
			if ( poInternalDispatch != NULL )
			{
				while ( bBlocked )
				{
					if ( ail_bIpcMessageWait( u16AppId, hMyInQueue, &oMsgObject, &u32Prio, OSAL_C_U32_INFINITE ) )
					{
					  bBlocked = poInternalDispatch->bDispatchCCAMessages(&oMsgObject);
					}
				}
			}
			else
			{
				ail_vTraceMsg( TR_LEVEL_ERROR, "Application %d: poInternalDispatch==NULL" );
			}
		}
		_BP_CATCH_ALL
		{
			ail_vTraceMsg( TR_LEVEL_ERROR, "Application %d: unhandled exception occured in Entry-Thread", u16AppId );
			vOnUnhandledAilException();
		}
		_BP_CATCH_END
		
		// stop exception-handling
		exh_vFreeExceptionContext();
	}
	else
	{
		ail_vTraceMsg( TR_LEVEL_ERROR, "Application %d: create exception-handling for entry-thread failed!", u16AppId );
	}

	/* release all basic class objects && inform LPX of correct shutdown */
	vTerminateMySelf ();

	/* from now, ail tracing is forbidden */
	ail_vExitAilTrace();
}

tVoid CCAMessageLoop::vOnNewMessage (amt_tclBaseMessage* poMessage)
{
	CCATestmessage newMessage(poMessage);
	switch(newMessage.u16GetFunctionID())
	{
	case CCA_C_U16_FCT_START:
	{
		// Got the start message from partner because it is ready now
		// Test is started, so set the counter to the startvalues
		mu32CurrentSizeCount   = 0;
		mu32CurrentNumberCount = 0;
		mu32MessagesToSend     = u32NrOfMsg[mu32CurrentNumberCount];
		mStartTime             = OSAL_ClockGetElapsedTime();
		bSendTestMessage(u32SizeOfMsg[mu32CurrentSizeCount], CCA_C_U16_FCT_DATA);
		break;
	}
	case CCA_C_U16_FCT_END:
	{
		// Send power message to own application
		OSAL_tMQueueHandle oSelfQueueHandle = scd_OpenQueue(u16AppId);
		bSendCCAPowerMsg(u16AppId, oSelfQueueHandle, MSG_PRIO, AMT_C_U16_PWR_APP_END_SUCCESSFUL, 0, 0);
		scd_s32CloseQueue(oSelfQueueHandle);
		// Send event to main thread to signalize that the test has been finished
		OSAL_s32EventPost(hEvPerfTest, hEvMask, OSAL_EN_EVENTMASK_OR);
		bSendTestMessage(newMessage.getPayloadSize(), CCA_C_U16_FCT_ECHO);
		break;
	}
	case CCA_C_U16_FCT_DATA:
	{
		// The echo thread gets a data message that it has to echo
		bSendTestMessage(newMessage.getPayloadSize(), CCA_C_U16_FCT_ECHO);
		break;
	}
	case CCA_C_U16_FCT_ECHO:
	{
		// The measure thread received an echo message
		mu32MessagesToSend--;
		if (0 == mu32MessagesToSend) 
		{
			// All messages for the current sub test were sent, now print out results
			OSAL_tMSecond stopTime = OSAL_ClockGetElapsedTime();
			OEDT_HelperPrintf(TR_LEVEL_USER_1, "CCA Messung %u Messages with Size %u in %lu msec",
			                 (unsigned int) u32NrOfMsg[mu32CurrentNumberCount],
			                 (unsigned int) u32SizeOfMsg[mu32CurrentSizeCount],
			                 stopTime-mStartTime);
			mu32CurrentNumberCount++;
			if ( NO_OF_MSGCOUNT == mu32CurrentNumberCount)
			{
				// All message counts for this message size were tested, now test a new size
				mu32CurrentSizeCount++;
				// Reset message number index
				mu32CurrentNumberCount = 0;
				if (NO_OF_MSGSIZES == mu32CurrentSizeCount)
				{
					// Tests are complete now you can send the shutdown message
					// Send power message to own application
					OSAL_tMQueueHandle oSelfQueueHandle = scd_OpenQueue(u16AppId);
					bSendCCAPowerMsg(u16AppId, oSelfQueueHandle, MSG_PRIO, AMT_C_U16_PWR_APP_END_SUCCESSFUL, 0, 0);
					scd_s32CloseQueue(oSelfQueueHandle);
					// Send end message to partner
					bSendTestMessage(1, CCA_C_U16_FCT_END);
					newMessage.bDelete();
					return;
				}
			}
			mu32MessagesToSend = u32NrOfMsg[mu32CurrentNumberCount];
			mStartTime         = OSAL_ClockGetElapsedTime();
		}
		bSendTestMessage(u32SizeOfMsg[mu32CurrentSizeCount], CCA_C_U16_FCT_DATA);
		break;
	}
	default:
		OEDT_HelperPrintf(TR_LEVEL_FATAL,"Unkown CCA Message received");		
	}
	newMessage.bDelete();
}

tBool CCAMessageLoop::bSendTestMessage(tU32 size, tU16 type)
{
	CCATestmessage testMessage(mpcMessageBuffer, size);
	testMessage.vSetFunctionID(type);
	testMessage.vSetServiceID(mu16ServiceID);
	testMessage.vSetSourceAppID(u16AppId);
	testMessage.vSetTargetAppID(mu16PartnerAppID);
	testMessage.vSetRegisterID(0);
	return enPostMessage(&testMessage);
}
