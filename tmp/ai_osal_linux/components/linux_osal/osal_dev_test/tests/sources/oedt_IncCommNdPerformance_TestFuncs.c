/************************************************************************
  | FILE:         oedt_IncCommNdPerformance_TestFuncs.c
  | PROJECT:      platform
  | SW-COMPONENT: OEDT_FrmWrk
  |------------------------------------------------------------------------------
  | DESCRIPTION:  Regression tests for INC communication and performance test.
  |
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2013 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 14.08.14  | Initial version            | Shashikant Suguni 
  | 17.02.15  | Lint fix for CFG3-1032     | Shashikant Suguni
  |
  |*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_IncCommNdPerformance_TestFuncs.h"
#include "oedt_helper_funcs.h"

#include <signal.h>
#include <sys/signal.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include "linux/input.h"
#include "dgram_service.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"
#include "inc_perf_common.h"

#define PR_ERR(fmt, args...) \
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "inc_commNdperformance_test: %s: " fmt, __func__, ## args)

#define EXPCT_EQ(val1, val2, ret, fmt, args...) \
    if (val1 != val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define EXPCT_NE(val1, val2, ret, fmt, args...) \
    if (val1 == val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define BUF_SIZE                        1024 
#define POLL_TIMEOUT_MS                 200000 
#define TIME_OUT                        10   /*Sec*/
#define PACKET_SIZE                     10   /*bytes*/
#define DELAY_MS                        20
#define NODE_SCC                        "scc"
#define NODE_ADR3                       "adr3"

/*Communication thread argument stucture*/
struct thread_arg {
    int frequency;
    uint8_t host[12];
    sk_dgram *dgram;
    uint8_t buffer[BUF_SIZE];
};

/*Performance measurement thread argument stucture*/
struct perf_thread_arg {
    int bytes;
    sk_dgram *dgram;
    uint8_t host[12];
};

/* Global flag to signal the termination of threads*/
int global_flag;

/* ADR3 Ping Command*/
char cmd_Fkt_Get[9] = {0x00,0x91, 0x01, 0x0f,0x02, 0x02, 0x00,0x01,0x21};
/* ADR3 Ping Command Response*/
char rsp_Fkt_Get[12] = {0x00,0x91, 0x01, 0x0f,0x02, 0x02, 0x00, 0x0f,0x00,0x02,0x01,0x00};

/* Analyse thread callback function*/
void *analyse_thread(void *arg)
{

    int ret,errsv;
    unsigned long count = 0; 
    struct perf_thread_arg* thread_arg = (struct perf_thread_arg*)arg;

    int *res = malloc(sizeof(int));
    if (res == NULL) {
        errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        return NULL;
    }

    *res = 0;

    while (count <= TIME_OUT)
    {
        ret = throughput_handler(thread_arg->host,1,count);
        if (ret == -1)
        {
            PR_ERR("throughput_handler Failed:\n");
            *res = 120;
            break;
        }
        count++;
        sleep (1);
    }

    global_flag = 1; 

    /* Evaluate the throughput with threshold values*/
    *res = inc_threshold_evaluate(thread_arg->host);

    pthread_exit(res);
    return res;
}

/*Tx thread function*/
void *inc_performance_tx_thread(void *arg)
{

    struct perf_thread_arg *thread_arg = (struct perf_thread_arg*)arg;
    char buffer[BUF_SIZE];
    int bytes,i;

    int *res = malloc(sizeof(int));
    if (res == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        return NULL;
    }

    bytes = thread_arg->bytes;
    /* Dummy data to be sent to inc-scc (V850)*/ 
    for (i = 0; i< bytes; i++)
    {
        buffer[i] = 's';
    }
    buffer[i] = '\0';

    *res = 0;
    while (global_flag == 0)
    {

        /*If host is ADR3 send the ping command */
        if (strcmp (thread_arg->host,"adr3") == 0)
        {
            dgram_send(thread_arg->dgram,cmd_Fkt_Get,sizeof(cmd_Fkt_Get));
        }
        /*If host is SCC(v850) send the dummy data of configurable size */
        else
        {
            dgram_send(thread_arg->dgram,buffer,strlen(buffer));

        }
    }
    pthread_exit(res);
    return res;
}

/*Rx thread callback function*/
void *inc_performance_rx_thread(void *arg)
{
    uint8_t recvbuf[BUF_SIZE];
    struct perf_thread_arg *thread_arg = (struct perf_thread_arg*)arg;

    int *res = malloc(sizeof(int));
    if (res == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        return NULL;
    }

    *res = 0;

    while (global_flag == 0)
    {
        dgram_recv(thread_arg->dgram,recvbuf,BUF_SIZE);
    }

    pthread_exit(res);
    return res;
}

/*Tx thread callback function*/
void *inc_comm_tx_thread(void *arg)
{
    int count = 0,loop_count = 0; 
    int errsv,ret;
    struct thread_arg * thread_arg = (struct thread_arg *)arg;

    int *res = malloc(sizeof(int));
    if (res == NULL) {
        errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        return NULL;
    }
    *res = 0;

    loop_count = ((1000/thread_arg->frequency) * TIME_OUT);

    while (count <= loop_count)
    {
        /*If host is ADR3 send the ping command */
        if (strcmp (thread_arg->host,"adr3") == 0)
        {
            ret = dgram_send(thread_arg->dgram,cmd_Fkt_Get,sizeof(cmd_Fkt_Get));
            errsv = errno;
            if (ret == -1)
            {
                PR_ERR("Data sending Failed:\n");
                *res = 1;
                break;
            }
        }
        /*If host is SCC(v850) send the dummy data of configurable size */
        else
        {
            ret = dgram_send(thread_arg->dgram,thread_arg->buffer,strlen(thread_arg->buffer));
            errsv = errno;
            if (ret == -1)
            {
                PR_ERR("Data sending Failed:\n");
                *res = 2;
                break;
            }
        }
        count ++;
        /*milisec sleep */
        usleep (thread_arg->frequency * 1000);
    }
    pthread_exit(res);
    return res;
}

/*Rx thread function*/
void *inc_comm_rx_thread(void *arg)
{
    int fd, len,rc,errsv;
	unsigned int i; //Fix for Lint CFG3-1032
    int loop_count = 0,count = 0;
    struct thread_arg * thread_arg = (struct thread_arg *)arg;
    struct pollfd fds[1];
    uint8_t recvbuf[BUF_SIZE];

    int *res = malloc(sizeof(int));
    if (res == NULL) {
        errsv = errno; //Fix for Lint CFG3-1032
        PR_ERR("memory allocation failed: %d\n", errsv);
        return NULL;
    }
    *res = 0;
    fd = thread_arg->dgram->sk;
    fds[0].fd = fd;
    fds[0].events = POLLIN;


    loop_count = ((1000/thread_arg->frequency) * TIME_OUT);

    while (count <= loop_count)
    {

        rc = poll(fds, 1, POLL_TIMEOUT_MS);
        if (rc < 0)
        {
            errsv = errno;
            PR_ERR("poll failed: %d\n", errsv);
            *res = 5;
            break;
        }

        if(rc == 0)
        {
            printf("Poll time out \n");
            continue;
        }
        if((fds[0].revents & POLLIN) || (fds[0].revents & POLLPRI))
        {

            len = dgram_recv(thread_arg->dgram,recvbuf,BUF_SIZE);
            errsv = errno;
            if (len <= 0)
            {
                PR_ERR("Data Reception Failed:\n");
                *res = 1;
                break;
            }
            if (strcmp (thread_arg->host,"adr3") == 0)
            {    
                for (i = 0 ; i < sizeof(rsp_Fkt_Get); i++)
                {    
                    if (recvbuf[i] != rsp_Fkt_Get[i])
                    {
                        PR_ERR("INVALID CMD Data Reception:\n");
                        *res = 15;
                        break;
                    }
                }
            }
            else
            {
                if (strcmp(thread_arg->buffer,recvbuf) != 0)
                {
                    PR_ERR("INVALID Data Reception:\n");
                    *res = 15;
                    break;
                }
            }
            memset(recvbuf,0,sizeof (recvbuf));
        }
        count ++;
    }
    pthread_exit(res);
    return res;
}


/*Analyse Thread Init*/
int init_analyse_thread(pthread_t *ptid, struct perf_thread_arg *thread_arg)
{
    int err;

    err = pthread_create(ptid, NULL, analyse_thread, thread_arg);
    EXPCT_NE(err, -1, 8, "pthread_create failed\n");

    return 0;
}

/*Tx Thread Init*/
int init_inc_performance_tx_thread(pthread_t *ptid, struct perf_thread_arg *thread_arg)
{
    int err;

    err = pthread_create(ptid, NULL, inc_performance_tx_thread, thread_arg);
    EXPCT_NE(err, -1, 8, "pthread_create failed\n");

    return 0;
}

/*Rx Thread Init*/
int init_inc_performance_rx_thread(pthread_t *ptid, struct perf_thread_arg *thread_arg)
{
    int err;

    err = pthread_create(ptid, NULL, inc_performance_rx_thread, thread_arg);
    EXPCT_NE(err, -1, 8, "pthread_create failed\n");

    return 0;
}

/*Tx Thread Init*/
int init_inc_comm_tx_thread(pthread_t *ptid, struct thread_arg *thread_arg)
{
    int err;

    err = pthread_create(ptid, NULL, inc_comm_tx_thread, thread_arg);
    EXPCT_NE(err, -1, 8, "pthread_create failed\n");

    return 0;
}

/*Rx Thread Init*/
int init_inc_comm_rx_thread(pthread_t *ptid, struct thread_arg *thread_arg)
{
    int err;

    err = pthread_create(ptid, NULL, inc_comm_rx_thread, thread_arg);
    EXPCT_NE(err, -1, 8, "pthread_create failed\n");

    return 0;
}

/*Analyse Thread Exit*/
int exit_analyse_thread(pthread_t tid)
{
    int err;
    void *res;

    err = pthread_join(tid, &res);
    EXPCT_NE(err, -1, 9, "pthread_join failed\n");
    err = *(int *) res;
    EXPCT_EQ(err, 0, (10*err), "analyse_thread returned err %d\n", err);

    /* free memory allocated by thread */
    free(res);
    return 0;
}

/*Tx Thread Exit*/
int exit_inc_performance_tx_thread(pthread_t tid)
{
    int err;
    void *res;

    err = pthread_join(tid, &res);
    EXPCT_NE(err, -1, 9, "pthread_join failed\n");
    err = *(int *) res;
    EXPCT_EQ(err, 0, (10*err), "exit_inc_performance_tx_thread returned err %d\n", err);

    /* free memory allocated by thread */
    free(res);

    return 0;
}

/*Rx Thread Exit*/
int exit_inc_performance_rx_thread(pthread_t tid)
{
    int err;
    void *res;

    err = pthread_join(tid, &res);
    EXPCT_NE(err, -1, 9, "pthread_join failed\n");
    err = *(int *) res;
    EXPCT_EQ(err, 0, (10*err), "exit_inc_performance_rx_thread returned err %d\n", err);

    /* free memory allocated by thread */
    free(res);

    return 0;
}

/*Tx Thread Exit*/
int exit_inc_comm_tx_thread(pthread_t tid)
{
    int err;
    void *res;

    err = pthread_join(tid, &res);
    EXPCT_NE(err, -1, 9, "pthread_join failed\n");
    err = *(int *) res;
    EXPCT_EQ(err, 0, (10*err), "inc_comm_rx_thread returned err %d\n", err);

    /* free memory allocated by thread */
    free(res);

    return 0;
}

/*Rx Thread Exit*/
int exit_inc_comm_rx_thread(pthread_t tid)
{
    int err;
    void *res;

    err = pthread_join(tid, &res);
    EXPCT_NE(err, -1, 9, "pthread_join failed\n");
    err = *(int *) res;
    EXPCT_EQ(err, 0, (10*err), "inc_comm_rx_thread returned err %d\n", err);

    /* free memory allocated by thread */
    free(res);

    return 0;
}

int inc_run_performance_test(char *host,int bytes, int frequency)
{
    int err;
    pthread_t tx_tid,rx_tid,anlys_tid;
    struct perf_thread_arg *tx_thr_arg,*anlys_thr_arg,*rx_thr_arg;
    char host_buff[24];
    inc_comm inccom;

    /*Initialise the flag*/
    global_flag = 0; 

    memset (host_buff,0,sizeof(host_buff));
    strcpy(host_buff,"inc-");
    strcat(host_buff,host);

    frequency = 0;

    /*Init Inc Communication*/
    err = inc_communication_init(host,&inccom);
    if (err != 0)
        return 10;

    /* allocate memory for thread arguments*/
    tx_thr_arg = malloc(sizeof(struct perf_thread_arg));
    if (tx_thr_arg == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        inc_communication_exit(&inccom);
        return -1;
    }

    strcpy(tx_thr_arg->host, host); 
    tx_thr_arg->bytes = bytes;
    tx_thr_arg->dgram = inccom.dgram;

    /* Init and start the Tx  thread*/
    err = init_inc_performance_tx_thread(&tx_tid, tx_thr_arg);
    if (err != 0) {
        free(tx_thr_arg);
        inc_communication_exit(&inccom);
        return 10000 * err;
    }

    /* allocate memory for thread arguments*/
    rx_thr_arg = malloc(sizeof(struct perf_thread_arg));
    if (rx_thr_arg == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        inc_communication_exit(&inccom);
        return -1;
    }

    rx_thr_arg->dgram = inccom.dgram;

    /* Init and start the rx  thread*/
    err = init_inc_performance_rx_thread(&rx_tid, rx_thr_arg);
    if (err != 0) {
        free(tx_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return 20000 * err;
    }

    /* allocate memory for thread arguments*/
    anlys_thr_arg = malloc(sizeof(struct perf_thread_arg));
    if (anlys_thr_arg == NULL) {
        int errsv = errno;
        PR_ERR("anlys_thr_arg memory allocation failed: %d\n", errsv);
        free(tx_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return -1;
    }

    /*Fill the thread argument values*/
    strcpy(anlys_thr_arg->host,host_buff); 

    /* Init and start the performance analyser  thread*/
    err = init_analyse_thread(&anlys_tid, anlys_thr_arg);
    if (err != 0) {
        free(anlys_thr_arg);
        free(tx_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return 30000 * err;
    }

    /*Exit and cleanup routine for Tx thread*/
    err = exit_inc_performance_tx_thread(tx_tid);
    if (err != 0) {
        free(tx_thr_arg);
        free(anlys_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return 1000000 * err;
    }

    /*Exit and cleanup routine for Rx thread*/
    err = exit_inc_performance_rx_thread(rx_tid);
    if (err != 0) {
        free(tx_thr_arg);
        free(anlys_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return 2000000 * err;
    }

    /*Exit and cleanup routine for analyser thread*/
    err = exit_analyse_thread(anlys_tid);
    if (err != 0) {
        free(tx_thr_arg);
        free(anlys_thr_arg);
        free(rx_thr_arg);
        inc_communication_exit(&inccom);
        return 2000000 * err;
    }

    free(tx_thr_arg);
    free(rx_thr_arg);
    free(anlys_thr_arg);

    /*Clear flag*/
    global_flag = 0; 

    /*Exit Inc Communication*/
    inc_communication_exit(&inccom);

    return 0;
}


int inc_run_comm_test(char *host,int bytes, int frequency)
{
    int err,i;
    pthread_t tx_tid,rx_tid;
    struct thread_arg *tx_thr_arg,*rx_thr_arg;
    char buffer[BUF_SIZE];
    char host_buff[24];
    inc_comm inccom;

    memset (host_buff,0,sizeof(host_buff));
    strcpy(host_buff,"inc-");
    strcat(host_buff,host);

    /*Init Inc Communication*/
    err = inc_communication_init(host,&inccom);
    if (err != 0)
        return (10 * err);

    /* Dummy data to be sent to inc-scc (V850)*/ 
    for (i = 0; i< bytes; i++)
    {
        buffer[i] = 's';
    }
    buffer[i] = '\0';

    /* allocate memory for thread arguments*/
    tx_thr_arg = malloc(sizeof(struct thread_arg));
    if (tx_thr_arg == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        return -1;
    }

    /*Fill the thread argument values*/
    tx_thr_arg->frequency = frequency;
    strcpy(tx_thr_arg->host, host); 
    strcpy(tx_thr_arg->buffer, buffer); 
    tx_thr_arg->dgram = inccom.dgram;

    /* Init and start the Tx  thread*/
    err = init_inc_comm_tx_thread(&tx_tid, tx_thr_arg);
    if (err != 0) {
        free(tx_thr_arg);
        return 10000 * err;
    }

    /* allocate memory for thread arguments*/
    rx_thr_arg = malloc(sizeof(struct thread_arg));
    if (rx_thr_arg == NULL) {
        int errsv = errno;
        PR_ERR("memory allocation failed: %d\n", errsv);
        free(tx_thr_arg);
        return -1;
    }
    /*Fill the thread argument values*/
    rx_thr_arg->frequency = frequency;
    strcpy(rx_thr_arg->host, host); 
    strcpy(rx_thr_arg->buffer, buffer);
    rx_thr_arg->dgram = inccom.dgram;

    /* Init and start the rx  thread*/
    err = init_inc_comm_rx_thread(&rx_tid, rx_thr_arg);
    if (err != 0) {
        free(tx_thr_arg);
        free(rx_thr_arg);
        return 20000 * err;
    }

    /*Exit and cleanup routine for Tx thread*/
    err = exit_inc_comm_tx_thread(tx_tid);
    if (err != 0) {
        free(tx_thr_arg);
        free(rx_thr_arg);
        return 30000 * err;
    }

    /*Exit and cleanup routine for Rx thread*/
    err = exit_inc_comm_rx_thread(rx_tid);
    if (err != 0) {
        free(tx_thr_arg);
        free(rx_thr_arg);
        return 40000 * err;
    }
    /*Exit Inc Communication*/

    free(tx_thr_arg);
    free(rx_thr_arg);
    inc_communication_exit(&inccom);

    return 0;
}

/*INC SCC  Comminucation Test Routines*/
tU32 u32IncCommSccV1(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 1),(DELAY_MS * 1));
}

tU32 u32IncCommSccV2(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 1),(DELAY_MS * 2));
}

tU32 u32IncCommSccV3(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 1),(DELAY_MS * 3));
}
tU32 u32IncCommSccV4(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 1),(DELAY_MS * 4));
}

tU32 u32IncCommSccV5(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 1),(DELAY_MS * 5));
}

tU32 u32IncCommSccV6(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 2),(DELAY_MS * 1));
}

tU32 u32IncCommSccV7(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 2),(DELAY_MS * 2));
}

tU32 u32IncCommSccV8(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 2),(DELAY_MS * 3));
}
tU32 u32IncCommSccV9(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 2),(DELAY_MS * 4));
}

tU32 u32IncCommSccV10(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 2),(DELAY_MS * 5));
}

tU32 u32IncCommSccV11(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 3),(DELAY_MS * 1));
}

tU32 u32IncCommSccV12(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 3),(DELAY_MS * 2));
}

tU32 u32IncCommSccV13(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 3),(DELAY_MS * 3));
}
tU32 u32IncCommSccV14(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 3),(DELAY_MS * 4));
}

tU32 u32IncCommSccV15(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 3),(DELAY_MS * 5));
}

tU32 u32IncCommSccV16(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 4),(DELAY_MS * 1));
}

tU32 u32IncCommSccV17(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 4),(DELAY_MS * 2));
}

tU32 u32IncCommSccV18(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 4),(DELAY_MS * 3));
}
tU32 u32IncCommSccV19(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 4),(DELAY_MS * 4));
}

tU32 u32IncCommSccV20(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 4),(DELAY_MS * 5));
}

tU32 u32IncCommSccV21(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 5),(DELAY_MS * 1));
}

tU32 u32IncCommSccV22(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 5),(DELAY_MS * 2));
}

tU32 u32IncCommSccV23(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 5),(DELAY_MS * 3));
}
tU32 u32IncCommSccV24(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 5),(DELAY_MS * 4));
}

tU32 u32IncCommSccV25(tVoid)
{
    return inc_run_comm_test(NODE_SCC,(PACKET_SIZE * 5),(DELAY_MS * 5));
}


/*INC ADR3 Comminucation Test Routines*/
tU32 u32IncCommAdrV1(tVoid)
{
    return inc_run_comm_test(NODE_ADR3,1,(DELAY_MS * 1));
}
tU32 u32IncCommAdrV2(tVoid)
{
    return inc_run_comm_test(NODE_ADR3,1,(DELAY_MS * 2));
}
tU32 u32IncCommAdrV3(tVoid)
{
    return inc_run_comm_test(NODE_ADR3,1,(DELAY_MS * 3));
}
tU32 u32IncCommAdrV4(tVoid)
{
    return inc_run_comm_test(NODE_ADR3,1,(DELAY_MS * 4));
}
tU32 u32IncCommAdrV5(tVoid)
{
    return inc_run_comm_test(NODE_ADR3,1,(DELAY_MS * 5));
}

/*INC SCC performance Test Routine*/

tU32 u32IncPerformanceScc(tVoid)
{
    return inc_run_performance_test(NODE_SCC,(PACKET_SIZE * 6),(DELAY_MS * 1));
}
/*INC ADR3 performance Test routines*/
tU32 u32IncPerformanceAdr(tVoid)
{
    return inc_run_performance_test(NODE_ADR3,1,(DELAY_MS * 1));
}
