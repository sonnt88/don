/******************************************************************************
 * FILE         : oedt_SWC_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the individual test cases for the SWC
 *                device in LSIM side.
 * AUTHOR(s)    :  Manikandan Dhanasekaran (RBEI/ECF5)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date                | 	Initial revision       | Author & comments
 *-----------------------------------------------------------------------------
 * 24,January, 2013      |        1.0              | Manikandan Dhanasekaran (RBEI/ECF5)
 *                                                 | File is modified for enabling
                                                     the SWC test cases in LSIM side.
 *-----------------------------------------------------------------------------
 * 30,December, 2015     |        1.1              | Sowmya Rajendra (RBEI/ECF5)
 *                                                 | File is modified for lint fix.
 *******************************************************************************/
//#define vTtfisTrace(...)

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_SWC_TestFuncs.h"

#define OEDT_SWC_EV_MASK  0xC000FFFF

/* version string */
#define OEDT_SWC_C_S32_IO_VERSION   0x0100  //read v01.00
#define OEDT_SWC_EV_NAME "oedt_SwcEv1"



#define EVENT_WAIT_TIMEOUT_SWC  10000         //10 sec time out value

/* Events */
#define OEDT_SWC_EV_KEY1_PRESSED_EVENT   0x80000001
#define OEDT_SWC_EV_KEY2_PRESSED_EVENT   0x80000002
#define OEDT_SWC_EV_KEY3_PRESSED_EVENT   0x80000004
#define OEDT_SWC_EV_KEY4_PRESSED_EVENT   0x80000008
#define OEDT_SWC_EV_KEY5_PRESSED_EVENT   0x80000010
#define OEDT_SWC_EV_KEY6_PRESSED_EVENT   0x80000020
#define OEDT_SWC_EV_KEY7_PRESSED_EVENT   0x80000040
//#define OEDT_SWC_EV_KEY8_PRESSED_EVENT   0x80000080
//#define OEDT_SWC_EV_KEY9_PRESSED_EVENT   0x80000100
//#define OEDT_SWC_EV_KEY10_PRESSED_EVENT   0x80000200

#define OEDT_SWC_EV_KEY1_RELEASED_EVENT   0x40000001
#define OEDT_SWC_EV_KEY2_RELEASED_EVENT   0x40000002
#define OEDT_SWC_EV_KEY3_RELEASED_EVENT   0x40000004
#define OEDT_SWC_EV_KEY4_RELEASED_EVENT   0x40000008
#define OEDT_SWC_EV_KEY5_RELEASED_EVENT   0x40000010
#define OEDT_SWC_EV_KEY6_RELEASED_EVENT   0x40000020
#define OEDT_SWC_EV_KEY7_RELEASED_EVENT   0x40000040
//#define OEDT_SWC_EV_KEY8_RELEASED_EVENT   0x40000080
//#define OEDT_SWC_EV_KEY9_RELEASED_EVENT   0x40000100
//#define OEDT_SWC_EV_KEY10_RELEASED_EVENT   0x40000200


#define KEY1 0x0001
#define KEY2 0x0002
#define KEY3 0x0004
#define KEY4 0x0008
#define KEY5 0x0010
#define KEY6 0x0020
#define KEY7 0x0040
//#define KEY8 0x0080
//#define KEY9 0x0100
//#define KEY10 0x0200

#define KEY_PRESS_MASK 0x80000000
#define KEY_RELEASE_MASK 0x40000000


static tU32 u32oedt_swc_currentkeycode_value = 0;

void u32SteeringWheelCallbackFunc(OSAL_trSWCKeyCodeInfo *);

static OSAL_tEventHandle  oedt_swc_rEventHnd;

tU16 oedt_swc_KeyCode;

extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevOpen( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error Code - On Failure
 * DESCRIPTION:  This function is to test the Device Open Functionality
 ******************************************************************************/

tU32 u32SteeringWheelDevOpen(tVoid)
{
   OSAL_tIODescriptor hDevice;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;
   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDevOpen-Start\n");
   hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if (hDevice == OSAL_ERROR )
   {
      u32Ret +=1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Failed\n");

   }
   else if(OSAL_s32IOClose(hDevice) != OSAL_OK)
   {
      u32Ret += 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Passed\n");
   }
   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDevOpen-End\n");
   return 	u32Ret;
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDev_MultiOpen( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This function is to test the Device multi open functionality
 ******************************************************************************/
tU32 u32SteeringWheelDev_MultiOpen(tVoid)
{
   OSAL_tIODescriptor hDevice1 , hDevice2 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;

   hDevice1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if(hDevice1 == OSAL_ERROR )
   {
      u32Ret +=1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Failed\n");

   }
   else if((hDevice2 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess )) != OSAL_ERROR)
   {
      u32Ret += 2;
      if(OSAL_s32IOClose(hDevice2) != OSAL_OK)
      {
         u32Ret += 4;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
      }

      if(OSAL_s32IOClose(hDevice1) != OSAL_OK)
      {
         u32Ret += 8;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
      }

   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Passed\n");

      if(OSAL_s32IOClose(hDevice1) != OSAL_OK)
      {
         u32Ret += 16;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC MultiOpen-Passed\n");
      }
   }

   return 	u32Ret;
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevclose( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error Code - On Failure
 * DESCRIPTION:  This function is to test the device close functionality
 ******************************************************************************/

tU32 u32SteeringWheelDevclose(tVoid)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;

   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDevclose-Start\n");
   hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if (hDevice == OSAL_ERROR )
   {
      u32Ret +=1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Failed\n");

   }
   else if(OSAL_s32IOClose(hDevice) != OSAL_OK)
   {
      u32Ret += 4;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDevclose-Passed\n");
   }
   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDevclose-End\n");
   return 	u32Ret;
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDev_Multiclose( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This function is to test the device multi close functionality
 ******************************************************************************/
tU32 u32SteeringWheelDev_Multiclose(tVoid)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;

   hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if (hDevice == OSAL_ERROR )
   {
      u32Ret +=1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Failed\n");

   }
   else if(OSAL_s32IOClose(hDevice) != OSAL_OK)
   {
      u32Ret += 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Close-Failed\n");
   }
   else if(OSAL_s32IOClose(hDevice) == OSAL_OK)
   {
      u32Ret += 4;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDev_Multiclose-Failed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"u32SteeringWheelDev_Multiclose-Passed\n");
   }
   return u32Ret;
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetVersion( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error Code - On Failure
 * DESCRIPTION:  This functions is to test the Get Version IO Control
 * HISTORY:
 *-----------------------------------------------------------------------------
 * 30,December, 2015      |        1.1              | Sowmya Rajendra (RBEI/ECF5)
 *                                                 	| File is modified for lint fix.
 *******************************************************************************/
tU32 u32SteeringWheelDevIOControl_GetVersion(tVoid)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;
   tS32 s32Arg = 0;		//lint fix


   hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if (hDevice == OSAL_ERROR )
   {
      u32Ret +=1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC Open-Failed\n");

   }
   else if(OSAL_s32IOControl(hDevice,
		   OSAL_C_S32_IOCTRL_DEV_SWC_DEVICE_VERSION,
         (tS32)&s32Arg)==OSAL_ERROR)
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC IOControl Get Version-Failed\n");

      (tVoid)OSAL_s32IOClose(hDevice);
      u32Ret +=2;
   }
   //lint -e774
   else if(s32Arg!=OEDT_SWC_C_S32_IO_VERSION)		//suppression tag added
   {
      (tVoid)OSAL_s32IOClose(hDevice);
      u32Ret +=4;
   }
   else
   {
      if(OSAL_s32IOClose(hDevice) != OSAL_OK)
      {
         u32Ret += 8;
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC IOControl Get Version-Passed\n");
      }
   }
   return u32Ret;
}


/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_SetCallBack( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error Code - On Failure
 * DESCRIPTION:  This functions is to test the Set Call Back IO Control
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_SetCallBack(tVoid)
{

   OSAL_tIODescriptor hDevice ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;
   hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess );
   if (hDevice == OSAL_ERROR )
   {
      u32Ret +=1;
   }
   else if(OSAL_s32IOControl(hDevice,
         OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK,
         (tS32)u32SteeringWheelCallbackFunc)!=OSAL_ERROR)
   {
      if(OSAL_s32IOClose(hDevice) != OSAL_OK)
      {
         u32Ret +=8;
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SWC IOControl SetCallBack-Passed\n");

      }
   }
   else
   {
      u32Ret +=16;

      if(OSAL_s32IOClose(hDevice) != OSAL_OK)
      {
         u32Ret +=32;
      }
   }
   return u32Ret;
}


/*****************************************************************************
 * FUNCTION:      bkeyPressReleaseEventCheck( )
 * PARAMETER:    roedt_ResultMask - Event occured
 *               u16PressReleaseCnt - no of key press or release in
 *                third function argument (structure pointer oedt_trkeypressinfo *)
 *               rReferencePressInfo - Structure pointer contains key press or
 *                key release information
 * RETURNVALUE:  tBool - TRUE if occured event available in structure pointer
                         FALSE if occured event not available in structure pointer
 * DESCRIPTION:  This functions is used by u32SWCKeyPressKeyRelease_Test to check
 * whether occured event(first argument) available in structure pointer (third argument)
 * If event avalable then it will remove corresponding press or release info from
 * structure pointer
 ******************************************************************************/

tBool bkeyPressReleaseEventCheck(OSAL_tEventMask roedt_ResultMask,tU16 u16PressReleaseCnt,oedt_trkeypressinfo * rReferencePressInfo)
{
   tU16 u16KeyPressReleaseIndex=0;
   tBool bRet=FALSE;
   OSAL_tEventMask rExpectedEvent;
//   if(!u16PressReleaseCnt)
// {
//      bRet=TRUE;
//   }
//   else
   {
      while(u16KeyPressReleaseIndex < u16PressReleaseCnt )
      {
         rExpectedEvent=(rReferencePressInfo+u16KeyPressReleaseIndex)->u32KeyEvent;
         if(rExpectedEvent)
         {
            if((roedt_ResultMask & rExpectedEvent)==rExpectedEvent)
            {
               // indicate Success
               bRet=TRUE;
               //Remove those entry
               (rReferencePressInfo+u16KeyPressReleaseIndex)->u32KeyEvent=0;
               (rReferencePressInfo+u16KeyPressReleaseIndex)->u32Keycode=0;
               //Terminate loop
               u16KeyPressReleaseIndex=u16PressReleaseCnt;

            }
         }

         u16KeyPressReleaseIndex++;

      }
   }
   return bRet;
}

/*****************************************************************************
 * FUNCTION:      bCheckAllEventsReceived( )
 * PARAMETER:    u16PressReleaseCnt - no of key press or release in
 *                third function argument (structure pointer oedt_trkeypressinfo *)
 *               rReferencePressInfo - Structure pointer contains key press or
 *                key release information
 * RETURNVALUE:  tBool - TRUE if rReferencePressInfo contains all zero member
                         FALSE if rReferencePressInfo contains one non zero member
 * DESCRIPTION:  This functions is used by u32SWCKeyPressKeyRelease_Test to check
 * any non aero structure member available in structure pointer rReferencePressInfo
 ******************************************************************************/
tBool bCheckAllEventsReceived(tU16 u16PressReleaseCnt,const oedt_trkeypressinfo * rReferencePressInfo)
{
   tBool bRet=TRUE;
   tU16 u16KeyPressReleaseIndex=0;
   while(u16KeyPressReleaseIndex < u16PressReleaseCnt)
   {
      if((rReferencePressInfo++)->u32KeyEvent!=0 && (rReferencePressInfo++)->u32Keycode != 0)
      {
         bRet = FALSE;
      }
      u16KeyPressReleaseIndex++;
   }

   return bRet;
}

/*****************************************************************************
 * FUNCTION:      u32GetpressedKeyUsingIOCtrl( )
 * PARAMETER:    hDevice - handle of already opened SWC handle
 *               u32Keycode - Expected key code to check keycode received by
 *               IOControl OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
 * RETURNVALUE:  tU32 - 0 in case expected key press is matched with
 * IOControl OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
                         Non-Zero in case expected key press is not matched with
 * IOControl OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
 * DESCRIPTION:  This functions is used by u32SWCKeyPressKeyRelease_Test to check
 * expected key press received by IOControl OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
 * HISTORY:
 *-----------------------------------------------------------------------------
 * 30,December, 2015      |        1.1              | Sowmya Rajendra (RBEI/ECF5)
 *                                                 	| File is modified for lint fix.
 *******************************************************************************/
tU32 u32GetpressedKeyUsingIOCtrl(OSAL_tIODescriptor hDevice,tU32 u32Keycode)
{
   tU32 u32Ret=0;
   tS32 s32Arg = 0;
   if(OSAL_s32IOControl(hDevice,
         OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE,
         (tS32)&s32Arg)==OSAL_ERROR)
   {
      (tVoid)OSAL_s32IOClose(hDevice);
      (tVoid)OSAL_s32EventClose(oedt_swc_rEventHnd);
      (tVoid)OSAL_s32EventDelete(OEDT_SWC_EV_NAME);
      u32Ret += 512;
   }
   //lint -e774
   else if(!(((tU32)s32Arg) & u32Keycode))		//suppression tag added
   {
      u32Ret += 1024;
   }
   else
   {
      //Print received keycode via callback function matching with IOControl Get Key OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
   }
   return u32Ret;

}

/*****************************************************************************
 * FUNCTION:      u32SWCKeyPressKeyRelease_Test( )

 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Single or Multi key detection functionality
 ******************************************************************************/
tU32 u32SWCKeyPressKeyRelease_Test(troedt_keyseqinfo roedt_keyseqinfo )
{
   OSAL_tIODescriptor hDevice ;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = OEDT_DRV_SWC_TEST_PASSED;
   OSAL_tEventMask roedt_ResultMask;
   tBool bMemoryCreationPassed = TRUE;
   tBool bAllKeyPressed = FALSE;
   tBool bAllKeyReceived = FALSE;
   tBool bKeyPress= FALSE;
   tBool bKeyRelease= FALSE;
   oedt_trkeypressinfo * rkeypressInfo = OSAL_NULL;
   oedt_trkeyreleaseinfo *rkeyreleaseinfo = OSAL_NULL;

   tU16 u16KeyPressCnt=roedt_keyseqinfo.swc_u16noofkeypress;
   tU16 u16KeyReleaseCnt = roedt_keyseqinfo.swc_u16noofkeyrelease;
   if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
   {
      rkeypressInfo=(oedt_trkeypressinfo *)OSAL_pvMemoryAllocate(sizeof(oedt_trkeypressinfo)*u16KeyPressCnt);
      if(rkeypressInfo == OSAL_NULL)
      {
         bMemoryCreationPassed = FALSE;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Press Memory Creation failed\n");
      }
      else
      {
         (tVoid)OSAL_pvMemorySet(rkeypressInfo,0,(sizeof(oedt_trkeypressinfo)*u16KeyPressCnt));
         (tVoid)OSAL_pvMemoryCopy(rkeypressInfo,(roedt_keyseqinfo.rkeypressinfo),(sizeof(oedt_trkeypressinfo)*u16KeyPressCnt));
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Press Memory Creation Passed\n");
      }
   }
   if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
   {
      rkeyreleaseinfo=(oedt_trkeyreleaseinfo *)OSAL_pvMemoryAllocate(sizeof(oedt_trkeyreleaseinfo)*u16KeyReleaseCnt);
      if(rkeyreleaseinfo == OSAL_NULL)
      {
         bMemoryCreationPassed = FALSE;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Release Memory Creation failed\n");
      }
      else
      {
         (tVoid)OSAL_pvMemorySet(rkeyreleaseinfo,0,(sizeof(oedt_trkeyreleaseinfo)*u16KeyReleaseCnt));
         (tVoid)OSAL_pvMemoryCopy(rkeyreleaseinfo,(roedt_keyseqinfo.rkeyreleaseinfo),(sizeof(oedt_trkeyreleaseinfo)*u16KeyReleaseCnt));
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Release Memory Creation Passed\n");

      }
   }
   if(!bMemoryCreationPassed)
   {
      u32Ret += 0x10000;
      if((roedt_keyseqinfo.swc_u16noofkeypress != 0) && (rkeypressInfo != OSAL_NULL))
      {
         OSAL_vMemoryFree(rkeypressInfo);
      }
      if((roedt_keyseqinfo.swc_u16noofkeyrelease != 0) && (rkeyreleaseinfo != OSAL_NULL))
      {
         OSAL_vMemoryFree(rkeyreleaseinfo);
      }

   }

   else if((hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_SWC , enAccess ))==OSAL_ERROR)
   {
      u32Ret += 1;
      if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
      {

         OSAL_vMemoryFree(rkeypressInfo);
         rkeypressInfo=OSAL_NULL;
      }
      if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
      {

         OSAL_vMemoryFree(rkeyreleaseinfo);
         rkeyreleaseinfo=OSAL_NULL;
      }
   }

   else if(OSAL_OK != OSAL_s32EventCreate(OEDT_SWC_EV_NAME, &oedt_swc_rEventHnd))
   {
      (tVoid)OSAL_s32IOClose(hDevice);
      if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
      {

         OSAL_vMemoryFree(rkeypressInfo);
         rkeypressInfo=OSAL_NULL;
      }
      if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
      {

         OSAL_vMemoryFree(rkeyreleaseinfo);
         rkeyreleaseinfo=OSAL_NULL;
      }
      u32Ret += 2;
   }

   else if(OSAL_s32IOControl(hDevice,
         OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK,
         (tS32)u32SteeringWheelCallbackFunc) == OSAL_ERROR)
   {
      if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
      {

         OSAL_vMemoryFree(rkeypressInfo);
         rkeypressInfo=OSAL_NULL;
      }
      if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
      {

         OSAL_vMemoryFree(rkeyreleaseinfo);
         rkeyreleaseinfo=OSAL_NULL;
      }
      (tVoid)OSAL_s32EventClose(oedt_swc_rEventHnd);
      (tVoid)OSAL_s32EventDelete(OEDT_SWC_EV_NAME);
      (tVoid)OSAL_s32IOClose(hDevice);
      u32Ret += 8;
   }
   else
   {
      while(bKeyPress != TRUE || bKeyRelease != TRUE)
      {
         if(OSAL_OK != OSAL_s32EventWait(oedt_swc_rEventHnd, OEDT_SWC_EV_MASK,
               OSAL_EN_EVENTMASK_OR,
               EVENT_WAIT_TIMEOUT_SWC,
               &roedt_ResultMask))
         {
            if(OSAL_s32IOControl(hDevice,
                  OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK,
                  (tS32)OSAL_NULL) == OSAL_ERROR)
            {
               u32Ret += 16;
            }
            if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
            {

               OSAL_vMemoryFree(rkeypressInfo);
               rkeypressInfo=OSAL_NULL;
            }
            if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
            {

               OSAL_vMemoryFree(rkeyreleaseinfo);
               rkeyreleaseinfo=OSAL_NULL;
            }
            (tVoid)OSAL_s32IOClose(hDevice);
            (tVoid)OSAL_s32EventClose(oedt_swc_rEventHnd);
            (tVoid)OSAL_s32EventDelete(OEDT_SWC_EV_NAME);
            u32Ret += 32;
            //Terminate while loop
            bKeyPress = TRUE;
            bKeyRelease = TRUE;
         }
         else
         {
            //Clear the corresponding event
            if(OSAL_OK != OSAL_s32EventPost(oedt_swc_rEventHnd,
                  ~roedt_ResultMask,
                  OSAL_EN_EVENTMASK_AND))
            {
               if(OSAL_s32IOControl(hDevice,
                     OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK,
                     (tS32)OSAL_NULL) == OSAL_ERROR)
               {

                  u32Ret += 64;
               }
               if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
               {

                  OSAL_vMemoryFree(rkeypressInfo);
                  rkeypressInfo=OSAL_NULL;
               }
               if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
               {

                  OSAL_vMemoryFree(rkeyreleaseinfo);
                  rkeyreleaseinfo=OSAL_NULL;
               }
               (tVoid)OSAL_s32IOClose(hDevice);
               (tVoid)OSAL_s32EventClose(oedt_swc_rEventHnd);
               (tVoid)OSAL_s32EventDelete(OEDT_SWC_EV_NAME);
               u32Ret += 128;
               //Terminate while loop
               bKeyPress = TRUE;
               bKeyRelease = TRUE;

            }

            //Check Event occured is available in current OEDT test key press array
            if(FALSE == bkeyPressReleaseEventCheck(roedt_ResultMask,roedt_keyseqinfo.swc_u16noofkeypress,rkeypressInfo))
            {
               //No Entry Found, Occured event is because of release, so dont perform //any check related to key press
            }
            //Check testing keycode via IOControl command
            else if(roedt_keyseqinfo.bIoCtrlTest)
            {
               //Application requested to check via IOControl command also
               //One key found Decrease counter by 1
               u16KeyPressCnt--;
               u32Ret += u32GetpressedKeyUsingIOCtrl(hDevice,u32oedt_swc_currentkeycode_value);
            }
            else if(roedt_keyseqinfo.swc_u16noofkeypress)
            {
               //Application not requested to check via IOControl command
               //One key found Decrease counter by 1
               u16KeyPressCnt--;

            }

            if(TRUE == bCheckAllEventsReceived(roedt_keyseqinfo.swc_u16noofkeypress,rkeypressInfo))
            {
               bAllKeyPressed=TRUE;
            }
            else
            {
               bAllKeyPressed=FALSE;
            }

            if(u32Ret==0 && bAllKeyPressed == TRUE && u16KeyPressCnt ==0)
            {
               bKeyPress = TRUE;
            }

            //Check Event occured is available in current OEDT test key Release array
            if(FALSE == bkeyPressReleaseEventCheck(roedt_ResultMask,roedt_keyseqinfo.swc_u16noofkeyrelease,rkeyreleaseinfo))
            {
               //No Entry Found, next iteration will check for remaining expected event for press/release
            }
            else if(roedt_keyseqinfo.bIoCtrlTest)
            {
               //Application requested to check via IOControl command also
               //One key found Decrease counter by 1
               u16KeyReleaseCnt--;
               //User released key so we should get 0 as keycode
               u32Ret += u32GetpressedKeyUsingIOCtrl(hDevice,0);
            }
            else if(roedt_keyseqinfo.swc_u16noofkeyrelease)
            {
               //Application not requested to check via IOControl command
               //One key found Decrease counter by 1
               u16KeyReleaseCnt--;
            }
            if(TRUE == bCheckAllEventsReceived(roedt_keyseqinfo.swc_u16noofkeyrelease,rkeyreleaseinfo))
            {
               bAllKeyReceived=TRUE;
            }

            else
            {
               bAllKeyReceived=FALSE;
            }
            if(u32Ret==0 && bAllKeyReceived == TRUE && u16KeyReleaseCnt ==0)
            {
               bKeyRelease = TRUE;
            }
         }
      }/* End of While*/
      if(u32Ret==0)
      {

         if(OSAL_s32IOControl(hDevice,
               OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK,
               (tS32)OSAL_NULL) == OSAL_ERROR)
         {

            u32Ret += 256;
         }
         if(roedt_keyseqinfo.swc_u16noofkeypress != 0)
         {

            OSAL_vMemoryFree(rkeypressInfo);
            rkeypressInfo=OSAL_NULL;
         }
         if(roedt_keyseqinfo.swc_u16noofkeyrelease != 0)
         {

            OSAL_vMemoryFree(rkeyreleaseinfo);
            rkeyreleaseinfo=OSAL_NULL;
         }
         (tVoid)OSAL_s32EventClose(oedt_swc_rEventHnd);
         (tVoid)OSAL_s32EventDelete(OEDT_SWC_EV_NAME);
         (tVoid)OSAL_s32IOClose(hDevice);

      }

   }
   return u32Ret;

}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 1 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey1presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey1presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelDevIOControl_GetKey1( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 1
 ******************************************************************************/
tU32 u32SteeringWheelDevIOControl_GetKey1(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey1presswithIOctrl =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey1presswithIOctrl);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY1Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 1 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY1Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY1,OEDT_SWC_EV_KEY1_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey1Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey1Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY1PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 1 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY1PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY1,OEDT_SWC_EV_KEY1_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey1pressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey1pressReleasetest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY2_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 2 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY2Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey2presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey2presstest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey2( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 2
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey2(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey2PresswithIOCotrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey2PresswithIOCotrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWhee2KEY1Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 2 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY2Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey2Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey2Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY2PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 2 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY2PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey2PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey2PressReleasetest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY3_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 3 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey3Presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey3Presstest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey3( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 3
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey3(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey3PresswithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey3PresswithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY3Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 3 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey3Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey3Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY3PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 3 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey3PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey3PressReleasetest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY4_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 4 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey4presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey4presstest);

}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey4( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 4
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey4(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey4PresswihIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey4PresswihIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 4 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey4Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey4Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY3PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 4 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey4PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey4PressReleasetest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY5_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 5 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY5Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY5,OEDT_SWC_EV_KEY5_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey5Presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey5Presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey5( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 5
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey5(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY5,OEDT_SWC_EV_KEY5_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey5PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey5PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY5Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 5 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY5Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY5,OEDT_SWC_EV_KEY5_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey5Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey5Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY5PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 5 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY5PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY5,OEDT_SWC_EV_KEY5_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY5,OEDT_SWC_EV_KEY5_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey5PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey5PressReleasetest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY6_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 6 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY6Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY6,OEDT_SWC_EV_KEY6_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey6presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey6presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey6( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 6
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey6(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY6,OEDT_SWC_EV_KEY6_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey6PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey6PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY6Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 6 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY6Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY6,OEDT_SWC_EV_KEY6_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey6Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey6Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY6PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 6 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY6PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY6,OEDT_SWC_EV_KEY6_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY6,OEDT_SWC_EV_KEY6_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey6PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey6PressReleasetest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY7_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 7 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY7Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY7,OEDT_SWC_EV_KEY7_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey7presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey7presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey7( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 7
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey7(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY7,OEDT_SWC_EV_KEY7_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey7PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey7PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY7Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 7 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY7Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY7,OEDT_SWC_EV_KEY7_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey7Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey7Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY7PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 7 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY7PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY7,OEDT_SWC_EV_KEY7_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY7,OEDT_SWC_EV_KEY7_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey7PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey7PressReleasetest);
}
//The following functions are commented, as the LSIM SWC image contains only 7 Keys at present. These can be used in future.
#if 0
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY8_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 8 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY8Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY8,OEDT_SWC_EV_KEY8_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey8presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey8presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelDevIOControl_GetKey8( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 8
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey8(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY8,OEDT_SWC_EV_KEY8_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey8PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };
   return u32SWCKeyPressKeyRelease_Test(rKey8PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY8Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 8 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY8Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY8,OEDT_SWC_EV_KEY8_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey8Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey8Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY8PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 8 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY8PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY8,OEDT_SWC_EV_KEY8_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY8,OEDT_SWC_EV_KEY8_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey8PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey8PressReleasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY9_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 9 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY9Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY9,OEDT_SWC_EV_KEY9_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey9presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey9presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelDevIOControl_GetKey9( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 9
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey9(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY9,OEDT_SWC_EV_KEY9_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey9PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };
   return u32SWCKeyPressKeyRelease_Test(rKey9PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY9Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 9 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY9Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY9,OEDT_SWC_EV_KEY9_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey9Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey9Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY9PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 9 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY9PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY9,OEDT_SWC_EV_KEY9_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY9,OEDT_SWC_EV_KEY9_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey9PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey9PressReleasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY10_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 10 press functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY10Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY10,OEDT_SWC_EV_KEY10_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey10presstest =
   {
         1,0,roedt_singlekeypress,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey10presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelDevIOControl_GetKey10( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE
               IO control after pressing the Key 10
 ******************************************************************************/

tU32 u32SteeringWheelDevIOControl_GetKey10(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY10,OEDT_SWC_EV_KEY10_PRESSED_EVENT}
   };
   troedt_keyseqinfo rKey10PressWithIOCtrltest =
   {
         1,0,roedt_singlekeypress,NULL,TRUE
   };
   return u32SWCKeyPressKeyRelease_Test(rKey10PressWithIOCtrltest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY10Release_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 10 release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY10Release_Test(tVoid)
{
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY10,OEDT_SWC_EV_KEY10_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey10Releasetest =
   {
         0,1,OSAL_NULL,roedt_singlekeyrelease,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey10Releasetest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY10PressRelease_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Key 10 press and release functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY10PressRelease_Test(tVoid)
{
   oedt_trkeypressinfo roedt_singlekeypress[]=
   {
         {KEY10,OEDT_SWC_EV_KEY10_PRESSED_EVENT}
   };
   oedt_trkeyreleaseinfo roedt_singlekeyrelease[]=
   {
         {KEY10,OEDT_SWC_EV_KEY10_RELEASED_EVENT}
   };
   troedt_keyseqinfo rKey10PressReleasetest =
   {
         1,1,roedt_singlekeypress,roedt_singlekeyrelease,FALSE
   };


   return u32SWCKeyPressKeyRelease_Test(rKey10PressReleasetest);
}
#endif
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY2KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(2&1) detection functionality.
 ******************************************************************************/

tU32 u32SteeringWheelKEY2KEY1Press_Test(tVoid)

{
   oedt_trkeypressinfo roedt_key_2_1_press[]=
   {
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_2_1_presstest =
   {
         2,0,roedt_key_2_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_2_1_presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY3KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(3&1) detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_3_1_press[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_3_1_presstest =
   {
         2,0,roedt_key_3_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_3_1_presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(4&1) detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_1_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_1_presstest =
   {
         2,0,roedt_key_4_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_1_presstest);
}
/*****************************************************************************
 * FUNCTION:		u32SteeringWheelKEY3KEY2_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(3&2) detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3KEY2Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_3_2_press[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_3_2_presstest =
   {
         2,0,roedt_key_3_2_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_3_2_presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY2_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(4&2) detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY2Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_2_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_2_presstest =
   {
         2,0,roedt_key_4_2_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_2_presstest);
}

/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY3_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY3Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_3_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_3_presstest =
   {
         2,0,roedt_key_4_3_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_3_presstest);
}


/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY3KEY2KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key(3,2&1) detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY3KEY2KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_3_2_1_press[]=
   {
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_3_2_1_presstest =
   {
         3,0,roedt_key_3_2_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_3_2_1_presstest);
}
/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY2KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY2KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_2_1_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_2_1_presstest =
   {
         3,0,roedt_key_4_2_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_2_1_presstest);
}
/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY3KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY3KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_3_1_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_3_1_presstest =
   {
         3,0,roedt_key_4_3_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_3_1_presstest);
}
/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY3KEY2_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY3KEY2Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_3_2_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT}

   };
   troedt_keyseqinfo rKey_4_3_2_presstest =
   {
         3,0,roedt_key_4_3_2_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_3_2_presstest);
}
/*****************************************************************************
 * FUNCTION:      u32SteeringWheelKEY4KEY3KEY2KEY1_Test( )
 * PARAMETER:    none
 * RETURNVALUE:  OEDT_DRV_SWC_TEST_PASSED - On Success
               Error code - On Failure
 * DESCRIPTION:  This functions is to test the Multi key detection functionality
 ******************************************************************************/

tU32 u32SteeringWheelKEY4KEY3KEY2KEY1Press_Test(tVoid)
{
   oedt_trkeypressinfo roedt_key_4_3_2_1_press[]=
   {
         {KEY4,OEDT_SWC_EV_KEY4_PRESSED_EVENT},
         {KEY3,OEDT_SWC_EV_KEY3_PRESSED_EVENT},
         {KEY2,OEDT_SWC_EV_KEY2_PRESSED_EVENT},
         {KEY1,OEDT_SWC_EV_KEY1_PRESSED_EVENT}


   };
   troedt_keyseqinfo rKey_4_3_2_1_presstest =
   {
         4,0,roedt_key_4_3_2_1_press,NULL,FALSE
   };

   return u32SWCKeyPressKeyRelease_Test(rKey_4_3_2_1_presstest);
}

/*****************************************************************************
 * FUNCTION:		u32SteeringWheelCallbackFunc( )
 * PARAMETER:    u16Keycode - Key Value
 * RETURNVALUE:  None
 * DESCRIPTION:   This functions can be called by steering wheel driver, whenever
               any valid key is pressed or released. After that  corresponding
               key value can be sent to the respective OEDT
 ******************************************************************************/

void u32SteeringWheelCallbackFunc(OSAL_trSWCKeyCodeInfo *rSWCKeycodeInfo)
{
   u32oedt_swc_currentkeycode_value= rSWCKeycodeInfo->u32KeyCode;
   switch(rSWCKeycodeInfo->enKeyState)
   {
   case OSAL_EN_SWC_KEY_PRESSED:
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"SWC Data:%d is pressed",rSWCKeycodeInfo->u32KeyCode);
      (tVoid)OSAL_s32EventPost(oedt_swc_rEventHnd,(rSWCKeycodeInfo->u32KeyCode | KEY_PRESS_MASK) ,
            OSAL_EN_EVENTMASK_OR);
      break;
   case OSAL_EN_SWC_KEY_RELEASED:
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"SWC Data:%d is released",rSWCKeycodeInfo->u32KeyCode);
      (tVoid)OSAL_s32EventPost(oedt_swc_rEventHnd,((rSWCKeycodeInfo->u32KeyCode)| KEY_RELEASE_MASK),
            OSAL_EN_EVENTMASK_OR);
      break;

   default:
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"SWC Unknown callback State");
      break;
   }
}

