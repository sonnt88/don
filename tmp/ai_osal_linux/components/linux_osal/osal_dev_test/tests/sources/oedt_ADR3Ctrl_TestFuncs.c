/******************************************************************************
 *FILE         : oedt_Adr3Ctrl_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : 
 *               
 *AUTHOR       : Madhu Sudhan Swargam (RBEI/ECF5)
 *
 *COPYRIGHT    : (c) 2012, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : 04.09.13 .Initial version
 *             : 24.12.13 .Response wait Time for events or messages is made for four seconds
                           swm2kor
               : 13.01.14 .Oedt Addition
                           SWM2KOR
               : 03.03.14 .Using ADR3CTRL state definition from dev_adr3ctrl.h file
                           SWM2KOR
               : 14.03.14 .Remove Lint warnings and update trace
                           SWM2KOR
 *
 *****************************************************************************/
 
 /*****************************************************************
 * Helper Defines and Macros (scope: module-local)
 *******************************************************************/


/*****************************************************************
* variable definition (scope: global)
******************************************************************/



/*****************************************************************
* function implementation (scope: global)
*****************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "oedt_Types.h"
#include "oedt_ADR3Ctrl_TestFuncs.h"
#include "dev_adr3ctrl.h"

/* CallBack Check Resources Used */
#define CALLBACKREGISTRY_EVENTNAME                       "Adr3CallBackRgstry_EV"
/* Reset Check Resources Used */
#define ADR3RESETCHECK_EVENTNAME                         "Adr3ResetCheck_EV"
/* Reset Check Resources Used */
#define ADR3SPIMODCHECK_EVENTNAME                        "Adr3SPIModCheck_EV"
/* Reset Check Resources Used */
#define ADR3NORMODCHECK_EVENTNAME                        "Adr3NORModCheck_EV"

#define ADR3_MP_RESETCHECK_EVENTNAME                     "Adr3_MP_ResetCheck_EV"
/* Used for Event post from CallBack*/
tBool bEventPostMechansmActive = FALSE; 
/* Handler for CallBack event mechanism Handling */
OSAL_tEventHandle CallBackEventHndlr = OSAL_C_INVALID_HANDLE;

extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level,tPCChar pchFormat,...);

/*Function Definitions*/
/***********************************************************************************
* FUNCTION    :  u32EventClose_Delete
*
* PARAMETER   :  
*                  tCString EventName
*                  OSAL_tEventHandle EventHndlr
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32, "OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  Close the event and delete the event
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*
*************************************************************************************/
static tS32 u32EventClose_Delete(tCString EventName,
                                 OSAL_tEventHandle *EventHndlr,
                                 tU32 u32LineNumbr
                                )
{
   tS32 s32Retval = OSAL_OK;
   if((EventName != NULL) && (*EventHndlr != OSAL_C_INVALID_HANDLE))
   {
      if(OSAL_OK != OSAL_s32EventClose(*EventHndlr))
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Event close failed err %lu at line =%d",
                           OSAL_u32ErrorCode(), 
                           u32LineNumbr 
                          );
         s32Retval = OSAL_ERROR;
      }
      else if(OSAL_OK != OSAL_s32EventDelete(EventName ))
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Event Delete failed err %lu at line =%d",
                           OSAL_u32ErrorCode(),
                           u32LineNumbr 
                          );
         *EventHndlr = OSAL_C_INVALID_HANDLE;
         s32Retval = OSAL_ERROR;
      }
      else
      {
         /** Event closed and Deleted  sucessfully*/
         *EventHndlr = OSAL_C_INVALID_HANDLE;
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                        "Invalid Event delete values at Line =%d", 
                        u32LineNumbr 
                       );
      s32Retval = OSAL_ERROR;
   }
   return (s32Retval);
}
/***************************************************************************************
* FUNCTION    :  u32ADR3StateWait
*
* PARAMETER   :  
*                  OSAL_tEventHandle EventHndlr
*                  tU32 dwEventMask
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32, valid event  on success  or Invalid event value in case of error
*
* DESCRIPTION :  Wait for the EventHndlr and send the event received 
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
static tU32 u32ADR3StateWait(OSAL_tEventHandle EventHndlr,tU32 dwEventMask,OSAL_tenEventMaskFlag fgMaskoption,tU32 u32LineNumbr)
{
   tU32 hEvRequest ;
   if(((OSAL_tEventHandle)OSAL_C_INVALID_HANDLE != EventHndlr) && (0 != dwEventMask))
   {
      if(OSAL_OK == OSAL_s32EventWait(EventHndlr,
                                       dwEventMask, 
                                       fgMaskoption, 
                                       ADR3CTRL_RESPONSE_WAIT_TIME, 
                                       &hEvRequest
                                      )
         )
      {
         /* Clear the Event */
         if(OSAL_OK != OSAL_s32EventPost(EventHndlr, 
                                           ~hEvRequest, 
                                           OSAL_EN_EVENTMASK_AND 
                                          ) 
            )
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                              "Event post Failed line %lu with error =%u",
                              u32LineNumbr , 
                              OSAL_u32ErrorCode() 
                             );
            hEvRequest = 134217735;
         } 
         
      }
      else
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Event wait Failed line %lu with error =%u",
                           u32LineNumbr , 
                           OSAL_u32ErrorCode() 
                          ); 
         hEvRequest = 67108877;
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                        "Invalid Event wait values at Line =%d", 
                        u32LineNumbr 
                       );
      hEvRequest = 33554449;
   }
   
   return (hEvRequest);
}
/***************************************************************************************
* FUNCTION    :  u32ADR3CtrlIOCntrlWait
*
* PARAMETER   :  
*                  OSAL_tIODescriptor hDevHandle
                   tU32 u32IOCONTROL
*                  tU32 u32ADR3State
* RETURNVALUE :    tVoid
*
* DESCRIPTION :  CallBack execution for ADR3 state change
*                Post the event for the received State from Dev_ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 13,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
tU32 u32ADR3CtrlIOCntrlWait(OSAL_tIODescriptor hDevHandle,tU32 u32IOCONTROL,tU32 u32ADR3State)
{  
   tU32   u32RetVal = 0;
   tU32 hEvRequest;
   tU32 dwEventMask ;
   if(OSAL_OK 
      == 
      OSAL_s32IOControl(hDevHandle,
                        (tS32)u32IOCONTROL,
                        (tS32)NULL 
                        )
     )
   {
      if(ADR3CTRL_NORMALMODE == u32ADR3State)
      {            
         dwEventMask = ADR3CALLBACK_ALIVESTATE | ADR3CALLBACK_DEADSTATE;
      }
      else
      {
         dwEventMask = ADR3CALLBACK_DNLSTATE | ADR3CALLBACK_DEADSTATE;
      }

      /* Wait for callback from ADR3*/
      hEvRequest = u32ADR3StateWait(CallBackEventHndlr,
                                    dwEventMask,
                                    OSAL_EN_EVENTMASK_AND,
                                    __LINE__
                                    );
      if(hEvRequest >= ADR3CTRL_STATEWAIT_MIN_ERROR)
      {
         /* Adding the wait error value with return Value*/
         u32RetVal += hEvRequest;
      }
      else if(dwEventMask & hEvRequest) 
      {
         // Expected behaviour
         // Wait for 2 seconds as the ADR3 takes 1.5 seconds to come up after reset
         OSAL_s32ThreadWait(2000);
      }
      else
      {  
         u32RetVal += 16777223;
      }
   }  
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                        "IOControl failed with error %d",
                        OSAL_u32ErrorCode()
                        );
      u32RetVal += 8388621;
   }
   return u32RetVal;
}
/***************************************************************************************
* FUNCTION    :  vAdr3ctrlCallback
*
* PARAMETER   :  
*                  tU32 u32State
*
* RETURNVALUE :    tVoid
*
* DESCRIPTION :  CallBack execution for ADR3 state change
*                Post the event for the received State from Dev_ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
static tVoid vAdr3ctrlCallback(tU32 u32State)
{
   tU32  tU32EventCommand = ADR3CALLBACK_INVALIDSTATE;
   if(u32State == (tU32)ADR3CTRL_ADR3_STATE_ALIVE)
   {
      /* event post ADR3 ALIVE state */
      tU32EventCommand = ADR3CALLBACK_ALIVESTATE;
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ADR3CTRL_ADR3_STATE_ALIVE");  
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DEAD)
   {
      /* event post ADR3 DEAD state */
      tU32EventCommand = ADR3CALLBACK_DEADSTATE;
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ADR3CTRL_ADR3_STATE_DEAD");
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DNL)
   {
      /* event post ADR3 DNL state */
      tU32EventCommand = ADR3CALLBACK_DNLSTATE ;
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ADR3CTRL_ADR3_STATE_DNL");
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_INIT)
   {
      /* event post ADR3 INIT state */
      tU32EventCommand = ADR3CALLBACK_INITSTATE ;
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ADR3CTRL_ADR3_STATE_INIT");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, 
                        "Unknow ADR3 State =%d",
                        u32State 
                       );
   }
   if(bEventPostMechansmActive == TRUE)
   {
      if(OSAL_C_INVALID_HANDLE == CallBackEventHndlr)  
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Invalid event Handler =%d",
                           u32State 
                          );
      }
      else if(OSAL_OK != OSAL_s32EventPost(CallBackEventHndlr, 
                                           tU32EventCommand, 
                                           OSAL_EN_EVENTMASK_OR 
                                          ) 
              )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS ,
                           "Event post Failed line %lu with error =%u",
                           __LINE__ , 
                           OSAL_u32ErrorCode() 
                         );
      }     
   }
}
/***********************************************************************************
* FUNCTION    :  u32ADR3Ctrlopen_RgstrtCallBack
*
* PARAMETER   :  
*                  OSAL_tIODescriptor hDevHandle
* RETURNVALUE :    tU32,"OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :   Open ADR3CTRL and Register for CallBack
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*************************************************************************************/
tU32 u32ADR3Ctrlopen_RgstrtCallBack(OSAL_tIODescriptor *hpDevHandle)
{
   tU32  u32RetVal = 0;
   /** ADR3CTRL Driver Open*/
   *hpDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if((*hpDevHandle) != OSAL_ERROR)
   {
      if(OSAL_OK 
         != 
         OSAL_s32IOControl(*hpDevHandle,
                           OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK,
                           (tS32)vAdr3ctrlCallback 
                          )
        )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS ,
                           "ADR3CTRL IOCOntrol failed with error %d",
                           OSAL_u32ErrorCode() 
                          );
         u32RetVal += 4096;
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS ,
                        "ADR3CTRL open failed with error %d",
                        OSAL_u32ErrorCode() 
                       );

      u32RetVal += 8192;
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlBasiOpenClose

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   : TU_OEDT_DEV_ADR3CTRL_TEST_001

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32  u32OedtADR3CtrlBasiOpenClose(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;   
    /*Opens ADR3 CTRL*/
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if(hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Device open failed with error %d",
                        OSAL_u32ErrorCode()
                       );
      u32RetVal = 1;
   }
   else
   {/*Open Successful*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal = 2;
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlReadonlyOpen

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   : TU_OEDT_DEV_ADR3CTRL_TEST_002

* DESCRIPTION :  Open the ADR3CTRL in READONLY permissions.
                 Open should fail with OSAL_E_NOACCESS error code
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/

tU32  u32OedtADR3CtrlReadonlyOpen(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;
   tS32 s32OSAL_Ret;
    /*Opens ADR3 CTRL*/
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READONLY);
   if (hDevHandle == OSAL_ERROR)
   {
     /* check the error status returned */
      s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
      switch(s32OSAL_Ret)
      {
         case OSAL_E_NOACCESS:  /* Expected error */		 
            u32RetVal = 0;
            break;
         case OSAL_E_UNKNOWN:
            u32RetVal += 1;
            break;
         case OSAL_E_MAXFILES:
            u32RetVal += 2;
            break;
         default:
            u32RetVal += 4;
            break;
      }//end of switch( s32OSAL_Ret )
   }
   else
   {/*Open Successful*/
      if (OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                         );
         u32RetVal = 8;
      }
      else
      {
         u32RetVal = 16;
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlWriteonlyOpen

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_003

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/

tU32  u32OedtADR3CtrlWriteonlyOpen(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;
   tS32 s32OSAL_Ret;
    /*Opens ADR3 CTRL*/
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_WRITEONLY);
   if(hDevHandle == OSAL_ERROR)
   {
     /* check the error status returned */
	    s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
	    switch(s32OSAL_Ret)
	    {
         case OSAL_E_NOACCESS:  /* Expected error */		 
            u32RetVal = 0;
            break;
         case OSAL_E_UNKNOWN:
            u32RetVal += 1;
            break;
         case OSAL_E_MAXFILES:
            u32RetVal += 2;
            break;
         default:
            u32RetVal += 4;
            break;
		}//end of switch( s32OSAL_Ret )
   }
   else
   {/*Open Successful*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal = 8;
      }
      else
      {
         u32RetVal = 16;
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlMaxOpenCheckOpen

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_004

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/

tU32  u32OedtADR3CtrlMaxOpenCheckOpen(tVoid)
{
   OSAL_tIODescriptor hDevHandle[11];
   tU32 u32RetVal = 0,u32count,u32OpenCount;
   tS32 s32OSAL_Ret;
   /*ADR3Ctrl support ADR3CTRL_MAXDEVICES opens only*/
   for(u32count=0;u32count< ADR3CTRL_MAXDEVICES;u32count++)
   {
      /*Opens ADR3 CTRL*/
      hDevHandle[u32count] = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,
                                         OSAL_EN_READWRITE
                                        );
      if (hDevHandle[u32count]  == OSAL_ERROR)
      {
         /* check the error status returned */
         s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
         switch(s32OSAL_Ret)
         {
            case OSAL_E_NOACCESS:  /* Expected error */		 
               u32RetVal += 1;
               break;
            case OSAL_E_UNKNOWN:
               u32RetVal += 2;
               break;
            case OSAL_E_MAXFILES:
               u32RetVal += 4;
               break;
            default:
               u32RetVal += 8;
               break;
         }//end of switch( s32OSAL_Ret )
         break;
      }
   }
   /* 11th open should fail (Count 9 equals 10 opens)*/
   if(10 == u32count)
   {
      hDevHandle[10] = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,
                                   OSAL_EN_READWRITE
                                  );
      if (hDevHandle[10]  == OSAL_ERROR)
      {
         /* check the error status returned */
         s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
         switch(s32OSAL_Ret)
         {
            case OSAL_E_NOACCESS:  		 
               u32RetVal += 16;
               break;
            case OSAL_E_UNKNOWN:
               u32RetVal += 32;
               break;
            case OSAL_E_MAXFILES:/* Expected error */
               u32RetVal += 0;
               break;
            default:
               u32RetVal += 128;
               break;
         }//end of switch( s32OSAL_Ret )
      }
      else
      {  
         if (OSAL_s32IOClose(hDevHandle[u32count]) == OSAL_ERROR)
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                              "Device Close failed with error %d",
                              OSAL_u32ErrorCode()
                             );
            u32RetVal += 256;
         }
         else
         {
            u32RetVal += 512;
         }
      }
   }
   else
   {
      u32RetVal += 1024;
   }
   u32OpenCount = u32count;
   for(u32count=0;u32count<u32OpenCount;u32count++)
   {/*Open Successful*/
      if (OSAL_s32IOClose(hDevHandle[u32count]) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 2048;
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlDoubleClose

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_005

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/

tU32  u32OedtADR3CtrlDoubleClose(tVoid)
{
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   tS32 s32OSAL_Ret;
    /*Opens ADR3 CTRL*/
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if ( hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                         "Device open failed with error %d",
                         OSAL_u32ErrorCode()
                       );
      u32RetVal += 1;
   }
   else
   {/*Open Successful*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 2;
      }
      else
      {
         /*Second Close*/
         if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
         {
           s32OSAL_Ret = (tS32)OSAL_u32ErrorCode();
            switch(s32OSAL_Ret)
            {
               case OSAL_E_BADFILEDESCRIPTOR:  /* Expected error */		 
                  break;
               default:
                  u32RetVal += 4;
                  break;
            }//end of switch( s32OSAL_Ret )
         }
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlInvalidIoControl

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_006

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/

tU32  u32OedtADR3CtrlInvalidIoControl(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   tU32 u32RetVal = 0;
   tS32 s32OSAL_Ret;
    /*Opens ADR3 CTRL*/
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if(hDevHandle == OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                        "Device open failed with error %d",
                        OSAL_u32ErrorCode()
                       );
      u32RetVal += 1;
   }
   else
   {/*Open Successful*/
     if(OSAL_ERROR ==  OSAL_s32IOControl(hDevHandle,
                                         (tS32)NULL, 
                                         (tS32)vAdr3ctrlCallback 
                                        )
	   )
      {
         s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
         switch(s32OSAL_Ret)
         {
            case OSAL_E_INVALIDVALUE:  /* Expected error */		 
               break;
            default:
              u32RetVal += 2;
			   break;
         }//end of switch( s32OSAL_Ret )
      }
	  else
	  {
		u32RetVal += 4;
	  }
      /* Close the device*/      
      if (OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS, 
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 8;
      }
   }
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlCallBackRegister

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_007

* DESCRIPTION :   To check OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK Iocontrol
*                Open the ADR3
*                Create the Event for callback changes handling
*                Register for the ADR3 CallBack
*                Wait for the ADR3 CallBack event
*                UnRegister the ADR3 Call Back
*                Delete the event
*                Close the ADR3      
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32  u32OedtADR3CtrlCallBackRegister(tVoid)
{
   OSAL_tIODescriptor hDevHandle = 0;
   OSAL_tEventHandle hCallBackRegistryEventHndlr; 
   tU32 u32RetVal = 0;
   tU32 hEvRequest;
   tU32 dwEventMask = ADR3CALLBACK_INVALIDSTATE |
                      ADR3CALLBACK_ALIVESTATE |
                      ADR3CALLBACK_DEADSTATE| 
                      ADR3CALLBACK_DNLSTATE|
                      ADR3CALLBACK_INITSTATE;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(CALLBACKREGISTRY_EVENTNAME,
                                     &hCallBackRegistryEventHndlr
                                    )
     )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Event create failed err %lu", 
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal = 1; 
   }
   /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hCallBackRegistryEventHndlr;
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      /* Wait for the event */
      hEvRequest = u32ADR3StateWait(hCallBackRegistryEventHndlr,
                                    dwEventMask,
                                    OSAL_EN_EVENTMASK_OR,
                                    __LINE__
                                   );
      /* ADR3CTRL_STATEWAIT_MIN_ERROR is the minimum error form 
                                         ADR3Statewait function*/
      if(hEvRequest >= ADR3CTRL_STATEWAIT_MIN_ERROR)
      {
         /* Adding the wait error value with return Value*/
         u32RetVal += hEvRequest;
      }
      else if(ADR3CALLBACK_INVALIDSTATE & hEvRequest)
      {
         u32RetVal += 4;
      }
       /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(CALLBACKREGISTRY_EVENTNAME,
                                         &hCallBackRegistryEventHndlr,
                                         __LINE__
                                        )
        )
      {
         u32RetVal += 16;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 32;
      }
   }
      /* Remove the permissions from CallBack to send the event*/
   bEventPostMechansmActive = FALSE;
   CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
   return(u32RetVal);
}

/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlResetCheck

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_008

* DESCRIPTION :   To check OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3 Iocontrol
*                Open the ADR3
*                Create the Event for callback changes handling
*                Register for the ADR3 CallBack
*                Reset the ADR3                
*                Wait for the ADR3 CallBack Dead and alive state changes
*                UnRegister the ADR3 Call Back
*                Delete the event
*                Close the ADR3 
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32  u32OedtADR3CtrlResetCheck(tVoid)
{
   OSAL_tEventHandle hADR3ResetCheckEvntHndlr; 
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3RESETCHECK_EVENTNAME ,
                                     &hADR3ResetCheckEvntHndlr 
                                    )
     )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Event create failed err %lu", 
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal += 1;
   }
   
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3ResetCheckEvntHndlr;
      /* Reset ADR3 */
       /* Reset the ADR3CRL using IOControl  and wait till the callback execution*/
      u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                          OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3,
                                          ADR3CTRL_NORMALMODE
                                         );
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
        /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(ADR3RESETCHECK_EVENTNAME,
                                         &hADR3ResetCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         u32RetVal += 8;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 16;
      }     
   }
   return(u32RetVal); 
}


/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlSPIModResetCheck

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32,"0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_009

* DESCRIPTION :  
*               a) Open the device ADR3CTRL
*               b) Register for callback
*               c) Change the ADR3CTRl operation mode to Download mode
*               d) RESET the ADR3CTRL
*               d) Wait for the callback
*               e) Close the device
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32  u32OedtADR3CtrlSPIModResetCheck(tVoid)
{
   OSAL_tEventHandle hADR3ResetCheckEvntHndlr; 
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3SPIMODCHECK_EVENTNAME ,
                                     &hADR3ResetCheckEvntHndlr 
                                    )
      )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                        "Event create failed err %lu",
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal += 1;
   }
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      /* Allow the permission for call Back to send the event */        
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3ResetCheckEvntHndlr;    
      /* Change the ADR3 state to Download mode using IOControl  and wait till the callback execution*/
      u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                          OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI,
                                          ADR3CTRL_SPIMODE
                                          );
      if(0 == u32RetVal)
      {
      /* Reset the ADR3CRL using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3,
                                             ADR3CTRL_SPIMODE
                                            );
      }
      if(0 == u32RetVal)
      {
          /* Change the ADR3 state to normal mode using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL,
                                             ADR3CTRL_NORMALMODE
                                             );
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;

   /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(ADR3SPIMODCHECK_EVENTNAME,
                                          &hADR3ResetCheckEvntHndlr,
                                          __LINE__
                                          )
        )
      {
         u32RetVal += 2;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                           );
         u32RetVal += 3;
      }     
   }
   return(u32RetVal); 
}
/*****************************************************************************
* FUNCTION    :  u32OedtADR3CtrlSPI_NOR_ModeCheck

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   : TU_OEDT_DEV_ADR3CTRL_TEST_010

* DESCRIPTION : To check OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL Iocontrol
*                Open the ADR3
*                Create the Event for callback changes handling
*                Register for the ADR3 CallBack
*                Change the ADR3 to DNL SPI_BOOTMODE Iocontrol
*                Wait till the ADR3 state changes to DNL mode
*                Change the DAR3 State Using BOOTMODE_NORMAL IOControl             
*                Wait for the ADR3 CallBack Dead and alive state changes
*                UnRegister the ADR3 Call Back
*                Delete the event
*                Close the ADR3 
*
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32OedtADR3CtrlSPI_NOR_ModeCheck(tVoid)
{
   OSAL_tEventHandle hADR3NorModCheckEvntHndlr; 
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3NORMODCHECK_EVENTNAME,
                                     &hADR3NorModCheckEvntHndlr 
                                    )
     )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Event create failed err %lu", 
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal = 1;
   }
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE; 
      CallBackEventHndlr = hADR3NorModCheckEvntHndlr;
      /* Change the state to DNL mode*/
      u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                          OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI,
                                          ADR3CTRL_SPIMODE
                                          );
      if(0 == u32RetVal)
      {
          /* Change the ADR3 state to normal mode using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL,
                                             ADR3CTRL_NORMALMODE
                                             );
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
 
      /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(ADR3NORMODCHECK_EVENTNAME,
                                         &hADR3NorModCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         u32RetVal += 2;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 4;
      }  
   }
   return(u32RetVal);
}
/*****************************************************************************
* FUNCTION    :  GetADR3CtrlProcessResponse

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION : 
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 GetADR3CtrlProcessResponse(OSAL_tEventHandle mqHandle, tU32 u32Response, tU32 u32Linenmbr)
{
   tU32    u32RetVal = 0;
   tU32    inmsg = 0;
   if(0 == OSAL_s32MessageQueueWait(mqHandle,
                                   (tPU8)&inmsg,
                                   sizeof(inmsg), 
                                   OSAL_NULL, 
                                   (OSAL_tMSecond)ADR3CTRL_RESPONSE_WAIT_TIME
                                  ) 
     )
   {
      u32RetVal += 32768;
   }
   else if(u32Response != inmsg)
   {
      u32RetVal += 16403;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Response didn't match expected =%d,received =%d at line =%d",
                           u32Response,
                           inmsg,
                           u32Linenmbr
                          );
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32ADR3Ctrl_OEDTProcSpwn

* PARAMETER   :  tU32 u32ProcCMDLineArg

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Spawn the process with command line argument
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3Ctrl_OEDTProcSpwn(tU32 u32ProcCMDLineArg)
{
   OSAL_trProcessAttribute prAtr = {OSAL_NULL};
   OSAL_tProcessID prID_2;
   OSAL_tProcessID prID_1;
   tU32 u32RetVal = 0;
   tChar sProcCmdInput[11] = {0};   
   snprintf(sProcCmdInput,sizeof(sProcCmdInput),"%d",u32ProcCMDLineArg);
   tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";
   /* Spawning the Threads*/
   prAtr.szName         = "procadr3ctrl1_out.out";
   OSAL_szStringNCopy(caProcessPath,
                      OEDT_TESTPROCESS_BINARY_PATH,
                      OSAL_C_U32_MAX_PATHLENGTH
                     );
   prAtr.szAppName = OSAL_szStringNConcat(caProcessPath,
                                          prAtr.szName,
                                          (OSAL_C_U32_MAX_PATHLENGTH
                                           -
                                           (OSAL_u32StringLength(caProcessPath)
                                             -
                                             1
                                            )
                                          )
                                        );
   prAtr.u32Priority    = 100;
   prAtr.szCommandLine  = sProcCmdInput;
   prID_1 = OSAL_ProcessSpawn(&prAtr);
   if(OSAL_ERROR == prID_1)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                        "proc1 spawn failed: %lu",
                        OSAL_u32ErrorCode()
                       );
      u32RetVal += 131072;
   }

   prAtr.szName         = "procadr3ctrl2_out.out";
   OSAL_szStringNCopy(caProcessPath,
                      OEDT_TESTPROCESS_BINARY_PATH,
                      OSAL_C_U32_MAX_PATHLENGTH
                     );
   prAtr.szAppName = OSAL_szStringNConcat(caProcessPath,
                                          prAtr.szName,
                                          (OSAL_C_U32_MAX_PATHLENGTH
                                           -
                                           (OSAL_u32StringLength(caProcessPath)
                                             -
                                             1
                                            )
                                          )
                                         );
   prAtr.u32Priority    = 100;
   prAtr.szCommandLine  = sProcCmdInput;
   prID_2 = OSAL_ProcessSpawn(&prAtr);
   if(OSAL_ERROR == prID_2)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                        "proc2 spawn failed: %d",
                        OSAL_u32ErrorCode()
                       );
      u32RetVal += 262144;
   } 
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32MP_MQCreate

* PARAMETER   :  tCString,OSAL_tMQueueHandle*,tCString,OSAL_tMQueueHandle*

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32MP_MQCreate(tCString csMQProc1Name,
                    OSAL_tMQueueHandle* pMQP1HandleR,
                    tCString csMQProc2Name,
                    OSAL_tMQueueHandle* pMQP2HandleR
                   )
{
   tU32 u32RetVal = 0;
   if(OSAL_OK != OSAL_s32MessageQueueCreate(csMQProc1Name,
                                            MQ_ADR3CTRL_MP_CONTROL_MAX_MSG, 
                                            MQ_ADR3CTRL_MP_CONTROL_MAX_LEN,
                                            OSAL_EN_READWRITE, 
                                            pMQP1HandleR
                                           ) 
      )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "MessageQueueCreate failed with error %d",
                           OSAL_u32ErrorCode()
                          );
      u32RetVal += 524288;
   }
   if(OSAL_OK != OSAL_s32MessageQueueCreate(csMQProc2Name,
                                            MQ_ADR3CTRL_MP_CONTROL_MAX_MSG, 
                                            MQ_ADR3CTRL_MP_CONTROL_MAX_LEN,
                                            OSAL_EN_READWRITE, 
                                            pMQP2HandleR
                                           ) 
      )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "MessageQueueCreate failed with error %d",
                           OSAL_u32ErrorCode()
                          );
      u32RetVal += 1048576;
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32MP_MQ_Cls_Del

* PARAMETER   :  tCString,OSAL_tMQueueHandle,tCString,OSAL_tMQueueHandle

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  To close and Delete Message queues
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32MP_MQ_Cls_Del(tCString csMQProc1Name,
                      OSAL_tMQueueHandle mqP1HandleR,
                      tCString csMQProc2Name,
                      OSAL_tMQueueHandle mqP2HandleR
                     )
{
   tU32 u32RetVal = 0;
   if(OSAL_OK ==OSAL_s32MessageQueueClose(mqP1HandleR))
   {
      if(OSAL_OK !=OSAL_s32MessageQueueDelete(csMQProc1Name))
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "MQDelete failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 1024;
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "MQClose failed with error %d",
                           OSAL_u32ErrorCode()
                          );
      u32RetVal += 512;
   }
   if(OSAL_OK ==OSAL_s32MessageQueueClose(mqP2HandleR))
   {
      if(OSAL_OK !=OSAL_s32MessageQueueDelete(csMQProc2Name))
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "MQDelete failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 256;
      }
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,
                           "MQClose failed with error %d",
                           OSAL_u32ErrorCode()
                          );
      u32RetVal += 128;
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMultiProcessTest

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   : TU_OEDT_DEV_ADR3CTRL_TEST_011

* DESCRIPTION :  
*                  OEDT operation
*                  a) Spawn two process
*                  b) Open the device ADR3CTRL
*                  c) Register CallBack
*                  d) Wait for the responses from Process for device register completion
*                  e) Reset the ADR3CTRL 
*                  f) Wait for callback response
*                  g) Wait for the response from process for Callback execute completion
*                  h) Close the device
*
*                  Process operation
*                  i)  Open the device ADR3CTRL
*                  j)  Register CallBack
*                  k) Send the register completion response to OEDT thread
*                  l)   Wait for reset callback
*                  m) Send the reset callback execute completion to OEDT thread
*                  n) Close the device
*               
*                        NOR ----> DEAD ----->NOR
*
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3CtrlMultiProcessTest(tVoid)
{
   OSAL_tMQueueHandle mqP1HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle mqP2HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tEventHandle hADR3_MPResetCheckEvntHndlr;  
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3_MP_RESETCHECK_EVENTNAME,
                                     &hADR3_MPResetCheckEvntHndlr 
                                    )
     )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Event create failed err %lu", 
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal += 1;
   }  
  // Create or open MQ for both processes to send their responses
   u32RetVal += u32MP_MQCreate(MQ_ADR3CTRL_MP1_RESPONSE_NAME,
                              &mqP1HandleR,
                              MQ_ADR3CTRL_MP2_RESPONSE_NAME,
                              &mqP2HandleR
                             );
   if(0 == u32RetVal)
   {
      u32RetVal += u32ADR3Ctrl_OEDTProcSpwn(MQ_ADR3CTRL_MP_NOR_RST_CNCRNCY);
   }
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      // Wait till two processes register for callback
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRl_MP1_CB_REGSTER,
                                             __LINE__
                                            );
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3_MPResetCheckEvntHndlr;
      /* Reset ADR3 */
      if(0 == u32RetVal)
      {
      /* Reset the ADR3CRL using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3,
                                             ADR3CTRL_NORMALMODE
                                            );
      }
      if(0 == u32RetVal)
      {
         u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                                MQ_ADR3CTRl_MP1_RESET_OBSERVED,
                                                __LINE__
                                               );  
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
      /* Close the resource*/
      if(OSAL_OK != u32EventClose_Delete(ADR3_MP_RESETCHECK_EVENTNAME,
                                         &hADR3_MPResetCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                              "u32EventClose_Delete failed with error %d",
                              OSAL_u32ErrorCode()
                             );
         u32RetVal += 2;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 4;
      }
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRL_MP1_PROCESS_EXIT,
                                             __LINE__
                                            );
   /* Wait till the 2 Process completes the resource clean*/
      OSAL_s32ThreadWait(2000);
   /* Cleaning up the created resources*/
      u32RetVal += u32MP_MQ_Cls_Del(MQ_ADR3CTRL_MP1_RESPONSE_NAME,
                                   mqP1HandleR,
                                   MQ_ADR3CTRL_MP2_RESPONSE_NAME,
                                   mqP2HandleR
                                 );
      
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMPOpenTest

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_012

* DESCRIPTION :  
*                     OEDT Operation:
*                  a) Spawn Two process
*                  b) Wait for the response from the Process
*                  c) Wait for 2 Secs so the process will exit
*
*                  Process Operation:
*                  d) Continuously open  device ADR3CTRL for five times
*                  e) Close the devices 
*                  f) Respond to the OEDT test case.
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3CtrlMPOpenTest(tVoid)
{
   tU32 u32RetVal = 0;
   OSAL_tMQueueHandle mqP1HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle mqP2HandleR = OSAL_C_INVALID_HANDLE;
   u32RetVal += u32MP_MQCreate(MQ_ADR3CTRL_MP1_OPEN_DEVICE_NAME,
                              &mqP1HandleR,
                              MQ_ADR3CTRL_MP2_OPEN_DEVICE_NAME,
                              &mqP2HandleR
                             );
   if(0 == u32RetVal)
   {
      u32RetVal += u32ADR3Ctrl_OEDTProcSpwn(MQ_ADR3CTRL_MP_OPEN_CNCRNCY);
   }
   if(0 == u32RetVal)   
   {
      /* Wait for both process to close the resources*/
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRl_MP1_OPEN1,
                                             __LINE__
                                            );
      u32RetVal +=GetADR3CtrlProcessResponse(mqP2HandleR,
                                             MQ_ADR3CTRl_MP2_OPEN2,
                                             __LINE__
                                            );
   }
   /* Wait till the 2 Process completes the resource clean*/
   OSAL_s32ThreadWait(2000);
   /* Cleaning up the created resources*/
   u32RetVal += u32MP_MQ_Cls_Del(MQ_ADR3CTRL_MP1_OPEN_DEVICE_NAME,
                                 mqP1HandleR,
                                 MQ_ADR3CTRL_MP2_OPEN_DEVICE_NAME,
                                 mqP2HandleR
                                );
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMPCloseTest 

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_013

* DESCRIPTION :  
*                   OEDT Operation:
*                  a) Spawn Two process
*                  b) Wait for the response from the Process
*                  c) Wait for 2 secs so the process will exit
*
*                  Process Operation:
*                  d) Open 5 ADR3CTRL Devices
*                  e) Continuously Close the devices 
*                  f) Respond to the OEDT test case.
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3CtrlMPCloseTest(tVoid)
{
   tU32 u32RetVal = 0;
   OSAL_tMQueueHandle mqP1HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle mqP2HandleR = OSAL_C_INVALID_HANDLE;
   u32RetVal += u32MP_MQCreate(MQ_ADR3CTRL_MP1_CLOSE_DEVICE_NAME,
                              &mqP1HandleR,
                              MQ_ADR3CTRL_MP2_CLOSE_DEVICE_NAME,
                              &mqP2HandleR
                             );
   if(0 == u32RetVal)
   {
      u32RetVal += u32ADR3Ctrl_OEDTProcSpwn(MQ_ADR3CTRL_MP_CLOSE_CNCRNCY);
   }
   if(0 == u32RetVal)  
   {   
      /* Wait for both process to close the resources*/
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRl_MP1_CLSE1,
                                             __LINE__
                                            );
      u32RetVal +=GetADR3CtrlProcessResponse(mqP2HandleR,
                                             MQ_ADR3CTRl_MP2_CLSE2,
                                             __LINE__
                                            );
   }
   /* Wait till the 2 Process completes the resource clean*/
   OSAL_s32ThreadWait(2000);
   /* Cleaning up the created resources*/
   u32RetVal += u32MP_MQ_Cls_Del(MQ_ADR3CTRL_MP1_CLOSE_DEVICE_NAME,
                                 mqP1HandleR,
                                 MQ_ADR3CTRL_MP2_CLOSE_DEVICE_NAME,
                                 mqP2HandleR
                               );

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMPRegistrCBTest

* PARAMETER   :  tVoid

* RETURNVALUE :  tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :  TU_OEDT_DEV_ADR3CTRL_TEST_014

* DESCRIPTION :  
                  OEDT Operation:
*                  a) Spawn Two process
*                  b) Wait for the response from the Process
*                  c) Wait for 2 secs so the process will exit
*
*                  Process Operation:
*                  d) Open ADR3CTRL Device
*                  e) Register for the callback for 5 times
*                  f) Close the device.
*                  g) Respond to the OEDT test case.
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3CtrlMPRegistrCBTest(tVoid)
{
   tU32 u32RetVal = 0;
   OSAL_tMQueueHandle mqP1HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle mqP2HandleR = OSAL_C_INVALID_HANDLE;
   /* Multi Process Message Queue resources creation */
   u32RetVal += u32MP_MQCreate(MQ_ADR3CTRL_MP1_REG_CB_NAME,
                              &mqP1HandleR,
                              MQ_ADR3CTRL_MP2_REG_CB_NAME,
                              &mqP2HandleR
                             );
   if(0 == u32RetVal)
   {
         /* Spawn the process to perform the operation*/
      u32RetVal += u32ADR3Ctrl_OEDTProcSpwn(MQ_ADR3CTRL_MP_CBREG_CNCRNCY);
   }
   if(0 == u32RetVal)
   {   /* Wait for both process to close the resources*/
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRl_MP1_REG1,
                                             __LINE__
                                            );
      u32RetVal +=GetADR3CtrlProcessResponse(mqP2HandleR,
                                             MQ_ADR3CTRl_MP2_REG2,
                                             __LINE__
                                            );
   }
   /* Wait till the 2 Process completes the resource clean  */
   OSAL_s32ThreadWait(2000);
   /* Cleaning up the created resources*/
   u32RetVal += u32MP_MQ_Cls_Del(MQ_ADR3CTRL_MP1_REG_CB_NAME,
                                mqP1HandleR,
                                MQ_ADR3CTRL_MP2_REG_CB_NAME,
                                mqP2HandleR
                               );

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMPResetSPITest

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   : TU_OEDT_DEV_ADR3CTRL_TEST_015

* DESCRIPTION :  
*                  OEDT operation
*                  a) Spawn two process
*                  b) Open the device ADR3CTRL
*                  c) Register CallBack
*                  d) Wait for the responses from Process for device register completion
*                  e) Change the ADR3CTRl state to Download mode
*                  f) Reset the ADR3CTRL 
*                  g) Wait for callback response
*                  h) Wait for the response from process for Callback execute completion
*                  i) Close the device
*
*                  Process operation
*                  j)  Open the device ADR3CTRL
*                  k)  Register CallBack
*                  l) Send the register completion response to OEDT thread
*                  m)   Wait for reset callback
*                  n) Send the reset callback execute completion to OEDT thread
*                  o) Close the device
*   NOR--->DEAD--->SPI(SPI mod IOCONtrol)   --->DEAD----->SPI(Reset IOCONtrol) -----> DEAD ---> NOR (NOR mod IOCONtrol)
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Jan 2,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
tU32 u32ADR3CtrlMPResetSPITest(tVoid)
{
   OSAL_tMQueueHandle mqP1HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle mqP2HandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tEventHandle hADR3_MPResetCheckEvntHndlr;  
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3_MP_RESETCHECK_EVENTNAME,
                                     &hADR3_MPResetCheckEvntHndlr 
                                    )
     )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                        "Event create failed err %lu", 
                        OSAL_u32ErrorCode() 
                       );
      u32RetVal += 1;
   }  
  // Create or open MQ for both processes to send their responses
   u32RetVal += u32MP_MQCreate(MQ_ADR3CTRL_MP1_RESPONSE_NAME,
                              &mqP1HandleR,
                              MQ_ADR3CTRL_MP2_RESPONSE_NAME,
                              &mqP2HandleR
                             );
   
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3_MPResetCheckEvntHndlr;
       /* Change the ADR3 state to Download mode using IOControl  and wait till the callback execution*/
      u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                          OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI,
                                          ADR3CTRL_SPIMODE
                                          );
      /* Spawn the process*/
      if(0 == u32RetVal)
      {
         u32RetVal += u32ADR3Ctrl_OEDTProcSpwn(MQ_ADR3CTRL_MP_SPI_RST_CNCRNCY);
      }
      /* Wait till the Process register for Callback*/
      if(0 == u32RetVal)
      {
         u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                                MQ_ADR3CTRl_MP1_CB_REGSTER,
                                                __LINE__
                                               );
      }
      /* Reset ADR3 */
      if(0 == u32RetVal)
      {
      /* Reset the ADR3CRL using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3,
                                             ADR3CTRL_SPIMODE
                                            );
      }
      if(0 == u32RetVal)
      {
         u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                                MQ_ADR3CTRl_MP1_RESET_OBSERVED,
                                                __LINE__
                                               );
      }
      /* Revert back the adr3ctrl state to alive*/
      if(0 == u32RetVal)
      {
          /* Change the ADR3 state to normal mode using IOControl  and wait till the callback execution*/
         u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,
                                             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL,
                                             ADR3CTRL_NORMALMODE
                                             );
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
      if(OSAL_OK != u32EventClose_Delete(ADR3_MP_RESETCHECK_EVENTNAME,
                                         &hADR3_MPResetCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                              "IOControl failed with error %d",
                              OSAL_u32ErrorCode()
                             );
         u32RetVal += 2;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,
                           "Device Close failed with error %d",
                           OSAL_u32ErrorCode()
                          );
         u32RetVal += 4;
      }
      u32RetVal +=GetADR3CtrlProcessResponse(mqP1HandleR,
                                             MQ_ADR3CTRL_MP1_PROCESS_EXIT,
                                             __LINE__
                                            );
   /* Wait till the 2 Process completes the resource clean*/
      OSAL_s32ThreadWait(2000);
   /* Cleaning up the created resources*/
      u32RetVal += u32MP_MQ_Cls_Del(MQ_ADR3CTRL_MP1_RESPONSE_NAME,
                                   mqP1HandleR,
                                   MQ_ADR3CTRL_MP2_RESPONSE_NAME,
                                   mqP2HandleR
                                 );
      
   }
   return u32RetVal;
}
