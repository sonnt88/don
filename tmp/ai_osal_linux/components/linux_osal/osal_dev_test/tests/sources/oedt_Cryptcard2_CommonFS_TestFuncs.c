/******************************************************************************
 * FILE         : oedt_Cryptcard2_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the file system test cases for Cryptcard2.
 *                          
 * AUTHOR(s)    : Anooj Gopi (RBEI/ECF1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                  | Author & comments
 * --.--.--       | Initial revision | ------------
 * 19 Apr, 2011   | version 1.0      | Anooj Gopi (RBEI/ECF1)
 *                |                  | Initial version ported from gen1
 *------------------------------------------------------------------------------*/
 
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
//#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_Cryptcard2_CommonFS_TestFuncs.h"

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpenClosedevice( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_001
* DESCRIPTION  :   Opens and closes Cryptcard2 device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSOpenClosedevice(tVoid)
{ 
   return u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpendevInvalParm( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_002
* DESCRIPTION  :   Try to Open Cryptcard2 device with invalid parameters
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSOpendevInvalParm(tVoid)
{
   return u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReOpendev( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_003
* DESCRIPTION  :   Try to Re-Open Cryptcard2 device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReOpendev(tVoid)
{
   return u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpendevDiffModes( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_004
* DESCRIPTION  :   Try to Re-Open Cryptcard2 device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSOpendevDiffModes(tVoid)
{
   return u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSClosedevAlreadyClosed( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_005
* DESCRIPTION  :   Try to Re-Open Cryptcard2 device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard2_CommonFSClosedevAlreadyClosed(tVoid)
{
   return u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpenClosedir( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_006
* DESCRIPTION  :   Try to Open and closes a directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard2_CommonFSOpenClosedir(tVoid)
{
   return u32FSOpenClosedir((const tPS8)OEDT_C_STRING_CRYPTCARD2_DIR1);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpendirInvalid( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_007
* DESCRIPTION  :   Try to Open invalid directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard2_CommonFSOpendirInvalid(tVoid)
{
   tPS8 dev_name[2] = {
                      (tPS8)OEDT_C_STRING_CRYPTCARD2_NONEXST,
                      (tPS8)OEDT_C_STRING_CRYPTCARD2_DIR1
                      };
					  
   return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSGetDirInfo( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_008
* DESCRIPTION  :   Get directory information
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSGetDirInfo(tVoid)
{
   return u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpenDirDiffModes( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_009
* DESCRIPTION  :   Try to open directory in different modes
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSOpenDirDiffModes(tVoid)
{
   tPS8 dev_name[2] = {
                      (tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2,
                      (tPS8)OEDT_C_STRING_CRYPTCARD2_DIR1
                      };
					  
   return u32FSOpenDirDiffModes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReOpenDir( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_010
* DESCRIPTION  :   Try to reopen directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReOpenDir(tVoid)
{
   tPS8 dev_name[2] = {
                     (tPS8)OEDT_C_STRING_DEVICE_CRYPTCARD2_ROOT,
                     (tPS8)OEDT_C_STRING_CRYPTCARD2_DIR1
                     };
					 
   return u32FSReOpenDir(dev_name, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileOpenClose( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_011
* DESCRIPTION  :   Open /close File
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileOpenClose(tVoid)
{
   return u32FSFileOpenClose((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}   


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileOpenInvalPath( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_012
* DESCRIPTION  :   Open file with invalid path name (should fail)
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileOpenInvalPath(tVoid)
{
   return u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_CRYPTCARD2);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileOpenInvalParam( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_013
* DESCRIPTION  :   Open a file with invalid parameters (should fail), 
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileOpenInvalParam(tVoid)
{
   return u32FSFileOpenInvalParam((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReOpen( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_014
* DESCRIPTION  :   Try to open and close the file which is already opened
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32  u32Cryptcard2_CommonFSFileReOpen(tVoid)
{
   return u32FSFileReOpen((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileRead( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_015
* DESCRIPTION  :   Read data from already exsisting file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileRead(tVoid)
{   
   return u32FSFileRead((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSGetPosFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_016
* DESCRIPTION  :   Get File Position from begining of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSGetPosFrmBOF(tVoid)
{
   return u32FSGetPosFrmBOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSGetPosFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_017
* DESCRIPTION  :   Get File Position from end of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSGetPosFrmEOF(tVoid)
{
   return u32FSGetPosFrmEOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_018
* DESCRIPTION  :   Read with a negative offset from BOF
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32   u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF()
{
   return u32FSFileReadNegOffsetFrmBOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_019
* DESCRIPTION  :   Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
   return u32FSFileReadOffsetBeyondEOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_020
* DESCRIPTION  :  Read from offset from Beginning of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileReadOffsetFrmBOF(tVoid)
{   
   return u32FSFileReadOffsetFrmBOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_021
* DESCRIPTION  :   Read file with few offsets from EOF
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileReadOffsetFrmEOF(tVoid)
{   
   return u32FSFileReadOffsetFrmEOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_022
* DESCRIPTION  :   Reads file by skipping certain offsets at specified intervals.
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
   return u32FSFileReadEveryNthByteFrmBOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_023
* DESCRIPTION  :   Reads file by skipping certain offsets at specified intervals.
*                  (This is from EOF)      
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
   return u32FSFileReadEveryNthByteFrmEOF((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSGetFileCRC( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_024
* DESCRIPTION  :   Get CRC value
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard2_CommonFSGetFileCRC(tVoid)
{
   return u32FSGetFileCRC((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadAsync()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_025
* DESCRIPTION  :   Read data asyncronously from a file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSReadAsync(tVoid)
{   
   return u32FSReadAsync((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSLargeReadAsync()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_026
* DESCRIPTION  :   Read data asyncronously from a large file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSLargeReadAsync(tVoid)
{   
   return u32FSLargeReadAsync((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);
}

   
/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileOpenCloseNonExstng()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_027
* DESCRIPTION  :   Open a non existing file 
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSFileOpenCloseNonExstng(tVoid)
{
   return u32FSFileOpenCloseNonExstng ((const tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/Dummy.txt");
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSSetFilePosDiffOff()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_028
* DESCRIPTION  :   Set file position to different offsets
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSSetFilePosDiffOff(tVoid)
{
   return u32FSSetFilePosDiffOff ((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileOpenDiffModes()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_029
* DESCRIPTION  :   Create file with different access modes
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSFileOpenDiffModes(tVoid)
{
   tU32 u32Ret = 0; 

   u32Ret = u32FSFileOpenDiffModes ((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE);

   if (u32Ret == 50)
   {
      u32Ret = 0;
   }
   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSFileThroughPutFirstFile()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_030
* DESCRIPTION  :   Calculate throughput for first Cryptcard2 file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSFileThroughPutFile(tVoid)
{
   return u32FSFileThroughPutFirstFile((const tPS8)OEDT_C_STRING_CRYPTCARD2_DAT_FILE);
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSLargeFileRead()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_031
* DESCRIPTION  :   Read large file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSLargeFileRead(tVoid)
{   
   return u32FSLargeFileRead ((const tPS8) OEDT_C_STRING_CRYPTCARD2_DAT_FILE, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSOpenCloseMultiThread()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_032
* DESCRIPTION  :   Open and close file from multiple threads
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSOpenCloseMultiThread(tVoid)
{   
   return u32FSOpenCloseMultiThread ((const tPS8) OEDT_C_STRING_CRYPTCARD2_DAT_FILE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadMultiThread()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_033
* DESCRIPTION  :   Read from file from multiple threads
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard2_CommonFSReadMultiThread(tVoid)
{   
   return u32FSReadMultiThread ((const tPS8) OEDT_C_STRING_CRYPTCARD2_DAT_FILE );
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadDirExt()
* PARAMETER    :   Device Name
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_034
* DESCRIPTION  :   Read file/directory contents 
* HISTORY      :   Created by Ravindran P(RBEI/ECF1) on 18 Aug, 2009
*                  Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported for gen2
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReadDirExt(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_ACCESS_DIR;
   OSAL_tIODescriptor hFile = 0; 
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExtDir s32Stat = {0};
   OSAL_trIOCtrlExtDirent pDir;
          
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;/*No of entries to be read,say 1 */                          
   pDir.enFileFlags = (OSAL_tenFileFlags)0;
   OSAL_pvMemorySet(pDir.s8Name,'\0',sizeof(pDir.s8Name));   
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;

   hFile = OSAL_IOOpen (OEDT_C_STRING_CRYPTCARD2_DIR1,enAccess );
   if( hFile != OSAL_ERROR )
   {
      if(OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOREADDIREXT, (tS32)&s32Stat) == OSAL_ERROR)
      {
         u32Ret += 10;
      }

      if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadDirExt2()
* PARAMETER    :   Device Name
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD2_CFS_035
* DESCRIPTION  :   Read file/directory contents 
* HISTORY      :   Created by Ravindran P(RBEI/ECF1) on 18 Aug, 2009
*                  Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported for gen2
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReadDirExt2(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_ACCESS_DIR;
   OSAL_tIODescriptor hFile = 0; 
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExtDir s32Stat = {0};
   OSAL_trIOCtrlExtDirent pDir;
   
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;/*No of entries to be read,say 1 */                          
   pDir.enFileFlags = (OSAL_tenFileFlags)0;
   OSAL_pvMemorySet(pDir.s8Name,'\0',sizeof(pDir.s8Name));   
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;

   hFile = OSAL_IOOpen (OEDT_C_STRING_CRYPTCARD2_DIR1,enAccess );
   if( hFile != OSAL_ERROR )
   {
      if(OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, (tS32)&s32Stat) == OSAL_ERROR)
      {
         u32Ret += 10;
      }

      if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadDirExtPartByPart
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_USB_CFS_036
* DESCRIPTION  :   ReadDirExtPartByPart
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReadDirExtPartByPart(tVoid)
{
   return u32FSReadDirExtPartByPart((tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard2_CommonFSReadDirExt2PartByPart
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_USB_CFS_037
* DESCRIPTION  :   ReadDirExt2PartByPart
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard2_CommonFSReadDirExt2PartByPart(tVoid)
{
   return u32FSReadDirExt2PartByPart((tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2);
}

/* EOF */                                                   

