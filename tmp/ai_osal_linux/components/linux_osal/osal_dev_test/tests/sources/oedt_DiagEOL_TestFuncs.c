/******************************************************************************
 * FILE          : oedt_DiagEOL_TestFuncs.c
 *
 * SW-COMPONENT  : OEDT_FrmWrk 
 *
 * DESCRIPTION   : This file implements the individual test cases for the DiagEOL 
 *                 device for GM-GE hardware.
 *              
 * AUTHOR(s)     :  Shilpa Bhat (RBIN/ECM1)
 *
 * HISTORY       :
 *-----------------------------------------------------------------------------
 * Date          | 		     		          | Author & comments
 * --.--.--      | Initial revision        | ------------
 * 26 Feb,2008   | version 1.0             | Shilpa Bhat(RBIN/ECM1)
*******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_DiagEOL_TestFuncs.h"
#include "osal_if.h"

/* EOL table and offset definitions */
#include "../../../../devices/EOLLib/include/EOLLib.h"


/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_001
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDevOpenClose(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
   
	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd != OSAL_ERROR)
	{
		/* Device Close */
		if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)
		{
			u32Ret = 1;	
		}
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_002
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Re-open the DiagEOL device
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDevReOpen(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
   OSAL_tIODescriptor hFd1 = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open device */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd != OSAL_ERROR)
	{
	   /* Attempt to Re-open Device */
		hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
		if(hFd1 == OSAL_ERROR)
		{
			u32Ret = 1;
		}
		else
		{
			if(OSAL_s32IOClose ( hFd1 ) == OSAL_ERROR)
			{
				u32Ret = 2;
			}   
		}
		
		/* Close the channel */
	   if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	else
	{
		u32Ret = 10;
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_003
* DESCRIPTION:  1. Open DiagEOL device
*               2. Close DiagEOL device
*               3. Attempt to Close device again
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDevCloseAlreadyClosed(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open Device */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd != OSAL_ERROR)
	{
		/* Close device */
		if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)
		{
			u32Ret = 1;
		}
		else
		{
			/* Attempt to Close the device which is already closed */
		   if(OSAL_s32IOClose ( hFd ) != OSAL_ERROR)
			{
				u32Ret += 2;
			}
		}
	}
	else
	{
		u32Ret = 4;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevCloseInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_004
* DESCRIPTION:  1. Open DiagEOL device
*               2. Attempt to Close device with invalid parameter
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
*               Modified by Shilpa Bhat (RBEI/ECM1) on 06 March, 2008
******************************************************************************/
tU32 u32DiagEOLDevCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFd_Inval = OSAL_NULL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open DiagEOL device */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd != OSAL_ERROR)
	{
		/* Attempt to close device with invalid handle */
		if(OSAL_s32IOClose ( hFd_Inval ) != OSAL_ERROR)
		{
			u32Ret = 1;
		}
		else
		{
			/* Close device with valid handle */
			if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)			
			{
				u32Ret = 2;				
			}
		}
	}
	else
	{
		u32Ret = 4;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevOpenCloseDiffAccessMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_005
* DESCRIPTION:  1. Open DiagEOL device with different access modes
*               2. Close device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDevOpenCloseDiffAccessMode(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

	/* Open device in READWRITE mode */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Close the device */
		if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)
		{
			u32Ret = 2;
		}
	}

	enAccess = OSAL_EN_READONLY;
	/* Open device in READONLY mode */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd == OSAL_ERROR)
	{
		u32Ret += 10;
	}
	else
	{
		/* Close the device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 20;
		}
	}

   enAccess = OSAL_EN_WRITEONLY;
	/* Open device in WRITEONLY mode */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd == OSAL_ERROR)
	{
		u32Ret += 100;
	}
	else
	{
		/* Close the device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 200;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		 u32DiagEOLDevOpenCloseInvalAccessMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_006
* DESCRIPTION:  1. Attempt to Open DiagEOL device with invalid access mode
*               2. Close device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDevOpenCloseInvalAccessMode(tVoid)
{
   OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = (OSAL_tenAccess)OSAL_C_STRING_INVALID_ACCESS_MODE;
	tU32 u32Ret = 0;

	/* Open device with invalid access mode */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL,enAccess);
	if(hFd != OSAL_ERROR)
	{
		/* If successful, indicate error */
		u32Ret = 1;
		/* Close the device */
		if(OSAL_s32IOClose ( hFd ) == OSAL_ERROR)
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_007
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (DISPLAY_INTERFACE)
*               3. Close DiagEOL device
* HISTORY:		 Created by TMS on 02 July, 2007
*               Modified by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceReadOneByte(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tS32 s32NumData = 0;
	tU8 u8Data = 0;
	tU32 u32Ret = 0; 
	OSAL_trDiagEOLEntry rEOLData;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, enAccess);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		/* Close device */
		if( OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_008
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (DisplayInterface)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_009
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one whole block of data (DisplayInterface)
*               3. Close DiagEOL device
* HISTORY:		 Created by TMS on 02 July, 2007
*               Modified by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceReadWholeBlock(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0; 
	static tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_F_B_END];
	OSAL_trDiagEOLEntry rEOLData;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_010
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (DisplayInterface)
*               3. Close DiagEOL device
* HISTORY:		 Created by TMS on 02 July, 2007
*               Modified by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_F_B_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		/* Close device */
		if( OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_011
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one block of data (DisplayInterface)
*               3. Close DiagEOL device
* HISTORY:		 Created by TMS on 02 July, 2007
*               Modified by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_F_B_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_F_B_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{
			//Write is not supported in Linux

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_F_B_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_F_B_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_F_B_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_F_B_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_F_B_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_F_B_END)
			{
				/* Read Original Data Error */
			   u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_F_B_END))
			{
			 	/* Verify error */
				u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLDisplayInterfaceWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_012
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLDisplayInterfaceWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_F_B_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_F_B_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_F_B_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_013
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_014
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_015
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_016
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
			/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_017
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END)
			{
				 /* Restore not possible */
				 u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END)
			{
				 /* Read Original Data Error */
				 u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END))
			{
				 /* Verify error */
				 u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
			
		}
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBluetoothWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_018
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (Bluetooth)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBluetoothWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BLUETOOTH;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_019
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR )
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_020
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_021
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
	
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_022
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_023
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{
			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationSystemWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_024
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (NavigationSystem)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationSystemWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
   }
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_025
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
	
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_026
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_027
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/*	Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_028
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData  == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_029
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}		
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLNavigationIconWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_030
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (NavigationIcon)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLNavigationIconWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_NAVIGATION_ICON;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_031
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_032
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_033
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_034
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_035
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{
			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLHfTuningWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_036
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (HfTuning)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLHfTuningWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_HAND_FREE_TUNING;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_037
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_038
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_039
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_040
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_041
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{
			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_SYSTEM_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_SYSTEM_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_SYSTEM_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_SYSTEM_END)
			{
			 /* Restore not possible */
			 u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_SYSTEM_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END))
			{
				/* Verify error */
				u32Ret += 100;
			}
			
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLSYSTEMWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_042
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (SYSTEM)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLSYSTEMWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_SYSTEM;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
  }
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_043
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR )
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_044
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_045
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_COUNTRY_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_046
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_COUNTRY_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_047
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_COUNTRY_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_COUNTRY_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_COUNTRY_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_COUNTRY_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_COUNTRY_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_COUNTRY_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_COUNTRY_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_COUNTRY_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_COUNTRY_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device used for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLCOUNTRYWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_048
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (COUNTRY)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLCOUNTRYWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_COUNTRY_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_COUNTRY_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_COUNTRY;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_049
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_050
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_051
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BRAND_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_052
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BRAND_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_053
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_BRAND_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BRAND_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BRAND_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_BRAND_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BRAND_END)
			{
				u32Ret += 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BRAND_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BRAND_END)
			{
				/* Restore not possible */
				u32Ret += 20;				  			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_BRAND_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_BRAND_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device used for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLBRANDWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_054
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (BRAND)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLBRANDWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_BRAND_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_BRAND_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_BRAND;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}

#if 0		  //commented as not implemented yet in driver
/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCReadOneByte()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_055
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one byte of data (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCReadOneByte(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCReadInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_056
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Read one byte of data with invalid buffer (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCReadInvalBuffer(tVoid)
{
	tU8 u8Data = 0;
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 byte data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = 1;
		rEOLData.pu8EntryData = &u8Data;
		s32NumData = OSAL_s32IORead(hFd, OSAL_NULL, sizeof(rEOLData));

		/* Check for Read Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCReadWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_057
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Read one block of data (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCReadWholeBlock(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0 ;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Read 1 block of data */
		rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		/* Check for read error */	
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCReadBlockInvalidSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_058
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to read one block of data with invalid size parameter (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCReadBlockInvalidSize(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END + 1];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Attempt to read with invalid size */
		rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
		rEOLData.u16Offset = 1;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END + 1;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

	   /* IMPT!!! An invalid u16EntryLength should not result in a read error */
		if (s32NumData == OSAL_ERROR)
		{
			u32Ret = 2;
		}

		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCWriteWholeBlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_059
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Write one whole block of data (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCWriteWholeBlock(tVoid)
{
	tU8 u8Backup[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END];
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	OSAL_tIODescriptor hFdBackup = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Device re-open */
		hFdBackup = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
		if (hFdBackup == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{

			/* Make a backup of existing data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));
		   
		   /* Backup not possible */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
			{
				u32Ret = 3;
			}

			memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);

			/* Perform write oepration */
			rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IOWrite(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

		   /* Write New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
			{
				u32Ret = 4;
			}

			/* Read the newly written data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFd, (tPS8)&rEOLData, sizeof(rEOLData));

			/* Read New Data Error */
			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
			{
				u32Ret += 10;
			}

			/* Write the backup data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
			rEOLData.pu8EntryData = u8Backup;
			s32NumData = OSAL_s32IOWrite(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
			{
				/* Restore not possible */
				u32Ret += 20;
			}

			/* Read the newly written back up data */
			rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
			rEOLData.u16Offset = 0;
			rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
			rEOLData.pu8EntryData = u8Data;
			s32NumData = OSAL_s32IORead(hFdBackup, (tPS8)&rEOLData, sizeof(rEOLData));

			if (s32NumData != EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
			{
				/* Read Original Data Error */
				u32Ret += 40;
			}

			if (0 != memcmp(u8Data, u8Backup, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END))
			{
				/* Verify error */
				u32Ret += 100;
			}

			/* Close device opened for backup */
			if(OSAL_s32IOClose(hFdBackup) == OSAL_ERROR)
			{
				u32Ret += 200;
			}
		}

		/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 400;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLRVCWriteInvalBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_060
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to Write with invalid buffer (RVC)
*               3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLRVCWriteInvalBuffer(tVoid)
{
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END];
	OSAL_trDiagEOLEntry rEOLData;
	OSAL_tIODescriptor hFd = 0;
	tS32 s32NumData = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
	  	memset(u8Data, 0, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);

		/* Perform write oepration */
		rEOLData.u8Table = EOLLIB_TABLE_ID_REAR_VISION_CAMERA;
		rEOLData.u16Offset = 0;
		rEOLData.u16EntryLength = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
		rEOLData.pu8EntryData = u8Data;
		s32NumData = OSAL_s32IOWrite(hFd, OSAL_NULL, sizeof(rEOLData));

	   /* Write New Data Error */
		if (s32NumData != OSAL_ERROR)
		{
			u32Ret = 2;
		}

	  	/* Close device */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}
	return u32Ret;
}
#endif

/*****************************************************************************
* FUNCTION:		 u32DiagEOLGetModuleIdentifier()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_061
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Get Module Identifier
*               3. Close DiagEOL device
* HISTORY:		 Created by TMS on 02 July, 2007
*               Modified by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLGetModuleIdentifier(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_trDiagEOLModuleIdentifier rEOLModuleId;
	tU32 u32PartNumber = 0;
	tU8 u8DesignLevelSuffix[2];
	tS32 s32Result = 0;
	tU32 u32Ret = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Get Module Identifier */
		rEOLModuleId.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE; 
		s32Result = OSAL_s32IOControl(hFd, 
		  						OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER, 
							  (tS32)(&rEOLModuleId));

		if (s32Result != OSAL_ERROR)
		{
		  u32PartNumber = rEOLModuleId.u32PartNumber;
		  u8DesignLevelSuffix[0] = rEOLModuleId.u8DesignLevelSuffix[0];
		  u8DesignLevelSuffix[1] = rEOLModuleId.u8DesignLevelSuffix[1];
		}
		else
		{
			u32Ret = 2;
		}

		/* Device close */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		 u32DiagEOLGetModuleIdentifierInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_DiagEOL_062
* DESCRIPTION:  1. Open DiagEOL device
*				    2. Attempt to get mocule identifier with invalid parameter
*					 3. Close DiagEOL device
* HISTORY:		 Created by Shilpa Bhat (RBIN/ECM1) on 26 Feb, 2008
******************************************************************************/
tU32 u32DiagEOLGetModuleIdentifierInvalParam(tVoid)
{
	OSAL_tIODescriptor hFd = 0;
	OSAL_trDiagEOLModuleIdentifier rEOLModuleId;
	tU32 u32Ret = 0;
	tS32 s32Result = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READONLY);
	if (hFd == OSAL_ERROR)
	{
		u32Ret = 1;
	}
	else
	{
		/* Get Module Identifier */
		rEOLModuleId.u8Table = EOLLIB_TABLE_ID_DISPLAY_INTERFACE; 
		s32Result = OSAL_s32IOControl(hFd, 
		  										OSAL_C_S32_IOCTRL_DIAGEOL_INVAL_IOCTL, 
		  										(tS32)(&rEOLModuleId));
		if (s32Result != OSAL_ERROR)
		{
			u32Ret = 2;
		}

		/* Device close */
		if(OSAL_s32IOClose(hFd) == OSAL_ERROR)
		{
			u32Ret += 4;
		}		
	}

	return u32Ret;
}

/******************************* End of File ************************************/





