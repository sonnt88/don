/******************************************************************************
 * FILE         : oedt_Cryptcard_SD_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the file system test cases for Crypt card (SD-Card) 
 *                          
 * AUTHOR(s)    : Martin Langer (CM-AI/PJ-CF33)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                    | Author & comments
 * --.--.--       | Initial revision   | ------------
 * 06 Jul, 2012   | version 1.0        | Martin Langer (CM-AI/PJ-CF33)
 * 01 Jun, 2015	  | Lint Fix:CFG3-1004 | Ranga Swamy V (RBEI/ECF5)
 *------------------------------------------------------------------------------*/
 
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_Cryptcard_SD_CommonFS_TestFuncs.h"


       
/*****************************************************************************
*  SD Card Detection 
*  for sdcard devices which are available as /dev/mmcblk1 instead of /dev/sdX
******************************************************************************/

tBool isDeviceSDCard( const tU8 * strDevName )
{
    tString OEDT_VALID_CRYPTNAV_DEVICE = "/dev/root/tmp/valid_cryptcarddev";
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
    OSAL_tIODescriptor hFile;
    tS8 ps8ReadChar;

    if (u32OEDT_BoardName() != (tU32)OEDT_LCN2KAI)
    {
        return FALSE;
    }
    
    // check UUID of mmcblk1 device and look then for the corresponding cryptcard position in the mount list
    system ("MMCDEV=$(cat /proc/mounts | grep /dev/mmcblk1 | wc -l);        if [ $MMCDEV -eq '0' ]; then echo '0' > /tmp/valid_cryptcarddev; fi" );
    system ("NUMBEROFLINES=$(cat /proc/mounts | grep /dev/media | grep -v none | wc -l);   if [ $NUMBEROFLINES -eq '1' ]; then MMCDEV=$(cat /proc/mounts | grep /dev/mmcblk1 | wc -l); if [ $MMCDEV -eq '1' ]; then echo '1' > /tmp/valid_cryptcarddev; fi; fi" );
    system ("NUMBEROFLINES=$(cat /proc/mounts | grep /dev/media | grep -v none | wc -l);   if [ $NUMBEROFLINES -eq '2' ]; then MMCDEV=$(cat /proc/mounts | grep /dev/media | grep -v none | head -1 | grep /dev/mmcblk1 | wc -l); if [ $MMCDEV -eq '1' ]; then echo '1' > /tmp/valid_cryptcarddev; fi; fi" );
    system ("NUMBEROFLINES=$(cat /proc/mounts | grep /dev/media | grep -v none | wc -l);   if [ $NUMBEROFLINES -eq '2' ]; then MMCDEV=$(cat /proc/mounts | grep /dev/media | grep -v none | tail -1 | grep /dev/mmcblk1 | wc -l); if [ $MMCDEV -eq '1' ]; then echo '2' > /tmp/valid_cryptcarddev; fi; fi" );
    system ("NUMBEROFLINES=$(cat /proc/mounts | grep /dev/media | grep -v none | wc -l);   if [ $NUMBEROFLINES -gt '2' ]; then echo 'n' > /tmp/valid_cryptcarddev; fi" );

 	hFile = OSAL_IOOpen ( OEDT_VALID_CRYPTNAV_DEVICE, enAccess );
    if( hFile == OSAL_ERROR )
    {
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1, "ERROR: OSAL_IOOpen fails with errorcode '%i'", OSAL_u32ErrorCode() );
        return FALSE;
	}
  
    if (OSAL_ERROR == OSAL_s32IORead( hFile, &ps8ReadChar, 1))
    {
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ERROR: OSAL_s32IORead fails with errorcode '%i'", OSAL_u32ErrorCode() );
        OSAL_s32IOClose ( hFile );
        return FALSE;
    }

    if (OSAL_ERROR == OSAL_s32IOClose ( hFile ))
    {
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ERROR: OSAL_s32IOClose failed with errorcode '%i'", OSAL_u32ErrorCode() );
    }
    
    system ("rm -f /tmp/valid_cryptcarddev");
    
    switch ( ps8ReadChar )
    {
    case '0':
        return FALSE;
        
    case '1':
        if (OSAL_s32StringCompare( (tCString)strDevName, (tCString)OEDTTEST_C_STRING_DEVICE_CRYPTCARD ) == 0)
        {
            return TRUE;
        }
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1, "ERROR: OSAL_s32IORead read '%c', but we are using %s", ps8ReadChar, (tCString)strDevName );
        return FALSE;
        
    case '2':
        if (OSAL_s32StringCompare( (tCString)strDevName, (tCString)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2 ) == 0)
        {
            return TRUE;
        }
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1, "ERROR: OSAL_s32IORead read '%c', but we are using %s", ps8ReadChar, (tCString)strDevName );
        return FALSE;
        
    default:
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ERROR: OSAL_s32IORead read '%c', unknown problem", ps8ReadChar );
        return FALSE;
        
    case 'n':
        OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4, "ERROR: OSAL_s32IORead read '%c', too many media devices", ps8ReadChar );
        return FALSE;
    }    
}

tPS8 getCryptcardDeviceNameForSDCard(tVoid)
{ 
  	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
    OSAL_tIODescriptor hDevice;
    
    if ( isDeviceSDCard( (const tPU8) OEDTTEST_C_STRING_DEVICE_CRYPTCARD2 ))
    {
        hDevice = OSAL_IOOpen( ( tCString )OEDTTEST_C_STRING_DEVICE_CRYPTCARD2, enAccess );
        if ( hDevice != OSAL_ERROR )
        {
            OSAL_s32IOClose ( hDevice );
            return (tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
        }
    }    

    if ( isDeviceSDCard( (const tPU8) OSAL_C_STRING_DEVICE_CRYPTCARD ))
    {
        hDevice = OSAL_IOOpen( ( tCString )OSAL_C_STRING_DEVICE_CRYPTCARD, enAccess );
        if ( hDevice != OSAL_ERROR )
        {
            OSAL_s32IOClose ( hDevice );
            return (tPS8)OSAL_C_STRING_DEVICE_CRYPTCARD;
        } else {
            // a device /dev/mmcblk1 is available but it is mounted as cryptcard2 instead of cryptcard
            hDevice = OSAL_IOOpen( ( tCString )OEDTTEST_C_STRING_DEVICE_CRYPTCARD2, enAccess );
            if ( hDevice != OSAL_ERROR )
            {
                OSAL_s32IOClose ( hDevice );
                return (tPS8)OEDTTEST_C_STRING_DEVICE_CRYPTCARD2;
            }
        }        
    }
    
    OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "ERROR: no cryptcard device available" );
    return OSAL_NULL;
}
       
       
       
/*****************************************************************************
*  Helper Functions      
******************************************************************************/
       
tPS8 getCryptcardDeviceNameForSDCard_Dir1(tVoid)
{ 
   static char path_dir1 [100];

   OSAL_s32PrintFormat ( path_dir1, "%s%s", getCryptcardDeviceNameForSDCard(), "/cfg" );
   
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return OSAL_NULL;
   }
   
   return (tPS8)path_dir1;
}

tPS8 getCryptcardDeviceNameForSDCard_NonExisting(tVoid)
{ 
   static char path_nonexisting [100];

   OSAL_s32PrintFormat ( path_nonexisting, "%s%s", getCryptcardDeviceNameForSDCard(), "/Invalid" );

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return OSAL_NULL;
   }
      
   return (tPS8)path_nonexisting;
}

tPS8 getCryptcardDeviceNameForSDCard_InvalidPath(tVoid)
{ 
   static char path_invalidpath [100];

   OSAL_s32PrintFormat ( path_invalidpath, "%s%s", getCryptcardDeviceNameForSDCard(), "/Dummydir/Dummy.txt" );
   
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return OSAL_NULL;
   }
   
   return (tPS8)path_invalidpath;
}

tPS8 getCryptcardDeviceNameForSDCard_DatFile(tVoid)
{ 
   static char path_datfile [100];

   OSAL_s32PrintFormat ( path_datfile, "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return OSAL_NULL;
   }
   
   return (tPS8)path_datfile;
}

tPS8 getCryptcardDeviceNameForSDCard_Root(tVoid)
{ 
   static char path_root [100];

   OSAL_s32PrintFormat ( path_root, "%s%s", getCryptcardDeviceNameForSDCard(), "/" );
   
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return OSAL_NULL;
   }
   
   return (tPS8)path_root;
}



/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpenClosedevice( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_001
* DESCRIPTION  :   Opens and closes Cryptcard device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSOpenClosedevice(tVoid)
{ 
   return u32FSOpenClosedevice( (const tPS8)getCryptcardDeviceNameForSDCard() );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpendevInvalParm( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_002
* DESCRIPTION  :   Try to Open Cryptcard device with invalid parameters
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSOpendevInvalParm(tVoid)
{
   return u32FSOpendevInvalParm((const tPS8)getCryptcardDeviceNameForSDCard());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReOpendev( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_003
* DESCRIPTION  :   Try to Re-Open Cryptcard device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReOpendev(tVoid)
{
   return u32FSReOpendev((const tPS8)getCryptcardDeviceNameForSDCard());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpendevDiffModes( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_004
* DESCRIPTION  :   Try to Re-Open Cryptcard device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSOpendevDiffModes(tVoid)
{
   return u32FSOpendevDiffModes((const tPS8)getCryptcardDeviceNameForSDCard());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSClosedevAlreadyClosed( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_005
* DESCRIPTION  :   Try to Re-Open Cryptcard device
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard_SD_CommonFSClosedevAlreadyClosed(tVoid)
{
   return u32FSClosedevAlreadyClosed((const tPS8)getCryptcardDeviceNameForSDCard());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpenClosedir( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_006
* DESCRIPTION  :   Try to Open and closes a directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard_SD_CommonFSOpenClosedir(tVoid)
{
   return u32FSOpenClosedir((const tPS8)getCryptcardDeviceNameForSDCard_Dir1());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpendirInvalid( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_007
* DESCRIPTION  :   Try to Open invalid directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32  u32Cryptcard_SD_CommonFSOpendirInvalid(tVoid)
{
   char pathInvalid [100];
   char pathDir [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathInvalid, "%s%s", getCryptcardDeviceNameForSDCard(), "/Invalid" );
   OSAL_s32PrintFormat ( pathDir,     "%s%s", getCryptcardDeviceNameForSDCard(), "/cfg" );
   
  
   tPS8 dev_name[2] = {
                      (tPS8)pathInvalid,
                      (tPS8)pathDir
                      };
			  
   return u32FSOpendirInvalid(dev_name );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSGetDirInfo( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_008
* DESCRIPTION  :   Get directory information
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSGetDirInfo(tVoid)
{
   return u32FSGetDirInfo((const tPS8)getCryptcardDeviceNameForSDCard(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpenDirDiffModes( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_009
* DESCRIPTION  :   Try to open directory in different modes
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSOpenDirDiffModes(tVoid)
{
   tPS8 dev_name[2] = {
                      (tPS8)getCryptcardDeviceNameForSDCard(),
                      (tPS8)getCryptcardDeviceNameForSDCard_Dir1()
                      };
					  
   return u32FSOpenDirDiffModes(dev_name, TRUE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReOpenDir( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_010
* DESCRIPTION  :   Try to reopen directory
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReOpenDir(tVoid)
{
   char pathRoot [100];
   char pathDir [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathRoot, "%s%s", getCryptcardDeviceNameForSDCard(), "/" );
   OSAL_s32PrintFormat ( pathDir,  "%s%s", getCryptcardDeviceNameForSDCard(), "/cfg" );
  
   tPS8 dev_name[2] = {
                      (tPS8)pathRoot,
                      (tPS8)pathDir
                      };
			  
   return u32FSReOpenDir(dev_name, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileOpenClose( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_011
* DESCRIPTION  :   Open /close File
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileOpenClose(tVoid)
{
   return u32FSFileOpenClose((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}   


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileOpenInvalPath( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_012
* DESCRIPTION  :   Open file with invalid path name (should fail)
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileOpenInvalPath(tVoid)
{
   return u32FSFileOpenInvalPath((const tPS8)getCryptcardDeviceNameForSDCard_InvalidPath());
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileOpenInvalParam( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_013
* DESCRIPTION  :   Open a file with invalid parameters (should fail), 
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileOpenInvalParam(tVoid)
{
   return u32FSFileOpenInvalParam((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReOpen( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_014
* DESCRIPTION  :   Try to open and close the file which is already opened
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32  u32Cryptcard_SD_CommonFSFileReOpen(tVoid)
{
   return u32FSFileReOpen((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileRead( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_015
* DESCRIPTION  :   Read data from already exsisting file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileRead(tVoid)
{   
   return u32FSFileRead((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSGetPosFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_016
* DESCRIPTION  :   Get File Position from begining of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSGetPosFrmBOF(tVoid)
{
   return u32FSGetPosFrmBOF((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSGetPosFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_017
* DESCRIPTION  :   Get File Position from end of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSGetPosFrmEOF(tVoid)
{
   return u32FSGetPosFrmEOF((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_018
* DESCRIPTION  :   Read with a negative offset from BOF
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32   u32Cryptcard_SD_CommonFSFileReadNegOffsetFrmBOF()
{
   return u32FSFileReadNegOffsetFrmBOF((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_019
* DESCRIPTION  :   Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
   return u32FSFileReadOffsetBeyondEOF((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE );
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_020
* DESCRIPTION  :  Read from offset from Beginning of file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetFrmBOF(tVoid)
{   
   return u32FSFileReadOffsetFrmBOF((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_021
* DESCRIPTION  :   Read file with few offsets from EOF
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetFrmEOF(tVoid)
{   
   char pathOfDatFile [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathOfDatFile,  "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   return u32FSFileReadOffsetFrmEOF((const tPS8)pathOfDatFile, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_022
* DESCRIPTION  :   Reads file by skipping certain offsets at specified intervals.
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
   char pathOfDatFile [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathOfDatFile,  "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   return u32FSFileReadEveryNthByteFrmBOF((const tPS8)pathOfDatFile, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_023
* DESCRIPTION  :   Reads file by skipping certain offsets at specified intervals.
*                  (This is from EOF)      
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
   char pathOfDatFile [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathOfDatFile,  "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   return u32FSFileReadEveryNthByteFrmEOF((const tPS8)pathOfDatFile, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSGetFileCRC( )
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_024
* DESCRIPTION  :   Get CRC value
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011 
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSGetFileCRC(tVoid)
{
   char pathOfDatFile [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathOfDatFile,  "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   return u32FSGetFileCRC((const tPS8)pathOfDatFile, FALSE);
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadAsync()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_025
* DESCRIPTION  :   Read data asyncronously from a file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSReadAsync(tVoid)
{   
   return u32FSReadAsync((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSLargeReadAsync()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_026
* DESCRIPTION  :   Read data asyncronously from a large file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSLargeReadAsync(tVoid)
{   
   return u32FSLargeReadAsync((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);
}

   
/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileOpenCloseNonExstng()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_027
* DESCRIPTION  :   Open a non existing file 
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSFileOpenCloseNonExstng(tVoid)
{
   char buffer [50];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   OSAL_s32PrintFormat ( buffer, "%s%s", getCryptcardDeviceNameForSDCard(), "/Dummy.txt" );
   return u32FSFileOpenCloseNonExstng ( (const tPS8) buffer );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSSetFilePosDiffOff()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_028
* DESCRIPTION  :   Set file position to different offsets
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSSetFilePosDiffOff(tVoid)
{
   char pathOfDatFile [100];

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   OSAL_s32PrintFormat ( pathOfDatFile,  "%s%s", getCryptcardDeviceNameForSDCard(), "/data/data/misc/content.dat" );
   return u32FSSetFilePosDiffOff ((const tPS8)pathOfDatFile, FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileOpenDiffModes()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_029
* DESCRIPTION  :   Create file with different access modes
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSFileOpenDiffModes(tVoid)
{
   tU32 u32Ret = u32FSFileOpenDiffModes ((const tPS8)getCryptcardDeviceNameForSDCard_DatFile(), FALSE);

   // SD-cards are mounted read-only only
   if (u32Ret == 50)
   {
      u32Ret = 0;
   }   
   return u32Ret;
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSFileThroughPutFirstFile()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_030
* DESCRIPTION  :   Calculate throughput for first Cryptcard file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSFileThroughPutFile(tVoid)
{
   return u32FSFileThroughPutFirstFile((const tPS8)getCryptcardDeviceNameForSDCard_DatFile());
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSLargeFileRead()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_031
* DESCRIPTION  :   Read large file
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSLargeFileRead(tVoid)
{   
   return u32FSLargeFileRead ((const tPS8) getCryptcardDeviceNameForSDCard_DatFile(), FALSE );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSOpenCloseMultiThread()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_032
* DESCRIPTION  :   Open and close file from multiple threads
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSOpenCloseMultiThread(tVoid)
{   
   return u32FSOpenCloseMultiThread ((const tPS8) getCryptcardDeviceNameForSDCard_DatFile() );
}


/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadMultiThread()
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_033
* DESCRIPTION  :   Read from file from multiple threads
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/  
tU32 u32Cryptcard_SD_CommonFSReadMultiThread(tVoid)
{   
   return u32FSReadMultiThread ((const tPS8) getCryptcardDeviceNameForSDCard_DatFile() );
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadDirExt()
* PARAMETER    :   Device Name
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_034
* DESCRIPTION  :   Read file/directory contents 
* HISTORY      :   Created by Ravindran P(RBEI/ECF1) on 18 Aug, 2009
*                  Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported for gen2
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReadDirExt(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_ACCESS_DIR;
   OSAL_tIODescriptor hFile; 
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExtDir s32Stat = {0};
   OSAL_trIOCtrlExtDirent pDir;
          
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;/*No of entries to be read,say 1 */                          
   pDir.enFileFlags = (OSAL_tenFileFlags)0;
   OSAL_pvMemorySet(pDir.s8Name,'\0',sizeof(pDir.s8Name));   
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   hFile = OSAL_IOOpen ((tCString)getCryptcardDeviceNameForSDCard_Dir1(),enAccess );
   if( hFile != OSAL_ERROR )
   {
      if(OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOREADDIREXT, (tS32)&s32Stat) == OSAL_ERROR)
      {
         u32Ret += 10;
      }

      if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadDirExt2()
* PARAMETER    :   Device Name
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_CRYPTCARD_CFS_035
* DESCRIPTION  :   Read file/directory contents 
* HISTORY      :   Created by Ravindran P(RBEI/ECF1) on 18 Aug, 2009
*                  Apr 10, 2011 | Anooj Gopi(RBEI/ECF1) | Ported for gen2
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReadDirExt2(tVoid)
{
   OSAL_tenAccess enAccess = OSAL_EN_ACCESS_DIR;
   OSAL_tIODescriptor hFile; 
   tU32 u32Ret = 0;
   OSAL_trIOCtrlExt2Dir s32Stat = {0};
   OSAL_trIOCtrlExt2Dirent pDir;
   
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;/*No of entries to be read,say 1 */                          
   pDir.enFileFlags = (OSAL_tenFileFlags)0;
   OSAL_pvMemorySet(pDir.s8Name,'\0',sizeof(pDir.s8Name));   
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;

   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   
   hFile = OSAL_IOOpen ((tCString)getCryptcardDeviceNameForSDCard_Dir1(),enAccess );
   if( hFile != OSAL_ERROR )
   {
      if(OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, (tS32)&s32Stat) == OSAL_ERROR)
      {
         u32Ret += 10;
      }

      if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
         u32Ret += 20;
      }
   }
   else
   {
      u32Ret += 400;
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadDirExtPartByPart
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_USB_CFS_036
* DESCRIPTION  :   ReadDirExtPartByPart
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReadDirExtPartByPart(tVoid)
{
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   return u32FSReadDirExtPartByPart((tPS8)getCryptcardDeviceNameForSDCard());
}

/*****************************************************************************
* FUNCTION     :   u32Cryptcard_CommonFSReadDirExt2PartByPart
* PARAMETER    :   none
* RETURNVALUE  :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :   TU_OEDT_USB_CFS_037
* DESCRIPTION  :   ReadDirExt2PartByPart
* HISTORY      :   Ported by Anooj Gopi(RBEI/ECF1) on Apr 10, 2011
******************************************************************************/
tU32 u32Cryptcard_SD_CommonFSReadDirExt2PartByPart(tVoid)
{
   if ( getCryptcardDeviceNameForSDCard() == OSAL_NULL )
   {
      return 9001;
   }
   return u32FSReadDirExt2PartByPart((tPS8)getCryptcardDeviceNameForSDCard());
}

/* EOF */                                                   

