/******************************************************************************
*FILE         : oedt_AcousticoutLong_TestFuncs.c
*
*SW-COMPONENT : OEDT
*
*DESCRIPTION  : This file implements a long-term test case
*               Acousticout device
*
*AUTHOR       : srt2hi Softec
*
*COPYRIGHT    : (c) 2011 Bosch
*
*HISTORY:     initial release
*****************************************************************************/
#include <alsa/asoundlib.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"
#include "oedt_Display.h"
#include "oedt_Acousticout_TestFuncs.h"

#include "odt_Globals.h"
#include "odt_LinkedList.h"


#include "acousticout_public.h"
#include "acousticin_public.h"

// ----------------------------------------------------------------------------------------------
/*typedef struct rJetsetChannelData_tag
{
 unsigned short freq;
 unsigned char  sample;
 unsigned char  vol   ;
}rJetsetChannelData_type;
*/



typedef struct WaveBlock_tag
{
   int  iBlockSize;
   int   iBytesPerSample;
   int   iChannels;
   struct WaveBlock_tag  *pNext;
   unsigned char *pucBuffer;
} WaveBlock_type;



#define SAMPLE_ZAHL 10
#define SAMPLE_LEN  50
// ================================================
typedef struct JetSet_tag
{
 unsigned char     *pucComputeBuffer;
 unsigned int      uiCounter;
 unsigned int uiChannels;
 unsigned int uiBlockSize;
 unsigned int uiNumberOfJetsInWaveBuffer;
 unsigned int uiLenOfComputeBuffer;
 unsigned int uiOneJetBufferByteLen; // L�nge einer Jet-Sequenz in Byte (von Jet-Note zu Jet-Note)
 int          iKanal_3_div;       // teiler f�r kanal 3 (16 bit)

 char     cSample[SAMPLE_ZAHL][SAMPLE_LEN];  // 4 samples
 unsigned int uiSample_len[SAMPLE_ZAHL];   // die laenge dieses samples
 unsigned int uiSample_nr_mal_samlen[SAMPLE_ZAHL];

 int           iLast_sample_anzahl[4]; // zum retten in berechne sample
 unsigned int uiLast_sample[4];
 unsigned int uiLast_sample_pointer[4];// zum retten   -"-

 #define LEN_RANDOM 4096
 unsigned char  ucRandom_puffer[LEN_RANDOM];  // enthaelt random zahlen
 unsigned int   uiRnd_ptr;               // pointer auf diesen puffer

 //***********
 unsigned char  ucMute[10];  // zum stummschalten
 unsigned int   uiVolumen;          // zum volumenaednern als >>volumen

 int  iSample_rate1000_sample_len[7];
 unsigned char  bIsInit;
 unsigned int uiSamplesPerSecond;
 unsigned short  usBytePerSample;

 unsigned int uiJetsPerSecond;

 WaveBlock_type  *pFirst;
 WaveBlock_type  *pLast;

 int     iNumberOfWaveBlocks;
 int     iMaxNumberOfWaveBlocks;
} JetSet_type;


#define OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(_X_) printf _X_
#define OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(_X_) printf _X_


#define PRINTF(X) printf X
//#define PRINTF(X)

//static int iJet = 0;

//static snd_pcm_t *playback_handle;
//static snd_pcm_hw_params_t *hw_params;

static int _iAlsaSampleRate = 44100;
static int _iChannels       = 2;

//============================================================
 void JetSet_var_ini();
 void JetSet_berechne_sample(unsigned char kanal,unsigned char sample_nr,unsigned short freq,unsigned char vol);
 unsigned int JetSet_addChannels(void  *buffer, unsigned int offset);
 unsigned int JetSet_FillWaveBuffer(void  *buffer);
 unsigned char JetSet_JetReset(unsigned char setToStart);
 void JetSet_JetSet(void);
 void JetSet_DesJetSet(void);
 unsigned char JetSet_Init(unsigned int sampleRate, int iChannels);
 unsigned char JetSet_Play(OSAL_tIODescriptor hAcousticout);
 unsigned char JetSet_DeleteWaveBlock(void);
 unsigned char JetSet_Stop(void);
 unsigned char JetSet_Close(void);
 unsigned char JetSet_Mute(int channel);
 unsigned char JetSet_GetMuteState(int channel);
 unsigned char JetSet_MuteAll(void);
 unsigned char JetSet_UnMuteAll(void);
 int JetSet_SetSpeed(unsigned int jetPerSecond);

//============================================================
  WaveBlock_type * WaveBlock_class_WaveBlock_class(int iBSize, int iBPS, int iChnl);
  void WaveBlock_class_DesWaveBlock_class(WaveBlock_type *pWBT);
  void WaveBlock_class_SetNext(WaveBlock_type  *pWBT, WaveBlock_type  *pNextWBT);
  WaveBlock_type  * WaveBlock_class_GetNext(const WaveBlock_type *pWBT);
  void  * WaveBlock_class_GetDataPtr(const WaveBlock_type *pWBT);
  void WaveBlock_class_Play(OSAL_tIODescriptor hAcousticout, WaveBlock_type *pWBT, unsigned int len);

//============================================================
static JetSet_type rJetSet;





//============================================================
// Jetsetengine
unsigned int uiJetCount = 0;

#define M(adresse,offset)       (jetmem[((adresse) - 0x21fd) + (offset)])
#define LDA(adresse,offset)     (A = M((adresse),(offset)))
#define STA(adresse,offset)     (M((adresse),(offset)) = A)
#define LDY(adresse,offset)     (Y = M(adresse,offset))
//#define STY(adresse,offset)     (M(adresse,offset) = Y)
#define LSL(_R_,_N_) ((unsigned char)((unsigned int)(_R_) << (_N_)))

typedef struct F_tag
{
  unsigned char f[16];
} F_type;



unsigned char d200[15] = {0};


static F_type F;

//#define STACKSIZE 5

static unsigned char A;
static unsigned char X;
static unsigned char Y;

static unsigned char *pucF0;    //pointer aus *getMem(0x234c und *getMem(0x234f, puc234c[]
static unsigned char *pucF2;    // 2352+236e, puc2352[]
static unsigned char *puc2134;  // xm2134


extern unsigned char jetmem[];
extern unsigned char *puc234c[];
extern unsigned char *puc2352[];
extern unsigned char *puc22aa[];


void m2971();

#define BASE_FREQ_8      32000
#define BASE_FREQ_16     119000

static unsigned short usFreq[3];
static unsigned short usAtariFreq[3];
static unsigned char ucSample[3];
static unsigned char ucVol[3];


void tonausgabe()
{
    int i;
    unsigned short s,f,v;

    //-- kanal 0 8 bit ----------------------------------------------------
    //kanal1 (8 bit) d204 = freq, d205&0xf0 = Sample, d205&0x0f = volumen
    i = 0;
    usAtariFreq[i] = d200[4];
    ucVol[i]    = d200[5] & 0x0f;
    ucSample[i] = (d200[5] & 0xf0) >> 4;
    f= usAtariFreq[i] == 0 ? 0 : BASE_FREQ_8 / usAtariFreq[i];
    switch (ucSample[i])
     {
      case 0x0a:
                s = 0;
                v= ucVol[i]; //at->av1&mute[1];
                if(usAtariFreq[i] <= 7)
                 {
                   s=2;
                   v=ucVol[i]; //at->av1 &= mute[7];
                   //at->af1 &= mute[7];
                 }
                //at->av1&=mute[1];
                //at->af1 &= mute[1];
        break;
      case 0x08:
                s = 3;
                v= ucVol[i]; //v=at->av1&=mute[7];
                if(usAtariFreq[i] <= 7) //if(at->af1<=7)
                 {
                   s=2;
                 }
                //at->af1 &= mute[7];
        break;
      default:
        s=0;
        v=0;
     }
    ucSample[i] = (unsigned char)s;
    usFreq[i] = f;
    //berechne_sample(i,s,f,v);

    //-- kanal 1 8 bit ----------------------------------------------------
    //kanal1 (8 bit) d206 = freq, d207&0xf0 = Sample, d207&0x0f = volumen
    i = 1;
    usAtariFreq[i] = d200[6];
    ucVol[i]    = d200[7] & 0x0f;
    ucSample[i] = (d200[7] & 0xf0) >> 4;
    f= usAtariFreq[i] == 0 ? 0 : BASE_FREQ_8 / usAtariFreq[i];
    switch (ucSample[i])
     {
      case 0x0a:
                s = 4;
                v = ucVol[i]; //at->av1&mute[1];
                if(usAtariFreq[i] <= 7)
                 {
                   s=2;
                   v=ucVol[i]; //at->av1 &= mute[7];
                   //at->af1 &= mute[7];
                 }
                //at->av1&=mute[1];
                //at->af1 &= mute[1];
        break;
      case 0x08:
                s = 3;
                v= ucVol[i]; //v=at->av1&=mute[7];
                if(usAtariFreq[i] == 0) //if(at->af1<=7)
                 {
                   s=2;
                 }
                //at->af1 &= mute[7];
        break;
      default:
        s=4;
        v=0;
     }
    ucSample[i] = (unsigned char)s;
    usFreq[i] = f;
    //berechne_sample(i,s,f,v);


    //-- kanal 3 16 bit ----------------------------------------------------
    //kanal2 (16 bit) d200+d202 = freq, d203&0xf0 = Sample, d203&0x0f = volumen
    i = 2;
    usAtariFreq[i] = ((unsigned short)d200[0]) | ((unsigned short)d200[2] << 8);
    ucVol[i]    = d200[3] & 0x0f;
    ucSample[i] = (d200[3] & 0xf0) >> 4;
    f= (unsigned short)(BASE_FREQ_16 / usAtariFreq[i]);
    switch (ucSample[i])
     {
      case 0x0c:
                v=ucVol[i]; //at->av3 &= mute[3];
                f=(unsigned short)((long)BASE_FREQ_16/((long)usAtariFreq[i]+1));
                s=5;

        break;
      case 0x0a:
                f=(unsigned short)((long)886738/((long)usAtariFreq[i] + 1));
                v=ucVol[i]; //at->av3 &= mute[6];
                s=1;   // auch rechteck aber mit nicht vorherberechneten freq

        break;
      case 0x08:
                f=1; //(unsigned)((long)886738/((long)at->af3+7));
                v=ucVol[i]; //at->av3 &= mute[9];
                s=6;  // 400 byte sample nur f�r FREQ 0x127

        break;
      default:
        s=5;
        v=0;

     }
    ucSample[i] = (unsigned char)s;
    usFreq[i] = f != 0 ? f : 1;
    //berechne_sample(i,s,f,v); // kanal sample, frequenz,volumen
    //------------------------------------------------------



    PRINTF(("Jet %07u: ", (unsigned int)uiJetCount));
    uiJetCount++;

    i = 0;
    PRINTF(("Ch%d %04X %02X %02X", i, (unsigned int)usFreq[i], (unsigned int)ucSample[i], (unsigned int)ucVol[i]));
    i = 1;
    PRINTF((" - Ch%d %04X %02X %02X", i, (unsigned int)usFreq[i], (unsigned int)ucSample[i], (unsigned int)ucVol[i]));
    i = 2;
    PRINTF((" - Ch%d %04X %02X %02X\n", i, (unsigned int)usFreq[i], (unsigned int)ucSample[i], (unsigned int)ucVol[i]));

    (void)v;

}


int jetmain()
{
    //iJet++;

    if((M(0x2297,0) & 0x80) != 0x00)
    {
         if((M(0x2297,0) & 0x40) == 0x00)
             return 0;

         memset(d200, 0, 8);
         M(0x2297,0) = 0x80;
         return 1;
    }

    if((M(0x2297, 0) & 0x40) != 0x00)
    {
         memset(&M(0x2274,0), 0, 0x0B + 1);
         M(0x2297,0) = 0;
		 printf("********** RESTART *********** Jet: %u\n", uiJetCount);
    }


        M(0x2273,0)++;
        X = 0x02;
        do //while((X & 0x80) == 0x00);
        {
             F.f[4 + 0] = X;
             F.f[5 + 0] = LSL((X+1),1);
             pucF0 = puc234c[X];
             LDA(0x2277,X);
             if(A == 0x00)
             {
                while((Y = pucF0[M(0x2274,X)]) == 0xFF)
                {
                         M(0x2277,X) = 0;
                         M(0x2274,X) = 0;
                         M(0x227A,X) = 0;
                }
                pucF2 = puc2352[Y];
                M(0x228c,X) = 0x00;

                while(LDY(0x227A,X), A = pucF2[Y], (A & 0x80) != 0x00)
                {
                         Y = LSL(A,2);
                         M(0x2283,X) = M(0x2310,Y);
                         M(0x2289,X) = M(0x2311,Y);
                         F.f[0x0C + X]     = M(0x2312,Y);
                         M(0x2293,X) = M(0x2313,Y);
                         M(0x227A,X)++;
                }

                 X = A & 0x1F;
                 LDA(0x21FD, X);  /*lint !e778*/
                 X = F.f[4 + 0];
                 STA(0x2277,X);
                 Y++;
                 M(0x227A,X) = Y;
                 A = pucF2[Y];

                if(A >= 0x40)
                {
                     M(0x228C, X) = A & 0xC0;
                     Y++;
                     M(0x227A,X)++;
                     M(0x2280,X) = pucF2[Y] & 0x0F;
                     M(0x2286,X) = pucF2[Y] >> 4;
                     A &= 0x3F;
                } //if(A >= 0x40)

                STA(0x227D,X);
                if(X == 0x00)
                {
                     M(0x2298,0) = M(0x2233, LSL(A,1));
                     M(0x229A,0) = M(0x2234, LSL(A,1));
                }
                else //if(X == 0x00)
                {
                     LDA(0x2205,A);             //LDA M2205,Y
                     F.f[6 + X] = A;               //STA $F6,X
                     STA(0x2298, F.f[5 + 0]);            //STA M2298,Y
                } //else //if(X == 0x00)

                 F.f[9 + X] = 0x00;               //STA $F9,X
                 M(0x227A,X)++;             //INC M227A,X
                 A = pucF2[M(0x227A,X)];              //LDA ($F2),Y

                if(A == 0xFF)
                {
                    M(0x227A,X) = 0x00;             //STA M227A,X
                    M(0x2274,X)++;             //INC M2274,X
                } //if(A == 0xFF)
             } //if(A == 0x00)

             LDA(0x2283,X);             //LDA M2283,X
             X = A;                     //TAX
             puc2134 = puc22aa[X];
             X = F.f[4 + 0];               //LDX $F4
             A = puc2134[F.f[9 + X]];            //LDA M22BA,Y

             if((A != 0xFF) && (A >= 0x7F))
             {
                 Y = F.f[5 + 0];
                 M(0x2298,Y) = 0;
                 STA(0x2299,Y);
                 F.f[9 + X]++;
             }
             else //if(((A - 0xFF) & 0x80) != 0x00) goto M21E8;                //JMP M21E8
             {
                if(A != 0xFF)
                {
                    Y = F.f[5 + 0];
                    F.f[9 + X]++;
                    A |= M(0x2289,X);
                    STA(0x2299,Y);

                    if(X != 0x00)
                    {
                        A = F.f[6 + X];               //LDA $F6,X
                        STA(0x2298,Y);             //STA M2298,Y
                    } //if(X != 0x00)
                }

                if(F.f[0x0c + X] != 0x00)
                {
                    if((M(0x2273,0) & 0x01) == 0x00)
                    {
                      M(0x2298, F.f[5 + 0]) = M(0x2205, M(0x227D,X) - F.f[0x0c + X]);
                    }
                    else //if(A == 0x00)
                    {
                      M(0x2298, F.f[5 + 0]) = M(0x2205, M(0x227D,X));
                    } //if(A == 0x00)
                } //if(A != 0x00)

                LDA(0x2293,X);             //LDA M2293,X
                if(A != 0x00)
                {
                    if((A & 0x80) == 0x00)
                    {
                         while(LDA(0x22A1, M(0x2290,X)), M(0x2290,X)++, A == 0x1F)
                         {
                            M(0x2290,X) = 0x00;
                         }
                         M(0x2298,F.f[5 + 0]) = A + F.f[6 + X];   //STA M2298,Y
                    }
                    else // //if((A & 0x80) == 0x00) goto M21BD;                //JMP M21BD
                    {
                         M(0x2298,F.f[5 + 0]) = F.f[6 + X];               //LDA $F6,X
                         F.f[6 + X]+=2;                 //INC $F6,X
                    } //else // //if((A & 0x80) == 0x00) goto M21BD;                //JMP M21BD
                } //if(A != 0x00)

                LDA(0x228C,X);             //LDA M228C,X
                if(A != 0x00)
                {
                    LDA(0x2286,X);             //LDA M2286,X
                    if(A != 0x00)
                    {
                        M(0x2286,X)--;             //DEC M2286,X
                    }
                    else //goto M21E8;                //JMP M21E8
                    {
                        LDA(0x228C,X);             //LDA M228C,X
                        if((A &= 0x80) != 0x00)
                        {
                         A = F.f[6 + X] + M(0x2280,X); //LDA $F6,X
                        }
                        else                            //CLC
                        {
                         A = F.f[6 + X] - M(0x2280,X);               //LDA $F6,X
                        }
                        F.f[6 + X] = A;               //STA $F6,X
                        STA(0x2298,F.f[5 + 0]);     //STA M2298,Y
                    } //else if(A != 0x00)
                } //if (A == 0x00)
             } //else //if(((A - 0xFF) & 0x80) != 0x00) goto M21E8;                //JMP M21E8

            M(0x2277,X)--;             //DEC M2277,X
            X--;                       //DEX
        }
        while((X & 0x80) == 0x00);

        memcpy(d200, &M(0x2298,0), 9);
        tonausgabe();
        return 2;                  //RTS
}

void m2962()
{
    A = 0x00;   //LDA #$00
    X = 0x0E;   //LDX #$0E
    do
    {
	 F.f[0 + X] = A;    //STA $F0,X
	 X--;            //DEX
	}
	while((X & 0x80) == 0x00);

    A = 0x40;       //LDA #$40
    STA(0x2297,0);  //STA M2297
                    //RTS
}

void m2971()
{
    A = 0xC0;   //    LDA #$C0
    STA(0x2297,0);   //JMP M296D
}




//============================================================
WaveBlock_type * WaveBlock_class_WaveBlock_class(int iBSize, int iBPS, int iChnl)
{
 WaveBlock_type *pWBT;

 pWBT = (WaveBlock_type*)malloc(sizeof(WaveBlock_type));
 if(NULL != pWBT)
 {
	 pWBT->iBlockSize      = iBSize;
	 pWBT->iBytesPerSample = iBPS;
	 pWBT->iChannels       = iChnl;
	 pWBT->pNext           = NULL;
	 pWBT->pucBuffer       = (unsigned char*)malloc((unsigned int)iBSize);
 } //if(NULL != pWBT)
 return pWBT;
}

//============================================================
void WaveBlock_class_DesWaveBlock_class(WaveBlock_type *pWBT)
{
  if(pWBT != NULL)
  {
    if(pWBT->pucBuffer != NULL)
    {
      free(pWBT->pucBuffer);
      pWBT->pucBuffer = NULL;
    }
    free(pWBT);
  } // if(pWBT != NULL)
  //pWBT = NULL;
}

//============================================================
  void WaveBlock_class_SetNext(WaveBlock_type  *pWBT, WaveBlock_type  *pNextWBT)
{
 pWBT->pNext = pNextWBT;
}

//============================================================
WaveBlock_type  * WaveBlock_class_GetNext(const WaveBlock_type *pWBT)
{
 return pWBT->pNext;
}

//============================================================
void  * WaveBlock_class_GetDataPtr(const WaveBlock_type *pWBT)
{
  return (void  *)(pWBT->pucBuffer);
}

//============================================================
void WaveBlock_class_Play(OSAL_tIODescriptor hAcousticout, WaveBlock_type *pWBT, unsigned int len)
{
	  tS32 s32Ret;

	  PRINTF(("WaveBlock_class_Play: pWBT %p,  len %5u\n", pWBT, len));

	  s32Ret = OSAL_s32IOWrite(hAcousticout, WaveBlock_class_GetDataPtr(pWBT), (tU32)len);
	  if(OSAL_ERROR == s32Ret)
	  {
		  OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
										"ERROR Aout Write bytes (%u) "
										"\n", 
										(unsigned int)len));
		  //u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_WRITE_RESULT_ERROR_BIT;
	  }
	  else
	  {
		  //OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n", (unsigned int)iRead));
	  }
}


void JetSet_JetSet()
{
  rJetSet.uiSample_len[0]=2;
  rJetSet.uiSample_len[1]=2;
  rJetSet.uiSample_len[2]=2;
  rJetSet.uiSample_len[3]=2;
  rJetSet.uiSample_len[4]=2;
  rJetSet.uiSample_len[5]=15;
  rJetSet.uiSample_len[6]=2;
  rJetSet.uiSample_len[7]=2;
  rJetSet.uiSample_len[8]=2;
  rJetSet.uiSample_len[9]=2;
  memcpy(rJetSet.cSample[0],"\x0\x15",rJetSet.uiSample_len[0]);
  memcpy(rJetSet.cSample[1],"\x0\x15",rJetSet.uiSample_len[1]);
  memcpy(rJetSet.cSample[2],"\x0\x15",rJetSet.uiSample_len[2]);
  memcpy(rJetSet.cSample[3],"\x0\x15",rJetSet.uiSample_len[3]);
  memcpy(rJetSet.cSample[4],"\x0\x15",rJetSet.uiSample_len[4]);
  memcpy(rJetSet.cSample[5],"\x0\x0\x0\x0\x1a\x5\x12\x8\x7\x13\x12\x3\xe\xd\xc",rJetSet.uiSample_len[5]);
  memcpy(rJetSet.cSample[6],"\x0\x15",rJetSet.uiSample_len[6]);
  memcpy(rJetSet.cSample[7],"\x0\x15",rJetSet.uiSample_len[7]);
  memcpy(rJetSet.cSample[8],"\x0\x15",rJetSet.uiSample_len[8]);
  memcpy(rJetSet.cSample[9],"\x0\x15",rJetSet.uiSample_len[9]);

  rJetSet.iLast_sample_anzahl[0]=0;
  rJetSet.iLast_sample_anzahl[1]=0;
  rJetSet.iLast_sample_anzahl[2]=0;
  rJetSet.iLast_sample_anzahl[3]=0;
  rJetSet.uiLast_sample[0]=0;
  rJetSet.uiLast_sample[1]=0;
  rJetSet.uiLast_sample[2]=0;
  rJetSet.uiLast_sample[3]=0;
  rJetSet.uiLast_sample_pointer[0]=0;// zum retten   -"-
  rJetSet.uiLast_sample_pointer[1]=0;// zum retten   -"-
  rJetSet.uiLast_sample_pointer[2]=0;// zum retten   -"-
  rJetSet.uiLast_sample_pointer[3]=0;// zum retten   -"-

  JetSet_UnMuteAll();

  rJetSet.uiCounter=0;
  rJetSet.uiChannels=0;
  rJetSet.uiNumberOfJetsInWaveBuffer=0;
  rJetSet.uiLenOfComputeBuffer  = 0;
  rJetSet.uiOneJetBufferByteLen = 0; // L�nge einer Jet-Sequenz in Byte (von Jet-Note zu Jet-Note)
  rJetSet.iKanal_3_div=119000L;       // teiler f�r kanal 3 (16 bit)
  rJetSet.pucComputeBuffer = (unsigned char*)malloc(65500);
  rJetSet.uiRnd_ptr=0;               // pointer auf diesen puffer
  rJetSet.uiVolumen=0;			// zum volumenaednern als >>volumen

  rJetSet.bIsInit = FALSE;
  rJetSet.uiSamplesPerSecond = 0;
  rJetSet.uiChannels         = 0;
  rJetSet.usBytePerSample    = 0;
  rJetSet.uiBlockSize        = 60000U;
  rJetSet.pFirst = NULL;
  rJetSet.pLast  = NULL;
  rJetSet.iNumberOfWaveBlocks = 0;
  rJetSet.iMaxNumberOfWaveBlocks = 4; // x Bloecke im Voraus berechnen

}

//==============================================
void JetSet_DesJetSet()
{
 JetSet_Close();
 free(rJetSet.pucComputeBuffer);
}

//==============================================
unsigned char JetSet_Init(unsigned int sampleRate, int channels)
{
  JetSet_Close();

  if(sampleRate == 0)
    rJetSet.uiSamplesPerSecond = (unsigned int)_iAlsaSampleRate;
  else
    rJetSet.uiSamplesPerSecond = sampleRate;

  if(channels == 0)
    rJetSet.uiChannels = (unsigned int)_iChannels;
  else
    rJetSet.uiChannels = (unsigned int)channels;

  rJetSet.usBytePerSample = 2;
  JetSet_JetReset( /*unsigned char setToStart*/FALSE);
  JetSet_UnMuteAll();
  rJetSet.bIsInit = TRUE;
  return TRUE;
}

//==============================================
unsigned char JetSet_Play(OSAL_tIODescriptor hAcousticout)
{
 WaveBlock_type *pWBT;

 if(!rJetSet.bIsInit)
  return FALSE;

 if(rJetSet.iNumberOfWaveBlocks >= rJetSet.iMaxNumberOfWaveBlocks)
  return FALSE;

  pWBT = WaveBlock_class_WaveBlock_class((int)rJetSet.uiBlockSize, (int)rJetSet.usBytePerSample, (int)rJetSet.uiChannels);
  WaveBlock_class_Play(hAcousticout, pWBT, JetSet_FillWaveBuffer(WaveBlock_class_GetDataPtr(pWBT)));
  rJetSet.iNumberOfWaveBlocks++;

  PRINTF(("JetSet_Play: iNumberOfWaveBlocks %d\n", rJetSet.iNumberOfWaveBlocks));

  if(rJetSet.pFirst == NULL)
  {
    rJetSet.pFirst = pWBT;
  }
  else
  {
    WaveBlock_class_SetNext(rJetSet.pLast, pWBT);
  }
  rJetSet.pLast = pWBT;

 return TRUE;
}

//==============================================
unsigned char JetSet_DeleteWaveBlock()
{
 WaveBlock_type *pWBT;

 if(rJetSet.pFirst!=NULL)
 {
   rJetSet.iNumberOfWaveBlocks--;
   pWBT   = rJetSet.pFirst;  // retten
   rJetSet.pFirst = WaveBlock_class_GetNext(rJetSet.pFirst); // neues first
   WaveBlock_class_DesWaveBlock_class(pWBT);    // altes, gespieltes first, loeschen
 }
 else
 {
  rJetSet.pLast=NULL;
  return FALSE;
 }

 return TRUE;
}

//==============================================
unsigned char JetSet_Stop()
{
 unsigned char ok=FALSE;
 return ok;
}

//==============================================
unsigned char JetSet_Mute(int channel)
{
 rJetSet.ucMute[channel] ^= 0xff;  // zum stummschalten
 return rJetSet.ucMute[channel] == 0;
}

//==============================================
unsigned char JetSet_GetMuteState(int channel)
{
 return rJetSet.ucMute[channel] == 0;
}

//==============================================
unsigned char JetSet_MuteAll()
{
  int i;
  for( i = 0; i< 10 ; i++)
    rJetSet.ucMute[i] = 0;  // zum stummschalten
 return TRUE;
}

//==============================================
unsigned char JetSet_UnMuteAll()
{
  int i;
  for( i = 0; i< 10 ; i++)
    rJetSet.ucMute[i] = 0xff;  // zum stummschalten
  return TRUE;
}


//==============================================
unsigned char JetSet_Close()
{
 rJetSet.bIsInit = FALSE;
 while(JetSet_DeleteWaveBlock())
	 {
	    
	 };  //keinen neuen Block erzeugen
 return FALSE;
}

//==============================================
int JetSet_SetSpeed(unsigned int jetsPerSecond)
{
 rJetSet.uiJetsPerSecond = jetsPerSecond;
 if(rJetSet.uiJetsPerSecond < 5)
  rJetSet.uiJetsPerSecond = 5;

 if(rJetSet.uiJetsPerSecond > 100)
  rJetSet.uiJetsPerSecond = 100;

 //_PostMessage(_hWnd, WM_USER + 3 , rJetSet.uiJetsPerSecond, rJetSet.uiJetsPerSecond);
 return (int)rJetSet.uiJetsPerSecond;
}

/*******************************************************/
void JetSet_var_ini()
{
 int i;

 for(i=0;i<=6;i++)
  {
   if(rJetSet.uiSample_len[i]!=0)
   {
     rJetSet.iSample_rate1000_sample_len[i] = (long)rJetSet.uiSamplesPerSecond * 128L / (long)rJetSet.uiSample_len[i]; // so oft/100 mu� jedes byte des samples wiederholt werden
   }
  }

 for(i=0;i<SAMPLE_ZAHL;i++)
  {
    rJetSet.uiSample_nr_mal_samlen[i]= (unsigned int)(i*SAMPLE_LEN);
  }

 for(i=0;i<LEN_RANDOM;i++)
  {
   rJetSet.ucRandom_puffer[i]= (unsigned char)(rand()%21);
  }
}

//----------------------------------------------
//****************************************************
//----------------------------------------------
//----------------------------------------------
//************************************************
void JetSet_berechne_sample(unsigned char kanal,unsigned char sample_nr,unsigned short freq,unsigned char vol)
{
 unsigned char v;
 unsigned char uc1,uc2;
 unsigned int s_ptr,h; //,i,im;
 int anzahl,l;
 unsigned int s_len;
 unsigned char *ptr1,*ptr2,byte;

 v = vol & rJetSet.ucMute[sample_nr]; //>>volumen;

 ptr1= &rJetSet.pucComputeBuffer[kanal*rJetSet.uiLenOfComputeBuffer];

 switch(sample_nr)
  {
   case 2:
   case 6:
	  for(h=0;h<rJetSet.uiLenOfComputeBuffer;h++)
	  {
		 uc1=rJetSet.ucRandom_puffer[rJetSet.uiRnd_ptr];
		 uc2 = v & 0x01 ?  uc1 : 0;
		 uc1<<=1;
		 if(v&0x02)
		   uc2+=uc1;
		 uc1<<=1;
		 if(v&0x04)
		  uc2+=uc1;
		 uc1<<=1;
		 if(v&0x08)
		  uc2+=uc1;
		 ptr1[h] = uc2;

	
		 if ((rJetSet.uiRnd_ptr++)>LEN_RANDOM)
		  rJetSet.uiRnd_ptr=0;
 	 }
	 break;

   case 3:  // pseudo sample

     // Frequenz und Volumen etwas von Hand nachbessern ...
     //if(freq==0) freq = 100;
	 l=anzahl=((int)rJetSet.iSample_rate1000_sample_len[3]/(int)(freq)); // so oft/100 mu� jedes byte des samples wiederholt werden
 	 byte = (unsigned char)(rJetSet.ucRandom_puffer[rJetSet.uiRnd_ptr++] * v);
   if (rJetSet.uiRnd_ptr>LEN_RANDOM)
    rJetSet.uiRnd_ptr=0;


   v*=8;
	 for(h=0;h<rJetSet.uiLenOfComputeBuffer;h++)
	  {
	   ptr1[h]=byte;
	   l-=128;
     if(l<=0)
	   {
	     l += anzahl;
      	 byte = (unsigned char)(rJetSet.ucRandom_puffer[rJetSet.uiRnd_ptr] * v);
         if (++rJetSet.uiRnd_ptr>LEN_RANDOM)
            rJetSet.uiRnd_ptr=0;
	   }
	  }
	 break;

   default:
     if(rJetSet.uiLast_sample[kanal]==sample_nr)
      {
       l     = rJetSet.iLast_sample_anzahl[kanal];
       s_ptr = rJetSet.uiLast_sample_pointer[kanal];
      }
     else
      {
       l = s_ptr = 0;
      }
      //if(freq==0) freq = 100;
  	 anzahl=((int)rJetSet.iSample_rate1000_sample_len[sample_nr]/ (int)freq); // so oft/100 muss jedes byte des samples wiederholt werden
     ptr2=(unsigned char *)((unsigned char *)rJetSet.cSample + rJetSet.uiSample_nr_mal_samlen[sample_nr]);
     s_len=rJetSet.uiSample_len[sample_nr];
     byte= (unsigned char)(ptr2[s_ptr]*v);

     for(h=0;h<rJetSet.uiLenOfComputeBuffer;h++)
     {
      ptr1[h]=byte;
      if((l-=128)<=0)
      {
        l+=anzahl;
        if(++s_ptr >= s_len)
        {
         s_ptr=0;
        }
        byte=(unsigned char)(ptr2[s_ptr]*v);
      }
     }

    rJetSet.iLast_sample_anzahl[kanal]  = l+128;
    rJetSet.uiLast_sample_pointer[kanal] = s_ptr;
    rJetSet.uiLast_sample[kanal]         = sample_nr;
   } // switch
}

//----------------------------------------------
unsigned int JetSet_addChannels(void  *buffer, unsigned int offset)
{
 unsigned int i;
 int iDestIndex;
 unsigned int destCounter;
 unsigned char *ch1,*ch2,*ch3;
 unsigned int uiB;
 unsigned char  *pucBuffer = (unsigned char*)buffer;
 //unsigned int   *puiBuffer = (unsigned int*)buffer;
 unsigned short *pusBuffer = (unsigned short*)buffer;
 //int iBytesPerFrame;

 ch1=(unsigned char *)((unsigned char *)rJetSet.pucComputeBuffer + rJetSet.uiLenOfComputeBuffer);
 ch2=(unsigned char *)((unsigned char *)ch1             + rJetSet.uiLenOfComputeBuffer);
 ch3=(unsigned char *)((unsigned char *)ch2             + rJetSet.uiLenOfComputeBuffer);

 //iBytesPerFrame = rJetSet.uiChannels * rJetSet.usBytePerSample;

 destCounter  = 0;
 // Anzahl "Samples" bzw JETs im Computebuffer

 // 8-bit mono
 if(rJetSet.uiChannels == 1 && rJetSet.usBytePerSample == 1)
  for(i=0;i<rJetSet.uiLenOfComputeBuffer;i+=1)
   {
    uiB = ch1[i]/3 + ch2[i]/3 + ch3[i]/3;
//    fwrite((const void *)&b, 1, 1, stream);
     pucBuffer[destCounter + offset]  =  (unsigned char)uiB;
     destCounter += rJetSet.usBytePerSample;
   }

 // 8-bit stereo
  if(rJetSet.uiChannels == 2 && rJetSet.usBytePerSample == 1)
    for(i=0;i<rJetSet.uiLenOfComputeBuffer;i+=1)
     {
        uiB = (unsigned int)ch1[i] * 3 / 6 + (unsigned int)ch2[i]*1 / 6 + (unsigned int)ch3[i] * 2 / 6;
        pucBuffer[destCounter + offset]  =  (unsigned char)uiB;
        destCounter += rJetSet.usBytePerSample;

        uiB = (unsigned int)ch1[i] * 1 / 6 + (unsigned int)ch2[i] *3 / 6 + (unsigned int)ch3[i] *2 / 6;
        pucBuffer[destCounter + offset]  =  (unsigned char)uiB;
        destCounter += rJetSet.usBytePerSample;
     }

 // 16-bit Mono
 if(rJetSet.uiChannels == 1 && rJetSet.usBytePerSample == 2)
  for(i=0;i<rJetSet.uiLenOfComputeBuffer;i+=1)
  {
	 iDestIndex = (int)((destCounter + offset) / rJetSet.usBytePerSample);
     pusBuffer[iDestIndex]  = ((unsigned short)ch1[i] + (unsigned short)ch2[i] + (unsigned short)ch3[i]) * 40;
     destCounter += rJetSet.usBytePerSample;
  }

 // 16-bit Stereo
 if(rJetSet.uiChannels == 2 && rJetSet.usBytePerSample == 2)
  for(i=0;i<rJetSet.uiLenOfComputeBuffer;i++)
  {
	 iDestIndex = (int)((destCounter + offset) / rJetSet.usBytePerSample);
     pusBuffer[iDestIndex]  = (unsigned short)((((unsigned short)ch1[i] * 5) / 4 /* *2*/ + (unsigned short)ch2[i] + (unsigned short)ch3[i]) * 40); //16;

	 destCounter += rJetSet.usBytePerSample;

	 iDestIndex = (int)((destCounter + offset) / rJetSet.usBytePerSample);
     pusBuffer[iDestIndex]  = (unsigned short)(((unsigned short)ch1[i] + ((unsigned short)ch2[i] * 5) / 4 /* *2*/ + (unsigned short)ch3[i]) * 40); //16;

	 destCounter += rJetSet.usBytePerSample;
  }
 return destCounter;
}


// gibt die tatsaechliche L�nge zur�ck
unsigned int JetSet_FillWaveBuffer(void  *buffer)
{
 unsigned short f;
 unsigned char v,s;
 unsigned int i;
 unsigned int len;
 unsigned int tmp;

 // Bufferlaenge bestimmen
  // Zuerst Anzahl Samples fuer einen JET (ganzzahlig durch 4 teilbar)
  tmp   = rJetSet.uiSamplesPerSecond / rJetSet.uiJetsPerSecond;
  tmp >>= 2; // durch 4
  tmp <<= 2; // mal 4

  rJetSet.uiLenOfComputeBuffer  = tmp;
  rJetSet.uiOneJetBufferByteLen =  tmp * rJetSet.uiChannels * rJetSet.usBytePerSample; // L�nge einer Jet-Sequenz in Byte (von Jet-Note zu Jet-Note)
  rJetSet.uiNumberOfJetsInWaveBuffer = rJetSet.uiBlockSize / rJetSet.uiOneJetBufferByteLen; // soviele Jets passen in den gerade belegten WaveBuffer


 len = 0; // Bytes (!) im WavePuffer
 for(i = 1; i <= rJetSet.uiNumberOfJetsInWaveBuffer; i++)
 {
   	jetmain();


    //-- kanal 1 8 bit ----------------------------------------------------
    //f=frequenz_8bit[at->af1];
    s = ucSample[0];
    f = usFreq[0];
    v = ucVol[0];
    JetSet_berechne_sample(1,s,f,v);
    //------------------------------------------------------
    //-- kanal 2 8 bit ----------------------------------------------------
    //f=32000/((unsigned)at->af2+1);
    s = ucSample[1];
    f = usFreq[1];
    v = ucVol[1];
    JetSet_berechne_sample(2,s,f,v); // kanal sample, frequenz,volumen

    s =  ucSample[2];
    f = usFreq[2];
    v = ucVol[2];
    JetSet_berechne_sample(3,s,f,v); // kanal sample, frequenz,volumen
    //------------------------------------------------------
    rJetSet.uiCounter++;
   len += (unsigned int)JetSet_addChannels(buffer, len);
 } // for
 return len;
}
//----------------------------------------------

/***************************************************************************/
unsigned char JetSet_JetReset(unsigned char setToStart)
{
 JetSet_SetSpeed(40);        //etwaige Anzahl Aufrufe von JetSet je Sekunde, aus dieser wird die Bufferl�nge eines JETs berechnet
 if(setToStart)
   rJetSet.uiCounter        = 0;               // jet-Counter (Noten)
 JetSet_var_ini();
//  stream = fopen("g:\\jett.pcm","wb+");
 return TRUE;
}


/*****************************************************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/************************* TEST 001 ******************************************/
/*****************************************************************************/

#define OEDT_ACOUSTICOUT_LONG_T001_DEVICE_NAME \
                 OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH "/OedtWithRate"
//#define OEDT_ACOUSTICOUT_LONG_T001_SAMPLE_RATE                  22050
//#define OEDT_ACOUSTICOUT_LONG_T001_CHANNELS                     2
#define OEDT_ACOUSTICOUT_LONG_T001_BUFFERSIZE                   (2048)
//#define OEDT_ACOUSTICOUT_LONG_T001_BYTES_PER_SAMPLE             (OEDT_ACOUSTICOUT_LONG_T001_CHANNELS * 2)

#define OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE                   0x00000000
#define OEDT_ACOUSTICOUT_LONG_T001_OPEN_OUT_RESULT_ERROR_BIT         0x00000001
#define OEDT_ACOUSTICOUT_LONG_T001_REG_NOTIFICATION_RESULT_ERROR_BIT 0x00000002
#define OEDT_ACOUSTICOUT_LONG_T001_SETSAMPLERATE_RESULT_ERROR_BIT    0x00000004
#define OEDT_ACOUSTICOUT_LONG_T001_SETCHANNELS_RESULT_ERROR_BIT      0x00000008
#define OEDT_ACOUSTICOUT_LONG_T001_SETSAMPLEFORMAT_RESULT_ERROR_BIT  0x00000010
#define OEDT_ACOUSTICOUT_LONG_T001_SETBUFFERSIZE_RESULT_ERROR_BIT    0x00000020
#define OEDT_ACOUSTICOUT_LONG_T001_START_RESULT_ERROR_BIT            0x00000040
//#define OEDT_ACOUSTICOUT_LONG_T001_WRITE_RESULT_ERROR_BIT            0x00000080

#define OEDT_ACOUSTICOUT_LONG_T001_STOP_RESULT_ERROR_BIT             0x20000000
#define OEDT_ACOUSTICOUT_LONG_T001_STOP_ACK_RESULT_ERROR_BIT         0x40000000
#define OEDT_ACOUSTICOUT_LONG_T001_CLOSE_OUT_RESULT_ERROR_BIT        0x80000000


static tBool OEDT_ACOUSTICOUT_LONG_T001_bStopped = FALSE;

/******************************************FunctionHeaderBegin************
*FUNCTION:    vAcousticOutTstCallback_T001
*DESCRIPTION: used as device callback function for device test T001
*PARAMETER:
*
*RETURNVALUE:
*
*HISTORY:     13.01.2011, Andre Storch TMS
*
*Initial Revision.
******************************************FunctionHeaderEnd*************/
static tVoid 
vAcousticOutTstCallback_T001 (OSAL_tenAcousticOutEvent enCbReason,
                               tPVoid pvAddData,tPVoid pvCookie)
{
    OSAL_trAcousticErrThrCfg rCbLastErrThr;
    (void)pvCookie;
    (void)pvAddData;
    
    switch (enCbReason)
    {
    case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                    "OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
      OEDT_ACOUSTICOUT_LONG_T001_bStopped = TRUE;
      break;
        
    case OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED:
        
        rCbLastErrThr = *(OSAL_trAcousticErrThrCfg*)pvAddData;
        OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: "
                                      "OSAL_EN_ACOUSTICOUT_EVERRTHRESHREACHED: %u == 0x%08X\n",\
                                      (unsigned int)rCbLastErrThr.enErrType,
                                      (unsigned int)rCbLastErrThr.enErrType));
        switch (rCbLastErrThr.enErrType)
        {
            case OSAL_EN_ACOUSTIC_ERRTYPE_XRUN:
            break;

            case OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM:
            break;

            case OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA:
            break;

            case OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT:
            break;

            case OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR:
            break;

            case OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR:
            break;

            default:
            break;
        }
        break;

    case OSAL_EN_ACOUSTICOUT_EVTIMER:
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("AOUT CALLBACK: "
                                    "OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
        break;

    case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("AOUT CALLBACK: "
                                    "OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
        break;

    case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("AOUT CALLBACK: "
                                    "OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
        break;

    case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("AOUT CALLBACK: "
                                    "OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
        break;
        
    default: /* callback unsupported by test code */
      OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("AOUT CALLBACK: "
                                    "DEFAULT %u == 0x%08X\n",
                                    (unsigned int)enCbReason,
                                    (unsigned int)enCbReason));
        break;
    }
}


/********************************************************************/ /**
  *  FUNCTION:      tU32 OEDT_ACOUSTICOUT_LONG_T001(void)
  *
  *  @brief         Play PCM Test
  *
  *  @param         
  *
  *  @return   0 if Succes, bitcoded Errorvalue if failed
  *
  *  HISTORY:
  *
  *  - 13.01.2011 Andre Storch, TMS
  *    Initial revision.
  ************************************************************************/
static tU32 OEDT_ACOUSTICOUT_LONG_T00X(int iSamplerate, int iChannels)
{
  tU32 u32ResultBitMask           = OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout; // l i n t = OSAL_ERROR;
  tS32 s32Ret;

  OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("tU32 OEDT_ACOUSTICOUT_LONG_T001(void)\n"));

  {
      /* open /dev/acousticout/speech */
      hAcousticout = OSAL_IOOpen(OEDT_ACOUSTICOUT_LONG_T001_DEVICE_NAME,
                                  OSAL_EN_WRITEONLY);
      //hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_MUSIC, OSAL_EN_WRITEONLY);
      if(OSAL_ERROR == hAcousticout)
      {
        OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                       "ERROR Open <%s>\n",
                                       OEDT_ACOUSTICOUT_LONG_T001_DEVICE_NAME
                                       ));
        u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_OPEN_OUT_RESULT_ERROR_BIT;
        hAcousticout = OSAL_ERROR;
      }
      else //if(OSAL_ERROR == hAcousticout)
      {
        OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                      "SUCCESS Open <%s> == %u\n",
                                      OEDT_ACOUSTICOUT_LONG_T001_DEVICE_NAME,
                                      (unsigned int)hAcousticout
                                      ));
      } //else //if(OSAL_ERROR == hAcousticout)


      /* register callback function */
      {
        OSAL_trAcousticOutCallbackReg rCallbackReg;
        rCallbackReg.pfEvCallback = vAcousticOutTstCallback_T001;
        rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
        s32Ret = OSAL_s32IOControl(hAcousticout,
                                   OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION,
                                   (tS32)&rCallbackReg);
        if(OSAL_OK != s32Ret)
        {
          OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_REG_NOTIFICATION_RESULT_ERROR_BIT;
        }
        else
        {
          OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS RegNotify\n"));

        }
      }
      

      /* configure sample rate */
      {
          OSAL_trAcousticSampleRateCfg   rSampleRateCfg;
          rSampleRateCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
          rSampleRateCfg.nSamplerate = (unsigned int)iSamplerate;
          s32Ret = OSAL_s32IOControl(hAcousticout,
                                     OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                     (tS32)&rSampleRateCfg);
          if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                             "ERROR SETSAMPLERATE: %u\n",
                                             (unsigned int)rSampleRateCfg.nSamplerate));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_SETSAMPLERATE_RESULT_ERROR_BIT;
          }
          else
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "SUCCESS SETSAMPLERATE: %u\n",
                                            (unsigned int)rSampleRateCfg.nSamplerate));
          }
      }


      /* configure channels */
      {
          tU32 u32Channels = (tU32)iChannels;
          s32Ret = OSAL_s32IOControl(hAcousticout,
                                     OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,
                                     (tS32)u32Channels);
          if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                             "ERROR SETCHANNELS: %u\n",
                                             (unsigned int)u32Channels));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_SETCHANNELS_RESULT_ERROR_BIT;
          }
          else
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "SUCCESS SETCHANNELS: %u\n",
                                            (unsigned int)u32Channels));
          }
      }

      /* configure sample format */
      {
          OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
          rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
          rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
          s32Ret = OSAL_s32IOControl(hAcousticout,
                                     OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT,
                                     (tS32)&rSampleFormatCfg);
          if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                             "ERROR SETSAMPLEFORMAT: %u\n",
                                             (unsigned int)rSampleFormatCfg.enSampleformat));
              u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_SETSAMPLEFORMAT_RESULT_ERROR_BIT;
          }
          else //if(OSAL_OK != s32Ret)
          {
              OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                            "SUCCESS SETSAMPLEFORMAT: %u\n",
                                            (unsigned int)rSampleFormatCfg.enSampleformat));
          } //else //if(OSAL_OK != s32Ret)
      }

      /* configure buffersize */
      {
        OSAL_trAcousticBufferSizeCfg rCfg;
        rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
        rCfg.nBuffersize = OEDT_ACOUSTICOUT_LONG_T001_BUFFERSIZE;
        s32Ret = OSAL_s32IOControl(hAcousticout,
                                   OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE,
                                   (tS32)&rCfg);
        if(OSAL_OK != s32Ret)
        {
            OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                           "ERROR SETBUFFERSIZE: %u\n",
                                           (unsigned int)rCfg.nBuffersize));
            u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_SETBUFFERSIZE_RESULT_ERROR_BIT;
        }
        else //if(OSAL_OK != s32Ret)
        {
            OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                          "SUCCESS SETBUFFERSIZE: %u\n",
                                          (unsigned int)rCfg.nBuffersize));
        } //else //if(OSAL_OK != s32Ret)
      }



      /* issue start command */
      s32Ret = OSAL_s32IOControl(hAcousticout,
                                 OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                                 (tS32)NULL);
      if(OSAL_OK != s32Ret)
      {
          OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START\n"));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_START_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
          OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START\n"));
      } //else //if(OSAL_OK != s32Ret)


      

      //write some samples if no error occured before
      if(OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE == u32ResultBitMask)
      {
            //play jet
            {
				printf("Jet Start\n");
				JetSet_JetSet();
				JetSet_JetReset(TRUE);
				JetSet_Init((unsigned int)iSamplerate,
							iChannels);
				JetSet_SetSpeed(42);
				//JetSet_SetSampleRate(10000);
				m2962();
				jetmain();
				uiJetCount = 0;
				while(uiJetCount < 12755) //12810)
				{
					JetSet_Play(hAcousticout);
					JetSet_DeleteWaveBlock();
				}
				JetSet_DesJetSet();
            }
            
            /* stop command */
            OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                          "INFO Send STOP command\n"));
            OEDT_ACOUSTICOUT_LONG_T001_bStopped = FALSE;
            s32Ret = OSAL_s32IOControl(hAcousticout,
                                       OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,
                                       (tS32)NULL);
            if(OSAL_OK != s32Ret)
            {
                OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP\n"));
                u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_STOP_RESULT_ERROR_BIT;
            }
            else //if(OSAL_OK != s32Ret)
            {
                OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS STOP\n"));
            } //else //if(OSAL_OK != s32Ret)

            //waiting for stop reply
            {
                int iEmergency = 50;
                while(!OEDT_ACOUSTICOUT_LONG_T001_bStopped && (iEmergency-- > 0))
                {
                    OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOR STOP\n"));
                    OSAL_s32ThreadWait(100);
                } //while(!OEDT_ACOUSTICOUT_LONG_T001_bStopped && iEmergency-- > 0)

                if(iEmergency <= 0)
                {
                    OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                                   "ERROR no STOP acknowledge\n"));
                    u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_STOP_ACK_RESULT_ERROR_BIT;
                }
                else //if(iEmergency <= 0)
                {
                    OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: STOPPED\n"));
                } //else //if(iEmergency <= 0)
            }
            
      } //if(OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE == u32ResultBitMask)
           


      


      
      /* close /dev/acousticout/speech */
      s32Ret = OSAL_s32IOClose(hAcousticout);
      if(OSAL_OK != s32Ret)
      {
          OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                         "ERROR AOUT CLOSE handle %u\n",
                                         (unsigned int)hAcousticout
                                         ));
          u32ResultBitMask |= OEDT_ACOUSTICOUT_LONG_T001_CLOSE_OUT_RESULT_ERROR_BIT;
      }
      else //if(OSAL_OK != s32Ret)
      {
          OEDT_ACOUSTICOUT_LONG_PRINTF_INFO(("OEDT_ACOUSTIC: "
                                        "SUCCESS AOUT CLOSE handle %u"
                                        "\n",
                                        (unsigned int)hAcousticout
                                        ));
      } //else //if(OSAL_OK != s32Ret)

    //      OSAL_s32ThreadWait(1000);
  }


  if(OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE != u32ResultBitMask)
  {
      OEDT_ACOUSTICOUT_LONG_PRINTF_ERROR(("OEDT_ACOUSTIC: "
                                     "T001 bit coded ERROR: 0x%08X\n",
                                     (unsigned int)u32ResultBitMask));
  } //if(OEDT_ACOUSTICOUT_LONG_T001_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
} //tU32 OEDT_ACOUSTICOUT_LONG_T001(void)


tU32 OEDT_ACOUSTICOUT_LONG_T001(void)
{
	return OEDT_ACOUSTICOUT_LONG_T00X(44100, 2);
}
tU32 OEDT_ACOUSTICOUT_LONG_T002(void)
{
	return OEDT_ACOUSTICOUT_LONG_T00X(22050, 2);
}
tU32 OEDT_ACOUSTICOUT_LONG_T003(void)
{
	return OEDT_ACOUSTICOUT_LONG_T00X(44100, 1);
}
tU32 OEDT_ACOUSTICOUT_LONG_T004(void)
{
	return OEDT_ACOUSTICOUT_LONG_T00X(22050, 1);
}

