/******************************************************************************
 * FILE         : oedt_ADC_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the individual test cases for the ADC
 *                device for Gen3 hardware.
 *
 * AUTHOR(s)    :  Basavaraj Nagar (RBEI/ECG4)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          |                             | Author & comments
 * --.--.--      | Initial revision            | ------------
 * 28,March,2013 | version 0.1                 | Basavaraj Nagar (RBEI/ECG4)
 *-----------------------------------------------------------------------------
 *               |                             | Basavaraj Nagar(RBEI/ECG4)
 *               |                             | Added helper traces
 * 10,june,2013  | version 0.2                 |
 *-----------------------------------------------------------------------------
 *               |                             | Dharmender Suresh Chander (RBEI/ECF5)
 *               |                             | Timeout included for Callback 
 * 11,june,2014  | version 0.3                 | Test case
*------------------------------------------------------------------------------
 *               |                             | Chandra Sekaran M (RBEI/ECF5)
 *               |                             | Modified for Lint fix
 *               |                             | Created a function u32ADCGetProcess\
 *               |                             | Response() for lint fix.
 *               |                             | Modified _u32ADCMultiProcessTest()
 *               |                             | replacing GetProcessResponse()
 * 30,March,2015 | version 0.4                 | by u32ADCGetProcessResponse(). 
*------------------------------------------------------------------------------
*******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_Adc_Test_Funcs.h"
#include "osal_if.h"
#include "oedt_helper_funcs.h"


/*Global parameters for multi process test*/
static tCString chn1Name;
static tCString chn2Name;

/*Event handles for threshold callback*/
static OSAL_tEventHandle ThresholdevHandle2 = 0;
static OSAL_tEventHandle  parallel_ADC_open;
static tS32 s32ParallelADCRetval=0,s32ParallelADCRetval1=0;

#define EVENT_WAIT_ADC_TIMEOUT OSAL_C_TIMEOUT_FOREVER
#define EVENT_ADC_POST_THREAD_1 (0x00000001) 
#define EVENT_ADC_POST_THREAD_2 (0x00000002)
#define EVENT_ADC_WAIT (0x00000003)
#define THREAD_ADC_STACK_SIZE (1024)
/*Added for Gen3 Compatibility*/ 
#define Gen3_ADC_CHANNEL_41 OSAL_C_STRING_DEVICE_ADC_TEST_1
#define Gen3_ADC_CHANNEL_42 OSAL_C_STRING_DEVICE_ADC_TEST_2
#define Gen3_ADC_CHANNEL_43 OSAL_C_STRING_DEVICE_ADC_TEST_3
#define ADC_MAX_DEVNODE_PATH 128
#define PRIORITY             100
#define ADC_INC_RESOLUTION_10BIT      0x0a
#define ADC_INC_RESOLUTION_12BIT      0x0c
#define OSAL_C_ADC_TIMEOUT            5000
OSAL_tIODescriptor hlFd2;
OSAL_tIODescriptor hlFd1;
OSAL_tEventMask ResultMaskADC;

static tU32 u32ADCGetProcessResponse(OSAL_tMQueueHandle mqHandle);

enum ThresholdEvent
{
   CH3ThresholdEvent = (tU32)0x01,
   CH5ThresholdEvent = (tU32)0x02
};

/* Threshold Callback function for CH5  */
static tVoid ADCCH5ThresholdCallback(tVoid)
{
   OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"test 5 callback function executed");
   (void)CH3ThresholdEvent; // For Lint Fix
   if(OSAL_s32EventPost(ThresholdevHandle2, (OSAL_tEventMask)CH5ThresholdEvent, OSAL_EN_EVENTMASK_OR)==OSAL_ERROR)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"CH5ThresholdEvent posting failed");
   } 
}

/*****************************************************************************
* FUNCTION:     OpenCloseADCSubUnit()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:
* DESCRIPTION:  Opens and closes the specified ADC Sub unit.
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015.
*               Typecasted the trace level in OEDT_HelperPrintf() for lint fix.   
******************************************************************************/
tU32 OpenCloseADCSubUnit(tVoid)
{
   OSAL_tIODescriptor hFd;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   /* Channel Open */
   hFd = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);
   
   if(OSAL_ERROR != hFd)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"about to close");
      /* Channel Close */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret = 1;
      }
   }
   else
   {
     OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"error in opening");

      u32Ret = 2;
   }
   return u32Ret;

}
/*****************************************************************************
* FUNCTION:     u32ADCChanReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open ADC channel    2
*               2. Attempt to reopen ADC channel 2
*
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April, 2008
                Ported to Linux by Basavaraj Nagar(RBEI/ECG4)
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015.
*               Typecasted the trace level in OEDT_HelperPrintf() for lint fix.
******************************************************************************/
tU32 u32ADCChanReOpen(tVoid)
{
   OSAL_tIODescriptor hFd;
   OSAL_tIODescriptor hFd1;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;

   /* Open ADC channel */
   hFd = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);
   if(OSAL_ERROR == hFd)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"failed in first open itself!!!");
      /* Multiple opens must not be supported. If not successful, indicate error */
      u32Ret += 4;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Opening second time,it should assert now");
      /* Attempt to Re-open same ADC channel  */
      hFd1 = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);
      if(OSAL_ERROR != hFd1)
      {
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"reopened again!!!");
         u32Ret += 1;
         /* Close the channel */
         if(OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
         {
            u32Ret += 2;
         }
      }
   }

   if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
   {
      u32Ret += 6;
   }

   return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32ADCChanCloseAlreadyClosed()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_004
* DESCRIPTION:  1. Open ADC channel    2
*               2. Close ADC channel 2
*               3. Attempt to Close channel 2 again

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
                Ported to Linux by Basavaraj Nagar(RBEI/ECG4)
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015
*               Removed if(Gen3_ADC_CHANNEL_42!=OSAL_NULL) for Lint fix
******************************************************************************/
tU32 u32ADCChanCloseAlreadyClosed(tVoid)
{
   OSAL_tIODescriptor hFd;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   
   /* Open ADC channel 2 */   
   hFd = OSAL_IOOpen( Gen3_ADC_CHANNEL_42,enAccess);
   if(OSAL_ERROR != hFd)
   {
      /* Close the channel */
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret = 1;
      }
      else
      {
         /* Attempt to Close the channel which is already closed */
         if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
         {
            u32Ret += 2;
         }
      }
   }
   else
   {
      u32Ret = 4;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32ADCRead()
* PARAMETER:    Channel ID
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1. Open requested ADC Unit
*               2. Performs read operation
*               3. Close ADC channel

* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
                Ported to Linux by Basavaraj Nagar(RBEI/ECG4)
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015
*               Added the function OSAL_vMemoryFree()
******************************************************************************/
tU32 u32ADCRead()
{
   OSAL_tIODescriptor hFd;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tU16 *tU16ADCdata = NULL;
   tPS8 ps8ADCdata = NULL;
   tU32 NoOfBlocks  = 1;
   tU32 u32NoOfBlocks = 1;
   tU32 U32_minblksize =2;
   
   /* Channel Open */
   hFd = OSAL_IOOpen(Gen3_ADC_CHANNEL_43, enAccess);
   if(OSAL_ERROR != hFd)
   {
      tU16ADCdata = (tU16 *)OSAL_pvMemoryAllocate((tU32)U32_minblksize* NoOfBlocks);
      if (tU16ADCdata != NULL)
      {
         ps8ADCdata = (tPS8)(tVoid *)tU16ADCdata;
         u32NoOfBlocks = (tU32)OSAL_s32IORead(hFd,(tPS8)ps8ADCdata, (tU32)U32_minblksize* NoOfBlocks);

         if((OSAL_ERROR == (tS32)u32NoOfBlocks) ||( ((tU32)U32_minblksize* NoOfBlocks)  !=  u32NoOfBlocks))
         {
            u32Ret += 100;
         }
         /* Get 12 bit ADC data */
         else
         {
            tU16ADCdata = (tU16*)(tVoid*)ps8ADCdata;
         }
         /* Channel Close */
         if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
         {
            u32Ret += 1000;
         }
         OSAL_vMemoryFree(tU16ADCdata);
      }
      else
      {
         u32Ret += 10000;
      }
   }
   else
   {
      u32Ret = 1;
   }
   return u32Ret;
}
/*****************************************************************************
* FUNCTION:     u32ADCGetResolution(tVoid)
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_016
* DESCRIPTION:  open a channel,perform ioctl to get resolution of ADC and then close it
                Also checks the adc resolution data returned from kernel driver.
                returns zero value on success

* HISTORY:      Created by Basavaraj Nagar (RBEI/ECG4) 
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015.
*               Typecasted the trace level in OEDT_HelperPrintf() for lint fix.               .
******************************************************************************/
tU32 u32ADCGetResolution(tVoid)
{

    tU32 retval=0;
    OSAL_tIODescriptor hFd;
    OSAL_trAdcConfiguration ADC_Resolution;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /* Perform an ioctl to get ADC resolution */
    ADC_Resolution.u8AdcResolution = 0;
    /* Channel Open */
    hFd = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);

    if(OSAL_ERROR != hFd)
    {
        if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_GET_CONFIG,(tS32)(&ADC_Resolution)))
        {
            retval += 2;

        }

        if((ADC_Resolution.u8AdcResolution!=ADC_INC_RESOLUTION_10BIT)&&(ADC_Resolution.u8AdcResolution!=ADC_INC_RESOLUTION_12BIT))
        {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"no match");           
            retval += 4;
        }
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            retval = 1;
        }
        else
        {

            return retval;
        }
    }
    else
    {
        OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"error in opening");

        retval = 2;
    }

    return retval;
}

/*****************************************************************************
* FUNCTION:     u32ADCSetSingleThresholdCallbackCH5(tVoid)
* PARAMETER:    None
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_016
* DESCRIPTION:  1. Event creation
*               2. Open requested ADC Unit
*               3. Sets threshold value and Single threshold
                   Callback 
*               4. Wait for the event posted by Threshold callback function.
*               5. Close ADC channel
* HISTORY:      Created by Haribabu Sannapaneni (RBEI/ECM1) on 2, July,2008
*               Modified to channel 7.
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015
*               Removed if(Gen3_ADC_CHANNEL_42!=OSAL_NULL) for Lint fix
*               Typecasted the trace level in OEDT_HelperPrintf() for lint fix.
******************************************************************************/
tU32 u32ADCSetSingleThresholdCallbackCH5(tVoid)
{

   OSAL_tIODescriptor hFd;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32Ret = 0;
   tU16 LowerComparedata = 10;
   tU16 UpperComparedata = 100;
   OSAL_trAdcSetThreshold  padcsetthreshold; /* Structure */
   OSAL_trAdcSetCallback padcsetcallback;
   OSAL_tEventMask gevMask1 = 0;
   /* Create Event for Thread 1*/
   if( OSAL_ERROR == OSAL_s32EventCreate("ADC_Threshold_CH5_CALLBACK",&ThresholdevHandle2 ) )
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"failed to create event");
      u32Ret = 1;
      return u32Ret;
   }

   /* Set thershold for Channel   */
   /* Channel Open */
   hFd = OSAL_IOOpen( Gen3_ADC_CHANNEL_42,enAccess);
   if(OSAL_ERROR != hFd)
   {

      /* Set Threshold Callback */
      padcsetcallback.enCallbackType = enAdcCallbackThreshold;
      padcsetcallback.pfnCallbackFunction = ADCCH5ThresholdCallback;
      if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
      {
         u32Ret = 3;

      }

      if(u32Ret == 0)
      {
         /* Set lower limit */
         padcsetthreshold.u16Threshold = LowerComparedata;
         padcsetthreshold.enComparison = enAdcThresholdLt;
         //padcsetthreshold.bEnable = TRUE;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"ioctl threshold being called");           
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"error in setting threshold");
            u32Ret = 5;
         }

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"past ioctl threshold being called");  
         /* Set Upper limit */
         padcsetthreshold.u16Threshold = UpperComparedata;
         padcsetthreshold.enComparison = enAdcThresholdGt;
         padcsetthreshold.bEnable = TRUE;
         if(OSAL_ERROR ==OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,(tS32)&padcsetthreshold))
         {
            u32Ret += 10;
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"error in setting threshold");
         }
         else
         {
            OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Waiting for threshold hit");
            if(OSAL_s32EventWait( ThresholdevHandle2,(tU32) CH5ThresholdEvent,
                     OSAL_EN_EVENTMASK_OR,
                     OSAL_C_ADC_TIMEOUT ,
                     &gevMask1 )== OSAL_ERROR)
            {			
               u32Ret = OSAL_u32ErrorCode();
               if(u32Ret == OSAL_E_TIMEOUT)
               {
                  OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Timeout :- No Threshold Hit");
               }
               u32Ret += 20;
            }
         }
         /* Unregister the callback*/
         padcsetcallback.enCallbackType = enAdcCallbackThreshold;
         padcsetcallback.pfnCallbackFunction = 0;
         if(OSAL_ERROR == OSAL_s32IOControl(hFd,OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,(tS32)&padcsetcallback))
         {
            u32Ret += 50;

         }

      }
      /* Close Event */
      if( OSAL_ERROR == OSAL_s32EventClose( ThresholdevHandle2 ) )
      {

         u32Ret += 100;
      }
      /*Delete the Event */
      if( OSAL_ERROR == OSAL_s32EventDelete( "ADC_Threshold_CH5_CALLBACK" ) )
      {
         u32Ret += 1000;
      }
      if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
      {
         u32Ret += 10000;
      }
   }
   else
   {
      u32Ret = 2;
   }
   return u32Ret;
}

/****************************************************************************
	 FUNCTION:     u32ADClogicalDualthreadRead()
* PARAMETER:   none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ADC_29
* DESCRIPTION:  1. Open requested ADC Unit using different threads
*               2. Performs read operation
*               3. Close ADC channels
			this function is to test the MUX switching functionality between mux channels
* HISTORY:  Created by Nikhil Ravidran (RBEI/ECF1)

*******************************************************************************/
tU32 u32ADClogicalDualthreadRead(tVoid)
{
    tU32 u32Ret=0;
    OSAL_tThreadID TID_1=OSAL_NULL,TID_2=OSAL_NULL;
    OSAL_trThreadAttribute rThAttr1, rThAttr2;
    OSAL_tenAccess enAccess = OSAL_EN_READONLY;

    /*thread stuff*/
    rThAttr1.szName  = "Thread_1";
    rThAttr1.pfEntry = (OSAL_tpfThreadEntry)Parallel_ADC_Open_Thread_1;
    rThAttr1.pvArg   =   (tPVoid)NULL;
    rThAttr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThAttr1.s32StackSize = THREAD_ADC_STACK_SIZE;

    rThAttr2.szName  = "Thread_2";
    rThAttr2.pfEntry = (OSAL_tpfThreadEntry)Parallel_ADC_Open_Thread_2;
    rThAttr2.pvArg   =   (tPVoid)NULL;
    rThAttr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThAttr2.s32StackSize = THREAD_ADC_STACK_SIZE;

    if(OSAL_OK == OSAL_s32EventCreate("parallel_ADC_open", &parallel_ADC_open))
    {		
        /*create and activate thread1 */
        hlFd2 = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);
        hlFd1 = OSAL_IOOpen(Gen3_ADC_CHANNEL_43,enAccess);
        if(hlFd2!=OSAL_ERROR)
        {
            TID_1 = OSAL_ThreadSpawn(&rThAttr1);
            if(OSAL_ERROR != TID_1)
            {
                /*create thread2*/
                TID_2 = OSAL_ThreadSpawn(&rThAttr2);
                if(OSAL_ERROR != TID_2)
                {
                    if(OSAL_OK != OSAL_s32EventWait(parallel_ADC_open, EVENT_ADC_WAIT,
                        OSAL_EN_EVENTMASK_AND, EVENT_WAIT_ADC_TIMEOUT, 
                        &ResultMaskADC))
                    {

                        if(s32ParallelADCRetval == 0 &&s32ParallelADCRetval1==0)
                        {
                            //wait event failed
                            u32Ret = 110;

                        }
                    }
                    if(s32ParallelADCRetval == 0 && s32ParallelADCRetval1==0)
                    {
                        u32Ret = 0;

                    }

                    if(OSAL_ERROR	== OSAL_s32IOClose ( hlFd2) )
                    {
                        u32Ret =+ 20;
                    }


                    if(OSAL_ERROR	== OSAL_s32IOClose ( hlFd1) )
                    {
                        u32Ret =+ 10;
                    }
                }
                else
                {
                    u32Ret = 104;
                    if (OSAL_s32ThreadDelete(TID_1) != OSAL_OK)
                    {
                        //error closing file1 
                        u32Ret = 106;
                    }

                }
            }
            else
            {
                u32Ret = 105;
            }
        }
        else
        {
            u32Ret = 109;
        }

    }

    if(OSAL_ERROR == OSAL_s32EventClose(parallel_ADC_open))
    {
        //event close error
        if(s32ParallelADCRetval == 0)
        {
            u32Ret = 102;
        }	
    }
    //delete the event
    if(OSAL_ERROR == OSAL_s32EventDelete("parallel_ADC_open"))
    {
        //event delete error
        if(s32ParallelADCRetval == 0)
        {
            u32Ret = 103;
        }
    }

    return u32Ret;
}

static tVoid Parallel_ADC_Open_Thread_1(tPVoid pvThread1Arg)
{
   tU16 *U16ADCdata;
   tPS8 ps8ADCdata;
   tU32 U32Blocks_toread  = 1, u32NoOfBlocks = 0;
   tU32 U32_minblksize=2;
   (tVoid)pvThread1Arg;

   U16ADCdata = (tU16 *)OSAL_pvMemoryAllocate((tU32)U32_minblksize* U32Blocks_toread);
   ps8ADCdata = (tPS8)(tVoid *)U16ADCdata;

   if(U16ADCdata!= OSAL_NULL)
   {

      u32NoOfBlocks = (tU32)OSAL_s32IORead(hlFd1,(tPS8)ps8ADCdata, (tU32)U32_minblksize* U32Blocks_toread) ;
      if((OSAL_ERROR == (tS32)u32NoOfBlocks) || ((U32_minblksize* U32Blocks_toread)  !=u32NoOfBlocks))
      {
         s32ParallelADCRetval =+ 4;
      }
      /* Get ADC data */
      else
      {
         if(s32ParallelADCRetval ==0)
         {
            s32ParallelADCRetval=0;
         }
         U16ADCdata = (tU16*)(tVoid*)ps8ADCdata;
      }
      OSAL_vMemoryFree(U16ADCdata);
   }
   if(OSAL_ERROR!= OSAL_s32EventPost(parallel_ADC_open, EVENT_ADC_POST_THREAD_1,
                                    OSAL_EN_EVENTMASK_OR))
   {
      s32ParallelADCRetval =+ 70;
   }
}

static tVoid Parallel_ADC_Open_Thread_2(tPVoid pvThread2Arg)
{
   tU16 *U16ADCdata1;
   tPS8 ps8ADCdata1;
   tU32 U32Blocks_toread  = 1, u32NoOfBlocks = 0;
   tU32 U32_minblksize=2;
   (tVoid)pvThread2Arg;

   U16ADCdata1 = (tU16 *)OSAL_pvMemoryAllocate((tU32)U32_minblksize* U32Blocks_toread);
   ps8ADCdata1 = (tPS8)(tVoid *)U16ADCdata1;

   if(U16ADCdata1!= OSAL_NULL)
   {
      u32NoOfBlocks = (tU32)OSAL_s32IORead(hlFd2,(tPS8)ps8ADCdata1, (tU32)U32_minblksize* U32Blocks_toread) ;
      if((OSAL_ERROR == (tS32)u32NoOfBlocks) || ((U32_minblksize* U32Blocks_toread)  !=u32NoOfBlocks))
      {
         s32ParallelADCRetval1 =+ 4;

      }
      /* Get ADC data */
      else
      {
         if(s32ParallelADCRetval1 ==0)
         {
            s32ParallelADCRetval1=0;
         }
         U16ADCdata1 = (tU16*)(tVoid*)ps8ADCdata1;
      }
      OSAL_vMemoryFree(U16ADCdata1);
   }
   else
   {
      s32ParallelADCRetval1 =+ 170;
   }
   if(OSAL_ERROR!= OSAL_s32EventPost(parallel_ADC_open, EVENT_ADC_POST_THREAD_2,
                                    OSAL_EN_EVENTMASK_OR))
   {
      s32ParallelADCRetval =+ 70;
   }
}

/*****************************************************************************
* FUNCTION:     _u32ADCMultiProcessTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Access DRV ADC from multiple processes
* HISTORY:      Created by Basavaraj Nagar(RBEI/ECG4)
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015
*               Replaced GetProcessResponse() with u32ADCGetProcessResponse()
*               because of lint warnings
*               Typecasted the trace level in OEDT_HelperPrintf() for lint fix.
*****************************************************************************/
tU32 _u32ADCMultiProcessTest(tVoid)
{
   OSAL_tMQueueHandle mqHandleR = OSAL_C_INVALID_HANDLE;
   OSAL_trProcessAttribute prAtr = {OSAL_NULL};
   OSAL_tProcessID prID_1;
   OSAL_tProcessID prID_2;
   tU32 u32Ret = 0;
   tChar ADCInput1[ADC_MAX_DEVNODE_PATH];
   tChar ADCInput2[ADC_MAX_DEVNODE_PATH];

   snprintf(ADCInput1, ADC_MAX_DEVNODE_PATH, "%s", chn1Name);
   snprintf(ADCInput2, ADC_MAX_DEVNODE_PATH, "%s", chn2Name);

   // Create or open MQ for both processes to send their responses
   if (OSAL_s32MessageQueueCreate(MQ_ADC_MP_RESPONSE_NAME,
         MQ_ADC_MP_CONTROL_MAX_MSG, MQ_ADC_MP_CONTROL_MAX_LEN,
         OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR)
   {
      if (OSAL_s32MessageQueueOpen(MQ_ADC_MP_RESPONSE_NAME,
            OSAL_EN_READONLY, &mqHandleR) == OSAL_ERROR)
      {
         return 1000;
      }
   }

   prAtr.szAppName      = "/opt/bosch/base/bin/procadcp1_out.out";
   prAtr.szName         = "procadcp1_out.out";
   prAtr.u32Priority    = PRIORITY;
   prAtr.szCommandLine  = ADCInput1;
   prID_1 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_1)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "proc1 spawn failed: %lu",
            OSAL_u32ErrorCode());
      return 1001;
   }

   prAtr.szAppName      = "/opt/bosch/base/bin/procadcp2_out.out";
   prAtr.szName         = "procadcp2_out.out";
   prAtr.u32Priority    = PRIORITY;
   prAtr.szCommandLine  = ADCInput2;
   prID_2 = OSAL_ProcessSpawn(&prAtr);
   if (OSAL_ERROR == prID_2)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "proc2 spawn failed: %lu",
            OSAL_u32ErrorCode());
      return 1002;
   }

   u32Ret += u32ADCGetProcessResponse(mqHandleR);
   u32Ret += u32ADCGetProcessResponse(mqHandleR);

   return u32Ret;
}

tU32 u32ADCMultiProcessTest(tVoid)
{
   chn1Name=Gen3_ADC_CHANNEL_41;
   chn2Name=Gen3_ADC_CHANNEL_42;
   return _u32ADCMultiProcessTest();
}

/*****************************************************************************
* FUNCTION:     u32ADCunsupportedioctrlcheck()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Check DRV ADC for unsupported ioctrl
* HISTORY:      Created by Basavaraj Nagar(RBEI/ECG4)
*               Modified by Chandra Sekaran M (RBEI/ECF5) on 30, March, 2015
*               Removed if(Gen3_ADC_CHANNEL_41!=OSAL_NULL) for Lint fix
*****************************************************************************/
tU32 u32ADCUnsupportedIoctrlCheck(tVoid)
{
   OSAL_tIODescriptor hFd;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tU32 u32BlockSize = 0;
   tU32 u32Ret = 0;
   tS32 s32Ret=0;
   /* Channel Open */
   hFd = OSAL_IOOpen(Gen3_ADC_CHANNEL_41,enAccess);
   if(OSAL_ERROR != hFd)
   {
      s32Ret=OSAL_s32IOControl( hFd,OSAL_C_S32_IOCTRL_SADC_GET_BLOCK_SIZE,(tS32)&u32BlockSize);

      if(OSAL_ERROR == s32Ret)
      {
       //currently unsupported in Gen3, hence this is the expected result
         u32Ret = 0;
      }
      else
      {
       u32Ret+=100;
       OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"unsupported IOCTRL block size being recognized" ); 			 
      }
      if(OSAL_ERROR	== OSAL_s32IOClose(hFd))
      {
          u32Ret =+ 200;
          OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"FAILED to close the opened channel" );
      }
   }
   else
   {
      u32Ret+=300;
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"unable to open the adc channel"); 
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   u32ADCGetProcessResponse()
* PARAMETER   :   mqHandle
* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :   Wrapper function for OSAL_s32MessageQueueWait()
*                 Created for lint fix.
* HISTORY     :   Created by Chandra Sekaran M (RBEI/ECF5) on Mar 30, 2015
*******************************************************************************/
static tU32 u32ADCGetProcessResponse(OSAL_tMQueueHandle mqHandle)
{
   tU32 u32Ret = 0;
   tU32 u32Inmsg, u32Procerr;
   if(OSAL_s32MessageQueueWait(mqHandle, (tPU8)&u32Inmsg, sizeof(u32Inmsg), OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
   {
      u32Ret += 1000;
   }
   u32Procerr = u32Inmsg & 0x0000ffff;
   if(u32Procerr != 0)
   {
      u32Ret += u32Procerr * 10000;
   }
   return u32Ret;
}
/* end of file */

