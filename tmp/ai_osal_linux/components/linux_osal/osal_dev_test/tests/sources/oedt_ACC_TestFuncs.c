/******************************************************************************
 * FILE         : oedt_ACC_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the individual test cases for the 
 *                Accelerometer device dev_acc.
 *
 * AUTHOR(s)    :  Haribabu Sannapaneni (RBEI/ECF1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          |                             | Author & comments
 * --.--.--      | Initial revision            | ------------
 * 09,March,2010 | Version 1.0                 | Haribabu Sannapaneni (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 11.DEC.2015   | Version 1.1                 | (RBEI/ECf5) Shivasharnappa Mothpalli 
                                               | Removed  un used IOCTRL calls (Fix for CFG3-1622):
                                               | OSAL_C_S32_IOCTRL_ACC_SET_TIMEOUT_VALUE,
                                               | OSAL_C_S32_IOCTRL_ACC_GET_TIMEOUT_VALUE,
                                               | OSAL_C_S32_IOCTRL_ACC_STOP,
                                               | OSAL_C_S32_IOCTRL_ACC_FLUSH,
                                               | OSAL_C_S32_IOCTRL_ACC_START
 *-----------------------------------------------------------------------------
 
 
*******************************************************************************/
#include "oedt_ACC_TestFuncs.h"
#include "oedt_helper_funcs.h"

#if defined (GEN3ARM)
#define ACC_READ_WAIT 6000
#else
#define ACC_READ_WAIT 0
#endif

#define ACC_OEDT_TRACE_WIDTH (25)
#define ACC_OEDT_MID_ACCVALUE (8192/2) 
#define ACC_OEDT_DATA_SCALE_FACTOR (ACC_OEDT_MID_ACCVALUE/ACC_OEDT_TRACE_WIDTH)
#define ACC_OEDT_NO_OF_PLOT_COLUMNS (6)
#define ACC_OEDT_TOTAL_PLOT_WIDTH (ACC_OEDT_TRACE_WIDTH * ACC_OEDT_NO_OF_PLOT_COLUMNS)
#define ACC_OEDT_X_PLOT_OFFSET (0)
#define ACC_OEDT_Y_PLOT_OFFSET (ACC_OEDT_TRACE_WIDTH *2)
#define ACC_OEDT_Z_PLOT_OFFSET (ACC_OEDT_TRACE_WIDTH *4)
#define ACC_OEDT_MAX_CONT_READ_FAILURES (10)
#define ACC_OEDT_TEST_DURATION_MS (60000)
#define ACC_OEDT_PLOT_DATA_INTERVALL (300)

#define ACC_ADC_BIT_RESOLUTION_RANGE 2048
#define ACC_NO_ERROR               0
#define ACC_IOOPEN_ERROR           1
#define ACC_IOREAD_ERROR           2
#define ACC_VALUE_RANGE_ERROR      3
#define ACC_MID_VALUE_ERROR        4
#define ACC_CLOSE_ERROR            5 
#define ACC_IOCTRL_GET_CYCLE_TIME_ERROR 6
#define ACC_CYCLE_TIME_VALUE_MISMATCH_ERROR  7

#define ACC_DEFAULT_TIME_INTERVAL 50000000 //NS

tVoid ACCOedt_vPlotdata(const OSAL_trIOCtrlAccData *prIOCtrlAccData);

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
* FUNCTION    : u32AccGetCycleTime()
* DESCRIPTION : Reads the cycle time from Acc .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 24.11.15  Shivasharnappa Mothpalli(RBEI/ECF5)
******************************************************************/
tU32 u32AccGetCycleTime(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = ACC_NO_ERROR;
   tU32 AccGetCyleTime = 0;

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACC,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = ACC_IOOPEN_ERROR;
   }
   else
   {
      if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ACC_GETCYCLETIME,(tS32)&AccGetCyleTime) == OSAL_ERROR) 
      {
         u32RetVal = ACC_IOCTRL_GET_CYCLE_TIME_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Acc  get cycle time failed error no  : %d",OSAL_u32ErrorCode() );
      }
      //PQM_authorized_multi_550
      else if (AccGetCyleTime != ACC_DEFAULT_TIME_INTERVAL)//lint !e774
      {
         u32RetVal = ACC_CYCLE_TIME_VALUE_MISMATCH_ERROR; 
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Mismatch in acc cycle time ,AccGetCyleTime  : %d",AccGetCyleTime );
      }
      else
      {
         u32RetVal =  ACC_NO_ERROR;
      }

   }

   if ( (hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = ACC_CLOSE_ERROR;
   }

   return u32RetVal;
}


/*****************************************************************************
* FUNCTION     : u32AccCheckAccValuesSeq_x()
* PARAMETER    : None
* RETURNVALUE  : "0" on success  or "non-zero" value in case of error
* DESCRIPTION  : Reads 100 data records from the driver,
                 Checks the variation in X-axis data is within 0.5% 
                 of full scale when the target is still(not moving).               
* HISTORY      : 30.09.15    Shivasharnappa Mothpalli (RBEI/ECF5)
*                19.09.16    Srinivas Anvekar(RBEI/ECF1) - Modifications    
*****************************************************************************/

tU32 u32AccCheckACCValuesSeq_x(void)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlAccData xData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8  u8Index = 0;
   tU16 au16AccReadVal_x[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tU8  u8PercentageOfRange = 5;
   tU16 u16Acc_Mid_x_Value_High = 0;
   tU16 u16Acc_Mid_x_Value_Low = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = ACC_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = ACC_IOOPEN_ERROR;
   }
   else
   {

      for( u8Index= 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal != ACC_IOREAD_ERROR) ; u8Index++)
      {
         if( OSAL_s32IORead ( hDevice,(tS8 *)&xData,sizeof(xData) ) == OSAL_ERROR )
         {
            u32RetVal = ACC_IOREAD_ERROR;
         }
         au16AccReadVal_x[u8Index] = xData.u16Acc_x;
      }

      if( u32RetVal == ACC_NO_ERROR )
      {
         for(u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  au16AccReadVal_x[u8Index];
         }
         
         //Calculate the 5 % of average value
         u32AvgVal = (u32Sum / u32NoOfRecsForRead); 
         u16BaseAvgVal = ( tU16 )( (u32AvgVal * 5)/100);
         u16Acc_Mid_x_Value_High = ( tU16 )( u32AvgVal + u16BaseAvgVal );
         u16Acc_Mid_x_Value_Low = ( tU16 ) ( u32AvgVal - u16BaseAvgVal );                          
         u32LowVal = u32AvgVal;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if(au16AccReadVal_x[u8Index] > u32HighVal)
            u32HighVal = au16AccReadVal_x[u8Index];

            if(au16AccReadVal_x[u8Index] <  u32LowVal)
            u32LowVal = au16AccReadVal_x[u8Index];
         }
         
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_x_Value_High: %d", u16Acc_Mid_x_Value_High);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_x_Value_Low:  %d", u16Acc_Mid_x_Value_Low);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);

         if( (u32HighVal - u32LowVal) > ((u8PercentageOfRange*ACC_ADC_BIT_RESOLUTION_RANGE)/100))
         {
            u32RetVal=ACC_VALUE_RANGE_ERROR;
         }
         else if( (u32AvgVal < u16Acc_Mid_x_Value_Low) || (u32AvgVal > u16Acc_Mid_x_Value_High) )
         {
           u32RetVal=ACC_MID_VALUE_ERROR;
         }

      }

      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = ACC_CLOSE_ERROR;
      }

   }

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION     : u32AccCheckAccValuesSeq_y()
* PARAMETER    : None
* RETURNVALUE  : "0" on success  or "non-zero" value in case of error
* DESCRIPTION  : Reads 100 data records from the driver,
                 Checks the variation in Y-axis data is within 0.5% 
                 of full scale when the target is still(not moving).               
* HISTORY      : 30.09.15    Shivasharnappa Mothpalli (RBEI/ECF5)
*                19.09.16    Srinivas Anvekar(RBEI/ECF1) - Modifications     
*****************************************************************************/

tU32 u32AccCheckACCValuesSeq_y(void)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlAccData yData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8  u8Index = 0;
   tU16 au16AccReadVal_y[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tU8  u8PercentageOfRange = 5;
   tU16 u16Acc_Mid_y_Value_High = 0;
   tU16 u16Acc_Mid_y_Value_Low = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = ACC_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = ACC_IOOPEN_ERROR;
   }
   else
   {
   
      for( u8Index= 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal != ACC_IOREAD_ERROR) ; u8Index++)
      {
         if( OSAL_s32IORead ( hDevice,(tS8 *)&yData,sizeof(yData) ) == OSAL_ERROR )
         {
            u32RetVal = ACC_IOREAD_ERROR;
         }
         au16AccReadVal_y[u8Index] = yData.u16Acc_y;
      }
      
      if( u32RetVal == ACC_NO_ERROR )
      {
         for(u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  au16AccReadVal_y[u8Index];
         }
         
         //Calculate the 5 % of average value
         u32AvgVal = (u32Sum / u32NoOfRecsForRead);
         u16BaseAvgVal = ( tU16 )( (u32AvgVal * 5)/100);
         u16Acc_Mid_y_Value_High =( tU16 )( u32AvgVal + u16BaseAvgVal );
         u16Acc_Mid_y_Value_Low = ( tU16 )( u32AvgVal - u16BaseAvgVal );
         u32LowVal = u32AvgVal;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if(au16AccReadVal_y[u8Index] > u32HighVal)
            u32HighVal = au16AccReadVal_y[u8Index];

            if(au16AccReadVal_y[u8Index] <  u32LowVal)
            u32LowVal = au16AccReadVal_y[u8Index];
         }
         
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_y_Value_High: %d", u16Acc_Mid_y_Value_High);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_y_Value_Low:  %d", u16Acc_Mid_y_Value_Low);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);

         if( (u32HighVal - u32LowVal) > ((u8PercentageOfRange*ACC_ADC_BIT_RESOLUTION_RANGE)/100))
         {
            u32RetVal=ACC_VALUE_RANGE_ERROR;
         }
         else if( (u32AvgVal < u16Acc_Mid_y_Value_Low) || (u32AvgVal > u16Acc_Mid_y_Value_High) )
         {
            u32RetVal=ACC_MID_VALUE_ERROR;
         }

      }

      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = ACC_CLOSE_ERROR;
      }

   }

   return u32RetVal;
}

/*****************************************************************************
* FUNCTION     : u32AccCheckAccValuesSeq_z()
* PARAMETER    : None
* RETURNVALUE  : "0" on success  or "non-zero" value in case of error
* DESCRIPTION  : Reads 100 data records from the driver,
                 Checks the variation in Z-axis data is within 0.5% 
                 of full scale when the target is still(not moving).               
* HISTORY      : 30.09.15    Shivasharnappa Mothpalli (RBEI/ECF5)
*                19.09.16    Srinivas Anvekar(RBEI/ECF1) - Modifications    
*****************************************************************************/ 

tU32 u32AccCheckACCValuesSeq_z(void)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlAccData zData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8  u8Index = 0;
   tU16 au16AccReadVal_z[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tU8  u8PercentageOfRange = 5;
   tU16 u16Acc_Mid_z_Value_High = 0;
   tU16 u16Acc_Mid_z_Value_Low = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = ACC_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = ACC_IOOPEN_ERROR;
   }
   else
   {

      for( u8Index= 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal != ACC_IOREAD_ERROR) ; u8Index++)
      {
         if( OSAL_s32IORead ( hDevice,(tS8 *)&zData,sizeof(zData) ) == OSAL_ERROR )
         {
            u32RetVal = ACC_IOREAD_ERROR;
         }
         au16AccReadVal_z[u8Index] = zData.u16Acc_z;
      }

      if( u32RetVal == ACC_NO_ERROR )
      {
         for(u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  au16AccReadVal_z[u8Index];
         }
         
         //Calculate the 5 % of average value
         u32AvgVal = (u32Sum / u32NoOfRecsForRead);
         u16BaseAvgVal = ( tU16 )( (u32AvgVal * 5)/100);
         u16Acc_Mid_z_Value_High = ( tU16 )( u32AvgVal + u16BaseAvgVal );
         u16Acc_Mid_z_Value_Low = ( tU16 )( u32AvgVal - u16BaseAvgVal );
         u32LowVal = u32AvgVal;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if(au16AccReadVal_z[u8Index] > u32HighVal)
            u32HighVal = au16AccReadVal_z[u8Index];
            
            if(au16AccReadVal_z[u8Index] <  u32LowVal)
            u32LowVal = au16AccReadVal_z[u8Index];
         }
         
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_z_Value_High: %d", u16Acc_Mid_z_Value_High);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Acc_Mid_z_Value_Low:  %d", u16Acc_Mid_z_Value_Low);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);

         if( (u32HighVal - u32LowVal) > ((u8PercentageOfRange*u32AvgVal)/100))
         {
            u32RetVal=ACC_VALUE_RANGE_ERROR;
         }

      }

      if( u32RetVal == ACC_NO_ERROR )
      {
         if( (u32AvgVal < u16Acc_Mid_z_Value_Low) || (u32AvgVal > u16Acc_Mid_z_Value_High) )
         {
            u32RetVal=ACC_MID_VALUE_ERROR;
         }
      }

      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = ACC_CLOSE_ERROR;
      }

   }

   return u32RetVal;
}


/*****************************************************************************
* FUNCTION:     u32AccSelfTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION: 1.Opens the device.
*              2.Call OSAL_C_S32_IOCTRL_ACC_SELF_TEST IOCTRL to start self test.
*              3.Close the device.
*               
* HISTORY:      
*****************************************************************************/
tU32 u32AccSelfTest(tVoid)
{
   OSAL_tIODescriptor hFd;
   tU32 u32Ret = 0;
   //tBool bGotResult;
   tS32 s32SelfTestResult = 0x1234;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   (void)s32SelfTestResult;  

   /* Open the device in readwrite mode */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACC,enAccess);
   if ( OSAL_ERROR == hFd )
   {
      u32Ret = 1;
   }
   else
   {
      if( OSAL_s32IOControl (hFd,OSAL_C_S32_IOCTRL_ACC_SELF_TEST,(tS32)&s32SelfTestResult) == OSAL_ERROR )
      {
         u32Ret = 2;
      }
      if(OSAL_ERROR == OSAL_s32IOClose ( hFd ))
      {
         u32Ret += 3;
      }
   }
    return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32ACCDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_ACC_GPIO_001
* DESCRIPTION:  1. Open Device
*               2. Close Device
*               
* HISTORY:      
*****************************************************************************/
tU32 u32ACCDevOpenClose(tVoid)
{
    OSAL_tIODescriptor hFd;
    tU32 u32Ret = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /* Open the device in readwrite mode */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACC,enAccess);
    if ( OSAL_ERROR == hFd )
    {
        u32Ret = 1;
    }
    else if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
    {
        u32Ret = 2;
    }

    return u32Ret;
}


/*****************************************************************************
* FUNCTION:     u32ACCDevReOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ACC_001
* DESCRIPTION:  1. Open Device 
*               2. Attempt to open the ACC device which has already been 
*                opened (should fail)
*               3. Close Device    

* HISTORY:      
                
*****************************************************************************/
tU32 u32ACCDevReOpen(tVoid)    
{
    OSAL_tIODescriptor hFd1;
    OSAL_tIODescriptor hFd2 = 0;
    tU32 u32Ret = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /* Open the device */
    hFd1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC , enAccess );
    if ( OSAL_ERROR == hFd1)
    {
        u32Ret = 1;
    } 
    else
    {
        /* Attempt to Re-open the device 
        Mutiple open is not supported. So it should
        fail*/
        hFd2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC , enAccess );
        if ( OSAL_ERROR != hFd2)
        {
            
            u32Ret = 2;

            /* If successful, Close the device */  
            if( OSAL_ERROR  == OSAL_s32IOClose ( hFd2 ) )
            {
                u32Ret = 3;
            }
        }

        /* Close the device */  
        if( OSAL_ERROR  == OSAL_s32IOClose ( hFd1 ) )
        {
            u32Ret += 4;
        }
    }

return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32ACCDevCloseAlreadyClosed( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ACC_002
* DESCRIPTION:  1. Open Device 
*               2. Close Device    
*               3. Attempt to Close the ACC device which has already been 
*               closed (should fail)
*
* HISTORY:      
*****************************************************************************/
tU32 u32ACCDevCloseAlreadyClosed(tVoid)
{
    OSAL_tIODescriptor hFd;
    tU32 u32Ret = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

       /* Open the device */
        hFd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC , enAccess );
        if ( OSAL_ERROR == hFd)
        {
            u32Ret = 1;
        } 
        /* Close the device */  
        else if( OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            u32Ret = 2;
        }

        /* Close the device which is already closed, if successful indicate
        error*/
        else if(OSAL_ERROR  != OSAL_s32IOClose ( hFd ) )
        {
            u32Ret = 3;
        }
        else
        {
            /*success*/
        }

     return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32ACCDevRead( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_ACC_003
* DESCRIPTION:  1.Open the device.
*               2.Allocates the memory to read 8 records
*               3.Read the 8 records from ACC.
*               4.checks whether the number of bytes read are equalent
*				 5.close the device
* HISTORY:      
*****************************************************************************/
tU32 u32ACCDevRead(tVoid)
{
    OSAL_tIODescriptor hDevice;
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = 0;
    tS32 s32AccDataRead = 0;
    tS32 s32NumOfElemRead = 0;
    tU32 u32NumOfElemToRead = 8;
    tU16 *pu16_DataBufferpointer = OSAL_NULL;
    tU32 u32RetVal = 0;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 1;

    }
    if(FALSE == bErrorOccured)
    {
        pu16_DataBufferpointer = OSAL_pvMemoryAllocate(
                (tU16)u32NumOfElemToRead * sizeof(OSAL_trIOCtrlAccData));
        if(pu16_DataBufferpointer == OSAL_NULL)
        {
            u8ErrorNo = 2;
            bErrorOccured = TRUE;
            (tVoid)OSAL_s32IOClose ( hDevice );

        }
    }


    if(FALSE == bErrorOccured )
    {
        OSAL_s32ThreadWait(ACC_READ_WAIT);
        
        s32AccDataRead = OSAL_s32IORead(
                                      hDevice,
                                      ( tPS8 )pu16_DataBufferpointer,
                                      u32NumOfElemToRead * sizeof( OSAL_trIOCtrlAccData ) );
        s32NumOfElemRead = s32AccDataRead / ( tS32 )sizeof( OSAL_trIOCtrlAccData );
        if ( s32NumOfElemRead != (tS32)u32NumOfElemToRead )
        {
            u8ErrorNo = 3;
            bErrorOccured = TRUE;
            (tVoid)OSAL_s32IOClose ( hDevice );

        }
    }
    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 4;

        }

    }
    if( FALSE == bErrorOccured )
    {
        u32RetVal = 0;
    }
    else
    {

       u32RetVal = u8ErrorNo ;

    }
    if(pu16_DataBufferpointer != OSAL_NULL)
    {
       OSAL_vMemoryFree(pu16_DataBufferpointer);
    }
    return (u32RetVal);
}


/*****************************************************************************
* FUNCTION     : u32ACCDevInterfaceCheckSeq( )

* PARAMETER    : none

* RETURNVALUE  : tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE    : TU_OEDT_ACC_004

* DESCRIPTION  : To check all the IO Control interface provided by the ACC Diver
*                1.Opens the device.
*                2.Gets the version no.
*                3.Set Default Time out
*                4.Get the Time Out
*                5.Read 1 record
*                6.Stop and Flush the device
*                7.Start the device again
*                8.Read 1 record
*                9.Close the device

* HISTORY      : 11.12.15 (RBEI/ECf5) Shivasharnappa Mothpalli 
                           Removed  un used IOCTRL calls(Fix for CFG3 -1622) 
                           OSAL_C_S32_IOCTRL_ACC_SET_TIMEOUT_VALUE,
                           OSAL_C_S32_IOCTRL_ACC_GET_TIMEOUT_VALUE,
                           OSAL_C_S32_IOCTRL_ACC_STOP,
                           OSAL_C_S32_IOCTRL_ACC_FLUSH,
                           OSAL_C_S32_IOCTRL_ACC_START.

*****************************************************************************/
tU32 u32ACCDevInterfaceCheckSeq(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tBool bErrorOccured = FALSE;
   tU8 u8ErrorNo = 0;

   tU32 u32NumofRecs = 0;
   tU32 u32Version = 0;
    
   (void)u32NumofRecs;//to remove lint 
   (void)u32Version;//to remove lint 
   
   OSAL_trIOCtrlAccData rData ={0,0,0,0,0};
   //OSAL_trIOCtrlDiagAccRawData rAccdiagrawdata= {0,0,0,0};      /*-----variable is not used, commented to remove lint-----*/
   tU32 u32RetVal = 0;


   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
       bErrorOccured = TRUE;
       u8ErrorNo = 1;

   }
   /*Get the driver version*/
   if(FALSE == bErrorOccured )
   {
       if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
       {
           bErrorOccured = TRUE;
           u8ErrorNo = 2;
           (tVoid)OSAL_s32IOClose ( hDevice );
       }
   }

   /*Get the No. of records in the Buffer*/
   if(FALSE == bErrorOccured )
   {
       if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ACC_GETCNT,(tS32)&u32NumofRecs) == OSAL_ERROR)
       {
           bErrorOccured = TRUE;
           u8ErrorNo = 3;
           (tVoid)OSAL_s32IOClose ( hDevice );

       }
   }

    /*Read 1 record*/
   //PQM_authorized_multi_547 
   if(FALSE == bErrorOccured )//lint !e774
   {
       if(OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR)
       {
           bErrorOccured = TRUE;
           u8ErrorNo = 7;
           (tVoid)OSAL_s32IOClose ( hDevice );
       }
   }

   /*Read after starting the device*/
   if(FALSE == bErrorOccured )
   {
       if(OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR)
       {
           bErrorOccured = TRUE;
           u8ErrorNo = 11;
           (tVoid)OSAL_s32IOClose ( hDevice );
       }
   }

   /*Close the device*/
   if(FALSE == bErrorOccured )
   {

       if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
       {
           bErrorOccured = TRUE;
           u8ErrorNo = 12;
       }

   }

   if( FALSE == bErrorOccured )
   {
       u32RetVal = 0;
   }
   else
   {

      u32RetVal = u8ErrorNo;

   }


   return ( u32RetVal );

}

/************************************************************************
*FUNCTION:    u32ACCDevReadpassingnullbuffer
*DESCRIPTION: 1.Opens the device.
              2.Reads the data by passing buffer as NULL
              3.Close the device.
* TEST CASE:    TU_OEDT_ACC_005
*PARAMETER:   None
*
*
*RETURNVALUE: Error Number or NULL to indicate that test is passed
*
*HISTORY:     
************************************************************************/
tU32 u32ACCDevReadpassingnullbuffer(tVoid)
{
    OSAL_tIODescriptor hDevice;

    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = 0;
    tU32 u32RetVal = 0;
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 1;
    }
    if(FALSE == bErrorOccured )
    {
        if(OSAL_s32IORead (hDevice,OSAL_NULL,sizeof(OSAL_trIOCtrlAccData)) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            (tVoid)OSAL_s32IOClose ( hDevice );
        }
    }
    /*Close the device*/
    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
        }

    }
    if( FALSE == bErrorOccured )
    {
        u32RetVal = 0;
    }
    else
    {
        u32RetVal = u8ErrorNo;
    }
    return ( u32RetVal );
}
/************************************************************************
*FUNCTION     : u32ACCDevIOCTRLpassingnullbuffer

*DESCRIPTION  : 1.Opens the device.
                2.Get the version no by passing NULL parameter
                3.Get the Acc getcount by passing NULL parameter
                4.Set the timeout value by passing NULL parameter
                5.Get the timeout value by passing NULL parameter
                6.Get the Acc diag data by passing NULL parameter
                7.Close the device.

* TEST CASE   : TU_OEDT_ACC_006

*PARAMETER    : None
*
*RETURNVALUE  : Error Number or NULL to indicate that test is passed

*HISTORY      : 11.12.15 (RBEI/ECf5) Shivasharnappa Mothpalli 
                         Removed  un used IOCTRL calls(Fix for CFG3-1622) 
                         OSAL_C_S32_IOCTRL_ACC_SET_TIMEOUT_VALUE,
                         OSAL_C_S32_IOCTRL_ACC_GET_TIMEOUT_VALUE,
************************************************************************/
tU32 u32ACCDevIOCTRLpassingnullbuffer(tVoid)
{
    OSAL_tIODescriptor hDevice;

    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = 0;
    tU32 u32RetVal = 0;
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
        bErrorOccured = TRUE;
        u8ErrorNo = 1;
    }
    /*Get the driver version*/
    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)OSAL_NULL) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 2;
            (tVoid)OSAL_s32IOClose ( hDevice );
        }
    }

    /*Get the No. of records in the Buffer*/
    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ACC_GETCNT,(tS32)OSAL_NULL) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 3;
            (tVoid)OSAL_s32IOClose ( hDevice );

        }
    }

    /*Get Temperature*/
    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ACC_GET_TEMP,(tS32)OSAL_NULL) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 5;
            (tVoid)OSAL_s32IOClose ( hDevice );
        }
    }
    
    /*Get Hardware info*/
    if(FALSE == bErrorOccured )
    {
        if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO,(tS32)OSAL_NULL) != OSAL_ERROR)
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 5;
            (tVoid)OSAL_s32IOClose ( hDevice );
        }
    }

    /*Close the device*/
    if(FALSE == bErrorOccured )
    {

        if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo = 7;
        }

    }
    
    if( FALSE == bErrorOccured )
    {
        u32RetVal = 0;
    }
    else
    {
        u32RetVal = u8ErrorNo;
    }
    return ( u32RetVal );
}


tU32 ACCOedt_u32ValuesPlot (tVoid)
{
   tU32 u32RetVal = 0;
   OSAL_trIOCtrlAccData rIOCtrlAccData;
   OSAL_tIODescriptor hAccDev;
   tBool bTestEndFlag = FALSE;
   tU32 u32ReadFailureCnt = 0;
   tU32 u32AccPrevPlotTS = 0;
   tS32 s32ErrChk = 0;
   tU32 u32TestEndTime = OSAL_ClockGetElapsedTime() + ACC_OEDT_TEST_DURATION_MS;
   if( OSAL_ERROR ==(hAccDev =  OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC , OSAL_EN_READWRITE)) )
   {
       u32RetVal += 1;
   }
   else
   {
       while ( TRUE != bTestEndFlag )
       {
           s32ErrChk = OSAL_s32IORead( hAccDev,
                                     ( tPS8 )&rIOCtrlAccData,
                                     sizeof( OSAL_trIOCtrlAccData ) );
           if ( s32ErrChk != (tS32)sizeof( OSAL_trIOCtrlAccData ) ) 
           {
               u32ReadFailureCnt++;
           }
           else
           {
               if ( (rIOCtrlAccData.u32TimeStamp - u32AccPrevPlotTS) > ACC_OEDT_PLOT_DATA_INTERVALL )
               {
                   ACCOedt_vPlotdata(&rIOCtrlAccData);
                   u32AccPrevPlotTS = rIOCtrlAccData.u32TimeStamp;
               }
               u32ReadFailureCnt =0;
           }
           if ( ( u32ReadFailureCnt > ACC_OEDT_MAX_CONT_READ_FAILURES ) ||
               ( OSAL_ClockGetElapsedTime() > u32TestEndTime ))
           {
               bTestEndFlag = TRUE;
           }
       }
       if( OSAL_ERROR  == OSAL_s32IOClose ( hAccDev ) )
       {
           u32RetVal += 2;
       }
       if ( u32ReadFailureCnt > ACC_OEDT_MAX_CONT_READ_FAILURES )
       {
           u32RetVal += 4;
       }
   }
   return u32RetVal;
}


tVoid ACCOedt_vPlotdata(const OSAL_trIOCtrlAccData *prIOCtrlAccData)
{
   tU8 au8PlotData[ACC_OEDT_TOTAL_PLOT_WIDTH + 1];
   tU16 u16X_Index, u16Y_Index, u16Z_Index;
    
   if (OSAL_NULL != prIOCtrlAccData)
   {
       OSAL_pvMemorySet( au8PlotData, '_', ACC_OEDT_TOTAL_PLOT_WIDTH);
       u16X_Index = prIOCtrlAccData->u16Acc_x / ACC_OEDT_DATA_SCALE_FACTOR;
       u16Y_Index = prIOCtrlAccData->u16Acc_y / ACC_OEDT_DATA_SCALE_FACTOR;
       u16Z_Index = prIOCtrlAccData->u16Acc_z / ACC_OEDT_DATA_SCALE_FACTOR;
    
       au8PlotData[u16X_Index + ACC_OEDT_X_PLOT_OFFSET] = 'R';
       au8PlotData[u16Y_Index + ACC_OEDT_Y_PLOT_OFFSET] = 'S';
       au8PlotData[u16Z_Index + ACC_OEDT_Z_PLOT_OFFSET] = 'T';

       au8PlotData[ACC_OEDT_TOTAL_PLOT_WIDTH] = '\0';
       OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "%s", au8PlotData);    
   }
} 

//*-----END OF FILE-------*/


/************************************************************************
 *FUNCTION:    u32AccHwInfo
 *DESCRIPTION: 1.Opens the device.
 *             2.Call OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO IOCTRL to read the hardware info.
 *             3.Close the device.
 *
 *PARAMETER:     Nil
 *
 *RETURNVALUE:   Error Number or NULL to indicate that test is passed
 *HISTORY:  
 ************************************************************************/

tU32 u32AccHwInfo(tVoid)
{
    OSAL_tIODescriptor hFd;
    tU32 u32Ret = 0;
    OSAL_trIOCtrlHwInfo sAccHwInfo = {0};
    tBool bErrorOccured = FALSE;
    tU8 u8ErrorNo = 0;

    /* Open the device in read only mode */
    hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READONLY);
    if ( OSAL_ERROR == hFd )
    {
        bErrorOccured = TRUE;
        u8ErrorNo |= 1;
    }
    else
    {
        if( OSAL_s32IOControl (hFd,OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO,(tS32)&sAccHwInfo)  == OSAL_ERROR )
        {
            bErrorOccured = TRUE;
            u8ErrorNo |= 2;
        }
        else
        {
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Acc : OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO succesful");
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Acc:Mount angle %d %d %d %d %d %d %d %d %d", sAccHwInfo.rMountAngles.u8AngRX, sAccHwInfo.rMountAngles.u8AngRY, sAccHwInfo.rMountAngles.u8AngRZ,\
																						   sAccHwInfo.rMountAngles.u8AngSX, sAccHwInfo.rMountAngles.u8AngSY, sAccHwInfo.rMountAngles.u8AngSZ,\
																						   sAccHwInfo.rMountAngles.u8AngTX, sAccHwInfo.rMountAngles.u8AngTY, sAccHwInfo.rMountAngles.u8AngTZ);
         
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - u32AdcRangeMin : %d", sAccHwInfo.rRAxes.u32AdcRangeMin);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - u32AdcRangeMax : %d", sAccHwInfo.rRAxes.u32AdcRangeMax);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - u32SampleMin : %d", sAccHwInfo.rRAxes.u32SampleMin);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - u32SampleMax : %d", sAccHwInfo.rRAxes.u32SampleMax);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MinNoiseValue : %f", sAccHwInfo.rRAxes.f32MinNoiseValue);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32EstimOffset : %f", sAccHwInfo.rRAxes.f32EstimOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MinOffset : %f", sAccHwInfo.rRAxes.f32MinOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MaxOffset : %f", sAccHwInfo.rRAxes.f32MaxOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32DriftOffset : %f", sAccHwInfo.rRAxes.f32DriftOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MaxUnsteadOffset : %f", sAccHwInfo.rRAxes.f32MaxUnsteadOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32BestCalibOffset : %f", sAccHwInfo.rRAxes.f32BestCalibOffset);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32EstimScaleFactor : %f", sAccHwInfo.rRAxes.f32EstimScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MinScaleFactor : %f", sAccHwInfo.rRAxes.f32MinScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MaxScaleFactor : %f", sAccHwInfo.rRAxes.f32MaxScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32DriftScaleFactor : %f", sAccHwInfo.rRAxes.f32DriftScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32MaxUnsteadScaleFactor : %f", sAccHwInfo.rRAxes.f32MaxUnsteadScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32BestCalibScaleFactor : %f", sAccHwInfo.rRAxes.f32BestCalibScaleFactor);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32DriftOffsetTime : %f", sAccHwInfo.rRAxes.f32DriftOffsetTime);
           OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Acc : RAxes - f32DriftScaleFactorTime : %f", sAccHwInfo.rRAxes.f32DriftScaleFactorTime);
        }
    }

    if( ( hFd != OSAL_ERROR ) && ( OSAL_ERROR == OSAL_s32IOClose( hFd ) ) )
    {
        bErrorOccured = TRUE;
        u8ErrorNo |= 4;
    }

    if( FALSE == bErrorOccured )
    {
        u32Ret = 0;
    }
    else
    {
        u32Ret = u8ErrorNo;
    }

    return u32Ret;
}


/************************************************************************
 *FUNCTION:    tu32AccOpnClsReg 
 *
 *DESCRIPTION:  1.Opens the device.
 *              2.Close the device.
 *              Imp: Full project autosar must be flashed on the target
 *              Note: To check if thread spawn fails or not.
 *PARAMETER:    Nil
 *
 *RETURNVALUE:  Error Number or 0 to indicate that test is passed
 *
 *HISTORY:      14.07.2015 - Srinivas Prakash Anvekar -Initial Revision.
 *
 * ************************************************************************/
tU32 tu32AccOpnClsReg(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32ErrorCode = 0;
   tU8 u8Index = 0;

   do
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
      if( hDevice == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "tu32AccOpnClsReg: Acc device open failed ");
         u32ErrorCode = 1;
      }
      
      else
      {
         if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
         {
            u32ErrorCode = 2;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS, "tu32AccOpnClsReg: Device Closing Failed at "
                    "Line : %d ErrorCode : %d ", __LINE__, OSAL_u32ErrorCode() );
         }
      }
      
      u8Index++;
      
   }while( (u8Index < 20) && (u32ErrorCode == 0) );

   return u32ErrorCode;
}



