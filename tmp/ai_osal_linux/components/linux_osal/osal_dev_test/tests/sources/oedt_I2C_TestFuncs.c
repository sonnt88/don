/* ******************************************************FileHeaderBegin** */
/* *   
 * @file        oedt_I2C_TestFuncs.c
 *
 *  O(E)DT test for the osal I/O- driver drv_i2c
 *  For the driver some devices can be tested:
 *    --/dev/i2c/tda7564
 *
 *------------------------------------------------------------------------------
 * HISTORY:
 * ____________________________________________________________________________
 *				 02 June 2006 ver  1.1  RBIN/ECM1-Narasimha Prasad 
 *               some more functions are added to the file
 *------------------------------------------------------------------------------
 *				 08 June 2006  ver 1.2  RBIN/ECM1-Narasimha Prasad 
 *               Updated after review comments
  *------------------------------------------------------------------------------
 *				 08 June 2006  ver 1.3  RBIN/ECM1-Narasimha Prasad 
 *               ISP1301 Code is removed and M24C64 code is added
 *------------------------------------------------------------------------------
 * 			     05 October 2006 ver 1.4 RBIN/ECM1-Rakesh Dhanya
 *               The following two cases were modified 
 *               tU32 u32I2C_M24C64AutoTest(void), 
 *				 tU32 u32I2C_TDA7564AutoTest(void)
 * ------------------------------------------------------------------------------
 * 			     06,October,2006 ver 1.5 Haribabu Sannapaneni (RBIN/ECM1)
 *               New test cases are added to improve TCA coverage. 
 *------------------------------------------------------------------------------
 * 			     16 October 2006 ver 1.6 RBIN/ECM1-Narasimha Prasad 
 *               The following cases were modified 
 *               tU32 u32I2C_M24C64AutoTest(void), 
 *				 tU32 u32I2C_TDA7564AutoTest(void)
 *				 TU_OEDT_I2C_009,11,13,15,17,19,21,23,24
 *				 ver 1.7 Venkateswara.N November 02,2006 
 *               Few test cases are updated 
 *-------------------------------------------------------------------------------
 *				19th, December 2006 ver 2.0 RBIN/ECM1 - Pradeep Chand C
 *				Functions modified as per the comments from Mr. Andreas Diesner
 *				a) All cases related to M24C64 as it does not exist on the GMGE H/W
 * 				b) Some cases commented due to the reason: the functionality cannot 
 * 				   be achieved just	using osal functions.  	
 *-------------------------------------------------------------------------------
 *				8th, January 2007 ver 2.1 RBIN/ECM1 - Pradeep Chand C
 *				Function OEDT_I2C_FReadRegisterTDA() modified as per comments from
 *				Mr. Andreas Diesner.
 *-------------------------------------------------------------------------------
 *				2nd Feb 2007 ver 2.2 RBIN/ECM1 - Sandeep M Joshi
 *				Added the test function  u32I2CGetVersion()
 *-------------------------------------------------------------------------------
				25 June 2007 ver 2.3 RBIN/EDI3 -Kishore Kumar.R
		        Added new test cases  TU_OEDT_I2C_027,028,029,030.
 *--------------------------------------------------------------------------------
 *				26 Sept 2007 ver 2.4 RBIN/EDI3 -Tinoy Mathews
 *              Updated cases :	 
 *								TU_OEDT_I2C_001,002,003,004,005,007,
 *								009,011,013,015,018,020
 *--------------------------------------------------------------------------------
 *				18 Sept 2008t ver 2.5 RBIN/ECM1 -Anoop Chandran
 *              Updated cases :	 TU_OEDT_I2C_018
 *--------------------------------------------------------------------------------
 *				27 Jan 2009 ver 2.6 RBEI/ECF1 -Haribabu
 *              Added New test case to test FPGA
 *--------------------------------------------------------------------------------
 *				25 Mar 2009 ver 2.7 RBEI/ECF1 -Sainath Kalpuri
 *              Removed lint and compiler warnings
 *--------------------------------------------------------------------------------
 *				22 Apr 2009 ver 2.8 RBEI/ECF1 -Sainath Kalpuri
 *              Removed lint and compiler warnings
 *--------------------------------------------------------------------------------


 										   EDT_I2C_Getversion() 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
 !!! TRACE Calls from test functions are NOT ALLOWED !!! 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
//#define vTtfisTrace(...)

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_I2C_TestFuncs.h"

static tU32 u32I2CCallbackTDA7564(tU16  u16CallbackId);
/*functions for test TDA7564 */
static tU32  OEDT_I2C_FTestTDAAutomatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32  OEDT_I2C_FWriteRegisterTDA(OSAL_tIODescriptor PI2cDevice, OEDT_I2C_Testmode PeTestMode,tU32 Pu32Ret );
static tU32  OEDT_I2C_FReadRegisterTDA(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);

static  tU32  OEDT_I2C_FTestADV1Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32   OEDT_I2C_FWriteRegisterADV1(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret);
static tU32   OEDT_I2C_FReadRegisterADV1(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);

static  tU32  OEDT_I2C_FTestADV2Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32   OEDT_I2C_FWriteRegisterADV2(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret);
static tU32  OEDT_I2C_FReadRegisterADV2(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);

static  tU32  OEDT_I2C_FTestFPGAAutomatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32   OEDT_I2C_FWriteRegisterFPGA(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret);
static tU32  OEDT_I2C_FReadRegisterFPGA(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);

static OSAL_trI2CReqCallback   OEDT_trI2CCallBack;

 OSAL_trI2CReqCallback VtsCallbackpointer;
 OSAL_tenI2CStateChan          StateChannel; 

/* **************************************************FunctionHeaderBegin***** 
 *
 *  tU32 u32I2CTDA7564AutoTest(tVoid)
 *
 *  This test open the channel of the device TDA7564 for mute, 
 *  demute and read the register. 
 *   This test run for several timeout's and several clockspeed
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-12
 *
 * @note
 *      
 * TEST CASE:    TU_OEDT_I2C_001
 * @history:
 * function updated with callback inclusion Narasimha prasad palasani 6 JUN 2006 
 * Updated by Pradeep Chand C(RBIN/ECM1), 19th Dec 06-Commented Callback regn 
 * Updated by Tinoy Mathews(RBIN/EDI3), 26th Sept 07-Added failure case for 
 * 										getting the version information. 
 * ***********************************************FunctionHeaderEnd***********/
tU32 u32I2CTDA7564AutoTest(tVoid)
{
	tU32				Vu32Ret = 0;
  	tS16                VtS16Version = 0;
  	OSAL_tIODescriptor  Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_TDA7564,
  	 OSAL_EN_READWRITE);
	 if(Vhi2cDeviceTDA7564 == OSAL_ERROR)
	 {
		return 1;
	 }
  /* check version of the driver */
   if(OSAL_s32IOControl(Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_VERSION,
	(tS32)(&VtS16Version))==OSAL_OK)
	{
		if(VtS16Version != I2C_C_S32_IO_VERSION)
		{
			Vu32Ret += 4;
		}
	}
  	else
	{
	  Vu32Ret += 8;	 
	}
    /* Call function test */
	Vu32Ret = OEDT_I2C_FTestTDAAutomatic(Vhi2cDeviceTDA7564,Vu32Ret);
#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'This functionality just registers the callback, but doesn't do 
	anything else with it... so it isn't really testing the callback.'
*/
	/*callback*/
	OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_WRITE;
    OEDT_trI2CCallBack.pCallback = u32I2CCallbackTDA7564;

    if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)
	{
      	Vu32Ret += 20;  
    }
	 					
	 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_READ;
	 OEDT_trI2CCallBack.pCallback = u32I2CCallbackTDA7564;
	 if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)
	 {
       		Vu32Ret += 100;  
     }
#endif
	/* Close the device */
  if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
	{
    Vu32Ret += 200;   
	}
	return(Vu32Ret);
}/*End function*/
/***************************************************FunctionHeaderBegin********
 *
 *  static  tU32  OEDT_I2C_FTestTDAAutomatic(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-14
 *
 * @note
  ***************************************************FunctionHeaderEnd*********/
static  tU32  OEDT_I2C_FTestTDAAutomatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
{
  Osal_tenI2CClockSpeed   VIncSpeed;
  tS32                    VIncTimeout;
  
 
  for (VIncTimeout = 1500;VIncTimeout >= 500;VIncTimeout -= 500) 
  { /*IF NO ERROR */
		if (Pu32Ret == 0)
		{	/*set timeout */
			if (OSAL_s32IOControl(PI2cDevice,
			OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,
			VIncTimeout)!=OSAL_OK)
			{
				Pu32Ret += 700;
			}/*end if*/
			else if (OSAL_s32IOControl(PI2cDevice,
			OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!=OSAL_OK)
			{
				Pu32Ret += 1400;
			}/*end if*/
			if (Pu32Ret == 0)
			{ /*for various speed */
				for (VIncSpeed=OSAL_I2C_CLK_SPEED_50K;
				VIncSpeed < OSAL_I2C_CLK_SPEED_400K;VIncSpeed++) 
				{ /*set ClkSpeed*/
					if (OSAL_s32IOControl(PI2cDevice,
					OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_OK)
					{
						Pu32Ret += 75;
					}/*end if*/
					else
					{ /* Testsequenz*/
						/* Mute */
					  	
						Pu32Ret = OEDT_I2C_FWriteRegisterTDA(PI2cDevice,
						I2C_IC_TEST_MODE_TDA_MUTE,Pu32Ret);
						/*  WAIT */
						OSAL_s32ThreadWait(300);/*300ms*/
						/* Demute */
						Pu32Ret=OEDT_I2C_FWriteRegisterTDA(PI2cDevice,
						I2C_IC_TEST_MODE_TDA_DEMUTE,Pu32Ret);
						/* Read */
						Pu32Ret = OEDT_I2C_FReadRegisterTDA(PI2cDevice,Pu32Ret);
						/* WAIT */
						OSAL_s32ThreadWait(300); /*300ms*/
					   				
					}/*end if*/
				}/*end for*/
			}/*end if*/
		}/*end if*/
  }/*end for*/
	return(Pu32Ret);
}/*end function*/

/****************************************************FunctionHeaderBegin******
 *
 *  static tU32   OEDT_I2C_FWriteRegisterTDA(OEDT_I2C_Testmode PeTestMode,
 * OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PeTestMode
 *      �Info�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-14
 *
 * @note
 *      
 **************************************************FunctionHeaderEnd********/
static tU32   OEDT_I2C_FWriteRegisterTDA(OSAL_tIODescriptor PI2cDevice,
OEDT_I2C_Testmode PeTestMode,tU32 Pu32Ret)
{
	OSAL_trI2CWriteData     VtrWriteData;
  tU8											Vi8TxDataArr[2];
  tS32										Vi32Num = 0; 

  if (PeTestMode == I2C_IC_TEST_MODE_TDA_MUTE)
  {
    Vi8TxDataArr[0] = 0x40;
    Vi8TxDataArr[1] = 0x10;  
  }/*end if*/
  else if (PeTestMode == I2C_IC_TEST_MODE_TDA_DEMUTE)
  {/*demute*/
    Vi8TxDataArr[0] = 0x5e;
    Vi8TxDataArr[1] = 0x10;  
  }/*end else*/

	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;
  /* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_TDA7564_IB);

  /* ERROR? */
  if (Vi32Num < 0)
	{
		Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != NUM_REG_TDA7564_IB)
	{/*TImeout Error*/
		Pu32Ret += 500;
	}/*end else if*/
	return(Pu32Ret);
}/*end function*/


/* **************************************************FunctionHeaderBegin*******
 *
 *  static tU32  OEDT_I2C_FReadRegisterTDA(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-14
 *
 * @note	a) Write to TDA funtion has to be called before this Read TDA, 
 *			   the reason being that here the response of read data from TDA7564 
 *			   is always 4 bytes that contains some diagnostic information, so we 
 *			   will never be able to read the bytes that is written.
 *			b) This function just tests for "Diagnostic status (= IB1 - D6)" 
 *			   [Byte 3 Bit 7 as provided in the TDA7564 Technical specification]
 *			   which are being set by the previous funtion of write to TDA7564 that
 *			   enables "Diagnostic enable (D6 = 1)" [Byte 1 Bit 7].
 *
 * @history:	Updated by Pradeep Chand(RBIN/ECM1), 8th January 2007 - As per the 
 * 				comments from Mr. Andreas Diesner for reading data bytes from TDA7564
 *
 *************************************************FunctionHeaderEnd***********/

static tU32  OEDT_I2C_FReadRegisterTDA(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
	OSAL_trI2CReadData      VtrReadData;
//OSAL_trI2CWriteData     VtrWriteData;
  tU8											Vi8Inc;
	tU8	Vi8RxDataArr[NUM_REG_TDA7564_DB], DiagnosticTest;
  	tS32 Vi32Num = 0, testStatus = 0; 

  /* Get ready for reading */
  for (Vi8Inc = 0; Vi8Inc < NUM_REG_TDA7564_DB; Vi8Inc++)
  {
    Vi8RxDataArr[Vi8Inc] = 0;
  }/*end for*/

	VtrReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   VtrReadData.i2cRxBuffer = &Vi8RxDataArr[0];

	/*read data */
  Vi32Num=OSAL_s32IORead(PI2cDevice,(tS8 *)&VtrReadData,NUM_REG_TDA7564_DB);   
    
  	/*
  	The Diagnostic status is being checked here, if the 7th bit(D6 of DB3) 
  	of byte 3 is 1 - Diagnostics is enabled and the test would pass, 
  	otherwise the test fails
  	*/

  	DiagnosticTest = Vi8RxDataArr[2]& 0x40;

	if(DiagnosticTest == 0x40)
	{
		testStatus = 1;
	}
	else
	{
		Pu32Ret += 100;
	}

  /* ERROR */
   	if (Vi32Num >= 0 && testStatus == 1)
  {/*check read data */
		if (Vi32Num != NUM_REG_TDA7564_DB)
      Pu32Ret += 400;
		/*TODO check if data wrong*/

  }/*end if*/
  	else //if(Vi32Num < 0)
	{
	  Pu32Ret += 16;
	}/*end else*/    
	return(Pu32Ret);
}/*end function*/


/****************************************************FunctionHeaderBegin*******
 *
 *  tU32 u32I2C_M24C64AutoTest(tVoid)
 *
 *  This test open the channel of the device M24C64 for write the 
 *  register and for read the register of the M24C64.
 *  The receive read data will be compare with the write data. 
 *  This test run for several timeout's and several clockspeed�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-12
 *
 * @note
 *      
 ***************************************************FunctionHeaderEnd*********/
#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
tU32 u32I2C_M24C64AutoTest(tVoid)
{
  tU32							  Vu32Ret = 0;
	tS16                VtS16Version;
  OSAL_tIODescriptor  Vhi2cDeviceM24C64;


	/* open the device;  */
  Vhi2cDeviceM24C64 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_M24C64,
   OSAL_EN_READWRITE);
	if (Vhi2cDeviceM24C64 == OSAL_ERROR)
	{
	Vu32Ret +=	666; 
   
	}

else
    {
	/*error?*/
	 
	

  /* check version of the driver */
	if(OSAL_s32IOControl(Vhi2cDeviceM24C64,
	OSAL_C_S32_IOCTRL_VERSION,(tS32)(&VtS16Version)) == OSAL_OK)
	{
		if(VtS16Version !=I2C_C_S32_IO_VERSION)
		{
				Vu32Ret += 4;
		}/*end if*/
	}/*end if*/
   /*	else
	  Vu32Ret += 8;	*/
  /* call function test */
	Vu32Ret=OEDT_I2C_FTestM24C64Automatic(Vhi2cDeviceM24C64,Vu32Ret);

	/*callback*/

		OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_WRITE;
		OEDT_trI2CCallBack.pCallback = u32I2CCallbackM24C64;

    if(OSAL_s32IOControl(Vhi2cDeviceM24C64,
    	OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)
	{
    		Vu32Ret += 3333;
    }                      
	 					
		OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_READ;
		OEDT_trI2CCallBack.pCallback = u32I2CCallbackM24C64;

	if(OSAL_s32IOControl(Vhi2cDeviceM24C64,
		OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)
	{
	 		Vu32Ret += 5555;  
    }

	/* close the device */
  if(OSAL_s32IOClose(Vhi2cDeviceM24C64)!= OSAL_OK)
	{
    		Vu32Ret += 200;   
	}/*end switch*/
	} /*end else*/
	return(Vu32Ret);
}/*end function*/


/***************************************************FunctionHeaderBegin********
 *
 *  static tU32  OEDT_I2C_FTestM24C64Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-18
 *
 * @note
 *TEST CASE:    TU_OEDT_I2C_001
 * function updated with callback inclusion Narasimha prasad palasani 6 JUN 2006    
 ************************************************FunctionHeaderEnd************/
static tU32  OEDT_I2C_FTestM24C64Automatic(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
	Osal_tenI2CClockSpeed   VIncSpeed;
    tU32                    VIncTimeout;
  
 
  for (VIncTimeout = 150;VIncTimeout >= 50;VIncTimeout -= 50) 
  { /*IF NO ERROR */
		if (Pu32Ret == 0)
		{	/*set timeout */
			if (OSAL_s32IOControl(PI2cDevice,
			OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,VIncTimeout)!= OSAL_OK)
			{
				Pu32Ret += 700;
			}/*end if*/
			else if (OSAL_s32IOControl(PI2cDevice,
			OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!= OSAL_OK)
			{
				Pu32Ret += 1400;
			}/*end if*/
			if (Pu32Ret == 0)
			{ /*for various speed */
				for (VIncSpeed = OSAL_I2C_CLK_SPEED_50K;
				VIncSpeed <= OSAL_I2C_CLK_SPEED_100K;VIncSpeed++) 
				{ /*set ClkSpeed*/
					if (OSAL_s32IOControl(PI2cDevice,
					OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed) != OSAL_OK)
					{
						Pu32Ret += 0075;
					}/*end if*/
					else
					{ /* Testsequenz*/
						/* Read and Compare IDs*/
						Pu32Ret = OEDT_I2C_FReadAndCompareIdM24C64(PI2cDevice,
						Pu32Ret);
						/* write and read one byte */
						Pu32Ret = OEDT_I2C_FWriteReadOneByteM24C64(PI2cDevice,Pu32Ret);
						/* Read all bytes*/
						Pu32Ret = OEDT_I2C_FReadByteM24C64(PI2cDevice,
						M24C64_VENDOR_ID,M24C64_NUM_REG,Pu32Ret);
						/* Todo compare the bytes */
					}/*end if*/
				}/*end for*/
			}/*end if*/
		}/*end if*/
  }/*end for*/
	return(Pu32Ret);
}/*end function*/

/***************************************************FunctionHeaderBegin********
 *
 *  static tU32  OEDT_I2C_FReadAndCompareIdM24C64(OSAL_tIODescriptor PI2cDevice,
 *tU32 Pu32Ret)
 *
 *  �LongInfo�
 *
 * @param   PTestParam
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-18
 *
 * @note
 *      
 ************************************************FunctionHeaderEnd************/
static tU32  OEDT_I2C_FReadAndCompareIdM24C64(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
	tU16 Vu16Id;

	/*-------------------------------------------------------------*/
	/*read VENDOR ID*/
	Pu32Ret = OEDT_I2C_FReadByteM24C64(PI2cDevice,M24C64_VENDOR_ID,2,Pu32Ret);
	/*error?*/
	if(Pu32Ret == 0)
	{ /*get Id */		
		Vu16Id  = I2C_u8RxDataArr[0];
    Vu16Id |= (I2C_u8RxDataArr[1]<<8);
		/*Compare*/
    if(M24C64_VENDORID != Vu16Id)
		{/*error*/
	    Pu32Ret += 8888;
		}/*end if*/
	  /*-------------------------------------------------------------*/
  	/*read Product ID */
	  Pu32Ret = OEDT_I2C_FReadByteM24C64(PI2cDevice,M24C64_PRODUCT_ID,2,Pu32Ret);
		/*error?*/
		if(Pu32Ret == 0)
		{ /*get Id */		
			Vu16Id  = I2C_u8RxDataArr[0];
			Vu16Id |= (I2C_u8RxDataArr[1]<<8);
			/*Compare*/
			if(M24C64_PRODUCTID != Vu16Id)
			{/*error*/
	      Pu32Ret += 8888;		
			}/*end if*/
			Pu32Ret = OEDT_I2C_FReadByteM24C64(PI2cDevice,
			M24C64_VERSION_ID,2,Pu32Ret);
			/*error?*/
			if(Pu32Ret==0)
			{ /*get Id */		
				Vu16Id  = I2C_u8RxDataArr[0];
				Vu16Id |= (I2C_u8RxDataArr[1]<<8);
				/*Compare*/
				if(M24C64_VERSIONID != Vu16Id)
				{/*error*/
				   Pu32Ret += 8888;		
				}/*end if*/
			}/*end if*/
		}/*end if*/
	}/*END IF*/
  return(Pu32Ret);
}/*end function*/

/****************************************************FunctionHeaderBegin*******
 *
 *  static STATUS   I2C_FWriteReadOneByteM24C64(I2C_ParamTest* PTestParam);
 *
 *  write one byte to register and then read this register.
 *  Are the bits set?
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      status: < 0 ERROR else NU_SUCCESS
 *
 * @date    2005-07-20
 *
 * @note
 *      
 ************************************************FunctionHeaderEnd************/
static tU32   OEDT_I2C_FWriteReadOneByteM24C64(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
{
	/*Write one byte to mode control register 1*/
	Pu32Ret = OEDT_I2C_FWriteOneByteM24C64(PI2cDevice,
	M24C64_MODE_CTRL1_SET,0x0c,Pu32Ret);	/*TRANSP_EN =1 and DAT_SEO=1 */
	/* if no error */
	if(Pu32Ret == 0)
	{ /*read the byte */
		Pu32Ret = OEDT_I2C_FReadByteM24C64(PI2cDevice,
		M24C64_MODE_CTRL1_SET,1,Pu32Ret);
		/*if no error*/
    if(Pu32Ret ==0 )
		{/*compare data*/
      if((I2C_u8RxDataArr[0] & 0x0c) != 0x0c)
			{ /*Error*/
				Pu32Ret += 8888;						
			}/*end if*/
		}/*end if*/
	}/*end if*/
  return(Pu32Ret);
}/*end function*/

/***************************************************FunctionHeaderBegin********
 *
 *  static tU32   OEDT_I2C_FReadByteM24C64(Uint8 Pu8SubAddr,
 *Uint8 Pu8Number,tU32 Pu32Ret)
 *
 *  �LongInfo�
 *
 * @param   Pu8SubAddr
 *      �Info�
 *
 * @param   Pu8Number
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    2005-07-19
 *
 * @note
 *      
 ************************************************FunctionHeaderEnd************/
static tU32   OEDT_I2C_FReadByteM24C64(OSAL_tIODescriptor PI2cDevice,
tU8 Pu8SubAddr,tU8 Pu8Number,tU32 Pu32Ret)
{
  OSAL_trI2CReadData      VtrReadData;
	tU8											Vu8Inc;
	tS32										Vi32Num = 0; 

  /*if read a byte the register address must set*/
  Pu32Ret = OEDT_I2C_FWriteRegisterAddrM24C64(PI2cDevice,Pu8SubAddr,Pu32Ret);

	if(Pu32Ret ==0 )
	{
		/* Get ready for reading */
		for(Vu8Inc = 0;Vu8Inc<Pu8Number;Vu8Inc++)
		{
			I2C_u8RxSubAddrArr[Vu8Inc] = Pu8SubAddr+Vu8Inc;
			I2C_u8RxDataArr[Vu8Inc]    = 0;				/* clear data   */
		}/*end for*/

		VtrReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
		VtrReadData.i2cRxBuffer = &I2C_u8RxDataArr[0];

		/*read data */
		Vi32Num=OSAL_s32IORead(PI2cDevice,(tS8 *)&VtrReadData,Pu8Number);   

		/* ERROR */
		if (Vi32Num >= 0)
		{/*check read data */
		  if (Vi32Num != Pu8Number)
	  	  Pu32Ret += 400;
		}/*end if*/
		else if(Vi32Num < 0)
		{
		  Pu32Ret |= OEDT_I2C_E_READ_FAILS;
		}/*end else*/    		  
	}/*end if*/
	return(Pu32Ret);
}/*end function*/


/***************************************************FunctionHeaderBegin********
 *
 *  static tU32  OEDT_I2C_FWriteRegisterAddrM24C64(tU8 Pu8Addr,tU32 Pu32Ret);
 *
 *  write the address register to the M24C64. From this address the data will
 *  be read
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @param   Pu8Addr
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      status: < 0 ERROR else NU_SUCCESS
 *
 * @date    2005-07-20
 *
 * @note
 *      
 ************************************************FunctionHeaderEnd*************/
static tU32  OEDT_I2C_FWriteRegisterAddrM24C64(OSAL_tIODescriptor PI2cDevice,
tU8 Pu8Addr,tU32 Pu32Ret)
{
	OSAL_trI2CWriteData     VtrWriteData;
  tS32                    Vi32Num = 0;

	/* Get ready for reading */
  I2C_u8RxDataArr[0]    = Pu8Addr;     /* addr data   */
  
  VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = I2C_u8RxDataArr;
  /* Output */
	Vi32Num = OSAL_s32IOWrite(PI2cDevice,(tS8 *)&VtrWriteData,1);

  /* ERROR? */
  if (Vi32Num < 0)
	{
		Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != 1)
	{/*TImeout Error*/
		Pu32Ret +=500;
	}/*end else if*/
	return(Pu32Ret);
}/*end function*/

/* **************************************************FunctionHeaderBegin*******
 *
 *  static tU32   OEDT_I2C_FWriteOneByteM24C64(OSAL_tIODescriptor PI2cDevice,
 *tU8 Pu8SubAddr,tU8 Pu8Data,tU32 Pu32Ret)
 *
 *  write one byte to the function I2C_FI2COutputSubAddr
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @param   Pu8SubAddr
 *      �Info�
 *
 * @param   Pu8Data
 *      �Info�
 *
 * @param   Pu32Ret
 *      �Info�
 *
 * @return
 *      status: < 0 ERROR else NU_SUCCESS
 *
 * @date    2005-07-20
 *
 * @note
 *      
 ************************************************FunctionHeaderEnd************/
static tU32   OEDT_I2C_FWriteOneByteM24C64(OSAL_tIODescriptor PI2cDevice,
tU8 Pu8SubAddr,tU8 Pu8Data,tU32 Pu32Ret)
{
  OSAL_trI2CWriteData     VtrWriteData;
  tS32                    Vi32Num = 0;

	/* Get ready for reading */
  I2C_u8RxDataArr[0]    = Pu8Data;     /* data   */
  I2C_u8RxSubAddrArr[0] = Pu8SubAddr;  /* Register*/
    
  VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE_SUB_ADDR;  
	VtrWriteData.i2cTxBuffer = I2C_u8RxDataArr;
	VtrWriteData.i2cSubaddr = I2C_u8RxSubAddrArr;
  VtrWriteData.i2cNumberofSubaddr = 1;
  /* Output */
	Vi32Num = OSAL_s32IOWrite(PI2cDevice,(tS8 *)&VtrWriteData,1);
			
	/* ERROR? */
  if (Vi32Num < 0)
	{
		Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != 1)
	{/*TImeout Error*/
		Pu32Ret += 500;
	}/*end else if*/
	return(Pu32Ret);
}/*end function*/

#endif	//End of Commented functions of M24C64

/*****************************************************************************
* FUNCTION:		u32I2CMultiDevOpen( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_002
* DESCRIPTION:  Opens the device channels for 2 times.
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Venkateswara.N November 02, 2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/

tU32 u32I2CMultiDevOpen(tVoid)
{
	OSAL_tIODescriptor hDevice[2] = {0};
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
    
    /*TDA7564*/
    hDevice[0] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_I2C_TDA7564,enAccess );
   	if ( hDevice[0] == OSAL_ERROR )
    	{
			u32Ret = 1;
    	}
    else
    {		
    	hDevice[1] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_I2C_TDA7564,enAccess );
	   	if ( hDevice[1] != OSAL_ERROR )
    {		
			u32Ret = 10;
			if( OSAL_s32IOClose ( hDevice[1] ) == OSAL_ERROR )
		{
		   		u32Ret += 20;
 		}
	}
		if( OSAL_s32IOClose ( hDevice[0] ) == OSAL_ERROR )
    	{
		   	u32Ret += 2;
    	}
    }

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	/* M24C64 */
	hDevice[0] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_I2C_M24C64,enAccess );
   	if ( hDevice[0] == OSAL_ERROR )
    {		
			u32Ret += 3;
	}
    else
		{
    	hDevice[1] = OSAL_IOOpen( OSAL_C_STRING_DEVICE_I2C_M24C64,enAccess );
	   	if ( hDevice[1] != OSAL_ERROR )
   		{
			u32Ret = 30;
			if( OSAL_s32IOClose ( hDevice[1] ) == OSAL_ERROR )
			{
		   		u32Ret = 50;
 			}
		}
		if( OSAL_s32IOClose ( hDevice[0] ) == OSAL_ERROR )
		{
		   	u32Ret += 5;
 		}
	}

#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2COpenDevDiffModes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_003
* DESCRIPTION:  Opens the Device Channels in READONLY,READWRITE,WRITEONLY 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/

tU32 u32I2COpenDevDiffModes(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_I2C_INIT_VAL;
	tU32 u32Ret = 0;
    
    /* In Read Only mode */
    enAccess = OSAL_EN_READONLY;
	/* TDA7564 */
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	/* In Read Only mode */
	/* M24C64*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 4;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret +=8;
		}
	}
	   	 
#endif
	   	 
	/* In Read Write mode */
	enAccess = OSAL_EN_READWRITE;
	/* TDA7564 */

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 15;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	/* In Read Write mode */
	/* M24C64*/
 	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 80;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 150;
		}
	}

#endif

	/* In Write Only mode */
	enAccess = OSAL_EN_WRITEONLY;
	/* TDA7564 */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 300;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 700;
		}
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/* In Write Only mode */
	/* M24C64 */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 1300;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 3000;
		}
	}

#endif
 	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CDevOpenInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_004
* DESCRIPTION:  Opening the device channels with invalid parameter 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26,2007
******************************************************************************/ 
 
tU32 u32I2CDevOpenInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = (OSAL_tenAccess)INVALID_VAL;
	tU32 u32Ret = 0;
        
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if ( hDevice != OSAL_ERROR )
    {
    	u32Ret = 1;

		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}

    }
    return u32Ret;
}                                    

//#if 0
///*
//	Commented by Pradeep Chand C (RBIN/ECM1)
//	Reason:  Test crashes
//	( Test Case is not recommended as per the comments from eSol)
//	Comment from eSol:
//	Please note that our providing C libraries are based on ISO C/POSIX standards, and these specifications do not clearly define the treatment of propriety of the file handles.
//	For your information, I examined your example on Windows environment, and an exception was asserted on also this environment.
//*/
//
///*****************************************************************************
//* FUNCTION:		u32I2CDevOpenNullParam( )
//* PARAMETER:    none
//* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
//* DESCRIPTION:  Opening the device channels with NULL handle
//* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006
//*
//******************************************************************************/
//
//tU32 u32I2CDevOpenNullParam(tVoid)
//{
//	OSAL_tIODescriptor hDevice = 0;
//    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
//	tU32 u32Ret = 0;
//
//    hDevice = OSAL_IOOpen ( OSAL_NULL , enAccess );
//
//    if ( hDevice != OSAL_ERROR )
//    {
//    	u32Ret = 1;
//
//		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
//		{
//			u32Ret += 10;
//		}
//
//    }
//    return u32Ret;
//}
//
//#endif

/*****************************************************************************
* FUNCTION:		u32I2CDevCloseInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_005
* DESCRIPTION:  Opening the device channels with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/

tU32 u32I2CDevCloseInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = INVALID_VAL;
	
	if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
	{
		return 1;
	}

	return 0;

} 

/*****************************************************************************
* FUNCTION:		u32I2CDevReClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_006
* DESCRIPTION:  closing the device channels,which are already closed.
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CDevReClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

    /*TDA7564*/      
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
			if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
			{
				u32Ret = 3;
			}
		}
			
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/ 
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
			if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
			{
				u32Ret += 20;
			}
		}
			
	}
#endif

	return u32Ret;
}
 
/*****************************************************************************
* FUNCTION:		u32I2CWriteDataInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_007
* DESCRIPTION:  Try to  write data with incorrect parameters  
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3), Sept 26, 2007
******************************************************************************/
 
tU32 u32I2CWriteDataInvalParam(tVoid)
{	
	OSAL_trI2CWriteData     VtrWriteData;
  	tU8						Vi8TxDataArr[2];
   	OSAL_tIODescriptor hDevice = INVALID_VAL;
   	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
	tU32 u32Ret = 0;
	Vi8TxDataArr[0] = 0x40;
    Vi8TxDataArr[1] = 0x10;  
 	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

    if(OSAL_s32IOWrite(hDevice,(tS8 *)&VtrWriteData,NUM_REG_TDA7564_IB)!= OSAL_ERROR)
	{
	   u32Ret = 1;
	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if(OSAL_s32IOWrite(hDevice,(tS8 *)&VtrWriteData,(tU32)I2C_INVALID_PARAM)!= OSAL_ERROR)
	{
	   u32Ret += 2;
	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	else
	{
		u32Ret += 40;
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if(OSAL_s32IOWrite(hDevice,(tS8 *)&VtrWriteData,I2C_INVALID_PARAM)!= OSAL_ERROR)
	{
	   		u32Ret += 100;
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}
	else
	{
		u32Ret += 500;
	}

#endif

    return(u32Ret);
}
	

/*****************************************************************************
* FUNCTION:		u32I2CWriteDataAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_008
* DESCRIPTION:  Try to write data to TDA7564 after closing the Channel  
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CWriteDataAfterDevClose(tVoid)
{
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	OSAL_trI2CWriteData     VtrWriteData;
  	tU8						Vi8TxDataArr[2];
   	Vi8TxDataArr[0] = 0x40;
    Vi8TxDataArr[1] = 0x10;  
 	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

    /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
	        if(OSAL_s32IOWrite(hDevice,(tS8 *)&VtrWriteData
	        ,NUM_REG_TDA7564_IB)!= OSAL_ERROR)
			{
	   			u32Ret = 3;
			}

        }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	        if(OSAL_s32IOWrite(hDevice,(tS8 *)&VtrWriteData,
	        NUM_REG_TDA7564_IB)!= OSAL_ERROR)
			{
	   			u32Ret += 20;
			}

        }
	}

#endif

	return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32I2CReadDataInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_009
* DESCRIPTION:  Try to read data from TDA7564 with incorrect parameters 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/ 

tU32 u32I2CReadDataInvalParam(tVoid)
{
   	OSAL_trI2CReadData      VtrReadData;
// 	tU8						Vi8Inc = 0;
	tU8						Vi8RxDataArr[NUM_REG_TDA7564_DB] = {0};
 	OSAL_tIODescriptor hDevice = INVALID_VAL;
 	OSAL_tenAccess enAccess = OSAL_EN_READWRITE; 
 	tU32 u32Ret = 0;

    /*for ( Vi8Inc = 0; Vi8Inc < NUM_REG_TDA7564_DB; Vi8Inc++ )
   	{
    	Vi8RxDataArr[Vi8Inc] = 0;
  	}*/

	VtrReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	VtrReadData.i2cRxBuffer = &Vi8RxDataArr[0];

	if(OSAL_s32IORead(hDevice,(tS8 *)&VtrReadData,
	NUM_REG_TDA7564_DB)!= OSAL_ERROR)
	{
	   u32Ret = 1;
	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if(OSAL_s32IORead(hDevice,(tS8 *)&VtrReadData,
	(tU32)I2C_INVALID_PARAM)!= OSAL_ERROR)
	{
	   u32Ret += 2;
	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
    if(OSAL_s32IORead(hDevice,(tS8 *)&VtrReadData,
	I2C_INVALID_PARAM)!= OSAL_ERROR)
	{
	   		u32Ret += 20;
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	else
	{
		u32Ret += 90;
	}

#endif

    return(u32Ret);
}

/*****************************************************************************
* FUNCTION:		u32I2CReadDataAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_010
* DESCRIPTION:  Try to read data from TDA7564 after closing the Channel 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/ 

tU32 u32I2CReadDataAfterDevClose(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	OSAL_trI2CReadData      VtrReadData;
  	tU8						Vi8Inc = 0;
	tU8						Vi8RxDataArr[NUM_REG_TDA7564_DB];
 	tU32 u32Ret = 0;
 	    
 	for (Vi8Inc=0; Vi8Inc < NUM_REG_TDA7564_DB; Vi8Inc++)
   	{
    	Vi8RxDataArr[Vi8Inc] = 0;
  	}

	VtrReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	VtrReadData.i2cRxBuffer = &Vi8RxDataArr[0];


    /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
	        if(OSAL_s32IORead(hDevice,(tS8 *)&VtrReadData,
	        NUM_REG_TDA7564_DB)!= OSAL_ERROR)
			{
	   			u32Ret = 3;
			}

        }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/

	for (Vi8Inc=0; Vi8Inc < NUM_REG_TDA7564_DB; Vi8Inc++)
   	{
    	Vi8RxDataArr[Vi8Inc] = 0;
  	}

	VtrReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	VtrReadData.i2cRxBuffer = &Vi8RxDataArr[0];

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	        if(OSAL_s32IORead(hDevice,(tS8 *)&VtrReadData,
	        NUM_REG_TDA7564_DB)!= OSAL_ERROR)
			{
	   			u32Ret += 20;
			}

        }
	}
#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CSetWriteBreakTimeInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_011
* DESCRIPTION:  Try to set write break time with incorrect parameters 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/ 

tU32 u32I2CSetWriteBreakTimeInvalParam(tVoid)
{
//  Osal_tenI2CClockSpeed   VIncSpeed;
	tS32                    VIncTimeout = 0;
  	tU32 u32Ret = 0;
	OSAL_tIODescriptor hDevice = INVALID_VAL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	VIncTimeout=1500;
 
   	if (OSAL_s32IOControl(hDevice,
   	OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,VIncTimeout)!=OSAL_ERROR)
	{	
		u32Ret = 1;
  	}

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if (OSAL_s32IOControl(hDevice,
   	OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,I2C_INVALID_PARAM)!=OSAL_ERROR)
	{	
		u32Ret += 2;
  	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}	

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if (OSAL_s32IOControl(hDevice,
   	OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,I2C_INVALID_PARAM)!=OSAL_ERROR)
	{	
			u32Ret += 20;
  		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	else
	{
		u32Ret += 90;
  	}

#endif

  	return u32Ret;
}	

/*****************************************************************************
* FUNCTION:		u32I2CSetWriteBreakTimeAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_012
* DESCRIPTION:  Try to set write break time after closing the Channels 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CSetWriteBreakTimeAfterDevClose(tVoid)
{  
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
//	Osal_tenI2CClockSpeed   VIncSpeed;
	tS32                    VIncTimeout = 0;
  	tU32 u32Ret = 0;
 	VIncTimeout=1500;
    /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
   			if (OSAL_s32IOControl(hDevice,
   			OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,VIncTimeout)!=OSAL_ERROR)
			{	
				u32Ret = 3;
  			}
 	    }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	        if (OSAL_s32IOControl(hDevice,
	        OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,VIncTimeout)!=OSAL_ERROR)
			{	
				u32Ret += 20;
  			}

        }
	}

#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CSetReadBreakTimeInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_013
* DESCRIPTION:  Try to set read break time with invalid parameters 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 26,2007
******************************************************************************/

tU32 u32I2CSetReadBreakTimeInvalParam(tVoid)
{
//	Osal_tenI2CClockSpeed   VIncSpeed;
	tS32                    VIncTimeout = 0;
  	tU32 u32Ret = 0;
	OSAL_tIODescriptor hDevice = INVALID_VAL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	VIncTimeout = 1500;
      
  	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!= OSAL_ERROR)
	{	
		u32Ret = 1;
  	}

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );
	
	if ( hDevice != OSAL_ERROR )
	{
	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,I2C_INVALID_PARAM)!= OSAL_ERROR)
	{	
		u32Ret += 2;
  	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}	

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,I2C_INVALID_PARAM)!= OSAL_ERROR)
	{	
			u32Ret += 20;
  		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	else
	{
		u32Ret += 90;
  	}

#endif

   	return u32Ret;
}
	

/*****************************************************************************
* FUNCTION:		u32I2CSetReadBreakTimeAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_014
* DESCRIPTION:  Try to set read break time after closing the Channel
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CSetReadBreakTimeAfterDevClose(tVoid)
{  
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
//	Osal_tenI2CClockSpeed   VIncSpeed;
	tS32                    VIncTimeout = 0;
  	tU32 u32Ret = 0;
 	VIncTimeout = 1500;
    /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
   			if (OSAL_s32IOControl(hDevice,
   			OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!= OSAL_ERROR)
			{	
				u32Ret = 3;
  			}
 	    }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	        if (OSAL_s32IOControl(hDevice,
	        OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!=OSAL_ERROR)
			{	
				u32Ret += 20;
  			}

        }
	}

#endif

	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32I2CSetClkSpeedInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_015
* DESCRIPTION:  Try to set clock speed with incorrect parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Venkateswara.N November 02, 2006
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 26,2007
******************************************************************************/

tU32 u32I2CSetClkSpeedInvalParam(tVoid)
{
  	Osal_tenI2CClockSpeed   VIncSpeed;
  	VIncSpeed = OSAL_I2C_CLK_SPEED_50K;
  	OSAL_tIODescriptor hDevice = INVALID_VAL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

  	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_ERROR)
	{	
		u32Ret = 1;
  	}
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
		VIncSpeed = (Osal_tenI2CClockSpeed)INVALID_VAL;
	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_ERROR)
	{	
		u32Ret += 2;
  	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}	

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
		VIncSpeed = -1;
	if (OSAL_s32IOControl(hDevice,
  	OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_ERROR)
	{	
			u32Ret += 20;
  		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	else
	{
		u32Ret += 90;
  	}

#endif

   	return u32Ret;
}
				
/*****************************************************************************
* FUNCTION:		u32I2CSetClkSpeedAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_016
* DESCRIPTION:  Try to set clock speed after closing the Channel
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CSetClkSpeedAfterDevClose(tVoid)
{  
	OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	Osal_tenI2CClockSpeed   VIncSpeed;
  	VIncSpeed = OSAL_I2C_CLK_SPEED_50K;
  	tU32 u32Ret = 0;
      
   /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
   		   if (OSAL_s32IOControl(hDevice,
   		   OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!= OSAL_ERROR)
		   {	
				u32Ret = 3;
  		   }
 	    }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/
 
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	       if (OSAL_s32IOControl(hDevice,
	       OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_ERROR)
		   {	
				u32Ret += 20;
  		   }

        }
	}

#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CGetVersion( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_017
* DESCRIPTION:  Get version for TDA7564 with valid parameters
* HISTORY:		Created Sandeep M Joshi (RBIN/ECM1)  Feb 02,2007 
******************************************************************************/

tU32 u32I2CGetVersion(tVoid)
{
   	tS16               VtS16Version = 0;
    OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	
	
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
		if(OSAL_s32IOControl(hDevice,
		OSAL_C_S32_IOCTRL_VERSION,(tS32)(&VtS16Version))== OSAL_ERROR)
		{
			u32Ret = 1;
  		}
		else
		{
			printf("\n Version of the I2C channel : %d", VtS16Version);
		}

		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
	}
	else
	{
		u32Ret = 100;
	}
	
	return u32Ret;
}		


/*****************************************************************************
* FUNCTION:		u32I2CGetVersionInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_018
* DESCRIPTION:  Get version for TDA7564,M24C64 with incorrect parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*               Updated by Tinoy Mathews(RBIN/EDI3) Sept 26,2007
*               Updated the parameter for IOControl
*						by Anoop Chandran(RBIN/EDI3) Sept 18,2008
******************************************************************************/

tU32 u32I2CGetVersionInvalParam(tVoid)
{
   	tS16               VtS16Version = 0;
    OSAL_tIODescriptor hDevice = INVALID_VAL;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	
	if(OSAL_s32IOControl(hDevice,
	OSAL_C_S32_IOCTRL_VERSION,(tS32)(&VtS16Version))!= OSAL_ERROR)
	{
		u32Ret = 1;
  	}

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if(OSAL_s32IOControl(hDevice,
	OSAL_C_S32_IOCTRL_VERSION,OSAL_NULL)!= OSAL_ERROR)
	{
		u32Ret += 2;
  	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}	

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	if(OSAL_s32IOControl(hDevice,
	OSAL_C_S32_IOCTRL_VERSION,I2C_INVALID_PARAM)!= OSAL_ERROR)
	{
			u32Ret += 20;
  		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 40;
		}
	}
	else
	{
		u32Ret += 90;
  	}

#endif

   	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32I2CGetVersionAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_019
* DESCRIPTION:  Get version for TDA7564 after closing the Channels
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Venkateswara.N November 02, 2006
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/


tU32 u32I2CGetVersionAfterDevClose(tVoid)
{  
	OSAL_tIODescriptor hDevice = 0;	
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tS16 VtS16Version = 0;
  	tU32 u32Ret = 0;
      
   /*TDA7564*/
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice)  == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
   		   if ( ( OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_VERSION,
   		   			(tS32)(&VtS16Version)) )!=OSAL_ERROR ) 
		   {	
				u32Ret = 3;
  		   }
 	    }
	}  

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/
 
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	       if (OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_VERSION,
	       (tS32)(&VtS16Version)!= OSAL_ERROR))
		   {	
				u32Ret += 20;
  		   }
        }
	}

#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CCallbackInvalParam( ) 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_020
* DESCRIPTION:  Callback registering for TDA7564 with invalid param
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
*				Updated by Tinoy Mathews(RBIN/EDI3) Sept 26, 2007
******************************************************************************/

tU32 u32I2CCallbackInvalParam(tVoid)
{
	 OSAL_tIODescriptor hDevice = INVALID_VAL;
	 OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	 tU32 u32Ret = 0;
	 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_WRITE;
	 OEDT_trI2CCallBack.pCallback = (OSAL_trI2CCallback)u32I2CCallbackTDA7564;

     if(OSAL_s32IOControl(hDevice,
     OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) != OSAL_ERROR)
     {
      	u32Ret = 1;  
     }
	 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_READ;
	 OEDT_trI2CCallBack.pCallback = (OSAL_trI2CCallback)u32I2CCallbackTDA7564;
	 if(OSAL_s32IOControl(hDevice,
	 OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack) != OSAL_ERROR)
     {
      	u32Ret += 2;  
     }
	 hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice != OSAL_ERROR )
	{
	 if(OSAL_s32IOControl(hDevice,
	 OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,I2C_INVALID_PARAM) != OSAL_ERROR)
     {
      		u32Ret += 5;  
    	 }
		 if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	     {
			u32Ret += 10;
		 }
	}
	else
	{
		u32Ret += 20;
     }

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/

	 hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	 if ( hDevice != OSAL_ERROR )
     {
	 if(OSAL_s32IOControl(hDevice,
	 OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,I2C_INVALID_PARAM) != OSAL_ERROR)
     {
      		u32Ret += 40;  
    	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	    {
			u32Ret += 90;
		}
	}
	else
	{
		u32Ret += 500;
     }

#endif

	 return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32I2CCallbackAfterDevClose( ) 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_021
* DESCRIPTION:  Callback registering for TDA7564 after closing channels
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

tU32 u32I2CCallbackAfterDevClose(tVoid)
{  
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;	      
   /*TDA7564*/
	OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_WRITE;
	OEDT_trI2CCallBack.pCallback = (OSAL_trI2CCallback)u32I2CCallbackTDA7564;
          
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		else
		{
   		   if (OSAL_s32IOControl(hDevice,
   		   OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,(tS32)&OEDT_trI2CCallBack)!=OSAL_ERROR)
		   {	
				u32Ret = 3;
  		   }
 	    }
	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	/*M24C64*/
	OEDT_trI2CCallBack.pCallback = u32I2CCallbackM24C64;
 
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 4;
    }
	else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 8;
		}
		else
		{
	       if (OSAL_s32IOControl(hDevice,
	       OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,
	       (tS32)&OEDT_trI2CCallBack)!= OSAL_ERROR)
		   {	
				u32Ret += 20;
  		   }
        }
	}

#endif

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32I2CIOCTRLWithInvalParam( ) 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_022
* DESCRIPTION:  Invalid function macro to IOCTRL
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/
tU32 u32I2CIOCTRLWithInvalParam(tVoid)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
	else
	{
		if(OSAL_s32IOControl(hDevice,
	 	OSAL_C_S32_IOCTRL_I2C_INVALID , I2C_INVALID_PARAM) != OSAL_ERROR)
	 	{
			u32Ret += 2;
	 	}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	    {
			u32Ret += 5;
		}

	}

#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'All cases related to M24C64 have to be commented 
	as it does not exist on the GMGE H/W'
*/
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_M24C64 , enAccess );

	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret +=10;
    }
	else
	{
	 	if(OSAL_s32IOControl(hDevice,
	 	OSAL_C_S32_IOCTRL_I2C_INVALID , I2C_INVALID_PARAM) != OSAL_ERROR)
		{
			u32Ret += 20;
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	    {
			u32Ret += 40;
		 }

	}

#endif

	return u32Ret;
}
 
			
/*****************************************************************************
* FUNCTION:		u32I2CCallbackTDA7564( ) 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Callback registering for TDA7564
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*
******************************************************************************/

static tU32 u32I2CCallbackTDA7564(tU16  u16CallbackId)
{
		switch(u16CallbackId)
		{
	  		case I2C_CALLBACK_ID_WRITE:
			     return 0;	
				 
			case I2C_CALLBACK_ID_READ:
			     return 0;
		   default:
                 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_NO;
         		 OEDT_trI2CCallBack.pCallback = OSAL_NULL;
   
		 }
		 return 0;
}

/*****************************************************************************
* FUNCTION:		u32I2CCallbackM24C64( ) 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Callback registering for TDA7564
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  May 30,2006 
*
******************************************************************************/
/*sak9kor - Below mentioned fucntion  is made as dead code to remove the compiler 
            warnings. Since the below mentioned function is a callback function,
            code which registers this function as a callback is a dead code,
            hence making this function as dead code */
#if 0
static tU32 u32I2CCallbackM24C64(tU16  u16CallbackId)
{
		switch(u16CallbackId)
		{
	  		case I2C_CALLBACK_ID_WRITE:
			     return 0;	
				 
			case I2C_CALLBACK_ID_READ:
			     return 0;
			 default:
                 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_NO;
         		 OEDT_trI2CCallBack.pCallback = OSAL_NULL;    
		 }
		 return 0;
}
#endif
#if 0
/*
	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason: As per Diesner's comment 'This functionality can not be achieved just 
	using osal functions, atleast not in the current implementation and "/dev/i2c/TDA7564"
	maps to a certain channel where you can not change anything about it from the outside'
*/

/*****************************************************************************
* FUNCTION:		u32I2COpendeviceWithInvalChanlId ( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to open device with channel ID higher value than 
                 DRV_I2C_CHANNEL_LAST
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*
******************************************************************************/

# define   OSAL_C_STRING_DEVICE_I2C_INVAL -1
tU32  u32I2COpendeviceWithInvalChanlId (tVoid)
{
	tU32 Vu32Ret = 0;
	OSAL_tIODescriptor Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_INVAL,
  	 OSAL_EN_READWRITE);
   	/*error?*/
	if(	Vhi2cDeviceTDA7564 != OSAL_ERROR )
	{
		 Vu32Ret = 1;
		 if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
		{
			Vu32Ret += 2;   
		}

	}
	return (Vu32Ret);
}

 /*****************************************************************************
* FUNCTION:		u32I2CClosedeviceWithInvalChanlId ( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to close device with channel ID higher value than  
                DRV_I2C_CHANNEL_LAST
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*
******************************************************************************/
tU32 u32I2CClosedeviceWithInvalChanlId (tVoid)
{
	tU32 Vu32Ret = 0;
	OSAL_tIODescriptor Vhi2cDeviceTDA7564 = -1;

	if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)== OSAL_OK)
		{
	    	Vu32Ret += 1;   
		}
	return (Vu32Ret);
}

#endif /* End of Commented Functions	u32I2COpendeviceWithInvalChanlId ( ) & 
		 u32I2CClosedeviceWithInvalChanlId ( ) */


/*****************************************************************************
* FUNCTION:		u32I2CGetCallbackPointer( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_023
* DESCRIPTION:  Get Callback pointer for the channel
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*				Updated by Venkateswara.N November 02, 2006
******************************************************************************/
tU32 u32I2CGetCallbackPointer(tVoid)
{
	tU32 Vu32Ret = 0;
	OSAL_tIODescriptor Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_TDA7564,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceTDA7564)
	{
	case OSAL_OK:
			break;
	case OSAL_E_NOTSUPPORTED:
			Vu32Ret = 1;
			break;
	case OSAL_E_CANCELED:
	case OSAL_E_ALREADYOPENED:
	       Vu32Ret += 2;
			break;
	case OSAL_ERROR:
		   Vu32Ret += 3;
		   break;
    default:
		  Vu32Ret += 0;

  }/*end switch*/
       /* registering the callback for write error*/	 					
	OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_WRITE;
    OEDT_trI2CCallBack.pCallback = (OSAL_trI2CCallback)u32I2CCallbackTDA7564;

if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,
                    (tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)
{
  	Vu32Ret += 10;  
}
    /* registering the callback for read error*/	 					
	 OEDT_trI2CCallBack.i2cCallbackId = I2C_CALLBACK_ID_READ;
	 OEDT_trI2CCallBack.pCallback = (OSAL_trI2CCallback)u32I2CCallbackTDA7564;
if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_I2C_SET_CALLBACK,
                       (tS32)&OEDT_trI2CCallBack) == OSAL_ERROR)

{
	        		Vu32Ret += 100;  
}

if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_I2C_GET_CALLBACK,
                    (tS32)&VtsCallbackpointer) == OSAL_OK)
{
	 if(VtsCallbackpointer.pCallback != (OSAL_trI2CCallback)u32I2CCallbackTDA7564)
	{
		Vu32Ret += 200;  
    }
}
else
{
	Vu32Ret += 300;
}

/* close the device */
if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
{
	Vu32Ret += 400;   
}
return(Vu32Ret);

}

 /*****************************************************************************
* FUNCTION:		u32I2CGetChannelState( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_024
* DESCRIPTION:  Get the state of the channel.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*				Updated by Venkateswara.N November 02, 2006
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/
 tU32  u32I2CGetChannelState(tVoid)
{
	tU32 Vu32Ret = 0;
	OSAL_tIODescriptor Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_TDA7564,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceTDA7564)
	{
    case OSAL_OK:
			break;
	case OSAL_E_NOTSUPPORTED:
			Vu32Ret += 1;
			break;
	case OSAL_E_CANCELED:
	case OSAL_E_ALREADYOPENED:
    		Vu32Ret += 2;
			break;
	case OSAL_ERROR:
		   Vu32Ret += 3;
		   break;
	default:
		  Vu32Ret += 0;
    }/*end switch*/

	if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,
	  OSAL_C_S32_IOCTRL_I2C_GET_STATE_OF_CHANNEL,(tS32)&StateChannel) == OSAL_ERROR)
	{
		Vu32Ret += 10;
	}
	else
	{
		switch(StateChannel)
		{
       		case OSAL_I2C_STATUS_CHANNEL_CLOSE:
       				break;
       		case OSAL_I2C_STATUS_CHANNEL_OPEN :
					break;
         	case OSAL_I2C_STATUS_CHANNEL_BUSY :
					break;
			default:
					Vu32Ret += 100; 
					break;
		}

	 }
	  if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
	{
   		 Vu32Ret += 200;   
	}

	return(Vu32Ret);

}

#if 0
/*
   	Commented by Pradeep Chand C (RBIN/ECM1)
	Reason:	Redundancy, the test has already been carried out
*/
 /*****************************************************************************
* FUNCTION:		u32I2C_IOCTRLWithInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Get the version with invalid Function.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*				Updated by Venkateswara.N November 02, 2006*
******************************************************************************/
#define  OSAL_C_S32_IOCTRL_INVAL_VERSION -1

tU32  u32I2C_IOCTRLWithInvalParam(tVoid)
{
	tU32 Vu32Ret = 0;
	tS16 VtS16Version = 0;
	OSAL_tIODescriptor Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_TDA7564,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceTDA7564)
	{
	case OSAL_OK:
			break;
	case OSAL_E_NOTSUPPORTED:
			Vu32Ret += 1;
			break;
	case OSAL_E_CANCELED:
	case OSAL_E_ALREADYOPENED:
    		Vu32Ret += 2;
			break;
    case OSAL_ERROR:
		   Vu32Ret += 3;
    default:
		  Vu32Ret += 0;
    
    }/*end switch*/

if(OSAL_s32IOControl( Vhi2cDeviceTDA7564,OSAL_C_S32_IOCTRL_INVAL_VERSION,
                   (tS32)&VtS16Version) != OSAL_ERROR)
{
	Vu32Ret += 10;
}
if(OSAL_s32IOControl( OSAL_C_STRING_DEVICE_I2C_INVAL,OSAL_C_S32_IOCTRL_VERSION,
                    (tS32)&VtS16Version) != OSAL_ERROR)
{
	Vu32Ret += 100;
}

if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
{
	Vu32Ret += 200;   
}


return(Vu32Ret);

}

#endif

/*****************************************************************************
* FUNCTION:		u32I2C_IOWriteRegisterTDAWithInvalParam( )
* PARAMETER:    PI2cDevice,PeTestMode and Pu32Ret.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to  write TDA device with incorrect parameters.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*
******************************************************************************/
#define OSAL_I2C_WRITE_TO_SLAVE_INVAL 3

tU32  u32I2C_IOWriteRegisterTDAWithInvalParam(OSAL_tIODescriptor PI2cDevice,
OEDT_I2C_Testmode PeTestMode,tU32 Pu32Ret)
{
	OSAL_trI2CWriteData VtrWriteData;
	tU8	Vi8TxDataArr[2]= {0};
	tS32 Vi32Num = 0; 

	if (PeTestMode == I2C_IC_TEST_MODE_TDA_MUTE)
	{
    	Vi8TxDataArr[0] = 0x40;
	    Vi8TxDataArr[1] = 0x10;  
	}/*end if*/
	else if (PeTestMode == I2C_IC_TEST_MODE_TDA_DEMUTE)
	{   /*demute*/
    	Vi8TxDataArr[0] = 0x5e;
		Vi8TxDataArr[1] = 0x10;  
	  }/*end else*/

	VtrWriteData.i2cWriteCommand = (Osal_tenI2CWriteMode)OSAL_I2C_WRITE_TO_SLAVE_INVAL;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;
	/* Output */
	Vi32Num = OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_TDA7564_IB);

	/* ERROR? */
	if (Vi32Num > 0)
	{
		Pu32Ret += 50;
		if(Vi32Num != NUM_REG_TDA7564_IB)
		{/*TImeout Error*/
			Pu32Ret += 500;
		}

		 }
	
	return(Pu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32I2C_IOReadRegisterTDAWithInvalParam( )
* PARAMETER:    PI2cDevice and Pu32Ret
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to  read from TDA device with incorrect parameters.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*
******************************************************************************/
#define OSAL_I2C_READ_FROM_SLAVE_N_ACK_INVAL 3

tU32  u32I2C_IOReadRegisterTDAWithInvalParam(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
	OSAL_trI2CReadData VtrReadData;
	tU8	Vi8Inc = 0 ;
	tU8	Vi8RxDataArr[NUM_REG_TDA7564_DB]= {0};
	tS32 Vi32Num = 0; 

	/* Get ready for reading */
	for (Vi8Inc = 0; Vi8Inc < NUM_REG_TDA7564_DB; Vi8Inc++)
	{
    	Vi8RxDataArr[Vi8Inc] = 0;
 	}/*end for*/

	VtrReadData.i2cReadCommand = (Osal_tenI2CReadMode)OSAL_I2C_READ_FROM_SLAVE_N_ACK_INVAL; 
	VtrReadData.i2cRxBuffer = &Vi8RxDataArr[0];

	/*read data */
	Vi32Num = OSAL_s32IORead(PI2cDevice,(tS8 *)&VtrReadData,NUM_REG_TDA7564_DB);   
    
	/* ERROR */
	if (Vi32Num >= 0)
	{
		Pu32Ret += 16;
		/*check read data */
		if (Vi32Num != NUM_REG_TDA7564_DB)
			 Pu32Ret += 400;
   		/*TODO check if data wrong*/

	}/*end if*/
 	
 	return(Pu32Ret);
}/*end function*/


/*****************************************************************************
* FUNCTION:		OEDT_I2C_FWriteAndReadWithInvalParam( )
* PARAMETER:    PI2cDevice and Pu32Ret.
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Try to do read and write operations with invalid parameters on
                TDA device 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
******************************************************************************/

tU32 OEDT_I2C_FWriteAndReadWithInvalParam(OSAL_tIODescriptor PI2cDevice,
                                        tU32 Pu32Ret)
{
	Osal_tenI2CClockSpeed VIncSpeed = (Osal_tenI2CClockSpeed)0;
	tS32 VIncTimeout = 0 ;
  
 
	for (VIncTimeout = 1500; VIncTimeout >= 500;VIncTimeout -= 500) 
	{ /*IF NO ERROR */
	if (Pu32Ret == 0)
	{	/*set timeout */
		if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,
		VIncTimeout)!=OSAL_OK)
		{
			Pu32Ret += 700;
		}/*end if*/
		else if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,VIncTimeout)!= OSAL_OK)
		{
			Pu32Ret += 1400;
		}/*end if*/
		if (Pu32Ret == 0)
		{ /*for various speed */
			for (VIncSpeed=OSAL_I2C_CLK_SPEED_50K;
			VIncSpeed < OSAL_I2C_CLK_SPEED_400K;VIncSpeed++) 
			{ /*set ClkSpeed*/
			if (OSAL_s32IOControl(PI2cDevice,
			OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!= OSAL_OK)
			{
				Pu32Ret += 75;
			}/*end if*/
			else
			{ /* Test sequenz*/
				/* Mute */
			  	
			Pu32Ret = u32I2C_IOWriteRegisterTDAWithInvalParam(PI2cDevice,
			I2C_IC_TEST_MODE_TDA_MUTE,Pu32Ret);
			/*  WAIT */
			OSAL_s32ThreadWait(300);/*300ms*/
			/* Demute */
			Pu32Ret=u32I2C_IOWriteRegisterTDAWithInvalParam(PI2cDevice,
			I2C_IC_TEST_MODE_TDA_DEMUTE,Pu32Ret);
			/* Read */
			Pu32Ret = u32I2C_IOReadRegisterTDAWithInvalParam(PI2cDevice,Pu32Ret);
			/* WAIT */
			OSAL_s32ThreadWait(300); /*300ms*/
			   				
			}/*end if*/
			}/*end for*/
		}/*end if*/
	}/*end if*/
  }/*end for*/
	return(Pu32Ret);
}/*end function*/


/*****************************************************************************
* FUNCTION:		u32I2CIOWriteAndReadTDAWithInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_025
* DESCRIPTION:  Try to do read and write operations with invalid parameters on 
                TDA device 
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/
tU32 u32I2CIOWriteAndReadTDAWithInvalParam(tVoid)
{
	tU32 Vu32Ret = 0;
	OSAL_tIODescriptor  Vhi2cDeviceTDA7564;

	/* open the device */
  	Vhi2cDeviceTDA7564 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_TDA7564,
  	 OSAL_EN_READWRITE);
	/*error?*/
   #if 0
	switch(Vhi2cDeviceTDA7564)
	{
	case OSAL_OK:
			break;
	case OSAL_E_NOTSUPPORTED:
			Vu32Ret += 1;
			break;
	case OSAL_E_CANCELED:
	case OSAL_E_ALREADYOPENED:
			Vu32Ret += 2;
			break;
	default:
			Vu32Ret += 3;
    }/*end switch*/
    #endif
    if(Vhi2cDeviceTDA7564 == OSAL_ERROR)
    {
      Vu32Ret = 1;
    }
	/* call function test */
	Vu32Ret = OEDT_I2C_FWriteAndReadWithInvalParam(Vhi2cDeviceTDA7564,Vu32Ret);
	if(OSAL_s32IOClose(Vhi2cDeviceTDA7564)!= OSAL_OK)
	{
		Vu32Ret += 10000;   
	}

	return(Vu32Ret);

}

/*****************************************************************************
* FUNCTION:	    u32I2CIOWriteWithInvalChanlId( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_026
* DESCRIPTION:  Try to  write data with incorrect parameters.
* HISTORY:		Created Haribabu Sannapaneni (RBIN/ECM1)  October, 6,2006 
*				Updated by Pradeep Chand.C (RBIN/ECM1) December 19, 2006
******************************************************************************/

# define   OSAL_C_STRING_DEVICE_I2C_INVAL -1

tU32 u32I2CIOWriteWithInvalChanlId( )
{
	tU32 Vu32Ret = 0;

	Vu32Ret = u32I2C_IOWriteRegisterTDAWithInvalParam(OSAL_C_STRING_DEVICE_I2C_INVAL,
						I2C_IC_TEST_MODE_TDA_MUTE,Vu32Ret);
    return(Vu32Ret);

}

/*****************************************************************************
* FUNCTION:	    u32I2CTDAOffsetDetection 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_027
* DESCRIPTION:  Enable Offset detection and find out the response.
				The 5th bit(from LSB) of the first instruction byte should be 
				set to enable Offset detection.
				Response from TDA 7564 is MSB of 2nd response byte should be 
				set.
* HISTORY:		Created by Kishore Kumar.R (RBIN/EDI3)  June 21,2006 
******************************************************************************/

tU32 u32I2CTDAOffsetDetection( )
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;
	OSAL_trI2CReadData      ReadData;
	OSAL_trI2CWriteData     WriteData;

	/*Buffer to store Instruction to TDA 7564*/
	tU8	 u8TxDataBuff[2]={0};
    tS32 s32BytesWritten = 0;
    
//  tU8  u8ReadCount=0;
	/*Buffer to store response from TDA 7564*/
	tU8	 u8RxDataBuff[NUM_REG_TDA7564_DB]={0};
	tS32 s32BytesRead = 0 ;
   
	   
  	OSAL_pvMemorySet(u8RxDataBuff,0,NUM_REG_TDA7564_DB);
 
  	/* TDA7564 */
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if (OSAL_ERROR == hDevice )
    {
    	u32Ret = 1;
    }
	else
	{
		/*Instruction for Offset Detection Enable*/
	 	u8TxDataBuff[0] =OFFSET_ENABLE_1 ;
    	u8TxDataBuff[1] =OFFSET_ENABLE_2 ;
    		    
  		WriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
		WriteData.i2cTxBuffer = u8TxDataBuff;

		
		s32BytesWritten = OSAL_s32IOWrite(hDevice,
		(tS8 *)&WriteData,NUM_REG_TDA7564_IB);
		if(  s32BytesWritten != NUM_REG_TDA7564_IB)
		{
		    u32Ret+=5;
		}
		else
		{
			/*Read the response */

        	ReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	 		ReadData.i2cRxBuffer = &u8RxDataBuff[0];

			s32BytesRead = OSAL_s32IORead(hDevice,
			(tS8 *)&ReadData,NUM_REG_TDA7564_DB);
			if(s32BytesRead != NUM_REG_TDA7564_DB)
			{
				u32Ret+=10;
								
			} 
		
			/*MSB of 2nd response byte set => offset detection enabled*/
			if( MASK_1 != (u8RxDataBuff[1]& MASK_1))
			{
				u32Ret+=15;

        	}
		}
		if( OSAL_OK != OSAL_s32IOClose(hDevice))
		{
			u32Ret +=100;   
		}

	     
	}
	
  	return u32Ret;

}

/*****************************************************************************
* FUNCTION:	    u32I2CSetClkSpeedZero( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_028
* DESCRIPTION:  Try to set the clock speed to invalid value.( 0).Only values 
				from 1 to 5 are defined as enumeration( 1=>50K  clkspeed,
				2=>100K clkspeed etc).(Should Fail)
* HISTORY:		Created by Kishore Kumar.R (RBIN/EDI3)  June 21,2006 
******************************************************************************/
	
tU32 u32I2CSetClkSpeedZero()
{

    OSAL_tIODescriptor hDevice = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32 u32Ret = 0;

  	
	hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

	if (OSAL_ERROR != hDevice )
	{
		
		if (OSAL_ERROR !=OSAL_s32IOControl(hDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,CLK_SPEED))
		{	
			u32Ret += 2;
  		}
		if( OSAL_ERROR == OSAL_s32IOClose ( hDevice ))
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 10;
	}	
	
	return u32Ret;

}
	    
/*****************************************************************************
* FUNCTION:	    u32I2CTDACurrentSensorActivte( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_029
* DESCRIPTION:  Activate Current Sensor (D2 of IB2 =1) 
				Set Stand-by OFF  ( D4 of IB2=1)
				Response from TDA 7564 is 
				D7 of DB3=1 (Stand By OFF).
				D6 of DB2=1 (Current Sensor Activated) 
* HISTORY:		Created by Kishore Kumar.R (RBIN/EDI3)  June 21,2006 
******************************************************************************/

tU32 u32I2CTDACurrentSensorActivte( )
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

	tU32 u32Ret = 0;
	OSAL_trI2CReadData      ReadData;
	OSAL_trI2CWriteData     WriteData;
	
	/*Buffer to store Instruction to TDA 7564*/
	tU8	 u8TxDataBuff[2]={0};
    tS32 s32BytesWritten = 0;
//  tU8  u8ReadCount=0;

	/*Buffer to store response from TDA 7564*/
	tU8	 u8RxDataBuff[NUM_REG_TDA7564_DB]={0};
	tS32 s32BytesRead = 0 ;
   
    OSAL_pvMemorySet(u8RxDataBuff,0,NUM_REG_TDA7564_DB);
  	/* TDA7564 */
    hDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if ( OSAL_ERROR == hDevice )
    {
    	u32Ret = 1;
    }
	else
	{
		/*Write the command*/
	 	u8TxDataBuff[0] =CURRENTSENSOR_STANDBY_1 ;
    	u8TxDataBuff[1] =CURRENTSENSOR_STANDBY_2 ;
    		    
  		WriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
		WriteData.i2cTxBuffer = u8TxDataBuff;

		
		s32BytesWritten = OSAL_s32IOWrite
		(hDevice,(tS8 *)&WriteData,NUM_REG_TDA7564_IB);
		if(  s32BytesWritten != NUM_REG_TDA7564_IB)
		{
		    u32Ret+=5;
		}
		else
		{
			/*Read the response */

        	ReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	 		ReadData.i2cRxBuffer = &u8RxDataBuff[0];

	    	s32BytesRead = OSAL_s32IORead(hDevice,
			(tS8 *)&ReadData,NUM_REG_TDA7564_DB);
			if(s32BytesRead != NUM_REG_TDA7564_DB)
			{
			    u32Ret+=10;
			} 
		
			/*D6(Bit 7  from Lsb) of 2nd response byte set => Current Sensor 
			Activated*/
			if( MASK_2 != (u8RxDataBuff[1]& MASK_2))
			{
				u32Ret+=15;

        	}
			/*D7 (Bit eight from Lsb) of third response byte set=> 
			Stand By OFF*/

			if( MASK_1 != (u8RxDataBuff[2]& MASK_1))
			{
				u32Ret+=50;

        	}
		}

		if(OSAL_OK != OSAL_s32IOClose(hDevice))
		{
			u32Ret +=100;   
		}

	     
	}
	
  	return u32Ret;

}



/*****************************************************************************
* FUNCTION:	    u32I2CTDAWriteMoreBytes( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_I2C_030
* DESCRIPTION: 	Try to write more than  2 Instruction bytes( for offset 
				detection enable) and read more than 4 response bytes from TDA 
				7564.(Should be Possible.However only 2 Instruction bytes and 4 
				response bytes are valid.)
* HISTORY:		Created by Kishore Kumar.R (RBIN/EDI3)  June 28,2006 
******************************************************************************/


tU32 u32I2CTDAWriteMoreBytes( )
{
	OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess =  OSAL_EN_READWRITE;

	tU32 u32Ret = 0;
	OSAL_trI2CReadData      ReadData;
	OSAL_trI2CWriteData     WriteData;

	/*Buffer to store Instruction to TDA 7564*/
	tU8	 u8TxDataBuff[BYTES]={0};
    tS32 s32BytesWritten = 0;
    tU8  u8WriteCount=0;

	/*Buffer to store response from TDA 7564*/
	/*some  random value other than 0x00 and 
	0xff has to be chosen as response bytes can have these values*/
	tU8	 u8RxDataBuff[BYTES]={0xab,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09};

	tS32 s32BytesRead = 0 ;   
	   
  	/* TDA7564 */
    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_I2C_TDA7564 , enAccess );

    if ( OSAL_ERROR == hDevice )
    {
    	u32Ret = 1;
    }
	else
	{
		/*Write the command*/
	   	   
  	    u8TxDataBuff[0]=OFFSET_ENABLE_1;
		u8TxDataBuff[1]=OFFSET_ENABLE_2;
  		for (u8WriteCount =2; u8WriteCount < BYTES-2; u8WriteCount++)
  		{
    		u8TxDataBuff[u8WriteCount] = OFFSET_ENABLE_2;
  		} 
    
  		WriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
		WriteData.i2cTxBuffer = u8TxDataBuff;

		
		s32BytesWritten = OSAL_s32IOWrite(hDevice,(tS8 *)&WriteData,BYTES);
		/*Even though it writes 10 bytes, only first 2 bytes are considered*/
		if(BYTES != s32BytesWritten)
		{
		    u32Ret+=5;
		}
		else
		{


			/*Read the response */

        	ReadData.i2cReadCommand = OSAL_I2C_READ_FROM_SLAVE_N_ACK; 
   	 		ReadData.i2cRxBuffer = &u8RxDataBuff[0];

	  		s32BytesRead = OSAL_s32IORead(hDevice,(tS8 *)&ReadData,BYTES);
			/*The Response will be valid 4 bytes,but rest of the (BYTES -4 )
			bytes will be 0xff .*/	 
			if( BYTES != s32BytesRead )
			{
				u32Ret +=50;
			}
			
			/*MSB of 2nd response byte set => offset detection enabled*/
			if(MASK_1 != (u8RxDataBuff[1]& MASK_1))
			{
				u32Ret+=100;

        	}


		}
		
		if( OSAL_OK != OSAL_s32IOClose(hDevice))
		{
			u32Ret +=1000;   
		}

	     
	}
	
  	return u32Ret;

}

/* **************************************************FunctionHeaderBegin***** 
 *
 *  tU32 u32I2CADV1AutoTest(tVoid)
 *
 *  This test 
 *  1)opens the channel of the device ADV1 ,
 *  2)Writes and reads the register. 
 *  This test run for several timeout's and several clockspeed
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
 *      
 * TEST CASE:    TU_OEDT_I2C_031
 * @history:
 * ***********************************************FunctionHeaderEnd***********/
tU32 u32I2CADV1AutoTest(tVoid)
{
	tU32				Vu32Ret = 0;
  	tS16                VtS16Version = 0;
  	OSAL_tIODescriptor  Vhi2cDeviceADV1;

	/* open the device */
  	Vhi2cDeviceADV1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_ADV1,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceADV1)
	{
    case OSAL_OK:
			break;
	  case OSAL_E_NOTSUPPORTED:
			 Vu32Ret += 1;
			break;
		case OSAL_E_CANCELED:
		case OSAL_E_ALREADYOPENED:
       Vu32Ret += 2;
			break;
     default:
	 	break;
  }/*end switch*/
  /* check version of the driver */
   if(OSAL_s32IOControl(Vhi2cDeviceADV1,OSAL_C_S32_IOCTRL_VERSION,
	(tS32)(&VtS16Version))==OSAL_OK)
	{
		if(VtS16Version != I2C_C_S32_IO_VERSION)
		{
			Vu32Ret += 4;
		}
	}
  	else
	{
	  Vu32Ret += 8;	 
	}
    /* Call function test */
	Vu32Ret = OEDT_I2C_FTestADV1Automatic(Vhi2cDeviceADV1,Vu32Ret);
	/* Close the device */
  if(OSAL_s32IOClose(Vhi2cDeviceADV1)!= OSAL_OK)
	{
    Vu32Ret += 200;   
	}
	return(Vu32Ret);
}/*End function*/


/***************************************************FunctionHeaderBegin********
 *
 *  static  tU32  OEDT_I2C_FTestADV1Automatic(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
  ***************************************************FunctionHeaderEnd*********/
static  tU32  OEDT_I2C_FTestADV1Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
{
Osal_tenI2CClockSpeed   VIncSpeed;
tU32                    VIncTimeout;


	for (VIncTimeout = 1500;VIncTimeout >= 500;VIncTimeout -= 500) 
	{ /*IF NO ERROR */
		if (Pu32Ret == 0)
		{	/*set timeout */
		if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,
		(tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 700;
		}/*end if*/
		else if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME, (tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 1400;
		}/*end if*/
		if (Pu32Ret == 0)
		{ /*for various speed */
			for (VIncSpeed=OSAL_I2C_CLK_SPEED_50K;
			VIncSpeed < OSAL_I2C_CLK_SPEED_400K;VIncSpeed++) 
			{ /*set ClkSpeed*/
				if (OSAL_s32IOControl(PI2cDevice,
				OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_OK)
				{
				Pu32Ret += 75;
				}/*end if*/
				else
				{ /* Testsequenz*/


				Pu32Ret = OEDT_I2C_FWriteRegisterADV1(PI2cDevice, Pu32Ret);
				/*  WAIT */
				OSAL_s32ThreadWait(300);/*300ms*/
				/* Read */
				Pu32Ret = OEDT_I2C_FReadRegisterADV1(PI2cDevice,Pu32Ret);
				/* WAIT */
				OSAL_s32ThreadWait(300); /*300ms*/
						
				}/*end else*/
				}/*end for*/
			}/*end if*/
		}/*end if*/
	}/*end for*/
return(Pu32Ret);

}/*end function*/

/****************************************************FunctionHeaderBegin******
 *
 *  static tU32   OEDT_I2C_FWriteRegisterADV1(OEDT_I2C_Testmode PeTestMode,
 * OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PeTestMode
 *      �Info�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
 *      
 **************************************************FunctionHeaderEnd********/
static tU32   OEDT_I2C_FWriteRegisterADV1(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret)
{
OSAL_trI2CWriteData     VtrWriteData;
tU8		   				Vi8TxDataArr[2];
tS32	   				Vi32Num = 0; 

Vi8TxDataArr[0] = 0x0F;
Vi8TxDataArr[1] = 0x80;  

	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

	/* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_ADV1_IB);

	OSAL_s32ThreadWait(20);/*20ms*/
    Vi8TxDataArr[0] = 0x0A;
    Vi8TxDataArr[1] = 0x40;  

	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

	/* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_ADV1_IB);

	/* ERROR? */
	if (Vi32Num < 0)
	{
	Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != NUM_REG_ADV1_IB)
	{/*TImeout Error*/
	Pu32Ret += 500;
	}/*end else if*/
return(Pu32Ret);
}/*end function*/


/* **************************************************FunctionHeaderBegin*******
 *
 *  static tU32  OEDT_I2C_FReadRegisterADV1(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * 
 * @history:	
 *
 *************************************************FunctionHeaderEnd***********/

static tU32  OEDT_I2C_FReadRegisterADV1(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
OSAL_trI2CCombinedTransfer CombinedTransfer;

tU8	Vi8TxDataArr;
tU8* 	Vi8RxDataArr;
tU8	 DiagnosticTest;


	Vi8TxDataArr = 0x0A;
	
	CombinedTransfer.i2cTxBuffer = &Vi8TxDataArr;

    Vi8RxDataArr = (tU8 *)OSAL_pvMemoryAllocate(10);

	CombinedTransfer.i2cRxBuffer = Vi8RxDataArr;
	CombinedTransfer.BytesToWrite = 1;
       CombinedTransfer.BytesToRead = 1;
	/* Combined Transaction */
	if (OSAL_s32IOControl(PI2cDevice,
	OSAL_C_S32_IOCTRL_I2C_COMBINED_TRANSFER, (tS32)&CombinedTransfer)!=OSAL_OK)
	{
	Pu32Ret += 75;
	}/*end if*/

	DiagnosticTest = *Vi8RxDataArr & 0x40;

	if(DiagnosticTest != 0x40)
	{
	Pu32Ret += 100;
	}
OSAL_vMemoryFree(Vi8RxDataArr);
return(Pu32Ret);
}/*end function*/


/* **************************************************FunctionHeaderBegin***** 
 *
 *  tU32 u32I2CADV2AutoTest(tVoid)
 *
 *  This test 
 *  1)opens the channel of the device ADV2 ,
 *  2)Writes and reads the register. 
 *  This test run for several timeout's and several clockspeed
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
 *      
 * TEST CASE:    TU_OEDT_I2C_032
 * @history:
 * ***********************************************FunctionHeaderEnd***********/
tU32 u32I2CADV2AutoTest(tVoid)
{
	tU32				Vu32Ret = 0;
  	tS16                VtS16Version = 0;
  	OSAL_tIODescriptor  Vhi2cDeviceADV2;
/*  WAIT */
	
	/* open the device */
  	Vhi2cDeviceADV2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_ADV2,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceADV2)
	{
    case OSAL_OK:
			break;
	  case OSAL_E_NOTSUPPORTED:
			 Vu32Ret += 1;
			break;
		case OSAL_E_CANCELED:
		case OSAL_E_ALREADYOPENED:
       Vu32Ret += 2;
			break;
   default:
	 	break;

  }/*end switch*/
  /* check version of the driver */
   if(OSAL_s32IOControl(Vhi2cDeviceADV2,OSAL_C_S32_IOCTRL_VERSION,
	(tS32)(&VtS16Version))==OSAL_OK)
	{
		if(VtS16Version != I2C_C_S32_IO_VERSION)
		{
			Vu32Ret += 4;
		}
	}
  	else
	{
	  Vu32Ret += 8;	 
	}
    /* Call function test */
	Vu32Ret = OEDT_I2C_FTestADV2Automatic(Vhi2cDeviceADV2,Vu32Ret);
	/* Close the device */
  if(OSAL_s32IOClose(Vhi2cDeviceADV2)!= OSAL_OK)
	{
    Vu32Ret += 200;   
	}
	return(Vu32Ret);
}/*End function*/


/***************************************************FunctionHeaderBegin********
 *
 *  static  tU32  OEDT_I2C_FTestADV2Automatic(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
  ***************************************************FunctionHeaderEnd*********/
static  tU32  OEDT_I2C_FTestADV2Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
{
Osal_tenI2CClockSpeed   VIncSpeed;
tU32                    VIncTimeout;


	for (VIncTimeout = 1500;VIncTimeout >= 500;VIncTimeout -= 500) 
	{ /*IF NO ERROR */
		if (Pu32Ret == 0)
		{	/*set timeout */
		if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,
		(tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 700;
		}/*end if*/
		else if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME, (tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 1400;
		}/*end if*/
		if (Pu32Ret == 0)
		{ /*for various speed */
			for (VIncSpeed=OSAL_I2C_CLK_SPEED_50K;
			VIncSpeed < OSAL_I2C_CLK_SPEED_400K;VIncSpeed++) 
			{ /*set ClkSpeed*/
				if (OSAL_s32IOControl(PI2cDevice,
				OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_OK)
				{
				Pu32Ret += 75;
				}/*end if*/
				else
				{ /* Testsequenz*/


				Pu32Ret = OEDT_I2C_FWriteRegisterADV2(PI2cDevice, Pu32Ret);
				/*  WAIT */
				OSAL_s32ThreadWait(300);/*300ms*/
				/* Read */
				Pu32Ret = OEDT_I2C_FReadRegisterADV2(PI2cDevice,Pu32Ret);
				/* WAIT */
				OSAL_s32ThreadWait(300); /*300ms*/
						
				}/*end else*/
				}/*end for*/
			}/*end if*/
		}/*end if*/
	}/*end for*/
return(Pu32Ret);

}/*end function*/

/****************************************************FunctionHeaderBegin******
 *
 *  static tU32   OEDT_I2C_FWriteRegisterADV2(OEDT_I2C_Testmode PeTestMode,
 * OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PeTestMode
 *      �Info�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
 *      
 **************************************************FunctionHeaderEnd********/
static tU32   OEDT_I2C_FWriteRegisterADV2(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret)
{
OSAL_trI2CWriteData     VtrWriteData;
tU8		   				Vi8TxDataArr[2];
tS32	   				Vi32Num = 0; 

Vi8TxDataArr[0] = 0x0F;
Vi8TxDataArr[1] = 0x80;  

	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

	/* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_ADV2_IB);

	OSAL_s32ThreadWait(20);/*20ms*/


Vi8TxDataArr[0] = 0x0A;
Vi8TxDataArr[1] = 0x40;  

	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

	/* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_ADV2_IB);

	/* ERROR? */
	if (Vi32Num < 0)
	{
	Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != NUM_REG_ADV2_IB)
	{/*TImeout Error*/
	Pu32Ret += 500;
	}/*end else if*/
return(Pu32Ret);
}/*end function*/


/* **************************************************FunctionHeaderBegin*******
 *
 *  static tU32  OEDT_I2C_FReadRegisterADV2(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * 
 * @history:	
 *
 *************************************************FunctionHeaderEnd***********/

static tU32  OEDT_I2C_FReadRegisterADV2(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{
OSAL_trI2CCombinedTransfer CombinedTransfer;
tU8	Vi8TxDataArr;
tU8* 	Vi8RxDataArr;
tU8	 DiagnosticTest;


	Vi8TxDataArr = 0x0A;
	
	CombinedTransfer.i2cTxBuffer = &Vi8TxDataArr;

    Vi8RxDataArr = (tU8 *)OSAL_pvMemoryAllocate(10);

	CombinedTransfer.i2cRxBuffer = Vi8RxDataArr;
	CombinedTransfer.BytesToWrite = 1;
       CombinedTransfer.BytesToRead = 1;
	/* Combined Transaction */
	if (OSAL_s32IOControl(PI2cDevice,
	OSAL_C_S32_IOCTRL_I2C_COMBINED_TRANSFER, (tS32)&CombinedTransfer)!=OSAL_OK)
	{
	Pu32Ret += 75;
	}/*end if*/

	DiagnosticTest = *Vi8RxDataArr & 0x40;

	if(DiagnosticTest != 0x40)
	{
	Pu32Ret += 100;
	}
	
OSAL_vMemoryFree(Vi8RxDataArr);
return(Pu32Ret);
}/*end function*/

/* **************************************************FunctionHeaderBegin***** 
 *
 *  tU32 u32I2CFPGAAutoTest(tVoid)
 *
 *  This test 
 *  1)opens the channel of the device FPGA ,
 *  2)Writes and reads the register. 
 *  This test run for several timeout's and several clockspeed
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
 *      
 * TEST CASE:    TU_OEDT_I2C_033
 * @history:
 * ***********************************************FunctionHeaderEnd***********/
tU32 u32I2CFPGAAutoTest(tVoid)
{
	tU32				Vu32Ret = 0;
  	tS16                VtS16Version = 0;
  	OSAL_tIODescriptor  Vhi2cDeviceFPGA;

	/* open the device */
  	Vhi2cDeviceFPGA = OSAL_IOOpen(OSAL_C_STRING_DEVICE_I2C_FPGA,
  	 OSAL_EN_READWRITE);
	/*error?*/
	switch(Vhi2cDeviceFPGA)
	{
    case OSAL_OK:
			break;
	  case OSAL_E_NOTSUPPORTED:
			 Vu32Ret += 1;
			break;
		case OSAL_E_CANCELED:
		case OSAL_E_ALREADYOPENED:
       Vu32Ret += 2;
			break;
   default:
	 	break;
 
	}/*end switch*/
  /* check version of the driver */
   if(OSAL_s32IOControl(Vhi2cDeviceFPGA,OSAL_C_S32_IOCTRL_VERSION,
	(tS32)(&VtS16Version))==OSAL_OK)
	{
		if(VtS16Version != I2C_C_S32_IO_VERSION)
		{
			Vu32Ret += 4;
		}
	}
  	else
	{
	  Vu32Ret += 8;	 
	}
    /* Call function test */
	Vu32Ret = OEDT_I2C_FTestFPGAAutomatic(Vhi2cDeviceFPGA,Vu32Ret);
	/* Close the device */
  if(OSAL_s32IOClose(Vhi2cDeviceFPGA)!= OSAL_OK)
	{
    Vu32Ret += 200;   
	}
	return(Vu32Ret);
}/*End function*/


/***************************************************FunctionHeaderBegin********
 *
 *  static  tU32  OEDT_I2C_FTestFPGAAutomatic(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @note
  ***************************************************FunctionHeaderEnd*********/
static  tU32  OEDT_I2C_FTestFPGAAutomatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret)
{
Osal_tenI2CClockSpeed   VIncSpeed;
tU32                    VIncTimeout;


	for (VIncTimeout = 1500;VIncTimeout >= 500;VIncTimeout -= 500) 
	{ /*IF NO ERROR */
		if (Pu32Ret == 0)
		{	/*set timeout */
		if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_WRITE_BREAKTIME,
		(tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 700;
		}/*end if*/
		else if (OSAL_s32IOControl(PI2cDevice,
		OSAL_C_S32_IOCTRL_I2C_SET_READ_BREAKTIME,(tS32)VIncTimeout)!=OSAL_OK)
		{
		Pu32Ret += 1400;
		}/*end if*/
		if (Pu32Ret == 0)
		{ /*for various speed */
			for (VIncSpeed=OSAL_I2C_CLK_SPEED_50K;
			VIncSpeed < OSAL_I2C_CLK_SPEED_400K;VIncSpeed++) 
			{ /*set ClkSpeed*/
				if (OSAL_s32IOControl(PI2cDevice,
				OSAL_C_S32_IOCTRL_I2C_SET_CLOCKSPEED,VIncSpeed)!=OSAL_OK)
				{
				Pu32Ret += 75;
				}/*end if*/
				else
				{ /* Testsequenz*/


				Pu32Ret = OEDT_I2C_FWriteRegisterFPGA(PI2cDevice, Pu32Ret);
				/*  WAIT */
				OSAL_s32ThreadWait(300);/*300ms*/
				/* Read */
				Pu32Ret = OEDT_I2C_FReadRegisterFPGA(PI2cDevice,Pu32Ret);
				/* WAIT */
				OSAL_s32ThreadWait(300); /*300ms*/
						
				}/*end else*/
				}/*end for*/
			}/*end if*/
		}/*end if*/
	}/*end for*/
return(Pu32Ret);

}/*end function*/

/****************************************************FunctionHeaderBegin******
 *
 *  static tU32   OEDT_I2C_FWriteRegisterFPGA(OEDT_I2C_Testmode PeTestMode,
 * OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PeTestMode
 *      �Info�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 * 
 *      
 **************************************************FunctionHeaderEnd********/
static tU32   OEDT_I2C_FWriteRegisterFPGA(OSAL_tIODescriptor PI2cDevice, tU32 Pu32Ret)
{
OSAL_trI2CWriteData     VtrWriteData;
tU8		   				Vi8TxDataArr[8];
tS32	   				Vi32Num = 0; 

Vi8TxDataArr[0] = 0x01;
Vi8TxDataArr[1] = 0x00;  
Vi8TxDataArr[2] = 0x00;
Vi8TxDataArr[3] = 0x00;  
Vi8TxDataArr[4] = 0x01;
Vi8TxDataArr[5] = 0x01;  
Vi8TxDataArr[6] = 0x01;
Vi8TxDataArr[7] = 0x05; 
	VtrWriteData.i2cWriteCommand = OSAL_I2C_WRITE_TO_SLAVE;  
	VtrWriteData.i2cTxBuffer = Vi8TxDataArr;

	/* Output */
	Vi32Num=OSAL_s32IOWrite(PI2cDevice,
	(tS8 *)&VtrWriteData,NUM_REG_FPGA_IB);

	/* ERROR? */
	if (Vi32Num < 0)
	{
	Pu32Ret += 50;
	}/*end else*/
	else if(Vi32Num != NUM_REG_FPGA_IB)
	{/*TImeout Error*/
	Pu32Ret += 500;
	}/*end else if*/
return(Pu32Ret);
}/*end function*/


/* **************************************************FunctionHeaderBegin*******
 *
 *  static tU32  OEDT_I2C_FReadRegisterFPGA(OSAL_tIODescriptor PI2cDevice)
 *
 *  �LongInfo�
 *
 * @param   PI2cDevice
 *      �Info�
 *
 * @return
 *      �Info�
 *
 * @date    06-08-2008
 *
 * @history:	

 *
 *************************************************FunctionHeaderEnd***********/
static tU32  OEDT_I2C_FReadRegisterFPGA(OSAL_tIODescriptor PI2cDevice,
tU32 Pu32Ret)
{

OSAL_trI2CCombinedTransfer CombinedTransfer;
tU8	Vi8TxDataArr[4];
tU8* 	Vi8RxDataArr;
tU8	 DiagnosticTest;
Vi8TxDataArr[0] = 0x01;
Vi8TxDataArr[1] = 0x00;  
Vi8TxDataArr[2] = 0x00;
Vi8TxDataArr[3] = 0x00;  


		
	CombinedTransfer.i2cTxBuffer = Vi8TxDataArr;

    Vi8RxDataArr = (tU8 *)OSAL_pvMemoryAllocate(10);

	CombinedTransfer.i2cRxBuffer = Vi8RxDataArr;
	CombinedTransfer.BytesToWrite = 4;
       CombinedTransfer.BytesToRead = 4;
	/* Combined Transaction */
	if (OSAL_s32IOControl(PI2cDevice,
	OSAL_C_S32_IOCTRL_I2C_COMBINED_TRANSFER, (tS32)&CombinedTransfer)!=OSAL_OK)
	{
	Pu32Ret += 75;
	}/*end if*/
OEDT_HelperPrintf(TR_LEVEL_FATAL,
                      "FPGA values: byte1 :%lu  byte2 :%lu byte3 :%lu byte4 :%lu ",
                      *(Vi8RxDataArr +0),
                      *(Vi8RxDataArr +1),
                      *(Vi8RxDataArr +2),
                      *(Vi8RxDataArr +3));

	DiagnosticTest = *(Vi8RxDataArr +3) ;

	if(DiagnosticTest != 0x05)
	{
	Pu32Ret += 100;
	}
	
OSAL_vMemoryFree(Vi8RxDataArr);
return(Pu32Ret);

}/*end function*/


/* EOF */
