/******************************************************************************
 * FILE         : oedt_fs_dev_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : Filesystem Devices
 *                          
 * AUTHOR(s)    :  Martin Langer (CM-AI/PJ-CF33)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * 27 Sep, 2011   | Initial revision    | Martin Langer (CM-AI/PJ-C33)
 ------------------------------------------------------------------------------
 *-----------------------------------------------------------------------------
 * Date              |                                                                       | Author & comments 
 * 6 Jun, 2016   | Removed /shared/cryptnav2 device check  | Ganesh Chandra Satish (RBEI/ECF5)
                         | as this is not supported in gen3                      |
 ------------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_fs_dev_testfuncs.h"

tU32 u32fs_dev_check_system_devices(tVoid)
{
	tU32 u32Status = 0;
	tU32 u32Ret = 0;
	int i=0;

	// max list of devices as default
	struct oedt_device_test fs_device[] = {
		{OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS,               OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS2,              OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS3,              OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS4,              OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_CARD,                  OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_CRYPT_CARD,            OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_DEV_MEDIA,             OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAVROOT,          OSAL_OK},
		{OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAV,              OSAL_OK},
		{(char *)NULL, 0}
	};	
	
	// lcn2kai doesn't need /shared/cryptnav2
	if (u32OEDT_BoardName() == OEDT_LCN2KAI) {
		fs_device[0].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS;
		fs_device[0].ret_val  =	OSAL_OK;
		fs_device[1].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS2;
		fs_device[1].ret_val  =	OSAL_OK;
		fs_device[2].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS3;
		fs_device[2].ret_val  =	OSAL_OK;
		fs_device[3].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS4;
		fs_device[3].ret_val  =	OSAL_OK;
		fs_device[4].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_CARD;
		fs_device[4].ret_val  =	OSAL_OK;
		fs_device[5].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_CRYPT_CARD;
		fs_device[5].ret_val  =	OSAL_OK;
		fs_device[6].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_DEV_MEDIA;
		fs_device[6].ret_val  =	OSAL_OK;
		fs_device[7].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAVROOT;
		fs_device[7].ret_val  =	OSAL_OK;
		fs_device[8].dev_name = OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAV;
		fs_device[8].ret_val  =	OSAL_OK;
		fs_device[9].dev_name = NULL;
		fs_device[9].ret_val  =	0;
	};	
		
	for (i=0; fs_device[i].dev_name!=NULL; i++)
	{
		u32Status = u32OEDT_OpenCloseDevice( fs_device[i].dev_name, fs_device[i].ret_val, OSAL_EN_READONLY );
		if(u32Status != 0)
		{
			u32Ret += u32Status + i*1000;
			return u32Ret;
		}
	}
	
	return u32Ret;
}
