#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include <OsalIoscTestCommon.h>
#include <OsalIoscMQCommon.h>

volatile tS32 gsRetVal2 = 0;
volatile tU32 guRetFlg2 = 0;


/*****************************************************************************
* FUNCTION		:  SubTestCase1( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase1 w.r.t Iosc MQ use-case sequence1
* 					Task 1 		Task 2
*					Create
*								Open
*					Close
*								Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase1( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hTestMQ_2 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem1 : waiting for Task1 to signal after CREATE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Open MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : OPEN");
	hTestMQ_2 = OpenMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state);

	/* Post to Sem2 : Task1 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after CLOSE MQ*/
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hTestMQ_2, state );

	/* Post to Sem2 : Task1 will DELETE MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem2 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

}

/*****************************************************************************
* FUNCTION		:  SubTestCase2( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase2 w.r.t Iosc MQ use-case sequence2
* 					Task 1 		Task 2
*								Create
*					Open
*								Close
*					Delete
*					Close
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase2( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hMessageQueue_2 = 0;
	OSAL_tMQueueHandle hTestMQ_2 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Create MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CREATE");
	hTestMQ_2 = CreateMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state );

	/* Post to Sem2 : Task1 will OPEN MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after OPEN MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hTestMQ_2, state );

	/* Post to Sem2 : Task1 will DELETE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CREATE NEW");
	/* create a MQ with different size to check whether
		the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						IOSC_TEST_MQ_CHANNEL,
					   (IOSC_TEST_MQ_MAX_MSGNUM/2),
					   (IOSC_TEST_MQ_MAX_MSGLEN/2),
					   OSAL_EN_READONLY, &hMessageQueue_2);

	if(s32ErrorCode == OSAL_OK)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : Error creating new MQ with different size");

	/* Close new MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_2, state );

	/*Delete new MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase3( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase3 w.r.t Iosc MQ use-case sequence3
* 					Task 1 		Task 2
*					Create
*								Open
*					Close
*								Close
*								Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase3( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hTestMQ_2 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem1 : waiting for Task1 to signal after CREATE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Open MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : OPEN");
	hTestMQ_2 = OpenMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state);

	/* Post to Sem2 : Task1 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hTestMQ_2, state );

	/* Delete MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/* Post to Sem2 : Task1 will CREATE NEW MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase4( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase4 w.r.t Iosc MQ use-case sequence4
* 					Task 1 		Task 2
*								Create
*					Open
*								Delete
*					Close
*								Close
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase4( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hMessageQueue_2 = 0;
	OSAL_tMQueueHandle hTestMQ_2 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Create MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CREATE");
	hTestMQ_2 = CreateMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state );

	/* Post to Sem2 : Task1 will OPEN MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after OPEN MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/*Delete MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/* Post to Sem2 : Task1 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hTestMQ_2, state );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CREATE NEW");
	/* create a MQ with different size to check whether
		the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						IOSC_TEST_MQ_CHANNEL,
					   (IOSC_TEST_MQ_MAX_MSGNUM/2),
					   (IOSC_TEST_MQ_MAX_MSGLEN/2),
					   OSAL_EN_READONLY, &hMessageQueue_2);

	if(s32ErrorCode == OSAL_OK)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : Error creating new MQ with different size");

	/* Close new MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_2, state );
	/*Delete new MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase5( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase5 w.r.t Iosc MQ use-case sequence5
* 					Task 1 			Task 2
*					Create
*									Open
*					Post
*									Receive
*									Close
*					Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase5( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	tU8 sMessage[IOSC_TEST_MQ_MAX_MSGLEN];
	OSAL_tMQueueHandle hMessageQueue_2 = 0;
	tS32 s32IOSC_MQ;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem1 : waiting for Task1 to signal after CREATE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Open MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : OPEN");
	hMessageQueue_2 = OpenMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_READONLY, state);

	/* Post to Sem2 : Task1 will POST to MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );
	/* Wait on Sem1 : waiting for Task1 to signal after POST to MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : RECEIVE");
	ReceiveMessage( hMessageQueue_2, sMessage, state );

	s32IOSC_MQ = TranslateMessage( sMessage, state );

	if(s32IOSC_MQ == 5)
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : Received Msg = 'write' ");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : ERROR in receiving msg");

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hMessageQueue_2, state );

	/* Post to Sem2 : Task1 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

}

/*****************************************************************************
* FUNCTION		:  SubTestCase6( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 2 of SubTestCase6 w.r.t Iosc MQ use-case sequence6
* 					Task 1 			Task 2
*					Create
*					Post
*					Close
*									Open
*									Receive
*									Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase6( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	tU8 sMessage[IOSC_TEST_MQ_MAX_MSGLEN];
	OSAL_tMQueueHandle hMessageQueue_2 = 0;
	tS32 s32IOSC_MQ;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem1 : waiting for Task1 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem1, OSAL_C_TIMEOUT_FOREVER );

	/* Open MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : OPEN");
	hMessageQueue_2 = OpenMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_READONLY, state);

	/* Receive the message from MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : RECEIVE");
	ReceiveMessage( hMessageQueue_2, sMessage, state );

	/* translate the message */
	s32IOSC_MQ = TranslateMessage( sMessage, state );

	if(s32IOSC_MQ == 5)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : Received Msg = 'write' ");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : ERROR in receiving msg");

	/* Close MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : CLOSE");
	CloseMessageQueue( hMessageQueue_2, state );

	/* Post to Sem2 : Task1 will DELETE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem2 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  vOsalIoscMQTestTsk2(tPVoid pvArg)
* PARAMETER		:  pvArg -- void pointer to the test case number
* RETURNVALUE	:  none
* DESCRIPTION	:  This function is the Thread2 which executes the corresponding
* 				   test case based on the parameter,and reports any errors
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tVoid vOsalIoscMQTestTsk2(tPVoid pvArg)
{

	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	tU8 *TestNum = (tU8 *) pvArg;

	 switch(*TestNum)
	 	{
	 		case TEST1 : {
	 					  /* run the SubTestCase1 */
						  SubTestCase1( &state );
						 }
							break;
	 		case TEST2 : {
	 					  /* run the SubTestCase2 */
						  SubTestCase2( &state );
						 }
							break;

	 		case TEST3 : {
	 					  /* run the SubTestCase3 */
						  SubTestCase3( &state );
						 }
							break;
	 		case TEST4 : {
	 					  /* run the SubTestCase4 */
						  SubTestCase4( &state );
						 }
							break;
	 		case TEST5 : {
	 					  /* run the SubTestCase5 */
						  SubTestCase5( &state );
						 }
							break;
	 		case TEST6 : {
	 					  /* run the SubTestCase6 */
						  SubTestCase6( &state );
						 }
							break;

	 		default : OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : BAD ARG");
	 						break;
	 	}

	  /* print the error if any */
	 OEDT_HelperPrintf(TR_LEVEL_FATAL,
	   "TASK2 : Exiting. Error code is %d. %d of %d check points succeeded.",
	   state.error_code,
	   state.error_position - state.error_count,
	   state.error_position);

	/* pass the errors, if any*/
    if(state.error_count)
		gsRetVal2 = (-1);
 	else
 		gsRetVal2 = state.error_count;

    guRetFlg2 = TRUE;

	/* Close the current thread */
    OSAL_vThreadExit();
}
