
/******************************************************************************
 * FILE         : oedt_Platform_Startup.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the platform startup measurement test 
 *
 * AUTHOR(s)    :  Dainius Ramanauskas (CM-AI/PJ-CF31)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                       | Author & comments
 * --.--.--       | Initial revision      | ------------
 * 25 Mai, 2011    | version 1.0           | Dainius Ramanauskas (CM-AI/PJ-CF31)
  -----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Platform_Startup.h"
#include "oedt_helper_funcs.h"

int klogctl(int type, char *bufp, int len);

/*****************************************************************************
 * FUNCTION:    u32StartupTimePlatform()
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * TEST CASE:
 * DESCRIPTION:
 * OEDT Test - platform startup measurement
 *
 * 1. Search in dmesg for lines:
 *
 * <4>[   66.498604] OEDT platform start
 * <4>[   69.498456] OEDT platform finished
 *
 * 2. Calculate time needed for platform startup
 *
 * HISTORY:      Created by Dainius Ramanauskas
 ******************************************************************************/

tU32 u32StartupTimePlatform(tVoid )
{
   char* bufDmesg = OSAL_NULL;
   tU32 u32Ret = 0;
   tS32 lenDmesg = 5000000;
   tDouble endTime = 0.0;
   tDouble requiredStartTime = 3.500000;
   tS32 firstCharInLine = 0;
   tS32 token = 0;
   tS32 n, i = 0;
   tS32 currentc = 0;

   if ((bufDmesg = (char *)OSAL_pvMemoryAllocate(lenDmesg)) == 0)
   {
      perror("malloc bufDmesg\n");
      return 100;
   }

   n = klogctl(3, bufDmesg, lenDmesg);
   if (n < 0)
   {
      perror("klogctl");
      OSAL_vMemoryFree(bufDmesg);
      return 200;
   }

   for (i = 0; i < n; i++)
   {
      currentc = bufDmesg[i];
      if (currentc == 'O') // searching for O (first char of OEDT)
         token = i;

      if (currentc == '\n')
      {
         firstCharInLine = token - 18;
         {
            if (OSAL_s32StringNCompare(&bufDmesg[firstCharInLine + 18], "OEDT platform f", 15) == 0)
            {
//               printf( TR_LEVEL_FATAL,"#################### found start %s\n", &bufDmesg[firstCharInLine]);
               endTime = OSAL_dAsciiToDouble(&bufDmesg[firstCharInLine + 4]);
               break;
            }
         }
      }
   }

   OEDT_HelperPrintf( TR_LEVEL_FATAL,"end time %Lf, required min time %Lf", endTime, requiredStartTime);

   if ((endTime == 0.0) || (endTime > requiredStartTime))
      u32Ret += 300;

   OSAL_vMemoryFree(bufDmesg);

   return u32Ret;
}
