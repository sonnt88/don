/******************************************************************************
| FILE:			oedt_Flash_TestFuncs.c
| PROJECT:      GM_GE_09 Project (TE_Plattform_INT_XX) 
|				(RBIN Sub Project ID: 70705_T_OEDT)
| SW-COMPONENT: OEDT FWK
|------------------------------------------------------------------------------
| DESCRIPTION:  This file implements the individual test cases for FLASH 
                device
|               The corresponding test specs:  TU_OEDT_Flash_TestSpec.xls
                                               version:  2.1
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
|31 July 2006   |  version 1.0            |Anoop Krishnan (RBIN/ECM1)
|03 Aug  2006   |  version 2.0            |Anoop Krishnan (RBIN/ECM1)
|               |                         |( Modified After the Review )
|------------------------------------------------------------------------------
|26 Dec 2006    | Version 2.1			  | Added and updated test cases			   
|				|                         |Haribabu Sannapaneni(RBIN/ECM1)
 
|______________________________________________________________________________
|				|					      |Included cases TU_OEDT_FLASH_021,022
|14 May 2007   	| Version 2.2         	  |Updated cases TU_OEDT_FLASH_007-010
|				|						  |to remove compilation errors/warnings.
|				|						  |Rakesh Dhanya (RBIN/EDI3)
|*****************************************************************************
|27 sep  2007	| version 2.3 			  |Anoop Chandran (RBIN/EDI3)
|				|						  |Updated cases numbers 
|				|						  |TU_OEDT_FLASH_006,007,
|				|						  |008,010,011
|				|						  |27 september 2007
|*****************************************************************************
|15 Feb  2008  | version 2.4 				|Anoop Chandran (RBIN/ECM1)
|					|						  		|Updated cases numbers 
|					|						  		|TU_OEDT_FLASH_004,008
|*****************************************************************************
|03 March 2008 | version 2.5 				|Anoop Chandran (RBIN/ECM1)
|					|						  		|Updated cases numbers 
|					|								|change the testcase 
|					|						  		|TU_OEDT_FLASH_ 006,007,022,023
*****************************************************************************
|*****************************************************************************
|29 Oct 2008 	| version 2.6 				|Anoop Chandran (RBIN/ECM1)
|					|						  		|Adde new tescases and removed 
|					|						  		|TU_OEDT_FLASH_ 024,025
|					|								|and  removed u32DevFlashHeavyParallelAccess 
|*****************************************************************************
|25 Mar 2009 	| version 2.7 				        |Sainath Kalpuri (RBEI/ECF1)
|					|						  		|Removed lint and compiler warnings
|*****************************************************************************
|11 Aug 2009 	| version 2.8 				        |Anoop Chandran (RBEI/ECF1)
|					|						  		|update the flash start offset 
|*****************************************************************************
|29  march 2010| version 2.9 				        |Anoop Chandran (RBEI/ECF1)
|					|						  		        |update the flash start offset
|              |                               | for GMGE 
*****************************************************************************/

 
/*****************************************************************
|  Header files inclusion
|----------------------------------------------------------------*/

#include "oedt_Flash_TestFuncs.h"
#include "oedt_helper_funcs.h"
  
 # define OEDT_NULL_PTR OSAL_NULL
//sak9kor - commented because it is being used only dead code
// # define OEDT_SUCCESS 1
//# define OEDT_FAILURE 0
#include "oedt_Flash_HelperFuncs.h"
/*****************************************************************
| variable declaration (scope: local)
|----------------------------------------------------------------*/
static tU32 u32OEDT_Flashstartoffset(tVoid);
//#define vTtfisTrace(...)

#if 0
static tS8 as8FLASHMesgToWrite [1024] =
{
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d',
'd','d','d','d','d','d','d','d','d','d','d','d','d','d','d','d'
};
#endif


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/




/*****************************************************************************
* FUNCTION:		u32DevFlashOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_001
* DESCRIPTION:  Open the flash device and close 
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/


tU32 u32DevFlashOpenClose(void)
{
	OSAL_tIODescriptor hDevice = 0;														 
	tU32 u32Ret = 0;

	hDevice = OSAL_IOOpen( OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32DevFlashOpenCloseDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_002
* DESCRIPTION:  Open the flash device in different modes(Read, 
*				Read Write, Write)and close 
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashOpenCloseDiffModes(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
    
    /* In Read Only mode */

    hDevice = OSAL_IOOpen ( OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READONLY );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
 	/* In Read Write mode */

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE );
   	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 10;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}
	/* In Write Only mode */

	hDevice = OSAL_IOOpen ( OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_WRITEONLY );
	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret += 100;
    }
    else
	{
	    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}
 	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32DevFlashOpenInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_003
* DESCRIPTION:  Try to open the invalid device. 
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/


tU32 u32DevFlashOpenInvalParam(void)
{
	OSAL_tIODescriptor hDevice = 0,hDevice_1 = 0;    
	tU32 u32Ret = 0;


	/* Try to open the Flash device with invalid access mode */
	hDevice = OSAL_IOOpen ( OSAL_C_STRING_FLASH_DEVICE, (OSAL_tenAccess)INVALID_ACCESS_MODE );
   	if ( hDevice != OSAL_ERROR)
    {
    	u32Ret = 1;
		if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}

    }	
    
    /* Try to open the Invalid device */    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_INVALID_DEVICE, OSAL_EN_READWRITE );
    if ( hDevice != OSAL_ERROR)
    {
    	u32Ret += 100;
		if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}

    }
	/* Try to close invalid handle(device) */
 	hDevice_1 = OSAL_IOOpen( OSAL_C_STRING_FLASH_DEVICE , OSAL_EN_READWRITE );
    if ( hDevice_1 == OSAL_ERROR)
    {	 
    	u32Ret += 300;
	}
	else
	{
	hDevice = INVALID_HANDLE;
		if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
		{
			if( OSAL_s32IOClose( hDevice_1 ) == OSAL_ERROR )
	{
		u32Ret += 300;
	}
		}
		else
		{
			if( OSAL_s32IOClose( hDevice_1 ) == OSAL_ERROR )
			{
				u32Ret += 400;
			}
			u32Ret += 500;
		}

    }
    return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32DevFlashOpenMulti()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_004
* DESCRIPTION:  Open the flash device multiple(2) times and close.
						second tme open should fail.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*					Updated the test case by Anoop Chandran(RBIN/ECM1) Feb 15,2008
*
******************************************************************************/

tU32 u32DevFlashOpenMulti(void)
{
	OSAL_tIODescriptor hDevice1 	= 0;
	OSAL_tIODescriptor hDevice2 	= 0;
	tU32 u32Ret = 0;
    
	/* Open the Flash in  OSAL_EN_READWRITE mode*/
	hDevice1 = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE,OSAL_EN_READWRITE);
	if( OSAL_ERROR != hDevice1 )
	{
		/* Open the Flash in second time with OSAL_EN_READWRITE mode
		open should fail.		*/
		hDevice2 = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE,OSAL_EN_READWRITE);
		if( OSAL_ERROR != hDevice2 )
    	{
			u32Ret = 10;

			/* if open try to close the device */
			if( OSAL_s32IOClose ( hDevice2 ) == OSAL_ERROR )									
			{
				u32Ret += 20;				
    	}

    }

		/* if open try to close the device */
		if( OSAL_s32IOClose ( hDevice1 ) == OSAL_ERROR )									
		{
			u32Ret += 50;				
 		}

	}
	else
	{
		u32Ret = 1;		
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32DevFlashReClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_005
* DESCRIPTION:  Try to Close the flash device which is already closed.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashReClose(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;

    hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
	          
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )	 /* Closing */
		{
			u32Ret = 2;
		}
		else
		{
		    if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )	 /* Re-Closing */
			{
				u32Ret = 3;
			}
		}
			
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32DevFlashReadWrtRU()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_006
* DESCRIPTION:  Read the Flash device's data.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
*                       Updated by Anoop Chandran (RBIN/ECM1)
*						on 15 Feb 2008
*                       Updated by Anoop Chandran (RBIN/ECM1)
*						on 11 Aug 2009
******************************************************************************/

tU32 u32DevFlashReadWrtRU(void)
{
	OSAL_tIODescriptor  	hDevice 												= 0;
	tU32 u32Ret = 0;
	OSAL_trFlashData    rData 													= {0};	
	tS32 s32Ret = 0;
	tChar tcTempBuff[OEDT_FLASH_NUMOFBYTES_TOREAD_WRT] 				= {0};

	/* open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret =+ 1;
    }
    else
	{		
		/* Initialize  the read setup*/	

		rData.u32FlashAddress = u32OEDT_Flashstartoffset();
		rData.u32NumBytes     = (tU32)OEDT_FLASH_NUMOFBYTES_TOREAD_WRT ;
		rData.ps8Buffer       = (tS8*) OSAL_pvMemoryAllocate 
												(
													OEDT_FLASH_NUMOFBYTES_TOREAD_WRT 
													+
													1
												);
		if(rData.ps8Buffer == OEDT_NULL_PTR)
		{
            u32Ret += 2;
		}
		else
		{
			/* start read data from flash */
			s32Ret = OSAL_s32IOControl
						( 
						 	hDevice, 
			                              OSAL_C_S32_IOCTRL_FLASH_READDAT,
							(tS32)&rData
						);
			if ( s32Ret == OSAL_ERROR )
		    {
				u32Ret =+ 50;
		    }
		    else 
		    {
				/* backup the data which write*/
				(tVoid)OSAL_szStringNCopy
				( 
					tcTempBuff, 
					rData.ps8Buffer,
					OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
				);

				/* start write data into flash */
				s32Ret = OSAL_s32IOControl
							( 
								hDevice, 
								OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
								(tS32)&rData
							);
				if( OSAL_ERROR != s32Ret )
						{
					/* clear the buffer to read the data */
					OSAL_pvMemorySet
					(
						rData.ps8Buffer,
						'\0',
						OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
					);

					/* Try to read back the data which write on flash */
					s32Ret = OSAL_s32IOControl
								( 
									hDevice, 
									OSAL_C_S32_IOCTRL_FLASH_READDAT,
									(tS32)&rData
								);

					if(OSAL_ERROR != s32Ret)
						{  	
						/* Compare the read and write data */
						if
						( 
							OSAL_s32StringNCompare
							(
								rData.ps8Buffer,
								tcTempBuff,
								OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
							)
						)
							{
							u32Ret =+ 10;
						}
							}
							else
							{  
						u32Ret =+ 50;
		    }
							}
				else
					{
					u32Ret =+ 100;
					}
		}
			if(OSAL_NULL != rData.ps8Buffer)
        {
				OSAL_vMemoryFree(rData.ps8Buffer);
				rData.ps8Buffer = OSAL_NULL;
			}
        }
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
	 		u32Ret += 200;
        }

	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32DevFlashReadWrtRV()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_007
* DESCRIPTION:  Read the Flash device's data.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
*                       Updated by Anoop Chandran (RBIN/ECM1)
*						on 15 Feb 2008
*                       Updated by Anoop Chandran (RBIN/ECM1)
*						on 11 Aug 2009

******************************************************************************/

tU32 u32DevFlashReadWrtRV(void)
{         
	OSAL_tIODescriptor  hDevice = 0;
	tU32 u32Ret = 0;
	OSAL_trFlashData    rData 													= {0};	
	tS32 s32Ret 																	= 0;
	tChar tcTempBuff[OEDT_FLASH_NUMOFBYTES_TOREAD_WRT] 				= {0};
   
	/* open the device */
	hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE );
	if ( hDevice == OSAL_ERROR )
	{
		u32Ret =+ 1;
	}
	else
	{	
		/* Initialize  the read setup*/	
		rData.u32FlashAddress = u32OEDT_Flashstartoffset() ;
		rData.u32NumBytes     = (tU32)OEDT_FLASH_NUMOFBYTES_TOREAD_WRT ;
		rData.ps8Buffer       = (tS8*) OSAL_pvMemoryAllocate 
												(
													OEDT_FLASH_NUMOFBYTES_TOREAD_WRT 
													+
													1
												);
		if(rData.ps8Buffer == OEDT_NULL_PTR)
		{
			u32Ret += 2;
		}
		else
		{
			/* start read data from flash */
			s32Ret = OSAL_s32IOControl
						( 
						 	hDevice, 
							OSAL_C_S32_IOCTRL_FLASH_READDAT,
							(tS32)&rData
						);
			if ( s32Ret == OSAL_ERROR )
			{
				u32Ret =+ 50;
			}
			else 
			{
				/* backup the data which write*/
				(tVoid)OSAL_szStringNCopy
				( 
					tcTempBuff, 
					rData.ps8Buffer,
					OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
				);
	
				/* start write data into flash */
				s32Ret = OSAL_s32IOControl
							( 
								hDevice, 
								OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
								(tS32)&rData
							);
				if( OSAL_ERROR != s32Ret )
				{
					/* clear the buffer to read the data */
					OSAL_pvMemorySet
					(
						rData.ps8Buffer,
						'\0',
						OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
					);

					/* Try to read back the data which write on flash */
					s32Ret = OSAL_s32IOControl
								( 
									hDevice, 
									OSAL_C_S32_IOCTRL_FLASH_READDAT,
									(tS32)&rData
								);

					if(OSAL_ERROR != s32Ret)
	{
						/* Compare the read and write data */
						if
						( 
							OSAL_s32StringNCompare
							(
								rData.ps8Buffer,
								tcTempBuff,
								OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
							)
						)
						{
							u32Ret =+ 10;
						}
	}
	else
	{
						u32Ret =+ 50;
					}
				}
				else
	    {
					u32Ret =+ 100;
	    }
	}
			if(OSAL_NULL != rData.ps8Buffer)
			{
		OSAL_vMemoryFree(rData.ps8Buffer);
				rData.ps8Buffer = OSAL_NULL;
			}
		}
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
	 	{
	 		u32Ret += 200;
	 	}
	}
	return  u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32DevFlashReadInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_008
* DESCRIPTION:  Try to get the Flash device's data using invalid parameters
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
******************************************************************************/

tU32 u32DevFlashReadInvalParam(void)
{         
	OSAL_tIODescriptor  hDevice = 0;
	tU32 u32Ret = 0;

	/* with invalid parameter */

    hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READONLY );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 3;
    }
    else
	{
		if(OSAL_s32IOControl(hDevice,	
		                 OSAL_C_S32_IOCTRL_FLASH_READDAT,
	                     OSAL_NULL) != OSAL_ERROR)
		{
			u32Ret += 10;
  		}
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
         	u32Ret += 100;
    	}
	}

  	return u32Ret;
 
}

#if 0
/* becasue of driver comment*/
/*****************************************************************************
* FUNCTION:		u32DevFlashWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_009
* DESCRIPTION:  Write data to the Flash device
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
*                       Updated by Anoop Chandran (RBIN/ECM1)
*						on 15 Feb 2008
******************************************************************************/

tU32 u32DevFlashWrite(void)
{
	tU32 u32Ret = 0;
	OSAL_tIODescriptor  hDevice = 0,hFile = 0;
	tS32 s32BytesRead = 0, s32BytesWritten = 0;
	OSAL_trFlashData    rData;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
	tU32  Addresstowrite = (tU32)OEDT_FLASH_STARTADDRESS_WRITETO;
	tU32  SectorSize     =  (tU32)OEDT_FLASH_SECTORSIZE ;
	tU32  ResidentUnitSize = 0, count =0;
	/*tS8  pSrcBuffer[] = "abcdefghijklmnopqrstuvwxyz";*/ /* commented because , dev_flash is 
     used to for Resident unit. so dummy data should not be written to the dev_flash */ 
	tS32 s32Ret = 0;
	tS8 *ps8ReadBuffer = NULL;
	tS32 s32PosToSet = 0;
    hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_WRITEONLY );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
		hFile = OSAL_IOOpen ( OSAL_C_STRING_DRAGON,enAccess );
						if( hFile == OSAL_ERROR )
						{
							u32Ret = 2;
						}
						else
						{  	
						     if( OSAL_s32IOControl (
                                             hFile,OSAL_C_S32_IOCTRL_FIONREAD,
                                             (tS32)&s32PosToSet	) == OSAL_ERROR)

	                 	     {
							   u32Ret = 3;
							   s32PosToSet = 0;
							 }
							 else 
							 {
								ResidentUnitSize = s32PosToSet;
								s32PosToSet = 0;
							 }

							ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate ( OEDT_FLASH_NUMOFBYTES_TOWRITE + 1);
							if ( ps8ReadBuffer == NULL )
							{
								u32Ret += 10;
							}
							else
							{
								while(Addresstowrite <= OEDT_FLASH_DEVSIZE && ResidentUnitSize <= OEDT_FLASH_DEVSIZE 
								     && s32PosToSet <= (ResidentUnitSize + SectorSize) )
								{
											s32BytesRead = OSAL_s32IORead (
																hFile,
																ps8ReadBuffer,
																(tS32) OEDT_FLASH_NUMOFBYTES_TOWRITE
															 );
											if ( s32BytesRead == OSAL_ERROR )
											{
												u32Ret += 100;	
					    					}
											else
								           {
												rData.u32FlashAddress =  Addresstowrite;
												rData.u32NumBytes     = (tU32)OSAL_u32StringLength(ps8ReadBuffer);
												rData.ps8Buffer       = ps8ReadBuffer;
												s32Ret  = OSAL_s32IOControl( hDevice, 
																				  OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
																				  (tS32)&rData);
												if ( s32Ret != OSAL_ERROR)
												{
													vTtfisTrace(OSAL_C_TRACELEVEL1,
													"\tWriting Success\n\tBytes Written = %d \n",s32BytesWritten);

												}
												else 
												{
													vTtfisTrace(OSAL_C_TRACELEVEL1,
														"\tWriting Failed\n\tReturn Value = %d\n",s32BytesWritten);
													u32Ret += 1000;

												}
								
							                  }
											  count++;
									Addresstowrite = (SectorSize * count)+1 ;
									s32PosToSet = 	(SectorSize * count)+1 ;
									if( OSAL_s32IOControl (
							                              hFile,
							                              OSAL_C_S32_IOCTRL_FIOSEEK,
							                              s32PosToSet
							                           ) == OSAL_ERROR )
									{
										u32Ret += 10000;
									}

								}
				if(OSAL_NULL != ps8ReadBuffer )
				{
					free( ps8ReadBuffer );
					ps8ReadBuffer = OSAL_NULL ;
				}
					     	  }
							}

		if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
        {
          u32Ret += 100000;
        }

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
          u32Ret += 200000;
        }
	}
	
	return u32Ret;
}
#endif

/*****************************************************************************
* FUNCTION:		u32DevFlashWriteInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_010
* DESCRIPTION:  Write data to the Flash device using invalid parameter
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashWriteInvalParam(void)
{
	OSAL_tIODescriptor  hDevice = 0;
	tU32 u32Ret = 0;
	//sak9kor - commented to remove lint & compiler warning
	//OSAL_trFlashData    rData;	
	//tS8  pSrcBuffer[30] = "abcdefghijklmnopqrstuvwxyz";
	

    /*rData.u32FlashAddress = (tU32)OEDT_FLASH_STARTADDRESS_WRITETO ;
	rData.u32NumBytes     = (tU32)OSAL_u32StringLength(pSrcBuffer);
	rData.ps8Buffer       = pSrcBuffer;*/
    	
	/* with invalid parameter */

    hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_WRITEONLY );
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 2;
    }
    else
	{
		if
		(
			OSAL_s32IOControl
			(
				hDevice,	
		                 OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
				OSAL_NULL
			) 
			!= 
			OSAL_ERROR
		)
		{
		 	u32Ret += 10;
  		}
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
    		u32Ret += 100;
    	}
	}
  	return u32Ret;
}

#if 0
/*****************************************************************************
* FUNCTION:		u32DevFlashPredefinedWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_011
* DESCRIPTION:  Write the pre-defined data to the Flash device
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
******************************************************************************/


tU32 u32DevFlashPredefinedWrite(void)
{
	tU32 u32Ret = 0;


	OSAL_tIODescriptor  hDevice = 0;
	tPS32 s32Ret = NULL;
	tU32 u32PartBytesToWrite = 0;
	tU32 u32BytesToWrite = OEDT_FLASH_NUMOFBYTES_PREDEFWRITE;
    tU32 u32Offset = 0;
	tBool bContinueWrite = OEDT_SUCCESS ;
	tS32 s32BytesWritten = 0;
	OSAL_trFlashData    rData;
	
	rData.u32FlashAddress = (tU32)OEDT_FLASH_ADDRESS_WRITETO ;
	rData.u32NumBytes     = (tU32)OEDT_FLASH_NUMOFBYTES_PREDEFWRITE;  
	rData.ps8Buffer       = (tS8*)OSAL_pvMemoryAllocate(rData.u32NumBytes + 1);	
    if(rData.ps8Buffer == OEDT_NULL_PTR)
	{
         u32Ret = 1;
	}
	else
	{
	     hDevice = OSAL_IOOpen (OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_WRITEONLY );
	     if ( hDevice ==  OSAL_ERROR)
	     {
	          u32Ret = 2;
	     }
	     else
	     {
		      do
              {
                     if(u32BytesToWrite > 1024)
                     {
                             u32PartBytesToWrite =  1024;
                             u32BytesToWrite     -= 1024;
                     }
                     else
                     {
                             u32PartBytesToWrite = u32BytesToWrite;

      /* Under the boundary condition where bytes to write is 1024,
       we need to write the array (sized 1024) only once. */
                            
                             if(u32BytesToWrite == 1024)
                             {
                                bContinueWrite = OEDT_FAILURE;
                             }
                     }

                     s32Ret =(tPS32 )OSAL_pvMemoryCopy(rData.ps8Buffer + u32Offset,
                                               (tU8*)as8FLASHMesgToWrite,
                                               u32PartBytesToWrite);
					 if ( s32Ret == OSAL_NULL )
                     {
					      	vTtfisTrace(OSAL_C_TRACELEVEL1,
				            "\n\tMemory Copy failed\n\tReturn value : %d ",
				            s32Ret);
						    if(rData.ps8Buffer != OEDT_NULL_PTR)
                            {
                               OSAL_vMemoryFree(rData.ps8Buffer);
                            }
							u32Ret = 3;
							return u32Ret;
					 }
					 u32Offset += u32PartBytesToWrite;
			 }
			 
			 while((u32PartBytesToWrite == 1024) &&
			                            (bContinueWrite == OEDT_SUCCESS));

			 s32BytesWritten = OSAL_s32IOControl(hDevice,
			                                 OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
			                                 (tS32)&rData);

             if(s32BytesWritten == OSAL_ERROR)
             {
			      bContinueWrite = OEDT_FAILURE;
				  u32Ret        += 10 ;
				  vTtfisTrace(OSAL_C_TRACELEVEL1,
				  "\n\tPredefined Write  failed\n\tReturn value : %d ",
				  s32BytesWritten);
			 }
			 else
			 {
			      vTtfisTrace(OSAL_C_TRACELEVEL1,
				  "\n\tPredefined Write Success\n\tReturn value : %d ",
				  s32BytesWritten);
   
			 }

			 if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
             {
                 u32Ret += 100;
             }


	     }
	     if(rData.ps8Buffer != OEDT_NULL_PTR)
		     OSAL_vMemoryFree(rData.ps8Buffer); 
	}
 	return u32Ret;
}

#endif
#if 0

/*****************************************************************************
* FUNCTION:		u32DevFlashWriteReadCombine()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_012
* DESCRIPTION:  Write and then read the data from the flash device
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*                       Updated by Anoop Chandran (RBIN/EDI3)
*						on 27 September 2007
******************************************************************************/

tU32 u32DevFlashWriteReadCombine(void)
{
	tU32 u32Ret = 0;
    tS32 s32Ret = 0;
    OSAL_tIODescriptor hDevice = 0;
    OSAL_trFlashData   rData;
    tS8 pSrcBuffer[] = "abcdefghijklmnopqrstuvwxyz";
    tS8*  pBuffer = OSAL_NULL;
  
    hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
    if(hDevice == OSAL_ERROR)
    {
      u32Ret = 1;
    }
    else
    {
      rData.u32NumBytes     =OSAL_u32StringLength(pSrcBuffer);
      rData.u32FlashAddress = OEDT_FLASH_ADDRESS_WRITETO ;
      rData.ps8Buffer       = pSrcBuffer;

      if(OSAL_s32IOControl(hDevice,
                           OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
                           (tS32)&rData) == OSAL_ERROR)
      {
        u32Ret = 2;
      }
      else
      {
          pBuffer = (tS8*)OSAL_pvMemoryAllocate(OSAL_u32StringLength
                                                            (pSrcBuffer) + 1);
		  rData.u32NumBytes     = OSAL_u32StringLength(pSrcBuffer);
          rData.u32FlashAddress = OEDT_FLASH_ADDRESS_READ ;
          rData.ps8Buffer       = pBuffer;

		  if(pBuffer == OEDT_NULL_PTR)
	      {
          	u32Ret = 1;
		  }
		  else
	      {
          	  s32Ret = OSAL_s32IOControl(hDevice,
                                     OSAL_C_S32_IOCTRL_FLASH_READDAT,
                                      (tS32)&rData);

          	  if((s32Ret == OSAL_ERROR) ||
                     (OSAL_s32StringNCompare(pSrcBuffer, rData.ps8Buffer, 26) != 0))
          	  {
            	  u32Ret = 3;
          	  }
          	  if(pBuffer!= OEDT_NULL_PTR)
   		            OSAL_vMemoryFree(pBuffer);
		  }
      }

      if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
      {
          u32Ret += 10;
      }
    }
    return u32Ret;
}
#endif

#define OEDT_FLASH_TEST
#ifdef OEDT_FLASH_TEST
/*define "OEDT_FLASH_TEST" only when testing*/
/*****************************************************************************
* FUNCTION:		u32DevFlashErase()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_013
* DESCRIPTION:  Erase the specified block of data from falsh device.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashErase(void)
{
	OSAL_tIODescriptor  hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	
	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if(hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
    {
		s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_FLASH_ERASE,
							       OEDT_FLASH_NUMOFBLOCKS_TOERASE);
		if ( s32Ret == OSAL_ERROR )
		{
			u32Ret = 10;
			/*vTtfisTrace(OSAL_C_TRACELEVEL1,
				"\n\tErasing failed\n\tReturn value : %d ",s32Ret);*/
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
			                  "\n\tErasing failed\n\tReturn value : %d ",
			                  s32Ret);

		}
        if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
          u32Ret += 100;
        }
	}
    return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32DevFlashEraseInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_014
* DESCRIPTION:  Try to erase a specified block of data using 
*				invalid parameter.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/


tU32 u32DevFlashEraseInvalidParam(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	tS32 s32NumofBlockInval = -1;

	/* with invalid device handle */

    hDevice = INVALID_HANDLE;
    s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_FLASH_ERASE,
							       OEDT_FLASH_NUMOFBLOCKS_TOERASE);
	if ( s32Ret != OSAL_ERROR )
	{
		u32Ret = 1;
	}

	/* with invalid Block No. */

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 2;
    }
    else
	{
		s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_FLASH_ERASE,
							       s32NumofBlockInval);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret += 10;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
        	u32Ret += 100;
    	}
	}
	
	return u32Ret;
}
#endif
#if 0
/*Because of Driver comment*/

/*****************************************************************************
* FUNCTION:		u32DevFlashGetRegionInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_015
* DESCRIPTION:  Get the information on erase block regions within the
                Flash memory using correct parameters.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashGetRegionInfo(void)
{
	tU32 u32Ret = 0;



	OSAL_tIODescriptor    hDevice = 0;
	tS32 s32Ret = 0;
  	OSAL_trRegionInfo   rRegionInfo ;

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if(hDevice == OSAL_ERROR)
    {
      u32Ret = 1;
    }
    else
    {
		s32Ret = OSAL_s32IORead(hDevice,(tS8*)&rRegionInfo,
								 (tS32)OEDT_FLASH_NUMOFREGIONS);

        if ( s32Ret == OSAL_ERROR )
		{
			u32Ret = 10;
			vTtfisTrace(OSAL_C_TRACELEVEL1,
				"\n\tGetting Region Info failed\n\tReturn value : %d ",s32Ret);
		}
		else
		{
			vTtfisTrace(OSAL_C_TRACELEVEL1,
                        "\n\tGet Region Info Success\n\tReturn value : %d\
                         \n\tNum of identical size erase block : %d\
						 \n\tBlock size in the region : %d",
                         s32Ret,
                         rRegionInfo.u16NumIdentblock,
                         rRegionInfo.u16BlockSize);				  									 

		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
          u32Ret += 100;
        }
		
	}
    return u32Ret;
}
#endif	


#if 0

/*****************************************************************************
* FUNCTION:		u32DevFlashGetRegionInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_016
* DESCRIPTION:  Try to get the information on erase block regions within the
                Flash memory using incorrect parameters.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/


tU32 u32DevFlashGetRegionInfoInvalParam(void)
{
	tU32 u32Ret = 0;

/*Because of Driver comment*/

	OSAL_tIODescriptor    hDevice = 0;
	tS32 s32Ret = 0;
	tU8  u8NumofRegionsInval = -1;
	OSAL_trRegionInfo   rRegionInfo ;

    	/* with invalid parameter */

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 2;
    }
    else
	{
		s32Ret = OSAL_s32IORead(hDevice,(tPS8)INVALID_PARAMETER,
		                                       (tS32)OEDT_FLASH_NUMOFREGIONS);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret += 10;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
        	u32Ret += 100;
        }
	}

	/* with invalid Number of regions */

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 5;
    }
    else
	{
		s32Ret = OSAL_s32IORead(hDevice,(tS8*)&rRegionInfo,
		                                       (tS32)u8NumofRegionsInval);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret += 20;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
        	u32Ret += 200;
        }
	}


	return u32Ret;

}

#endif

/*****************************************************************************
* FUNCTION:		u32DevFlashGetVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_017
* DESCRIPTION:  Get the device version number.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashGetVersion(void)
{
	OSAL_tIODescriptor   hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	tS32 s32Version = 0;

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
    if(hDevice == OSAL_ERROR)
    {
      u32Ret =  1;
    }
    else
    {
		s32Ret = OSAL_s32IOControl( hDevice,
			                        OSAL_C_S32_IOCTRL_VERSION,
								    (tS32)&s32Version );
							       
		if ( s32Ret == OSAL_ERROR )
		{
			u32Ret = 10;
			/*vTtfisTrace(OSAL_C_TRACELEVEL1,
				"\n\tGetting device version failed\n\tReturn value : %d ",
				s32Ret);*/
			 OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                       "\n\tGetting device version failed\n\tReturn value : %d",
		                       s32Ret);

		}
		else
		{
			/*vTtfisTrace(OSAL_C_TRACELEVEL1,
				"\n\tGet Version Success\n\tVersion Number : %s",
						s32Version); */
			 OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                       "\n\tGet Version Success\n\tVersion Number : %d",
		                       s32Version);

	    }
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
          u32Ret += 100;
        }
		
	}
	
    return u32Ret;
}




/*****************************************************************************
* FUNCTION:		u32DevFlashGetVersionInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_018
* DESCRIPTION:  Try to get the Flash Device Information 
                using in-correct parameters.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashGetVersionInvalParam(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
   
	/* with invalid parameter */

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 2;
    }
    else
	{
		s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_VERSION,
							       OSAL_NULL);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret += 10;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
        	u32Ret += 100;
        }
	}

	return u32Ret;
}




/*****************************************************************************
* FUNCTION:		u32DevFlashGetInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_019
* DESCRIPTION:  Get the Flash Device Information using correct parameters.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashGetInfo(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	OSAL_trFlashInfo rFlashInfo;

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if(hDevice == OSAL_ERROR)
    {
      u32Ret = 1;
    }
    else
    {
		s32Ret = OSAL_s32IOControl( hDevice,
			                        OSAL_C_S32_IOCTRL_FLASH_GETINFO,
								    (tS32)&rFlashInfo );
	   	if ( s32Ret == OSAL_ERROR )
		{
			u32Ret = 10;
			/*vTtfisTrace(OSAL_C_TRACELEVEL1,
				"\n\tGetting Info failed\n\tReturn value : %d ",s32Ret);*/
			 OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                      "\n\tGetting Info failed\n\tReturn value : %d ",
		                      s32Ret);

		}
		else
		{
			/*vTtfisTrace(OSAL_C_TRACELEVEL1, 
		                "\n\tNum of erase block region : %d\
						 \n\tThe device size : %d\
						 \n\tThe Interface Code %d",
		                 rFlashInfo.u8NumRegions,
		                 rFlashInfo.u8DevSize,
		                 rFlashInfo.u8IntCode);*/
			 OEDT_HelperPrintf(TR_LEVEL_USER_1,
		                "\n\tNum of erase block region : %d\
						 \n\tThe device size : %d\
						 \n\tThe Interface Code %d",
		                 rFlashInfo.u8NumRegions,
		                 rFlashInfo.u8DevSize,
		                 rFlashInfo.u8IntCode);
		    									 									 

		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
        {
          u32Ret += 100;
        }
	}
    return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32DevFlashGetInfoInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_020
* DESCRIPTION:  Try to get the Flash Device Information 
                using in-correct parameters.
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashGetInfoInvalParam (void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;


	/* with invalid parameter */

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret += 2;
    }
    else
	{
		s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_FLASH_GETINFO,
							       OSAL_NULL);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret += 10;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
    		u32Ret += 100;
    	}
	}
	return u32Ret;
}



/*****************************************************************************
* FUNCTION:		u32DevFlashInvalIOCtlMacro
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_021
* DESCRIPTION:  Test the IOControl Function with invalid macro
* HISTORY:		Created Anoop Krishnan(RBIN/ECM1)  July 31,2006 
*
******************************************************************************/

tU32 u32DevFlashInvalIOCtlMacro(void)
{
	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	OSAL_trFlashInfo rFlashInfo;

	hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
		s32Ret = OSAL_s32IOControl(hDevice,
			                       OSAL_C_S32_IOCTRL_FLASH_INVALID,
							       (tS32)&rFlashInfo);
		if ( s32Ret != OSAL_ERROR )
		{
			u32Ret = 10;
		}

		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
    		u32Ret += 100;
    	}
	}

  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32WriteFlashWithInvalAddr( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_022
* DESCRIPTION:  Write to Flash with invalid address (invalid address is 
				a part of the NOR FLASH (Corresponds to dev_kds)
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 14,2007 

*
******************************************************************************/

tU32 u32WriteFlashWithInvalAddr(void)
{
	OSAL_tIODescriptor  	hDevice 	= 0;
 tU32 u32Ret = 0;
	OSAL_trFlashData    wData 		= {0};	
	tS32 s32Ret 						= 0;

	/* open the flsh device*/
 hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
		u32Ret =+ 1;
    }
   else
	{
		/* Initialize  write setup for invalid address*/
		wData.u32FlashAddress = (tU32)OEDT_FLASH_INVAL_START_OFFSET ;
		wData.u32NumBytes     = (tU32)OEDT_FLASH_NUMOFBYTES_TOREAD_WRT ;
		wData.ps8Buffer       = (tS8*) OSAL_pvMemoryAllocate 
												(
													OEDT_FLASH_NUMOFBYTES_TOREAD_WRT 
													+
													1
												);
		if(wData.ps8Buffer == OEDT_NULL_PTR)
      {
        u32Ret += 2;
    	}
		else
		{
			OSAL_pvMemorySet
			(
				wData.ps8Buffer,
				'\0',
				OEDT_FLASH_NUMOFBYTES_TOREAD_WRT
			);
			
			/* Write data to flash */
			s32Ret = OSAL_s32IOControl
						( 
							hDevice, 
							OSAL_C_S32_IOCTRL_FLASH_WRITEDAT,
							(tS32)&wData
						);
			if( OSAL_ERROR != s32Ret )
			{
				u32Ret =+ 100;	
				}
			if(OSAL_NULL != wData.ps8Buffer )
				{
				OSAL_vMemoryFree(wData.ps8Buffer );
				wData.ps8Buffer  = OSAL_NULL;
				}
			}
	
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
    		u32Ret += 100;
    	}

	}
 return u32Ret;

 }
/*****************************************************************************
* FUNCTION:		u32ReadFlashWithInvalAddr( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_023
* DESCRIPTION:  Write to Flash with invalid address. Invalid address is a 
				part of the NAND FLASH.
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 14,2007 
*
******************************************************************************/

tU32 u32ReadFlashWithInvalAddr(void)	
{
 OSAL_tIODescriptor hDevice=0;
  tU32 u32Ret = 0;
	OSAL_trFlashData    rData 		= {0};	
	tS32 s32Ret 						= 0;

	/* open the flsh device*/
 hDevice = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
	if ( hDevice == OSAL_ERROR )
    {
		u32Ret =+ 1;
    }
    else
	{
		/* Initialize  read setup for valid address*/	
		rData.u32FlashAddress = (tU32)OEDT_FLASH_INVAL_START_OFFSET ;
		rData.u32NumBytes     = (tU32)OEDT_FLASH_NUMOFBYTES_TOREAD_WRT ;
		rData.ps8Buffer       = (tS8*)OSAL_pvMemoryAllocate 
												(
													OEDT_FLASH_NUMOFBYTES_TOREAD_WRT 
													+
													1
												);
		if(rData.ps8Buffer == OEDT_NULL_PTR)
      {
        u32Ret += 2;
	  }
		else
      {
			/* Read data from flash */
			s32Ret = OSAL_s32IOControl
						( 
							hDevice, 
							OSAL_C_S32_IOCTRL_FLASH_READDAT,
							(tS32)&rData
						);
			if ( s32Ret != OSAL_ERROR )
			{
				u32Ret =+ 50;
      }
		
		}
		if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    	{
    		u32Ret += 100;
	}

	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		 u32DevFlashHeavyParallelAccess_nor0()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_024 
* DESCRIPTION:  
* HISTORY:	    Anoop Chandran (RBEI/ECM1): Initial version
*
******************************************************************************/

tU32 u32DevFlashHeavyParallelAccess_nor0(void)
{
    
  tU8 * pu8pathname = ( tU8 *)OEDT_FLASH_HELPER_FILE_NAME_NOR0; 
    
    return((tU32) s32_start_all_working_tsk(pu8pathname));
}

/*****************************************************************************
* FUNCTION:		 u32DevFlashHeavyParallelAccess_nand0()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FLASH_025 
* DESCRIPTION:  
* HISTORY:	    Anoop Chandran (RBEI/ECM1): Initial version
*
******************************************************************************/

tU32 u32DevFlashHeavyParallelAccess_nand0(void)
{
    
  tU8 * pu8pathname = ( tU8 *)OEDT_FLASH_HELPER_FILE_NAME_NAND0; 
    
    return((tU32) s32_start_all_working_tsk(pu8pathname));
}


#if 0 /* Commented by Patil: Earlier test cases in this file are commented. Only 
       RBIN test cases are used for current testing. In next release, we will try 
       to include these test cases also in our test specs */
        
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 


/*********************************************************************FunctionHeaderBegin***
* u32FLASHMultipleDevOpen
* tests posibility to open the device twice (read+write)
**********************************************************************FunctionHeaderEnd***/
tU32 u32FLASHMultipleDevOpen(void)
{
  tU32 u32Ret = 0;
  OSAL_tIODescriptor hFd1;
  OSAL_tIODescriptor hFd2;

  hFd1 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    hFd2 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
    if(hFd2 != OSAL_ERROR)
    {
      u32Ret = 2;
      if(OSAL_s32IOClose(hFd2) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32FLASHMultDevOpenRO()
* tests posibility to open the device twice (readonly)
**********************************************************************FunctionHeaderEnd***/
tU32 u32FLASHMultDevOpenRO(void)
{
  tU32 u32Ret = 0;
  OSAL_tIODescriptor hFd1;
  OSAL_tIODescriptor hFd2;

  hFd1 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READONLY);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    hFd2 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READONLY);
    if(hFd2 != OSAL_ERROR)
    {
      u32Ret = 2;
      if(OSAL_s32IOClose(hFd2) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}

/*********************************************************************FunctionHeaderBegin***
* u32FLASHGetInfo()
* gets flash info
**********************************************************************FunctionHeaderEnd***/
tU32 u32FLASHGetInfo(void)
{
  tU32 u32Ret = 0;
  OSAL_tIODescriptor hFd1;
  OSAL_trFlashInfo   sFlashInfo;

  hFd1 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READONLY);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    sFlashInfo.u8DevSize    = 0;
    sFlashInfo.u8IntCode    = 0;
    sFlashInfo.u8NumRegions = 0;

    if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FLASH_GETINFO, (tS32)&sFlashInfo) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if((sFlashInfo.u8DevSize == 0) || (sFlashInfo.u8IntCode == 0))
      {
        u32Ret = 3;
      }
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}



#if 0
//
// TMS-Reich 23.02.2006
// Not called. This is quite evident, since writing to block 1 would cause real problems
// DO NOT OUTCOMMENT and NEVER CALL without according changes
//
/*********************************************************************FunctionHeaderBegin***
* u32FLASHWriteAndRead()
* AUTHOR: tnn
* CALLED: YES
* gets flash info
**********************************************************************FunctionHeaderEnd***/
tU32 u32FLASHWriteAndRead(void)
{
  tU32 u32Ret = 0;
  tS32 s32Ret = 0;
  OSAL_tIODescriptor hFd1;
  OSAL_trFlashData   sData;
  tS8*  pSrcBuffer = (void*)"abcdefghijklmnopqrstuvwxyz";
  tS8*  pBuffer;
  
  hFd1 = OSAL_IOOpen(OSAL_C_STRING_FLASH_DEVICE, OSAL_EN_READWRITE);
  if(hFd1 == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    sData.u32NumBytes     = 26;
    // sData.u32FlashAddress = au32BlockOffset[1];
    LLD_FLASH_BlockAddress(&sData.u32FlashAddress, 1);
    sData.ps8Buffer       = pSrcBuffer;

    if(OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FLASH_WRITEDAT, (tS32)&sData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      pBuffer = (tS8*)malloc(26);

      sData.u32NumBytes     = 26;
      // sData.u32FlashAddress = au32BlockOffset[1];
      LLD_FLASH_BlockAddress(&sData.u32FlashAddress, 1);
      sData.ps8Buffer       = pBuffer;

      s32Ret = OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FLASH_READDAT, (tS32)&sData);
      if((s32Ret == OSAL_ERROR) || (strncmp((const char*)pSrcBuffer, (const char*)sData.ps8Buffer, 26) != 0))
      {
        u32Ret = 3;
      }

      free(pBuffer);
    }

    if(OSAL_s32IOClose(hFd1) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}
#endif

#endif /* Commented by Patil */

/*****************************************************************************
* FUNCTION:		 u32OEDT_Flashstartoffset()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:   fuction to get the start of offset of flash
* HISTORY:	    Anoop Chandran (RBEI/ECM1): Initial version
*					11 Aug 2009
					update the flash address by anc2hi, 29 march 2010
******************************************************************************/

tU32 u32OEDT_Flashstartoffset(tVoid)
{
   tU32 u32testflashoffset = 0;
   tU32 u32HWID = 0 ;
	u32HWID = u32OEDT_BoardName();
	switch(u32HWID)
	{
		case OEDT_GMGE:
		{
			u32testflashoffset = OEDT_FLASH_RU_GMGE_START_OFFSET;
			break;
		}
		case OEDT_FORD:
		{
			u32testflashoffset = OEDT_FLASH_RU_MFD_START_OFFSET;
			break;
		}
		case OEDT_JLR:
		{
			u32testflashoffset = OEDT_FLASH_JLR_START_OFFSET;	//ROW5 start address is taken, since JLR is used 8MB
			break;
		}
		case OEDT_ICM:
		{
			u32testflashoffset = OEDT_FLASH_ICM_START_OFFSET;
			break;
		}
		default:
		{
			// does nothing
			break;
		}
	
	}
	return u32testflashoffset;
  
}

/* EOF */
