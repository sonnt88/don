/******************************************************************************
 * FILE         : oedt_USB_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the file system test cases for USB  
 *                          
 * AUTHOR(s)    :  Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * --.--.--       | Initial revision    | ------------
 * 2 Dec, 2008    | version 1.0         | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   | version 1.1         | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Mar, 2009	| version 1.2		    | Lint Removal
 *			         |						| Update by Shilpa Bhat(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 20 May, 2009   | version 1.3		| Addes new OEDTs TU_OEDT_USB_CFS_091,92,93
 *			         |						| Update by Anoop Chandran(RBEI/ECF1)
  *-----------------------------------------------------------------------------
 * 10 Sep, 2009   | version 1.4    	|  Commenting the Test cases which uses 
 *					   |  					|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *					   |	   				|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010   | version 1.5   |  Updated the Device name 
 *				      |   			    |  Anoop Chandran (RBEI/ECF1)
 * 26 Feb, 2013   | vERSION 1.6   | Removed u32USB_CommonFSPrepareEject()
 *                    |                 |	Under unsed io control removal
 *                    |                 |	By SWM2KOR
 ------------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"
#include "oedt_USB_CommonFS_TestFuncs.h"

#ifdef SWITCH_OEDT_USB
#define OEDTTEST_C_STRING_DEVICE_USB "/dev/uda1"
#else
#define OEDTTEST_C_STRING_DEVICE_USB OSAL_C_STRING_DEVICE_USB
#endif

#define OEDT_USB_CFS_COMMONFILE			OEDTTEST_C_STRING_DEVICE_USB"/commonfile.txt"
#define OEDT_USB_CFS_DUMMYFILE			OEDTTEST_C_STRING_DEVICE_USB"/Dummy.txt"
#define OEDT_USB_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_USB"/Test.txt"
#define OEDT_USB_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_USB"/DIR/Test.txt"
#define OEDT_USB_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_USB"/��汪�.txt"
#define OEDT_USB_CFS_INVALCHAR_FILE		OEDTTEST_C_STRING_DEVICE_USB"//*/</>>.txt"
#ifndef HIWORD
  #undef HIWORD	
  #define HIWORD( dwValue )	((tU16)((((tU32)( dwValue )) >> 16) & 0xFFFF))
#endif
#define OEDT_USB_ACTIVATE_POST_PATTERN  1
#define OEDT_USB_DEACTIVATE_POST_PATTERN  2
#define OEDT_USB_EVENT_NAME "OEDT_USB_Event_Name"

OSAL_tEventHandle  tOEDT_USBEveHandle        = 0;

static tVoid vInsertNotiHandlerUSBH(const tU32* );

/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpenClosedevice( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_001
* DESCRIPTION	:	Opens and closes device
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSOpenClosedevice(tVoid)
{ 
	tU32 u32Ret = 0;

	u32Ret = u32FSOpenClosedevice((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpendevInvalParm( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_002
* DESCRIPTION	:	Try to Open device with invalid parameters
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSOpendevInvalParm(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpendevInvalParm((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSReOpendev( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_003
* DESCRIPTION	:	Try to Re-Open device
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSReOpendev(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSReOpendev((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpendevDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_004
* DESCRIPTION	:	Try to open device with different modes
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSOpendevDiffModes(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpendevDiffModes((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSClosedevAlreadyClosed( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_005
* DESCRIPTION	:	Try to close an already closed device
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSClosedevAlreadyClosed(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSClosedevAlreadyClosed((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpenClosedir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_006
* DESCRIPTION	:	Try to Open and closes a directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSOpenClosedir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSOpenClosedir((const tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpendirInvalid( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_FILESYSTEM_007
* DESCRIPTION	:	Try to Open invalid directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSOpendirInvalid(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
					(tPS8)OEDT_C_STRING_USB_CFS_NONEXST,
					(tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT
					};

	u32Ret = u32FSOpendirInvalid(dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateDelDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_008
* DESCRIPTION	:	Try create and delete directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSCreateDelDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSCreateDelDir((const tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateDelSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_009
* DESCRIPTION	:	Try create and delete sub directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSCreateDelSubDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSCreateDelSubDir((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateDirInvalName( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_010
* DESCRIPTION	:	Try create and delete directory with invalid name
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSCreateDirInvalName(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
						(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
						(tPS8)OEDT_C_STRING_USB_CFS_DIR_INV_NAME
						};
	u32Ret = u32FSCreateDirInvalName(dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSRmNonExstngDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_011
* DESCRIPTION	:	Try to remove non exsisting directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSRmNonExstngDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSRmNonExstngDir((const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSRmDirUsingIOCTRL( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_012
* DESCRIPTION	:	Delete directory with which is not a directory (with files) 
*						using IOCTRL
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSRmDirUsingIOCTRL(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)OEDT_C_STRING_USB_CFS_FILE1
							};
	u32Ret = u32FSRmDirUsingIOCTRL( dev_name );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSGetDirInfo( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_013
* DESCRIPTION	:	Get directory information
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSGetDirInfo(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetDirInfo((const tPS8)OEDTTEST_C_STRING_DEVICE_USB, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSOpenDirDiffModes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_014
* DESCRIPTION	:	Try to open directory in different modes
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSOpenDirDiffModes(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)SUBDIR_PATH_USB_CFS
							};

	u32Ret = u32FSOpenDirDiffModes(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSReOpenDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_015
* DESCRIPTION	:	Try to reopen directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSReOpenDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT,
							(tPS8)OEDT_C_STRING_USB_CFS_DIR1
							};

	u32Ret = u32FSReOpenDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSDirParallelAccess( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_016
* DESCRIPTION	:	When directory is accessed by another thread try to rename it
*						delete it
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSDirParallelAccess(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSDirParallelAccess((const tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT); 
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateDirMultiTimes( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_017
* DESCRIPTION	:	Try to Create directory with similar name 
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSCreateDirMultiTimes(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
								(tPS8)OEDT_C_STRING_DEVICE_USB_CFS_ROOT,
								(tPS8)SUBDIR_PATH_USB_CFS,
								(tPS8)SUBDIR_PATH2_USB_CFS
								};

	u32Ret = u32FSCreateDirMultiTimes(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateRemDirInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_018
* DESCRIPTION	:	Create/ remove directory with Invalid path
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
tU32 u32USB_CommonFSCreateRemDirInvalPath(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)OEDT_C_STRING_USB_CFS_DIR_INV_PATH
							};

	u32Ret = u32FSCreateRemDirInvalPath(dev_name);
	return u32Ret;
}



/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateRmNonEmptyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_019
* DESCRIPTION	:	Try to remove non Empty directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSCreateRmNonEmptyDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)SUBDIR_PATH_USB_CFS
							};

	u32Ret = u32FSCreateRmNonEmptyDir(dev_name, TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCopyDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_020
* DESCRIPTION	:	Copy the files in source directory to destination directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSCopyDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
									(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
									(tPS8)FILE13_USB_CFS,
									(tPS8)FILE14_USB_CFS
									};

	u32Ret = u32FSCopyDir(dev_name,TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSMultiCreateDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_021
* DESCRIPTION	:	Create multiple sub directories
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSMultiCreateDir(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSMultiCreateDir((const tPS8)OEDTTEST_C_STRING_DEVICE_USB, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCreateSubDir( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_022
* DESCRIPTION	:	Attempt to create sub-directory within a directory opened
* 						in READONLY modey
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32 u32USB_CommonFSCreateSubDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
								(tPS8)SUBDIR_PATH_USB_CFS
								};

	u32Ret = u32FSCreateSubDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSDelInvDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_023
* DESCRIPTION	:	Delete invalid Directory
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSDelInvDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[2] = {
								(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
								(tPS8)OEDT_C_STRING_USB_CFS_DIR_INV_NAME
								};

	u32Ret = u32FSDelInvDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSCopyDirRec( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_024
* DESCRIPTION	:	Copy directories recursively
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
tU32  u32USB_CommonFSCopyDirRec(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[3] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)FILE13_USB_CFS,
							(tPS8)FILE14_USB_CFS
							};

	u32Ret = u32FSCopyDirRec(dev_name);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSRemoveDir ( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_025
* DESCRIPTION	:	Remove directories recursively
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSRemoveDir(tVoid)
{
	tU32 u32Ret = 0;
	tPS8 dev_name[4] = {
							(tPS8)OEDTTEST_C_STRING_DEVICE_USB,
							(tPS8)FILE11_USB_CFS,
							(tPS8)FILE12_USB_CFS,
							(tPS8)FILE12_RECR2_USB_CFS
							};

	u32Ret = u32FSRemoveDir(dev_name, TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileCreateDel( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_026
* DESCRIPTION	:  Create/ Delete file 
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileCreateDel(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileCreateDel((const tPS8)CREAT_FILE_PATH_USB_CFS);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileOpenClose( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_027
* DESCRIPTION	:	Open /close File
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileOpenClose(tVoid)
{
	tU32 u32Ret = 0;

	u32Ret = u32FSFileOpenClose((const tPS8)OSAL_TEXT_FILE_FIRST_USB_CFS, (tBool)TRUE);
	return u32Ret;
}	


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileOpenInvalPath( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_028
* DESCRIPTION	:  Open file with invalid path name (should fail)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileOpenInvalPath(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileOpenInvalPath((const tPS8)OEDT_C_STRING_FILE_INVPATH_USB_CFS);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileOpenInvalParam( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_029
* DESCRIPTION	:	Open a file with invalid parameters (should fail), 
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileOpenInvalParam(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileOpenInvalParam((const tPS8)OSAL_TEXT_FILE_FIRST_USB_CFS, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReOpen( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_030
* DESCRIPTION	:  Try to open and close the file which is already opened
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32  u32USB_CommonFSFileReOpen(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReOpen((const tPS8)OSAL_TEXT_FILE_FIRST_USB_CFS, (tBool)TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileRead( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_031
* DESCRIPTION	:	Read data from file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileRead(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileRead((const tPS8)OEDT_USB_CFS_COMNFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileWrite( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_032
* DESCRIPTION	:  Write data to file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileWrite(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileWrite((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);	
	return u32Ret;
}	


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSGetPosFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_033
* DESCRIPTION	:  Get File Position from begining of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSGetPosFrmBOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetPosFrmBOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSGetPosFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_034
* DESCRIPTION	:	Get File Position from end of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSGetPosFrmEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetPosFrmEOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_035
* DESCRIPTION	:	Read with a negative offset from BOF
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32   u32USB_CommonFSFileReadNegOffsetFrmBOF()
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadNegOffsetFrmBOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_036
* DESCRIPTION	:	Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileReadOffsetBeyondEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetBeyondEOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadOffsetFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_037
* DESCRIPTION	:  Read from offset from Beginning of file
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileReadOffsetFrmBOF(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetFrmBOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadOffsetFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_038
* DESCRIPTION	:	Read file with few offsets from EOF
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileReadOffsetFrmEOF(tVoid)
{	
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadOffsetFrmEOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadEveryNthByteFrmBOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_039
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*                   (From BOF)
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileReadEveryNthByteFrmBOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadEveryNthByteFrmBOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSFileReadEveryNthByteFrmEOF( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_040
* DESCRIPTION	:	Reads file by skipping certain offsets at specified intervals.
*						(This is from EOF)		
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSFileReadEveryNthByteFrmEOF(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSFileReadEveryNthByteFrmEOF((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION		:	u32USB_CommonFSGetFileCRC( )
* PARAMETER		:	none
* RETURNVALUE	:	tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE		:	TU_OEDT_USB_CFS_041
* DESCRIPTION	:	Get CRC value
* HISTORY		:	Created by Shilpa Bhat (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
tU32 u32USB_CommonFSGetFileCRC(tVoid)
{
	tU32 u32Ret = 0;
	
	u32Ret = u32FSGetFileCRC((const tPS8)OEDT_USB_CFS_WRITEFILE, (tBool)TRUE);
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32USB_CommonFSReadAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_042
* DESCRIPTION:  Read data asyncronously from a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSReadAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSReadAsync((tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSLargeReadAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_043
* DESCRIPTION:  Read data asyncronously from a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSLargeReadAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeReadAsync((tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_044
* DESCRIPTION:  Write data asyncronously to a file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteAsync((tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSLargeWriteAsync()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_045
* DESCRIPTION:  Write data asyncronously to a large file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSLargeWriteAsync(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeWriteAsync((tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteOddBuffer()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_046
* DESCRIPTION:  Write data to an odd buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteOddBuffer(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteOddBuffer((tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteFileWithInvalidSize()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_047
* DESCRIPTION:  Write data to a file with invalid size
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteFileWithInvalidSize(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileWithInvalidSize
				(
					(tPS8)OEDT_USB_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteFileInvalidBuffer()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_048
* DESCRIPTION:  Write data to a file with invalid buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteFileInvalidBuffer(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileInvalidBuffer
				(
					(tPS8)OEDT_USB_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteFileStepByStep()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_049
* DESCRIPTION:  Write data to a file stepwise
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteFileStepByStep(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFileStepByStep
				(
					(tPS8)OEDT_USB_CFS_COMMONFILE, 
					(tBool)TRUE
				);
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSGetFreeSpace()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_050
* DESCRIPTION:  Get free space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSGetFreeSpace(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSGetFreeSpace(	(tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSGetTotalSpace()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_051
* DESCRIPTION:  Get total space of device
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSGetTotalSpace(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSGetTotalSpace( (tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileOpenCloseNonExstng()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_052
* DESCRIPTION:  Open a non existing file 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileOpenCloseNonExstng(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenCloseNonExstng( (tPS8)OEDT_USB_CFS_DUMMYFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSDelWithoutClose()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_053
* DESCRIPTION:  Delete a file without closing 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileDelWithoutClose(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileDelWithoutClose( (tPS8)OEDT_USB_CFS_TESTFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSSetFilePosDiffOff()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_054
* DESCRIPTION:  Set file position to different offsets
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSSetFilePosDiffOff(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSSetFilePosDiffOff( (tPS8)OEDT_USB_CFS_COMMONFILE,TRUE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSCreateFileMultiTimes()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_055
* DESCRIPTION:  create file multiple times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSCreateFileMultiTimes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSCreateFileMultiTimes( (tPS8)OEDT_USB_CFS_COMMONFILE,TRUE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileCreateUnicodeName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_056
* DESCRIPTION:  create file with unicode characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileCreateUnicodeName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateUnicodeName((tPS8) OEDT_USB_CFS_UNICODEFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSCreateInvalName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_057
* DESCRIPTION:  create file with invalid characters
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileCreateInvalName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateInvalName( (tPS8)OEDT_USB_CFS_INVALCHAR_FILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileCreateLongName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_058
* DESCRIPTION:  create file with long file name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileCreateLongName(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateLongName( (tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileCreateLongName()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_059
* DESCRIPTION:  create file with long file name
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileCreateDiffModes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateDiffModes( (tPS8)OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileCreateInvalPath()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_060
* DESCRIPTION:  Create file with invalid path
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileCreateInvalPath(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileCreateInvalPath( (tPS8)OEDT_USB_CFS_INVALIDFILE );
	return u32Ret;
	
}
/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileStringSearch()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_061
* DESCRIPTION:  Search for string 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileStringSearch(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSStringSearch((tPS8) OEDT_USB_CFS_COMMONFILE, (tBool)TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileOpenDiffModes()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_062
* DESCRIPTION:  File Open different modes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileOpenDiffModes(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileOpenDiffModes	((tPS8) OEDT_USB_CFS_COMMONFILE, (tBool) TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_ommonFSFileReadAccessCheck()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_063
* DESCRIPTION:  Read access check
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileReadAccessCheck(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileReadAccessCheck( (tPS8)OEDT_USB_CFS_COMMONFILE, (tBool) TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileWriteAccessCheck()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_064
* DESCRIPTION:  Write access check
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileWriteAccessCheck(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileWriteAccessCheck( (const tPS8)OEDT_USB_CFS_COMMONFILE, (tBool) TRUE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileReadSubDir()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_065
* DESCRIPTION:  Read from sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileReadSubDir(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileReadSubDir( (const tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileWriteSubDir()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_066
* DESCRIPTION:  Write to sub-directory
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFileWriteSubDir(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileWriteSubDir((const tPS8) OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof1KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_067
* DESCRIPTION:  Read/Write access time for 1kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof1KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1KbFile((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof10KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_068
* DESCRIPTION:  Read/Write access time for 10kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof10KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof10KbFile((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof100KbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_069
* DESCRIPTION:  Read/Write access time for 100kb file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof100KbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof100KbFile ((const tPS8) OEDT_USB_CFS_COMMONFILE	);
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof1MBbFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_070
* DESCRIPTION:  Read/Write access time for 1MB file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof1MBbFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1MBFile((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}


/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof1KbFilefor1000times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_071
* DESCRIPTION:  Read/Write access time for 1kb file for 1000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof1KbFilefor1000times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1KbFilefor1000times((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof10KbFilefor10000times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_072
* DESCRIPTION:  Read/Write access time for 10kb file for 10000 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof10KbFilefor1000times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof10KbFilefor1000times((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof100KbFilefor100times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_073
* DESCRIPTION:  Read/Write access time for 100kb file for 100 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof100KbFilefor100times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof100KbFilefor100times	((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
	
}


/*****************************************************************************
* FUNCTION:		u32USB_CommonFSTimeMeasureof1MBFilefor16times()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_074
* DESCRIPTION:  Read/Write access time for 1MB file for 16 times
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSTimeMeasureof1MBFilefor16times(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSTimeMeasureof1MBFilefor16times
			    ( 
					(const tPS8)OEDTTEST_C_STRING_DEVICE_USB, (const tPS8)OEDT_USB_CFS_COMMONFILE
				);

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSRenameFile()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_075
* DESCRIPTION:  Rename file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSRenameFile(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSRenameFile( (const tPS8)OEDTTEST_C_STRING_DEVICE_USB);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteFrmBOF()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_076
* DESCRIPTION:  Write from BOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteFrmBOF(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFrmBOF ( (const tPS8)OEDT_USB_CFS_COMMONFILE, (tBool)TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteFrmEOF()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_077
* DESCRIPTION:  Write from EOF
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteFrmEOF(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteFrmEOF((const tPS8) OEDT_USB_CFS_COMMONFILE, (tBool) TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSLargeFileRead()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_078
* DESCRIPTION:  Large file read
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSLargeFileRead(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeFileRead ( (const tPS8)OEDT_USB_CFS_COMMONFILE, (tBool) TRUE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSLargeFileWrite()
* PARAMETER:  none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_079
* DESCRIPTION:  Large file write
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSLargeFileWrite(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSLargeFileWrite((const tPS8) OEDT_USB_CFS_COMMONFILE);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSOpenCloseMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_080
* DESCRIPTION:  Open/Close from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSOpenCloseMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSOpenCloseMultiThread((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_081
* DESCRIPTION:  Writ from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteMultiThread((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSWriteReadMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_082
* DESCRIPTION:  Read/Write from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSWriteReadMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSWriteReadMultiThread((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSReadMultiThread()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_083
* DESCRIPTION:  Read from multiple threads
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSReadMultiThread(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSReadMultiThread((const tPS8) OEDT_USB_CFS_COMMONFILE	);
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFormat()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_084
* DESCRIPTION:  Format FS
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSFormat(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFormat((const tPS8) OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32USB_CommonFSCheckDisk()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_085
* DESCRIPTION:  Check Disk
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSCheckDisk(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSCheckDisk((const tPS8) OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSPerformance_Diff_BlockSize()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_086
* DESCRIPTION:  Read Write Performance check for different block sizes
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Dec 01, 2008
******************************************************************************/
tU32 u32USB_CommonFSPerformance_Diff_BlockSize(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSRW_Performance_Diff_BlockSize((const tPS8) OEDT_USB_CFS_COMMONFILE );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileGetExt()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_087
* DESCRIPTION:  Read file/directory contents 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32USB_CommonFSFileGetExt(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt( (const tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32USB_CommonFSFileGetExt2()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_088
* DESCRIPTION:  Read file/directory contents with 2 IOCTL 
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on Jan 13, 2009
******************************************************************************/
tU32 u32USB_CommonFSFileGetExt2(tVoid)
{
	tU32 u32Ret = 0; // u32Ret to identify the error

	u32Ret = u32FSFileGetExt2( (const tPS8)OEDTTEST_C_STRING_DEVICE_USB );
	return u32Ret;
}




/*****************************************************************************
* FUNCTION:		u32USB_GetDeviceInfo()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_091
* DESCRIPTION:  Get the USBH Device info
* HISTORY:		Created by Anoop Chandran(RBIN/ECF1) on May 18, 2009
******************************************************************************/
tU32 u32USB_GetDeviceInfo(tVoid)
{
  	OSAL_tIODescriptor hFile	= 0;
  	OSAL_trUsbDeviceInfo rusbDevInfo = {0};
  	tU32 u32Ret = 0;
  	tS32 s32Result	= 0;

	/* Open device */
  	hFile = OSAL_IOOpen((tCString)OEDTTEST_C_STRING_DEVICE_USB, OSAL_EN_READWRITE);
  	if(hFile == OSAL_ERROR)
  	{
	   	u32Ret = 1;
  	}
  	else
  	{

		/* Get USB Device info such as
		      tU16  u16Media;                 // DATA_MEDIA, INCORRECT_MEDIA, AUDIO_MEDIA, MEDIA_EJECTED
      tU16  u16VendorID;
      tU16  u16ProductID;
      tU16  u16DeviceVersionNo;
      char  achSerialnumberstring[LENGTH_USBINFO_STRING];
      char  achManufacturerstring[LENGTH_USBINFO_STRING];
      char  achProductstring[LENGTH_USBINFO_STRING]; 
      tBool bMounted;                // for u16Media == DATA_MEDIA */
	    s32Result = OSAL_s32IOControl
	    				(
	    					hFile, 
	    					OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
	    					(tS32)&rusbDevInfo
	    				);
	    if( s32Result == OSAL_ERROR )
	    {
	      u32Ret = 2;
	    }
		 else
		 {
			#if DEBUG_MODE
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo(): IOCtrl OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO success \n");
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.u16Media %d \n",rusbDevInfo.u16Media);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.u16VendorID %d \n",rusbDevInfo.u16VendorID);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.u16ProductID %d \n",rusbDevInfo.u16ProductID);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.u16DeviceVersionNo %d \n",rusbDevInfo.u16DeviceVersionNo);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.achSerialnumberstring %s \n",rusbDevInfo.achSerialnumberstring);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.achManufacturerstring %s \n",rusbDevInfo.achManufacturerstring);
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():rusbDevInfo.achProductstring %s \n",rusbDevInfo.achProductstring);		 	
		 	OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32USB_GetDeviceInfo():bMounted.achProductstring %d \n",rusbDevInfo.bMounted);		 			
			#endif
		  
		  }
			/* Close device */
			if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
			{
			u32Ret += 4;
			}

		}
  	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32USB_GetDeviceInfoInval()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_092
* DESCRIPTION:  Get the USBH Device info with inval para
* HISTORY:		Created by Anoop Chandran(RBIN/ECF1) on May 18, 2009
******************************************************************************/
tU32 u32USB_GetDeviceInfoInval(tVoid)
{
  	OSAL_tIODescriptor hFile	= 0;
  	tU32 u32Ret = 0;
  	tS32 s32Result	= 0;

	/* Open device */
  	hFile = OSAL_IOOpen((tCString)OEDTTEST_C_STRING_DEVICE_USB, OSAL_EN_READWRITE);
  	if(hFile == OSAL_ERROR)
  	{
	   	u32Ret = 1;
  	}
  	else
	{

		/* Get USB Device info with OSAL_NULL*/
		s32Result = OSAL_s32IOControl
						(
							hFile, 
							OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
							(tS32)OSAL_NULL
						);
		if( s32Result == OSAL_ERROR )
		{
			u32Ret = 2;
		}
		/* Close device */
		if(OSAL_s32IOClose(hFile) == OSAL_ERROR)
		{
			u32Ret += 4;
		}

	}
  	return u32Ret;
}

/************************************************************************
* FUNCTION		:	vInsertNotiHandlerUSBH ()
* PARAMETER		:	Notification status
* RETURNVALUE	:	"void"
* DESCRIPTION	:	call back function for insert/eject event for USBH
* HISTORY		:	Created by Anoop Chandran (RBEI/ECF1) on May 19, 2009
************************************************************************/
tVoid vInsertNotiHandlerUSBH(const tU32* pu32MediaChangeInfo)
{
	OEDT_HelperPrintf(OSAL_C_TRACELEVEL1,
                  "In InsertNotihandler !!!\n");
	
	if(pu32MediaChangeInfo != NULL)
  	{
    	tU16 u16NotiType = 0;
    	u16NotiType = HIWORD(*pu32MediaChangeInfo);
		if (u16NotiType  == OSAL_C_U16_NOTI_MEDIA_CHANGE)
		{
			if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_DATA_MEDIA )
			{
				if( OSAL_ERROR == OSAL_s32EventPost(   tOEDT_USBEveHandle ,
									   OEDT_USB_ACTIVATE_POST_PATTERN ,
									   OSAL_EN_EVENTMASK_OR ) )
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "DATA_MEDIA:OEDT_USB_ACTIVATE_POST_PATTERN  Failed \n");
				}
				else
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "--DATA_MEDIA:OEDT_USB_ACTIVATE_POST_PATTERN  Posted-- \n");
				}
				
			}
			else if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_UNKNOWN_MEDIA )
			{
				OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Notification is OSAL_C_U16_UNKNOWN_MEDIA \n");
				if( OSAL_ERROR == OSAL_s32EventPost(   tOEDT_USBEveHandle ,
									   OEDT_USB_ACTIVATE_POST_PATTERN ,
									   OSAL_EN_EVENTMASK_OR ) )
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "UNKNOWN_MEDIA:OEDT_USB_ACTIVATE_POST_PATTERN  Failed \n");
				}
				else
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "--UNKNOWN_MEDIA:OEDT_USB_ACTIVATE_POST_PATTERN  Posted-- \n");
				}
				
			}
			else if ((tU16) *pu32MediaChangeInfo == OSAL_C_U16_MEDIA_EJECTED )
			{
				OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "Notification is OSAL_C_U16_MEDIA_EJECTED \n");
				if( OSAL_ERROR == OSAL_s32EventPost(   tOEDT_USBEveHandle ,
									   OEDT_USB_DEACTIVATE_POST_PATTERN ,
									   OSAL_EN_EVENTMASK_OR ) )
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "MEDIA_EJECTED:OEDT_USB_DEACTIVATE_POST_PATTERN  Failed \n");
				}
				else
				{
					OEDT_HelperPrintf(OSAL_C_TRACELEVEL1, "--MEDIA_EJECTED:OEDT_USB_DEACTIVATE_POST_PATTERN  Posted-- \n");
				}
				
			}
			
		}
	}
}  /*  End of vInsertNotiHandlerUSB */

/*****************************************************************************
* FUNCTION:		u32USB_IPOD_Activate_Deactivate_USB_VBUS()
* PARAMETER:  	none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_USB_CFS_093
* DESCRIPTION:  Activate and Deavtivate the USBH
* HISTORY:		Created by Anoop Chandran(RBIN/ECF1) on May 18, 2009
******************************************************************************/
tU32 u32USB_IPOD_Activate_Deactivate_USB_VBUS(tVoid)
{
	OSAL_tIODescriptor hDevice	= 0;
	tU32 u32Ret = 0;
	tS32 s32Result	= 0;
	OSAL_trNotifyData rNotifyData = {0};
	OSAL_tEventMask EM = 0;
	OSAL_trUsbDeviceInfo rusbDevInfo = {0};

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate(OEDT_USB_EVENT_NAME,&tOEDT_USBEveHandle ) )
	{

		/* Open USB device */
		hDevice = OSAL_IOOpen((tCString)OEDTTEST_C_STRING_DEVICE_USB, OSAL_EN_READWRITE);
		if(hDevice == OSAL_ERROR)
		{
			u32Ret = 2;
		}
		else
		{


			/*-----Register the Notification Callback---------*/
			rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
			rNotifyData.ResourceName = OEDTTEST_C_STRING_DEVICE_USB; 
			rNotifyData.u16NotificationType = (OSAL_C_U16_NOTI_MEDIA_CHANGE );
			rNotifyData.pCallback = (OSALCALLBACKFUNC)vInsertNotiHandlerUSBH;
			if(
					OSAL_s32IOControl 
					(	
						hDevice, 
						OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,
						(tS32)&rNotifyData
					) 
					== 
					OSAL_ERROR
			  )
			{
				u32Ret = 4;
			}
			/*--------------------------------------------------*/

			/* ----------Deactivate the USB ---------------*/

			/*Deactivate the USBH with OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS*/
			s32Result = OSAL_s32IOControl
							(
								hDevice, 
								OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS, 
								(tS32)OSAL_NULL
							);
			if( s32Result == OSAL_ERROR )
			{
				u32Ret += 8;
			}
			else
			{

				/*Wait for Deactivate Event pattern*/ 
				if( 
						OSAL_ERROR 
						== 
						OSAL_s32EventWait
						( 
							tOEDT_USBEveHandle,
							OEDT_USB_DEACTIVATE_POST_PATTERN ,
							OSAL_EN_EVENTMASK_AND,
							OSAL_C_TIMEOUT_FOREVER,
							&EM 
						) 
				  )
				{
					u32Ret += 15;
				}
				else
				{
					/*Clear the Event pattern*/
					if( 
							OSAL_ERROR 
							== 
							OSAL_s32EventPost
							(   
								tOEDT_USBEveHandle ,
								0,
								OSAL_EN_EVENTMASK_AND 
							) 
						)
					{
						u32Ret += 30;
					}

					s32Result = 0;
					/*Try to get the USB Device Info, in this case OSAL_s32IOControl
					should return error  */ 
					s32Result = OSAL_s32IOControl
									(
										hDevice, 
										OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
										(tS32)&rusbDevInfo
									);
					if( s32Result != OSAL_ERROR )
					{
						u32Ret += 40;
					}
				
					/*---------------------------------------------------------------*/


					/* ----------Activate the USB ---------------*/
					s32Result = 0;

					/*Activate the USBH with OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS*/

					s32Result = OSAL_s32IOControl
									(
										hDevice, 
										OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS, 
										(tS32)OSAL_NULL
									);
					if( s32Result == OSAL_ERROR )
					{
						u32Ret += 107;
					}
					else
					{
						/*Wait for Activate Event pattern*/ 
						if( 
								OSAL_ERROR 
								== 
								OSAL_s32EventWait
								( 
									tOEDT_USBEveHandle,
									OEDT_USB_ACTIVATE_POST_PATTERN ,
									OSAL_EN_EVENTMASK_AND,
									OSAL_C_TIMEOUT_FOREVER,
									&EM 
								) 
							)
						{
							u32Ret += 210;
						}
						else
						{
							/*Clear the posted even pattern*/
							if( 
									OSAL_ERROR 
									== 
									OSAL_s32EventPost
									(   
										tOEDT_USBEveHandle ,
										0,
										OSAL_EN_EVENTMASK_AND 
									) 
								)
							{
								u32Ret += 330;
							}
							/*Delay reqiured for USB Communication*/
							OSAL_s32ThreadWait(2000);
							s32Result = 0;

							/*Try to get the USB Device Info*/ 
							s32Result = OSAL_s32IOControl
											(
												hDevice, 
												OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
												(tS32)&rusbDevInfo
											);
							if( s32Result == OSAL_ERROR )
							{
								u32Ret += 440;
							}
							else
							{
								
								#if DEBUG_MODE
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16Media %d \n",rusbDevInfo.u16Media);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16VendorID %d \n",rusbDevInfo.u16VendorID);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16ProductID %d \n",rusbDevInfo.u16ProductID);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16DeviceVersionNo %d \n",rusbDevInfo.u16DeviceVersionNo);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achSerialnumberstring %s \n",rusbDevInfo.achSerialnumberstring);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achManufacturerstring %s \n",rusbDevInfo.achManufacturerstring);
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achProductstring %s \n",rusbDevInfo.achProductstring);		 	
								OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:bMounted.achProductstring %d \n",rusbDevInfo.bMounted);		 			
								#endif

							}
							/*---------------------------------------------------------------*/
							
							
							/* ----------Deactivate the USB ---------------*/
							s32Result = OSAL_s32IOControl
											(
											hDevice, 
											OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS, 
											(tS32)OSAL_NULL
											);
							if( s32Result == OSAL_ERROR )
							{
								u32Ret += 1008;
							}
							else
							{
								/*Wait for Deactivate Event pattern*/ 

								if( 
										OSAL_ERROR 
										== 
										OSAL_s32EventWait
										( 
											tOEDT_USBEveHandle,
											OEDT_USB_DEACTIVATE_POST_PATTERN ,
											OSAL_EN_EVENTMASK_AND,
											OSAL_C_TIMEOUT_FOREVER,
											&EM 
										) 
									)
								{
									u32Ret += 2000;
								}
								else
								{
									/*Clear the posted even pattern*/

									if( 
											OSAL_ERROR 
											== 
											OSAL_s32EventPost
											(   
												tOEDT_USBEveHandle ,
												0,
												OSAL_EN_EVENTMASK_AND 
											) 
										)
									{
										u32Ret += 3000;
									}
									else
									{
										/*Try to get the USB Device Info, in this case OSAL_s32IOControl
										should return error  */ 
										s32Result = OSAL_s32IOControl
														(
															hDevice, 
															OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
															(tS32)&rusbDevInfo
														);
										if( s32Result != OSAL_ERROR )
										{
											u32Ret += 4000;
										}
										/*-------------------------------------------------------------*/
									}
								}
							}
						}
					}		
				}
			}
		
			/* ----------Make the USB Active---------------*/
			s32Result = 0;

			/*Activate the USBH with OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS*/

			s32Result = OSAL_s32IOControl
							(
								hDevice, 
								OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS, 
								(tS32)OSAL_NULL
							);
			if( s32Result == OSAL_ERROR )
			{
				u32Ret += 10007;
			}
			else
			{
				/*Wait for Activate Event pattern*/ 
				if( 
						OSAL_ERROR 
						== 
						OSAL_s32EventWait
						( 
							tOEDT_USBEveHandle,
							OEDT_USB_ACTIVATE_POST_PATTERN ,
							OSAL_EN_EVENTMASK_AND,
							OSAL_C_TIMEOUT_FOREVER,
							&EM 
						) 
					)
				{
					u32Ret += 20000;
				}
				else
				{
					/*Clear the posted even pattern*/
					if( 
							OSAL_ERROR 
							== 
							OSAL_s32EventPost
							(   
								tOEDT_USBEveHandle ,
								0,
								OSAL_EN_EVENTMASK_AND 
							) 
						)
					{
						u32Ret += 30000;
					}
					/*Delay reqiured for USB Communication*/
					OSAL_s32ThreadWait(2000);
					s32Result = 0;
					s32Result = OSAL_s32IOControl
									(
										hDevice, 
										OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO, 
										(tS32)&rusbDevInfo
									);
					if( s32Result == OSAL_ERROR )
					{
						u32Ret += 40000;
					}
					else
					{
						#if DEBUG_MODE
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO: success \n");
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16Media %d \n",rusbDevInfo.u16Media);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16VendorID %d \n",rusbDevInfo.u16VendorID);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16ProductID %d \n",rusbDevInfo.u16ProductID);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.u16DeviceVersionNo %d \n",rusbDevInfo.u16DeviceVersionNo);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achSerialnumberstring %s \n",rusbDevInfo.achSerialnumberstring);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achManufacturerstring %s \n",rusbDevInfo.achManufacturerstring);
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:rusbDevInfo.achProductstring %s \n",rusbDevInfo.achProductstring);		 	
						OEDT_HelperPrintf(TR_LEVEL_USER_1,"USBH_GETDEVICEINFO:bMounted.achProductstring %d \n",rusbDevInfo.bMounted);		 			
						#endif

					}
				}
			}
				/*---------------------------------------------------------------*/
			/* Close device */
			if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
			{
				u32Ret += 50000;
			}
		}
		
		/*Shutdown the event - close and delete*/
		if( OSAL_ERROR == OSAL_s32EventClose( tOEDT_USBEveHandle ) )
		{
			/*Event close failed*/
			u32Ret += 80000;
		}
		else
		{

			if( OSAL_ERROR == OSAL_s32EventDelete( OEDT_USB_EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 100000;
			}
		}
	}
	else
	{
		u32Ret = 1;
	}
	return u32Ret;
}


/* EOF */																	


