/************************************************************************
 * @file: oedt_virtio_testfuncs.c
 *
 * oedt component test for virtio driver.
 * This test writes data to the VIRTIO-LOOP device and reads data back.
 * 
 * @component: VIRTIO
 *
 * @author: Andreas Pape
 *
 * @copyright: (c) 2003 - 2010 ADIT Corporation
 *
 * @see <related items>
 * 
 *
 * ASSUMPTION(s): virtio loop device loaded and started.
 ***********************************************************************/

#include <strings.h>
#include <stdlib.h>

#define VIOTEST_OEDT /*switch code to oedt usage */

#ifdef VIOTEST_OEDT
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_virtio_testfuncs.h"
#define VIO_USE_SE 
#endif

#ifdef VIO_USE_SE
#include <extension/extension.h>
#define VIOTST_CRE_TSK(ctsk) tkse_cre_tsk((ctsk)->task, (ctsk)->itskpri)
#define VIOTST_STA_TSK(id, parm) tkse_sta_tsk(id, (W)parm)
#define VIOTST_EXT_TSK tkse_ext_tsk /* this is exit and delete!! */
#define VIOTST_DEL_TSK(id)          /* no delete supported */

#define VIOTST_CRE_FLG tkse_cre_flg
#define VIOTST_DEL_FLG tkse_del_flg
#define VIOTST_SET_FLG tkse_set_flg
#define VIOTST_WAI_FLG tkse_wai_flg

#define VIOTST_OPN_DEV tkse_opn_dev
#define VIOTST_CLS_DEV tkse_cls_dev
#define VIOTST_SWRI_DEV tkse_swri_dev
#define VIOTST_SREA_DEV tkse_srea_dev
#else
#include <tk/tkernel.h>
#define VIOTST_CRE_TSK(ctsk) tk_cre_tsk(ctsk)
#define VIOTST_STA_TSK(id,parm) tk_sta_tsk(id, (INT)parm)
#define VIOTST_EXT_TSK tk_ext_tsk
#define VIOTST_DEL_TSK tk_del_tsk

#define VIOTST_CRE_FLG tk_cre_flg
#define VIOTST_DEL_FLG tk_del_flg
#define VIOTST_SET_FLG tk_set_flg
#define VIOTST_WAI_FLG tk_wai_flg

#define VIOTST_OPN_DEV tk_opn_dev
#define VIOTST_CLS_DEV tk_cls_dev
#define VIOTST_SWRI_DEV tk_swri_dev
#define VIOTST_SREA_DEV tk_srea_dev
#endif



#define VIOTST_TASK_PRIO     100
#define VIOTST_WAIT_TMO      TMO_FEVR
#define VIOTST_ITERATIONS    100
#define VIOTST_PATTERN       0xa5

LOCAL B* g_tsk_name = "tsk_x";
LOCAL ID waitflg = -1;
LOCAL UINT waitptn = 0;
LOCAL UW g_loopmax = VIOTST_ITERATIONS;
LOCAL UB g_test_data_idx = 0;

typedef enum
{
  VIOTST_ERR_OK           = 0,
  VIOTST_ERR_FLG          = 1,
  VIOTST_ERR_TSK_LIMIT    = 2,
  VIOTST_ERR_CRE_TSK      = 3,
  VIOTST_ERR_DEL_TSK      = 4,
  VIOTST_ERR_WAI_TSK      = 5,
  VIOTST_ERR_OPN_DEV      = 6,
  VIOTST_ERR_RD           = 7,
  VIOTST_ERR_WR           = 8,
  VIOTST_ERR_MAX_DATA_SET = 9,
  VIOTST_ERR_UNKNOWN      = 255,
}vio_err_t;


/* Parameters monitored during test */
typedef struct
{
  UW  wait_fail;
  UW  rd_data_corrupted;
  UW  rd_sz_match_fail;
  UW  total_iterations;
  UINT data_pattern_index;
  UW  transfer_sum;
}test_mgr;


/* Parameters passed to test task */
typedef struct
{
  UB  devname[L_DEVNM];
  UW loop_count;
  ER  rc;
  vio_err_t vioerr;
  UW **rwbuf;
  UW *data_sz;
  UW data_cnt;
  ID flgid;
  ID tskid;
  UINT flgmask;
  test_mgr result;
}task_param;


#define VIOTST_MAX_TASKS 32
task_param * gparms[VIOTST_MAX_TASKS];


/* Dynamic data size combinations */
struct test_data_set
{
  UW cnt;
  UW *size;
};

LOCAL UW g_testset_1[] = { 1612, 11134, 6, 24590, 204812 };
LOCAL UW g_testset_2[] = { 1024*1024 };                          
LOCAL UW g_testset_3[] = { 1,2,3,4,5,6 };  

#define MAX_DATA_SET 3
LOCAL struct test_data_set g_test_data[MAX_DATA_SET]=
{
    {sizeof(g_testset_1)/sizeof(UW), g_testset_1 },
    {sizeof(g_testset_2)/sizeof(UW), g_testset_2 },
    {sizeof(g_testset_3)/sizeof(UW), g_testset_3 },
};


/***********************************************************************
 * \func VIOTST_buf_prolog
 *
 * Create buffer(s)
 *
 * \param  buf          : holds pointer to created buffer
 *         test_data_sz : holds pointer to various data size
 *                        to be used during TX & RX
 *
 * \return E_OK         : E_OK if normal end
 *         E_NOMEM      : E_NOMEM if no memory
 *
 */
LOCAL ER VIOTST_buf_prolog(task_param* parm , UINT test_data_sz[], UB test_data_cnt)
{
  ER     rc  = E_OK;
  UINT   i   = 0;
  size_t sz  = 0;
  UB     dat = VIOTST_PATTERN;
  parm->rwbuf = malloc(test_data_cnt * sizeof(UW*));
  if(parm->rwbuf == NULL)
    return E_NOMEM;
  parm->data_sz = test_data_sz;
  parm->data_cnt = test_data_cnt;

  for(; (i<parm->data_cnt) && (E_OK <= rc); i++)
  {
    sz = test_data_sz[i];
    parm->rwbuf[i] = malloc(sz);
    if(NULL == parm->rwbuf[i])
    {
      rc = E_NOMEM;
    }
    else
    {
      memset(parm->rwbuf[i], dat, sz);
    }
  }        
  return rc;
}

/***********************************************************************
 * \func VIOTST_buf_epilog
 *
 * Release buffer(s)
 *
 * \param  buf  : holds pointer to created buffer
 *
 * \return NONE
 *         
 */
LOCAL void VIOTST_buf_epilog(task_param* parm )
{
  UINT i = 0;
  for(; i < parm->data_cnt; i++)
  {
    if(NULL != parm->rwbuf[i])
    {
      free(parm->rwbuf[i]);
      parm->rwbuf[i] = NULL;
    }
  }
  free(parm->rwbuf);
  parm->rwbuf = NULL;
}

/***********************************************************************
 * \func VIOTST_exit_task
 *
 * - Signals test creator task about test completion.
 * - To be called from test task during error occurence or at the end of 
 *   test completion.
 *
 * \param  parm  : Structure holding runtime info exchanged between
 *                 test creator and test task.
 *         rc    : E_OK if test completed successfully, else
 *                 T-Kernel error code if test encountered error
 *
 * \return NONE
 *
 */
LOCAL void VIOTST_exit_task(task_param* parm, ER rc, vio_err_t vioerr)
{

  if(rc!=E_OK)
  {
    if(vioerr==VIOTST_ERR_OK)
      vioerr = VIOTST_ERR_UNKNOWN;
  }

  parm->rc = rc;
  parm->vioerr = vioerr;
  VIOTST_SET_FLG(parm->flgid, parm->flgmask);
  VIOTST_EXT_TSK();
}


/***********************************************************************
 * \func VIOTST_tx_task
 *
 * \param  stacd : holds context pointer
 *
 * \return NONE
 *         
 */
#ifdef VIO_USE_SE
LOCAL void VIOTST_tx_task(W stacd)
#else
LOCAL void VIOTST_tx_task(INT stacd, VP exinf)
#endif
{
  ER                 rc           = E_OK;
  task_param*        parm         = (task_param*) stacd;  
  UW**               wbuf         = parm->rwbuf;
  ID                 dd           = 0;  
  test_mgr           *result      = &parm->result; 
  INT                size         = 0;
  UINT               index        = 0;  
  UW                 loop         = parm->loop_count;  
  UINT               *data_sz     = parm->data_sz;
 vio_err_t vioerr= VIOTST_ERR_OK;
  /* Open device and get device handle */
  dd = VIOTST_OPN_DEV(parm->devname, TD_UPDATE);
  if(E_OK > dd)
  {
    VIOTST_exit_task(parm, rc, VIOTST_ERR_OPN_DEV);
  /*no return from call*/
  }		            
  
  do
  {
    index = result->data_pattern_index;     
    /* TX request */
    rc = VIOTST_SWRI_DEV(dd, 0, (char*) wbuf[index], data_sz[index], &size);   
    if(rc==E_OK)
      result->transfer_sum+= size;
    result->data_pattern_index++;          
    if(result->data_pattern_index >= parm->data_cnt) 
    {
      result->data_pattern_index = 0;
    }
       
    /* Total test iterations performed */
    result->total_iterations++;      
    loop--;
  }while((loop > 0) && (E_OK <= rc));
 
  VIOTST_CLS_DEV(dd, 0);
  if(rc!= E_OK)
    vioerr = VIOTST_ERR_WR;

  VIOTST_exit_task(parm, rc, vioerr);
  /*no return from call*/
}


/***********************************************************************
 * \func VIOTST_rx_task
 *
 * Receiver task. 
 *
 * \param  stacd    : context pointer
 *
 * \return NONE
 *
 */
#ifdef VIO_USE_SE
LOCAL void VIOTST_rx_task( W stacd)
#else
LOCAL void VIOTST_rx_task(INT stacd, VP exinf)
#endif
{
  task_param*          parm         = (task_param*) stacd;
  ER                 rc           = E_OK;
  ID                 dd           = 0;
  INT                size         = 0;
  test_mgr          * result      = &parm->result;
  UW**               rbuf         = parm->rwbuf;
  UW                 loop         = parm->loop_count; 
  UINT               i            = 0;  
  UINT               *data_sz     = parm->data_sz;
  vio_err_t vioerr= VIOTST_ERR_OK;
  /* Open device and get device handle */
  dd = VIOTST_OPN_DEV(parm->devname, TD_UPDATE);
  if(E_OK > dd)
  {
    VIOTST_exit_task(parm, rc, VIOTST_ERR_OPN_DEV);
    /*no return from call*/
  }		      
 
  do
  {
    i = result->data_pattern_index;
    /* Fill rd buffer with 0 */
    rc = VIOTST_SREA_DEV(dd, 0, rbuf[i], data_sz[i], &size);      
  
   if(rc==E_OK)
      result->transfer_sum+= size;

    /* Check: (TX data size == RX data size) */
    if(size != data_sz[i]) {
      result->rd_sz_match_fail++;
    } 
    else 
    {
#if 0
	/*as virtio loop device does NOT implement virtual channels/protocol layer,
	the read data has random character, so no check possible. 
	This test is intended to simulate high load on VIRTIO regarding shared memory access and interrupts.*/
      /* Check: (TX data == RX data) */
      rc = memcmp(rbuf[i], wbuf[i], data_sz[i]);
      if(0 != rc)
      {
        result.rd_data_corrupted++;
      }
#endif
    }
        
    result->data_pattern_index++;          
    if(result->data_pattern_index >= parm->data_cnt) 
    {
      result->data_pattern_index = FALSE;        
    }      
    /* Total test iterations performed */
    result->total_iterations++;      
              
    loop--;
  }while((loop > 0) && (E_OK <= rc));
    
  VIOTST_CLS_DEV(dd, 0);

  if(rc!= E_OK)
    vioerr = VIOTST_ERR_RD;
  VIOTST_exit_task(parm, rc, vioerr);
  /*no return from call*/
}

/*
* prolog for task params
*/
LOCAL task_param *setup_param(UINT tsk_cnt)
{
  char* devname = "violpa";
  ER rc= E_OK;
  task_param *parm = malloc(sizeof(task_param));
  if(parm == NULL)
  {
    return NULL;
  }
  memset(parm, 0, sizeof(task_param));
  strcpy((char*) parm->devname, (char*) devname);

  parm->loop_count = g_loopmax;
  rc = VIOTST_buf_prolog(parm, g_test_data[g_test_data_idx].size, g_test_data[g_test_data_idx].cnt);
  if(rc<E_OK)
  {
    VIOTST_buf_epilog(parm);
    free(parm);
    return NULL;
  }

  parm->flgmask = 1<<tsk_cnt;
  parm->flgid = waitflg;
  waitptn |= parm->flgmask;
  return parm;
}

/*
* epilog for task params
*/
LOCAL void exit_param(task_param *parm)
{
  VIOTST_buf_epilog(parm);
  free(parm);
}


/*
* delete flag
*/
LOCAL ER del_flg(void)
{
  if(waitflg>=E_OK)
  {
    VIOTST_DEL_FLG(waitflg);
  }
  waitflg=-1;
  return E_OK;
}

/*
* create flag for synchronisation
*/
LOCAL ER cre_flg(void)
{
  ER err; 
  T_CFLG     flg      = {0};
  flg.flgatr  = TA_TFIFO | TA_WMUL;
  err = VIOTST_CRE_FLG(&flg);
  if(err<E_OK)
  {
    return err;
  }
  else
  {
    waitflg = err;
    return E_OK;
  }
}

/***********************************************************************
 * \func VIOTST_create_tk_task
 *
 * Creates T-Kernel task with user supplied attributes.
 *
 * \param  tsk_idx    : Task index
 * 	     task_entry : Task entry point
 *         parm       : context pointer passed to task.
 *
 * \return rc         : E_OK if everything went fine, else
 *                      T-Kernel specific error code
 *         
 */
LOCAL ER VIOTST_create_task(UINT tsk_idx, FP task_entry, task_param *parm)
{
  T_CTSK ctsk = {0};
  ER     rc   = E_OK;
  ID     id   = 0;
  PRI    prio = VIOTST_TASK_PRIO;

  if(parm== NULL)
  {
    return E_NOMEM;
  }

  ctsk.tskatr  = TA_HLNG | TA_FPU | TA_RNG0 | TA_DSNAME | TA_LCID;
  ctsk.lcid    = TLCID_SMP;
  strcpy((char*) ctsk.dsname, (char*) g_tsk_name);
  ctsk.dsname[4] = '1' + tsk_idx;
  ctsk.itskpri = prio;
  ctsk.stksz   = 4096;            
  ctsk.task    = task_entry;
  id           = VIOTST_CRE_TSK(&ctsk);
  if(id >= E_OK)
  {
    parm->tskid = id;
    rc = VIOTST_STA_TSK(id, parm);
  }
  return rc;
}


/*
* create test tasks
*/
LOCAL ER create_tasks(UINT read, UINT write) 
{
  UINT task_cnt=0;
  ER rc = E_OK;

  if(read+write > VIOTST_MAX_TASKS)
  {
    return VIOTST_ERR_TSK_LIMIT;
  }
  
  while((rc == E_OK)&& (task_cnt<(read+write)))
  {
    gparms[task_cnt] = setup_param(task_cnt);
    if(gparms[task_cnt] == 0)
      rc = E_NOMEM;
    task_cnt++;
  }

  task_cnt=0;
  while((rc == E_OK)&& read)
  {
    rc = VIOTST_create_task(task_cnt, VIOTST_rx_task, gparms[task_cnt]);
    task_cnt++;
    read--;
  }

  while((rc==E_OK) && write)
  {
    rc = VIOTST_create_task(task_cnt, VIOTST_tx_task, gparms[task_cnt]);
    task_cnt++;
    write--;
  }
  return rc;

}

/*
* scan all tasks for error code
*/
LOCAL vio_err_t scan_result(void) 
{
  UINT i;
  for(i=0;i<VIOTST_MAX_TASKS;i++)
  {
    if(gparms[i]==0)
      break;
    if(gparms[i]->tskid)
    {
      if(gparms[i]->vioerr != VIOTST_ERR_OK)
      {
          return gparms[i]->vioerr;
      }
    }
  }
  return VIOTST_ERR_OK;
}


/*
* delete all tasks
*/
LOCAL ER delete_tasks(void) 
{
  UINT i;
  for(i=0;i<VIOTST_MAX_TASKS;i++)
  {
    if(gparms[i]==0) /*no spaces in list*/
      break;
    if(gparms[i]->tskid)
    {
      VIOTST_DEL_TSK(gparms[i]->tskid);
      exit_param(gparms[i]);
      gparms[i]=NULL;
    }
  }
  return E_OK;
}


/*
* component test: entry 
* @param task_cnt		amount of read/write tasks (always same for R/W)
*	   loops		number of repetitions
*        dataset_select	select data set to use
*/
EXPORT vio_err_t VIOTST_comp_test(UB task_cnt, UW loops, UB dataset_select)
{
  ER rc = E_OK;
  vio_err_t result = VIOTST_ERR_OK;
  waitptn = 0;
  waitflg = -1;
  memset(gparms, 0, sizeof(gparms));
  UINT flg_ptn;
 
  if(loops>0)
  {
    g_loopmax = loops;
  }
  else
  {
    g_loopmax = VIOTST_ITERATIONS;
  }

  if(dataset_select >= MAX_DATA_SET)
    return VIOTST_ERR_MAX_DATA_SET;
  g_test_data_idx = dataset_select;


  if(cre_flg()< E_OK)
  {
     return VIOTST_ERR_FLG;
  }

  if(create_tasks(task_cnt, task_cnt)<E_OK)
  {
     return VIOTST_ERR_CRE_TSK;
  }


  /* wait for tasks finished */
  rc = VIOTST_WAI_FLG(waitflg, waitptn, (TWF_ANDW | TWF_BITCLR), &flg_ptn, VIOTST_WAIT_TMO);
  if(rc < E_OK)
  {
    result = VIOTST_ERR_WAI_TSK;
  }

  if(result == VIOTST_ERR_OK)
  {
    result = scan_result();
  }

  rc = delete_tasks();
  if(rc<E_OK)
  {
    if(result == VIOTST_ERR_OK)
    {
      result = VIOTST_ERR_DEL_TSK;
    }
  }

  del_flg();  

  return result;
}




#ifdef VIOTEST_OEDT
/* test 1: one task with different data sizes */
EXPORT tU32 u32virtio_test1(void)
{
  return VIOTST_comp_test(1, 100, 0);
}
/* test 2: multiple tasks with different data sizes */
EXPORT tU32 u32virtio_test2(void)
{
  return VIOTST_comp_test(10, 10, 0);
}
/* test 3: one task with big data size */
EXPORT tU32 u32virtio_test3(void)
{
  return VIOTST_comp_test(1, 20, 1);
}
/* test 4: multiple tasks with big data size */
EXPORT tU32 u32virtio_test4(void)
{
  return VIOTST_comp_test(10, 2, 1);
}
/* test 5: one task with small data sizes */
EXPORT tU32 u32virtio_test5(void)
{
  return VIOTST_comp_test(1, 1000, 2);
}
/* test 6: multiple tasks with small data sizes */
EXPORT tU32 u32virtio_test6(void)
{
  return VIOTST_comp_test(10, 100, 2);
}
#endif
