/******************************************************************************
 *FILE         : oedt_usbdnl_TestFuncs.c

 *
 *SW-COMPONENT : OEDT_FrmWrk(osal_dev_test) 
 *
 *DESCRIPTION  : This file implements the individual test cases for usbdnl device 
 *               Corresponding Test spec: TU_OEDT_USBDNL_TestSpec.xls (draft v0.1)
				 Ported from Paramount Platform.
 *
 *AUTHOR(s)    :  Pankaj Kumar(RBIN/EDI3)

 *COPYRIGHT    : (c) 2007 Robert Bosch India Limited (RBIN)

 *HISTORY      :  10-Dec-2007 -Initial version-Pankaj Kuamr (RBIN/EDI3)
						27-05-2008 update by-Anoop Chandran (RBEI/ECM1)
						Added new test csae
						TU_OEDT_USBDNL_020-027
				  13-10-2008 - Added new test case TU_OEDT_USBDNL_028 -hbs2kor 
				  1.2 version ,31-01-2009 - Added new test case TU_OEDT_USBDNL_029 
				  Anoop Chandran(RBEI/ECF1)
				  1.3 version ,13-04-2009 - Removed Lint and Compiler warnings 
				  Jeryn Mathew(RBEI/ECF1)				  
*******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"

//#define OSAL_C_TRACELEVEL1 			TR_LEVEL_ERRORS
#define OEDT_USBDNL_FILE1   			"/dev/usb_dnl/a.bin"
#define OEDT_USBDNL_FILE2   			"/dev/usb_dnl/b.bin"
#define OEDT_USBDNL_FILE3	  			"/dev/usb_dnl/c.bin"
#define OEDT_USBDNL_FILE4   			"/dev/usb_dnl/d.bin"
#define OEDT_USBDNL_FILE5	  			"/dev/usb_dnl/e.bin"
#define OEDT_USBDNL_FILE6   			"/dev/usb_dnl/f.bin"
#define OEDT_USBDNL_FILE                "/dev/usb_dnl/dragon.bin"
#define NUMBER_OF_BYTES      			32
#define OEDT_BREAT_TIME 				3000
#define USBDNL_BUFFER_SIZE_ONE_MB 	(1024*1024)

struct boardID
{
   tU32 processID;
   tU32 boardID;
   tU8 processName[21];
   tU8 boardName[21];
};
struct usbRaw
{
   tU32 size;
   tU16 wEP0Expected; 
   tU8 data[63];
};

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_DeviceOpenClose    
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_001
* DESCRIPTION:  Open  and Close  of the device.
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 u32usbdnlOEDT_DeviceOpenClose(void)
{
 	OSAL_tIODescriptor hUSBDevice   = 0;
   	tU32 u32Ret = 0;
   	tS32 s32Status = 0;
	
   	/*Open the device in Readonly mode */
   	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
   	if ( OSAL_ERROR == hUSBDevice )
    {
		/* Error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
   			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
    else
	{
	/* Wait is use to avoid the device close hanging.*/
	  OSAL_s32ThreadWait(1000);
		/* Close the device */
     	if(OSAL_ERROR  == OSAL_s32IOClose ( hUSBDevice ) )
		{
			u32Ret += 6;
		}
	}
	return u32Ret;
}
 
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_DeviceMultipleOpen    
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_002
* DESCRIPTION:  Open the device multiple times
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/

tU32 U32usbdnlOEDT_DeviceMultipleOpen(void)
{
 	OSAL_tIODescriptor hUSBDevice1 = 0 ,hUSBDevice2   = 0;
   	tU32 u32Ret = 0;
   	tS32 s32Status = 0;

    /*Open the device in Readonly mode*/
    hUSBDevice1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
    if ( OSAL_ERROR == hUSBDevice1)
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
    else
   	{
		/*Reopen the device without closing  the device  in Readonly mode */
      	hUSBDevice2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
        if ( OSAL_ERROR != hUSBDevice2 )
	    {
			/* check the error status returned */
			s32Status = (tS32) OSAL_u32ErrorCode();
			switch(s32Status)
			{
				case OSAL_E_ALREADYEXISTS:
					u32Ret += 10;
					break;
				case OSAL_E_UNKNOWN:
					u32Ret += 20;
					break;
				default:
					u32Ret += 30;
			}
		}
		else
      	{
			/* Wait is use to avoid the device close hanging.*/
	   		OSAL_s32ThreadWait(100);
			/*Close the second handle of the device */
        	if (OSAL_ERROR != OSAL_s32IOClose(hUSBDevice2))
			{	
				u32Ret += 40;
      		}
			else
			{
			    /*Close  the first handle of the device */
				if  (OSAL_ERROR == OSAL_s32IOClose(hUSBDevice1))
				{
					u32Ret += 50;
				}
			}
      	}
	  }

   return u32Ret;
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_DeviceMultipleClose   
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_003
* DESCRIPTION:  Close the device multiple times
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_DeviceMultipleClose(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;
   tU32 u32Ret = 0;
   tS32 s32Status = 0;

   /*Open the device in Readonly mode */
   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
   if ( OSAL_ERROR == hUSBDevice)
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
    }
   	else
	{
	 	/* Wait is use to avoid the device close hanging.*/
	 	OSAL_s32ThreadWait(100);
		/* Close the device */
        if( OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
      	{
			u32Ret += 10;
		}
		else
		{
	        /*Trying to close the device once more*/
        	if (OSAL_s32IOClose(hUSBDevice)!= OSAL_ERROR)
			{
         		u32Ret += 20;
      		}
		}
	}
  return  u32Ret;
 }
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_DeviceMultipleOpenCloseLoop
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_004
* DESCRIPTION:  Open  and Close the device multiple times
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_DeviceOpenCloseLoop(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;
   tU32 u32Ret = 0;
   tS32 s32Status = 0,count = 0;
   
   // Check open close 10 times
   for(count = 0 ; (count < 10) && (u32Ret == 0); count++)
   {
      OSAL_s32ThreadWait(100);
	  /* Open the device in read omly mode */
      hUSBDevice =  OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
      if ( OSAL_ERROR == hUSBDevice)
	  {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
     {
	     	/* Wait is use to avoid the device close hanging.*/
	     OSAL_s32ThreadWait(100);
		 /* Close  the device */
         if( OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
         {
            u32Ret += 20;
		  }
		  else
		  {
           	 //Again trying to close the device*/
           	if (OSAL_s32IOClose( hUSBDevice)!= OSAL_ERROR)
			{
				u32Ret += 30;
			}
         }
      } 
   }
   return u32Ret;
}
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_FileOpen
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_005
* DESCRIPTION:  Open the file 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/

tU32 U32usbdnlOEDT_FileOpen(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0;   // Handle for File
   tU32 u32Ret = 0;
   tS32 s32Status = 0;

   /*Open the device in Readonly mode */
   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
   if ( OSAL_ERROR == hUSBDevice)
	  {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
   	    fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
      	if(fdSourceFile == OSAL_ERROR)
      	{
			u32Ret = 20;
		}
		else
		{
        	if ( OSAL_s32IOClose(fdSourceFile)== OSAL_ERROR)
		 	{
		 		u32Ret = 30;
			}
			else
			{
				if( OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
				{
					u32Ret = 40;
				}
			}
		}
	}
   return u32Ret; 
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_FileMultipleOpen
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_006
* DESCRIPTION:  Open the file multiple times 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_FileMultipleOpen(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0 ;                   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile1 = 0 ,fdSourceFile2 = 0 ;   // Handle for File
   tU32 u32Ret = 0;
   tS32 s32Status = 0;
   
   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
   if ( OSAL_ERROR == hUSBDevice)
	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
   }
	else
 	{
		/* The file exists in the device */
		fdSourceFile1 = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
   	if( fdSourceFile1 == OSAL_ERROR)
  		{
         u32Ret = 20;
   	}
  		else
		{
			fdSourceFile2  = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
			if( fdSourceFile2 != OSAL_ERROR ) 
			{
				u32Ret = 30;
				if (OSAL_s32IOClose(fdSourceFile2)!= OSAL_ERROR)
				{
					u32Ret = 40 ;
				} 
			}
			if (OSAL_s32IOClose(fdSourceFile1)== OSAL_ERROR)
			{
				u32Ret = 50;
			}
			else
			{
				if (OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
				{
					u32Ret = 60;
				}
			}
		}
	}
	return u32Ret;
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_FileClose
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_007
* DESCRIPTION:  Close the file
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/

tU32 U32usbdnlOEDT_FileClose(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0;   // Handle for File
   tU32 u32Ret = 0;
   tS32 s32Status = 0;
   
   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);

   if( OSAL_ERROR == hUSBDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
	 	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
      	if(fdSourceFile == OSAL_ERROR)
     	{
			u32Ret = 10;
		}
		else
		{
        	if( OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR )
         	{
            	u32Ret = 20;
         	}
         	else
         	{	
             	if( OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
					{
						u32Ret = 30;
					}
				}
			}
     	}
   return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_FileMultipleClose
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_008
* DESCRIPTION:  Close the file  multiple times
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/

tU32 U32usbdnlOEDT_FileMultipleClose(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0;   // Handle for File
   tU32 u32Ret = 0;
   tS32  s32Status = 0;

   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
   if( OSAL_ERROR == hUSBDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
	 	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
      	if(fdSourceFile == OSAL_ERROR)
      	{
			u32Ret = 10;
		}
		else
		{
        	 /* Close the file */
        	if( OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR )
         	{
				u32Ret = 20;
			}
			else
			{
				if( OSAL_s32IOClose(fdSourceFile) != OSAL_ERROR )
            	{
               		u32Ret = 30;
            	}
            	else
				{
				    /* Still Trying to close the file */
					if (OSAL_s32IOClose(fdSourceFile)!= OSAL_ERROR)
					{
						u32Ret = 40;
					}
					else
					{
						if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
						{
						u32Ret = 50;
						}
					}
				}
		 	}
	  	}
	}
   return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_FileOpenCloseLoop
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_009
* DESCRIPTION:  Open  and Close the file  multiple times
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_FileOpenCloseLoop(void)
{
   OSAL_tIODescriptor hUSBDevice   = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0;   // Handle for File
   tU32 u32Ret = 0,count = 0;
   tS32 s32Status = 0;
   
   hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL,OSAL_EN_READONLY);
   if( OSAL_ERROR == hUSBDevice )
   {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
		// check open close 10 times
      	for(count = 0 ; (count < 10) && (u32Ret == 0); count++)
      	{
        	fdSourceFile = OSAL_IOOpen(OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
         	if( fdSourceFile == OSAL_ERROR)
         	{
            	u32Ret = 10;
         	}
         	else
         	{
            	if( OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR )
            	{
               		u32Ret = 20;
				}
				else
				{
				 	/*Try to close the file  again*/
					if (OSAL_s32IOClose( fdSourceFile) != OSAL_ERROR)
					{
						u32Ret = 30;
					}
              	}
			 }
		 }
	if(OSAL_s32IOClose( hUSBDevice)== OSAL_ERROR)
	{
	u32Ret = 40;
	}

	 }

    return u32Ret;
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_ReadNBytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  Number of bytes user requested to read 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_ReadNBytes(tU32 u32bytesToRead, tU32 * u32bytesRead)
{
   OSAL_tIODescriptor hUSBDevice = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0; // File handle
   tU32 u32SizeOfFile = 0;
   tS8 *pdataBuffer = NULL ;
   tU32 u32BytesReadOfBlock = 0;
   tU32 u32Ret = 0;
   tS32 s32Ret = 0 ,s32Status = 0;
   
   pdataBuffer= (tS8 *)OSAL_pvMemoryAllocate((USBDNL_BUFFER_SIZE_ONE_MB));

   OSAL_pvMemorySet( pdataBuffer, 0x00, (USBDNL_BUFFER_SIZE_ONE_MB) );
      
   if(pdataBuffer !=NULL)
   {
      /*Open the device in Readonly mode */
     hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
     if( OSAL_ERROR == hUSBDevice )
  	  {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
	    /* Open the file in Readonly mode */
      	fdSourceFile = OSAL_IOOpen(OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
        if(fdSourceFile != OSAL_ERROR)
         {
            s32Ret = OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, 3000);	       
            if(s32Ret != OSAL_ERROR )
            {
               s32Ret = OSAL_s32IOControl(fdSourceFile, 
                  OSAL_C_S32_IOCTRL_FIONREAD, (tS32)&u32SizeOfFile);  
               if(s32Ret != OSAL_ERROR )
               {
                  u32SizeOfFile = (u32SizeOfFile > u32bytesToRead)? 
                                    u32bytesToRead: u32SizeOfFile;
                  u32SizeOfFile = (u32SizeOfFile > USBDNL_BUFFER_SIZE_ONE_MB)? 
                                    USBDNL_BUFFER_SIZE_ONE_MB:	u32SizeOfFile;
                  
                  u32BytesReadOfBlock = (tU32)OSAL_s32IORead(fdSourceFile, 
                     									(tPS8)pdataBuffer, u32SizeOfFile);
                  if(	!
                  		(  (u32BytesReadOfBlock != (tU32)OSAL_ERROR)
                 			   &&
                  			(u32BytesReadOfBlock != u32SizeOfFile)
								)
							)
                  {
                     u32Ret = 10;
                  }
						*u32bytesRead = u32BytesReadOfBlock;
            }
			/*Close the file */
            if(OSAL_s32IOClose(fdSourceFile)== OSAL_ERROR)
            {
               u32Ret = 30;
            }
			else
			{
            if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
				{
					u32Ret = 40;
				}
			}
		 }
	  }
	}
  }
   OSAL_vMemoryFree(pdataBuffer);
   pdataBuffer = NULL;
   return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read0Byte
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_010
* DESCRIPTION:  Test read operation for 0 byte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_Read0Byte(void)
{
   tU32 u32Ret = 0;
	tU32  u32bytesRead = 0;
   u32Ret = U32usbdnlOEDT_ReadNBytes(0,&u32bytesRead);
	if( (tU32)OSAL_ERROR == u32bytesRead ) 
  	   return 0;

	return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read1Byte
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_011
* DESCRIPTION:  Test read operation for 1 byte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/

tU32 U32usbdnlOEDT_Read1Byte(void)
{
   tU32 u32Ret = 0;
	tU32  u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(1,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead ) 
   	   return 0;

	return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read63Bytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_012
* DESCRIPTION:  Test read operation for 63 byte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_Read63Bytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(63,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
   	   return 0;

	return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read64Bytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_013
* DESCRIPTION:  Test read operation for 64 byte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/

tU32 U32usbdnlOEDT_Read64Bytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(64,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
	   return 0;

	return u32Ret;
}
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read65Bytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_014
* DESCRIPTION:  Test read operation for 65 byte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_Read65Bytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(65,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
	   return 0;

	return u32Ret;
}
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read4KBytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_015
* DESCRIPTION:  Test read operation for 4 Kilobytes
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/

tU32 U32usbdnlOEDT_Read4KBytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(4096,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
	   return 0;

	return u32Ret;
}
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read8KBytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_016
* DESCRIPTION:  Test read operation for 8 Kilobytes
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/

tU32 U32usbdnlOEDT_Read8KBytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(4096*2,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
	   return 0;

	return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_Read1MBytes
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_017
* DESCRIPTION:  Test read operation for 1Megabyte 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
* 13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_Read1MBytes(void)
{
   tU32 u32Ret = 0;
   tU32 u32bytesRead = (tU32)OSAL_ERROR;
   u32Ret = U32usbdnlOEDT_ReadNBytes(1024*1024,&u32bytesRead);
	if( (tU32)OSAL_ERROR != u32bytesRead )
	   return 0;

	return u32Ret;
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_SeekReadTest
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_018
* DESCRIPTION:  Test the READ operation after SEEK 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_SeekReadTest(void)
{
   
   OSAL_tIODescriptor hUSBDevice = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0; // File handle
   tU32 u32SizeOfFile = 0;
   tS8 *pdataBuffer = NULL;
   tU32 u32BytesReadOfBlock = 0;
   tU32 u32Ret = 0;
   tS32 s32Ret = 0,s32Status = 0;
   

   pdataBuffer= (tS8 *)OSAL_pvMemoryAllocate((USBDNL_BUFFER_SIZE_ONE_MB));

   OSAL_pvMemorySet( pdataBuffer, 0x00, (USBDNL_BUFFER_SIZE_ONE_MB) );

   
   if(pdataBuffer !=NULL)
   {
      hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
   if( OSAL_ERROR == hUSBDevice )
  	  {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
         fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
         if(fdSourceFile != OSAL_ERROR)
         {
            s32Ret = OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, 3000);	       
            if(s32Ret == OSAL_ERROR )
            {
               u32Ret = 20;
            }
            else if( OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_FIONREAD, (tS32)&u32SizeOfFile) == OSAL_ERROR )
            {
               u32Ret = 30;
            }  
            else 
            {
            	if(OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)u32SizeOfFile - NUMBER_OF_BYTES)== OSAL_ERROR )
               {
               	u32Ret = 40;
			   }
			   else
               {
               		u32BytesReadOfBlock = (tU32)OSAL_s32IORead(fdSourceFile, 
                  		(tPS8)pdataBuffer, NUMBER_OF_BYTES +1);
               		if(u32BytesReadOfBlock != NUMBER_OF_BYTES)
               		{
                  		u32Ret = 50;
               		}
            	}
           		if(OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR)
            	{
               		u32Ret = 60;
            	}
         	}
         	if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
         	{
            	u32Ret = 70;
         	}
		 }
	   }
	}
   OSAL_vMemoryFree(pdataBuffer);
   pdataBuffer = NULL;
   return u32Ret;
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_SeekReadMultipleFiles
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_019
* DESCRIPTION:  Test the READ operation after SEEK for multiple files 
* History :     Created by Pankaj Kumar (RBIN/EDI3)
**************************************************************************/
tU32 U32usbdnlOEDT_SeekReadMultipleFiles(void)
{
   
   OSAL_tIODescriptor hUSBDevice = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0; // File handle
   tU32 u32SizeOfFile = 0 ;
   tS8 *pdataBuffer = NULL ;
   tU32 u32BytesReadOfBlock = 0;
   tU32 u32Ret = 0;
   tS32 s32Ret,s32Status,j = 0;
   
   pdataBuffer= (tS8 *)OSAL_pvMemoryAllocate ((USBDNL_BUFFER_SIZE_ONE_MB));

   if(pdataBuffer !=NULL)
   {
      hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
   if( OSAL_ERROR == hUSBDevice )
  	  {
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_ALREADYEXISTS:
				u32Ret = 1;
				break;
			case OSAL_E_UNKNOWN:
				u32Ret = 2;
				break;
			default:
				u32Ret = 3;
		}
     }
	 else
	 {
		 for( j = 0; (j < 20) && (u32Ret == 0); j++)
         {
            /* For next Run */
            OSAL_pvMemorySet( pdataBuffer, 0x00, (USBDNL_BUFFER_SIZE_ONE_MB) );
            if((j%6)==0) 
            { 
           		fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
             	if(fdSourceFile == OSAL_ERROR)
            	{
               	u32Ret = 10;
               	break; 
            	}
            }
            if((j%6)==1) 
            {
            	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE2, OSAL_EN_READONLY); 
            	if(fdSourceFile == OSAL_ERROR)
            	{
               		u32Ret = 20;
               		break; 
            	}
            }
            if((j%6)==2)
            {
            	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE3, OSAL_EN_READONLY); 
            	if(fdSourceFile == OSAL_ERROR)
            	{
               		u32Ret = 30;
               		break; 
            	}
            }
            if((j%6)==3) 
            {
            	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE4, OSAL_EN_READONLY); 
            	if(fdSourceFile == OSAL_ERROR)
            	{
               		u32Ret = 40;
               		break; 
            	}
            }
            if((j%6)==4)
            {
            	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE5, OSAL_EN_READONLY); 
	            if(fdSourceFile == OSAL_ERROR)
    	        {
         	    	u32Ret = 50;
               		break; 
            	}
            }
            if((j%6)==5)
            {
            	fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE6, OSAL_EN_READONLY); 
            	if(fdSourceFile == OSAL_ERROR)
            	{
               		u32Ret = 60;
               		break; 
            	}
            }
            s32Ret = OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, 3000);	       
            if(s32Ret != OSAL_ERROR )
            {

               s32Ret = OSAL_s32IOControl(fdSourceFile, 
                  OSAL_C_S32_IOCTRL_FIONREAD, (tS32)&u32SizeOfFile);  
               if(s32Ret != OSAL_ERROR )
               {
                  
                  s32Ret = OSAL_s32IOControl(fdSourceFile, 
                     OSAL_C_S32_IOCTRL_FIOSEEK, (tS32)u32SizeOfFile - NUMBER_OF_BYTES);
                  if(s32Ret != OSAL_ERROR )
                  {
                     u32BytesReadOfBlock = (tU32)OSAL_s32IORead(fdSourceFile, 
                        (tPS8)pdataBuffer, NUMBER_OF_BYTES +1);
                     if(u32BytesReadOfBlock != NUMBER_OF_BYTES)
                     {
                        u32Ret = 70;
                     }
                  }
                  else
                  {
                     u32Ret = 80;
                  }
               }
               else
               {
                  u32Ret = 90;
               }
            }
            else
            {
               u32Ret = 100;
            }
            if(OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR)
            {
               u32Ret = 110;
            }

			}
			if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
			{
				u32Ret = 120;
			}
          
          }

	   	}
    OSAL_vMemoryFree( pdataBuffer );
	pdataBuffer = NULL;
	return u32Ret;
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_Version
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_020
* DESCRIPTION:  This function is used to get the version number of the device.  
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_Version(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Arg = 0;
	tS32 s32Ret = 0;
/* Wait is use to avoid the device open hanging. */	
	OSAL_s32ThreadWait(100);
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		s32Ret = OSAL_s32IOControl(hUSBDevice, OSAL_C_S32_IOCTRL_VERSION, 
			                   (tS32)&s32Arg);
		if(s32Ret == OSAL_ERROR )
		{
		   u32Ret = 1;   
		}
     	/* Wait is use to avoid the device close hanging.*/
	  	OSAL_s32ThreadWait(1000);

		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
          
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret; 
	
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_LinkStatusCheck
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_021
* DESCRIPTION:  This function is used to check the link between host and target.   
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_LinkStatusCheck(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	tS32 s32Arg = 0;
	
	/* Wait is use to avoid the device open hanging. */
	OSAL_s32ThreadWait(100);
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
			OSAL_C_S32_IOCTRL_USB_LINK_STATUS_CHECK, (tS32)&s32Arg);
		if(s32Ret == OSAL_ERROR )
		{
		   u32Ret = 1;
		}
     	/* Wait is use to avoid the device close hanging.*/
	  OSAL_s32ThreadWait(1000);

		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}
	else
	{
		u32Ret = 10;
	}
	return u32Ret; 
	
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_LinkStatusWait
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_022
* DESCRIPTION:  This function is used to check the link between host and target.    
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_LinkStatusWait(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
			                      OSAL_C_S32_IOCTRL_USB_LINK_STATUS_WAIT, 0);  
		if(s32Ret == OSAL_ERROR)
		{
		   u32Ret = 1;
        }
	     	/* Wait is use to avoid the device close hanging.*/
		OSAL_s32ThreadWait(100);

		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 100;
		}

	}
	else
	{
		u32Ret = 10;
	}
	return u32Ret; 
	
}

/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_ReadBreakTimeSet
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_023
* DESCRIPTION:  This function is used to set read time out value for the driver.    
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_ReadBreakTimeSet(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	OSAL_tIODescriptor fdSourceFile = OSAL_ERROR; // File handle
	
	tS32 s32Ret =0;
	tU32 u32Ret = 0;
	tS32 s32Arg = 3000;
    tS8 *ps8Buffer;
	ps8Buffer = (tS8*)OSAL_pvMemoryAllocate(USBDNL_BUFFER_SIZE_ONE_MB);
	if(ps8Buffer == NULL)
	{
		/* sorry; can't continue with out buffer */
		u32Ret = 1;
		return u32Ret;
	}
     	/* Wait is use to avoid the open hanging.*/
	OSAL_s32ThreadWait(100);
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
			OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, s32Arg);
		if(s32Ret != OSAL_ERROR )
		{
			/* Open file; open function inturn call the 
			     OSAL_C_S32_IOCTRL_USB_FOPEN IOCTRL to open a file */
			fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY); 
			if(fdSourceFile != OSAL_ERROR)
			{	
				/* read; the driver read call inturn call the 
				    OSAL_C_S32_IOCTRL_USB_FREAD IOCTRL for Initiating read operation */
				s32Ret = OSAL_s32IORead(fdSourceFile, (tPS8)ps8Buffer, 
				                                            USBDNL_BUFFER_SIZE_ONE_MB );
				if(s32Ret != USBDNL_BUFFER_SIZE_ONE_MB)
				{
					u32Ret = 2;
				}
				OSAL_s32IOClose(fdSourceFile);
			}
			else
			{
				u32Ret = 3;
			}						
		}
		else
		{
			u32Ret = 4;
		}
		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}
	else
	{
		u32Ret = 10;
	}
	if(ps8Buffer)
	{
		OSAL_vMemoryFree(ps8Buffer);
		ps8Buffer = OSAL_NULL;
	}
	
	return u32Ret; 
}

/* this structure is declared here to test the Flash progress IOCTRL */
struct flashProgress
{
	tU8		flashMode;
	tU32	flashAddress;
	tU32	programmedUnits;
	tU32	totalUnits ;
}; 


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_FileRelatedTest
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_024
* DESCRIPTION: This test covers the follwoing IOCTRLs. 
*             OSAL_C_S32_IOCTRL_USB_LINK_STATUS_CHECK  
*             OSAL_C_S32_IOCTRL_USB_LINK_STATUS_WAIT
*             OSAL_C_S32_IOCTRL_USB_WRITE_BREAKTIME_SET
*             OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET
*             OSAL_C_S32_IOCTRL_USB_FOPEN
*             OSAL_C_S32_IOCTRL_USB_FSEEK
*             OSAL_C_S32_IOCTRL_USB_FREAD
*             OSAL_C_S32_IOCTRL_USB_FILE_ABORT
*             OSAL_C_S32_IOCTRL_USB_FILE_COMPLETE
*             OSAL_C_S32_IOCTRL_USB_FCLOSE
*             OSAL_C_S32_IOCTRL_USB_FLASH_PROGRESS
*             OSAL_C_S32_IOCTRL_USB_FLASH_ERROR
*             OSAL_C_S32_IOCTRL_USB_FLASH_COMPLETE
*             OSAL_C_S32_IOCTRL_USB_ALL_COMPLETE 
* Note1: 
*  OSAL_C_S32_IOCTRL_USB_FOPEN - the File open call intruns call this IOCTRL.
*  OSAL_C_S32_IOCTRL_USB_FCLOSE - The file close call inturns call this IOCTRL.
*  OSAL_C_S32_IOCTRL_USB_FSEEK - same as OSAL_C_S32_IOCTRL_FIOSEEK
*  OSAL_C_S32_IOCTRL_USB_FREAD - the file read call inturns call this IOCTRL. 
*
* Note2: OSAL_C_S32_IOCTRL_USB_FWRITE - not used. because there is no 
*                                              write operation in drv_usbdnl.
*  
* History :     Created by Anoop Chandran(RBEI/ECM1)
*13-April-2009 | Lint removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/

tU32 U32usbdnlOEDT_IOCTRL_FileRelatedTest(void)
{
	tS32 s32Ret = 0;
	struct flashProgress arFlashProgress;
	tS32 cnt0, cnt1, cnt2;
	tS8 *ps8Buffer;
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	OSAL_tIODescriptor fdSourceFile = OSAL_ERROR; // File handle
	tU32 u32Ret = 0;
	tU32 fileSize;
	
	ps8Buffer = (tS8*)OSAL_pvMemoryAllocate((USBDNL_BUFFER_SIZE_ONE_MB));
	if(ps8Buffer == NULL)
	{
		u32Ret = 1;
		return u32Ret;   
	}
	/* open device */
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		/* check if device is available */   
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		OSAL_C_S32_IOCTRL_USB_LINK_STATUS_CHECK, 0);
		if(s32Ret == OSAL_ERROR )
		{
			/* wait for file being available */
			s32Ret = OSAL_s32IOControl(hUSBDevice, 
			OSAL_C_S32_IOCTRL_USB_LINK_STATUS_WAIT, 0);

			if(s32Ret == OSAL_ERROR ) 
			{    		
				u32Ret = 1;
				/* close device */
				s32Ret = OSAL_s32IOClose(hUSBDevice);
				if(s32Ret == OSAL_ERROR)
				   u32Ret = 7000;

				/* free the memory */
				if(OSAL_NULL != ps8Buffer)
				{
					OSAL_vMemoryFree(ps8Buffer);
		 			ps8Buffer = OSAL_NULL;
				}
				return u32Ret;
			}	
		}
	}
	else
	{
		u32Ret = 5;
		OSAL_vMemoryFree(ps8Buffer);
		return u32Ret;
	}// device open fail
	
	/* do this multiple times */
	for (cnt0 = 0; cnt0 < 2; cnt0++)
	{
	     	/* Wait is use to avoid the open hanging.*/
		OSAL_s32ThreadWait(100);

		/* Open file; open function inturn call the 
		     OSAL_C_S32_IOCTRL_USB_FOPEN IOCTRL to open a file */
		fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, 	OSAL_EN_READONLY); 
		if(fdSourceFile == OSAL_ERROR)
		{
			u32Ret += 10;
		}
		/* set the appropriate time out here for the write */
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		                     OSAL_C_S32_IOCTRL_USB_WRITE_BREAKTIME_SET, OEDT_BREAT_TIME);
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 30;
		}
		
		/* set  maximum time our for read */
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		                     OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, OEDT_BREAT_TIME);
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 50;
		}
		
		/* get file size */
		s32Ret = OSAL_s32IOControl(fdSourceFile, OSAL_C_S32_IOCTRL_FIONREAD, 
		                                                   (tS32)&fileSize);  
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 100;
		}

		/* OSAL_C_S32_IOCTRL_FIOSEEK IOCTRL and OSAL_C_S32_IOCTRL_USB_FSEEK 
		     is same. */	 
		/* set file pointer to beginning i.e offset = 0 */
		s32Ret = OSAL_s32IOControl(fdSourceFile, OSAL_C_S32_IOCTRL_FIOSEEK, OSAL_NULL);
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 200;
		}
		/* read; the driver read call inturn call the 
		    OSAL_C_S32_IOCTRL_USB_FREAD IOCTRL for Initiating read operation */
		s32Ret = OSAL_s32IORead(fdSourceFile, (tPS8)ps8Buffer, 
		                                            USBDNL_BUFFER_SIZE_ONE_MB );
		if(s32Ret != USBDNL_BUFFER_SIZE_ONE_MB)
		{
			u32Ret += 500;
		}
		
		/* this is just for test purpose, in real download we can see
		abort operation, if some thing is really went wrong */
#if 0	//anoop
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		                                 OSAL_C_S32_IOCTRL_USB_FILE_ABORT, OSAL_NULL);	
     
#endif //anoop
        /* since we are doing test, the return value which we get form the above
                      has no meaning. if you take care the return value here, 
		we cant test further. This abort operation must be real time usecase 
		not in OEDT. */						  
#if 0										 
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret = 40011;
			OSAL_s32IOClose(fdSourceFile);
			OSAL_s32IOClose(hUSBDevice);
			OSAL_vMemoryFree(ps8Buffer);
			return u32Ret;
		}
#endif		
		 /* this is just for test purpose. when ever the file read operation
		completed the DLTool automatically take cares this step */
#if 0	//anoop

		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		                              OSAL_C_S32_IOCTRL_USB_FILE_COMPLETE, OSAL_NULL);
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 1000;
		}
#endif //anoop
	     	/* Wait is use to avoid the device close hanging.*/
		OSAL_s32ThreadWait(100);
	/* close file; the file close function inturn call the 
	    OSAL_C_S32_IOCTRL_USB_FCLOSE ioctrl */

		s32Ret = OSAL_s32IOClose(fdSourceFile);
		if(s32Ret == OSAL_ERROR)
		{
			u32Ret += 3000;
		}
		arFlashProgress.totalUnits = fileSize;
		for (cnt1 = 2; cnt1 < 5; cnt1++)
		{
			arFlashProgress.flashMode = (tU8)cnt1;
			
			for (cnt2 = 0; cnt2 < 8; cnt2++)
			{
				arFlashProgress.flashAddress =(tU32)( 0xF0000000 + 
					                                 (fileSize  * (tU32)cnt2) / 7);
				arFlashProgress.programmedUnits = (tU32)((fileSize * (tU32)cnt2) / 7);
			     	/* Wait is use to avoid the IOcontrol hanging.*/
				OSAL_s32ThreadWait(100);
				/* this is just for test purpose only and to 
				     get feel of download . */
				s32Ret = OSAL_s32IOControl(hUSBDevice, 
				                       OSAL_C_S32_IOCTRL_USB_FLASH_PROGRESS, 
				                       (tS32) &arFlashProgress);
				if(s32Ret == OSAL_ERROR)
				{
			       u32Ret += 5000;
					 break;
   	      }
			     	/* Wait is use to avoid the IOcontrol hanging.*/

				OSAL_s32ThreadWait(100);

				/* this is just for test purpose */
				if(cnt2==4 ||cnt2==6)
				{
					s32Ret = OSAL_s32IOControl(hUSBDevice, 
					                        OSAL_C_S32_IOCTRL_USB_FLASH_ERROR, 
					                        (tS32) &arFlashProgress);
				    if(s32Ret == OSAL_ERROR)
				    {
			           u32Ret += 8000;
						  break;
		          }
		      }										
			}
		}
			     	/* Wait is use to avoid the IOcontrol hanging.*/

				OSAL_s32ThreadWait(100);

		/* flash complete */
		s32Ret = OSAL_s32IOControl(hUSBDevice, 
		                              OSAL_C_S32_IOCTRL_USB_FLASH_COMPLETE, OSAL_NULL);
		if(s32Ret == OSAL_ERROR)
		{
	       u32Ret += 10000;
      }
									  
	}

    	/* Wait is use to avoid the IOcontrol hanging.*/
	OSAL_s32ThreadWait(100);
	/* all complete */
	s32Ret = OSAL_s32IOControl(hUSBDevice, 
	                                  OSAL_C_S32_IOCTRL_USB_ALL_COMPLETE, OSAL_NULL);
	if(s32Ret == OSAL_ERROR)
	{
       u32Ret += 20000;

    }
	/* Wait is use to avoid the device close hanging. */
	  OSAL_s32ThreadWait(1000);

		/* close device */
		s32Ret = OSAL_s32IOClose(hUSBDevice);
		if(s32Ret == OSAL_ERROR)
		   u32Ret = 40000;

		/* free the memory */
		if(OSAL_NULL != ps8Buffer)
		{
			OSAL_vMemoryFree(ps8Buffer);
 			ps8Buffer = OSAL_NULL;
		}
	return u32Ret;
}



/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_usbBoardID
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_025
* DESCRIPTION: This test is only test the IOcontrol interface for board ID.     
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_usbBoardID(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	struct boardID board = {0};
	
	tU8 proName[] = "OEDT TEST FOR IOCTRL";
	tU8 boaName[] = "OMAP 5948 OK"; 
	
	tU8 i = 0;
	
	 OSAL_pvMemorySet(&board, 0, sizeof(board));	
	(void)OSAL_szStringCopy(board.processName, proName);
	(void)OSAL_szStringCopy(board.boardName, boaName);

	board.processID = (tU32)0x11111111;
	board.boardID = (tU32)(tU32)0x00000100;
	
	/* open device */
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{

		for(i=0; i<10; i++)
		{
		    	/* Wait is use to avoid the IOcontrol hanging.*/
			OSAL_s32ThreadWait(100);

			/* send the board ID to the host */
			s32Ret = OSAL_s32IOControl(hUSBDevice, 
			                OSAL_C_S32_IOCTRL_USB_BOARD_ID, (tS32)&board);
			if(s32Ret == OSAL_ERROR)
			{
				// there is some problem of sending board ID to host
				u32Ret = 1;
			}
		}
	/* Wait is use to avoid the device close hanging. */		
 	  OSAL_s32ThreadWait(1000);

		/* close device */
		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 100;
		}
	}
	else
	{
		u32Ret = 10;
	}// device open fail
	return u32Ret; 
	
}


/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_usbRawData
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_026
* DESCRIPTION: This test is to send the raw command to the host
*  raw command: file abort, file complete, file progress, etc,. 
* History :     Created by Anoop Chandran(RBEI/ECM1)
*13-April-2009 | Lint removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_usbRawData(void)
{
#define USB_IND_ALL_COMPLETE        (0x0017)
	   
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
	tU8 tempData[2] = {0};
	struct usbRaw rawData = {0};

	OSAL_pvMemorySet(&rawData, 0, sizeof(rawData));
   tempData[0] = (tU8)(USB_IND_ALL_COMPLETE & 0x00FF);
	/*lint -e778  results may be zero, don't worry*/
   tempData[1] = (tU8)((USB_IND_ALL_COMPLETE & 0xFF00) >> 8);/*lint !e572 results may be zero, don't worry */ 
   /*lint +e778  results may be zero, don't worry*/

 	rawData.size = 2;
	rawData.wEP0Expected = (tU16)0xFF;
		
	(void)OSAL_pvMemoryCopy(rawData.data, tempData, sizeof(tempData));

	/* open device */
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{

	    	/* Wait is use to avoid the IOcontrol hanging.*/
		OSAL_s32ThreadWait(500);
		/* raw data  */
		s32Ret = OSAL_s32IOControl(hUSBDevice, OSAL_C_S32_IOCTRL_USB_RAW, 
			(tS32)&rawData);
		if(s32Ret == OSAL_ERROR)
		{
			// there is some problem of sending command to host	
			u32Ret = 1;
		}
   	/* Wait is use to avoid the device close hanging. */
 	  	OSAL_s32ThreadWait(1000);
		/* close device */
		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 100;
		}

	}
	else
	{
		u32Ret = 10;
	}// device open fail
	return u32Ret; 
	
}
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_InterfaceInvalPara()
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_027
* DESCRIPTION: This test covers the invalid parameter for 
					following IOCTRLs. 
						OSAL_C_S32_IOCTRL_VERSION
						OSAL_C_S32_IOCTRL_USB_BOARD_ID
						OSAL_C_S32_IOCTRL_USB_RAW
						OSAL_C_S32_IOCTRL_FIONREAD
						OSAL_C_S32_IOCTRL_USB_FLASH_ERROR
						OSAL_C_S32_IOCTRL_USB_FLASH_PROGRESS
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_InterfaceInvalPara(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32Ret = 0;
/* Wait is use to avoid the device open hanging. */	
	OSAL_s32ThreadWait(100);
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_VERSION*/
		
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_VERSION, 
			         (tS32)OSAL_NULL
			      );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret = 1;   
		}
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_USB_BOARD_ID*/
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_USB_BOARD_ID, 
                  (tS32)OSAL_NULL
               );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret += 5;   
		}
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_USB_RAW*/
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_USB_RAW, 
			         (tS32)OSAL_NULL
			      );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret += 10;   
		}
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_FIONREAD*/
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_FIONREAD, 
			         (tS32)OSAL_NULL
			      );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret += 20;   
		}
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_USB_FLASH_ERROR*/
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_USB_FLASH_ERROR, 
			         (tS32)OSAL_NULL
			      );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret += 50;   
		}
		OSAL_s32ThreadWait(100);

		/* set the invalid parameter for OSAL_C_S32_IOCTRL_USB_FLASH_PROGRESS*/
		s32Ret = OSAL_s32IOControl
					(
						hUSBDevice, 
						OSAL_C_S32_IOCTRL_USB_FLASH_PROGRESS, 
			         (tS32)OSAL_NULL
			      );
		if(s32Ret != OSAL_ERROR )
		{
		   u32Ret += 100;   
		}

     	/* Wait is use to avoid the device close hanging.*/
	  	OSAL_s32ThreadWait(1000);

		if(OSAL_s32IOClose(hUSBDevice) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
          
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret; 
	
}
#define OEDT_FFS2_MAX_FILE_PATH_LEN         260 
#ifdef SWITCH_FFS2
#define OEDTTEST_C_STRING_DEVICE_FFS2 OSAL_C_STRING_DEVICE_FFS2
#else
#define OEDTTEST_C_STRING_DEVICE_FFS2 "/dev/nand0"
#endif
#define MAX_READ_BUFFER_SIZE (1024*100)
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_ReadLargeFileInChunks()
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_028
* DESCRIPTION:  Tests the read operation of large file  in small chunks.
* History :     Created by Haribabu Sannapaneni (RBEI/ECM1)
*13-April-2009 | Lint removed | Jeryn Mathew (RBEI/ECF1)
**************************************************************************/
tU32 U32usbdnlOEDT_ReadLargeFileInChunks(void)
{
   OSAL_tIODescriptor hUSBDevice = 0;   // Handle for USB Device
   OSAL_tIODescriptor fdSourceFile = 0; // File handle
   OSAL_tIODescriptor hFile = 0; /* File handle of the destination file */
   tChar szFilePath [OEDT_FFS2_MAX_FILE_PATH_LEN + 1] = OEDTTEST_C_STRING_DEVICE_FFS2"/dragon.bin";
   tU32 u32SizeOfFile = 0;
   tS8 *pdataBuffer = NULL ;
   tU32 u32BytesReadOfBlock = 0;
   tU32 u32Ret = 0;
   tS32 s32Ret = 0;
   tU32 u32ChunKSize = MAX_READ_BUFFER_SIZE;
   tU32 u32bytesToRead = 0;
   tU32 u32BytesToWrite = 0, u32bytesReamin = 0;
   tS32 s32BytesWritten = 0;

   pdataBuffer= (tS8 *)OSAL_pvMemoryAllocate((u32ChunKSize));

   OSAL_pvMemorySet( pdataBuffer, 0x00, u32ChunKSize );
      
   if(pdataBuffer !=NULL)
   {
      /*Open the device in Readonly mode */
     hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
     if( OSAL_ERROR == hUSBDevice )
  	  {
		OSAL_vMemoryFree(pdataBuffer);
		return 2;
      }
	 else
	 {
	    /* Open the file in Readonly mode */
      	fdSourceFile = OSAL_IOOpen(OEDT_USBDNL_FILE, OSAL_EN_READONLY);
        if(fdSourceFile != OSAL_ERROR)
         {
            s32Ret = OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, 3000);	       
            if(s32Ret != OSAL_ERROR )
            {
               s32Ret = OSAL_s32IOControl(fdSourceFile, 
                  OSAL_C_S32_IOCTRL_FIONREAD, (tS32)&u32SizeOfFile);  
               if(s32Ret != OSAL_ERROR )
               {
			   /* File to store the data read from host */
				if( (hFile = (OSAL_IOCreate (szFilePath, OSAL_EN_READWRITE)))== OSAL_ERROR )
				{
					OSAL_vMemoryFree(pdataBuffer);
					return u32Ret = 10;
				}
                  u32bytesToRead = (u32SizeOfFile > u32ChunKSize)? 
                                    u32ChunKSize: u32SizeOfFile;
				  u32bytesReamin = 	u32SizeOfFile;
				  /* Read Large File */
                  for(; 0<u32bytesReamin; )
				  {
	                  u32BytesReadOfBlock = (tU32)OSAL_s32IORead(fdSourceFile, 
	                     									             (tPS8)pdataBuffer, 
                                                                u32bytesToRead);
	                  if( ( (u32BytesReadOfBlock == (tU32)OSAL_ERROR) ||  (u32BytesReadOfBlock != u32bytesToRead) ) )
	                  {
	                     u32Ret = 100;
						/*Close the file */
			            if(OSAL_s32IOClose(fdSourceFile)== OSAL_ERROR)
			            {
			               u32Ret += 30;
			            }
						else
						{
			            	if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
							{
								u32Ret += 40;
							}
						}
						
					   return u32Ret;
	                  }

 						/* Write data read from host to the file in nand0 */
						u32BytesToWrite = u32bytesToRead;
						s32BytesWritten = OSAL_s32IOWrite (
						                            hFile,(tPS8)pdataBuffer,
																 (tU32) u32BytesToWrite
														    );
							if ( (s32BytesWritten == OSAL_ERROR)||(s32BytesWritten != (tS32)u32BytesToWrite) )
							{
								u32Ret = 1000;
								/* Close destination file */
								if(OSAL_s32IOClose ( hFile )== OSAL_ERROR)
								{
									u32Ret += 20;
							    }
								else if( OSAL_s32IORemove ( szFilePath ) )
								{
									u32Ret += 30;
								}
							  return u32Ret; 
							}
							
 					  u32bytesReamin = u32bytesReamin - u32bytesToRead;
					  u32bytesToRead = (u32bytesReamin > u32ChunKSize)? 
                                    u32ChunKSize: u32bytesReamin;
						

				 } 
            }
			/* Close destination file */
			if(OSAL_s32IOClose ( hFile )== OSAL_ERROR)
			{
				u32Ret += 20;
		    }
			OSAL_s32IORemove ( szFilePath );
			/*Close the file */
            if(OSAL_s32IOClose(fdSourceFile)== OSAL_ERROR)
            {
               u32Ret = 30;
            }
			else
			{

            if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
				{
					u32Ret = 40;
				}
			}
		 }
	  }/* if*/
	  else
	  {
	   OSAL_vMemoryFree(pdataBuffer);
	   return 3;
	  }
	}/*else*/
  }	/*if*/
  else
  {
  return 1;
  }
   OSAL_vMemoryFree(pdataBuffer);
   pdataBuffer = NULL;
   return u32Ret;

}
#define OEDT_USBDNL_FSEEK_SIZE 1024
/**************************************************************************
* FUNCTION:     U32usbdnlOEDT_IOCTRL_Fseek
* PARAMETER:    None  
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:	TU_OEDT_USBDNL_029
* DESCRIPTION:  This function is used to Check the Fseek(File size 1MB).  
* History :     Created by Anoop Chandran(RBEI/ECM1)
**************************************************************************/
tU32 U32usbdnlOEDT_IOCTRL_Fseek_FIONREAD(void)
{
	OSAL_tIODescriptor hUSBDevice   = OSAL_ERROR;   // Handle for USB Device
	OSAL_tIODescriptor fdSourceFile = OSAL_ERROR;   // Handle for USB Device
	tU32 u32Ret = 0;
	tS32 s32FileCurSize = 0;
	tS32 s32Ret = 0;
/* Wait is use to avoid the device open hanging. */	
	OSAL_s32ThreadWait(100);
	/*open dev usbdnl*/
	hUSBDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_USB_DNL, OSAL_EN_READONLY);
	if( hUSBDevice != OSAL_ERROR)
	{
         /*open the file OEDT_USBDNL_FILE1 */
         fdSourceFile = OSAL_IOOpen( OEDT_USBDNL_FILE1, OSAL_EN_READONLY);
         if(fdSourceFile != OSAL_ERROR)
         {
            s32Ret = OSAL_s32IOControl(fdSourceFile, 
               OSAL_C_S32_IOCTRL_USB_READ_BREAKTIME_SET, 3000);	       
            if(s32Ret != OSAL_ERROR )
				{
					
					OSAL_s32ThreadWait(10);
					/*Check the current file size*/
					s32Ret = OSAL_s32IOControl(fdSourceFile, OSAL_C_S32_IOCTRL_FIONREAD, 
						                   (tS32)&s32FileCurSize);
					if(s32Ret == OSAL_ERROR )
					{
					   u32Ret = 1;   
					}
					while( s32FileCurSize != 0)
					{

						OSAL_s32ThreadWait(10);
						/*Seek the file by OEDT_USBDNL_FSEEK_SIZE*/
						s32Ret = OSAL_s32IOControl(fdSourceFile, OSAL_C_S32_IOCTRL_FIOSEEK, 
							                   (tS32)OEDT_USBDNL_FSEEK_SIZE);
						if(s32Ret == OSAL_ERROR )
						{
						   u32Ret += 10;   
						}

						OSAL_s32ThreadWait(10);
						/*Check the current file size*/
						s32Ret = OSAL_s32IOControl(fdSourceFile, OSAL_C_S32_IOCTRL_FIONREAD, 
							                   (tS32)&s32FileCurSize);
						if(s32Ret == OSAL_ERROR )
						{
						   u32Ret = 1;   
						}
	    				OEDT_HelperPrintf
	    				(
	    					TR_LEVEL_USER_1,
	                  "Remaining File size:-> %ld\n ",
	                  s32FileCurSize
						);

					}
				}
			/* Wait is use to avoid the device close hanging.*/
			OSAL_s32ThreadWait(100);

			if(OSAL_s32IOClose(fdSourceFile) == OSAL_ERROR )
			{
				u32Ret += 10;
			}
		}
		/*Close the file */
	   if(OSAL_s32IOClose(hUSBDevice)== OSAL_ERROR)
	   {
	      u32Ret = 30;
	   }
		    
	}
	else
	{
		u32Ret = 2;
	}
	return u32Ret; 
	
}


// End of oedt_usbdnl_TestFuncs.c file

