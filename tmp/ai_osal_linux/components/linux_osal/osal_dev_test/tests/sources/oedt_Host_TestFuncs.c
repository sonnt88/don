/******************************************************************************
 *FILE         : oedt_Host_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the  test cases for Host device 
 *               
 *AUTHOR       : Martin Langer (CM-AI/PJ-CF33)
 *
 *COPYRIGHT    : (c) 2011  Bosch Car Multimedia
 *
 *HISTORY      : 22.09.11  Initial version
 *
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "oedt_Host_TestFuncs.h"


/************************************************************************
 * FUNCTION:      u32HostOpenDev 
 * DESCRIPTION:   1.Opens the device.
 * 			      2.Close the device. 			     
 *
 * PARAMETER:     Nil
 *             
 * RETURNVALUE:   Open and Close Host Device
 * HISTORY:       22.09.11  Martin Langer (CM-AI/PJ-CF33)
 *
 * ************************************************************************/
tU32 u32HostOpenDev(void)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32Ret = 0;
   tS32 s32Status = 0;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_HOST, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   	{
		/* check the error status returned */
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret = 21;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret = 22;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret = 23;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret = 24;
				 break;
		    default:
				 u32Ret = 25;
	 	}    
		return u32Ret;
	}

	if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	{
		s32Status = (tS32) OSAL_u32ErrorCode();
		switch(s32Status)
		{
			case OSAL_E_DOESNOTEXIST:
				 u32Ret = 31;
				 break;
		    case OSAL_E_INVALIDVALUE:
			     u32Ret = 32;
				 break;    
            case OSAL_E_NOACCESS:      	
				 u32Ret = 33;
				 break;
            case OSAL_E_UNKNOWN:
			   	 u32Ret = 34;
				 break;
		    default:
				 u32Ret = 35;
	 	}    
		return 1000 + u32Ret;
	}
	
	return 0 ;
} // End of u32HostOpenDev()

