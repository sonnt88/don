
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 


extern void os_assertion_failed(const char *file, unsigned long line);
#define OS_ASSERT(_INT_) \
        ((_INT_) ? (tVoid)(0) : (os_assertion_failed(__FILE__, (__LINE__)&(~0x80000000))) )



#define NO_OF_SEMAPHORES 25
typedef struct tSemaphoreTableEntry_
{
   OSAL_tSemHandle hSem;
   const char *szName;
   tBool bBeingDeleted;
} tSemaphoreTableEntry;


tSemaphoreTableEntry m_oSemaphoreTable[NO_OF_SEMAPHORES] = {0};
OSAL_tSemHandle m_hTableSemaphore;


#define POST_TASK_WAIT_MS              5
#define WAIT_TASK_WAIT_MS              5
#define CLOSE_AND_DELETE_TASK_WAIT_MS  25
#define CREATE_TASK_WAIT_MS            10

#define NO_OF_POST_TASKS               10
#define NO_OF_WAIT_TASKS               15
#define NO_OF_CLOSE_TASKS              1
#define NO_OF_CREATE_TASKS             2


OSAL_trThreadAttribute m_hPostTasks[NO_OF_POST_TASKS] = {0};
OSAL_trThreadAttribute m_hWaitTasks[NO_OF_WAIT_TASKS] = {0};
OSAL_trThreadAttribute m_hCloseTasks[NO_OF_CLOSE_TASKS] = {0};
OSAL_trThreadAttribute m_hCreateTasks[NO_OF_CREATE_TASKS] = {0};


tU32 u32RandomSemaphore ()
{
   tDouble rnd = (tDouble) OSAL_s32Random();
   tDouble d = (tDouble) NO_OF_SEMAPHORES;

   d = (rnd / RAND_MAX) * d + 0.5 - 1;

   OS_ASSERT ( ((tU32) d) < NO_OF_SEMAPHORES);

   return (tU32) d;
}



void vShuffleThreadPrio2()
{
   tU8 newPrio;
   tDouble rnd = (tDouble) OSAL_s32Random();     // 0-32767

   rnd = (rnd / RAND_MAX) * 70;
   newPrio = (tU8) rnd+180;

   OSAL_s32ThreadPriority (OSAL_ThreadWhoAmI(), newPrio);
}


void vWaitRandom2(int msecMax)
{
   tDouble max = (tDouble) msecMax;
   tS32 tmp = OSAL_s32Random();
   tDouble rnd = (tDouble) tmp;

   rnd = (rnd / RAND_MAX) * max + 0.5;

   OSAL_s32ThreadWait((OSAL_tMSecond) rnd);
}





void postThread(void *arg)
{
   (void) arg;  // not necessary in this function

   while (1)   /*lint !e716     Yes, this is an endless loop... */
   {
      tU32 u32SemId;
      OSAL_tSemHandle hSem;

      vShuffleThreadPrio2();

      u32SemId = u32RandomSemaphore();
      
      if (OSAL_s32SemaphoreOpen (m_oSemaphoreTable[u32SemId].szName, &hSem) == OSAL_OK)
      {
         OSAL_s32SemaphorePost(hSem);

         if (u32RandomSemaphore() < NO_OF_SEMAPHORES/2)
         {
            OSAL_s32SemaphoreClose(hSem);      
            vWaitRandom2(POST_TASK_WAIT_MS);
         }
         else
         {
            vWaitRandom2(POST_TASK_WAIT_MS);
            OSAL_s32SemaphoreClose(hSem);      
         }
      }
   }
}


void waitThread(void *arg)
{
   (void) arg;  // not necessary in this function

   while (1)   /*lint !e716     Yes, this is an endless loop... */
   {
      tU32 u32SemId;
      OSAL_tSemHandle hSem;

      vShuffleThreadPrio2();

      u32SemId = u32RandomSemaphore();
      if (OSAL_s32SemaphoreOpen (m_oSemaphoreTable[u32SemId].szName, &hSem) == OSAL_OK)
      {
         OSAL_s32SemaphoreWait(hSem, OSAL_C_TIMEOUT_FOREVER);
      
         if (u32RandomSemaphore() < NO_OF_SEMAPHORES/2)
         {
            OSAL_s32SemaphoreClose(hSem);      
            vWaitRandom2(WAIT_TASK_WAIT_MS);
         }
         else
         {
            vWaitRandom2(WAIT_TASK_WAIT_MS);
            OSAL_s32SemaphoreClose(hSem);      
         }
      }
   }
}


void closeThread(void *arg)
{
   (void) arg;  // not necessary in this function

   while (1)   /*lint !e716     Yes, this is an endless loop... */
   {
      tS32 s32RetVal;
      tU32 u32SemId;
      int i;
      vShuffleThreadPrio2();

      u32SemId = u32RandomSemaphore();

      OSAL_s32SemaphoreWait (m_hTableSemaphore, OSAL_C_TIMEOUT_FOREVER);
      if (m_oSemaphoreTable[u32SemId].bBeingDeleted != TRUE && m_oSemaphoreTable[u32SemId].hSem != OSAL_C_INVALID_HANDLE)
      {
         m_oSemaphoreTable[u32SemId].bBeingDeleted = TRUE;
         OSAL_s32SemaphorePost (m_hTableSemaphore);

         if (OSAL_s32SemaphoreDelete(m_oSemaphoreTable[u32SemId].szName) == OSAL_OK)
         {
            for (i=0; i<40; ++i)
            {
               s32RetVal = OSAL_s32SemaphorePost (m_oSemaphoreTable[u32SemId].hSem);
               OS_ASSERT(s32RetVal == OSAL_OK);
            }

            s32RetVal = OSAL_s32SemaphoreClose(m_oSemaphoreTable[u32SemId].hSem);
            OS_ASSERT(s32RetVal == OSAL_OK);

            m_oSemaphoreTable[u32SemId].hSem = OSAL_C_INVALID_HANDLE;
      
            vWaitRandom2(CLOSE_AND_DELETE_TASK_WAIT_MS);
         }

         m_oSemaphoreTable[u32SemId].bBeingDeleted = FALSE;
      }
      else
      {
         OSAL_s32SemaphorePost (m_hTableSemaphore);
      }

   }
}

void createThread(void *arg)
{
   (void) arg;  // not necessary in this function

   while (1)   /*lint !e716     Yes, this is an endless loop... */
   {
      tU32 u32SemId;
      vShuffleThreadPrio2();

      u32SemId = u32RandomSemaphore();
      if (m_oSemaphoreTable[u32SemId].hSem == OSAL_C_INVALID_HANDLE)
      {
         OSAL_s32SemaphoreCreate (m_oSemaphoreTable[u32SemId].szName, &m_oSemaphoreTable[u32SemId].hSem, 0);         
      }

      vWaitRandom2(CREATE_TASK_WAIT_MS);
   }
}


void vStartThreads (void (*threadFct) (void *), const tC8* name, OSAL_trThreadAttribute attributes[], tU32 u32NoOfThreads)
{
   tU32 i;
   for (i=0; i<u32NoOfThreads; ++i)
   {
      char buf[256] = {0};
      OSAL_s32PrintFormat(buf, "%s_%d", name, i);

      attributes[i].szName = buf;
      attributes[i].u32Priority = 200;
      attributes[i].s32StackSize = 4096;
      attributes[i].pfEntry = threadFct;
      attributes[i].pvArg = NULL;

      (void) OSAL_ThreadSpawn(&attributes[i]);
   }
}


tU32 u32OEDT_OSAL_Semaphore_StressTest( void )
{
   int i;

   OSAL_vRandomSeed(42);

   OSAL_s32SemaphoreCreate ("TableSem", &m_hTableSemaphore, 1);

   /*
    *  Semaphore Table initialisieren
    */
   for(i=0; i<NO_OF_SEMAPHORES; ++i)
   {
      tS32 retVal = OSAL_ERROR;
      char *buf = malloc(256);
      if (buf != NULL)
      {
         memset (buf,0,256);

         OSAL_s32PrintFormat(buf, "TSem_%d", i);
         retVal = OSAL_s32SemaphoreCreate (buf, &m_oSemaphoreTable[i].hSem, 0);
         m_oSemaphoreTable[i].szName = buf;
         m_oSemaphoreTable[i].bBeingDeleted = FALSE;
      }
      OS_ASSERT (retVal == OSAL_OK);
   }

   /*
    *  Tasks initialisieren
    */
   vStartThreads(postThread, "PostT", m_hPostTasks, NO_OF_POST_TASKS);
   vStartThreads(waitThread, "WaitT", m_hWaitTasks, NO_OF_WAIT_TASKS);
   vStartThreads(closeThread, "ClosT", m_hCloseTasks, NO_OF_CLOSE_TASKS);
   vStartThreads(createThread, "CreatT", m_hCreateTasks, NO_OF_CREATE_TASKS);

   OSAL_s32ThreadWait(OSAL_C_TIMEOUT_FOREVER);

   
   OSAL_s32SemaphoreClose(m_hTableSemaphore);
   OSAL_s32SemaphoreDelete("TableSem");

   return 0;
}
