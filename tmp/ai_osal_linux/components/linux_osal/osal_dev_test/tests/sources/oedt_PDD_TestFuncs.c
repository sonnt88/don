/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file implements the individual test cases ( OEDT testcases)
 *				       for PDD
 * @addtogroup   PDD unit test
 * @{
 */

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 
#define SYSTEM_S_IMPORT_INTERFACE_KDS_DEF
#include "system_pif.h"
#include "oedt_PDD_TestFuncs.h"
#include "oedt_helper_funcs.h"
#include "pdd.h" 
#include "pdd_trace.h"
#include "pdd_config_nor_user.h" 
#include "pdd_variant_gen.h"
#include "DataPoolSCC.h"
#ifdef PDD_NOR_KERNEL_POOL_EXIST
#include "DataPoolKernel.h"
#endif

/*********************************************************************************/
/*                         define                                                */
/*********************************************************************************/
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
#define PDD_DP_TEST_LOCATION_V850            PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
#endif
#ifdef PDD_NOR_KERNEL_POOL_EXIST
#define PDD_DP_TEST_LOCATION_NOR_KERNEL      "DpPddKernel"
#endif

#define PDD_COMMAND    3
#define PDD_ARGUMENT   4

#define PDD_COMMAND_MAX_LEN   200
#define PDD_DUMP_FILE                        "/var/opt/bosch/dynamic/pdd/datapool/testdump/PddNorUserDump.dat"
#define PDD_DUMP_FILE_PATH_CHECKSUM          "/var/opt/bosch/dynamic/pdd/datapool/testdump/PddNorUserDumpChecksumError.dat"
#define PDD_DUMP_FILE_CHECKSUM_ERROR         "testdump/PddNorUserDumpChecksumError"
#define PDD_DUMP_FILE_PATH_OK                "/var/opt/bosch/dynamic/pdd/datapool/testdump/PddNorUserDumpOK.dat"
#define PDD_DUMP_FILE_OK                     "testdump/PddNorUserDumpOK"
#define PDD_DUMP_FILE_PATH_MAGIC_WRONG       "/var/opt/bosch/dynamic/pdd/datapool/testdump/PddNorUserDumpMagicWrong.dat"
#define PDD_DUMP_FILE_MAGIC_WRONG            "testdump/PddNorUserDumpMagicWrong"
#define PDD_DUMP_FILE_PATH_WRITE_INTERRUPT   "/var/opt/bosch/dynamic/pdd/datapool/testdump/PddNorUserDumpWriteInterrupt.dat"
#define PDD_DUMP_FILE_WRITE_INTERRUPT        "testdump/PddNorUserDumpWriteInterrupt"


#define PDD_NOR_USER_MIN_SECTOR_SIZE         0x20000
#define PDD_NOR_USER_CLUSTER_SIZE            0x8000  
#define PDD_NOR_USER_SECTOR_NUM              2    
#define PDD_NOR_USER_CLUSTER_NUM             (tU8) ((PDD_NOR_USER_MIN_SECTOR_SIZE*PDD_NOR_USER_SECTOR_NUM)/PDD_NOR_USER_CLUSTER_SIZE)
#define PDD_NOR_USER_MIN_CHUNK_SIZE          0x20
#define PDD_NOR_USER_HEADERSIZE              (8*PDD_NOR_USER_MIN_CHUNK_SIZE)
#define KDS_C_S32_IO_VERSION                  0x00000206

#define PRIORITY             100
#define INVALID_PID          ((tS32)-100)
/*********************************************************************************/
/*                         macro                                                 */
/*********************************************************************************/

/*********************************************************************************/
/*                         static variable                                       */
/*********************************************************************************/
tU32    vu32PDD_SccVersion=1;  //default version
tBool   vbGetVersion=FALSE;

/*********************************************************************************/
/*                         static function                                       */
/*********************************************************************************/

/******************************************************************************
* FUNCTION: tS32 s32PddStartProcessIncOutAndSendData()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created  2014 08 1
*****************************************************************************/
static tS32 s32PddStartProcessIncOutAndSendData(tU8* Vu8pVersion)
{
  tS32 Vs32ReturnCode=0;
  tU8  VubBuffer[100];
  snprintf(VubBuffer,100,"-p 50948 -b 50948 -r scc 02-44-01-00-08-%d-%d-%d-%d-ff-ff-ff-ff",Vu8pVersion[0],Vu8pVersion[1],Vu8pVersion[2],Vu8pVersion[3]);
  OSAL_tProcessID            VprID = INVALID_PID;
 
 // fprintf(stderr, "s32PddStartProcessIncOutAndSendData %s\n", VubBuffer);
  OSAL_trProcessAttribute	 VprAtr 	  = {
						       "inc_send_out.out",                                               /*szName*/
						       PRIORITY,						                                             /*u32Priority*/								 	
						       "/opt/bosch/base/bin/inc_send_out.out",		                       /*szAppName*/
						       (tString) VubBuffer,                                              /*szCommandLine*/
						       0                                                                 /*szCGroup_path*/
						    };

  VprID = OSAL_ProcessSpawn(&VprAtr);
  if (OSAL_ERROR == VprID)
  {
    Vs32ReturnCode=-1;
  }
  else
  {
    sleep(5);
  }
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tU32 s32PddGetVersionV850Pool()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: version of the pool
*
* HISTORY:Created 2016 02 122
*****************************************************************************/
static tU32 s32PddGetVersionV850Pool(void)
{
  tU32 Vu32Version = 1; //default version 
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
  TDataPoolScc_PddDpTest  VStructDataWrite;
  tS32 Vs32ReturnCode=0;
  if(vbGetVersion==FALSE)
  { 
    memset(&VStructDataWrite.PddDpTestElement1.PddDpTestElement1[0],0x1,4);
    for(Vu32Version==1;Vu32Version<0xffffffff;Vu32Version++)
    {
	    Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
      if(Vs32ReturnCode>=PDD_OK)
        break;
    }
    vu32PDD_SccVersion=Vu32Version;
    vbGetVersion=TRUE;
  }
  else
  {
    Vu32Version=vu32PDD_SccVersion;
  }
  #endif
  return(Vu32Version);
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserReadEarlyBufferGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  14.10.2015
******************************************************************************/
tU32 u32PDDNorUserReadEarlyBufferGreat(void)
{
  tU32 Vu32Ret = 0;
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU32 Vu32Value=0x12345678;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret=10;
  }
  else
  {//read datastream with great buffer
    char VubBuffer[20];
    tU8  Vu8Info=0;
    Vs32ReturnCode=pdd_read_datastream_early_from_nor("PddDpTestLocationRawNor1",(tU8*)&VubBuffer,sizeof(VubBuffer),Vu32Version,&Vu8Info);
    //check size
    if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret=20;
    }
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserReadEarlyBufferSmall()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  14.10.2015
******************************************************************************/
tU32 u32PDDNorUserReadEarlyBufferSmall(void)
{
  tU32 Vu32Ret = 0;
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU32 Vu32Value=0x12345678;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret=10;
  }
  else
  {//read datastream with smaller buffer
    char VubBuffer;
    tU8  Vu8Info=0;
    Vs32ReturnCode=pdd_read_datastream_early_from_nor("PddDpTestLocationRawNor1",(tU8*)&VubBuffer,sizeof(VubBuffer),Vu32Version,&Vu8Info);
    //check size
    if(Vs32ReturnCode>= PDD_OK)
    {
      Vu32Ret=20;
    }
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserReadEarlyDeleteStreamAndCompleteNor()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  14.10.2015
******************************************************************************/
tU32 u32PDDNorUserReadEarlyDelStreamAndNor(void)
{
  tU32 Vu32Ret = 0;
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU32 Vu32Value;
  tU32 Vu32Version=0x01;
  tU8  Vu8Info=0;
  //clear datastream
  memset(Vu8Buffer,0,PDD_COMMAND_MAX_LEN);
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_DELETE_DATASTREAM;
  Vu8Buffer[PDD_ARGUMENT]=PDD_LOCATION_NOR_USER;
  memcpy(&Vu8Buffer[PDD_ARGUMENT+sizeof(tePddLocation)],"PddDpTestLocationRawNor1",strlen("PddDpTestLocationRawNor1"));
  pdd_vTraceCommand(&Vu8Buffer[0]); 
  {//read stream
    tS32 Vs32ReturnCode=pdd_read_datastream_early_from_nor("PddDpTestLocationRawNor1",(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version,&Vu8Info);
    //check size
    if(Vs32ReturnCode>= PDD_OK)
    {
      Vu32Ret=20;
    }
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserReadWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDNorUserReadWrite(void)
{
  tU32 Vu32Ret = 0;
  tU32 Vu32Value=0x12345678;
  tU32 Vu32Version=0x01;

#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret=10;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret=20;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret=40;
	  }
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserReadSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDNorUserReadSize(void)
{
  tU32 Vu32Ret = 0;
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tS32 Vs32ReturnCode=pdd_get_data_stream_size("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER);
  if(Vs32ReturnCode<=0)
  {	  
    Vu32Ret=10;
  }
#endif
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteLengthToGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDNorUserWriteLengthToGreat(void)
{
  #define PDD_CLUSTER_SIZE 0x8000
  tU32 Vu32Ret = 0;
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU8* VubBuffer= malloc(PDD_CLUSTER_SIZE); //cluster size
  tU32 Vu32Version=0x01;
  tU32 Vu32Size=0x7000;
  tS32 Vs32ReturnCode;
  if(VubBuffer==NULL)
  {
     Vu32Ret=1;
  }
  else
  {
    memset(VubBuffer,0x5a,PDD_CLUSTER_SIZE);
    do
    {
      Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,VubBuffer,Vu32Size,Vu32Version);
	    if(Vs32ReturnCode >= 0)
	    {
	      Vu32Size+=100;    
	      usleep(1000);
	    }
    }while((Vu32Size<PDD_CLUSTER_SIZE)&&(Vs32ReturnCode>=0));
    if(Vs32ReturnCode>=0)
    {
      Vu32Ret=10;
    }
    else
    {
      Vu32Size-=100;
      do
      {
        Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,VubBuffer,Vu32Size,Vu32Version);
	      if(Vs32ReturnCode >= 0)
	      {
	        Vu32Size+=10;    
	        usleep(1000);
	      }
      }while((Vu32Size<PDD_CLUSTER_SIZE)&&(Vs32ReturnCode>=0));
	    if(Vs32ReturnCode>=0)
	    {
        Vu32Ret=20;
      }
      else
      {
        Vu32Size-=10;
        do
        {
	        Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,VubBuffer,Vu32Size,Vu32Version);
	        if(Vs32ReturnCode >= 0)
	        { 
	          Vu32Size+=1;    
	          usleep(1000);
	        }
        }while((Vu32Size<PDD_CLUSTER_SIZE)&&(Vs32ReturnCode>=0));
	      if(Vs32ReturnCode>=0)
	      {
          Vu32Ret=30;
        }
	    }
    }
    /*clear raw nor*/
    Vu32Size=10;
    Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,VubBuffer,Vu32Size,Vu32Version);
    free(VubBuffer);
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteReadMorePools()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDNorUserWriteReadMorePools(void)
{
  tU32 Vu32Ret = 0;
  tU32 Vu32Value=0x12345678;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode;

  /*********************************PddDpTestLocationRawNor1 *********************************************/
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  Vu32Value=0x12345678;
  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret=10;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret=20;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret=40;
	  }
	  /*set to zero for the next test*/
	  Vu32Value=0;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
    if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
    {
      Vu32Ret=80;
    }
  }
#endif
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR2
  /*********************************PddDpTestLocationRawNor2 *********************************************/
  Vu32Value=0x12345678;
  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor2",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret|=1000;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor2",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret|=2000;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret|=4000;
  	}
	  /*set to zero for the next test*/
	  Vu32Value=0;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor2",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
    if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
    {
      Vu32Ret|=8000;
    }
  }
#endif
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR3
  /*********************************PddDpTestLocationRawNor3 *********************************************/
  Vu32Value=0x12345678;
  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor3",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret|=10000;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor3",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret|=20000;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret|=40000;
	  }
	  /*set to zero for the next test*/
	  Vu32Value=0;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor3",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
    if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
    {
      Vu32Ret|=80000;
    }
  }
#endif
  /*********************************PddDpTestLocationRawNor4 *********************************************/
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR4
  Vu32Value=0x12345678;
  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor4",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret|=100000;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor4",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret|=200000;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret|=400000;
	  }
	  /*set to zero for the next test*/
	  Vu32Value=0;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor4",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
    if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
    {
      Vu32Ret|=800000;
    }
  }
#endif
  /*********************************PddDpTestLocationRawNor5 *********************************************/
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR5
  Vu32Value=0x12345678;
  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor5",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
  if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
  {
    Vu32Ret|=1;
  }
  else
  {/* read datastream*/
    tU32 Vu32ValueRead;
    Vs32ReturnCode=pdd_read_data_stream("PddDpTestLocationRawNor5",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(Vu32ValueRead),Vu32Version);
	  if(Vs32ReturnCode!=sizeof(Vu32Value))
    {
      Vu32Ret|=2;
    }
	  else if(Vu32ValueRead!=Vu32Value)
	  {
	    Vu32Ret|=4;
	  }
	  /*set to zero for the next test*/
	  Vu32Value=0;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor5",PDD_LOCATION_NOR_USER,(tU8*)&Vu32Value,sizeof(Vu32Value),Vu32Version);
    if((Vs32ReturnCode!=sizeof(Vu32Value))&&(Vs32ReturnCode!=0))
    {
      Vu32Ret|=8;
    }
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserCheckEqualCluster()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserCheckEqualCluster(void)
{
  tU32 Vu32Ret = 0;
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8LastCluster=0xff;
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
  /*get actuel cluster*/
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
  pdd_vTraceCommand(&Vu8Buffer[0]);
  Vu8LastCluster=Vu8Buffer[PDD_ARGUMENT];
  if(Vu8LastCluster<PDD_NOR_USER_CLUSTER_NUM)
  {/*data saved => dump partition*/
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE;
    pdd_vTraceCommand(&Vu8Buffer[0]);
	  /*open file */
	  intptr_t ViFile = open(PDD_DUMP_FILE, O_RDONLY);
	  /* check file handle */
    if(ViFile < 0)
    { /*  error*/
      Vu32Ret|=10;
    }
	  else
	  {
      struct stat VFileStat;
      if(fstat(ViFile,&VFileStat) < 0)
	    { /* error*/
          Vu32Ret|=20;
	    }
	    else
	    { /* create buffer*/
	      void* VvpBuffer=malloc(VFileStat.st_size);
		    if(VvpBuffer==NULL)
		    {
		      Vu32Ret|=40;
		    }
		    else
	      {/*read file */
		      tS32 Vs32Result = read(ViFile,VvpBuffer,VFileStat.st_size);		
		      if(Vs32Result<0)
		      {
		        Vu32Ret|=80;
		      }
		      else
		      {
		       tU8   Vu8Inc;
			     tU8*  Vu8pBufferCmp1=(tU8*)VvpBuffer;
			     tU8*  Vu8pBufferCmp2=(tU8*)VvpBuffer;
			     Vu8pBufferCmp2+=PDD_NOR_USER_CLUSTER_SIZE;
			     for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CLUSTER_NUM;Vu8Inc++)
			     { /*check if cluster formated*/
			       if((*Vu8pBufferCmp1==0xAD)&&(*Vu8pBufferCmp2==0xAD))
			       { /*check if cluster used*/
			         if((*(Vu8pBufferCmp1+PDD_NOR_USER_HEADERSIZE)!=0xff)&&((*(Vu8pBufferCmp2+PDD_NOR_USER_HEADERSIZE)!=0xff)))
				       {
                 if(memcmp(Vu8pBufferCmp1+PDD_NOR_USER_HEADERSIZE,Vu8pBufferCmp2+PDD_NOR_USER_HEADERSIZE,PDD_NOR_USER_CLUSTER_SIZE-PDD_NOR_USER_HEADERSIZE)==0)
				         {
				           Vu32Ret|=Vu8Inc; 
				           break;
				         }
				       }
			       }
			       Vu8pBufferCmp1+=PDD_NOR_USER_CLUSTER_SIZE;
			       Vu8pBufferCmp2+=PDD_NOR_USER_CLUSTER_SIZE;
			       if(Vu8Inc==PDD_NOR_USER_CLUSTER_NUM-1)
			       {
			         Vu8pBufferCmp2=(tU8*)VvpBuffer;
			       }
           }/*end for*/
          }
		      free(VvpBuffer);
        }/*end else*/
      }/*end else*/
    }/*end else*/
	  if (close(ViFile)<0) 
	  {
	    Vu32Ret|=100;
	  }
  }
  else
  {
    Vu32Ret|=1000;
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserLoadDumpOK()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserLoadDumpOK(void)
{
  tU32 Vu32Ret = 0;
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8ClusterLast=4;
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));

  /*check if file exist:open file */
  intptr_t ViFile = open(PDD_DUMP_FILE_PATH_OK, O_RDONLY);
  /* check file handle */
  if(ViFile >= 0)
  { /*close file*/
	  close(ViFile);
	  /*write dump to */
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR;
    memcpy(&Vu8Buffer[PDD_ARGUMENT],PDD_DUMP_FILE_OK,strlen(PDD_DUMP_FILE_OK));
    pdd_vTraceCommand(&Vu8Buffer[0]);
    /*check if last valid cluster used */
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
    pdd_vTraceCommand(&Vu8Buffer[0]);
#ifdef PDD_NOR_USER_DATASTREAM_NAME_KDS
	  //read datastream
	  tS32 Vs32Size=pdd_read_data_stream(PDD_NOR_USER_DATASTREAM_NAME_KDS,PDD_LOCATION_NOR_USER,Vu8Buffer,sizeof(Vu8Buffer),KDS_C_S32_IO_VERSION);
    if(Vs32Size!=13)
	  {
	    Vu32Ret=Vs32Size;
	  }
	  else
	  { /*check if last valid cluster used */
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      /*check*/
      if(Vu8Buffer[PDD_ARGUMENT]<Vu8ClusterLast)
      {
        Vu32Ret=Vu8Buffer[PDD_ARGUMENT];
      }
    }
#endif
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserLoadDumpMagicWrong()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserLoadDumpMagicWrong(void)
{
  tU32 Vu32Ret = 0;
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8ClusterLast=4;
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));

  /*check if file exist:open file */
  intptr_t ViFile = open(PDD_DUMP_FILE_PATH_MAGIC_WRONG, O_RDONLY);
  /* check file handle */
  if(ViFile >= 0)
  { /*close file*/
	  close(ViFile);
	  /*write dump to */
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR;
    memcpy(&Vu8Buffer[PDD_ARGUMENT],PDD_DUMP_FILE_MAGIC_WRONG,strlen(PDD_DUMP_FILE_MAGIC_WRONG));
    pdd_vTraceCommand(&Vu8Buffer[0]);
#ifdef PDD_NOR_USER_DATASTREAM_NAME_KDS
    tU8   Vu8Info;
	  //read datastream
	  tS32 Vs32Size=pdd_read_datastream(PDD_NOR_USER_DATASTREAM_NAME_KDS,PDD_LOCATION_NOR_USER,Vu8Buffer,sizeof(Vu8Buffer),KDS_C_S32_IO_VERSION,&Vu8Info);
    if((Vs32Size!=13)&&(Vu8Info==PDD_READ_INFO_NORMAL_FILE))
	  {
	    Vu32Ret=Vs32Size;
	  }
	  else
	  { /*check if last valid cluster used */
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      /*check*/
      if((Vu8ClusterLast==Vu8Buffer[PDD_ARGUMENT])||(Vu8Buffer[PDD_ARGUMENT]>7))
      {
        Vu32Ret=Vu8Buffer[PDD_ARGUMENT];
      }
    }
#endif
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserLoadDumpWriteInterrupt()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserLoadDumpWriteInterrupt(void)
{
  tU32 Vu32Ret = 0;
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8ClusterLast=4;
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));

  /*check if file exist:open file */
  intptr_t ViFile = open(PDD_DUMP_FILE_PATH_WRITE_INTERRUPT, O_RDONLY);
  /* check file handle */
  if(ViFile >= 0)
  { /*close file*/
	close(ViFile);
	/*write dump to */
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR;
    memcpy(&Vu8Buffer[PDD_ARGUMENT],PDD_DUMP_FILE_WRITE_INTERRUPT,strlen(PDD_DUMP_FILE_WRITE_INTERRUPT));
    pdd_vTraceCommand(&Vu8Buffer[0]);
 #ifdef PDD_NOR_USER_DATASTREAM_NAME_KDS
	//read datastream
	tS32 Vs32Size=pdd_read_data_stream(PDD_NOR_USER_DATASTREAM_NAME_KDS,PDD_LOCATION_NOR_USER,Vu8Buffer,sizeof(Vu8Buffer),KDS_C_S32_IO_VERSION);
    if(Vs32Size!=13)
	{
	  Vu32Ret=Vs32Size;
	}
	else
	{ /*check if last valid cluster used */
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      /*check*/
      if((Vu8ClusterLast==Vu8Buffer[PDD_ARGUMENT])||(Vu8Buffer[PDD_ARGUMENT]>7))
      {
        Vu32Ret=Vu8Buffer[PDD_ARGUMENT];
      }
	}
#endif
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserLoadDumpChecksumWrong()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserLoadDumpChecksumWrong(void)
{
  tU32 Vu32Ret = 0;
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8ClusterError=4;
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));

  /*check if file exist:open file */
  intptr_t ViFile = open(PDD_DUMP_FILE_PATH_CHECKSUM, O_RDONLY);
  /* check file handle */
  if(ViFile >= 0)
  { /*close file*/
	  close(ViFile);
	  /*write dump to */
    Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR;
    memcpy(&Vu8Buffer[PDD_ARGUMENT],PDD_DUMP_FILE_CHECKSUM_ERROR,strlen(PDD_DUMP_FILE_CHECKSUM_ERROR));
    pdd_vTraceCommand(&Vu8Buffer[0]);
#ifdef PDD_NOR_USER_DATASTREAM_NAME_KDS
	  //read datastream
    tU8   Vu8Info;
	  tS32 Vs32Size=pdd_read_datastream(PDD_NOR_USER_DATASTREAM_NAME_KDS,PDD_LOCATION_NOR_USER,Vu8Buffer,sizeof(Vu8Buffer),KDS_C_S32_IO_VERSION,&Vu8Info);
    if((Vs32Size!=13)&&(Vu8Info==PDD_READ_INFO_NORMAL_FILE))
	  {
	    Vu32Ret=Vs32Size;
	  }
	  else
	  { /*check if last valid cluster used */
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      /*check*/
      if((Vu8ClusterError==Vu8Buffer[PDD_ARGUMENT])||(Vu8Buffer[PDD_ARGUMENT]>7))
      {
        Vu32Ret=Vu8Buffer[PDD_ARGUMENT];
      }
    }
#endif
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteTwoStreams()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.05.2014
******************************************************************************/
tU32 u32PDDNorUserWriteTwoStreams(void)
{
  tU32 Vu32Ret = 0;
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
  tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
  tU8  Vu8Cluster=2;  //cluster after first write
  memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
  /*save trace level => precondition backup should be exist */
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_SET_TRACE_LEVEL;
  Vu8Buffer[PDD_ARGUMENT]='2';
  pdd_vTraceCommand(&Vu8Buffer[0]);
  /*erase flash */
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_ERASE;
  pdd_vTraceCommand(&Vu8Buffer[0]);
  /*save trace level */
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_SET_TRACE_LEVEL;
  Vu8Buffer[PDD_ARGUMENT]='f';
  pdd_vTraceCommand(&Vu8Buffer[0]);
  /*get actual cluster */
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
  pdd_vTraceCommand(&Vu8Buffer[0]);
  /*check*/
  if(Vu8Buffer[PDD_ARGUMENT]<Vu8Cluster)
  {
    Vu32Ret=Vu8Buffer[PDD_ARGUMENT];
  }
  else
  {/*write kds entry*/
    tU8 Vu8KdsStream[32]={0xAB,0x0F,0x00,0xA0,0x18,0x00,0x00,0x00,
	                      0x50,0x4c,0x41,0x54,0x46,0x4f,0x52,0x4d,
	      				  0x5f,0x58,0x58,0x58,0x58,0x00,0x14,0x20,
	                      0x07,0x00,0xCC,0xAF,0xED,0x76,0x00,0x00};
	tS32 Vs32ReturnCode=pdd_write_data_stream("kds_data",PDD_LOCATION_NOR_USER,(tU8*)&Vu8KdsStream,sizeof(Vu8KdsStream),KDS_C_S32_IO_VERSION);
	if(Vs32ReturnCode<0)
	{
	  Vu32Ret=100;
	}
	else
	{/*dump file*/
	  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE;
      pdd_vTraceCommand(&Vu8Buffer[0]);
	  /*get actual cluster */
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      /*check*/
	  Vu8Cluster++;
      if(Vu8Buffer[PDD_ARGUMENT]<Vu8Cluster)
      {
        Vu32Ret=Vu8Buffer[PDD_ARGUMENT]+10;
      }
	  else
	  { /*read dump*//*open file */
	    intptr_t ViFile = open(PDD_DUMP_FILE, O_RDONLY);
	    /* check file handle */
        if(ViFile < 0)
        { /*  error*/
          Vu32Ret|=200;
        }
	    else
	    {
          struct stat VFileStat;
          if(fstat(ViFile,&VFileStat) < 0)
	      { /* error*/
            Vu32Ret|=400;
	      }
	      else
	      { /* create buffer*/
	        void* VvpBuffer=malloc(VFileStat.st_size);
		    if(VvpBuffer==NULL)
		    {
		      Vu32Ret|=600;
		    }
		    else
	        {/*read file */
		      tS32 Vs32Result = read(ViFile,VvpBuffer,VFileStat.st_size);		
		      if(Vs32Result<0)
		      {
		        Vu32Ret|=800;
		      }
		      else
		      {
			    tU32 Vu32Level;
			    if(pdd_helper_get_element_from_stream("TraceLevelPDD",VvpBuffer,Vs32Result,&Vu32Level,sizeof(tU32),0)!=0)
				{/*elemete string not found in buffer*/
				   Vu32Ret|=1000;
				}
			  }
			  free(VvpBuffer);
			}			
		  }
		}  
		close(ViFile);
	  }
	}
  }
  /*clear trace level*/
  Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_CLEAR_TRACE_LEVEL;
  pdd_vTraceCommand(&Vu8Buffer[0]);
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDSccReadDataSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDSccReadDataSize(void)
{
  tU32 Vu32Ret = 0;
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
  tS32 Vs32ReturnCode=pdd_get_data_stream_size(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC);
  if(Vs32ReturnCode!=sizeof(TDataPoolScc_PddDpTest))
  {
    Vu32Ret=10;
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDReadWriteDPTrTestScc()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDSccReadWrite(void)
{
  tU32					  Vu32Ret = 0;
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST 
  tU32					          Vu32Version=s32PddGetVersionV850Pool();
  TDataPoolScc_PddDpTest  VStructData;
  TDataPoolScc_PddDpTest  VStructDataRead;
  tS32                    Vs32ReturnCode;
  tU8                     Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(&VStructData,0,sizeof(VStructData));
  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructData,sizeof(VStructData),Vu32Version);
  if(Vs32ReturnCode==0)
  {/*stream identical no error write new value*/
    memset(&VStructData.PddDpTestElement1.PddDpTestElement1[0],0x1,4);
	  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructData,sizeof(VStructData),Vu32Version);
  }
  if(Vs32ReturnCode!=sizeof(VStructData))
  {
    Vu32Ret|=20;
  }
  else
  {/* read datastream*/
    Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
	  if(Vs32ReturnCode!=sizeof(VStructDataRead))
    {
      Vu32Ret|=30;
    }
	  else if(memcmp(&VStructDataRead,&VStructData,Vs32ReturnCode)!=0)
	  {
	    Vu32Ret|=40;
	  }
	  else if(Vu8Info!=PDD_READ_INFO_NORMAL_FILE)
	  {
	    Vu32Ret|=80;
	  }
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDReadDPTrTestSccLengthToGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDSccReadLengthToGreat(void)
{
  tU32 Vu32Ret = 0;  
  tS32 Vs32Lenght=200;

#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST 
  tU32 Vu32Version=s32PddGetVersionV850Pool();
  /* read datastream*/
  tU8* Vu8pBuffer=malloc(Vs32Lenght);
  tS32 Vs32ReturnCode=pdd_read_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,Vu8pBuffer,Vs32Lenght,Vu32Version);
  if(Vs32ReturnCode==Vs32Lenght)
  {
    Vu32Ret=20;
  }
  free(Vu8pBuffer);
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDWriteDPTrTestSccLengthToGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  06.08.2013
******************************************************************************/
tU32 u32PDDSccWriteLengthToGreat(void)
{
  tU32 Vu32Ret = 0;  
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST 
  tU32 Vu32Version=s32PddGetVersionV850Pool();
  tS32 Vs32Lenght=200;

  /* read datastream*/
  tU8* Vu8pBuffer=malloc(Vs32Lenght);
  if(Vu8pBuffer==NULL)
  {
    Vu32Ret=10;
  }
  else
  {
    memset(Vu8pBuffer,0xa5,Vs32Lenght);
    tS32 Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,Vu8pBuffer,Vs32Lenght,Vu32Version);
    if(Vs32ReturnCode>=0)
    {
      Vu32Ret=20;
    }
    /*restore old length*/
    Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,Vu8pBuffer,sizeof(TDataPoolScc_PddDpTest),Vu32Version);
    if(Vs32ReturnCode<0)
    {
      Vu32Ret=40;
    }
    free(Vu8pBuffer);
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  17.01.2014
******************************************************************************/
tU32 u32PDDNorKernelWrite(void)
{
  tU32                          Vu32Ret = 0;  

  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  TDataPoolKernel_DpPddKernel   VsDpPddKernel;
  tS32                          Vs32ReturnCode;
  tU32                          Vu32Version=DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL;
  memset(&VsDpPddKernel,0x00,sizeof(TDataPoolKernel_DpPddKernel));
  memset(&VsDpPddKernel.DisplayTiming.DisplayTiming[0],0x5a,sizeof(TDataPoolKernel_DisplayTiming));
  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
  if(Vs32ReturnCode<0)
  {
    Vu32Ret=20;
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelReadDataSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  17.01.2014
******************************************************************************/
tU32 u32PDDNorKernelReadDataSize(void)
{
  tU32 Vu32Ret = 0;  
  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  tS32 Vs32ReturnCode;
  Vs32ReturnCode=pdd_get_data_stream_size(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL);
  if(Vs32ReturnCode!=sizeof(TDataPoolKernel_DpPddKernel))
  {
    Vu32Ret=20;
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelReadWrite()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  17.01.2014
******************************************************************************/
tU32 u32PDDNorKernelReadWrite(void)
{
  tU32 Vu32Ret = 0;  
  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  TDataPoolKernel_DpPddKernel   VsDpPddKernel;
  tS32                          Vs32ReturnCode;
  tU32                          Vu32Version=DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL;
 
  /*read data*/
  Vs32ReturnCode=pdd_read_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
  if(Vs32ReturnCode!=sizeof(TDataPoolKernel_DpPddKernel))
  {
    Vu32Ret=20;
  }
  /*change data*/
  VsDpPddKernel.DisplayTiming.DisplayTiming[0]+=1;
  VsDpPddKernel.SynergyDriver.SynergyDriver+=1;
  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
  if(Vs32ReturnCode!=sizeof(TDataPoolKernel_DpPddKernel))
  {
    Vu32Ret=40;
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelWriteReadCompare()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  17.01.2014
******************************************************************************/
tU32 u32PDDNorKernelWriteReadCompare(void)
{
  tU32 Vu32Ret = 0;  
  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  TDataPoolKernel_DpPddKernel   VsDpPddKernelWrite;
  TDataPoolKernel_DpPddKernel   VsDpPddKernelRead;
  tS32                          Vs32ReturnCode;
  tU32                          Vu32Version=DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL;
 
  memset(&VsDpPddKernelWrite,0x00,sizeof(TDataPoolKernel_DpPddKernel));
  memset(&VsDpPddKernelWrite.DisplayTiming.DisplayTiming[0],0xf0,sizeof(TDataPoolKernel_DisplayTiming));
  VsDpPddKernelWrite.SynergyDriver.SynergyDriver=0x1234;
  /*write data*/
  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernelWrite,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
  if(Vs32ReturnCode!=sizeof(TDataPoolKernel_DpPddKernel))
  {
    Vu32Ret=10;
  }
  else
  { /*read data*/
    Vs32ReturnCode=pdd_read_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernelRead,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
    if(Vs32ReturnCode!=sizeof(TDataPoolKernel_DpPddKernel))
    {
      Vu32Ret=20;
    }
	else
	{/*compare data*/
	  if(VsDpPddKernelWrite.SynergyDriver.SynergyDriver!=VsDpPddKernelRead.SynergyDriver.SynergyDriver)
	  {
	    Vu32Ret=40;
	  }
	  if(memcmp(&VsDpPddKernelWrite.DisplayTiming.DisplayTiming[0],&VsDpPddKernelRead.DisplayTiming.DisplayTiming[0],sizeof(TDataPoolKernel_DisplayTiming))!=0)
	  {
	    Vu32Ret|=80;
	  }
	}
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelReadWrongVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  21.01.2014
******************************************************************************/
tU32 u32PDDNorKernelReadWrongVersion(void)
{
 tU32 Vu32Ret = 0;  
 #ifdef PDD_NOR_KERNEL_POOL_EXIST
 TDataPoolKernel_DpPddKernel   VsDpPddKernel;
 tS32                          Vs32ReturnCode;
 tU32                          Vu32Version=0xffff;
 
 /*read data*/
 Vs32ReturnCode=pdd_read_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,sizeof(TDataPoolKernel_DpPddKernel),Vu32Version);
 if(Vs32ReturnCode==sizeof(TDataPoolKernel_DpPddKernel))
 {
   Vu32Ret=20;
 }
 #endif
 return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelReadLengthToGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  21.01.2014
******************************************************************************/
tU32 u32PDDNorKernelReadLengthToGreat(void)
{
 tU32 Vu32Ret = 0;  
 #ifdef PDD_NOR_KERNEL_POOL_EXIST
 TDataPoolKernel_DpPddKernel   VsDpPddKernel;
 tS32                          Vs32ReturnCode;
 tU32                          Vu32Version=DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL;
 tS32                          Vs32Size=0x20001;//>128k 
 /*read data*/
 Vs32ReturnCode=pdd_read_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,Vs32Size,Vu32Version);
 if(Vs32ReturnCode==Vs32Size)
 {
   Vu32Ret=20;
 }
 #endif
 return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorKernelWriteLengthToGreat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  21.01.2014
******************************************************************************/
tU32 u32PDDNorKernelWriteLengthToGreat(void)
{
 tU32 Vu32Ret = 0;  
 #ifdef PDD_NOR_KERNEL_POOL_EXIST
 TDataPoolKernel_DpPddKernel   VsDpPddKernel;
 tS32                          Vs32ReturnCode;
 tU32                          Vu32Version=DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL;
 tS32                          Vs32Size=sizeof(TDataPoolKernel_DpPddKernel)+1;
 
 /*read data*/
 Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_NOR_KERNEL,PDD_LOCATION_NOR_KERNEL,(tU8*)&VsDpPddKernel,Vs32Size,Vu32Version);
 if(Vs32ReturnCode==Vs32Size)
 {
   Vu32Ret=20;
 }
 #endif
 return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDSccWriteCheckBackup()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDSccWriteCheckBackup(void)
{
  tU32 Vu32Ret = 0; 
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
  tU32					          Vu32Version=s32PddGetVersionV850Pool();
  TDataPoolScc_PddDpTest  VStructDataWrite;
  TDataPoolScc_PddDpTest  VStructDataRead;
  tS32                    Vs32ReturnCode;
  tU8                     Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(&VStructDataWrite,0,sizeof(VStructDataWrite));
  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
  if(Vs32ReturnCode==0)
  {/*stream identical no error write new value*/   
    memset(&VStructDataWrite.PddDpTestElement1.PddDpTestElement1[0],0x1,4);
	  Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
  }
  if(Vs32ReturnCode!=sizeof(VStructDataWrite))
  {
      Vu32Ret|=20;
  }
  else
  {/* read datastream*/
    Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
	  if((Vs32ReturnCode!=sizeof(VStructDataRead))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
    {
      Vu32Ret|=30;
    }
	  else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
	  {
	    Vu32Ret|=40;
	  }
	  else
	  {//change data in SCC without PDD => element invalid
      if(s32PddStartProcessIncOutAndSendData((tU8*)&Vu32Version)!=0)
	    {
	      Vu32Ret|=80;
	    }
	    else
	    {//read datastream should the backup datastream      
	      Vu8Info=PDD_READ_INFO_NORMAL_FILE;
	      Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
	      if(Vs32ReturnCode!=sizeof(VStructDataRead))
        {
          Vu32Ret|=100;
        }
	      else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
	      {
	        Vu32Ret|=200;
	      }
		    else
		    {
		      if(Vu8Info!=PDD_READ_INFO_BACKUP_FILE)
		      {
	          Vu32Ret|=400;
	        }
		    }
      }
    }
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDSccWriteDestroyCheckSync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDSccWriteCheckSync(void)
{
  tU32 Vu32Ret = 0; 
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
  tU32					          Vu32Version=s32PddGetVersionV850Pool();
  TDataPoolScc_PddDpTest  VStructDataWrite;
  TDataPoolScc_PddDpTest  VStructDataRead;
  tS32                    Vs32ReturnCode;
  tU8                     Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  //this test work only if the version defined match with the version in the V850
  if (Vu32Version==DATAPOOL_SCC_VERSION_POOL_PDDDPTEST)
  {
    memset(&VStructDataWrite,0,sizeof(VStructDataWrite));
    Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
    if(Vs32ReturnCode==0)
    {/*stream identical no error write new value*/
      memset(&VStructDataWrite.PddDpTestElement1.PddDpTestElement1[0],0x1,4);
      Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
    }
    if(Vs32ReturnCode!=sizeof(VStructDataWrite))
    {
      Vu32Ret|=20;
    }
    else
    {/* read datastream*/
      Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
      if((Vs32ReturnCode!=sizeof(VStructDataRead))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
      {
        Vu32Ret|=30;
      }
      else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
      {
        Vu32Ret|=40;
      }
      else
      { // sync 
	      pdd_sync_scc_streams();
	      //read datastream should the normal datastream
        Vu8Info=PDD_READ_INFO_NORMAL_FILE;
        Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
        if(Vs32ReturnCode!=sizeof(VStructDataRead))
        {
          Vu32Ret|=100;
        }
        else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
        {
          Vu32Ret|=200;
        }
	      else
	      { //this test works only if the version match with the pool
          if(Vu8Info!=PDD_READ_INFO_NORMAL_FILE)
	        {
            Vu32Ret|=400;
          }        
        }
      }
    }
  }
  #endif
  return Vu32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PDDSccWriteDestroyCheckSync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDSccWriteDestroyCheckSync(void)
{
  tU32 Vu32Ret = 0; 
  #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTEST
  tU32				        	  Vu32Version=s32PddGetVersionV850Pool();
  TDataPoolScc_PddDpTest  VStructDataWrite;
  TDataPoolScc_PddDpTest  VStructDataRead;
  tS32                    Vs32ReturnCode;
  tU8                     Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  if (Vu32Version==DATAPOOL_SCC_VERSION_POOL_PDDDPTEST)
  {
    memset(&VStructDataWrite,0,sizeof(VStructDataWrite));
    Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
    if(Vs32ReturnCode==0)
    {/*stream identical no error write new value*/
      memset(&VStructDataWrite.PddDpTestElement1.PddDpTestElement1[0],0x1,4);
	    Vs32ReturnCode=pdd_write_data_stream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataWrite,sizeof(VStructDataWrite),Vu32Version);
    }
    if(Vs32ReturnCode!=sizeof(VStructDataWrite))
    {
        Vu32Ret|=20;
    }
    else
    {/* read datastream*/
      Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
	    if((Vs32ReturnCode!=sizeof(VStructDataRead))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
      {
        Vu32Ret|=30;
      }
	    else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
	    {
	      Vu32Ret|=40;
	    }
	    else
	    {//change data in SCC without PDD => element invalid
        if(s32PddStartProcessIncOutAndSendData((tU8*)&Vu32Version)!=0)
	      {
	        Vu32Ret|=80;
	      }
	      else
	      {// sync 
		      pdd_sync_scc_streams();
		      //read datastream should the normal datastream
          Vu8Info=PDD_READ_INFO_NORMAL_FILE;
	        Vs32ReturnCode=pdd_read_datastream(PDD_DP_TEST_LOCATION_V850,PDD_LOCATION_SCC,(tU8*)&VStructDataRead,sizeof(VStructDataRead),Vu32Version,&Vu8Info);
	        if(Vs32ReturnCode!=sizeof(VStructDataRead))
          {
            Vu32Ret|=100;
          }
	        else if(memcmp(&VStructDataRead,&VStructDataWrite,Vs32ReturnCode)!=0)
	        {
	          Vu32Ret|=200;
	        }
		      else
		      {
		        if(Vu8Info!=PDD_READ_INFO_NORMAL_FILE)
		        {
	            Vu32Ret|=400;
	          }
          }
	      }
      }
    }
  }
  #endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteCheckBackup()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDNorUserWriteCheckBackup(void)
{
  tU32 Vu32Ret = 0; 
 
/*write data to NOR */
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;
  tU32 Vu32ValueWrite=0x12345678;
  tU32 Vu32ValueRead=0;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  if((Vs32ReturnCode==0))
  {
    Vu32ValueWrite=0x89abcdef;
	  Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  }
  if(Vs32ReturnCode!=sizeof(tU32))
  {
    Vu32Ret|=20;
  }
  else
  {/* read datastream*/
    Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
	  if((Vs32ReturnCode!=sizeof(tU32))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
    {
      Vu32Ret|=30;
    }
	  else if(memcmp(&Vu32ValueRead,&Vu32ValueWrite,Vs32ReturnCode)!=0)
	  {
	    Vu32Ret|=40;
	  }
	  else
	  {//erase NOR flash
	    tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
      memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
      Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_ERASE;
      pdd_vTraceCommand(&Vu8Buffer[0]);
      //read datastream should the backup datastream      
      Vu8Info=PDD_READ_INFO_NORMAL_FILE;
      Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
      if(Vs32ReturnCode!=sizeof(tU32))
      {
        Vu32Ret|=100;
      }
      else if(memcmp(&Vu32ValueWrite,&Vu32ValueRead,Vs32ReturnCode)!=0)
      {
        Vu32Ret|=200;
      }
      else
      {
        if(Vu8Info!=PDD_READ_INFO_BACKUP_FILE)
        {
          Vu32Ret|=400;
        }
      }
    }
  }
#endif
  return Vu32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteCheckSync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDNorUserWriteCheckSync(void)
{
   tU32 Vu32Ret = 0; 
 
/*write data to NOR */
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;
  tU32 Vu32ValueWrite=0x12345678;
  tU32 Vu32ValueRead=0;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  if((Vs32ReturnCode==0))
  {
    Vu32ValueWrite=0x89abcdef;
	Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  }
  if(Vs32ReturnCode!=sizeof(tU32))
  {
      Vu32Ret|=20;
  }
  else
  {/* read datastream*/
    Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
	if((Vs32ReturnCode!=sizeof(tU32))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
    {
      Vu32Ret|=30;
    }
	else if(memcmp(&Vu32ValueRead,&Vu32ValueWrite,Vs32ReturnCode)!=0)
	{
	  Vu32Ret|=40;
	}
	else
	{
     //sync
     pdd_sync_nor_user_streams();
	 //read datastream should the normal datastream      
	 Vu8Info=PDD_READ_INFO_NORMAL_FILE;
	 Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
	 if(Vs32ReturnCode!=sizeof(tU32))
     {
       Vu32Ret|=100;
     }
	 else if(memcmp(&Vu32ValueWrite,&Vu32ValueRead,Vs32ReturnCode)!=0)
	 {
	   Vu32Ret|=200;
	 }
	 else
	 {
	   if(Vu8Info!=PDD_READ_INFO_NORMAL_FILE)
	   {
	     Vu32Ret|=400;
	   }
	 }
    }
  }
#endif
  return Vu32Ret;
}

/*****************************************************************************
* FUNCTION:		u32PDDNorUserWriteEraseNorCheckSync()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  01.08.2014
******************************************************************************/
tU32 u32PDDNorUserWriteEraseNorCheckSync(void)
{
   tU32 Vu32Ret = 0; 
 
/*write data to NOR */
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDDPTESTLOCATIONRAWNOR1
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;
  tU32 Vu32ValueWrite=0x12345678;
  tU32 Vu32ValueRead=0;
  tU32 Vu32Version=0x01;
  tS32 Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  if((Vs32ReturnCode==0))
  {
    Vu32ValueWrite=0x89abcdef;
	Vs32ReturnCode=pdd_write_data_stream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueWrite,sizeof(tU32),Vu32Version);
  }
  if(Vs32ReturnCode!=sizeof(tU32))
  {
      Vu32Ret|=20;
  }
  else
  {/* read datastream*/
    Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
	if((Vs32ReturnCode!=sizeof(tU32))||(Vu8Info==PDD_READ_INFO_BACKUP_FILE))
    {
      Vu32Ret|=30;
    }
	else if(memcmp(&Vu32ValueRead,&Vu32ValueWrite,Vs32ReturnCode)!=0)
	{
	  Vu32Ret|=40;
	}
	else
	{//erase NOR flash
	 tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
         memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
         Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_ERASE;
	 pdd_vTraceCommand(&Vu8Buffer[0]);
         //sync
         pdd_sync_nor_user_streams();
	 //read datastream should the normal datastream      
	 Vu8Info=PDD_READ_INFO_NORMAL_FILE;
	 Vs32ReturnCode=pdd_read_datastream("PddDpTestLocationRawNor1",PDD_LOCATION_NOR_USER,(tU8*)&Vu32ValueRead,sizeof(tU32),Vu32Version,&Vu8Info);
	 if(Vs32ReturnCode!=sizeof(tU32))
         {
           Vu32Ret|=100;
         }
	 else if(memcmp(&Vu32ValueWrite,&Vu32ValueRead,Vs32ReturnCode)!=0)
	 {
	   Vu32Ret|=200;
	 }
	 else
	 {
	   if(Vu8Info!=PDD_READ_INFO_NORMAL_FILE)
	   {
	     Vu32Ret|=400;
	   }
	 }
    }
  }
#endif
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataFs()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataFs(void)
{
  tU32 Vu32Ret = 0; 
  tU8  Vu8Buffer[30];
  tU8  Vu8BufferRead[30];
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(Vu8Buffer,22,sizeof(Vu8Buffer));
  /* write data*/
  if(pdd_write_data_stream("2222/DeleteDataFS1",PDD_LOCATION_FS,Vu8Buffer,sizeof(Vu8Buffer),0x01)<0)
  {
    Vu32Ret=1;
  }
  else
  {/* read data*/
    if(pdd_read_datastream("2222/DeleteDataFS1",PDD_LOCATION_FS,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)<0)
	{
	   Vu32Ret=2;
	}
	else
	{/*compare data*/
	  if(memcmp(Vu8Buffer,Vu8BufferRead,sizeof(Vu8BufferRead))!=0)
	  {
	    Vu32Ret=3;
	  }
	  else
	  { /* delete data*/
	    if(pdd_delete_data_stream("2222/DeleteDataFS1",PDD_LOCATION_FS)<0)
		{
		  Vu32Ret=4;
		}
		else
		{ /* read data*/
          if(pdd_read_datastream("2222/DeleteDataFS1",PDD_LOCATION_FS,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)>=0)
	      {
	        Vu32Ret=5;
	      }
		}
	  }
	}
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataFs()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataFsSecure(void)
{
  tU32 Vu32Ret = 0; 
  tU8  Vu8Buffer[30];
  tU8  Vu8BufferRead[30];
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(Vu8Buffer,22,sizeof(Vu8Buffer));
  /* write data*/
  if(pdd_write_data_stream("2222/DeleteDataFSSecure1",PDD_LOCATION_FS_SECURE,Vu8Buffer,sizeof(Vu8Buffer),0x01)<0)
  {
    Vu32Ret=1;
  }
  else
  {/* read data*/
    if(pdd_read_datastream("2222/DeleteDataFSSecure1",PDD_LOCATION_FS_SECURE,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)<0)
	  {
	     Vu32Ret=2;
	  }
	  else
	  {/*compare data*/
	    if(memcmp(Vu8Buffer,Vu8BufferRead,sizeof(Vu8BufferRead))!=0)
	    {
	      Vu32Ret=3;
	    }
	    else
	    { /* delete data*/
	      if(pdd_delete_data_stream("2222/DeleteDataFSSecure1",PDD_LOCATION_FS_SECURE)<0)
		    {
		      Vu32Ret=4;
		    }
		    else
		    { /* read data*/
          if(pdd_read_datastream("2222/DeleteDataFSSecure1",PDD_LOCATION_FS_SECURE,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)>=0)
	        {
	          Vu32Ret=5;
	        }
		    }
	    }
	  }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataCheckParamString()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataCheckParamString(void)
{
  tU32 Vu32Ret = 0; 
  /*check parameter string*/
  if (pdd_delete_data_stream(NULL,PDD_LOCATION_FS_SECURE)!=PDD_ERROR_WRONG_PARAMETER)
  {
    Vu32Ret=1;
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataCheckParamLocation()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataCheckParamLocation(void)
{
  tU32 Vu32Ret = 0; 
   /*check parameter location PDD_LOCATION_NOR_USER */
  if (pdd_delete_data_stream("2222/DeleteDataFS",PDD_LOCATION_LAST)!=PDD_ERROR_WRONG_PARAMETER)
  {
    Vu32Ret|=1;
  }
  /*check parameter location PDD_LOCATION_NOR_USER */
  if (pdd_delete_data_stream("2222/DeleteDataFS",PDD_LOCATION_NOR_USER)!=PDD_ERROR_NOT_SUPPORTED)
  {
    Vu32Ret|=2;
  }
  /*check parameter location PDD_LOCATION_NOR_KERNEL */
  if (pdd_delete_data_stream("2222/DeleteDataFS",PDD_LOCATION_NOR_KERNEL)!=PDD_ERROR_NOT_SUPPORTED)
  {
    Vu32Ret|=4;
  }
  /*check parameter location PDD_LOCATION_SCC */
  if (pdd_delete_data_stream("2222/DeleteDataFS",PDD_LOCATION_SCC)!=PDD_ERROR_NOT_SUPPORTED)
  {
    Vu32Ret|=8;
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataTwice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataTwice(void)
{
  tU32 Vu32Ret = 0; 
  tU8  Vu8Buffer[30];
  tU8  Vu8BufferRead[30];
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(Vu8Buffer,44,sizeof(Vu8Buffer));
  /* write data*/
  if(pdd_write_data_stream("2222/DeleteDataFS2",PDD_LOCATION_FS,Vu8Buffer,sizeof(Vu8Buffer),0x01)<0)
  {
    Vu32Ret=1;
  }
  else
  {/* read data*/
    if(pdd_read_datastream("2222/DeleteDataFS2",PDD_LOCATION_FS,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)<0)
	{
	   Vu32Ret=2;
	}
	else
	{/*compare data*/
	  if(memcmp(Vu8Buffer,Vu8BufferRead,sizeof(Vu8BufferRead))!=0)
	  {
	    Vu32Ret=3;
	  }
	  else
	  { /* delete data*/
	    if(pdd_delete_data_stream("2222/DeleteDataFS2",PDD_LOCATION_FS)<0)
		{
		  Vu32Ret=4;
		}
		else
		{ /* delete data twice*/
          if(pdd_delete_data_stream("2222/DeleteDataFS2",PDD_LOCATION_FS)!=PDD_ERROR_DATA_STREAM_DOESNOTEXIST)
		  {
		    Vu32Ret=5;
		  }
		}
	  }
	}
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataBackupNotExist()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataBackupNotExist(void)
{
  tU32 Vu32Ret = 0; 
  tU8  Vu8Buffer[30];
  tU8  Vu8BufferRead[30];
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(Vu8Buffer,55,sizeof(Vu8Buffer));
  /* write data*/
  if(pdd_write_data_stream("2222/DeleteDataFS3",PDD_LOCATION_FS,Vu8Buffer,sizeof(Vu8Buffer),0x01)<0)
  {
    Vu32Ret=1;
  }
  else
  {/* read data*/
    if(pdd_read_datastream("2222/DeleteDataFS3",PDD_LOCATION_FS,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)<0)
	  {
	   Vu32Ret=2;
	  }
    else
    {/*compare data*/
      if(memcmp(Vu8Buffer,Vu8BufferRead,sizeof(Vu8BufferRead))!=0)
      {
        Vu32Ret=3;
      }
      else
      { /* delete normal file */
        if(remove("/var/opt/bosch/dynamic/pdd/datapool/2222/DeleteDataFS3.dat")<0)
	      {
	        Vu32Ret=4;
	      }
	      else
	      { /* delete data twice*/
          if(pdd_delete_data_stream("2222/DeleteDataFS3",PDD_LOCATION_FS)!=PDD_OK)
	        {
	          Vu32Ret=5;
	        }
	      }
      }
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32PDDDeleteDataNormalNotExist()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  15.01.2015
******************************************************************************/
tU32 u32PDDDeleteDataNormalNotExist(void)
{
  tU32 Vu32Ret = 0; 
  tU8  Vu8Buffer[30];
  tU8  Vu8BufferRead[30];
  tU8  Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  memset(Vu8Buffer,88,sizeof(Vu8Buffer));
  /* write data*/
  if(pdd_write_data_stream("2222/DeleteDataFS4",PDD_LOCATION_FS,Vu8Buffer,sizeof(Vu8Buffer),0x01)<0)
  {
    Vu32Ret=1;
  }
  else
  {/* read data*/
    if(pdd_read_datastream("2222/DeleteDataFS4",PDD_LOCATION_FS,Vu8BufferRead,sizeof(Vu8BufferRead),0x01,&Vu8Info)<0)
	{
	   Vu32Ret=2;
	}
	else
	{/*compare data*/
	  if(memcmp(Vu8Buffer,Vu8BufferRead,sizeof(Vu8BufferRead))!=0)
	  {
	    Vu32Ret=3;
	  }
	  else
	  { /* delete backup file */
	    if(remove("/var/opt/bosch/dynamic/pdd/backup/datapool/2222/DeleteDataFS4.tmp")<0)
		{
		  Vu32Ret=4;
		}
		else
		{ /* delete data twice*/
          if(pdd_delete_data_stream("2222/DeleteDataFS4",PDD_LOCATION_FS)!=PDD_OK)
		  {
		    Vu32Ret=5;
		  }
		}
	  }
	}
  }
  return(Vu32Ret);
}

