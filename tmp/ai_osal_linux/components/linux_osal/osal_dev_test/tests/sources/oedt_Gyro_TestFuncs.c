/******************************************************************************
 *FILE         : oedt_Gyro_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk
 *
 *DESCRIPTION  : This file implements the  test cases for the Gyro
 *               device.Ref:SW Test specification 0.3 draft
 *
 *AUTHOR       : Ravindran P (RBEI/ECF1)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 30.09.15 - Shivasharnappa Mothpalli(RBEI/ECF5)
                            :Updated all the OEDTs in proper format.
                 11.12.15 - (RBEI/ECF5)Shivasharnappa  Mothpalli 
                             Removed un used IOCTRL calls(Fix for CFG3-1622).
                             OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE,
                             OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE,
                             OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION.
******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"


/*****************************************************************
| defines and macros (scope: module-local)
|------------------------------------------------------------------*/

static tVoid vGyroThread(tPVoid pvArg);
static tVoid vOdoThread(tPVoid pvArg);
static tVoid vAccThread(tPVoid pvArg);
static tVoid GyroOedt_vPlotdata(const OSAL_trIOCtrl3dGyroData *prIOCtrl3dGyroData);

/******************************************************************
|Error codes used
|-----------------------------------------------------------------*/
#define GYRO_NO_ERROR                            0
#define GYRO_IOOPEN_ERROR                        1
#define GYRO_IOREAD_ERROR                        2
#define GYRO_CLOSE_ERROR                         3
#define GYRO_VALUE_RANGE_ERROR                   4
#define GYRO_MID_VALUE_ERROR                     5
#define GYRO_READBUFFER_NULL_ERROR               6
#define GYRO_MEMORY_ALLOC_ERROR                  7
#define GYRO_SELF_TEST_ERROR                     8 
#define GYRO_RE_OPEN_ERROR                       9
#define GYRO_RE_CLOSE_ERROR                      10
#define GYRO_IOCTRL_GETCNT_ERROR                 11
#define GYRO_IOCTRL_GET_VERSION_ERROR            12
#define GYRO_IOCTRL_GYRO_GET_TEMP_ERROR          13
#define GYRO_IOCTRL_GET_CYCLE_TIME_ERROR         14
#define GYRO_IOCTRL_GYRO_GET_HW_INFO_ERROR       15
#define GYRO_CYCLE_TIME_VALUE_MISMATCH_ERROR     19

/**
 * @def GYRO_SENSOR_DATA_RESOLUTION_RANGE
 * range of values delivered by sensor derived
*/
#define GYRO_SENSOR_DATA_RESOLUTION_RANGE    65536
#define NO_OF_RECORDS 10
#define NO_OF_ELEMENTS_TO_READ 10
#define OSAL_MULTITHREAD_WAIT_TIMEOUT    10000
#define MULTI_THREAD_EVENT               (tString)"MthrdEvent"
#define GYRO_DEFAULT_TIME_INTERVAL  50000000 //NS 
#define GYRO_READ_WAIT 1000

#define GYRO_OEDT_TRACE_WIDTH (25)
#define GYRO_OEDT_MID_GYROVALUE (0xFFFF/2)
#define GYRO_OEDT_DATA_SCALE_FACTOR (GYRO_OEDT_MID_GYROVALUE/GYRO_OEDT_TRACE_WIDTH)
#define GYRO_OEDT_NO_OF_PLOT_COLUMNS (6)
#define GYRO_OEDT_TOTAL_PLOT_WIDTH (GYRO_OEDT_TRACE_WIDTH * GYRO_OEDT_NO_OF_PLOT_COLUMNS)
#define GYRO_OEDT_R_PLOT_OFFSET (0)
#define GYRO_OEDT_S_PLOT_OFFSET (GYRO_OEDT_TRACE_WIDTH *2)
#define GYRO_OEDT_T_PLOT_OFFSET (GYRO_OEDT_TRACE_WIDTH *4)
#define GYRO_OEDT_MAX_CONT_READ_FAILURES (10)
#define GYRO_OEDT_TEST_DURATION_MS (60000)
#define GYRO_OEDT_PLOT_DATA_INTERVALL (300)
#define GYRO_MASK_VALUE 0x0001
#define ACC_MASK_VALUE  0x0002
#define ODO_MASK_VALUE  0x0004

/****************************************************************
Global variables
*****************************************************************/

tU32  u32MthrdErrorNo;
OSAL_tEventHandle MthrdEvent_handler;
tU32  u32TimerCnt;


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
* FUNCTION    : u32GyroGetCycleTime()
* DESCRIPTION : Reads the cycle time from gyro .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 24.11.15 Shivasharnappa Mothpalli(RBEI/ECF5)
******************************************************************/
tU32 u32GyroGetCycleTime(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = GYRO_NO_ERROR;
   tU32 GyroGetCyleTime = 0;

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GYRO,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else
   {
      if  ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME,(tS32)&GyroGetCyleTime) == OSAL_ERROR)  
      {
         u32RetVal = GYRO_IOCTRL_GET_CYCLE_TIME_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Gyro  get cycle time failed error no  : %d",OSAL_u32ErrorCode() );
      }
      //PQM_authorized_multi_550
      else if (GyroGetCyleTime != GYRO_DEFAULT_TIME_INTERVAL)//lint !e774
      {
         u32RetVal = GYRO_CYCLE_TIME_VALUE_MISMATCH_ERROR;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS," Mismatch in Gyro cycle time , GyroGetCyleTime : %d",GyroGetCyleTime );
      }         
      else 
      {
         u32RetVal = GYRO_NO_ERROR;   
      }
   }

   if ( (hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroOpenClose()
* DESCRIPTION : Tests device open and close.
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 21.03.13  Swathi Bolar(RBEI/ECF5)
******************************************************************/
tU32 u32GyroOpenClose(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = GYRO_NO_ERROR;

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GYRO,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }

   if ( (hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroReOpen()
* DESCRIPTION : Tests device re open. driver should not allow re open
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 21.03.13  Swathi Bolar(RBEI/ECF5)
******************************************************************/
tU32 u32GyroReOpen(tVoid)
{
   OSAL_tIODescriptor hDevice1;
   OSAL_tIODescriptor hDevice2;
   tU32 u32RetVal = GYRO_NO_ERROR;

   //Open the device
   hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( OSAL_ERROR == hDevice1)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   } 
   else
   {
      /* Attempt to Re-open the device 
         Multiple open is not supported. So it should fail
      */
      hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
      if ( OSAL_ERROR != hDevice2)
      {
         u32RetVal = GYRO_RE_OPEN_ERROR;
         //If successful, Close the device
         if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice2 ) )
         {
            u32RetVal = GYRO_CLOSE_ERROR;
         }
      }
      //Close the device
      if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice1 ) )
      {
         u32RetVal = GYRO_RE_CLOSE_ERROR;
      }
   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroCloseAlreadyClosed()
* DESCRIPTION : Tests device re closed . driver should not allow 
                re close
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 21.03.13  Swathi Bolar(RBEI/ECF5)
******************************************************************/
tU32 u32GyroCloseAlreadyClosed(tVoid)
{
   OSAL_tIODescriptor hDevice ;
   tU32 u32RetVal = GYRO_NO_ERROR;

   //Open the device
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO , OSAL_EN_READWRITE );
   if ( OSAL_ERROR == hDevice)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }

   //Close the device
   else if( OSAL_ERROR  == OSAL_s32IOClose ( hDevice ) )
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   //Close the device which is already closed, if successful indicate error
   else if(OSAL_ERROR  != OSAL_s32IOClose ( hDevice ) )
   {
      u32RetVal = GYRO_RE_CLOSE_ERROR;
   }
   else
   {
      //success
   }

   return u32RetVal;
}

 /*****************************************************************
* FUNCTION    : u32GyroBasicRead()
* DESCRIPTION : Reads 8 record and checks with the no of record 
                to be read .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 21.03.13  Swathi Bolar(RBEI/ECF5)
******************************************************************/

tU32 u32GyroBasicRead(tVoid)
{

   OSAL_tIODescriptor hDevice ;
   tS32 s32GyroDataRead = 0;
   tU32 u32NumOfElemToRead = 8;
   OSAL_trIOCtrl3dGyroData *DataBufferPointer = OSAL_NULL;
   OSAL_trIOCtrl3dGyroData *pTempDataBuffer = OSAL_NULL;
   tU32 u32RetVal = GYRO_NO_ERROR;
   tU32 u32Index;
   tS32 s32ElemReadFromBuf = 0;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;   
   }

   if( u32RetVal == GYRO_NO_ERROR )
   {
      DataBufferPointer = (OSAL_trIOCtrl3dGyroData*)OSAL_pvMemoryAllocate(
                             u32NumOfElemToRead * sizeof(OSAL_trIOCtrl3dGyroData));
      if( DataBufferPointer == OSAL_NULL )
      {
         u32RetVal = GYRO_MEMORY_ALLOC_ERROR;
      }
   }

   if( u32RetVal == GYRO_NO_ERROR )
   {
      OSAL_s32ThreadWait(GYRO_READ_WAIT);
      s32GyroDataRead = OSAL_s32IORead( hDevice,
                                        (tPS8)DataBufferPointer,
                                        u32NumOfElemToRead * sizeof( OSAL_trIOCtrl3dGyroData ) );

      s32ElemReadFromBuf = s32GyroDataRead / (tS32)sizeof(OSAL_trIOCtrl3dGyroData);
      if ( s32GyroDataRead == OSAL_ERROR )
      {
         u32RetVal = GYRO_IOREAD_ERROR;
      }
      /* Ring buffer has less than requested number of elements */
      else if ( s32ElemReadFromBuf < (tS32)u32NumOfElemToRead )
      {
         OEDT_HelperPrintf( (tS32)TR_LEVEL_USER_4, "Gyro: Ring buffer has less than requested number of elements."
                             " Num of elements read: %d", s32ElemReadFromBuf );
         pTempDataBuffer = DataBufferPointer;
         if( pTempDataBuffer != OSAL_NULL )
         {
            for( u32Index = 0; u32Index < (tU32)s32ElemReadFromBuf; u32Index++ )
            {
               OEDT_HelperPrintf( (tS32)TR_LEVEL_USER_4, "Gyro: u32TimeStamp=%lu   u16ErrorCounter:%lu", 
                                  pTempDataBuffer->u32TimeStamp, pTempDataBuffer->u16ErrorCounter);
               OEDT_HelperPrintf( (tS32)TR_LEVEL_USER_4, "Gyro: u16Gyro_r=%lu   u16Gyro_s=%lu  u16Gyro_t=%lu", 
                                  pTempDataBuffer->u16Gyro_r, pTempDataBuffer->u16Gyro_s, 
                                  pTempDataBuffer->u16Gyro_t);

               pTempDataBuffer++;
            }
         }
      }
      else
      {
         pTempDataBuffer = DataBufferPointer;
         if( pTempDataBuffer != OSAL_NULL )
         {
            for( u32Index = 0; u32Index < u32NumOfElemToRead; u32Index++ )
            {
               OEDT_HelperPrintf( (tS32)TR_LEVEL_USER_4, "Gyro: u32TimeStamp=%lu   u16ErrorCounter:%lu", 
                                  pTempDataBuffer->u32TimeStamp, pTempDataBuffer->u16ErrorCounter);
               OEDT_HelperPrintf( (tS32)TR_LEVEL_USER_4, "Gyro: u16Gyro_r=%lu   u16Gyro_s=%lu  u16Gyro_t=%lu", 
                                  pTempDataBuffer->u16Gyro_r, pTempDataBuffer->u16Gyro_s, 
                                  pTempDataBuffer->u16Gyro_t);

               pTempDataBuffer++;
            }
         }
      }
   }

   if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   if( DataBufferPointer != OSAL_NULL )
   {
      OSAL_vMemoryFree( DataBufferPointer );
   }

   return u32RetVal;
}

 /*****************************************************************
* FUNCTION    : u32OpenCloseRepeatedly()
* DESCRIPTION : Tests driver response for repeated open and close calls. 
                Full project autosar must be flashed .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 14.07.2015 - Srinivas Prakash Anvekar -Initial Revision.
******************************************************************/

tU32 u32OpenCloseRepeatedly(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = GYRO_NO_ERROR;
   tU8 u8Index = 0;

   do
   {
      hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
      if( hDevice == OSAL_ERROR )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "Gyro device open failed ");
         u32RetVal = GYRO_IOOPEN_ERROR;
      }
      else if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32RetVal = GYRO_CLOSE_ERROR;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_ERRORS,
                            "Device Closing Failed Line : %d ErrorCode:%d",
                            __LINE__, OSAL_u32ErrorCode() );
      }
      else
      {
         //success
      }

      u8Index++;

   }while( ( u8Index < 20 ) && ( u32RetVal == GYRO_NO_ERROR ) );

   return u32RetVal;
}

 /*****************************************************************
* FUNCTION    : u32GyroSelfTest()
* DESCRIPTION : Tests Gyro self test functionality.  
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : Sanjay G.
******************************************************************/

tU32 u32GyroSelfTest(tVoid)
{

   OSAL_tIODescriptor hDevice;
   tS32 s32SelfTestResult = -1;
   tU32 u32RetVal = GYRO_NO_ERROR;
   (void)s32SelfTestResult; //to remove lint 

   //Open the device in read write mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GYRO,OSAL_EN_READWRITE);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   //Trigger self test
   else if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_SELF_TEST,(tS32)& s32SelfTestResult) == OSAL_ERROR )
   {
      u32RetVal = GYRO_SELF_TEST_ERROR;  
   }
   else
   {
     //success
   }

   if( ( hDevice != OSAL_ERROR ) && ( OSAL_ERROR == OSAL_s32IOClose( hDevice ) ) )
   {
      u32RetVal = GYRO_CLOSE_ERROR; 
   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroInterfaceCheckSeq()

* DESCRIPTION : Tests all the IO Control interface provided by the
                Gyro Driver. The Supported Interfaces:
                VERSION,GETCNT,SET_TIMEOUT_VALUE,GET_TIMEOUT_VALUE,
                GETRESOLUTION,GETCYCLETIME.
* PARAMETER   : None

* RETURNVALUE : 0 - On success
                Non-zero error code in case of error

* HISTORY     : 21.03.13  Swathi Bolar(RBEI/ECF5)
                11.12.15  (RBEI/ECF5)Shivasharnappa  Mothpalli 
                          Removed un used IOCTRL calls(Fix for CFG3-1622).
                          OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE,
                          OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE,
                          OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION.         

******************************************************************/

tU32 u32GyroInterfaceCheckSeq(void)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32NumofRecs = 0;
   tU32 u32Version = 0;
   tU32 u32CycleTimeNs = 0;
  
   OSAL_trIOCtrl3dGyroData rData ={0,0,0,0,0};
   tU32 u32RetVal = GYRO_NO_ERROR;
   (void)u32Version;//to remove lint 
   (void)u32NumofRecs;//to remove lint 
   (void)u32CycleTimeNs;//to remove lint 

   

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }

   //Get the driver version
   if(u32RetVal == GYRO_NO_ERROR )
   {
      if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)&u32Version) == OSAL_ERROR)
      {
         u32RetVal = GYRO_IOCTRL_GET_VERSION_ERROR;  
      }
   }

   //Get the No.of records in the Buffer
   if(u32RetVal == GYRO_NO_ERROR )
   {
      if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GETCNT,(tS32)&u32NumofRecs) == OSAL_ERROR)
      {
         u32RetVal = GYRO_IOCTRL_GETCNT_ERROR;
      }
   }

   //Get the Cycle time
   if(u32RetVal == GYRO_NO_ERROR )
   {
      if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME,(tS32)&u32CycleTimeNs) == OSAL_ERROR)
      {
         u32RetVal = GYRO_IOCTRL_GET_CYCLE_TIME_ERROR;
      }
   }

   //Read 1 record
   if(u32RetVal == GYRO_NO_ERROR )
   {
      if(OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR)
      {
         u32RetVal = GYRO_IOREAD_ERROR;
      }
   }

   //Close the device
   if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ) )
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return  u32RetVal;

}

/*****************************************************************
* FUNCTION    : u32GyroReadPassingNullBuffer()

* DESCRIPTION : Tests driver response by passing NULL buffer.

* PARAMETER   : None

* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
                
* HISTORY     : 02.06.10 RBEI(ECF1)- Sainath Kalpuri
 
******************************************************************/

tU32 u32GyroReadPassingNullBuffer(tVoid)
{

   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else if( OSAL_s32IORead (hDevice,OSAL_NULL,sizeof(OSAL_trIOCtrl3dGyroData)) != OSAL_ERROR )
   {
      u32RetVal = GYRO_READBUFFER_NULL_ERROR;
   }
   else
   {
      //success
   }

   //Close the device
   if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return  u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroIOCTRLPassingNullBuffer()

* DESCRIPTION : Tests All the supported IOCTRLs by passing NULL,
                The  supported IOCTRLs are.
                VERSION,GETCNT,SET_TIMEOUT_VALUE,
                GET_TIMEOUT_VALUE,GETRESOLUTION,GETCYCLETIME.

* PARAMETER   : None

* RETURNVALUE : 0 - On success
                Non-zero error code in case of error

* HISTORY     : 02.06.10  RBEI(ECF1)- Sainath Kalpuri
                11.12.15 (RBEI/ECF5)Shivasharnappa  Mothpalli 
                         Removed un used IOCTRL calls(Fix for CFG3-1622).
                         OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE,
                         OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE,
                         OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION.
******************************************************************/

tU32 u32GyroIOCTRLPassingNullBuffer(tVoid)
{
   OSAL_tIODescriptor hDevice;
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else
   {
      //Get the driver version
      if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_VERSION,(tS32)OSAL_NULL) != OSAL_ERROR)
      {
         u32RetVal = GYRO_IOCTRL_GET_VERSION_ERROR;
      }
   
      //Get the No. of records in the Buffer
      if( u32RetVal == GYRO_NO_ERROR )
      {
         if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GETCNT,(tS32)OSAL_NULL) != OSAL_ERROR)
         {
            u32RetVal = GYRO_IOCTRL_GETCNT_ERROR;
         }
      }

      //Get the Cycle time
      if( u32RetVal == GYRO_NO_ERROR )
      {
         if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME,(tS32)OSAL_NULL) != OSAL_ERROR)
         {
            u32RetVal = GYRO_IOCTRL_GET_CYCLE_TIME_ERROR; 
         }
      }

      //Close the device
      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = GYRO_CLOSE_ERROR;
      }
   }

   return  u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroCheckGyroValuesSeq_r()
* DESCRIPTION : Reads 100 data records from the driver,
                Checks the variation in R-axis data is within 0.5% 
                of full scale when the target is still(not moving). 
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 09.28.2015  Shivasharnappa Mothpalli (RBEI/ECF5)
******************************************************************/

tU32 u32GyroCheckGyroValuesSeq_r(void)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrl3dGyroData rData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8  u8Index = 0;
   tU16 au16GyroReadVal_r[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tF32 f32PercentageOfRange = 0.5;
   tU16 u16Gyro_r_varience_higher_limit = 0;
   tU16 u16Gyro_r_variance_lower_limit = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else
   {

      for( u8Index = 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal == GYRO_NO_ERROR ) ; u8Index++)
      {
         if( OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR )
         {
            u32RetVal = GYRO_IOREAD_ERROR;
         }
         au16GyroReadVal_r[u8Index] = rData.u16Gyro_r;
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         for(u8Index= 0; u8Index < NO_OF_RECORDS; u8Index++)
         {
            u32Sum  +=  au16GyroReadVal_r[u8Index];
         }

         u32AvgVal = (u32Sum / NO_OF_RECORDS);

         //Calculate the 0.5 % of average value
         u16BaseAvgVal = ( ( tU16 )(u32AvgVal * 0.5)/100);
         u16Gyro_r_varience_higher_limit = (tU16)(u32AvgVal + u16BaseAvgVal);
         u16Gyro_r_variance_lower_limit = (tU16)(u32AvgVal - u16BaseAvgVal);

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_r_varience_higher_limit: %d", u16Gyro_r_varience_higher_limit);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_r_variance_lower_limit:  %d", u16Gyro_r_variance_lower_limit);

         u32Sum = 0;
         u32AvgVal = 0;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  au16GyroReadVal_r[u8Index];
            if(au16GyroReadVal_r[u8Index] > u32HighVal)
               u32HighVal = au16GyroReadVal_r[u8Index];
         }

         u32LowVal = GYRO_SENSOR_DATA_RESOLUTION_RANGE ;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if(au16GyroReadVal_r[u8Index] <  u32LowVal)
               u32LowVal = au16GyroReadVal_r[u8Index];
         }

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);

         if( (u32HighVal - u32LowVal) > ((tU32)( f32PercentageOfRange*GYRO_SENSOR_DATA_RESOLUTION_RANGE)/100))
         {
            u32RetVal=GYRO_VALUE_RANGE_ERROR;    
         }
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         u32AvgVal = (u32Sum /u32NoOfRecsForRead);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);

         if( (u32AvgVal < u16Gyro_r_variance_lower_limit) || (u32AvgVal > u16Gyro_r_varience_higher_limit) )
         {
           u32RetVal=GYRO_MID_VALUE_ERROR;
         }
      }

      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = GYRO_CLOSE_ERROR;
      }

   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroCheckGyroValuesSeq_s()
* DESCRIPTION : Reads 100 data records from the driver,
                Checks the variation in S-axis data is within 0.5% 
                of full scale when the target is still(not moving). 
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 09.28.2015  Shivasharnappa Mothpalli (RBEI/ECF5)
******************************************************************/
 
tU32 u32GyroCheckGyroValuesSeq_s(void)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_trIOCtrl3dGyroData rData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8 u8Index = 0;
   tU16 au16GyroReadVal_s[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tF32 f32PercentageOfRange = 0.5;
   tU16 u16Gyro_s_varience_higher_limit = 0;
   tU16 u16Gyro_s_variance_lower_limit = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else
   {
      for( u8Index= 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal != GYRO_IOREAD_ERROR ); u8Index++)
      {
         if(OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR)
         {
            u32RetVal = GYRO_IOREAD_ERROR;
         }
         au16GyroReadVal_s[u8Index] = rData.u16Gyro_s;
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         for(u8Index= 0; u8Index < NO_OF_RECORDS; u8Index++)
         {
            u32Sum  +=  au16GyroReadVal_s[u8Index];
         }

         u32AvgVal =(tU16) (u32Sum / NO_OF_RECORDS);
         //Calculate the 0.5% of average value
         u16BaseAvgVal = (tU16)((u32AvgVal * 0.5)/100);
         u16Gyro_s_varience_higher_limit = (tU16)(u32AvgVal + u16BaseAvgVal);
         u16Gyro_s_variance_lower_limit = (tU16)(u32AvgVal - u16BaseAvgVal);

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_s_varience_higher_limit: %d", u16Gyro_s_varience_higher_limit);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_s_variance_lower_limit:  %d", u16Gyro_s_variance_lower_limit);


         u32Sum = 0;
         u32AvgVal = 0;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  au16GyroReadVal_s[u8Index];
            if( au16GyroReadVal_s[u8Index] > u32HighVal )
               u32HighVal = au16GyroReadVal_s[u8Index];
         }

         u32LowVal = GYRO_SENSOR_DATA_RESOLUTION_RANGE ;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if( au16GyroReadVal_s[u8Index] <  u32LowVal )
               u32LowVal = au16GyroReadVal_s[u8Index];
         }

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);

         if( (u32HighVal - u32LowVal) > ((tU32)(f32PercentageOfRange*GYRO_SENSOR_DATA_RESOLUTION_RANGE)/100))
         {
            u32RetVal = GYRO_VALUE_RANGE_ERROR;
         }
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         u32AvgVal = u32Sum / u32NoOfRecsForRead;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);
         if( (u32AvgVal < u16Gyro_s_variance_lower_limit) || (u32AvgVal > u16Gyro_s_varience_higher_limit) )
         {
            u32RetVal=GYRO_MID_VALUE_ERROR;
         }
      }

      if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = GYRO_CLOSE_ERROR;
      }
   }

   return  u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32GyroCheckGyroValuesSeq_t()
* DESCRIPTION : Reads 100 data records from the driver,
                Checks the variation in T-axis data is within 0.5% 
                of full scale when the target is still(not moving). 
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 09.28.2015  Shivasharnappa Mothpalli (RBEI/ECF5)
******************************************************************/
 
tU32 u32GyroCheckGyroValuesSeq_t(void)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_trIOCtrl3dGyroData rData = {0,0,0,0,0};
   tU32 u32NoOfRecsForRead = 100;
   tU8  u8Index = 0;
   tU16 aU16GyroReadVal_t[100] = {0};
   tU32 u32LowVal = 0;
   tU32 u32HighVal = 0;
   tU32 u32Sum = 0;
   tU32 u32AvgVal = 0;
   tF32 f32PercentageOfRange = 0.5;
   tU16 u16Gyro_t_varience_higher_limit = 0;
   tU16 u16Gyro_t_variance_lower_limit = 0;
   tU16 u16BaseAvgVal = 0;
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR )
   {
      u32RetVal=GYRO_IOOPEN_ERROR;
   }
   else
   {
      for( u8Index = 0; ( u8Index < u32NoOfRecsForRead ) && ( u32RetVal != GYRO_IOREAD_ERROR ); u8Index++ )
      {
         if( OSAL_s32IORead ( hDevice,(tS8 *)&rData,sizeof(rData) ) == OSAL_ERROR )
         {
            u32RetVal=GYRO_IOREAD_ERROR;
         }
         aU16GyroReadVal_t[u8Index] = rData.u16Gyro_t;
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         for(u8Index= 0; u8Index < NO_OF_RECORDS; u8Index++)
         {
            u32Sum  +=  aU16GyroReadVal_t[u8Index];
         }

         u32AvgVal = u32Sum / NO_OF_RECORDS;

         //Calculate the 0.5 % of average value
         u16BaseAvgVal = (tU16)((u32AvgVal * 0.5)/100);
         u16Gyro_t_varience_higher_limit = (tU16)(u32AvgVal + u16BaseAvgVal);
         u16Gyro_t_variance_lower_limit = (tU16)(u32AvgVal - u16BaseAvgVal);

         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16BaseAvgVal: %d", u16BaseAvgVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_t_varience_higher_limit: %d", u16Gyro_t_varience_higher_limit);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u16Gyro_t_variance_lower_limit:  %d", u16Gyro_t_variance_lower_limit);

         u32Sum = 0;
         u32AvgVal = 0;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            u32Sum  +=  aU16GyroReadVal_t[u8Index];
            if(aU16GyroReadVal_t[u8Index] > u32HighVal)
            u32HighVal = aU16GyroReadVal_t[u8Index];
         }

         u32LowVal = GYRO_SENSOR_DATA_RESOLUTION_RANGE;

         for( u8Index= 0; u8Index < u32NoOfRecsForRead; u8Index++)
         {
            if(aU16GyroReadVal_t[u8Index] <  u32LowVal)
            u32LowVal = aU16GyroReadVal_t[u8Index];
         }
         
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32HighVal: %d", u32HighVal);
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32LowVal: %d", u32LowVal);

         if( (u32HighVal - u32LowVal) > ((tU32)(f32PercentageOfRange*GYRO_SENSOR_DATA_RESOLUTION_RANGE)/100))
         {
            u32RetVal = GYRO_VALUE_RANGE_ERROR;
         }
      }

      if( u32RetVal == GYRO_NO_ERROR )
      {
         u32AvgVal = u32Sum / u32NoOfRecsForRead ;
         OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"u32AvgVal: %d", u32AvgVal);
         if( ( u32AvgVal < u16Gyro_t_variance_lower_limit ) || ( u32AvgVal > u16Gyro_t_varience_higher_limit ) )
         {
            u32RetVal = GYRO_MID_VALUE_ERROR;
         }
      }

      if ( ( hDevice != OSAL_ERROR )&& ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
      {
         u32RetVal = GYRO_CLOSE_ERROR;
      }
   }

   return  u32RetVal ;
}

/*****************************************************************
* FUNCTION    : u32GyroTemperature()
* DESCRIPTION : Gets the Gyro temperature
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 05.07.12  Sainath Kalpuri (RBEI/ECF5)
******************************************************************/

tU32  u32GyroTemperature ( tVoid )
{
   OSAL_tIODescriptor hDevice;
   tF32 f32Gyrotemperature = 0;
   tChar strGyroTemperature[8];
   tU32 u32RetVal = GYRO_NO_ERROR;

   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      u32RetVal = GYRO_IOOPEN_ERROR;   
   }

   if(u32RetVal == GYRO_NO_ERROR)
   {
      OSAL_s32ThreadWait(GYRO_READ_WAIT);
      if ( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GET_TEMP,(tS32)&f32Gyrotemperature ) == OSAL_ERROR )
      {
         u32RetVal = GYRO_IOCTRL_GYRO_GET_TEMP_ERROR;
      }
      else
      {
         sprintf(strGyroTemperature,"%.4f",f32Gyrotemperature);
         OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"Gyro temperature = %s",strGyroTemperature);
      }
   }

   if ( (hDevice != OSAL_ERROR )&& (OSAL_s32IOClose ( hDevice ) == OSAL_ERROR ))
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return u32RetVal;
}

/*****************************************************************
* FUNCTION    : u32MultiThreadBasicRead()
* DESCRIPTION : Reads the data from Gyro ,Acc and Odo.
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 21.03.13  Sanjay Gurugubelli (RBEI/ECF5)  
******************************************************************/
 
tU32 u32MultiThreadBasicRead(tVoid)
{

   OSAL_trThreadAttribute rThreadAttr;
   tBool bErrorOccured = FALSE;
   OSAL_tEventMask ResultMask = 0;

   if(OSAL_ERROR==OSAL_s32EventCreate(MULTI_THREAD_EVENT,&MthrdEvent_handler)) 
   {
      u32MthrdErrorNo+=1;                                        
      bErrorOccured=TRUE;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Event create  failed in MultiThreadbasicread  Thread   Error No: %d",u32MthrdErrorNo);
   }

   if( FALSE == bErrorOccured )
   {
      //Update thread attributes
      rThreadAttr.szName         = "Gyro thread";
      rThreadAttr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;		
      rThreadAttr.s32StackSize   = 2048;   
      rThreadAttr.pfEntry        = vGyroThread;
      rThreadAttr.pvArg          = OSAL_NULL;

      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr) )
     {
         u32MthrdErrorNo+=2;
         bErrorOccured = TRUE;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Gyro Thread Spawning failed  Error No: %d",u32MthrdErrorNo);
     }
   }

   if( FALSE == bErrorOccured )
   {
    //Update thread attributes
      rThreadAttr.szName         = "ACC thread";
      rThreadAttr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;		
      rThreadAttr.s32StackSize   = 2048;   
      rThreadAttr.pfEntry        = vAccThread;
      rThreadAttr.pvArg          = OSAL_NULL;

      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr) )
      {
         u32MthrdErrorNo+=4;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Acc Thread Spawning failed  Error No: %d",u32MthrdErrorNo);
      }
   }

   if( FALSE == bErrorOccured )
   {
      //Update thread attributes
      rThreadAttr.szName		   = "ODO thread";
      rThreadAttr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;		
      rThreadAttr.s32StackSize   = 2048;   
      rThreadAttr.pfEntry        = vOdoThread;
      rThreadAttr.pvArg		   = OSAL_NULL;
      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
      {
         u32MthrdErrorNo+=8;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"ODO  Thread Spawning failed  Error No: %d",u32MthrdErrorNo);
      }
   }

   if( FALSE == bErrorOccured )
   {

      if(OSAL_ERROR==OSAL_s32EventWait(MthrdEvent_handler,GYRO_MASK_VALUE|ACC_MASK_VALUE|ODO_MASK_VALUE,OSAL_EN_EVENTMASK_AND,OSAL_MULTITHREAD_WAIT_TIMEOUT ,&ResultMask))
      {
         u32MthrdErrorNo+=16;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Eventwait  failed in  MultiThreadbasicread  Error No: %d",u32MthrdErrorNo);
      }

      else if(OSAL_ERROR==OSAL_s32EventPost(MthrdEvent_handler,~ResultMask,OSAL_EN_EVENTMASK_AND))
      {
         u32MthrdErrorNo+=32;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"EventPost  failed in  MultiThreadbasicread  Error No: %d",u32MthrdErrorNo);
      }

      if(OSAL_ERROR==OSAL_s32EventClose(MthrdEvent_handler))
      {
         u32MthrdErrorNo+=64;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Eventclose  failed in  MultiThreadbasicread  Error No: %d",u32MthrdErrorNo);
      }

      if(OSAL_ERROR==OSAL_s32EventDelete(MULTI_THREAD_EVENT))
      {
         u32MthrdErrorNo+=128;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"EventDelete  failed in  MultiThreadbasicread  Error No: %d",u32MthrdErrorNo);
      }

   }

   if(FALSE == bErrorOccured)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"Multithreadbasicread   Thread Execution Completed Successfully ");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"TOTAL ERRORS IN Multithreadbasicread thread  : ");
   }
   return  u32MthrdErrorNo;
}
/*******************************************/
//              GYROMETER THREAD
/*******************************************/

static  tVoid vGyroThread( tPVoid pvArg)
{
   
   OSAL_tIODescriptor hDevice ;
   tS32 s32NumOfElemRead ;
   tBool bErrorOccured = FALSE;
   tS32 s32GyroDataRead; 
   tChar strVar[8];
   tU8 u8Index ;
   OSAL_trIOCtrl3dGyroData rGyroData[10]={0};
   (void)pvArg;//to remove lint //
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u32MthrdErrorNo+= 256;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Gyro IO open failed  Error No: %d",u32MthrdErrorNo);
   }
   /*Just Opened Gyro Driver.Just Wait Till Ring Buffer
   Is Filled With Some Entries */
   OSAL_s32ThreadWait(6000);
   //Read Data
   if(FALSE == bErrorOccured )
   {
      s32GyroDataRead = OSAL_s32IORead(hDevice,(tPS8 )rGyroData,
                           NO_OF_ELEMENTS_TO_READ * sizeof( OSAL_trIOCtrl3dGyroData ));
      s32NumOfElemRead = s32GyroDataRead / ( tS32 )sizeof( OSAL_trIOCtrl3dGyroData );
      if( s32NumOfElemRead != (tS32)NO_OF_ELEMENTS_TO_READ )
      {
         u32MthrdErrorNo+= 512;
         OSAL_s32IOClose ( hDevice );
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Gyro IO close failed  Error No: %d",u32MthrdErrorNo);
      }
   }
   
   for(u8Index=0;u8Index<NO_OF_ELEMENTS_TO_READ;u8Index++)
   {
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rGyroData[u8Index].u16Gyro_r);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rGyroData[%d]->u16Gyro_r = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rGyroData[u8Index].u16Gyro_s);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rGyroData[%d]->u16Gyro_s = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rGyroData[u8Index].u16Gyro_t);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rGyroData[%d]->u16Gyro_t = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rGyroData[u8Index].u16ErrorCounter);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rGyroData[%d]->u16ErrorCounterGyro = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%lu",rGyroData[u8Index].u32TimeStamp);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rGyroData[%d]->u32TimeStampGyro = %s",u8Index,strVar);
   }
    
   if(FALSE == bErrorOccured )
   {
      if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
      {
         u32MthrdErrorNo+= 1024;
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Gyro IO close failed  Error No: %d",u32MthrdErrorNo);
      }
   }
   
   if(OSAL_ERROR==OSAL_s32EventPost(MthrdEvent_handler,GYRO_MASK_VALUE,OSAL_EN_EVENTMASK_OR))
   {
      u32MthrdErrorNo+=2048;
      bErrorOccured=TRUE;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Event post failed in Gyro Thread   Error No: %d",u32MthrdErrorNo);
   }
   
   if(FALSE == bErrorOccured)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"ACC  Thread Execution Completed Successfully ");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"TOTAL ERRORS IN ACC : ");
   }
   
   OSAL_vThreadExit();
}

/*******************************************/
//            ACCELEROMETER THREAD
/*******************************************/
static tVoid vAccThread( tPVoid pvArg)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_trIOCtrlAccData rAccData[10] ={0,0,0,0,0};
   tBool bErrorOccured = FALSE;
   tChar strVar[8];
   tU8 u8Index;
   (void)pvArg;//to remove lint//
   //Open Accelerometer Device
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ACC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u32MthrdErrorNo+= 4096;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Acc IO open failed  Error No: %d",u32MthrdErrorNo);\
   }
   OSAL_s32ThreadWait(6000);
   if(FALSE == bErrorOccured )
   {
      if(OSAL_s32IORead (hDevice,(tS8 *)rAccData,
                sizeof(OSAL_trIOCtrlAccData)*NO_OF_ELEMENTS_TO_READ) == OSAL_ERROR)
      {
         u32MthrdErrorNo+= 8192;
         (tVoid)OSAL_s32IOClose ( hDevice );
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Acc IO close failed  Error No: %d",u32MthrdErrorNo);
      }
   }

   //TraceOut Read Data
   for(u8Index=0;u8Index<NO_OF_ELEMENTS_TO_READ;u8Index++)
   {
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rAccData[u8Index].u16Acc_x);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rAccData[%d]->u16Acc_r = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rAccData[u8Index].u16Acc_y);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rAccData[%d]->u16Acc_s = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rAccData[u8Index].u16Acc_z);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rAccData[%d]->u16Acc_t = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rAccData[u8Index].u16ErrorCounter);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rAccData[%d]->u16ErrorCounter = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%lu",rAccData[u8Index].u32TimeStamp);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rAccData[%d]->u32TimeStamp = %s",u8Index,strVar);
   }
   
   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      bErrorOccured = TRUE;
      u32MthrdErrorNo+= 16384;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Acc IO close failed  Error No: %d",u32MthrdErrorNo);
   }
   
   if(OSAL_ERROR==OSAL_s32EventPost(MthrdEvent_handler,ACC_MASK_VALUE,OSAL_EN_EVENTMASK_OR))
   {
      u32MthrdErrorNo+=32768;
      bErrorOccured=TRUE;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Event post failed in Acc  Thread   Error No: %d",u32MthrdErrorNo);
   }
   
   if(FALSE == bErrorOccured)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"ACC  Thread Execution Completed Successfully ");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"TOTAL ERRORS IN ACC : ");
   }

   OSAL_vThreadExit();
}

/*******************************************/
//              ODOMETER THREAD
/*******************************************/

static tVoid vOdoThread( tPVoid pvArg)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlOdometerData rOdoData[10]={0};
   tBool bErrorOccured = FALSE;
   tChar strVar[8];
   tU8 u8Index;
   (void)pvArg;//to remove lint//
   //Open Odometer Device
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ODOMETER, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      bErrorOccured = TRUE;
      u32MthrdErrorNo += 65536;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"ODO  IO open failed  Error No: %d",u32MthrdErrorNo);
  }
   OSAL_s32ThreadWait(6000);
   //Sequential read
   if(FALSE == bErrorOccured )
   {
      if(OSAL_s32IORead (hDevice,(tS8 *)rOdoData,
           sizeof(OSAL_trIOCtrlOdometerData)*NO_OF_ELEMENTS_TO_READ) == OSAL_ERROR)
      {
         u32MthrdErrorNo += 131072;
         OSAL_s32IOClose ( hDevice );
         OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"ODO  IO close failed  Error No: %d",u32MthrdErrorNo);
      }
   }

   for(u8Index=0;u8Index<NO_OF_ELEMENTS_TO_READ;u8Index++)
   {
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rOdoData[u8Index].enDirection);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rOdoData[%d]->enDirection = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rOdoData[u8Index].u16ErrorCounter);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rOdoData[%d]->u16ErrorCounter = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%lu",rOdoData[u8Index].u32TimeStamp);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rOdoData[%d]->u32TimeStamp = %s",u8Index,strVar);
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%lu",rOdoData[u8Index].u32WheelCounter);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rOdoData[%d]->u32WheelCounter = %s",u8Index,strVar); 
      memset(strVar,'\0',sizeof(strVar));
      sprintf(strVar,"%d",rOdoData[u8Index].u16OdoMsgCounter);
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1,"rOdoData[%d]->u16OdoMsgCounter = %s",u8Index,strVar);
   }

   if(FALSE == bErrorOccured )
   {
       if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
       {
           bErrorOccured = TRUE;
           u32MthrdErrorNo +=262144;
           OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"ODO  IO close failed  Error No: %d",u32MthrdErrorNo);
       }
   }

   if(OSAL_ERROR==OSAL_s32EventPost(MthrdEvent_handler,ODO_MASK_VALUE,OSAL_EN_EVENTMASK_OR))
   {   
      bErrorOccured=TRUE;
      u32MthrdErrorNo+=524288 ;
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"Event post failed in Odo Thread    Error No: %d",u32MthrdErrorNo);
   }

   if(FALSE == bErrorOccured)
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_4,"ACC  Thread Execution Completed Successfully ");
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_ERRORS,"TOTAL ERRORS IN ACC : ");
   }
   
   OSAL_vThreadExit();
}

/*****************************************************************
* FUNCTION    : u32GyroOedt_u32ValuesPlot()
* DESCRIPTION : Plot gyro data on TTfis .
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     : 10.04.2014  Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
******************************************************************/
 
tU32 u32GyroOedt_u32ValuesPlot (tVoid)
{
   tU32 u32RetVal = 0;
   OSAL_trIOCtrl3dGyroData rIOCtrl3dGyroData;
   OSAL_tIODescriptor hGyroDev;
   tU32 u32TestEndTime = OSAL_ClockGetElapsedTime() + GYRO_OEDT_TEST_DURATION_MS;
   tBool bTestEndFlag = FALSE;
   tU32 u32ReadFailureCnt = 0;
   tU32 u32GyroPrevPlotTS = 0;
   tS32 s32ErrChk = 0;

   if( OSAL_ERROR == 
       (hGyroDev =  OSAL_IOOpen( OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READWRITE)) )
   {
      u32RetVal += 1;
   }
   else
   {
      while ( TRUE != bTestEndFlag )
      {
         s32ErrChk = OSAL_s32IORead( hGyroDev,
                                     ( tPS8 )&rIOCtrl3dGyroData,
                                     sizeof( OSAL_trIOCtrl3dGyroData ) );
         if (  s32ErrChk!= (tS32)sizeof( OSAL_trIOCtrl3dGyroData ) )  
         {
            u32ReadFailureCnt++;
         }
         else
         {
            if ( (rIOCtrl3dGyroData.u32TimeStamp - u32GyroPrevPlotTS)> GYRO_OEDT_PLOT_DATA_INTERVALL )
            {
               GyroOedt_vPlotdata(&rIOCtrl3dGyroData);
               u32GyroPrevPlotTS = rIOCtrl3dGyroData.u32TimeStamp;
            }
            u32ReadFailureCnt =0;
         }
         if ( ( u32ReadFailureCnt > GYRO_OEDT_MAX_CONT_READ_FAILURES ) ||
              ( OSAL_ClockGetElapsedTime() > u32TestEndTime ))
         {
            bTestEndFlag = TRUE;
         }
      }

      if( OSAL_ERROR  == OSAL_s32IOClose ( hGyroDev ) )
      {
         u32RetVal += 2;
      }

      if ( u32ReadFailureCnt > GYRO_OEDT_MAX_CONT_READ_FAILURES )
      {
         u32RetVal += 4;
      }
   }
   return u32RetVal;
}

static tVoid GyroOedt_vPlotdata(const OSAL_trIOCtrl3dGyroData *prIOCtrl3dGyroData)
{
   tU8 au8PlotData[GYRO_OEDT_TOTAL_PLOT_WIDTH + 1];
   tU32 u32R_Index, u32S_Index, u32T_Index;

   if (OSAL_NULL != prIOCtrl3dGyroData)
   {
      OSAL_pvMemorySet( au8PlotData, '_', GYRO_OEDT_TOTAL_PLOT_WIDTH);
      u32R_Index = prIOCtrl3dGyroData->u16Gyro_r / GYRO_OEDT_DATA_SCALE_FACTOR;
      u32S_Index = prIOCtrl3dGyroData->u16Gyro_s / GYRO_OEDT_DATA_SCALE_FACTOR;
      u32T_Index = prIOCtrl3dGyroData->u16Gyro_t / GYRO_OEDT_DATA_SCALE_FACTOR;

      au8PlotData[u32R_Index + GYRO_OEDT_R_PLOT_OFFSET] = 'R';
      au8PlotData[u32S_Index + GYRO_OEDT_S_PLOT_OFFSET] = 'S';
      au8PlotData[u32T_Index + GYRO_OEDT_T_PLOT_OFFSET] = 'T';

      au8PlotData[GYRO_OEDT_TOTAL_PLOT_WIDTH] = '\0';
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "%s", au8PlotData);    
   }
}

/*****************************************************************
* FUNCTION    : u32GyroHwInfo()
* DESCRIPTION : Tests the IOCTRL by Calling 
                OSAL_C_S32_IOCTRL_GYRO_GET_HW_INFO IOCTRL to read the
                hardware info.
* PARAMETER   : None
* RETURNVALUE : 0 - On success
                Non-zero error code in case of error
* HISTORY     :
******************************************************************/
tU32 u32GyroHwInfo(tVoid)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlHwInfo sGyroHwInfo={0};
   tU32 u32RetVal = GYRO_NO_ERROR;

   //Open the device in read only mode
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GYRO, OSAL_EN_READONLY);
   if ( OSAL_ERROR == hDevice )
   {
      u32RetVal = GYRO_IOOPEN_ERROR;
   }
   else if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_GYRO_GET_HW_INFO,(tS32)&sGyroHwInfo)  == OSAL_ERROR )
   {
      u32RetVal = GYRO_IOCTRL_GYRO_GET_HW_INFO_ERROR;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Gyro:Mounting angles RX = %d RY = %d RZ = %d",
                                             sGyroHwInfo.rMountAngles.u8AngRX,
                                             sGyroHwInfo.rMountAngles.u8AngRY,
                                             sGyroHwInfo.rMountAngles.u8AngRZ);

      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Gyro:Mounting angles SX = %d SY = %d SZ = %d",
                                             sGyroHwInfo.rMountAngles.u8AngSX,
                                             sGyroHwInfo.rMountAngles.u8AngSY,
                                             sGyroHwInfo.rMountAngles.u8AngSZ);

      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL,"Gyro:Mounting angles TX = %d TY = %d TZ = %d",
                                             sGyroHwInfo.rMountAngles.u8AngTX,
                                             sGyroHwInfo.rMountAngles.u8AngTY,
                                             sGyroHwInfo.rMountAngles.u8AngTZ);

      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - u32AdcRangeMin  : %d", sGyroHwInfo.rRAxes.u32AdcRangeMin);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - u32AdcRangeMax  : %d", sGyroHwInfo.rRAxes.u32AdcRangeMax);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - u32SampleMin    : %d", sGyroHwInfo.rRAxes.u32SampleMin);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - u32SampleMax    : %d", sGyroHwInfo.rRAxes.u32SampleMax);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MinNoiseValue: %f", sGyroHwInfo.rRAxes.f32MinNoiseValue);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32EstimOffset  : %f", sGyroHwInfo.rRAxes.f32EstimOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MinOffset    : %f", sGyroHwInfo.rRAxes.f32MinOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MaxOffset    : %f", sGyroHwInfo.rRAxes.f32MaxOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32DriftOffset  : %f", sGyroHwInfo.rRAxes.f32DriftOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MaxUnsteadOffset : %f", sGyroHwInfo.rRAxes.f32MaxUnsteadOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32BestCalibOffset  : %f", sGyroHwInfo.rRAxes.f32BestCalibOffset);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32EstimScaleFactor : %f", sGyroHwInfo.rRAxes.f32EstimScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MinScaleFactor   : %f", sGyroHwInfo.rRAxes.f32MinScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MaxScaleFactor   : %f", sGyroHwInfo.rRAxes.f32MaxScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32DriftScaleFactor : %f", sGyroHwInfo.rRAxes.f32DriftScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32MaxUnsteadScaleFactor : %f", sGyroHwInfo.rRAxes.f32MaxUnsteadScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32BestCalibScaleFactor  : %f", sGyroHwInfo.rRAxes.f32BestCalibScaleFactor);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32DriftOffsetTime       : %f", sGyroHwInfo.rRAxes.f32DriftOffsetTime);
      OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Gyro : RAxes - f32DriftScaleFactorTime  : %f", sGyroHwInfo.rRAxes.f32DriftScaleFactorTime);
   }

   if( ( hDevice != OSAL_ERROR ) && ( OSAL_ERROR == OSAL_s32IOClose( hDevice ) ) )
   {
      u32RetVal = GYRO_CLOSE_ERROR;
   }

   return u32RetVal;
}
 
/*-----END OF FILE-------*/
