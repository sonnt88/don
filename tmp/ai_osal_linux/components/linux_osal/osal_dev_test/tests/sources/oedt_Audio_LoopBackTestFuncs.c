/******************************************************************************
| FILE			:	oedt_Audio_LoopBackTestFuncs.c
| PROJECT		:	ADIT GEN 2
| SW-COMPONENT	:	OEDT AUDIO LOOPBACK 
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains audio loopback tests
|------------------------------------------------------------------------------
| COPYRIGHT 	:	(c) 2011 Bosch
| HISTORY		:	   
|------------------------------------------------------------------------------
| Date		|	Modification			|	Author
|------------------------------------------------------------------------------
| 11.05.2011	| Initial revision			|	SUR2HI
| --.--.--	| ----------------			 | ------------
| 11.08.2011  	| Audio loobback test fix	| 	SRJ5KOR
| --.--.--  	| ----------------		| ------------
|  9-7-2012    |The completed audio loopback test | ysu1kor
|  14/10/2015   | Lint Fix              |       KKO5KOR
|*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "../osal_oedt_audio_helper/stm_helper_client.h"
#include "acousticout_public.h"
#include "acousticin_public.h"
#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"
#include "oedt_Display.h"

#include "oedt_Acousticin_TestFuncs.h"

/* declarations*/
/* This function is not used anywhere in the code for present. To fix lint, it is commented*/
//extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);


/*macros*/
#define OEDT_ACOUSTICIN_PRINTF_INFO(_X_) printf _X_
#define OEDT_ACOUSTICIN_PRINTF_ERROR(_X_) printf _X_


/*****************************************************************************/
/*************************** OEDT_AUDIO_LOOPBACK ****************************/
/*****************************************************************************/
#define OEDT_AUDIO_LOOPBACK_COUNT                        1
#define OEDT_AUDIO_LOOPBACK_DEVICE_NAME_OUT \
            "/dev/acousticout/speech" 
#define OEDT_AUDIO_LOOPBACK_DEVICE_NAME_IN \
              "/dev/acousticin/speech" 


#define OEDT_AUDIO_LOOPBACK_CHANNELS_IN                 1
#define OEDT_AUDIO_LOOPBACK_CHANNELS_OUT                OEDT_AUDIO_LOOPBACK_CHANNELS_IN
#define OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN               2048
#define OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT              OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN
#define OEDT_AUDIO_LOOPBACK_BUFFERS_TO_READ             1 
#define OEDT_AUDIO_LOOPBACK_WRITE_LOOP_COUNT            OEDT_AUDIO_LOOPBACK_BUFFERS_TO_READ

#define OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE                         (0x00000000)
#define OEDT_AUDIO_LOOPBACK_OPEN_OUT_RESULT_ERROR_BIT               (0x00000001)
#define OEDT_AUDIO_LOOPBACK_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT   (0x00000002)
#define OEDT_AUDIO_LOOPBACK_SETCHANNELS_OUT_RESULT_ERROR_BIT        (0x00000004)
#define OEDT_AUDIO_LOOPBACK_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT    (0x00000008)
#define OEDT_AUDIO_LOOPBACK_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT      (0x00000010)
#define OEDT_AUDIO_LOOPBACK_START_OUT_RESULT_ERROR_BIT              (0x00000020)
#define OEDT_AUDIO_LOOPBACK_OPEN_IN_RESULT_ERROR_BIT                (0x00000040)
#define OEDT_AUDIO_LOOPBACK_SETCHANNELS_IN_RESULT_ERROR_BIT         (0x00000080)
#define OEDT_AUDIO_LOOPBACK_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT     (0x00000100)
#define OEDT_AUDIO_LOOPBACK_SETBUFFERSIZE_IN_RESULT_ERROR_BIT       (0x00000200)
#define OEDT_AUDIO_LOOPBACK_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT    (0x00000400)
#define OEDT_AUDIO_LOOPBACK_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT   (0x00000800)
#define OEDT_AUDIO_LOOPBACK_START_IN_RESULT_ERROR_BIT               (0x00001000)
#define OEDT_AUDIO_LOOPBACK_CLOSE_IN_RESULT_ERROR_BIT               (0x00002000)
#define OEDT_AUDIO_LOOPBACK_CLOSE_OUT_RESULT_ERROR_BIT              (0x00004000)
#define OEDT_AUDIO_LOOPBACK_PLAYBACKDATA_CAPTUREDATA_MISMATCH       (0x00008000)
#define OEDT_AUDIO_LOOPBACK_ACOUSTICIN_READ_ERROR_BIT               (0x00010000)
#define OEDT_AUDIO_LOOPBACK_ACOUSTICOUT_WRITE_ERROR_BIT       		(0x00020000)
#define OEDT_AUDIO_LOOPBACK_REG_NOTIFICATION_IN_RESULT_ERROR_BIT    (0x00040000)
#define OEDT_AUDIO_LOOPBACK_STM_REQUEST_ERROR_BIT    				(0x00080000)
#define OEDT_AUDIO_LOOPBACK_STM_SEND_STARTCMD_ERROR_BIT    			(0x00100000)
#define OEDT_AUDIO_LOOPBACK_STM_SEND_STOPCMD_ERROR_BIT    			(0x00200000)
#define OEDT_AUDIO_LOOPBACK_ACOUT_STOP_RESULT_ERROR_BIT             (0x00400000)
#define OEDT_AUDIO_LOOPBACK_ACOUT_STOP_ACK_RESULT_ERROR_BIT         (0x00800000)
#define OEDT_AUDIO_LOOPBACK_ACIN_STOP_RESULT_ERROR_BIT              (0x01000000)
#define OEDT_AUDIO_LOOPBACK_ACIN_STOP_ACK_RESULT_ERROR_BIT          (0x02000000)
#define OEDT_AUDIO_LOOPBACK_STM_FREE_ERROR_BIT  			        (0x04000000)


#define OEDT_AUDIO_LOOPBACK_DATAPATTERN                             ('A')

#define OEDT_COMPARE_DATA_MATCH 0
#define OEDT_COMPARE_DATA_MISMATCH 1

static tBool OEDT_ACOUSTICOUT_bOutStopped = FALSE;
static tBool OEDT_bAudioInStopped = FALSE;

static tU8 OEDT_AUDIO_LOOPBACK_u8PCMBufferArray[OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN*OEDT_AUDIO_LOOPBACK_BUFFERS_TO_READ];

//static tU32 u32_totalReadBytes=0; /*Lint Fix*/


static void OEDT_AUDIO_LOOPBACK_vCaptureThread(void *pvData);
static void OEDT_AUDIO_LOOPBACK_vPlaybackThread(void *pvData);


static tU32 u32_CompareData=OEDT_COMPARE_DATA_MISMATCH;

unsigned int u32ResultBitMask_Capture;
unsigned int u32ResultBitMask_Playback;

typedef struct OEDT_AUDIO_LOOPBACK_ThreadData_tag
{
	OSAL_tIODescriptor hAcousticIn;
	OSAL_tIODescriptor hAcousticOut;
	tBool bCaptureThreadIsRunning;
	tBool bPlaybackThreadIsRunning;
	tBool bCaptureThreadForceEnd;
	tBool bPlaybackThreadForceEnd;
}OEDT_Audio_Loopback_ThreadData_type;

static volatile OEDT_Audio_Loopback_ThreadData_type OEDT_AUDIO_LOOPBACK_threadData={0};

static OSAL_tThreadID         OEDT_AUDIO_LOOPBACK_captureThreadID   = OSAL_ERROR;

static OSAL_trThreadAttribute OEDT_AUDIO_LOOPBACK_captureThreadAttr = {"OEDTLBc", /*Name*/
                                                OSAL_C_U32_THREAD_PRIORITY_LOWEST,       /*Priority*/
                                                1024,     /*Stacksize*/
                                                (OSAL_tpfThreadEntry)OEDT_AUDIO_LOOPBACK_vCaptureThread,
                                                (void*)&OEDT_AUDIO_LOOPBACK_threadData};

static OSAL_tThreadID         OEDT_AUDIO_LOOPBACK_playbackThreadID   = OSAL_ERROR;
static OSAL_trThreadAttribute OEDT_AUDIO_LOOPBACK_playbackThreadAttr = {"OEDTLBp", /*Name*/
                                                OSAL_C_U32_THREAD_PRIORITY_LOWEST,       /*Priority*/
                                                1024,     /*Stacksize*/
                                                (OSAL_tpfThreadEntry)OEDT_AUDIO_LOOPBACK_vPlaybackThread,
                                                (void*)&OEDT_AUDIO_LOOPBACK_threadData};


/*****************************************************************************
* FUNCTION		: vAcousticOutTstCallback
* PARAMETER		: enCbReason,pvAddData,pvCookie
* RETURNVALUE	:  None
* DESCRIPTION	:  used as device callback function
* HISTORY		:  9-7-2012, mem4kor
******************************************************************************/
static tVoid vAcousticOutTstCallback (OSAL_tenAcousticOutEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{

(void)pvCookie;
(void)pvAddData;

	switch (enCbReason)
	{
		case OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED:
		OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC CALLBACK: OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED\n"));
		OEDT_ACOUSTICOUT_bOutStopped = TRUE;
		break;

		case OSAL_EN_ACOUSTICOUT_EVTIMER:
		OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVTIMER\n"));
		break;

		case OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED:
		OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVSTARTMARKREACHED\n"));
		break;

		case OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED: /*!< Episode end Event */
		OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_EVEPISODEFINISHED\n"));
		break;

		case OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED: /*!< Call Back has been registered */
		OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: OSAL_EN_ACOUSTICOUT_LOAN_CB_REGISTERED\n"));
		break;

		default: /* callback unsupported by test code */
		OEDT_ACOUSTICIN_PRINTF_INFO(("AOUT CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
		break;
	}
}


/*****************************************************************************
* FUNCTION		: vAcousticInTstCallback
* PARAMETER	: enCbReason,pvAddData,pvCookie
* RETURNVALUE	:  None
* DESCRIPTION	:  used as AcousticIn device callback function
* HISTORY		:  23-11-2012, nro2kor
******************************************************************************/
static OSAL_trAcousticErrThrCfg rCbLastErrThrIn_Temp;
static tVoid vAcousticInTstCallback (OSAL_tenAcousticInEvent enCbReason, tPVoid pvAddData,tPVoid pvCookie)
{
	(void)pvCookie;
	(void)pvAddData;

    switch (enCbReason)
    {
    case OSAL_EN_ACOUSTICIN_EVAUDIOSTOPPED:
      OEDT_ACOUSTICIN_PRINTF_INFO(("AIN CALLBACK: OSAL_EN_ACOUSTICIN_EVAUDIOSTOPPED STOPPED \n"));
        /* no additional data */
        OEDT_bAudioInStopped = TRUE;
        break;

    case OSAL_EN_ACOUSTICIN_EVERRTHRESHREACHED:
        rCbLastErrThrIn_Temp = *(OSAL_trAcousticErrThrCfg*)pvAddData;
        OEDT_ACOUSTICIN_PRINTF_INFO(("AIN CALLBACK: OSAL_EN_ACOUSTICIN_EVERRTHRESHREACHED: %u == 0x%08X\n", (unsigned int)rCbLastErrThrIn_Temp.enErrType, (unsigned int)rCbLastErrThrIn_Temp.enErrType));
        break;

    default: /* callback unsupported by test code */
		OEDT_ACOUSTICIN_PRINTF_INFO(("AIN CALLBACK: DEFAULT %u == 0x%08X\n", (unsigned int)enCbReason, (unsigned int)enCbReason));
        break;
    }
}

static tS32 s32Ret_AcousticInRead = OSAL_E_NOERROR;
static tS32 s32Ret_AcousticOutWrite = OSAL_E_NOERROR;

/*****************************************************************************
* FUNCTION		:  void OEDT_AUDIO_LOOPBACK_vCaptureThread
* PARAMETER		:  pvData
* RETURNVALUE	:  None
* DESCRIPTION	:  Captures Data from AcousticIn 
				   fg_audiorenderer->vad->virtiosnd->Alsa->acousticin
* HISTORY		:  9-7-2012, mem4kor
******************************************************************************/
static void OEDT_AUDIO_LOOPBACK_vCaptureThread(void *pvData)
{
	tS32 readCount ;
	tS8 *ps8PCM  ;
	tS32 iCount = 0;
	OEDT_Audio_Loopback_ThreadData_type *pThreadData = (OEDT_Audio_Loopback_ThreadData_type*)pvData;
	int s32Ret;
	tS32 iEmergency;


    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: CAPTURE THREAD START\n"));

	ps8PCM = (tS8*)&OEDT_AUDIO_LOOPBACK_u8PCMBufferArray[0];

	/*This threads reads AcousticIn data only once.*/
	for(readCount = 0; readCount < OEDT_AUDIO_LOOPBACK_BUFFERS_TO_READ; readCount++)
	{        
		s32Ret_AcousticInRead = OSAL_s32IORead( pThreadData->hAcousticIn,ps8PCM,OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN );

		if(s32Ret_AcousticInRead <= 0)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Read from AcousticIn %d\n",(tS32)s32Ret_AcousticInRead));
			break;
		}
		else 
		{
			/*During comparison if there is difference in one byte also,
			the u32_CompareData is set to OEDT_COMPARE_DATA_MISMATCH and breaks the flow*/
			u32_CompareData=OEDT_COMPARE_DATA_MATCH;
			for(iCount=0;iCount<OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN;iCount++)
			{
				if(ps8PCM[iCount]!=(OEDT_AUDIO_LOOPBACK_DATAPATTERN +(readCount)))
				{		
					/* This  means the Playback and Capture data MISMATCHED */
					u32_CompareData=OEDT_COMPARE_DATA_MISMATCH;					
					break;
				}
			}
		} 

		if(pThreadData->bCaptureThreadForceEnd)
		{
			break;
		} 	
	} 
	
		/*Stop capture device*/
	    OEDT_bAudioInStopped = FALSE;	
		s32Ret = OSAL_s32IOControl(pThreadData->hAcousticIn, OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP,(tS32)NULL);
		   
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP IN\n"));
			u32ResultBitMask_Capture |= OEDT_AUDIO_LOOPBACK_ACIN_STOP_RESULT_ERROR_BIT;
		}

		/*waiting for stop reply from AcousticIn for 5 secondsand poll on OEDT_bAudioInStopped variable to ensure 
		whether the playback device is stopped successfully*/		
		iEmergency = 50; 
		while((!OEDT_bAudioInStopped) && (iEmergency > 0))
		{
			OSAL_s32ThreadWait(100);
			iEmergency--;
		} 

		if(iEmergency <= 0)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:ERROR No STOPOUT Ack from AcousticIn within 5 seconds\n"));
			u32ResultBitMask_Capture |= OEDT_AUDIO_LOOPBACK_ACIN_STOP_ACK_RESULT_ERROR_BIT; 		
		}
		
	
    OEDT_ACOUSTICIN_PRINTF_INFO(("\n OEDT_ACOUSTIC: CAPTURE THREAD END\n"));		
    pThreadData->bCaptureThreadIsRunning  = FALSE;	
	
}

/*****************************************************************************
* FUNCTION		:  void OEDT_AUDIO_LOOPBACK_vPlaybackThread
* PARAMETER		:  pvData
* RETURNVALUE	:  None
* DESCRIPTION	:  play pcm data to AcousticOut
                              acousticout-> Alsa ->virtiosnd ->Vad->fg_audio reader
* HISTORY		:  9-7-2012, mem4kor
******************************************************************************/
static void OEDT_AUDIO_LOOPBACK_vPlaybackThread(void *pvData)
{
	tS32 Writecount= 0;
    OEDT_Audio_Loopback_ThreadData_type *pThreadData = (OEDT_Audio_Loopback_ThreadData_type*)pvData;
    tS8 *ps8PCM ;
	tU8 OEDT_u8Acoustic_test[OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT];
	unsigned short int fillcount = 0;
	ps8PCM = (tS8 *)&OEDT_u8Acoustic_test[0];
	int s32Ret;
	tS32 iEmergency;
	
	while(Writecount < OEDT_AUDIO_LOOPBACK_WRITE_LOOP_COUNT)
	{
		for(fillcount = 0;fillcount < OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT; fillcount++)
		{
			OEDT_u8Acoustic_test[fillcount] = OEDT_AUDIO_LOOPBACK_DATAPATTERN + (tU8)Writecount;	
		}
		
		s32Ret_AcousticOutWrite = OSAL_s32IOWrite(pThreadData->hAcousticOut, ps8PCM, (tU32)OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT);

		Writecount++;

		if(OSAL_E_NOERROR != (tU32)s32Ret_AcousticOutWrite)
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: "
								  "ERROR Aout Write bytes (%u)  \n", (unsigned int)OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT ));
		}
		else
		{
 			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS AOut Write Bytes (%u)\n",	(unsigned int)OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT));
		}	

		if(pThreadData->bPlaybackThreadForceEnd)
		{
			break;
		} 	
	}
	
		s32Ret = OSAL_s32IOControl(pThreadData->hAcousticOut, OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP,(tS32)NULL);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR STOP OUT\n"));
			u32ResultBitMask_Capture |= OEDT_AUDIO_LOOPBACK_ACOUT_STOP_RESULT_ERROR_BIT;			
		}

		/*waiting for stop reply from AcousticOut for 5 seconds and poll on OEDT_ACOUSTICOUT_bOutStopped variable to ensure 
		whether the playback device is stopped successfully*/	

		iEmergency = 50; 
		while((!OEDT_ACOUSTICOUT_bOutStopped) && (iEmergency > 0))
		{
			OSAL_s32ThreadWait(100);
			iEmergency--;
		} 

		if(iEmergency <= 0)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:ERROR No STOPOUT Ack from AcousticOut within 5 seconds\n"));
			u32ResultBitMask_Capture |= OEDT_AUDIO_LOOPBACK_ACOUT_STOP_ACK_RESULT_ERROR_BIT;						
		}
	
 
    OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: PLAYBACK THREAD END\n"));
	pThreadData->bPlaybackThreadIsRunning = FALSE;
	
}


/*****************************************************************************
* FUNCTION		:  OEDT_AUDIO_LOOPBACK_T001(void)
* PARAMETER		:  None
* RETURNVALUE	:  0 if Succes, bitcoded Errorvalue if failed
* DESCRIPTION	:  loop back : acousticout-> Alsa ->virtiosnd ->Vad->fg_audio
                              reader->fg_audiorenderer->vad->virtiosnd->Alsa->acousticin        
* HISTORY		:  9-7-2012, mem4kor
******************************************************************************/
tU32 OEDT_AUDIO_LOOPBACK_T001(void)
{
  tU32 u32ResultBitMask           = OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE;
  OSAL_tIODescriptor hAcousticout = OSAL_ERROR;
  OSAL_tIODescriptor hAcousticin  = OSAL_ERROR;
  tS32 s32Ret;
  tS32  iCount;
  OSAL_trAcousticOutCallbackReg rCallbackReg;
  OSAL_trAcousticInCallbackReg rACinCallbackReg;  
  OSAL_trAcousticSampleFormatCfg rSampleFormatCfg;
  tU32 u32Channels;
  OSAL_trAcousticBufferSizeCfg rCfg;
  ACOUSTICOUT_trTestModeCfg rTestModeCfg;
  tS32 iEmergency;

  /*Here route 19 is route number in stm_routes.xml which is created only for this loopback oedt with the following path
     loop back : acousticout-> Alsa ->virtiosnd ->Vad->fg_audio reader->fg_audiorenderer->vad->virtiosnd->Alsa->acousticin*/
  FG_Comp p_route = 19;
  FG_Comp filter = 0;

	OEDT_ACOUSTICIN_PRINTF_INFO(("tU32 OEDT_AUDIO_LOOPBACK enter\n"));

	s32Ret =(int)STM_requestDBRoute(p_route, &filter);
  
	if (s32Ret !=(int)STM_ER_OK)
	{
		u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_STM_REQUEST_ERROR_BIT;
		OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: Error in stream request = %u \n",(unsigned int)u32ResultBitMask));		
		return u32ResultBitMask;
	}
	else
	{
		s32Ret = (int)STM_sendCommand (&filter, FG_CMD_START);
		if (s32Ret !=(int) STM_ER_OK)
		{
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_STM_SEND_STARTCMD_ERROR_BIT;
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: Error in stream send cmd = %u \n",(unsigned int)u32ResultBitMask));		
			return u32ResultBitMask;		
		}
	}
	  
	for(iCount = 0; iCount < OEDT_AUDIO_LOOPBACK_COUNT; iCount++)
	{
		/* Reset the buffers before usage*/
		memset(OEDT_AUDIO_LOOPBACK_u8PCMBufferArray, 0, sizeof(OEDT_AUDIO_LOOPBACK_u8PCMBufferArray));

		/*OUT open /dev/acousticout/speech */
		hAcousticout = OSAL_IOOpen(OEDT_AUDIO_LOOPBACK_DEVICE_NAME_OUT, OSAL_EN_WRITEONLY);

		if(OSAL_ERROR == hAcousticout)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_DEVICE_NAME_OUT, iCount));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_OPEN_OUT_RESULT_ERROR_BIT;
			hAcousticout = OSAL_ERROR;
		}

		/*OUT register callback function */
		rCallbackReg.pfEvCallback = vAcousticOutTstCallback; /* common callback function */
		rCallbackReg.pvCookie = (tPVoid)&hAcousticout;  // cookie unused
		s32Ret = OSAL_s32IOControl(hAcousticout,
		OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION, (tS32)&rCallbackReg);
		
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR RegNotify OUT\n"));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_REG_NOTIFICATION_OUT_RESULT_ERROR_BIT;
		}
			  
		/*OUT configure channels */
		u32Channels = OEDT_AUDIO_LOOPBACK_CHANNELS_OUT;
		s32Ret = OSAL_s32IOControl(hAcousticout,OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS,(tS32)u32Channels);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS OUT: %u\n", (unsigned int)u32Channels));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETCHANNELS_OUT_RESULT_ERROR_BIT;
		}

		/*OUT configure sample format */
		rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_DEC_PCM;
		rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
		s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT OUT: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETSAMPLEFORMAT_OUT_RESULT_ERROR_BIT;
		} 
	  
		/*OUT configure buffersize */
		rCfg.enCodec     = OSAL_EN_ACOUSTIC_DEC_PCM;
		rCfg.nBuffersize = OEDT_AUDIO_LOOPBACK_BUFFERSIZE_OUT;
		s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE, (tS32)&rCfg);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE OUT: %u\n", (unsigned int)rCfg.nBuffersize));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETBUFFERSIZE_OUT_RESULT_ERROR_BIT;
		}

		/*IN open /dev/acousticin/speechreco */
		hAcousticin = OSAL_IOOpen(OEDT_AUDIO_LOOPBACK_DEVICE_NAME_IN, OSAL_EN_READONLY );
		if(OSAL_ERROR == hAcousticin)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Open IN <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_DEVICE_NAME_IN, iCount));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_OPEN_IN_RESULT_ERROR_BIT;
			hAcousticin = OSAL_ERROR;
		}
	 
		/*IN configure channels */
		u32Channels = OEDT_AUDIO_LOOPBACK_CHANNELS_IN;
		s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS, (tS32)u32Channels);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETCHANNELS IN: %u\n", (unsigned int)u32Channels));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETCHANNELS_IN_RESULT_ERROR_BIT;
		}	    

		/*IN configure sample format */
		rSampleFormatCfg.enCodec        = OSAL_EN_ACOUSTIC_ENC_PCM;
		rSampleFormatCfg.enSampleformat = OSAL_EN_ACOUSTIC_SF_S16;
		s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT, (tS32)&rSampleFormatCfg);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETSAMPLEFORMAT IN: %u\n", (unsigned int)rSampleFormatCfg.enSampleformat));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETSAMPLEFORMAT_IN_RESULT_ERROR_BIT;
		} 

		/*IN configure buffersize */
		rCfg.enCodec     = OSAL_EN_ACOUSTIC_ENC_PCM;
		rCfg.nBuffersize = OEDT_AUDIO_LOOPBACK_BUFFERSIZE_IN;
		s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE, (tS32)&rCfg);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR SETBUFFERSIZE IN: %u\n", (unsigned int)rCfg.nBuffersize));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_SETBUFFERSIZE_IN_RESULT_ERROR_BIT;
		}	  

		/*IN register callback function */
		{
		  rACinCallbackReg.pfEvCallback = vAcousticInTstCallback;
		  rACinCallbackReg.pvCookie = (tPVoid)&hAcousticin;  // cookie unused
		  s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_REG_NOTIFICATION, (tS32)&rACinCallbackReg);
		
		  if(OSAL_OK != s32Ret)
		  {
			  OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR REGISTER CALLBK IN\n"));
			  u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_REG_NOTIFICATION_IN_RESULT_ERROR_BIT;
		
		  }
		} 

		/*IN start command */
		s32Ret = OSAL_s32IOControl(hAcousticin, OSAL_C_S32_IOCTRL_ACOUSTICIN_START, (tS32)NULL);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START IN\n"));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_START_IN_RESULT_ERROR_BIT;
		}
		else 
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS START IN\n"));
		} 

		/*OUT start command */
		s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_START, (tS32)NULL);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR START OUT\n"));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_START_OUT_RESULT_ERROR_BIT;
		}
		else 
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTICOUT: START SUCCESS\n"));
		} 

		OEDT_AUDIO_LOOPBACK_threadData.hAcousticIn  = hAcousticin;
		OEDT_AUDIO_LOOPBACK_threadData.hAcousticOut = hAcousticout;
		OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadIsRunning  = TRUE;
		OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadIsRunning = TRUE;
		OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadForceEnd   = FALSE;
		OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadForceEnd  = FALSE;
		
		u32ResultBitMask_Capture = OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE;
		u32ResultBitMask_Playback = OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE;

		/*run capture-thread*/
		OEDT_AUDIO_LOOPBACK_captureThreadID = OSAL_ERROR;
		OEDT_AUDIO_LOOPBACK_captureThreadID = OSAL_ThreadSpawn(&OEDT_AUDIO_LOOPBACK_captureThreadAttr);    
		if(OSAL_ERROR == OEDT_AUDIO_LOOPBACK_captureThreadID)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Capture Thread <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_captureThreadAttr.szName, iCount));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_CAPTURE_THREADSPAWN_RESULT_ERROR_BIT;
		}
		else 
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Capture Thread <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_captureThreadAttr.szName, iCount));
		} 
        OSAL_s32ThreadWait(250);
	  
		/*run playback-thread*/
		OEDT_AUDIO_LOOPBACK_playbackThreadID = OSAL_ERROR;
		OEDT_AUDIO_LOOPBACK_playbackThreadID = OSAL_ThreadSpawn(&OEDT_AUDIO_LOOPBACK_playbackThreadAttr);    
		if(OSAL_ERROR == OEDT_AUDIO_LOOPBACK_playbackThreadID)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR Create Playback Thread <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_playbackThreadAttr.szName, iCount));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_PLAYBACK_THREADSPAWN_RESULT_ERROR_BIT;
		}
		else 
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: SUCCESS Creating Playback Thread <%s> (count %d)\n",OEDT_AUDIO_LOOPBACK_playbackThreadAttr.szName, iCount));
		} 
	        
		/* Wait for Capture and Playback threads to exit for 15secs*/
		iEmergency = 75; // As stop is also performed inside the threads, this value is increased to 75 
		while((OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadIsRunning
			||OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadIsRunning)
			&&(iEmergency > 0) )
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 1, iEmergencyExit %d\n", iEmergency));
			iEmergency--;
			OSAL_s32ThreadWait(1000);
		}
	  
		OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadForceEnd   = TRUE;
		OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadForceEnd  = TRUE;

		/*wait for threads again*/
		iEmergency = 6; //Increased from 5 to 6 to accomodate stop
		while((OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadIsRunning
			||OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadIsRunning)
			&&(iEmergency > 0))
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: WAIT FOr THREAD END 2, iEmergencyExit %d\n", iEmergency));		
			iEmergency--;
			OSAL_s32ThreadWait(1000);
		}
	  
		/* delete Playback Thread if running yet */
		if(OEDT_AUDIO_LOOPBACK_threadData.bPlaybackThreadIsRunning)
		{
			if(OSAL_ERROR != OEDT_AUDIO_LOOPBACK_playbackThreadID)
			{
				OSAL_s32ThreadDelete(OEDT_AUDIO_LOOPBACK_playbackThreadID);
				OEDT_AUDIO_LOOPBACK_playbackThreadID = OSAL_ERROR;
			}
		}

		/* delete Cature Thread if running yet*/
		if(OEDT_AUDIO_LOOPBACK_threadData.bCaptureThreadIsRunning)
		{
			if(OSAL_ERROR != OEDT_AUDIO_LOOPBACK_captureThreadID)
			{
				OSAL_s32ThreadDelete(OEDT_AUDIO_LOOPBACK_captureThreadID);
				OEDT_AUDIO_LOOPBACK_captureThreadID = OSAL_ERROR;
			}
		}

		/*OUT turn off Testmode*/
		rTestModeCfg.iSizeOfStruct         = sizeof(rTestModeCfg);
		rTestModeCfg.bTestModeIsOn         = FALSE;
		rTestModeCfg.bMono2StereoUpmixIsOn = FALSE;
		s32Ret = OSAL_s32IOControl(hAcousticout, OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETTIME, (tS32)&rTestModeCfg);
		if(OSAL_OK != s32Ret)
		{
			OEDT_ACOUSTICIN_PRINTF_INFO(("OEDT_ACOUSTIC: ERROR Clear TestMode OUT\n"));
		}
		
		u32ResultBitMask |= u32ResultBitMask_Playback;
		u32ResultBitMask |= u32ResultBitMask_Capture;

		
		/*ACOUT  close /dev/acousticout/speech */
		if (hAcousticout != OSAL_ERROR)
		{
			s32Ret = OSAL_s32IOClose(hAcousticout);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AOUT CLOSE handle %u (count %d)\n", (unsigned int)hAcousticout, iCount));
				u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_CLOSE_OUT_RESULT_ERROR_BIT;
			}
		}

		/*ACIN  close /dev/acousticin/speechreco */
		if (hAcousticin != OSAL_ERROR)		
		{
			s32Ret = OSAL_s32IOClose(hAcousticin);
			if(OSAL_OK != s32Ret)
			{
				OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: ERROR AIN CLOSE handle %u (count %d)\n", (unsigned int)hAcousticin, iCount));
				u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_CLOSE_IN_RESULT_ERROR_BIT;
			}
		}

		OSAL_s32ThreadWait(250);	  

	if(s32Ret_AcousticInRead <= 0)
	{
		u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_ACOUSTICIN_READ_ERROR_BIT;
	}
	if(OSAL_E_NOERROR!=(tU32)s32Ret_AcousticOutWrite)
	{
		u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_ACOUSTICOUT_WRITE_ERROR_BIT;
	}

	if(OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE != u32ResultBitMask)
	{	
		break;
	}	
	
	} 


	if(OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE == u32ResultBitMask)
	{	
		if(OEDT_COMPARE_DATA_MATCH!=u32_CompareData)
		{
			OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:  Playback and Capture data MISMATCHED \n"));
			u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_PLAYBACKDATA_CAPTUREDATA_MISMATCH;
		}	
	}

	s32Ret = (int)STM_sendCommand (&filter, FG_CMD_STOP);
	
	if (s32Ret !=(int) STM_ER_OK)
	{
		u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_STM_SEND_STOPCMD_ERROR_BIT;
		OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: Error in stream send cmd = %u \n",(unsigned int)u32ResultBitMask)); 	
		return u32ResultBitMask;		
	}


	/*free Audio route*/
	s32Ret = (int)STM_freeRoute(&filter);
	if (s32Ret !=(int)STM_ER_OK)
	{
		u32ResultBitMask |= OEDT_AUDIO_LOOPBACK_STM_FREE_ERROR_BIT;
		OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC: Error in stream route freeing = %u \n",(unsigned int)u32ResultBitMask));		
		return u32ResultBitMask;
	}
	
	if(OEDT_AUDIO_LOOPBACK_RESULT_OK_VALUE != u32ResultBitMask)
	{
		OEDT_ACOUSTICIN_PRINTF_ERROR(("OEDT_ACOUSTIC:bit coded ERROR: 0x%08X\n",(unsigned int)u32ResultBitMask));
	} 

	return u32ResultBitMask;
} 





