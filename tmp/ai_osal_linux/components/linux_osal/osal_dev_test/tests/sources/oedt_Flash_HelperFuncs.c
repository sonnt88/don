/******************************************************************************
 * Copyright (C) Blaupunkt GmbH, [year]
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ************************************************************************/

/************************************************************************/
/*! \file  oedt_Flash_HelperFuncs.c
 *\brief    oedt Flash-test functions-cases

 *\author		CM-DI/PJ-GM32 - Resch

 *\par Copyright: 
 *(c) *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH

 *\par History:  

 **********************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|----------------------------------------------------------------*/

/*------  standard includes -------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"




/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
//#define OEDT_FLASH_HELPER_FILE_NAME                ((tCString) "/dev/nor0/paralleltest4.txt")
#define OEDT_FLASH_HELPER_BLOCKSIZE                ((tInt) 1024 << 2) // blocksize for read / write
#define OEDT_FLASH_HELPER_NR_CYCLES                ((tInt) 20)
#define OEDT_FLASH_HELPER_MAX_READ_TASKS           ((tInt) 10)
#define OEDT_FLASH_HELPER_OUTPUT_RATE              ((tInt) OEDT_FLASH_HELPER_NR_CYCLES >> 2) //OEDT_FLASH_HELPER_NR_CYCLES >> 2)
#define OEDT_FLASH_HELPER_ERRMEM_ERASE_RATE        ((tInt) 1)
#define OEDT_FLASH_HELPER_READ_TSK_NAME            ((tCString) "READ_THR")
#define OEDT_FLASH_HELPER_WRITE_TSK_NAME           ((tCString) "WRITE_THR")
#define OEDT_FLASH_HELPER_ERRMEM_TSK_NAME           ((tCString) "ERR_MEM_THR")
//#define OEDT_FLASH_HELPER_SLICETIME                ((tInt) 5) /* slice time of tasks in ms */

#define OEDT_FLASH_HELPER_NUM_READ_TASKS           ((tInt)  10)
#define OEDT_FLASH_HELPER_NUM_WRITE_TASKS          ((tInt)  1)
#define OEDT_FLASH_HELPER_NUM_ERR_MEM_TASKS        ((tInt)  1)
#define OEDT_FLASH_HELPER_NUM_TASKS                (OEDT_FLASH_HELPER_NUM_WRITE_TASKS+OEDT_FLASH_HELPER_NUM_READ_TASKS+OEDT_FLASH_HELPER_NUM_ERR_MEM_TASKS)

#define OEDT_FLASH_HELPER_SWAP_PRIORITIES

#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
#define OEDT_FLASH_HELPER_PRIO_SWAP_CYCLE          ((tU32) 100)
#define OEDT_FLASH_HELPER_HIGH_PRIO                ((tU32) 35)
#define OEDT_FLASH_HELPER_LOW_PRIO                 ((tU32) 120)
#endif /*#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES*/


/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/
typedef struct _tag_task_info{
    tInt           n_task_parameter;
    OSAL_tThreadID n_task_id;
    tBool          b_error_occured;
}trOedtFlash_WorkingTaskInfo;
/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
static trOedtFlash_WorkingTaskInfo  n_task_info[OEDT_FLASH_HELPER_NUM_TASKS];
static tInt                         n_used_task_info_entries = 0;

static tInt                         n_num_ready_tasks    = 0;
static tInt                         n_num_finished_tasks = 0;
/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/
static tS32 oedt_flash_helper_start_task(OSAL_tpfThreadEntry funcptr, int prio,int nr, char *tsk_name);
int read_block(OSAL_tIODescriptor hFile,size_t block_size,int read_position, char *buffer);
static void thread_write_file(trOedtFlash_WorkingTaskInfo *p_task_info);
static void read_thr(trOedtFlash_WorkingTaskInfo *p_task_info);
static void err_mem_thread(trOedtFlash_WorkingTaskInfo *p_task_info);
/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/
static    tU8 * file_name;

static tS32 oedt_flash_helper_start_task(OSAL_tpfThreadEntry funcptr, int prio,int nr, char *tsk_name)
{
    OSAL_trThreadAttribute tr_thread_attr; 

    tS32 s32_err;
    /*
     * create driver starting task 
     */
    
    if(n_used_task_info_entries >= OEDT_FLASH_HELPER_NUM_TASKS){
        return -1;
    }
    n_task_info[n_used_task_info_entries].n_task_parameter = nr;
   
   tr_thread_attr.u32Priority  = (tU32)prio;
   tr_thread_attr.s32StackSize = 2048;
   tr_thread_attr.szName       = tsk_name;
   tr_thread_attr.pfEntry      = funcptr;
   tr_thread_attr.pvArg        = &(n_task_info[n_used_task_info_entries]);   
   
   s32_err = OSAL_ThreadSpawn(&tr_thread_attr);
   if (s32_err == OSAL_ERROR){
       OEDT_HelperPrintf(TR_LEVEL_USER_1,"creation of task failed /n");
       return -31; 
   }

   n_task_info[n_used_task_info_entries].n_task_id = s32_err;
   n_used_task_info_entries++;
   
   return 0;   
}   
    
/*  
 *  read_block
 *  reads a block and compares it to the expected value
 *  Return Value: 0 OK
 *                <0 -> error
 */ 
int read_block(OSAL_tIODescriptor hFile,size_t block_size,int read_position, char *buffer)
{
    tS32 s32_err=0;
    int current_read_position=0;
    int i=0;
    char expected_res = 0;
    char expected_res_alternate = 0;
    
    current_read_position= read_position*OEDT_FLASH_HELPER_BLOCKSIZE;
    
    s32_err = OSAL_s32IOControl (hFile,
                                 OSAL_C_S32_IOCTRL_FIOSEEK,
                                 current_read_position);
    
    if (s32_err == OSAL_ERROR){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"Setting position for reading failed read_position=%d ",read_position*OEDT_FLASH_HELPER_BLOCKSIZE);
        return -20;
    }
    
    s32_err = OSAL_s32IORead (hFile,
                              (tPS8) buffer,
                              (tU32) block_size);
    
    if (s32_err != (tS32)block_size){
        return -21;	
    }
    
    s32_err=0;
    expected_res = (char) read_position;
    expected_res_alternate = (char) (read_position | 0x80);
    for(i=0;i<OEDT_FLASH_HELPER_BLOCKSIZE;i++){
        if ((buffer[i] != expected_res) && 
            (buffer[i] != expected_res_alternate)){
            
            OEDT_HelperPrintf(TR_LEVEL_USER_1," Read Error: hFile=0x%08X  Expected result=0x%02X Result=0x%02X ",
                              hFile,
                              expected_res,
                              (buffer[i] & 0x00FF));
            s32_err--;
        }
    }
    
    return s32_err;
}	

void thread_write_file(trOedtFlash_WorkingTaskInfo *p_task_info)
{
    tS32 s32_err=0;         
    OSAL_tIODescriptor hFile = 0;
    char *buffer;
    char alternate_flag = 0;
    int n_wait_cnt = 0;
    int i=0;
    int j=0;  
    int k=0;  
    int w_error_count=0;
    int r_error_count=0;
    int current_write_position=0;
#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
    tU32 u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
#endif
    tBool b_initial_write_done = FALSE;

 //   tCString file_name=OEDT_FLASH_HELPER_FILE_NAME;

    
    size_t block_size=OEDT_FLASH_HELPER_BLOCKSIZE;	  
    
    buffer=(char*) OSAL_pvMemoryAllocate(block_size);
    if(buffer == OSAL_NULL){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread0_write_file: MemAlllocation failed");
        n_num_finished_tasks++;
        OSAL_vThreadExit();
		return;
    }
    
    OSAL_pvMemorySet(buffer, 0, block_size);
    
    /* create file */
    
    hFile = OSAL_IOCreate ((tCString)file_name,  OSAL_EN_READWRITE);
    if (hFile == OSAL_ERROR){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: ERROR on create");
        OSAL_vMemoryFree( buffer);
        n_num_finished_tasks++;
        OSAL_vThreadExit();
    }else{
        s32_err=OSAL_s32IOClose(hFile);
    }
    
    b_initial_write_done = FALSE;
    
    if(n_wait_cnt >= 500){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: Reader tasks have not started yet");
    }else{ /*if(n_wait_cnt >= 500){*/
        
        hFile = OSAL_IOOpen( (tCString)file_name, OSAL_EN_READWRITE );
        if(hFile == OSAL_ERROR){
            OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: ERROR: open for writing failed");                         
        }else{ /*if(hFile == OSAL_ERROR){*/      
            for(i=0; i<OEDT_FLASH_HELPER_NR_CYCLES; i++){

#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
                if((tU32)i%OEDT_FLASH_HELPER_PRIO_SWAP_CYCLE == 0){
                    if(u32_curr_prio == OEDT_FLASH_HELPER_HIGH_PRIO){
                        u32_curr_prio = OEDT_FLASH_HELPER_LOW_PRIO;
                    }else{
                        u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
                    }
                    if(OSAL_s32ThreadPriority( OSAL_ProcessWhoAmI(), 
                                               u32_curr_prio) == OSAL_ERROR){
                        OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: ERROR: Failed to set threads priority");
                    }
                }
#endif


                if(i%OEDT_FLASH_HELPER_OUTPUT_RATE==0){
                    
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: Cycle %d (%d) passed Errors: Write=%d Read=%d ",
                                      i,
                                      OEDT_FLASH_HELPER_NR_CYCLES,
                                      w_error_count,
                                      r_error_count);
                    
                    /* OSAL_s32ThreadWait(500); */
                } 
                
                alternate_flag = alternate_flag ^ 0x01;

                for(j=0; j<OEDT_FLASH_HELPER_MAX_READ_TASKS; j++){
                    
                    for(k=0;k<(int)block_size;k++){
                        buffer[k] = (char)j;
                        buffer[k] |= (char) (alternate_flag << 7);
                    }
                    
                    current_write_position= j*OEDT_FLASH_HELPER_BLOCKSIZE;
                    s32_err = OSAL_s32IOControl (hFile,
                                                 OSAL_C_S32_IOCTRL_FIOSEEK,
                                                 current_write_position);
                    
                    if (s32_err == OSAL_ERROR){
                        OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: Setting position for writing to %d failed ",current_write_position);
                        w_error_count++;
                    }
                    s32_err = OSAL_s32IOWrite (hFile, 
                                               (tPS8) buffer,
                                               (tU32) block_size);
                    if (s32_err != (tS32)block_size){
                        w_error_count++;
                    }
                }

                if(b_initial_write_done == FALSE){
                    n_num_ready_tasks++;
                    while((n_num_ready_tasks < OEDT_FLASH_HELPER_NUM_TASKS) &&
                          (n_wait_cnt < 500)){
                        n_wait_cnt++;
                        OSAL_s32ThreadWait(10);
                    }
                }

                b_initial_write_done = TRUE;

                /* read file content */
                for(j=0; j<OEDT_FLASH_HELPER_MAX_READ_TASKS; j++){  
                    
                    OSAL_pvMemorySet(buffer, 0, block_size);
                    s32_err = read_block(hFile,block_size,j,buffer);
                    if (s32_err != 0){
                        r_error_count++;
                    }
                }
            }  
            
            if ((OSAL_s32IOClose ( hFile )) == OSAL_ERROR){
                OEDT_HelperPrintf(TR_LEVEL_USER_1,"thread_write_file: Close file error");
            }
        }/*if(hFile == OSAL_ERROR){*/
    }/*if(n_wait_cnt >= 500){*/
    OSAL_vMemoryFree( buffer);
    OSAL_s32IORemove ((tCString)file_name );
    if((r_error_count != 0) ||
       (w_error_count != 0)){
        p_task_info->b_error_occured = TRUE;
        n_num_finished_tasks++;

        OSAL_vThreadExit();
    }
    p_task_info->b_error_occured = FALSE;
    n_num_finished_tasks++;
    OSAL_vThreadExit();
}


int read_block_from_file(int read_position)
{
    tS32 s32_err=0;         
    OSAL_tIODescriptor hFile = 0;
    char *buffer;    
    int j=0;
    int r_error_count=0;   
#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
    tU32 u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
#endif                                                                                                                            
//    tCString file_name=OEDT_FLASH_HELPER_FILE_NAME;
    size_t block_size=OEDT_FLASH_HELPER_BLOCKSIZE;	  
    
    buffer=(char*) OSAL_pvMemoryAllocate(block_size);
    if(buffer == OSAL_NULL){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"read_block_from_file: MemAlllocation failed");
        return -1;
    }
    OSAL_pvMemorySet(buffer, 0, block_size);    
    
    
    hFile = OSAL_IOOpen((tCString) file_name, OSAL_EN_READONLY );
    if(hFile == OSAL_ERROR){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"ERROR: open for reading failed");
        OSAL_vMemoryFree( buffer);
        return -2;
    }
    
    /* read file content */
    for(j=0; j<OEDT_FLASH_HELPER_NR_CYCLES; j++){

#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
        if((tU32)j%OEDT_FLASH_HELPER_PRIO_SWAP_CYCLE == 0){
            if(u32_curr_prio == OEDT_FLASH_HELPER_HIGH_PRIO){
                u32_curr_prio = OEDT_FLASH_HELPER_LOW_PRIO;
            }else{
                u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
            }
            if(OSAL_s32ThreadPriority( OSAL_ProcessWhoAmI(), 
                                       u32_curr_prio) == OSAL_ERROR){
                OEDT_HelperPrintf(TR_LEVEL_USER_1,"read_block_from_file: ERROR: Failed to set threads priority");
            }
        }
#endif


        if(j%OEDT_FLASH_HELPER_OUTPUT_RATE==0){
            OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Task[%d] Cycle %d (%d) passed. Errors=%d ",
                              read_position,
                              j,
                              OEDT_FLASH_HELPER_NR_CYCLES,
                              r_error_count);
            
            /* OSAL_s32ThreadWait(500);*/
        }
        
        OSAL_pvMemorySet(buffer, 0, block_size);
        s32_err = read_block(hFile,block_size,read_position, buffer);
        if (s32_err != 0){
            r_error_count++;	
        }
    }  
    OSAL_s32IOClose(hFile);
    OSAL_vMemoryFree( buffer);
    
    if(r_error_count != 0){
        return -100;
    }
    return 0;
}


/* read threads started via script in seperate tasks */

void read_thr(trOedtFlash_WorkingTaskInfo *p_task_info)
{
	int err=0;
        int     n_wait_cnt = 0;
        n_num_ready_tasks++;
        while((n_num_ready_tasks < OEDT_FLASH_HELPER_NUM_TASKS) &&
              (n_wait_cnt < 500)){
            n_wait_cnt++;
            OSAL_s32ThreadWait(10);
        }
        
        if(n_wait_cnt >= 500){
            OEDT_HelperPrintf(TR_LEVEL_USER_1,"read_thr: Reader tasks have not started yet");
        }else{ /*if(n_wait_cnt >= 500){*/
            
            err=read_block_from_file(p_task_info->n_task_parameter);
        }
        if(err != 0){
            p_task_info->b_error_occured = TRUE;
        }else{
            p_task_info->b_error_occured = FALSE;
        }
        n_num_finished_tasks++;
        OSAL_vThreadExit();
}

static const char* Entry_text_normal = "Normal error: 0x 12345678 \0";

int write_block_to_errmem(){

    int err=0;
    trErrmemEntry glob_entry;
    OSAL_tIODescriptor hDevice = 0;
    int i=0;
#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
    tU32 u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
#endif
    /* Length of the data string to be written*/
    glob_entry.u16EntryLength = (tU16)strlen(Entry_text_normal); 
    
    /* Copy data into the buffer */
    OSAL_pvMemoryCopy(glob_entry.au8EntryData,
                      Entry_text_normal,glob_entry.u16EntryLength);
    /* Specify the type of entry */
    glob_entry.eEntryType = eErrmemEntryNormal;

    hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );

    if (OSAL_ERROR == hDevice){    
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"write_block_to_errmem: Failed to open dev errmem");
        err = -1;
    }else{
        for(i=0;i<OEDT_FLASH_HELPER_NR_CYCLES;i++){
#ifdef OEDT_FLASH_HELPER_SWAP_PRIORITIES
            if((tU32)i%OEDT_FLASH_HELPER_PRIO_SWAP_CYCLE == 0){
                if(u32_curr_prio == OEDT_FLASH_HELPER_HIGH_PRIO){
                    u32_curr_prio = OEDT_FLASH_HELPER_LOW_PRIO;
                }else{
                    u32_curr_prio = OEDT_FLASH_HELPER_HIGH_PRIO;
                }
                if(OSAL_s32ThreadPriority( OSAL_ProcessWhoAmI(), 
                                           u32_curr_prio) == OSAL_ERROR){
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"write_block_to_errmem: ERROR: Failed to set threads priority");
                }
            }
#endif
            glob_entry.u16Entry = (tU16)OSAL_s32Random();
            OSAL_s32IOWrite(hDevice, 
                            (tPCS8)(&glob_entry), 
                            sizeof(glob_entry));
            if((i%OEDT_FLASH_HELPER_OUTPUT_RATE) == 0){
                OEDT_HelperPrintf(TR_LEVEL_USER_1,"write_block_to_errmem: One err mem block written");
            }

            if((i%OEDT_FLASH_HELPER_ERRMEM_ERASE_RATE) == 0){
                if(OSAL_s32IOControl (hDevice,
                                      OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,
                                      ERRMEM_CLEAR_MAGIC_VALUE) == OSAL_ERROR){
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"write_block_to_errmem: Failed to clear ErrMem");
                } /*if(OSAL_s32IOControl (hDevice,*/
            } /*if((i%OEDT_FLASH_HELPER_ERRMEM_ERASE_RATE) == 0){*/
            
        }
        OSAL_s32IOClose(hDevice); 
    }
    return(err);
}

void  err_mem_thread(trOedtFlash_WorkingTaskInfo *p_task_info){

    int err=0;
    int     n_wait_cnt = 0;
    n_num_ready_tasks++;
    while((n_num_ready_tasks < OEDT_FLASH_HELPER_NUM_TASKS) &&
          (n_wait_cnt < 500)){
        n_wait_cnt++;
        OSAL_s32ThreadWait(10);
    }
    
    if(n_wait_cnt >= 500){
        OEDT_HelperPrintf(TR_LEVEL_USER_1,"err_mem_thread: Reader tasks have not started yet");
    }else{ /*if(n_wait_cnt >= 500){*/
        
        err=write_block_to_errmem();
    }
    if(err != 0){
        p_task_info->b_error_occured = TRUE;
    }else{
        p_task_info->b_error_occured = FALSE;
    }
    n_num_finished_tasks++;
    OSAL_vThreadExit();
    
}

/* start tasks */
tS32 s32_start_all_working_tsk(tU8 * pu8ArgPathname)
{                                                     
    int err=0;                                  
    int i=0;
    tChar sz_thread_name[32] = {'\0'};
    int     n_wait_cnt = 0;
	file_name = (tU8 *)pu8ArgPathname;
    for(i=0; i < OEDT_FLASH_HELPER_NUM_TASKS;i++){
        n_task_info[i].n_task_id        = -1;
        n_task_info[i].n_task_parameter = -1;
        n_task_info[i].b_error_occured = TRUE;
    }
    n_used_task_info_entries = 0;
    n_num_ready_tasks    = 0;
    n_num_finished_tasks = 0;
    
    for( i=0;i<OEDT_FLASH_HELPER_NUM_WRITE_TASKS;i++){
        OSALUTIL_s32NPrintFormat((tPS8)sz_thread_name,
                                 sizeof(sz_thread_name), 
                                 "%s%d",
                                 OEDT_FLASH_HELPER_WRITE_TSK_NAME,i);
        oedt_flash_helper_start_task(((FP) thread_write_file), 60,-1,sz_thread_name);
    }
    for( i=0;i<OEDT_FLASH_HELPER_NUM_READ_TASKS;i++){
        OSALUTIL_s32NPrintFormat((tPS8)sz_thread_name,
                                 sizeof(sz_thread_name),
                                 "%s%d",
                                 OEDT_FLASH_HELPER_READ_TSK_NAME,i);
        oedt_flash_helper_start_task(((FP)read_thr), 60,i,sz_thread_name);
    }
    for( i=0;i<OEDT_FLASH_HELPER_NUM_ERR_MEM_TASKS;i++){
        OSALUTIL_s32NPrintFormat((tPS8)sz_thread_name,
                                 sizeof(sz_thread_name),
                                 "%s%d",
                                 OEDT_FLASH_HELPER_ERRMEM_TSK_NAME,i);
        oedt_flash_helper_start_task(((FP)err_mem_thread), 60,i,sz_thread_name);
    }
    
    n_wait_cnt = 0;
    while((n_num_finished_tasks < OEDT_FLASH_HELPER_NUM_TASKS) &&
          (n_wait_cnt < 1000)){
        n_wait_cnt++;
        OSAL_s32ThreadWait(1000);
    }
    for(i=0; i < OEDT_FLASH_HELPER_NUM_TASKS;i++){
        OSAL_s32ThreadDelete(n_task_info[i].n_task_id);
    }
    
    if(n_wait_cnt < 1000){
        for(i=0; i < OEDT_FLASH_HELPER_NUM_TASKS;i++){
            if(n_task_info[i].b_error_occured == TRUE){
                err--;
            }
        }
    }else{
        err = -100;
    }

    return err;                                         
}   
