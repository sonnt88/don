/******************************************************************************
 * FILE         : oedt_UART_TestFuncs.c
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the PRAM 
 *                device for Gen2 hardware.
 *              
 * AUTHOR(s)    :  Ulrich Schulz / TMS 
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date          | 		     		           | Author & comments
 * --.--.--      | Initial revision            | ------------
 *17,August,2010 | version 1.0                 | Ulrich Schulz / TMS 
 *01 Jun, 2015	 | Lint Fix:CFG3-1003          | Ranga Swamy V (RBEI/ECF5)
 *-----------------------------------------------------------------------------
*******************************************************************************/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"


/************************************************************************
 * FUNCTION:      u32_oedt_pram_devOpenClose 
 * DESCRIPTION:   1.Opens the device.
 * 			      2.Close the device. 			     
 *
 * PARAMETER:     Nil
 *             
 * RETURNVALUE:   Open and Close PRAM Device
 * HISTORY:       05.09.11  Martin Langer (CM-AI/CF-33)
 *
 * ************************************************************************/
tU32 u32_oedt_pram_devOpenClose(void)
{
    OSAL_tIODescriptor hDevice;
    tU32 u32Error = 0;
    tU32 u32ret;

    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRAM, OSAL_EN_READWRITE );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Error = OSAL_u32ErrorCode();
		if (u32Error != OSAL_E_DOESNOTEXIST) 
		{		
			u32Error &= ~OSAL_C_COMPONENT;
			u32Error &= ~OSAL_C_ERROR_TYPE;
			OEDT_HelperPrintf( (tU8)TR_LEVEL_FATAL, "ERROR: OSAL_IOOpen() failed, errcode %i", u32Error);
			return 1000 + u32Error;
		}
		// expected return value is OSAL_E_DOESNOTEXIST
		return 0;
	} 
	else 
	{
		OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "ERROR: OSAL_IOOpen() was fine, but not allowed");
		u32ret = 2000;
	}

	if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	{
    	u32Error = OSAL_u32ErrorCode();
		u32Error &= ~OSAL_C_COMPONENT;
		u32Error &= ~OSAL_C_ERROR_TYPE;			
		OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "ERROR: OSAL_s32IOClose() failed, errcode %i", u32Error);
		u32ret = (3000 + u32Error);
	}
	
	return u32ret;
} // End of u32_oedt_pram_devOpenClose()


tU32 u32_oedt_pram_test(tVoid)
{
    /*
    Fix for Lint ticket CFG3-1003
    Variables s32temp_position & s32temp_size_of_device have been added to fix the lint errors 530 & 774.
    */
    OSAL_tIODescriptor d;
    tS32 s32temp_position;
    tS32 *ps32position;
    ps32position = &s32temp_position;
    tS32 s32ret;
    char buffer[256];
    const char teststring[] = "teststring";
    tU32 u32teststring_len;
    tS32 s32temp_size_of_device;
    tS32 *ps32size_of_device;
    ps32size_of_device = &s32temp_size_of_device;

    u32teststring_len = strlen(teststring)+1;
    /* open device */
    /* OSAL_C_STRING_DEVICE_PRAM_ERRMEM is "/dev/pram/errmem" */
    d = OSAL_IOOpen("/dev/pram/reserved" , OSAL_EN_READWRITE);
    if(OSAL_ERROR == d)
    {
        return 1;
    }
    /* erase content */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 2;
    }
    /* try to read (must give an error) */
    s32ret = OSAL_s32IORead(d, (tPS8)buffer, sizeof(buffer));
    if(OSAL_ERROR != s32ret)
    {
        return 3;
    }
    /* seek to position 0*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 4;
    }
    /* seek to position 4*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 4);
    if(OSAL_ERROR != s32ret)
    {
        return 5;
    }
    /* seek to position 0 again*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 6;
    }
    /* write teststring */
    s32ret = OSAL_s32IOWrite(d, (tPCS8)teststring, u32teststring_len);
    if(s32ret != (tS32)u32teststring_len)
    {
        return 7;
    }
    /* write teststring again*/
    s32ret = OSAL_s32IOWrite(d, (tPCS8)teststring, u32teststring_len);
    if(s32ret != (tS32)u32teststring_len)
    {
        return 8;
    }
    /* seek to position 0 again*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 9;
    }
    /* read teststring (the first copy) */
    s32ret = OSAL_s32IORead(d, (tPS8)buffer, u32teststring_len);
    if(s32ret != (tS32)u32teststring_len)
    {
        return 10;
    }
    /* compare data */
    if(strcmp(buffer, teststring) != 0)
    {
        return 11;
    }
    /* test position (with wrong arg) */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_TELL, 0);
    if(OSAL_ERROR != s32ret)
    {
        return 12;
    }
    /* test position (with right arg) */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_TELL, (tS32)ps32position);
    if(OSAL_ERROR == s32ret)
    {
        return 13;
    }
    if(*ps32position != (tS32)u32teststring_len)
    {
        return 14;
    }
    /* seek to position 0 again*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 15;
    }
    /* read teststring (was written twice) */
    s32ret = OSAL_s32IORead(d, (tPS8)buffer, sizeof(buffer));
    if(s32ret != (tS32)u32teststring_len*2)
    {
        return 16;
    }
    /* reset content*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 17;
    }
    /* check position */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_TELL, (tS32)ps32position);
    if((OSAL_ERROR == s32ret) || (*ps32position != 0))
    {
        return 18;
    }
    /* try to read (must give an error) */
    s32ret = OSAL_s32IORead(d, (tPS8)buffer, sizeof(buffer));
    if(OSAL_ERROR != s32ret)
    {
        return 19;
    }
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_GET_SIZE, (tS32)ps32size_of_device);
    if(OSAL_ERROR == s32ret)
    {
        return 20;
    }
    do
    {
        /* write to the complete device */
        s32ret = OSAL_s32IOWrite(d, (tPCS8)teststring, u32teststring_len);
    } while (s32ret != OSAL_ERROR);

    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_TELL, (tS32)ps32position);
    if((OSAL_ERROR == s32ret) || (*ps32position != *ps32size_of_device))
    {
        return 21;
    }
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE, (tS32)ps32position);
    if((OSAL_ERROR == s32ret) || (*ps32position != *ps32size_of_device))
    {
        return 22;
    }
    /* seek to position 0 again*/
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 23;
    }
    /* compare read data size with overall size */
    do
    {
        /* write to the complete device */
        s32ret = OSAL_s32IORead(d, (tPS8)buffer, sizeof(buffer));
        if(s32ret != OSAL_ERROR)
        {
            *ps32size_of_device -= s32ret;
        }
    } while (s32ret != OSAL_ERROR);
    if(*ps32size_of_device != 0)
    {
        return 24;
    }

    /* erase content */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR, 0);
    if(OSAL_ERROR == s32ret)
    {
        return 25;
    }
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE, (tS32)ps32position);
    if((OSAL_ERROR == s32ret) || (*ps32position != 0))
    {
        return 26;
    }
    /* close device*/
    s32ret = OSAL_s32IOClose(d);
    if(OSAL_ERROR == s32ret)
    {
        return 27;
    }
    /* try to read (must fail) */
    s32ret = OSAL_s32IORead(d, (tPS8)buffer, sizeof(buffer));
    if(OSAL_ERROR != s32ret)
    {
        return 28;
    }
    /* write teststring (must fail) */
    s32ret = OSAL_s32IOWrite(d, (tPCS8)teststring, u32teststring_len);
    if(OSAL_ERROR != s32ret)
    {
        return 29;
    }
    /* try to make an iocontrol (must fail) */
    s32ret = OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0);
    if(OSAL_ERROR != s32ret)
    {
        return 30;
    }
    return 0;
}

