/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        oedt_FFD_TestFuncs.c
 *
 *  O(E)DT test for the osale device dev_ffd
 *
**********************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

#include  "oedt_FFD_TestFuncs.h"

/**********************************************************************************/
#define OEDT_FFD_BUFFER_SIZE_OEDT     128
#define OEDT_FFD_SAVE_COMPLETE_MAX     33

/**********************************************************************************/
/*   enum for test                                                                */
/**********************************************************************************/

/**********************************************************************************/
/*   static variable                                                              */
/**********************************************************************************/
static tU8 *vpu8data = OSAL_NULL;

/**********************************************************************************/
/*   static function                                                              */
/**********************************************************************************/
#if OSAL_OS==OSAL_TENGINE 
static tU32 u32DrvFfd_Test_SaveForTest( tVoid );
static tU32 u32DrvFfd_Test_RestoreAfterTest( tVoid );
#endif

#if OSAL_OS==OSAL_TENGINE 
/*****************************************************************************

* FUNCTION:    u32DrvFfd_Test_SaveForTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1.Open the FFD device,
*                       2.if it success, Get the RAW size
*                       3.get the data in RAW area.
*                       4.close the device
* HISTORY:     Created by Anoop Chandran(RBEI/ECF1)    22 Sept, 2009
******************************************************************************/
tU32 u32DrvFfd_Test_SaveForTest(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  tS32                    Vs32size = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set data set only SPM can get raw size*/
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 10000;
  }
  else
  {/*Get the RAW Size*/
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 20000;
    }
    vpu8data = OSAL_NULL;
    vpu8data = (tU8*)OSAL_pvMemoryAllocate((tU32)Vs32size );
    if(OSAL_NULL != vpu8data)
    { /*set pointer*/
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
    }
    else
    {
      Vu32Ret += 30000;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 70000;
    }
  }
  return Vu32Ret;
}

/*****************************************************************************
* FUNCTION:    u32DrvFfd_Test_RestoreAfterTest()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION:  1.Open the FFD device,
*                       2.if it success, set the backup data
*                       3.reload the data
*                       4.Close the device.
* HISTORY:     Created by Anoop Chandran(RBEI/ECF1)    22 Sept, 2009
******************************************************************************/
tU32 u32DrvFfd_Test_RestoreAfterTest(void)
{
  tU32                    Vu32Ret = 0;
  tS32                    Vs32status = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR; 
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set data set only SPM can get raw size*/
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
  /*open the device */
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD, OSAL_EN_READWRITE);
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 100000;
  }
  if(vpu8data)
  {/*set pointer*/
    VtsFFDDevInfo.pvArg=vpu8data;
    /*Set the RAW data*/
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo );
    /*check error*/
    if(OSAL_ERROR ==  Vs32status)
    {
      Vu32Ret = 200000;
    }
    if(OSAL_NULL != vpu8data)
    {
      OSAL_vMemoryFree(vpu8data);
      vpu8data = NULL;
    }
  }
  if(Vs32status == OSAL_ERROR)
  {
    Vu32Ret += 3000000;
  }
  /*close the device*/
  if(OSAL_ERROR == OSAL_s32IOClose(VhDeviceFFD))
  {
    Vu32Ret += 4000000;
  }
  return Vu32Ret;
}
#endif

/*****************************************************************************
* FUNCTION:		u32FFDDevOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open close the driver dev_ffd
* HISTORY:		Created Andrea B�ter (TMS)  20.04.2010 
******************************************************************************/
tU32 u32FFDDevOpenClose(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor hDeviceFFD = 0;

  /*open the ffd device*/
  hDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(hDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    if(OSAL_ERROR==OSAL_s32IOClose(hDeviceFFD))
    {
      Vu32Ret    = 2;
    }
  }
  return(Vu32Ret);
}

/*****************************************************************************
* FUNCTION:		u32FFDDevOpenWithInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open the driver dev_ffd with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  20.04.2010 
******************************************************************************/
tU32 u32FFDDevOpenWithInvalidParam(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor hDeviceFFD = 0;

  /*open the ffd device*/
  hDeviceFFD = OSAL_IOOpen("test",OSAL_EN_READWRITE);
  //error check
  if(hDeviceFFD != OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  /*open the ffd device*/
  hDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_TEXT);
  //error check
  if(hDeviceFFD != OSAL_ERROR)
  {
    Vu32Ret += 10;
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevCloseWithInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  lose the driver dev_ffd with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  20.04.2010 
******************************************************************************/
tU32 u32FFDDevCloseWithInvalidParam(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor hDeviceFFD = -1;

  if(OSAL_OK==OSAL_s32IOClose(hDeviceFFD))
  {
    Vu32Ret    = 2;
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevOpenCloseSeveralTimes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open close the driver dev_ffd for several times
* HISTORY:		Created Andrea B�ter (TMS)  20.04.2010 
******************************************************************************/
tU32 u32FFDDevOpenCloseSeveralTimes(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor hDeviceFFD1 = 0;
  OSAL_tIODescriptor hDeviceFFD2 = 0;
  OSAL_tIODescriptor hDeviceFFD3 = 0;

  /*open the ffd device*/
  hDeviceFFD1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(hDeviceFFD1 == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*open the ffd device*/
    hDeviceFFD2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
    //error check
    if(hDeviceFFD2 == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    else
    {/*open the ffd device*/
      hDeviceFFD3= OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
      //error check
      if(hDeviceFFD3 == OSAL_ERROR)
      {
        Vu32Ret = 3;
      }
      else
      {
        if(OSAL_OK!=OSAL_s32IOClose(hDeviceFFD1))
        {
          Vu32Ret    = 10;
        }
        if(OSAL_OK!=OSAL_s32IOClose(hDeviceFFD2))
        {
          Vu32Ret    += 20;
        }
        if(OSAL_OK!=OSAL_s32IOClose(hDeviceFFD3))
        {
          Vu32Ret    += 40;
        }
        if(OSAL_OK==OSAL_s32IOClose(hDeviceFFD1))
        {
          Vu32Ret    += 80;
        }
      }
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevWriteRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevWriteRead(tVoid)
{
  tU32                    Vu32Ret = 0;
  tS32                    Vs32Status = 0;
  tS32                    Vs32CompareResult = 0;
  OSAL_tIODescriptor      hDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  const tS8               Vts8WriteTestData[] ={0x01, 0x07, 0x08, 0x03, 0x04};                                           
  tU16                    Vu16DataSize = sizeof(Vts8WriteTestData);
  tS8                     Vts8ReadTestData[Vu16DataSize];

  /*set to data set OEDT*/
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
#if OSAL_OS==OSAL_TENGINE 
  /* save actual data for test*/
  Vu32Ret = u32DrvFfd_Test_SaveForTest();  
  if (Vu32Ret != 0)
  {
	return Vu32Ret;
  }
#endif

  /* open the device*/
  hDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /* ckeck id */
  if(hDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret += 1;
  }
  else
  {/*Write the test data to FFD*/
    VtsFFDDevInfo.pvArg=(void*)&Vts8WriteTestData[0];
    Vs32Status = OSAL_s32IOWrite(hDeviceFFD,(tPCS8)&VtsFFDDevInfo,Vu16DataSize);
    /* compare size */
    if(Vs32Status != Vu16DataSize)
    {
      Vu32Ret += 2;
    }
    else
    { /*read back the test data*/
      VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
      Vs32Status = OSAL_s32IORead(hDeviceFFD,(tPS8)&VtsFFDDevInfo,Vu16DataSize );
      if(Vs32Status != Vu16DataSize)
      {
        Vu32Ret += 10;
      }
      else
      {/*compare the data in read buffer and write data*/
        Vs32CompareResult = OSAL_s32MemoryCompare( Vts8ReadTestData, Vts8WriteTestData, Vu16DataSize );
        if(Vs32CompareResult)
        {
          Vu32Ret += 20;
        }
      }
    }          
    /*close the device*/
    if(OSAL_ERROR == OSAL_s32IOClose(hDeviceFFD))
    {
      Vu32Ret += 40;
    }
  }    
#if OSAL_OS==OSAL_TENGINE 
  Vu32Ret += u32DrvFfd_Test_RestoreAfterTest();
#endif
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevWriteReadInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		Created Andrea B�ter (TMS)  23.04.2010 
******************************************************************************/
tU32 u32FFDDevWriteReadInvalidParam(void)
{
  tU32                      Vu32Ret = 0;
  OSAL_tIODescriptor        VhDeviceFFD = 0;
  OSAL_trFFDDeviceInfo      VtsFFDDevInfo;                                       
  tU16                      Vu16DataSize = 5;
  tS8                       Vts8ReadTestData[Vu16DataSize];
  tS32                      Vs32Status = 0;


#if OSAL_OS==OSAL_TENGINE 
  /* save actual data for test*/
  Vu32Ret = u32DrvFfd_Test_SaveForTest();  
  if (Vu32Ret != 0)
  {
	return Vu32Ret;
  }
#endif
  /*open the ffd device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*++++++++++++++++++++++++   write ++++++++++++++++++++++++++++++++++++++*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IOWrite(-1,(tPCS8)&VtsFFDDevInfo,Vu16DataSize);
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret    += 2;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,Vu16DataSize);
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret    += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+10;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,Vu16DataSize);
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret    += 8;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,0);
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret    += 10;
    }
    /*++++++++++++++++++++++++   read ++++++++++++++++++++++++++++++++++++++*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IORead(-1,(tPS8)&VtsFFDDevInfo,Vu16DataSize );
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret += 100;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,Vu16DataSize );
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret += 200;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+10;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,Vu16DataSize );
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret += 300;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
    Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,0 );
    if(Vs32Status == Vu16DataSize)
    {
      Vu32Ret += 400;
    }
    /*close*/
    if(OSAL_ERROR==OSAL_s32IOClose(VhDeviceFFD))
    {
      Vu32Ret    += 2000;
    }
  }
#if OSAL_OS==OSAL_TENGINE 
  Vu32Ret += u32DrvFfd_Test_RestoreAfterTest();
#endif
  return(Vu32Ret);
}/*****************************************************************************
* FUNCTION:		u32FFDDevGetRawSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test raw size with  
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRawSize(void)
{
  tU32                      Vu32Ret = 0;
  OSAL_tIODescriptor        VhDeviceFFD = 0;
  OSAL_trFFDDeviceInfo      VtsFFDDevInfo;
  tS32                      Vs32size = 0;
  tS32                      Vs32status = 0;

  /*open the ffd device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
    if(Vs32status == OSAL_ERROR)
#else
    if(Vs32status == OSAL_OK)
#endif
    {
      Vu32Ret = 20;
    }
    /*close */
    if(OSAL_ERROR==OSAL_s32IOClose(VhDeviceFFD))
    {
      Vu32Ret+=2;
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetRawSizeInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test raw size with invalid param 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRawSizeInvalidParam(void)
{
  tU32                      Vu32Ret = 0;
  OSAL_tIODescriptor        VhDeviceFFD = 0;
  OSAL_trFFDDeviceInfo      VtsFFDDevInfo;
  tS32                      Vs32size = 0;
  tS32                      Vs32status = 0;

  /*open the ffd device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(-1, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret = 20;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, 20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 40;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 80;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 100;
    }
    /*close */
    if(OSAL_ERROR==OSAL_s32IOClose(VhDeviceFFD))
    {
      Vu32Ret+=2;
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetRawData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test get raw data 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRawData(void)
{
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = 0;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tS32                    Vs32size = 0;
  tS32                    Vs32status = 0;

  /*open the ffd device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*Get the RAW Size*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
    if(Vs32status == OSAL_ERROR)
#else
    if(Vs32status == OSAL_OK)
#endif
    {
      Vu32Ret = 20;
    }
    vpu8data = OSAL_NULL;
    vpu8data = (tU8*) OSAL_pvMemoryAllocate((tU32)Vs32size );
    if(OSAL_NULL != vpu8data)
    {
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
      if(Vs32status == OSAL_ERROR)
#else
      if(Vs32status == OSAL_OK)
#endif
      {
        Vu32Ret += 40;
      }
      /* free memory*/
      if(OSAL_NULL != vpu8data)
      {
        OSAL_vMemoryFree(vpu8data);
        vpu8data = NULL;
      }
    }
    else
    {
      Vu32Ret += 30;
    }   
    /*close driver */
    if(OSAL_ERROR==OSAL_s32IOClose(VhDeviceFFD))
    {
      Vu32Ret    = 2;
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetRawDataInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test get raw data invalid paramer
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRawDataInvalidParam(void)
{
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = 0;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tS32                    Vs32size = 0;
  tS32                    Vs32status = 0;

  /*open the ffd device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  //error check
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*Get the RAW Size*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
    if(Vs32status == OSAL_ERROR)
#else
    if(Vs32status == OSAL_OK)
#endif
    {
      Vu32Ret = 20;
    }
    vpu8data = OSAL_NULL;
    vpu8data = (tU8*) OSAL_pvMemoryAllocate((tU32)Vs32size );
    if(OSAL_NULL != vpu8data)
    {
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( -1, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
      if(Vs32status == OSAL_OK)
      {
        Vu32Ret += 40;
      }
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, 20,(tS32)&VtsFFDDevInfo);
      if(Vs32status == OSAL_OK)
      {
        Vu32Ret += 80;
      }
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
      if(Vs32status == OSAL_OK)
      {
        Vu32Ret += 100;
      }
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=NULL;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
      if(Vs32status == OSAL_OK)
      {
        Vu32Ret += 200;
      }
      /* free memory*/
      if(OSAL_NULL != vpu8data)
      {
        OSAL_vMemoryFree(vpu8data);
        vpu8data = NULL;
      }
    }
    else
    {
      Vu32Ret += 30;
    }   
    /*close driver */
    if(OSAL_ERROR==OSAL_s32IOClose(VhDeviceFFD))
    {
      Vu32Ret    = 2;
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSetRawData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test set raw data 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevSetRawData(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  tS32                    Vs32size = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*Get the RAW Size*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
    if(Vs32status == OSAL_ERROR)
#else
    if(Vs32status == OSAL_OK)
#endif
    {
      Vu32Ret = 2;
    }
    vpu8data = OSAL_NULL;
    vpu8data = (tU8*) OSAL_pvMemoryAllocate((tU32)Vs32size );
    if(OSAL_NULL != vpu8data)
    { /*set pointer*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=vpu8data;
      memset(vpu8data,0xff,Vs32size);
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
      if(Vs32status != OSAL_OK)
#else
      if(Vs32status == OSAL_OK)
#endif
      {
        Vu32Ret += 3;
      }
      /* free memory*/
      if(OSAL_NULL != vpu8data)
      {
        OSAL_vMemoryFree(vpu8data);
        vpu8data = NULL;
      }
    }
    else
    {
      Vu32Ret += 4;
    }   
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 5;
    }
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSetRawDataInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevSetRawDataInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  tS32                    Vs32size = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/*Get the RAW Size*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=&Vs32size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
    if(Vs32status == OSAL_ERROR)
#else
    if(Vs32status == OSAL_OK)
#endif
    {
      Vu32Ret = 2;
    }
    vpu8data = OSAL_NULL;
    vpu8data = (tU8*) OSAL_pvMemoryAllocate((tU32)Vs32size );
    if(OSAL_NULL != vpu8data)
    { /*set pointer*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      VtsFFDDevInfo.pvArg=vpu8data;
      /*Get the RAW Data*/    
      Vs32status = OSAL_s32IOControl( VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo);
#if OSAL_OS==OSAL_TENGINE 
      if(Vs32status != OSAL_OK)
#else
      if(Vs32status == OSAL_OK)
#endif
      {
        Vu32Ret += 3;
      }
      else
      {
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
        VtsFFDDevInfo.pvArg=vpu8data;
        /*Set the RAW data*/
        Vs32status = OSAL_s32IOControl(-1, OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo );
        if(Vs32status == OSAL_OK)
        {
          Vu32Ret += 10;
        }
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
        VtsFFDDevInfo.pvArg=vpu8data;
        /*Set the RAW data*/
        Vs32status = OSAL_s32IOControl(VhDeviceFFD, 20,(tS32)&VtsFFDDevInfo );
        if(Vs32status == OSAL_OK)
        {
          Vu32Ret += 20;
        }
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
        VtsFFDDevInfo.pvArg=vpu8data;
        /*Set the RAW data*/
        Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo );
        if(Vs32status == OSAL_OK)
        {
          Vu32Ret += 40;
        }
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
        VtsFFDDevInfo.pvArg=NULL;
        /*Set the RAW data*/
        Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA,(tS32)&VtsFFDDevInfo );
        if(Vs32status == OSAL_OK)
        {
          Vu32Ret += 80;
        }
      }
      /* free memory*/
      if(OSAL_NULL != vpu8data)
      {
        OSAL_vMemoryFree(vpu8data);
        vpu8data = NULL;
      }
    }
    else
    {
      Vu32Ret += 4;
    }   
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 5;
    }
  }
  return Vu32Ret;
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSaveNow()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevSaveNow(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call save now*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSaveNowInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  save data with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevSaveNowInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call save now*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(-1, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, 20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+20;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 5;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 6;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevReload()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  reload data  
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevReload(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call reload*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevReloadInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  reload data with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevReloadInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call reload with invalid parameter*/   
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(-1, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 2;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD+20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+10;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 5;
    }
  }
  return Vu32Ret;  
}

/*****************************************************************************
* FUNCTION:		u32FFDDevSaveReloadCompare()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_FFD_014
* DESCRIPTION:  save data to flash; load data from flash and compare it
* HISTORY:		Created Andrea B�ter (TMS)  26.10.2007 
******************************************************************************/
tU32 u32FFDDevSaveReloadCompare(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU8*                    VubpOEDTWrite;
  tU8*                    VubpOEDTRead;

  /*get memory*/
  VubpOEDTWrite = (tU8*) OSAL_pvMemoryAllocate(OEDT_FFD_BUFFER_SIZE_OEDT);
  if (VubpOEDTWrite == OSAL_NULL)
  {
	return 97;
  }
  VubpOEDTRead  = (tU8*) OSAL_pvMemoryAllocate(OEDT_FFD_BUFFER_SIZE_OEDT);
  if (VubpOEDTRead == OSAL_NULL)
  {
	OSAL_vMemoryFree(VubpOEDTWrite);
	return 98;
  }
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call write data */   
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=VubpOEDTWrite;
    /* ---------------  write -----------------------------------------------*/
    /* write without pos*/
    memset(VubpOEDTWrite,0x55,OEDT_FFD_BUFFER_SIZE_OEDT);
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,OEDT_FFD_BUFFER_SIZE_OEDT);
    if(Vs32Status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/
    /* --------------- save to flash ---------------------------------------------*/
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
    if(Vs32Status == OSAL_ERROR)
    {
      Vu32Ret += 20;
    }/*end if*/
    else
    {/* --------------- load from flash ---------------------------------------------*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
      Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD,(tS32)&VtsFFDDevInfo);
      if(Vs32Status == OSAL_ERROR)
      {
        Vu32Ret += 200;
      }/*end if*/
      else
      { /* --------------- read --------------------------------------------------------*/
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
        VtsFFDDevInfo.pvArg=VubpOEDTRead;
        Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,OEDT_FFD_BUFFER_SIZE_OEDT);
        if(Vs32Status == OSAL_ERROR)
        {
          Vu32Ret += 2000;
        }/*end if*/
        else
        {/* --------------- compare------------------------------------------------------*/
          if(memcmp(VubpOEDTRead,VubpOEDTWrite,OEDT_FFD_BUFFER_SIZE_OEDT)!=0)
          {
            Vu32Ret += 20000;
          }/*end if*/
        }/*end else*/
      }/*end else no error load from flash*/
    }/*end else no error save from flash*/    
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {/*error*/
      Vu32Ret += 200000;
    }/*end if*/
  }/*end else*/
  /*set memory free*/
  OSAL_vMemoryFree(VubpOEDTWrite);
  OSAL_vMemoryFree(VubpOEDTRead);
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32FFDDevSeek()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  set seek position 
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevSeek(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Pos=32;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call seek*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Pos;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSeekInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test seek invalif parameter 
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevSeekInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Pos=32;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call seek*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Pos;
    Vs32status = OSAL_s32IOControl(-1, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 2;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Pos;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK+20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+20;
    VtsFFDDevInfo.pvArg=&Vu32Pos;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 5;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 6;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevWriteReadWithSeek()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevWriteReadWithSeek(void)
{
  tU32                    Vu32Ret = 0;
  tS32                    Vs32Status = 0;
  tS32                    Vs32CompareResult = 0;
  OSAL_tIODescriptor      hDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  const tS8               Vts8WriteTestData[] ={0x55, 0x55, 0x55, 0x55, 0x55};                                           
  tU16                    Vu16DataSize = sizeof(Vts8WriteTestData);
  tS8                     Vts8ReadTestData[Vu16DataSize];
  tU32                    Vu32Pos=32;

  /*set to data set OEDT*/
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
#if OSAL_OS==OSAL_TENGINE 
  /* save actual data for test*/
  Vu32Ret = u32DrvFfd_Test_SaveForTest();  
  if (Vu32Ret != 0)
  {
	return Vu32Ret;
  }
#endif
  /* open the device*/
  hDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /* ckeck id */
  if(hDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret += 1;
  }
  else
  {/*set position with seek*/
    VtsFFDDevInfo.pvArg=&Vu32Pos;
    Vs32Status = OSAL_s32IOControl(hDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
    if(Vs32Status == OSAL_ERROR)
    {
      Vu32Ret += 2;
    }
    /*Write the test data to FFD*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT|0x80;
    VtsFFDDevInfo.pvArg=(void*)&Vts8WriteTestData[0];
    Vs32Status = OSAL_s32IOWrite(hDeviceFFD,(tPCS8)&VtsFFDDevInfo,Vu16DataSize);
    /* compare size */
    if(Vs32Status != Vu16DataSize)
    {
      Vu32Ret += 4;
    }
    else
    { /*set position with seek*/
      VtsFFDDevInfo.pvArg=&Vu32Pos;
      Vs32Status = OSAL_s32IOControl(hDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK,(tS32)&VtsFFDDevInfo);
      if(Vs32Status == OSAL_ERROR)
      {
        Vu32Ret += 8;
      }
      /*read back the test data*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT|0x80;
      VtsFFDDevInfo.pvArg=&Vts8ReadTestData[0];
      Vs32Status = OSAL_s32IORead(hDeviceFFD,(tPS8)&VtsFFDDevInfo,Vu16DataSize );
      if(Vs32Status != Vu16DataSize)
      {
        Vu32Ret += 10;
      }
      else
      {/*compare the data in read buffer and write data*/
        Vs32CompareResult = OSAL_s32MemoryCompare( Vts8ReadTestData, Vts8WriteTestData, Vu16DataSize );
        if(Vs32CompareResult)
        {
          Vu32Ret += 20;
        }
      }
    }          
    /*close the device*/
    if(OSAL_ERROR == OSAL_s32IOClose(hDeviceFFD))
    {
      Vu32Ret += 40;
    }
  }    
#if OSAL_OS==OSAL_TENGINE 
    Vu32Ret += u32DrvFfd_Test_RestoreAfterTest();
#endif
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetRomDataVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get rom data version
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRomDataVersion(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Version;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call get rom data version*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Version;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetRomDataVersionInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get rom data version with invalid parameter 
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevGetRomDataVersionInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Version;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Version;
    Vs32status = OSAL_s32IOControl(-1,OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret = 2;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Version;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION+20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST;
    VtsFFDDevInfo.pvArg=&Vu32Version;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 5;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 6;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetTotolFlashSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get total flash size  
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevGetTotolFlashSize(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32FlashSize;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call get total flash*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32FlashSize;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetTotolFlashSizeInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get total flash size with invalid parameter 
* HISTORY:		Created Andrea B�ter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevGetTotolFlashSizeInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32FlashSize;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32FlashSize;
    Vs32status = OSAL_s32IOControl(-1,OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32FlashSize;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE+20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+10;
    VtsFFDDevInfo.pvArg=&Vu32FlashSize;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 5;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 6;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetDataSetSize()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  get data set size
* HISTORY:		Created Andrea B�ter (TMS)  21.04.2010 
******************************************************************************/
tU32 u32FFDDevGetDataSetSize(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Size;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {/* call get data size*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 3;
    }
  }
  return Vu32Ret;  
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetDataSetSizeInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  test read write 
* HISTORY:		get data set size with invalid parameter (TMS)  26.04.2010 
******************************************************************************/
tU32 u32FFDDevGetDataSetSizeInvalidParam(void)
{
  tS32                    Vs32status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU32                    Vu32Size;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Size;
    Vs32status = OSAL_s32IOControl(-1,OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret = 2;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32Size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE+20,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 3;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_LAST+1;
    VtsFFDDevInfo.pvArg=&Vu32Size;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 4;
    }
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32status = OSAL_s32IOControl(VhDeviceFFD,OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE,(tS32)&VtsFFDDevInfo);
    if(Vs32status == OSAL_OK)
    {
      Vu32Ret += 6;
    }
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {
      Vu32Ret += 7;
    }
  }
  return Vu32Ret;  
}

/*****************************************************************************
* FUNCTION:		u32FFDDevSaveCompleteFlashArea()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_FFD_050
* DESCRIPTION:  save flash area complete
* HISTORY:		Created Andrea B�ter (TMS)  01.02.2008 
******************************************************************************/
tU32 u32FFDDevSaveCompleteFlashArea(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret = 0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU8*                     VubpOEDTWrite;
  tU8*                     VubpOEDTRead;
  tU8                      VubInc;
  tU8                      VubValue=0x55;

  /*get memory*/
  VubpOEDTWrite = (tU8*) OSAL_pvMemoryAllocate(OEDT_FFD_BUFFER_SIZE_OEDT);
  if (VubpOEDTWrite == OSAL_NULL)
  {
	return 97;
  }
  VubpOEDTRead  = (tU8*) OSAL_pvMemoryAllocate(OEDT_FFD_BUFFER_SIZE_OEDT);
  if (VubpOEDTRead == OSAL_NULL)
  {
	OSAL_vMemoryFree(VubpOEDTWrite);
	return 98;
  }
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    /* save 33 different values into the flash*/
    for(VubInc=0;VubInc < OEDT_FFD_SAVE_COMPLETE_MAX;VubInc++)
    {/*increment value*/
      VubValue+=1;
      /* ---------------  write -----------------------------------------------*/
      /* write*/
      memset(VubpOEDTWrite,VubValue,OEDT_FFD_BUFFER_SIZE_OEDT);
      VtsFFDDevInfo.pvArg=VubpOEDTWrite;
      Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,OEDT_FFD_BUFFER_SIZE_OEDT);
      if(Vs32Status == OSAL_ERROR)
      {
        Vu32Ret = 2;
      }/*end if*/
      /* --------------- save to flash ---------------------------------------------*/
      Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
      if(Vs32Status == OSAL_ERROR)
      {
        Vu32Ret += 20;
      }/*end if*/
      else
      {/* --------------- load from flash ---------------------------------------------*/
        //OSAL_s32ThreadWait(100);
        VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_SPM;
        Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD,(tS32)&VtsFFDDevInfo);
        if(Vs32Status == OSAL_ERROR)
        {
          Vu32Ret += 200;
        }/*end if*/
        else
        { /* --------------- read --------------------------------------------------------*/
          //OSAL_s32ThreadWait(100);
          VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
          VtsFFDDevInfo.pvArg=VubpOEDTRead;
          Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,OEDT_FFD_BUFFER_SIZE_OEDT);
          if(Vs32Status == OSAL_ERROR)
          {
            Vu32Ret += 2000;
          }/*end if*/
          else
          {/* --------------- compare------------------------------------------------------*/
            if(memcmp(VubpOEDTRead,VubpOEDTWrite,OEDT_FFD_BUFFER_SIZE_OEDT)!=0)
            {
              Vu32Ret += 20000;
            }/*end if*/
          }/*end else*/
        }/*end else no error load from flash*/
      }/*end else no error save from flash*/
      if(Vu32Ret!=0) break; 
    }/*end for*/
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {/*error*/
      Vu32Ret += 200000;
    }/*end if*/
  }/*end else*/
  /*set memory free*/
  OSAL_vMemoryFree(VubpOEDTWrite);
  OSAL_vMemoryFree(VubpOEDTRead);
  return(Vu32Ret);
}/*end if*/

/*****************************************************************************
* FUNCTION:		u32FFDDevReadSaveAssertMode()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_FFD_051
* DESCRIPTION:  read/write assert mode
* HISTORY:		Created Andrea B�ter (TMS)  13.01.2009 
******************************************************************************/
tU32 u32FFDDevReadSaveAssertMode(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  tU32                    Vu32AssertModeOld;
  tU32                    Vu32AssertModeNew=4;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OSAL_ASSERT_MODE;
    VtsFFDDevInfo.pvArg=&Vu32AssertModeOld;
    /* read old assert mode*/
    Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,sizeof(Vu32AssertModeOld)); 
    if(Vs32Status==OSAL_ERROR)
    {
      Vu32AssertModeOld=1; /*set to default mode*/ /*???*/
    }
    /* write new assert mode*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OSAL_ASSERT_MODE;
    VtsFFDDevInfo.pvArg=&Vu32AssertModeNew;
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,sizeof(Vu32AssertModeNew));    
    if(Vs32Status==OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/
    else
    {/* read new assert mode*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OSAL_ASSERT_MODE;
      VtsFFDDevInfo.pvArg=&Vu32AssertModeNew;
      Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,sizeof(Vu32AssertModeNew)); 
      if(Vs32Status==OSAL_ERROR)
      {
        Vu32Ret = 3;
      }
      else
      {/* check length*/
        if(Vs32Status!=sizeof(Vu32AssertModeNew))
        {
          Vu32Ret |= 10;
        }
        /* check value*/
        if(4 != Vu32AssertModeNew)
        {
          Vu32Ret |= 100;
        }
      }/*end else*/
      /* write old assert mode*/
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OSAL_ASSERT_MODE;
      VtsFFDDevInfo.pvArg=&Vu32AssertModeOld;
      Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,sizeof(Vu32AssertModeOld));    
      if(Vs32Status==OSAL_ERROR)
      {
        Vu32Ret |= 1000;
      }/*end if*/
    }/*end else*/
    /*close the driver*/
    /*close the device*/
    if(OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
    {/*error*/     
      Vu32Ret |= 10000;
    }/*end if*/
  }/*end else*/
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32FFDDevSaveBackupFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  save backup file
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevSaveBackupFile(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=NULL;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret += 2;
    }/*end if*/
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevSaveBackupFileInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  save backup file invalid param
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevSaveBackupFileInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=NULL;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/
    else if (OSAL_s32IOControl(0, OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
    {
      Vu32Ret = 4;
    }
    else if (OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE+100,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
    {
      Vu32Ret = 4;
    }
    else
    {
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT+100;
      if (OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
      {
        Vu32Ret = 10;
      }
    }
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevLoadBackupFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  load backup file 
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevLoadBackupFile(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=NULL;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret += 2;
    }/*end if*/
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevLoadBackupFileInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  load backup file invalid param
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevLoadBackupFileInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=NULL;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/
    else if (OSAL_s32IOControl(0, OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
    {
      Vu32Ret = 4;
    }
    else if (OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE+100,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
    {
      Vu32Ret = 4;
    }
    else
    {
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT+100;
      if (OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE,(tS32)&VtsFFDDevInfo)!=OSAL_ERROR)
      {
        Vu32Ret = 10;
      }
    }
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD ))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetInfoReadData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  load backup file invalid param
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevGetInfoReadData(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU16                    Vu16InfoReadData=0xff;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=&Vu16InfoReadData;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/
    else
    {
      if(Vu16InfoReadData > M_FFD_INFO_READ_FLASH_DATA_LAST_VALID)
      {/*not valid value*/
         Vu32Ret = 4;
      }
    }
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32FFDDevGetInfoReadDataInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  load backup file invalid param
* HISTORY:		Created Andrea B�ter (BSOT)  09.03.2012
******************************************************************************/
tU32 u32FFDDevGetInfoReadDataInvalidParam(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
  tU16                    Vu16InfoReadData;

  /* set dev info */
  VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
  VtsFFDDevInfo.pvArg=&Vu16InfoReadData;
  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if (VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)&VtsFFDDevInfo);
    if (Vs32Status == OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/  
    Vs32Status = OSAL_s32IOControl(0, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)&VtsFFDDevInfo);
    if (Vs32Status != OSAL_ERROR)
    {
      Vu32Ret |= 4;
    }/*end if*/  
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA+100,(tS32)&VtsFFDDevInfo);
    if (Vs32Status != OSAL_ERROR)
    {
      Vu32Ret |= 8;
    }/*end if*/
    VtsFFDDevInfo.u8DataSet=250;
    VtsFFDDevInfo.pvArg=&Vu16InfoReadData;
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)&VtsFFDDevInfo);
    if (Vs32Status != OSAL_ERROR)
    {
      Vu32Ret |= 10;
    }/*end if*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=NULL;
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)&VtsFFDDevInfo);
    if (Vs32Status != OSAL_ERROR)
    {
      Vu32Ret |= 20;
    }/*end if*/
    Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA,(tS32)NULL);
    if (Vs32Status != OSAL_ERROR)
    {
      Vu32Ret |= 40;
    }/*end if*/
  }/*end else*/
  /*close the device*/
  if (OSAL_ERROR ==   OSAL_s32IOClose(VhDeviceFFD))
  {/*error*/     
    Vu32Ret |= 10000;
  }/*end if*/
  return(Vu32Ret);
}

#if defined(LSIM) || defined(GEN3X86)
/*****************************************************************************
* FUNCTION:		u32FFDDevLSIMWriteBeforeReboot()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Write an entry into KDS. This test has to be followed by a
                reboot and u32FFDDevLSIMReadAfterReboot.
				The 2 tests together check for persistence across reboots.
* HISTORY:		Created Vineetha Menon (RBEI/ECF1)  12.10.2011 
******************************************************************************/
tU32 u32FFDDevLSIMWriteBeforeReboot(void)
{
  tS32                    Vs32Status = 0;
  tU32                    Vu32Ret=0;
  tU32                    Vu32TestData=4;
  OSAL_tIODescriptor      VhDeviceFFD = OSAL_ERROR;
  OSAL_trFFDDeviceInfo    VtsFFDDevInfo;

  /*open the device*/
  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
  /*check handle*/
  if(VhDeviceFFD == OSAL_ERROR)
  {
    Vu32Ret = 1;
  }
  else
  {
    /* write test data*/
    VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
    VtsFFDDevInfo.pvArg=&Vu32TestData;
    Vs32Status = OSAL_s32IOWrite(VhDeviceFFD,(tPCS8)&VtsFFDDevInfo,sizeof(Vu32TestData));    
    if(Vs32Status==OSAL_ERROR)
    {
      Vu32Ret = 2;
    }/*end if*/

	Vs32Status = OSAL_s32IOControl(VhDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtsFFDDevInfo);
    if(Vs32Status == OSAL_ERROR)
    {
      Vu32Ret += 20;
    }/*end if*/
	return(Vu32Ret);

	if(OSAL_OK!=OSAL_s32IOClose(VhDeviceFFD))
        {
          Vu32Ret    = 10;
        }
}
}

/*****************************************************************************
* FUNCTION:		u32FFDDevLSIMReadAfterReboot()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Check a content of FFD for persistence after reboot.This test
                has to be run after a reboot is carried out after 
				u32FFDDevLSIMWrite.
				The 2 tests together check for persistence across reboots.
* HISTORY:		Vineetha Menon (RBEI/ECF1) 12.10.11
******************************************************************************/
tU32 u32FFDDevLSIMReadAfterReboot(void)
{
	  tS32					Vs32Status = 0;
	  tU32					  Vu32Ret=0;
	  tU32                    Vu32TestDataRead=0;
	  OSAL_tIODescriptor	  VhDeviceFFD = OSAL_ERROR;
	  OSAL_trFFDDeviceInfo	  VtsFFDDevInfo;
	
	  /*open the device*/
	  VhDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
	  /*check handle*/
	  if(VhDeviceFFD == OSAL_ERROR)
	  {
		Vu32Ret = 1;
	  }
	  else
	  {
      VtsFFDDevInfo.u8DataSet=EN_FFD_DATA_SET_OEDT;
      VtsFFDDevInfo.pvArg=&Vu32TestDataRead;
      Vs32Status = OSAL_s32IORead(VhDeviceFFD,(tPS8)&VtsFFDDevInfo,sizeof(Vu32TestDataRead)); 
      if(Vs32Status==OSAL_ERROR)
      {
        Vu32Ret = 3;
      }
      else
      {/* check length*/
        if(Vs32Status!=sizeof(Vu32TestDataRead))
        {
          Vu32Ret |= 10;
        }
        /* check value*/
        if(4 != Vu32TestDataRead)
        {
          Vu32Ret |= 100;
        }
      }/*end else*/

	  if(OSAL_OK!=OSAL_s32IOClose(VhDeviceFFD))
        {
          Vu32Ret    = 10;
        }
	  	}

	  
	  return(Vu32Ret);
}
#endif
