#include "oedt_FileSystem_Benchmark_TestFuncs.h"
#include "oedt_helper_funcs.h"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>


//parameters for character wise benchmark
#define BENCHMARK_FILENAME "/benchmarkFile.bin"
#define BENCHMARK_FILESIZE (1000 * 1024 * 1024)

#define BLOCKSIZE (10 * 1024 * 1024)
#define BLOCKCOUNT 100
extern OEDT_TESTSTATE OEDT_CREATE_TESTSTATE(tVoid);
extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);
//parameters for block wise benchmark (set/cleaned up by setup/teardown routine of each test)
tString sMemoryBlock;
tS32    s32BlockSize;
tU32    u32BlockCount;
//commented to avoid lint warnings
/*typedef struct FSBENCHMARK_RESULT {
  tDouble BytesPerSec;
  tDouble CPUUsage;
} FSBENCHMARK_RESULT;*/

typedef struct FSBenchmarkArguments {
	FILE* fp;
	tS32 fd;
	tU32 u32NumOfBytes;
//	tS32 s32ShMemSegID;
} FSBenchmarkArguments;

//commented to avoid lint warnings
/*typedef struct FSBenchmarkResults {
	OEDT_TESTSTATE teststate;
	FSBENCHMARK_RESULT measurement;
} FSBenchmarkResults;*/

//commented to avoid lint warnings
//#define SHM_KEY 0xDECAFBAD
//defined new 
//typedef tVoid (*SequentialCharTestOp)(FILE*, tU32, OEDT_TESTSTATE* state);
//typedef tVoid (*SequentialBlockTestOp)(tS32, tU32, OEDT_TESTSTATE* state);

/********************************************/
/* allocate globally used memory test block */
/********************************************/

tVoid AllocGlobalBlock(tU32 u32Size, OEDT_TESTSTATE* state)
{
	s32BlockSize = u32Size;
	sMemoryBlock = (tString)malloc(s32BlockSize);

	OEDT_ADD_RESULT(state, (sMemoryBlock == NULL) ? TEST_FAILURE : TEST_SUCCESS );
}

/**************************************/
/* fill test block with some data ... */
/**************************************/

tVoid FillMemoryBlock(OEDT_TESTSTATE* state)
{
	tS32 s32Count;
	OEDT_ADD_RESULT(state, (sMemoryBlock == NULL) ? TEST_FAILURE : TEST_SUCCESS );

	for (s32Count = 0; s32Count < s32BlockSize && sMemoryBlock; s32Count++)
		sMemoryBlock[s32Count] = s32Count % 256;
}

/****************************************/
/* free globally used memory test block */
/****************************************/

tVoid FreeMemoryBlock(OEDT_TESTSTATE* state)
{
	OEDT_ADD_RESULT(state, (sMemoryBlock == NULL) ? TEST_FAILURE : TEST_SUCCESS );

	if (sMemoryBlock)
		free(sMemoryBlock);

	sMemoryBlock = NULL;

	s32BlockSize = 0;
}

/*********************/
/* Open a given file */
/*********************/

tS32 OpenFile(tCString filename, tS32 flags, mode_t mode, OEDT_TESTSTATE* state)
{
	tS32 s32FileDescriptor = open(filename, flags, mode);
	OEDT_ADD_RESULT(state, (s32FileDescriptor == -1) ? TEST_FAILURE : TEST_SUCCESS );
	return s32FileDescriptor;
}

/************************************/
/* write block-wise to a given file */
/************************************/

tVoid SequentialBlockWrite(tS32 s32FileDescriptor, tU32 BlockCount, OEDT_TESTSTATE* state)
{
	tU32 u32Count;
	//commented to avoid lint warnings
	BlockCount = 2>1 ? BlockCount :BlockCount;
	OEDT_ADD_RESULT(state, (sMemoryBlock == NULL) ? TEST_FAILURE : TEST_SUCCESS );
	OEDT_TEST_BEGIN_LOOP_STATE(state);
	for (u32Count = 0; u32Count < u32BlockCount; u32Count++)
	{
		if (write(s32FileDescriptor, sMemoryBlock, s32BlockSize) != s32BlockSize)
			OEDT_ADD_FAILURE(state);
	}
	OEDT_TEST_END_LOOP_STATE(state);
}

/*************************************/
/* read block-wise from a given file */
/*************************************/

tVoid SequentialBlockRead(tS32 s32FileDescriptor, tU32 BlockCount, OEDT_TESTSTATE* state)
{
	tU32 u32Count;
	//commented to avoid lint warnings
	BlockCount = 2>1 ? BlockCount :BlockCount;
	OEDT_TEST_BEGIN_LOOP_STATE(state);
	for (u32Count = 0; u32Count < u32BlockCount; u32Count++)
	{
		if (read(s32FileDescriptor, sMemoryBlock, s32BlockSize) != s32BlockSize)
			OEDT_ADD_FAILURE(state);
	}
	OEDT_TEST_END_LOOP_STATE(state);
}

/********************************************/
/* CloseFile does what the name implies ... */
/********************************************/

tVoid CloseFile(tS32 s32FileDescriptor, OEDT_TESTSTATE* state)
{
	OEDT_ADD_RESULT(state, (close(s32FileDescriptor) != 0) ? TEST_FAILURE : TEST_SUCCESS );
}

/*********************************/
/* Open a given file as a stream */
/*********************************/

FILE* OpenStream(tCString filename, tCString mode, OEDT_TESTSTATE* state)
{
  FILE* fp = fopen(filename, mode);
  OEDT_ADD_RESULT(state, (fp == NULL) ? TEST_FAILURE : TEST_SUCCESS );
  return fp;
}

/******************************************/
/* write character-wise to a given stream */
/******************************************/

tVoid SequentialCharWrite(FILE* fp, tU32 u32BytesToWrite, OEDT_TESTSTATE* state)
{
  tU32 u32Count;
  OEDT_TEST_BEGIN_LOOP_STATE(state);
  for (u32Count = 0; u32Count < u32BytesToWrite; u32Count++)
  {
    tU32 u32byteToWrite = u32Count % 256;
    if ( putc(u32byteToWrite, fp) == EOF )
      OEDT_ADD_FAILURE(state);
  }
  OEDT_TEST_END_LOOP_STATE(state);
}

/*******************************************/
/* read character-wise from a given stream */
/*******************************************/

tVoid SequentialCharRead(FILE* fp, tU32 u32BytesToRead, OEDT_TESTSTATE* state)
{
  tU32 u32Count;
  OEDT_TEST_BEGIN_LOOP_STATE(state);

  for (u32Count = 0; u32Count < u32BytesToRead; u32Count++)
  {
    tS32 s32ExpectedChar = u32Count % 256;
    int val = getc( fp );
    if ( val != s32ExpectedChar )
      OEDT_ADD_FAILURE(state);
  }
  OEDT_TEST_END_LOOP_STATE(state);
}

/**********************************************/
/* CloseStream does what the name implies ... */
/**********************************************/

tVoid CloseStream(FILE* fp, OEDT_TESTSTATE* state)
{
  OEDT_ADD_RESULT(state, (fclose(fp) != 0) ? TEST_FAILURE : TEST_SUCCESS );
}

/*****************************************************************/
/* Execute the actual benchmark operation as a child process,    */
/* measure execution time and IO data rate and write the results */
/* to a common shared memory area                                */
/*****************************************************************/

tVoid ExecBenchmarkProcess(FSBenchmarkArguments* BenchmarkArguments, tVoid* operation, OEDT_TESTSTATE* state)
{
  struct rusage Usage1;
  struct rusage Usage2;

  tDouble sys_useconds;
  tDouble usr_useconds;

  tU32 u32NumBytes = 0;
  tVoid (*SequentialCharTestPtr)(FILE*, tU32, OEDT_TESTSTATE* state);
  tVoid (*SequentialBlockTestptr)(tS32, tU32, OEDT_TESTSTATE* state);



  if ( (BenchmarkArguments->fp && BenchmarkArguments->fd != -1) || ( (!BenchmarkArguments->fp) && BenchmarkArguments->fd == -1) )
	  OEDT_ADD_FAILURE(state);
  else
  {
	  OEDT_ADD_SUCCESS(state);

	  getrusage(RUSAGE_SELF, &Usage1);
	  if (BenchmarkArguments->fp)
	  	
	  { SequentialCharTestPtr =  operation; 
		  u32NumBytes = BENCHMARK_FILESIZE;
		  //assigned at the start of
		  //((SequentialCharTestOp)operation)(BenchmarkArguments->fp, BENCHMARK_FILESIZE, state);
		  (SequentialCharTestPtr)(BenchmarkArguments->fp, BENCHMARK_FILESIZE, state);
	  }
	  else
	  {   
		SequentialBlockTestptr = operation;
		  u32NumBytes = u32BlockCount * s32BlockSize;
		  //commented to avoid lint warnings
		  //((SequentialBlockTestOp)operation)( BenchmarkArguments->fd, BENCHMARK_FILESIZE, state);
		  (SequentialBlockTestptr)( BenchmarkArguments->fd, BENCHMARK_FILESIZE, state);
	  }
	  getrusage(RUSAGE_SELF, &Usage2);

      sys_useconds = (tDouble)((long int) ((Usage2.ru_stime.tv_sec - Usage1.ru_stime.tv_sec) *(long int) 1000000) +(long int) ((Usage2.ru_stime.tv_usec - Usage1.ru_stime.tv_usec)));
	  usr_useconds = (tDouble)((long int) ((Usage2.ru_utime.tv_sec - Usage1.ru_utime.tv_sec) *(long int) 1000000) +(long int) ((Usage2.ru_utime.tv_usec - Usage1.ru_utime.tv_usec)));

	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "CPU usage: %.3f%, MB/s: %.3f\n",
			  (sys_useconds / ( sys_useconds + usr_useconds )) * 100,
			  ( (tDouble)u32NumBytes * 1.048576)/ (sys_useconds + usr_useconds) );

  }
}


/***********************/
/* erase the test file */
/***********************/

tVoid CleanupFile(OEDT_TESTSTATE* state)
{
	if (unlink(BENCHMARK_FILENAME))
		OEDT_ADD_FAILURE(state);
	else
		OEDT_ADD_SUCCESS(state);
}


/*****************************BEGIN TESTCASES*********************************/


/****************************************************************************/
/* write character-wise to the eMMC and measure the data rate and CPU-usage */
/****************************************************************************/

OEDT_TEST(FSBenchmarkSeqCharWrite)
{
	FSBenchmarkArguments arguments;
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	FILE* fp;

	fp = OpenStream(BENCHMARK_FILENAME, "w+", &state);
	if (!fp) return state.error_code;

	arguments.fp = fp;
	arguments.fd = -1;
	arguments.u32NumOfBytes = BENCHMARK_FILESIZE;

	ExecBenchmarkProcess(&arguments, &SequentialCharWrite, &state);

	CloseStream(fp, &state);

	return state.error_code;
}

OEDT_TEST_TEARDOWN(FSBenchmarkSeqCharWrite)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	CleanupFile(&state);
	return 0;
}

/*****************************************************************************/
/* read character-wise from the eMMC and measure the data rate and CPU-usage */
/*****************************************************************************/

OEDT_TEST_SETUP(FSBenchmarkSeqCharRead)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	FILE* fp = OpenStream(BENCHMARK_FILENAME, "w+", &state);
	if (!fp) return state.error_code;
	SequentialCharWrite( fp, BENCHMARK_FILESIZE, &state);
	CloseStream(fp, &state);
	return state.error_code;
}

OEDT_TEST(FSBenchmarkSeqCharRead)
{
	FSBenchmarkArguments arguments;
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	FILE* fp;

	fp = OpenStream(BENCHMARK_FILENAME, "r", &state);
	if (!fp) return state.error_code;

	arguments.fp = fp;
	arguments.fd = -1;
	arguments.u32NumOfBytes = BENCHMARK_FILESIZE;

	ExecBenchmarkProcess(&arguments, &SequentialCharRead, &state);

	CloseStream(fp, &state);

	return state.error_code;
}

OEDT_TEST_TEARDOWN(FSBenchmarkSeqCharRead)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	CleanupFile(&state);
	return 0;
}

/************************************************************************/
/* write block-wise to the eMMC and measure the data rate and CPU-usage */
/************************************************************************/

OEDT_TEST_SETUP(FSBenchmarkSeqBlockWrite)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	AllocGlobalBlock(BLOCKSIZE, &state);
	FillMemoryBlock(&state);
	u32BlockCount = BLOCKCOUNT;
	return state.error_code;
}

OEDT_TEST(FSBenchmarkSeqBlockWrite)
{
	FSBenchmarkArguments arguments;
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	tS32 fd;

	fd = OpenFile(BENCHMARK_FILENAME, O_WRONLY | O_CREAT, 0666, &state);
	if (fd == -1) return state.error_code;

	arguments.fp = NULL;
	arguments.fd = fd;
	arguments.u32NumOfBytes = BENCHMARK_FILESIZE;

	ExecBenchmarkProcess(&arguments, &SequentialBlockWrite, &state);

	CloseFile(fd, &state);

	return state.error_code;
}

OEDT_TEST_TEARDOWN(FSBenchmarkSeqBlockWrite)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	CleanupFile(&state);
	FreeMemoryBlock(&state);
	u32BlockCount = 0;
	return 0;
}

/*************************************************************************/
/* read block-wise from the eMMC and measure the data rate and CPU-usage */
/*************************************************************************/

OEDT_TEST_SETUP(FSBenchmarkSeqBlockRead)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	AllocGlobalBlock(BLOCKSIZE, &state);
	u32BlockCount = BLOCKCOUNT;

	int fd = OpenFile(BENCHMARK_FILENAME, O_CREAT | O_WRONLY, 0666 , &state);
	if (fd == -1) return state.error_code;

	SequentialBlockWrite(fd, u32BlockCount, &state);

	CloseFile(fd, &state);

	return state.error_code;
}

OEDT_TEST(FSBenchmarkSeqBlockRead)
{
	FSBenchmarkArguments arguments;
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	tS32 fd;

	fd = OpenFile(BENCHMARK_FILENAME, O_RDONLY, 0666, &state);
	if (fd == -1) return state.error_code;

	arguments.fp = NULL;
	arguments.fd = fd;
	arguments.u32NumOfBytes = BENCHMARK_FILESIZE;

	ExecBenchmarkProcess(&arguments,&SequentialBlockRead, &state);

	CloseFile(fd, &state);

	return state.error_code;
}

OEDT_TEST_TEARDOWN(FSBenchmarkSeqBlockRead)
{
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	CleanupFile(&state);
	FreeMemoryBlock(&state);
	u32BlockCount = 0;
	return 0;
}

