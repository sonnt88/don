/******************************************************************************
 *FILE         : oedt_Cdrom_Navi_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk  
 *
 *DESCRIPTION  : This file implements the individual test cases for the cdrom 
 *               device	( OEDT testcases).
 *               Checking for valid pointer in each test cases is not required
 *               as it is ensured in the calling function that that it cannot
 *               be NULL.
 *
 *AUTHOR(s)    : Venkateswara.N
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : DVD used for testing - dvd0-EUR07.3NT_FM7.2S_N1210M8T8_07 [v2_oS]
 * ____________________________________________________________________________
 *			     Initial  Version
 *               15.05.06 ver  1.0  RBIN/ECM1- Venkateswara.N
 *               Created a seperate file for OEDT-CD-ROM Test functions
 *				 13.09.2006 ver 1.1 Anoop Krishnan (RBIN/ECM1)
 *               Modified test case TS_OEDT_CDROM_045
 *				 07.10.2006 ver 1.2 Rakesh Dhanya(RBIN/ECM1)
 *               Modified test case TS_OEDT_CDROM_038
 *				 07.10.2006 ver 1.3 Narasimha Prasad Palasani (RBIN/ECM1)
 *               Modified test case TS_OEDT_CDROM_045
 *				 01.02.2007 ver 1.4 Tinoy Mathews(RBIN/ECM1)
 *				 Modified test case TS_OEDT_CDROM_042				
 *				 11.07.2007 ver 1.5 Shilpa Bhat(RBIN/EDI3)
 *				 Added test cases TS_OEDT_CDROM_058 to 081
 *               Updated test cases TS_OEDT_CDROM_056, 057
 *				 
 *				 26.09.2007 ver 1.6 Tinoy Mathews(RBIN/EDI3)
 *				 Updated test cases TS_OEDT_CDROM_006,009,
 *               013,015,017,018,020,021,027,032,034,036,
 *               040,049,051,053,060
 *
 *               06.03.08 ver 1.7
 *               renamed to oedt_Cdrom_Navi_TestFuncs.c
 *				 19.11.2007 ver 1.8 Anoop Chandran(RBIN/EDI3)
 *				 Removed test cases which using invalid handle
 *			     TS_OEDT_CDROM_007,016,030,037,067,068,078,080
 *				 previous version ver 1.6
 *				 Test case updated 
 *				 TS_OEDT_CDROM_052,053
 * -----------------------------------------------------------------------------
 *          VER 1.9 
 *          Updated by Shilpa Bhat (RBIN/ECM1) on 18/02/2008
 * 			Removed cases u32CDROMFileGetInfo() and u32CDROMFileGetInfoInvalParm()
 *          Updated testcases u32CDROMInvalidMedia, u32CDROMWithReverseCD, 
 *          u32CDROMFirstFileRead, u32CDROMLastFileRead, u32CDROMFileThroughPutFirstFile 
 *          u32CDROMFileThroughPutLastFile
 *
 *          ver 2.0
 *          Updated by Shilpa Bhat (RBIN/ECM1) on 30/07/2008
 *		    Removed cases u32CDROMNaviOpenDirNonExist, u32CDROMNaviFileOpenNonExist,
 *          u32CDROMNaviCloseDevInvalParm,
 *          u32CDROMNaviCloseDirInvalParm, u32CDROMNaviGetInfoInvalParm,
 *          u32CDROMNaviFileCloseInvParm, u32CDROMNaviFileGetPosInvalParm,
 *          u32CDROMNaviFileGetPosFrmEOFInvalParm, u32CDROMNaviFileSetPosInvalParm,
 *          u32CDROMNaviFileReadWithInvalParm, u32CDROMNaviGetVersionNumberInvalParm
 *          u32CDROMNaviGetVolumeLabelInvalParm, u32CDROMNaviOpenDirWithNULL, 
 *          u32CDROMNaviFileOpenWithNULLParam, u32CDROMNaviDirReClose,
 *          u32CDROMNaviLowVoltTest, u32CDROMNaviMaxFileOpen, u32CDROMNaviFileOpenGrtrMax
 *          Added new cases u32CDROMNaviReadAsync, u32CDROMNaviFileReClose
 *          
 *          ver 2.1 
 *          Updated by Shilpa Bhat (RBEI/ECF1) on Mar 24, 2009
 *          Lint Removal
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! TRACE Calls from test functions are NOT ALLOWED !!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomar (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "oedt_Cdrom_Navi_TestFuncs.h"
#include "oedt_helper_funcs.h"
/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/






/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/
/******************************************************************************
 *FILE         : oedt_Cdrom_Navi_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases (OEDT) for the
 *				 cdrom device.
 *               Checking for valid pointer in each test cases is not required
 *               as it is ensured in the calling function that that it cannot
 *               be NULL.
 *
 *AUTHOR       :  Venkateswara.N(OEDT Test Cases)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
 *****************************************************************************/
#include "osdevice.h"
#define OSAL_C_STRING_DEVICE_INVAL  	"/dev/Invalid"
#define OSAL_C_STRING_DIR1				"/dev/cdrom/DATA"


#define OSAL_C_STRING_TXT_FILE			"/dev/cdrom/INFO.TXT"
#define OSAL_C_STRING_CNF_FILE 			"/dev/cdrom/CFG/TIMA/TMC.CNF"
#define	OSAL_C_STRING_IDX_FILE			"/dev/cdrom/DATA/CONNECT/MAP/N3E2AA.IDX"
#define OSAL_C_STRING_FILE_SUBDIR		"/dev/cdrom/DATA/DATA/RNW/CCP/ACL/NAV20217.DAT"

#define OSAL_C_STRING_FILE_INVPATH		"/dev/cdrom/InvDir/INFO.TXT"
#define OSAL_C_S32_IOCTRL_INVAL_FUNC	-1000

#define BYTES_TO_READ_TPUT1 1040
#define BYTES_TO_READ_TPUT2 2000000 
#define BYTES_TO_READ_NAVI 20
#define MAX_LASER_JUMPS_NAVI 100

#define DIR_NAME_CREATE "/dev/cdrom/DATA/NewDir"
#define DIR_NAME_DEL "/dev/cdrom/DATA/CONNECT"
#define FILE_PATH_CREATE  "/dev/cdrom/DATA/NewFile"
#define DIR_PATH_FILE_DEL "/dev/cdrom/DATA/CONNECT/LID"
#define FILE_PATH_DEL  "/dev/cdrom/DATA/CONNECT/LID/META0000.DAT"
#define FIRST_DAT_FILE "/dev/cdrom/DATA/CONNECT/LID/CONNECT.DAT"
#define LAST_DAT_FILE "/dev/cdrom/DATA/DATA/TRAVEL/1F24E8E1/SCA/LID00004.DAT"
#define DIR_MAX_PATH "/dev/cdrom/DATA/DATA/TRAVEL/1F24E8E1/ACL/IMG/AUT"
#define FILE_DAT_069 "/dev/cdrom/DATA/CONNECT/LID/META0000.DAT"

OSAL_tSemHandle hSemAsyncCDROMNavi = 0;


/************************************************************************
*FUNCTION:      vOEDTCDROMNaviAsyncCallback 
* PARAMETER:    none
* RETURNVALUE:  "void"
* DESCRIPTION:  Call back function for Async read operation
* HISTORY:		Created Shilpa Bhat(RBIN/ECM1)on July 29, 2008
 ************************************************************************/
tVoid vOEDTCDROMNaviAsyncCallback (OSAL_trAsyncControl *prAsyncCtrl)
{
	tU32 u32RetVal = 0;
	u32RetVal = OSAL_u32IOErrorAsync(prAsyncCtrl);
	if ( u32RetVal == OSAL_E_NOERROR )
	{
	    OSAL_s32SemaphorePost ( hSemAsyncCDROMNavi );   
	}
} 

/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_001
* DESCRIPTION:  Opens the CDROM device
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006 
*
******************************************************************************/

tU32 u32CDROMNaviOpenDevice(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM , OSAL_EN_READONLY );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}
      
/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDevInvalParm()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_002
* DESCRIPTION:  Opening an Invalid Device. 
				(should fail).
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006 
*
******************************************************************************/

tU32 u32CDROMNaviOpenDevInvalParm(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
    
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_INVAL, OSAL_EN_READONLY );
    if ( hDevice != OSAL_ERROR)
    {
    	u32Ret = 1;
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}

    }
    return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMNaviReOpenDev()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_003
* DESCRIPTION:  Opening an already opened device 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006 
*
******************************************************************************/
tU32 u32CDROMNaviReOpenDev(void)
{
	OSAL_tIODescriptor hDevice1 = 0;
	OSAL_tIODescriptor hDevice2 = 0;
	tU32 u32Ret = 0;
    
    hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
    if ( hDevice1 == OSAL_ERROR )
    {
    	u32Ret = 1;
    }
    else
	{
		OEDT_HelperPrintf(TR_LEVEL_USER_1,"Dev Handle1: %d",hDevice1);
  	    /* Re Opening the Device */ 
  	    hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
		if( hDevice2 == OSAL_ERROR )
	    {
		   u32Ret = 10;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"Dev Handle2: %d",hDevice2);
			if( OSAL_s32IOClose ( hDevice2 )== OSAL_ERROR )
			{
				u32Ret = 20;
			}
		}
		if( OSAL_s32IOClose ( hDevice1 )== OSAL_ERROR )
		{
			u32Ret += 2;
		}

	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDevWriteOnly()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_004
* DESCRIPTION:  open the device in write only mode (should fail) 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006 
*
******************************************************************************/
tU32 u32CDROMNaviOpenDevWriteOnly(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_WRITEONLY );
    if ( hDevice != OSAL_ERROR)
    {
    	u32Ret = 1;
    	if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDROMNaviCloseDevice()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_005
* DESCRIPTION:  Closes the CDROM device.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006 
*
******************************************************************************/
tU32  u32CDROMNaviCloseDevice(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
    if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_006
* DESCRIPTION:  Open an existing directory.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviOpenDir(void)
{
	OSAL_tIODescriptor hDir = 0;
    tU32 u32Ret = 0;
    
    hDir = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDirWithFile()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_007
* DESCRIPTION:  Try to open a file with the function parameter of DIR-Open. 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32 u32CDROMNaviOpenDirWithFile(void)
{
	OSAL_tIODescriptor hDir = 0;
	tU32 u32Ret = 0;
    

	hDir = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviReOpenDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_008
* DESCRIPTION:  Try to open a DIR which is already opened. 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32 u32CDROMNaviReOpenDir(void)
{
	OSAL_tIODescriptor hDir1 = 0;
	OSAL_tIODescriptor hDir2 = 0;
	tU32 u32Ret = 0;
    
	hDir1 = OSAL_IOOpen (OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hDir1 == OSAL_ERROR )
	{
		u32Ret=1;
	}
	else
	{
		/* Re Opening the Dir */
		hDir2 = OSAL_IOOpen (OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
		if(hDir2 == OSAL_ERROR )
		{
			u32Ret = 10;
		}
		else
		{
			if( OSAL_s32IOClose ( hDir2 ) == OSAL_ERROR )
			{
				u32Ret = 20;
			}
		}
		if( OSAL_s32IOClose ( hDir1 ) == OSAL_ERROR )
		{
			u32Ret +=2;
		}
	}
 	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviOpenDirWriteAccess()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_009
* DESCRIPTION:  Try to open a directory with write permissions (should fail) 
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviOpenDirWriteAccess( void)
{
	OSAL_tIODescriptor hDir = 0;
	tU32 u32Ret = 0;
    
	hDir = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_WRITEONLY );
	if( hDir != OSAL_ERROR )
	{
		u32Ret = 1;
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
 	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviCloseDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_010
* DESCRIPTION:  Closes the specified directory.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviCloseDir(void)
{
	OSAL_tIODescriptor hDir = 0;
	tU32 u32Ret = 0;
   
   	hDir = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else 
	{
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviGetDirInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_011
* DESCRIPTION:  Retreives the directory Information.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 26, 2007
******************************************************************************/

tU32 u32CDROMNaviGetDirInfo(void)
{
	OSAL_tIODescriptor hDir = 0;
	OSAL_trIOCtrlDirent rDirent; 
	tU32 u32Ret = 0;

	    
	hDir = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hDir == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOControl (hDir, OSAL_C_S32_IOCTRL_FIOREADDIR,
							(tS32) &rDirent ) == OSAL_ERROR	)
		{
			u32Ret = 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"DIR Info :\t %s "
									,(tString)rDirent.s8Name );
		}
		if( OSAL_s32IOClose ( hDir ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_012
* DESCRIPTION:  Opens an existing file.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileOpen(void)
{
   
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileOpenInvalPath()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_013
* DESCRIPTION:  Opening a file with Invalid path should fail.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 26, 2007
******************************************************************************/

tU32 u32CDROMNaviFileOpenInvalPath(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_FILE_INVPATH,OSAL_EN_READONLY );
	if( hFile != OSAL_ERROR )
	{
		u32Ret = 1;
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileOpenDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_014
* DESCRIPTION:  Opens file in different modes.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileOpenDiffModes(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
    
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if(OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	/* Opening in WRITE only mode */
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_WRITEONLY );
	if( hFile != OSAL_ERROR )
	{
		u32Ret += 10;
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}

	/* Opening in READ-WRITE only mode */
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READWRITE );
	if( hFile != OSAL_ERROR )
	{
		u32Ret += 100;
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileOpenDiffExtns()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_015
* DESCRIPTION:  Opens files with different extensions.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				Modified by Shilpa Bhat (RBIN/ECM1) on 31 July, 2008
******************************************************************************/
tU32 u32CDROMNaviFileOpenDiffExtns()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
    	
	/* Opening The .CNF File*/
	hFile = OSAL_IOOpen ( OSAL_C_STRING_CNF_FILE,OSAL_EN_READONLY );	
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	
	/* Opening The .IDX File*/
		
	hFile = OSAL_IOOpen ( OSAL_C_STRING_IDX_FILE,OSAL_EN_READONLY );	
	if( hFile == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileOpenWithDirName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_016
* DESCRIPTION:  Opens a directory.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileOpenWithDirName(void)
{
   
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_017
* DESCRIPTION:  Opening an already opened file should succeed.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReOpen(void)
{
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
	tU32 u32Ret = 0;
	
	hFile1 = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Re Opening The File */
		hFile2 = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
		if(hFile2 == OSAL_ERROR)
		{
			u32Ret = 10;
		}
		else
		{
			if( OSAL_s32IOClose ( hFile2 ) == OSAL_ERROR )
			{
				u32Ret = 20;
			}
		}
		if( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_018
* DESCRIPTION:  Closes the file.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32 u32CDROMNaviFileClose(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
   	
   	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileCloseDiffExtns()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_019
* DESCRIPTION:  Closes files with different extensions.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				Modified by Shilpa Bhat (RBIN/ECM1) on 31 July, 2008
******************************************************************************/
tU32 u32CDROMNaviFileCloseDiffExtns()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	
	/* Opening the .CNF file */
	hFile = OSAL_IOOpen ( OSAL_C_STRING_CNF_FILE,OSAL_EN_READONLY ); 
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}

		/* Opening the .IDX file */
	hFile = OSAL_IOOpen ( OSAL_C_STRING_IDX_FILE,OSAL_EN_READONLY ); 
	if( hFile == OSAL_ERROR )
	{
		u32Ret += 10;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 20;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileCloseWithDirName()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_020
* DESCRIPTION:  Test file close with directory name.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileCloseWithDirName(void)
{
   
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_DIR1,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret = 2;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileGetPosFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_021
* DESCRIPTION:  Retreives the number of bytes between current position and BOF.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32 u32CDROMNaviFileGetPosFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/*	OSAL_C_S32_IOCTRL_FIOWHERE -- Gives the file pos 
			from beginning of the file*/
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"File Pos Frm BOF:  %d",u32FilePos);
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileGetPosFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_022
* DESCRIPTION:  Retreives the number of bytes between current position and EOF.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileGetPosFrmEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* OSAL_C_S32_IOCTRL_FIONREAD -- Gives the file pos 
		from the end of file */
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"File Pos Frm EOF:  %d",u32FilePos);
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}
 
/*****************************************************************************
* FUNCTION:		u32CDROMNaviFileSetPos()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_023
* DESCRIPTION:  Sets position for different offsets from different positions.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32 u32CDROMNaviFileSetPos(void)
{
    OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	tS32 s32PosToSet = 0;
	
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* 	OSAL_C_S32_IOCTRL_FIOSEEK -- Set the file pointer to
		corresponding offset 
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )
		OEDT_HelperPrintf(TR_LEVEL_USER_1,
		"File Pos Frm BOF:  %d",u32FilePos);*/

	
		s32PosToSet = 100;
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		(tS32)s32PosToSet) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
	   /*	if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )
		OEDT_HelperPrintf(TR_LEVEL_USER_1,
		"File Pos Frm BOF:  %d",u32FilePos);*/
		
		s32PosToSet = 200;
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		(tS32)s32PosToSet) == OSAL_ERROR )
		{
			u32Ret += 30;
		}
		/*if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )
		OEDT_HelperPrintf(TR_LEVEL_USER_1,
		"File Pos Frm BOF:  %d",u32FilePos);*/
		
		
		/*s32PosToSet = 0;
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		(tS32)s32PosToSet) == OSAL_ERROR )
		{
			u32Ret += 300;
		} */
		s32PosToSet = -100;
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		(tS32)s32PosToSet) != OSAL_ERROR )
		{
			u32Ret += 300;
		}
		/*if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOWHERE,
		(tS32)&u32FilePos) != OSAL_ERROR )
		OEDT_HelperPrintf(TR_LEVEL_USER_1,
		"File Pos Frm BOF:  %d",u32FilePos);*/
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	   u32CDROMNaviFileSetPosBeyondEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_024
* DESCRIPTION:  should fail to set file pointer beyond the EOF.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/
tU32  u32CDROMNaviFileSetPosBeyondEOF(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 u32FilePos = 0;
	tS32 s32PosToSet = 0;

	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Getting the File size */
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			/* Makes the offset greater than the file size */
			 s32PosToSet = (tS32)(u32FilePos + 1000);
			
			 if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
			 (tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret = 30;
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}


/*****************************************************************************
* FUNCTION:	   u32CDROMNaviFileRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_025
* DESCRIPTION:  Reads data from the file.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileRead(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 100;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead );
		if( (s32BytesRead == OSAL_ERROR) || (s32BytesRead != s32BytesToRead ))
		{
			u32Ret = 3;
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadSubDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_026
* DESCRIPTION:  Reads data from the file in a sub-directory.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/


tU32 u32CDROMNaviFileReadSubDir(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 200;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_FILE_SUBDIR,OSAL_EN_READONLY);
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead );
		if( (s32BytesRead == OSAL_ERROR) || (s32BytesRead != s32BytesToRead ))
		{
			u32Ret = 3;
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadLargeData()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_027
* DESCRIPTION:  Reads large data from the file.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				Updated by Tinoy Mathews(RBIN/ECM1) Feb 01,2007
******************************************************************************/

tU32 u32CDROMNaviFileReadLargeData(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 1048576;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_FILE_SUBDIR,OSAL_EN_READONLY);
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead );
		if( (s32BytesRead == OSAL_ERROR) || (s32BytesRead != s32BytesToRead ))
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,
			"No of Bytes Read: %d(LARGE DATA)",s32BytesRead);
			u32Ret = 3;
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadOffset()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_028
* DESCRIPTION:  Read file with few offsets from BOF
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReadOffset(void)
{
    OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 500;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 200;
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
		(tS32)s32PosToSet) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead );
		if( (s32BytesRead == OSAL_ERROR) || (s32BytesRead != s32BytesToRead) )
		{
			u32Ret += 40;
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadOffsetFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_029
* DESCRIPTION:  Reads file with few offsets from EOF
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReadOffsetFrmEOF(void)
{

	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 s32Filepos_frm_end = 0;
	tU32 u32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 50;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			s32Filepos_frm_end =500;
			s32PosToSet = (tS32)u32FilePos - s32Filepos_frm_end;
			if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
			(tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret = 30;
			}
			else
			{
				s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,(tU32)s32BytesToRead );
				if((s32BytesRead == OSAL_ERROR) || (s32BytesRead != s32BytesToRead))
				{
					u32Ret = 300;
				}
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadOffsetBeyondEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_030
* DESCRIPTION:  Reading data beyond EOF should fail.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReadOffsetBeyondEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 s32Filepos_frm_end = 0;
	tU32 u32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 50;
	tS32 s32BytesRead = 0;

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			s32Filepos_frm_end =500;
			s32PosToSet = (tS32)u32FilePos + s32Filepos_frm_end;
			if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
			(tS32)s32PosToSet) == OSAL_ERROR )
			{
				u32Ret = 30;
			}
		
			else
			{
				s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,
				(tU32)s32BytesToRead );
				if(!((s32BytesRead == 0)||(s32BytesRead == -1)) )
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_1,
					"No of Bytes Read: %d(Reading data beyond EOF)",s32BytesRead);
					u32Ret = 300;
				}
			}
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadEveryNthByteFrmBOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_031
* DESCRIPTION:  Reads from every nth position from BOF.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReadEveryNthByteFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0, s32File_size = 0;
	tU32 u32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 5, s32BytesRead = 0, s32nthbyte = 100;
	tS32 No_of_loops_run = 0;

	/* 	s32BytesToRead,	s32nthbyte always should be less than file size
		i.e. 1 KB (INFO.TXT(for present case)) */

	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY );
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Gives the file Size */
		if(OSAL_s32IOControl ( hFile,
		OSAL_C_S32_IOCTRL_FIONREAD,(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			s32File_size = (tS32)u32FilePos;
			while( s32PosToSet < s32File_size)
			{
				if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
				(tS32)s32PosToSet) == OSAL_ERROR )
				{
					u32Ret = 30;
					break;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead( hFile,
					ps8ReadBuffer,(tU32)s32BytesToRead );
					if( (s32BytesRead == OSAL_ERROR)||
					(s32BytesRead != s32BytesToRead))
					{
						u32Ret = 300;
						OEDT_HelperPrintf(TR_LEVEL_USER_1,
						"No of loops  :%d( bytes read:%d)",
						No_of_loops_run,s32BytesRead);
						break;
					}
				}
				s32PosToSet += s32nthbyte;
				No_of_loops_run++;
			}
		} 
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	if( No_of_loops_run == 0)
	{
		u32Ret += 3000;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadEvreyNthByteFrmEOF()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_032
* DESCRIPTION:  Reads from every nth position from EOF.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32 u32CDROMNaviFileReadEvreyNthByteFrmEOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0, s32File_size = 0,s32Filepos_frm_end =5 ;
	tU32 u32FilePos = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 s32BytesToRead = 5, s32BytesRead = 0, s32nthbyte = 100;
	tS32 No_of_loops_run = 0;

	/* 	s32BytesToRead,	s32nthbyte ,s32Filepos_frm_end always should 
		be less than file size i.e. 1 KB (INFO.TXT (for present case)) */


	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate ((tU32)s32BytesToRead);
	hFile = OSAL_IOOpen ( OSAL_C_STRING_TXT_FILE,OSAL_EN_READONLY);
	if( hFile == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		/* Gives the file Size */

		if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD,
		(tS32)&u32FilePos) == OSAL_ERROR )
		{
			u32Ret = 3;
		}
		else
		{
			s32File_size = (tS32)u32FilePos;
			s32PosToSet = s32File_size - s32Filepos_frm_end; 
			while( s32PosToSet > 0)
			{
				if(OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
				(tS32)s32PosToSet) == OSAL_ERROR )
				{
					u32Ret = 30;
					break;
				}
				else
				{
					s32BytesRead = OSAL_s32IORead( hFile,ps8ReadBuffer,
					(tU32)s32BytesToRead );
					if((s32BytesRead == OSAL_ERROR)||
					(s32BytesRead != s32BytesToRead))
					{
						OEDT_HelperPrintf(TR_LEVEL_USER_1,
						"No of loops  :%d( bytes read:%d)",No_of_loops_run,
						s32BytesRead);

						u32Ret = 300;
						break;
					}
				}
				s32Filepos_frm_end += s32nthbyte;
				s32PosToSet = s32File_size - s32Filepos_frm_end; 
				No_of_loops_run++;
			}
		} 
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	if(ps8ReadBuffer != OSAL_NULL)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	if( No_of_loops_run == 0)
	{
		u32Ret += 3000;
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviGetVersionNumber()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_033
* DESCRIPTION:  Gets the Version Number.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32  u32CDROMNaviGetVersionNumber(void)
{
   	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tChar szVersionBuffer [5];
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if ( OSAL_s32IOControl ( hDevice,OSAL_C_S32_IOCTRL_VERSION,
						 (tS32)szVersionBuffer ) == OSAL_ERROR )
		{
			u32Ret	= 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"VersionNumber :\t %s "
									,(tString)szVersionBuffer );
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviGetVolumeLabel()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_034
* DESCRIPTION:  Gets the Volume label.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32  u32CDROMNaviGetVolumeLabel(void)
{
   	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tChar szLabelBuffer [100];
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if ( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_FIOLABELGET,
						 (tS32)szLabelBuffer ) == OSAL_ERROR )
		{
			u32Ret	= 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"VolumeLabel :\t %s "
							,(tString)szLabelBuffer );
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviGetMediaInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_035
* DESCRIPTION:  Get the information about the media.
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32  u32CDROMNaviGetMediaInfo(void)
{
   	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tChar szMediaInfo [100];
    
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if ( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO,
						 (tS32)szMediaInfo ) == OSAL_ERROR )
		{
			u32Ret	= 3;
		}
		else
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_1,"MediaInfo :\t %s "
							,(tString)szMediaInfo);
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviInvalFuncParm2IOCTL()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_036
* DESCRIPTION:  Invalid func paramter to IOCTL should fail
* HISTORY:		Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*
******************************************************************************/

tU32  u32CDROMNaviInvalFuncParm2IOCTL(void)
{
   	OSAL_tIODescriptor hDevice = 0;
	tU32 u32Ret = 0;
	tChar szMediaInfo [100];
	
    hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDROM, OSAL_EN_READONLY );
	if ( hDevice == OSAL_ERROR)
    {
    	u32Ret = 1;
    }
    else
	{
		if ( OSAL_s32IOControl ( hDevice, OSAL_C_S32_IOCTRL_INVAL_FUNC,
						 (tS32)szMediaInfo ) != OSAL_ERROR )
		{
//			OEDT_HelperPrintf(TR_LEVEL_USER_1,
//			"Return Value :%d(%d)",tIoctl_ret,OSAL_ERROR);
			u32Ret	= 3;
		}
		if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
		{
			u32Ret += 2;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviInvalidMedia()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_037
* DESCRIPTION:  Test all above cases with Invalid media
* HISTORY:		 Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				    Updated Shilpa Bhat (RBIN/EDI3) July 31, 2007
*          		 Updated Shilpa Bhat (RBIN/ECM1) 12 Feb, 2008
******************************************************************************/

tU32 u32CDROMNaviInvalidMedia(void)
{
	tS32 tFunc_ret = 0;

	tFunc_ret = (tS32)u32CDROMNaviOpenDevice();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDevice-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviOpenDevInvalParm();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDevInvalParm-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviReOpenDev();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CDROMReOpenDev-%s(%d)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);


	tFunc_ret = (tS32)u32CDROMNaviOpenDevWriteOnly();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CDROMOpenDevWriteOnly-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviCloseDevice();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CloseDevice-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

   	tFunc_ret = (tS32)u32CDROMNaviOpenDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviOpenDirWithFile() ;
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDirWithFile-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviReOpenDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"ReOpenDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMNaviOpenDirWriteAccess( );
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDirWriteAccess-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret); 

	tFunc_ret = (tS32)u32CDROMNaviCloseDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CloseDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMNaviGetDirInfo();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetDirInfo-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpen();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileOpen-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenInvalPath();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenInvalPath-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenDiffModes();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDiffModes-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenDiffExtns();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDiffExtns-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenWithDirName();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileOpenDirName-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReOpen();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReOpen-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);  

	tFunc_ret = (tS32)u32CDROMNaviFileClose();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileClose-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileGetPosFrmBOF()	;
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileGetPosFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileGetPosFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileGetPosFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileSetPos();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileSetPos-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileSetPosBeyondEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"SetPosBeyondEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileRead-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadSubDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"ReadSubDir-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadLargeData();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadLargeData-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffset();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffset-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffsetFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffsetFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffsetBeyondEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffsetBeyondEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadEveryNthByteFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadEveryNthByteFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadEvreyNthByteFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadEvreyNthByteFrmEOF-%s(%d)(INV MEDIA)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetVersionNumber();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetVersionNumber-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetVolumeLabel();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetVolumeLabel-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetMediaInfo();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetMediaInfo-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviInvalFuncParm2IOCTL();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"InvalFuncParm2IOCTL-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);	
	
	tFunc_ret = (tS32)u32CDROMNaviCreateDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CreateDir-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviDelDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"DelDir-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviOpenMaxPathLength();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenMaxPathLength-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileCreate();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileCreate-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileSetPosFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileSetPosFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFirstFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FirstFileRead-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviLastFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"LastFileRead-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadInJumps();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadInJumps-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadNegOffsetFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadNegOffsetFrmBOF-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileThroughPutFirstFile();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileThroughPutFirstFile-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileThroughPutLastFile();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileThroughPutLastFile-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadInvaildCount();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadInvalidCount-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadToInvaildBuffer();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadToInvalidBuffer-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviGetMediaInfoInvalParam();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetMediaInfoInvalParam-%s(%d)(INV MEDIA)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	return 0;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviWithReverseCD()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_038
* DESCRIPTION:  Test all above cases by reversing (UP SIDE DOWN)the CD.
* HISTORY:		 Created Venkateswara.N (RBIN/ECM1) March 06 , 2006  
*				    Updated Shilpa Bhat (RBIN/EDI3) July 31, 2007
*          		 Updated Shilpa Bhat (RBIN/ECM1) 12 Feb, 2008
******************************************************************************/

tU32 u32CDROMNaviWithReverseCD(void)
{
	tS32 tFunc_ret = 0;

    tFunc_ret = (tS32)u32CDROMNaviOpenDevice();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDevice-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviOpenDevInvalParm();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDevInvalParm-%s(%d)(REV CD)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviReOpenDev();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CDROMReOpenDev-%s(%d)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);


	tFunc_ret = (tS32)u32CDROMNaviOpenDevWriteOnly();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CDROMOpenDevWriteOnly-%s(%d)(REV CD)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviCloseDevice();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CloseDevice-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

   	tFunc_ret = (tS32)u32CDROMNaviOpenDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviOpenDirWithFile() ;
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDirWithFile-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviReOpenDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"ReOpenDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMNaviOpenDirWriteAccess( );
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDirWriteAccess-%s(%d)(REV CD)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret); 

	tFunc_ret = (tS32)u32CDROMNaviCloseDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CloseDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetDirInfo();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetDirInfo-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpen();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileOpen-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenInvalPath();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenInvalPath-%s(%d)(REV CD)",tFunc_ret == 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenDiffModes();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDiffModes-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenDiffExtns();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenDiffExtns-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileOpenWithDirName();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileOpenDirName-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReOpen();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReOpen-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret); 

	tFunc_ret = (tS32)u32CDROMNaviFileClose();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileClose-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileGetPosFrmBOF()	;
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileGetPosFrmBOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileGetPosFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileGetPosFrmEOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileSetPos();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileSetPos-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);
	
	tFunc_ret = (tS32)u32CDROMNaviFileSetPosBeyondEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileSetPosBeyondEOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileRead-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadSubDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"ReadSubDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadLargeData();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadLargeData-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffset();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffset-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffsetFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffsetFrmEOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadOffsetBeyondEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadOffsetBeyondEOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadEveryNthByteFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadEveryNthByteFrmBOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviFileReadEvreyNthByteFrmEOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadEvreyNthByteFrmEOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetVersionNumber();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetVersionNumber-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetVolumeLabel();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetVolumeLabel-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviGetMediaInfo();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetMediaInfo-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	tFunc_ret = (tS32)u32CDROMNaviInvalFuncParm2IOCTL();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"InvalFuncParm2IOCTL-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);	
	
    tFunc_ret = (tS32)u32CDROMNaviCreateDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"CreateDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviDelDir();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"DelDir-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviOpenMaxPathLength();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"OpenMaxPathLenght-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileCreate();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileCreate-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileSetPosFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileSetPosFrmBOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFirstFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FirstFileRead-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviLastFileRead();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"LastFileRead-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadInJumps();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadInJumps-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadNegOffsetFrmBOF();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadNegOffsetFrmBOF-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileThroughPutFirstFile();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileThroughPutFirstFile-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileThroughPutLastFile();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileThroughPutLastFile-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadInvaildCount();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadInvalidCount-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviFileReadToInvaildBuffer();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"FileReadToInvalidBuffer-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

    tFunc_ret = (tS32)u32CDROMNaviGetMediaInfoInvalParam();
	OEDT_HelperPrintf(TR_LEVEL_USER_1,
	"GetMediaInfoInvalParam-%s(%d)(REV CD)",tFunc_ret != 0?"PASSED":"FAILED",tFunc_ret);

	return 0;
}

   /********** DTS Additions ***********/	 
/*****************************************************************************
* FUNCTION:	    u32CDROMNaviCreateDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_039
* DESCRIPTION:  Attempt to Create Directory (Should Fail)
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviCreateDir(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	tU32 u32Ret = 0;

	/* Open the directory */
	hDir = OSALUTIL_prOpenDir(OSAL_C_STRING_DIR1);
	if(OSAL_NULL != hDir)
	{
		/* Attempt to create directory */
		if(OSAL_ERROR != OSALUTIL_s32CreateDir(hDir->fd, DIR_NAME_CREATE))
		{
			u32Ret += 2;
			/* If directory create is successful, remove it */
			if(OSAL_ERROR == OSALUTIL_s32RemoveDir(hDir->fd, DIR_NAME_CREATE))
			{
				u32Ret += 4;
			}
		}
		/* Close the opened directory */
		if(OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret += 8;
		}
	}
	else
	{
		u32Ret += 1;
	}
	
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviDelDir()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_040
* DESCRIPTION:  Attempt to Remove Directory (Should Fail)
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
				Updated by Tinoy Mathews(RBIN/EDI3) on Sept 26, 2007
******************************************************************************/
tU32 u32CDROMNaviDelDir(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	tU32 u32Ret = 0;

	/* Open the directory */
	hDir = OSALUTIL_prOpenDir(OSAL_C_STRING_DIR1);	
	if(OSAL_NULL != hDir)
	{
		/* Attempt to remove an existing directory */
		if(OSAL_ERROR != OSALUTIL_s32RemoveDir(hDir->fd,DIR_NAME_DEL))
		{
			u32Ret += 2;
		}			
		/* Close the directory */
		if(OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret +=4;
		}
	}
	else
	{
		u32Ret += 1;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviOpenMaxPathLength()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_041
* DESCRIPTION:  Try to open directory with max path length
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) 
******************************************************************************/
tU32 u32CDROMNaviOpenMaxPathLength(void)
{
	OSAL_tIODescriptor hDir = 0;
	tU32 u32Ret = 0;

	/* Open the directory with maximum path length */
	hDir = OSAL_IOOpen(DIR_MAX_PATH, OSAL_EN_READONLY);
	if(OSAL_ERROR == hDir)
	{
		u32Ret = 1;
	}
	else
	{
		/* Close the directory */
		if(OSAL_ERROR == OSAL_s32IOClose(hDir))
		{
			u32Ret = 2;
		}
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileCreate()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_042
* DESCRIPTION:  Attempt to create a file on CDROM device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileCreate(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	
	/* Open the directory */
	hDir = OSALUTIL_prOpenDir(OSAL_C_STRING_DIR1);
	if(OSAL_NULL != hDir)
	{
		/* Attempt to create file within opened directory */
		hFile = OSAL_IOCreate(FILE_PATH_CREATE, OSAL_EN_READONLY);	
		if(OSAL_ERROR != hFile)
		{
			u32Ret += 2;
			/* if file create is successful, close the file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile))
			{
			   u32Ret += 4;
			}
			/* if error on close, remove the created file */	
			else if(OSAL_ERROR == OSAL_s32IORemove(FILE_PATH_CREATE))
			{
				u32Ret += 8;
			}
		}
		/* Close the opened directory */
		if(OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret += 16;
		}
	}
	else
	{
		u32Ret += 1;
	}

	return u32Ret;
}	 		

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileRemove()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDROM_043
* DESCRIPTION:  Attempt to remove a file from CDROM device
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileRemove(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	tU32 u32Ret = 0;

	/* Open the directory */
	hDir = OSALUTIL_prOpenDir(DIR_PATH_FILE_DEL);
	if(OSAL_NULL != hDir)
	{
		/* Attempt to remove file */
		if(OSAL_ERROR != OSAL_s32IORemove(FILE_PATH_DEL))
		{
			u32Ret += 2;
		}
		/* Close the opened directory */
		if(OSAL_ERROR == OSALUTIL_s32CloseDir(hDir))
		{
			u32Ret += 4;
		}
	}
	else
	{
		u32Ret += 1;
	}

	return u32Ret;
}

	#define NO_BYTES_TO_READ	12
/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileSetPosFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_044
* DESCRIPTION:  Sets position for different offsets from different positions
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileSetPosFrmBOF(void)
{
/* 	static char DataArr1[NO_BYTES_TO_READ] = {0xA8,0x8F,0x1A,0x00,
											0xDC,0xA1,0xA1,0x00,
											0x20,0xBF,0x1A,0x00};
	
	static char DataArr2[NO_BYTES_TO_READ] = {0xF8,0x02,0x9A,0x00,
											0x2C,0x1F,0x9A,0x00,
											0x88,0x32,0x9A,0x00};
											
	static char DataArr3[NO_BYTES_TO_READ] = {0xD4,0x5B,0x2C,0x01,
											0xB4,0x75,0x2C,0x01,
											0x04,0x8F,0x2C,0x01}; */	
									   	
   	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 ReadRet = 0;
	tU32 s32PosToSet=0;
	tS8 ReadData[NO_BYTES_TO_READ];

	/*Open file */
	hFile = OSAL_IOOpen(FILE_DAT_069,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		s32PosToSet = 2240;	
		/* Set byte position specified */	
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 2;
		}		
		/* Read specified number of bytes into array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,NO_BYTES_TO_READ);
		/* Check return value for error */
		if( (OSAL_ERROR == ReadRet) || ( NO_BYTES_TO_READ != ReadRet) )
		{
			u32Ret += 4; 
		}
		/*else
		{
			if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr1,
								   NO_BYTES_TO_READ))
			{
				u32Ret += 8;
			}	
		} */

 		s32PosToSet = 12800;
 		/* Set byte position specified */		 
 		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 16;
		}		
		/* Read specified number of bytes into array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,NO_BYTES_TO_READ);
		/* Check return value for error */
		if( (OSAL_ERROR == ReadRet) || ( NO_BYTES_TO_READ != ReadRet) )
		{
			u32Ret += 32; 
		}
		/*else
		{
			if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr2,
								   NO_BYTES_TO_READ))
			{
				u32Ret += 64;
			}	
		} */
		
		/* Set byte position specified */
		s32PosToSet = 24960;	   
		if(OSAL_ERROR == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet) )
		{
			u32Ret += 128;
		}		
		/* Read specified number of bytes into array */
		ReadRet = OSAL_s32IORead(hFile,ReadData,NO_BYTES_TO_READ);
		/* Check return value for error */
		if( (OSAL_ERROR == ReadRet) || ( NO_BYTES_TO_READ != ReadRet) )
		{
			u32Ret += 256; 
		}
		/*else
		{
			if(OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr3,
								   NO_BYTES_TO_READ))
			{
				u32Ret += 512;
			}	
		}
		*/
		/* Close file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 1024;
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFirstFileRead( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_045
* DESCRIPTION:  Read Contents of the first data file				
* HISTORY:		 Created by Shilpa Bhat (RBIN/EDI3)
* CONDITION:    Copy the specified file, i.e. "/dev/cdrom/DATA/LIB/COMPRESS.DAT" 
				    into ramdisk folder (/dev/ram1)
*					 Modified logic by Shilpa Bhat (RBIN/ECM1) on 21/02/08
******************************************************************************/
tU32 u32CDROMNaviFirstFileRead(void)
{
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;
	tS32 s32PosToSet = 0;
	tU32 u32Ret = 0;

	tU32 FileLen = 0;
	tS8 *ReadBuf1 = OSAL_NULL;
	tS8 *ReadBuf2 = OSAL_NULL;

	tS32 s32ReadRet = 0;
	tChar szFilePath [45] = OSAL_C_STRING_DEVICE_RAMDISK"/CONNECT.DAT";
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;

	/* Open file from /dev/ram1 */
	hFile1 = OSAL_IOOpen (szFilePath, enAccess);	
	if ( hFile1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		FileLen = (tU32)OSALUTIL_s32FGetSize(hFile1);

		/* Allocate memory for read buffer */
		ReadBuf1 = (tS8 *)OSAL_pvMemoryAllocate(FileLen+1);
		if(OSAL_NULL == ReadBuf1)		
		{
			u32Ret += 2;
			return u32Ret;
		}
		else
		{
			s32ReadRet = OSAL_s32IORead (
                                hFile1,
                                ReadBuf1,
                                FileLen
                             );
			/*Check the status of read*/
			if ( s32ReadRet == OSAL_ERROR )
			{
				u32Ret = 3;	
    		}
		 }
		 /* Close file */
		if( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		
		/* Open file */	  
		 hFile2 = OSAL_IOOpen(FIRST_DAT_FILE,OSAL_EN_READONLY);
		 if(OSAL_ERROR != hFile2)
		 {
			/* Set byte position specified */
			s32PosToSet = 0;					
			if(OSAL_ERROR == OSAL_s32IOControl(hFile2, OSAL_C_S32_IOCTRL_FIOSEEK,(tS32)s32PosToSet))
			{
				u32Ret += 8;
			}
			/* Allocate memory for read buffer */
			ReadBuf2 = (tS8 *)OSAL_pvMemoryAllocate(FileLen+1);
			if(OSAL_NULL == ReadBuf2)
			{
				u32Ret += 10;
				return u32Ret;
			}
			/* Read contents of file into read buffer */
			s32ReadRet = OSAL_s32IORead(hFile2,ReadBuf2,FileLen);
			if((OSAL_ERROR == s32ReadRet) || (FileLen != (tU32)s32ReadRet))
			{
				u32Ret += 100;
			}

			else
			{
				/* Compare read bytes with contents of buffer read previously */
				if(OSAL_ERROR == OSAL_s32StringNCompare(ReadBuf2,ReadBuf1,FileLen))
				{
					u32Ret += 1000;
				}
			}
			/* Close file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile2))
			{
				u32Ret += 10000;
			}
		 }
		 else
		 {
			u32Ret += 100000;
		 }
		 /* Free memory */
		 if(OSAL_NULL != ReadBuf1)
		 {
			OSAL_vMemoryFree(ReadBuf1);
			ReadBuf1 = OSAL_NULL;
		 }

		 if(OSAL_NULL != ReadBuf2)
		 {
			OSAL_vMemoryFree(ReadBuf2);
			ReadBuf2 = OSAL_NULL;
		 }
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviLastFileRead( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_046
* DESCRIPTION:  Read Contents of the last data file.				
* HISTORY:	    Created by Shilpa Bhat (RBIN/EDI3)
*CONDITION:     Copy the specified file, i.e. "/dev/cdrom/DATA/RNW/NAV54379.DAT" 
*				    into ramdisk folder (/dev/ram1)
*					 Modified logic by Shilpa Bhat (RBIN/ECM1) on 21/02/08
******************************************************************************/
tU32 u32CDROMNaviLastFileRead(void)
{
	OSAL_tIODescriptor hFile1 = 0;
	OSAL_tIODescriptor hFile2 = 0;

	tU32 u32Ret = 0;

	tU32 FileLen = 0;
	tS8 *ReadBuf1 = OSAL_NULL;
	tS8 *ReadBuf2 = OSAL_NULL;
	tS32 s32PosToSet = 0;
	tS32 s32ReadRet = 0;
	tChar szFilePath [45] = OSAL_C_STRING_DEVICE_RAMDISK"/LID00004.DAT";
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;

	/* Open file from /dev/ram1 */
	hFile1 = OSAL_IOOpen (szFilePath, enAccess);	
	if ( hFile1 == OSAL_ERROR )
	{
		u32Ret = 1;
	}
	else
	{
		FileLen = (tU32)OSALUTIL_s32FGetSize(hFile1);

		/* Allocate memory for read buffer */
		ReadBuf1 = (tS8 *)OSAL_pvMemoryAllocate(FileLen+1);
		if(OSAL_NULL == ReadBuf1)		
		{
			u32Ret += 2;
			return u32Ret;
		}
		else
		{
			s32ReadRet = OSAL_s32IORead (
                                hFile1,
                                ReadBuf1,
                                FileLen
                             );
			/*Check the status of read*/
			if ( s32ReadRet == OSAL_ERROR )
			{
				u32Ret = 3;	
    		}
		 }
		 /* Close file */
		if( OSAL_s32IOClose ( hFile1 ) == OSAL_ERROR )
		{
			u32Ret += 10;
		}
		
		/* Open file */	  
		 hFile2 = OSAL_IOOpen(LAST_DAT_FILE,OSAL_EN_READONLY);
		 if(OSAL_ERROR != hFile2)
		 {
			/* Set byte position specified */
			s32PosToSet = 0;					
			if(OSAL_ERROR == OSAL_s32IOControl(hFile2, OSAL_C_S32_IOCTRL_FIOSEEK,(tS32)s32PosToSet))
			{
				u32Ret += 8;
			}
			/* Allocate memory for read buffer */
			ReadBuf2 = (tS8 *)OSAL_pvMemoryAllocate(FileLen+1);
			if(OSAL_NULL == ReadBuf2)
			{
				u32Ret += 10;
				return u32Ret;
			}
			/* Read contents of file into read buffer */
			s32ReadRet = OSAL_s32IORead(hFile2,ReadBuf2,FileLen);
			if((OSAL_ERROR == s32ReadRet) || (FileLen != (tU32)s32ReadRet))
			{
				u32Ret += 100;
			}

			else
			{
				/* Compare read bytes with contents of buffer read previously */
				if(OSAL_ERROR == OSAL_s32StringNCompare(ReadBuf2,ReadBuf1,FileLen))
				{
					u32Ret += 1000;
				}
			}
			/* Close file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile2))
			{
				u32Ret += 10000;
			}
		 }
		 else
		 {
			u32Ret += 100000;
		 }
		 /* Free memory */
		 if(OSAL_NULL != ReadBuf1)
		 {
			OSAL_vMemoryFree(ReadBuf1);
			ReadBuf1 = OSAL_NULL;
		 }

		 if(OSAL_NULL != ReadBuf2)
		 {
			OSAL_vMemoryFree(ReadBuf2);
			ReadBuf2 = OSAL_NULL;
		 }
	}
		
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadInJumps( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_047
* DESCRIPTION:  Read Contents of the first and last file alternatively.				
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileReadInJumps(void)
{
	OSAL_tIODescriptor hFile1=0;
	OSAL_tIODescriptor hFile2=0;
	static char DataArr1[BYTES_TO_READ_NAVI] = {0x03,0x00,0x10,0x00,
											0x43,0x50,0x52,0x4E,
											0x41,0x56,0x5F,0x32,
											0x20,0x07,0x00,0x00,
											0x03,0x00,0x00,0x00};
	static char DataArr2[BYTES_TO_READ_NAVI] = {0xD0,0x01,0x6B,0xD4,
											0x00,0x80,0x0A,0x00,
											0x22,0x01,0x36,0x01,
											0x1A,0x00,0x06,0x00,
											0x00,0x00,0x00,0x00};

	tU32 u32Ret = 0;
	tU32 LoopCount = 0;
	tS8 ReadData[BYTES_TO_READ_NAVI];
	tS32 ReadRet = 0;
	tU32 s32PosToSet = 0;
        tU32 u32_max_laser_jumps = MAX_LASER_JUMPS_NAVI;

	/* Read contents of two files alternatively 100 times */
	for(LoopCount =0 ; LoopCount < u32_max_laser_jumps; LoopCount++)
	{
            OSAL_s32ThreadWait(10);	//wait for 0,1s
		/* First File Open */
		hFile1 = OSAL_IOOpen (FIRST_DAT_FILE,OSAL_EN_READONLY );
		if( OSAL_ERROR == hFile1)
		{
			u32Ret = 1;
		}
		else
		{
			/* Set byte positon specified */
			s32PosToSet = 0;
			if(OSAL_ERROR == OSAL_s32IOControl(hFile1,OSAL_C_S32_IOCTRL_FIOSEEK,
								(tS32)s32PosToSet))
			{
				u32Ret += 2;
			}
			/* Read file contents into buffer */		
			ReadRet = OSAL_s32IORead(hFile1,ReadData,BYTES_TO_READ_NAVI);
			if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_NAVI != ReadRet ) )
			{
				u32Ret += 4; 
			}
			else
			{
				/* Compare read bytes with content previously read into buffer */
				if(OSAL_ERROR == OSAL_s32StringNCompare((tCString)ReadData,
									(tCString)DataArr1, BYTES_TO_READ_NAVI))
				{
					u32Ret += 8;
				}	
			}
			/* Close file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile1))
			{
				u32Ret += 16;
			}
		}
		/* Second File Open */
		hFile2 = OSAL_IOOpen (LAST_DAT_FILE,OSAL_EN_READONLY );
		if( OSAL_ERROR == hFile2)
		{
			u32Ret += 32;
		}
		else
		{
			/* Set byte position specified */
			s32PosToSet = 0;
			if(OSAL_ERROR == OSAL_s32IOControl ( hFile2,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
			{
				u32Ret += 64;
			}
			/* Read contents of file into buffer */		
			ReadRet = OSAL_s32IORead(hFile2,ReadData,BYTES_TO_READ_NAVI);
			if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_NAVI != ReadRet) )
			{
				u32Ret += 128; 
			}
			else
			{
				/* Compare read bytes with contents previously read into buffer */
				if(OSAL_ERROR == OSAL_s32StringNCompare((tCString)ReadData,(tCString)DataArr2,
								   BYTES_TO_READ_NAVI))
				{
					u32Ret += 256;
				}	
			}
			/* Close file */
			if(OSAL_ERROR == OSAL_s32IOClose(hFile2))
			{
				u32Ret += 512;
			}
		}
	}
	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadNegOffsetFrmBOF( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_048
* DESCRIPTION:  Try to Read Contents with negative offset from BOF				
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) 
******************************************************************************/
tU32 u32CDROMNaviFileReadNegOffsetFrmBOF(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 s32PosToSet = 0;
	tS32 ReadRet = 0;
	tS8 *ReadBuf = 0;
	tS32 s32BytesToRead = 10;

	/* File Open */
	hFile = OSAL_IOOpen(FIRST_DAT_FILE, OSAL_EN_READONLY);
	if(OSAL_ERROR != hFile)
	{
		/* Set byte position specified */
		s32PosToSet = -10;
		if(OSAL_ERROR != OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOSEEK, (tS32) s32PosToSet))
		{
			u32Ret += 2;
		}
		else
		{
			/* Allocate memory for read buffer */
			ReadBuf = (tS8 *)OSAL_pvMemoryAllocate((tU32)s32BytesToRead);
			/* Read the contents of file into buffer */
			ReadRet = OSAL_s32IORead(hFile,ReadBuf,(tU32)s32BytesToRead);
			if(OSAL_ERROR != ReadRet)
			{
				u32Ret += 4;
			}
		}
		/* Close file */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 10;
		}
	}
	else
	{
		u32Ret += 1;
	}
	
	return u32Ret;
}	

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileThroughPutFirstFile( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_049
* DESCRIPTION:  Calculate the rate of data transfer for a read operation 
* HISTORY:		Created by Shilpa Bhat(RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileThroughPutFirstFile(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 Read_ThroughPut=0;
   	OSAL_tMSecond StartTime=0; 
	OSAL_tMSecond EndTime =0;
	OSAL_tMSecond ReadTime=0;
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	
	/* Allocate memory for read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_TPUT1 + 10);

	/* File Open */
	hFile = OSAL_IOOpen (FIRST_DAT_FILE,OSAL_EN_READONLY);
	if(OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
            /* Set byte position specified */
            s32PosToSet = 0;
            if(OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
                                                  (tS32)s32PosToSet))
		{
                    u32Ret += 2;
		}
            /* Read the time before the start of the read operation */	
            StartTime = OSAL_ClockGetElapsedTime();
            /* Read the specified number of bytes into array */
            ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ_TPUT1);
            /* Read the time before the end of the read operation */
            EndTime = OSAL_ClockGetElapsedTime();
            
            if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_TPUT1 != ReadRet ) )
		{
                    u32Ret += 4; 
		}
            else
		{
                    /* Check if end time greater than start time */
                    if(EndTime > StartTime){
                        /* Calculate the time required for read in millisec */
                        /* Resch: Time si alrteady in ms */
                        ReadTime =   (EndTime - StartTime); // /1000;
                    }else{
                        /*ReadTime =   (StartTime-EndTime); // /1000;*/		  //changed for testing - shilpa
								ReadTime = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
                    }
                    /* Throughput needs to be expressed in kB/s */ 
                    /* check for ReadTime != 0 */
                    if(ReadTime != 0){
                        /* now we have bytes/ms which is == kB/s */ 
                        Read_ThroughPut = (BYTES_TO_READ_TPUT1) / (ReadTime);                                                              
                    }else{
                        Read_ThroughPut = 0;
                    }
                    OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32CDROMNaviFileThroughPutFirstFile: %d kB/s  (%d Bytes read) ",
                                      Read_ThroughPut, 
                                      BYTES_TO_READ_TPUT1);
                    if(Read_ThroughPut < 300 )
                        {
                            u32Ret += 8;
                        }
                    
                }
            /* Close file */
            if(OSAL_ERROR == OSAL_s32IOClose(hFile))
                {
                    u32Ret += 32;
                }
        }
        /* Free Memory */
        if(OSAL_NULL != ps8ReadBuffer)
            {
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
            }
	return u32Ret;
}

/*****************************************************************************
 * FUNCTION:	    u32CDROMNaviFileThroughPutLastFile( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_050
* DESCRIPTION:  Calculate the rate of data transfer for a read operation 
* HISTORY:		Created by Shilpa Bhat(RBIN/EDI3)
******************************************************************************/
tU32 u32CDROMNaviFileThroughPutLastFile(void)
{
  	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tU32 Read_ThroughPut=0;
   	OSAL_tMSecond StartTime=0; 
	OSAL_tMSecond EndTime =0;
	OSAL_tMSecond ReadTime=0;
	tU32 s32PosToSet = 0;
	tS8 *ps8ReadBuffer = OSAL_NULL;
	tS32 ReadRet = 0;
	/* Allocate memory for read buffer */
	ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (BYTES_TO_READ_TPUT2 + 10);

	/* File Open */
	hFile = OSAL_IOOpen (LAST_DAT_FILE,OSAL_EN_READONLY );
	if( OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/* Set file position specified */
		s32PosToSet = 0;
		if(OSAL_ERROR  == OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,
							(tS32)s32PosToSet))
		{
			u32Ret += 2;
		}
		/* Read the time before the start of the read operation */	
	    StartTime = OSAL_ClockGetElapsedTime();
	   	/* Read specified number of bytes into array */
		ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,BYTES_TO_READ_TPUT2);
		/* Read the time before the end of the read operation */
		EndTime = OSAL_ClockGetElapsedTime();
	
		if( (OSAL_ERROR == ReadRet) || (BYTES_TO_READ_TPUT2 != ReadRet ) )
		{
			u32Ret += 4; 
		}
		else
		{
			/* Check if end time greater than start time */
			if(EndTime > StartTime)
			{
				/* Calculate the time required for read in millisec*/
                            ReadTime =   (EndTime - StartTime); 
				/* Throughput needs to be expressed in kB/s */
                        }else{
                         /*   ReadTime =   (StartTime-EndTime); // /1000;*/   //changed for testing - shilpa
								 ReadTime = ( 0xFFFFFFFF - StartTime ) + EndTime+1;
                        }
                        if(ReadTime != 0){
                            /* now we have bytes/ms which is == kB/s */ 
                            Read_ThroughPut = (BYTES_TO_READ_TPUT2) / (ReadTime);                                                              
                        }else{
                            Read_ThroughPut = 0;
                        }
                        
                        OEDT_HelperPrintf(TR_LEVEL_USER_1,"u32CDROMNaviFileThroughPutFirstFile: %d kB/s  (%d Bytes read) ",
                                          Read_ThroughPut, 
                                          BYTES_TO_READ_TPUT2);
                        if(	Read_ThroughPut < 300 )
                            {
                                u32Ret += 8;
                            }
                        
                        
                }
		/* File Close */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 32;
		}
	}
	/* Free Memory */
	if(OSAL_NULL != ps8ReadBuffer)
	{
		OSAL_vMemoryFree(ps8ReadBuffer);
		ps8ReadBuffer = OSAL_NULL;
	}
	return u32Ret;
}

#define READ_BYTES 10
/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadInvaildCount()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_051
* DESCRIPTION:  Try to Read Contents with invalid read count				
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) 
******************************************************************************/
tU32 u32CDROMNaviFileReadInvaildCount(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 ReadRet = 0;
	tS8 *ReadBuf = 0;
	tS32 s32BytesToRead = READ_BYTES;

	/* File Open */
	hFile = OSAL_IOOpen(FIRST_DAT_FILE,OSAL_EN_READONLY);
	if(OSAL_ERROR != hFile)
	{
		/* Allocate memory for read buffer */
		ReadBuf = (tS8 *)OSAL_pvMemoryAllocate((tU32)s32BytesToRead);
		/* Set byte position specified */
		s32BytesToRead = -10;
		/* Read specified number of bytes into read buffer */	
		ReadRet = OSAL_s32IORead(hFile,ReadBuf,(tU32)s32BytesToRead);
		if(OSAL_ERROR != ReadRet)
		{
			u32Ret += 2;
		}
		/* File Close */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 4;
		}			
	}
	else
	{
		u32Ret += 1;
	}
	/* Free Memory */
	if(OSAL_NULL != ReadBuf)
	{
		OSAL_vMemoryFree(ReadBuf);
		ReadBuf = OSAL_NULL;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReadToInvaildBuffer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_052
* DESCRIPTION:  Try to Read Contents into invalid buffer
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3) 
******************************************************************************/
tU32 u32CDROMNaviFileReadToInvaildBuffer(void)
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;
	tS32 ReadRet = 0;
	tS32 s32BytesToRead = READ_BYTES;
	/* File Open */
	hFile = OSAL_IOOpen(FIRST_DAT_FILE, OSAL_EN_READONLY);
	if(OSAL_ERROR != hFile)
	{
		/* Read specified number of bytes into read buffer */
		ReadRet = OSAL_s32IORead(hFile,OSAL_NULL,(tU32)s32BytesToRead);
		if(OSAL_ERROR != ReadRet)
		{
			u32Ret += 2;
		}
		/* File Close */
		if(OSAL_ERROR == OSAL_s32IOClose(hFile))
		{
			u32Ret += 32;
		}			
	}
	else
	{
		u32Ret += 1;
	}

	return u32Ret;
}



/*****************************************************************************
* FUNCTION:	   	u32CDROMNaviGetMediaInfoInvalParam( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_053
* DESCRIPTION:  Get information about the media with invalid parameters
* HISTORY:		Created by Shilpa Bhat (RBIN/EDI3)
*               Modified by Shilpa Bhat (RBIN/ECM1) on 31 July, 2008
******************************************************************************/
tU32 u32CDROMNaviGetMediaInfoInvalParam(void)
{
	OSAL_tIODescriptor hDevice = 0;
    tU32 u32Ret = 0;


    /* Open Device */
    hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDROM,OSAL_EN_READONLY);
	if (OSAL_ERROR != hDevice)
	{
		/* Attempt to get media information with invalid buffer */
		if(OSAL_ERROR != OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO,
							OSAL_NULL))
		{
			u32Ret	+= 2;
		}
		/* Close Device */
		if(OSAL_ERROR == OSAL_s32IOClose(hDevice))
		{
			u32Ret += 4;
		}
	}
	else
    {
    	u32Ret += 1;
    }

	return u32Ret;
}

/*****************************************************************************
* FUNCTION:	   	u32CDROMNaviReadAsync( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_054
* DESCRIPTION:  Read asynchronously from the file
* HISTORY:		Created by Shilpa Bhat (RBIN/ECM1) on July 29, 2008
******************************************************************************/
tU32 u32CDROMNaviReadAsync(tVoid)
{
	OSAL_tIODescriptor hFile = 0;	
	OSAL_trAsyncControl rAsyncCtrl = {0};   	
    tS8 *ps8ReadBuffer = NULL;
    tU32 u32Ret = 0;               
    tU32 u32BytesToRead = 10;
    tS32 s32BytesRead = 0;
    tS32 s32Result = 0;
   	tU32 u32OsalRetVal = 0;
	tChar szSemName[]   = "SemAsyncCDROMRead";
   	tS32 s32RetVal = OSAL_ERROR;
	
	hFile = OSAL_IOOpen(FILE_DAT_069,OSAL_EN_READONLY);
	if(OSAL_ERROR == hFile)
	{
		u32Ret = 1;
	}
	else
	{
		/*Allocate memory*/
		ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (u32BytesToRead+1);
		/*Check if memory allocated successfully*/
   		if ( ps8ReadBuffer == NULL )
	   	{
	      	u32Ret = 2;
	   	}
		else
		{
	   		/*Create semaphore for event signalling used by async callback*/
			s32RetVal = OSAL_s32SemaphoreCreate (szSemName, &hSemAsyncCDROMNavi,0);
			/*Check if semaphore is already existent*/
		   	if(s32RetVal == OSAL_ERROR)
		    {
				u32Ret = 3;      
		    }
			else
			{
				/*asynchronous control structure*/
				rAsyncCtrl.id = hFile;
			    rAsyncCtrl.s32Offset = 0;
			    rAsyncCtrl.pvBuffer = ps8ReadBuffer;
			    rAsyncCtrl.u32Length = u32BytesToRead;
			    rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTCDROMNaviAsyncCallback;
			    rAsyncCtrl.pvArg = &rAsyncCtrl;

			   	if ( OSAL_s32IOReadAsync ( &rAsyncCtrl ) == OSAL_ERROR )
			    {
			    	u32Ret += 10;     
				}
    			else
    			{
				      s32Result = OSAL_s32SemaphoreWait (hSemAsyncCDROMNavi, 5000);
					  if (( s32Result != OSAL_OK )&&( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT ))
      				  {
         				s32Result = OSAL_s32IOCancelAsync (rAsyncCtrl.id,&rAsyncCtrl);
						u32Ret += 20; 			
        			  }
      				  else
      				  {
					  	u32OsalRetVal = OSAL_u32IOErrorAsync ( &rAsyncCtrl );        				
         				if (!(u32OsalRetVal == OSAL_E_INPROGRESS||u32OsalRetVal == OSAL_E_CANCELED))
						{
							s32BytesRead = OSAL_s32IOReturnAsync(&rAsyncCtrl);				
							if( s32BytesRead != (tS32)u32BytesToRead)
            				{
            					u32Ret += 100;
            				}     	
						}
         	    	}
		    	}
			}		    	      
   			if(ps8ReadBuffer != NULL)
			{
   				OSAL_vMemoryFree (ps8ReadBuffer);
				ps8ReadBuffer = NULL;
			} 
		}
		if( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
		{
			u32Ret += 200;
		}
	}

	OSAL_s32SemaphoreClose ( hSemAsyncCDROMNavi);
   	OSAL_s32SemaphoreDelete( szSemName );

   	return u32Ret;   
}

/*****************************************************************************
* FUNCTION:	    u32CDROMNaviFileReClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_CDROM_055
* DESCRIPTION:  Try to Close a file that is already closed
* HISTORY:		Created by Rakesh Dhanya (RBIN/EDI3) on May 28,2007 
******************************************************************************/
tU32 u32CDROMNaviFileReClose()
{
	OSAL_tIODescriptor hFile = 0;
	tU32 u32Ret = 0;

	hFile = OSAL_IOOpen(FILE_DAT_069,OSAL_EN_READONLY);
	if( OSAL_ERROR == hFile )
	{
		u32Ret += 1;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32IOClose ( hFile ))
		{
			u32Ret += 2;
		}
		else
		{
			if(OSAL_ERROR != OSAL_s32IOClose ( hFile )  )
			{
				u32Ret += 4;
			}
		}
 	}
	return u32Ret;
}

