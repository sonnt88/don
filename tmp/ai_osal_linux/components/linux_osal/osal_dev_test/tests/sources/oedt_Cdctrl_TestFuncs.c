/******************************************************************************
*FILE         : oedt_Cdctrl_TestFuncs.c
*
*SW-COMPONENT : OEDT_FrmWrk  
*
*DESCRIPTION  : This file implements the individual test cases for the CdCtrl 
*               device	( OEDT testcases).
*               Checking for valid pointer in each test cases is not required
*               as it is ensured in the calling function that that it cannot
*               be NULL.
*
*AUTHOR(s)    : Narasimha Prasad Palasani
*
*COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
*
*HISTORY      : 
* ____________________________________________________________________________
*			     Initial  Version
*               15.05.06 ver  1.0  RBIN/ECM1-Narasimha Prasad Palasani
*               Created a seperate file for OEDT-CD-Ctrl Test functions
*               
*				23.05.06 ver  1.1  RBIN/ECM1-Narasimha Prasad Palasani
*               vTtfisTrace is used for printing 
*               Register and Unregister notification functions are updated
*               
*               02.04.09 ver 1.2  RBIN/ECF1-Shilpa Bhat 
*               Lint Removal 
______________________________________________________________________________

*****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! TRACE Calls from test functions are NOT ALLOWED !!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
#define vTtfisTrace(...)
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomar (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osdevice.h"

#define HIWORD(l) ((tU16)((tU32)(l) >> 16))

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define NUM_BYTES_PER_BLOCK   2048   //2Kb

#define OSAL_C_U16_NOTI_ALL   ( OSAL_C_U16_NOTI_MEDIA_CHANGE  | \
OSAL_C_U16_NOTI_TOTAL_FAILURE  | OSAL_C_U16_NOTI_MODE_CHANGE  | \
OSAL_C_U16_NOTI_MEDIA_STATE    | OSAL_C_U16_NOTI_DVD_OVR_TEMP ) 

#define OSAL_C_S32_IOCTRL_INVALID_DEVICE "dev/invalid"

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/******************************************************************************
 *FUNCTION     :  vMediaChangeNotiHandler 
 *
 *DESCRIPTION  :  Function to handle the notification of type 
 *                OSAL_C_U16_NOTI_MEDIA_CHANGE.This is a callback function.
 *
 *PARAMETER    :  tU32* pu32MediaChangeInfo:(->I) - Data regarding Media change
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  28.07.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *****************************************************************************/
tVoid vMediaChangeNotiHandler(const tU32* pu32MediaChangeInfo)
{
  if(pu32MediaChangeInfo != NULL)
  {
    switch((tU16) *pu32MediaChangeInfo)
    {
    case OSAL_C_U16_MEDIA_EJECTED:
      vTtfisTrace("Media Change Notification - Media Ejected"); 
      break;

    case OSAL_C_U16_INCORRECT_MEDIA:
      vTtfisTrace("Media Change Notification - Incorrect Media"); 
      break;

    case OSAL_C_U16_DATA_MEDIA:
      vTtfisTrace("Media Change Notification - Data Media"); 
      break;

    case OSAL_C_U16_AUDIO_MEDIA:
      vTtfisTrace("Media Change Notification - Audio Media"); 
      break;

    case OSAL_C_U16_UNKNOWN_MEDIA:
      vTtfisTrace("Media Change Notification - Unknown Media");
      break;

    default:
      vTtfisTrace("Media Change Notification - Error Unknown data");

    } /*End of switch*/

  }
  else /*Invalid pointer*/
  {
    vTtfisTrace("Invalid pointer");
  }

}  /* End of vMediaChangeNotiHandler*/


/******************************************************************************
 *FUNCTION     :  vTotalFailureNotiHandler 
 *
 *DESCRIPTION  :  Function to handle notification of type 
 *                OSAL_C_U16_NOTI_TOTAL_FAILURE.This is a callback function.
 *
 *PARAMETER    :  tU32* pu32FailureInfo:(->I) - Data regarding Failure
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  28.07.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *****************************************************************************/
tVoid vTotalFailureNotiHandler(const tU32* pu32FailureInfo)
{
  if(pu32FailureInfo != NULL)
  {
    switch((tU16) *pu32FailureInfo)
    {
    case OSAL_C_U16_DEVICE_OK:
      vTtfisTrace("Device Failure Notification - Device OK");
      break;

    case OSAL_C_U16_DEVICE_FAIL:
      vTtfisTrace("Device Failure Notification - Device FAIL");
      break;

    default:
      vTtfisTrace("Device Failure Notification - Error Unknown data");
    }
  }
  else /*Invalid pointer */
  {
    vTtfisTrace("Invalid pointer");
  }

} /* End of vTotalFailureNotiHandler*/


/******************************************************************************
 *FUNCTION     :  vModeChangeNotiHandler 
 *
 *DESCRIPTION  :  Function to handle notification of type 
 *                OSAL_C_U16_NOTI_MODE_CHANGE.This is a callback function.
 *
 *PARAMETER    :  tU32* pu32ModeChangeInfo:(->I) - Data regarding mode change
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  28.07.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *                24.09.03  RBIN/ECM2- Pemmaiah
 *                Corrected the type of notification handled by this function.
 *                - A MODE_CHANGE notification happens whenever an exclusive
                  access is granted or released (the information within the
                  notification is the AppID of the application that currently
                  has exclusive access, or APPID_INVALID on shared access)
 *****************************************************************************/
tVoid vModeChangeNotiHandler(const tU32* pu32ModeChangeInfo)
{
  if(pu32ModeChangeInfo != NULL)
  {
    vTtfisTrace("Mode Change Notification - The AppID is: 0x%X",(tS32)*pu32ModeChangeInfo);
  }
  else /*Invalid pointer*/
  {
    vTtfisTrace("Invalid pointer");
  }

} /* End of vModeChangeNotiHandler */


/******************************************************************************
 *FUNCTION     :  vMediaStateChangeNotiHandler 
 *
 *DESCRIPTION  :  Function to handle notification of type 
 *                OSAL_C_U16_NOTI_MEDIA_STATE.This is a callback function.
 *
 *PARAMETER    :  tU32* pu32StateChangeInfo:(->I) - Data regarding state change
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  28.07.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *                24.09.03  RBIN/ECM2- Pemmaiah
 *                Corrected the type of notification handled by this function.
 *****************************************************************************/
tVoid vMediaStateChangeNotiHandler(const tU32* pu32StateChangeInfo)
{
  if(pu32StateChangeInfo != NULL)
  {
    switch((tU16) *pu32StateChangeInfo)
    {
    case OSAL_C_U16_MEDIA_READY:
      vTtfisTrace("Media State Change Notification - Media Ready");
      break;

    case OSAL_C_U16_MEDIA_NOT_READY:
      vTtfisTrace("Media State Change Notification - Media Not Ready");
      break;

    default:
      vTtfisTrace("Media State Change Notification - Error Unknown data");
    }

  }
  else /*Invalid pointer*/
  {
    vTtfisTrace("Invalid pointer");
  }

} /* End of vMediaStateChangeNotiHandler */


/******************************************************************************
 *FUNCTION     :  vTemperatureNotiHandler 
 *
 *DESCRIPTION  :  Function to handle notification of type 
 *                OSAL_C_U16_NOTI_DVD_OVR_TEMP. This is a callback function.
 *
 *PARAMETER    :  tU32* pu32TempChangeInfo:(->I) - Data regarding Temp change
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  28.07.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *****************************************************************************/
tVoid vTemperatureNotiHandler(const tU32* pu32TempChangeInfo)
{
  if(pu32TempChangeInfo != NULL)
  {
    switch((tU16) *pu32TempChangeInfo)
    {
    case OSAL_C_U16_OVER_TEMPERATURE:
      vTtfisTrace("Temperature Notification - Over Temperature");
      break;

    case OSAL_C_U16_NORMAL_TEMPERATURE:
      vTtfisTrace("Temperature Notification - Normal Temperature");
      break;

    default:
      vTtfisTrace("Temperature Notification - Error Unknown data");
    }

  }
  else /*Invalid pointer*/
  {
    vTtfisTrace("Invalid pointer");

  }

}  /* End of vTemperatureNotiHandler */

/******************************************************************************
 *FUNCTION     :  vAllChangeNotiHandler 
 *
 *DESCRIPTION  :  Function to handle all type of notification mentioned below
 *                i.e for 
 *                OSAL_C_U16_NOTI_MEDIA_CHANGE  
 *                OSAL_C_U16_NOTI_TOTAL_FAILURE       
 *                OSAL_C_U16_NOTI_MODE_CHANGE          
 *                OSAL_C_U16_NOTI_MEDIA_STATE          
 *                OSAL_C_U16_NOTI_DVD_OVR_TEMP              
 *
 *PARAMETER    :  tU32* pu32MediaChangeInfo:(->I) - Data regarding Media change
 *             
 *RETURNVALUE  :  None
 *
 *HISTORY      :  30.09.03  RBIN/ECM2- Pemmaiah
 *                Initial Revision.
 *****************************************************************************/
tVoid vAllChangeNotiHandler(const tU32* pu32ChangeNotiInfo)
{
  if(pu32ChangeNotiInfo != NULL)
  {
    /*Check for the type of notification. For this extract the high word
    & based on the notification type call the corresponding notification
    handler*/

    tU16 u16NotiType = 0;
    u16NotiType = HIWORD(*pu32ChangeNotiInfo);

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      vMediaChangeNotiHandler(pu32ChangeNotiInfo);
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      vTotalFailureNotiHandler(pu32ChangeNotiInfo);
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE:
      vModeChangeNotiHandler(pu32ChangeNotiInfo);
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE:
      vMediaStateChangeNotiHandler(pu32ChangeNotiInfo);
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
      vTemperatureNotiHandler(pu32ChangeNotiInfo);
      break;

    default:
      vTtfisTrace("Error - Unknown NOTIFICATION type!");

    } /*End of switch*/
  }
  else /*Invalid pointer*/
  {
    vTtfisTrace("Invalid pointer");
  }
}


/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/




/*****************************************************************************
* FUNCTION:		u32CDCtrlDevOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_001
* DESCRIPTION:  Opens the CDCtrl device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23, 2006 
*
******************************************************************************/
tU32 u32CDCtrlDevOpen(void)
{
  OSAL_tIODescriptor hDevice = 0;                            
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlOpenDevDiffModes()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_002
* DESCRIPTION:  Try to open the device in different access modes
*				(Read Only,Read write,Write Only)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23, 2006 
*
******************************************************************************/
tU32 u32CDCtrlOpenDevDiffModes(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = (OSAL_tenAccess)0;
  tU32 u32Ret = 0;

  /* In Read Only mode */
  enAccess = OSAL_EN_READONLY;
  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
  }
  /* In Read Write mode */
  enAccess = OSAL_EN_READWRITE;
  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR)
  {
    u32Ret += 10;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 20;
    }
  }
  /* In Write Only mode */
  enAccess = OSAL_EN_WRITEONLY;
  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR)
  {
    u32Ret += 100;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 200;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlDevOpenInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_003
* DESCRIPTION:  Device open with invalid parameter/handle (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23, 2006 
*
******************************************************************************/
tU32 u32CDCtrlDevOpenInvalParam(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_S32_IOCTRL_INVALID_DEVICE , enAccess );
  if( hDevice != OSAL_ERROR )
  {
    u32Ret = 1;

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlDevReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_004
* DESCRIPTION:  Try to open the device which is already opened
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlDevReOpen(void)
{
  OSAL_tIODescriptor hDevice1 = 0,hDevice2 = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice1 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice1 == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    hDevice2 = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

    if( hDevice2 == OSAL_ERROR )
    {
      u32Ret = 10;
    }
    else
    {
      if( OSAL_s32IOClose ( hDevice2 ) == OSAL_ERROR )
      {
        u32Ret = 100;
      }
    }
    if( OSAL_s32IOClose ( hDevice1 ) == OSAL_ERROR )
    {
      u32Ret += 1000;
    }

  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlDevOpenNullParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_005
* DESCRIPTION:  Try to open the device by passing NULL 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlDevOpenNullParam(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( NULL , enAccess );

  if( hDevice != OSAL_ERROR )
  {
    u32Ret = 1;

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }

  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_006
* DESCRIPTION:  Closes the CDCtrl Device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlDevCloseInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_007
* DESCRIPTION:  Device close with invalid parameter/handle (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlDevCloseInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;

  if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
  {
    return 1;
  }

  return 0;

}

/*****************************************************************************
* FUNCTION:		u32CDCtrlDevReClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_008
* DESCRIPTION:  Try to close an already closed device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlDevReClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOClose ( hDevice ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }

  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadMedia()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_009
* DESCRIPTION:  loads the media into the drive
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlLoadMedia(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl(hDevice ,OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR,0) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose(hDevice) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadMediaInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_010
* DESCRIPTION:  loading  the media into the drive with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlLoadMediaInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR , 0 ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadMediaAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_011
* DESCRIPTION:  Try Loading the media after closing the device (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
*****************************************************************************/
tU32 u32CDCtrlLoadMediaAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR , 0) != OSAL_ERROR )
      {
        u32Ret = 3;
      }

    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOn( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_012
* DESCRIPTION:  Setting the motor ON
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlMotorOn(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON , 0) == OSAL_ERROR )
    {
      u32Ret = 2;

    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOnInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_013
* DESCRIPTION:  Setting the Motor On with invalid parameters 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlMotorOnInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON, 0 ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOnAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_014
* DESCRIPTION:  Setting the Motor ON after closing the device 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlMotorOnAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON , 0) != OSAL_ERROR )
      {
        u32Ret = 3;
      }

    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOff()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_015
* DESCRIPTION:  Setting the Motor OFF
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*               30.07.2008, Lars Tracht (PJ-AI/CF32):
*               added test for availability of device after motor on
*               ( due to changed behaviour of driver )
*
******************************************************************************/

tU32 u32CDCtrlMotorOff(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trIOCtrlDir* hDir = NULL;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  { /*set motor off*/
    if( OSAL_s32IOControl ( hDevice,
                            OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF, 0 ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }

    /*set motor on*/
    if( OSAL_s32IOControl ( hDevice,
                            OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON, 0 ) == OSAL_ERROR )
    {
      u32Ret = 3;
    }

    /* try to read directory to check availability of device */
    do
    {
      hDir = OSALUTIL_prOpenDir ( "/dev/cdrom/TestCD/DreamTheatre" );
      if( hDir == OSAL_NULL )
      {
       // OEDT_HelperPrintf( TR_LEVEL_USER_1, "Trying to read directory on DVD. Wait 1s before next try." );
        OSAL_s32ThreadWait(1000);
      }
    } while( hDir == OSAL_NULL );

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }

  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOffInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_016
* DESCRIPTION:  Setting the Motor OFF with invalid parameters (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlMotorOffInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF , 0 ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlMotorOffAfterDevClose() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_017
* DESCRIPTION:  Setting the Motor OFF after closing the device (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlMotorOffAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF , 0 ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadStat()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_018
* DESCRIPTION:  Tests for the function that gets the loader status
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlLoadStat(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trLoaderInfo rLoaderInfo; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO ,
                            (tS32)&rLoaderInfo) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      vTtfisTrace("Loader status information : %d \n" , rLoaderInfo.u8LoaderInfoByte);
    } 
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadStatInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_019
* DESCRIPTION:  Try to get the loader info with invalid parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlLoadStatInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trLoaderInfo rLoaderInfo;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO ,(tS32)&rLoaderInfo) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlLoadStatAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_020
* DESCRIPTION:  Try to get the loader info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlLoadStatAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trLoaderInfo rLoaderInfo;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice, 
                              OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO, 
                              (tS32)&rLoaderInfo) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDriveTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_021
* DESCRIPTION:  Gets the drive temperature 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDriveTemp(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDriveTemperature rDriveTemp; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP , (tS32)&rDriveTemp) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      vTtfisTrace("Temparature : %d \n",rDriveTemp.u8Temperature);
      /*check value*/
      if(rDriveTemp.u8Temperature==0)
      {
        u32Ret = 3;
      }/*end if*/
    }/*end else*/
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDriveTempInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_022
* DESCRIPTION:  Try to get the drive temperature by giving invalid parameters 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetDriveTempInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trDriveTemperature rDriveTemp;

  if( OSAL_s32IOControl ( hDevice ,
                          OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP , (tS32)&rDriveTemp ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDriveTempAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_023
* DESCRIPTION:  Try to get the drive temperature after closing the device 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetDriveTempAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDriveTemperature rDriveTemp;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP,(tS32)&rDriveTemp ) != OSAL_ERROR)
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetMediaInfo() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_024
* DESCRIPTION:  Gets the media info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetMediaInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trMediaInfo rMediaInfo; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice,
                            OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO,(tS32)&rMediaInfo) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("Type of media : %d /n",rMediaInfo.u8MediaType);
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetMediaInfoInvalParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_025
* DESCRIPTION:  Gets the media info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetMediaInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trMediaInfo rMediaInfo; 

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO , (tS32)&rMediaInfo ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetMediaInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_026
* DESCRIPTION:  Try to get the Media info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetMediaInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trMediaInfo rMediaInfo; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO , 
                              (tS32)&rMediaInfo ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetCDInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_027
* DESCRIPTION:  Gets the CD info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetCDInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trCDInfo rCDInfo; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO , (tS32)&rCDInfo) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("Min Track : %u , Max Track : %u ",
                rCDInfo.u32MinTrack,rCDInfo.u32MaxTrack);
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetCDInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_028
* DESCRIPTION:  Try to get the CD info with invalid parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetCDInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trCDInfo  rCDInfo; 

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO , (tS32)&rCDInfo ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetCDInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_029
* DESCRIPTION:  Try to get the CD info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetCDInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trCDInfo  rCDInfo;  

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO ,(tS32)&rCDInfo ) != OSAL_ERROR)
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetTrackInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_030
* DESCRIPTION:  Gets the track info of the inserted media
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetTrackInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trCDROMTrackInfo rTrackInfo; 
  rTrackInfo.u32TrackNumber = 1;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO,(tS32)&rTrackInfo ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("Track number : %u \n Address: %u \n Track Control Flags: %u \n  ", 
                rTrackInfo.u32TrackNumber, rTrackInfo.u32LBAAddress, 
                rTrackInfo.u32TrackControl);
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetTrackInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_031
* DESCRIPTION:  Try to get the track info with invalid parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetTrackInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trCDROMTrackInfo rTrackInfo;
  rTrackInfo.u32TrackNumber = 1;

  if( OSAL_s32IOControl ( hDevice, 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO, (tS32)&rTrackInfo ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetTrackInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_032
* DESCRIPTION:  Try to get the track info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetTrackInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trCDROMTrackInfo rTrackInfo;  
  rTrackInfo.u32TrackNumber = 1;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice, 
                              OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO, 
                              (tS32)&rTrackInfo ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetErrInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_033
* DESCRIPTION:  Gets the CDctrl error info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
#define SIZE 15
tU32 u32CDCtrlGetErrInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trGetErrorBuffer rErrInfo; 
  rErrInfo.ps8ErrorBuffer = OSAL_NULL;
  rErrInfo.s16BufferSize = SIZE;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {   /*this function are not supported for project GMGE !!*/
    if( OSAL_s32IOControl ( hDevice , OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER,(tS32)&rErrInfo) == OSAL_ERROR)
    { /*check if error code not supported set*/
      if(OSAL_u32ErrorCode()!=OSAL_E_NOTSUPPORTED)
      {
        u32Ret = 2;
      }/*end if*/
    }/*end if*/
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("Error Information  : %d \n ", *(rErrInfo.ps8ErrorBuffer));
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetErrInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_034
* DESCRIPTION:  Try to get  the error info with invalid parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetErrInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trGetErrorBuffer rErrInfo; 
  rErrInfo.ps8ErrorBuffer = OSAL_NULL;
  rErrInfo.s16BufferSize = SIZE;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER , (tS32)&rErrInfo ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetErrInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_035
* DESCRIPTION:  Try to get  the error info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetErrInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trGetErrorBuffer rErrInfo; 
  rErrInfo.ps8ErrorBuffer = OSAL_NULL;
  rErrInfo.s16BufferSize = SIZE;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER , 
                              (tS32)&rErrInfo ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlRawDataRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_036
* DESCRIPTION:  Tests for the fuction that reads the raw data from the given
 *              address. The size of the data to be read is also given by 
 *              the user, This is in terms of number of block - each block
 *              is 2 K bytes
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRawDataRead(void)
{ 
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;    
  OSAL_trReadRawInfo rReadRawInfo;

  rReadRawInfo.u32LBAAddress = 10;
  rReadRawInfo.u32NumBlocks = 2;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {/*allocate memory*/
    rReadRawInfo.ps8Buffer = OSAL_pvMemoryAllocate(NUM_BYTES_PER_BLOCK*rReadRawInfo.u32NumBlocks);
    if( OSAL_s32IOControl ( hDevice , OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA,(tS32)&rReadRawInfo) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
    /*set memory free*/
    OSAL_vMemoryFree(rReadRawInfo.ps8Buffer);
  }
  if(!u32Ret)
  {
    vTtfisTrace("Raw Data Read  : %s /n ", rReadRawInfo.ps8Buffer);
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlRawDataReadInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_037
* DESCRIPTION:  Try to get the raw data with invalid parameters
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRawDataReadInvalidParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trReadRawInfo rReadRawInfo;

  rReadRawInfo.ps8Buffer = OSAL_NULL;
  rReadRawInfo.u32LBAAddress = 10;
  rReadRawInfo.u32NumBlocks = 2;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA ,(tS32)&rReadRawInfo) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRawDataReadAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_038
* DESCRIPTION:  Try to get the raw data after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRawDataReadAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trReadRawInfo rReadRawInfo;

  rReadRawInfo.ps8Buffer = OSAL_NULL;
  rReadRawInfo.u32LBAAddress = 10;
  rReadRawInfo.u32NumBlocks = 2;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA ,
                              (tS32)&rReadRawInfo ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlMSFRead()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_039
* DESCRIPTION:  Tests for the fuction that reads the msf data from the given
 *              address. The size of the data to be read is also given by 
 *              the user, This is in terms of number of block - each block
 *              is 2 K bytes
* HISTORY:		Created Andrea Bueter(TMS)  Apr 16,2008 
*
******************************************************************************/
tU32 u32CDCtrlMSFRead(void)
{
  OSAL_tIODescriptor       VhDevice = 0;
  tS32                     Vs32OsalReturn = 0;    
  tU32                     Vu32Ret = 0;

  /*open device*/
  VhDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL ,OSAL_EN_READWRITE );
  /*error*/
  if(VhDevice==OSAL_ERROR)
  {
    Vu32Ret = 1;
  }/*end if*/
  else
  {
    tS8                      Vs8SectorBuffer[NUM_BYTES_PER_BLOCK];
    OSAL_trReadRawMSFInfo    VrRawSector;

    VrRawSector.ps8Buffer    =  Vs8SectorBuffer;
    VrRawSector.u32NumBlocks = 1;
    // set parameter (special)
    VrRawSector.u8Minute = 0;
    VrRawSector.u8Second = 1;
    VrRawSector.u8Frame = 8;
    // read sector
    Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);
    /*check error*/ 
    if(OSAL_OK != Vs32OsalReturn)
    {
      Vu32Ret = 2;
    }/*end if*/
    /*close device*/
    if( OSAL_s32IOClose(VhDevice)==OSAL_ERROR)
    {
      Vu32Ret += 10;
    }
  }/*end else*/
  return Vu32Ret;
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CDCtrlMSFReadInvalidParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_040
* DESCRIPTION:  Try to get the msf data with invalid parameters
* HISTORY:		Created Andrea Bueter(TMS)  Apr 16,2008 
*
******************************************************************************/
tU32 u32CDCtrlMSFReadInvalidParam(void)
{
  OSAL_tIODescriptor       VhDevice = -1;
  tS32                     Vs32OsalReturn = 0;    
  OSAL_trReadRawMSFInfo    VrRawSector;
  tU32                     Vu32Ret = 0;

  VrRawSector.ps8Buffer    =  OSAL_NULL;
  VrRawSector.u32NumBlocks = 0;
  // set parameter (special)
  VrRawSector.u8Minute = 0;
  VrRawSector.u8Second = 0;
  VrRawSector.u8Frame = 0;
  // read sector
  Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);
  /*check error*/ 
  if(OSAL_OK == Vs32OsalReturn)
  {
    Vu32Ret = 2;
  }/*end if*/
  return Vu32Ret;
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CDCtrlMSFReadAfterDevClose( )
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_041
* DESCRIPTION:  Try to get the raw data after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlMSFReadAfterDevClose(void)
{
  OSAL_tIODescriptor       VhDevice = 0;
  tS32                     Vs32OsalReturn = 0;    
  tU32                     Vu32Ret = 0;

  /*open device*/
  VhDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL ,OSAL_EN_READWRITE );
  /*error*/
  if(VhDevice==OSAL_ERROR)
  {
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*close device*/
    if( OSAL_s32IOClose(VhDevice)==OSAL_ERROR)
    {
      Vu32Ret += 10;
    }
    else
    {
      tS8                      Vs8SectorBuffer[NUM_BYTES_PER_BLOCK];
      OSAL_trReadRawMSFInfo    VrRawSector;

      VrRawSector.ps8Buffer    =  Vs8SectorBuffer;
      VrRawSector.u32NumBlocks = 1;
      // set parameter (special)
      VrRawSector.u8Minute = 0;
      VrRawSector.u8Second = 1;
      VrRawSector.u8Frame = 8;
      // read sector
      Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);
      /*check error*/ 
      if(OSAL_OK == Vs32OsalReturn)
      {
        Vu32Ret = 2;
      }/*end if*/     
    }/* end else*/
  }/*end else*/
  return Vu32Ret;
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetVer()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_042
* DESCRIPTION:  Gets the version
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetVer(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  tS32 s32VersionInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_VERSION ,(tS32)&s32VersionInfo) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace( " Version   : %d \n ",s32VersionInfo );
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetVerInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_043
* DESCRIPTION:  Try to get the version with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetVerInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  tS32 s32VersionInfo = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_VERSION ,(tS32)&s32VersionInfo) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;

}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetVerAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_044
* DESCRIPTION:  Try to get the version without opening the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetVerAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  tS32 s32VersionInfo;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_VERSION , (tS32)&s32VersionInfo ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDVDInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_045
* DESCRIPTION:  Gets DVD info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDVDInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDvdInfo rDVDInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice, 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO,(tS32)&rDVDInfo) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("StartSector : %d"  ,rDVDInfo.u32StartSector );
    vTtfisTrace("EndSector : %d \n ",rDVDInfo.u32EndSector );
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDVDInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_046
* DESCRIPTION:  Try to get the DVD info with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDVDInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trDvdInfo rDVDInfo;

  if( OSAL_s32IOControl ( hDevice ,
                          OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO ,(tS32)&rDVDInfo ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDVDInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_047
* DESCRIPTION:  Try to get the DVD info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDVDInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDvdInfo rDVDInfo;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO,(tS32)&rDVDInfo) != OSAL_ERROR)
      {
        u32Ret = 3;
      }
    }                  
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDevInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_048
* DESCRIPTION:  Gets the device info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDevInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  tS32 s32DeviceInfo = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice, 
                            OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO,
                            (tS32)&s32DeviceInfo) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  if(!u32Ret)
  {
    vTtfisTrace("Device information : %d \n",s32DeviceInfo);
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrGetDevInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_049
* DESCRIPTION:  Gets the device info
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrGetDevInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  tS32 s32DeviceInfo = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO,(tS32)&s32DeviceInfo ) != OSAL_ERROR)
  {
    u32Ret = 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDevInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_050
* DESCRIPTION:  Try to get the device info afte closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlGetDevInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  tS32 s32DeviceInfo = 0;  

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO ,
                              (tS32)&s32DeviceInfo) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }                  
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDiskInfo()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_051
* DESCRIPTION:  Gets the disk type
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetDiskInfo(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDiskType rDiskType;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl( hDevice,
                           OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE,(tS32)&rDiskType) == OSAL_ERROR )
    {
      u32Ret = 2;

    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }

  }
  if(!u32Ret)
  {
    vTtfisTrace("Disk Type : %d /n Disk Sub Type : %d /n",
                rDiskType.u8DiskType,rDiskType.u8DiskSubType);
  }

  return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDiskInfoInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_052
* DESCRIPTION:  Try to get the disk type with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetDiskInfoInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trDiskType rDiskType;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE ,(tS32)&rDiskType) != OSAL_ERROR )
  {
    u32Ret = 1;
  }

  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDiskInfoAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_053
* DESCRIPTION:  Try to get the device info afte closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlGetDiskInfoAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trDiskType rDiskType;  

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE ,
                              (tS32)&rDiskType) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }                  
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagMedChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_054
* DESCRIPTION:  Register & Unregister Notification for Media change for DIAGNOSIS 
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackDiagMedChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagTotFailure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_055
* DESCRIPTION:  Register & Unregister  Notification for Total Failure for DIAGNOSIS
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackDiagTotFailure(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_TOTAL_FAILURE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTotalFailureNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagModeChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_056
* DESCRIPTION:  Register & Unregister  Notification for Mode Change for DIAGNOSIS
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackDiagModeChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MODE_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vModeChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice , 
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice , 
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagMedState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_057
* DESCRIPTION:  Register & Unregister  Notification for Media State for DIAGNOSIS
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackDiagMedState(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_STATE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaStateChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice, 
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice, 
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagDvdOvrTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_058
* DESCRIPTION:  Register & Unregister Notification for DVD Over Temp for DIAGNOSIS
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackDiagDvdOvrTemp(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DVD_OVR_TEMP;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTemperatureNotiHandler;

    if(OSAL_s32IOControl (hDevice, 
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice, 
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDiagNotiAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_059
* DESCRIPTION:  Register & Unregister Notification for Notify All for DIAGNOSIS
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDiagNotiAll(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DIAGNOSIS_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_ALL;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vAllChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownMedChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_060
* DESCRIPTION:  Register & Unregister Notification for Media Change for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownMedChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownTotFailure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_061
* DESCRIPTION:  Register & Unregister  Notification for Total Failure for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownTotFailure(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_TOTAL_FAILURE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTotalFailureNotiHandler;

    if(OSAL_s32IOControl ( hDevice ,
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice ,
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }

  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownModeChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_062
* DESCRIPTION:  Register & Unregister Notification for Mode Change for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownModeChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MODE_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vModeChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice, 
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice, 
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownMedState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_063
* DESCRIPTION:  Register & Unregister  Notification for Media State for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownMedState(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_STATE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaStateChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice,
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice,
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;

      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownDvdOvrTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_064
* DESCRIPTION:  Register & Unregister  Notification for DVD Over Temp for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownDvdOvrTemp(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DVD_OVR_TEMP;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTemperatureNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDownNotiAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_065
* DESCRIPTION:  Register & Unregister  Notification for Notify All for DOWNLOAD
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDownNotiAll(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DOWNLOAD_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_ALL;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vAllChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiMedState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_066
* DESCRIPTION:  Registers and Unregisters the callback for media state for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiMedState(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_STATE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaStateChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiTotFailure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_067
* DESCRIPTION:  Register & Unregister Notification for Total Failure for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiTotFailure(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_TOTAL_FAILURE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTotalFailureNotiHandler;

    if(OSAL_s32IOControl ( hDevice,
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice,
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiModeChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_068
* DESCRIPTION:  Register & Unregister Notification for Mode Change for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiModeChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MODE_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vModeChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice ,
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice ,
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData) == OSAL_ERROR )
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiMedChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_069
* DESCRIPTION:  Register & Unregister Notification for Media Change for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiMedChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiDvdOvrTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_070
* DESCRIPTION:  Register & Unregister  Notification for DVD Over Temp for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiDvdOvrTemp(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DVD_OVR_TEMP;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTemperatureNotiHandler;

    if(OSAL_s32IOControl ( hDevice, 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice, 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackDapiNotiAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_071
* DESCRIPTION:  Register & Unregister Notification for Notify All for DAPI
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackDapiNotiAll(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_DAPI_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_ALL;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vAllChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }   
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherMedChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_072
* DESCRIPTION:  Register & Unregister  Notification for Media Change for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherMedChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice,
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice,
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherTotFailure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_073
* DESCRIPTION:  Register & Unregister  Notification for Total Failure for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherTotFailure(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_TOTAL_FAILURE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTotalFailureNotiHandler;

    if( OSAL_s32IOControl ( hDevice ,
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherModeChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_074
* DESCRIPTION:  Register & Unregister  Notification for Mode Change for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherModeChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MODE_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vModeChangeNotiHandler;

    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherMedState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_075
* DESCRIPTION:  Register & Unregister  Notification for Media State for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherMedState(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_STATE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaStateChangeNotiHandler;

    if( OSAL_s32IOControl ( hDevice ,
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherDvdOvrTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_076
* DESCRIPTION:  Register & Unregister Notification for DVD Over Temp for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherDvdOvrTemp(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DVD_OVR_TEMP;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTemperatureNotiHandler;

    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR )
      {
        u32Ret += 10;

      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackOtherNotiAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_077
* DESCRIPTION:  Register & Unregister  Notification for Notify All for OTHERAPP
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackOtherNotiAll(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_ALL;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vAllChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice,
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice,
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalMedChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_078
* DESCRIPTION:  Register & Unregister  Notification for Media Change for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackInvalMedChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaChangeNotiHandler;

    if( OSAL_s32IOControl ( hDevice ,
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice ,
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalTotFailure()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_079
* DESCRIPTION:  Register & Unregister Notification for Total Failure for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackInvalTotFailure(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_TOTAL_FAILURE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTotalFailureNotiHandler;

    if(OSAL_s32IOControl ( hDevice , 
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData ) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice , 
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData ) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalModeChange()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_080
* DESCRIPTION:  Register & Unregister  Notification for Mode Change for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackInvalModeChange(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MODE_CHANGE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vModeChangeNotiHandler;

    if(OSAL_s32IOControl (hDevice, 
                          OSAL_C_S32_IOCTRL_REG_NOTIFICATION , (tS32)&rNotifyData) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl (hDevice, 
                            OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION , (tS32)&rNotifyData) != OSAL_ERROR)
      {
        u32Ret += 10;

      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalMedState()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_081
* DESCRIPTION:  Register & Unregister Notification for Media State for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlRegUnregCallBackInvalMedState(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_STATE;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vMediaStateChangeNotiHandler;

    if(OSAL_s32IOControl ( hDevice ,
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice ,
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalDvdOvrTemp()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_082
* DESCRIPTION:  Register & Unregister  Notification for DVD Over Temp for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackInvalDvdOvrTemp(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_DVD_OVR_TEMP;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vTemperatureNotiHandler;

    if(OSAL_s32IOControl ( hDevice ,
                           OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if(OSAL_s32IOControl ( hDevice ,
                             OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
    } 
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlRegUnregCallBackInvalNotiAll()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_083
* DESCRIPTION:  Register & Unregister  Notification for Notify All for INVALID
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlRegUnregCallBackInvalNotiAll(void)
{    
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trNotifyData rNotifyData;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    rNotifyData.u16AppID = OSAL_C_U16_INVALID_APPID;
    rNotifyData.ResourceName = OSAL_C_STRING_RES_CDCTRL; 
    rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_ALL;
    rNotifyData.pCallback = (OSALCALLBACKFUNC)vAllChangeNotiHandler;

    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_REG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION ,(tS32)&rNotifyData ) != OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 100;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlCopyProtection()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_084
* DESCRIPTION:  Tests for copy protected
* HISTORY:		Created Andrea Bueter TMS 11.09.07 
*
******************************************************************************/
#define DAP_DEV_COPY_PROTECTION_STR "BLAUPUNKT GMBH"

tU32 u32CDCtrlCopyProtection(void)
{
  OSAL_tIODescriptor            VhDevice = 0;
  tU32                          Vu32Ret = 0;
  tU8                           VbTestCopyCdInDVDDevice=FALSE;


  /*open device*/
  VhDevice=OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL,OSAL_EN_READONLY);
  if( VhDevice == OSAL_ERROR )
  {
    Vu32Ret = 1;
  }/*end if*/
  else
  { /*--------------------------------------------------------------*/
    OSAL_tenIOCtrlMediaDevice     VenTempDeviceInfo;
    OSAL_tenIOCtrlMediaDevice     VrfenDeviceType=OSAL_EN_MEDIADEV_CD;
    /*################### get device type ###################*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO,(tS32)&VenTempDeviceInfo)==OSAL_ERROR )
    {
      Vu32Ret = 2;
    }/*end if*/
    else
    {
      if(OSAL_EN_MEDIADEV_DVD == VenTempDeviceInfo)
      {
        VrfenDeviceType = OSAL_EN_MEDIADEV_DVD;
      }/*end if*/
      else if(OSAL_EN_MEDIADEV_CD == VenTempDeviceInfo)
      {
        VrfenDeviceType = OSAL_EN_MEDIADEV_CD;
      }/*end else if*/
      else
      {
        VrfenDeviceType = OSAL_EN_MEDIADEV_CD;
      }/*else*/
    }/*end else*/
    /* ###################copy protection for DVD ###########################*/
    if(VrfenDeviceType == OSAL_EN_MEDIADEV_DVD)
    { //1000 1111 Bit 0-3  = DiskSubType
      //          Bit 4-6  = Reserved
      //          Bit 7    = Blaupunkt-Rom
      //ATAPI_C_U8_DISK_SUB_TYPE_BLAUPUNKT_ROM  = 0x80
      //ATAPI_C_U8_DISK_SUB_TYPE_DVD_ROM        = 0x04
      tU8               Vu8ValidBits=0x8f;
      OSAL_trDiskType   VrDiskType;
      if(OSAL_s32IOControl( VhDevice,OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE, (tS32)&VrDiskType)==OSAL_OK)
      {
        if((ATAPI_C_U8_DISK_SUB_TYPE_BLAUPUNKT_ROM | ATAPI_C_U8_DISK_SUB_TYPE_DVD_ROM) ==
           ((VrDiskType.u8DiskSubType) & Vu8ValidBits) //Nur die gueltigen Bits kontrollieren
          )
        {
          ///valid DVD
        }/*end if*/
        else
        {
          if(VrDiskType.u8DiskType==ATAPI_C_U8_DISK_TYPE_CD)
          {/*check COPY protection for CD in DVD device*/
            VbTestCopyCdInDVDDevice=TRUE;
          }/*end if*/
          else
          {
            Vu32Ret += 10; /*unvalid CD*/
          }/*end if*/
        }
      }/*end if*/
      else Vu32Ret += 100; /*IO- Control failed*/
    }/*end if*/
    if(  (VrfenDeviceType == OSAL_EN_MEDIADEV_CD)||(VbTestCopyCdInDVDDevice == TRUE))
    /* ###################copy protection for CD ###########################*/
    {
      /* OSAL_EN_MEDIADEV_CD*/
      tS8                      Vs8SectorBuffer[NUM_BYTES_PER_BLOCK];
      tS32                     Vs32OsalReturn;
      OSAL_trReadRawMSFInfo    VrRawSector;
      VrRawSector.ps8Buffer =  Vs8SectorBuffer;
      // set parameter (global)
      VrRawSector.ps8Buffer = Vs8SectorBuffer;
      VrRawSector.u32NumBlocks = 1;
      // set parameter (special)
      VrRawSector.u8Minute = 0;
      VrRawSector.u8Second = 1;
      VrRawSector.u8Frame = 8;
      // read sector
      Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);
      /*check error*/ 
      if( OSAL_OK != Vs32OsalReturn)
      { // set parameter (special)
        VrRawSector.u8Minute = 0;
        VrRawSector.u8Second = 0;
        VrRawSector.u8Frame = 6;        
        // read sector
        Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);
        /*check  error*/
        if( OSAL_OK != Vs32OsalReturn)
        {  // set parameter (special)
          VrRawSector.u8Minute = 0;
          VrRawSector.u8Second = 0;
          VrRawSector.u8Frame = 2;           
          // read sector
          Vs32OsalReturn = OSAL_s32IOControl( VhDevice, OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF,(tS32)&VrRawSector);          
        }/*end if*/
      }/*end if*/
      if( Vs32OsalReturn == OSAL_OK)
      {
        if( 0 != OSAL_s32StringNCompare( Vs8SectorBuffer,DAP_DEV_COPY_PROTECTION_STR,OSAL_u32StringLength(DAP_DEV_COPY_PROTECTION_STR)))
        {
          Vu32Ret += 10; /*unvalid CD*/
        }/*end if*/
      }/*end if*/
      else
      {
        Vu32Ret += 100; /*IO- Control failed*/
      }     
    }/*end else OSAL_EN_MEDIADEV_CD*/
    /*--------------------------------------------------------------*/
    /*close device */
    if( OSAL_s32IOClose( VhDevice ) == OSAL_ERROR )
    {
      Vu32Ret += 1000;
    }/*end if*/
  }/*end else*/
  return Vu32Ret;
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectMedia()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_085
* DESCRIPTION:  Ejects the media present in the drive
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectMedia(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA, 0 ) == OSAL_ERROR )
    {
      u32Ret = 2;

    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  } 
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectMediaInvalParam() 
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_086
* DESCRIPTION:  Media Eject with invalid parameter/handle (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectMediaInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;

  if( OSAL_s32IOControl ( hDevice, 
                          OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA , 0 ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectMedAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_087
* DESCRIPTION:  Media Eject with after Device Close (should fail)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectMedAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , 
                              OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA , 0) != OSAL_ERROR )
      {
        u32Ret = 3;
      }

    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectLockInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_088
* DESCRIPTION:  Try to lock the eject with invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectLockInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK, ATAPI_C_U8_LOCK_EJECT ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectLockAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_089
* DESCRIPTION:  Try to get the DVD info after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectLockAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice, 
                              OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK, ATAPI_C_U8_LOCK_EJECT ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }                   
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectLock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_090
* DESCRIPTION:  Tests for the CD eject lock function of the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/

tU32 u32CDCtrlEjectLock(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK , ATAPI_C_U8_LOCK_EJECT ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlEjectUnlock()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_091
* DESCRIPTION:  Tests for the CD eject unlock function of the device
* HISTORY:		Created Andrea Bueter TMS 11.09.07 
*
******************************************************************************/
tU32 u32CDCtrlEjectUnlock(void)
{
  OSAL_tIODescriptor VhDevice = 0;
  OSAL_tenAccess VenAccess = OSAL_EN_READWRITE;
  tU32 Vu32Ret = 0;

  /*open device*/
  VhDevice=OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL,VenAccess);
  if( VhDevice == OSAL_ERROR )
  {
    Vu32Ret = 1;
  }/*end if*/
  else
  {
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK,ATAPI_C_U8_UNLOCK_EJECT )==OSAL_ERROR )
    {
      Vu32Ret = 2;
    }/*end if*/
    if( OSAL_s32IOClose( VhDevice ) == OSAL_ERROR )
    {
      Vu32Ret += 10;
    }/*end if*/
  }/*end else*/
  return Vu32Ret;
}/*end fucntion*/
/*****************************************************************************
* FUNCTION:		u32CDCtrlMultiDevOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_092
* DESCRIPTION:  Opens CDCtrl device for multiple times(65 times)
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlMultiDevOpen(void)
{
  OSAL_tIODescriptor hDevice[255];
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tU32 u32Loopcount = 0;
  tU32 u32Loopend = 255;

  for(u32Loopcount = 0; u32Loopcount < u32Loopend; u32Loopcount++)
  {
    hDevice[u32Loopcount] = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL ,enAccess );

    if( hDevice[u32Loopcount] == OSAL_ERROR )
    {
      u32Loopend = u32Loopcount;
      u32Ret =  u32Loopcount;

      vTtfisTrace("Opening failed for %d time \n",u32Loopend);
      break;
    }
  }
  for(u32Loopcount = 0;u32Loopcount < u32Loopend; u32Loopcount++)
  {
    if( OSAL_s32IOClose ( hDevice[u32Loopcount] ) == OSAL_ERROR )
    {
      vTtfisTrace("Closing failed for %d th time",u32Loopcount);
      u32Ret += 1000;
    }
  }
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32CDCtrlPowerOffInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_093
* DESCRIPTION:  Try to off the power invalid parameter
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlPowerOffInvalParam(void){
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;

  if( OSAL_s32IOControl ( hDevice , 
                          OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF , 0) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlPowerOffAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_094
* DESCRIPTION:  Try to off the power after closing the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlPowerOffAfterDevClose(void){
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice, 
                              OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF, 0 ) != OSAL_ERROR )
      {
        u32Ret = 3;
      }
    }                  
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlPowerOff()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_095
* DESCRIPTION:  Tests for the Power Off function of the device
* HISTORY:		Created Narasimha Prasad Palasani (RBIN/ECM1)  Feb 23,2006 
*
******************************************************************************/
tU32 u32CDCtrlPowerOff(void){
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );

  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl ( hDevice , 
                            OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF , 0 ) != OSAL_ERROR )
    {
      u32Ret = 2;
    }
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDeviceVersion()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_096
* DESCRIPTION:  Tests for the get device version
* HISTORY:		Created Andrea B�ter(TMS)  9.10.2008
*
******************************************************************************/
tU32 u32CDCtrlGetDeviceVersion(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trCdDeviceVersion rDriveVersion; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_CDCTRL_GET_DEVICE_VERSION , (tS32)&rDriveVersion) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    { /*check value*/
      if(rDriveVersion.SWVersion[0]==0)
      {
        u32Ret = 3;
      }/*end if*/
    }/*end else*/
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDeviceVersionInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_097
* DESCRIPTION:  Tests for the get device version with invalid param
* HISTORY:		Created Andrea Bueter(TMS)  9.10.2008
*
******************************************************************************/
tU32 u32CDCtrlGetDeviceVersionInvalParam(void)
{
  OSAL_tIODescriptor hDevice = -1;
  tU32 u32Ret = 0;
  OSAL_trCdDeviceVersion rDriveVersion; 

  if( OSAL_s32IOControl ( hDevice ,OSAL_C_S32_IOCTRL_CDCTRL_GET_DEVICE_VERSION , (tS32)&rDriveVersion ) != OSAL_ERROR )
  {
    u32Ret = 1;
  }
  return u32Ret;
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32CDCtrlGetDeviceVersionAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TS_OEDT_CDCTRL_098
* DESCRIPTION:  Tests for the get device version after dev close
* HISTORY:		Created Andrea Bueter(TMS)  9.10.2008
*
******************************************************************************/
tU32 u32CDCtrlGetDeviceVersionAfterDevClose(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trCdDeviceVersion rDriveVersion; 

  hDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL , enAccess );
  if( hDevice == OSAL_ERROR )
  {
    u32Ret = 1;
  }
  else
  {
    if( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
    {
      u32Ret = 2;
    }
    else
    {
      if( OSAL_s32IOControl ( hDevice , OSAL_C_S32_IOCTRL_CDCTRL_GET_DEVICE_VERSION,(tS32)&rDriveVersion ) != OSAL_ERROR)
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;
}/*end function*/







