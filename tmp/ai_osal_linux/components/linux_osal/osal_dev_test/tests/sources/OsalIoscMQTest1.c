#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include <OsalIoscTestCommon.h>
#include <OsalIoscMQCommon.h>

OSAL_tSemHandle hOsalSem1;
OSAL_tSemHandle hOsalSem2;

volatile tS32 gsRetVal = 0;
volatile tU32 guRetFlg = 0;

extern volatile tS32 gsRetVal2;
extern volatile tU32 guRetFlg2;

/*****************************************************************************
* FUNCTION		:  SubTestCase1( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase1 w.r.t Iosc MQ use-case sequence1
* 					Task 1 		Task 2
*					Create
*								Open
*					Close
*								Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase1( OEDT_TESTSTATE* state )
{

	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hTestMQ_1 = 0;
	OSAL_tMQueueHandle hMessageQueue_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Create MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE");
	hTestMQ_1 = CreateMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state );

	/* Post to Sem1 : Task2 will OPEN MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after OPEN MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hTestMQ_1, state );

	/* Post to to Sem1 : Task2 will CLOSE MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Delete MQ*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE NEW");
	/* create a MQ with different size to check whether
	the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						IOSC_TEST_MQ_CHANNEL,
					   (IOSC_TEST_MQ_MAX_MSGNUM/2),
					   (IOSC_TEST_MQ_MAX_MSGLEN/2),
					   OSAL_EN_READONLY, &hMessageQueue_1);

	if(s32ErrorCode == OSAL_OK)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : Error creating new MQ with different size");

	/* Close new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Delete new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

}

/*****************************************************************************
* FUNCTION		:  SubTestCase2( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase2 w.r.t Iosc MQ use-case sequence2
* 					Task 1 		Task 2
*								Create
*					Open
*								Close
*					Delete
*					Close
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase2( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hTestMQ_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem2 : waiting for Task2 to signal after CREATE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Open MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : OPEN");
	hTestMQ_1 = OpenMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state);

	/* Post to Sem1 : Task2 will CLOSE MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem1);
	/* Wait on Sem2 : waiting for Task2 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Delete MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hTestMQ_1, state );

	/* Post to Sem1 : Task2 will CREATE_NEW MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem1 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase3( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase3 w.r.t Iosc MQ use-case sequence3
* 					Task 1 		Task 2
*					Create
*								Open
*					Close
*								Close
*								Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase3( OEDT_TESTSTATE* state )
{

	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hMessageQueue_1 = 0;
	OSAL_tMQueueHandle hTestMQ_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE");
	hTestMQ_1 = CreateMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state );

	/* Post to Sem1 : Task2 will OPEN MQ on this Post*/
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after OPEN MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hTestMQ_1, state );

	/* Post to Sem1 : Task2 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE NEW");
	/* create a MQ with different size to check whether
		the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						IOSC_TEST_MQ_CHANNEL,
						(IOSC_TEST_MQ_MAX_MSGNUM/2),
						(IOSC_TEST_MQ_MAX_MSGLEN/2),
						OSAL_EN_READONLY, &hMessageQueue_1);

	if(s32ErrorCode == OSAL_OK)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : Error creating new MQ with different size");

	/* Close new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Delete new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase4( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase4 w.r.t Iosc MQ use-case sequence4
* 					Task 1 		Task 2
*								Create
*					Open
*								Delete
*					Close
*								Close
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase4( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hTestMQ_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Wait on Sem2 : waiting for Task2 to signal on CREATE MQ*/
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : OPEN");
	hTestMQ_1 = OpenMessageQueue( IOSC_TEST_MQ_CHANNEL, OSAL_EN_READONLY, state);

	/* Post to Sem1 : Task2 will DELETE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after DELETE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hTestMQ_1, state );

	/* Post to Sem1 : Task2 will CLOSE MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase5( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase5 w.r.t Iosc MQ use-case sequence5
* 					Task 1 			Task 2
*					Create
*									Open
*					Post
*									Receive
*									Close
*					Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase5( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hMessageQueue_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
 	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE");
	/* create a msg q as a data channel, and read only permission */
	hMessageQueue_1 = CreateMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_WRITEONLY, state );

	/* Post to Sem1 : Task2 will OPEN MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after OPEN MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Post to MQ "write"*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : POST");
	PostMessage( hMessageQueue_1,IOSC_TEST_S_OP_WRITE, state );

	/* Post to Sem1 : Task2 will RECEIVE from MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Delete MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, state );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE NEW");
	/* create a MQ with different size to check whether
		the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						  IOSC_TEST_MQ_CHANNEL,
						 (IOSC_TEST_MQ_MAX_MSGNUM/2),
						 (IOSC_TEST_MQ_MAX_MSGLEN/2),
						 OSAL_EN_READONLY, &hMessageQueue_1);

	if(s32ErrorCode == OSAL_OK)
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : Error creating new MQ with different size");

	/* Close new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Delete new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
	/*Close Sem2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  SubTestCase6( OEDT_TESTSTATE* state )
* PARAMETER		:  state -- OEDT test state
* RETURNVALUE	:  none
* DESCRIPTION	:  Task 1 of SubTestCase6 w.r.t Iosc MQ use-case sequence6
* 					Task 1 			Task 2
*					Create
*					Post
*					Close
*									Open
*									Receive
*									Close
*					Delete
*				   Create new MQ with different size to check if old MQ shared
*				   memory is not further mapped
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
static tVoid SubTestCase6( OEDT_TESTSTATE* state )
{
	tS32 s32ErrorCode;
	OSAL_tMQueueHandle hMessageQueue_1 = 0;
	OSAL_tSemHandle hSem1 = 0;
	OSAL_tSemHandle hSem2 = 0;

	/* Open Sem1 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM1NAME,&hSem1);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	/* Open Sem2 */
	s32ErrorCode = OSAL_s32SemaphoreOpen(SEM2NAME,&hSem2);
	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE");
	/* create a msg q as a data channel, and read only permission */
	hMessageQueue_1 = CreateMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_WRITEONLY, state );

	/* Post to MQ "write"*/
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : POST");
	PostMessage( hMessageQueue_1,IOSC_TEST_S_OP_WRITE, state );

	/* Close MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Post to Sem1 : Task2 will OPEN MQ on this Post */
	OSAL_s32SemaphorePost ( hSem1 );
	/* Wait on Sem2 : waiting for Task2 to signal after CLOSE MQ */
	OSAL_s32SemaphoreWait ( hSem2, OSAL_C_TIMEOUT_FOREVER );

	/* Delete MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE");
	DeleteMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, state );

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CREATE NEW");
	/* create a MQ with different size to check whether
		the shared memory is remapped or not */
	s32ErrorCode = OSAL_s32MessageQueueCreate(
						  IOSC_TEST_MQ_CHANNEL,
						 (IOSC_TEST_MQ_MAX_MSGNUM/2),
						 (IOSC_TEST_MQ_MAX_MSGLEN/2),
						 OSAL_EN_READONLY, &hMessageQueue_1);

	if(s32ErrorCode == OSAL_OK)

		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : New MQ created with different size");
	else
		OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : Error creating new MQ with different size");

	/* Close new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : CLOSE NEW");
	CloseMessageQueue( hMessageQueue_1, state );

	/* Delete new MQ */
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK1 : DELETE NEW");
	DeleteMessageQueue( IOSC_TEST_MQ_CHANNEL, state );

	/*Close Sem1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hSem1 );
 	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
 	/*Close Sem2*/
 	s32ErrorCode = OSAL_s32SemaphoreClose( hSem2 );
 	OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

/*****************************************************************************
* FUNCTION		:  vOsalIoscMQTestTsk1(tPVoid pvArg)
* PARAMETER		:  pvArg -- void pointer to the test case number
* RETURNVALUE	:  none
* DESCRIPTION	:  This function is the Thread1 which executes the corresponding
* 				   test case based on the parameter,and reports any errors
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tVoid vOsalIoscMQTestTsk1(tPVoid pvArg)
{
	tS32 s32ErrorCode;
	tU32 uRet =0 ;
	OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
	tU8 *TestNum = (tU8 *) pvArg;

	switch(*TestNum)
	 	{
	 		case TEST1 : {
	 					  /* run the SubTestCase1 */
						  OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase1 *****");
						  SubTestCase1( &state );
						 }
							break;
	 		case TEST2 : {
						  /* run the SubTestCase2 */
	 					  OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase2 *****");
						  SubTestCase2( &state );
						 }
							break;

	 		case TEST3 : {
						  /* run the SubTestCase3 */
	 			          OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase3 *****");
						  SubTestCase3( &state );
						 }
							break;
	 		case TEST4 : {
						  /* run the SubTestCase4 */
	 			          OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase4 *****");
						  SubTestCase4( &state );
						 }
							break;
	 		case TEST5 : {
						  /* run the SubTestCase5 */
	 			          OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase5 *****");
						  SubTestCase5( &state );
						 }
							break;
	 		case TEST6 : {
						  /* run the SubTestCase6 */
						  OEDT_HelperPrintf(TR_LEVEL_FATAL, "***** Starting SubTestCase6 *****");
						  SubTestCase6( &state );
						 }
							break;

	 		default : OEDT_HelperPrintf(TR_LEVEL_FATAL, "TASK2 : BAD ARG");
	 			 			break;
	 	}

	/* delay to make sure that thread2 has finished its work */
	OSAL_s32ThreadWait(10);

	/*Close the base semaphore1*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hOsalSem1 );
	if(s32ErrorCode < OSAL_OK)
	{
		uRet += 1;
	}
	/*Close the base semaphore2*/
	s32ErrorCode = OSAL_s32SemaphoreClose( hOsalSem2 );
	if(s32ErrorCode < OSAL_OK)
	{
		uRet += 2;
	}

	/*Delete the base semaphore1*/
	s32ErrorCode = OSAL_s32SemaphoreDelete( SEM1NAME );
	if(s32ErrorCode < OSAL_OK)
	{
		uRet += 3;
	}
	/*Delete the base semaphore2*/
	s32ErrorCode = OSAL_s32SemaphoreDelete( SEM2NAME );
	if(s32ErrorCode < OSAL_OK)
	{
		uRet += 4;
	}

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "EXIT : u32Ret = %d",uRet);

	 /* print the error if any */
	 OEDT_HelperPrintf(TR_LEVEL_FATAL,
			   "TASK1 : Exiting. Error code is %d. %d of %d check points succeeded.",
			   state.error_code,
			   state.error_position - state.error_count,
			   state.error_position);
	/* pass the errors, if any*/
	if(state.error_count)
		gsRetVal = (-1);
	else
		gsRetVal = state.error_count;

	guRetFlg = TRUE;
	/* Close the current thread */
	OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION		:  vInitOsalIoscMQTasks(tU32 arg)
* PARAMETER		:  arg -- testcase to be executed
* RETURNVALUE	:  none
* DESCRIPTION	:  This function creates the semaphores required and spawns
* 				   the 2 threads for the execution of the testcase sequences
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tVoid vInitOsalIoscMQTasks(tU32 arg)
{
  tS32 er = 0;
  tU32 u32Ret   = 0;
  OSAL_tThreadID Thread1ID 	   = 0;
  OSAL_tThreadID Thread2ID 	   = 0;
  OSAL_trThreadAttribute  attr1   = { 0 };
  OSAL_trThreadAttribute  attr2   = { 0 };

  /*Create the base semaphore1*/
  er = OSAL_s32SemaphoreCreate( SEM1NAME,&hOsalSem1, 0 );
  if (er < OSAL_OK)
  {
	  u32Ret = 1;
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Error = %d",u32Ret);
  }
  /*Create the base semaphore2*/
  er = OSAL_s32SemaphoreCreate( SEM2NAME,&hOsalSem2, 0 );
  if (er < OSAL_OK)
  {
	  u32Ret += 2;
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Error = %d",u32Ret);
  }

  /* Setting attributes for Task 1 */
  attr1.szName = MQ_THR_NAME_1;
  attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
  attr1.s32StackSize = MQ_TR_STACK_SIZE;
  attr1.pfEntry = vOsalIoscMQTestTsk1;
  attr1.pvArg = (tPVoid)&arg;

  /* Setting attributes for Task 2 */
  attr2.szName = MQ_THR_NAME_2;
  attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
  attr2.s32StackSize = MQ_TR_STACK_SIZE;
  attr2.pfEntry = vOsalIoscMQTestTsk2;
  attr2.pvArg = (tPVoid)&arg;

  /* Spawn the Thread Thread_1 */
  Thread1ID = OSAL_ThreadSpawn(&attr1);

  if (Thread1ID == OSAL_ERROR)
  {
	  u32Ret += 3;
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Error = %d",u32Ret);
  }

  /* Spawn the Thread Thread_2 */
  Thread2ID = OSAL_ThreadSpawn(&attr2);
  if (Thread2ID == OSAL_ERROR)
  {
	  u32Ret += 4;
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Error = %d",u32Ret);
  }

  if(u32Ret == 0)
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Success");
  else
	  OEDT_HelperPrintf(TR_LEVEL_FATAL, "INIT : Error = %d",u32Ret);
}

/*****************************************************************************
* FUNCTION		:  vCreDelMQSequence1()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts creation and deletion of MQ in sequence1
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vCreDelMQSequence1(void)
{
	vInitOsalIoscMQTasks(TEST1);
	while(!(guRetFlg && guRetFlg2))
	{
		/* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}

/*****************************************************************************
* FUNCTION		:  vCreDelMQSequence2()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts creation and deletion of MQ in sequence2
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vCreDelMQSequence2(void)
{
	vInitOsalIoscMQTasks(TEST2);
	while(!(guRetFlg && guRetFlg2))
	{
		/* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}

/*****************************************************************************
* FUNCTION		:  vCreDelMQSequence3()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts creation and deletion of MQ in sequence3
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vCreDelMQSequence3(void)
{
	vInitOsalIoscMQTasks(TEST3);
	while(!(guRetFlg && guRetFlg2))
	{
		/* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}

/*****************************************************************************
* FUNCTION		:  vCreDelMQSequence4()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts creation and deletion of MQ in sequence4
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vCreDelMQSequence4(void)
{
	vInitOsalIoscMQTasks(TEST4);
	while(!(guRetFlg && guRetFlg2))
	{
		/* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}

/*****************************************************************************
* FUNCTION		:  vPostRecSequence1()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts posting and receiving from MQ in sequence1
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vPostRecSequence1(void)
{
	vInitOsalIoscMQTasks(TEST5);
	while(!(guRetFlg && guRetFlg2))
	{
	    /* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}

/*****************************************************************************
* FUNCTION		:  vPostRecSequence2()
* PARAMETER		:  none
* RETURNVALUE	:  SUCCESS or FAILURE
* DESCRIPTION	:  Starts posting and receiving from MQ in sequence2
* HISTORY		:  Created by Kavan R Patil (RBEI/ECF5) on 13 June, 2012
******************************************************************************/
tS32 vPostRecSequence2(void)
{
	vInitOsalIoscMQTasks(TEST6);
	while(!(guRetFlg && guRetFlg2))
	{
		/* Wait for the threads to exit*/
		continue;
	}
	guRetFlg = guRetFlg2 = 0;

	if((gsRetVal == -1) || (gsRetVal2 == -1))
		return -1;

	return 0;
}
