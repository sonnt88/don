#include "ostrace.h"

#ifndef OEDT_OSAL_CORE_MQ_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_MQ_TESTFUNCS_HEADER

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define MAX_MESG                    1
#define MAX_MESG3                    3
#define MAX_MESG10                   10
#define MAX_LEN                     20
#define MAX_EXC_LEN                 24
#define MESSAGEQUEUE_NAME 			"Message Queue MP"
#define MESSAGEQUEUE_NAME_1        "u32MQOpenDoubleCloseOnceMP"
#define MQ_MESSAGE_EXC_LEN			"-------|-------|-------|-------|"
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|-"
#define MQ_MESSAGE 					"----|----|----|----|"
#define MQ_MSG						"Message Queue Data"
#define MQ_PRIO0 					0
#define MQ_PRIO1                    1
#define MQ_PRIO2 					2
#define MQ_PRIO3                    3
#define MQ_PRIO4 					4
#define MQ_PRIO5                    5
#define MQ_PRIO6 					6
#define MQ_PRIO7 					7
#define INVAL_PRIO                  20 /*>>7*/
#define MQ_BUFF_SIZE				21
#define MQ_THR_NAME_1 				"MQ_Thread_1"
#define MQ_THR_NAME_2 				"MQ_Thread_2"
#define MQ_THR_NAME_NOTIFY_1 		"MQ_Thread_Notify1"
#define MQ_THR_NAME_NOTIFY_2 		"MQ_Thread_Notify2"
#define MQ_THR_NAME_NOTIFY_3 		"MQ_Thread_Notify3"
#define MQ_EVE_NAME1				"MQ_Event1"
#define MQ_EVE_NAME2				"MQ_Event2"
#define MQ_EVE1						1
#define MQ_EVE2						2
#define MQ_EVE3						4
#define MQ_EVE						7
#define MQ_TR_STACK_SIZE			2048
#define MQ_COUNT_MAX 				5
#define MQ_HARRAY 					20
#define MQ_MESSAGE_TO_POST          "In a message queue"
#define MESSAGE_20BYTES             "ABCDEFGHIJKLMNOPQRS"
#undef  MAX_LENGTH
#define MAX_LENGTH                  256
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                    0x7FFFFFFF
#undef  SRAND_MAX
#define SRAND_MAX                   (tU32)65535
#define MAX_MESSAGE_QUEUES          20
#define MAX_NO_MESSAGES             20
#undef  FIVE_SECONDS
#define FIVE_SECONDS                5000

#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS       10
#undef  NO_OF_WAIT_THREADS
#define NO_OF_WAIT_THREADS       15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS      1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS     2
#undef  TOT_THREADS
#define TOT_THREADS              (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)
/*rav8kor - error code for MQ wait*/
#define MQ_WAIT_RETURN_ERROR	 0
/*rav8kor - disable*/
#undef  DEBUG_MODE
#define DEBUG_MODE               0

#define P1_SPM_VOLT_EVENT     "P1_SPM_EVENT"
#define P2_SPM_VOLT_EVENT     "P2_SPM_EVENT"
/*****************************************************************
| mp tests
|----------------------------------------------------------------*/
#define  ZERO_PARAMETER 0
// perf
//#define PRINTF printf
#define PRINTF

#define THREAD_STACKSIZE 100000
#define THREAD_PRITORITY 80
#define MSGS 1000

#define MAX_MSG_LENGTH    4192
#define MAX_MSG          10
#define MSG_PRIO         2
#define START_MSG        0xFE
#define END_MSG          0xFF

tU32 u32NrOfMsg[] = { 1000/*, 10000, 50000 */};
tU32 u32SizeOfMsg[] = { 2, 4, 8/*, 16, 32, 64, 128, 256,512, 1024, 2048, 4192*/};

tU32 u32MaxMessages = MAX_MSG;
tU32 u32MaxLength = MAX_MSG_LENGTH;

tCString szName1 = "IOSC_MQ1_MP";
tCString szName2 = "IOSC_MQ2_MP";

tCString szMQSyncName = "MP_SYNC_MQ";

// content

tS32 msgSnd, msgSndSuccess, msgSndFailed=0;
tS32 msgRcv, msgRcvSuccess, msgRcvFailed=0;

#define BUFFER_SIZE          256

tString szName = "IOSC_CONTENT_MP";
//#define TRACE_MSG_CONTENT 1
tS32 FixSize = 8;


#define SH_NAME "SH_MEM_1_MP"
#define VALID_STACK_SIZE        2048
#define SH_SIZE  100
#define SH_SEM_NAME "MP_SEM_1"

#define  SHM_AREA "/shmarea"
#define  MYSHMSIZE 128


/*****************************************************************
| MsgPool tests
|----------------------------------------------------------------*/
#define MSGPOOL_MSG_SIZE 512
#define MSGPOOL_MSG_CNT 50


/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/


/*Data Types*/
typedef struct MessageQueueTableEntry_
{
	tChar coszName[MAX_LENGTH];
	tU32  u32MaxMessages;
	tU32  u32MaxLength;
	OSAL_tenAccess enAccess;
	OSAL_tMQueueHandle phMQ;
}MessageQueueTableEntry; 


tVoid vMQTestCallBack(tVoid)
{
   //printf("p1: callb %d \n", counter++);
}
void TraceOut(const char* cBuffer)
{
  char au8Buf[OSAL_C_U32_MAX_PATHLENGTH];
  tU16 u16Len;
  memset(au8Buf,' ',OSAL_C_U32_MAX_PATHLENGTH);
  au8Buf[0] = 0xf1;
  u16Len = strlen(cBuffer);
  if(u16Len > 250) u16Len = 250;
  au8Buf[u16Len+1] = '\0';
  memcpy(&au8Buf[1],cBuffer,u16Len);
  LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,u16Len+1);
}

#endif
