#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <stdio.h>                       //fix for lint cfg3_999
#include "OsalConf.h"
#include "osal_if.h"
#include "Linux_osal.h"
#include "oedt_osalcore_PR_TestFuncs.h"
#include "unistd.h"
#include "process1.h"
#include "system_kds_def.h"
#include "system_ffd_def.h"
#include "oedt_rtc_TestFuncs.h"
#include "oedt_spm_TestFuncs.h"
#include <sys/mman.h>
#include <fcntl.h>







char Buffer[BUFFER_SIZE];
#define OEDT_Wait_Time			5000
/******************************************************************************
 *FUNCTION      :SyncP1P2
 *
 *DESCRIPTION   :Synchronize process1 and process2
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32, OSAL_OK on success  or OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2011 11 23
 *
 *Process1                              Process2
 *-----------------------------------------------------------------------------
 *Create MQ
 *Wait for message from Process2
 *                                      Open MQ
 *                                      Send Message to Process1
 *                                      Close MQ
 *Close MQ
 *Delete MQ
 *****************************************************************************/
tS32 SyncP1P2()
{
    tS32 s32ReturnValue = OSAL_OK;
    OSAL_tMQueueHandle Handle = OSAL_C_INVALID_HANDLE;
    tU32 SyncMsg = 0;
    tU32 Prio;
    int  i;

    for(i=0; Handle == OSAL_C_INVALID_HANDLE && i<50; i++)
        if(OSAL_s32MessageQueueCreate(szMQSyncName, 1, sizeof(SyncMsg), OSAL_EN_READWRITE, &Handle) == OSAL_ERROR)
            OSAL_s32ThreadWait(500);

    if(Handle == OSAL_C_INVALID_HANDLE)
    {
        s32ReturnValue = OSAL_ERROR;
    }
    else
    {
        // wait for sync
        while(s32ReturnValue == OSAL_OK && SyncMsg != START_MSG)
        {
            if(OSAL_s32MessageQueueWait(Handle, (tPU8)&SyncMsg, sizeof(SyncMsg), &Prio, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
            {
                s32ReturnValue = OSAL_ERROR;
            }
        }
    }

    if(Handle != OSAL_C_INVALID_HANDLE)
    {
        if(OSAL_s32MessageQueueClose(Handle) != OSAL_OK)
        {
            s32ReturnValue = OSAL_ERROR;
        }
        else if(OSAL_s32MessageQueueDelete(szMQSyncName) != OSAL_OK)
        {
            s32ReturnValue = OSAL_ERROR;
        }
    }

    return s32ReturnValue;
}



/*****************************************************************************
 * FUNCTION:    u32KDSWriteFullAndBackToFlash()
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * TEST CASE:    TU_OEDT_KDS_038
 * DESCRIPTION:  Try to write entries, some 'n' number
 till the KDS memory is full.
 then save it into Flash
 the clear flash
 * HISTORY:      Created by andrea B�ter
 ******************************************************************************/
tU32 u32KDSWriteFullAndBackToFlash(void)
{
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0, u32Loop = 1;
   tBool bAccessOption = 0;
   tChar Write_Buffer[15] = "REWRITING_DATA";
   tsKDS_Info rKDSInfo;
   tsKDSEntry rEntryInfo;

   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE);
   if (hDevice == OSAL_ERROR)
   {
      u32Ret = 1;
   }
   else
   {
      bAccessOption = 1;
      if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE, (tS32) bAccessOption) == OSAL_ERROR)
      {
         u32Ret = 2;
      }
      else
      {
         if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CLEAR, ZERO_PARAMETER) == OSAL_ERROR)
         {
            u32Ret += 3;
         }
         if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_INIT, ZERO_PARAMETER) == OSAL_ERROR)
         {
            u32Ret += 10;
         }
         if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CHECK, (tS32) & rKDSInfo) == OSAL_ERROR)
         {
            u32Ret += 100;
         }
         /*  can be written maximum of 1000 entries to KDS */
         /* Each entry consumes 246 bytes = sizeof(tsKDSEntry) */
         do
         {
            rEntryInfo.u16EntryLength = (tU16) (strlen(Write_Buffer));
            rEntryInfo.u16Entry = (tU16) u32Loop;
            (tVoid) OSAL_szStringCopy(rEntryInfo.au8EntryData, Write_Buffer);
            if (OSAL_s32IOWrite(hDevice, (tS8 *) &rEntryInfo, (tS32) sizeof(rEntryInfo)) == OSAL_ERROR)
            {
               if ((OSAL_u32ErrorCode() != (tU32)OSAL_C_S32_IOCTRL_KDS_FULL) || (OSAL_u32ErrorCode() == (tU32)
               OSAL_E_NOERROR))
               {
                  u32Ret += 1000;
               }
               if (((tS32) OSAL_u32ErrorCode() == OSAL_C_S32_IOCTRL_KDS_FULL))
                  break;/* Exit from the loop if KDS is FULL */
            }
            u32Loop++;
         }
         while (((tS32) OSAL_u32ErrorCode() != OSAL_C_S32_IOCTRL_KDS_FULL) && (u32Ret == 0) && (u32Loop <= 1010));
      }
      if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CHECK, (tS32) & rKDSInfo) == OSAL_ERROR)
      {
         u32Ret += 2000;
      }
      if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_WRITE_BACK, ZERO_PARAMETER) == OSAL_ERROR)
      {
         u32Ret += 5000;
      }
      if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CLEAR, ZERO_PARAMETER) == OSAL_ERROR)
      {
         u32Ret += 5000;
      }
      if (OSAL_s32IOClose(hDevice) == OSAL_ERROR)
      {
         u32Ret += 10000;
      }
   }
   return u32Ret;
}

/*****************************************************************************
 * FUNCTION:    u32KDSReadEntry()
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * TEST CASE:    TU_OEDT_KDS_017
 * DESCRIPTION:  Try to read data for a particular ID.
 * HISTORY:     Created Venkateswara.N (RBIN/ECM1) JUNE 19 , 2006
 *           Modified by Haribabu Sannapaneni (RBEI/ECM1) March 11,2008
 ******************************************************************************/
tU32 u32KDSReadEntry(void)
{
#define  OEDT_KDS_M_KDS_ENTRY_FLAG_NONE                  ((tU16)0x0000)
   OSAL_tIODescriptor hDevice = 0;
   tU32 u32Ret = 0;
   tBool bAccessOption = 0;
   tU32 EntryLength = 0;
   tU32 EntryID = 0;
   tsKDSEntry rEntryInfo;
   tsKDS_Info rKDSInfo;
   tChar Software_Version[25] = "PLATFORM_XXXX";
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY);
   if (hDevice == OSAL_ERROR)
   {
      u32Ret = 1;
   }
   else
   {
      bAccessOption = 1;
      if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE, (tS32) bAccessOption) == OSAL_ERROR)
      {
         u32Ret = 2;
      }
      else
      {/* Write the Entry */
         rEntryInfo.u16EntryLength = 24;
         rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
         rEntryInfo.u16EntryFlags = OEDT_KDS_M_KDS_ENTRY_FLAG_NONE;
         (tVoid) OSAL_szStringCopy(rEntryInfo.au8EntryData, Software_Version);
         if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CHECK, (tS32) & rKDSInfo) == OSAL_ERROR)
         {
            u32Ret = 3;
         }
         if (OSAL_s32IOWrite(hDevice, (tS8 *) &rEntryInfo, (tS32) sizeof(rEntryInfo)) == OSAL_ERROR)
         {
            u32Ret += 10;
         }
         else
         {/* Read the Entry */
            rEntryInfo.u16Entry = M_KDS_ENTRY(KDS_TARGET_DIAGNOSE,KDS_TYPE_SW_VERSION);
            EntryID = rEntryInfo.u16Entry;
            rEntryInfo.u16EntryLength = 24;
            EntryLength = rEntryInfo.u16EntryLength;
            memset(rEntryInfo.au8EntryData, 0, sizeof(rEntryInfo.au8EntryData));
            if (OSAL_s32IORead(hDevice, (tS8 *) &rEntryInfo, (tS32) sizeof(rEntryInfo)) == OSAL_ERROR)
            {
               u32Ret += 100;
            }
            else
            {
               if (OSAL_s32StringNCompare(Software_Version, rEntryInfo.au8EntryData, EntryLength))
                  u32Ret += 200;

               if (EntryLength != rEntryInfo.u16EntryLength || EntryID != rEntryInfo.u16Entry)
               {
                  u32Ret += 500;
               }
               //          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Length of the entry:%d",rEntryInfo.u16EntryLength);
               //          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Read Entry :%s",rEntryInfo.au8EntryData);
               //          OEDT_HelperPrintf(TR_LEVEL_USER_1,"Flags of the Entry :%d",rEntryInfo.u16EntryFlags);
            }
         }
         if (OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_KDS_CHECK, (tS32) & rKDSInfo) != OSAL_ERROR)
         {
            //        OEDT_HelperPrintf(TR_LEVEL_USER_1,"NO .of active Entries %d",rKDSInfo.u32NumberOfActiveEntries);
         }
      }

      if (OSAL_s32IOClose(hDevice) == OSAL_ERROR)
      {
         u32Ret += 1000;
      }
   }
   return u32Ret;
}
/*****************************************************************************
 * FUNCTION:    u32FFDDevWriteReadWithSeek()
 * PARAMETER:    none
 * RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
 * TEST CASE:
 * DESCRIPTION:  test read write
 * HISTORY:     Created Andrea Bueter (TMS)  26.04.2010
 *              Updated dku6kor(RBEI/ECF5) 09.03.2015
 *              Updated dku6kor(RBEI/ECF5) 20.03.2015
 ******************************************************************************/
tU32 u32FFDDevWriteReadWithSeek(void)
{
   tU32 Vu32Ret = 0;
   tS32 Vs32Status = 0;
   tS32 Vs32CompareResult = 0;
   OSAL_tIODescriptor hDeviceFFD = OSAL_ERROR;
   OSAL_trFFDDeviceInfo VtsFFDDevInfo;
   const tS8 Vts8WriteTestData[] = { 0x55, 0x55, 0x55, 0x55, 0x55 };
   tU16 Vu16DataSize = sizeof(Vts8WriteTestData);
   tS8 Vts8ReadTestData[Vu16DataSize];
   tU32 Vu32Pos = 32;

   /*set to data set OEDT*/
   VtsFFDDevInfo.u8DataSet = (tU8)EN_FFD_DATA_SET_OEDT;            //fix for lints
   /* error ?*/
   /* open the device*/
      hDeviceFFD = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD, OSAL_EN_READWRITE);
      /* ckeck id */
      if (hDeviceFFD == OSAL_ERROR)
      {
         Vu32Ret += 1;
      }
      else
      {/*set position with seek*/
         VtsFFDDevInfo.pvArg = &Vu32Pos;
         Vs32Status = OSAL_s32IOControl(hDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK, (tS32) & VtsFFDDevInfo);
         if (Vs32Status == OSAL_ERROR)
         {
            Vu32Ret += 2;
         }
         /*Write the test data to FFD*/
         VtsFFDDevInfo.u8DataSet = (tU8)EN_FFD_DATA_SET_OEDT | 0x80;   //fix for lint cfg3_999
         VtsFFDDevInfo.pvArg = static_cast<void*>(const_cast<tS8*>(&Vts8WriteTestData[0]));  // lint fix
         Vs32Status = OSAL_s32IOWrite(hDeviceFFD, (tPCS8) &VtsFFDDevInfo, Vu16DataSize);
         /* compare size */
         if (Vs32Status != Vu16DataSize)
         {
            Vu32Ret += 4;
         }
         else
         { /*set position with seek*/
            VtsFFDDevInfo.pvArg = &Vu32Pos;
            Vs32Status = OSAL_s32IOControl(hDeviceFFD, OSAL_C_S32_IOCTRL_DEV_FFD_SEEK, (tS32) & VtsFFDDevInfo);
            if (Vs32Status == OSAL_ERROR)
            {
               Vu32Ret += 8;
            }
            /*read back the test data*/
            VtsFFDDevInfo.u8DataSet = (tU8)EN_FFD_DATA_SET_OEDT | 0x80;   //fix for lint cfg3_999
            VtsFFDDevInfo.pvArg = &Vts8ReadTestData[0];
            Vs32Status = OSAL_s32IORead(hDeviceFFD, (tPS8) &VtsFFDDevInfo, Vu16DataSize);
            if (Vs32Status != Vu16DataSize)
            {
               Vu32Ret += 10;
            }
            else
            {/*compare the data in read buffer and write data*/
               Vs32CompareResult = OSAL_s32MemoryCompare(Vts8ReadTestData, Vts8WriteTestData, Vu16DataSize);
               if (Vs32CompareResult)
               {
                  Vu32Ret += 20;
               }
            }
         }
         /*close the device*/
         if (OSAL_ERROR == OSAL_s32IOClose(hDeviceFFD))
         {
            Vu32Ret += 40;
         }
      }

   return (Vu32Ret);
}



tS32 TestMQ()
{
   tS32 counter = 0;
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tMQueueHandle mqHandle = 0;

   if (OSAL_ERROR == OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG10, MAX_LEN, OSAL_EN_READWRITE, &mqHandle))
   {
      //sprintf(&Buffer[0], "p1: ERROR %s mq create failed %d ", __func__, OSAL_u32ErrorCode());
      //TraceOut(&Buffer[0]);

      if (OSAL_ERROR == OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_READWRITE, &mqHandle) )
      {
         sprintf(&Buffer[0], "p1: ERROR %s mq open failed %d ", __func__, OSAL_u32ErrorCode());
         TraceOut(&Buffer[0]);
      }
   }

   if (mqHandle > 0)
   {
      do
      {
         if (OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandle, (tPCU8) "p1:test 1\0", 10, 2))
         {
            printf("p1: mq_post ERROR\n");
         }
         else
         {
            printf("p1: mq_post %d \n", counter);
         }
         counter++;

      }
      while (counter < 5);

      if (OSAL_ERROR != OSAL_s32MessageQueueClose(mqHandle) )
      {
         s32ReturnValue = OSAL_OK;
      }
   }

   return s32ReturnValue;
}

tS32 TestMQ_IPC_Perf()
{
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tMQueueHandle Handle1 = 0;
   OSAL_tMQueueHandle Handle2 = 0;
   tU8 InMsg[MAX_MSG_LENGTH];
   tU8 OutMsg[MAX_MSG_LENGTH];
   tU32 dwCountRead = 0, dwErrCountRead = 0;
   tU32 dwCountWrite = 0, dwErrCountWrite = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   int i = 0;
   tU32 Prio;
   tU32 u32Messages;
   tU32 u32Length;
   OSAL_tMSecond mSecStart, mSecStop;

   // prepare messages
   for (i = 0; i < MAX_MSG_LENGTH; i++)
   {
      InMsg[i] = 0;
      OutMsg[i] = i;
   }

   u32Messages = u32MaxMessages;
   u32Length = u32MaxLength;

   // prepare connection
   if (OSAL_ERROR == OSAL_s32MessageQueueCreate(szName1, u32Messages, u32Length, enAccess, &Handle1))
   {
      //sprintf(&Buffer[0], "p1: ERROR %s mq create failed %d ", __func__, OSAL_u32ErrorCode());
      //TraceOut(&Buffer[0]);
      if (OSAL_ERROR == OSAL_s32MessageQueueOpen(szName1, enAccess, &Handle1))
      {
         sprintf(&Buffer[0], "p1: ERROR %s mq open failed %d ", __func__, OSAL_u32ErrorCode());
         TraceOut(&Buffer[0]);
      }

   }

   if (OSAL_ERROR == OSAL_s32MessageQueueCreate(szName2, u32Messages, u32Length, enAccess, &Handle2))
   {
      //sprintf(&Buffer[0], "p1: ERROR %s mq create failed %d ", __func__, OSAL_u32ErrorCode());
      //TraceOut(&Buffer[0]);
      if (OSAL_ERROR == OSAL_s32MessageQueueOpen(szName2, enAccess, &Handle2))
      {
         sprintf(&Buffer[0], "p1: ERROR %s mq open failed %d ", __func__, OSAL_u32ErrorCode());
         TraceOut(&Buffer[0]);
      }
   }

   if ((Handle1 > 0) && (Handle2 > 0) &&
		   (Handle1 != OSAL_C_INVALID_HANDLE) && (Handle2 != OSAL_C_INVALID_HANDLE))
   {
      // wait for start (sync)
      while (InMsg[0] != START_MSG)
      {
         if (OSAL_s32MessageQueueWait(Handle1, (tPU8) InMsg, u32MaxLength, &Prio,
                  (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
         {
            //TraceOut("P1 _____ START MSG RCV");
         }
      }
      tU8 currSize;
      tU8 currNr;
      for (currSize = 0; currSize < sizeof(u32SizeOfMsg) / sizeof(u32SizeOfMsg[0]); currSize++)
      {
         for (currNr = 0; currNr < sizeof(u32NrOfMsg) / sizeof(u32NrOfMsg[0]); currNr++)
         {
            if (u32SizeOfMsg[currSize] > 2)
            {
               // store start time
               mSecStart = OSAL_ClockGetElapsedTime();

               // start message loop
               for (i = 0; (tU32)i < u32NrOfMsg[currNr]; i++)   //fix for lints cfg3_999
               {
                  // send message
                  if (OSAL_s32MessageQueuePost(Handle2, (tPCU8) OutMsg, u32SizeOfMsg[currSize], MSG_PRIO) == OSAL_OK)
                     if(dwCountWrite==0)   //fix for lint
                        dwCountWrite++;
                  else
                     if(dwErrCountWrite==0)    //fix for lint
                        dwErrCountWrite++;
                  // receive reply
                  if (OSAL_s32MessageQueueWait(Handle1, (tPU8) InMsg, u32MaxLength, &Prio,
                           (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) > 0)
                     if(dwCountRead==0)           //fix for lint
                        dwCountRead++;
                  else
                     if(dwErrCountRead==0)           //fix for lint
                       dwErrCountRead++;
               }
               // store end time
               mSecStop = OSAL_ClockGetElapsedTime();
               s32ReturnValue = OSAL_OK;
               sprintf(&Buffer[0], "OSAL IOSC MQ Messung %d Messages with Size %d in %d msec", u32NrOfMsg[currNr],
                        u32SizeOfMsg[currSize], mSecStop - mSecStart);
               TraceOut(&Buffer[0]);
            }
         }
      }

      // prepare shutdown
      OutMsg[0] = END_MSG;
      if (OSAL_s32MessageQueuePost(Handle2, (tPCU8) OutMsg, MAX_MSG_LENGTH, MSG_PRIO) != OSAL_OK)
         TraceOut("ERROR OSAL_s32MessageQueuePost !!!!");

      if (OSAL_s32MessageQueueClose(Handle1) != OSAL_OK)
      {
         TraceOut("ERROR OSAL_s32MessageQueueClose!!!!");
         s32ReturnValue = OSAL_ERROR;
      }

      if (OSAL_s32MessageQueueClose(Handle2) != OSAL_OK)
      {
         TraceOut("ERROR OSAL_s32MessageQueueClose!!!!");
         s32ReturnValue = OSAL_ERROR;
      }
   }
   else
   {
      TraceOut("FAILED OSAL_s32MessageQueueCreate ");
   }

   return s32ReturnValue;

}

tS32 TestMQ_IPC_Content()
{
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tMQueueHandle mqHandle = OSAL_C_INVALID_HANDLE;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   char MQ_msg[50];
   memset(Buffer, 0, 100);

   //TraceOut("----------------------- Start OSAL IOSC MQ Content Test IPC-------------------");
   if (OSAL_s32MessageQueueCreate(szName, 50, 8, enAccess, &mqHandle) == OSAL_ERROR)
   {
      //sprintf(&Buffer[0], "p1: ERROR %s mq create failed %d ", __func__, OSAL_u32ErrorCode());
      //TraceOut(&Buffer[0]);
      if (OSAL_s32MessageQueueOpen(szName, enAccess, &mqHandle) == OSAL_ERROR)
      {
         sprintf(&Buffer[0], "p1: ERROR %s mq open failed %d ", __func__, OSAL_u32ErrorCode());
         TraceOut(&Buffer[0]);
      }
   }

   if(SyncP1P2() != OSAL_OK)
   {
       OSAL_s32MessageQueueClose(mqHandle);
       mqHandle = OSAL_C_INVALID_HANDLE;
   }

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/
   if(mqHandle != OSAL_C_INVALID_HANDLE)
   {
      int Len = 3;
      int i, j;

      for (i = 0; i < MSGS; i++)
      {
         /* ------------------------------ POST ----------------------------------- */

         Len = FixSize;
         for (j = 1; j < Len; j++)
         {
            MQ_msg[j] = i;
         }
         MQ_msg[0] = Len;



         if (OSAL_s32MessageQueuePost(mqHandle, (tPCU8) &MQ_msg[0], Len, 0) == OSAL_ERROR)
         {
            sprintf(&Buffer[0], "(i=%d, th1)OSAL_s32MessageQueuePost failed ErrorCode:%d", i, (int) OSAL_u32ErrorCode());
            TraceOut(&Buffer[0]);
            msgSndFailed++;
         }
         else
         {
            msgSndSuccess++;

#ifdef TRACE_MSG_CONTENT
            sprintf(&Buffer[0],"(i=%d th1) Send Len:%d Data:%d , %d ... ,%d , %d", i,
                     MQ_msg[0],
                     MQ_msg[1],
                     MQ_msg[2],
                     MQ_msg[Len-2],
                     MQ_msg[Len-1]);
            TraceOut(&Buffer[0]);
#endif
         }
         msgSnd++;

         /* ------------------------------ POST ----------------------------------- */

      }//end of  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
      OSAL_s32MessageQueueClose(mqHandle);
      s32ReturnValue = OSAL_OK;
   }
   else
   {
      sprintf(&Buffer[0], "OSAL_s32MessageQueueOpen %s failed ErrorCode:%d", szName, (int) OSAL_u32ErrorCode());
      TraceOut(&Buffer[0]);

   }
   sprintf(&Buffer[0], "P1 : msgSnd, msgSndSuccess, msgSndFailed %d %d %d\n", msgSnd, msgSndSuccess, msgSndFailed);
   TraceOut(&Buffer[0]);

   return s32ReturnValue;
}



/******************************************************************************
 *FUNCTION      :s32MQOpenCloseTwice
 *
 *DESCRIPTION   :Create a MQ in P1, open in P2 and close in P1 twice
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32, "0" on success  or "non-zero" value in case of error
 *
 *Process1                              Process2
 *-----------------------------------------------------------------------------
 *Create MQ
 *                                      Open MQ
 *Sync with Process2 <----------------> Sync with Process1
 *Close MQ twice
 *                                      Close MQ twice
 *Delete MQ
 *****************************************************************************/
tS32 s32MQOpenCloseTwice(void)
{
   tS32 s32ReturnValue = 0;
   OSAL_tMQueueHandle mqHandle = OSAL_C_INVALID_HANDLE;

   if(OSAL_ERROR == OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG10, MAX_LEN, OSAL_EN_READWRITE, &mqHandle))
   {
       s32ReturnValue += 1;
   }

   if(mqHandle != OSAL_C_INVALID_HANDLE)
   {
       if(SyncP1P2() != OSAL_OK)
           s32ReturnValue += 2;

       if(OSAL_s32MessageQueueClose(mqHandle) != OSAL_OK)
           s32ReturnValue += 3;

       // This close has to fail!
       if(OSAL_s32MessageQueueClose(mqHandle) == OSAL_OK)
           s32ReturnValue += 4;

       if(OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME) != OSAL_OK)
           s32ReturnValue += 5;
   }

   return s32ReturnValue;
}



/******************************************************************************
 *FUNCTION     :u32SHOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *HISTORY      :07.07.2011  Dainius Ramanauskas
 *
 * PROCESS_1                    PROCESS_2
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
*****************************************************************************/
tU32 u32SHOpenDoubleCloseOnce( tVoid )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tShMemHandle handle_1 = 0;
   tPVoid map_1 = 0;
   handle_1 = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if (handle_1 != OSAL_ERROR)
   {
      OSAL_s32ThreadWait(3000);  // wait for process 2
                                 // todo create mq or event for syncronisation
      if ((map_1 = OSAL_pvSharedMemoryMap(handle_1, OSAL_EN_READWRITE, SH_SIZE, 0)) != 0)
      {
         if ((OSAL_pvMemorySet(map_1, 0, SH_SIZE) && OSAL_pvMemorySet(map_1, 1, SH_SIZE)))
         {
            if (OSAL_s32SharedMemoryUnmap(map_1, SH_SIZE) != OSAL_ERROR)
            {
               if (OSAL_s32SharedMemoryClose(handle_1) != OSAL_ERROR)
               {
                  if (OSAL_s32SharedMemoryDelete(SH_NAME) != OSAL_ERROR)
                  {
                     s32ReturnValue = OSAL_OK; // test ok
                  }
                  else
                     s32ReturnValue += 700;
               }
               else
                  s32ReturnValue += 600;
            }
            else
               s32ReturnValue += 500;
         }
         else
            s32ReturnValue += 400;
      }
      else
         s32ReturnValue += 300;
   }
   else
      s32ReturnValue += 200;

   return s32ReturnValue;
}

/******************************************************************************
 *FUNCTION     :u32MQOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *HISTORY      :07.07.2011  Dainius Ramanauskas
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
*****************************************************************************/
tU32 u32MQOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   OSAL_tMQueueHandle handle_1 = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   tU32 err=0;

   if (OSAL_ERROR == (OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME_1, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &handle_1) ))
   {
      //sprintf(&Buffer[0], "p1: ERROR %s mq create failed %d ", __func__, OSAL_u32ErrorCode());
      //TraceOut(&Buffer[0]);

      if (OSAL_ERROR == OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME_1, OSAL_EN_READWRITE, &handle_1) )
      {
         sprintf(&Buffer[0], "p1: ERROR %s mq open failed %d ", __func__, OSAL_u32ErrorCode());
         TraceOut(&Buffer[0]);
      }
   }

   if(handle_1)
   {
      OSAL_s32ThreadWait(3000);  // todo create mq or event for syncronisation
      if (OSAL_s32MessageQueuePost(handle_1, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
      {
         if (OSAL_s32MessageQueueWait(handle_1, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueClose(handle_1) != OSAL_ERROR)
            {
               if (OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME_1) != OSAL_ERROR)
               {
                  s32ReturnValue = OSAL_OK; // test ok
               }
               else
                  s32ReturnValue += 700;
            }
            else
               s32ReturnValue += 500;
         }
         else
            s32ReturnValue += 400;
      }
      else
         s32ReturnValue += 300;
      }
   else
   {
      s32ReturnValue += 100;
      err = OSAL_u32ErrorCode( );
   }

   return s32ReturnValue;
}



/******************************************************************************
 *FUNCTION      :s32MsgPoolOpenClose
 *
 *DESCRIPTION   :
 *               a) Open the MessagePool
 *               c) Close the MessagePool if open succeeded
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32, "0" on success  or "non-zero" value in case of error
 *
 *HISTORY:      :Created by FAN4HI 2011 11 23
 *****************************************************************************/
tS32 s32MsgPoolOpenClose(void)
{
    tS32    s32ReturnValue = 0;

    if(OSAL_s32MessagePoolOpen() == OSAL_ERROR)
        s32ReturnValue += 1;
    else
        if(OSAL_s32MessagePoolClose() == OSAL_ERROR)
            s32ReturnValue += 2;

    return s32ReturnValue;
}



/******************************************************************************
 *FUNCTION      :s32MsgPoolCheckSize
 *
 *DESCRIPTION   :Create a message in shared memory
 *               a) Create a message
 *               b) Get the absolute size of the message pool
 *                  after creating a message in shared memory
 *               c) Get the current size of the message pool after creating
 *                  a message in shared memory
 *               d) Delete the message
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32, "0" on success  or "non-zero" value in case of error
 *
 *HISTORY:      :Created by FAN4HI 2011 11 23
                :Updated By sja3kor 2012 08 22    
 *****************************************************************************/
tS32 s32MsgPoolCheckSize(void)
{
    tS32 s32ReturnValue = 0;
    tS32 s32AbsoluteSize = 0;
    tS32 s32SizeBeforeCreate = 0;
    tS32 s32SizeAfterCreate = 0;
    tS32 s32SizeAfterDelete = 0;
    OSAL_trMessage OSAL_msg[MSGPOOL_MSG_CNT];
    tS32 i = 0;
    tBool bsizecheck = FALSE ;
    tS32 s32Count;
    
    if(OSAL_s32MessagePoolOpen() == OSAL_ERROR)
    {
        s32ReturnValue += 1;
    }
    else
    {
        s32AbsoluteSize = OSAL_s32MessagePoolGetAbsoluteSize();
        s32SizeBeforeCreate = OSAL_s32MessagePoolGetCurrentSize();

        sprintf(Buffer, "P1 : MsgPoolCheckSize AbsoluteSize %d ", s32AbsoluteSize);
        TraceOut(Buffer);
        sprintf(Buffer, "P1 : MsgPoolCheckSize SizeBeforeCreate %d ", s32SizeBeforeCreate);
        TraceOut(Buffer);

        for(s32Count = 0; s32Count < MSGPOOL_MSG_CNT; s32Count++)
        {
        	if(OSAL_s32MessageCreate(&OSAL_msg[s32Count], MSGPOOL_MSG_SIZE, OSAL_EN_MEMORY_SHARED) == OSAL_ERROR)
        		break;
        }

        if(s32Count != MSGPOOL_MSG_CNT)
        {
        	/* On error Delete the created messages */
            for(i = 0; i < s32Count; i++)
            {
            	OSAL_s32MessageDelete(OSAL_msg[i]);
            }
            s32ReturnValue += 10;
        }
        else
        {
            s32SizeAfterCreate = OSAL_s32MessagePoolGetCurrentSize();
            sprintf(Buffer, "P1 : MsgPoolCheckSize SizeAfterCreate %d ", s32SizeAfterCreate);
            TraceOut(Buffer);

            for(s32Count = 0; s32Count < MSGPOOL_MSG_CNT; s32Count++)
            {
            	if(OSAL_s32MessageDelete(OSAL_msg[s32Count]) == OSAL_ERROR)
            		break;
            }

            if(s32Count != MSGPOOL_MSG_CNT)
            {
                s32ReturnValue += 100;
            }
            else
            {
              /*This test case is failing because the size of msg pool after creating msg and after deleting the same msg is not coming same.
             so to fix this,here is the retry for three times with wait for 5ms to check the size of msg pool.sja3kor*/
             for(i=0;(i<3)&&(bsizecheck ==FALSE);i++)
               {
                  s32SizeAfterDelete = OSAL_s32MessagePoolGetCurrentSize();
                  sprintf(Buffer, "P1 : MsgPoolCheckSize SizeAfterDelete %d ", s32SizeAfterDelete);
                  TraceOut(Buffer);

                  usleep(OEDT_Wait_Time); //wait for 5ms

                  if(s32SizeAfterDelete != s32SizeBeforeCreate)
                  {
                     bsizecheck = FALSE;
                  }
                  else
                  {
                     bsizecheck = TRUE;
                      
                  }                      
               } 
               if((i>=3)&&(bsizecheck ==FALSE)) 
               {
                  s32ReturnValue += 1000;
               }  
            }
        }

        if(OSAL_s32MessagePoolClose() == OSAL_ERROR)
        {
            s32ReturnValue += 2;
        }
    }

   return s32ReturnValue;
}
/************************************************************************
*FUNCTION:    tu32Gen2RTCSetGpsTime
*DESCRIPTION: Sets the GPS Time 
            
*PARAMETER:   Nil
*             
*             
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     20.05.10, Tinoy Mathews( RBEI/ECF1 )
*
*Initial Revision.
************************************************************************/
tU32 tu32Gen2RTCSetGpsTime( void )
{
   OSAL_tIODescriptor hDevice  = 0;
   tU32 u32RetVal              = 0;

   
   hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
   if ( hDevice == OSAL_ERROR)
   {
      return(RTC_DEVICE_OPEN_ERROR);
   }

   if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps) == OSAL_ERROR )
   {
      OSAL_s32IOClose ( hDevice );
      return( RTC_DEVICE_SET_GPS_TIME_ERROR );
   }

   if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
   {
      return( RTC_DEVICE_CLOSE_ERROR );
   }
   
   return ( u32RetVal );
}

/************************************************************************
*FUNCTION:    u32Test_SPMVOLT
*DESCRIPTION: tests all the OEDT's

*PARAMETER:   Nil
*
*
*
*RETURNVALUE: tU32(0-success,Error code-failure)
*
*HISTORY:     17.04.13, Suresh Dhandapani( RBEI/ECF1 )
              26.06.15, Deepak Kumar( RBEI/ECF ) Fix for lints
*
*Initial Revision.
************************************************************************/
tU32 u32Test_SPMVOLT( void )
{
   OSAL_tIODescriptor hIODescVoltDriver;
   tU32 u32RetVal              = 0;
   tS32 s32Result = 0;
   tU32 s32CurScale;
   tU32 s32readCurScale = 0;
   tU32 s32readedScale;
   tS32 s32VoltageNoficiationClientId = 0;
   OSAL_tVoltSystemThresholdHistory rSysLevelHistory;
   OSAL_tVoltSystemThresholdNotification rVoltSystemThresholdNotification;
   OSAL_tVoltUserThresholdNotification rUserNotification;
   OSAL_tVoltRemoveThresholdNotification rRemnotification;
   OSAL_tEventHandle  hEventHistory       = OSAL_C_INVALID_HANDLE;
   OSAL_tVoltUserThresholdHistory rUserLevelHistory;
   hIODescVoltDriver = OSAL_IOOpen( OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE );
   if ( hIODescVoltDriver != OSAL_ERROR)
   {
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT,
                                                (tS32)&s32VoltageNoficiationClientId))
      {
         u32RetVal += 1;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE,
                                                (tS32)&s32Result))
      {
         u32RetVal += 2;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_MV,
                                          (tS32)&s32Result))
      {
         u32RetVal += 3;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_RAW,
                                          (tS32)&s32Result))
      {
         u32RetVal += 10;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                               OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_RANGE,
                                               (tS32)&s32Result))
      {
         u32RetVal += 20;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_SCALE,
                                                (tS32)&s32readCurScale))
      {
         u32RetVal += 30;
      }
      s32readedScale = s32readCurScale;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF,
                                          (tS32)&s32CurScale))
      {
         u32RetVal += 40;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_SCALE,
                                          (tS32)&s32readCurScale))
      {
         u32RetVal += 50;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF,
                                          (tS32)&s32readedScale))
      {
         u32RetVal += 100;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL,
                                                (tS32)&s32Result))
      {
         u32RetVal += 200;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_CVM_VOLTAGE_LEVEL,
                                          (tS32)&s32Result))
      {
         u32RetVal += 300;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_CURRENT,
                                          (tS32)&s32Result))
      {
         u32RetVal += 400;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                                OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_SCALE,
                                                (tS32)&s32Result))
      {
         u32RetVal += 500;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_CURRENT_SCALE,
                                          (tS32)&s32Result))
      {
         u32RetVal += 600;
      }
      if(OSAL_OK != OSAL_s32EventCreate((tCString)P1_SPM_VOLT_EVENT, &hEventHistory))
      {
         u32RetVal += 700;
      }
      rVoltSystemThresholdNotification.hdl      = hEventHistory;
      rVoltSystemThresholdNotification.idClient = s32VoltageNoficiationClientId;
      rVoltSystemThresholdNotification.kind     = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;
      rVoltSystemThresholdNotification.mask     = 0x00000001;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_SYSTEM_THRESHOLD_NOTIFICATION,
                                          (tS32)&rVoltSystemThresholdNotification))
      {
         u32RetVal += 800;
      }
      rSysLevelHistory.idClient = s32VoltageNoficiationClientId;                        /* client id from create client              */
      rSysLevelHistory.kind = OSAL_VOLT_KIND_SYSTEM_THRESHOLDS;                            /* pure system or cvm thresholds             */
      rSysLevelHistory.idx = 0;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL_HISTORY,
                                          (tS32)&rSysLevelHistory))
      {
         u32RetVal += 900;
      }
      rRemnotification.idClient = s32VoltageNoficiationClientId;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_DEREG_SYSTEM_THRESHOLD_NOTIFICATION,
                                          (tS32)&rRemnotification))
      {
         u32RetVal += 1000;
      }
      rUserNotification.hdl = hEventHistory;
      rUserNotification.idClient = s32VoltageNoficiationClientId;
      rUserNotification.idGroup = 7;
      rUserNotification.mask = 0x00000001;
      rUserNotification.idThreshold = 1;
      rUserNotification.thresholdVoltage = 6000;
      rUserNotification.thresholdHysteresis = 500;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_SET_USER_THRESHOLD_NOTIFICATION,
                                          (tS32)&rUserNotification))
      {
         u32RetVal += 2000;
      }
      rUserLevelHistory.idClient = s32VoltageNoficiationClientId;
      rUserLevelHistory.idThreshold = 1;
      rUserLevelHistory.idGroup = 7;
      rUserLevelHistory.idx = 0;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_LEVEL_HISTORY,
                                          (tS32)&rUserLevelHistory))
      {
         u32RetVal += 3000;
      }
      rRemnotification.idClient = s32VoltageNoficiationClientId;
      rRemnotification.idThreshold = 1;
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_DEREG_USER_THRESHOLD_NOTIFICATION,
                                          (tS32)&rRemnotification))
      {
         u32RetVal += 4000;
      }
      if(OSAL_OK != OSAL_s32IOControl(hIODescVoltDriver,
                                          OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT,
                                          (tS32)&s32VoltageNoficiationClientId))
      {
         u32RetVal += 5000;
      }
      if(OSAL_OK != OSAL_s32EventClose(hEventHistory))
      {
         u32RetVal += 6000;
      }
      if(OSAL_OK != OSAL_s32EventDelete((tCString)P1_SPM_VOLT_EVENT))
      {
         u32RetVal += 7000;
      }
      if(OSAL_OK != OSAL_s32IOClose ( hIODescVoltDriver ))
      {
         u32RetVal += 8000;
      }
   }
   return ( u32RetVal );
}
tS32 StartTests()
{
   tS32 s32ReturnValue = 0;
#if 0
   TraceOut("-------------------------------------------");
   TraceOut("         START SIMPLE IPC MQ TEST          ");
   TraceOut("-------------------------------------------");
   if (TestMQ() == OSAL_OK)
   {
      //TraceOut("Sender FINISHED");
   }
   else
   {
      TraceOut("### Sender ERROR");
      s32ReturnValue += 1;
   }
   OSAL_s32ThreadWait(100);

   TraceOut("-------------------------------------------");
   TraceOut("         START PERF IPC MQ TEST            ");
   TraceOut("-------------------------------------------");
   if (TestMQ_IPC_Perf() == OSAL_OK)
   {
      //TraceOut("PING FINISHED");
   }
   else
   {
      TraceOut("### PING ERROR");
      s32ReturnValue += 2;
   }
   OSAL_s32ThreadWait(100);

   TraceOut("-------------------------------------------");
   TraceOut("         START CONTENT IPC MQ TEST         ");
   TraceOut("-------------------------------------------");
   if (TestMQ_IPC_Content() == OSAL_OK)
   {
      //TraceOut("Sender FINISHED");
   }
   else
   {
      TraceOut("Sender ERROR");
      s32ReturnValue += 3;
   }
#endif

#if 0
   OSAL_s32ThreadWait(100);
   TraceOut("-------------------------------------------");
   TraceOut("         FFD TEST (P1)                     ");
   TraceOut("-------------------------------------------");
   if (u32FFDDevWriteReadWithSeek() == OSAL_OK)
   {
      //TraceOut("FFD TEST OK (P1)");
   }
   else
   {
      TraceOut("### FFD TEST ERROR (P1)");
      s32ReturnValue += 4;
   }
   OSAL_s32ThreadWait(100);

#endif

#if 1
   TraceOut("-------------------------------------------");
   TraceOut("-------  KDS TEST (P1)  -------------------");
   TraceOut("-------------------------------------------");
   if ((u32KDSWriteFullAndBackToFlash() + u32KDSReadEntry()) == OSAL_OK)
   {
      TraceOut("KDS TEST OK (P1)");
   }
   else
   {
      TraceOut("FFD TEST ERROR (P1)");
      s32ReturnValue += 5;
   }
#endif

#if 1
   TraceOut("-------------------------------------------");
   TraceOut("         SH MEMORY TEST (P1)               ");
   TraceOut("-------------------------------------------");
   if (u32SHOpenDoubleCloseOnce() == OSAL_OK)
   {
   }
   else
   {
      TraceOut("### SH MEMORY TEST ERROR (P1)");
      s32ReturnValue += 5;
   }
   OSAL_s32ThreadWait(100);
#endif
#if 1

   TraceOut("-------------------------------------------");
   TraceOut("         MQ TEST (P1)                      ");
   TraceOut("-------------------------------------------");
   if (u32MQOpenDoubleCloseOnce() == OSAL_OK)
   {
   }
   else
   {
      TraceOut("### MQ TEST ERROR (P1)");
      s32ReturnValue += 6;
   }
   OSAL_s32ThreadWait(100);
#endif

   if (OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME_1) != OSAL_OK)
      TraceOut("ERROR OSAL_s32MessageQueueDelete Message Queue !!!!");

   return s32ReturnValue;
}



tS32 RunTest(tU32 TestID)
{
    tS32    s32ReturnValue = 0;
    // int     i;        //fix for lint cfg3_999

    switch(TestID)
    {
    case MP_TEST_SPMVOLT:
#ifdef LSIM
        s32ReturnValue = (tS32)u32Test_SPMVOLT();
        sprintf(Buffer, "P1: MP_TEST_SPMVOLT returned %i", s32ReturnValue);
#endif
        break;
    case MP_TEST_TESTRTC: /*Multi Process Test Case for RTC*/
/*This conditional compilation has to be removed when RTC driver is added to linux side*/
#ifdef LSIM
        s32ReturnValue = tu32Gen2RTCSetGpsTime();
        sprintf(Buffer, "P1: MP_TEST_TESTRTC returned %i", s32ReturnValue);
#endif
        break;
        
    case MP_TEST_TESTMQ:
        s32ReturnValue = TestMQ();
        sprintf(Buffer, "P1: MP_TEST_TESTMQ returned %i", s32ReturnValue);
        break;
    case MP_TEST_TESTMQ_IPC_PERF:
        s32ReturnValue = TestMQ_IPC_Perf();
        sprintf(Buffer, "P1: MP_TEST_TESTMQ_IPC_PERF returned %i", s32ReturnValue);
        break;
    case MP_TEST_TESTMQ_IPC_CONTENT:
        s32ReturnValue = TestMQ_IPC_Content();
        sprintf(Buffer, "P1: MP_TEST_TESTMQ_IPC_CONTENT returned %i", s32ReturnValue);
        break;
    case MP_TEST_TESTMQ_OPEN_CLOSE_TWICE:
        s32ReturnValue = s32MQOpenCloseTwice();
        sprintf(Buffer, "P1: MP_TEST_TESTMQ_OPEN_CLOSE_TWICE returned %i", s32ReturnValue);
        break;

    case MP_TEST_FFD_WRS:
        s32ReturnValue = u32FFDDevWriteReadWithSeek();
        sprintf(Buffer, "P1: MP_TEST_FFD_WRS returned %i", s32ReturnValue);
        break;

    case MP_TEST_KDS_WRITE_FULL:
        s32ReturnValue = u32KDSWriteFullAndBackToFlash();
        sprintf(Buffer, "P1: MP_TEST_KDS_WRITE_FULL returned %i", s32ReturnValue);
        break;
    case MP_TEST_KDS_READ_ENTRY:
        s32ReturnValue = u32KDSReadEntry();
        sprintf(Buffer, "P1: MP_TEST_KDS_READ_ENTRY returned %i", s32ReturnValue);
        break;

    case MP_TEST_MSGPOOL_OPEN:
        s32ReturnValue = s32MsgPoolOpenClose();
        sprintf(Buffer, "P1: MP_TEST_MSGPOOL_OPEN returned %i", s32ReturnValue);
        break;
    case MP_TEST_MSGPOOL_CHECK_SIZE:
        s32ReturnValue = s32MsgPoolCheckSize();
        sprintf(Buffer, "P1: MP_TEST_MSGPOOL_CHECK_SIZE returned %i", s32ReturnValue);
        break;

    default:
        s32ReturnValue = -1;
        sprintf(Buffer, "P1: Unknown request");
        break;
    }
    TraceOut(Buffer);

    return s32ReturnValue;
}



int main()                                                       // fix for lint
{
    tS32 s32ReturnValue                 = 0;
    OSAL_tMQueueHandle mqHandleCntrl    = OSAL_C_INVALID_HANDLE;
    OSAL_tMQueueHandle mqHandleRspns    = OSAL_C_INVALID_HANDLE;
    tU32    inmsg, outmsg;

    (void)PRINTF("process 1\n");   //fix for lints cfg3_999

    // Open MQ to receive controlmessages
    for(tU32 i = 0; mqHandleCntrl == OSAL_C_INVALID_HANDLE && i < 200; i++)
        if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_P1_CONTROL_NAME, OSAL_EN_READONLY, &mqHandleCntrl) == OSAL_ERROR)
            OSAL_s32ThreadWait(100);

    if(mqHandleCntrl == OSAL_C_INVALID_HANDLE)
        s32ReturnValue += 1;

    // Open MQ to send testresults
    if(s32ReturnValue == 0)
        for(tU32 i = 0; mqHandleRspns == OSAL_C_INVALID_HANDLE && i < 200; i++)
            if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_RESPONSE_NAME, OSAL_EN_WRITEONLY, &mqHandleRspns) == OSAL_ERROR)
                OSAL_s32ThreadWait(100);

    if(mqHandleRspns == OSAL_C_INVALID_HANDLE)
        s32ReturnValue += 1;


    inmsg = 0;
    if(s32ReturnValue == 0)
        // Receive and process messages from OEDT until MP_TEST_END was sent
        while(inmsg != (tU32)MP_TEST_END)        //fix for lint cfg3_999
        {
            if(OSAL_s32MessageQueueWait(mqHandleCntrl, (tPU8)&inmsg, sizeof(inmsg), OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
                s32ReturnValue += 10;

            // Run requested test...
            if(s32ReturnValue == 0 && inmsg != (tU32)MP_TEST_END)    //fix for lint cfg3_999
                s32ReturnValue = RunTest(inmsg);

            // Send result to OEDT
            outmsg = (1 << 16) | s32ReturnValue;
            if(OSAL_s32MessageQueuePost(mqHandleRspns, (tPCU8)&outmsg, sizeof(outmsg), 2) != OSAL_OK)
                s32ReturnValue += 100;
        }


    // Close MQs
    if(mqHandleCntrl != OSAL_C_INVALID_HANDLE)
        OSAL_s32MessageQueueClose(mqHandleCntrl);

    if(mqHandleRspns != OSAL_C_INVALID_HANDLE)
        OSAL_s32MessageQueueClose(mqHandleRspns);

    TraceOut("-------------------------------------------");
    TraceOut("               P1 returns now!             ");
    TraceOut("-------------------------------------------");
    _exit(s32ReturnValue);
}
