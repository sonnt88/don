/*****************************************************************************
* FILE         : process1.c
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 1 for multi-process testing of DRV ADC
*              
* AUTHOR(s)    : Basavaraj Nagar (RBEI/ECG4)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 04.04.2013    | version 0.1               | Basavaraj Nagar (RBEI/ECG4)
*               |                           | Initial version for Linux
*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Adc_Test_Funcs.h"

#define Gen3_ADC_CHANNEL_41 OSAL_C_STRING_DEVICE_ADC_TEST_1

static tU32 OpenCloseADCChannel(tCString chnName)
{
    OSAL_tIODescriptor hFd = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tU32 u32Ret = 0;
    //printf("chnname is  %s\n",chnName);
    /* Channel Open */
    hFd = OSAL_IOOpen(chnName,enAccess);

    if(OSAL_ERROR != hFd)
    {
        /* Channel Close */
        if(OSAL_ERROR  == OSAL_s32IOClose ( hFd ) )
        {
            u32Ret = 1;
        }
    }
    else
    {
        u32Ret = 2;
    }
    return u32Ret;

}
int main(int argc, char **argv)
{
   tS32 s32ReturnValue = 0;
   tU32 i = 0;

   
   tCString chn1Name=Gen3_ADC_CHANNEL_41;
   OSAL_tMQueueHandle mqHandleRspns = OSAL_C_INVALID_HANDLE;
   tU32 outmsg;

   if (argc == 2)
   {
      chn1Name = (argv[1]);
   }
   
   // Open MQ to send testresults
   for (i = 0; mqHandleRspns == OSAL_C_INVALID_HANDLE && i < 200; i++)
   {
     if (OSAL_s32MessageQueueOpen(MQ_ADC_MP_RESPONSE_NAME,
           OSAL_EN_WRITEONLY, &mqHandleRspns) == OSAL_ERROR)
     {
        OSAL_s32ThreadWait(100);
     }
   }

   if (mqHandleRspns == OSAL_C_INVALID_HANDLE)
       s32ReturnValue += 100;

   if (s32ReturnValue == 0)
   {
     // run test
     s32ReturnValue = (tS32)OpenCloseADCChannel(chn1Name);

     // send result to OEDT
     outmsg = (1 << 16) | s32ReturnValue;
     if (OSAL_s32MessageQueuePost(mqHandleRspns, (tPCU8)&outmsg,
           sizeof(outmsg), 2) != OSAL_OK)
     {
         s32ReturnValue += 200;
     }
   }

   // Close MQs
   if (mqHandleRspns != OSAL_C_INVALID_HANDLE)
   {
       OSAL_s32MessageQueueClose(mqHandleRspns);
   }

   _exit(s32ReturnValue);
}

/************************ End of file ********************************/




