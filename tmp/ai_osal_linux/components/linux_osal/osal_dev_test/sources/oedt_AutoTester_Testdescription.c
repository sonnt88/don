#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "oedt_Types.h"
#include "oedt_testing_macros.h"
#include "oedt_AutoTester_Testdescription.h"
#include "oedt_ComTest.h"
#include "oedt_spm_TestFuncs.h"
#include "oedt_Cryptdev_Signature_TestFuncs.h"
#include "oedt_KDS_TestFuncs.h"
#include "oedt_osalcore_FPE_TestFuncs.h"
#include "oedt_prm_testfuncs.h"
#include "oedt_FileSystem_Benchmark_TestFuncs.h"
#include "oedt_SWC_TestFuncs.h"
#include "oedt_Acousticin_TestFuncs.h"
#include "oedt_Acousticout_TestFuncs.h"
#include "oedt_Cd_TestFuncs.h"
#include "oedt_FFS3_CommonFS_TestFuncs.h"
#include "oedt_Gyro_TestFuncs.h"
#include "oedt_GNSS_TestFuncs.h"
#include "oedt_Abs_TestFuncs.h"
#if defined(GEN3ARM)
#include "oedt_ACC_TestFuncs.h"
#include "oedt_chenc_TestFuncs.h"
#include "oedt_PDD_TestFuncs.h"
#include "oedt_DataPool_TestFuncs.h"
#endif
#include "oedt_ADR3Ctrl_TestFuncs.h"
#if 0   // COMMENT OUT all Tests to make it compilable  OSR
#include "oedt_FD_CSM_TestFuncs.h"  
#include "oedt_FD_CSM_Sample.h"
#include "oedt_FD_CSM_Sample_Uudt.h"
#include "oedt_FD_CSM_UpperTester.h"
#endif    // COMMENT OUT all Tests to make it compilable  OSR
#include "oedt_IncCommNdPerformance_TestFuncs.h"
#include "oedt_Audioroute_TestFuncs.h"
#include "oedt_Adc_Test_Funcs.h"

//#define GPS_ANTENNA_AVAILABLE

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!--------------------------- Common FFS3--------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The following test cases have been moved to regression data set by cma5cob */
#if 0
OEDT_FUNC_CFG oedt_pFFS3_CommonFS_Entries[] =
{
  {OEDT_TEST_SIMPLE(FFS3_CommonFSStressTest), OEDT_HW_EXC_NO, 100, "StressTest"},  /* 0  */
  {OEDT_EMPTY_TEST                          , OEDT_HW_EXC_ALL,  5, ""}
};
#endif

#ifdef LSIM
/* -== SPM VOLT ==- */	
OEDT_FUNC_CFG oedt_pSPM_VOLT_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StartOverVoltageListener), OEDT_HW_EXC_NO, 10, "Start Over voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StopOverVoltageListener), OEDT_HW_EXC_NO, 10, "Stop Over voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StartCVMLowVoltageListener), OEDT_HW_EXC_NO, 10, "Start CVM Low voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StopCVMLowVoltageListener), OEDT_HW_EXC_NO, 10, "Stop CVM Low voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StartUserVoltageListener), OEDT_HW_EXC_NO, 10, "Start User voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION( u32SPM_StopUserVoltageListener), OEDT_HW_EXC_NO, 10, "Stop User voltage Listener" },
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
				/* !!!--------------------------FD_CSM-------------------------!!! */
				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#if 0   // COMMENT OUT all Tests to make it compilable  OSR

OEDT_FUNC_CFG oedt_pFD_CSM_Entries[] = 
{

{OEDT_SIMPLE_TEST_FUNCTION(u32CsmTest1), OEDT_HW_EXC_ALL, 50, "Test 1"},                                /*  0   */ 
{OEDT_SIMPLE_TEST_FUNCTION(u32CsmTest2), OEDT_HW_EXC_ALL, 50, "Test 2"},                                                              /*  1   */ 
{OEDT_SIMPLE_TEST_FUNCTION(u32CsmTest3), OEDT_HW_EXC_ALL, 50, "Test 3"},                                                               /*  2   */ 
{OEDT_SIMPLE_TEST_FUNCTION(u32CsmTest4), OEDT_HW_EXC_ALL, 50, "Test 4"},                                                               /*  3   */ 
{OEDT_SIMPLE_TEST_FUNCTION(u32CsmStartUpperTester), OEDT_HW_EXC_NO, 500, "Start Uppertester"},                                               /*  4   */ 
{OEDT_SIMPLE_TEST_FUNCTION(u32CsmStopUpperTester), OEDT_HW_EXC_NO, 500, "Stop Uppertester"},                                                 /*  5   */ 
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!--------------Communication Performanctests--------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pComPerfTest_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32ComPerfTest_OSALMailboxPerf), OEDT_HW_EXC_NO, 1800, "OSAL Mailbox Performance Test"},
//{OEDT_SIMPLE_TEST_FUNCTION(u32ComPerfTest_MoccaPerf), OEDT_HW_EXC_NO, 1800, "Mocca Performance Test"},
//{OEDT_SIMPLE_TEST_FUNCTION(u32ComPerfTest_CCAPerf), OEDT_HW_EXC_NO, 1800, "CCA Performance Test"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};



                /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
                /* !!!-------------------------GPS-----------------------------!!! */
                /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pGPS_Entries[] =
{
#ifdef GPS_ANTENNA_AVAILABLE
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDurationTimeFix), OEDT_HW_EXC_NO,    180, "Duration for Time fix"},           /*   3 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDuration2DFix_ColdStart), OEDT_HW_EXC_NO,    180, "Duration 4 2DFix in Cold start"},  /*   4 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDuration3DFix_ColdStart), OEDT_HW_EXC_NO,    360, "Duration 4 3DFix in Cold start"},  /*   5 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsGetPersistentDataForWamBoot), OEDT_HW_EXC_NO,    360, "Get pers. data -2D warm boot"},    /*   6 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDuration2DFix_WarmStart), OEDT_HW_EXC_NO,    180, "Duration 4 2D Fix in Warm start"}, /*   7 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsGetPersistentDataForWamBoot), OEDT_HW_EXC_NO,    180, "Get pers. data -3D warm boot"},    /*   8 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDuration3DFix_WarmStart), OEDT_HW_EXC_NO,    360, "Duration 4 3D Fix in Warm start"}, /*   9 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsProductionTest), OEDT_HW_EXC_NO,    120, "Production Test"},                 /*  10 */
{OEDT_SIMPLE_TEST_FUNCTION(u32TestCriticalVoltageColdStart), OEDT_HW_EXC_NO,    620, "CVM cold start test"},             /*  11 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsGetPersistentDataForWamBoot), OEDT_HW_EXC_NO,    305, "Get pers. data -3D warm boot"},    /*  12 */
{OEDT_SIMPLE_TEST_FUNCTION(u32TestCriticalVoltageWarmStart), OEDT_HW_EXC_NO,    620, "CVM warm start test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Check2DFixAfterSetHints ), OEDT_HW_EXC_NO,    620, "Check 2DFix After SetHints"},
    /*Regression test for 8 hrs*/
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsRegressionTest), OEDT_HW_EXC_NO,  28805, "Regression Test"},
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL,  5,     ""}
};

#endif    // COMMENT OUT all Tests to make it compilable  OSR


#ifdef LSIM
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- SWC----------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_SWC_Entries[] =
{
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY1_PRESS_CALLBACK_TEST"},         /*   1 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY2Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY2_PRESS_CALLBACK_TEST"},         /*   2 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3_PRESS_CALLBACK_TEST"},         /*   3 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4_PRESS_CALLBACK_TEST"},         /*   4 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY5Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY5_PRESS_CALLBACK_TEST"},         /*   5 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY6Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY6_PRESS_CALLBACK_TEST"},         /*   6 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY7Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY7_PRESS_CALLBACK_TEST"},         /*   7 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY2KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY2KEY1_PRESS_TEST"},         /*   8 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3KEY1_PRESS_TEST"},         /*   9 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY1_PRESS_TEST"},         /*   10 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3KEY2Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3KEY2_PRESS_TEST"},         /*   11 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY2Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY2_PRESS_TEST"},         /*   12 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY3Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY3_PRESS_TEST"},         /*   13 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3KEY2KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3KEY2KEY1_PRESS_TEST"},         /*   14 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY3KEY2Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY3KEY2_PRESS_TEST"},         /*   15 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY3KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY3KEY1_PRESS_TEST"},         /*   16 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY2KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY2KEY1_PRESS_TEST"},         /*   17 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4KEY3KEY2KEY1Press_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4KEY3KEY2KEY1_PRESS_TEST"},         /*   18 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY1Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY1_RELEASE_TEST"},         /*   19 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY1PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY1_PRESS_RELEASE_TEST"},         /*   20 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY2Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY2_RELEASE_TEST"},         /*   21 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY2PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY2_PRESS_RELEASE_TEST"},         /*   22 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3_RELEASE_TEST"},         /*   23 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY3PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3_PRESS_RELEASE_TEST"},         /*   24 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4_RELEASE_TEST"},         /*   25 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY4PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4_PRESS_RELEASE_TEST"},         /*   26 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY5Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY5_RELEASE_TEST"},         /*   27 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY5PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY5_PRESS_RELEASE_TEST"},         /*   28 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY6Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY6_RELEASE_TEST"},         /*   29 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY6PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY6_PRESS_RELEASE_TEST"},         /*   30 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY7Release_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY7_RELEASE_TEST"},         /*   31 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelKEY7PressRelease_Test) ,OEDT_HW_EXC_NO,      50, "SWC:KEY7_PRESS_RELEASE_TEST"},         /*   32 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey1) ,OEDT_HW_EXC_NO,      50, "SWC:KEY1_PRESS_IOCONTROL_TEST"},         /*   33 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey2) ,OEDT_HW_EXC_NO,      50, "SWC:KEY2_PRESS_IOCONTROL_TEST"},         /*   34 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey3) ,OEDT_HW_EXC_NO,      50, "SWC:KEY3_PRESS_IOCONTROL_TEST"},         /*   35 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey4) ,OEDT_HW_EXC_NO,      50, "SWC:KEY4_PRESS_IOCONTROL_TEST"},         /*   36 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey5) ,OEDT_HW_EXC_NO,      50, "SWC:KEY5_PRESS_IOCONTROL_TEST"},         /*   37 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey6) ,OEDT_HW_EXC_NO,      50, "SWC:KEY6_PRESS_IOCONTROL_TEST"},         /*   38 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetKey7) ,OEDT_HW_EXC_NO,      50, "SWC:KEY7_PRESS_IOCONTROL_TEST"},         /*   39 */
   {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL,  5,     ""}
};
#endif


/* -== CRYPTCARD ==- */
OEDT_FUNC_CFG oedt_pCryptdev_Entries[] =
{
#ifdef VARIANT_S_FTR_ENABLE_GEN3_CRYPT_SUPPORT
/*/dev/cryptcard*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_IOCTRL_TriggerSignVerify), OEDT_HW_EXC_NO, 10, "Cryptcard:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_NO, 10, "cryptcard:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_IOCTRL_SignFileType), OEDT_HW_EXC_NO, 10, "cryptcard:Get SignFile type" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_InvalidCertPath_TriggerSignVerify), OEDT_HW_EXC_NO, 10, "cryptcard:Invalid Path signveri" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_InvalidCertPath_GetSignFileType), OEDT_HW_EXC_NO, 10, "cryptcard:Invalid Path signType " },
   /* Signature verify tests for /dev/media/UUID(SD CARD) device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32SDCard_IOCTRL_TriggerSignVerify), OEDT_HW_EXC_NO,70, "SDCard:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32SDCard_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_NO, 70, "SDCard:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32SDCard_IOCTRL_SignFileType), OEDT_HW_EXC_NO, 70, "SDCard:Get SignFile type" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32SDCard_InvalidCertPath_TriggerSignVerify), OEDT_HW_EXC_NO, 70, "SDCard:Invalid Path signveri" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32SDCard_InvalidCertPath_GetSignFileType), OEDT_HW_EXC_NO, 70, "SDCard:Invalid Path signType " },
   /* Signature verify tests for /dev/media/UUID(USB) device */
	{OEDT_SIMPLE_TEST_FUNCTION( u32USB_IOCTRL_TriggerSignVerify), OEDT_HW_EXC_NO, 70, "USB:Trigger SignVerify" },		 
	{OEDT_SIMPLE_TEST_FUNCTION( u32USB_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_NO, 70, "USB:Get SignVerify Status" },
	{OEDT_SIMPLE_TEST_FUNCTION( u32USB_IOCTRL_SignFileType), OEDT_HW_EXC_NO, 70, " USB:Get SignFile type" },
   /* Multi thread sign verify */	
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_Signverify), OEDT_HW_EXC_NO, 70, "Multi Thread sign verify"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_SignverifyStatus), OEDT_HW_EXC_NO, 70, "Multi Thread sign verify status"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_SignType), OEDT_HW_EXC_NO, 70, "Multi Thread Get sign file type"},   
   /* Multi function with same device*/
	{OEDT_SIMPLE_TEST_FUNCTION( u32SDcard_Multifunction), OEDT_HW_EXC_NO, 70, "SDcard:Multifunction"},
   /* Multi device with same functionality*/
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_Signverify), OEDT_HW_EXC_NO, 70, "Multi dev sign verify"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_SignverifyStatus), OEDT_HW_EXC_NO, 70, "Multi dev sign veri status "},
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_SignfileType), OEDT_HW_EXC_NO, 70, "Multi dev sign file type"},
   /* Multi device with Multi functionality*/
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_Multifunction), OEDT_HW_EXC_NO, 70, "Multi dev multi fun"},	
   
  { OEDT_SIMPLE_TEST_FUNCTION(u32WriteVINValuetoKDS), OEDT_HW_EXC_NO, 10, "write Valid VIN to KDS" },
  { OEDT_SIMPLE_TEST_FUNCTION(u32WriteDIDValuetoKDS), OEDT_HW_EXC_NO, 10, "write Valid DID to KDS" },
  { OEDT_SIMPLE_TEST_FUNCTION(u32WriteErrorVINtoKDS), OEDT_HW_EXC_NO, 10, "write inValid VIN to KDS" },
  { OEDT_SIMPLE_TEST_FUNCTION(u32WriteErrorDIDtoKDS), OEDT_HW_EXC_NO, 10, "write inValid DID to KDS" },

{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
#else
 /* Signature verify tests for Cryptcard device */
   { {u32Cryptcard_IOCTRL_TriggerSignVerify, NULL,NULL}, OEDT_HW_EXC_NO, 10, "Cryptcard:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_NO, 10, "cryptcard:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_IOCTRL_SignFileType), OEDT_HW_EXC_NO, 10, "cryptcard:Get SignFile type" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_InvalidCertPath_TriggerSignVerify), OEDT_HW_EXC_NO, 10, "cryptcard:Invalid Path signveri" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_InvalidCertPath_GetSignFileType), OEDT_HW_EXC_NO, 10, "cryptcard:Invalid Path signType " },
   /* Signature verify tests for Cryptcard2 device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard2_IOCTRL_TriggerSignVerify), OEDT_HW_EXC_LCN2KAI, 10, "Cryptcard2:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard2_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Cryptcard2:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard2_IOCTRL_SignFileType), OEDT_HW_EXC_LCN2KAI, 10, " Cryptcard2:Get SignFile type" },
   /* Signature verify tests for Cryptnav device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptnav_IOCTRL_TriggerSignVerify), OEDT_HW_EXC_LCN2KAI, 10, "u32Cryptnav:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptnav_IOCTRL_SignVerifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "u32Cryptnav:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptnav_IOCTRL_SignFileType), OEDT_HW_EXC_LCN2KAI, 10, "u32Cryptnav:Get SignFile type" },
   /* Signature verify tests for card device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev1_IOCTRL_SignVerify), OEDT_HW_EXC_LCN2KAI, 10, "Dev1:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev1_IOCTRL_VerifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Dev1:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev1_IOCTRL_FileType), OEDT_HW_EXC_LCN2KAI, 10, "Dev1:Get SignFile type" },
   /* Signature verify tests for USBMS device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev2_IOCTRL_SignVerify), OEDT_HW_EXC_LCN2KAI, 10, "Dev2:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev2_IOCTRL_VerifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Dev2:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev2_IOCTRL_FileType), OEDT_HW_EXC_LCN2KAI, 10, "Dev2:Get SignFile type" },
   /* Signature verify tests for Cryptnavroot device */
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev3_IOCTRL_SignVerify), OEDT_HW_EXC_LCN2KAI, 10, "Dev3:Trigger SignVerify" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev3_IOCTRL_VerifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Dev3:Get SignVerify Status" },
   {OEDT_SIMPLE_TEST_FUNCTION( u32Noncrypt_dev3_IOCTRL_FileType), OEDT_HW_EXC_LCN2KAI, 10, "Dev3:Get SignFile type" },
   /* Multi thread sign verify */	
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_Signverify), OEDT_HW_EXC_LCN2KAI, 10, "Multi Thread sign verify"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_SignverifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Multi Thread sign verify status"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32MultiThread_SignType), OEDT_HW_EXC_LCN2KAI, 10, "Multi Thread Get sign file type"},
   /* Multi function with same device*/
   {OEDT_SIMPLE_TEST_FUNCTION( u32Cryptcard_Multifunction), OEDT_HW_EXC_NO, 10, "Cryptcard:Multifunction"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32Card_Multifunction), OEDT_HW_EXC_NO, 10, "Card:Multifunction"},
   /* Multi device with same functionality*/
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_Signverify), OEDT_HW_EXC_LCN2KAI, 10, "Multi dev sign verify"},
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_SignverifyStatus), OEDT_HW_EXC_LCN2KAI, 10, "Multi dev sign veri status "},
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_SignfileType), OEDT_HW_EXC_LCN2KAI, 10, "Multi dev sign file type"},
   /* Multi device with Multi functionality*/
   {OEDT_SIMPLE_TEST_FUNCTION( u32Multidev_Multifunction), OEDT_HW_EXC_LCN2KAI, 10, "Multi dev multi fun"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}


#endif /* */

};


/* === AUDIO ROUTE TEST === */
OEDT_FUNC_CFG oedt_pAUDIO_ROUTE_TEST_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_00),OEDT_HW_EXC_NO,5,"Special Config file required"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_01),OEDT_HW_EXC_NO,100,"ADevEnt1Out Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_02),OEDT_HW_EXC_NO,100,"AdevAudioSampler1Out Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_03),OEDT_HW_EXC_NO,100,"AdevAudioSampler2Out Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_04),OEDT_HW_EXC_NO,100,"AdevAcousticoutSpeech Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_05),OEDT_HW_EXC_NO,100,"AdevAcousticinSpeech Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_06),OEDT_HW_EXC_NO,100,"AdevVoiceOut Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_07),OEDT_HW_EXC_NO,100,"AdevMicro1In Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_08),OEDT_HW_EXC_NO,100,"AdevAudioInfoMonoOut Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_09),OEDT_HW_EXC_NO,100,"AdevAudioSampleStereo Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_11),OEDT_HW_EXC_NO,100,"AdevMPOut Test"},
#ifndef GEN3X86
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_10),OEDT_HW_EXC_NO,100,"AdevBTAudio Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_12),OEDT_HW_EXC_NO,100,"AdevMicro12AmpRef Test"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_13),OEDT_HW_EXC_NO,100,"AdevEsaiClockOnOff Test"},
#endif

{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)OEDT_AUDIO_ROUTE_TEST_14),OEDT_HW_EXC_NO,100,"Parellel Use Case1"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};




/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------DEV-KDS --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The following test cases have been moved to regression data set by cma5cob */

#if 0
OEDT_FUNC_CFG oedt_pKDS_DEV_Entries[] =
{
#if !defined(LSIM) && !defined(GEN3X86)
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpen), OEDT_HW_EXC_NO, 10, "DEV:OpenClose"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpenInvalParm), OEDT_HW_EXC_NO, 10, "DEV:ReOpen"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevReOpen), OEDT_HW_EXC_NO, 10, "DEV:CloseWithInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpenDiffModes), OEDT_HW_EXC_NO, 10, "DEV:OpenDiffModes"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevClose), OEDT_HW_EXC_NO, 10, "DEV:DevClose"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevCloseInvalParm), OEDT_HW_EXC_NO, 10, "DEV:DevCloseInvalParm"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevReClose), OEDT_HW_EXC_NO, 10, "DEV:DevReClose"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSEnaDisAccessright), OEDT_HW_EXC_NO, 10, "DEV:EnaDisAccessright"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSEnaDisAccessrightWithInvalParm), OEDT_HW_EXC_NO, 10, "DEV:EnaDisAccessrightWithInvalParm"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetNextEntryID), OEDT_HW_EXC_NO, 10, "DEV:GetNextEntryID"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetNextEntryIDWithInvalParm), OEDT_HW_EXC_NO, 10, "DEV:GetNextEntryIDWithInvalParm"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevInfo), OEDT_HW_EXC_NO, 10, "DEV:GetDevInfo"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevInfoWithInvalParm), OEDT_HW_EXC_NO, 10, "DEV:GetDevInfoWithInvalParm"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevInit), OEDT_HW_EXC_NO, 10, "DEV:Init"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDeleteEntryID), OEDT_HW_EXC_NO, 10, "DEV:DeleteEntryID"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSDeleteEntryIDWithInvalID), OEDT_HW_EXC_NO, 10, "DEV:DeleteEntryIDWithInvalID"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntry), OEDT_HW_EXC_NO, 10, "DEV:ReadEntry"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntryWithInvalID), OEDT_HW_EXC_NO, 10, "DEV:GetNextEntryIDWithInvalParm"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntryWithInvalParm), OEDT_HW_EXC_NO, 10, "DEV:ReadEntryWithInvalID"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntry), OEDT_HW_EXC_NO, 10, "DEV:WriteEntry"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryWithInvalID), OEDT_HW_EXC_NO, 10, "DEV:WriteEntryWithInvalID"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryWithInvalParam), OEDT_HW_EXC_NO, 10, "DEV:WriteEntryWithInvalParam"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersion), OEDT_HW_EXC_NO, 10, "DEV:GetVersion"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersionWithInvalParm), OEDT_HW_EXC_NO, 10, "DEV:GetVersionWithInvalParm"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSInvalMacroToIOCTL), OEDT_HW_EXC_NO, 10, "DEV:InvalMacroToIOCTL"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSClearAllData), OEDT_HW_EXC_NO, 10, "DEV:ClearAllData"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersionAfterDevclose), OEDT_HW_EXC_NO, 10, "DEV:GetVersionAfterDevclose"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSReWriteProtected), OEDT_HW_EXC_NO, 10, "DEV:ReWriteProtected"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteProtectedDel), OEDT_HW_EXC_NO, 10, "DEV:WriteProtectedDel"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryGreater), OEDT_HW_EXC_NO, 10, "DEV:WriteEntryGreater"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadcheck), OEDT_HW_EXC_NO, 10, "DEV:Readcheck"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetRemainingSize), OEDT_HW_EXC_NO, 10, "DEV:GetRemainingSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetRemainingSizeInval), OEDT_HW_EXC_NO, 10, "DEV:GetRemainingSizeInval"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteWithoutWEnable), OEDT_HW_EXC_NO, 10, "DEV:WriteWithoutWEnable"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSClearWithoutWEnable), OEDT_HW_EXC_NO, 10, "DEV:ClearWithoutWEnable"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSOverWrite), OEDT_HW_EXC_NO, 10, "DEV:OverWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteFull), OEDT_HW_EXC_NO, 10, "DEV:WriteFull"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteFullAndBackToFlash), OEDT_HW_EXC_NO, 10, "DEV:WriteFullAndBackToFlash"},
{OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteBackToFlash), OEDT_HW_EXC_NO, 10, "DEV:WriteBackToFlash"},
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------PDD -- --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#if 0

/* The following testcases have been moved to Regression dataset by cma5cob for the ticket CFG3-1263 */

OEDT_FUNC_CFG oedt_pPDD_Entries[] =
{
#if !defined(LSIM) && !defined(GEN3X86)
#if defined(GEN3ARM)   
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadWrite),              OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadSize),               OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteLengthToGreat),     OEDT_HW_EXC_NO, 30, "u32PDDNorUserWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteReadMorePools),     OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteReadMorePools"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserCheckEqualCluster),      OEDT_HW_EXC_NO, 10, "u32PDDNorUserCheckEqualCluster"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteTwoStreams),        OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteTwoStreams"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpOK),             OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpOK"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpMagicWrong),     OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpMagicWrong"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpWriteInterrupt), OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpWriteInterrupt"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpChecksumWrong),  OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpChecksumWrong"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadDataSize),               OEDT_HW_EXC_NO, 10, "u32PDDSccReadDataSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadWrite),                  OEDT_HW_EXC_NO, 10, "u32PDDSccReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadLengthToGreat),          OEDT_HW_EXC_NO, 10, "u32PDDSccReadLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteLengthToGreat),         OEDT_HW_EXC_NO, 10, "u32PDDSccWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWrite),                OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadDataSize),         OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadDataSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadWrite),            OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWriteReadCompare),     OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWriteReadCompare"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadWrongVersion),     OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadWrongVersion"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadLengthToGreat) ,   OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWriteLengthToGreat),   OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteCheckBackup) ,          OEDT_HW_EXC_NO, 10, "u32PDDSccWriteCheckBackup"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteCheckSync),             OEDT_HW_EXC_NO, 10, "u32PDDSccWriteCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteDestroyCheckSync),      OEDT_HW_EXC_NO, 10, "u32PDDSccWriteDestroyCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteCheckBackup) ,      OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteCheckBackup"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteCheckSync),         OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteEraseNorCheckSync), OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteEraseNorCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataFs) ,                 OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataFs"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataFsSecure),            OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataFsSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataCheckParamString),    OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataCheckParamString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataCheckParamLocation),  OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataCheckParamLocation"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataTwice),               OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataTwice"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataBackupNotExist) ,     OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataBackupNotExist"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataNormalNotExist),      OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataNormalNotExist"},
#endif
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------DataPool--------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The following testcases have been moved to Regression dataset by cma5cob for the ticket CFG3-1263 */

#if 0
OEDT_FUNC_CFG oedt_pDataPool_Entries[] =
{
#if !defined(LSIM) && !defined(GEN3X86)
#if defined(GEN3ARM)   
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefaultValueTrue),                   OEDT_HW_EXC_NO, 30, "u32DPGetDefaultValueTrue"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefaultValueAndState),               OEDT_HW_EXC_NO, 30, "u32DPGetDefaultValueAndState"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetChangedValueAndState),               OEDT_HW_EXC_NO, 30, "u32DPGetChangedValueAndState"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetConfig),                             OEDT_HW_EXC_NO, 30, "u32DPSetConfig"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetSetCodeDataAndSetConfig),            OEDT_HW_EXC_NO, 30, "u32DPChangeCodingdataAndSetConfig"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetPoolV850),                           OEDT_HW_EXC_NO, 30, "u32DPSetPoolV850"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetTestValueU8),                     OEDT_HW_EXC_NO, 30, "u32DPSetGetTestValueU8"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetToDefaultTestValueU8),               OEDT_HW_EXC_NO, 30, "u32DPSetToDefaultTestValueU8"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEarlyConfigDisplaySet),                 OEDT_HW_EXC_NO, 30, "u32DPEarlyConfigDisplaySet"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEarlyConfigDisplayGet),                 OEDT_HW_EXC_NO, 30, "u32DPEarlyConfigDisplayGet"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefaultValueCheckIfStored),          OEDT_HW_EXC_NO, 30, "u32DPSetDefaultValueCheckIfStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccess),                      OEDT_HW_EXC_NO, 30, "u32DPSetGetSRAMAccess"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccessWriteAll),              OEDT_HW_EXC_NO, 30, "u32DPSetGetSRAMAccessWriteAll"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccessMultiThread),           OEDT_HW_EXC_NO, 60, "u32DPSetGetSRAMAccessMultiThread"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoKdsEntryStored),       OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSRecordNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoInitNoKdsEntryStored), OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSRecordNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoKdsEntryStored),         OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoInitNoKdsEntryStored),   OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSWrongItemNoKdsEntryStored),    OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSWrongItemNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU32NoKdsEntryStored),     OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtU32NoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU16NoKdsEntryStored),     OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtU16NoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoKdsEntryStored),  OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtStringNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored), OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecord),                       OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSRecord"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoInit),                 OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSRecordNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItem),                         OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItem"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoInit),                   OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSWrongItem),                    OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSWrongItem"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU32),                     OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtU32"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU16),                     OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtU16"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtString),                  OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoInit),            OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtStringNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSBankElementsNoKdsEntryStored), OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSBankElementsNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSBankElementsStored),           OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSBankElementsStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefKDSToDefault),                    OEDT_HW_EXC_NO, 60, "u32DPSetDefKDSToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUndefElementCheckParameter),         OEDT_HW_EXC_NO, 30, "u32DPSetUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetUndefElementCheckParameter),         OEDT_HW_EXC_NO, 30, "u32DPGetUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementCheckParameter),        OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPDeleteUndefElementCheckParameter),      OEDT_HW_EXC_NO, 30, "u32DPDeleteUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUndefElement),                       OEDT_HW_EXC_NO, 30, "u32DPSetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetUndefElementUnknown),                OEDT_HW_EXC_NO, 30, "u32DPGetUndefElementUnknown"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPDeleteUndefElementUnknown),             OEDT_HW_EXC_NO, 30, "u32DPDeleteUndefElementUnknown"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetUndefElement),                    OEDT_HW_EXC_NO, 30, "u32DPSetGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDeleteGetUndefElement),              OEDT_HW_EXC_NO, 30, "u32DPSetDeleteGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitGetUndefElement),                   OEDT_HW_EXC_NO, 30, "u32DPInitGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitSetGetUndefElement),                OEDT_HW_EXC_NO, 30, "u32DPInitSetGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInit20UndefElements),                   OEDT_HW_EXC_NO, 30, "u32DPInit20UndefElements"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementNewProperty),           OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementNewProperty"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementSetDefaultProperty),    OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementSetDefaultProperty"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPWriteInitUndefElement),                 OEDT_HW_EXC_NO, 30, "u32DPWriteInitUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPReadInitUndefElementAfterReboot),       OEDT_HW_EXC_NO, 30, "u32DPReadInitUndefElementAfterReboot"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitSetGetUndefElementShared),          OEDT_HW_EXC_NO, 30, "u32DPInitSetGetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessInitUndefElementShared),    OEDT_HW_EXC_NO, 30, "u32DPOtherProcessInitUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessDeleteUndefElementShared),  OEDT_HW_EXC_NO, 30, "u32DPOtherProcessDeleteUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessSetUndefElementShared),     OEDT_HW_EXC_NO, 30, "u32DPOtherProcessSetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessGetUndefElementShared),     OEDT_HW_EXC_NO, 30, "u32DPOtherProcessGetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessAccessDelUndefElementShared),OEDT_HW_EXC_NO, 30, "u32DPOtherProcessAccessDelUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessInitDelUndefElementShared)  ,OEDT_HW_EXC_NO, 30, "u32DPOtherProcessInitDelUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserLockUnlock),						 OEDT_HW_EXC_NO, 30, "u32DPEndUserLockUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementEndUserIfLocked),    OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementEndUserIfLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementEndUserAfterUnLocked),OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementEndUserAfterUnLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementNoEndUserIfLocked),  OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementNoEndUserIfLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserLock2ModesUnlock),				 OEDT_HW_EXC_NO, 30, "u32DPEndUserLock2ModesUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetEndUserReadNewDefaultElement), OEDT_HW_EXC_NO, 30, "u32DPEndUserSetEndUserReadNewDefaultElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSaveNewEndUser1AndSetAllElements), OEDT_HW_EXC_NO, 30, "u32DPEndUserSaveNewEndUser1AndSetAllElements"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault), OEDT_HW_EXC_NO, 30, "u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetNoCurrentUserToDefault),       OEDT_HW_EXC_NO, 30, "u32DPEndUserSetNoCurrentUserToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetAllPoolsToFactoryDefault),     OEDT_HW_EXC_NO, 30, "u32DPEndUserSetAllPoolsToFactoryDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetAllPoolsToUserDefault),        OEDT_HW_EXC_NO, 30, "u32DPEndUserSetAllPoolsToUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserGetNoCurrentEndUserValue),        OEDT_HW_EXC_NO, 30, "u32DPEndUserGetNoCurrentEndUserValue"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserOtherProcessSetEndUser),          OEDT_HW_EXC_NO, 30, "u32DPEndUserOtherProcessSetEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserOtherProcessLockSetEndUserUnlock),OEDT_HW_EXC_NO, 30, "u32DPEndUserOtherProcessLockSetEndUserUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankLoadBankToCurrentEndUser),    OEDT_HW_EXC_NO, 30, "u32DPEndUserBankLoadBankToCurrentEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSaveCurrentEndUserToBank),    OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSaveCurrentEndUserToBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetValueSaveToBankLoadSavedBank),OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetValueSaveToBankLoadSavedBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankGetBank),                     OEDT_HW_EXC_NO, 30, "u32DPEndUserBankGetBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetBankDefault),              OEDT_HW_EXC_NO, 30, "u32SetBankDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetAllBanksDefault),          OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetAllBanksDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetEndUserDefault),           OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetEndUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetUserDefault),              OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetBankDefaultWithElemSecure),           OEDT_HW_EXC_NO, 30, "u32DPSetBankDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetEndUserDefaultWithElemSecure),        OEDT_HW_EXC_NO, 30, "u32DPSetEndUserDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUserDefaultWithElemSecure),           OEDT_HW_EXC_NO, 30, "u32DPSetUserDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemWrongParamString),       OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemWrongParamString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemBufferNull),             OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemBufferNull"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemWrongSize),              OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemWrongSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemCompleteWriteGetCmp),    OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemCompleteWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemArrayWriteGetCmp),       OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemArrayWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32StorePoolNow),                        OEDT_HW_EXC_NO, 30, "u32DPs32StorePoolNow"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32StorePoolNowWrongAccess),             OEDT_HW_EXC_NO, 30, "u32DPs32StorePoolNowWrongAccess"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPProcessExitWithOsalExitFunction),        OEDT_HW_EXC_NO, 30, "u32DPProcessExitWithOsalExitFunction"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPProcessExitWithoutOsalExitFunction),     OEDT_HW_EXC_NO, 30, "u32DPProcessExitWithoutOsalExitFunction"},
#endif
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

#if !defined(LSIM) && !defined(GEN3X86)
/* -== OSAL FPE (Floating Point exception) tests ==- */
OEDT_FUNC_CFG oedt_pOSAL_FPE_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPZeroDivAbort), OEDT_HW_EXC_NO, 10, "FPECheckFPZeroDivAbort" },
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPOverflowAbort), OEDT_HW_EXC_NO, 10, "FPECheckFPOverflowAbort" },
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPUnderflowAbort), OEDT_HW_EXC_NO, 10, "FPECheckFPUnderflowAbort" },
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPInvalidAbort), OEDT_HW_EXC_NO, 10, "FPECheckFPInvalidAbort" },
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

/* -== PRM (Physical Resource Manager) tests ==- */
OEDT_FUNC_CFG oedt_pPRM_Entries[] =
{
//   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_CDVD_inserted_event), OEDT_HW_EXC_NO, 60, "PRM: CDVD_inserted_event"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_CARD_inserted_event), OEDT_HW_EXC_GEN3G  , 60, "PRM: CARD_inserted_event"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_NaviDataMedia_event), OEDT_HW_EXC_GEN3G  , 60, "PRM: NaviDataMedia_event"},  
   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_DataMedia_event), OEDT_HW_EXC_NO, 60, "PRM: DataMedia_event"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_NaviDataMedia_event_TEST),OEDT_HW_EXC_GEN3G, 60, "PRM: NaviDataMedia_event_TEST"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_DevMedia_event), OEDT_HW_EXC_NO, 60, "PRM: Dev Media event"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_OC_test),OEDT_HW_EXC_NO, 60, "PRM: USB power overcurrent test"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32CARD_RegisterforprmNotification),OEDT_HW_EXC_NO, 60, "PRM: REGISTRATION FOR CARD_STATE"},
#if defined (LSIM) || defined (OSAL_GEN3_SIM)
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_UV_test),OEDT_HW_EXC_NO, 60, "PRM: USB power undervoltage test"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_UNDEF_test),OEDT_HW_EXC_NO, 60, "PRM: USB power undefined signal test"},
#else
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_CTRL_test),OEDT_HW_EXC_NO, 60, "PRM: USB power control test"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_CTRL_P1OFF),OEDT_HW_EXC_NO, 60, "PRM: USB power control PORT1 OFF"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_CTRL_P1ON),OEDT_HW_EXC_NO, 60, "PRM: USB power control PORT1 ON"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_CTRL_P2OFF),OEDT_HW_EXC_NO, 60, "PRM: USB power control PORT2 OFF"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32USB_PWR_CTRL_P2ON),OEDT_HW_EXC_NO, 60, "PRM: USB power control PORT2 ON"},
#endif
   {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_FSBenchmark_Entries[] = 
{
  { OEDT_TEST_WITH_TEARDOWN(FSBenchmarkSeqCharWrite), OEDT_HW_EXC_NO, 7200, "benchmark character-wise writing on /" },
  { OEDT_TEST_WITH_SETUP_AND_TEARDOWN(FSBenchmarkSeqCharRead), OEDT_HW_EXC_NO, 7200, "benchmark character-wise reading on /" },
  { OEDT_TEST_WITH_SETUP_AND_TEARDOWN(FSBenchmarkSeqBlockWrite), OEDT_HW_EXC_NO, 7200, "benchmark block-wise writing on /" },
  { OEDT_TEST_WITH_SETUP_AND_TEARDOWN(FSBenchmarkSeqBlockRead), OEDT_HW_EXC_NO, 7200, "benchmark block-wise reading on /" },
  { OEDT_EMPTY_TEST, OEDT_HW_EXC_ALL, 5, "" }
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------- SENSOR_PROXY------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pSENSOR_PROXY_Entries[] =
{
#if defined(GEN3ARM)
/* !!!--------------------- ODOMETER--------------------------!!! */
    /* The following test cases have been moved to regression data set*/
#if 0
   {OEDT_SIMPLE_TEST_FUNCTION(u32OdoOpenDev),                         OEDT_HW_EXC_NO,    50, "ODO: Open and Close Device"},        /*test case no.#01 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoReOpen),                         OEDT_HW_EXC_NO,    50, "ODO: Re-Open"},                      /*test case no.#02 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoCloseAlreadyClosed) ,            OEDT_HW_EXC_NO,    50, "ODO: Re-Close"},                     /*test case no.#03 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoReadpassingnullbuffer) ,         OEDT_HW_EXC_NO,    10, "ODO: Read ODO passing NULL buffer"}, /*test case no.#04 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoBasicRead),                      OEDT_HW_EXC_NO,    50, "ODO: Basic read"},                   /*test case no.#05 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoBasicReadSeq),                   OEDT_HW_EXC_ALL,   50, "ODO: Basic Read Seq"},               /*test case no.#06 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoReadAndFlushSeq),                OEDT_HW_EXC_ALL,   50, "ODO: Read and Flush"},               /*test case no.#07 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoInterfaceCheckSeq),              OEDT_HW_EXC_ALL,   50, "ODO: Interface Check"},              /*test case no.#08 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoCheckReadLargeNoOfRecords),      OEDT_HW_EXC_ALL,   50, "ODO: Read Large NoOfBlocks"},        /*test case no.#09 */
   {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoCheckReadMoreThanBufferLength),  OEDT_HW_EXC_ALL,   50, "ODO: Read More than BufLen"},        /*test case no.#10 */
   
#endif
/* !!!-------------------------- GYRO --------------------------!!! */

   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroOpenClose),                     OEDT_HW_EXC_NO,    50, "GYRO: Gyro DevOpnClose"},                 /*test case no. #01 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroReOpen),                        OEDT_HW_EXC_NO,    50, "GYRO: Gyro Re-Open"},                     /*test case no. #02 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCloseAlreadyClosed),            OEDT_HW_EXC_NO,    50, "GYRO: Gyro Re-Close"},                    /*test case no. #03 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroTemperature ),                  OEDT_HW_EXC_NO,   10, "GYRO: Gyro temperature"},                 /*test case no. #04 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroBasicRead),                     OEDT_HW_EXC_NO,    10, "GYRO: Gyro basicRead"},                   /*test case no. #05 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroReadPassingNullBuffer),         OEDT_HW_EXC_NO,    10, "GYRO: Gyro read passing NULL buffer"},    /*test case no. #06 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_r),          OEDT_HW_EXC_NO,    10, "GYRO: Chck R axis readseq vals"}, /*test case no. #07 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_s),          OEDT_HW_EXC_NO,    10, "GYRO: Chck S axis readseq vals"}, /*test case no. #08 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_t),          OEDT_HW_EXC_NO,    10, "GYRO: Chck T axis readseq vals"}, /*test case no. #09 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroInterfaceCheckSeq),             OEDT_HW_EXC_NO,    10, "GYRO: Check all Gyro  IOctrl interfaces"},/*test case no. #10 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroIOCTRLPassingNullBuffer),       OEDT_HW_EXC_NO,    10, "GYRO: Gyro IOCTRL passing NULL buffer"},  /*test case no. #11 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroOedt_u32ValuesPlot),            OEDT_HW_EXC_NO,   120, "GYRO: Gyro R-S-T Graphical plot"},              /*test case no. #12 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroSelfTest),                      OEDT_HW_EXC_NO,    30, "GYRO: Gyro selftest"},                    /*test case no. #13 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroHwInfo),                        OEDT_HW_EXC_NO,    30, "GYRO: Read Gyro  hardware info"},         /*test case no. #14 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroGetCycleTime),                  OEDT_HW_EXC_NO,    50, "GYRO: Get Gyro Cycle time"},              /*test case no. #15 */

/* !!!-------------------------- ACC --------------------------!!! */

   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevOpenClose),                    OEDT_HW_EXC_NO,   50, "ACC: Open Close"},                /*test case no.  #25 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevReOpen),                       OEDT_HW_EXC_NO,   50, "ACC: Re Open"},                    /*test case no. #26 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevCloseAlreadyClosed),           OEDT_HW_EXC_NO,   50, "ACC: Re-Close"},                   /*test case no. #27 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevRead),                         OEDT_HW_EXC_NO,   50, "ACC: Data read"},                  /*test case no. #28 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevReadpassingnullbuffer),        OEDT_HW_EXC_NO,   50, "ACC: Read with null buffer"},      /*test case no. #29 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevInterfaceCheckSeq),            OEDT_HW_EXC_ALL,  50, "ACC: IO Control Interfaces test"}, /*test case no. #30 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevIOCTRLpassingnullbuffer),      OEDT_HW_EXC_ALL,  50, "ACC: IO Control with null buffer"},/*test case no. #31 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32MultiThreadBasicRead),               OEDT_HW_EXC_NO,    30, "Sensor Proxy Multi Thread"},       /*test case no. #32 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccSelfTest),                        OEDT_HW_EXC_NO,   30, "ACC: Acc selftest"},               /*test case no. #33 */ 
   {OEDT_SIMPLE_TEST_FUNCTION(ACCOedt_u32ValuesPlot),                 OEDT_HW_EXC_NO,  120, "ACC: ACC Values post "},           /*test case no. #34 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccHwInfo),                          OEDT_HW_EXC_NO,   30, "ACC: Read acc hardware info"},     /*test case no. #35 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccCheckACCValuesSeq_x),             OEDT_HW_EXC_NO,   10, "ACC: Chck X axis readseq vals"},   /*test case no. #36*/  
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccCheckACCValuesSeq_y),             OEDT_HW_EXC_NO,   10, "ACC: Chck Y axis readseq vals"},   /*test case no. #37*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccCheckACCValuesSeq_z),             OEDT_HW_EXC_NO,   10, "ACC: Chck Z axis readseq vals"},   /*test case no. #38*/  
   {OEDT_SIMPLE_TEST_FUNCTION(u32AccGetCycleTime),                    OEDT_HW_EXC_NO,   50, "ACC: Get Acc CycleTime"},          /*test case no. #39*/     
   

/* !!!-------------------------- ABS --------------------------!!! */

  {OEDT_SIMPLE_TEST_FUNCTION(u32AbsOpenDev),                         OEDT_HW_EXC_NO,    50, "ABS:Open and close device"},    /*test case no. #39 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsBasicRead),                      OEDT_HW_EXC_NO,    50, "ABS:Basic basic read"},         /*test case no. #40 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsBasicReadSeq),                   OEDT_HW_EXC_NO,    50, "ABS:Basic read seq"},           /*test case no. #41 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsInterfaceCheckSeq),              OEDT_HW_EXC_NO,    50, "ABS:Interface check"},          /*test case no. #42 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsReOpen),                         OEDT_HW_EXC_NO ,   50, "ABS:Re open"},                  /*test case no. #43 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsCloseAlreadyClosed),             OEDT_HW_EXC_NO ,   50, "ABS:Re close"},                 /*test case no. #44 */
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsReadpassingnullbuffer),          OEDT_HW_EXC_NO ,   50, "ABS:Re close"},                 /*test case no. #45 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32AbsStatusCheck),                     OEDT_HW_EXC_NO,    50, "ABS:Status Check"},             /*test case no. #46 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32AbsGetCycleTime),                    OEDT_HW_EXC_NO,    50, "ABS:Get Abs CycleTime"},        /*test case no. #47*/
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AbsStubTestReadDirection),          OEDT_HW_EXC_NO,    50, "ABS: Stub Test for ABS Direction"},/*test case no. #48*/
#endif
   {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* GNSS test cases. To connect to V850 and get GNSS data */
OEDT_FUNC_CFG oedt_pGNSS_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyBasiOpenClose)            ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: DevOpnClose................."}, /*  0 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReOpen)                   ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: ReOpen......................"}, /*  1 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReClose)                  ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: ReClose....................."}, /*  2 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyBasicRead)                ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: Basic Read.................."}, /*  3 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReadByPassingNullBufffer) ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: Basic Read By Passing NULL.."}, /*  4 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyIOCntInterfaceCheck)      ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: IOControl Interfaces........"}, /*  5 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyIOCntChkInvPar)           ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: IOControl Invalid param....."}, /*  6 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyReOpen)                    ,  OEDT_HW_EXC_NO  , 05, "GPS Proxy : ReOpen......................"}, /*  7 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyReClose)                   ,  OEDT_HW_EXC_NO  , 05, "GPS Proxy : ReClose....................."}, /*  8 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyBasiOpenClose)             ,  OEDT_HW_EXC_NO  , 05, "GpS Proxy : DevOpnClose................."}, /*  9 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyBasicRead)                 ,  OEDT_HW_EXC_NO  , 05, "GpS Proxy : Device read................."}, /*  10*/
  {OEDT_SIMPLE_TEST_FUNCTION(GnssOedt_u32Teseo2FwUpdate)               ,  OEDT_HW_EXC_NO  , 500,"GNSS Proxy: Teseo-2 Fw Update..........."}, /*  11*/
  {OEDT_SIMPLE_TEST_FUNCTION(GnssOedt_s32TesFwUpdateStub)              ,  OEDT_HW_EXC_NO  , 500,"GNSS Proxy: Firmware update Stub........"}, /*  12*/
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxySetEpoch)                 ,  OEDT_HW_EXC_NO  , 200, "GNSS Proxy: Set GNSS Epoch............."}, /*  13*/
  {OEDT_SIMPLE_TEST_FUNCTION(GnssOedt_s32TesGetCrc)                    ,  OEDT_HW_EXC_NO  , 100,"GNSS Proxy: FW update: Get Teseo CRC...."}, /*  14*/  
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatInterChk)                  ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: Sat Inter Get and Set Value Check.."}, /*  15*/
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyInfiniteRead)             ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: Infinite Read..............."}, /*  16 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyMultipleIOOpenIOClose)    ,  OEDT_HW_EXC_NO  , 100,"GNSS Proxy: MultipleIOOpenIOClose......."}, /*  17*/
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32DiagSatInterChk)              ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: Set Sat Sys using Diag......"}, /*  18*/
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyGetEpoch)                 ,  OEDT_HW_EXC_NO  , 200, "GNSS Proxy: Get GNSS Epoch............."}, /*  19*/
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)GnssOedt_u32GetNmeaMsgList) ,       OEDT_HW_EXC_NO,   20, "NMEA MSG List-Antenna is must."}, /*  20 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatCfgBlk200)                 , OEDT_HW_EXC_NO  , 40,  "GNSS Proxy: SetSatSys-CfgBlk200.......  "}, /*  21 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32ChkSatCfgBlk227)              , OEDT_HW_EXC_NO   , 40,  "GNSS Proxy: SetSatSys-CfgBlk227......"},   /*  22 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32ChkSatCfgBlk200227)           , OEDT_HW_EXC_NO   , 40, "GNSS Proxy: SetSatSys-CfgBlk200 & 227.  "}, /*  23 */
  {OEDT_SIMPLE_TEST_FUNCTION(GnssOedt_u32Teseo3FwUpdate)               , OEDT_HW_EXC_NO   , 40,  "Teseo-3 Fw update"},                       /*  24 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
  
};
/* ADR3Ctrl test cases. To connect to V850 and get ADR3Ctrl Status */
OEDT_FUNC_CFG oedt_pADR3Ctrl_Entries[] =
{
#if defined(GEN3ARM)
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlBasiOpenClose)         ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: DevOpnClose................."}, /*  0 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlReadonlyOpen)          ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: ReadOnly Open..............."}, /*  1 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlWriteonlyOpen)         ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: WriteOnly Open.............."}, /*  2 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlMaxOpenCheckOpen)      ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: Max Open Check.............."}, /*  3 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlDoubleClose)           ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: Double Close................"}, /*  4 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlInvalidIoControl)      ,  OEDT_HW_EXC_NO  , 05,    "ADR3Ctrl: Invalid IOControl..........."}, /*  5 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlCallBackRegister)      ,  OEDT_HW_EXC_NO  , 5000,  "ADR3Ctrl: Register CallBack..........."}, /*  6 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlResetCheck)            ,  OEDT_HW_EXC_NO  , 5000,  "ADR3Ctrl: Reset ADR3Ctrl.............."}, /*  7 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlSPIModResetCheck)      ,  OEDT_HW_EXC_NO  , 9000,  "ADR3Ctrl: Reset SPI mod ADR3Ctrl......"}, /*  8 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtADR3CtrlSPI_NOR_ModeCheck)     ,  OEDT_HW_EXC_NO  , 9000,  "ADR3Ctrl: SPI and Nor mode Check......"}, /*  9 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADR3CtrlMultiProcessTest)          ,  OEDT_HW_EXC_NO  , 10000, "ADR3Ctrl: MultiProcess................"}, /*  10 */  
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADR3CtrlMPOpenTest)                ,  OEDT_HW_EXC_NO  , 9000,  "ADR3Ctrl: open Concuny................"}, /*  11 */  
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADR3CtrlMPCloseTest)               ,  OEDT_HW_EXC_NO  , 9000,  "ADR3Ctrl: close Concuny..............."}, /*  12 */  
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADR3CtrlMPRegistrCBTest)           ,  OEDT_HW_EXC_NO  , 9000,  "ADR3Ctrl: Register Concuny............"}, /*  13 */ 
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADR3CtrlMPResetSPITest)            ,  OEDT_HW_EXC_NO  , 10000, "ADR3Ctrl: SPI mod reset..............."}, /*  14 */ 
#endif  
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};
/* GNSS test cases. To connect to V850 and get GNSS data */
OEDT_FUNC_CFG oedt_pChEnc_Entries[] =
{
#if defined(GEN3ARM)
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChencOpenCloseDevice)                 ,  OEDT_HW_EXC_NO  , 05, "ChEnc: DevOpnClose......................."}, /*  0 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChencOpenDeviceInvalParam)            ,  OEDT_HW_EXC_NO  , 05, "ChEnc: Open Invalid Param................"}, /*  1 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChencMultOpenDevice)                  ,  OEDT_HW_EXC_NO  , 05, "ChEnc: Multiple open....................."}, /*  2 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChencOpenDeviceDiffModes)             ,  OEDT_HW_EXC_NO  , 05, "ChEnc: Open Device DiffModes............."}, /*  3 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChenDevCloseAlreadyClosed)            ,  OEDT_HW_EXC_NO  , 05, "ChEnc: Close already closed.............."}, /*  4 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChenDevEncodePosInfo)                 ,  OEDT_HW_EXC_NO  , 05, "ChEnc: Encode pos info..................."}, /*  5 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ChenDevEncodeChkDisFeature) 	       ,	 OEDT_HW_EXC_NO  , 05, "ChEnc: Chk Device Dis feature............"}, /*  6 */

#endif  
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!---------------------- Inputadr3Comm -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pIncAdr3Comm_RegressionEntries[] =
{
#ifdef OSAL_GEN3

   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV1),         OEDT_HW_EXC_NO,   30, "IncCommAdr3V1"},               /*   0 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV2),         OEDT_HW_EXC_NO,   30, "IncCommAdr3V2"},               /*   1 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV3),         OEDT_HW_EXC_NO,   30, "IncCommAdr3V3"},               /*   2 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV4),         OEDT_HW_EXC_NO,   30, "IncCommAdr3V4"},               /*   3 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV5),         OEDT_HW_EXC_NO,   30, "IncCommAdr3V5"},               /*   4 */
#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------IncAdr3Performance------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pIncAdr3Performance_RegressionEntries[] =
{
#ifdef OSAL_GEN3
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncPerformanceAdr),         OEDT_HW_EXC_NO,   30, "IncPerformanceAdr"},           /*   0  */
#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- ADC --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pADC_Entries[] =
{
#ifdef LSIM
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCChanReOpen) ,OEDT_HW_EXC_NO,   			50, "u32ADCChanReOpen"},        		/*   1 */
  {OEDT_SIMPLE_TEST_FUNCTION(OpenCloseADCSubUnit) ,OEDT_HW_EXC_NO,   			50, "OpenCloseADCSubUnit"},        		/*   2 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCGetResolution) ,OEDT_HW_EXC_NO,   			50, "u32ADCGetResolution"},        		/* 3 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCChanCloseAlreadyClosed) ,OEDT_HW_EXC_NO,   			50, "u32ADCChanCloseAlreadyClosed"},        		/*   4 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCRead) ,OEDT_HW_EXC_NO,   			50, "u32ADCRead"},        		/*   5 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCSetSingleThresholdCallbackCH5) ,OEDT_HW_EXC_NO,   			100, "u32ADCSetSingleThresholdCallbackCH5"},        		/*   7 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADClogicalDualthreadRead) ,OEDT_HW_EXC_NO,        1000, "Logical Dual thread Channel Read"},        /*  8 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCMultiProcessTest) ,OEDT_HW_EXC_NO,        1000, "Multi Process Test"},        /*  9 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCUnsupportedIoctrlCheck) ,OEDT_HW_EXC_NO,        100, "Unsupported ioctrl check"},        /*  10 */
#endif
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_TESTCON_ENTRY oedt_pTestOverallDataset[] =
{
{oedt_pCryptdev_Entries,    "CryptSIGN"},
{oedt_pAUDIO_ROUTE_TEST_Entries,    "AUDIO ROUTE TESTING"},              /*AUDIO ROUTE TESTING */
#if 0   // The following test cases have been moved to regression data set by cma5cob
{oedt_pKDS_DEV_Entries,     "DEV_KDS"},         /* DEV_KDS tests       */
#endif
#if !defined(LSIM) && !defined(GEN3X86)
{oedt_pOSAL_FPE_Entries,    "OSAL_FPE"},        /* OSAL Floating Point exception tests in Abort Mode */
#endif
	{oedt_pPRM_Entries,         						"PRM"},             /* PRM tests */
#if 0   // COMMENT OUT all Tests to make it compilable  OSR
{oedt_pComPerfTest_Entries,						   "COMM_PERF"},       // Performance tests for communication (frameworks)
{oedt_pGPS_Entries,         						   "GPS"},             /*  1   */  
{oedt_pFD_CSM_Entries,      							"FD_CSM"},          /*	3	*/
#endif
{oedt_FSBenchmark_Entries,                   "filesystem speed test"},
#if 0   // The following test cases have been moved to regression data set by cma5cob
{oedt_pFFS3_CommonFS_Entries, 					"FFS3"},
#endif
#ifdef LSIM
{oedt_pSPM_VOLT_Entries,  							"Volt device test"},  
{oedt_pADC_Entries,                          "ADC"},
#endif
#if defined(GEN3ARM)   
{oedt_pSENSOR_PROXY_Entries,	"Sensor proxy tests"}, /* ACCELEROMETER */
#endif

{oedt_pGNSS_Entries,	"GNSS proxy tests"}, /* GNSS proxy support for GEN3 platform */

#if defined(GEN3ARM)   
{oedt_pChEnc_Entries,	"China encoder tests"}, /* GNSS proxy support for GEN3 platform */
#endif
#if defined(GEN3ARM)   
#if 0   // The following entries have been moved to Regression dataset by cma5cob for the ticket CFG3-1263 
{oedt_pPDD_Entries     ,	"PDD"},      /* PDD tests for GEN3 platform */
{oedt_pDataPool_Entries,	"DataPool"}, /* DataPool tests for GEN3 platform */
#endif
{oedt_pADR3Ctrl_Entries,	"Dev_ADR3Ctrl"}, /* ADR3Ctrl Pool tests for GEN3 platform */
#endif
{oedt_pIncAdr3Comm_RegressionEntries,             "INC ADR3 COMMUNICATION TEST"},      /*INC COMMUNICATION TEST*/
{oedt_pIncAdr3Performance_RegressionEntries,      "INC ADR3 PERFORMANCE TEST"},        /*INC PERFORMANCE TEST*/
{NULL,                                       ""}

};




/* -== ACOUSTICOUT ==- */
OEDT_FUNC_CFG oedt_pACOUSTICOUTLong_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T001), OEDT_HW_EXC_NO,  300, "TL001 play long 44100-16-2      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T002), OEDT_HW_EXC_NO,  300, "TL002 play long 22050-16-2      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T003), OEDT_HW_EXC_NO,  300, "TL003 play long 44100-16-1      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T004), OEDT_HW_EXC_NO,  300, "TL004 play long 22050-16-1      "},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


OEDT_TESTCON_ENTRY oedt_pTestAcousticLongDataset[] =
{
{oedt_pACOUSTICOUTLong_Entries,    "AcousticoutLong"},
{NULL, ""}
};



/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------ DEV_CDCTRL + DEV_CDAUDIO--------------------!!! */
/* \di_projects\linked\di_linux_os\linux_osal\osal_dev_test\tests\sources\oedt_Cd_TestFuncs.c */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pCDCTRLCDAUDIO_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T000)           ,OEDT_HW_EXC_NO,   5, "T000 - Trace Off................"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T001)           ,OEDT_HW_EXC_NO,  60, "T001 - Open+Close CDCTRL........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T002)           ,OEDT_HW_EXC_NO,  60, "T002 - Open+Close CDAUDIO......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_TPRINT_NOCD)    ,OEDT_HW_EXC_NO,3600, "T--- - REMOVE CD................"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO, 600, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca01)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA01 monitor"},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T050)           ,OEDT_HW_EXC_NO, 120, "T050 - detect UDF format........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_HW_EXC_NO,  60, "T003 - Eject-Insert Loop........"},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_inslot)    ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL CD INSLOT."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO CD INSLOT"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_HW_EXC_NO,  60, "T003 - Eject-Insert Loop........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_TPRINT_NOCD)    ,OEDT_HW_EXC_NO,3600, "T--- - REMOVE CD................"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca02)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA02 monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T100)           ,OEDT_HW_EXC_NO, 240, "T100 - read sectors bwd+verify.."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T101)           ,OEDT_HW_EXC_NO, 240, "T101 - read sectors fwd+speedchk"},
  //{OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T102)           ,OEDT_HW_EXC_NO, 120, "T102 - read  /cdrom/LEER0000.DAT"},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_inslot)    ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL CD INSLOT."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO CD INSLOT"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_HW_EXC_NO,  60, "T003 - Eject-Insert Loop........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_TPRINT_NOCD)    ,OEDT_HW_EXC_NO,3600, "T--- - REMOVE CD................"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca03)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA03 monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T150)           ,OEDT_HW_EXC_NO,  60, "T150 - CDDA detect.............."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T151)           ,OEDT_HW_EXC_NO, 300, "T151 - CDDA playrange play......"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T152)           ,OEDT_HW_EXC_NO,  60, "T152 - CDDA CD-Text compare....."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T153)           ,OEDT_HW_EXC_NO, 200, "T153 - CDDA playrange fwd......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T154)           ,OEDT_HW_EXC_NO, 300, "T154 - CDDA playrange fbw......."},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_inslot)    ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL CD INSLOT."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO CD INSLOT"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_HW_EXC_NO,  60, "T003 - Eject-Insert Loop........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T013)           ,OEDT_HW_EXC_NO,3600, "T013 - REMOVE CD Monitor........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},
  
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca04)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA04 monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_HW_EXC_NO,  60, "T003 - Eject-Insert Loop........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T013)           ,OEDT_HW_EXC_NO,3600, "T013 - REMOVE CD Monitor........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},

  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca05)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA05 monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T155)           ,OEDT_HW_EXC_NO,  60, "T155 - Mixed Mode CD detect....."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T156)           ,OEDT_HW_EXC_NO, 300, "T156 - Mixed Mode CD play range."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T157)           ,OEDT_HW_EXC_NO, 300, "T157 - Mixed Mode Read Data....."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T158_masca05)   ,OEDT_HW_EXC_NO, 300, "T158 - Mixed Mode Switch Monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T013)           ,OEDT_HW_EXC_NO,3600, "T013 - REMOVE CD Monitor........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_nocd)      ,OEDT_HW_EXC_NO,  60, "T010 - IOCTRLs CDCTRL W/O CD...."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_HW_EXC_NO,  60, "T011 - IOCTRLs CDAUDIO W/O CD..."},

  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca03)   ,OEDT_HW_EXC_NO,3600, "T012 - Insert CD MASCA03 monitor"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T150)           ,OEDT_HW_EXC_NO,  60, "T150 - CDDA detect.............."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T151)           ,OEDT_HW_EXC_NO, 300, "T151 - CDDA playrange play......"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T152)           ,OEDT_HW_EXC_NO,  60, "T152 - CDDA CD-Text compare....."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T153)           ,OEDT_HW_EXC_NO, 200, "T153 - CDDA playrange fwd......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T154)           ,OEDT_HW_EXC_NO, 300, "T154 - CDDA playrange fbw......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T159)           ,OEDT_HW_EXC_NO, 180, "T159 - Voltage Handling....."},

  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T255)           ,OEDT_HW_EXC_NO,   5, "T255 - TraceLevel++............."},
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


OEDT_TESTCON_ENTRY oedt_pVWMIBDataset[] =
{
	{oedt_pCDCTRLCDAUDIO_Entries,       "MASCA SCSI CD"},             /* 01   Masca SCSI cd Drive (OSAL dev_cdctrl, dev_cdaudio)*/
	{NULL, ""}
};
