extern "C" {
#include "osal_if.h"
#include "oedt_testing_macros.h"
#include "oedt_AutoTester_RegressionDataSet.h"
#include "oedt_Acousticin_TestFuncs.h"
#include "oedt_Acousticout_TestFuncs.h"
#include "oedt_Acousticsrc_TestFuncs.h"
#include "oedt_osalcore_TestFuncs.h"
#include "oedt_FFS2_CommonFS_TestFuncs.h"
#include "oedt_FFS3_CommonFS_TestFuncs.h"
#include "oedt_Registry_TestFuncs.h"
#include "oedt_DiagEOL_TestFuncs.h"
#include "oedt_Cryptcard_CommonFS_TestFuncs.h"
#include "oedt_Cryptcard2_CommonFS_TestFuncs.h"
#include "oedt_Cryptcard_SD_CommonFS_TestFuncs.h"
#include "oedt_Cryptnav_CommonFS_TestFuncs.h"
#include "../RPC/oedt_test_rpc_core.h"
#include "oedt_FFD_TestFuncs.h"
#include "oedt_RAMDISK_CommonFS_TestFuncs.h"
#include "oedt_Audio_LoopBackTestFuncs.h"
#include "oedt_Platform_Startup.h"
#include "oedt_RegistryTest.h"
#include "oedt_osalcore_FPE_TestFuncs.h"
#include "oedt_RebootDummy.h"
#include "oedt_testing_macros.h"
#include "oedt_KDS_TestFuncs.h"
#include "oedt_Errmem_TestFuncs.h"
#include "oedt_Trace_TestFuncs.h"
#include "oedt_prm_testfuncs.h"
#include "oedt_Host_TestFuncs.h"
#include "oedt_fs_dev_testfuncs.h"
#include "oedt_Auxclock_TestFuncs.h"
#include "oedt_Audio_TestFuncs.h"
#include "oedt_Modules_TestFuncs.h"
#ifdef IOSC_ACTIV
#include "iosc_event_testcases.h"
#include "iosc_ringbuffer_testcases.h"
#include "iosc_semaphore_testcases.h"
#include "iosc_sharedmem_testcases.h"
#include "iosc_testcases_global.h"
#endif
#include "oedt_spm_TestFuncs.h"
#include "oedt_SWC_TestFuncs.h"
#include "OsalIoscTestCommon.h"
#include "OsalIoscMQCommon.h"
#include "oedt_GPIO_TestFuncs.h"
#include "oedt_rtc_TestFuncs.h"
#include "oedt_Odometer_TestFuncs.h"
#include "oedt_Gyro_TestFuncs.h"
}

#include <persigtest.h>


char oedt_boardname[30]="DummyBoardID";
#define RET_SUCCESS 0

#ifdef LSIM


//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_OpenCloseDevs_Test)
{
        tU32 ret    = u32SPM_OpenCloseDevs();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_CreateRemoveNotifications_Test)
{
        tU32 ret    = u32SPM_Volt_CreateRemoveNotifications();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_BoardVoltage_Test)
{
        tU32 ret    = u32SPM_Volt_BoardVoltage();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_UserVoltageRange_Test)
{
        tU32 ret    = u32SPM_Volt_UserVoltageRange();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_BoardCurrentScale_Test)
{
        tU32 ret    = u32SPM_Volt_BoardCurrentScale();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_VoltageLevel_Test)
{
        tU32 ret    = u32SPM_Volt_VoltageLevel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_ReadBoardCurrent_Test)
{
        tU32 ret    = u32SPM_Volt_ReadBoardCurrent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_IOCTRL_Verify_Test)
{
        tU32 ret    = u32SPM_Volt_IOCTRL_Verify();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_SysLevelHistory_Test)
{
        tU32 ret    = u32SPM_Volt_SysLevelHistory();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSPM_VOLT_RegressionEntries, u32SPM_Volt_UserLevelHistory_Test)
{
        tU32 ret    = u32SPM_Volt_UserLevelHistory();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#endif

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsBasic_OpenReadDevice_Test)
{
        tU32 ret    = u32GpsBasic_OpenReadDevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsInterfaceCheck_Test)
{
        tU32 ret    = u32GpsInterfaceCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsRegressionTest_Test)
{
        tU32 ret    = u32GpsRegressionTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsDurationTimeFix_Test)
{
        tU32 ret    = u32GpsDurationTimeFix();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsDeviceOpenClosetest_Test)
{
        tU32 ret    = u32GpsDeviceOpenClosetest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPS_RegressionEntries, u32GpsDeviceAntennaStatustest_Test)
{
        tU32 ret    = u32GpsDeviceAntennaStatustest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, u32OdoOpenDev_Test)
{
        tU32 ret    = u32OdoOpenDev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, tu32OdoBasicReadSeq_Test)
{
        tU32 ret    = tu32OdoBasicReadSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, tu32OdoReadAndFlushSeq_Test)
{
        tU32 ret    = tu32OdoReadAndFlushSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, tu32OdoInterfaceCheckSeq_Test)
{
        tU32 ret    = tu32OdoInterfaceCheckSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, tu32OdoCheckReadLargeNoOfRecords_Test)
{
        tU32 ret    = tu32OdoCheckReadLargeNoOfRecords();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pODO_RegressionEntries, tu32OdoCheckReadMoreThanBufferLength_Test)
{
        tU32 ret    = tu32OdoCheckReadMoreThanBufferLength();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroCheckGyroValuesSeq_r_Test)
{
        tU32 ret    = tu32GyroCheckGyroValuesSeq_r();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroCheckGyroValuesSeq_s_Test)
{
        tU32 ret    = tu32GyroCheckGyroValuesSeq_s();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroInterfaceCheckSeq_Test)
{
        tU32 ret    = tu32GyroInterfaceCheckSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroCheckGyroValuesSeq_t_Test)
{
        tU32 ret    = tu32GyroCheckGyroValuesSeq_t();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroReadpassingnullbuffer_Test)
{
        tU32 ret    = tu32GyroReadpassingnullbuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroIOCTRLpassingnullbuffer_Test)
{
        tU32 ret    = tu32GyroIOCTRLpassingnullbuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32Gyrobasicread_Test)
{
        tU32 ret    = tu32Gyrobasicread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroReadSeq_Test)
{
        tU32 ret    = tu32GyroReadSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGYRO_RegressionEntries, tu32GyroCheckTimeOutIllegalReadSeq_Test)
{
        tU32 ret    = tu32GyroCheckTimeOutIllegalReadSeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pGYRO_RegressionEntries, u32Gyrotemperature_Test)
{
        tU32 ret    = u32Gyrotemperature();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevOpenClose_Test)
{
        tU32 ret    = u32GPIODevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevCloseAlreadyClosed_Test)
{
        tU32 ret    = u32GPIODevCloseAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevReOpen_Test)
{
        tU32 ret    = u32GPIODevReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevOpenCloseDiffModes_Test)
{
        tU32 ret    = u32GPIODevOpenCloseDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevOpenCloseInvalAccessMode_Test)
{
        tU32 ret    = u32GPIODevOpenCloseInvalAccessMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevRead_Test)
{
        tU32 ret    = u32GPIODevRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIODevWrite_Test)
{
        tU32 ret    = u32GPIODevWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOGetVersion_Test)
{
        tU32 ret    = u32GPIOGetVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetInput_Test)
{
        tU32 ret    = u32GPIOSetInput();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutput_Test)
{
        tU32 ret    = u32GPIOSetOutput();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetRemoveCallback_Test)
{
        tU32 ret    = u32GPIOSetRemoveCallback();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetTrigEdgeHIGHIntEnable_Test)
{
        tU32 ret    = u32GPIOSetTrigEdgeHIGHIntEnable();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetTrigEdgeLOWIntEnable_Test)
{
        tU32 ret    = u32GPIOSetTrigEdgeLOWIntEnable();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateInactive_Test)
{
        tU32 ret    = u32GPIOSetStateInactive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateActive_Test)
{
        tU32 ret    = u32GPIOSetStateActive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOIsStateActive_Test)
{
        tU32 ret    = u32GPIOIsStateActive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputActive_Test)
{
        tU32 ret    = u32GPIOSetOutputActive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputInactive_Test)
{
        tU32 ret    = u32GPIOSetOutputInactive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputActiveInactive_Test)
{
        tU32 ret    = u32GPIOSetOutputActiveInactive();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputInInputMode_Test)
{
        tU32 ret    = u32GPIOSetOutputInInputMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOInvalFuncParamToIOCtrl_Test)
{
        tU32 ret    = u32GPIOInvalFuncParamToIOCtrl();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOMultiThreadTest_Test)
{
        tU32 ret    = u32GPIOMultiThreadTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOTwoInputPins_Test)
{
        tU32 ret    = u32GPIOTwoInputPins();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOMultiProcessTest_Test)
{
        tU32 ret    = u32GPIOMultiProcessTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetInputRemote_Test)
{
        tU32 ret    = u32GPIOSetInputRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputRemote_Test)
{
        tU32 ret    = u32GPIOSetOutputRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetTrigEdgeHIGHIntEnableRemote_Test)
{
        tU32 ret    = u32GPIOSetTrigEdgeHIGHIntEnableRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetTrigEdgeLOWIntEnableRemote_Test)
{
        tU32 ret    = u32GPIOSetTrigEdgeLOWIntEnableRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateInactiveRemote_Test)
{
        tU32 ret    = u32GPIOSetStateInactiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateActiveRemote_Test)
{
        tU32 ret    = u32GPIOSetStateActiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOIsStateActiveRemote_Test)
{
        tU32 ret    = u32GPIOIsStateActiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputActiveRemote_Test)
{
        tU32 ret    = u32GPIOSetOutputActiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputInactiveRemote_Test)
{
        tU32 ret    = u32GPIOSetOutputInactiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputActiveInactiveRemote_Test)
{
        tU32 ret    = u32GPIOSetOutputActiveInactiveRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputInInputModeRemote_Test)
{
        tU32 ret    = u32GPIOSetOutputInInputModeRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOMultiThreadTestRemote_Test)
{
        tU32 ret    = u32GPIOMultiThreadTestRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOTwoInputPinsRemote_Test)
{
        tU32 ret    = u32GPIOTwoInputPinsRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOMultiProcessTestRemote_Test)
{
        tU32 ret    = u32GPIOMultiProcessTestRemote();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateLOW_Test)
{
        tU32 ret    = u32GPIOSetStateLOW();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetStateHIGH_Test)
{
        tU32 ret    = u32GPIOSetStateHIGH();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOGetState_Test)
{
        tU32 ret    = u32GPIOGetState();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputHIGH_Test)
{
        tU32 ret    = u32GPIOSetOutputHIGH();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputLOW_Test)
{
        tU32 ret    = u32GPIOSetOutputLOW();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOSetOutputHIGHLOW_Test)
{
        tU32 ret    = u32GPIOSetOutputHIGHLOW();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOCallbackTest_Test)
{
        tU32 ret    = u32GPIOCallbackTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOCallbackRegOutputMode_Test)
{
        tU32 ret    = u32GPIOCallbackRegOutputMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOCallbackRegUnReg_Test)
{
        tU32 ret    = u32GPIOCallbackRegUnReg();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOThreadCallbackReg_Test)
{
        tU32 ret    = u32GPIOThreadCallbackReg();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIO_OSAL_test_Test)
{
        tU32 ret    = u32GPIO_OSAL_test();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pGPIO_RegressionEntries, u32GPIOActiveStateTest_Test)
{
        tU32 ret    = u32GPIOActiveStateTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCMulOpenClose_Test)
{
        tU32 ret    = tu32Gen2RTCMulOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCMultipleRead_Test)
{
        tU32 ret    = tu32Gen2RTCMultipleRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCSetGpsTimeRegressionSet_Test)
{
        tU32 ret    = tu32Gen2RTCSetGpsTimeRegressionSet();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCSetGpsTimeIllegalRegressionSet_Test)
{
        tU32 ret    = tu32Gen2RTCSetGpsTimeIllegalRegressionSet();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCNonExistentTime_Test)
{
        tU32 ret    = tu32Gen2RTCNonExistentTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCReadVersion_Test)
{
        tU32 ret    = tu32Gen2RTCReadVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCReadVersionInval_Test)
{
        tU32 ret    = tu32Gen2RTCReadVersionInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCReadInval_Test)
{
        tU32 ret    = tu32Gen2RTCReadInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCReadWithoutOpen_Test)
{
        tU32 ret    = tu32Gen2RTCReadWithoutOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCReadExceedMax_Test)
{
        tU32 ret    = tu32Gen2RTCReadExceedMax();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCSetGPSTimeNoOpen_Test)
{
        tU32 ret    = tu32Gen2RTCSetGPSTimeNoOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCGpsTimeStateValid_Test)
{
        tU32 ret    = tu32Gen2RTCGpsTimeStateValid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, u32MultiThread_OpenClose_Test)
{
        tU32 ret    = u32MultiThread_OpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, u32MultiThread_SetGpsTime_Test)
{
        tU32 ret    = u32MultiThread_SetGpsTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, u32MultiThread_GetVersion_Test)
{
        tU32 ret    = u32MultiThread_GetVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, u32MultiThread_RtcIORead_Test)
{
        tU32 ret    = u32MultiThread_RtcIORead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRTC_RegressionEntries, u32MultiThread_MultiFunctionTest_Test)
{
        tU32 ret    = u32MultiThread_MultiFunctionTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pRTC_RegressionEntries, tu32Gen2RTCSetGpsTime_Test)
{
        tU32 ret    = tu32Gen2RTCSetGpsTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pRTC_RegressionEntries, u32RTCMultiProcessTest_Test)
{
        tU32 ret    = u32RTCMultiProcessTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T000_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T000();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T001_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T001();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T002_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T002();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T003_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T003();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T004_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T004();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T005_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T005();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T006_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T006();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICIN_Entries, OEDT_ACOUSTICIN_T007_Test)
{
        tU32 ret    = OEDT_ACOUSTICIN_T007();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICSRC_Entries, OEDT_ACOUSTICSRC_T000_Test)
{
        tU32 ret    = OEDT_ACOUSTICSRC_T000();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICSRC_Entries, OEDT_ACOUSTICSRC_T001_Test)
{
        tU32 ret    = OEDT_ACOUSTICSRC_T001();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T000_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T000();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T001_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T001();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T002_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T002();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T003_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T003();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T004_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T004();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T005_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T005();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T006_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T006();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T007_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T007();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T008_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T008();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T009_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T009();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T010_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T010();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pACOUSTICOUT_Entries, OEDT_ACOUSTICOUT_T011_Test)
{
        tU32 ret    = OEDT_ACOUSTICOUT_T011();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pAUDIOTEST_Entries, OEDT_AUDIO_LOOPBACK_T001_Test)
{
        tU32 ret    = OEDT_AUDIO_LOOPBACK_T001();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pAUDIOTEST_Entries, u32_oedt_audio_test_open_close_devs_Test)
{
        tU32 ret    = u32_oedt_audio_test_open_close_devs();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReOpendev_Test)
{
        tU32 ret    = u32FFS2_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32FFS2_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateDelDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateDelDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateDelSubDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateDelSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateDirInvalName_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateDirInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRmNonExstngDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSRmNonExstngDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRmDirUsingIOCTRL_Test)
{
        tU32 ret    = u32FFS2_CommonFSRmDirUsingIOCTRL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenDirDiffModes_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenDirDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSDirParallelAccess_Test)
{
        tU32 ret    = u32FFS2_CommonFSDirParallelAccess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateDirMultiTimes_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateDirMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateRemDirInvalPath_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateRemDirInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateRmNonEmptyDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateRmNonEmptyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCopyDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSCopyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSMultiCreateDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSMultiCreateDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateSubDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSDelInvDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSDelInvDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCopyDirRec_Test)
{
        tU32 ret    = u32FFS2_CommonFSCopyDirRec();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRemoveDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSRemoveDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateDel_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateDel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileOpenInvalParam_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileOpenInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileRead_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileWrite_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadAsync_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSLargeReadAsync_Test)
{
        tU32 ret    = u32FFS2_CommonFSLargeReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteAsync_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSLargeWriteAsync_Test)
{
        tU32 ret    = u32FFS2_CommonFSLargeWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteOddBuffer_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteOddBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteFileWithInvalidSize_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteFileWithInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteFileInvalidBuffer_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteFileInvalidBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteFileStepByStep_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteFileStepByStep();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetFreeSpace_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetFreeSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetTotalSpace_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetTotalSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileDelWithoutClose_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileDelWithoutClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32FFS2_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateFileMultiTimes_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateFileMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateUnicodeName_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateUnicodeName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateInvalName_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateLongName_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateLongName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateDiffModes_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileCreateInvalPath_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileCreateInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSStringSearch_Test)
{
        tU32 ret    = u32FFS2_CommonFSStringSearch();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadAccessCheck_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileWriteAccessCheck_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileWriteAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileReadSubDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileReadSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileWriteSubDir_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileWriteSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof1KbFile_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof1KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof10KbFile_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof10KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof100KbFile_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof100KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof1MBFile_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof1MBFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof1KbFilefor1000times_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof1KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof10KbFilefor1000times_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof10KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof100KbFilefor100times_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof100KbFilefor100times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteTimeMeasureof1MBFilefor16times_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteTimeMeasureof1MBFilefor16times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRenameFile_Test)
{
        tU32 ret    = u32FFS2_CommonFSRenameFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteFrmBOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteFrmEOF_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSLargeFileRead_Test)
{
        tU32 ret    = u32FFS2_CommonFSLargeFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSLargeFileWrite_Test)
{
        tU32 ret    = u32FFS2_CommonFSLargeFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenCloseMultiThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenCloseMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteMultiThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteReadMultiThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadMultiThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFormat_Test)
{
        tU32 ret    = u32FFS2_CommonFSFormat();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileGetExt_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileGetExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileGetExt2_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileGetExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSSaveNowIOCTRL_SyncWrite_Test)
{
        tU32 ret    = u32FFS2_CommonFSSaveNowIOCTRL_SyncWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSSaveNowIOCTRL_AsynWrite_Test)
{
        tU32 ret    = u32FFS2_CommonFSSaveNowIOCTRL_AsynWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileOpenCloseMultipleTime_Test)
{
        tU32 ret    = u32FFS2_CommonFileOpenCloseMultipleTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileOpenCloseMultipleTimeRandom1_Test)
{
        tU32 ret    = u32FFS2_CommonFileOpenCloseMultipleTimeRandom1();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileOpenCloseMultipleTimeRandom2_Test)
{
        tU32 ret    = u32FFS2_CommonFileOpenCloseMultipleTimeRandom2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileLongFileOpenCloseMultipleTime_Test)
{
        tU32 ret    = u32FFS2_CommonFileLongFileOpenCloseMultipleTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom1_Test)
{
        tU32 ret    = u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom1();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom2_Test)
{
        tU32 ret    = u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFileOpenCloseMultipleTimeMultDir_Test)
{
        tU32 ret    = u32FFS2_CommonFileOpenCloseMultipleTimeMultDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSGetFileSize_Test)
{
        tU32 ret    = u32FFS2_CommonFSGetFileSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadDirValidate_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadDirValidate();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRmRecursiveCancel_Test)
{
        tU32 ret    = u32FFS2_CommonFSRmRecursiveCancel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateManyThreadDeleteMain_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateManyThreadDeleteMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCreateManyDeleteInThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSCreateManyDeleteInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenManyCloseInThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenManyCloseInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenInThreadCloseMain_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenInThreadCloseMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteMainReadInThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteMainReadInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSWriteThreadReadInMain_Test)
{
        tU32 ret    = u32FFS2_CommonFSWriteThreadReadInMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSFileAccInDiffThread_Test)
{
        tU32 ret    = u32FFS2_CommonFSFileAccInDiffThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadDirExtPartByPart_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadDirExtPartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadDirExt2PartByPart_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadDirExt2PartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSCopyDirTest_Test)
{
        tU32 ret    = u32FFS2_CommonFSCopyDirTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSReadWriteHugeData_Test)
{
        tU32 ret    = u32FFS2_CommonFSReadWriteHugeData();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSRW_Performance_Diff_BlockSize_Test)
{
        tU32 ret    = u32FFS2_CommonFSRW_Performance_Diff_BlockSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pFFS2_CommonFS_RegressionEntries, u32FFS2_CommonFSOpenDirIOCTL_Test)
{
        tU32 ret    = u32FFS2_CommonFSOpenDirIOCTL();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReOpendev_Test)
{
        tU32 ret    = u32FFS3_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32FFS3_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateDelDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateDelDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateDelSubDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateDelSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateDirInvalName_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateDirInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRmNonExstngDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSRmNonExstngDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRmDirUsingIOCTRL_Test)
{
        tU32 ret    = u32FFS3_CommonFSRmDirUsingIOCTRL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenDirDiffModes_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenDirDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSDirParallelAccess_Test)
{
        tU32 ret    = u32FFS3_CommonFSDirParallelAccess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateDirMultiTimes_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateDirMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateRemDirInvalPath_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateRemDirInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateRmNonEmptyDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateRmNonEmptyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCopyDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSCopyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSMultiCreateDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSMultiCreateDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateSubDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSDelInvDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSDelInvDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCopyDirRec_Test)
{
        tU32 ret    = u32FFS3_CommonFSCopyDirRec();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRemoveDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSRemoveDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateDel_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateDel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileOpenInvalParam_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileOpenInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileRead_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileWrite_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadAsync_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSLargeReadAsync_Test)
{
        tU32 ret    = u32FFS3_CommonFSLargeReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteAsync_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSLargeWriteAsync_Test)
{
        tU32 ret    = u32FFS3_CommonFSLargeWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteOddBuffer_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteOddBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteFileWithInvalidSize_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteFileWithInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteFileInvalidBuffer_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteFileInvalidBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteFileStepByStep_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteFileStepByStep();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetFreeSpace_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetFreeSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetTotalSpace_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetTotalSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileDelWithoutClose_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileDelWithoutClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32FFS3_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateFileMultiTimes_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateFileMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateUnicodeName_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateUnicodeName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateInvalName_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateLongName_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateLongName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateDiffModes_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileCreateInvalPath_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileCreateInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSStringSearch_Test)
{
        tU32 ret    = u32FFS3_CommonFSStringSearch();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadAccessCheck_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileWriteAccessCheck_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileWriteAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileReadSubDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileReadSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileWriteSubDir_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileWriteSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRenameFile_Test)
{
        tU32 ret    = u32FFS3_CommonFSRenameFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteFrmBOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteFrmEOF_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSLargeFileRead_Test)
{
        tU32 ret    = u32FFS3_CommonFSLargeFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSLargeFileWrite_Test)
{
        tU32 ret    = u32FFS3_CommonFSLargeFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenCloseMultiThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenCloseMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteMultiThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteReadMultiThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadMultiThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFormat_Test)
{
        tU32 ret    = u32FFS3_CommonFSFormat();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileGetExt_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileGetExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileGetExt2_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileGetExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite_Test)
{
        tU32 ret    = u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite_Test)
{
        tU32 ret    = u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileOpenCloseMultipleTime_Test)
{
        tU32 ret    = u32FFS3_CommonFileOpenCloseMultipleTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileOpenCloseMultipleTimeRandom1_Test)
{
        tU32 ret    = u32FFS3_CommonFileOpenCloseMultipleTimeRandom1();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileOpenCloseMultipleTimeRandom2_Test)
{
        tU32 ret    = u32FFS3_CommonFileOpenCloseMultipleTimeRandom2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileLongFileOpenCloseMultipleTime_Test)
{
        tU32 ret    = u32FFS3_CommonFileLongFileOpenCloseMultipleTime();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1_Test)
{
        tU32 ret    = u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2_Test)
{
        tU32 ret    = u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFileOpenCloseMultipleTimeMultDir_Test)
{
        tU32 ret    = u32FFS3_CommonFileOpenCloseMultipleTimeMultDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSGetFileSize_Test)
{
        tU32 ret    = u32FFS3_CommonFSGetFileSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadDirValidate_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadDirValidate();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRmRecursiveCancel_Test)
{
        tU32 ret    = u32FFS3_CommonFSRmRecursiveCancel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateManyThreadDeleteMain_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateManyThreadDeleteMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCreateManyDeleteInThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSCreateManyDeleteInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenManyCloseInThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenManyCloseInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenInThreadCloseMain_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenInThreadCloseMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteMainReadInThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteMainReadInThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSWriteThreadReadInMain_Test)
{
        tU32 ret    = u32FFS3_CommonFSWriteThreadReadInMain();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSFileAccInDiffThread_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileAccInDiffThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadDirExtPartByPart_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadDirExtPartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadDirExt2PartByPart_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadDirExt2PartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSCopyDirTest_Test)
{
        tU32 ret    = u32FFS3_CommonFSCopyDirTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSReadWriteHugeData_Test)
{
        tU32 ret    = u32FFS3_CommonFSReadWriteHugeData();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSRW_Performance_Diff_BlockSize_Test)
{
        tU32 ret    = u32FFS3_CommonFSRW_Performance_Diff_BlockSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pFFS3_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenDirIOCTL_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenDirIOCTL();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pMULTIPROCESS_Entries, u32SpawnMultiProcesses_Test)
{
        tU32 ret    = u32SpawnMultiProcesses();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pPERFORMANCE_Entries, u32SpawnPerformanceProcess_Test)
{
        tU32 ret    = u32SpawnPerformanceProcess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pPLATFORM_Startup_Entries, u32StartupTimePlatform_Test)
{
        tU32 ret    = u32StartupTimePlatform();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32PRCreateThreadNameNULL_Test)
{
        tU32 ret    = u32PRCreateThreadNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateThreadNameTooLong_Test)
{
        tU32 ret    = u32CreateThreadNameTooLong();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateThreadWrongPriority_Test)
{
        tU32 ret    = u32CreateThreadWrongPriority();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateThreadLowStack_Test)
{
        tU32 ret    = u32CreateThreadLowStack();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateThreadEntryFuncNULL_Test)
{
        tU32 ret    = u32CreateThreadEntryFuncNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DeleteThreadNotExisting_Test)
{
        tU32 ret    = u32DeleteThreadNotExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DeleteThreadWithNegID_Test)
{
        tU32 ret    = u32DeleteThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateDeleteThreadValidParam_Test)
{
        tU32 ret    = u32CreateDeleteThreadValidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateTwoThreadSameName_Test)
{
        tU32 ret    = u32CreateTwoThreadSameName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ActivateThreadNonExisting_Test)
{
        tU32 ret    = u32ActivateThreadNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ActivateThreadWithNegID_Test)
{
        tU32 ret    = u32ActivateThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ActivateThreadTwice_Test)
{
        tU32 ret    = u32ActivateThreadTwice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SuspendThreadNonExisting_Test)
{
        tU32 ret    = u32SuspendThreadNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SuspendThreadWithNegID_Test)
{
        tU32 ret    = u32SuspendThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ResumeThreadNonExisting_Test)
{
        tU32 ret    = u32ResumeThreadNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ResumeThreadWithNegID_Test)
{
        tU32 ret    = u32ResumeThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ChgPriorityThreadNonExisting_Test)
{
        tU32 ret    = u32ChgPriorityThreadNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ChgPriorityThreadWithNegID_Test)
{
        tU32 ret    = u32ChgPriorityThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ChgPriorityThreadWithWrongVal_Test)
{
        tU32 ret    = u32ChgPriorityThreadWithWrongVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ReadTCBThreadNonExisting_Test)
{
        tU32 ret    = u32ReadTCBThreadNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ReadTCBThreadWithNegID_Test)
{
        tU32 ret    = u32ReadTCBThreadWithNegID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MaxThreadCreate_Test)
{
        tU32 ret    = u32MaxThreadCreate();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ThreadWhoAmI_Test)
{
        tU32 ret    = u32ThreadWhoAmI();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ThreadListWithListNULL_Test)
{
        tU32 ret    = u32ThreadListWithListNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ThreadListWithListSizeNULL_Test)
{
        tU32 ret    = u32ThreadListWithListSizeNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ThreadListWithValidParam_Test)
{
        tU32 ret    = u32ThreadListWithValidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ProcessSpawnInvalidParam_Test)
{
        tU32 ret    = u32ProcessSpawnInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ProcessWhoAmI_Test)
{
        tU32 ret    = u32ProcessWhoAmI();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ProcessSelfDelete_Test)
{
        tU32 ret    = u32ProcessSelfDelete();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ProcessListValid_Test)
{
        tU32 ret    = u32ProcessListValid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ProcessListInvalidSize_Test)
{
        tU32 ret    = u32ProcessListInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetPCBValidParam_Test)
{
        tU32 ret    = u32GetPCBValidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32QueryPCBInvalidID_Test)
{
        tU32 ret    = u32QueryPCBInvalidID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32QueryPCBInvalidAddress_Test)
{
        tU32 ret    = u32QueryPCBInvalidAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateCloseDelSemVal_Test)
{
        tU32 ret    = u32CreateCloseDelSemVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateSemNameNULL_Test)
{
        tU32 ret    = u32CreateSemNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateSemHandleNULL_Test)
{
        tU32 ret    = u32CreateSemHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateCloseDelSemNameMAX_Test)
{
        tU32 ret    = u32CreateCloseDelSemNameMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateSemWithNameExceedsMAX_Test)
{
        tU32 ret    = u32CreateSemWithNameExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateTwoSemSameName_Test)
{
        tU32 ret    = u32CreateTwoSemSameName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelSemWithNULL_Test)
{
        tU32 ret    = u32DelSemWithNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelSemWithNonExistingName_Test)
{
        tU32 ret    = u32DelSemWithNonExistingName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelSemCurrentlyUsed_Test)
{
        tU32 ret    = u32DelSemCurrentlyUsed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenSemWithNameNULL_Test)
{
        tU32 ret    = u32OpenSemWithNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenSemWithHandleNULL_Test)
{
        tU32 ret    = u32OpenSemWithHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenSemWithNameExceedsMAX_Test)
{
        tU32 ret    = u32OpenSemWithNameExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenSemWithNonExistingName_Test)
{
        tU32 ret    = u32OpenSemWithNonExistingName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CloseSemWithInvalidHandle_Test)
{
        tU32 ret    = u32CloseSemWithInvalidHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitPostSemValid_Test)
{
        tU32 ret    = u32WaitPostSemValid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitSemWithInvalidHandle_Test)
{
        tU32 ret    = u32WaitSemWithInvalidHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ReleaseSemWithInvalidHandle_Test)
{
        tU32 ret    = u32ReleaseSemWithInvalidHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetRegularSemValue_Test)
{
        tU32 ret    = u32GetRegularSemValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetSemValueInvalidHandle_Test)
{
        tU32 ret    = u32GetSemValueInvalidHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitPostSemValidExceedsMAX_Test)
{
        tU32 ret    = u32WaitPostSemValidExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32AccessSemMpleThreads_Test)
{
        tU32 ret    = u32AccessSemMpleThreads();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32NoBlockingSemWait_Test)
{
        tU32 ret    = u32NoBlockingSemWait();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32BlockingDurationSemWait_Test)
{
        tU32 ret    = u32BlockingDurationSemWait();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitPostSemAfterDel_Test)
{
        tU32 ret    = u32WaitPostSemAfterDel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateEventNameNULL_Test)
{
        tU32 ret    = u32CreateEventNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateEventHandleNULL_Test)
{
        tU32 ret    = u32CreateEventHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateEventNameMAX_Test)
{
        tU32 ret    = u32CreateEventNameMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateEventNameExceedsMAX_Test)
{
        tU32 ret    = u32CreateEventNameExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateTwoEventsSameName_Test)
{
        tU32 ret    = u32CreateTwoEventsSameName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateEventRegularName_Test)
{
        tU32 ret    = u32CreateEventRegularName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelEventNULL_Test)
{
        tU32 ret    = u32DelEventNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelEventNonExisting_Test)
{
        tU32 ret    = u32DelEventNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelEventNameExceedsMAX_Test)
{
        tU32 ret    = u32DelEventNameExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelEventInUse_Test)
{
        tU32 ret    = u32DelEventInUse();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenEventExisting_Test)
{
        tU32 ret    = u32OpenEventExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenEventNameNULL_Test)
{
        tU32 ret    = u32OpenEventNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenEventHandleNULL_Test)
{
        tU32 ret    = u32OpenEventHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenEventNonExisting_Test)
{
        tU32 ret    = u32OpenEventNonExisting();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32OpenEventNameExceedsMAX_Test)
{
        tU32 ret    = u32OpenEventNameExceedsMAX();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CloseEventHandleNULL_Test)
{
        tU32 ret    = u32CloseEventHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CloseEventHandleInval_Test)
{
        tU32 ret    = u32CloseEventHandleInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetEventStatusHandleValid_Test)
{
        tU32 ret    = u32GetEventStatusHandleValid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetEventStatusHandleNULL_Test)
{
        tU32 ret    = u32GetEventStatusHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetEventStatusHandleInval_Test)
{
        tU32 ret    = u32GetEventStatusHandleInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32PostEventHandleNULL_Test)
{
        tU32 ret    = u32PostEventHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32PostEventHandleInval_Test)
{
        tU32 ret    = u32PostEventHandleInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitAndPostEventVal_Test)
{
        tU32 ret    = u32WaitAndPostEventVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitEventHandleNULL_Test)
{
        tU32 ret    = u32WaitEventHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitEventHandleInval_Test)
{
        tU32 ret    = u32WaitEventHandleInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitEventWithoutTimeout_Test)
{
        tU32 ret    = u32WaitEventWithoutTimeout();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitEventWithTimeout_Test)
{
        tU32 ret    = u32WaitEventWithTimeout();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SyncEventMultipleThreads_Test)
{
        tU32 ret    = u32SyncEventMultipleThreads();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32XORFlagPostEvent_Test)
{
        tU32 ret    = u32XORFlagPostEvent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ReplaceFlagPostEvent_Test)
{
        tU32 ret    = u32ReplaceFlagPostEvent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ORFlagWaitEvent_Test)
{
        tU32 ret    = u32ORFlagWaitEvent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32NoBlockingEventWait_Test)
{
        tU32 ret    = u32NoBlockingEventWait();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32BlockingDurationEventWait_Test)
{
        tU32 ret    = u32BlockingDurationEventWait();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32WaitPostEventAfterDelete_Test)
{
        tU32 ret    = u32WaitPostEventAfterDelete();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32EventDeletewithoutclose_Test)
{
        tU32 ret    = u32EventDeletewithoutclose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateTimerWithCallbackNULL_Test)
{
        tU32 ret    = u32CreateTimerWithCallbackNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelTimerNonExistent_Test)
{
        tU32 ret    = u32DelTimerNonExistent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32DelTimerWithHandleNULL_Test)
{
        tU32 ret    = u32DelTimerWithHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateDelTimer_Test)
{
        tU32 ret    = u32CreateDelTimer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SetTimeTimerStTimeOrIntToNULL_Test)
{
        tU32 ret    = u32SetTimeTimerStTimeOrIntToNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SetTimeTimerNonExistent_Test)
{
        tU32 ret    = u32SetTimeTimerNonExistent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetTimeTimerNonExistent_Test)
{
        tU32 ret    = u32GetTimeTimerNonExistent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetTimeTimerWithDiffParam_Test)
{
        tU32 ret    = u32GetTimeTimerWithDiffParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SetResetTwoTimers_Test)
{
        tU32 ret    = u32SetResetTwoTimers();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetElaspedTimeFromRef_Test)
{
        tU32 ret    = u32GetElaspedTimeFromRef();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SetOsalTimerInvalStruct_Test)
{
        tU32 ret    = u32SetOsalTimerInvalStruct();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SetOsalTimerGetOsalTimer_Test)
{
        tU32 ret    = u32SetOsalTimerGetOsalTimer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32GetOsalTimerNULL_Test)
{
        tU32 ret    = u32GetOsalTimerNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CyclePhaseTimeCheck_Test)
{
        tU32 ret    = u32CyclePhaseTimeCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32ChangeSystemTimeTest_Test)
{
        tU32 ret    = u32ChangeSystemTimeTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, U32MaxCallHandleCheck_Test)
{
        tU32 ret    = U32MaxCallHandleCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32TimerPerformanceST_Counter_Test)
{
        tU32 ret    = u32TimerPerformanceST_Counter();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32TimerPerformanceMT_Counter_Test)
{
        tU32 ret    = u32TimerPerformanceMT_Counter();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32TimerPerformanceMT_Time_Test)
{
        tU32 ret    = u32TimerPerformanceMT_Time();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateDeleteNameNULL_Test)
{
        tU32 ret    = u32SHCreateDeleteNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateNameVal_Test)
{
        tU32 ret    = u32SHCreateNameVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateDeleteNameExceedNameLength_Test)
{
        tU32 ret    = u32SHCreateDeleteNameExceedNameLength();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateNameSameNames_Test)
{
        tU32 ret    = u32SHCreateNameSameNames();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateSizeZero_Test)
{
        tU32 ret    = u32SHCreateSizeZero();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHDeleteNameInval_Test)
{
        tU32 ret    = u32SHDeleteNameInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHDeleteNameInUse_Test)
{
        tU32 ret    = u32SHDeleteNameInUse();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHOpenCloseDiffModes_Test)
{
        tU32 ret    = u32SHOpenCloseDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHOpenNameNULL_Test)
{
        tU32 ret    = u32SHOpenNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHOpenNameInVal_Test)
{
        tU32 ret    = u32SHOpenNameInVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCloseNameInVal_Test)
{
        tU32 ret    = u32SHCloseNameInVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateMaxSegment_Test)
{
        tU32 ret    = u32SHCreateMaxSegment();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHMemoryMapNonExistentHandle_Test)
{
        tU32 ret    = u32SHMemoryMapNonExistentHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHMemoryMapLimitCheck_Test)
{
        tU32 ret    = u32SHMemoryMapLimitCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHMemoryMapDiffAccessModes_Test)
{
        tU32 ret    = u32SHMemoryMapDiffAccessModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHMemoryThreadAccess_Test)
{
        tU32 ret    = u32SHMemoryThreadAccess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHMemoryScanMemory_Test)
{
        tU32 ret    = u32SHMemoryScanMemory();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32UnmapNonMappedAddress_Test)
{
        tU32 ret    = u32UnmapNonMappedAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32UnmapNULLAddress_Test)
{
        tU32 ret    = u32UnmapNULLAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32UnmapMappedAddress_Test)
{
        tU32 ret    = u32UnmapMappedAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHOpenDoubleCloseOnce_Test)
{
        tU32 ret    = u32SHOpenDoubleCloseOnce();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHCreateNameValLoop_Test)
{
        tU32 ret    = u32SHCreateNameValLoop();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLOpen_Test)
{
        tU32 ret    = u32MSGPLOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLCreateMessageNULL_Test)
{
        tU32 ret    = u32MSGPLCreateMessageNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLCreateSizeZero_Test)
{
        tU32 ret    = u32MSGPLCreateSizeZero();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLCreateLocationInVal_Test)
{
        tU32 ret    = u32MSGPLCreateLocationInVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLGetSMSize_Test)
{
        tU32 ret    = u32MSGPLGetSMSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLGetSMContent_Test)
{
        tU32 ret    = u32MSGPLGetSMContent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLGetSMContentDiffMode_Test)
{
        tU32 ret    = u32MSGPLGetSMContentDiffMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLGetLMSize_Test)
{
        tU32 ret    = u32MSGPLGetLMSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLGetLMContentDiffMode_Test)
{
        tU32 ret    = u32MSGPLGetLMContentDiffMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLDeleteMessageNULL_Test)
{
        tU32 ret    = u32MSGPLDeleteMessageNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLMsgCreateWithNoMSGPLInSH_Test)
{
        tU32 ret    = u32MSGPLMsgCreateWithNoMSGPLInSH();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MSGPLMsgCreateWithNoMSGPLInLM_Test)
{
        tU32 ret    = u32MSGPLMsgCreateWithNoMSGPLInLM();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQuePostMsgExcMaxLen_Test)
{
        tU32 ret    = u32MsgQuePostMsgExcMaxLen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CreateMsgQueWithHandleNULL_Test)
{
        tU32 ret    = u32CreateMsgQueWithHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueCreateMsgExcMaxLen_Test)
{
        tU32 ret    = u32MsgQueCreateMsgExcMaxLen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueCreateDiffModes_Test)
{
        tU32 ret    = u32MsgQueCreateDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueCreateSameName_Test)
{
        tU32 ret    = u32MsgQueCreateSameName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueDeleteNameNULL_Test)
{
        tU32 ret    = u32MsgQueDeleteNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueDeleteNameInval_Test)
{
        tU32 ret    = u32MsgQueDeleteNameInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueDeleteWithoutClose_Test)
{
        tU32 ret    = u32MsgQueDeleteWithoutClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueOpenDiffMode_Test)
{
        tU32 ret    = u32MsgQueOpenDiffMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueTwoThreadMsgPost_Test)
{
        tU32 ret    = u32MsgQueTwoThreadMsgPost();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueSingleThreadMsgNotify_Test)
{
        tU32 ret    = u32MsgQueSingleThreadMsgNotify();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueSingleThreadMsgNotifyCallback_Test)
{
        tU32 ret    = u32MsgQueSingleThreadMsgNotifyCallback();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueThreadMsgNotify_Test)
{
        tU32 ret    = u32MsgQueThreadMsgNotify();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueNotifyEvent_Test)
{
        tU32 ret    = u32MsgQueNotifyEvent();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueQueryStatus_Test)
{
        tU32 ret    = u32MsgQueQueryStatus();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueQueryStatusMMParamNULL_Test)
{
        tU32 ret    = u32MsgQueQueryStatusMMParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueQueryStatusMLParamNULL_Test)
{
        tU32 ret    = u32MsgQueQueryStatusMLParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueQueryStatusCMParamNULL_Test)
{
        tU32 ret    = u32MsgQueQueryStatusCMParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueQueryStatusAllParamNULL_Test)
{
        tU32 ret    = u32MsgQueQueryStatusAllParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQuePostMsgInvalPrio_Test)
{
        tU32 ret    = u32MsgQuePostMsgInvalPrio();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQuePostMsgBeyondQueLimit_Test)
{
        tU32 ret    = u32MsgQuePostMsgBeyondQueLimit();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, MsgQueSimplePostWaitPerfTest_Test)
{
        tU32 ret    = MsgQueSimplePostWaitPerfTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, MsgQueThreadPostWaitPerfTest_Test)
{
        tU32 ret    = MsgQueThreadPostWaitPerfTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQOpenDoubleCloseOnce_Test)
{
        tU32 ret    = u32MQOpenDoubleCloseOnce();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueNotifyRemove_Test)
{
        tU32 ret    = u32MsgQueNotifyRemove();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MsgQueTimeoutTest_Test)
{
        tU32 ret    = u32MsgQueTimeoutTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

#ifndef LSIM
//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPZeroDivNoAbort_Test)
{
        tU32 ret    = u32CheckFPZeroDivNoAbort();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPOverflowNoAbort_Test)
{
        tU32 ret    = u32CheckFPOverflowNoAbort();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPUnderflowNoAbort_Test)
{
        tU32 ret    = u32CheckFPUnderflowNoAbort();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPInvalidNoAbort_Test)
{
        tU32 ret    = u32CheckFPInvalidNoAbort();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckResetFPEFlag_Test)
{
        tU32 ret    = u32CheckResetFPEFlag();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckResetMultithreads_Test)
{
        tU32 ret    = u32CheckResetMultithreads();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckMultipleFPEFlags_Test)
{
        tU32 ret    = u32CheckMultipleFPEFlags();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPEBitSetMultithreaded_Test)
{
        tU32 ret    = u32CheckFPEBitSetMultithreaded();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPEModeChange_Test)
{
        tU32 ret    = u32CheckFPEModeChange();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32CheckFPEModeMultithreaded_Test)
{
        tU32 ret    = u32CheckFPEModeMultithreaded();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#endif

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateDeleteNameNULL_Test)
{
        tU32 ret    = u32SHnoIOSCCreateDeleteNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateNameVal_Test)
{
        tU32 ret    = u32SHnoIOSCCreateNameVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateDeleteNameExceedNameLength_Test)
{
        tU32 ret    = u32SHnoIOSCCreateDeleteNameExceedNameLength();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateNameSameNames_Test)
{
        tU32 ret    = u32SHnoIOSCCreateNameSameNames();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateSizeZero_Test)
{
        tU32 ret    = u32SHnoIOSCCreateSizeZero();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCDeleteNameInval_Test)
{
        tU32 ret    = u32SHnoIOSCDeleteNameInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCDeleteNameInUse_Test)
{
        tU32 ret    = u32SHnoIOSCDeleteNameInUse();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCOpenCloseDiffModes_Test)
{
        tU32 ret    = u32SHnoIOSCOpenCloseDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCOpenNameNULL_Test)
{
        tU32 ret    = u32SHnoIOSCOpenNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCOpenNameInVal_Test)
{
        tU32 ret    = u32SHnoIOSCOpenNameInVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCloseNameInVal_Test)
{
        tU32 ret    = u32SHnoIOSCCloseNameInVal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateMaxSegment_Test)
{
        tU32 ret    = u32SHnoIOSCCreateMaxSegment();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCMemoryMapNonExistentHandle_Test)
{
        tU32 ret    = u32SHnoIOSCMemoryMapNonExistentHandle();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCMemoryMapLimitCheck_Test)
{
        tU32 ret    = u32SHnoIOSCMemoryMapLimitCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCMemoryMapDiffAccessModes_Test)
{
        tU32 ret    = u32SHnoIOSCMemoryMapDiffAccessModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCMemoryThreadAccess_Test)
{
        tU32 ret    = u32SHnoIOSCMemoryThreadAccess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCMemoryScanMemory_Test)
{
        tU32 ret    = u32SHnoIOSCMemoryScanMemory();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCUnmapNonMappedAddress_Test)
{
        tU32 ret    = u32SHnoIOSCUnmapNonMappedAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCUnmapNULLAddress_Test)
{
        tU32 ret    = u32SHnoIOSCUnmapNULLAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCUnmapMappedAddress_Test)
{
        tU32 ret    = u32SHnoIOSCUnmapMappedAddress();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCOpenDoubleCloseOnce_Test)
{
        tU32 ret    = u32SHnoIOSCOpenDoubleCloseOnce();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32SHnoIOSCCreateNameValLoop_Test)
{
        tU32 ret    = u32SHnoIOSCCreateNameValLoop();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCPostMsgExcMaxLen_Test)
{
        tU32 ret    = u32MQnoIOSCPostMsgExcMaxLen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCCreateMsgQueWithHandleNULL_Test)
{
        tU32 ret    = u32MQnoIOSCCreateMsgQueWithHandleNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCCreateMsgExcMaxLen_Test)
{
        tU32 ret    = u32MQnoIOSCCreateMsgExcMaxLen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCCreateDiffModes_Test)
{
        tU32 ret    = u32MQnoIOSCCreateDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCCreateSameName_Test)
{
        tU32 ret    = u32MQnoIOSCCreateSameName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCDeleteNameNULL_Test)
{
        tU32 ret    = u32MQnoIOSCDeleteNameNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCDeleteNameInval_Test)
{
        tU32 ret    = u32MQnoIOSCDeleteNameInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCDeleteWithoutClose_Test)
{
        tU32 ret    = u32MQnoIOSCDeleteWithoutClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCOpenDiffMode_Test)
{
        tU32 ret    = u32MQnoIOSCOpenDiffMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCTwoThreadMsgPost_Test)
{
        tU32 ret    = u32MQnoIOSCTwoThreadMsgPost();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCThreadMsgNotify_Test)
{
        tU32 ret    = u32MQnoIOSCThreadMsgNotify();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCQueryStatus_Test)
{
        tU32 ret    = u32MQnoIOSCQueryStatus();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCQueryStatusMMParamNULL_Test)
{
        tU32 ret    = u32MQnoIOSCQueryStatusMMParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCQueryStatusMLParamNULL_Test)
{
        tU32 ret    = u32MQnoIOSCQueryStatusMLParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCQueryStatusCMParamNULL_Test)
{
        tU32 ret    = u32MQnoIOSCQueryStatusCMParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCQueryStatusAllParamNULL_Test)
{
        tU32 ret    = u32MQnoIOSCQueryStatusAllParamNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCPostMsgInvalPrio_Test)
{
        tU32 ret    = u32MQnoIOSCPostMsgInvalPrio();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCPostMsgBeyondQueLimit_Test)
{
        tU32 ret    = u32MQnoIOSCPostMsgBeyondQueLimit();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCSimplePostWaitPerfTest_Test)
{
        tU32 ret    = u32MQnoIOSCSimplePostWaitPerfTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCThreadPostWaitPerfTest_Test)
{
        tU32 ret    = u32MQnoIOSCThreadPostWaitPerfTest();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCOpenDoubleCloseOnce_Test)
{
        tU32 ret    = u32MQnoIOSCOpenDoubleCloseOnce();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCTimeoutCheck_Test)
{
        tU32 ret    = u32MQnoIOSCTimeoutCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pOSALCORERegressionEntries, u32MQnoIOSCPostWaitMsgPriowise_Test)
{
        tU32 ret    = u32MQnoIOSCPostWaitMsgPriowise();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32REGISTRYBasic1Test_Test)
{
        tU32 ret    = u32REGISTRYBasic1Test();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32REGISTRYBasic2Test_Test)
{
        tU32 ret    = u32REGISTRYBasic2Test();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32REGISTRYBasic3Test_Test)
{
        tU32 ret    = u32REGISTRYBasic3Test();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDevOpenClose_Test)
{
        tU32 ret    = u32RegDevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDevOpenCloseDiffModes_Test)
{
        tU32 ret    = u32RegDevOpenCloseDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDevOpenCloseInvalParam_Test)
{
        tU32 ret    = u32RegDevOpenCloseInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegCreateKey_Test)
{
        tU32 ret    = u32RegCreateKey();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegOpenCloseKey_Test)
{
        tU32 ret    = u32RegOpenCloseKey();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegGetVersion_Test)
{
        tU32 ret    = u32RegGetVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegSetGetValue_Test)
{
        tU32 ret    = u32RegSetGetValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegWriteValue_Test)
{
        tU32 ret    = u32RegWriteValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegSetQueryValue_Test)
{
        tU32 ret    = u32RegSetQueryValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegRemoveValue_Test)
{
        tU32 ret    = u32RegRemoveValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDevReOpen_Test)
{
        tU32 ret    = u32RegDevReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDevCloseAlreadyClosed_Test)
{
        tU32 ret    = u32RegDevCloseAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegCreateMultipleValues_Test)
{
        tU32 ret    = u32RegCreateMultipleValues();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegCreateMultipleKeys_Test)
{
        tU32 ret    = u32RegCreateMultipleKeys();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegReOpenKey_Test)
{
        tU32 ret    = u32RegReOpenKey();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegCloseKeyAlreadyClosed_Test)
{
        tU32 ret    = u32RegCloseKeyAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDeleteKeyWithValues_Test)
{
        tU32 ret    = u32RegDeleteKeyWithValues();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegOpenKeyDiffAccessModes_Test)
{
        tU32 ret    = u32RegOpenKeyDiffAccessModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegWriteNumericValue_Test)
{
        tU32 ret    = u32RegWriteNumericValue();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDelKeyAlreadyRemoved_Test)
{
        tU32 ret    = u32RegDelKeyAlreadyRemoved();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegDelKeyStructure_Test)
{
        tU32 ret    = u32RegDelKeyStructure();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegSearchKey_Test)
{
        tU32 ret    = u32RegSearchKey();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegOpenAsDir_Test)
{
        tU32 ret    = u32RegOpenAsDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegBlockCreateAndRemove_Test)
{
        tU32 ret    = u32RegBlockCreateAndRemove();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pREGISTRY_RegressionEntries, u32RegUseInvalidKey_Test)
{
        tU32 ret    = u32RegUseInvalidKey();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevOpenClose_Test)
{
        tU32 ret    = u32DiagEOLDevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevReOpen_Test)
{
        tU32 ret    = u32DiagEOLDevReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevCloseAlreadyClosed_Test)
{
        tU32 ret    = u32DiagEOLDevCloseAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevCloseInvalParam_Test)
{
        tU32 ret    = u32DiagEOLDevCloseInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevOpenCloseDiffAccessMode_Test)
{
        tU32 ret    = u32DiagEOLDevOpenCloseDiffAccessMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLDevOpenCloseInvalAccessMode_Test)
{
        tU32 ret    = u32DiagEOLDevOpenCloseInvalAccessMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMReadOneByte_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMReadOneByte();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMReadInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMReadInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMReadWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMReadWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMReadBlockInvalidSize_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMReadBlockInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMWriteWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMWriteWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLSYSTEMWriteInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLSYSTEMWriteInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYReadOneByte_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYReadOneByte();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYReadInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYReadInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYReadWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYReadWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYReadBlockInvalidSize_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYReadBlockInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYWriteWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYWriteWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLCOUNTRYWriteInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLCOUNTRYWriteInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDReadOneByte_Test)
{
        tU32 ret    = u32DiagEOLBRANDReadOneByte();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDReadInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLBRANDReadInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDReadWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLBRANDReadWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDReadBlockInvalidSize_Test)
{
        tU32 ret    = u32DiagEOLBRANDReadBlockInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDWriteWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLBRANDWriteWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLBRANDWriteInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLBRANDWriteInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCReadOneByte_Test)
{
        tU32 ret    = u32DiagEOLRVCReadOneByte();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCReadInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLRVCReadInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCReadWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLRVCReadWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCReadBlockInvalidSize_Test)
{
        tU32 ret    = u32DiagEOLRVCReadBlockInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCWriteWholeBlock_Test)
{
        tU32 ret    = u32DiagEOLRVCWriteWholeBlock();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLRVCWriteInvalBuffer_Test)
{
        tU32 ret    = u32DiagEOLRVCWriteInvalBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLGetModuleIdentifier_Test)
{
        tU32 ret    = u32DiagEOLGetModuleIdentifier();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pEOLLIB_RegressionEntries, u32DiagEOLGetModuleIdentifierInvalParam_Test)
{
        tU32 ret    = u32DiagEOLGetModuleIdentifierInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#ifdef LSIM

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDevOpen_Test)
{
        tU32 ret    = u32SteeringWheelDevOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDev_MultiOpen_Test)
{
        tU32 ret    = u32SteeringWheelDev_MultiOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDevclose_Test)
{
        tU32 ret    = u32SteeringWheelDevclose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDev_Multiclose_Test)
{
        tU32 ret    = u32SteeringWheelDev_Multiclose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDevIOControl_GetVersion_Test)
{
        tU32 ret    = u32SteeringWheelDevIOControl_GetVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pSWC_Entries_reg, u32SteeringWheelDevIOControl_SetCallBack_Test)
{
        tU32 ret    = u32SteeringWheelDevIOControl_SetCallBack();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#endif

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevOpenClose_Test)
{
        tU32 ret    = u32FFDDevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevOpenWithInvalidParam_Test)
{
        tU32 ret    = u32FFDDevOpenWithInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevCloseWithInvalidParam_Test)
{
        tU32 ret    = u32FFDDevCloseWithInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevOpenCloseSeveralTimes_Test)
{
        tU32 ret    = u32FFDDevOpenCloseSeveralTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevWriteRead_Test)
{
        tU32 ret    = u32FFDDevWriteRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevWriteReadInvalidParam_Test)
{
        tU32 ret    = u32FFDDevWriteReadInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRawSize_Test)
{
        tU32 ret    = u32FFDDevGetRawSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRawSizeInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetRawSizeInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRawData_Test)
{
        tU32 ret    = u32FFDDevGetRawData();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRawDataInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetRawDataInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSetRawData_Test)
{
        tU32 ret    = u32FFDDevSetRawData();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSetRawDataInvalidParam_Test)
{
        tU32 ret    = u32FFDDevSetRawDataInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveNow_Test)
{
        tU32 ret    = u32FFDDevSaveNow();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveNowInvalidParam_Test)
{
        tU32 ret    = u32FFDDevSaveNowInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevReload_Test)
{
        tU32 ret    = u32FFDDevReload();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevReloadInvalidParam_Test)
{
        tU32 ret    = u32FFDDevReloadInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveReloadCompare_Test)
{
        tU32 ret    = u32FFDDevSaveReloadCompare();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSeek_Test)
{
        tU32 ret    = u32FFDDevSeek();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSeekInvalidParam_Test)
{
        tU32 ret    = u32FFDDevSeekInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevWriteReadWithSeek_Test)
{
        tU32 ret    = u32FFDDevWriteReadWithSeek();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRomDataVersion_Test)
{
        tU32 ret    = u32FFDDevGetRomDataVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetRomDataVersionInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetRomDataVersionInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetTotolFlashSize_Test)
{
        tU32 ret    = u32FFDDevGetTotolFlashSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetTotolFlashSizeInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetTotolFlashSizeInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetDataSetSize_Test)
{
        tU32 ret    = u32FFDDevGetDataSetSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetDataSetSizeInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetDataSetSizeInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveCompleteFlashArea_Test)
{
        tU32 ret    = u32FFDDevSaveCompleteFlashArea();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevReadSaveAssertMode_Test)
{
        tU32 ret    = u32FFDDevReadSaveAssertMode();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveBackupFile_Test)
{
        tU32 ret    = u32FFDDevSaveBackupFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevSaveBackupFileInvalidParam_Test)
{
        tU32 ret    = u32FFDDevSaveBackupFileInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevLoadBackupFile_Test)
{
        tU32 ret    = u32FFDDevLoadBackupFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevLoadBackupFileInvalidParam_Test)
{
        tU32 ret    = u32FFDDevLoadBackupFileInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetInfoReadData_Test)
{
        tU32 ret    = u32FFDDevGetInfoReadData();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevGetInfoReadDataInvalidParam_Test)
{
        tU32 ret    = u32FFDDevGetInfoReadDataInvalidParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

#ifdef LSIM
//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevLSIMWriteBeforeReboot_Test)
{
        tU32 ret    = u32FFDDevLSIMWriteBeforeReboot();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFFD_DEV_RegressionEntries, u32FFDDevLSIMReadAfterReboot_Test)
{
        tU32 ret    = u32FFDDevLSIMReadAfterReboot();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#endif

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReOpendev_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateDelDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateDelDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateDelSubDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateDelSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateDirInvalName_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateDirInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSRmNonExstngDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSRmNonExstngDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSRmDirUsingIOCTRL_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSRmDirUsingIOCTRL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpenDirDiffModes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpenDirDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSDirParallelAccess_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSDirParallelAccess();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateDirMultiTimes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateDirMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateRemDirInvalPath_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateRemDirInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateRmNonEmptyDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateRmNonEmptyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCopyDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCopyDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSMultiCreateDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSMultiCreateDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateSubDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSDelInvDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSDelInvDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCopyDirRec_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCopyDirRec();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSRemoveDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSRemoveDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileCreateDel_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileCreateDel();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileOpenInvalParam_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileOpenInvalParam();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileRead_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileWrite_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadAsync_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSLargeReadAsync_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSLargeReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteAsync_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSLargeWriteAsync_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSLargeWriteAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteOddBuffer_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteOddBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteFileWithInvalidSize_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteFileWithInvalidSize();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteFileInvalidBuffer_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteFileInvalidBuffer();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteFileStepByStep_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteFileStepByStep();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetFreeSpace_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetFreeSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSGetTotalSpace_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSGetTotalSpace();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileDelWithoutClose_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileDelWithoutClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSCreateFileMultiTimes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSCreateFileMultiTimes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileCreateUnicodeName_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileCreateUnicodeName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileCreateInvalName_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileCreateInvalName();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileCreateDiffModes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileCreateDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileCreateInvalPath_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileCreateInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSStringSearch_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSStringSearch();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadAccessCheck_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileWriteAccessCheck_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileWriteAccessCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileReadSubDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileReadSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSFileWriteSubDir_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSFileWriteSubDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSRenameFile_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSRenameFile();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteFrmBOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteFrmEOF_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSLargeFileRead_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSLargeFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSLargeFileWrite_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSLargeFileWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSOpenCloseMultiThread_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSOpenCloseMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteMultiThread_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSWriteReadMultiThread_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSWriteReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32RAMDISK_CommonFSReadMultiThread_Test)
{
        tU32 ret    = u32RAMDISK_CommonFSReadMultiThread();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32FFS3_CommonFSFileGetExt_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileGetExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32FFS3_CommonFSFileGetExt2_Test)
{
        tU32 ret    = u32FFS3_CommonFSFileGetExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pRAMDISK_CommonFS_RegressionEntries, u32FFS3_CommonFSOpenDirIOCTL_Test)
{
        tU32 ret    = u32FFS3_CommonFSOpenDirIOCTL();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSReOpendev_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileRead_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSReadAsync_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSReadDirExt_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSReadDirExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_CommonFS_Entries, u32Cryptcard_CommonFSReadDirExt2_Test)
{
        tU32 ret    = u32Cryptcard_CommonFSReadDirExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSReOpendev_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileRead_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSReadAsync_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSReadDirExt_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSReadDirExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard2_CommonFS_Entries, u32Cryptcard2_CommonFSReadDirExt2_Test)
{
        tU32 ret    = u32Cryptcard2_CommonFSReadDirExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSReOpendev_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileRead_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSReadAsync_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSReadDirExt_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSReadDirExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptcard_SD_CommonFS_Entries, u32Cryptcard_SD_CommonFSReadDirExt2_Test)
{
        tU32 ret    = u32Cryptcard_SD_CommonFSReadDirExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#ifdef OEDT_CRYPTNAV_FS_TEST_ENABLE

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSOpenClosedevice_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSOpenClosedevice();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSOpendevInvalParm_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSOpendevInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReOpendev_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReOpendev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSOpendevDiffModes_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSOpendevDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSClosedevAlreadyClosed_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSClosedevAlreadyClosed();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSOpenClosedir_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSOpenClosedir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSOpendirInvalid_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSOpendirInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSGetDirInfo_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSGetDirInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReOpenDir_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReOpenDir();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileOpenClose_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileOpenInvalPath_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileOpenInvalPath();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReOpen_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileRead_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileRead();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSGetPosFrmBOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSGetPosFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSGetPosFrmEOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSGetPosFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadNegOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadNegOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadOffsetBeyondEOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadOffsetBeyondEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadOffsetFrmBOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadOffsetFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadOffsetFrmEOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadOffsetFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadEveryNthByteFrmBOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadEveryNthByteFrmBOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileReadEveryNthByteFrmEOF_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileReadEveryNthByteFrmEOF();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSGetFileCRC_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSGetFileCRC();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReadAsync_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReadAsync();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileOpenCloseNonExstng_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileOpenCloseNonExstng();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSSetFilePosDiffOff_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSSetFilePosDiffOff();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSFileOpenDiffModes_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSFileOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReadDirExt_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReadDirExt();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReadDirExt2_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReadDirExt2();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReadDirExtPartByPart_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReadDirExtPartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pCryptnav_CommonFS_Entries, u32Cryptnav_CommonFSReadDirExt2PartByPart_Test)
{
        tU32 ret    = u32Cryptnav_CommonFSReadDirExt2PartByPart();
        EXPECT_EQ(ret, RET_SUCCESS);
}
#endif

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigureSignalNULL_Test)
{
        tU32 ret    = u32ConfigureSignalNULL();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigureSignalInvalid_Test)
{
        tU32 ret    = u32ConfigureSignalInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigureSignalDoesntExist_Test)
{
        tU32 ret    = u32ConfigureSignalDoesntExist();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32GetStatusInvalid_Test)
{
        tU32 ret    = u32GetStatusInvalid();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32GetInvalidSignalStatus_Test)
{
        tU32 ret    = u32GetInvalidSignalStatus();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32EnableTwiceCheck_Test)
{
        tU32 ret    = u32EnableTwiceCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32DisableTwiceCheck_Test)
{
        tU32 ret    = u32DisableTwiceCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigureSignalCheckWithStatus_Test)
{
        tU32 ret    = u32ConfigureSignalCheckWithStatus();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigureCheck_Test)
{
        tU32 ret    = u32ConfigureCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigurTwiceDisableCheck_Test)
{
        tU32 ret    = u32ConfigurTwiceDisableCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32ConfigurSignalThreadsCheck_Test)
{
        tU32 ret    = u32ConfigurSignalThreadsCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_SignalHndlr_RegressionEntries, u32RandomConfigurSignal_Test)
{
        tU32 ret    = u32RandomConfigurSignal();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pAUXCLOCK_DEV_RegressionEntries, tu32AuxclockOpenCloseDev_Test)
{
        tU32 ret    = tu32AuxclockOpenCloseDev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pAUXCLOCK_DEV_RegressionEntries, tu32AuxclockCheckTimeStampValiditySeq_Test)
{
        tU32 ret    = tu32AuxclockCheckTimeStampValiditySeq();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_pAUXCLOCK_DEV_RegressionEntries, tu32AuxclockPosixTimeCheck_Test)
{
        tU32 ret    = tu32AuxclockPosixTimeCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevOpen_Test)
{
        tU32 ret    = u32KDSDevOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevReOpen_Test)
{
        tU32 ret    = u32KDSDevReOpen();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevOpenDiffModes_Test)
{
        tU32 ret    = u32KDSDevOpenDiffModes();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevClose_Test)
{
        tU32 ret    = u32KDSDevClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevCloseInvalParm_Test)
{
        tU32 ret    = u32KDSDevCloseInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevReClose_Test)
{
        tU32 ret    = u32KDSDevReClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSEnaDisAccessright_Test)
{
        tU32 ret    = u32KDSEnaDisAccessright();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSEnaDisAccessrightWithInvalParm_Test)
{
        tU32 ret    = u32KDSEnaDisAccessrightWithInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetNextEntryIDWithInvalParm_Test)
{
        tU32 ret    = u32KDSGetNextEntryIDWithInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetDevInfo_Test)
{
        tU32 ret    = u32KDSGetDevInfo();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetDevInfoWithInvalParm_Test)
{
        tU32 ret    = u32KDSGetDevInfoWithInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSDevInit_Test)
{
        tU32 ret    = u32KDSDevInit();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSReadEntryWithInvalID_Test)
{
        tU32 ret    = u32KDSReadEntryWithInvalID();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSReadEntryWithInvalParm_Test)
{
        tU32 ret    = u32KDSReadEntryWithInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetDevVersion_Test)
{
        tU32 ret    = u32KDSGetDevVersion();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetDevVersionWithInvalParm_Test)
{
        tU32 ret    = u32KDSGetDevVersionWithInvalParm();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetDevVersionAfterDevclose_Test)
{
        tU32 ret    = u32KDSGetDevVersionAfterDevclose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pKDS_Startup_Entries, u32KDSGetRemainingSizeInval_Test)
{
        tU32 ret    = u32KDSGetRemainingSizeInval();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pERRMEM_Startup_Entries, u32ErrMemDevOpenClose_Test)
{
        tU32 ret    = u32ErrMemDevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pERRMEM_Startup_Entries, u32ErrMemWrite_Test)
{
        tU32 ret    = u32ErrMemWrite();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pTRACE_Startup_Entries, u32TraceDevOpenClose_Test)
{
        tU32 ret    = u32TraceDevOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pPRM_Startup_Entries, u32PRM_DrvOpenClose_Test)
{
        tU32 ret    = u32PRM_DrvOpenClose();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pPRM_Startup_Entries, u32PRM_GetStatus_Test)
{
        tU32 ret    = u32PRM_GetStatus();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pPRM_Startup_Entries, u32PRM_CallbackHandler_Test)
{
        tU32 ret    = u32PRM_CallbackHandler();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pHOST_Startup_Entries, u32HostOpenDev_Test)
{
        tU32 ret    = u32HostOpenDev();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pFSDEV_RegressionEntries, u32fs_dev_check_system_devices_Test)
{
        tU32 ret    = u32fs_dev_check_system_devices();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pRPC_RegressionEntries, u32RPCConnTester_Test)
{
        tU32 ret    = u32RPCConnTester();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
TEST(oedt_pMODULES_RegressionEntries, u32ModuleLoadCheck_Test)
{
        tU32 ret    = u32ModuleLoadCheck();
        EXPECT_EQ(ret, RET_SUCCESS);
}

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vCreDelMQSequence1_Test)
{
        tU32 ret    = vCreDelMQSequence1();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vCreDelMQSequence2_Test)
{
        tU32 ret    = vCreDelMQSequence2();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vCreDelMQSequence3_Test)
{
        tU32 ret    = vCreDelMQSequence3();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vCreDelMQSequence4_Test)
{
        tU32 ret    = vCreDelMQSequence4();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vPostRecSequence1_Test)
{
        tU32 ret    = vPostRecSequence1();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/

//Googletest Wrapper
/*TEST(oedt_OsalIOSCMQ_RegressionEntries, vPostRecSequence2_Test)
{
        tU32 ret    = vPostRecSequence2();
        EXPECT_EQ(ret, RET_SUCCESS);
}*/
#ifdef OEDT_CRYPTNAV_FS_TEST_ENABLE
#endif
#ifdef LSIM
#endif
#ifdef LSIM
#endif
