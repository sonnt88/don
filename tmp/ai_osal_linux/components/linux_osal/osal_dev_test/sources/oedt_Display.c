/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Display.c
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Output functions for OEDT.
 *
 * NOTES: Ported for ADIT platform Gen2.
 *      -
 * History:
 *           : Apr 22, 2009, 
             - Removed lint and compiler warnings by Sainath Kalpuri (RBEI/ECF1)
 *           : Feb 19, 2015,
 *          - Removed lint and compiler warnings by Deepak Kumar (RBEI/ECF5)
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Macros.h"

#include "oedt_Display.h"
extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...);
tU32 u32Step = 0;

/*********************************************************************FunctionHeaderBegin***
* oedt_vSend2TTFis()
* sends a message to the configured output device
**********************************************************************FunctionHeaderEnd***/
void oedt_vSend2TTFis(tPCS8 ps8Msg, tU32 u32Len)
{
  OSAL_trIOWriteTrace rMsg2Trace;
  OSAL_tIODescriptor  fdTrace;

  rMsg2Trace.u32Class    = TR_COMP_OSALTEST;
  rMsg2Trace.u32Level    =(tU32) TR_LEVEL_FATAL;
  rMsg2Trace.u32Length   = u32Len;
  rMsg2Trace.pcos8Buffer = ps8Msg;

  fdTrace = OSAL_IOOpen(_OEDT_TRACE_DEVICE, _OEDT_TRACE_PERMISSION);

  if(fdTrace != OSAL_ERROR)
  {
    OSAL_s32IOWrite(fdTrace, (tPCS8)&rMsg2Trace, sizeof(rMsg2Trace));
    OSAL_s32IOClose(fdTrace);
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_u32Check4Zero()
* check 4 zero termination, if there's no one return the length
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_u32Check4Zero(const tU8* pBuf, tU32 u32Len)
{
  tU32 i;

  for(i = 0; i < u32Len; ++i)
  {
    if(pBuf[i] == 0) return i;
  }

  return u32Len;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendLineMsg()
* sends a line
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendLineMsg(tU8 u8ID)
{
  tS8 ps8Msg[4];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_LINE;

  ps8Msg[3] = (tS8)u8ID;

  oedt_vSend2TTFis(ps8Msg, 4);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendTableEntry()
* sends table line content msg
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendTableEntry(tU32 u32Idx, tU32 u32ClassBeg, tU32 u32ClassEnd, tU32 u32TestBeg, tU32 u32TestEnd)
{
  tS8 ps8Msg[23];

  ps8Msg[0]  = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1]  = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2]  = OEDT_RESPONSEMSG_TABLEENTRY;

  ps8Msg[3]  = (tS8)LOBYTE(LOWORD(u32Idx));
  ps8Msg[4]  = (tS8)HIBYTE(LOWORD(u32Idx));
  ps8Msg[5]  = (tS8)LOBYTE(HIWORD(u32Idx));
  ps8Msg[6]  = (tS8)HIBYTE(HIWORD(u32Idx));

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(u32ClassBeg));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(u32ClassBeg));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(u32ClassBeg));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(u32ClassBeg));

  ps8Msg[11] = (tS8)LOBYTE(LOWORD(u32ClassEnd));
  ps8Msg[12] = (tS8)HIBYTE(LOWORD(u32ClassEnd));
  ps8Msg[13] = (tS8)LOBYTE(HIWORD(u32ClassEnd));
  ps8Msg[14] = (tS8)HIBYTE(HIWORD(u32ClassEnd));

  ps8Msg[15] = (tS8)LOBYTE(LOWORD(u32TestBeg));
  ps8Msg[16] = (tS8)HIBYTE(LOWORD(u32TestBeg));
  ps8Msg[17] = (tS8)LOBYTE(HIWORD(u32TestBeg));
  ps8Msg[18] = (tS8)HIBYTE(HIWORD(u32TestBeg));

  ps8Msg[19] = (tS8)LOBYTE(LOWORD(u32TestEnd));
  ps8Msg[20] = (tS8)HIBYTE(LOWORD(u32TestEnd));
  ps8Msg[21] = (tS8)LOBYTE(HIWORD(u32TestEnd));
  ps8Msg[22] = (tS8)HIBYTE(HIWORD(u32TestEnd));
  
  oedt_vSend2TTFis(ps8Msg, 23);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendTestMsg()
* sends test start msg
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendTestMsg(const OEDT_TEST_INFO* pTestInfo)
{
  tS8 ps8Msg[_OEDT_TESTNAME_LEN + 15];

  ps8Msg[0]  = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1]  = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2]  = OEDT_RESPONSEMSG_TESTSTART;

  ps8Msg[3]  = (tS8)LOBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[4]  = (tS8)HIBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[5]  = (tS8)LOBYTE(HIWORD(pTestInfo->u32TestID));
  ps8Msg[6]  = (tS8)HIBYTE(HIWORD(pTestInfo->u32TestID));

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(pTestInfo->u32ClassID));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(pTestInfo->u32ClassID));

  ps8Msg[11] = (tS8)LOBYTE(LOWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[12] = (tS8)HIBYTE(LOWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[13] = (tS8)LOBYTE(HIWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[14] = (tS8)HIBYTE(HIWORD(pTestInfo->u32TimeOutVal));
  
  char cBuffer[256];
  
  memset(ps8Msg + 15, 0x20, 32);
  OSAL_pvMemoryCopy(ps8Msg + 15, pTestInfo->pu8TestName, oedt_u32Check4Zero(pTestInfo->pu8TestName, _OEDT_TESTNAME_LEN));

#ifdef SHORT_TRACE_OUTPUT
   oedt_vSend2TTFis(ps8Msg, _OEDT_TESTNAME_LEN + 15);
#else
  TraceString("OEDT:   %s %u.%u %u",
	          pTestInfo->pu8TestName,pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->u32TimeOutVal);
#endif

//snprintf(cBuffer,256,"INFO: %s [%03u.%03u] [max %04u] %s\n",pTestInfo->pu8ClassName,pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->u32TimeOutVal,pTestInfo->pu8TestName);
  snprintf(cBuffer,256,"<gen3flex@dlt>(core0)INFO: %s %03u.%03u \"%-32.32s\"   [max %04us]\n","PERFORMING TEST   ",pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->pu8TestName,pTestInfo->u32TimeOutVal);
  
  write(1,cBuffer,strlen(cBuffer));

}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendResultMsg()
* sends test result msg
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendResultMsg(const OEDT_TEST_INFO* pTestInfo)
{
  tS8 ps8Msg[_OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN + 25];

  ps8Msg[0]  = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1]  = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2]  = OEDT_RESPONSEMSG_TESTRESULT;

  ps8Msg[3]  = (tS8)LOBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[4]  = (tS8)HIBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[5]  = (tS8)LOBYTE(HIWORD(pTestInfo->u32TestID));
  ps8Msg[6]  = (tS8)HIBYTE(HIWORD(pTestInfo->u32TestID));

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(pTestInfo->u32ErrCode));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(pTestInfo->u32ErrCode));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(pTestInfo->u32ErrCode));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(pTestInfo->u32ErrCode));

  ps8Msg[11] = (tS8)LOBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[12] = (tS8)HIBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[13] = (tS8)LOBYTE(HIWORD(pTestInfo->u32ClassID));
  ps8Msg[14] = (tS8)HIBYTE(HIWORD(pTestInfo->u32ClassID));

  ps8Msg[15] = (tS8)pTestInfo->u8TimedOut;
  ps8Msg[16] = (tS8)LOBYTE(LOWORD(pTestInfo->s32TestConsumedMemory));
  ps8Msg[17] = (tS8)HIBYTE(LOWORD(pTestInfo->s32TestConsumedMemory));
  ps8Msg[18] = (tS8)LOBYTE(HIWORD(pTestInfo->s32TestConsumedMemory));
  ps8Msg[19] = (tS8)HIBYTE(HIWORD(pTestInfo->s32TestConsumedMemory));

  ps8Msg[20] = (tS8)LOBYTE(LOWORD(pTestInfo->u32FreeMemory));
  ps8Msg[21] = (tS8)HIBYTE(LOWORD(pTestInfo->u32FreeMemory));
  ps8Msg[22] = (tS8)LOBYTE(HIWORD(pTestInfo->u32FreeMemory));
  ps8Msg[23] = (tS8)HIBYTE(HIWORD(pTestInfo->u32FreeMemory));
  ps8Msg[24] = (tS8)pTestInfo->u8Eq2Stored;

  char cBuffer[256];
  
  memset(ps8Msg + 25, 0x20, 48);

  OSAL_pvMemoryCopy(ps8Msg + 25, pTestInfo->pu8ClassName, oedt_u32Check4Zero(pTestInfo->pu8ClassName, _OEDT_CLASSNAME_LEN));
  OSAL_pvMemoryCopy(ps8Msg + 41, pTestInfo->pu8TestName, oedt_u32Check4Zero(pTestInfo->pu8TestName, _OEDT_TESTNAME_LEN));

  // Changed by rec2hi, this leads to corruted log files
  // ps8Msg[64] = (tS8)pTestInfo->u8Eq2Stored;
#ifdef  SHORT_TRACE_OUTPUT 
  oedt_vSend2TTFis(ps8Msg, _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN + 25);
#else
  if(pTestInfo->u32ErrCode)
  {
	  TraceString("OEDT FAILED (ERR: %d)(Consumed Memory:%d Free:%u)",
		          pTestInfo->u32ErrCode,pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
  }
  else
  {
	  if(pTestInfo->u8TimedOut)
	  {
		   TraceString("OEDT FAILED (TIMED OUT) (Consumed Memory:%4d(22,4,I,1) Free:%4)",
			           pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
	  }
	  else
	  {
		  TraceString("OEDT PASSED (Consumed Memory:%d Free:%u",
			           pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
	  }
  }

#endif

  if(pTestInfo->u32ErrCode)
  {
	  snprintf(cBuffer,256,"\n<gen3flex@dlt>(core0)OEDT:   %-16.16s %03u.%03u \"%-32.32s\" - FAILED (ERR: %d) (Consumed Memory: %4d Free: %u)\n",pTestInfo->pu8ClassName,
		          pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->pu8TestName,pTestInfo->u32ErrCode,pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
  }
  else
  {
	  if(pTestInfo->u8TimedOut)
	  {
		   snprintf(cBuffer,256,"\n<gen3flex@dlt>(core0)OEDT:   %-16.16s %03u.%03u \"%-32.32s\" - FAILED (TIMED OUT) (Consumed Memory: %4d Free: %u)\n",pTestInfo->pu8ClassName,
			           pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->pu8TestName,pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
	  }
	  else
	  {
		  snprintf(cBuffer,256,"\n<gen3flex@dlt>(core0)OEDT:   %-16.16s %03u.%03u \"%-32.32s\" - PASSED (Consumed Memory:%4d Free: %u)\n",pTestInfo->pu8ClassName,
			           pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->pu8TestName,pTestInfo->s32TestConsumedMemory,pTestInfo->u32FreeMemory);
	  }
  }

  write(1,cBuffer,strlen(cBuffer));
  
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendExtMsg()
* sends user-(test-)given info string 
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendExtMsg(tPCS8 ps8ExtMsg, tU32 u32Len)
{
  tS8* ps8Msg = (tS8*)OSAL_pvMemoryAllocate(u32Len + 3);

  if (ps8Msg != NULL)
  {
	ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
	ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
	ps8Msg[2] = OEDT_RESPONSEMSG_EXTINFO;

	memset(ps8Msg + 3, 0x20, u32Len);
	OSAL_pvMemoryCopy(ps8Msg + 3, ps8ExtMsg, u32Len);

	oedt_vSend2TTFis(ps8Msg, u32Len + 3);

	OSAL_vMemoryFree(ps8Msg);
  }
  else
  {
    OEDT_HelperPrintf((tU8) TR_LEVEL_FATAL, "oedt_vSendExtMsg: memory allocation failed\n" );
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendEnduranceStatsMsg()
* sends endurance statistics line
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendEnduranceStatsMsg(tU32 u32Class, tU32 u32Test,const OEDT_TESTER_STATS* pStats, tPCS8 ps8ClassName, tPCS8 ps8TestName)
{
  tU32 u32Tmp;
  tS8  ps8Msg[23 + _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN];

  ps8Msg[0]  = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1]  = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2]  = OEDT_RESPONSEMSG_ENDURSTATS;

  ps8Msg[3]  = (tS8)LOBYTE(LOWORD(u32Class));
  ps8Msg[4]  = (tS8)HIBYTE(LOWORD(u32Class));
  ps8Msg[5]  = (tS8)LOBYTE(HIWORD(u32Class));
  ps8Msg[6]  = (tS8)HIBYTE(HIWORD(u32Class));

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(u32Test));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(u32Test));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(u32Test));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(u32Test));

  ps8Msg[11]  = (tS8)LOBYTE(LOWORD(pStats->u32Passed));
  ps8Msg[12]  = (tS8)HIBYTE(LOWORD(pStats->u32Passed));
  ps8Msg[13]  = (tS8)LOBYTE(HIWORD(pStats->u32Passed));
  ps8Msg[14]  = (tS8)HIBYTE(HIWORD(pStats->u32Passed));

  u32Tmp = pStats->u32Failed + pStats->u32TimeOut;

  ps8Msg[15]  = (tS8)LOBYTE(LOWORD(u32Tmp));
  ps8Msg[16]  = (tS8)HIBYTE(LOWORD(u32Tmp));
  ps8Msg[17]  = (tS8)LOBYTE(HIWORD(u32Tmp));
  ps8Msg[18]  = (tS8)HIBYTE(HIWORD(u32Tmp));

  ps8Msg[19]  = (tS8)LOBYTE(LOWORD(pStats->u32TimeOut));
  ps8Msg[20]  = (tS8)HIBYTE(LOWORD(pStats->u32TimeOut));
  ps8Msg[21]  = (tS8)LOBYTE(HIWORD(pStats->u32TimeOut));
  ps8Msg[22]  = (tS8)HIBYTE(HIWORD(pStats->u32TimeOut));
  
  char cBuffer[256];
  
  snprintf(cBuffer,256,"OEDT: [%03u.%03u] [max %04u]\n",u32Class,u32Test,pStats->u32TimeOut);
  write(1,cBuffer,strlen(cBuffer));
  

  memset(ps8Msg + 23, 0x20, _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN);
  OSAL_pvMemoryCopy(ps8Msg + 23, ps8ClassName, oedt_u32Check4Zero((tU8*)ps8ClassName, _OEDT_CLASSNAME_LEN));
  OSAL_pvMemoryCopy(ps8Msg + 39, ps8TestName, oedt_u32Check4Zero((tU8*)ps8TestName, _OEDT_TESTNAME_LEN));

  oedt_vSend2TTFis(ps8Msg, 23 + _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendOverallStatsMsg()
* sends overall statistics info
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendOverallStatsMsg(tU32 u32Passed, tU32 u32Failed, tU32 u32TimedOut)
{
  tS8 ps8Msg[15];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_OVERALLSTATS;

  ps8Msg[3] = (tS8)LOBYTE(LOWORD(u32Passed));
  ps8Msg[4] = (tS8)HIBYTE(LOWORD(u32Passed));
  ps8Msg[5] = (tS8)LOBYTE(HIWORD(u32Passed));
  ps8Msg[6] = (tS8)HIBYTE(HIWORD(u32Passed));

  u32Failed += u32TimedOut;

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(u32Failed));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(u32Failed));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(u32Failed));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(u32Failed));

  ps8Msg[11] = (tS8)LOBYTE(LOWORD(u32TimedOut));
  ps8Msg[12] = (tS8)HIBYTE(LOWORD(u32TimedOut));
  ps8Msg[13] = (tS8)LOBYTE(HIWORD(u32TimedOut));
  ps8Msg[14] = (tS8)HIBYTE(HIWORD(u32TimedOut));

  char cBuffer[256];
  snprintf(cBuffer,256,"\n<gen3flex@dlt>(core0)OEDT: -==TEST STATISTICS==-  PASSED: %d  FAILED: %d  (TIMED OUT: %d) \n",u32Passed,u32Failed,u32TimedOut);
  write(1,cBuffer,strlen(cBuffer));
  
#ifdef  SHORT_TRACE_OUTPUT 
  u32Step++;
  oedt_vSend2TTFis(ps8Msg, 15);
#else
  TraceString("OEDT: -==TEST STATISTICS==-  PASSED: %d  FAILED: %d  (TIMED OUT: %d)",
              u32Passed,u32Failed,u32TimedOut);
  u32Step++;
void ShutdownMain(void);
  if(u32Step == 6)ShutdownMain();
#endif
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendMemLeakMsg()
* sends 'blocked memory' info line
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendMemLeakMsg(tU32 u32PreVal, tU32 u32PostVal)
{
  tS8  ps8Msg[7];
  tU32 u32Leak;

  u32Leak = u32PreVal - u32PostVal;

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_MEMLEAK;
  ps8Msg[3] = (tS8)LOBYTE(LOWORD(u32Leak));
  ps8Msg[4] = (tS8)HIBYTE(LOWORD(u32Leak));
  ps8Msg[5] = (tS8)LOBYTE(HIWORD(u32Leak));
  ps8Msg[6] = (tS8)HIBYTE(HIWORD(u32Leak));

  oedt_vSend2TTFis(ps8Msg, 7);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendDatasetMsg()
* sends dataset info line
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendDatasetMsg(tU32 u32DatasetID, tPCS8 ps8Dataset)
{
  tS8 ps8Msg[_OEDT_DATASETNAME_LEN + 7];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_DATASET;
  ps8Msg[3] = (tS8)LOBYTE(LOWORD(u32DatasetID));
  ps8Msg[4] = (tS8)HIBYTE(LOWORD(u32DatasetID));
  ps8Msg[5] = (tS8)LOBYTE(HIWORD(u32DatasetID));
  ps8Msg[6] = (tS8)HIBYTE(HIWORD(u32DatasetID));

  OSAL_pvMemoryCopy(ps8Msg + 7, ps8Dataset, _OEDT_DATASETNAME_LEN);
  oedt_vSend2TTFis(ps8Msg, _OEDT_DATASETNAME_LEN + 7);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendConfigMsg()
* sends internal config info line
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendConfigMsg(OEDT_FLOW_MODE_ID eModeFlow, tU8 u8ModeSpec)
{
  tS8 ps8Msg[5];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_CONFIG;
  
  ps8Msg[3] = (tS8)eModeFlow;
  ps8Msg[4] = (tS8)u8ModeSpec;
  
  oedt_vSend2TTFis(ps8Msg, 5);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendTestCfgMsg()
* sends test configuration details
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendTestCfgMsg(const OEDT_TEST_INFO* pTestInfo)
{
  tS8 ps8Msg[_OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN + 15];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_TESTENTRY;

  ps8Msg[3] = (tS8)LOBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[4] = (tS8)HIBYTE(LOWORD(pTestInfo->u32ClassID));
  ps8Msg[5] = (tS8)LOBYTE(HIWORD(pTestInfo->u32ClassID));
  ps8Msg[6] = (tS8)HIBYTE(HIWORD(pTestInfo->u32ClassID));

  ps8Msg[7]  = (tS8)LOBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[8]  = (tS8)HIBYTE(LOWORD(pTestInfo->u32TestID));
  ps8Msg[9]  = (tS8)LOBYTE(HIWORD(pTestInfo->u32TestID));
  ps8Msg[10] = (tS8)HIBYTE(HIWORD(pTestInfo->u32TestID));

  ps8Msg[11] = (tS8)LOBYTE(LOWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[12] = (tS8)HIBYTE(LOWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[13] = (tS8)LOBYTE(HIWORD(pTestInfo->u32TimeOutVal));
  ps8Msg[14] = (tS8)HIBYTE(HIWORD(pTestInfo->u32TimeOutVal));
  
  char cBuffer[256];
  
  snprintf(cBuffer,256,"<gen3flex@dlt>(core0)INFO: \"%-15.15s\" [%03u.%03u] \"%-32.32s\" TIMEOUT: %04us \n",pTestInfo->pu8ClassName,pTestInfo->u32ClassID,pTestInfo->u32TestID,pTestInfo->pu8TestName,pTestInfo->u32TimeOutVal);
  write(1,cBuffer,strlen(cBuffer));

  memset(ps8Msg + 15, 0x20, _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN);

  OSAL_pvMemoryCopy(ps8Msg + 15, pTestInfo->pu8ClassName, oedt_u32Check4Zero(pTestInfo->pu8ClassName, _OEDT_CLASSNAME_LEN));
  OSAL_pvMemoryCopy(ps8Msg + 31, pTestInfo->pu8TestName, oedt_u32Check4Zero(pTestInfo->pu8TestName, _OEDT_TESTNAME_LEN));

  oedt_vSend2TTFis(ps8Msg, _OEDT_CLASSNAME_LEN + _OEDT_TESTNAME_LEN + 15);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendConfirmMsg()
* sends confirmation msg
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendConfirmMsg(tBool fDone)
{
  tS8 ps8Msg[4];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = OEDT_RESPONSEMSG_CONFIRM;

  if(fDone == TRUE)
    ps8Msg[3] = 0x00;
  else
    ps8Msg[3] = 0x01;

  oedt_vSend2TTFis(ps8Msg, 4);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendHelpMsg()
* sends help response
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendHelpMsg(void)
{
  tS8 ps8Msg[3];

  ps8Msg[0] = _OEDT_TTFIS_ODTDEV_ID;
  ps8Msg[1] = _OEDT_TTFIS_RESPONSE_ID;
  ps8Msg[2] = (tS8)OEDT_RESPONSEMSG_HELP;

  oedt_vSend2TTFis(ps8Msg, 3);
}

