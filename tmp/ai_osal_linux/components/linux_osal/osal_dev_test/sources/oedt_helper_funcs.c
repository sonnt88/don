/******************************************************************************
 * Copyright (C) Blaupunkt GmbH, [year]
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ************************************************************************/

/************************************************************************/
/*! \file  oedt_helper_funcs.c
 *\brief    oedt help functions

 *\author		CM-DI/PJ-GM32 - Resch

 *\par Copyright: 
 *(c) *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH

 *\par History: 05.03.08 Lars Tracht, Brunel
                           added vPrintOutTime to avoid implementation in each TestCase
                           13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
                           22-Apr-2009  | Lint warnings and info removed | Sainath Kalpuri(RBEI/ECF1)
                           20-Mar-2015  | Lint warnings and info removed | Deepak Kumar(RBEI/ECF5)
 **********************************************************************/


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "oedt_Configuration.h"
#include "oedt_helper_funcs.h"
#include "OsalConf.h"
#ifdef ADIT_TRACE_ACTIVE
#include "adit-components/trace_interface.h"
#define TRACE_S_ALREADY_INCLUDE_TYPES
#endif

/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
#define OEDT_HELPER_MAX_TRACE_STR_SIZE                       ((tInt) 256)
//#define USBDEBUG_CONSOLE_TRACE /*Enable for USBPOWER Testing*/
/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/
/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/
 /********************************************************************/
 
 
/*****************************************************************************
* FUNCTION:		u32OEDT_OpenCloseDevice()
* PARAMETER:    DeviceName, expected return value of an OSAL_IOOpen() access
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Opens and closes the specified SPI device.
* HISTORY:		Created by Martin Langer (CM-AI/PJ-CF33) on  2011-09-14
*               
******************************************************************************/
tU32 u32OEDT_OpenCloseDevice(tCString DevName, tU32 u32ExpectedRetVal, OSAL_tenAccess enAccess)
{
	OSAL_tIODescriptor hFd = 0;
	tU32 u32Ret = 0;
	tU32 u32Status = 0;

	/* Device Open */
	hFd = OSAL_IOOpen(DevName, enAccess);
	if(OSAL_ERROR != hFd)
	{
		if (u32ExpectedRetVal != OSAL_OK)
		{
			u32Ret += 100;			
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"OSAL_IOOpen() return value '%i' using accesss rights '%i'", hFd, enAccess);	
		}

		/* Device Close */
		if (OSAL_ERROR == OSAL_s32IOClose( hFd ))
		{
			u32Status = OSAL_u32ErrorCode();
			u32Status &= ~OSAL_C_COMPONENT;
			u32Status &= ~OSAL_C_ERROR_TYPE;			
			u32Ret += 200 + u32Status;
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"OSAL_s32IOClose() return error code '%i' using accesss rights '%i'", u32Status, enAccess);	
		}
	}
	else
	{
		/* check the error status returned */
		u32Status = OSAL_u32ErrorCode();
		if (u32Status != u32ExpectedRetVal)
		{
			u32Status &= ~OSAL_C_COMPONENT;
			u32Status &= ~OSAL_C_ERROR_TYPE;			
			u32Ret += 400 + u32Status;
      OEDT_HelperPrintf(TR_LEVEL_USER_1,"OSAL_IOOpen() return error code '%i' using accesss rights '%i'", u32Status, enAccess);	
		}
	}
	return u32Ret;	
}

tU32 u32OEDT_BoardName()
{
	tU32 u32Result = 0;
	/* 
		buffer "oedt_boardname" will be update by function
		"oedt_vTestHandler()" form oedt_core.c
		Board name will get form drv_registry
		with entry "/dev/registry/LOCAL_MACHINE/SOFTWARE/
		BLAUPUNKT/VERSIONS/BOARDCFG"
	*/
	if ( OSAL_ps8StringSubString ( oedt_boardname, "GMGE" ) != 0 ) 
	{ 
		u32Result = OEDT_GMGE; 
	}

	if ( OSAL_ps8StringSubString ( oedt_boardname, "JLR HLDF" ) != 0 ) 
	{
		u32Result = OEDT_JLR; 
	}
	if ( OSAL_ps8StringSubString ( oedt_boardname, "Ford" ) != 0 ) 
	{ 
		u32Result = OEDT_FORD; 
	}
	if ( OSAL_ps8StringSubString ( oedt_boardname, "ICM" ) != 0 ) 
	{ 
		u32Result = OEDT_ICM; 
	}
	if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13" ) != 0 ) 
	{ 
		u32Result = OEDT_GM_MY13; 
	}
	if ( OSAL_ps8StringSubString ( oedt_boardname, "LCN2 KAI" ) != 0 ) 
	{ 
		u32Result = OEDT_LCN2KAI; 
	}
	return u32Result;
	
}

 
  /**
 
  *  FUNCTION:      tVoid vPrintOutTime(OSAL_tMSecond TimetakenToRead, OSAL_tMSecond TimetakenToWrite,
 *                                                                   tU64 u64AllBytesToRead, tU64 u64AllBytesToWrite )
  *
  *  @brief         Prints performance measurement data
  *
  *  @param        TimetakenToRead      time for read
  *  @param        TimetakenToWrite    time for write
  *  @param        u64AllBytesToRead   number of bytes that were read
  *  @param        u64AllBytesToWrite number of bytes that were written
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 05.03.08 Lars Tracht, Brunel
  *                      moved here to avoid implementation in each TestCase
  ************************************************************************/
tVoid vPrintOutTime(OSAL_tMSecond TimetakenToRead, OSAL_tMSecond TimetakenToWrite,
                    tU64 u64AllBytesToRead, tU64 u64AllBytesToWrite ) 
{
	tU32 speedOfWrite = 0;
	tU32 speedOfRead = 0;
	
	if (!TimetakenToRead) {
		speedOfRead = 0xffffffff;
	} else {
		speedOfRead = (tU32)(((tU64)(u64AllBytesToRead*(tU64)1000)/(tU64)TimetakenToRead)/(tU64)1024);
	}
	
	if (!TimetakenToWrite) {
		speedOfWrite = 0xffffffff;
	} else {
		speedOfWrite = (tU32)(((tU64)(u64AllBytesToWrite*(tU64)1000)/(tU64)TimetakenToWrite)/(tU64)1024);
	}

	OEDT_HelperPrintf(TR_LEVEL_FATAL,
					"Time taken: Read :%lu ms -> %lu kB/s , Write :%lu ms -> %lu kB/s ",
					TimetakenToRead, speedOfRead, TimetakenToWrite, speedOfWrite);
    return;
}



 /********************************************************************/ /**
  *  FUNCTION:      tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...){
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u8_trace_level     TTFIS trace level
  *  @param         tChar *pchFormat   VA Format string
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
  ************************************************************************/
tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...){
    
    tChar pcBuffer[OEDT_HELPER_MAX_TRACE_STR_SIZE+2];
    va_list argList = {0}; 
    tInt noChar = 0;
    va_start(argList, pchFormat);
    pcBuffer[0] = _OEDT_TTFIS_ODTDEV_ID;
    pcBuffer[1] = _OEDT_TTFIS_HELPER_TRACE_ID;
    
    noChar = vsnprintf(&pcBuffer[2], 255, pchFormat, argList);
    pcBuffer[OEDT_HELPER_MAX_TRACE_STR_SIZE-1] = '\0';  //last buffer entry
    pcBuffer[noChar+2] = '\0'; //end of the real msg
    // TTFIS tracing
#ifdef ADIT_TRACE_ACTIVE
	TR_core_uwTraceOut((tU32)noChar+3,TR_COMP_OSALTEST,(TR_tenTraceLevel)u8_trace_level,(tU8*)pcBuffer);
#else
 void TraceString(const char* cBuffer,...);
    TraceString(&pcBuffer[2]);
#endif

#ifdef USBDEBUG_CONSOLE_TRACE
	fprintf(stderr,"\n",0);
	fprintf(stderr,pcBuffer);
#endif

	va_end( argList );
    return;
}
/*! @}*/
