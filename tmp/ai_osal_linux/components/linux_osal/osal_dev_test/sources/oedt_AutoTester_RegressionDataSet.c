#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Types.h"
#include "oedt_AutoTester_RegressionDataSet.h"
#include "oedt_Acousticin_TestFuncs.h" 
#include "oedt_Acousticout_TestFuncs.h"

/*Osal Core Test Function defines*/

#include "oedt_osalcore_TestFuncs.h"
#include "oedt_FFS2_CommonFS_TestFuncs.h"
#include "oedt_FFS3_CommonFS_TestFuncs.h"
#include "oedt_Registry_TestFuncs.h"
#include "oedt_DiagEOL_TestFuncs.h"
#include "oedt_Cryptcard_CommonFS_TestFuncs.h"
#include "oedt_Cryptcard2_CommonFS_TestFuncs.h"
#include "oedt_Cryptcard_SD_CommonFS_TestFuncs.h"
#include "oedt_Cryptnav_CommonFS_TestFuncs.h"
#include "../RPC/oedt_test_rpc_core.h"
#include "oedt_FFD_TestFuncs.h"
#include "oedt_RAMDISK_CommonFS_TestFuncs.h"
#include "oedt_Audio_LoopBackTestFuncs.h"
#include "oedt_Platform_Startup.h"
#include "oedt_RegistryTest.h"
#include "oedt_osalcore_FPE_TestFuncs.h"
#include "oedt_RebootDummy.h"
#include "oedt_testing_macros.h"
#include "oedt_KDS_TestFuncs.h"
#include "oedt_Errmem_TestFuncs.h"
#include "oedt_Trace_TestFuncs.h"
#include "oedt_prm_testfuncs.h"
#include "oedt_Host_TestFuncs.h"
#include "oedt_fs_dev_testfuncs.h"
#include "oedt_Auxclock_TestFuncs.h"
#include "oedt_Audio_TestFuncs.h"
#include "oedt_Adc_Test_Funcs.h"
#include "oedt_Modules_TestFuncs.h"
#ifdef IOSC_ACTIV
#include "iosc_event_testcases.h"
#include "iosc_ringbuffer_testcases.h"
#include "iosc_semaphore_testcases.h"
#include "iosc_sharedmem_testcases.h"
#include "iosc_testcases_global.h"
#endif
#include "oedt_spm_TestFuncs.h"
#include "oedt_SWC_TestFuncs.h"
#include "OsalIoscTestCommon.h"
#include "OsalIoscMQCommon.h"
#include "oedt_GPIO_TestFuncs.h"
#include "oedt_InputInc_TestFuncs.h"
#include "oedt_rtc_TestFuncs.h"
#include "oedt_Cd_TestFuncs.h"
#include "oedt_GNSS_TestFuncs.h"
#include "oedt_Odometer_TestFuncs.h"
#include "oedt_Gyro_TestFuncs.h"
#include "oedt_PDD_TestFuncs.h"
#include "oedt_DataPool_TestFuncs.h"
#include "oedt_pram_TestFuncs.h"
#include "oedt_IncCommNdPerformance_TestFuncs.h"
#include "oedt_ACC_TestFuncs.h"
#include "oedt_Acousticsrc_TestFuncs.h"

#ifdef LSIM
/* -== SPM_VOLT ==- */
OEDT_FUNC_CFG oedt_pSPM_VOLT_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_OpenCloseDevs), OEDT_HW_EXC_NO,   50, "VOLT open/close test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_CreateRemoveNotifications), OEDT_HW_EXC_NO,   50, "VOLT create/remove notification test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_BoardVoltage), OEDT_HW_EXC_NO,   50, "VOLT board voltage test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_UserVoltageRange), OEDT_HW_EXC_NO,   50, "VOLT user voltage range test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_BoardCurrentScale), OEDT_HW_EXC_NO,   50, "VOLT board current scale test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_VoltageLevel), OEDT_HW_EXC_NO,   50, "present voltage level test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_ReadBoardCurrent), OEDT_HW_EXC_NO,   50, "Board current test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_IOCTRL_Verify), OEDT_HW_EXC_NO,   50, "Verify IOCTRL test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_SysLevelHistory), OEDT_HW_EXC_NO,   50, "System History level test"},
{OEDT_SIMPLE_TEST_FUNCTION(u32SPM_Volt_UserLevelHistory), OEDT_HW_EXC_NO,   50, "User History level test"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

/* -== GPS ==- */
OEDT_FUNC_CFG oedt_pGPS_RegressionEntries[] =
{
#ifdef LSIM
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsBasic_OpenReadDevice), OEDT_HW_EXC_NO,   50, "GPS open read "},
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsInterfaceCheck), OEDT_HW_EXC_NO,   500, "GPS interface check "},
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsRegressionTest), OEDT_HW_EXC_NO,   500, "GPS Regression test "},
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDurationTimeFix), OEDT_HW_EXC_NO,   500, "GPS time set "},
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDeviceOpenClosetest), OEDT_HW_EXC_NO,   50, "GPS Open/Close check "},
{OEDT_SIMPLE_TEST_FUNCTION(u32GpsDeviceAntennaStatustest), OEDT_HW_EXC_NO,   100, "GPS AntennaStatus check "},
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- PRAM --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pram_RegressionEntries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32_oedt_pram_devOpenClose), OEDT_HW_EXC_NO,   10, "pram open close device"},        /*   0 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32_oedt_pram_test) ,OEDT_HW_EXC_NO,   10, "pram overall test"},             /*   1 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-------------------------- GPIO -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pGPIO_RegressionEntries[] =
{
//#ifdef OSAL_GEN3
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevOpenClose),                   OEDT_HW_EXC_NO,   20, "DevOpenClose"},                   /*   0 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevCloseAlreadyClosed),          OEDT_HW_EXC_NO,   20, "DevCloseAlreadyClosed"},          /*   1 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevReOpen),                      OEDT_HW_EXC_NO,   20, "DevReOpen"},                      /*   2 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevOpenCloseDiffModes),          OEDT_HW_EXC_NO,   20, "DevOpenCloseDiffModes"},          /*   3 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevOpenCloseInvalAccessMode),    OEDT_HW_EXC_NO,   20, "DevOpenCloseInvalAccessMode"},    /*   4 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevRead),                        OEDT_HW_EXC_NO,   20, "DevRead"},                        /*   5 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIODevWrite),                       OEDT_HW_EXC_NO,   20, "DevWrite"},                       /*   6 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOGetVersion),                     OEDT_HW_EXC_NO,   20, "GetVersion"},                     /*   7 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetInput),                       OEDT_HW_EXC_NO,   20, "SetInput"},                       /*   8 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutput),                      OEDT_HW_EXC_NO,   20, "SetOutput"},                      /*   9 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetRemoveCallback),              OEDT_HW_EXC_NO,   20, "SetRemoveCallback"},              /*  10 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetTrigEdgeHIGHIntEnable),       OEDT_HW_EXC_NO,   20, "SetTrigEdgeHIGHIntEnable"},       /*  11 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetTrigEdgeLOWIntEnable),        OEDT_HW_EXC_NO,   20, "SetTrigEdgeLOWIntEnable"},        /*  12 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateInactive),               OEDT_HW_EXC_LSIM, 20, "SetStateInactive"},               /*  13 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateActive),                 OEDT_HW_EXC_LSIM, 20, "SetStateActive"},                 /*  14 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOIsStateActive),                  OEDT_HW_EXC_LSIM, 20, "IsStateActive"},                  /*  15 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputActive),                OEDT_HW_EXC_LSIM, 20, "SetOutputActive"},                /*  16 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputInactive),              OEDT_HW_EXC_LSIM, 20, "SetOutputInactive"},              /*  17 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputActiveInactive),        OEDT_HW_EXC_LSIM, 20, "SetOutputActiveInactive"},        /*  18 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputInInputMode),           OEDT_HW_EXC_NO,   20, "SetOutputInInputMode"},           /*  19 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOInvalFuncParamToIOCtrl),         OEDT_HW_EXC_NO,   20, "InvalFuncParamToIOCtrl"},         /*  20 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOMultiThreadTest),                OEDT_HW_EXC_NO,   20, "MultiThreadTest"},                /*  21 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOTwoInputPins),                   OEDT_HW_EXC_LSIM, 20, "TwoInputPins"},                   /*  22 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOMultiProcessTest),               OEDT_HW_EXC_LSIM, 20, "MultiProcessTest"},               /*  23 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetInputRemote),                 OEDT_HW_EXC_LSIM, 20, "SetInputRemote"},                 /*  24 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputRemote),                OEDT_HW_EXC_LSIM, 20, "SetOutputRemote"},                /*  25 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetTrigEdgeHIGHIntEnableRemote), OEDT_HW_EXC_LSIM, 20, "SetTrigEdgeHIGHIntEnableRemote"}, /*  26 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetTrigEdgeLOWIntEnableRemote),  OEDT_HW_EXC_LSIM, 20, "SetTrigEdgeLOWIntEnableRemote"},  /*  27 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateInactiveRemote),         OEDT_HW_EXC_LSIM, 20, "SetStateInactiveRemote"},         /*  28 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateActiveRemote),           OEDT_HW_EXC_LSIM, 20, "SetStateActiveRemote"},           /*  29 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOIsStateActiveRemote),            OEDT_HW_EXC_LSIM, 20, "IsStateActiveRemote"},            /*  30 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputActiveRemote),          OEDT_HW_EXC_LSIM, 20, "SetOutputActiveRemote"},          /*  31 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputInactiveRemote),        OEDT_HW_EXC_LSIM, 20, "SetOutputInactiveRemote"},        /*  32 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputActiveInactiveRemote),  OEDT_HW_EXC_LSIM, 20, "SetOutputActiveInactiveRemote"},  /*  33 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputInInputModeRemote),     OEDT_HW_EXC_LSIM, 20, "SetOutputInInputModeRemote"},     /*  34 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOMultiThreadTestRemote),          OEDT_HW_EXC_LSIM, 20, "MultiThreadTestRemote"},          /*  35 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOTwoInputPinsRemote),             OEDT_HW_EXC_LSIM, 20, "TwoInputPinsRemote"},             /*  36 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOMultiProcessTestRemote),         OEDT_HW_EXC_LSIM, 20, "MultiProcessTestRemote"},         /*  37 */
#if defined(LSIM) || defined(GEN3X86) //TBD: replace by OEDT_HW_EXC_XXX once defined for Gen3 targets
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateLOW),                    OEDT_HW_EXC_NO,   20, "SetStateLOW"},                    /*  38 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetStateHIGH),                   OEDT_HW_EXC_NO,   20, "SetStateHIGH"},                   /*  39 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOGetState),                       OEDT_HW_EXC_NO,   20, "GetState"},                       /*  40 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputHIGH),                  OEDT_HW_EXC_NO,   20, "SetOutputHIGH"},                  /*  41 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputLOW),                   OEDT_HW_EXC_NO,   20, "SetOutputLOW"},                   /*  42 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOSetOutputHIGHLOW),               OEDT_HW_EXC_NO,   20, "SetOutputHIGHLOW"},               /*  43 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOCallbackTest),                   OEDT_HW_EXC_NO,   20, "GPIOCallbackTest"},               /*  44 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOCallbackRegOutputMode),          OEDT_HW_EXC_NO,   20, "GPIOCallbackRegOutputMode"},      /*  45 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOCallbackRegUnReg),               OEDT_HW_EXC_NO,   20, "GPIOCallbackRegUnReg"},           /*  46 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOThreadCallbackReg),              OEDT_HW_EXC_NO,   20, "GPIOThreadCallbackReg"},          /*  47 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIO_OSAL_test),                     OEDT_HW_EXC_NO,   20, "GPIO_OSAL_test"},                 /*  48 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32GPIOActiveStateTest),                OEDT_HW_EXC_NO,   20, "GPIOActiveStateTest"},            /*  49 */
#endif
//#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!---------------------- InputInc -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pInputInc_RegressionEntries[] =
{
#ifdef OSAL_GEN3
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultipleDevicesV1),         OEDT_HW_EXC_NO,   20, "MultipleDevicesV1"},               /*   0 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultipleDevicesV2),         OEDT_HW_EXC_NO,   20, "MultipleDevicesV2"},               /*   1 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncManyEventsV1),              OEDT_HW_EXC_NO,   20, "ManyEventsV1"},                    /*   2 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncManyEventsV2),              OEDT_HW_EXC_NO,   20, "ManyEventsV2"},                    /*   3 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncManyCodesV1),               OEDT_HW_EXC_NO,   20, "ManyCodesV1"},                     /*   4 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncManyCodesV2),               OEDT_HW_EXC_NO,   20, "ManyCodesV2"},                     /*   5 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncExtendedKeysV1),            OEDT_HW_EXC_NO,   20, "ExtendedKeysV1"},                  /*   6 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncExtendedKeysV2),            OEDT_HW_EXC_NO,   20, "ExtendedKeysV2"},                  /*   7 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultiTouchTypeAV1),         OEDT_HW_EXC_NO,   20, "MultiTouchTypeAV1"},               /*   8 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultiTouchTypeAV2),         OEDT_HW_EXC_NO,   20, "MultiTouchTypeAV2"},               /*   9 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultiTouchTypeBV1),         OEDT_HW_EXC_NO,   20, "MultiTouchTypeBV1"},               /*  10 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncMultiTouchTypeBV2),         OEDT_HW_EXC_NO,   20, "MultiTouchTypeBV2"},               /*  11 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncNonFatalProtoViolationsV1), OEDT_HW_EXC_NO,   20, "NonFatalProtoViolationsV1"},       /*  12 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32InputIncNonFatalProtoViolationsV2), OEDT_HW_EXC_NO,   20, "NonFatalProtoViolationsV2"},       /*  13 */
#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- ADC --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pADC_RegressionEntries[] =
{
#ifdef OSAL_GEN3
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCChanReOpen) ,OEDT_HW_EXC_NO,   			50, "u32ADCChanReOpen"},        		/*   1 */
  {OEDT_SIMPLE_TEST_FUNCTION(OpenCloseADCSubUnit) ,OEDT_HW_EXC_NO,   			50, "OpenCloseADCSubUnit"},        		/*   2 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCGetResolution) ,OEDT_HW_EXC_NO,   			50, "u32ADCGetResolution"},        		/* 3 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCChanCloseAlreadyClosed) ,OEDT_HW_EXC_NO,   			50, "u32ADCChanCloseAlreadyClosed"},        		/*   4 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCRead) ,OEDT_HW_EXC_NO,   			50, "u32ADCRead"},        		/*   5 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCSetSingleThresholdCallbackCH5) ,OEDT_HW_EXC_NO,   			100, "u32ADCSetSingleThresholdCallbackCH5"},        		/*   7 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADClogicalDualthreadRead) ,OEDT_HW_EXC_NO,        1000, "Logical Dual thread Channel Read"},        /*  8 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCMultiProcessTest) ,OEDT_HW_EXC_NO,        1000, "Multi Process Test"},        /*  9 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32ADCUnsupportedIoctrlCheck) ,OEDT_HW_EXC_NO,        100, "Unsupported ioctrl check"},        /*  10 */
#endif /*OSAL_GEN3*/
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-------------------------- GNSS -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The testcases mentioned in the ticket CFG3-1262 have been moved from AutoTester_Testdescription dataset by cma5cob */

OEDT_FUNC_CFG oedt_pGNSS_RegressionEntries[] =
{
/* The following testcases have been moved from AutoTester_Testdescription dataset */
#if defined(GEN3ARM)
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyBasiOpenClose)            ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: DevOpnClose................."}, /*  00 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReOpen)                   ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: ReOpen......................"}, /*  01 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReClose)                  ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: ReClose....................."}, /*  02 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyBasicRead)                ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: Basic Read.................."}, /*  03 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyReadByPassingNullBufffer) ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: Basic Read By Passing NULL.."}, /*  04 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyIOCntInterfaceCheck)      ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: IOControl Interfaces........"}, /*  05 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyIOCntChkInvPar)           ,  OEDT_HW_EXC_NO  , 05, "GNSS Proxy: IOControl Invalid param....."}, /*  06 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyReOpen)                    ,  OEDT_HW_EXC_NO  , 05, "GPS Proxy : ReOpen......................"}, /*  07 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyReClose)                   ,  OEDT_HW_EXC_NO  , 05, "GPS Proxy : ReClose....................."}, /*  08 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyBasiOpenClose)             ,  OEDT_HW_EXC_NO  , 05, "GpS Proxy : DevOpnClose................."}, /*  09 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGPSProxyBasicRead)                 ,  OEDT_HW_EXC_NO  , 05, "GpS Proxy : Device read................."}, /*  10 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxySetEpoch)                 ,  OEDT_HW_EXC_NO  , 200, "GNSS Proxy: Set GNSS Epoch............."}, /*  11 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSFwOpenCloseContinuously)       ,  OEDT_HW_EXC_NO  , 200, "GNSS FW: DevOpnClose continuously......"}, /*  12 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatInterChk)                  ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: Sat Get and Set Value Check "}, /*  14 */
  {OEDT_SIMPLE_TEST_FUNCTION(u32OedtGNSSProxyGetEpoch)                 ,  OEDT_HW_EXC_NO  , 200, "GNSS Proxy: Get GNSS Epoch............."}, /*  15 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32DiagSatInterChk)              ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: Set Sat Sys using Diag......"}, /*  16 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatCfgBlk200)                 ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: SetSatSys-CfgBlk200.......  "}, /*  17 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatCfgBlk227)                 ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: SetSatSys-CfgBlk227......   "}, /*  18 */
  {OEDT_SIMPLE_TEST_FUNCTION(GNSSOedt_u32SatCfgBlk200227)              ,  OEDT_HW_EXC_NO  , 40, "GNSS Proxy: SetSatSys-CfgBlk200 & 227.  "}, /*  19 */
#endif

   {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- ODOMETER --------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pOdometer_RegressionEntries[] =
{
#if defined(GEN3ARM)
  
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32OdoOpenDev),                          OEDT_HW_EXC_NO,   50, "ODO: Open and Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoReOpen),                          OEDT_HW_EXC_NO,   50, "ODO: Re-Open"},     
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoCloseAlreadyClosed) ,             OEDT_HW_EXC_NO,   50, "ODO: Re-Close"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoReadpassingnullbuffer) ,          OEDT_HW_EXC_NO,   10, "ODO: Read ODO passing NULL buffer"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoBasicRead),                       OEDT_HW_EXC_NO,   50, "ODO: Basic read"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoBasicReadSeq),                    OEDT_HW_EXC_NO,   50, "ODO: Basic Read Seq"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoReadAndFlushSeq),                 OEDT_HW_EXC_NO,   50, "ODO: Read and Flush"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoInterfaceCheckSeq),               OEDT_HW_EXC_NO,   50, "ODO: Interface Check"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoCheckReadLargeNoOfRecords),       OEDT_HW_EXC_NO,   50, "ODO: Read Large NoOfBlocks"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoCheckReadMoreThanBufferLength),   OEDT_HW_EXC_NO,   50, "ODO: Read More than BufLen"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoTimeoutRead),                     OEDT_HW_EXC_NO,   50, "ODO: No Odo Conn,Timeout Read"},
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32OdoGetCycleTime),                     OEDT_HW_EXC_NO,   50, "ODO: Get Odo CycleTime"},             
 
    {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)tu32OdoStubTestReadDirection),           OEDT_HW_EXC_NO,   50, "ODO: Stub Test for Odo Direction"},
#endif

{OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}

};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!--------------------- LSIM ODOMETER--------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pODO_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32OdoOpenDev),                         OEDT_HW_EXC_NO,    50, "ODO:Open and Close Device"},    
  {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoBasicReadSeq),                   OEDT_HW_EXC_NO,    50, "ODO:Basic Read Seq"},           
  {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoReadAndFlushSeq),                OEDT_HW_EXC_ALL,   50, "ODO:Read and Flush"},           
  {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoInterfaceCheckSeq),              OEDT_HW_EXC_NO,    50, "ODO:Interface Check"},          
  {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoCheckReadLargeNoOfRecords),      OEDT_HW_EXC_ALL,   50, "ODO:Read Large NoOfBlocks"},    
  {OEDT_SIMPLE_TEST_FUNCTION(tu32OdoCheckReadMoreThanBufferLength),  OEDT_HW_EXC_NO ,   50, "ODO:Read More than BufLen"},    
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!--------------------------GYRO --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The testcases mentioned in the ticket CFG3-1262 have been moved from AutoTester_Testdescription dataset by cma5cob */

OEDT_FUNC_CFG oedt_pGYRO_Entries[] =
{
/* The following testcases have been moved from AutoTester_Testdescription dataset */
#if defined(GEN3ARM)
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroOpenClose),                     OEDT_HW_EXC_NO,    50, "GYRO: Gyro DevOpnClose"},                     
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroReOpen),                        OEDT_HW_EXC_NO,    50, "GYRO: Gyro Re-Open"},                         
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCloseAlreadyClosed),            OEDT_HW_EXC_NO,    50, "GYRO: Gyro Re-Close"},                        
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroTemperature),                   OEDT_HW_EXC_NO,    10, "GYRO: Gyro temperature"},              
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroBasicRead),                     OEDT_HW_EXC_NO,    10, "GYRO: Gyro basicRead"},                   
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroReadPassingNullBuffer),         OEDT_HW_EXC_NO,    10, "GYRO: Read gyro passing NULL buffer"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroOedt_u32ValuesPlot),            OEDT_HW_EXC_NO,   120, "GYRO: Gyro R-S-T Graphical plot"},    
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroSelfTest),                      OEDT_HW_EXC_NO,    30, "Gyro: Gyro selftest"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32GyroHwInfo),                        OEDT_HW_EXC_NO,    30, "Gyro: Read gyro hardware info"},
   {OEDT_SIMPLE_TEST_FUNCTION(u32OpenCloseRepeatedly),               OEDT_HW_EXC_NO,    30, "Gyro: Continuous Open Close"},
#endif
#if defined(GEN3X86)
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_r) ,            OEDT_HW_EXC_NO,   10, "chck gyroval readseq for gyro_r"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_s) ,            OEDT_HW_EXC_NO,   10, "chck gyroval readseq for gyro_s"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroInterfaceCheckSeq) ,               OEDT_HW_EXC_NO,   10, "check all the IOctrl interfaces"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroCheckGyroValuesSeq_t) ,            OEDT_HW_EXC_NO,   10, "chck gyroval readseq for gyro_t"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroInterfaceCheckSeq) ,               OEDT_HW_EXC_NO,   10, "check all the IOctrl interfaces"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroReadPassingNullBuffer) ,           OEDT_HW_EXC_NO,   10, "Read gyro passing NULL buffer"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroIOCTRLPassingNullBuffer) ,         OEDT_HW_EXC_NO,   10, "IOCTRL passing NULL buffer"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32GyroBasicRead) ,                       OEDT_HW_EXC_NO,   10, "GyrobasicRead"},
#endif
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!--------------------------ACC --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The following testcases have been moved from AutoTester_Testdescription dataset by cma5cob for the ticket CFG3-1262 */

OEDT_FUNC_CFG oedt_pACC_RegressionEntries[] =
{
#if defined(GEN3ARM)
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevOpenClose),                    OEDT_HW_EXC_NO,   50, "ACC: DevOpnClose"},                
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevReOpen),                       OEDT_HW_EXC_NO,   50, "ACC: Re-Open"},                    
   {OEDT_SIMPLE_TEST_FUNCTION(u32ACCDevCloseAlreadyClosed),           OEDT_HW_EXC_NO,   50, "ACC: Re-Close"},                   
   {OEDT_SIMPLE_TEST_FUNCTION(tu32AccOpnClsReg),                      OEDT_HW_EXC_NO,   50, "ACC: Continuous Open Close"},                   
#endif
   {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, "" }
};

OEDT_FUNC_CFG oedt_pRTC_RegressionEntries[] =
{
#ifdef LSIM
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCMulOpenClose) ,OEDT_HW_EXC_NO, 	20, "Multiple Open" }, 					                       /* 0  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCMultipleRead) ,OEDT_HW_EXC_NO, 	20, "Multiple Read" },  					                   /* 1  */
  
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCSetGpsTimeRegressionSet) ,OEDT_HW_EXC_NO, 	20, "Set GPS Time in regression" },                    /*2 */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCSetGpsTimeIllegalRegressionSet) ,OEDT_HW_EXC_NO, 	20, "Set Illegal GPS Time in regression" },    /* 3  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCNonExistentTime) ,OEDT_HW_EXC_NO, 	20, "Read non existent time" },                                /* 4  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCReadVersion) ,OEDT_HW_EXC_NO, 	20, "Read Version" },                                               /* 5 */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCReadVersionInval) ,OEDT_HW_EXC_NO, 	20, "Read Version Invalid" },                               /* 6  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCReadInval) ,OEDT_HW_EXC_NO, 	20, "Read Inval" },                                                 /* 7  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCReadWithoutOpen) ,OEDT_HW_EXC_NO, 	20, "Read without Open" },                                      /* 8  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCReadExceedMax) ,OEDT_HW_EXC_NO, 	20, "Read Exceeds update rate" },                               /* 9  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCSetGPSTimeNoOpen) ,OEDT_HW_EXC_NO, 	20, "Set GPS Time no open" },                               /* 10  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCGpsTimeStateValid) ,OEDT_HW_EXC_NO, 	20, "Time State Valid" },                                   /* 11  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32MultiThread_OpenClose) ,OEDT_HW_EXC_NO, 	20, "u32MultiThread_OpenClose" },                               /* 12  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32MultiThread_SetGpsTime) ,OEDT_HW_EXC_NO, 	20, "u32MultiThread_SetGpsTime" },                              /* 13  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32MultiThread_GetVersion) ,OEDT_HW_EXC_NO, 	20, "u32MultiThread_GetVersion" },                              /* 14  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32MultiThread_RtcIORead) ,OEDT_HW_EXC_NO, 	20, "u32MultiThread_OpenClose" },                                /* 15  */
  {OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32MultiThread_MultiFunctionTest) ,OEDT_HW_EXC_NO, 	20, "u32MultiThread_MultiFunctionTest" },                /* 16  */
  //{OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) tu32Gen2RTCSetGpsTime) ,OEDT_HW_EXC_NO, 	20, "Set GPS Time" },                                              /* 17  */
  //{OEDT_SIMPLE_TEST_FUNCTION( (OEDT_TEST_FUNCPTR) u32RTCMultiProcessTest) ,OEDT_HW_EXC_NO, 	20, "u32RTCMultiProcessTest" },                                 /* 18  */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, "" }
#endif
};

/* -== ACOUSTICIN ==- */
OEDT_FUNC_CFG oedt_pACOUSTICIN_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T000), OEDT_HW_EXC_NO,   5, "T000*special asound.conf needed*"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T001), OEDT_HW_EXC_NO,  90, "T001 capture micro + playback   "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T002), OEDT_HW_EXC_NO,  90, "T002 echo from micro via threads"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T003), OEDT_HW_EXC_NO,  90, "T003 Capture+Play stereo streams"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T004), OEDT_HW_EXC_NO,  90, "T004 simultan capture + playback"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T005), OEDT_HW_EXC_NO,  90, "T005 simul C+P diff stream sizes"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T006), OEDT_HW_EXC_NO,  90, "T006 like T005 long duration    "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T007), OEDT_HW_EXC_NO,   5, "T007 Error handler check        "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T008), OEDT_HW_EXC_NO,  20, "T008 16k-MIC-ADC                "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICIN_T009), OEDT_HW_EXC_NO,  20, "T009 44.1k-MIC-ADC              "},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* -== ACOUSTICOUT ==- */

/* The testcases mentioned in the ticket CFG3-1264 have been copied from AutoTester_Testdescription by cma5cob */

OEDT_FUNC_CFG oedt_pACOUSTICOUT_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T000), OEDT_HW_EXC_NO,   5, "T000*special asound.conf needed*"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T001), OEDT_HW_EXC_NO,  60, "T001 open-close                 "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T002), OEDT_HW_EXC_NO,  60, "T002 set parameters             "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T003), OEDT_HW_EXC_NO,  60, "T003 start-stop                 "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T004), OEDT_HW_EXC_NO,  60, "T004 play 22-16-1 stop-callback "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T005), OEDT_HW_EXC_NO,  60, "T005 play 22-16-1 stop-event    "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T006), OEDT_HW_EXC_NO,  60, "T006 play 44-16-2 stop-callback "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T007), OEDT_HW_EXC_NO,  60, "T007 play 22-16-2 stop-callback "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T008), OEDT_HW_EXC_NO,  60, "T008 play 22-16-1 abort-callback"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T009), OEDT_HW_EXC_NO,  60, "T009 play 22-16-1 abort-event   "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T010), OEDT_HW_EXC_NO,  60, "T010 play 22-16-1 pause-resume  "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_T011), OEDT_HW_EXC_NO,	5,	"T011 Error handler check        "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T001), OEDT_HW_EXC_NO,  300, "TL001 play long 44100-16-2      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T002), OEDT_HW_EXC_NO,  300, "TL002 play long 22050-16-2      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T003), OEDT_HW_EXC_NO,  300, "TL003 play long 44100-16-1      "},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICOUT_LONG_T004), OEDT_HW_EXC_NO,  300, "TL004 play long 22050-16-1      "},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* -== ACOUSTICSRC ==- */

/* As per the project requirement AcousticSRC is added. To validate the functionality of the device, the following test cases are written.
   The Acoustic SRC OEDTs will not run on LSIM. Because virtual box will not support USB Audio device */
#ifndef GEN3X86
OEDT_FUNC_CFG oedt_pACOUSTICSRC_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICSRC_T000), OEDT_HW_EXC_NO,   5, "T000*DeLock-USB-Soundcard should be connected*"},
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_ACOUSTICSRC_T001), OEDT_HW_EXC_NO,  60, "T001 Play PCM Test File "},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif 


/* -== AUDIO TEST ==- */
OEDT_FUNC_CFG oedt_pAUDIOTEST_Entries[] =
{
#if (!defined (GEN3ARM) && !defined(GEN3X86))
{OEDT_SIMPLE_TEST_FUNCTION(OEDT_AUDIO_LOOPBACK_T001), OEDT_HW_EXC_LSIM,	 20, "Audio Loopback Test"},
#endif
{OEDT_SIMPLE_TEST_FUNCTION(u32_oedt_audio_test_open_close_devs), OEDT_HW_EXC_NO,	 5, "Audio Devs Open/Close"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
								/* !!!--------------------------- Common FFS2--------------------!!! */
								/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pFFS2_CommonFS_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenClosedevice), OEDT_HW_EXC_NO,	100, "Open Close Device"},						/* 0  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpendevInvalParm), OEDT_HW_EXC_NO,	100, "Open Device with Inval param"},		/* 1  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReOpendev), OEDT_HW_EXC_NO,	100, "reopen Device"}, 							/* 2  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpendevDiffModes), OEDT_HW_EXC_NO,	100, "Open Device in different modes"},	/* 3  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSClosedevAlreadyClosed), OEDT_HW_EXC_NO,	100, "Close devide already closed"}, /* causes exception and can not be taken care at OSAL level */ 	/* 4  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenClosedir	), OEDT_HW_EXC_NO,	100, "Open / Close Dir"}, 						/* 5  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpendirInvalid ), OEDT_HW_EXC_NO,	100, "Open inval dir"}, 						/* 6  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateDelDir	), OEDT_HW_EXC_NO,	100, "Create / Del Dir"}, 						/* 7  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateDelSubDir	), OEDT_HW_EXC_NO,	100, "Create sub Directory"},					/* 8  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateDirInvalName	), OEDT_HW_EXC_NO,	100, "Create dir with inval name"}, 	/* 9  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRmNonExstngDir ), OEDT_HW_EXC_NO,	100, "Remove non exsisting Dir"},			/* 10 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRmDirUsingIOCTRL	), OEDT_HW_EXC_NO,	100, "Remove Dir using IOCTRL"},				/* 11 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetDirInfo	), OEDT_HW_EXC_NO,	100, "Get Dir info "}, 							/* 12 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenDirDiffModes	), OEDT_HW_EXC_NO,	100, "Open Dir in diff modes"}, 				/* 13 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReOpenDir	), OEDT_HW_EXC_NO,	100, "Reopen Dir"}, 								/* 14 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSDirParallelAccess ), OEDT_HW_EXC_NO,	100, "Dir Paralell Access "},					/* 15 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateDirMultiTimes), OEDT_HW_EXC_NO,	100, "Create Dir Multiple times"}, 			/* 16 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateRemDirInvalPath), OEDT_HW_EXC_NO,	100, "Remove Dir with inval path"}, 	/* 17 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateRmNonEmptyDir), OEDT_HW_EXC_NO,	100, "Remove non empty dir"},					/* 18 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCopyDir	), OEDT_HW_EXC_NO,	100, "Copy Dir"},										/* 19 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSMultiCreateDir ), OEDT_HW_EXC_NO,	100, "Create Multiple dir"}, 					/* 20 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateSubDir	), OEDT_HW_EXC_NO,	100, "Create sub dir"},							/* 21 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSDelInvDir	), OEDT_HW_EXC_NO,	100, "Delete Inval Dir"}, 							/* 22 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCopyDirRec	), OEDT_HW_EXC_NO,	100, "Copy Dir recursively"},					/* 23 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRemoveDir	), OEDT_HW_EXC_NO,	100, "Remove dir"}, 								/* 24 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateDel	), OEDT_HW_EXC_NO,	100, "Create / Del file"},						/* 25 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileOpenClose	), OEDT_HW_EXC_NO,	100, "Open Close file"},						/* 26 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileOpenInvalPath ), OEDT_HW_EXC_NO,	100, "open file with inval path"},			/* 27 */
	/*This testcase is removed because osal manual doesn't tells that, 
	for invalid access parameter OSAL_IOOpen() would return error.*/
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileOpenInvalParam	), OEDT_HW_EXC_NO,	100, "open file with inval param"}, 	/* 28 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReOpen	), OEDT_HW_EXC_NO,	100, "Reopen the file"},						/* 29 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileRead ), OEDT_HW_EXC_NO,	100, "File read"},									/* 30 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileWrite	), OEDT_HW_EXC_NO,	100, "File write"}, 								/* 31 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetPosFrmBOF	), OEDT_HW_EXC_NO,	100, "Get Posistion from BOF"}, 				/* 32 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetPosFrmEOF	), OEDT_HW_EXC_NO,	100, "Get Posistion from EOF"}, 				/* 33 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_NO,	100, "Read neg offset from BOF"},	/* 34 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_NO,	100, "Read beyond EOF"},				/* 35 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_NO,	100, "read offset from BOF"},				/* 36 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_NO,	100, "read offset from EOF"},				/* 37 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_NO,	100, "Read every nth byte from BOF "},/* 38 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_NO,	100, "Read every nth byte from EOF"}, /* 39 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetFileCRC	), OEDT_HW_EXC_NO,	100, "Get file CRC"},							/* 40 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadAsync), OEDT_HW_EXC_NO, 100, 	"Read Async"}, 							/* 41 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSLargeReadAsync), OEDT_HW_EXC_NO, 100,	  "Large Read Async"},					/* 42 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteAsync), OEDT_HW_EXC_NO, 100, 	"Write Async"},							/* 43 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSLargeWriteAsync), OEDT_HW_EXC_NO, 100, 	"Large Write Async"},					/* 44 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteOddBuffer), OEDT_HW_EXC_NO, 	100,    "Write to odd buf"},					/* 45 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteFileWithInvalidSize), OEDT_HW_EXC_NO, 100, 	"Write With Inval Size"},		/* 46 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteFileInvalidBuffer), OEDT_HW_EXC_NO, 100, 	"Write Inval Buf"},				/* 47 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteFileStepByStep), OEDT_HW_EXC_NO, 100, 	"Write Step By Step"},			/* 48 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetFreeSpace), OEDT_HW_EXC_NO, 100, 	"Get Free Space"},						/* 49 */ 					
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetTotalSpace), OEDT_HW_EXC_NO, 100,    "Get Total Space"}, 						/* 50 */				 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_NO, 100, 	"Fil Open Close NonExstng"},		/* 51 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileDelWithoutClose), OEDT_HW_EXC_NO, 100, 	"File Del Without Close"},			/* 52 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_NO, 100, 	"Set Pos Diff Off"},				/* 53 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateFileMultiTimes), OEDT_HW_EXC_NO, 100,	  "Create File Multi Times"},			/* 54 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateUnicodeName), OEDT_HW_EXC_NO, 100,	  "File Create Unicode Name"},		/* 55 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateInvalName), OEDT_HW_EXC_NO, 100, 	"File Create Inval Name"},				/* 56 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateLongName), OEDT_HW_EXC_NO, 100, 	"File Create Long Name"},			/* 57 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateDiffModes), OEDT_HW_EXC_NO, 100, 	"File Create Diff Modes"},				/* 58 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileCreateInvalPath), OEDT_HW_EXC_NO, 100,	  "File Create Inval Path"},			/* 59 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSStringSearch), OEDT_HW_EXC_NO, 100, 	"String Search"},							/* 60 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileOpenDiffModes), OEDT_HW_EXC_NO, 100,	  "File Open Diff Modes"},				/* 61 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadAccessCheck), OEDT_HW_EXC_NO, 100,	  "File Read Access Check"},			/* 62 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileWriteAccessCheck), OEDT_HW_EXC_NO, 100, 	"File Write Access Check"},		/* 63 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileReadSubDir), OEDT_HW_EXC_NO, 100, 	"File Read SubDir"},						/* 64 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileWriteSubDir), OEDT_HW_EXC_NO, 100,	  "File Write SubDir"},					/* 65 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof1KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 1Kb"},			/* 66 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof10KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 10Kb"},			/* 67 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof100KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 100Kb"},	/* 68 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof1MBFile), OEDT_HW_EXC_NO, 100, "Read/Write 1MB"},	/* 69 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof1KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 1Kb for 1000 times"},	/* 70 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof10KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 10Kb for 1000 times"},	/* 71 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof100KbFilefor100times), OEDT_HW_EXC_NO, 100, "Read/Write 100Kb for 100 times"},	/* 72 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteTimeMeasureof1MBFilefor16times), OEDT_HW_EXC_NO, 100, "Read/Write 1MB for 16 times"},			/* 73 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRenameFile), OEDT_HW_EXC_NO, 100,  "Rename File"}, 						/* 74 */								
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteFrmBOF), OEDT_HW_EXC_NO, 100,   "Write From BOF"},					/* 75 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteFrmEOF), OEDT_HW_EXC_NO, 100,   "Write From EOF"},				/* 76 */													
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSLargeFileRead), OEDT_HW_EXC_NO, 100,  "Large File Read"},				/* 77 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSLargeFileWrite), OEDT_HW_EXC_NO, 100,  "Large File Write"},				/* 78 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenCloseMultiThread), OEDT_HW_EXC_NO, 100,   "Open Close Multi Thread"},	/* 79 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteMultiThread), OEDT_HW_EXC_NO, 100,  "Write Multi Thread"}, 		/* 80 */											
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteReadMultiThread), OEDT_HW_EXC_NO, 100,  "Write Read Multi Thread"},	/* 81 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadMultiThread), OEDT_HW_EXC_NO, 100,   "Read Multi Thread"},				/* 82 */
	//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFormat), OEDT_HW_EXC_NO, 200,	 "File System Format"},  //This case formats filesystem devices /* 83 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileGetExt), OEDT_HW_EXC_NO, 	100,   "Read file/directory contents"},													   
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileGetExt2), OEDT_HW_EXC_NO, 	100,   "Read file/directory contents with 2 IOCTL"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSSaveNowIOCTRL_SyncWrite), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSSaveNowIOCTRL_SyncWrite"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSSaveNowIOCTRL_AsynWrite), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSSaveNowIOCTRL_AsynWrite"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileOpenCloseMultipleTime), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileOpenCloseMultipleTime"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileOpenCloseMultipleTimeRandom1), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileOpenCloseMultipleTimeRandom1"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileOpenCloseMultipleTimeRandom2), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileOpenCloseMultipleTimeRandom2"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileLongFileOpenCloseMultipleTime), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileLongFileOpenCloseMultipleTime"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom1), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom1"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom2), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom2"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFileOpenCloseMultipleTimeMultDir), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFileOpenCloseMultipleTimeMultDir"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSGetFileSize), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSGetFileSize"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadDirValidate), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSReadDirValidate"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRmRecursiveCancel), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSRmRecursiveCancel"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateManyThreadDeleteMain), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSCreateManyThreadDeleteMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCreateManyDeleteInThread), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSCreateManyDeleteInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenManyCloseInThread), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSOpenManyCloseInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenInThreadCloseMain), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSOpenInThreadCloseMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteMainReadInThread), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSWriteMainReadInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSWriteThreadReadInMain), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSWriteThreadReadInMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSFileAccInDiffThread), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSFileAccInDiffThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSReadDirExtPartByPart"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSReadDirExt2PartByPart"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSCopyDirTest), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSCopyDirTest"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSReadWriteHugeData), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSReadWriteHugeData"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSRW_Performance_Diff_BlockSize), OEDT_HW_EXC_NO, 200, "u32FFS2_CommonFSRW_Performance_Diff_BlockSize"},
 //{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS2_CommonFSOpenDirIOCTL), OEDT_HW_EXC_NO, 	100,    "Open directory using IOCTL"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

								/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
								/* !!!--------------------------- Common FFS3--------------------!!! */
								/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
                        
/*  Testcases mentioned in the ticket CFG3-1261 have been moved from AutoTester_Testdescription by cma5cob */

OEDT_FUNC_CFG oedt_pFFS3_CommonFS_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenClosedevice), OEDT_HW_EXC_NO,	100, "Open Close Device"},						/* 0  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpendevInvalParm), OEDT_HW_EXC_NO,	100, "Open Device with Inval param"},		/* 1  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReOpendev), OEDT_HW_EXC_NO,	100, "reopen Device"}, 							/* 2  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpendevDiffModes), OEDT_HW_EXC_NO,	100, "Open Device in different modes"},	/* 3  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSClosedevAlreadyClosed), OEDT_HW_EXC_NO,	100, "Close devide already closed"}, /* causes exception and can not be taken care at OSAL level */ 	/* 4  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenClosedir	), OEDT_HW_EXC_NO,	100, "Open / Close Dir"}, 						/* 5  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpendirInvalid ), OEDT_HW_EXC_NO,	100, "Open inval dir"}, 						/* 6  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateDelDir	), OEDT_HW_EXC_NO,	100, "Create / Del Dir"}, 						/* 7  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateDelSubDir	), OEDT_HW_EXC_NO,	100, "Create sub Directory"},					/* 8  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateDirInvalName	), OEDT_HW_EXC_NO,	100, "Create dir with inval name"}, 	/* 9  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRmNonExstngDir ), OEDT_HW_EXC_NO,	100, "Remove non exsisting Dir"},			/* 10 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRmDirUsingIOCTRL	), OEDT_HW_EXC_NO,	100, "Remove Dir using IOCTRL"},				/* 11 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetDirInfo	), OEDT_HW_EXC_NO,	100, "Get Dir info "}, 							/* 12 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenDirDiffModes	), OEDT_HW_EXC_NO,	100, "Open Dir in diff modes"}, 				/* 13 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReOpenDir	), OEDT_HW_EXC_NO,	100, "Reopen Dir"}, 								/* 14 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSDirParallelAccess ), OEDT_HW_EXC_NO,	100, "Dir Paralell Access "},					/* 15 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateDirMultiTimes), OEDT_HW_EXC_NO,	100, "Create Dir Multiple times"}, 			/* 16 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateRemDirInvalPath), OEDT_HW_EXC_NO,	100, "Remove Dir with inval path"}, 	/* 17 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateRmNonEmptyDir), OEDT_HW_EXC_NO,	100, "Remove non empty dir"},					/* 18 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCopyDir	), OEDT_HW_EXC_NO,	100, "Copy Dir"},										/* 19 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSMultiCreateDir ), OEDT_HW_EXC_NO,	100, "Create Multiple dir"}, 					/* 20 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateSubDir	), OEDT_HW_EXC_NO,	100, "Create sub dir"},							/* 21 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSDelInvDir	), OEDT_HW_EXC_NO,	100, "Delete Inval Dir"}, 							/* 22 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCopyDirRec	), OEDT_HW_EXC_NO,	100, "Copy Dir recursively"},					/* 23 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRemoveDir	), OEDT_HW_EXC_NO,	100, "Remove dir"}, 								/* 24 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateDel	), OEDT_HW_EXC_NO,	100, "Create / Del file"},						/* 25 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileOpenClose	), OEDT_HW_EXC_NO,	100, "Open Close file"},						/* 26 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileOpenInvalPath ), OEDT_HW_EXC_NO,	100, "open file with inval path"},			/* 27 */
	/*This testcase is removed because osal manual doesn't tells that, 
	for invalid access parameter OSAL_IOOpen() would return error.*/
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileOpenInvalParam	), OEDT_HW_EXC_NO,	100, "open file with inval param"}, 	/* 28 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReOpen	), OEDT_HW_EXC_NO,	100, "Reopen the file"},						/* 29 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileRead ), OEDT_HW_EXC_NO,	100, "File read"},									/* 30 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileWrite	), OEDT_HW_EXC_NO,	100, "File write"}, 								/* 31 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetPosFrmBOF	), OEDT_HW_EXC_NO,	100, "Get Posistion from BOF"}, 				/* 32 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetPosFrmEOF	), OEDT_HW_EXC_NO,	100, "Get Posistion from EOF"}, 				/* 33 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_NO,	100, "Read neg offset from BOF"},	/* 34 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_NO,	100, "Read beyond EOF"},				/* 35 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_NO,	100, "read offset from BOF"},				/* 36 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_NO,	100, "read offset from EOF"},				/* 37 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_NO,	100, "Read every nth byte from BOF "},/* 38 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_NO,	100, "Read every nth byte from EOF"}, /* 39 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetFileCRC	), OEDT_HW_EXC_NO,	100, "Get file CRC"},							/* 40 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadAsync), OEDT_HW_EXC_NO, 100, 	"Read Async"}, 							/* 41 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSLargeReadAsync), OEDT_HW_EXC_NO, 100,	  "Large Read Async"},					/* 42 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteAsync), OEDT_HW_EXC_NO, 100, 	"Write Async"},							/* 43 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSLargeWriteAsync), OEDT_HW_EXC_NO, 100, 	"Large Write Async"},					/* 44 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteOddBuffer), OEDT_HW_EXC_NO, 	100,    "Write to odd buf"},					/* 45 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteFileWithInvalidSize), OEDT_HW_EXC_NO, 100, 	"Write With Inval Size"},		/* 46 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteFileInvalidBuffer), OEDT_HW_EXC_NO, 100, 	"Write Inval Buf"},				/* 47 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteFileStepByStep), OEDT_HW_EXC_NO, 100, 	"Write Step By Step"},			/* 48 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetFreeSpace), OEDT_HW_EXC_NO, 100, 	"Get Free Space"},						/* 49 */ 					
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetTotalSpace), OEDT_HW_EXC_NO, 100,    "Get Total Space"}, 						/* 50 */				 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_NO, 100, 	"Fil Open Close NonExstng"},		/* 51 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileDelWithoutClose), OEDT_HW_EXC_NO, 100, 	"File Del Without Close"},			/* 52 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_NO, 100, 	"Set Pos Diff Off"},				/* 53 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateFileMultiTimes), OEDT_HW_EXC_NO, 100,	  "Create File Multi Times"},			/* 54 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateUnicodeName), OEDT_HW_EXC_NO, 100,	  "File Create Unicode Name"},		/* 55 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateInvalName), OEDT_HW_EXC_NO, 100, 	"File Create Inval Name"},				/* 56 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateLongName), OEDT_HW_EXC_NO, 100, 	"File Create Long Name"},			/* 57 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateDiffModes), OEDT_HW_EXC_NO, 100, 	"File Create Diff Modes"},				/* 58 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileCreateInvalPath), OEDT_HW_EXC_NO, 100,	  "File Create Inval Path"},			/* 59 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSStringSearch), OEDT_HW_EXC_NO, 100, 	"String Search"},							/* 60 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileOpenDiffModes), OEDT_HW_EXC_NO, 100,	  "File Open Diff Modes"},				/* 61 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadAccessCheck), OEDT_HW_EXC_NO, 100,	  "File Read Access Check"},			/* 62 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileWriteAccessCheck), OEDT_HW_EXC_NO, 100, 	"File Write Access Check"},		/* 63 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileReadSubDir), OEDT_HW_EXC_NO, 100, 	"File Read SubDir"},						/* 64 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileWriteSubDir), OEDT_HW_EXC_NO, 100,	  "File Write SubDir"},					/* 65 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 1Kb"},			/* 66 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 10Kb"},			/* 67 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile), OEDT_HW_EXC_NO, 100,  "Read/Write 100Kb"},	/* 68 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile), OEDT_HW_EXC_NO, 100, "Read/Write 1MB"},	/* 69 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 1Kb for 1000 times"},	/* 70 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 10Kb for 1000 times"},	/* 71 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times), OEDT_HW_EXC_NO, 100, "Read/Write 100Kb for 100 times"},	/* 72 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times), OEDT_HW_EXC_NO, 100, "Read/Write 1MB for 16 times"},			/* 73 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRenameFile), OEDT_HW_EXC_NO, 100,  "Rename File"}, 						/* 74 */								
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteFrmBOF), OEDT_HW_EXC_NO, 100,   "Write From BOF"},					/* 75 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteFrmEOF), OEDT_HW_EXC_NO, 100,   "Write From EOF"},				/* 76 */													
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSLargeFileRead), OEDT_HW_EXC_NO, 100,  "Large File Read"},				/* 77 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSLargeFileWrite), OEDT_HW_EXC_NO, 100,  "Large File Write"},				/* 78 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenCloseMultiThread), OEDT_HW_EXC_NO, 100,   "Open Close Multi Thread"},	/* 79 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteMultiThread), OEDT_HW_EXC_NO, 100,  "Write Multi Thread"}, 		/* 80 */											
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteReadMultiThread), OEDT_HW_EXC_NO, 100,  "Write Read Multi Thread"},	/* 81 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadMultiThread), OEDT_HW_EXC_NO, 100,   "Read Multi Thread"},				/* 82 */
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFormat), OEDT_HW_EXC_NO, 200,	 "File System Format"},  //This case formats filesystem devices /* 83 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileGetExt), OEDT_HW_EXC_NO, 	100,   "Read file/directory contents"},													   
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileGetExt2), OEDT_HW_EXC_NO, 	100,   "Read file/directory contents with 2 IOCTL"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileOpenCloseMultipleTime), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileOpenCloseMultipleTime"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileOpenCloseMultipleTimeRandom1), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileOpenCloseMultipleTimeRandom1"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileOpenCloseMultipleTimeRandom2), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileOpenCloseMultipleTimeRandom2"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileLongFileOpenCloseMultipleTime), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileLongFileOpenCloseMultipleTime"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFileOpenCloseMultipleTimeMultDir), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFileOpenCloseMultipleTimeMultDir"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSGetFileSize), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSGetFileSize"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadDirValidate), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSReadDirValidate"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRmRecursiveCancel), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSRmRecursiveCancel"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateManyThreadDeleteMain), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSCreateManyThreadDeleteMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCreateManyDeleteInThread), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSCreateManyDeleteInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenManyCloseInThread), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSOpenManyCloseInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenInThreadCloseMain), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSOpenInThreadCloseMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteMainReadInThread), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSWriteMainReadInThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSWriteThreadReadInMain), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSWriteThreadReadInMain"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileAccInDiffThread), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSFileAccInDiffThread"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSReadDirExtPartByPart"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSReadDirExt2PartByPart"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSCopyDirTest), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSCopyDirTest"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSReadWriteHugeData), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSReadWriteHugeData"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSRW_Performance_Diff_BlockSize), OEDT_HW_EXC_NO, 200, "u32FFS3_CommonFSRW_Performance_Diff_BlockSize"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenDirIOCTL), OEDT_HW_EXC_NO, 	100,    "Open directory using IOCTL"},
{OEDT_TEST_SIMPLE(FFS3_CommonFSStressTest), OEDT_HW_EXC_NO, 100, "StressTest"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------MultiProcess Tests----------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pMULTIPROCESS_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32SpawnMultiProcesses), OEDT_HW_EXC_NO , 1000, "Spawn Multiple Processes"},               /*   0 */
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------Performance Tests----------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pPERFORMANCE_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32SpawnPerformanceProcess), OEDT_HW_EXC_NO , 1000, "Spawn Performance Process"},               /*   0 */
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------Platform Startup Tests----------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pPLATFORM_Startup_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32StartupTimePlatform), OEDT_HW_EXC_NO , 1000, "Platform Startup Measurement"},               /*   0 */
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


					/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
				/* !!!------------------------OSALCORE-------------------------!!! */
				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pOSALCORERegressionEntries[] =
{
    /*OSAL Thread API test functions*/

{OEDT_SIMPLE_TEST_FUNCTION(u32PRCreateThreadNameNULL), OEDT_HW_EXC_NO,     50, "Try Create Thread with name NULL"},                            /*   0 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateThreadNameTooLong), OEDT_HW_EXC_NO,     50, "Try Create Thread with name greater than MAX"},                /*   1 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateThreadWrongPriority), OEDT_HW_EXC_NO,     50, "Try Create Thread with wrong priority "},                      /*   2 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateThreadLowStack), OEDT_HW_EXC_NO,     50, "Try Create Thread with low stack"},                            /*   3 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateThreadEntryFuncNULL), OEDT_HW_EXC_NO,     50, "Try Create Thread with entry func NULL"},                      /*   4 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DeleteThreadNotExisting), OEDT_HW_EXC_NO,     50, "Try Delete Thread not existing"},                              /*   5 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DeleteThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try Delete Thread with negative id"},                          /*   6 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateDeleteThreadValidParam), OEDT_HW_EXC_NO,     50, "Create and Delete a thread with valid param"},                 /*   7 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateTwoThreadSameName), OEDT_HW_EXC_NO,     50, "Try ReCreate Thread with same name"},                          /*   8 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ActivateThreadNonExisting), OEDT_HW_EXC_NO,     50, "Try Activate Thread not existing"},                            /*   9 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ActivateThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try Activate Thread with negative id"},                        /*  10 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ActivateThreadTwice), OEDT_HW_EXC_NO,     50, "Try Activate Thread which is already activated"},              /*  11 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SuspendThreadNonExisting), OEDT_HW_EXC_NO,     50, "Try Suspend Thread not existing"},                             /*  12 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SuspendThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try Suspend Thread with negative id"},                         /*  13 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ResumeThreadNonExisting), OEDT_HW_EXC_NO,     50, "Try Resume Thread not existing"},                              /*  14 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ResumeThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try Resume Thread with negative id"},                          /*  15 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ChgPriorityThreadNonExisting), OEDT_HW_EXC_NO,     50, "Try Change Thread Priority of a non existing thread"},         /*  16 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ChgPriorityThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try Change Thread Priority with negative id"},                 /*  17 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ChgPriorityThreadWithWrongVal), OEDT_HW_EXC_NO,     50, "Try Change Thread Priority with inval priority"},              /*  18 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ReadTCBThreadNonExisting), OEDT_HW_EXC_NO,     50, "Try TCB retrieve on Thread not existing"},                     /*  19 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ReadTCBThreadWithNegID), OEDT_HW_EXC_NO,     50, "Try TCB retrieve on Thread with negative id"},                 /*  20 */
    //{u32ReadTCBThreadDifferentStatus), OEDT_HW_EXC_NO,     50, "Try TCB retrieve on Thread in various states"},                /*  21 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MaxThreadCreate), OEDT_HW_EXC_NO,   3000, "Create Maximum no of Threads in user and kernel mode"},        /*  22 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ThreadWhoAmI), OEDT_HW_EXC_NO,   3000, "Thread Who AM I"},                                             /*  23 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ThreadListWithListNULL), OEDT_HW_EXC_NO,     50, "Try to use ThreadList API with List NULL"},                    /*  24 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ThreadListWithListSizeNULL), OEDT_HW_EXC_NO,     50, "Try to use ThreadList API with Size NULL"},                    /*  25 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ThreadListWithValidParam), OEDT_HW_EXC_NO,     50, "ThreadList API with valid parameter"},                         /*  26 */
    //{u32AttainVariousThreadState), OEDT_HW_EXC_NO,     50, "Cover thread States"},                                         /*  27 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ProcessSpawnInvalidParam), OEDT_HW_EXC_NO,     50, "Try Spawn Process invalid Param"},                             /*  28 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ProcessWhoAmI), OEDT_HW_EXC_NO,     50, "Get running Process ID"},                                      /*  29 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ProcessSelfDelete), OEDT_HW_EXC_TSIM,   50, "Try to Delete the Current Process"},                           /*  30 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ProcessListValid), OEDT_HW_EXC_NO,     50, "Get Process List"},                                            /*  31 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ProcessListInvalidSize), OEDT_HW_EXC_NO,     50, "Try Get Process List with size invalid"},                      /*  32 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetPCBValidParam), OEDT_HW_EXC_NO,     50, "Get PCB Valid Param"},                                         /*  33 */
{OEDT_SIMPLE_TEST_FUNCTION(u32QueryPCBInvalidID), OEDT_HW_EXC_NO,     50, "Try Get PCB with invalid ID"},                                 /*  34 */
{OEDT_SIMPLE_TEST_FUNCTION(u32QueryPCBInvalidAddress), OEDT_HW_EXC_NO,     50, "Try Get PCB with Address NULL"},                               /*  35 */
    /*OSAL Thread API test functions*/                                                                                  

    /*OSAL Semaphore API test functions*/
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateCloseDelSemVal), OEDT_HW_EXC_NO,   50, "Create Close Delete Semaphore"},                                 /*  36 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateSemNameNULL), OEDT_HW_EXC_NO,   50, "Try to Create Semaphore with NULL name"},                        /*  37 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateSemHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Create Semaphore with NULL handle"},                      /*  38 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateCloseDelSemNameMAX), OEDT_HW_EXC_NO,   50, "Create Close Del Semaphore with name 32 chars"},                 /*  39 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateSemWithNameExceedsMAX), OEDT_HW_EXC_NO,   50, "Try to Create Semaphore with name more than 32 chars"},          /*  40 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateTwoSemSameName), OEDT_HW_EXC_NO,   50, "Try to Create Two Semaphore with the same name"},                /*  41 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelSemWithNULL), OEDT_HW_EXC_NO,   50, "Try to Delete Semaphore with NULL"},                             /*  42 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelSemWithNonExistingName), OEDT_HW_EXC_NO,   50, "Try to Delete Semaphore not existent"},                          /*  43 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelSemCurrentlyUsed), OEDT_HW_EXC_NO,   50, "Try to Delete Semaphore in use"},                                /*  44 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenSemWithNameNULL), OEDT_HW_EXC_NO,   50, "Try to Open Semaphore with semaphore name OSAL_NULL"},           /*  45 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenSemWithHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Open Semaphore with semaphore handle OSAL_NULL"},         /*  46 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenSemWithNameExceedsMAX), OEDT_HW_EXC_NO,   50, "Try to Open Semaphore with semaphore with name more than MAX"},  /*  47 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenSemWithNonExistingName), OEDT_HW_EXC_NO,   50, "Try to Open Semaphore with non existent name"},                  /*  48 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CloseSemWithInvalidHandle), OEDT_HW_EXC_NO,   50, "Try to Close Semaphore with invalid handle"},                    /*  49 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitPostSemValid), OEDT_HW_EXC_NO,   50, "Wait and Post API Test on Semaphore"},                           /*  50 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitSemWithInvalidHandle), OEDT_HW_EXC_NO,   50, "Wait on semaphore with invalid handle"},                         /*  51 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ReleaseSemWithInvalidHandle), OEDT_HW_EXC_NO,   50, "Post on semaphore with invalid handle"},                         /*  52 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetRegularSemValue), OEDT_HW_EXC_NO,   50, "Query count value of semaphore"},                                /*  53 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetSemValueInvalidHandle), OEDT_HW_EXC_NO,   50, "Get Sem value for an invalid handle"},                           /*  54 */
  //{OEDT_SIMPLE_TEST_FUNCTION(u32WaitPostSemValidExceedsMAX), OEDT_HW_EXC_NO,   50, "Post the semaphore one more than the max semaphore count"},      /*  55 */ forbidden because change in semaphore handling
{OEDT_SIMPLE_TEST_FUNCTION(u32AccessSemMpleThreads), OEDT_HW_EXC_NO,   50, "Multiple threaded operation using semaphores"},                  /*  56 */
{OEDT_SIMPLE_TEST_FUNCTION(u32NoBlockingSemWait), OEDT_HW_EXC_NO,   50, "No Blocking Sem Wait"},                                          /*  57 */
{OEDT_SIMPLE_TEST_FUNCTION(u32BlockingDurationSemWait), OEDT_HW_EXC_NO,   50, "Blocking Duration Test"},                                        /*  58 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitPostSemAfterDel), OEDT_HW_EXC_NO,   50, "Wait and Post after Sem Delete"},                                /*  59 */
    /*OSAL Semaphore API test functions*/

    /*OSAL Event API test functions*/
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateEventNameNULL), OEDT_HW_EXC_NO,   50, "Try to Create an event with event name NULL"},                       /*  60 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateEventHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Create an event with handle NULL"},                           /*  61 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateEventNameMAX), OEDT_HW_EXC_NO,   50, "Try to Create an event with event name set to 32 chars"},            /*  62 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateEventNameExceedsMAX), OEDT_HW_EXC_NO,   50, "Try to Create an event with event name set greater than 32 chars"},  /*  63 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateTwoEventsSameName), OEDT_HW_EXC_NO,   50, "Try to Create one more event with same name"},                       /*  64 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateEventRegularName), OEDT_HW_EXC_NO,   50, "Create an event in the system"},                                     /*  65 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelEventNULL), OEDT_HW_EXC_NO,   50, "Try to Delete an event with NULL name"},                             /*  66 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelEventNonExisting), OEDT_HW_EXC_NO,   50, "Try to Delete an event non existing"},                               /*  67 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelEventNameExceedsMAX), OEDT_HW_EXC_NO,   50, "Try to Delete an event whose name is greater than 32 chars"},        /*  68 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelEventInUse), OEDT_HW_EXC_NO,   50, "Try to Delete an event which is in use"},                            /*  69 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenEventExisting), OEDT_HW_EXC_NO,   50, "Open an event existing"},                                            /*  70 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenEventNameNULL), OEDT_HW_EXC_NO,   50, "Try to Open an event with name as NULL"},                            /*  71 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenEventHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Open an event with handle as NULL"},                          /*  72 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenEventNonExisting), OEDT_HW_EXC_NO,   50, "Try to Open an event non existing"},                                 /*  73 */
{OEDT_SIMPLE_TEST_FUNCTION(u32OpenEventNameExceedsMAX), OEDT_HW_EXC_NO,   50, "Try to Open an event with name greater than 32 chars"},              /*  74 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CloseEventHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Close an event with handle NULL"},                            /*  75 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CloseEventHandleInval), OEDT_HW_EXC_NO,   50, "Try to Close an event with handle invalid"},                         /*  76 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetEventStatusHandleValid), OEDT_HW_EXC_NO,   50, "Get the event status for an event"},                                 /*  77 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetEventStatusHandleNULL), OEDT_HW_EXC_NO,   50, "Try Get the event status for event handle NULL"},                    /*  78 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetEventStatusHandleInval), OEDT_HW_EXC_NO,   50, "Try Get the event status for event handle invalid"},                 /*  79 */
{OEDT_SIMPLE_TEST_FUNCTION(u32PostEventHandleNULL), OEDT_HW_EXC_NO,   50, "Try Post event handle NULL"},                                        /*  80 */
{OEDT_SIMPLE_TEST_FUNCTION(u32PostEventHandleInval), OEDT_HW_EXC_NO,   50, "Try Post event handle invalid"},                                     /*  81 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitAndPostEventVal), OEDT_HW_EXC_NO,   50, "Wait and Post event"},                                               /*  82 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitEventHandleNULL), OEDT_HW_EXC_NO,   50, "Try Wait with NULL for event handle"},                               /*  83 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitEventHandleInval), OEDT_HW_EXC_NO,   50, "Try Wait with invalid event handle"},                                /*  84 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitEventWithoutTimeout), OEDT_HW_EXC_NO,   50, "Try Wait for an unposted event without timeout"},                    /*  85 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitEventWithTimeout), OEDT_HW_EXC_NO,   50, "Try Wait for an unposted event with timeout"},                       /*  86 */
 //{OEDT_SIMPLE_TEST_FUNCTION(u32SyncEventMultipleThreads), OEDT_HW_EXC_NO,   50, "Synchronise Threads using events"},                                  /*  87 */
{OEDT_SIMPLE_TEST_FUNCTION(u32XORFlagPostEvent), OEDT_HW_EXC_NO,   50, "Post With XOR Flag"},                                                /*  88 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ReplaceFlagPostEvent), OEDT_HW_EXC_NO,   50, "Post With Replace Flag"},                                            /*  89 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ORFlagWaitEvent), OEDT_HW_EXC_NO,   50, "Test OR Wait"},                                                      /*  90 */
{OEDT_SIMPLE_TEST_FUNCTION(u32NoBlockingEventWait), OEDT_HW_EXC_NO,   50, "Test No Blocking Event Wait"},                                       /*  91 */
{OEDT_SIMPLE_TEST_FUNCTION(u32BlockingDurationEventWait), OEDT_HW_EXC_NO,   50, "Test Wait Duration in a Blocking Event Wait"},                       /*  92 */
{OEDT_SIMPLE_TEST_FUNCTION(u32WaitPostEventAfterDelete), OEDT_HW_EXC_NO,   50, "Try Wait Post after Event Delete"},                                  /*  93 */
{OEDT_SIMPLE_TEST_FUNCTION(u32EventDeletewithoutclose), OEDT_HW_EXC_NO ,  50, "Event Delete before close"},
    
    /*OSAL Event API test functions*/

    /*OSAL Timer API test functions*/
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateTimerWithCallbackNULL), OEDT_HW_EXC_NO,   50, "Try to Create a Timer with Callback NULL"},                      /*  94 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelTimerNonExistent), OEDT_HW_EXC_NO,   50, "Try to Create an Timer not existent in the system"},             /*  95 */
{OEDT_SIMPLE_TEST_FUNCTION(u32DelTimerWithHandleNULL), OEDT_HW_EXC_NO,   50, "Try to Delete a Timer with handle NULL"},                        /*  96 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateDelTimer), OEDT_HW_EXC_NO,   50, "Create and Delete a Timer"},                                     /*  97 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SetTimeTimerStTimeOrIntToNULL), OEDT_HW_EXC_NO,   50, "Use various combinations of fields for Set Time API"},           /*  98 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SetTimeTimerNonExistent), OEDT_HW_EXC_NO,   50, "Try to Set Timer for a non existent Timer"},                     /*  99 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetTimeTimerNonExistent), OEDT_HW_EXC_NO,   50, "Try to Get Timer for a non existent Timer"},                     /* 100 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetTimeTimerWithDiffParam), OEDT_HW_EXC_NO,   50, "Try different valid/invalid param for Get Time"},                /* 101 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SetResetTwoTimers), OEDT_HW_EXC_NO,   50, "Set and Reset Two timers on two threads"},                       /* 102 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetElaspedTimeFromRef), OEDT_HW_EXC_NO,   50, "Elapsed API Test"},                                              /* 103 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SetOsalTimerInvalStruct), OEDT_HW_EXC_NO,   50, "Try to Set OsalTimer with invalid structure "},                  /* 104 */
    //{u32GetOsalTimerWithoutPrevSet), OEDT_HW_EXC_NO,   50, "Try to Get OsalTimer without a previous Set"},                 /* 105 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SetOsalTimerGetOsalTimer), OEDT_HW_EXC_NO,   50, "Set OsalTimer and Get OsalTimer"},                               /* 106 */
{OEDT_SIMPLE_TEST_FUNCTION(u32GetOsalTimerNULL), OEDT_HW_EXC_NO,   50, "Get OsalTimer passing NULL"},                                    /* 107 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CyclePhaseTimeCheck), OEDT_HW_EXC_NO,   50, "Cycle Time Phase Time check"},                                   /* 108 */
{OEDT_SIMPLE_TEST_FUNCTION(u32ChangeSystemTimeTest), OEDT_HW_EXC_NO,   50, "Change System Time while timer callback active"},                /* 108 */
{OEDT_SIMPLE_TEST_FUNCTION(U32MaxCallHandleCheck), OEDT_HW_EXC_NO,   50, "Check MAX Call Back Handles"}, 
    //{OEDT_SIMPLE_TEST_FUNCTION(u32TimerPerformanceST_Counter), OEDT_HW_EXC_NO, 1000, "TimerPerformanceST_Counter"},                                    /* 109 */
    //{OEDT_SIMPLE_TEST_FUNCTION(u32TimerPerformanceMT_Counter), OEDT_HW_EXC_NO, 1000, "TimerPerformanceMT_Counter"},                                    /* 110 */
    //{OEDT_SIMPLE_TEST_FUNCTION(u32TimerPerformanceMT_Time), OEDT_HW_EXC_NO, 1000, "TimerPerformanceMT_Time"},                                       /* 111 */
    /*OSAL Timer API test functions*/

    /*OSAL Shared Memory API test functions*/
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateDeleteNameNULL), OEDT_HW_EXC_NO,   50, "SHCreateDeleteNameNULL"},                                    /* 111 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateNameVal), OEDT_HW_EXC_NO,   50, "SHCreateNameVal"},                                           /* 112 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateDeleteNameExceedNameLength), OEDT_HW_EXC_NO,   50, "SHCreateDeleteNameExceedNameLength"},                        /* 113 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateNameSameNames), OEDT_HW_EXC_NO,   50, "SHCreateNameSameNames"},                                     /* 114 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateSizeZero), OEDT_HW_EXC_NO,   50, "CreateSizeZero"},                                            /* 115 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHDeleteNameInval), OEDT_HW_EXC_NO,   50, "SHDeleteNameInval"},                                         /* 116 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHDeleteNameInUse), OEDT_HW_EXC_NO,   50, "SHDeleteNameInUse"},                                         /* 117 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHOpenCloseDiffModes), OEDT_HW_EXC_NO,   50, "SHOpenCloseDiffModes"},                                      /* 118 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHOpenNameNULL), OEDT_HW_EXC_NO,   50, "SHOpenNameNULL"},                                            /* 119 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHOpenNameInVal), OEDT_HW_EXC_NO,   50, "SHOpenNameInVal"},                                           /* 120 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCloseNameInVal), OEDT_HW_EXC_NO,   50, "SHCloseNameInVal"},                                          /* 121 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateMaxSegment), OEDT_HW_EXC_NO,   50, "SHCreateMaxSegment"},                                        /* 122 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHMemoryMapNonExistentHandle), OEDT_HW_EXC_NO,   50, "u32SHMemoryMapNonExistentHandle"},                           /* 123 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHMemoryMapLimitCheck), OEDT_HW_EXC_NO,   50, "u32SHMemoryMapLimitCheck"},                                  /* 124 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHMemoryMapDiffAccessModes), OEDT_HW_EXC_NO,   50, "u32SHMemoryMapDiffAccessModes"},                             /* 125 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHMemoryThreadAccess), OEDT_HW_EXC_NO,   50, "SHMemoryThreadAccess"},                                      /* 126 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHMemoryScanMemory), OEDT_HW_EXC_NO,   50, "SHMemoryScanMemory"},                                        /* 127 */
{OEDT_SIMPLE_TEST_FUNCTION(u32UnmapNonMappedAddress), OEDT_HW_EXC_NO,   50, "UnmapNonMappedAddress"},                                     /* 128 */
{OEDT_SIMPLE_TEST_FUNCTION(u32UnmapNULLAddress), OEDT_HW_EXC_NO,   50, "UnmapNULLAddress"},                                          /* 129 */
{OEDT_SIMPLE_TEST_FUNCTION(u32UnmapMappedAddress), OEDT_HW_EXC_NO,   50, "UnmapMappedAddress"},                                        /* 130 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHOpenDoubleCloseOnce), OEDT_HW_EXC_NO,   50, "SHOpenDoubleCloseOnce"},                                     /* 131 */
{OEDT_SIMPLE_TEST_FUNCTION(u32SHCreateNameValLoop), OEDT_HW_EXC_NO,   50, "SH:CreateNameValLoop"},

    /*OSAL Shared Memory API test functions*/

    /*OSAL Message Pool API test functions*/
    
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLOpen), OEDT_HW_EXC_NO,     50, "MsgPool Open "},                                               /* 132 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLCreateMessageNULL), OEDT_HW_EXC_NO,     50, "CreateMessageNULL"},                                           
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLCreateSizeZero), OEDT_HW_EXC_NO,     50, "CreateSizeZero"},                                              
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLCreateLocationInVal), OEDT_HW_EXC_NO,     50, "CreateLocationInVal"},                                         
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLGetSMSize), OEDT_HW_EXC_NO,     50, "GetSMSize"},                                                   
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLGetSMContent), OEDT_HW_EXC_NO,     50, "GetSMContent"},                                                
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLGetSMContentDiffMode), OEDT_HW_EXC_NO,     50, "GetSMContentDiffMode"},                                        
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLGetLMSize), OEDT_HW_EXC_NO,     50, "GetLMContent"},                                                
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLGetLMContentDiffMode), OEDT_HW_EXC_NO,     50, "GetLMContentDiffMode"},                                        
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLDeleteMessageNULL), OEDT_HW_EXC_TSIM,   50, "DeleteMessageNULL"},                                           
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLMsgCreateWithNoMSGPLInSH), OEDT_HW_EXC_NO,     50, "MsgCreateWithNoMSGPLInSH"},                                    
{OEDT_SIMPLE_TEST_FUNCTION(u32MSGPLMsgCreateWithNoMSGPLInLM), OEDT_HW_EXC_NO,     50, "MsgCreateWithNoMSGPLInLM"},									/* 143  */
    /*OSAL Message Pool API test functions*/

    /*OSAL Message Queue API test functions*/
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQuePostMsgExcMaxLen), OEDT_HW_EXC_NO,   50, "MsgQuePostMsgExcMaxLen"},                                        /* 150 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CreateMsgQueWithHandleNULL), OEDT_HW_EXC_NO,   50, "CreateMsgQueWithHandleNULL"},                                    /* 151 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueCreateMsgExcMaxLen), OEDT_HW_EXC_NO,   50, "MsgQueCreateMsgExcMaxLen"},                                      /* 152 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueCreateDiffModes), OEDT_HW_EXC_NO,   50, "MsgQueCreateDiffModes"},                                         /* 153 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueCreateSameName), OEDT_HW_EXC_NO,   50, "MsgQueCreateSameName"},                                          /* 154 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueDeleteNameNULL), OEDT_HW_EXC_NO,   50, "MsgQueDeleteNameNULL"},                                          /* 155 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueDeleteNameInval), OEDT_HW_EXC_NO,   50, "MsgQueDeleteNameInval"},                                         /* 156 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueDeleteWithoutClose), OEDT_HW_EXC_NO,   50, "MsgQueDeleteWithoutClose"},                                      /* 157 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueOpenDiffMode), OEDT_HW_EXC_NO,   50, "MsgQueOpenDiffMode"},                                            /* 158 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueTwoThreadMsgPost), OEDT_HW_EXC_NO,   50, "MsgQueTwoThreadMsgPost"},                                        /* 159 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueSingleThreadMsgNotify), OEDT_HW_EXC_NO,   50, "MsgQueSingleThreadMsgNotify"},                                         /* 160 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueSingleThreadMsgNotifyCallback), OEDT_HW_EXC_NO,   50, "MsgQueSingleThreadMsgNotifyCallback"},                                         /* 160 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueThreadMsgNotify), OEDT_HW_EXC_NO,   50, "MsgQueThreadMsgNotify"},                                         /* 160 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueNotifyEvent), OEDT_HW_EXC_NO,   50, "u32MsgQueNotifyEvent"},                                         /* 160 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueQueryStatus), OEDT_HW_EXC_NO,   50, "MsgQueQueryStatus"},                                             /* 161 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueQueryStatusMMParamNULL), OEDT_HW_EXC_NO, 1000, "MsgQueQueryStatusMMParamNULL"},                                  /* 162 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueQueryStatusMLParamNULL), OEDT_HW_EXC_NO, 1000, "MsgQueQueryStatusMLParamNULL"},                                  /* 163 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueQueryStatusCMParamNULL), OEDT_HW_EXC_NO, 1000, "MsgQueQueryStatusCMParamNULL"},                                  /* 164 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueQueryStatusAllParamNULL), OEDT_HW_EXC_NO,   50, "MsgQueQueryStatusAllParamNULL"},                                 /* 165 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQuePostMsgInvalPrio), OEDT_HW_EXC_NO,   50, "MsgQuePostMsgInvalPrio"},                                        /* 166 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQuePostMsgBeyondQueLimit), OEDT_HW_EXC_NO,   50, "MsgQuePostMsgBeyondQueLimit"},                                   /* 167 */
{OEDT_SIMPLE_TEST_FUNCTION(MsgQueSimplePostWaitPerfTest), OEDT_HW_EXC_NO,   50, "MsgQueSimplePostWaitPerfTest"},                                  /* 168 */
{OEDT_SIMPLE_TEST_FUNCTION(MsgQueThreadPostWaitPerfTest), OEDT_HW_EXC_NO,   50, "MsgQueThreadPostWaitPerfTest"},                                  /* 169 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MQOpenDoubleCloseOnce), OEDT_HW_EXC_NO,   50, "MsgQOpenDoubleCloseOnce"},                                     /* 131 */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueNotifyRemove), OEDT_HW_EXC_NO,   50, "MsgQueNotifyRemove"},                                         /* ??? */
{OEDT_SIMPLE_TEST_FUNCTION(u32MsgQueTimeoutTest), OEDT_HW_EXC_NO,	 50, "MsgQueTimeoutTest"},                                                                 /* 225 */

    //{u32MessageQueueStressTest), OEDT_HW_EXC_NO, 1000, "u32MessageQueueStressTest"},                                     /* 170 */
    /*OSAL Message Queue API test functions*/

#if !defined(LSIM) && !defined(GEN3X86)
    /* OSAL FPE Test function */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPZeroDivNoAbort), OEDT_HW_EXC_NO,   50, "FPECheckFPZeroDivNoAbort"},                                         /* 171 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPOverflowNoAbort), OEDT_HW_EXC_NO,   50, "FPECheckFPOverflowNoAbort"},                                        /* 172 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPUnderflowNoAbort), OEDT_HW_EXC_NO,   50, "FPECheckFPUnderflowNoAbort"},                                       /* 173 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPInvalidNoAbort), OEDT_HW_EXC_NO,   50, "FPECheckFPInvalidNoAbort"},                                         /* 174 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckResetFPEFlag), OEDT_HW_EXC_NO,   50, "FPECheckResetFPEFlag"},                                             /* 175 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckResetMultithreads), OEDT_HW_EXC_NO,   50, "FPECheckResetMultithreads"},                                        /* 176 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckMultipleFPEFlags), OEDT_HW_EXC_NO,   50, "FPEMultipleFPEFlags"},                                              /* 177 */
{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPEBitSetMultithreaded), OEDT_HW_EXC_NO,   50, "FPECheckFPEBitSetMultithreaded"},                                   /* 178 */
//{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPEModeChange), OEDT_HW_EXC_NO,   50, "FPECheckFPEModeChange"},        /* OSAL floating point API is not used in Gen3 platform */                                            /* 179 */
//{OEDT_SIMPLE_TEST_FUNCTION(u32CheckFPEModeMultithreaded), OEDT_HW_EXC_NO,   50, "FPECheckFPEModeSetMultithreaded"},       /* OSAL floating point API is not used in Gen3 platform */                                   /* 180 */
    //{u32CheckDefaultFPEMode), OEDT_HW_EXC_NO,   50, "FPECheckDefaultFPEMode"},                                           /* 181 */
    /* OSAL FPE Test functions end */
#endif

#ifdef IOSC_ACTIV
    /*OSAL Shared Memory _NoIOSC_ API test functions*/
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateDeleteNameNULL),             OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateDeleteNameNULL"},                          /* 182 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateNameVal),                    OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateNameVal"},                                 /* 183 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateDeleteNameExceedNameLength), OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateDeleteNameExceedNameLength"},              /* 184 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateNameSameNames),              OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateNameSameNames"},                           /* 185 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateSizeZero),                   OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateSizeZero"},                                /* 186 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCDeleteNameInval),                  OEDT_HW_EXC_NO,   50, "SH_noIOSC:DeleteNameInval"},                               /* 187 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCDeleteNameInUse),                  OEDT_HW_EXC_NO,   50, "SH_noIOSC:DeleteNameInUse"},                               /* 188 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCOpenCloseDiffModes),               OEDT_HW_EXC_NO,   50, "SH_noIOSC:OpenCloseDiffModes"},                            /* 189 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCOpenNameNULL),                     OEDT_HW_EXC_NO,   50, "SH_noIOSC:OpenNameNULL"},                                  /* 190 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCOpenNameInVal),                    OEDT_HW_EXC_NO,   50, "SH_noIOSC:OpenNameInVal"},                                 /* 191 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCloseNameInVal),                   OEDT_HW_EXC_NO,   50, "SH_noIOSC:CloseNameInVal"},                                /* 192 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateMaxSegment),                 OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateMaxSegment"},                              /* 193 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCMemoryMapNonExistentHandle),       OEDT_HW_EXC_NO,   50, "SH_noIOSC:MemoryMapNonExistentHandle"},                    /* 194 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCMemoryMapLimitCheck),              OEDT_HW_EXC_NO,   50, "SH_noIOSC:MemoryMapLimitCheck"},                           /* 195 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCMemoryMapDiffAccessModes),         OEDT_HW_EXC_NO,   50, "SH_noIOSC:MemoryMapDiffAccessModes"},                      /* 196 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCMemoryThreadAccess),               OEDT_HW_EXC_NO,   50, "SH_noIOSC:MemoryThreadAccess"},                            /* 197 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCMemoryScanMemory),                 OEDT_HW_EXC_NO,   50, "SH_noIOSC:MemoryScanMemory"},                              /* 198 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCUnmapNonMappedAddress),            OEDT_HW_EXC_NO,   50, "SH_noIOSC:UnmapNonMappedAddress"},                         /* 199 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCUnmapNULLAddress),                 OEDT_HW_EXC_NO,   50, "SH_noIOSC:UnmapNULLAddress"},                              /* 200 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCUnmapMappedAddress),               OEDT_HW_EXC_NO,   50, "SH_noIOSC:UnmapMappedAddress"},                            /* 201 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCOpenDoubleCloseOnce),              OEDT_HW_EXC_NO,   50, "SH_noIOSC:OpenDoubleCloseOnce"},                           /* 202 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32SHnoIOSCCreateNameValLoop),                OEDT_HW_EXC_NO,   50, "SH_noIOSC:CreateNameValLoop"},
    /*OSAL Shared Memory _NoIOSC_API test functions*/

    /*OSAL Message Queue API test functions*/
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCPostMsgExcMaxLen),           OEDT_HW_EXC_NO,   50, "MQ_noIOSC:PostMsgExcMaxLen"},                                    /* 203 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCCreateMsgQueWithHandleNULL), OEDT_HW_EXC_NO,   50, "MQ_noIOSC:CreateMsgQueWithHandleNULL"},                          /* 204 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCCreateMsgExcMaxLen),         OEDT_HW_EXC_NO,   50, "MQ_noIOSC:CreateMsgExcMaxLen"},                                  /* 205 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCCreateDiffModes),            OEDT_HW_EXC_NO,   50, "MQ_noIOSC:CreateDiffModes"},                                     /* 206 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCCreateSameName),             OEDT_HW_EXC_NO,   50, "MQ_noIOSC:CreateSameName"},                                      /* 207 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCDeleteNameNULL),             OEDT_HW_EXC_NO,   50, "MQ_noIOSC:DeleteNameNULL"},                                      /* 208 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCDeleteNameInval),            OEDT_HW_EXC_NO,   50, "MQ_noIOSC:DeleteNameInval"},                                     /* 209 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCDeleteWithoutClose),         OEDT_HW_EXC_NO,   50, "MQ_noIOSC:DeleteWithoutClose"},                                  /* 210 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCOpenDiffMode),               OEDT_HW_EXC_NO,   50, "MQ_noIOSC:OpenDiffMode"},                                        /* 211 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCTwoThreadMsgPost),           OEDT_HW_EXC_NO,   50, "MQ_noIOSC:TwoThreadMsgPost"},                                    /* 212 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCThreadMsgNotify),            OEDT_HW_EXC_NO,   50, "MQ_noIOSC:ThreadMsgNotify"},                                     /* 213 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCQueryStatus),                OEDT_HW_EXC_NO,   50, "MQ_noIOSC:QueryStatus"},                                         /* 214 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCQueryStatusMMParamNULL),     OEDT_HW_EXC_NO, 1000, "MQ_noIOSC:QueryStatusMMParamNULL"},                              /* 215 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCQueryStatusMLParamNULL),     OEDT_HW_EXC_NO, 1000, "MQ_noIOSC:QueryStatusMLParamNULL"},                              /* 216 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCQueryStatusCMParamNULL),     OEDT_HW_EXC_NO, 1000, "MQ_noIOSC:QueryStatusCMParamNULL"},                              /* 217 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCQueryStatusAllParamNULL),    OEDT_HW_EXC_NO,   50, "MQ_noIOSC:QueryStatusAllParamNULL"},                             /* 218 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCPostMsgInvalPrio),           OEDT_HW_EXC_NO,   50, "MQ_noIOSC:PostMsgInvalPrio"},                                    /* 219 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCPostMsgBeyondQueLimit),      OEDT_HW_EXC_NO,   50, "MQ_noIOSC:PostMsgBeyondQueLimit"},                               /* 220 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCSimplePostWaitPerfTest),     OEDT_HW_EXC_NO,   50, "MQ_noIOSC:SimplePostWaitPerfTest"},                              /* 221 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCThreadPostWaitPerfTest),     OEDT_HW_EXC_NO,   50, "MQ_noIOSC:ThreadPostWaitPerfTest"},                              /* 222 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCOpenDoubleCloseOnce),        OEDT_HW_EXC_NO,   50, "MQ_noIOSC:OpenDoubleCloseOnce"},                                 /* 223 */
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCTimeoutCheck),               OEDT_HW_EXC_NO,   30, "MQ_noIOSC:TimeoutCheck"},
    {OEDT_SIMPLE_TEST_FUNCTION(u32MQnoIOSCPostWaitMsgPriowise),        OEDT_HW_EXC_NO,   50, "MQ_noIOSC:PostWaitMsgPriowise"}, /*  */
    //{u32MessageQueueStressTest,         OEDT_HW_EXC_NO, 1000, "u32MessageQueueStressTest"},                                       /* 224 */
    /*OSAL Message Queue API test functions*/
#endif

{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
				/* !!!-----------------------REGISTRY--------------------------!!! */
				/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pREGISTRY_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32REGISTRYBasic1Test), OEDT_HW_EXC_NO,    2, "key creation and dir reading"},      /*   0 */
{OEDT_SIMPLE_TEST_FUNCTION(u32REGISTRYBasic2Test), OEDT_HW_EXC_NO,    2, "subkey create and content check"},   /*   1 */
{OEDT_SIMPLE_TEST_FUNCTION(u32REGISTRYBasic3Test), OEDT_HW_EXC_NO,    2, "key op's using alternative ctrls"},  /*   2 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDevOpenClose), OEDT_HW_EXC_NO,	1000, "OpenCloseRegistry"},                 /*   3 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDevOpenCloseDiffModes), OEDT_HW_EXC_NO,	1000, "OpenCloseDiffModes"},                /*   4 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDevOpenCloseInvalParam), OEDT_HW_EXC_NO,	1000, "OpenCloseInvalParam"},               /*   5 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegCreateKey), OEDT_HW_EXC_NO,	1000, "Create Key"},                        /*   6 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegOpenCloseKey), OEDT_HW_EXC_NO,	1000, "Open and Close Key"},                /*   7 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegGetVersion), OEDT_HW_EXC_NO,	1000, "Get Version "},                      /*   8 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegSetGetValue), OEDT_HW_EXC_NO,	1000, "Set and Get RegValue "},             /*   9 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegWriteValue), OEDT_HW_EXC_NO,	1000, "WriteRegValue"},                     /*  10 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegSetQueryValue), OEDT_HW_EXC_NO,	1000, "Set and Query RegValue"},            /*  11 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegRemoveValue), OEDT_HW_EXC_NO,	1000, "RemoveRegValue"},                    /*  12 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDevReOpen), OEDT_HW_EXC_NO,	1000, "DevReOpen"},                         /*  13 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDevCloseAlreadyClosed), OEDT_HW_EXC_NO,	1000, "DevCloseAlreadyClosed"},             /*  14 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegCreateMultipleValues), OEDT_HW_EXC_NO,	1000, "CreateMultipleValues"},              /*  15 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegCreateMultipleKeys), OEDT_HW_EXC_NO,	1000, "CreateMultipleKeys"},                /*  16 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegReOpenKey), OEDT_HW_EXC_NO,	1000, "ReOpenKey"},                         /*  17 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegCloseKeyAlreadyClosed), OEDT_HW_EXC_NO,	1000, "CloseKeyAlreadyClosed"},             /*  18 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDeleteKeyWithValues), OEDT_HW_EXC_NO,	1000, "DeleteKeyWithValues"},               /*  19 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegOpenKeyDiffAccessModes), OEDT_HW_EXC_NO,	1000, "OpenKeyDiffAccessModes"},            /*  20 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegWriteNumericValue), OEDT_HW_EXC_NO,	1000, "WriteNumericValue"},                 /*  21 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDelKeyAlreadyRemoved), OEDT_HW_EXC_NO,	1000, "DelKeyAlreadyRemoved"},              /*  22 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegDelKeyStructure), OEDT_HW_EXC_NO,	1000, "DelKeyStructure"},                   /*  23 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegSearchKey), OEDT_HW_EXC_NO,	1000, "SearchKey"},                         /*  24 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegOpenAsDir), OEDT_HW_EXC_NO,	1000, "OpenAsDir"},                         /*  25 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegBlockCreateAndRemove), OEDT_HW_EXC_NO,   20, "Create and Remove Registry Block"},	/*  26 */
{OEDT_SIMPLE_TEST_FUNCTION(u32RegUseInvalidKey), OEDT_HW_EXC_NO,   20, "create, remove using invalid key"},	/*  27 */
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pEOLLIB_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevOpenClose), OEDT_HW_EXC_NO,	100, "open EOL Device"},			/* 0  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevReOpen), OEDT_HW_EXC_NO,	100, "ReOpen EOL Device"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevCloseAlreadyClosed), OEDT_HW_EXC_NO,	100, "Close allready closed EOL device"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevCloseInvalParam), OEDT_HW_EXC_NO,	100, "close EOL Device with invalid parameters"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevOpenCloseDiffAccessMode), OEDT_HW_EXC_NO,	100, "Open Close EOL Dvice with different AccessMode"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLDevOpenCloseInvalAccessMode), OEDT_HW_EXC_NO,	100, "Open Close EOL Dvice with invalid AccessMode"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMReadOneByte), OEDT_HW_EXC_NO,	100, "EOL System Read one Byte"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMReadInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL System Read Inv Buffer"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMReadWholeBlock ), OEDT_HW_EXC_NO,	100, "EOL System Read whole Block"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMReadBlockInvalidSize	), OEDT_HW_EXC_NO,	100, "EOL System Read Block Inv Size"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMWriteWholeBlock	), OEDT_HW_EXC_DUAL_OS,	100, "EOL System Write whole block"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLSYSTEMWriteInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL System Write Inv Buffer"}, 	/* 10  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYReadOneByte ), OEDT_HW_EXC_NO,	100, "EOL Country Read one Byte"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYReadInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL Country Read Inv Buffer"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYReadWholeBlock	), OEDT_HW_EXC_NO,	100, "EOL Country Read whole Block "},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYReadBlockInvalidSize	), OEDT_HW_EXC_NO,	100, "EOL Country Read Block Inv Size"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYWriteWholeBlock	), OEDT_HW_EXC_DUAL_OS,	100, "EOL Country Write whole block"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLCOUNTRYWriteInvalBuffer ), OEDT_HW_EXC_NO,	100, "EOL Country Write Inv Buffer"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDReadOneByte), OEDT_HW_EXC_NO,	100, "EOL BRAND Read one Byte"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDReadInvalBuffer), OEDT_HW_EXC_NO,	100, "EOL BRAND Read Inv Buffer"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDReadWholeBlock), OEDT_HW_EXC_NO,	100, "EOL BRAND Read whole Block"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDReadBlockInvalidSize	), OEDT_HW_EXC_NO,	100, "EOL BRAND Read Block Inv Size"},                           /* 20 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDWriteWholeBlock ), OEDT_HW_EXC_DUAL_OS,	100, "EOL BRAND Write whole block"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLBRANDWriteInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL BRAND Write Inv Buffer"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCReadOneByte	), OEDT_HW_EXC_NO,	100, "EOL RVC Read one Byte"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCReadInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL RVC Read Inavlid Parameters"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCReadWholeBlock	), OEDT_HW_EXC_NO,	100, "EOL RVC Read whole Block"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCReadBlockInvalidSize	), OEDT_HW_EXC_NO,	100, "EOL RVC Read Block Inavlid Size"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCWriteWholeBlock	), OEDT_HW_EXC_NO,	100, "EOL RVC Write Block Inavlid Size"},
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLRVCWriteInvalBuffer	), OEDT_HW_EXC_NO,	100, "EOL RVC Write Block Inavlid Buffer"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLGetModuleIdentifier	), OEDT_HW_EXC_NO,	100, "EOL Getmodule Identifier"},
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32DiagEOLGetModuleIdentifierInvalParam	), OEDT_HW_EXC_NO,	100, "EOL Getmodule Identifier Invalid Parameters"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

#ifdef LSIM
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------------------- SWC ---------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


OEDT_FUNC_CFG oedt_pSWC_Entries_reg[] =
{
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevOpen) ,OEDT_HW_EXC_NO,   	 50, "SWC:DeviceOpen"},        	/*   1  */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDev_MultiOpen) ,OEDT_HW_EXC_NO,   	 50, "SWC:DeviceMultiOpen"},        	/*   2 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevclose) ,OEDT_HW_EXC_NO,   	 50, "SWC:Device Close"},        	/*   3 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDev_Multiclose) ,OEDT_HW_EXC_NO,   	 50, "SWC:DeviceMultiClose"},        	/*   4 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_GetVersion) ,OEDT_HW_EXC_NO,   	 50, "SWC:IOControl Get Version"},        	/*   5 */
   {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32SteeringWheelDevIOControl_SetCallBack) ,OEDT_HW_EXC_NO,   	 50, "SWC:IOControl Set Callback"},        	/*   6 */
   {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif



/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------DEV-FFD --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pFFD_DEV_RegressionEntries[] =
{
#ifndef TSIM_OSAL
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevOpenClose), OEDT_HW_EXC_NO, 10, "DEV:OpenClose"},   
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevOpenWithInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:OpenWithInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevCloseWithInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:CloseWithInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevOpenCloseSeveralTimes), OEDT_HW_EXC_NO, 10, "DEV:OpenCloseSeveralTimes"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevWriteRead), OEDT_HW_EXC_NO, 10, "DEV:WriteRead"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevWriteReadInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:WriteReadInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRawSize), OEDT_HW_EXC_NO, 10, "DEV:GetRawSize"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRawSizeInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:GetRawSizeInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRawData), OEDT_HW_EXC_NO, 10, "DEV:GetRawData"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRawDataInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:GetRawDataInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSetRawData), OEDT_HW_EXC_NO, 10, "DEV:SetRawData"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSetRawDataInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:SetRawDataInvalidParam"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveNow), OEDT_HW_EXC_NO, 10, "DEV:SaveNow"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveNowInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:SaveNowInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevReload), OEDT_HW_EXC_NO, 10, "DEV:Reload"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevReloadInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:ReloadInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveReloadCompare), OEDT_HW_EXC_NO, 10, "DEV:SaveReloadCompare"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSeek), OEDT_HW_EXC_NO, 10, "DEV:Seek"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSeekInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:SeekInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevWriteReadWithSeek), OEDT_HW_EXC_NO, 10, "DEV:WriteReadWithSeek"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRomDataVersion), OEDT_HW_EXC_NO, 10, "DEV:GetRomDataVersion"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetRomDataVersionInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:GetRomDataVersionInvalidParam"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetTotolFlashSize), OEDT_HW_EXC_NO, 10, "DEV:GetTotolFlashSize"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetTotolFlashSizeInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:GetTotolFlashSizeInvalidParam"},  
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetDataSetSize), OEDT_HW_EXC_NO, 10, "DEV:GetDataSetSize"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetDataSetSizeInvalidParam), OEDT_HW_EXC_NO, 10, "DEV:GetDataSetSizeInvalidParam"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveCompleteFlashArea), OEDT_HW_EXC_NO, 30, "DEV:SaveCompleteFlashArea"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevReadSaveAssertMode), OEDT_HW_EXC_NO, 10, "DEV:ReadSaveAssertMode"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveBackupFile) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevSaveBackupFile"},
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevSaveBackupFileInvalidParam) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevSaveBackupFileInvalidParam"},
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevLoadBackupFile) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevLoadBackupFile"},
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevLoadBackupFileInvalidParam) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevLoadBackupFileInvalidParam"},
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetInfoReadData) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevGetInfoReadData"},
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevGetInfoReadDataInvalidParam) ,OEDT_HW_EXC_NO, 10, "DEV:u32FFDDevLoadBackupFileInvalidParam"},
#endif /*#ifndef TSIM_OSAL*/
#if 0  //def LSIM
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevLSIMWriteBeforeReboot), OEDT_HW_EXC_NO, 10, "DEV:LSIM:Write before reboot"}, 
{OEDT_SIMPLE_TEST_FUNCTION(u32FFDDevLSIMReadAfterReboot), OEDT_HW_EXC_NO, 10, "DEV:LSIM:Read after reboot"}, 
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};


OEDT_FUNC_CFG oedt_pRAMDISK_CommonFS_RegressionEntries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpenClosedevice), OEDT_HW_EXC_NO,   100, "Open Close Device"},                /* 0  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpendevInvalParm), OEDT_HW_EXC_NO,   100, "Open Device with Inval param"},     /* 1  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReOpendev), OEDT_HW_EXC_NO,   100, "reopen Device"},                    /* 2  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpendevDiffModes), OEDT_HW_EXC_NO,   100, "Open Device in different modes"},   /* 3  */
/*{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSClosedevAlreadyClosed), OEDT_HW_EXC_NO, 100, "Close devide already closed"},*//* causes exception and can not be taken care at OSAL level */  /* 4  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpenClosedir), OEDT_HW_EXC_NO,   100, "Open / Close Dir"},                 /* 5  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpendirInvalid ), OEDT_HW_EXC_NO,   100, "Open inval dir"},                   /* 6  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateDelDir), OEDT_HW_EXC_NO,   100, "Create / Del Dir"},                 /* 7  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateDelSubDir), OEDT_HW_EXC_NO,   100, "Create sub Directory"},             /* 8  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateDirInvalName), OEDT_HW_EXC_NO,   100, "Create dir with inval name"},       /* 9  */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSRmNonExstngDir), OEDT_HW_EXC_NO,   100, "Remove non exsisting Dir"},         /* 10 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSRmDirUsingIOCTRL), OEDT_HW_EXC_NO,   100, "Remove Dir using IOCTRL"},          /* 11 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetDirInfo), OEDT_HW_EXC_NO,   100, "Get Dir info "},                    /* 12 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpenDirDiffModes), OEDT_HW_EXC_NO,   100, "Open Dir in diff modes"},           /* 13 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReOpenDir), OEDT_HW_EXC_NO,   100, "Reopen Dir"},                       /* 14 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSDirParallelAccess), OEDT_HW_EXC_NO,   100, "Dir Paralell Access "},             /* 15 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateDirMultiTimes), OEDT_HW_EXC_NO,   100, "Create Dir Multiple times"},        /* 16 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateRemDirInvalPath), OEDT_HW_EXC_NO, 100, "Remove Dir with inval path"},       /* 17 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateRmNonEmptyDir), OEDT_HW_EXC_NO,   100, "Remove non empty dir"},             /* 18 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCopyDir), OEDT_HW_EXC_NO,   100, "Copy Dir"},                         /* 19 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSMultiCreateDir), OEDT_HW_EXC_NO,   100, "Create Multiple dir"},              /* 20 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateSubDir), OEDT_HW_EXC_NO,   100, "Create sub dir"},                   /* 21 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSDelInvDir), OEDT_HW_EXC_NO,   100, "Delete Inval Dir"},                 /* 22 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCopyDirRec), OEDT_HW_EXC_NO,   100, "Copy Dir recursively"},             /* 23 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSRemoveDir), OEDT_HW_EXC_NO,   100, "Remove dir"},                       /* 24 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateDel), OEDT_HW_EXC_NO,   100, "Create / Del file"},                /* 25 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileOpenClose), OEDT_HW_EXC_NO,   100, "Open Close file"},                  /* 26 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileOpenInvalPath), OEDT_HW_EXC_NO,   100, "open file with inval path"},        /* 27 */
   /*This testcase is removed because osal manual doesn't tells that, 
   for invalid access parameter OSAL_IOOpen() would return error.*/
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileOpenInvalParam), OEDT_HW_EXC_NO,   100, "open file with inval param"},       /* 28 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReOpen), OEDT_HW_EXC_NO,   100, "Reopen the file"},                  /* 29 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileRead), OEDT_HW_EXC_NO,   100, "File read"},                        /* 30 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileWrite), OEDT_HW_EXC_NO,   100, "File write"},                       /* 31 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetPosFrmBOF), OEDT_HW_EXC_NO,   100, "Get Posistion from BOF"},           /* 32 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetPosFrmEOF), OEDT_HW_EXC_NO,   100, "Get Posistion from EOF"},           /* 33 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_NO,   100, "Read neg offset from BOF"},   /* 34 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_NO,   100, "Read beyond EOF"},            /* 35 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_NO,   100, "read offset from BOF"},          /* 36 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_NO,   100, "read offset from EOF"},          /* 37 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_NO,   100, "Read every nth byte from BOF "},/* 38 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_NO,   100, "Read every nth byte from EOF"}, /* 39 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetFileCRC), OEDT_HW_EXC_NO,   100, "Get file CRC"},                     /* 40 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadAsync), OEDT_HW_EXC_NO,   100, "Read Async"},                       /* 41 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSLargeReadAsync), OEDT_HW_EXC_NO,   100, "Large Read Async"},                 /* 42 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteAsync), OEDT_HW_EXC_NO,   100, "Write Async"},                      /* 43 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSLargeWriteAsync), OEDT_HW_EXC_NO,   100, "Large Write Async"},                /* 44 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteOddBuffer), OEDT_HW_EXC_NO,   100, "Write to odd buf"},                 /* 45 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteFileWithInvalidSize), OEDT_HW_EXC_NO, 100, "Write With Inval Size"},        /* 46 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteFileInvalidBuffer), OEDT_HW_EXC_NO, 100, "Write Inval Buf"},              /* 47 */
   // This crashes the system !!! TO BE Investigated{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteFileStepByStep), OEDT_HW_EXC_NO, 100, 	"Write Step By Step"}, /* 48 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetFreeSpace), OEDT_HW_EXC_NO,   100, "Get Free Space"},                   /* 49 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSGetTotalSpace), OEDT_HW_EXC_NO,   100, "Get Total Space"},                  /* 50 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_NO, 100,"Fil Open Close NonExstng"},         /* 51 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileDelWithoutClose), OEDT_HW_EXC_NO,   100, "File Del Without Close"},           /* 52 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_NO,   100, "Set Pos Diff Off"},                 /* 53 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSCreateFileMultiTimes), OEDT_HW_EXC_NO,  100, "Create File Multi Times"},          /* 54 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateUnicodeName), OEDT_HW_EXC_NO, 100, "File Create Unicode Name"},         /* 55 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateInvalName), OEDT_HW_EXC_NO,   100, "File Create Inval Name"},           /* 56 */
   //{(OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateLongName), OEDT_HW_EXC_NO, 100,"File Create Long Name"},            /* 57 */ 
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateDiffModes), OEDT_HW_EXC_NO,   100, "File Create Diff Modes"},           /* 58 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileCreateInvalPath), OEDT_HW_EXC_NO,   100, "File Create Inval Path"},           /* 59 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSStringSearch), OEDT_HW_EXC_NO,   100, "String Search"},                    /* 60 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileOpenDiffModes), OEDT_HW_EXC_NO,   100, "File Open Diff Modes"},             /* 61 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadAccessCheck), OEDT_HW_EXC_NO,   100, "File Read Access Check"},           /* 62 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileWriteAccessCheck), OEDT_HW_EXC_NO,   100, "File Write Access Check"},          /* 63 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileReadSubDir), OEDT_HW_EXC_NO,   100, "File Read SubDir"},                 /* 64 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSFileWriteSubDir), OEDT_HW_EXC_NO,   100, "File Write SubDir"},                /* 65 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile), OEDT_HW_EXC_NO, 100, "Read/Write 1Kb"},         /* 66 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile), OEDT_HW_EXC_NO, 100, "Read/Write 10Kb"},        /* 67 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile), OEDT_HW_EXC_NO, 100, "Read/Write 100Kb"},       /* 68 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile), OEDT_HW_EXC_NO, 100, "Read/Write 1MB"},         /* 69 */
   /* below 4 functions are moved to performance data set - sak9kor */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 1Kb for 1000 times"},   /* 70 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times), OEDT_HW_EXC_NO, 100, "Read/Write 10Kb for 1000 times"},  /* 71 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times), OEDT_HW_EXC_NO, 100, "Read/Write 100Kb for 100 times"},  /* 72 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times), OEDT_HW_EXC_NO, 100, "Read/Write 1MB for 16 times"},     /* 73 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSRenameFile), OEDT_HW_EXC_NO,   100, "Rename File"},                      /* 74 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteFrmBOF), OEDT_HW_EXC_NO,   100, "Write From BOF"},                   /* 75 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteFrmEOF), OEDT_HW_EXC_NO,   100, "Write From EOF"},                   /* 76 */
   /* below 2 functions are moved to data set 1 - sak9kor*/
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSLargeFileRead), OEDT_HW_EXC_NO,   100, "Large File Read"},                  /* 77 */
//{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSLargeFileWrite), OEDT_HW_EXC_NO,   100, "Large File Write"},                 /* 78 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSOpenCloseMultiThread), OEDT_HW_EXC_NO, 100, "Open Close Multi Thread"},         /* 79 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteMultiThread), OEDT_HW_EXC_NO,   100, "Write Multi Thread"},               /* 80 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSWriteReadMultiThread), OEDT_HW_EXC_NO,   100, "Write Read Multi Thread"},          /* 81 */
{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RAMDISK_CommonFSReadMultiThread), OEDT_HW_EXC_NO,   100, "Read Multi Thread"},                /* 82 */
  //{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileGetExt), OEDT_HW_EXC_NO,   100, "Read file/directory contents"},
  //{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSFileGetExt2), OEDT_HW_EXC_NO,   100, "Read file/directory contents with 2 IOCTL"},
  //{OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32FFS3_CommonFSOpenDirIOCTL), OEDT_HW_EXC_NO,   100, "Open directory using IOCTL"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pCryptcard_CommonFS_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSOpenClosedevice), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open Close Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSOpendevInvalParm), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open Device with Inval param"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSReOpendev), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: reopen Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSOpendevDiffModes), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open Device in different modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSClosedevAlreadyClosed), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Close devide already closed"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSOpenClosedir), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open / Close Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSOpendirInvalid), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open inval dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSGetDirInfo), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Get Dir info "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSReOpenDir), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Reopen Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileOpenClose), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Open Close file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileOpenInvalPath), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: open file with inval path"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReOpen), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Reopen the file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileRead), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: File read"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSGetPosFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Get Posistion from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSGetPosFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Get Posistion from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read neg offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read beyond EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: read offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: read offset from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read every nth byte from BOF "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read every nth byte from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSGetFileCRC), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Get file CRC"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSReadAsync), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read Async"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: File Open Close Non Exstng"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Set Pos Diff Off"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSFileOpenDiffModes), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: File Open Diff Modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSReadDirExt), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read Dir ext"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_CommonFSReadDirExt2), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C1: Read dir ext2"},
   //Since cryptcard is mounted as READ-ONLY, test cases which try to write into it are removed.
   //{u32Cryptcard_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_NO, 10, "C1: Read dir ext part by part"},
   //{u32Cryptcard_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_NO, 10, "C1: Read dir ext2 part by part"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pCryptcard2_CommonFS_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSOpenClosedevice), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open Close Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSOpendevInvalParm), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open Device with Inval param"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSReOpendev), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: reopen Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSOpendevDiffModes), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open Device in different modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSClosedevAlreadyClosed), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Close devide already closed"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSOpenClosedir), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open / Close Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSOpendirInvalid), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open inval dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSGetDirInfo), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Get Dir info "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSReOpenDir), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Reopen Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileOpenClose), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Open Close file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileOpenInvalPath), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: open file with inval path"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReOpen), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Reopen the file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileRead), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: File read"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSGetPosFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Get Posistion from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSGetPosFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Get Posistion from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read neg offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read beyond EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: read offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: read offset from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read every nth byte from BOF "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read every nth byte from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSGetFileCRC), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Get file CRC"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSReadAsync), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read Async"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: File Open Close Non Exstng"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Set Pos Diff Off"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSFileOpenDiffModes), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: File Open Diff Modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSReadDirExt), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read Dir ext"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard2_CommonFSReadDirExt2), OEDT_HW_EXC_LCN2KAI|OEDT_HW_EXC_GEN3G, 10, "C2: Read dir ext2"},
   //Since cryptcard2 is mounted as READ-ONLY, test cases which try to write into it are removed.
   //{u32Cryptcard2_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_NO, 10, "C2: Read dir ext part by part"},
   //{u32Cryptcard2_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_NO, 10, "C2: Read dir ext2 part by part"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
OEDT_FUNC_CFG oedt_pCryptcard_SD_CommonFS_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSOpenClosedevice),            OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSOpendevInvalParm),             OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open Device with Inval param"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSReOpendev),                    OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: reopen Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSOpendevDiffModes),             OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open Device in different modes"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSClosedevAlreadyClosed),        OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Close devide already closed"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSOpenClosedir),                 OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open / Close Dir"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSOpendirInvalid),               OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open inval dir"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSGetDirInfo),                   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Get Dir info "},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSReOpenDir),                    OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Reopen Dir"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileOpenClose),                OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Open Close file"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileOpenInvalPath),            OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: open file with inval path"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReOpen),                   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Reopen the file"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileRead),                     OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: File read"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSGetPosFrmBOF),                 OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Get Posistion from BOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSGetPosFrmEOF),                 OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Get Posistion from EOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadNegOffsetFrmBOF),      OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read neg offset from BOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadOffsetBeyondEOF),      OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read beyond EOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadOffsetFrmBOF),         OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: read offset from BOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadOffsetFrmEOF),         OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: read offset from EOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmBOF),   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read every nth byte from BOF "},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmEOF),   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read every nth byte from EOF"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSGetFileCRC),                   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Get file CRC"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSReadAsync),                    OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read Async"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileOpenCloseNonExstng),       OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: File Open Close Non Exstng"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSSetFilePosDiffOff),            OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Set Pos Diff Off"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSFileOpenDiffModes),            OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: File Open Diff Modes"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSReadDirExt),                   OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read Dir ext"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32Cryptcard_SD_CommonFSReadDirExt2),                  OEDT_HW_EXC_GM_MY13|OEDT_HW_EXC_GEN3G, 10, "CSD: Read dir ext2"},
   //Since cryptcard2 is mounted as READ-ONLY, test cases which try to write into it are removed.
   //{u32Cryptcard2_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_NO, 10, "CSD: Read dir ext part by part"},
   //{u32Cryptcard2_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_NO, 10, "CSD: Read dir ext2 part by part"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

#ifdef OEDT_CRYPTNAV_FS_TEST_ENABLE
OEDT_FUNC_CFG oedt_pCryptnav_CommonFS_Entries[] =
{
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSOpenClosedevice), OEDT_HW_EXC_GEN3G, 10, "C3: Open Close Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSOpendevInvalParm),OEDT_HW_EXC_GEN3G, 10, "C3: Open Device with Inval param"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReOpendev), OEDT_HW_EXC_GEN3G, 10, "C3: reopen Device"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSOpendevDiffModes), OEDT_HW_EXC_GEN3G, 10, "C3: Open Device in different modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSClosedevAlreadyClosed),OEDT_HW_EXC_GEN3G, 10, "C3: Close devide already closed"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSOpenClosedir), OEDT_HW_EXC_GEN3G, 10, "C3: Open / Close Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSOpendirInvalid),OEDT_HW_EXC_GEN3G, 10, "C3: Open inval dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSGetDirInfo), OEDT_HW_EXC_GEN3G, 10, "C3: Get Dir info "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReOpenDir), OEDT_HW_EXC_GEN3G, 10, "C3: Reopen Dir"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileOpenClose), OEDT_HW_EXC_GEN3G, 10, "C3: Open Close file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileOpenInvalPath), OEDT_HW_EXC_GEN3G, 10, "C3: open file with inval path"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReOpen), OEDT_HW_EXC_GEN3G, 10, "C3: Reopen the file"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileRead), OEDT_HW_EXC_GEN3G, 10, "C3: File read"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSGetPosFrmBOF), OEDT_HW_EXC_GEN3G, 10, "C3: Get Posistion from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSGetPosFrmEOF), OEDT_HW_EXC_GEN3G, 10, "C3: Get Posistion from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadNegOffsetFrmBOF), OEDT_HW_EXC_GEN3G, 10, "C3: Read neg offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadOffsetBeyondEOF), OEDT_HW_EXC_GEN3G, 10, "C3: Read beyond EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadOffsetFrmBOF), OEDT_HW_EXC_GEN3G, 10, "C3: read offset from BOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadOffsetFrmEOF), OEDT_HW_EXC_GEN3G, 10, "C3: read offset from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadEveryNthByteFrmBOF), OEDT_HW_EXC_GEN3G, 10, "C3: Read every nth byte from BOF "},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileReadEveryNthByteFrmEOF), OEDT_HW_EXC_GEN3G, 10, "C3: Read every nth byte from EOF"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSGetFileCRC), OEDT_HW_EXC_GEN3G, 10, "C3: Get file CRC"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReadAsync), OEDT_HW_EXC_GEN3G, 10, "C3: Read Async"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileOpenCloseNonExstng), OEDT_HW_EXC_GEN3G, 10, "C3: File Open Close Non Exstng"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSSetFilePosDiffOff), OEDT_HW_EXC_GEN3G, 10, "C3: Set Pos Diff Off"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSFileOpenDiffModes), OEDT_HW_EXC_GEN3G, 10, "C3: File Open Diff Modes"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReadDirExt), OEDT_HW_EXC_GEN3G, 10, "C3: Read Dir ext"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReadDirExt2), OEDT_HW_EXC_GEN3G, 10, "C3: Read dir ext2"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReadDirExtPartByPart), OEDT_HW_EXC_GEN3G, 10, "C3: Read dir ext part by part"},
{OEDT_SIMPLE_TEST_FUNCTION(u32Cryptnav_CommonFSReadDirExt2PartByPart), OEDT_HW_EXC_GEN3G, 10, "C3: Read dir ext2 part by part"},
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

#ifdef OEDT_SIGNAL_HANDLER_TEST_ENABLE
OEDT_FUNC_CFG oedt_SignalHndlr_RegressionEntries[]=
{
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureSignalNULL),                 ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureSignalNULL"},                      /*01*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureSignalInvalid),              ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureSignalInvalid"},                   /*02*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureSignalDoesntExist),          ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureSignalDoesnt Exist"},              /*03*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32GetStatusInvalid),                    ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32GetStatusInvalid"},                         /*04*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32GetInvalidSignalStatus),              ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32GetInvalidSignalStatus"},                   /*05*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32EnableTwiceCheck),                    ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32EnableTwiceCheck"},                         /*06*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32DisableTwiceCheck),                   ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32DisableTwiceCheck"},                        /*07*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureSignalCheckWithStatus),      ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureSignalCheck"},                     /*08*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureCheck),                      ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureCheck"},                           /*09*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigurTwiceDisableCheck),           ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigurTwiceDisableCheck"},                /*10*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigurSignalThreadsCheck),          ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigurSignalThreadsCheck"},               /*11*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32RandomConfigurSignal),                ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32RandomConfigurSignal"},                     /*12*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32StatusCheckOfSignals),                ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32StatusCheckOfSignals"},                     /*13*/
   {OEDT_SIMPLE_TEST_FUNCTION(u32ConfigureCheckVariousSignals),        ~(OEDT_HW_EXC_LCN2KAI | OEDT_HW_EXC_DUAL_OS),   30, "SIG:u32ConfigureSignalCheckforVariousSignals"},    /*14*/   

   {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};
#endif

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!---------------------- AUXCLOCK -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pAUXCLOCK_DEV_RegressionEntries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AuxclockOpenCloseDev), OEDT_HW_EXC_NO, 10, "AUXCLOCK:OpenClose Device"},   
  {OEDT_SIMPLE_TEST_FUNCTION(tu32AuxclockCheckTimeStampValiditySeq), OEDT_HW_EXC_NO, 10, "AUXCLOCK:CheckTimeStamp"},   
  //{OEDT_SIMPLE_TEST_FUNCTION(tu32AuxclockPosixTimeCheck), OEDT_HW_EXC_NO, 10, "AUXCLOCK:PosixTimeCheck"},
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_NO, 5, ""}
};

/*  Testcases mentioned in the ticket CFG3-1261 have been moved from AutoTester_Testdescription by cma5cob */

OEDT_FUNC_CFG oedt_pKDS_Startup_Entries[] =
{
  #if !defined(LSIM) && !defined(GEN3X86)
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpen)                         ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DevOpenClose"},  
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevReOpen)                       ,OEDT_HW_EXC_NO, 10, "KDS_DEV:Reopen"},  
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpenInvalParm)                ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DevOpenInvalParm"}, 
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevOpenDiffModes)                ,OEDT_HW_EXC_NO, 10, "KDS_DEV:OpenDiffModes"}, 
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevClose)                        ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DevClose"}, 
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevCloseInvalParm)               ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DevCloseInvalParm"},  
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevReClose)                      ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DevReClose"},  
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSEnaDisAccessright)               ,OEDT_HW_EXC_NO, 10, "KDS_DEV:EnaDisAccessright"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSEnaDisAccessrightWithInvalParm)  ,OEDT_HW_EXC_NO, 10, "KDS_DEV:EnaDisAccessrightWithInvalParm"}, 
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetNextEntryID)                  ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetNextEntryID"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetNextEntryIDWithInvalParm)     ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetNextEntryIDWithInvalParm"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevInfo)                      ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetDevInfo"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevInfoWithInvalParm)         ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetDevInfoWithInvalParm"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDevInit)                         ,OEDT_HW_EXC_NO, 10, "KDS_DEV:Init"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDeleteEntryID)                   ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DeleteEntryID"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSDeleteEntryIDWithInvalID)        ,OEDT_HW_EXC_NO, 10, "KDS_DEV:DeleteEntryIDWithInvalID"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntry)                       ,OEDT_HW_EXC_NO, 10, "KDS_DEV:ReadEntry"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntryWithInvalID)            ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetNextEntryIDWithInvalParm"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadEntryWithInvalParm)          ,OEDT_HW_EXC_NO, 10, "KDS_DEV:ReadEntryWithInvalID"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntry)                      ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteEntry"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryWithInvalID)           ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteEntryWithInvalID"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryWithInvalParam)        ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteEntryWithInvalParam"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersion)                   ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetVersion"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersionWithInvalParm)      ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetVersionWithInvalParm"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSInvalMacroToIOCTL)               ,OEDT_HW_EXC_NO, 10, "KDS_DEV:InvalMacroToIOCTL"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSClearAllData)                    ,OEDT_HW_EXC_NO, 10, "KDS_DEV:ClearAllData"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetDevVersionAfterDevclose)      ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetVersionAfterDevclose"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSReWriteProtected)                ,OEDT_HW_EXC_NO, 10, "KDS_DEV:ReWriteProtected"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteProtectedDel)               ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteProtectedDel"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteEntryGreater)               ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteEntryGreater"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSReadcheck)                       ,OEDT_HW_EXC_NO, 10, "KDS_DEV:Readcheck"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetRemainingSize)                ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetRemainingSize"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSGetRemainingSizeInval)           ,OEDT_HW_EXC_NO, 10, "KDS_DEV:GetRemainingSizeInval"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteWithoutWEnable)             ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteWithoutWEnable"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSClearWithoutWEnable)             ,OEDT_HW_EXC_NO, 10, "KDS_DEV:ClearWithoutWEnable"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSOverWrite)                       ,OEDT_HW_EXC_NO, 10, "KDS_DEV:OverWrite"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteFull)                       ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteFull"}, 
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteFullAndBackToFlash)         ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteFullAndBackToFlash"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32KDSWriteBackToFlash)                ,OEDT_HW_EXC_NO, 10, "KDS_DEV:WriteBackToFlash"},
  #endif
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pERRMEM_Startup_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32ErrMemDevOpenClose), OEDT_HW_EXC_NO, 10, "ERRMEM: Open Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32ErrMemWrite), OEDT_HW_EXC_NO, 10, "ERRMEM: u32ErrMemWrite"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pTRACE_Startup_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32TraceDevOpenClose), OEDT_HW_EXC_NO, 10, "TRACE: Open Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pPRM_Startup_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_DrvOpenClose), OEDT_HW_EXC_NO, 10, "PRM: Open Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_GetStatus), OEDT_HW_EXC_GEN3G, 50, "PRM: Get Device Ready Status"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_CallbackHandler), OEDT_HW_EXC_NO, 10, "PRM: Test Callback Handler"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_IOCTRL_REG_SYSTEM_INFO),OEDT_HW_EXC_NO, 60, "PRM: REGISTRATION FOR PROCESS_TERMINATED"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_IOCTRL_TRIG_ERRMEM_TTF),OEDT_HW_EXC_NO, 60, "PRM: TRIGGER ERR_MEM TTFIs"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_IOCTRL_TRIG_DEL_ERRMEM),OEDT_HW_EXC_NO, 60, "PRM: TRIGGER DEL ERR_MEM"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_IOCTRL_ACTIVATE_SIGNAL),OEDT_HW_EXC_NO, 60, "PRM: TRIGGER ACTIVATE SIGNAL"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_IOCTRL_REMOUNT),OEDT_HW_EXC_NO, 60, "PRM: REMOUNT SD CARD"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32PRM_TEST_max_registry),OEDT_HW_EXC_NO, 60, "PRM: PRM_MAX_REGISTRATION"},
  {OEDT_SIMPLE_TEST_FUNCTION(u32SDCARD_RegisterforprmNotification), OEDT_HW_EXC_NO, 10, "PRM: Registration for card state "},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pHOST_Startup_Entries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32HostOpenDev), OEDT_HW_EXC_ALL, 10, "HOST: Open Close Device"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pFSDEV_RegressionEntries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(u32fs_dev_check_system_devices), OEDT_HW_EXC_NO, 10, "FSDEV: Open Close System Devices"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pRPC_RegressionEntries[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32RPCConnTester	), OEDT_HW_EXC_LSIM,	100, "RPC test"},
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_pMODULES_RegressionEntries[] =
{
#ifdef OSAL_GEN3
  {OEDT_SIMPLE_TEST_FUNCTION((OEDT_TEST_FUNCPTR)u32ModuleLoadCheck	), OEDT_HW_EXC_NO,	5, "kernel modules loading check"},
#endif
  {OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_NativeIOSC_RegressionEntries[] =
{
#ifdef IOSC_ACTIV
  {OEDT_TEST_SIMPLE(EventCreateDestroy), OEDT_HW_EXC_NO, 100, "create and destroy an event"} , 
  {OEDT_TEST_SIMPLE(EventDoubleDestroy), OEDT_HW_EXC_NO, 100, "try to destroy an event twice"} , 
  {OEDT_TEST_SIMPLE(EventSetAndWaitForEvent_AND), OEDT_HW_EXC_NO, 100, "set an event mask and wait for that mask (AND flag)"} , 
  {OEDT_TEST_SIMPLE(EventSetAndWaitForEvent_OR), OEDT_HW_EXC_NO, 100, "set an event mask and wait for that mask (OR flag)"} , 
  {OEDT_TEST_SIMPLE(EventSetAndWaitForEvent_OR2), OEDT_HW_EXC_NO, 100, "set an event mask and wait for that mask shifted by 2 bits (OR flag)"} , 
  {OEDT_TEST_SIMPLE(EventClear), OEDT_HW_EXC_NO, 100, "clear an event"} , 
  {OEDT_TEST_SIMPLE(EventWaitWithClearFlag), OEDT_HW_EXC_NO, 100, "wait for an event (CLEAR flag)"} , 
  {OEDT_TEST_SIMPLE(EventClearBadArg), OEDT_HW_EXC_NO, 100, "try to clear bits from an eventmask with no bits chosen"} , 
  {OEDT_TEST_SIMPLE(EventWaitBadArg), OEDT_HW_EXC_NO, 100, "try to wait for an event with NULL mask"} , 
  {OEDT_TEST_SIMPLE(EventPerformSetAfterDestroy), OEDT_HW_EXC_NO, 100, "try to set an event mask for a destroyed event"} , 
  {OEDT_TEST_SIMPLE(EventPerformClearAfterDestroy), OEDT_HW_EXC_NO, 100, "try to clear a destroyed event"} , 
  {OEDT_TEST_SIMPLE(EventCreateMaxNumOfHandlesForOneEvent), OEDT_HW_EXC_NO, 100, "try to create a large number of handles for one event"} , 
  {OEDT_TEST_SIMPLE(EventCreateMaxNumOfHandlesForDiffEvents), OEDT_HW_EXC_NO, 100, "try to create a large number of different events"} , 
  {OEDT_TEST_SIMPLE(RingBufCreate), OEDT_HW_EXC_NO, 100, "create a ringbufer"}, 
  {OEDT_TEST_SIMPLE(RingBufDoubleDestroy), OEDT_HW_EXC_NO, 100, "try to destroy a ringbuffer twice"}, 
  {OEDT_TEST_SIMPLE(RingBufSendReceive), OEDT_HW_EXC_NO, 100, "send a message to a ringbuffer and receive it again"}, 
  {OEDT_TEST_SIMPLE(SemaphoreCreateWithDiffIDsAndObtainAfterDestroy), OEDT_HW_EXC_NO, 100, "create semaphores with different IDs and try to obtain them after their destruction"} , 
  {OEDT_TEST_SIMPLE(SemaphoreCreateWithEqIDsAndObtainAfterDestroy), OEDT_HW_EXC_NO, 100, "create semaphores with equal IDs and try to obtain them after their destruction"} , 
  {OEDT_TEST_SIMPLE(SemaphoreCreateWithDiffIDsAndObtainBeforeDestroy), OEDT_HW_EXC_NO, 100, "create semaphores with different IDs, obtain them and destroy them"} , 
  {OEDT_TEST_SIMPLE(SemaphoreCreateWithEqIDsAndObtainBeforeDestroy), OEDT_HW_EXC_NO, 100, "create semaphores with equal IDs, obtain them and destroy them"} , 
  {OEDT_TEST_SIMPLE(SemaphoreTryObtainWithValueNull), OEDT_HW_EXC_NO, 100, "try to obtain a semaphore whereat its value is already zero"} , 
  {OEDT_TEST_SIMPLE(SemaphoreTryObtainWithNegativeValue), OEDT_HW_EXC_NO, 100, "try to obtain a semaphore using a negative value"} , 
  {OEDT_TEST_SIMPLE(SemaphoreTryDoubleDestroy), OEDT_HW_EXC_NO, 100, "try to destroy a semaphore twice"} , 
  {OEDT_TEST_SIMPLE(SemaphoreObtainAndRelease), OEDT_HW_EXC_NO, 100, "obtain and release a semaphore"} , 
  {OEDT_TEST_SIMPLE(SemaphoreTryReleaseAfterDestroy), OEDT_HW_EXC_NO, 100, "try to release a semaphore after its destruction"} , 
  {OEDT_TEST_SIMPLE(SharedMemAllocWithGrowingSizes), OEDT_HW_EXC_NO, 100, "try to allocate shared memory with different sizes"} , 
  {OEDT_TEST_SIMPLE(SharedMemFree), OEDT_HW_EXC_NO, 100, "free shared memory in different scenarios"} , 
  {OEDT_TEST_SIMPLE(SharedMemAllocWithIdMultipleTimes), OEDT_HW_EXC_NO, 100, "allocate named shared memory multiple times"} , 
#endif
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

OEDT_FUNC_CFG oedt_dummy_tests[] =
{
  {OEDT_TEST_WITH_SETUP_AND_TEARDOWN(RebootDummy) , OEDT_HW_EXC_NO, 50, "check if the oedt reboot functionality works"},
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_NO, 5, ""}
};

/* Osal iosc MQ test cases */
OEDT_FUNC_CFG oedt_OsalIOSCMQ_RegressionEntries[] =
{
#ifdef IOSC_ACTIV
  {OEDT_SIMPLE_TEST_FUNCTION(vCreDelMQSequence1), OEDT_HW_EXC_NO, 100, "creation and deletion of MQ in sequence1"} ,
  {OEDT_SIMPLE_TEST_FUNCTION(vCreDelMQSequence2), OEDT_HW_EXC_NO, 100, "creation and deletion of MQ in sequence2"} ,
  {OEDT_SIMPLE_TEST_FUNCTION(vCreDelMQSequence3), OEDT_HW_EXC_NO, 100, "creation and deletion of MQ in sequence3"} ,
  {OEDT_SIMPLE_TEST_FUNCTION(vCreDelMQSequence4), OEDT_HW_EXC_NO, 100, "creation and deletion of MQ in sequence4"} ,
  {OEDT_SIMPLE_TEST_FUNCTION(vPostRecSequence1), OEDT_HW_EXC_NO, 100, "posting and receiving from MQ in sequence1"} ,
  {OEDT_SIMPLE_TEST_FUNCTION(vPostRecSequence2), OEDT_HW_EXC_NO, 100, "posting and receiving from MQ in sequence2"} ,
#endif
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!------------ DEV_CDCTRL + DEV_CDAUDIO, MASCA ------------!!! */
/* \di_tengine_os\products\tengine_osal\osal_dev_test\oedt_Cd_TestFuncs.c */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
#define OEDT_MASCA   (OEDT_HW_EXC_ALL)
#define OEDT_MASCA_W (OEDT_HW_EXC_NO)
OEDT_FUNC_CFG oedt_pCDCTRLCDAUDIO_Entries_reg[] =
{
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T000)           ,OEDT_MASCA_W,   5, "T000 - Trace Off................"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T001)           ,OEDT_MASCA_W,  20, "T001 - Open+Close CDCTRL........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T002)           ,OEDT_MASCA_W,  20, "T002 - Open+Close CDAUDIO......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_TPRINT_MASCA03) ,OEDT_MASCA_W, 600, "T--- - Insert Test-CD 'MASCA 03'"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T012_masca03_1) ,OEDT_MASCA, 180, "T012 - MASCA03 monitoring......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T150)           ,OEDT_MASCA_W,  60, "T150 - CDDA detect.............."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T151)           ,OEDT_MASCA_W, 300, "T151 - CDDA playrange play......"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T152)           ,OEDT_MASCA_W,  20, "T152 - CDDA CD-Text compare....."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T153)           ,OEDT_MASCA_W, 200, "T153 - CDDA playrange fwd......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T154)           ,OEDT_MASCA_W, 300, "T154 - CDDA playrange fbw......."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T003)           ,OEDT_MASCA,  90, "T003 - Eject-Insert Loop........"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T010_inslot)    ,OEDT_MASCA,  60, "T010 - IOCTRLs CDCTRL CD INSLOT."},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T011)           ,OEDT_MASCA,  60, "T011 - IOCTRLs CDAUDIO CD INSLOT"},
  {OEDT_SIMPLE_TEST_FUNCTION(OEDT_CD_T255)           ,OEDT_MASCA,   5, "T255 - TraceLevel++............."},
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------PDD -- --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pPDD_Entries_reg[] =
{
#if !defined(LSIM) && !defined(GEN3X86)
#if defined(GEN3ARM)   
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadEarlyBufferGreat),   OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadEarlyBufferGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadEarlyBufferSmall),   OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadEarlyBufferSmall"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadEarlyDelStreamAndNor),OEDT_HW_EXC_NO, 30, "u32PDDNorUserReadEarlyDelStreamAndNor"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadWrite),              OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserReadSize),               OEDT_HW_EXC_NO, 10, "u32PDDNorUserReadSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteLengthToGreat),     OEDT_HW_EXC_NO, 30, "u32PDDNorUserWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteReadMorePools),     OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteReadMorePools"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserCheckEqualCluster),      OEDT_HW_EXC_NO, 10, "u32PDDNorUserCheckEqualCluster"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteTwoStreams),        OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteTwoStreams"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpOK),             OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpOK"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpMagicWrong),     OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpMagicWrong"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpWriteInterrupt), OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpWriteInterrupt"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserLoadDumpChecksumWrong),  OEDT_HW_EXC_NO, 10, "u32PDDNorUserLoadDumpChecksumWrong"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadDataSize),               OEDT_HW_EXC_NO, 10, "u32PDDSccReadDataSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadWrite),                  OEDT_HW_EXC_NO, 10, "u32PDDSccReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccReadLengthToGreat),          OEDT_HW_EXC_NO, 10, "u32PDDSccReadLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteLengthToGreat),         OEDT_HW_EXC_NO, 10, "u32PDDSccWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWrite),                OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadDataSize),         OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadDataSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadWrite),            OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadWrite"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWriteReadCompare),     OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWriteReadCompare"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadWrongVersion),     OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadWrongVersion"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelReadLengthToGreat) ,   OEDT_HW_EXC_NO, 10, "u32PDDNorKernelReadLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorKernelWriteLengthToGreat),   OEDT_HW_EXC_NO, 10, "u32PDDNorKernelWriteLengthToGreat"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteCheckBackup) ,          OEDT_HW_EXC_NO, 10, "u32PDDSccWriteCheckBackup"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteCheckSync),             OEDT_HW_EXC_NO, 10, "u32PDDSccWriteCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDSccWriteDestroyCheckSync),      OEDT_HW_EXC_NO, 10, "u32PDDSccWriteDestroyCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteCheckBackup) ,      OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteCheckBackup"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteCheckSync),         OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDNorUserWriteEraseNorCheckSync), OEDT_HW_EXC_NO, 10, "u32PDDNorUserWriteEraseNorCheckSync"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataFs) ,                 OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataFs"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataFsSecure),            OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataFsSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataCheckParamString),    OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataCheckParamString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataCheckParamLocation),  OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataCheckParamLocation"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataTwice),               OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataTwice"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataBackupNotExist) ,     OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataBackupNotExist"},
{OEDT_SIMPLE_TEST_FUNCTION(u32PDDDeleteDataNormalNotExist),      OEDT_HW_EXC_NO, 10, "u32PDDDeleteDataNormalNotExist"},
#endif
#endif
{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------DataPool--------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* The testcase mentioned in the ticket CFG3-1263 have been moved from AutoTester_Testdescription by cma5cob */

OEDT_FUNC_CFG oedt_pDataPool_Entries_reg[] =
{
#if !defined(LSIM) && !defined(GEN3X86)
#if defined(GEN3ARM)   
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetElementWithoutClassAccess),       OEDT_HW_EXC_NO, 30, "u32DPSetGetElementWithoutClassAccess"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefaultValueTrue),                   OEDT_HW_EXC_NO, 30, "u32DPGetDefaultValueTrue"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefaultValueAndState),               OEDT_HW_EXC_NO, 30, "u32DPGetDefaultValueAndState"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetChangedValueAndState),               OEDT_HW_EXC_NO, 30, "u32DPGetChangedValueAndState"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetConfig),                             OEDT_HW_EXC_NO, 30, "u32DPSetConfig"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetSetCodeDataAndSetConfig),            OEDT_HW_EXC_NO, 30, "u32DPChangeCodingdataAndSetConfig"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPAddNotificationAndSetConfig),           OEDT_HW_EXC_NO, 30, "u32DPAddNotificationAndSetConfig"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetPoolV850),                           OEDT_HW_EXC_NO, 30, "u32DPSetPoolV850"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetTestValueU8),                     OEDT_HW_EXC_NO, 30, "u32DPSetGetTestValueU8"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetTestArrayGreat4k),                OEDT_HW_EXC_NO, 30, "u32DPSetGetTestArrayGreat4k"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetTestArrayGreat2k),                OEDT_HW_EXC_NO, 30, "u32DPSetGetTestArrayGreat2k"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetTestStructureArray),              OEDT_HW_EXC_NO, 30, "u32DPSetGetTestStructureArray"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetToDefaultTestValueU8),               OEDT_HW_EXC_NO, 30, "u32DPSetToDefaultTestValueU8"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEarlyConfigDisplaySet),                 OEDT_HW_EXC_NO, 30, "u32DPEarlyConfigDisplaySet"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEarlyConfigDisplayGet),                 OEDT_HW_EXC_NO, 30, "u32DPEarlyConfigDisplayGet"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEarlyConfigCSCSetGet),                  OEDT_HW_EXC_NO, 30, "u32DPEarlyConfigCSCSetGet"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefaultValueCheckIfStored),          OEDT_HW_EXC_NO, 30, "u32DPSetDefaultValueCheckIfStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccess),                      OEDT_HW_EXC_NO, 30, "u32DPSetGetSRAMAccess"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccessWriteAll),              OEDT_HW_EXC_NO, 30, "u32DPSetGetSRAMAccessWriteAll"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetSRAMAccessMultiThread),           OEDT_HW_EXC_NO, 60, "u32DPSetGetSRAMAccessMultiThread"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoKdsEntryStored),       OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSRecordNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoInitNoKdsEntryStored), OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSRecordNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoKdsEntryStored),         OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoInitNoKdsEntryStored),   OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSWrongItemNoKdsEntryStored),    OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSWrongItemNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU32NoKdsEntryStored),     OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtU32NoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU16NoKdsEntryStored),     OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtU16NoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoKdsEntryStored),  OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtStringNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored), OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSBankElementsNoKdsEntryStored), OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSBankElementsNoKdsEntryStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecord),                       OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSRecord"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSRecordNoInit),                 OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSRecordNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItem),                         OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItem"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemNoInit),                   OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSWrongItem),                    OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSWrongItem"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU32),                     OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtU32"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtU16),                     OEDT_HW_EXC_NO, 30, "u32DPGetDefKDSItemtU16"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtString),                  OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSItemtStringNoInit),            OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSItemtStringNoInit"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetDefKDSBankElementsStored),           OEDT_HW_EXC_NO, 60, "u32DPGetDefKDSBankElementsStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefKDSToDefault),                    OEDT_HW_EXC_NO, 60, "u32DPSetDefKDSToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPLoadDefaultBankToUserAndStore),         OEDT_HW_EXC_NO, 60, "u32DPLoadDefaultBankToUserAndStore"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUndefElementCheckParameter),         OEDT_HW_EXC_NO, 30, "u32DPSetUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetUndefElementCheckParameter),         OEDT_HW_EXC_NO, 30, "u32DPGetUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementCheckParameter),        OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPDeleteUndefElementCheckParameter),      OEDT_HW_EXC_NO, 30, "u32DPDeleteUndefElementCheckParameter"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUndefElement),                       OEDT_HW_EXC_NO, 30, "u32DPSetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPGetUndefElementUnknown),                OEDT_HW_EXC_NO, 30, "u32DPGetUndefElementUnknown"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPDeleteUndefElementUnknown),             OEDT_HW_EXC_NO, 30, "u32DPDeleteUndefElementUnknown"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetGetUndefElement),                    OEDT_HW_EXC_NO, 30, "u32DPSetGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDeleteGetUndefElement),              OEDT_HW_EXC_NO, 30, "u32DPSetDeleteGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitGetUndefElement),                   OEDT_HW_EXC_NO, 30, "u32DPInitGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitSetGetUndefElement),                OEDT_HW_EXC_NO, 30, "u32DPInitSetGetUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInit20UndefElements),                   OEDT_HW_EXC_NO, 30, "u32DPInit20UndefElements"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementNewProperty),           OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementNewProperty"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitUndefElementSetDefaultProperty),    OEDT_HW_EXC_NO, 30, "u32DPInitUndefElementSetDefaultProperty"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPWriteInitUndefElement),                 OEDT_HW_EXC_NO, 30, "u32DPWriteInitUndefElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPReadInitUndefElementAfterReboot),       OEDT_HW_EXC_NO, 30, "u32DPReadInitUndefElementAfterReboot"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPInitSetGetUndefElementShared),          OEDT_HW_EXC_NO, 30, "u32DPInitSetGetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessInitUndefElementShared),    OEDT_HW_EXC_NO, 30, "u32DPOtherProcessInitUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessDeleteUndefElementShared),  OEDT_HW_EXC_NO, 30, "u32DPOtherProcessDeleteUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessSetUndefElementShared),     OEDT_HW_EXC_NO, 30, "u32DPOtherProcessSetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessGetUndefElementShared),     OEDT_HW_EXC_NO, 30, "u32DPOtherProcessGetUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessAccessDelUndefElementShared),OEDT_HW_EXC_NO, 30, "u32DPOtherProcessAccessDelUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPOtherProcessInitDelUndefElementShared)  ,OEDT_HW_EXC_NO, 30, "u32DPOtherProcessInitDelUndefElementShared"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserLockUnlock)                      ,OEDT_HW_EXC_NO, 30, "u32DPEndUserLockUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementEndUserIfLocked)     ,OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementEndUserIfLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementEndUserAfterUnLocked),OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementEndUserAfterUnLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetGetElementNoEndUserIfLocked),  OEDT_HW_EXC_NO, 30, "u32DPEndUserSetGetElementNoEndUserIfLocked"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserLock2ModesUnlock),				         OEDT_HW_EXC_NO, 30, "u32DPEndUserLock2ModesUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetEndUserReadNewDefaultElement), OEDT_HW_EXC_NO, 30, "u32DPEndUserSetEndUserReadNewDefaultElement"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSaveNewEndUser1AndSetAllElements), OEDT_HW_EXC_NO, 30, "u32DPEndUserSaveNewEndUser1AndSetAllElements"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault), OEDT_HW_EXC_NO, 30, "u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetNoCurrentUserToDefault),       OEDT_HW_EXC_NO, 30, "u32DPEndUserSetNoCurrentUserToDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetAllPoolsToFactoryDefault),     OEDT_HW_EXC_NO, 30, "u32DPEndUserSetAllPoolsToFactoryDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSetAllPoolsToUserDefault),        OEDT_HW_EXC_NO, 30, "u32DPEndUserSetAllPoolsToUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserGetNoCurrentEndUserValue),        OEDT_HW_EXC_NO, 30, "u32DPEndUserGetNoCurrentEndUserValue"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserOtherProcessSetEndUser),          OEDT_HW_EXC_NO, 30, "u32DPEndUserOtherProcessSetEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserOtherProcessLockSetEndUserUnlock),OEDT_HW_EXC_NO, 30, "u32DPEndUserOtherProcessLockSetEndUserUnlock"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserCopyToCurrentUserNotAllowed),     OEDT_HW_EXC_NO, 30, "u32DPEndUserCopyToCurrentUserNotAllowed"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserCopyFromCurrentUser),             OEDT_HW_EXC_NO, 30, "u32DPEndUserCopyFromCurrentUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserCopyFromFileNotCurrentUser),      OEDT_HW_EXC_NO, 30, "u32DPEndUserCopyFromFileNotCurrentUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserCopySwitchEndUser),               OEDT_HW_EXC_NO, 30, "u32DPEndUserCopySwitchEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserCopySetDefaultCopyReadDefault),   OEDT_HW_EXC_NO, 30, "u32DPEndUserCopySetDefaultCopyReadDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankLoadBankToCurrentEndUser),    OEDT_HW_EXC_NO, 30, "u32DPEndUserBankLoadBankToCurrentEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSaveCurrentEndUserToBank),    OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSaveCurrentEndUserToBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetValueSaveToBankLoadSavedBank),OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetValueSaveToBankLoadSavedBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankGetBank),                     OEDT_HW_EXC_NO, 30, "u32DPEndUserBankGetBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetEndGreatUserLoadBank),     OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetEndGreatUserLoadBank"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetBankDefault),              OEDT_HW_EXC_NO, 30, "u32SetBankDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetAllBanksDefault),          OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetAllBanksDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetEndUserDefault),           OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetEndUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetUserDefault),              OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSwitchbackBankAction),            OEDT_HW_EXC_NO, 30, "u32DPEndUserSwitchbackBankAction"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserSwitchbackUserDefault),           OEDT_HW_EXC_NO, 30, "u32DPEndUserSwitchbackUserDefault"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankLoadSwitchUser),              OEDT_HW_EXC_NO, 30, "u32DPEndUserBankLoadSwitchUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPEndUserBankSetBankDefaultGetSwitchEndUser), OEDT_HW_EXC_NO, 30, "u32DPEndUserBankSetBankDefaultGetSwitchEndUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefault_TefUser),                     OEDT_HW_EXC_NO, 30, "u32DPSetDefault_TefUser"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefault_EndUserCurrent),              OEDT_HW_EXC_NO, 30, "u32DPSetDefault_EndUserCurrent"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetDefault_EndUserNotCurrent),           OEDT_HW_EXC_NO, 30, "u32DPSetDefault_EndUserNotCurrent"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUserDefault_CurrentUser1),            OEDT_HW_EXC_NO, 30, "u32DPSetUserDefault_CurrentUser1"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUserDefault_CurrentUser3),            OEDT_HW_EXC_NO, 30, "u32DPSetUserDefault_CurrentUser3"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetBankDefaultWithElemSecure),           OEDT_HW_EXC_NO, 30, "u32DPSetBankDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetEndUserDefaultWithElemSecure),        OEDT_HW_EXC_NO, 30, "u32DPSetEndUserDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPSetUserDefaultWithElemSecure),           OEDT_HW_EXC_NO, 30, "u32DPSetUserDefaultWithElemSecure"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemWrongParamString),       OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemWrongParamString"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemArrayWriteGetCmp),       OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemArrayWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemBufferNull),             OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemBufferNull"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemWrongSize),              OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemWrongSize"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemCompleteWriteGetCmp),    OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemCompleteWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemArrayWriteGetCmp),       OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemArrayWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemByteWriteGetCmp),        OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemByteWriteGetCmp"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetConfigItemFromTestProcessProcdatapool5),OEDT_HW_EXC_NO, 30, "u32DPs32GetConfigItemFromTestProcessProcdatapool5"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32GetRegistryFromTestProcessProcdatapool6),OEDT_HW_EXC_NO, 30, "u32DPs32GetRegistryFromTestProcessProcdatapool6"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32StorePoolNow),                        OEDT_HW_EXC_NO, 30, "u32DPs32StorePoolNow"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPs32StorePoolNowWrongAccess),             OEDT_HW_EXC_NO, 30, "u32DPs32StorePoolNowWrongAccess"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPChangeConfigSetUserDpEndUserNotStored),  OEDT_HW_EXC_NO, 30, "u32DPChangeConfigSetUserDpEndUserNotStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPChangeConfigSetBankDpEndUserNotStored),  OEDT_HW_EXC_NO, 30, "u32DPChangeConfigSetBankDpEndUserNotStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPChangeConfigSetDefaultDpEndUserNotStored),OEDT_HW_EXC_NO, 30, "u32DPChangeConfigSetDefaultDpEndUserNotStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPChangeConfigCopyUserDpEndUserNotStored),  OEDT_HW_EXC_NO, 30, "u32DPChangeConfigCopyUserDpEndUserNotStored"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPProcessExitWithOsalExitFunction),        OEDT_HW_EXC_NO, 30, "u32DPProcessExitWithOsalExitFunction"},
{OEDT_SIMPLE_TEST_FUNCTION(u32DPProcessExitWithoutOsalExitFunction),     OEDT_HW_EXC_NO, 30, "u32DPProcessExitWithoutOsalExitFunction"},
#endif
#endif

{OEDT_SIMPLE_TEST_FUNCTION(NULL), OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!---------------------- IncSccComm -------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

OEDT_FUNC_CFG oedt_pIncComm_RegressionEntries[] =
{
#ifdef OSAL_GEN3
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV1),         OEDT_HW_EXC_NO,   30, "IncCommSccV1"},               /*   0  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV2),         OEDT_HW_EXC_NO,   30, "IncCommSccV2"},               /*   1  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV3),         OEDT_HW_EXC_NO,   30, "IncCommSccV3"},               /*   2  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV4),         OEDT_HW_EXC_NO,   30, "IncCommSccV4"},               /*   3  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV5),         OEDT_HW_EXC_NO,   30, "IncCommSccV5"},               /*   4  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV6),         OEDT_HW_EXC_NO,   30, "IncCommSccV6"},               /*   5  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV7),         OEDT_HW_EXC_NO,   30, "IncCommSccV7"},               /*   6  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV8),         OEDT_HW_EXC_NO,   30, "IncCommSccV8"},               /*   7  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV9),         OEDT_HW_EXC_NO,   30, "IncCommSccV9"},               /*   8  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV10),        OEDT_HW_EXC_NO,   30, "IncCommSccV10"},              /*   9  */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV11),        OEDT_HW_EXC_NO,   30, "IncCommSccV11"},              /*   10 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV12),        OEDT_HW_EXC_NO,   30, "IncCommSccV12"},              /*   11 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV13),        OEDT_HW_EXC_NO,   30, "IncCommSccV13"},              /*   12 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV14),        OEDT_HW_EXC_NO,   30, "IncCommSccV14"},              /*   13 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV15),        OEDT_HW_EXC_NO,   30, "IncCommSccV15"},              /*   14 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV16),        OEDT_HW_EXC_NO,   30, "IncCommSccV16"},              /*   15 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV17),        OEDT_HW_EXC_NO,   30, "IncCommSccV17"},              /*   16 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV18),        OEDT_HW_EXC_NO,   30, "IncCommSccV18"},              /*   17 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV19),        OEDT_HW_EXC_NO,   30, "IncCommSccV19"},              /*   18 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV20),        OEDT_HW_EXC_NO,   30, "IncCommSccV20"},              /*   19 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV21),        OEDT_HW_EXC_NO,   30, "IncCommSccV21"},              /*   20 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV22),        OEDT_HW_EXC_NO,   30, "IncCommSccV22"},              /*   21 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV23),        OEDT_HW_EXC_NO,   30, "IncCommSccV23"},              /*   22 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV24),        OEDT_HW_EXC_NO,   30, "IncCommSccV24"},              /*   23 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommSccV25),        OEDT_HW_EXC_NO,   30, "IncCommSccV25"},              /*   24 */
#if 0 
   /* As Adr3 is not supported in GM targets & No HW exclusion flag supported for GM IN GEN3 
   The test cases related to ADR3 are added in oedt_AutoTester_Testdescription.c*/

   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV1),         OEDT_HW_EXC_NO,   30, "IncCommAdrV1"},               /*   25 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV2),         OEDT_HW_EXC_NO,   30, "IncCommAdrV2"},               /*   26 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV3),         OEDT_HW_EXC_NO,   30, "IncCommAdrV3"},               /*   27 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV4),         OEDT_HW_EXC_NO,   30, "IncCommAdrV4"},               /*   28 */
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncCommAdrV5),         OEDT_HW_EXC_NO,   30, "IncCommAdrV5"},               /*   29 */
#endif
#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------IncSccPerformance------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
OEDT_FUNC_CFG oedt_pIncPerformance_RegressionEntries[] =
{
#ifdef OSAL_GEN3
   {OEDT_SIMPLE_TEST_FUNCTION(u32IncPerformanceScc),         OEDT_HW_EXC_NO,   30, "IncPerformanceScc"},           /*   0  */
   #if 0
   /* As Adr3 is not supported in GM targets & No HW exclusion flag supported for GM in GEN3 
   The test cases related to ADR3 are added in oedt_AutoTester_Testdescription.c*/

   {OEDT_SIMPLE_TEST_FUNCTION(u32IncPerformanceAdr),         OEDT_HW_EXC_NO,   30, "IncPerformanceAdr"},           /*   1  */
   #endif
#endif /* OSAL_GEN3 */
  {OEDT_EMPTY_TEST ,OEDT_HW_EXC_ALL, 5, ""}
};

/* -== Sequence of regression tests ==- */
OEDT_TESTCON_ENTRY oedt_pTestRegressionDataset[] =
{
   {oedt_pOSALCORERegressionEntries,            "OSALCORE-RegSet"},
   {oedt_pFFS2_CommonFS_RegressionEntries,      "Common FFS2"},         /* 28 Tests for FFS2 common FS      */
   {oedt_pCryptcard_CommonFS_Entries,           "Cryptcard Common FS"},
//   {oedt_pCryptcard2_CommonFS_Entries,          "Cryptcard2 Common FS"},
#if defined (OSAL_GEN3) || defined (OSAL_GEN3_SIM )
   /*CF3PF-3743 -The Check if CRYPTCARD or CRYPTCARD2 is present is not valid for Gen3 as Only CRYPTCARD is available.
   Since we already have the same tests for CRYPTCARD, these redundant tests are removed for GEN3.  */
#else
   {oedt_pCryptcard_SD_CommonFS_Entries,        "Cryptcard SDCard Common FS"},
#endif
#ifdef OEDT_CRYPTNAV_FS_TEST_ENABLE
   {oedt_pCryptnav_CommonFS_Entries,            "Cryptnav Common FS"},
#endif
   {oedt_pAUDIOTEST_Entries,                    "AUDIO TEST"},        	  /* AUDIO tests		 */
   {oedt_pACOUSTICOUT_Entries,                  "Acoustic Out"},
   {oedt_pACOUSTICIN_Entries,                   "Acoustic In"},
   #ifndef GEN3X86
   {oedt_pACOUSTICSRC_Entries,                  "Acoustic SRC"},
   #endif
   {oedt_pREGISTRY_RegressionEntries,           "REGISTRY-RegSet"},     /*   Registry */
   {oedt_pMULTIPROCESS_Entries,                 "MultiProcess-RegSet"}, /*   Tests for Multiprocess OSAL */
   {oedt_pPERFORMANCE_Entries,                  "Performance-RegSet"},  /*   Tests for OSAL Performance */
   {oedt_pEOLLIB_RegressionEntries,             "EOLLIB ctrl tests"},
   {oedt_pFFD_DEV_RegressionEntries,            "DEV_FFD"},             /* DEV_FFD tests       */   
   {oedt_pRAMDISK_CommonFS_RegressionEntries,   "DEV_RAMDISK"},         /* DEV_RAMDISK tests       */   
   //{oedt_pPLATFORM_Startup_Entries,             "Platform Startup TEST"},
   {oedt_pAUXCLOCK_DEV_RegressionEntries,       "Auxiliary Clock TEST"},
   {oedt_pKDS_Startup_Entries,                  "KDS Test"},
   {oedt_pERRMEM_Startup_Entries,               "ERRMEM Test"},
   {oedt_pTRACE_Startup_Entries,                "TRACE Test"},
   {oedt_pPRM_Startup_Entries,                  "PRM Test"},
   {oedt_pHOST_Startup_Entries,                 "HOST Test"},
   {oedt_pFSDEV_RegressionEntries,              "FS DEV Test"},
    // {oedt_pRPC_RegressionEntries,                "RPC Test"},
    
    // {oedt_pMODULES_RegressionEntries,            "Kernel Modules Test"},
   {oedt_NativeIOSC_RegressionEntries,          "native IOSC tests"},
   {oedt_pFFS3_CommonFS_RegressionEntries,      "Common FFS3"},
   {oedt_pGPS_RegressionEntries,                "GPS device check"},
   {oedt_OsalIOSCMQ_RegressionEntries,	        "OsalIoscMQTests"},					/* 27 Osal IOSC-MQ tests */
#ifdef OEDT_SIGNAL_HANDLER_TEST_ENABLE
   {oedt_SignalHndlr_RegressionEntries,         "Signal Handle"}, /* 30 Signal Handler Tests*/
#endif
   {oedt_pGPIO_RegressionEntries,               "GPIO"},     /* GPIO tests */
   {oedt_pInputInc_RegressionEntries,           "InputInc"}, /* InputInc tests */
   {oedt_pADC_RegressionEntries,                "ADC"},
   {oedt_pGNSS_RegressionEntries,               "GNSS_REG_TESTS"},
   {oedt_pOdometer_RegressionEntries,           "ODOMETER_REG_TESTS"},
   {oedt_pCDCTRLCDAUDIO_Entries_reg,          "MASCA SCSI CD"},                /*  MASCA /dev/sr0 */   
#ifdef LSIM
   {oedt_pSPM_VOLT_RegressionEntries,			"VOLT device Test"},
   {oedt_pSWC_Entries_reg,            			"SWC Tests"}, 		  /*   SWC     */
#endif
   {oedt_pRTC_RegressionEntries,                "RTC"},     /*   3  RTC            */
#if defined(GEN3X86)
   {oedt_pODO_Entries,                         "ODOMETER tests"},/*ODOMETER*/
#endif
   {oedt_pGYRO_Entries,                       "GYRO tests"},   /* GYROMETER */ 
#if defined(GEN3ARM)
   {oedt_pACC_RegressionEntries,                "ACC tests"},     /* ACCELEROMETER */
   {oedt_pPDD_Entries_reg,    	                "PDD"},                          /*  PDD Test*/
   {oedt_pDataPool_Entries_reg,                 "Datapool"},                     /*  Datapool Test*/
#endif
   {oedt_pram_RegressionEntries,                   "PRAM"},                           /*  PRAM            */
   {oedt_pIncComm_RegressionEntries,             "INC SCC COMMUNICATION TEST"},      /*INC COMMUNICATION TEST*/
   {oedt_pIncPerformance_RegressionEntries,      "INC SCC PERFORMANCE TEST"},        /*INC PERFORMANCE TEST*/
   {NULL,                                       ""}
};
