/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Core.c
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      OEDT core functionality.
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Types.h"
#include "oedt_Configuration.h"
#include "oedt_Database.h"
#include "oedt_Core.h"
#include "oedt_Display.h"
#include "oedt_Macros.h"
#include "oedt_regtest_results.h"

#include <malloc.h>
#include <sys/sysinfo.h>
extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...);

typedef struct{
  OEDT_FLOW_MODE_ID eFlowMode;
  tU8               u8SpecMode;
  tU32              u32SelectedDataset;
}OEDT_CONFIG;

typedef struct{
  tBool             fInvalid;
  tU8               u8TaskState;
  OEDT_TEST_INFO    sTestInfo;
  OSAL_tThreadID    hThread;
  OSAL_tTimerHandle hTimer;
  void*             pTableEntry;
}OEDT_TASK_CONTROL;

typedef struct{
  tU32            u32ClassBegin;
  tU32            u32ClassEnd;
  tU32            u32TestBegin;
  tU32            u32TestEnd;
  void            *pNext;
  void            *pPrev;
}OEDT_TABLE_ENTRY;

#define OEDT_TASKSYNC_RUNNING 2
#define OEDT_TASKSYNC_WAITING 1
#define OEDT_TASKSYNC_MASKOUT 0

#define OEDT_EXTINFO_MAXLEN 80

tS8   oedt_ps8ExtInfo[OEDT_EXTINFO_MAXLEN];
tU32  oedt_u32ExtInfoLen;

char oedt_boardname[30] = "";     // name of actual HW for test exclusion
char oedt_os_variant[30] = "";     // name of actual HW for test exclusion

OEDT_CONFIG  oedt_sConfig;
OEDT_CONFIG* oedt_pConfig_Bkp;

OEDT_TESTER_STATS   oedt_sOverallTestStats;
OEDT_TESTER_STATS** oedt_ResTable_pStats;
tU32                oedt_ResTable_u32MaxID;
tBool oedt_bStopFlg        = FALSE;
tU8   oedt_u8HoneypotLevel = 0;
tU32  oedt_u32TableLineIdx = 0;

OEDT_INTERNAL_STATE oedt_eState = OEDT_STATE_NOTREADY;

OEDT_TABLE_ENTRY  *oedt_pTestTable       = NULL;
tU8*               oedt_pu8TestTable_Bkp = NULL;

OEDT_TASK_CONTROL *oedt_pTaskControl = NULL;

OSAL_tMQueueHandle oedt_hJobMsgQ  = 0;
OSAL_tSemHandle    oedt_hTaskWaitSem = 0;

OSAL_tThreadID         oedt_TestHandler_threadID   = 0;
OSAL_trThreadAttribute oedt_TestHandler_threadAttr = {_OEDT_PROCESSNAME_TESTHANDLER,
                                                      _OEDT_MAX_PRIO + 1,
                                                      _OEDT_PROCESS_STACK_SIZE,
                                                      (OSAL_tpfThreadEntry)oedt_vTestHandler,
                                                      (void*)NULL};
tU32 u32FreeMemoBeforeTest = 0;
tU32 u32FreeMemoAfterTest = 0;

static tS32 u32GetSystemFreeMemory(void)
{
   struct sysinfo info;
   if(sysinfo(&info) != 0)
   {
      perror("SysInfo");
      info.freeram = 0;
   }
   return (info.freeram) / 1024;
}

static tS32 u32GetUsedMemory(void)
{
   struct mallinfo info;
   info = mallinfo();
   return info.hblkhd + info.uordblks;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vHoneypotInitHeap()
* initialise the heap honeypots            
**********************************************************************FunctionHeaderEnd***/
void oedt_vHoneypotInitHeap(tU8 value)
{
    tU32 i = 0;
    tU32 u32_num_allocated_cells = 0;
    tU8 *p_u8_memcells = NULL;
        
    tS32 s32FreeMem = u32GetSystemFreeMemory();
	if (s32FreeMem > 0) 
	{
		u32_num_allocated_cells = 7 * 1024 * (tU32) s32FreeMem;
        p_u8_memcells = (tU8*) OSAL_pvMemoryAllocate(u32_num_allocated_cells * sizeof(tU8));
        if (p_u8_memcells != OSAL_NULL) 
		{
            for(i=0;i<(u32_num_allocated_cells);i++)
			{
                p_u8_memcells[i] = value;
            }
            OSAL_vMemoryFree(p_u8_memcells);
        }
	}
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vHoneypotSetActive()
* activate honeypots for the heap
**********************************************************************FunctionHeaderEnd***/
void oedt_vHoneypotSetLevel(tU8 tU8HoneypotLevel)
{
        oedt_u8HoneypotLevel = tU8HoneypotLevel;
}


/*********************************************************************FunctionHeaderBegin***
* oedt_vThreadTimerCallback()
* handles test timeouts
**********************************************************************FunctionHeaderEnd***/
void oedt_vThreadTimerCallback(void* u32Idx)
{
  OSAL_s32TimerDelete(oedt_pTaskControl[(tU32)u32Idx].hTimer);
  OSAL_s32ThreadSuspend(oedt_pTaskControl[(tU32)u32Idx].hThread);

  oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u8TimedOut = 1;
  OSAL_s32MessageQueuePost(oedt_hJobMsgQ, (tPCU8)&u32Idx, sizeof(tU32), OSAL_NULL);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_bIsTestExc4HW()
* checks whether test is allowed for the actual hardware
* (return value of TRUE means test is excluded)
* 2008-04-17, Lars Tracht: rewritten for use with different HW with GM plattform
* currently working for GMGE MY 09 and JLR HLDF
* 2013.26.02,sja3kor Added check for Gen-3 variation: Gen3G 
**********************************************************************FunctionHeaderEnd***/
tBool oedt_bIsTestExc4HW(tU32 u32HwExcMask)
{
    tBool result = FALSE;

    switch ( u32HwExcMask )
    {
        case OEDT_HW_EXC_NO:
            // no exclusion
            result = FALSE;
            break;

        case OEDT_HW_EXC_ALL:
            // exclude all
            result = TRUE;
            break;

        default:
#if defined TSIM_OSAL
            if (u32HwExcMask & OEDT_HW_EXC_TSIM) {
                result = TRUE;
            }else{
                result = FALSE;
            }
#elif defined(LSIM)
            if (u32HwExcMask & OEDT_HW_EXC_LSIM) {
                result = TRUE;
            }else{
                result = FALSE;
            }
#else /*#if defined TSIM_OSAL #elif defined LSIM*/
            // exclude specific HW
            // open registry handle to read board name
            if ( u32HwExcMask & OEDT_HW_EXC_DUAL_OS ) {
                // check for DUAL OS
                if ( OSAL_ps8StringSubString ( oedt_os_variant, "DUAL_OS" ) != 0 ) { 
                    result = TRUE; 
                }
            }
            if ( u32HwExcMask & OEDT_HW_EXC_SINGLE_OS ) {
                // check for DUAL OS
                if ( OSAL_ps8StringSubString ( oedt_os_variant, "SINGLE_OS" ) != 0 ) { 
                    result = TRUE; 
                }
            }
            if ( u32HwExcMask & OEDT_HW_EXC_GM_MY13 ) {
                // check for Gen-2 variation: GM NextGen MY13
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13" ) != 0 )   { result = TRUE; }
            }            
            if ( u32HwExcMask & OEDT_HW_EXC_GM_MY13_NAE ) {
                // check for Gen-2 variation: GM NextGen MY13 (North America + Europe)
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1001" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1002" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1003" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1004" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1005" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1006" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1007" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1008" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1009" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100A" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100B" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100C" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100D" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100E" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x100F" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1010" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1011" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1012" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1014" ) != 0 )   { result = TRUE; }
                if ( OSAL_ps8StringSubString ( oedt_boardname, "GM NG MY13 BID 0x1015" ) != 0 )   { result = TRUE; }
            }
            if ( u32HwExcMask & OEDT_HW_EXC_LCN2KAI ) {
                // check for Gen-2 variation: Nissan LCN2kai
                if ( OSAL_ps8StringSubString ( oedt_boardname, "LCN2 KAI" ) != 0 ) { 
                    result = TRUE; 
                }
            }
			if ( u32HwExcMask & OEDT_HW_EXC_GEN3G  ) {
                // check for Gen-3 variation: Gen3G LSIM
                if ( OSAL_ps8StringSubString ( oedt_boardname, "Gen3G" ) != 0 ) { 
                    result = TRUE; 
                }
            } 
			
#endif /*#if defined TSIM_OSAL #elif defined LSIM*/
            break; // default
            
    } // switch ( u32HwExcMask )    
    return result;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vDatasetSelect()
* set oedt config dataset
**********************************************************************FunctionHeaderEnd***/
void oedt_vDatasetSelect(tU32 u32ID)
{
  oedt_sConfig.u32SelectedDataset = oedt_DB_u32SelectSet(u32ID);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vModeSet_Flow()
* set oedt config flow mode
**********************************************************************FunctionHeaderEnd***/
void oedt_vModeSet_Flow(OEDT_FLOW_MODE_ID eMode)
{
  oedt_sConfig.eFlowMode = eMode;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vModeSet_Special()
* set oedt config special mode mask (endurance, load)
**********************************************************************FunctionHeaderEnd***/
void oedt_vModeSet_Special(OEDT_SPECIAL_MODE_ID eMode, tBool fFlag)
{
  if(fFlag == TRUE)
    oedt_sConfig.u8SpecMode = oedt_sConfig.u8SpecMode | eMode;
  else
    oedt_sConfig.u8SpecMode = oedt_sConfig.u8SpecMode & ~eMode;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vDelay()
* avoids problems with fast listing to ttfis
**********************************************************************FunctionHeaderEnd***/
void oedt_vDelay(void)
{
#if _OEDT_LIST_DELAY > 0
  OSAL_s32ThreadWait(_OEDT_LIST_DELAY);
#endif   
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_pGetLastEntry()
* returns pointer to last entry of test table
**********************************************************************FunctionHeaderEnd***/
OEDT_TABLE_ENTRY* oedt_TestTable_pGetLastEntry(void)
{
  OEDT_TABLE_ENTRY* pEntry = oedt_pTestTable;
  OEDT_TABLE_ENTRY* pLast  = NULL;

  while(pEntry != NULL)
  {
    pLast  = pEntry;
    pEntry = pEntry->pNext;
  }
   
  return pLast;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_pCreateNewEntry()
* adds a new entry to the test table
**********************************************************************FunctionHeaderEnd***/
OEDT_TABLE_ENTRY* oedt_TestTable_pCreateNewEntry(void)
{
  OEDT_TABLE_ENTRY *pEntry, *pLast;

  pLast  = oedt_TestTable_pGetLastEntry();
  pEntry = pLast;

  if(pEntry == NULL)
  {
    oedt_pTestTable = (OEDT_TABLE_ENTRY*)OSAL_pvMemoryAllocate(sizeof(OEDT_TABLE_ENTRY));
    pEntry = oedt_pTestTable;
  }
  else
  {
    pEntry->pNext = (OEDT_TABLE_ENTRY*)OSAL_pvMemoryAllocate(sizeof(OEDT_TABLE_ENTRY));
    pEntry = pEntry->pNext;
  }

  pEntry->pNext     = NULL;
  pEntry->pPrev     = pLast;

  pEntry->u32ClassBegin = 0;
  pEntry->u32ClassEnd   = 255;
  pEntry->u32TestBegin  = 0;
  pEntry->u32TestEnd    = 255;

  return pEntry;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_vReleaseLastEntry()
* deletes the last table entry
**********************************************************************FunctionHeaderEnd***/
void oedt_TestTable_vReleaseLastEntry(void)
{
  OEDT_TABLE_ENTRY *pLast, *pPreLast;

  pLast = oedt_TestTable_pGetLastEntry();
  if(pLast != NULL)
  {
    pPreLast = pLast->pPrev;
    OSAL_vMemoryFree(pLast);
  
    if(pPreLast != NULL) pPreLast->pNext = NULL;
    else oedt_pTestTable = NULL;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_vReleaseAll()
* releases the test table
**********************************************************************FunctionHeaderEnd***/
void oedt_TestTable_vReleaseAll(void)
{
  OEDT_TABLE_ENTRY* pEntry = oedt_pTestTable;
  OEDT_TABLE_ENTRY* pNext;

  while(pEntry != NULL)
  {
    pNext = pEntry->pNext;
    OSAL_vMemoryFree(pEntry);

    pEntry = pNext;
  }

  oedt_pTestTable = NULL;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_u32GetDimension()
* returns the size of the test table
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_TestTable_u32GetDimension(void)
{
  OEDT_TABLE_ENTRY* pEntry = oedt_pTestTable;
  tU32 u32Cnt = 0;

  while(pEntry != NULL)
  {
    ++u32Cnt;
    pEntry = pEntry->pNext;
  }

  return u32Cnt;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_TestTable_vReconfigure()
* reconfigures the test table size
**********************************************************************FunctionHeaderEnd***/
void oedt_TestTable_vReconfigure(tU8 u8TableSize)
{
  while(oedt_TestTable_u32GetDimension() < (tU32)u8TableSize) oedt_TestTable_pCreateNewEntry();
  while(oedt_TestTable_u32GetDimension() > (tU32)u8TableSize) oedt_TestTable_vReleaseLastEntry();
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fTableSetSize()
* sets the table size
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fTableSetSize(tU8 u8TableSize)
{
  if(u8TableSize < 1) return FALSE; /* table deletion not allowed (at least one entry) */
  oedt_TestTable_vReconfigure(u8TableSize);

  return TRUE;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vCorrectBoundary()
* ensures for valid address ranges
**********************************************************************FunctionHeaderEnd***/
void oedt_vCorrectBoundary(tU32 *u32ClassBegin, tU32 *u32ClassEnd, tU32 *u32TestBegin, tU32 *u32TestEnd)
{
  if(*u32ClassEnd < *u32ClassBegin) *u32ClassEnd = *u32ClassBegin;
  if(*u32TestEnd < *u32TestBegin)   *u32TestEnd  = *u32TestBegin;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vTableSetEntry()
* configures a line within test table
**********************************************************************FunctionHeaderEnd***/
void oedt_vTableSetEntry(tU8 u8EntryIdx, tU8 u8ClassBegin, tU8 u8ClassEnd, tU8 u8TestBegin, tU8 u8TestEnd)
{
  OEDT_TABLE_ENTRY *pEntry = oedt_pTestTable;
  tU32 u32EntryCnt = 0;
  /* tU32 u32TableDim = oedt_TestTable_u32GetDimension(); */

  while(pEntry != NULL)
  {
    if((tU32)u8EntryIdx == u32EntryCnt)
    {
      pEntry->u32ClassBegin = (tU32)u8ClassBegin;
      pEntry->u32ClassEnd   = (tU32)u8ClassEnd;
      pEntry->u32TestBegin  = (tU32)u8TestBegin;
      pEntry->u32TestEnd    = (tU32)u8TestEnd;
      oedt_vCorrectBoundary(&(pEntry->u32ClassBegin), &(pEntry->u32ClassEnd), &(pEntry->u32TestBegin), &(pEntry->u32TestEnd));

      return;
    }

    ++u32EntryCnt;
    pEntry = pEntry->pNext;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fTableSave()
* stores a user configured tester table to configured path
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fTableSave(void)
{
  tBool fRet = TRUE;
  OSAL_tIODescriptor hFd;
  OEDT_TABLE_ENTRY  *pEntry = oedt_pTestTable;
  tU8 u8EntryCnt = (tU8)oedt_TestTable_u32GetDimension();

  hFd = OSAL_IOCreate(_OEDT_STORAGEPATH_TABLE, OSAL_EN_WRITEONLY);
  if(hFd != OSAL_ERROR)
  {
    OSAL_s32IOWrite(hFd, (tPCS8)&(u8EntryCnt), 1);

    while(pEntry != NULL)
    {
      OSAL_s32IOWrite(hFd, (tPCS8)&(pEntry->u32ClassBegin), 1);
      OSAL_s32IOWrite(hFd, (tPCS8)&(pEntry->u32ClassEnd), 1);
      OSAL_s32IOWrite(hFd, (tPCS8)&(pEntry->u32TestBegin), 1);
      OSAL_s32IOWrite(hFd, (tPCS8)&(pEntry->u32TestEnd), 1);
      
      pEntry = pEntry->pNext;
    }

    OSAL_s32IOClose(hFd);
  }
  else
  {
    fRet = FALSE;
  }

  return fRet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fTableLoad()
* loads a former stored tester table
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fTableLoad(void)
{
  OSAL_tIODescriptor hFd;
  OEDT_TABLE_ENTRY *pEntry;
  tU8 u8EntryCnt = 0;
  tBool fRet = TRUE;

  hFd = OSAL_IOOpen(_OEDT_STORAGEPATH_TABLE, OSAL_EN_READONLY);
  if(hFd != OSAL_ERROR)
  {
    OSAL_s32IORead(hFd, (tPS8)&u8EntryCnt, 1);
    oedt_fTableSetSize(u8EntryCnt);

    pEntry = oedt_pTestTable;

    while(pEntry != NULL)
    {
      OSAL_s32IORead(hFd, (tPS8)&(pEntry->u32ClassBegin), 1);
      OSAL_s32IORead(hFd, (tPS8)&(pEntry->u32ClassEnd), 1);
      OSAL_s32IORead(hFd, (tPS8)&(pEntry->u32TestBegin), 1);
      OSAL_s32IORead(hFd, (tPS8)&(pEntry->u32TestEnd), 1);

      pEntry = pEntry->pNext;
    }

    OSAL_s32IOClose(hFd);
  }
  else
  {
    fRet = FALSE;
  }

  return fRet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vTableList()
* lists the test table (fMode means detailed or not)
**********************************************************************FunctionHeaderEnd***/
void oedt_vTableList(tBool fMode)
{
  OEDT_TABLE_ENTRY *pEntry = oedt_pTestTable;
  tU32 u32LineCnt = 0;
  OEDT_TESTCON_ENTRY* pClassEntry;
  OEDT_TEST_INFO      sTestInfo;

  if(fMode == TRUE) oedt_DB_vBuildSizeMap();

  oedt_vSendLineMsg(OEDT_LINEID_TABLE);

  while(pEntry != NULL)
  {
    oedt_vSendTableEntry(u32LineCnt, pEntry->u32ClassBegin, pEntry->u32ClassEnd, pEntry->u32TestBegin, pEntry->u32TestEnd);

    if(fMode == TRUE)
    {
      sTestInfo.u32ClassID = pEntry->u32ClassBegin;
      sTestInfo.u32TestID  = pEntry->u32TestBegin;

      while((sTestInfo.u32ClassID < oedt_DB_u32GetMaxEntries()) && (sTestInfo.u32ClassID <= pEntry->u32ClassEnd))
      {
        pClassEntry = (OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(sTestInfo.u32ClassID);
        while((sTestInfo.u32TestID < oedt_DB_u32GetMaxTest(sTestInfo.u32ClassID)) && (sTestInfo.u32TestID <= pEntry->u32TestEnd))
        {
          if(oedt_bIsTestExc4HW(pClassEntry->pFuncs[sTestInfo.u32TestID].u32HwExcMask) != TRUE)
          {
            sTestInfo.pu8ClassName  = pClassEntry->pClassName;
            sTestInfo.pu8TestName   = pClassEntry->pFuncs[sTestInfo.u32TestID].pU8TestName;
            sTestInfo.u32TimeOutVal = (tU32)pClassEntry->pFuncs[sTestInfo.u32TestID].u16SecTimeout;
  
            if(sTestInfo.u32TimeOutVal == 0) sTestInfo.u32TimeOutVal = _OEDT_TEST_TIMEOUT_DEFAULT;
  
            oedt_vDelay();
            oedt_vSendTestCfgMsg(&sTestInfo);
          }
          ++sTestInfo.u32TestID;
        }
        sTestInfo.u32TestID = 0;
        ++sTestInfo.u32ClassID;
      }
    }

    ++u32LineCnt;
    pEntry = pEntry->pNext;
  }

  oedt_vSendLineMsg(OEDT_LINEID_NORMAL);

  if(fMode == TRUE) oedt_DB_vReleaseSizeMap();
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fConfigSave()
* save oedt's internal configuration to configured path
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fConfigSave(void)
{
  tBool fRet  = TRUE;
  OSAL_tIODescriptor hFd;

  hFd = OSAL_IOCreate(_OEDT_STORAGEPATH_CONFIG, OSAL_EN_WRITEONLY);
  if(hFd != OSAL_ERROR)
  {
    OSAL_s32IOWrite(hFd, (tPCS8)&(oedt_sConfig.eFlowMode), 4);
    OSAL_s32IOWrite(hFd, (tPCS8)&(oedt_sConfig.u8SpecMode), 1);
    OSAL_s32IOWrite(hFd, (tPCS8)&(oedt_sConfig.u32SelectedDataset), 4);

    OSAL_s32IOClose(hFd);
  }
  else
  {
    fRet = FALSE;
  }

  return fRet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fConfigLoad()
* load oedt's internal configuration from configured path
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fConfigLoad(void)
{
  OSAL_tIODescriptor hFd;
  tBool fRet = TRUE;

  hFd = OSAL_IOOpen(_OEDT_STORAGEPATH_CONFIG, OSAL_EN_READONLY);
  if(hFd != OSAL_ERROR)
  {
    OSAL_s32IORead(hFd, (tPS8)&(oedt_sConfig.eFlowMode), 4);
    OSAL_s32IORead(hFd, (tPS8)&(oedt_sConfig.u8SpecMode), 1);
    OSAL_s32IORead(hFd, (tPS8)&(oedt_sConfig.u32SelectedDataset), 4);
    oedt_vDatasetSelect(oedt_sConfig.u32SelectedDataset);

    OSAL_s32IOClose(hFd);
  }
  else
  {
    fRet = FALSE;
  }

  return fRet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vDatasetList()
* lists current sellected dataset
**********************************************************************FunctionHeaderEnd***/
void oedt_vDatasetList(void)
{
  OEDT_TEST_INFO sTestInfo;
  OEDT_TESTCON_ENTRY* pClassEntry;

  sTestInfo.u32ClassID = 0;
  sTestInfo.u32TestID  = 0;

  oedt_DB_vBuildSizeMap();
  oedt_vSendLineMsg(OEDT_LINEID_DATASET);

  while(sTestInfo.u32ClassID < oedt_DB_u32GetMaxEntries())
  {
    pClassEntry = (OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(sTestInfo.u32ClassID);
    while(sTestInfo.u32TestID < oedt_DB_u32GetMaxTest(sTestInfo.u32ClassID))
    {
      if(oedt_bIsTestExc4HW(pClassEntry->pFuncs[sTestInfo.u32TestID].u32HwExcMask) != TRUE)
      {
        sTestInfo.pu8ClassName  = pClassEntry->pClassName;
        sTestInfo.pu8TestName   = pClassEntry->pFuncs[sTestInfo.u32TestID].pU8TestName;
        sTestInfo.u32TimeOutVal = (tU32)pClassEntry->pFuncs[sTestInfo.u32TestID].u16SecTimeout;
  
        if(sTestInfo.u32TimeOutVal == 0) sTestInfo.u32TimeOutVal = _OEDT_TEST_TIMEOUT_DEFAULT;
  
        oedt_vDelay();
        oedt_vSendTestCfgMsg(&sTestInfo);
      }
      ++(sTestInfo.u32TestID);
    }
    sTestInfo.u32TestID = 0;
    ++(sTestInfo.u32ClassID);
  }

  oedt_vSendLineMsg(OEDT_LINEID_NORMAL);
  oedt_DB_vReleaseSizeMap();
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vDatabaseList()
* lists registered datasets from db
**********************************************************************FunctionHeaderEnd***/
void oedt_vDatabaseList(void)
{
  tU32 u32Idx = 0;

  oedt_vSendLineMsg(OEDT_LINEID_DATABASE);

  while(u32Idx < OEDT_DATABASE_REGISTERED_SETS)
  {
    oedt_vSendDatasetMsg(u32Idx, (tPCS8)((OEDT_DATASET_ENTRY*)oedt_DB_pGetDatasetByID(u32Idx))->pTestConName);
    ++u32Idx;
  }

  oedt_vSendLineMsg(OEDT_LINEID_NORMAL);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vStartTest()
* starts testing procedure in a new thread
**********************************************************************FunctionHeaderEnd***/
void oedt_vStartTest(void)
{
  if(oedt_TestHandler_threadID != 0)
  {
    OSAL_s32ThreadDelete(oedt_TestHandler_threadID);
    oedt_TestHandler_threadID = 0;
  }

  oedt_bStopFlg = FALSE;
  oedt_eState   = OEDT_STATE_RUNNING;
  oedt_TestHandler_threadID = OSAL_ThreadSpawn(&oedt_TestHandler_threadAttr);    
}

/*********************************************************************FunctionHeaderBegin***
* oedt_COMP_vBackupTable()
* backup oedt's table
**********************************************************************FunctionHeaderEnd***/
void oedt_COMP_vBackupTable(void)
{
  tU32 u32Cnt = 0;
  OEDT_TABLE_ENTRY *pEntry = oedt_pTestTable;

  if(oedt_pu8TestTable_Bkp == NULL)
  {
    oedt_pu8TestTable_Bkp = (tU8*)OSAL_pvMemoryAllocate((oedt_TestTable_u32GetDimension() * 4) + 1);

    oedt_pu8TestTable_Bkp[0] = (tU8)oedt_TestTable_u32GetDimension();

    while(pEntry != NULL)
    {
      oedt_pu8TestTable_Bkp[u32Cnt + 1] = (tU8)pEntry->u32ClassBegin;
      oedt_pu8TestTable_Bkp[u32Cnt + 2] = (tU8)pEntry->u32ClassEnd;
      oedt_pu8TestTable_Bkp[u32Cnt + 3] = (tU8)pEntry->u32TestBegin;
      oedt_pu8TestTable_Bkp[u32Cnt + 4] = (tU8)pEntry->u32TestEnd;
    
      pEntry = pEntry->pNext;
      u32Cnt += 4;
    }
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_COMP_vRestoreTable()
* restores oedt's table from backup
**********************************************************************FunctionHeaderEnd***/
void oedt_COMP_vRestoreTable(void)
{
  tU32 u32Cnt = 0;
  OEDT_TABLE_ENTRY *pEntry = oedt_pTestTable;

  if(oedt_pu8TestTable_Bkp == NULL) return;
  
  oedt_fTableSetSize(oedt_pu8TestTable_Bkp[0]);

  while(pEntry != NULL)
  {
    pEntry->u32ClassBegin = oedt_pu8TestTable_Bkp[u32Cnt + 1];
    pEntry->u32ClassEnd   = oedt_pu8TestTable_Bkp[u32Cnt + 2];
    pEntry->u32TestBegin  = oedt_pu8TestTable_Bkp[u32Cnt + 3];
    pEntry->u32TestEnd    = oedt_pu8TestTable_Bkp[u32Cnt + 4];

    pEntry = pEntry->pNext;
    u32Cnt += 4;
  }

  OSAL_vMemoryFree(oedt_pu8TestTable_Bkp);
  oedt_pu8TestTable_Bkp = NULL;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_COMP_vBackupConfig()
* backup oedt's config
**********************************************************************FunctionHeaderEnd***/
void oedt_COMP_vBackupConfig(void)
{
  if(oedt_pConfig_Bkp == NULL)
  {
    oedt_pConfig_Bkp = (OEDT_CONFIG*)OSAL_pvMemoryAllocate(sizeof(OEDT_CONFIG));
    OSAL_pvMemoryCopy((void*)oedt_pConfig_Bkp, (void*)&oedt_sConfig, sizeof(OEDT_CONFIG));
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_COMP_vRestoreConfig()
* restores oedt's config from backup
**********************************************************************FunctionHeaderEnd***/
void oedt_COMP_vRestoreConfig(void)
{
  if(oedt_pConfig_Bkp != NULL)
  {
    OSAL_pvMemoryCopy((void*)&oedt_sConfig, (void*)oedt_pConfig_Bkp, sizeof(OEDT_CONFIG));
    oedt_vDatasetSelect(oedt_sConfig.u32SelectedDataset);
    OSAL_vMemoryFree(oedt_pConfig_Bkp);
    oedt_pConfig_Bkp = NULL;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vStartTestCompatible()
* backups config and testtable and performs old fashioned oedt functionality
**********************************************************************FunctionHeaderEnd***/
void oedt_vStartTestCompatible(tU8 u8ClassBeg, tU8 u8ClassEnd, tU8 u8TestBeg, tU8 u8TestEnd, tBool fEndurance)
{
  oedt_COMP_vBackupTable();
  oedt_COMP_vBackupConfig();

  oedt_vModeSet_Flow(OEDT_MODE_FLOW_SEQUENTIAL);
  oedt_vModeSet_Special(OEDT_MODE_SPECIAL_ENDURANCE, fEndurance);
  oedt_vModeSet_Special(OEDT_MODE_SPECIAL_LOAD, FALSE);
  oedt_fTableSetSize(1);
  oedt_vTableSetEntry(0, u8ClassBeg, u8ClassEnd, u8TestBeg, u8TestEnd);
  oedt_vDatasetSelect(oedt_sConfig.u32SelectedDataset);

  oedt_vStartTest();
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vInitCore()
* inits oedt's core
**********************************************************************FunctionHeaderEnd***/
void oedt_vInitCore(void)
{
  oedt_sConfig.eFlowMode  = OEDT_MODE_FLOW_SEQUENTIAL;
  oedt_sConfig.u8SpecMode = 0;

  oedt_u32ExtInfoLen = 0;
  memset(oedt_ps8ExtInfo, 0, OEDT_EXTINFO_MAXLEN);

  if(oedt_TestTable_u32GetDimension() < 1) oedt_fTableSetSize(1);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vConfigList()
* lists oedt's internal configuration
**********************************************************************FunctionHeaderEnd***/
void oedt_vConfigList(void)
{
  oedt_vSendLineMsg(OEDT_LINEID_CONFIG);
  oedt_vSendDatasetMsg(oedt_DB_u32GetSelectedSet(), (tPCS8)((OEDT_DATASET_ENTRY*)oedt_DB_pGetDataset())->pTestConName);
  oedt_vSendConfigMsg(oedt_sConfig.eFlowMode, oedt_sConfig.u8SpecMode);
  oedt_vSendLineMsg(OEDT_LINEID_NORMAL);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_ResTable_vInit()
* initializes the result array for endurance testing
**********************************************************************FunctionHeaderEnd***/
void oedt_ResTable_vInit(void)
{
  tU32 u32Cnt, u32MaxClasses;

  u32MaxClasses = oedt_DB_u32GetMaxEntries();
  oedt_ResTable_pStats = (OEDT_TESTER_STATS**)OSAL_pvMemoryAllocate(sizeof(OEDT_TESTER_STATS*) * u32MaxClasses);
  
  u32Cnt = 0;
  oedt_ResTable_u32MaxID = 0;
  while(u32Cnt < u32MaxClasses)
  {
    if(oedt_ResTable_u32MaxID < oedt_DB_u32GetMaxTest(u32Cnt)) oedt_ResTable_u32MaxID = oedt_DB_u32GetMaxTest(u32Cnt);
    ++u32Cnt;
  }

  u32Cnt = 0;
  while(u32Cnt < u32MaxClasses)
  {
    oedt_ResTable_pStats[u32Cnt] = (OEDT_TESTER_STATS*)OSAL_pvMemoryAllocate(sizeof(OEDT_TESTER_STATS) * oedt_ResTable_u32MaxID);
    memset(oedt_ResTable_pStats[u32Cnt], 0, (size_t)(sizeof(OEDT_TESTER_STATS) * oedt_ResTable_u32MaxID));
    ++u32Cnt;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_ResTable_vRelease()
* releases endurance testers result array
**********************************************************************FunctionHeaderEnd***/
void oedt_ResTable_vRelease(void)
{
  tU32 u32Cnt = 0;

  while(u32Cnt < oedt_DB_u32GetMaxEntries())
  {
    OSAL_vMemoryFree(oedt_ResTable_pStats[u32Cnt]);
    ++u32Cnt;
  }

  OSAL_vMemoryFree(oedt_ResTable_pStats);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_ResTable_vStoreStats()
* stores endurance statistics
**********************************************************************FunctionHeaderEnd***/
void oedt_ResTable_vStoreStats(tU32 u32ClassID, tU32 u32TestID, tU32 u32ErrCode, tU8 u8TimedOut)
{
  if((u32ClassID >= oedt_DB_u32GetMaxEntries()) || (u32TestID >= oedt_ResTable_u32MaxID)) return;

  if(u32ErrCode != 0)
  {
    ++(oedt_ResTable_pStats[u32ClassID][u32TestID].u32Failed);
  }
  else
  {
    if(u8TimedOut != 0)
    {
      ++(oedt_ResTable_pStats[u32ClassID][u32TestID].u32TimeOut);
    }
    else
    {
      ++(oedt_ResTable_pStats[u32ClassID][u32TestID].u32Passed);
    }
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vStoreStats()
* stores statistics
**********************************************************************FunctionHeaderEnd***/
void oedt_vStoreStats(tU32 u32ClassID, tU32 u32TestID, tU32 u32ErrCode, tU8 u8TimedOut)
{
  if(u32ErrCode != 0)
  {
    ++(oedt_sOverallTestStats.u32Failed);
  }
  else
  {
    if(u8TimedOut != 0)
    {
      ++(oedt_sOverallTestStats.u32TimeOut);
    }
    else
    {
      ++(oedt_sOverallTestStats.u32Passed);
    }
  }

  if(oedt_sConfig.u8SpecMode &(tU8) OEDT_MODE_SPECIAL_ENDURANCE)
  {
    oedt_ResTable_vStoreStats(u32ClassID, u32TestID, u32ErrCode, u8TimedOut);
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSendStats()
* sends stored statistics
**********************************************************************FunctionHeaderEnd***/
void oedt_vSendStats(void)
{
  tU32 u32ClassCnt = 0;
  tU32 u32TestCnt  = 0;

  oedt_vSendOverallStatsMsg(oedt_sOverallTestStats.u32Passed, oedt_sOverallTestStats.u32Failed, oedt_sOverallTestStats.u32TimeOut);
  
  if(oedt_sConfig.u8SpecMode & (tU8) OEDT_MODE_SPECIAL_ENDURANCE)
  {
    oedt_vSendLineMsg(OEDT_LINEID_NORMAL);

    while(u32ClassCnt < oedt_DB_u32GetMaxEntries())
    {
      while(u32TestCnt < oedt_DB_u32GetMaxTest(u32ClassCnt))
      {
        if((oedt_ResTable_pStats[u32ClassCnt][u32TestCnt].u32Failed != 0) ||
           (oedt_ResTable_pStats[u32ClassCnt][u32TestCnt].u32Passed != 0) ||
           (oedt_ResTable_pStats[u32ClassCnt][u32TestCnt].u32TimeOut != 0))
        {
          oedt_vDelay();
          oedt_vSendEnduranceStatsMsg(u32ClassCnt,
                                      u32TestCnt,
                                      &(oedt_ResTable_pStats[u32ClassCnt][u32TestCnt]),
                                      (tPCS8)((OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(u32ClassCnt))->pClassName,
                                      (tPCS8)((OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(u32ClassCnt))->pFuncs[u32TestCnt].pU8TestName);
        }
       
        ++u32TestCnt;
      }
      u32TestCnt = 0;
      ++u32ClassCnt;
    }

    oedt_vSendLineMsg(OEDT_LINEID_NORMAL);
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vShowExtInfo()
* sends ext info string
**********************************************************************FunctionHeaderEnd***/
void oedt_vShowExtInfo(void)
{
  if((oedt_u32ExtInfoLen == 0) || (oedt_ps8ExtInfo[0] == 0)) return;
  
  oedt_vSendExtMsg(oedt_ps8ExtInfo, oedt_u32ExtInfoLen);

  oedt_u32ExtInfoLen = 0;
  memset(oedt_ps8ExtInfo, 0, OEDT_EXTINFO_MAXLEN);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vSetExtInfo()
* function is called by user-implemented test function to submit ext info string
**********************************************************************FunctionHeaderEnd***/
void oedt_vSetExtInfo(tPCS8 ps8InfoStr, tU32 u32Len)
{
  if(oedt_sConfig.eFlowMode != OEDT_MODE_FLOW_SEQUENTIAL) return;

  if(u32Len > OEDT_EXTINFO_MAXLEN) 
    oedt_u32ExtInfoLen = OEDT_EXTINFO_MAXLEN;
  else 
    oedt_u32ExtInfoLen = u32Len;

  OSAL_pvMemoryCopy(oedt_ps8ExtInfo, ps8InfoStr, oedt_u32ExtInfoLen);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vInitTaskControl()
* allocates and inits the structures for task control
**********************************************************************FunctionHeaderEnd***/
void oedt_vInitTaskControl(void)
{
  OEDT_TABLE_ENTRY *pEntry = oedt_pTestTable;
  tU32 u32EntryCnt = oedt_TestTable_u32GetDimension();
  tU32 u32Idx = 0;

  if(oedt_pTaskControl == NULL)
    oedt_pTaskControl = (OEDT_TASK_CONTROL*)OSAL_pvMemoryAllocate(u32EntryCnt * sizeof(OEDT_TASK_CONTROL));

  while((u32Idx < u32EntryCnt) && (pEntry != NULL))
  {
    oedt_pTaskControl[u32Idx].pTableEntry = (void*)pEntry;
    oedt_pTaskControl[u32Idx].fInvalid    = TRUE;
    oedt_pTaskControl[u32Idx].hThread     = 0;

    pEntry = pEntry->pNext;
    ++u32Idx;
  }
}


/*********************************************************************FunctionHeaderBegin***
* oedt_vReleaseTaskControl()
* releases allocated mem for task control structures
**********************************************************************FunctionHeaderEnd***/
void oedt_vReleaseTaskControl(void)
{
  OSAL_vMemoryFree(oedt_pTaskControl);
  oedt_pTaskControl = NULL;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_vSetRunning()
* sets a task (identified by id) to RUNNING
**********************************************************************FunctionHeaderEnd***/
void oedt_SyncHandler_vSetRunning(tU32 u32Idx)
{
  oedt_pTaskControl[u32Idx].u8TaskState = OEDT_TASKSYNC_RUNNING;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_vSetWaiting()
* sets a task (identified by id) to WAITING
**********************************************************************FunctionHeaderEnd***/
void oedt_SyncHandler_vSetWaiting(tU32 u32Idx)
{
  oedt_pTaskControl[u32Idx].u8TaskState = OEDT_TASKSYNC_WAITING;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_vSetMaskedOut()
* sets a task (identified by id) to MASKED OUT
**********************************************************************FunctionHeaderEnd***/
void oedt_SyncHandler_vSetMaskedOut(tU32 u32Idx)
{
  oedt_pTaskControl[u32Idx].fInvalid    = TRUE;
  oedt_pTaskControl[u32Idx].u8TaskState = OEDT_TASKSYNC_MASKOUT;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_fIsLineFinished()
* checks whether all previous test threads are finished 
* (whether all tasks are WAITING or MASKED OUT)
**********************************************************************FunctionHeaderEnd***/
tBool oedt_SyncHandler_fIsLineFinished(void)
{
  tU32 u32Idx = 0;
  tU32 u32TableSize;

  if(oedt_sConfig.eFlowMode != OEDT_MODE_FLOW_PARALLEL_SYNC) return FALSE;

  u32TableSize = oedt_TestTable_u32GetDimension();

  while(u32Idx < u32TableSize)
  {
    if(oedt_pTaskControl[u32Idx].u8TaskState > OEDT_TASKSYNC_WAITING) return FALSE;
    ++u32Idx;
  }

  return TRUE;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_vResumeTasks()
* resumes all suspended test threads (parallel start in sync mode)
**********************************************************************FunctionHeaderEnd***/
void oedt_SyncHandler_vResumeTasks(void)
{
  tU32 u32TableSize, u32Idx = 0;

  if(oedt_sConfig.eFlowMode != OEDT_MODE_FLOW_PARALLEL_SYNC) return;

  u32TableSize = oedt_TestTable_u32GetDimension();

  while(u32Idx < u32TableSize)
  {
    if(oedt_pTaskControl[u32Idx].u8TaskState == OEDT_TASKSYNC_WAITING)
    {
      OSAL_s32ThreadResume(oedt_pTaskControl[u32Idx].hThread);
      oedt_pTaskControl[u32Idx].u8TaskState = OEDT_TASKSYNC_RUNNING;
    }
    ++u32Idx;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_SyncHandler_vWait4Task()
* waits for semaphore (released by the test thread) and suspends the test thread
**********************************************************************FunctionHeaderEnd***/
void oedt_SyncHandler_vWait4Task(tU32 u32Idx)
{
  if(oedt_sConfig.eFlowMode != OEDT_MODE_FLOW_PARALLEL_SYNC) return;

  OSAL_s32SemaphoreCreate(_OEDT_SEMAPHORENAME_TASKWAIT, &oedt_hTaskWaitSem, 0);
  OSAL_s32SemaphoreWait(oedt_hTaskWaitSem, OSAL_C_TIMEOUT_FOREVER);
  OSAL_s32SemaphoreClose(oedt_hTaskWaitSem);
  OSAL_s32SemaphoreDelete(_OEDT_SEMAPHORENAME_TASKWAIT);

  OSAL_s32ThreadSuspend(oedt_pTaskControl[u32Idx].hThread);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vStartByMsgQ()
* fills the message queue to start the testing procedure
**********************************************************************FunctionHeaderEnd***/
tBool oedt_vStartByMsgQ(tU32 *pu32Columns2Process)
{
  if(oedt_bStopFlg == TRUE) return FALSE;

  if(oedt_u32TableLineIdx >= oedt_TestTable_u32GetDimension())
  {
    if(oedt_sConfig.u8SpecMode & (tU8) OEDT_MODE_SPECIAL_ENDURANCE)
    {
      oedt_u32TableLineIdx = 0;
    }
    else
    {
      return FALSE;
    }
  }
    
  switch(oedt_sConfig.eFlowMode)
  {
  case OEDT_MODE_FLOW_SEQUENTIAL:
    *pu32Columns2Process = 1;
    OSAL_s32MessageQueuePost(oedt_hJobMsgQ, (tPCU8)&oedt_u32TableLineIdx, sizeof(tU32), OSAL_NULL);
    oedt_SyncHandler_vSetRunning(oedt_u32TableLineIdx);

    ++oedt_u32TableLineIdx;
    break;

  case OEDT_MODE_FLOW_PARALLEL_ASYNC:
  case OEDT_MODE_FLOW_PARALLEL_SYNC:
    *pu32Columns2Process = oedt_TestTable_u32GetDimension();
    while(oedt_u32TableLineIdx < oedt_TestTable_u32GetDimension())
    {
      OSAL_s32MessageQueuePost(oedt_hJobMsgQ, (tPCU8)&oedt_u32TableLineIdx, sizeof(tU32), OSAL_NULL);
      oedt_SyncHandler_vSetRunning(oedt_u32TableLineIdx);
      ++oedt_u32TableLineIdx;
    }
    break;
  }

  return TRUE;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vInitTimer4Test()
* recalculates the timeout value for a test (worst case value)
**********************************************************************FunctionHeaderEnd***/
void oedt_vInitTimer4Test(tU32 u32Idx)
{
  tU32 u32Timeout = ((OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32ClassID))->pFuncs[oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32TestID].u16SecTimeout;
  tU32 u32TableSize = oedt_TestTable_u32GetDimension();
  tU32 u32Cnt = 0;
  tU32 u32Multiplier = 0;

  while(u32Cnt < u32TableSize)
  {
    if(oedt_pTaskControl[u32Cnt].u8TaskState > OEDT_TASKSYNC_MASKOUT) ++u32Multiplier;
    ++u32Cnt;
  }

  OSAL_s32TimerSetTime(oedt_pTaskControl[(tU32)u32Idx].hTimer, (OSAL_tMSecond)((u32Timeout * u32Multiplier) * 1000UL), 0);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_fInitNextTest()
* searches the next valid test entry within the test dataset (for a given thread id)
**********************************************************************FunctionHeaderEnd***/
tBool oedt_fInitNextTest(tU32 u32Idx)
{
  OEDT_TESTCON_ENTRY* pClassEntry;

  if(oedt_pTaskControl[u32Idx].fInvalid == TRUE)
  {
    oedt_pTaskControl[u32Idx].fInvalid             = FALSE;
    oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID = ((OEDT_TABLE_ENTRY*)oedt_pTaskControl[u32Idx].pTableEntry)->u32ClassBegin;
    oedt_pTaskControl[u32Idx].sTestInfo.u32TestID  = ((OEDT_TABLE_ENTRY*)oedt_pTaskControl[u32Idx].pTableEntry)->u32TestBegin;
  }
  else
  {
    ++(oedt_pTaskControl[u32Idx].sTestInfo.u32TestID);
  }

  while((oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID <= ((OEDT_TABLE_ENTRY*)oedt_pTaskControl[u32Idx].pTableEntry)->u32ClassEnd)
        && (oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID < oedt_DB_u32GetMaxEntries())
        && (oedt_bStopFlg == FALSE))
  {
    pClassEntry = (OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID);
    while((oedt_pTaskControl[u32Idx].sTestInfo.u32TestID <= ((OEDT_TABLE_ENTRY*)oedt_pTaskControl[u32Idx].pTableEntry)->u32TestEnd)
          && (oedt_pTaskControl[u32Idx].sTestInfo.u32TestID) < oedt_DB_u32GetMaxTest(oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID)
          && (oedt_bStopFlg == FALSE))
    {
      if(oedt_bIsTestExc4HW(pClassEntry->pFuncs[oedt_pTaskControl[u32Idx].sTestInfo.u32TestID].u32HwExcMask) != TRUE)
      {
        oedt_pTaskControl[u32Idx].sTestInfo.pu8ClassName  = pClassEntry->pClassName;
        oedt_pTaskControl[u32Idx].sTestInfo.pu8TestName   = pClassEntry->pFuncs[oedt_pTaskControl[u32Idx].sTestInfo.u32TestID].pU8TestName;
        oedt_pTaskControl[u32Idx].sTestInfo.u32TimeOutVal = pClassEntry->pFuncs[oedt_pTaskControl[u32Idx].sTestInfo.u32TestID].u16SecTimeout;
        return TRUE;
      }
      ++(oedt_pTaskControl[u32Idx].sTestInfo.u32TestID);
    }
    ++(oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID);
    oedt_pTaskControl[u32Idx].sTestInfo.u32TestID = ((OEDT_TABLE_ENTRY*)oedt_pTaskControl[u32Idx].pTableEntry)->u32TestBegin;
  }

  return FALSE;
}

/************************************************************************/
/* oedt_GetCurrentTestConfiguration
/* returns the OEDT_FUNC_CFG struct that belongs to the test specified 
/* by u32Idx
/************************************************************************/
OEDT_FUNC_CFG oedt_GetCurrentTestConfiguration(tU32 u32Idx)
{
  tU32 u32TestClassID = oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32ClassID;
  tU32 u32TestID = oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32TestID;

  OEDT_TESTCON_ENTRY* testClass = (OEDT_TESTCON_ENTRY*)oedt_DB_GetDatasetEntry(u32TestClassID);
  return testClass->pFuncs[u32TestID];
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vTester()
* the test thread-func (calls the test)
**********************************************************************FunctionHeaderEnd***/
void oedt_vTester(void* u32Idx)
{
  /* NU_SUPERV_USER_VARIABLES */

  OSAL_s32TimerCreate(oedt_vThreadTimerCallback, u32Idx, &(oedt_pTaskControl[(tU32)u32Idx].hTimer));
  oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32ErrCode = 0;

  if(oedt_sConfig.eFlowMode == OEDT_MODE_FLOW_PARALLEL_SYNC) OSAL_s32SemaphorePost(oedt_hTaskWaitSem);

  oedt_vSendTestMsg(&(oedt_pTaskControl[(tU32)u32Idx].sTestInfo));
  oedt_vInitTimer4Test((tU32)u32Idx);

  OEDT_FUNC_CFG currentTestConfiguration = oedt_GetCurrentTestConfiguration((tU32)u32Idx);
  oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u32ErrCode = (*(currentTestConfiguration.pTestFunctions.pTestFunc))();
 
  OSAL_s32TimerDelete(oedt_pTaskControl[(tU32)u32Idx].hTimer);
  oedt_pTaskControl[(tU32)u32Idx].sTestInfo.u8TimedOut = 0;
  OSAL_s32MessageQueuePost(oedt_hJobMsgQ, (tPCU8)&u32Idx, sizeof(tU32), OSAL_NULL);
  OSAL_vThreadExit();
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vTestHandler()
* the test handler
**********************************************************************FunctionHeaderEnd***/
void oedt_vTestHandler(void)     
{
  tU32 u32Idx = 0;
   #define MAX_MSG_LEN_RCV     10000
   char msgcontent_rcv[MAX_MSG_LEN_RCV];
   tU32 loop = 0;
   tU32 u32Lines2Process;
   OSAL_trThreadAttribute threadAttr;
   tS8 ps8ThreadName[16];
   tU8 honey = 0x00;
   tU32 u32UsedMemoBeforeTest = 0;
   tU32 u32UsedMemoAfterTest = 0;
   OEDT_FUNC_CFG currentTestConfiguration;

   tS32 s32RetAddr = 0; // for reading board name from registry
   tS32 s32ErrorCode = 0; // for reading board name from registry

   tBool bPerformReboot = FALSE;

   oedt_u32TableLineIdx = 0;

   oedt_DB_vBuildSizeMap();

   oedt_ResTable_vInit();
   oedt_sOverallTestStats.u32Passed = 0;
   oedt_sOverallTestStats.u32Failed = 0;
   oedt_sOverallTestStats.u32TimeOut = 0;

   // open registry handle to read board name
   // board name is saved to oedt_boardname for HW exclusion
   if ((s32RetAddr = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/BOARDCFG", OSAL_EN_READWRITE)) != OSAL_ERROR)
   {
      OSAL_trIOCtrlRegistry rReg;
      tS8 pBuffer[30];

      // fill struct
      memset((void*) &rReg, 0, sizeof(OSAL_trIOCtrlRegistry));
      rReg.pcos8Name = (tS8*) "BOARD_NAME";
      rReg.u32Size = 30;
      rReg.ps8Value = (tPU8) pBuffer;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;

      // try to get registry value
      if ((s32ErrorCode = OSAL_s32IOControl(s32RetAddr, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32) & rReg)) != OSAL_ERROR)
      {
         OSAL_szStringCopy(oedt_boardname, (char*) pBuffer);
      } // if ( ( s32ErrorCode =  OSAL_s32IOControl ( ... ) ) )


      memset((void*) &rReg, 0, sizeof(OSAL_trIOCtrlRegistry));
      rReg.pcos8Name = (tS8*) "OS_VARIANT_STR";
      rReg.u32Size = 30;
      rReg.ps8Value = (tPU8) pBuffer;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;

      // try to get registry value
      if ((s32ErrorCode = OSAL_s32IOControl(s32RetAddr, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32) & rReg)) != OSAL_ERROR)
      {
         OSAL_szStringCopy(oedt_os_variant, (char*) pBuffer);
      } // if ( ( s32ErrorCode =  OSAL_s32IOControl ( ... ) ) )

      // close handle
      OSAL_s32IOClose(s32RetAddr);
   } // if ( ( s32RetAddr = OSAL_IOOpen ( ... ) ) )

   oedt_vInitTaskControl();

   OSAL_s32MessageQueueCreate(_OEDT_MSGQNAME_TESTER, oedt_TestTable_u32GetDimension() + 10, sizeof(tU32), OSAL_EN_READWRITE, &oedt_hJobMsgQ);

   while (oedt_vStartByMsgQ(&u32Lines2Process))
   {
      while (u32Lines2Process > 0)
      {
         if (oedt_SyncHandler_fIsLineFinished() == TRUE) 
           oedt_SyncHandler_vResumeTasks();

         OSAL_s32MessageQueueWait(oedt_hJobMsgQ, (tPU8) msgcontent_rcv, MAX_MSG_LEN_RCV, OSAL_NULL, OSAL_C_TIMEOUT_FOREVER);

         if (oedt_pTaskControl[u32Idx].hThread != 0)
         {
            OSAL_s32ThreadDelete(oedt_pTaskControl[u32Idx].hThread);
            oedt_pTaskControl[u32Idx].hThread = 0;

            u32UsedMemoAfterTest = u32GetUsedMemory();
            oedt_pTaskControl[(tU32) u32Idx].sTestInfo.s32TestConsumedMemory = (u32UsedMemoAfterTest - u32UsedMemoBeforeTest);
            oedt_pTaskControl[(tU32) u32Idx].sTestInfo.u32FreeMemory = u32GetSystemFreeMemory();

            oedt_vSendResultMsg(&(oedt_pTaskControl[u32Idx].sTestInfo));
            oedt_vShowExtInfo();

            oedt_vStoreStats(oedt_pTaskControl[u32Idx].sTestInfo.u32ClassID,
                     oedt_pTaskControl[u32Idx].sTestInfo.u32TestID, oedt_pTaskControl[u32Idx].sTestInfo.u32ErrCode,
                     oedt_pTaskControl[u32Idx].sTestInfo.u8TimedOut);

            currentTestConfiguration = oedt_GetCurrentTestConfiguration(u32Idx);

            if (currentTestConfiguration.pTestFunctions.pFuncTearDown)
            {
              if ( (*(currentTestConfiguration.pTestFunctions.pFuncTearDown))() )
                bPerformReboot = TRUE;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "TearDown function returned an error.");
            } //bTearDownFunctionExists
         }

         if (oedt_fInitNextTest(u32Idx) == TRUE && !bPerformReboot)
         {
            u32UsedMemoBeforeTest = u32GetUsedMemory();

			switch (oedt_u8HoneypotLevel) {
            case 0:
				break;
            case 1:
            default:
                honey = 0x00;
                oedt_vHoneypotInitHeap(honey);
                break;
            case 2:
                honey = 0xA5;
                oedt_vHoneypotInitHeap(honey);
                break;
            }

            sprintf((tString) ps8ThreadName, "%s%03u", "T", loop);

            currentTestConfiguration = oedt_GetCurrentTestConfiguration(u32Idx);

            if (currentTestConfiguration.pTestFunctions.pFuncSetUp)
            {
              if ( (*(currentTestConfiguration.pTestFunctions.pFuncSetUp))() )
                bPerformReboot = TRUE;
                OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "Setup function returned an error.");
            }

            if (!bPerformReboot)
            {

              threadAttr.u32Priority = _OEDT_MAX_PRIO + 2;
              threadAttr.s32StackSize = _OEDT_PROCESS_STACK_SIZE;
              threadAttr.szName = (tString) ps8ThreadName;
              threadAttr.pfEntry = (OSAL_tpfThreadEntry) oedt_vTester;
              threadAttr.pvArg = (void*) u32Idx;

              oedt_pTaskControl[u32Idx].hThread = OSAL_ThreadSpawn(&threadAttr);

              oedt_SyncHandler_vWait4Task(u32Idx);
              oedt_SyncHandler_vSetWaiting(u32Idx);
              loop++;

            } //!bPerformReboot

         }
         else
         {
            oedt_SyncHandler_vSetMaskedOut(u32Idx);
            --u32Lines2Process;
         }
      }
   }

   OSAL_s32MessageQueueClose(oedt_hJobMsgQ);
   OSAL_s32MessageQueueDelete(_OEDT_MSGQNAME_TESTER);

   if (bPerformReboot)
     OEDT_HelperPrintf((tU8)TR_LEVEL_FATAL, "-==REBOOT==-");

   oedt_vSendStats();

   oedt_vReleaseTaskControl();
   oedt_ResTable_vRelease();
   oedt_DB_vReleaseSizeMap();

   oedt_COMP_vRestoreTable();
   oedt_COMP_vRestoreConfig();

   oedt_eState = OEDT_STATE_READY;
   return;
}


