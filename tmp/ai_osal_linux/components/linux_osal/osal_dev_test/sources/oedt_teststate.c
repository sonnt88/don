#include "oedt_teststate.h"
#include <string.h>
#include <errno.h>
extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...);

OEDT_TESTSTATE OEDT_CREATE_TESTSTATE()
{
  OEDT_TESTSTATE state;
  memset(&state, 0, sizeof(OEDT_TESTSTATE));
  return state;
}

tVoid OEDT_TEST_BEGIN_LOOP_STATE(OEDT_TESTSTATE* teststate)
{
  teststate->__loop_state = 1;
}

tVoid _OEDT_TEST_END_LOOP_STATE(OEDT_TESTSTATE* teststate, tCString sFilename, tU32 u32LineNo) {
  if (teststate->__loop_state)
  {
    if (teststate->error_position < 32)
    {
      if (teststate->failure_repeat_count[teststate->error_position])
      {
        OEDT_HelperPrintf((tU8) TR_LEVEL_FATAL,
          "OEDT test loop ended with a failure at %s:%d.\n OSAL_u32ErrorCode is %d, errno is %d.",
        sFilename, u32LineNo, OSAL_u32ErrorCode(), errno);
        teststate->error_code = teststate->error_code + (1 << teststate->error_position);
        teststate->error_count++;
      }
    }
    else
    {
      if (teststate->__error_in_loop)
      {
        teststate->error_code = 0xFFFFFFFF;
        teststate->error_count++;
        teststate->__overflow = 1;
      }
    }
    teststate->error_position++;
    teststate->__loop_state = 0;
    teststate->__error_in_loop = 0;
  }
}

tVoid OEDT_ADD_SUCCESS(OEDT_TESTSTATE* teststate)
{
  if (!teststate->__loop_state)
    teststate->error_position++;
}

tVoid _OEDT_ADD_RESULT(OEDT_TESTSTATE* teststate, tU8 result, tCString sFilename, tU32 u32LineNo)
{
  if (result == TEST_SUCCESS)
    OEDT_ADD_SUCCESS(teststate);
  else
    _OEDT_ADD_FAILURE(teststate, sFilename, u32LineNo);
}

tVoid _OEDT_ADD_FAILURE(OEDT_TESTSTATE* teststate, tCString sFilename, tU32 u32LineNo)
{
  if (!teststate->__loop_state)
  {
    OEDT_HelperPrintf((tU8) TR_LEVEL_FATAL,
      "OEDT test failure at %s:%d.\n OSAL_u32ErrorCode is %d, errno is %d.",
      sFilename, u32LineNo, OSAL_u32ErrorCode(), errno);
    if (teststate->error_position < 32)
    {
      teststate->error_code = teststate->error_code + (1 << teststate->error_position);
      teststate->failure_repeat_count[teststate->error_position]++;
    }
    else
    {
      teststate->error_code = 0xFFFFFFFF;
      teststate->__overflow = 1;
    }
    teststate->error_count++;
    teststate->error_position++;
  }
  else
  {
    teststate->__error_in_loop = 1;
    if (teststate->error_position < 32)
      teststate->failure_repeat_count[teststate->error_position]++;
  }
}

tBool OEDT_CHECK_TESTSTATE_OVERFLOW(OEDT_TESTSTATE* teststate)
{
  return (teststate->__overflow == 1);
}

OEDT_TESTSTATE OEDT_JOIN_TESTSTATES(OEDT_TESTSTATE* teststate1, OEDT_TESTSTATE* teststate2)
{
  tU32 tU32FailureRepeatCountIndex;
  OEDT_TESTSTATE joinedstate = OEDT_CREATE_TESTSTATE();

  joinedstate.error_position = (teststate1->error_position > teststate2->error_position) ? teststate1->error_position : teststate2->error_position;
  joinedstate.error_code     = teststate1->error_code | teststate2->error_code;
  joinedstate.__overflow     = teststate1->__overflow | teststate2->__overflow;

  for (tU32FailureRepeatCountIndex = 0; tU32FailureRepeatCountIndex < 32; tU32FailureRepeatCountIndex++)
  {
    joinedstate.failure_repeat_count[tU32FailureRepeatCountIndex] = teststate1->failure_repeat_count[tU32FailureRepeatCountIndex] + teststate2->failure_repeat_count[tU32FailureRepeatCountIndex];
    if (joinedstate.failure_repeat_count[tU32FailureRepeatCountIndex])
      joinedstate.error_count++;
  }

  return joinedstate;
}

OEDT_TESTSTATE OEDT_CONCAT_TESTSTATES(OEDT_TESTSTATE* teststate1, OEDT_TESTSTATE* teststate2)
{
  tU32 u32Position;
  tU32 u32PositionInConcatedState;
  OEDT_TESTSTATE concatedstate = OEDT_CREATE_TESTSTATE();
  memcpy(&concatedstate, teststate1, sizeof(OEDT_TESTSTATE));
  concatedstate.error_count += teststate2->error_count;
  concatedstate.error_position += teststate2->error_position;

  if (concatedstate.__loop_state)
  {
    concatedstate.__loop_state = 0;
    concatedstate.__error_in_loop = 0;
    if (concatedstate.error_position < 32)
      concatedstate.failure_repeat_count[concatedstate.error_position] = 0;
  }

  for (u32Position = 0; u32Position < 32; u32Position++)
  {
    if ( teststate2->error_code & ((tU32)0x01 << u32Position) ) //error bit is set in testcase 2 for position u32Position
    {
      u32PositionInConcatedState = teststate1->error_position + u32Position;
      if (u32PositionInConcatedState < 32)
      {
        concatedstate.error_code |= (tU32)0x01 << u32PositionInConcatedState;
        concatedstate.failure_repeat_count[u32PositionInConcatedState] = teststate2->failure_repeat_count[u32Position];
      }
      else {
        concatedstate.__overflow = 1;
        concatedstate.error_code = 0xFFFFFFFF;
        break;
      }
    }
  }

  return concatedstate;
}
