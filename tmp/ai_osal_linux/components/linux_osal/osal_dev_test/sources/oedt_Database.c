/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Database.c
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      data handling of OEDT.
 *
 * NOTES:Ported for ADIT platform Gen2- Haribabu S.
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

#include "oedt_Types.h"
#include "oedt_Database.h"


tU32* oedt_Dataset_pu32Sizemap = NULL;
tU32  oedt_Dataset_u32Entries;
tU32  oedt_Database_ActSelectedSet = 0;
tBool oedt_Database_SwitchLock = FALSE;

extern OEDT_TESTCON_ENTRY oedt_pTestOverallDataset[];
extern OEDT_TESTCON_ENTRY oedt_pTestRegressionDataset[];
extern OEDT_TESTCON_ENTRY oedt_pTestAcousticLongDataset[];
extern OEDT_TESTCON_ENTRY oedt_pVWMIBDataset[];

OEDT_DATASET_ENTRY oedt_pDatabase[OEDT_DATABASE_REGISTERED_SETS] =
{
    {oedt_pTestRegressionDataset,     "Regression Dataset"},
    {oedt_pTestOverallDataset,        "Overall Dataset"},
    {oedt_pTestAcousticLongDataset,   "Acoustic Dataset"},
    {oedt_pVWMIBDataset,              "Gen3 VW MIB ..................."}
};

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_vSelectSet()
* select a dataset from db
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_DB_u32SelectSet(tU32 u32Idx)
{
  if((oedt_Database_SwitchLock == TRUE) || (u32Idx >= oedt_DB_u32GetSize())) return oedt_Database_ActSelectedSet;
  oedt_Database_ActSelectedSet = u32Idx;

  return oedt_Database_ActSelectedSet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_u32GetSelectedSet()
* get id of current selected set
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_DB_u32GetSelectedSet(void)
{
  return oedt_Database_ActSelectedSet;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_u32GetSize()
* get amount of registered sets within db
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_DB_u32GetSize(void)
{
  return OEDT_DATABASE_REGISTERED_SETS;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_pGetEntry()
* get ptr to a dataset
**********************************************************************FunctionHeaderEnd***/
void* oedt_DB_pGetDatasetByID(tU32 u32Idx)
{
  if(u32Idx >= OEDT_DATABASE_REGISTERED_SETS) return NULL;
  return (void*)&(oedt_pDatabase[u32Idx]);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_pGetDataset()
* get ptr to selected dataset
**********************************************************************FunctionHeaderEnd***/
void* oedt_DB_pGetDataset(void)
{
  return (void*)&(oedt_pDatabase[oedt_Database_ActSelectedSet]);
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_vBuildSizeMap()
* builds size map containing array boundaries
**********************************************************************FunctionHeaderEnd***/
void oedt_DB_vBuildSizeMap(void)
{
  tU32 u32ClassCnt, u32TestCnt;
  OEDT_TESTCON_ENTRY* pTestCon;

  oedt_Database_SwitchLock = TRUE;

  pTestCon = ((OEDT_DATASET_ENTRY*)oedt_DB_pGetDataset())->pTestCon;
  
  if(oedt_Dataset_pu32Sizemap == NULL)
  {
    oedt_Dataset_u32Entries = 0;
    while(pTestCon[oedt_Dataset_u32Entries].pFuncs != NULL) ++oedt_Dataset_u32Entries;

    oedt_Dataset_pu32Sizemap = (tU32*)OSAL_pvMemoryAllocate(oedt_Dataset_u32Entries * sizeof(tU32));

    u32ClassCnt = 0;
    while(u32ClassCnt < oedt_Dataset_u32Entries)
    {
      u32TestCnt = 0;
      while(pTestCon[u32ClassCnt].pFuncs[u32TestCnt].pTestFunctions.pTestFunc != NULL) ++u32TestCnt;

      oedt_Dataset_pu32Sizemap[u32ClassCnt] = u32TestCnt;
      ++u32ClassCnt;
    }
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_vReleaseSizeMap()
* releases sizes table mem
**********************************************************************FunctionHeaderEnd***/
void oedt_DB_vReleaseSizeMap(void)
{
  if(oedt_Dataset_pu32Sizemap != NULL)
  {
    OSAL_vMemoryFree((tU8*)oedt_Dataset_pu32Sizemap);
    oedt_Dataset_pu32Sizemap = NULL;
    oedt_Dataset_u32Entries = 0;
  }

  oedt_Database_SwitchLock = FALSE;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_u32GetMaxEntries()
* returns amount of test classes
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_DB_u32GetMaxEntries(void)
{
  return oedt_Dataset_u32Entries;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_u32GetMaxTest()
* returns amount of registered funcs in a test class
**********************************************************************FunctionHeaderEnd***/
tU32 oedt_DB_u32GetMaxTest(tU32 u32Class)
{
  if(u32Class < oedt_Dataset_u32Entries) return oedt_Dataset_pu32Sizemap[u32Class];
  return 0;
}

/*********************************************************************FunctionHeaderBegin***
* oedt_DB_GetDatasetEntry()
* returns pointer to a testclass entry (if there are registered testfunc ptr's in it)
**********************************************************************FunctionHeaderEnd***/
void* oedt_DB_GetDatasetEntry(tU32 u32Idx)
{
  if(oedt_DB_u32GetMaxTest(u32Idx) > 0) return (void*)&(oedt_pDatabase[oedt_Database_ActSelectedSet].pTestCon[u32Idx]);
  return NULL;
}

