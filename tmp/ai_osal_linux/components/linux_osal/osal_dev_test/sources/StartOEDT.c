/************************************************************************
 * FILE:        StartOEDT.c
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Main of OEDT Framework .
 *
 * NOTES: Ported for ADIT platform Gen2 - Haribabu S. 
 *      -
 *
 * COPYRIGHT: Bosch. All Rights reserved!
 * Date     | Modification                                     | Author
 * 5.11.14  | fix for cfg3 920, initialisation of prm task     | dku6kor
 *          |because of OSAL_C_S32_IOCTRL_GET_EXCLUSIVE_ACCESS | 
 * 19.02.15 | fix for cfg3-992                                 | dku6kor
 ***********************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"


#define TRACE_S_IMPORT_INTERFACE_TYPES
#include "trace_if.h"


extern void oedt_vInit(void);
extern void vOEDT_RPC_init (void);
extern void TraceString(const char *buf);

OSAL_tEventHandle  hEvShutdown = 0;
OSAL_tEventMask    hEvRequest  = 0x00000001;

#define SPM_EV_SHUTDOWN_NAME "PROC_OEDT_SHUTDOWN" // That's the name the FC PSM uses to inform the main task to shut down
#define SPM_TRACE_CLASS_SPM_BOOT_BASE     (TR_COMP_SPM+0x0008)

#define I_BOOT_ERROR 0x0880
#define I_BOOT_INFO_STRING  0x0882


#define TRACE_BOOT_ERROR   \
   ET_TRACE_ERROR_BIN                        \
   (                                         \
   SPM_TRACE_CLASS_SPM_BOOT_BASE,            \
   ET_EN_T16 _                               \
   I_BOOT_ERROR _                            \
   ET_EN_T16 _                               \
   __LINE__ _                                \
   ET_EN_STRING _                            \
   __FILE__ _                                \
   ET_EN_DONE                                \
   )


#define TRACE_BOOT_INFO_STRING(STRING_PARAM) \
   ET_TRACE_INFO_BIN                         \
   (                                         \
   SPM_TRACE_CLASS_SPM_BOOT_BASE,            \
   ET_EN_T16 _                               \
   I_BOOT_INFO_STRING _                      \
   ET_EN_STRING _                            \
   STRING_PARAM _                            \
   ET_EN_DONE                                \
   )

char  cProcName[8] = {'O', 'E', 'D', 'T', '_', '_', '_', '_'};

void vStartApp(int argc, char *argv[])
{
      (void)argc; // satisfy l i n t
      (void)argv; // satisfy l i n t

   if ( OSAL_s32MessagePoolCreate(0x001FA000) != OSAL_OK )
   {
          if(OSAL_u32ErrorCode()!=OSAL_E_ALREADYEXISTS)
         {
            TraceString("OSAL IO Error: OSAL_s32MessagePoolCreate failed");
         }
   }


      oedt_vInit();
#ifdef OSAL_GEN2
      vOEDT_RPC_init ();
#endif
      // shutdown event has to be created before creating SPM instance otherwise it is possible that
      // during fast shutdown of spm the event is not yet created. So do it now!
      if (OSAL_s32EventCreate(SPM_EV_SHUTDOWN_NAME, &hEvShutdown) == OSAL_ERROR)
      {
         TRACE_BOOT_INFO_STRING("Creation of SPM shutdown event failed");
         TRACE_BOOT_ERROR;
      }


      {
      
         // Wait for Shutdown-Signal    
         OSAL_s32EventWait(hEvShutdown, hEvRequest, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hEvRequest);


         // Shutdown everything else
         OSAL_s32EventClose(hEvShutdown);
         OSAL_s32EventDelete(SPM_EV_SHUTDOWN_NAME);
         
         // end trace
         et_vTraceClose();
      }
}


void ShutdownMain(void)
{
   OSAL_tEventHandle  hEvent = 0;
   OSAL_s32EventOpen(SPM_EV_SHUTDOWN_NAME, &hEvent);
   OSAL_s32EventPost(hEvent, hEvRequest, OSAL_EN_EVENTMASK_OR);
   OSAL_s32EventClose(hEvent);
}
