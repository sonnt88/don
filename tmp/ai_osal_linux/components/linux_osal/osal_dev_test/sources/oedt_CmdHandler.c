/**********************************************************FileHeaderBegin******
 * FILE:        oedt_CmdHandler.c
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Implementation of OEDT command handler and init func.
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 

#include "Linux_osal.h"

#include "oedt_Configuration.h"
#include "oedt_Types.h"
#include "oedt_Core.h"
#include "oedt_Display.h"

OSAL_tMQueueHandle oedt_hCmdMsgQ;
extern tU32 u32Step;
extern tS32 s32NrOfArg;
extern tS32 s32Arg[10];

#define OEDT_CMD_TEST_START           0x00
#define OEDT_CMD_TEST_STOP            0x01

#define OEDT_CMD_TABLE_LIST           0x02
#define OEDT_CMD_TABLE_LISTDETAILED   0x03
#define OEDT_CMD_TABLE_SETSIZE        0x04
#define OEDT_CMD_TABLE_SETENTRY       0x05
#define OEDT_CMD_TABLE_SAVE           0x06
#define OEDT_CMD_TABLE_LOAD           0x07
  
#define OEDT_CMD_MODE_PAR_ASYNC       0x08
#define OEDT_CMD_MODE_PAR_SYNC        0x09
#define OEDT_CMD_MODE_SEQ             0x0a
#define OEDT_CMD_MODE_ENDURANCE_OFF   0x0b
#define OEDT_CMD_MODE_ENDURANCE_ON    0x0c
#define OEDT_CMD_MODE_LOAD_OFF        0x0d
#define OEDT_CMD_MODE_LOAD_ON         0x0e

#define OEDT_CMD_DATABASE_LIST        0x0f
#define OEDT_CMD_DATASET_LIST         0x10
#define OEDT_CMD_DATASET_SELECT       0x11


#define OEDT_CMD_CFG_LIST             0x12
#define OEDT_CMD_CFG_SAVE             0x13
#define OEDT_CMD_CFG_LOAD             0x14

#define OEDT_CMD_MODE_HONEYPOT_LEVEL  0xf0
#define OEDT_CMD_TEST_ALL             0xfa
#define OEDT_CMD_TEST_SEL             0xfb
#define OEDT_CMD_TEST_ENDURANCE_ALL   0xfc
#define OEDT_CMD_TEST_ENDURANCE_SEL   0xfd

#define OEDT_CMD_HELP                 0xff

OSAL_tIODescriptor         hTraceDev;


/*********************************************************************FunctionHeaderBegin***
* oedt_vCmdHandler()
* OEDT Command Handler
**********************************************************************FunctionHeaderEnd***/
void oedt_vCmdHandler(void)
{
  tU8 pMsg[_OEDT_INCOMING_MSG_LEN];
  tS32 n;
  
#ifdef SHORT_TRACE_OUTPUT
  OSAL_s32MessageQueueCreate(_OEDT_MSGQNAME_CMD, 10, _OEDT_INCOMING_MSG_LEN, OSAL_EN_READWRITE, &oedt_hCmdMsgQ);
  if (OSAL_NULL == oedt_hCmdMsgQ){
     NORMAL_M_ASSERT_ALWAYS();
     return;
  }
#endif
  oedt_eState = OEDT_STATE_READY;

  oedt_vInitCore();
  
  if(s32NrOfArg == 4)
  {
     OSAL_s32ThreadWait(5000);
	 if((s32Arg[0] >=0 ) 
	  &&(s32Arg[1] >=0 ) 
	  &&(s32Arg[2] >=0 )
	  &&(s32Arg[3] >=0 ))
	 {
	    TraceString("Run OEDT test Start Group:%d Last Group:%d First Test Nr:%d Last Test Nr:%d",s32Arg[0] ,s32Arg[1] ,s32Arg[2] , s32Arg[3] );
        oedt_vStartTestCompatible(s32Arg[0],s32Arg[1],s32Arg[2], s32Arg[3], FALSE);
        while(u32Step < 1)
        {  
           OSAL_s32ThreadWait(5000);
        }
     }
	 else
	 {
	    TraceString("Cannot run OEDT due wrong parameter (%d %d %d %d)",s32Arg[0],s32Arg[1],s32Arg[2],s32Arg[3]);
	 }
	 
     OSAL_vProcessExit();
  }
  
  if(s32NrOfArg == 1)
  {
     OSAL_s32ThreadWait(5000);
	 if(OEDT_CMD_DATASET_LIST == s32Arg[0]) 
	 {
	    TraceString("Run OEDT DATASET LIST");
		pMsg[0] = s32Arg[0];
		oedt_vDatasetList();
     }
	 else
	 {
	    TraceString("Cannot run OEDT due wrong parameter (%d)",s32Arg[0]);
	 }
	 
     OSAL_vProcessExit();
	 
  }
  
#ifndef SHORT_TRACE_OUTPUT
   TraceString("Start OSAL Core Test");
   oedt_vStartTestCompatible(0, 0, 0, 255, FALSE);
   while(u32Step < 1)
    {  
       OSAL_s32ThreadWait(5000);
    }
TraceString("Start OSAL Registry Test");
   oedt_vStartTestCompatible( 9, 9, 0, 255, FALSE);
   while(u32Step < 2)
    {  
       OSAL_s32ThreadWait(5000);
    }
TraceString("Start OSAL Ramdisk Test");
   oedt_vStartTestCompatible(14,14, 0, 255, FALSE);
   while(u32Step < 3)
    {  
       OSAL_s32ThreadWait(5000);
    }
TraceString("Start OSAL FFS2 Test");
   oedt_vStartTestCompatible( 1, 1, 0, 255, FALSE);
   while(u32Step < 4)
    {  
       OSAL_s32ThreadWait(5000);
    }
TraceString("Start OSAL ErrMem Test");
   oedt_vStartTestCompatible( 18, 18, 0, 255, FALSE);
   while(u32Step < 5)
    {  
       OSAL_s32ThreadWait(5000);
    }
TraceString("Start OSAL PRM Test");
   oedt_vStartTestCompatible( 20, 20, 0, 255, FALSE);
#endif
 
#ifdef SHORT_TRACE_OUTPUT
  while(TRUE)
  {
    n = OSAL_s32MessageQueueWait(oedt_hCmdMsgQ, (tU8*)pMsg, _OEDT_INCOMING_MSG_LEN, OSAL_NULL, OSAL_C_TIMEOUT_FOREVER);
    if (n<=0)
    {
       NORMAL_M_ASSERT_ALWAYS();
       (void) OSAL_s32ThreadWait(1000);
       continue;
    }
    if(oedt_eState == OEDT_STATE_READY)
    {
      switch(pMsg[0])
      {
      case OEDT_CMD_TEST_START:
        oedt_vStartTest();    
        break;
  
      case OEDT_CMD_TEST_STOP:
        oedt_bStopFlg = TRUE;
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_DATASET_LIST:
	    TraceString("CMD DATASET LIST");
        oedt_vDatasetList();
        break;
  
      case OEDT_CMD_DATABASE_LIST:
        oedt_vDatabaseList();
        break;
  
      case OEDT_CMD_TABLE_LIST:
        oedt_vTableList(FALSE);
        break;
  
      case OEDT_CMD_TABLE_LISTDETAILED:
        oedt_vTableList(TRUE);
        break;
  
      case OEDT_CMD_TABLE_SETSIZE:
        oedt_vSendConfirmMsg(oedt_fTableSetSize(pMsg[1]));
        break;
  
      case OEDT_CMD_TABLE_SETENTRY:
        oedt_vTableSetEntry(pMsg[1], pMsg[2], pMsg[3], pMsg[4], pMsg[5]);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_TABLE_SAVE:
        oedt_vSendConfirmMsg(oedt_fTableSave());
        break;
  
      case OEDT_CMD_TABLE_LOAD:
        oedt_vSendConfirmMsg(oedt_fTableLoad());
        break;
  
      case OEDT_CMD_MODE_PAR_SYNC:
        oedt_vModeSet_Flow(OEDT_MODE_FLOW_PARALLEL_SYNC);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_PAR_ASYNC:
        oedt_vModeSet_Flow(OEDT_MODE_FLOW_PARALLEL_ASYNC);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_SEQ:
        oedt_vModeSet_Flow(OEDT_MODE_FLOW_SEQUENTIAL);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_LOAD_ON:
        oedt_vModeSet_Special(OEDT_MODE_SPECIAL_LOAD, TRUE);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_LOAD_OFF:
        oedt_vModeSet_Special(OEDT_MODE_SPECIAL_LOAD, FALSE);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_ENDURANCE_ON:
        oedt_vModeSet_Special(OEDT_MODE_SPECIAL_ENDURANCE, TRUE);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_MODE_ENDURANCE_OFF:
        oedt_vModeSet_Special(OEDT_MODE_SPECIAL_ENDURANCE, FALSE);
        oedt_vSendConfirmMsg(TRUE);
        break;
  
      case OEDT_CMD_DATASET_SELECT:
        oedt_vDatasetSelect((tU32)pMsg[1]);
        oedt_vSendConfirmMsg(TRUE);
        break;
    
      case OEDT_CMD_CFG_SAVE:
        oedt_vSendConfirmMsg(oedt_fConfigSave());
        break;
  
      case OEDT_CMD_CFG_LOAD:
        oedt_vSendConfirmMsg(oedt_fConfigLoad());
        break;
  
      case OEDT_CMD_CFG_LIST:
        oedt_vConfigList();
        break;

      case OEDT_CMD_MODE_HONEYPOT_LEVEL:
		oedt_vHoneypotSetLevel(pMsg[1]);
		break;

		case OEDT_CMD_TEST_ALL:
      case OEDT_CMD_TEST_SEL:
        oedt_vStartTestCompatible(pMsg[1], pMsg[2], pMsg[3], pMsg[4], FALSE);
        break;

      case OEDT_CMD_TEST_ENDURANCE_ALL:
      case OEDT_CMD_TEST_ENDURANCE_SEL:
        oedt_vStartTestCompatible(pMsg[1], pMsg[2], pMsg[3], pMsg[4], TRUE);
        break;

      case OEDT_CMD_HELP:
        oedt_vSendHelpMsg();
        break;
  
      default:
        break;
      }
    }
    else if(pMsg[0] == OEDT_CMD_TEST_STOP)
    {
      oedt_eState = OEDT_STATE_READY;
      oedt_bStopFlg = TRUE;
      oedt_vSendConfirmMsg(TRUE);
    }
  }
#endif
}

void oedt_vCmdHandler_Callback(tU8 *pu8Data)
{
  if((pu8Data[1] == 0x1a) && (pu8Data[2] == 0))
  {
    OSAL_s32MessageQueuePost(oedt_hCmdMsgQ, (tPCU8)pu8Data + 3, _OEDT_INCOMING_MSG_LEN, OSAL_NULL);
    return;
  }
}

/*********************************************************************FunctionHeaderBegin***
* oedt_vInit()
* inits oedt
**********************************************************************FunctionHeaderEnd***/
void oedt_vInit(void)
{
  OSAL_tThreadID         threadID = 0;
  OSAL_trThreadAttribute threadAttr;
  OSAL_trIOCtrlLaunchChannel rInfo;

  if(oedt_eState != OEDT_STATE_NOTREADY) return;

  hTraceDev = OSAL_IOOpen(_OEDT_TRACE_DEVICE, OSAL_EN_WRITEONLY);
  if (hTraceDev==OSAL_ERROR) {
	  FATAL_M_ASSERT_ALWAYS();		// No trace, No OEDT !!!
  }

  threadAttr.u32Priority  = _OEDT_MAX_PRIO;
  threadAttr.s32StackSize = _OEDT_PROCESS_STACK_SIZE;
  threadAttr.szName       = _OEDT_PROCESSNAME_CMDHANDLER;
  threadAttr.pfEntry      = (OSAL_tpfThreadEntry)oedt_vCmdHandler;
  threadAttr.pvArg        = (tVoid*)NULL;

  threadID = OSAL_ThreadSpawn(&threadAttr);

  if(threadID != 0)
  { 
    while(oedt_eState == OEDT_STATE_NOTREADY) OSAL_s32ThreadWait(5);
     
    rInfo.enTraceChannel = _OEDT_TTFIS_CHANNEL;
    rInfo.pCallback      = (OSAL_tpfCallback)oedt_vCmdHandler_Callback;
    OSAL_s32IOControl(hTraceDev, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32)&rInfo);
  }
}

