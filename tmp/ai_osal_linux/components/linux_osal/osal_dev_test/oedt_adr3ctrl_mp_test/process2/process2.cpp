/******************************************************************************     
 *FILE         : process2.cpp
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : 
 *               
 *AUTHOR       : Madhu Sudhan Swargam (RBEI/ECF5)
 *
 *COPYRIGHT    : (c) 2012, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : 04.11.13 .Initial version
 *             : 24.12.13 .Response wait Time for events or messages is made for four seconds
                           swm2kor
               : 13.01.14 .Oedt Addition
                           SWM2KOR
               : 03.03.14 .Using ADR3CTRL state definition from dev_adr3ctrl.h file
                           SWM2KOR
               : 11.03.14 .Trace Update and Lint Removal
                           SWM2KOR
 *
 *****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "OsalConf.h"
#include "osal_if.h"
#include "Linux_osal.h"
#include "oedt_helper_funcs.h"
#include "ostrace.h"
#include "oedt_ADR3Ctrl_TestFuncs.h"
#include "dev_adr3ctrl.h"

#define ADR3RESETCHECK_EVENTNAME                "P2_Adr3ResetCheck_EV"

/* Used for Event post from CallBack*/
tBool bEventPostMechansmActive = FALSE; 
/* Handler for CallBack event mechanism Handling */
OSAL_tEventHandle CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
/***************************************************************************************
* FUNCTION    :  TraceOut
*
* PARAMETER   :  
*                 char* cBuffer
*
* RETURNVALUE :    tVoid
*
* DESCRIPTION : Does the trace of the OEDT
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
void TraceOut(const char* cBuffer)
{
  char   u8TraceBuff[TRACE_BUF_SIZE] ={0};
  tU16   u16Len;
  memset(u8TraceBuff,' ',TRACE_BUF_SIZE);
  u8TraceBuff[0] = 0xf1;
  u16Len = (tU16)strlen(cBuffer);
  if(u16Len > TRACE_BUF_SIZE) 
  {
      u16Len = TRACE_BUF_SIZE - 2;
  }
  u8TraceBuff[u16Len+1] = '\0';
  memcpy(&u8TraceBuff[1],cBuffer,u16Len);
  LLD_vTrace((tU32)TR_COMP_OSALCORE,(tU32)TR_LEVEL_FATAL,u8TraceBuff,u16Len+1);
}
/***************************************************************************************
* FUNCTION    :  vAdr3ctrlCallback
*
* PARAMETER   :  
*                  tU32 u32State
*
* RETURNVALUE :    tVoid
*
* DESCRIPTION :  CallBack execution for ADR3 state change
*                Post the event for the recived State from Dev_ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
static tVoid vAdr3ctrlCallback(tU32 u32State)
{
   tU32  tU32EventCommand = ADR3CALLBACK_INVALIDSTATE;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   if(u32State == (tU32)ADR3CTRL_ADR3_STATE_ALIVE)
   {
      /* event post ADR3 ALIVE state */
      tU32EventCommand = ADR3CALLBACK_ALIVESTATE;
      TraceOut("Proc2: ADR3CTRL_ADR3_STATE_ALIVE");  
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DEAD)
   {
      /* event post ADR3 DEAD state */
      tU32EventCommand = ADR3CALLBACK_DEADSTATE;
      TraceOut("Proc2:ADR3CTRL_ADR3_STATE_DEAD");
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DNL)
   {
      /* event post ADR3 DNL state */
      tU32EventCommand = ADR3CALLBACK_DNLSTATE ;
      TraceOut("Proc2:ADR3CTRL_ADR3_STATE_DNL");
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_INIT)
   {
      /* event post ADR3 INIT state */
      tU32EventCommand = ADR3CALLBACK_INITSTATE ;
      TraceOut("Proc2:ADR3CTRL_ADR3_STATE_INIT");
   }
   else
   {
     snprintf(u8TraceBuff,
              sizeof(u8TraceBuff),
              "P2:Unknow ADR3 State =%d",
              u32State 
             );
     TraceOut(&u8TraceBuff[0]);
   }
   if(bEventPostMechansmActive == TRUE)
   {
      if(OSAL_C_INVALID_HANDLE == CallBackEventHndlr)  
      {
         TraceOut("P2:Invalid event Handler");
      }
      else if(OSAL_OK != OSAL_s32EventPost(CallBackEventHndlr,
                                           tU32EventCommand,
                                           OSAL_EN_EVENTMASK_OR 
                                          ) 
              )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:Event post Failed at line %d with error =%d",
                  __LINE__,
                  OSAL_u32ErrorCode() 
                 );
         TraceOut(&u8TraceBuff[0]);
      }
   }
}
/***********************************************************************************
* FUNCTION    :  u32ADR3Ctrlopen_RgstrtCallBack
*
* PARAMETER   :  
*                  OSAL_tIODescriptor hDevHandle
* RETURNVALUE :    tU32,"OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :   Open ADR3CTRL and Register for CallBack
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*************************************************************************************/
tU32 u32ADR3Ctrlopen_RgstrtCallBack(OSAL_tIODescriptor *hpDevHandle)
{
   tU32  u32RetVal = 0;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   /** ADR3CTRL Driver Open*/
   *hpDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if((*hpDevHandle) != OSAL_ERROR)
   {
      if(OSAL_OK 
         != 
         OSAL_s32IOControl(*hpDevHandle,
                           OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK,
                           (tS32)vAdr3ctrlCallback 
                          )
        )
      {
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:ADR3CTRL IOCOntrol failed with error %d",
                  OSAL_u32ErrorCode()
                  );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 67108864;
      }
   }
   else
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:ADR3CTRL open failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 33554432;
   }
   return u32RetVal;
}
/***********************************************************************************
* FUNCTION    :  u32EventClose_Delete
*
* PARAMETER   :  
*                  tCString EventName
*                  OSAL_tEventHandle EventHndlr
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32,"OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  Close the event and delete the event
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*************************************************************************************/
static tS32 u32EventClose_Delete(tCString EventName,
                                 OSAL_tEventHandle *EventHndlr,
                                 tU32 u32LineNumbr
                                )
{
   tS32  s32Retval = OSAL_OK;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   if((EventName != NULL) && (*EventHndlr != OSAL_C_INVALID_HANDLE))
   {
      if(OSAL_OK != OSAL_s32EventClose(*EventHndlr))
      {
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:Event close failed err %lu at line =%d",
                  OSAL_u32ErrorCode(),
                  u32LineNumbr
                 );
         TraceOut(&u8TraceBuff[0]);
         s32Retval = OSAL_ERROR;
      }
      else if(OSAL_OK != OSAL_s32EventDelete(EventName))
      {
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:Event Delete of Event:%s failed with err %lu at line =%d",
                  EventName,
                  OSAL_u32ErrorCode(),
                  u32LineNumbr
                 );
         TraceOut(&u8TraceBuff[0]);
         s32Retval = OSAL_ERROR;
      }
      else
      {
         /** Event closed and Deleted  sucessfully*/
         *EventHndlr = OSAL_C_INVALID_HANDLE;
      }
   }
   else
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:Invalid Event handler deleted at Line =%d",
               u32LineNumbr
              );
      TraceOut(&u8TraceBuff[0]);
      s32Retval = OSAL_ERROR;
   }
   return (s32Retval);
}
/***************************************************************************************
* FUNCTION    :  u32ADR3StateWait
*
* PARAMETER   :  
*                  OSAL_tEventHandle EventHndlr
*                  tU32 dwEventMask
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32,valid event  on success  or Invalid event value in case of error
*
* DESCRIPTION :  Wait for the EventHndlr and send the event received 
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************************/
static tU32 u32ADR3StateWait(OSAL_tEventHandle EventHndlr,
                             tU32 dwEventMask,
                             OSAL_tenEventMaskFlag fgMaskoption,
                             tU32 u32LineNumbr
                            )
{
   tU32  hEvRequest ;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   if(((OSAL_tEventHandle)OSAL_C_INVALID_HANDLE != EventHndlr) 
      && 
      (0 != dwEventMask)
     )
   {
      if(OSAL_OK == OSAL_s32EventWait(EventHndlr,
                                       dwEventMask,
                                       fgMaskoption,
                                       ADR3CTRL_RESPONSE_WAIT_TIME,
                                       &hEvRequest
                                      )
         )
      {
         if(OSAL_OK != OSAL_s32EventPost(EventHndlr,
                                         ~hEvRequest,
                                         OSAL_EN_EVENTMASK_AND 
                                        ) 
            )
         {
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:Event post Failed line %lu with error =%u",
                     u32LineNumbr,
                     OSAL_u32ErrorCode()
                    );
            TraceOut(&u8TraceBuff[0]);
            NORMAL_M_ASSERT_ALWAYS();
            hEvRequest = 16777216;
         } 
         
      }
      else
      {
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:Event wait Failed line %lu with error =%u",
                  u32LineNumbr,
                  OSAL_u32ErrorCode()
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         hEvRequest = 8388608;
      }
   }
   else
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:Invalid Event wait values at Line =%d",
               u32LineNumbr
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      hEvRequest = 4194304;
   }
   
   return (hEvRequest);
}
/*****************************************************************************
* FUNCTION    :  u32ResetConcryCheck

* RETURNVALUE :    tU32,"0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Checks the reset callback for ADR3CTRL in normal mode n
                  and informs to OEDT
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  jan 6,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
static tU32 u32ResetConcryCheck(tVoid)
{
   tU32  u32RetVal = 0,u32MsgPostValue,hEvRequest,dwEventMask;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   OSAL_tIODescriptor   hDevHandle;
   OSAL_tEventHandle    hADR3ResetCheckEvntHndlr; 
   OSAL_tMQueueHandle   mqHandleR = OSAL_C_INVALID_HANDLE;
   /* Open the resource to interact with procadr3ctrl1*/
   if (OSAL_s32MessageQueueOpen(MQ_ADR3CTRL_MP2_RESPONSE_NAME,
            OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR
      )
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:MessageQueueOpen failed with error %d at line = %d",
               OSAL_u32ErrorCode(),
               __LINE__
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 1;
   }
      /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3RESETCHECK_EVENTNAME ,
                                     &hADR3ResetCheckEvntHndlr 
                                    )
     )
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:EventCreate failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 2;
   }
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      u32MsgPostValue = MQ_ADR3CTRl_MP2_CB_REGSTER; 
      if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                (tPCU8)&u32MsgPostValue,
                                                 sizeof(u32MsgPostValue),
                                                0
                                               )
        )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode(),
                  __LINE__
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 4;
      }  
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3ResetCheckEvntHndlr;
      /* Reset ADR3 */
      
      dwEventMask = ADR3CALLBACK_ALIVESTATE | ADR3CALLBACK_DEADSTATE;
      /* Wait for Dead state  and Alive state callback from ADR3*/
      hEvRequest = u32ADR3StateWait(hADR3ResetCheckEvntHndlr,
                                    dwEventMask,
                                    OSAL_EN_EVENTMASK_AND,
                                    __LINE__
                                   );
      if(hEvRequest >= ADR3CTRL_STATEWAIT_MIN_ERROR)
      {
         /* Adding the wait error value with return Value*/
         u32RetVal += hEvRequest;
      }
      else if((ADR3CALLBACK_ALIVESTATE & hEvRequest) 
              && 
              (ADR3CALLBACK_DEADSTATE & hEvRequest)
              )
      {
         u32MsgPostValue = MQ_ADR3CTRl_MP2_RESET_OBSERVED; 
         if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                   (tPCU8)&u32MsgPostValue,
                                                    sizeof(u32MsgPostValue),
                                                   0
                                                  )
           )
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:QueuePost failed with error %d",
                     OSAL_u32ErrorCode(),
                     __LINE__
                    );
            TraceOut(&u8TraceBuff[0]);
            NORMAL_M_ASSERT_ALWAYS();
            u32RetVal += 8;
         }  
      }
      else
      {  
         u32RetVal += 16;
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
       /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(ADR3RESETCHECK_EVENTNAME,
                                         &hADR3ResetCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         u32RetVal += 32;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         u32RetVal += 64;
      }     
   }
   u32MsgPostValue = MQ_ADR3CTRL_MP2_PROCESS_EXIT; 
   if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                             (tPCU8)&u32MsgPostValue,
                                             sizeof(u32MsgPostValue),
                                             0
                                            )
      )
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode(),
                  __LINE__
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 256;
   }
   if(OSAL_OK !=OSAL_s32MessageQueueClose(mqHandleR))
   {
     memset(u8TraceBuff,0,sizeof(u8TraceBuff));
     snprintf(u8TraceBuff,
              sizeof(u8TraceBuff),
              "P2:QueueClose failed with error %d at line =%d",
              OSAL_u32ErrorCode(),
              __LINE__
             );
     TraceOut(&u8TraceBuff[0]);
     NORMAL_M_ASSERT_ALWAYS();
     u32RetVal += 1024;
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32OpenConcryChk

* RETURNVALUE :  tU32,"0" on success  or "non-zero" value in case of error

* DESCRIPTION :  checks the open concurrency for ADR3CTRL
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  jan 6,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
static tU32 u32OpenConcryChk(tVoid)
{
   tU32  u32RetVal = 0,u32OpenCount = 0,u32count,u32MsgPostValue;
   char  u8TraceBuff[TRACE_BUF_SIZE] = {0};
   OSAL_tMQueueHandle   mqHandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tIODescriptor   hDevHandle[11];
   /* Open message to communicate with OEDT test case */
   if(OSAL_ERROR == OSAL_s32MessageQueueOpen(MQ_ADR3CTRL_MP2_OPEN_DEVICE_NAME,
                                            OSAL_EN_READWRITE,
                                            &mqHandleR
                                           )
      )
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:MessageQueueOpen failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 1;
   }
   if(0 == u32RetVal)
   {
      /* ADR3CTRL open happens on two process, So open only half 
         of the available adr3ctrl device handlers               */
      for(u32count = 0;u32count<(ADR3CTRL_MAXDEVICES/2);u32count++)
      {
         /*Open Successful*/
         if(OSAL_ERROR
             == 
            (hDevHandle[u32count] = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,
                                                OSAL_EN_READWRITE
                                                )
            ) 
           )
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:Device Open failed with error %d",
                     OSAL_u32ErrorCode()
                    );
            TraceOut(&u8TraceBuff[0]);
            u32RetVal += 2;
            break;
         }
         else
         {
            u32OpenCount++;
         }
         /** Thread is waiting so other process will execute **/
         OSAL_s32ThreadWait(50);
      }
      /* Close the open devices */
      for(u32count = 0;u32count<u32OpenCount;u32count++)
      {
         /*Open Successful*/
         if(OSAL_s32IOClose(hDevHandle[u32count]) == OSAL_ERROR)
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:Device Close failed with error %d",
                     OSAL_u32ErrorCode()
                    );
            TraceOut(&u8TraceBuff[0]);
            u32RetVal += 4;
         }
      }
      /* Inform the OEDT test case about the  Open Operation completion */
      u32MsgPostValue = (tU32)MQ_ADR3CTRl_MP2_OPEN2; 
      if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                (tPCU8)&u32MsgPostValue,
                                                sizeof(u32MsgPostValue),
                                                0
                                               )
        )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode()
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 8;
      }  
      /* Closing the opened resource*/
      if(OSAL_OK !=OSAL_s32MessageQueueClose(mqHandleR))
      {
        memset(u8TraceBuff,0,sizeof(u8TraceBuff));
        snprintf(u8TraceBuff,
                 sizeof(u8TraceBuff),
                 "P2:QueueClose failed with error %d",
                 OSAL_u32ErrorCode()
                );
        TraceOut(&u8TraceBuff[0]);
        NORMAL_M_ASSERT_ALWAYS();
        u32RetVal += 16;
      }
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32CloseConcryChk

* RETURNVALUE :    tU32,"0" on success  or "non-zero" value in case of error

* DESCRIPTION :  checks the close concurrency for ADR3CTRL
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  jan 6,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
static tU32 u32CloseConcryChk(tVoid)
{
   tU32  u32RetVal = 0,u32OpenCount=0,u32count,u32MsgPostValue;
   char  u8TraceBuff[TRACE_BUF_SIZE]={0};
   OSAL_tMQueueHandle   mqHandleR = OSAL_C_INVALID_HANDLE;
   OSAL_tIODescriptor   hDevHandle[11];
      /* Open message to communicate with OEDT test case */
   if(OSAL_ERROR ==OSAL_s32MessageQueueOpen(MQ_ADR3CTRL_MP2_CLOSE_DEVICE_NAME,
                                            OSAL_EN_READWRITE,
                                            &mqHandleR
                                           )
      )
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:MessageQueueOpen failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 1;
   }
   if(0 == u32RetVal)
   {
     /* ADR3CTRL open happens on two process, So open only half 
         of the available adr3ctrl device handlers               */
      for(u32count=0;u32count<(ADR3CTRL_MAXDEVICES/2);u32count++)
      {/*Open Successful*/
         if(OSAL_ERROR 
            ==
            (hDevHandle[u32count] = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,
                                                OSAL_EN_READWRITE
                                               )
            )
           )
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:Device Open failed with error %d",
                     OSAL_u32ErrorCode()
                    );
            TraceOut(&u8TraceBuff[0]);
            u32RetVal += 2;
            break;
         }
         else
         {
            u32OpenCount++;
         }
      }
      for(u32count=0;u32count<u32OpenCount;u32count++)
      {/*Open Successful*/
         if(OSAL_s32IOClose(hDevHandle[u32count]) == OSAL_ERROR)
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:Device Close failed with error %d",
                     OSAL_u32ErrorCode()
                    );
            TraceOut(&u8TraceBuff[0]);
            u32RetVal += 4;
         }
         /** Thread is waiting so other process will execute **/
         OSAL_s32ThreadWait(50);
      }
     /* Inform the OEDT test case about the  Open Operation completion */
      u32MsgPostValue = (tU32)MQ_ADR3CTRl_MP2_CLSE2; 
      if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                (tPCU8)&u32MsgPostValue,
                                                sizeof(u32MsgPostValue),
                                                0
                                               )
        )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode()
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 8;
      }  
       if(OSAL_OK !=OSAL_s32MessageQueueClose(mqHandleR))
      {
        memset(u8TraceBuff,0,sizeof(u8TraceBuff));
        snprintf(u8TraceBuff,
                 sizeof(u8TraceBuff),
                 "P2:QueueClose failed with error %d",
                 OSAL_u32ErrorCode()
                );
        TraceOut(&u8TraceBuff[0]);
        NORMAL_M_ASSERT_ALWAYS();
        u32RetVal += 16;
      }
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32RegistrConcryChk

* RETURNVALUE :    tU32,"0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Continuously register for ADR3CTRL callback information 
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  jan 6,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
static tU32 u32RegistrConcryChk(tVoid)
{
   OSAL_tIODescriptor hDevHandle;
   OSAL_tMQueueHandle mqHandleR = OSAL_C_INVALID_HANDLE;
   tU32 u32RetVal = 0,u32count;
   tU32 u32MsgPostValue;
   char u8TraceBuff[TRACE_BUF_SIZE]={0};
   if(OSAL_ERROR ==OSAL_s32MessageQueueOpen(MQ_ADR3CTRL_MP2_REG_CB_NAME,
                                            OSAL_EN_READWRITE,
                                            &mqHandleR
                                           )
      )
   {
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:MessageQueueOpen failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 1;
   }
   hDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if(hDevHandle != OSAL_ERROR)
   {
      for(u32count = 0; u32count < 6; u32count++)
      {
         if(OSAL_ERROR 
            == 
           OSAL_s32IOControl(hDevHandle,
                             OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK,
                             (tS32)vAdr3ctrlCallback 
                            )
           )
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:ADR3CTRL IOCOntrol failed with error %d",
                     OSAL_u32ErrorCode()
                     );
            TraceOut(&u8TraceBuff[0]); 
            u32RetVal += 2; 
         }
        /** Thread is waiting so other process will execute **/
         OSAL_s32ThreadWait(50);
     }
     if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:ADR3CTRL Close failed with error %d",
               OSAL_u32ErrorCode()
              );
         TraceOut(&u8TraceBuff[0]);         
         u32RetVal += 4;
      }  
   }
   else
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:ADR3CTRL open failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      u32RetVal += 8;
   }
   if(OSAL_C_INVALID_HANDLE != mqHandleR)
   {
      u32MsgPostValue = MQ_ADR3CTRl_MP2_REG2; 
      if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                (tPCU8)&u32MsgPostValue,
                                                sizeof(u32MsgPostValue),
                                                0
                                               )
        )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode()
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 16;
      }  
       if(OSAL_OK !=OSAL_s32MessageQueueClose(mqHandleR))
      {
        memset(u8TraceBuff,0,sizeof(u8TraceBuff));
        snprintf(u8TraceBuff,
                 sizeof(u8TraceBuff),
                 "P2:QueueClose failed with error %d",
                 OSAL_u32ErrorCode()
                );
        TraceOut(&u8TraceBuff[0]);
        NORMAL_M_ASSERT_ALWAYS();
        u32RetVal += 32;
      }
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32SPIMOD_RSTConcryChk

* RETURNVALUE :    tU32,"0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Checks the reset callback for ADR3CTRL in SPI mode n
                  and informs to OEDT
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  jan 6,2013
*             :   Trace Update and Lint removal  :   SWM2KOR   : Mar 11,2014
*
*******************************************************************************/
static tU32 u32SPIMOD_RSTConcryChk(tVoid)
{
   tU32  u32RetVal = 0,u32MsgPostValue,hEvRequest,dwEventMask;
   char  u8TraceBuff[TRACE_BUF_SIZE]={0};
   OSAL_tIODescriptor   hDevHandle;
   OSAL_tEventHandle    hADR3ResetCheckEvntHndlr; 
   OSAL_tMQueueHandle   mqHandleR = OSAL_C_INVALID_HANDLE;
   if(OSAL_ERROR == OSAL_s32MessageQueueOpen(MQ_ADR3CTRL_MP2_RESPONSE_NAME,
                                              OSAL_EN_READWRITE,
                                              &mqHandleR
                                             )
      )
   {
     snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:MessageQueueOpen failed with error %d at line = %d",
               OSAL_u32ErrorCode(),
               __LINE__
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 1;
   }
      /** Create Event for handle the ADR3 callback Changes*/
   if(OSAL_OK != OSAL_s32EventCreate(ADR3RESETCHECK_EVENTNAME,
                                     &hADR3ResetCheckEvntHndlr 
                                    )
     )
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:EventCreate failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 2;
   }
   if((0 == u32RetVal) 
      && 
      (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle)))
     )
   {
      u32MsgPostValue = MQ_ADR3CTRl_MP2_CB_REGSTER; 
      if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                (tPCU8)&u32MsgPostValue,
                                                 sizeof(u32MsgPostValue),
                                                0
                                               )
        )
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
                  sizeof(u8TraceBuff),
                  "P2:QueuePost failed with error %d",
                  OSAL_u32ErrorCode(),
                  __LINE__
                 );
         TraceOut(&u8TraceBuff[0]);
         NORMAL_M_ASSERT_ALWAYS();
         u32RetVal += 4;
      }  
      /* Allow the permission for call Back to send the event */
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3ResetCheckEvntHndlr;
      /* Reset ADR3 */
      
      dwEventMask = ADR3CALLBACK_DNLSTATE | ADR3CALLBACK_DEADSTATE;
      /* Wait for Dead state  and DNL state callback from ADR3*/
      hEvRequest = u32ADR3StateWait(hADR3ResetCheckEvntHndlr,
                                    dwEventMask,
                                    OSAL_EN_EVENTMASK_AND,
                                    __LINE__
                                   );
      if(hEvRequest >= ADR3CTRL_STATEWAIT_MIN_ERROR)
      {
         /* Adding the wait error value with return Value*/
         u32RetVal += hEvRequest;
      }
      else if((ADR3CALLBACK_DNLSTATE & hEvRequest) 
              && 
              (ADR3CALLBACK_DEADSTATE & hEvRequest)
              )
      {
         u32MsgPostValue = MQ_ADR3CTRl_MP2_RESET_OBSERVED; 
         if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                                   (tPCU8)&u32MsgPostValue,
                                                   sizeof(u32MsgPostValue),
                                                   0
                                                  )
           )
         {
            memset(u8TraceBuff,0,sizeof(u8TraceBuff));
            snprintf(u8TraceBuff,
                     sizeof(u8TraceBuff),
                     "P2:QueuePost failed with error %d",
                     OSAL_u32ErrorCode(),
                     __LINE__
                    );
            TraceOut(&u8TraceBuff[0]);
            NORMAL_M_ASSERT_ALWAYS();
            u32RetVal += 8;
         }  
      }
      else
      {  
         u32RetVal += 16;
      }
      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
      /* Close and delete the Event*/
      if(OSAL_OK != u32EventClose_Delete(ADR3RESETCHECK_EVENTNAME,
                                         &hADR3ResetCheckEvntHndlr,
                                         __LINE__
                                        )
        )
      {
         u32RetVal += 32;
      }
      /* ADR3 close*/
      if(OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         memset(u8TraceBuff,0,sizeof(u8TraceBuff));
         snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:ADR3CTRL Close failed with error %d",
               OSAL_u32ErrorCode()
              );
         TraceOut(&u8TraceBuff[0]);     
         u32RetVal += 64;
      }     
   }
   u32MsgPostValue = MQ_ADR3CTRL_MP2_PROCESS_EXIT; 
   if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleR,
                                             (tPCU8)&u32MsgPostValue,
                                             sizeof(u32MsgPostValue),
                                             0
                                            )
      )
   {
      memset(u8TraceBuff,0,sizeof(u8TraceBuff));
      snprintf(u8TraceBuff,
               sizeof(u8TraceBuff),
               "P2:QueuePost failed with error %d",
               OSAL_u32ErrorCode()
              );
      TraceOut(&u8TraceBuff[0]);
      NORMAL_M_ASSERT_ALWAYS();
      u32RetVal += 128;
   }
   if(OSAL_OK !=OSAL_s32MessageQueueClose(mqHandleR))
   {
     memset(u8TraceBuff,0,sizeof(u8TraceBuff));
     snprintf(u8TraceBuff,
              sizeof(u8TraceBuff),
              "P2:QueueClose failed with error %d at line =%d",
              OSAL_u32ErrorCode(),
              __LINE__
             );
     TraceOut(&u8TraceBuff[0]);
     NORMAL_M_ASSERT_ALWAYS();
     u32RetVal += 256;
   }
   return u32RetVal;
}
/*****************************************************************************
* FUNCTION    :  u32ADR3CtrlMultiProcessTest

* PARAMETER   :  tVoid

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* DESCRIPTION :  Open and Close the ADR3
*               
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Sep 19,2013
*
*******************************************************************************/
int main(int argc, char **argv)
{
   tU32 u32RetVal = 0;
   if (argc == 2)
   {
      switch(atoi(argv[1] ))
      {
         case MQ_ADR3CTRL_MP_OPEN_CNCRNCY:
            u32RetVal = u32OpenConcryChk();
            break;
         case MQ_ADR3CTRL_MP_CLOSE_CNCRNCY:
            u32RetVal = u32CloseConcryChk();
            break;
         case MQ_ADR3CTRL_MP_CBREG_CNCRNCY:
            u32RetVal = u32RegistrConcryChk();
            break;
         case MQ_ADR3CTRL_MP_NOR_RST_CNCRNCY:            
            u32RetVal = u32ResetConcryCheck();
            break;
         case MQ_ADR3CTRL_MP_SPI_RST_CNCRNCY:
            u32RetVal = u32SPIMOD_RSTConcryChk();
            break; 
         default:
            break;            
      }
   } 
   OSAL_vProcessExit();
   _exit((tS32)u32RetVal);    
}
