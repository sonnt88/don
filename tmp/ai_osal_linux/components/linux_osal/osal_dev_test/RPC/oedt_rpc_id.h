/******************************************************************************
| FILE			:	oedt_rpc_id.h
| PROJECT		:      ADIT GEN 2
| SW-COMPONENT	: 	OEDT RPC 
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains rpc definitions same files should be used in both OS
|------------------------------------------------------------------------------
| COPYRIGHT		:	(c) 2011 Bosch
| HISTORY		:      
|------------------------------------------------------------------------------
| Date 		| 	Modification			|	Author
|------------------------------------------------------------------------------
| 14.02.2011  	| Initial revision			| 	SUR2HI
| --.--.--  	| ----------------           | ------------
| 11.05.2011  	| Added STM Helper		| 	SUR2HI
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/
#if !defined (OEDT_RPC_ID_H)
#define OEDT_RPC_ID_H
#define OEDT_RPC_INVAL_ID OEDT_RPC_LAST_CHAN
typedef enum
{
    OEDT_RPC_OEDT_START_LINUX			=  1,
    OEDT_RPC_OEDT_START_TENGINE			=  2,
    OEDT_RPC_OEDT_SEND_LINUX			=  3,
    OEDT_RPC_OEDT_SEND_TENGINE			=  4,
    OEDT_RPC_TEST_CONNECTION_TENGINE	=  5,
    OEDT_RPC_TEST_CONNECTION_LINUX		=  6,
    RPC_ID_STM_REQUEST_FILTER			=  7,
    RPC_ID_STM_CREATE_ROUTE				=  8,
    RPC_ID_STM_FREE_FILTER				=  9,
    RPC_ID_STM_REQUEST_DB_ROUTE			= 10,
    RPC_ID_STM_FREE_ROUTE				= 11,
    RPC_ID_STM_SEND_COMMAND				= 12,
    
    OEDT_RPC_LAST_CHAN /*Unused Ch*/
}OedtRpcID;
#endif

