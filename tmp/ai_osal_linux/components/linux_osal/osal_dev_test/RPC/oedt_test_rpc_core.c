/******************************************************************************
| FILE			:	oedt_test_rpc_core.c
| PROJECT		:      ADIT GEN 2
| SW-COMPONENT	: 	OEDT RPC TEST
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains OEDTs for rpc
|------------------------------------------------------------------------------
| COPYRIGHT		:	(c) 2011 Bosch
| HISTORY		:      
|------------------------------------------------------------------------------
| Date 		| 	Modification			|	Author
|------------------------------------------------------------------------------
| 23.02.2011  	| Initial revision			| 	SUR2HI
| --.--.--  	| ----------------           | ------------
| 11.08.2011  	| Lint Fix					| 	SRJ5KOR
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/
#include "oedt_test_rpc_core.h"

extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);

/*****************************************************************************
* FUNCTION		:  u32RPCRegCallTest()
* PARAMETER		:  OEDT_RPC_CallData
* RETURNVALUE	:  tU32, "OSAL_OK" on success  or "non-zero" value in case of error
* DESCRIPTION	:  Callback function for rpc id OEDT_RPC_TEST_CONNECTION_LINUX
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 23 Feb, 2011
******************************************************************************/
tS32 u32RPCRegCallTest(OEDT_RPC_CallData* CallData )
{
	char StrPrn []= {"Hello From LI"};
	OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"Linux Callback function ");
	CallData->out_size = sizeof(StrPrn);
	OSAL_pvMemoryCopy (CallData->out_arg, StrPrn, CallData->out_size);
	return OSAL_OK;
}


/*****************************************************************************
* FUNCTION		:  u32RPCConnTester()
* PARAMETER		:  none
* RETURNVALUE	:  tU32, "OSAL_OK" on success  or "non-zero" value in case of error
* DESCRIPTION	:  OEDT for testing the RPC connection
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 23 Feb, 2011
******************************************************************************/
tU32 u32RPCConnTester(void)
{
	tU32 u32Ret = OSAL_OK;
	char StrSnd [] = {"Msg From LI:Say Hello !!"};
	//u32Ret = u32RPCRegCallTest();
	OEDT_RPC_CallData CallData = {OEDT_RPC_LAST_CHAN, {""}, 0, {""}, 0};
	
	CallData.enRpcId = OEDT_RPC_TEST_CONNECTION_TENGINE;
	CallData.in_size = sizeof(StrSnd);
	OSAL_pvMemoryCopy (CallData.in_arg, StrSnd, CallData.in_size);
	
	if (OSAL_OK != OEDT_RPC_Call(&CallData,1000))
	{
		u32Ret += 100;
	}
	return u32Ret;
}

