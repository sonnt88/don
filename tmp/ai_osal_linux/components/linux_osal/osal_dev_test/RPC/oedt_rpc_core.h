/******************************************************************************
| FILE			:	oedt_rpc_core.h
| PROJECT		:      ADIT GEN 2
| SW-COMPONENT	: 	OEDT RPC 
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains rpc definitions same files should be used in both OS
|------------------------------------------------------------------------------
| COPYRIGHT		:	(c) 2011 Bosch
| HISTORY		:      
|------------------------------------------------------------------------------
| Date 		| 	Modification			|	Author
|------------------------------------------------------------------------------
| 14.02.2011  	| Initial revision			| 	SUR2HI
| --.--.--  	| ----------------           | ------------
| 11.08.2011  	| Lint Fix					| 	SRJ5KOR
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/
#if !defined (OEDT_RPC_CORE_H)
#define OEDT_RPC_CORE_H

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_rpc_id.h"

#ifndef  LOCAL
#define  LOCAL static
#endif

/*Error codes */
#define OEDT_RPC_OK					 0
#define OEDT_RPC_E_INVALID_ARG 		-1
#define OEDT_RPC_E_CH_ALREADY_REG 	-2
#define OEDT_RPC_E_CH_NOT_REG 		-3
#define OEDT_RPC_E_CB_NOT_REG 		-4
#define OEDT_RPC_E_UNKNOWN	 		-5



#define OEDT_RPC_MESSAFGE_Q_TE_LI_CMD 	"RPC_TE_CMD"
#define OEDT_RPC_MESSAFGE_Q_TE_LI_RES 	"RPC_TE_RES"
#define OEDT_RPC_MESSAFGE_Q_LI_TE_CMD 	"RPC_LI_CMD"
#define OEDT_RPC_MESSAFGE_Q_LI_TE_RES 	"RPC_LI_RES"
#define OEDT_RPC_THREAD_NAME		"OEDT_RPC"


#define OEDT_RPC_MESSAFGE_Q_LEN	512
#define OEDT_RPC_MAX_RPC_IC		256
#define _RPC_ARG_LEN	256


typedef enum
{
  RPC_STATE_RUNNING,
  RPC_STATE_READY,
  RPC_STATE_NOTREADY
}RPC_INTERNAL_STATE;


typedef struct
{
	OedtRpcID enRpcId;
	char in_arg[_RPC_ARG_LEN];	/* Input argument pointer -- NULL incase of no argument */
	tU16 in_size;	/* Input argument size -- Maximum 256bytes supported */
	char out_arg[_RPC_ARG_LEN];	/* Output argument pointer -- NULL incase of no argument */
	tU16 out_size;	/* Output argument size -- Maximum 256bytes supported */
} OEDT_RPC_CallData;

typedef tS32 (*OedtRPCfCallback) ( OEDT_RPC_CallData* );

typedef struct
{
	OedtRpcID enRpcId;
	OedtRPCfCallback	pCallback;
} OEDT_RPC_Channel;

tS32 OEDT_RPC_Reg_callback (const OEDT_RPC_Channel* ChInfo);
tS32 OEDT_RPC_Call (OEDT_RPC_CallData* CallData, OSAL_tMSecond timeout);
tS32 OEDT_RPC_Unreg_callback (OedtRpcID RpcId );

#endif

