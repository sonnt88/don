/******************************************************************************
| FILE			:	oedt_rpc_core.c
| PROJECT		:      ADIT GEN 2
| SW-COMPONENT	: 	OEDT RPC 
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains rpc implementation
|------------------------------------------------------------------------------
| COPYRIGHT		:	(c) 2011 Bosch
| HISTORY		:      
|------------------------------------------------------------------------------
| Date 		| 	Modification			|	Author
|------------------------------------------------------------------------------
| 14.02.2011  	| Initial revision			| 	SUR2HI
| --.--.--  	| ----------------           | ------------
| 11.05.2011  	| Added STM Helper		| 	SUR2HI
| --.--.--  	| ----------------           | ------------
| 11.08.2011  	| Lint Fix					| 	SRJ5KOR
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/

#include "oedt_rpc_core.h"
#include "oedt_Configuration.h"
#include "oedt_test_rpc_core.h"


LOCAL OedtRPCfCallback cbArr[255] = {NULL};
LOCAL OSAL_tMQueueHandle mqHandleTEtoLICmd	= 0;
LOCAL OSAL_tMQueueHandle mqHandleLItoTECmd	= 0;
LOCAL OSAL_tMQueueHandle mqHandleTEtoLIRes	= 0;
LOCAL OSAL_tMQueueHandle mqHandleLItoTERes	= 0;

RPC_INTERNAL_STATE oedt_rpc_eState = RPC_STATE_NOTREADY;

extern tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);


/*Call back function Array for initialisation in Linux*/
OEDT_RPC_Channel RPCInitEntries [] = 
{
	/*Add all callback functions here with ID */
	{OEDT_RPC_TEST_CONNECTION_LINUX, u32RPCRegCallTest},
	
	{OEDT_RPC_LAST_CHAN, NULL} /*Last Callback Dont delete this */
};

/*Call data used for message queue*/
typedef struct
{
	char arg[_RPC_ARG_LEN];	/* Input argument pointer -- NULL incase of no argument */
	tU16 size;				/* Input argument size -- Maximum 256bytes supported */
} _RPC_CallData;

/*Struct  used for message queue*/
typedef struct
{
	OedtRpcID enRpcId;
	tS32 RetVal;
	_RPC_CallData CbData;
} OEDT_RPC_SndRcvData;

/*****************************************************************************
* FUNCTION		:  vCopyData()
* PARAMETER		:  OEDT_RPC_SndRcvData and char ponter "src"
* RETURNVALUE	:  none
* DESCRIPTION	:  Copies the content of Char buffer to structure OEDT_RPC_SndRcvData
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
void vCopyData(OEDT_RPC_SndRcvData* Dest, const tU8* src )
{
	tU32 Cnt;
	
	Dest->enRpcId = (OedtRpcID)(src[0]);
	Dest->RetVal = OSAL_OK;
	
	for (Cnt = 0 ; Cnt<_RPC_ARG_LEN; Cnt++)
		Dest->CbData.arg[Cnt] = src[8 + Cnt];

	Dest->CbData.size = (tU16)(src[264]); 
}


/*****************************************************************************
* FUNCTION		:  rpc_vCmdHandler()
* PARAMETER		:  none
* RETURNVALUE	:  none
* DESCRIPTION	:  Thread witing for RPC request
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
void rpc_vCmdHandler(void)
{
	tU8 pMsg[OEDT_RPC_MESSAFGE_Q_LEN] = {""};
	tU32 Vu32Result=OSAL_E_NOERROR;
	tS32 sRes = 0;
	OEDT_RPC_SndRcvData SndRcvData;
	OEDT_RPC_CallData CbData = {OEDT_RPC_LAST_CHAN, {"0"}, 0, {"0"}, 0};
  	/*Create 2 msg q one each for one direction*/
	if ( OSAL_ERROR == OSAL_s32MessageQueueCreate
			         (
			   			OEDT_RPC_MESSAFGE_Q_TE_LI_CMD,
						1,
						OEDT_RPC_MESSAFGE_Q_LEN,
						OSAL_EN_READWRITE,
						&mqHandleTEtoLICmd
					 )
					) 
	{
		/*check if error not error " already exist"*/
   		Vu32Result=OSAL_u32ErrorCode();
		if (OSAL_E_ALREADYEXISTS == Vu32Result)
		{
			if (OSAL_ERROR == OSAL_s32MessageQueueOpen(
					OEDT_RPC_MESSAFGE_Q_TE_LI_CMD, 
					OSAL_EN_READWRITE, &mqHandleTEtoLICmd))
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
					"Queue Creation/open failed...RPC will not work");
				return;
			}
		}
		else
		{
			OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
				"Queue Creation/open failed...RPC will not work");
			return;
		}
	}
	if ( OSAL_ERROR == OSAL_s32MessageQueueCreate
			         (
			   			OEDT_RPC_MESSAFGE_Q_LI_TE_CMD,
						1,
						OEDT_RPC_MESSAFGE_Q_LEN,
						OSAL_EN_READWRITE,
						&mqHandleLItoTECmd
					 )
					) 
	{
		/*check if error not error " already exist"*/
   		Vu32Result=OSAL_u32ErrorCode();
		if (OSAL_E_ALREADYEXISTS == Vu32Result)
		{
			if (OSAL_ERROR == OSAL_s32MessageQueueOpen(OEDT_RPC_MESSAFGE_Q_LI_TE_CMD, OSAL_EN_READWRITE, &mqHandleLItoTECmd))
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
				return;
			}
		}
		else
		{
			OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
			return;
		}
	}
	
	if ( OSAL_ERROR == OSAL_s32MessageQueueCreate
			         (
			   			OEDT_RPC_MESSAFGE_Q_TE_LI_RES,
						1,
						OEDT_RPC_MESSAFGE_Q_LEN,
						OSAL_EN_READWRITE,
						&mqHandleTEtoLIRes
					 )
					) 
	{
		Vu32Result=OSAL_u32ErrorCode();
		if (OSAL_E_ALREADYEXISTS == Vu32Result)
		{
			if (OSAL_ERROR == OSAL_s32MessageQueueOpen(
						OEDT_RPC_MESSAFGE_Q_TE_LI_RES, 
						OSAL_EN_READWRITE, &mqHandleTEtoLIRes))
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
				return;
			}
		}
		else
		{
			OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
			return;
		}
	}
	
	if ( OSAL_ERROR == OSAL_s32MessageQueueCreate
			         (
			   			OEDT_RPC_MESSAFGE_Q_LI_TE_RES,
						1,
						OEDT_RPC_MESSAFGE_Q_LEN,
						OSAL_EN_READWRITE,
						&mqHandleLItoTERes
					 )
					) 
	{
		Vu32Result=OSAL_u32ErrorCode();
		if (OSAL_E_ALREADYEXISTS == Vu32Result)
		{
			if (OSAL_ERROR == OSAL_s32MessageQueueOpen(
						OEDT_RPC_MESSAFGE_Q_LI_TE_RES, 
						OSAL_EN_READWRITE, &mqHandleLItoTERes))
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
				return;
			}
		}
		else
		{
			OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
							"Queue Creation/open failed...RPC will not work");
			return;
		}
	}
	
	oedt_rpc_eState = RPC_STATE_READY;
	
	for (;;)
	{
		OSAL_pvMemorySet(pMsg,0,OEDT_RPC_MESSAFGE_Q_LEN);
		sRes = OSAL_s32MessageQueueWait(mqHandleTEtoLICmd, 
						(tU8*)pMsg, OEDT_RPC_MESSAFGE_Q_LEN, OSAL_NULL, 
						OSAL_C_TIMEOUT_FOREVER);
		if (sRes != OSAL_OK)
		{
			Vu32Result=OSAL_u32ErrorCode(); 
			if (OSAL_E_TIMEOUT == Vu32Result)
			{
				SndRcvData.RetVal = OSAL_E_TIMEOUT;
			}
			else 
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),
										"RPC Internal error Terminating RPC");
				break;
			}
		}
	
		if(oedt_rpc_eState == RPC_STATE_READY)
		{
			vCopyData(&SndRcvData, (tU8*)pMsg);					
			if (SndRcvData.enRpcId >= OEDT_RPC_LAST_CHAN )
			{
				SndRcvData.RetVal = OEDT_RPC_E_CH_NOT_REG;
			}
			else if (cbArr[SndRcvData.enRpcId] == NULL )
			{
				SndRcvData.RetVal = OEDT_RPC_E_CB_NOT_REG;
			}
			else
			{	
				CbData.enRpcId = SndRcvData.enRpcId;
				if (SndRcvData.CbData.size > 0)
				{
					OSAL_pvMemoryCopy (CbData.in_arg, SndRcvData.CbData.arg, 
										SndRcvData.CbData.size);
					CbData.in_size	= SndRcvData.CbData.size;
				}
				SndRcvData.RetVal = 
						(cbArr[SndRcvData.enRpcId])(&(CbData));
			}
			if (CbData.out_size > 0 )
			{
				OSAL_pvMemoryCopy((char*)(SndRcvData.CbData.arg),
								(char*)(CbData.out_arg), CbData.out_size);
				SndRcvData.CbData.size = CbData.out_size;
			}
			
			if ( OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleLItoTERes, 
						(tU8*)&SndRcvData, OEDT_RPC_MESSAFGE_Q_LEN, OSAL_NULL))
			{
				OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"Result Post Failed!!");
			}
		}
	}
}


/*****************************************************************************
* FUNCTION		:  vInitRpcCb()
* PARAMETER		:  none
* RETURNVALUE	:  none
* DESCRIPTION	:  Initialises the RPC call-back functions
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
void vInitRpcCb(void)
{	
	tS32 Cnt = 0;
	while (RPCInitEntries[Cnt].enRpcId != OEDT_RPC_LAST_CHAN)
	{
		OEDT_RPC_Reg_callback(&(RPCInitEntries[Cnt]));
		Cnt ++ ;
	}
	OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"RPC %d Callbacks regestered", Cnt );
}


/*****************************************************************************
* FUNCTION		:  vOEDT_RPC_init()
* PARAMETER		:  none
* RETURNVALUE	:  none
* DESCRIPTION	:  Initialises the RPC
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
void vOEDT_RPC_init (void)
{
	OSAL_tThreadID         threadID ;
	OSAL_trThreadAttribute threadAttr;
	threadAttr.u32Priority  = _OEDT_MAX_PRIO;
	threadAttr.s32StackSize = _OEDT_PROCESS_STACK_SIZE;
	threadAttr.szName       = OEDT_RPC_THREAD_NAME;
	threadAttr.pfEntry      = (OSAL_tpfThreadEntry)rpc_vCmdHandler;
	threadAttr.pvArg        = (tVoid*)NULL;

	threadID = OSAL_ThreadSpawn(&threadAttr);

	if(threadID != 0)
	{ 
		while(oedt_rpc_eState == RPC_STATE_NOTREADY) OSAL_s32ThreadWait(5);
	}
	vInitRpcCb();
	OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"RPC init finished" );
}


/*****************************************************************************
* FUNCTION		:  OEDT_RPC_Reg_callback()
* PARAMETER		:  OEDT_RPC_Channel -- Channel information
* RETURNVALUE	:  OSAL_OK in case of success else error code
* DESCRIPTION	:  Registering RPC call back
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
tS32 OEDT_RPC_Reg_callback (const OEDT_RPC_Channel* ChInfo)
{
	if (ChInfo == NULL)
	{
		return OEDT_RPC_E_INVALID_ARG;
	}
	if (ChInfo->enRpcId == OEDT_RPC_LAST_CHAN  || ChInfo->pCallback == NULL  )
	{
		return OEDT_RPC_E_INVALID_ARG;
	}
	if (cbArr[ChInfo->enRpcId] != NULL)
	{
		return OEDT_RPC_E_CH_ALREADY_REG;
	}
	else
	{
		cbArr[ChInfo->enRpcId] = ChInfo->pCallback;
	}
	return OSAL_OK;
}


/*****************************************************************************
* FUNCTION		:  OEDT_RPC_Call()
* PARAMETER		:  OEDT_RPC_CallData -- Call data, timeout -- timeout in milliseconds
* RETURNVALUE	:  OSAL_OK in case of success else error code
* DESCRIPTION	:  function for calling a RPC procedure
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
tS32 OEDT_RPC_Call (OEDT_RPC_CallData* CallData, OSAL_tMSecond timeout)
{
	OEDT_RPC_SndRcvData SndRcvData;
	tU8 pMsg[OEDT_RPC_MESSAFGE_Q_LEN];
	OEDT_RPC_SndRcvData* RcvData = NULL;
	tU32  Vu32Result=OSAL_E_NOERROR;
	
	if (CallData == NULL)
	{
		return OEDT_RPC_E_INVALID_ARG;
	}
	if (CallData->enRpcId == OEDT_RPC_LAST_CHAN)
	{
		return OEDT_RPC_E_INVALID_ARG;
	}
	
	SndRcvData.enRpcId = CallData->enRpcId;
	OSAL_pvMemoryCopy (SndRcvData.CbData.arg, CallData->in_arg, 
														CallData->in_size);
	SndRcvData.CbData.size = CallData->in_size;
	
	if (OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandleLItoTECmd, 
					(tU8*)(&SndRcvData), sizeof(SndRcvData), OSAL_NULL))
	{
		OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"RPC Msg Post Failed" );
	}

	if (OSAL_ERROR == OSAL_s32MessageQueueWait(mqHandleTEtoLIRes, (tU8*)pMsg, 
										sizeof(SndRcvData), OSAL_NULL, timeout))
	{
		Vu32Result=OSAL_u32ErrorCode(); 
		if (OSAL_E_TIMEOUT == Vu32Result)
		{
			SndRcvData.RetVal = OSAL_E_TIMEOUT;
		}
		else
		{
			SndRcvData.RetVal = OEDT_RPC_E_UNKNOWN;
		}
		OEDT_HelperPrintf( (tU8)(TR_LEVEL_FATAL),"RPC Msg wait Failed" );
		return SndRcvData.RetVal;	
	}
	else 
	{
		RcvData = (OEDT_RPC_SndRcvData*)pMsg;
		if (RcvData->RetVal == 0)
		{
			if (RcvData->enRpcId == CallData->enRpcId && RcvData->CbData.size > 0)
			{
				CallData->out_size= RcvData->CbData.size;
				OSAL_pvMemoryCopy (CallData->out_arg,  RcvData->CbData.arg, 
								RcvData->CbData.size);
			}
			else if (RcvData->enRpcId != CallData->enRpcId)
			{
				RcvData->RetVal = OEDT_RPC_E_UNKNOWN;
			}
		}
		return RcvData->RetVal;	
	}
}

/*****************************************************************************
* FUNCTION		:  OEDT_RPC_Unreg_callback()
* PARAMETER		:  RpcId -- RPC ID
* RETURNVALUE	:  OSAL_OK in case of success else error code
* DESCRIPTION	:  Unregistering RPC call
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 14 Feb, 2011
******************************************************************************/
tS32 OEDT_RPC_Unreg_callback (OedtRpcID RpcId )
{
	if (RpcId >= OEDT_RPC_LAST_CHAN )
	{
		return OEDT_RPC_E_INVALID_ARG;
	}
	
	if (cbArr[RpcId] == NULL)
	{
		return OEDT_RPC_E_CH_NOT_REG;
	}
	else
	{
		cbArr[RpcId] = NULL;
	}
	return OSAL_OK;
}


