#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "unistd.h"
#include "system_kds_def.h"
#include "system_ffd_def.h"
#include <sys/mman.h>
#include <fcntl.h>

#include <stdio.h>
#include <signal.h>

#include "include/OsalIoscShmOperations.h"
#include "include/OsalIoscTestCommon.h"

tU8 oedt_boardname[30];
tBool bSigIntReceived;

OSAL_tMQueueHandle hMessageQueue;
OSAL_tMQueueHandle hACKQueue;

extern tU32 IOSC_TEST_SHM_SIZE;

tVoid SetBoardName()
{
  memset(oedt_boardname, 0, sizeof(oedt_boardname));
  sprintf(oedt_boardname, "unspecified");
}

tVoid WaitForAck( OEDT_TESTSTATE* state )
{
  tU8 sMessage[IOSC_TEST_MQ_MAX_MSGLEN];
  do
  {
    ReceiveMessage( hACKQueue, sMessage, state );
  } while ( TranslateMessage( sMessage, state ) != IOSC_TEST_ACK && (!state->error_code) );
}

tVoid PostAckedMessage( tCString szMessage, OEDT_TESTSTATE* state )
{
  PostMessage( hMessageQueue, szMessage, state );
  WaitForAck( state );
}

tVoid TestCase1( OEDT_TESTSTATE* state )
{
  PostAckedMessage( IOSC_TEST_S_OP_CREATE,  state);
  IOSC_TEST_shm_open ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_WRITE,   state);
  IOSC_TEST_shm_write ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_READ,    state);
  IOSC_TEST_shm_read ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_CLOSE,   state);
  PostAckedMessage( IOSC_TEST_S_OP_DELETE,  state);
  IOSC_TEST_shm_close ( state );
}

tVoid TestCase2( OEDT_TESTSTATE* state )
{
  PostAckedMessage( IOSC_TEST_S_OP_CREATE,  state);
  IOSC_TEST_shm_open ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_WRITE,   state);
  IOSC_TEST_shm_write ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_READ,    state);
  IOSC_TEST_shm_read ( &state );
  IOSC_TEST_shm_close ( &state );
  IOSC_TEST_shm_delete ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_CLOSE,   state);
}

tVoid TestCase3( OEDT_TESTSTATE* state )
{
  IOSC_TEST_shm_create ( &state );
  PostAckedMessage( IOSC_TEST_S_OP_OPEN,  state);
  PostAckedMessage( IOSC_TEST_S_OP_WRITE,   state);
  IOSC_TEST_shm_write ( state );
  PostAckedMessage( IOSC_TEST_S_OP_READ,    state);
  IOSC_TEST_shm_read ( state );
  PostAckedMessage( IOSC_TEST_S_OP_CLOSE,   state);
  PostAckedMessage( IOSC_TEST_S_OP_DELETE,  state);
  IOSC_TEST_shm_close ( state );
}

tVoid TestCase4( OEDT_TESTSTATE* state )
{
  IOSC_TEST_shm_create ( state );
  PostAckedMessage( IOSC_TEST_S_OP_OPEN,  state);
  PostAckedMessage( IOSC_TEST_S_OP_WRITE,   state);
  IOSC_TEST_shm_write ( state );
  PostAckedMessage( IOSC_TEST_S_OP_READ,    state);
  IOSC_TEST_shm_read ( state );
  IOSC_TEST_shm_close ( state );
  IOSC_TEST_shm_delete ( state );
  PostAckedMessage( IOSC_TEST_S_OP_CLOSE,   state);
}

tVoid ExecuteTestCases( OEDT_TESTSTATE* state )
{
  OEDT_TESTSTATE state1 = OEDT_CREATE_TESTSTATE();
  TestCase1( &state1 );
  OEDT_HelperPrintf(TR_LEVEL_FATAL, "TestCase1 finished with error code %d (shared memory size = %d).", state1.error_code, IOSC_TEST_SHM_SIZE);
  *state = OEDT_JOIN_TESTSTATES( state, &state1 );

  OEDT_TESTSTATE state2 = OEDT_CREATE_TESTSTATE();
  TestCase2( &state2 );
  OEDT_HelperPrintf(TR_LEVEL_FATAL, "TestCase2 finished with error code %d (shared memory size = %d).", state1.error_code, IOSC_TEST_SHM_SIZE);
  *state = OEDT_JOIN_TESTSTATES( state, &state2 );

  OEDT_TESTSTATE state3 = OEDT_CREATE_TESTSTATE();
  TestCase3( &state3 );
  OEDT_HelperPrintf(TR_LEVEL_FATAL, "TestCase3 finished with error code %d (shared memory size = %d).", state1.error_code, IOSC_TEST_SHM_SIZE);
  *state = OEDT_JOIN_TESTSTATES( state, &state3 );

  OEDT_TESTSTATE state4 = OEDT_CREATE_TESTSTATE();
  TestCase4( &state4 );
  OEDT_HelperPrintf(TR_LEVEL_FATAL, "TestCase4 finished with error code %d (shared memory size = %d).", state1.error_code, IOSC_TEST_SHM_SIZE);
  *state = OEDT_JOIN_TESTSTATES( state, &state4 );
}

tS32 main(int argc, char** argv)
{
  tU8 sMessage[IOSC_TEST_MQ_MAX_MSGLEN];
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  IOSC_TEST_OPERATION eIOSC_OP;
  bSigIntReceived = FALSE;

  SetBoardName();

  OEDT_HelperPrintf(TR_LEVEL_FATAL, "Starting standalone iosc test process");

  hMessageQueue = OpenMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_WRITEONLY, &state);
  hACKQueue = OpenMessageQueue( IOSC_TEST_MQ_ACK_CHANNEL, OSAL_EN_READONLY, &state);

  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "Unable to create message queue. The error code is %d", state.error_code);
    _exit(0);
  }

  ExecuteTestCases( &state );

  PostAckedMessage( IOSC_TEST_S_SET_HALF_SIZE,   &state);
  IOSC_TEST_shm_SetHalfSize();
  ExecuteTestCases( &state );

  PostAckedMessage( IOSC_TEST_S_SET_DOUBLE_SIZE,   &state);
  IOSC_TEST_shm_SetDoubleSize();
  ExecuteTestCases( &state );

  PostAckedMessage( IOSC_TEST_S_SET_DOUBLE_SIZE,   &state);
  IOSC_TEST_shm_SetDoubleSize();
  ExecuteTestCases( &state );

  PostAckedMessage( IOSC_TEST_S_OP_LEAVE,   &state);

  CloseMessageQueue( hACKQueue, &state );
  CloseMessageQueue( hMessageQueue, &state );

  OEDT_HelperPrintf(TR_LEVEL_FATAL, 
    "Exiting %s. Error code is %d. %d of %d check points succeeded.", 
    argv[0], state.error_code, 
    state.error_position - state.error_count, 
    state.error_position);

  _exit(state.error_code);
}
