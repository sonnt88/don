#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "unistd.h"
#include "system_kds_def.h"
#include "system_ffd_def.h"
#include <sys/mman.h>
#include <fcntl.h>

#include <stdio.h>
#include <signal.h>

#include "include/OsalIoscShmOperations.h"
#include "include/OsalIoscTestCommon.h"

tU8 oedt_boardname[30];
OSAL_tMQueueHandle hMessageQueue;
OSAL_tMQueueHandle hACKQueue;

tVoid SetBoardName()
{
  memset(oedt_boardname, 0, sizeof(oedt_boardname));
  sprintf(oedt_boardname, "unspecified");
}

tVoid SigIntHandler(int signal)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  PostMessage(hMessageQueue, IOSC_TEST_S_OP_LEAVE, &state);
}

tVoid mainLoop( OEDT_TESTSTATE* state )
{
  tU8 sMessage[IOSC_TEST_MQ_MAX_MSGLEN];
  IOSC_TEST_OPERATION eIOSC_OP;
  tBool bContinue = TRUE;

  do {
      ReceiveMessage( hMessageQueue, sMessage, state );
      eIOSC_OP = TranslateMessage( sMessage, state );

    if (state->error_code)
      break;

    bContinue = DispatchMessage( eIOSC_OP, state );
    if (!state->error_code)
      PostMessage( hACKQueue, IOSC_TEST_S_ACK, state );

  } while ( bContinue );
}

tS32 main(int argc, char** argv)
{
  OEDT_TESTSTATE state = OEDT_CREATE_TESTSTATE();
  OEDT_HelperPrintf(TR_LEVEL_FATAL, "Starting standalone iosc test process");

  SetBoardName();

  hMessageQueue = CreateMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, OSAL_EN_READONLY, &state );
  hACKQueue = CreateMessageQueue( IOSC_TEST_MQ_ACK_CHANNEL, OSAL_EN_WRITEONLY, &state );

  signal(SIGINT, &SigIntHandler);
   
  if (state.error_code)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "Unable to create message queue. The error code is %d", state.error_code);
    _exit(0);
  }

  mainLoop( &state );

  CloseMessageQueue( hACKQueue, &state );
  CloseMessageQueue( hMessageQueue, &state );

  DeleteMessageQueue( IOSC_TEST_MQ_ACK_CHANNEL, &state );
  DeleteMessageQueue( IOSC_TEST_MQ_DATA_CHANNEL, &state );

  OEDT_HelperPrintf(TR_LEVEL_FATAL, 
    "Exiting %s. Error code is %d. %d of %d check points succeeded.", 
    argv[0], state.error_code, 
    state.error_position - state.error_count, 
    state.error_position);

  _exit(state.error_code);
}
