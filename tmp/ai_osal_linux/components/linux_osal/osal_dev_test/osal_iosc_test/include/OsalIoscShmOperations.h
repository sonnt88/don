#ifndef OEDT_IOSC_SHM_OPERATIONS_H
#define OEDT_IOSC_SHM_OPERATIONS_H

#include <oedt_testing_macros.h>

#define IOSC_TEST_SHM_NAME "IOSC_TEST_SHM"

tVoid IOSC_TEST_shm_create( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_open( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_write( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_read( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_close( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_delete( OEDT_TESTSTATE* state );
tVoid IOSC_TEST_shm_SetHalfSize( tVoid );
tVoid IOSC_TEST_shm_SetDoubleSize( tVoid );

#endif //OEDT_IOSC_SHM_OPERATIONS_H
