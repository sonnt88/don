#include "include/OsalIoscTestCommon.h"
#include "include/OsalIoscShmOperations.h"

OSAL_tMQueueHandle CreateMessageQueue( tCString szMQName, OSAL_tenAccess mode, OEDT_TESTSTATE* state)
{
  tS32 s32ErrorCode;

  OSAL_tMQueueHandle hMessageQueue = 0;

  s32ErrorCode = OSAL_s32MessageQueueCreate(
    szMQName, 
    IOSC_TEST_MQ_MAX_MSGNUM, 
    IOSC_TEST_MQ_MAX_MSGLEN, 
    mode, &hMessageQueue);
  OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

  return hMessageQueue;
}

OSAL_tMQueueHandle OpenMessageQueue(tCString szMQName, OSAL_tenAccess mode, OEDT_TESTSTATE* state)
{
  
  tS32 s32ErrorCode = OSAL_ERROR;
  OSAL_tMQueueHandle hMessageQueue = 0;

  s32ErrorCode = OSAL_s32MessageQueueOpen(szMQName, mode, &hMessageQueue);
  OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);

  return hMessageQueue;
}

tVoid ReceiveMessage(OSAL_tMQueueHandle hMessageQueue, tU8* pu8Buffer, OEDT_TESTSTATE* state)
{
  tS32 s32ErrorCode;
  tU32 u32MQprio = IOSC_TEST_MQ_PRIO;

  s32ErrorCode = OSAL_s32MessageQueueWait( hMessageQueue, pu8Buffer, IOSC_TEST_MQ_MAX_MSGLEN, &u32MQprio, OSAL_C_TIMEOUT_FOREVER );
  OEDT_ADD_RESULT( state, (0 == s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS);
}

tS32 TranslateMessage(tCString szMessage, OEDT_TESTSTATE* state)
{
  tS32 s32TranslatedMessage = -1;

  if ( strncmp(IOSC_TEST_S_ACK, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_ACK;
  if ( strncmp(IOSC_TEST_S_OP_LEAVE,  szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_LEAVE;
  else if ( strncmp(IOSC_TEST_S_OP_CREATE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_CREATE;
  else if ( strncmp(IOSC_TEST_S_OP_OPEN, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_OPEN;
  else if ( strncmp(IOSC_TEST_S_OP_READ, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_READ;
  else if ( strncmp(IOSC_TEST_S_OP_WRITE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_WRITE;
  else if ( strncmp(IOSC_TEST_S_OP_CLOSE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_CLOSE;
  else if ( strncmp(IOSC_TEST_S_OP_DELETE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_OP_DELETE;
  else if ( strncmp(IOSC_TEST_S_SET_HALF_SIZE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_SET_HALF_SIZE;
  else if ( strncmp(IOSC_TEST_S_SET_DOUBLE_SIZE, szMessage, IOSC_TEST_MQ_MAX_MSGLEN) == 0 )
    s32TranslatedMessage = IOSC_TEST_SET_DOUBLE_SIZE;

  OEDT_ADD_RESULT( state, ( s32TranslatedMessage == -1 ) ? TEST_FAILURE : TEST_SUCCESS);

  return s32TranslatedMessage;
}

tBool DispatchMessage(tS32 s32Message, OEDT_TESTSTATE* state)
{
  switch (s32Message) {
    case IOSC_TEST_ACK:
      return TRUE;

    case IOSC_TEST_OP_LEAVE:
      return FALSE;

    case IOSC_TEST_OP_CREATE:
      IOSC_TEST_shm_create( state );
      break;

    case IOSC_TEST_OP_OPEN:
      IOSC_TEST_shm_open( state );
      break;

    case IOSC_TEST_OP_READ:
      IOSC_TEST_shm_read( state );
      break;

    case IOSC_TEST_OP_WRITE:
      IOSC_TEST_shm_write( state );
      break;

    case IOSC_TEST_OP_CLOSE:
      IOSC_TEST_shm_close( state );
      break;

    case IOSC_TEST_OP_DELETE:
      IOSC_TEST_shm_delete( state );
      break;

    case IOSC_TEST_SET_HALF_SIZE:
      IOSC_TEST_shm_SetHalfSize();
      break;

    case IOSC_TEST_SET_DOUBLE_SIZE:
      IOSC_TEST_shm_SetDoubleSize();
      break;

    default:
      OEDT_ADD_FAILURE( state ); //should never happen
      break;
  }

  return (state->error_code == 0);
}

tVoid CreateMessage(tU8* pu8Message, tCString sMessage)
{
  memset(pu8Message, 0, IOSC_TEST_MQ_MAX_MSGLEN);
  strncpy((tCString)pu8Message, sMessage, IOSC_TEST_MQ_MAX_MSGLEN);
}

tBool PostMessage( OSAL_tMQueueHandle hMessageQueue, tU8* pu8Message, OEDT_TESTSTATE* state )
{
  tS32 s32ErrorCode;
  tU8 csMessage[IOSC_TEST_MQ_MAX_MSGLEN];
  CreateMessage(csMessage, (tCString)pu8Message);
  s32ErrorCode = OSAL_s32MessageQueuePost( hMessageQueue, csMessage, IOSC_TEST_MQ_MAX_MSGLEN, IOSC_TEST_MQ_PRIO);
  OEDT_ADD_RESULT( state, (OSAL_OK != s32ErrorCode) ? TEST_FAILURE : TEST_SUCCESS );
  return (s32ErrorCode != OSAL_OK);
}

tVoid CloseMessageQueue(OSAL_tMQueueHandle hMessageQueue, OEDT_TESTSTATE* state)
{
  OEDT_ADD_RESULT( state, (OSAL_OK != OSAL_s32MessageQueueClose( hMessageQueue )) ? TEST_FAILURE : TEST_SUCCESS );
}

tVoid DeleteMessageQueue( tCString szMQName, OEDT_TESTSTATE* state)
{
  OEDT_ADD_RESULT( state, (OSAL_OK != OSAL_s32MessageQueueDelete( szMQName )) ? TEST_FAILURE : TEST_SUCCESS );
}
