/*****************************************************************************
* FILE         : process2.c
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 2 for multi-process testing of DRV GPIO
*              
* AUTHOR(s)    : Matthias Thomae (CM-AI/PJ-CF31)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 16.08.2012    | version 0.1               | Matthias Thomae (CM-AI/PJ-CF31)
*               |                           | Initial version for Linux
*------------------------------------------------------------------------------
* 6.2.2015      | version 0.2               | Shahida Mohammed Ashraf(RBEI/ECF5)
*               |                           | Modified MyCallback() for lintfix
*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"

#include "oedt_GPIO_TestFuncs.h"

tVoid MyCallback(tVoid *Arg)
{
    (tVoid)Arg;//for lint
//   printf("MyCallback proc2 called\n");
}

static tU32 mp_u32GPIOSetTrigEdgeHIGHIntEnable(OSAL_tGPIODevID DevID)
{
   OSAL_tIODescriptor hFd = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trGPIOCallbackData CbData;
   OSAL_trGPIOData Data = {0};

   /* Open the device */
   hFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, enAccess);
   if (OSAL_ERROR == hFd)
   {
      u32Ret = 1;
   }
   else
   {
      /* Set device to input mode */
      if (OSAL_ERROR == OSAL_s32IOControl(hFd,
          OSAL_C_32_IOCTRL_GPIO_SET_INPUT, (tS32) DevID))
      {
          u32Ret = 2;
      }
      else
      {
         // param to pass to the callback function
         CbData.rData.tId = DevID;
         CbData.rData.unData.pfCallback = MyCallback;
         CbData.pvArg = NULL;

         //param for setting the trigger
         Data.tId = DevID;
         Data.unData.u16Edge = OSAL_GPIO_EDGE_HIGH;

         /* Set callback */
         if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
         {
            u32Ret = 3;
         }
         /* Set trigger edge HIGH  */
         else if (OSAL_ERROR == OSAL_s32IOControl(hFd,
           OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, (tS32)&Data))
         {
            u32Ret = 4;
            CbData.rData.unData.pfCallback = NULL;
            OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
         }
         else
         {
            // param for enabling interrupt
            Data.unData.bState = TRUE;

            /* Enable interrupts */
            if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                  OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
            {
               u32Ret = 5;
               CbData.rData.unData.pfCallback = NULL;
               OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData);
            }
            else
            {
               /* wait for 5 secs */
               OSAL_s32ThreadWait(5000);

               Data.unData.bState = FALSE;
               /* Disable interrupts */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, (tS32)&Data))
               {
                  u32Ret = 6;
               }
               CbData.rData.unData.pfCallback = NULL;
               /* Remove callback  */
               if (OSAL_ERROR == OSAL_s32IOControl(hFd,
                     OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK, (tS32)&CbData))
               {
                  u32Ret += 7;
               }
            }
         }
      }

      /* Close the device */
      if (OSAL_ERROR == OSAL_s32IOClose(hFd))
      {
          u32Ret += 8;
      }
   }

   return u32Ret;
}

int main(int argc, char **argv)
{
   tS32 s32ReturnValue = 0;
   tU32 i = 0;

   OSAL_tGPIODevID DevID = OSAL_EN_TEST_GPIO_INPUT_LOCAL_2;
   OSAL_tMQueueHandle mqHandleRspns = OSAL_C_INVALID_HANDLE;
   tU32 outmsg;

   if (argc == 2)
   {
      DevID = atoi(argv[1]);
   }
//   printf("%s: using GPIO %d\n", argv[0], DevID);

   // Open MQ to send testresults
   for (i = 0; mqHandleRspns == OSAL_C_INVALID_HANDLE && i < 200; i++)
   {
     if (OSAL_s32MessageQueueOpen(MQ_GPIO_MP_RESPONSE_NAME,
           OSAL_EN_WRITEONLY, &mqHandleRspns) == OSAL_ERROR)
     {
        OSAL_s32ThreadWait(100);
     }
   }

   if (mqHandleRspns == OSAL_C_INVALID_HANDLE)
       s32ReturnValue += 100;

   if (s32ReturnValue == 0)
   {
     // run test
      s32ReturnValue = (tS32) mp_u32GPIOSetTrigEdgeHIGHIntEnable(DevID);

     // send result to OEDT
     outmsg = (1 << 16) | s32ReturnValue;
     if (OSAL_s32MessageQueuePost(mqHandleRspns, (tPCU8)&outmsg,
           sizeof(outmsg), 2) != OSAL_OK)
     {
         s32ReturnValue += 200;
     }
   }

   // Close MQs
   if (mqHandleRspns != OSAL_C_INVALID_HANDLE)
   {
       OSAL_s32MessageQueueClose(mqHandleRspns);
   }

   _exit(s32ReturnValue);
}

/************************ End of file ********************************/




