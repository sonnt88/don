/*****************************************************************************
* FILE         : process1.c
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 1 for multi-process testing of DRV RTC
*              
* AUTHOR(s)    : Sudharsanan Sivagnanam (RBEI/ECF5)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 23.11.2012    | version 0.1               | Sudharsanan Sivagnanam (RBEI/ECF5)
*               |                           | Initial version for Linux
*****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "oedt_rtc_TestFuncs.h"

/************************************************************************************
* FUNCTION         : tu32Gen2RTCSetGpsTime

* PARAMETER        : None

* RETURNVALUE      : tS32 error codes

* DESCRIPTION      :this function sets gps time.
*
* HISTORY          : 23.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 tu32Gen2RTCSetGpsTime( void )
{
	OSAL_tIODescriptor hDevice  = 0;
	tU32 u32RetVal              = 0;

			
	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC, OSAL_EN_READWRITE );
  if ( hDevice == OSAL_ERROR)
  {
			return(RTC_DEVICE_OPEN_ERROR);      		
  }

	if( OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME,(tS32)&rRTCDateTimeSetGps) == OSAL_ERROR )
	{				
			OSAL_s32IOClose ( hDevice );
			return( RTC_DEVICE_SET_GPS_TIME_ERROR );			
	}	

	if ( OSAL_s32IOClose ( hDevice ) == OSAL_ERROR )
	{
			return( RTC_DEVICE_CLOSE_ERROR );
	}
	
	return ( u32RetVal );
}

/************************************************************************************
* FUNCTION         : main

* PARAMETER        : None

* RETURNVALUE      : tS32 error codes

* DESCRIPTION      : This function contains process1 test case for rtc driver.
*
* HISTORY          : 23.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
int main(int argc, char **argv)
{
   tS32 s32ReturnValue = 0;
   tU32 i = 0;

   OSAL_tMQueueHandle mqHandleRspns    = OSAL_C_INVALID_HANDLE;
   tU32 outmsg;

   // Open MQ to send testresults
   for (i = 0; mqHandleRspns == OSAL_C_INVALID_HANDLE && i < 200; i++)
   {
     if (OSAL_s32MessageQueueOpen(MQ_RTC_MP_RESPONSE_NAME,
           OSAL_EN_WRITEONLY, &mqHandleRspns) == OSAL_ERROR)
     {
        OSAL_s32ThreadWait(100);
     }
   }

   if (mqHandleRspns == OSAL_C_INVALID_HANDLE)
       s32ReturnValue += 100;

   if (s32ReturnValue == 0)
   {
     // run test
      s32ReturnValue = (tS32) tu32Gen2RTCSetGpsTime();

     // send result to OEDT
     outmsg = (1 << 16) | s32ReturnValue;
     if (OSAL_s32MessageQueuePost(mqHandleRspns, (tPCU8)&outmsg,
           sizeof(outmsg), 2) != OSAL_OK)
     {
         s32ReturnValue += 200;
     }
   }

   // Close MQs
   if (mqHandleRspns != OSAL_C_INVALID_HANDLE)
   {
       OSAL_s32MessageQueueClose(mqHandleRspns);
   }

   _exit(s32ReturnValue);
}

/************************ End of file ********************************/




