/******************************************************************************
 *FILE         : oedt_Macros.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION:   This file contains variable definitions and declarations used
 *				 in the corresponding C file.
 *AUTHOR       : 
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 12.09.03  Rev. 1.0 RBIN
 *               - Initial Revision.
 *
 *****************************************************************************/
#ifndef OEDT_MACROS_HEADER
#define OEDT_MACROS_HEADER

#ifndef LOBYTE
  #define LOBYTE( wValue )    ((tU8)( wValue ))
#endif

/* @mac HIBYTE  | Returns value of high byte of parameter */
/* @parm tWORD  | wValue  | Word of which high byte is returned */
#ifndef HIBYTE
  #define HIBYTE( wValue )    ((tU8)((tU16)( wValue ) >> 8))
#endif

/* @mac LOWORD  | Returns value of low word of parameter */
/* @parm tDWORD  | dwValue  | Double word of which low word is returned */
#ifndef LOWORD
  #define LOWORD( dwValue )           ((tU16)(tU32)( dwValue ))
#endif

/* @mac HIWORD  | Returns value of high word of parameter */
/* @parm tDWORD  | dwValue  | Double word of which high word is returned */
#ifndef HIWORD
  #define HIWORD( dwValue )           ((tU16)((((tU32)( dwValue )) >> 16) & 0xFFFF))
#endif

#endif
