/******************************************************************************
 *FILE         : odt_Globals.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : Contains global declaration for this component
 *
 *AUTHOR       : Pemmaiah BD
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 24.07.03  Rev. 1.0 RBIN/ECM2- Pemmaiah
 *               - Initial Revision.
 *               11.08.03  Rev. 1.1 RBIN/ECM2- Pemmaiah
 *               - Added the macro ODT_LOWORD & ODT_HIWORD
 *               - Defined the priority of thread of ODT Frame work
 *               14.10.03  Rev. 1.2 RBIN/ECM2- Pemmaiah
 *               - Added ODT_TEST_GET_THREAD_INFO
 *               - Added definition of RESRC_REL_FUNCPTR.
 *               15.05.06 Rev  1.4  RBIN/ECM1- Venkateswara.N
 *		  -separation of the OEDT from
 *		     ODT Frm Work
 *                -Unnecessary Prototypes are removed
 *                  (int Check4TEF(void);
 *                   void WriteTEF(void);	 
 *                   void RemoveTEF(void); )
 *                    
 *
 *****************************************************************************/

#ifndef ODT_GLOBALS_HEADER
#define ODT_GLOBALS_HEADER 

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define ODT_MAKEWORD(low, high) \
((tU16) (((tU8) (low)) | ((tU16) ((tU8) (high))) << 8)) 

#define ODT_GET_HIBYTE(w)  \
((tU8) (((tU16) (w) >> 8) & 0xFF)) 

#define ODT_GET_LOBYTE(w)  ((tU8) (w)) 

#define ODT_MAKELONG(low, high) \
((tU32) (((tU16) (low)) | ((tU32) ((tU16) (high))) << 16)) 

#define ODT_LOWORD(l)           ((tU16)((tU32)(l) & 0xffff))

#define ODT_HIWORD(l)           ((tU16)((tU32)(l) >> 16))

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/
//If more number of devices has to be supported then increase this 
//value accordingly
#define ODT_NUM_OF_DEVICES_SUPPORTED         32
#define ODT_NUM_THREADS_PER_DEVICE           8

//For Message Queue
#define ODT_MQ_MAX_LEN                       100
#define ODT_MQ_MAX_MSGS                      10
#define ODT_TEST_THRD_STACK_SIZE             8000

//Used in thread command handler
#define ODT_TEST_THREAD_CREATE               241
#define ODT_TEST_THREAD_KILL                 242
#define ODT_TEST_GET_TEST_STATISTICS         243
#define ODT_TEST_LOOP_EXIT                   244
#define ODT_TEST_GET_THREAD_INFO             245

#define ODT_TEST_PASS                        0
#define ODT_TEST_FAIL                        1
#define ODT_SUCCESS                          0
#define ODT_FAILURE                          1


#define ODT_INVALID_DEV                      0
#define ODT_NULL_STRING                      "\0"
#define ODT_NULL_PTR                         OSAL_NULL
#define ODT_INVALID_MQ_HANDLE                OSAL_ERROR
#define ODT_INVALID_THRD_ID                  0

#define ODT_MAX_FILE_PATH_LEN                44 
#define ODT_INFINITE_TEST_LOOP               255
#define ODT_MAX_SIZE_RESP_PACKET             239  //Trace device limitation
#define ODT_MAX_SIZE_RESP_STRING             (ODT_MAX_SIZE_RESP_PACKET - 7)

//Priority of threads in ODT frame work
#define ODT_THRD_PRIOTITY_THRD_CMND_HNDLR    40
#define ODT_THRD_PRIOTITY_TEST_CMND_HNDLR    100

//Semaphore initial state.
#define ODT_SEMPHORE_UNAVAILABLE             0
#define ODT_SEMPHORE_AVAILABLE               1
/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
//Structure of command packet
typedef struct odt_CommandPacket 
{
   tU8 u8DeviceID;
   tU8 u8TestID;
   tU8 u8ThreadID;
   tU8 u8NumOfCycles;
   tU8 u8LenTestSpecParam;
   tU8 au8TestSpecParams[ODT_MQ_MAX_LEN - 5];
} odt_trCommandPacket;

//Structure of Response packet
typedef struct odt_ResponsePacket   
{
   tU8 u8DeviceID;
   tU8 u8TestID;
   tU8 u8ThreadID;
   tU8 u8NumOfCycles;
   tU8 u8Result;
   tU8 u8MsgType;
   tU8 u8LenRespString;
   tU8 au8RespString[ODT_MAX_SIZE_RESP_STRING];  
} odt_trResponsePacket;     

//Structure for holding test stats
typedef struct odt_TestFuncParm
{
   odt_trCommandPacket *ptrCmndPkt;
   odt_trResponsePacket *ptrRespPkt;
} odt_trTestFuncParm;

/*  Device Test Stats array   */
typedef struct odt_DeviceTestStats
{
   tU32 u32NumOfTestsPassed;
   tU32 u32NumOfTestsFailed;
} odt_trDeviceTestStats;

//Contains device Info
typedef const struct odt_DeviceInfo
{
   tU8 u8DeviceID;   
   tBool bIsTestingEnabled;
}odt_cotrDeviceInfo;

//Prototype for function pointer
typedef tBool (*TEST_FUNCPTR)(odt_trTestFuncParm *);

/* Prototype for the device-specific resource release function pointer  */
typedef tBool (*RESRC_REL_FUNCPTR) (tVoid *);


/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/
extern odt_cotrDeviceInfo odt_acotrDevInfo[ODT_NUM_OF_DEVICES_SUPPORTED];
extern TEST_FUNCPTR *odt_acoppTestFunc[];
extern RESRC_REL_FUNCPTR odt_afpReleaseDevResource[];

extern OSAL_tSemHandle hSemIOLst;
extern OSAL_tSemHandle hSemAsync;
extern odt_trDeviceTestStats odt_atrDevTestStats [ODT_NUM_OF_DEVICES_SUPPORTED];
extern tS32 odt_ahs32OSALThrdId_DevThrd [ODT_NUM_OF_DEVICES_SUPPORTED];
extern tU32 odt_ahu32MsgQ_FrmCmdHandler [ODT_NUM_OF_DEVICES_SUPPORTED];
extern OSAL_tIODescriptor odt_hTraceDevice;

/* Array of Semaphores to protect access to the 
  elements of the DevTestStats array*/
extern OSAL_tSemHandle odt_ahSemDevTestStats [ODT_NUM_OF_DEVICES_SUPPORTED];

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

#ifndef CPP_COMPILER_ODT
//Odt_init.c
tBool bInit_TraceDevice (tVoid);
tBool bInit_ODTFrmWrk (tVoid);

//odt_CommandHandler.c
tVoid vCmndHandler_Callback (tU8 *pu8Data);
tVoid vSendRespPkt (tU32 u32TraceLevel, odt_trResponsePacket *rRespPkt);
tBool bSendBlockOfData(odt_trResponsePacket *prRespPkt, tS8* ps8DataBlock,
                       tU32 u32DataBlockSize);

//odt_ThreadCmndHandler.c
tVoid vThreadCmndHandler (tVoid *);


#else


extern "C" tBool bInit_TraceDevice (tVoid);
extern "C" tBool bInit_ODTFrmWrk (tVoid);

extern "C" tVoid vCmndHandler_Callback (tU8 *pu8Data);
extern "C" tVoid vSendRespPkt (tU32 u32TraceLevel, odt_trResponsePacket *rRespPkt);
extern "C" tBool bSendBlockOfData(odt_trResponsePacket *prRespPkt, tS8* ps8DataBlock, tU32 u32DataBlockSize);

//odt_ThreadCmndHandler.c
extern "C" tVoid vThreadCmndHandler (tVoid *);

extern "C" int Check4TEF();
extern "C" void WriteTEF();	
extern "C" void RemoveTEF();

#endif

#endif

