#ifndef OEDT_TESTSTATE_H
#define OEDT_TESTSTATE_H

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_Types.h"

typedef struct OEDT_TESTSTATE {
  tU8  __loop_state;
  tU8  __error_in_loop;
  tU8  __overflow;
  tU8  error_position;
  tU32 error_code;
  tU32 error_count;
  tU32 failure_repeat_count[32];
} OEDT_TESTSTATE;

#ifndef TEST_FAILURE
#define TEST_FAILURE 1
#else
#error TEST_FAILURE is already defined
#endif

#ifndef TEST_SUCCESS
#define TEST_SUCCESS 0
#else
#error TEST_SUCCESS is already defined
#endif

OEDT_TESTSTATE OEDT_CREATE_TESTSTATE();

tVoid OEDT_TEST_BEGIN_LOOP_STATE      (OEDT_TESTSTATE* teststate);
tVoid _OEDT_TEST_END_LOOP_STATE       (OEDT_TESTSTATE* teststate, tCString sFilename, tU32 u32LineNo);
tVoid OEDT_ADD_SUCCESS                (OEDT_TESTSTATE* teststate);
tVoid _OEDT_ADD_FAILURE               (OEDT_TESTSTATE* teststate, tCString sFilename, tU32 u32LineNo);
tVoid _OEDT_ADD_RESULT				        (OEDT_TESTSTATE* teststate, tU8 result, tCString sFilename, tU32 u32LineNo);
tBool OEDT_CHECK_TESTSTATE_OVERFLOW   (OEDT_TESTSTATE* teststate);
OEDT_TESTSTATE OEDT_JOIN_TESTSTATES   (OEDT_TESTSTATE* teststate1, OEDT_TESTSTATE* teststate2);
OEDT_TESTSTATE OEDT_CONCAT_TESTSTATES (OEDT_TESTSTATE* teststate1, OEDT_TESTSTATE* teststate2);

#define OEDT_TEST_END_LOOP_STATE(teststate) _OEDT_TEST_END_LOOP_STATE(teststate, __FILE__, __LINE__)
#define OEDT_ADD_FAILURE(teststate)         _OEDT_ADD_FAILURE(teststate, __FILE__, __LINE__)
#define OEDT_ADD_RESULT(teststate, result)  _OEDT_ADD_RESULT(teststate, result, __FILE__, __LINE__)

#define OEDT_EXPECT_EQUALS( EXPECTED_VALUE, VALUE, state )  if ( EXPECTED_VALUE == VALUE ) OEDT_ADD_SUCCESS ( state ); else OEDT_ADD_FAILURE ( state )
#define OEDT_EXPECT_DIFFERS( EXPECTED_VALUE, VALUE, state ) if ( EXPECTED_VALUE == VALUE ) OEDT_ADD_FAILURE ( state ); else OEDT_ADD_SUCCESS ( state )
#define OEDT_EXPECT_TRUE( EXPRESSION, state )               if ( EXPRESSION ) OEDT_ADD_SUCCESS ( state ); else OEDT_ADD_FAILURE ( state )
#define OEDT_EXPECT_FALSE( EXPRESSION, state )              if ( EXPRESSION ) OEDT_ADD_FAILURE ( state ); else OEDT_ADD_SUCCESS ( state )

#define OEDT_EXPECT_NULL( PTR, state )       OEDT_EXPECT_FALSE( PTR , state )
#define OEDT_EXPECT_NOT_NULL( PTR, state )   OEDT_EXPECT_TRUE( PTR , state )


#endif //OEDT_TESTSTATE_H
