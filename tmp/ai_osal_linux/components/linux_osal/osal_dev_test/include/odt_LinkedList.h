/******************************************************************************
 *FILE         : odt_LinkedList.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : Contains structures and defines required by the linked list
 *               component.
 *
 *AUTHOR       : Joseph Fernandez
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 24.07.03  Rev. 1.0 RBIN/ECM2- Joseph
 *               Initial Revision.
 *****************************************************************************/


#ifndef ODT_LINKEDLIST_HEADER

#define ODT_LINKEDLIST_HEADER


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
   

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

#define ODT_FOUND          0
#define ODT_NOT_FOUND         1


/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
   

typedef struct odt_IOHdlNodeData
{
   tU8 u8ThreadID;
   tU8 u8DevID;
   tChar szName [ ODT_MAX_FILE_PATH_LEN ];
   OSAL_tIODescriptor hIO;   
   OSAL_trAsyncControl rAsyncCtrl;
} odt_trIOHdlNodeData;


struct odt_trIOHdlLnkLst
{
   odt_trIOHdlNodeData rNodeData;
   struct odt_trIOHdlLnkLst *ptrNxtNode;
};



/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/


struct odt_trIOHdlLnkLst *
ptrGetFirst
   (
   tVoid
   );


tBool
bCreateNode
   (
   odt_trIOHdlNodeData *ptrInNodeData  /*    (->I)    */
   );


tBool
bDeleteNode
   (
   odt_trIOHdlNodeData *ptrInNodeData  /*    (->I)    */
   );


OSAL_tIODescriptor
hGetIOHdl
   (
   odt_trIOHdlNodeData *ptrInNodeData  /*    (->I)    */
   );

tBool
bUpdateAsyncCtrl
   (
   odt_trIOHdlNodeData *prInNodeData  /*    (->IO)      */
   );

OSAL_trAsyncControl *
prGetAsyncCtrl
   (
   odt_trIOHdlNodeData *prInNodeData  /*    (->IO)      */
   );

#endif
/* */
