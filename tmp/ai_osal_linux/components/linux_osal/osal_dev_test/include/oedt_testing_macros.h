#ifndef OEDT_TESTING_MACROS_H
#define OEDT_TESTING_MACROS_H

#include "oedt_teststate.h"

/* represents the test that does nothing*/
#define OEDT_EMPTY_TEST {NULL, NULL, NULL}

/* get the testing function by the test name */
//#define OEDT_TESTFUNC_NAME(name) u32##name##TestFunc
#define OEDT_TESTFUNC_NAME(name) name

/* get the testing function signature by the test name */
#define OEDT_TESTFUNC_SIGNATURE(name) tU32 OEDT_TESTFUNC_NAME(name)(tVoid)

/* get the setup function by the test name */
#define OEDT_SETUPFUNC_NAME(name) u32##name##SetUpFunc

/* get the setup function signature by the test name */
#define OEDT_SETUPFUNC_SIGNATURE(name) tU32 OEDT_SETUPFUNC_NAME(name)(tVoid)

/* get the teardown function by the test name */
#define OEDT_TEARDOWNFUNC_NAME(name) u32##name##TearDownFunc

/* get the teardown function signature by the test name */
#define OEDT_TEARDOWNFUNC_SIGNATURE(name) tU32 OEDT_TEARDOWNFUNC_NAME(name)(tVoid)

/* create an OEDT_TEST_FUNCTIONS value in a way compatible to the "old" oedt-way */
#define OEDT_SIMPLE_TEST_FUNCTION(testfunc) {testfunc, NULL, NULL}

/* create a simple OEDT_TEST_FUNCTIONS value that only contains a testing function.
Use OEDT_TEST() to declare the testing function. Do not use this if your test contains setup and/or teardown logic. */
#define OEDT_TEST_SIMPLE(name) {OEDT_TESTFUNC_NAME(name), NULL, NULL}

/* declare a simple test that only contains a testing function. Do not use this if your test contains setup and/or teardown logic. */
#define OEDT_TEST_SIMPLE_DECLARATION(name) \
  OEDT_TESTFUNC_SIGNATURE(name);

/* create a OEDT_TEST_FUNCTIONS value for a test that that contains setup logic. */
#define OEDT_TEST_WITH_SETUP(name) {OEDT_TESTFUNC_NAME(name), OEDT_SETUPFUNC_NAME(name), NULL}

/* declare a test that contains setup logic. Use OEDT_TEST() to declare the testing function and OEDT_TEST_SETUP() 
to declare the setup function. */
#define OEDT_TEST_WITH_SETUP_DECLARATION(name) \
  OEDT_TESTFUNC_SIGNATURE(name); \
  OEDT_SETUPFUNC_SIGNATURE(name);

/* create a OEDT_TEST_FUNCTIONS value for a test that that contains teardown logic. */
#define OEDT_TEST_WITH_TEARDOWN(name) {OEDT_TESTFUNC_NAME(name), NULL, OEDT_TEARDOWNFUNC_NAME(name)}

/* declare a test that contains teardown logic. Use OEDT_TEST() to declare the testing function and OEDT_TEST_TEARDOWN() 
to declare the teardown function. */
#define OEDT_TEST_WITH_TEARDOWN_DECLARATION(name) \
  OEDT_TESTFUNC_SIGNATURE(name); \
  OEDT_TEARDOWNFUNC_SIGNATURE(name);

/* create a OEDT_TEST_FUNCTIONS value for a test that that contains both setup and teardown logic. */
#define OEDT_TEST_WITH_SETUP_AND_TEARDOWN(name) {OEDT_TESTFUNC_NAME(name), OEDT_SETUPFUNC_NAME(name), OEDT_TEARDOWNFUNC_NAME(name)}

/* declare a test that contains both setup and teardown logic. Use OEDT_TEST() to declare the testing function, OEDT_TEST_SETUP() to
declare the setup function and OEDT_TEST_TEARDOWN() to declare the teardown function. */
#define OEDT_TEST_WITH_SETUP_AND_TEARDOWN_DECLARATION(name) \
  OEDT_TESTFUNC_SIGNATURE(name); \
  OEDT_SETUPFUNC_SIGNATURE(name); \
  OEDT_TEARDOWNFUNC_SIGNATURE(name);

/* declare the actual testing function for the test specified by 'name' */
#define OEDT_TEST(name)          OEDT_TESTFUNC_SIGNATURE(name)

/* declare the setup function for the test specified by 'name' */
#define OEDT_TEST_SETUP(name)    OEDT_SETUPFUNC_SIGNATURE(name)

/* declare the teardown function for the test specified by 'name' */
#define OEDT_TEST_TEARDOWN(name) OEDT_TEARDOWNFUNC_SIGNATURE(name) 

#endif //OEDT_TESTING_MACROS_H
