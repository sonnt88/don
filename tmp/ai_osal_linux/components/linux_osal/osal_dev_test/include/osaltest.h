/*****************************************************************************
| $Id: osaltest.h,v 1.7 2002/12/09 11:48:33 kan2hi Exp $
|*****************************************************************************
| FILE:         osaltest.h
| PROJECT:      ELeNa
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the headerfile for the OSAL test suite  Component.
|
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) TECNE S.r.l, CAgliari (ITALY)
| HISTORY:
| Date      | Modification               | Author
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************
| This file is under RCS control (do not edit the following lines)
| $RCSfile: osaltest.h,v $
| $Revision:   1.1  $
| $Date:   Jun 22 2004 13:02:42  $
|*****************************************************************************/

#if !defined (OSAL_OSALTEST_HEADER)
#define OSAL_OSALTEST_HEADER

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/*****************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/*****************************************************************************
|typedefs and struct defs (scope: global)
|----------------------------------------------------------------------------*/

/*****************************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------------------*/

/*****************************************************************************
|function prototypes (scope: global)
|----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#else
#error osaltest.h included several times
#endif
