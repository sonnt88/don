#ifndef _OEDT_HELPER_FUNCS_H_
#define _OEDT_HELPER_FUNCS_H_
/*!
 *\file     oedt_helper_funcs
 *\ref      
 *\brief    some help functions for audio tester
 *              
 *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH
 *\author		CM-DI/PJ-GM32 - Resch
 *\version:
 * CVS Log: 
 * $Log: oedt_helper_funcs
 * \bugs      
 * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
 * \warning   
 **********************************************************************/
#ifdef __cplusplus
extern "C"
{
#endif



/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
 enum OEDT_HW_ID
{
	OEDT_GMGE = 1,
	OEDT_JLR,
	OEDT_FORD,
	OEDT_ICM,
    OEDT_GM_MY13,
    OEDT_LCN2KAI    

};
extern char oedt_boardname[30];     // name of actual HW for test exclusion

struct oedt_device_test {
	tCString dev_name;
	tU32     ret_val;
};
	
/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tVoid vPrintOutTime(OSAL_tMSecond TimetakenToRead, OSAL_tMSecond TimetakenToWrite, tU64 u64AllBytesToRead, tU64 u64AllBytesToWrite );
tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...);
tU32 u32OEDT_BoardName(tVoid);
tU32 u32OEDT_OpenCloseDevice(tCString DevName, tU32 u32ExpectedRetVal, OSAL_tenAccess enAccess);

#ifdef __cplusplus
}
#endif
#else/* #ifndef _OEDT_HELPER_FUNCS_H_ */
#error "oedt_helper_funcs.h multiple included"
#endif/* #ifndef _OEDT_HELPER_FUNCS_H_ */
