/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Database.h
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Interface for OEDT database access funcs.
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#define OEDT_DATABASE_REGISTERED_SETS       4
#define OEDT_DATABASE_OVERALLTEST_SET_ID    0
#define OEDT_DATABASE_REGRESSIONTEST_SET_ID 1

#define OEDT_DATABASE_DEFAULT_SET_ID        OEDT_DATABASE_OVERALLTEST_SET_ID


void oedt_DB_vReleaseSizeMap(void);
void oedt_DB_vBuildSizeMap(void);
void* oedt_DB_pGetDataset(void);
void* oedt_DB_pGetDatasetByID(tU32 u32Idx);
tU32 oedt_DB_u32GetSize(void);
tU32 oedt_DB_u32GetSelectedSet(void);
tU32 oedt_DB_u32SelectSet(tU32 u32Idx);
void* oedt_DB_GetDatasetEntry(tU32 u32Idx);
tU32 oedt_DB_u32GetMaxTest(tU32 u32Class);
tU32 oedt_DB_u32GetMaxEntries(void);

