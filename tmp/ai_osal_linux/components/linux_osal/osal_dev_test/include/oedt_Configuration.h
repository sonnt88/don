/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Configuration.h
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *              Project specific configuration for OEDT
 *
 * NOTES:
 *              -
 * History:
 *			    Apr 22, 2009, Removed lint and compiler warnings by Sainath Kalpuri (RBEI/ECF1)
 *
 * COPYRIGHT:   TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#ifndef OEDT_CONFIGURATION_H
#define OEDT_CONFIGURATION_H

#define _OEDT_PROCESS_STACK_SIZE 	  65536                   /* system dependent value for stacksize */

/* assigned to cmd handler. all other oedt tasks are less priored. */
#define _OEDT_MAX_PRIO			      OSAL_C_U32_THREAD_PRIORITY_NORMAL

#define _OEDT_INCOMING_MSG_LEN        7                       /* incoming msg. length from ttfis-callback to oedt's msg-q */

#define _OEDT_STANDALONE              TRUE                    /* this defines whether oedt runs alone or piggyback at odt [TRUE | FALSE] */
#define _OEDT_TEST_TIMEOUT_DEFAULT    120                     /* timeout default value for the tests */
#define _OEDT_LIST_DELAY              5                       /* 5ms is recommended for paramount (ttfis via com) */

#define _OEDT_TRACE_DEVICE            "/dev/trace"
#define _OEDT_TRACE_CLASS             TR_COMP_OSALTEST
#define _OEDT_TRACE_LEVEL             TR_LEVEL_DATA
#define _OEDT_TRACE_PERMISSION        OSAL_EN_READWRITE
#define _OEDT_TTFIS_CHANNEL           TR_TTFIS_LINUX_OEDT

#define _OEDT_TTFIS_ODTDEV_ID         0x1a

#define _OEDT_TTFIS_COMMAND_ID        0x00
#define _OEDT_TTFIS_RESPONSE_ID       0x01
#define _OEDT_TTFIS_HELPER_TRACE_ID   0x02

#define _OEDT_DATASETNAME_LEN         32
#define _OEDT_CLASSNAME_LEN           32
#define _OEDT_TESTNAME_LEN            100

#define _OEDT_PROCESSNAME_CMDHANDLER  "OEDTcmdh"
#define _OEDT_PROCESSNAME_TESTHANDLER "OEDTtesth"
#define _OEDT_PROCESSNAME_LOADTASK	  "OEDTload"
#define _OEDT_PROCESSNAME_TESTFUNCS	  "OEDT_"

#define _OEDT_MSGQNAME_CMD            "OEDTLiCmdQ"
#define _OEDT_MSGQNAME_TESTER         "OEDTLiJobQ"

#define _OEDT_SEMAPHORENAME_TASKWAIT  "OEDTtasksem"

#define _OEDT_STORAGEPATH_CONFIG      "/dev/nand0/OEDT_CONFIG"
#define _OEDT_STORAGEPATH_TABLE       "/dev/nand0/OEDT_TABLE"

#endif  // OEDT_CONFIGURATION_H
