/* enum */
typedef enum {
	DTT_GPIO_POWER_U5,
	DTT_GPIO_TUN_HMI_RUN,
	DTT_GPIO_POWER_SUSPEND,
	DTT_GPIO_RFLS,
	DTT_GPIO_FGS_CAP_REQ,
	DTT_GPIO_TUN_HMI_PWR_ON,
	DTT_GPIO_DNL_PWR_ON,
	DTT_GPIO_BPCD_CD_INSERT,
	DTT_GPIO_FGS_CAP_RST,
	DTT_GPIO_POWER_UDROP_90,
	DTT_GPIO_BPCD_RESET,
	DTT_GPIO_BPCD_BUSY,
	DTT_GPIO_BPCD_WAKEUP,
	DTT_GPIO_TUN_HMI_RST_RU,
	DTT_GPIO_NFS_RDY,
	DTT_GPIO_TUN_HMI_RST_WARN_RU,
	DTT_GPIO_TUN_RU_RST_WARN_HMI,
	DTT_GPIO_TUN_SPI_REQ,
	DTT_GPTIMER_PWR_SNT_SYNC_FREQ,
	DTT_GPTIMER_FAN_PWM,
	DTT_GPIO_FGS_LCD_ACK_0,
	DTT_GPIO_FGS_LCD_ACK_1,
	DTT_GPIO_FAN_ON,
	DTT_GPIO_GYRO_BIT_OUT,
	DTT_GPIO_GYRO_BIT_IN,
	DTT_GPIO_MMC_DET,
	DTT_GPIO_EJECT_KEY,
	DTT_GPIO_GPS_ANT_ON2,
	DTT_GPIO_U140_SW1_ON,
	DTT_GPIO_DVD_DETECT,
	DTT_GPIO_IGNITON,
	DTT_GPIO_LINEOUT_ENA,
	DTT_GPIO_CODE_DETECT,
	DTT_GPIO_U140_SW2_ON,
	DTT_GPIO_UBAT_SENSE_ON,
	DTT_GPIO_NAV_DAC_ENA,
	DTT_GPIO_CODE_RST,
	DTT_GPIO_CAN1_MODE0,
	DTT_GPIO_CAN1_MODE1,
	DTT_GPIO_CAN1_WAKEUP,
	DTT_IPN_FGS_SPI,
	DTT_IPN_FGS_SPI_CS,
	DTT_IPN_V850_SPI,
	DTT_IPN_V850_SPI_CS,
	DTT_GPS_SPI,
	DTT_GPS_SPI_CS,
	DTT_DAB_SPI,
	DTT_DAB_SPI_CS,
	DTT_DEBUG_UART_ARM,
	DTT_DEBUG_UART_DSP,
	DTT_DEBUG_UART_GPS,
	DTT_BP_CD_UART,
	DTT_USB_DNL,
        DTT_I2C_1_LOCAL_PATCH,
        DTT_I2C_2_LOCAL_PATCH,
        DTT_I2C_3_LOCAL_PATCH,
        ULPD_WAKEUP_CONFIG,
        DTT_GYRO_TYPE,
	DTT_GPIO_CAN2_STB,
	DTT_GPIO_CAN2_EN,
	DTT_GPIO_CAN2_ERR,
	DTT_DMA_LCH_SPI1_TX,
	DTT_DMA_LCH_SPI1_RX,
	DTT_DMA_LCH_SPI2_TX,
	DTT_DMA_LCH_SPI2_RX,
	DTT_DMA_LCH_MEMORY,
	DTT_DMA_LCH_DAB,
	DTT_HW_MUTE_SENSE,
	DTT_HW_MUTE_RELEASE,
	DTT_EMBEDDEDRADIO_GPIO_ADR_RESET_LOCAL_PATCH,
    DTT_EMBEDDEDRADIO_GPIO_AUX_OUT_MUTE,
    DTT_EMBEDDEDRADIO_GPIO_ADR_REQ_LOCAL_PATCH,
    DTT_EMBEDDEDRADIO_SPI_ADR_SPI1_LOCAL_PATCH,
    DTT_EMBEDDEDRADIO_SPI_ADR_SPI1_CS_LOCAL_PATCH,
    DTT_EMBEDDEDRADIO_GPIO_PWR_TIM_REDUCTION,
    DTT_EMBEDDEDRADIO_GPIO_PWR_ON,
    DTT_EMBEDDEDRADIO_SPI_PWR_SPI1,
    DTT_EMBEDDEDRADIO_SPI_PWR_SPI1_CS,   
    DTT_EMBEDDEDRADIO_GPIO_APEB_DRV_EN_HIGH,
	DTT_LAST_TABLE_ENTRY  // this enum MUST be the last one for the DTT consistency check
} DTT_deviceName_t;


#define DTT_GET_SPI_CS_ID_FROM_DEVICEID( deviceId)  ( (tU16) ((deviceId) >> 16))     /**< extract the SPI Id for CS from the deviceId value */
