/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Core.h
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Interface func prototypes to OEDT core.
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
void oedt_vDatabaseList(void);
void oedt_vDatasetSelect(tU32 u32ID);
void oedt_vDatasetList(void);

tBool oedt_fTableSetSize(tU8 u8TableSize);
void  oedt_vTableSetEntry(tU8 u8EntryIdx, tU8 u8ClassBegin, tU8 u8ClassEnd, tU8 u8TestBegin, tU8 u8TestEnd);
tBool oedt_fTableSave(void);
tBool oedt_fTableLoad(void);
void  oedt_vTableList(tBool fMode);

void oedt_vModeSet_Flow(OEDT_FLOW_MODE_ID eMode);
void oedt_vModeSet_Special(OEDT_SPECIAL_MODE_ID eMode, tBool fFlag);
void oedt_vDelay(void);
void oedt_vConfigList(void);
tBool oedt_fConfigLoad(void);
tBool oedt_fConfigSave(void);

void oedt_vHoneypotInitHeap(tU8 value);
void oedt_vHoneypotSetLevel(tU8 tU8HoneypotLevel);

void oedt_vTestHandler(void);
void oedt_vInitCore(void);

void oedt_vStartTest(void);
void oedt_vStartTestCompatible(tU8 u8ClassBeg, tU8 u8ClassEnd, tU8 u8TestBeg, tU8 u8TestEnd, tBool fEndurance);

extern OEDT_INTERNAL_STATE oedt_eState;
extern tBool oedt_bStopFlg;

