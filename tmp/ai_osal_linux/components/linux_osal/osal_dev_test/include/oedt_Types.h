/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Types.h
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *              Type defionitions for OEDT.
 *
 * NOTES:
 *              -
 *
 * COPYRIGHT:   TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#ifndef OEDT_TYPES_H
#define OEDT_TYPES_H

#include "oedt_Configuration.h"

#ifdef OSAL_GEN2
#define OEDT_TESTPROCESS_BINARY_PATH "/opt/bosch/processes/"
#else /*OSAL_GEN2*/
#define OEDT_TESTPROCESS_BINARY_PATH "/opt/bosch/base/bin/"
#endif


/* HW exclusion defines */
#define OEDT_HW_EXC_NO          0x00000000      // no exclusion
#define OEDT_HW_EXC_TSIM        0x00000800      // TSIM-PC-Simulation
#define OEDT_HW_EXC_DUAL_OS     0x00004000      // TEngine + Linux
#define OEDT_HW_EXC_SINGLE_OS   0x00008000      // TEngine
#define OEDT_HW_EXC_GM_MY13     0x00010000      // GM NextGen MY13
#define OEDT_HW_EXC_GM_MY13_NAE 0x00020000      // GM NextGen MY13 North America + Europe (non-China)
#define OEDT_HW_EXC_LCN2KAI     0x00040000      // Nissan LCN2kai
#define OEDT_HW_EXC_LSIM        0x00080000      // LSIM
#define OEDT_HW_EXC_GEN3G       0x00100000      //Gen3LSIM

#define OEDT_HW_EXC_ALL         0xFFFFFFFF      // exclude all


typedef tU32 (*OEDT_TEST_FUNCPTR)(void);
typedef tU32 (*OEDT_SETUP_FUNCPTR)(void);
typedef tU32 (*OEDT_TEARDOWN_FUNCPTR)(void);

typedef enum
{
    OEDT_STATE_RUNNING,
    OEDT_STATE_READY,
    OEDT_STATE_NOTREADY
} OEDT_INTERNAL_STATE;

typedef enum
{
    OEDT_MODE_FLOW_SEQUENTIAL       = 0,
    OEDT_MODE_FLOW_PARALLEL_ASYNC,
    OEDT_MODE_FLOW_PARALLEL_SYNC
} OEDT_FLOW_MODE_ID;

typedef enum
{
    OEDT_MODE_SPECIAL_ENDURANCE     = 0x01,
    OEDT_MODE_SPECIAL_LOAD          = 0x02
} OEDT_SPECIAL_MODE_ID;

typedef struct OEDT_TEST_FUNCTIONS 
{
    OEDT_TEST_FUNCPTR     pTestFunc;
    OEDT_SETUP_FUNCPTR    pFuncSetUp;                       /* called before the test -> e.g. for test environment setup */
    OEDT_TEARDOWN_FUNCPTR pFuncTearDown;                    /* called after the test -> e.g. for cleanup or reboot purposes */
} OEDT_TEST_FUNCTIONS;

typedef struct
{
    OEDT_TEST_FUNCTIONS   pTestFunctions;                   /* pointer to the test functions */
    tU32                  u32HwExcMask;                     /* indicates the hw the test has to be performed on (HW exclusion mask)*/
    tU16                  u16SecTimeout;                    /* if no timeout is set (value is 0) - the default one is used (2min) */
    tU8                   pU8TestName[_OEDT_TESTNAME_LEN];  /* short description */
} OEDT_FUNC_CFG;

typedef struct
{
    OEDT_FUNC_CFG*        pFuncs;
    tU8                   pClassName[_OEDT_CLASSNAME_LEN];
} OEDT_TESTCON_ENTRY;

typedef struct
{
    OEDT_TESTCON_ENTRY*   pTestCon;
    tU8                   pTestConName[_OEDT_DATASETNAME_LEN];
} OEDT_DATASET_ENTRY;

typedef struct
{
    tU32  u32ClassID;
    tU32  u32TestID;
    tU32  u32ErrCode;
    tU8   u8TimedOut;
    tU8   u8Eq2Stored;
    tU8*  pu8ClassName;
    tU8*  pu8TestName;
    tU32  u32TimeOutVal;
    tS32  s32TestConsumedMemory;
    tU32  u32FreeMemory;
} OEDT_TEST_INFO;

typedef struct
{
    tU32  u32Failed;
    tU32  u32Passed;
    tU32  u32TimeOut;
} OEDT_TESTER_STATS;

#endif  // OEDT_TYPES_H
