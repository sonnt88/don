/**********************************************************FileHeaderBegin******
 * FILE:        oedt_Display.h
 *
 * AUTHOR:
 *
 * DESCRIPTION:
 *      Interface for OEDT display funcs.
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 **********************************************************FileHeaderEnd*******/
#define OEDT_RESPONSEMSG_LINE         0x00
#define OEDT_RESPONSEMSG_TABLEENTRY   0x01
#define OEDT_RESPONSEMSG_TESTSTART    0x02
#define OEDT_RESPONSEMSG_TESTRESULT   0x03
#define OEDT_RESPONSEMSG_EXTINFO      0x04
#define OEDT_RESPONSEMSG_MEMLEAK      0x05
#define OEDT_RESPONSEMSG_DATASET      0x06
#define OEDT_RESPONSEMSG_CONFIG       0x07
#define OEDT_RESPONSEMSG_TESTENTRY    0x08
#define OEDT_RESPONSEMSG_CONFIRM      0x09
#define OEDT_RESPONSEMSG_OVERALLSTATS 0x0a
#define OEDT_RESPONSEMSG_ENDURSTATS   0x0b
#define OEDT_RESPONSEMSG_HELP         0xff

#define OEDT_LINEID_NORMAL            0x00
#define OEDT_LINEID_CONFIG            0x01
#define OEDT_LINEID_TABLE             0x02
#define OEDT_LINEID_DATABASE          0x03
#define OEDT_LINEID_DATASET           0x04

void oedt_vSendLineMsg(tU8 u8ID);
void oedt_vSendTableEntry(tU32 u32Idx, tU32 u32ClassBeg, tU32 u32ClassEnd, tU32 u32TestBeg, tU32 u32TestEnd);
void oedt_vSendTestMsg(const OEDT_TEST_INFO* pTestInfo);
void oedt_vSendResultMsg(const OEDT_TEST_INFO* pTestInfo);
void oedt_vSendExtMsg(tPCS8 ps8ExtMsg, tU32 u32Len);
void oedt_vSendMemLeakMsg(tU32 u32PreVal, tU32 u32PostVal);
void oedt_vSendDatasetMsg(tU32 u32DatasetID, tPCS8 ps8Dataset);
void oedt_vSendTestCfgMsg(const OEDT_TEST_INFO* pTestInfo);
void oedt_vSendConfirmMsg(tBool fDone);
void oedt_vSendOverallStatsMsg(tU32 u32Passed, tU32 u32Failed, tU32 u32TimedOut);
void oedt_vSendEnduranceStatsMsg(tU32 u32Class, tU32 u32Test, const OEDT_TESTER_STATS* pStats, tPCS8 ps8ClassName, tPCS8 ps8TestName);
void oedt_vSendHelpMsg(void);
void oedt_vSendConfigMsg(OEDT_FLOW_MODE_ID eModeFlow, tU8 u8ModeSpec);

