#ifndef _OEDT_AUDIO_HELPER_TRACE_H_
#define _OEDT_AUDIO_HELPER_TRACE_H_
/*!
 *\file     oedt_Audio_HelperTrace
 *\ref      
 *\brief    some help functions for audio tester
 *              
 *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH
 *\author		CM-DI/PJ-GM32 - Resch
 *\version:
 * CVS Log: 
 * $Log: oedt_Audio_HelperTrace
 * \bugs      
 * \warning   
 **********************************************************************/
#ifdef __cplusplus
extern "C"
{
#endif



/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/
#define __FILE_SHORT_NAME__              (strrchr(__FILE__,'/')   \
                                             ? strrchr(__FILE__,'/')+1  \
                                             : __FILE__                 \
                                             )


#define OEDT_AUDIO_TRACE(a,b,c,d,e,f)    OEDT_AudioTrace(a, __FILE_SHORT_NAME__, __LINE__, b,c,d,e,f)

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
    tVoid OEDT_AudioTrace(TR_tenTraceLevel enTraceLevel,
                          tPCChar sz_filename, 
                          tU32 u32_line_num,
                          tPCChar copchDescription,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4);
    

#ifdef __cplusplus
}
#endif
#else/* #ifndef _OEDT_AUDIO_HELPER_TRACE_H_ */
#error "oedt_Audio_HelperTrace.h multiple included"
#endif/* #ifndef _OEDT_AUDIO_HELPER_TRACE_H_ */
