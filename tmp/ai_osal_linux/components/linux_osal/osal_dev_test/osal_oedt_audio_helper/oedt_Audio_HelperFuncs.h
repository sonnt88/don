#ifndef _OEDT_AUDIO_HELPER_FUNCS_H_
#define _OEDT_AUDIO_HELPER_FUNCS_H_
/*!
 *\file     oedt_Audio_HelperFuncs
 *\ref      
 *\brief    some help functions for audio tester
 *              
 *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH
 *\author		CM-DI/PJ-GM32 - Resch
 *\version:
 * CVS Log: 
 * $Log: oedt_Audio_HelperFuncs
 * \bugs      
 * \warning   
 **********************************************************************/
#ifdef __cplusplus
extern "C"
{
#endif


/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
    typedef enum _tag_oedt_audio_helper_error{
        OEDT_AUDIO_TEST_RES_OK    = 0,
        OEDT_AUDIO_TEST_RES_ERROR = -1,
        OEDT_AUDIO_TEST_RES_ALREADY_OPENED = -2
    }enAudioTestError;
    
    
/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
    enAudioTestError en_oedt_audio_reset_CS42888(void);
    enAudioTestError en_oedt_audio_VoiceOut(void);
    enAudioTestError fg_end_test();
    enAudioTestError fg_start_test(tU8 *reader_filename);
    enAudioTestError fg_setup_route(tU32 u32_route_id);

#ifdef __cplusplus
}
#endif
#else/* #ifndef _OEDT_AUDIO_HELPER_FUNCS_H_ */
#error "oedt_Audio_HelperFuncs.h multiple included"
#endif/* #ifndef _OEDT_AUDIO_HELPER_FUNCS_H_ */
