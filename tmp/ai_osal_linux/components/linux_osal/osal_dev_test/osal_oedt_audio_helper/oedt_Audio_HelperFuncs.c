
/******************************************************************************
 * Copyright (C) Blaupunkt GmbH, [year]
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ************************************************************************/

/************************************************************************/
/*! \file  oedt_Audio_TestFuncs.c
 *\brief    oedt audio stet-cases

 *\author		CM-DI/PJ-GM32 - Resch

 *\par Copyright: 
 *(c) *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH

 *\par History:  

 **********************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


#include <extension/extension.h>

#include "oedt_Audio_HelperFuncs.h"
#include "oedt_Audio_HelperTrace.h"

#include "iic.h"
#include "hwcl.h"

#include "fg_interface.h"
#include "stm_interface.h"



/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
#define IIC_DEV_NAME            ((UB*) "iica")
#define FG_TEST_SLEEPLENGTH     2000
#define FG_TEST_TMO_WAIT         255
#define OEDT_AUDIO_MAX_FG_PER_ROUTE 10

#define C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH  OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH
#define OEDT_AUDIO_TEST_DATA_1000HZ_16KHZ_SIZE       ((tU16) 16)

#define OEDT_AUDIO_ACOUSTICOUT_NUM_PERIODS_PER_BUFFER    ((tS32) 512)
#define OEDT_AUDIO_ACOUSTICOUT_BUFF_SIZE                 ((tS32) OEDT_AUDIO_TEST_DATA_1000HZ_16KHZ_SIZE * OEDT_AUDIO_ACOUSTICOUT_NUM_PERIODS_PER_BUFFER)
#define OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF                  ((tS32) 16)

#define OEDT_AUDIO_ACOUSTICOUT_BUFF_DURATION              ((tFloat) 0.5 /* s */)

#define OEDT_AUDIO_ACOUSTICOUT_NUM_WRITE_CALLS            ((tS32) 10)
#define OEDT_AUDIO_ACOUSTICOUT_TEST_DURATION              ((tU32) ((tFloat) OEDT_AUDIO_ACOUSTICOUT_NUM_WRITE_CALLS * \
                                                                   (tFloat) OEDT_AUDIO_ACOUSTICOUT_BUFF_DURATION)

/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/
/* Declares an DCM_MediaplayerTest-object called dcm_mp_test_app */


/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/
static ID rea_mbID       = -1;
static ID test_task_id       = -1;
static FG_ID route             = NULL;
static FG_ID admp              = NULL;
static FG_ID reader_generic    = NULL;
static FG_ID binary_buffer     = NULL;
static FG_ID writer_generic    = NULL;
static FG_ID audio_renderer    = NULL;

static U32 cnt = OEDT_AUDIO_MAX_FG_PER_ROUTE;
static FG_ID ids[OEDT_AUDIO_MAX_FG_PER_ROUTE] = {0};

static tU8 tx_cs42888_reg_values [] = { 
    0x83, // base addr
    0x00,  /* 0x03 Functional Mode default 0xF0 */
    0x49,   /* 0x04 Interface Formats default 0x36 */
    0x00,  /* 0x05 ADC Control & DAC De-Emph default 0x00 */
    0x10,   /* 0x06 Transition Control default 0x10 */
    0x00,  /* 0x07 DAC channel Mute default 0x00 */
    0x00,  /* 0x08 AOUTx Volume Control default 0x00 */
    0x00,  /* 0x09 AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0A AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0B AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0C AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0D AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0E AOUTx Volume Control default 0x00 */
    0x00,  /* 0x0F AOUTx Volume Control default 0x00 */
    0x00,  /* 0x10 DAC Channel invert default 0x00 */
    0x00,  /* 0x11 AINx Volume Control default 0x00 */
    0x00,  /* 0x12 AINx Volume Control default 0x00 */
    0x00,  /* 0x13 AINx Volume Control default 0x00 */
    0x00,  /* 0x14 AINx Volume Control default 0x00 */
    0x00,  /* 0x15 reserved */
    0x00,  /* 0x16 reserved */
    0x00,  /* 0x17 ADC Channel invert default 0x00 */
    0x00,  /* 0x18 Status Control default 0x00 */
    0x00,  /* 0x19 Status (Read only) default 0x00 */
    0x00,  /* 0x1A Status Mask default 0x00 */
    0x02}; /* 0x1B Mutex Pin Control default 0x00 */



tS16 s16_audio_test_data_16kHz_1kHz_mono[OEDT_AUDIO_TEST_DATA_1000HZ_16KHZ_SIZE] = {
#include "audio_test_data/test_data_1000Hz_16kHz_mono.h"
};

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/
static enAudioTestError en_oedt_audio_set_gpio(OSAL_tGPIODevID dev_id, tU32 u32_gpio_ioctl );
static STM_ret fg_test_requestPlaybackRoute(U8 *reader_filename);
static VP      fg_test_worker(int tskID);
static STM_ret fg_playback_test(FG_ID id);

/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/
enAudioTestError fg_setup_route(tU32 u32_route_id){

  enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_ERROR;
  STM_ret             ret = STM_ER_OK;
  ret = STM_requestDBRoute(&route, u32_route_id, ids, &cnt, rea_mbID);
  if (ret == STM_ER_OK){
      en_ret = OEDT_AUDIO_TEST_RES_OK;
      
  }
  return(en_ret);
  
}



enAudioTestError en_oedt_audio_VoiceOut(void){

    enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_ERROR;
    OSAL_tIODescriptor hAcousticout = NULL;
    OSAL_trAcousticSampleRateCfg rSampleRateCfg;
    tS32 s32_ret = -1, i=0, j=0;
    tS32 n_buff_idx = 0;
    tS32 s32WriteBytes = OEDT_AUDIO_ACOUSTICOUT_BUFF_SIZE;
    tS16 **as16Buffer = NULL; 

    s32_ret = 0;
    as16Buffer = (tS16**) OSAL_pvMemoryAllocate(OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF * sizeof(tS16*));
    if(as16Buffer != OSAL_NULL){
        for(i=0;i<OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF;i++){
            as16Buffer[i] = (tS16*) OSAL_pvMemoryAllocate(OEDT_AUDIO_ACOUSTICOUT_BUFF_SIZE * sizeof(tS16));
            if(as16Buffer == OSAL_NULL){
                s32_ret = -1;
            }
        }
    }else{
        s32_ret = -1;
    }

    if(s32_ret == 0){

        for(j=0;j<OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF;j++){
            for(i=0;i < OEDT_AUDIO_ACOUSTICOUT_NUM_PERIODS_PER_BUFFER;i++){
                
                OSAL_pvMemoryCopy(&(as16Buffer[j][i* OEDT_AUDIO_TEST_DATA_1000HZ_16KHZ_SIZE]),
                                  s16_audio_test_data_16kHz_1kHz_mono,
                                  sizeof(s16_audio_test_data_16kHz_1kHz_mono));
                
            }
        }
        
        hAcousticout = OSAL_IOOpen(C_SZ_DEV_ACOUSTICOUT_WITH_SPEECH, OSAL_EN_WRITEONLY);
        
        if(OSAL_ERROR == hAcousticout){
            en_ret = OEDT_AUDIO_TEST_RES_ERROR;
        }else{
            
            /* configure sample rate */
            rSampleRateCfg.enCodec = OSAL_EN_ACOUSTIC_DEC_PCM;
            rSampleRateCfg.nSamplerate = 16000;
            s32_ret = OSAL_s32IOControl(hAcousticout,
                                        OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE,
                                        (tS32)&rSampleRateCfg);
            
            if(OSAL_OK != s32_ret){
                en_ret = OEDT_AUDIO_TEST_RES_ERROR;
            }else{        
                /* issue start command */
                s32_ret = OSAL_s32IOControl(hAcousticout,
                                            OSAL_C_S32_IOCTRL_ACOUSTICOUT_START,
                                            NULL);
                if(OSAL_OK != s32_ret){
                    en_ret = OEDT_AUDIO_TEST_RES_ERROR;
                }else{        
                    /* test duration */
                    en_ret = OEDT_AUDIO_TEST_RES_OK;
                    n_buff_idx = 0;
                    for(i=0;i<OEDT_AUDIO_ACOUSTICOUT_NUM_WRITE_CALLS;i++){
                        s32_ret = OSAL_s32IOWrite(hAcousticout, (tPCS8) &(as16Buffer[n_buff_idx][0]), (tU32)s32WriteBytes);
                        if(s32_ret != OSAL_E_NOERROR){
                            en_ret = OEDT_AUDIO_TEST_RES_ERROR;
                        }
                        n_buff_idx++;
                        if(n_buff_idx >= OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF){
                            n_buff_idx = 0;
                        }
                    }
                    
                    if(OSAL_s32IOClose(hAcousticout) == OSAL_ERROR){
                        en_ret = OEDT_AUDIO_TEST_RES_ERROR;
                    }
                    
                }
            }
        }
    } /*if(s32_ret == 0){*/
    for(i=0;i<OEDT_AUDIO_ACOUSTICOUT_NUM_BUFF;i++){
        if(as16Buffer[i] != OSAL_NULL){
            OSAL_vMemoryFree(as16Buffer[i]);
            as16Buffer[i] = OSAL_NULL;
        }
    }
    
    if(as16Buffer != OSAL_NULL){
        OSAL_vMemoryFree(as16Buffer);
        as16Buffer = OSAL_NULL;
    }
    
    return(en_ret);
}


static enAudioTestError en_oedt_audio_set_gpio(OSAL_tGPIODevID dev_id, tU32 u32_gpio_ioctl ){

    enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_ERROR;
    tU32 u32Ret = 0;
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    
    hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO,enAccess);

    if ( OSAL_ERROR != hDevice){
        u32Ret = OSAL_s32IOControl ( hDevice,
                                     u32_gpio_ioctl,
                                     (tS32) dev_id);
        

        if ( OSAL_ERROR == u32Ret){
            en_ret = OEDT_AUDIO_TEST_RES_ERROR;
        }else{
            en_ret = OEDT_AUDIO_TEST_RES_OK;
        }

        OSAL_s32IOClose ( hDevice );
    }else{
        en_ret = OEDT_AUDIO_TEST_RES_ERROR;
    }
    

    return(en_ret);

}


LOCAL VP fg_test_worker(int tskID)
{
    FG_Message message;
    FG_ER error = FG_ER_OK;
    tskID = tskID;
    
    FG_SeekMode seeker;
    seeker.Offset = -1;
    seeker.Time = 0;
    
    while(error != FG_ER_FAILED)
        {
            tkse_rcv_mbf(rea_mbID, &message, FG_TEST_TMO_WAIT);
            
            switch (message.messageType)
                {
                case 0:
                    break;
                case 1:
                    if (message.notification == FG_NF_END_OF_STREAM)
                        {
                            FG_sendCommand(reader_generic, FG_CMD_SEEK, (VP)&seeker);
                        }
                    else if(message.notification == FG_NF_DEL_TSK)
                        {
                            error = FG_ER_FAILED;
                        }
                    break;
                default: 
                    break;
                }
        }
    tkse_ext_tsk();
    return 0;
}


enAudioTestError en_oedt_audio_reset_CS42888(void){

    enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
    ER rc     = E_OK; 
    ID dd     = 0;
    tS32 s32_size  = 0;
    IIC_info  master = {(0x48 << 1), IIC_MASTER, IIC_CLOCK_STANDARD};
    UB cs_en_pdn[] = {0x02, 0x01};
    UB cs_di_pdn[] = {0x02, 0x00};
    
    IIC_transfer tx_pkts[] = 
        { /* iic addr ,mode(rd/wr), no of bytes, data */
        {(0x48 << 1), IIC_XFER_WRITE, sizeof(cs_en_pdn), cs_en_pdn},
        {(0x48 << 1), IIC_XFER_WRITE, sizeof(tx_cs42888_reg_values), tx_cs42888_reg_values},
        {(0x48 << 1), IIC_XFER_WRITE, sizeof(cs_di_pdn), cs_di_pdn}
    };

    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"en_oedt_audio_reset_CS42888 entered",0,0,0,0);
    
    
    
    /* Reset cs42888 */
    //  rc = GPIO_setup(JV_IRQ_GPIO4_0, 1, (GPIO_mode_t)(GPIO_OUTPUT | GPIO_HIGH));
    en_ret = en_oedt_audio_set_gpio(JV_IRQ_GPIO4_0,  
                                    OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH);

    /* set i2s 4 from 3-wire to 4-wire mode */
    //rc = GPIO_setup((JV_SYSC | 0x1C0), 1, (GPIO_mode_t)(GPIO_OUTPUT | GPIO_HIGH));
    // volatile U32* PINFLOATCTRL = (volatile U32*)(JV_SYSC | X1C0);
    // (*PINFLOATCTRL) |= PINFLOAT_SETTING;  
    dd = tkse_opn_dev ( (UB*) IIC_DEV_NAME, TD_UPDATE );
    
    if ( dd > E_OK ){
        /* setup iic */
        rc = tkse_swri_dev ( dd, DN_IICSETUP, &master,
                             sizeof(master), (INT*)&s32_size );
        
        if ( E_OK == rc ){
            /* write config */
            rc = tkse_swri_dev ( dd, DN_IICTRANSFER, &tx_pkts,
                                 sizeof(tx_pkts), (INT*)&s32_size );

            if ( E_OK == rc ){
                en_ret = OEDT_AUDIO_TEST_RES_OK;
            }else{
                OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"Failes to write iic data",rc,0,0,0);
                en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
            }
            
        }else{
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"Failes to write iic data",rc,0,0,0);
            en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
        }
    }else{
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR,"Failes to open iic device",dd,0,0,0);
        en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
    }
    
    
    (void)tkse_cls_dev ( dd, 0 );
    
    OEDT_AUDIO_TRACE(TR_LEVEL_USER_1,"en_oedt_audio_reset_CS42888 left",0,0,0,0);
    return en_ret;
}




enAudioTestError fg_start_test(tU8 *reader_filename)
{
    STM_ret             ret = STM_ER_OK;
    enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_OK;  
    FG_Message msg = {0};
    
    /* Message Buffer */
    T_CMBF pk_cmbf;    

	/* Test task
	T_CTSK fg_test_task = {(VP)0, FG_POC_TSK_ATR, (FP)&fg_test_worker,
            20, 0x100, 0, NULL, NULL, 0, 0, "FGPOC", TLCID_SMP };
	*/
    /* create message buffer */
    /* set callbacks */
    pk_cmbf.exinf = NULL;
    pk_cmbf.mbfatr = (TA_TFIFO);
    pk_cmbf.bufsz = sizeof(FG_Message);
    pk_cmbf.maxmsz = sizeof(FG_Message);
    rea_mbID = tkse_cre_mbf(&pk_cmbf);

    /* start callback task */
    test_task_id = tkse_cre_tsk((P_FP)&fg_test_worker, 20);
    tkse_sta_tsk(test_task_id, test_task_id);
            
    
    /* test playback via requestDBRoute */
    ret = fg_test_requestPlaybackRoute(reader_filename);
        
    if (ret != STM_ER_OK)
        {
            en_ret = OEDT_AUDIO_TEST_RES_ERROR;
        }
    
    /* delete message task after tests */
    msg.messageType =   1; /* Notification */
    msg.notification =  FG_NF_DEL_TSK;
    msg.p_Message =       NULL;
    tkse_snd_mbf(rea_mbID, &msg, sizeof(FG_Message), FG_LONG_TIME_WAIT);
    return en_ret;
}

/* just for cleanup operations */
enAudioTestError fg_end_test()
{
    enAudioTestError en_ret     = OEDT_AUDIO_TEST_RES_OK;  
    ER               tkse_error = E_OK;
    STM_ret          ret        = STM_ER_OK;
	
    ret = STM_freeRoute(route);
    if (ret != STM_ER_OK)
    {
        en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR, "Test failed in STM_freeRoute",0,0,0,0);
    }

    
    tkse_error = tkse_del_mbf(rea_mbID);
    rea_mbID= -1;
    if (tkse_error != E_OK)
    {
        en_ret = OEDT_AUDIO_TEST_RES_ERROR;  
        OEDT_AUDIO_TRACE(TR_LEVEL_ERROR, "Test failed in releasing message buffer",0,0,0,0);
    }
    return en_ret;
}



static STM_ret fg_playback_test(FG_ID id)
{
    STM_ret ret = STM_ER_OK;

#ifdef OEDT_AUDIO_TRICKMODE_TEST_ENABLED
    S32 fg_rewind = -2;
    S32 fg_forward = 2;
    S32 fg_normal = 1;
    FG_SeekMode seeker;
    seeker.Offset = -1;
    seeker.Time = FG_TEST_SLEEPLENGTH;


    /* trickmode forward test */
    (void)STM_sendCommand(id, FG_CMD_SETSPEED, (VP)&fg_forward);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    
    /* trickmode backward test */
    (void)STM_sendCommand(id, FG_CMD_SETSPEED, (VP)&fg_rewind);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    
    /* switch to normal playback speed */
    (void)STM_sendCommand(id, FG_CMD_SETSPEED, (VP)&fg_normal);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    
    /* seek to a specified time */
    (void)STM_sendCommand(id, FG_CMD_SEEK, (VP)&seeker);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    
#else /*#ifdef OEDT_AUDIO_TRICKMODE_TEST_ENABLED*/

    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
    tkse_slp_tsk(FG_TEST_SLEEPLENGTH);

#endif /*#ifdef OEDT_AUDIO_TRICKMODE_TEST_ENABLED*/
    /* stop and release route */
    (void)STM_sendCommand(id, FG_CMD_STOP, NULL);
    (void)STM_sendCommand(id, FG_CMD_FLUSH, NULL);
    (void)STM_sendCommand(id, FG_CMD_RELEASE, NULL);
    
    /* free filter id */
    ret = STM_freeFilter(id);
    /* maybe id is a route then free route */
    if (ret != STM_ER_OK)
    {
        ret = STM_freeRoute(id);
    }
    return ret;
}



/* This test requests a specific route from the STM */
static  STM_ret fg_test_requestPlaybackRoute(U8 *reader_filename)
{
    STM_ret ret = STM_ER_OK;
    enAudioTestError en_ret = OEDT_AUDIO_TEST_RES_ERROR;


    en_ret =  fg_setup_route(1);
    if (en_ret == OEDT_AUDIO_TEST_RES_OK)
    {
        /* set filename */
        ret = STM_sendCommand(route, FG_CMD_SETSOURCEPATH, reader_filename);
        if (ret != FG_ER_OK)
        {
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR, "Set source path failed!",ret,0,0,0);
            ret = STM_ER_FAILURE;


        }
    }else{
        ret = STM_ER_FAILURE;
    }

    if (ret == STM_ER_OK)
    {
        /* wait here */
        tkse_slp_tsk(2000);

        /* let it roll */
        ret = STM_sendCommand(route, FG_CMD_START, NULL);
        tkse_slp_tsk(FG_TEST_SLEEPLENGTH);
        if (ret != STM_ER_OK)
        {
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR, "FAILED: Start playback of DB route!",ret,0,0,0);

        }
        
        /* The standard test routine */
        ret = fg_playback_test(route);
        if (ret != STM_ER_OK){
            OEDT_AUDIO_TRACE(TR_LEVEL_ERROR, "FAILED: Free of DB route!",ret,0,0,0);
        }
    }

   
    return ret;
}





/*! @}*/
