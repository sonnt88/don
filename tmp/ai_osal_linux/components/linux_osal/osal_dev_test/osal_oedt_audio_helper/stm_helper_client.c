/******************************************************************************
| FILE			:	stm_helper_client.c
| PROJECT		:	ADIT GEN 2
| SW-COMPONENT	:	OEDT STM HELPER CLIENT
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains STM helper client implementation
|------------------------------------------------------------------------------
| COPYRIGHT 	:	(c) 2011 Bosch
| HISTORY		:	   
|------------------------------------------------------------------------------
| Date		|	Modification			|	Author
|------------------------------------------------------------------------------
| 11.05.2011	| Initial revision			|	SUR2HI
| --.--.--	| ----------------			 | ------------
| 11.08.2011  	| Lint Fix					| 	SRJ5KOR
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/
#include "stm_helper_client.h"
#include "../RPC/oedt_rpc_core.h"

#define OEDT_RPC_TMO_DEFAULT 2000

/*****************************************************************************
* FUNCTION		:  STM_requestDBRoute()
* PARAMETER		:  route information
* RETURNVALUE	:  STM_ER_OK or Error
* DESCRIPTION	:  helper function for STM_requestDBRoute in TE
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 11 May, 2011
******************************************************************************/
STM_ret STM_requestDBRoute (FG_Comp p_route, FG_ID filterOrRoute)
{
    /*the default value returned if RPC call fails*/
    STM_ret ret = STM_ER_FAILURE;
	tS32 oedt_rpc_ret = OEDT_RPC_E_UNKNOWN;
	OEDT_RPC_CallData callData = {OEDT_RPC_LAST_CHAN, "0", 0, "0", 0};
	if (p_route != 0)
	{	
		callData.enRpcId = RPC_ID_STM_REQUEST_DB_ROUTE;
		callData.in_size= sizeof (p_route);
		OSAL_pvMemoryCopy (callData.in_arg, &p_route, callData.in_size);
		oedt_rpc_ret = OEDT_RPC_Call (&callData, OEDT_RPC_TMO_DEFAULT);
		if (oedt_rpc_ret != OEDT_RPC_OK)
		{
			ret = STM_ER_FAILURE;
		}
		else
		{
			OSAL_pvMemoryCopy (filterOrRoute, callData.out_arg, 
											callData.out_size);
			ret = STM_ER_OK;
		}
	}
	else 
	{
		ret = STM_ER_PARAM;
	}
    return ret;
}


/*****************************************************************************
* FUNCTION		:  STM_freeRoute()
* PARAMETER		:  route information
* RETURNVALUE	:  STM_ER_OK or Error
* DESCRIPTION	:  helper function for STM_freeRoute in TE
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 11 May, 2011
******************************************************************************/
STM_ret STM_freeRoute(FG_ConstComp route)
{
    /*the default value returned if RPC call fails*/
    STM_ret ret = STM_ER_FAILURE;
	tS32 oedt_rpc_ret = OEDT_RPC_E_UNKNOWN;
	OEDT_RPC_CallData callData = {OEDT_RPC_LAST_CHAN, "0", 0, "0", 0};
	tU32 filter_temp = *route ;
	if (filter_temp != OSAL_NULL){
		callData.enRpcId = RPC_ID_STM_FREE_ROUTE;
		callData.in_size= sizeof (filter_temp);
		
		OSAL_pvMemoryCopy (&(callData.in_arg [0]), 
								&filter_temp, sizeof (filter_temp)); // 2nd filter
		oedt_rpc_ret = OEDT_RPC_Call (&callData, OEDT_RPC_TMO_DEFAULT);
		
		if (oedt_rpc_ret != OEDT_RPC_OK)
		{
			ret = STM_ER_FAILURE;
		}
		else
		{
			ret = STM_ER_OK;
		}
	}
	else 
	{
		ret = STM_ER_PARAM;
	}
	
    return ret;
}


/*****************************************************************************
* FUNCTION		:  STM_sendCommand()
* PARAMETER		:  route information
* RETURNVALUE	:  STM_ER_OK or Error
* DESCRIPTION	:  helper function for STM_sendCommand in TE
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 11 May, 2011
******************************************************************************/
STM_ret STM_sendCommand(FG_ConstComp filterOrRoute, FG_CMDINDEX cmd)
{
    /*the default value returned if RPC call fails*/
    STM_ret ret = STM_ER_FAILURE;
	tS32 oedt_rpc_ret = OEDT_RPC_E_UNKNOWN;
	OEDT_RPC_CallData callData = {OEDT_RPC_LAST_CHAN, "0", 0, "0", 0};
	tU32 filter_temp = *filterOrRoute;
	if (filter_temp != OSAL_NULL)
	{
		callData.enRpcId = RPC_ID_STM_SEND_COMMAND;
		callData.in_size= sizeof (filter_temp) + sizeof (cmd);
		
		OSAL_pvMemoryCopy (callData.in_arg, &cmd, sizeof (cmd)); // First ele cmd
		OSAL_pvMemoryCopy (&(callData.in_arg [sizeof (cmd)]), 
								&filter_temp, sizeof (filter_temp)); // 2nd fiter
		oedt_rpc_ret = OEDT_RPC_Call (&callData, OEDT_RPC_TMO_DEFAULT);
		if (oedt_rpc_ret != OEDT_RPC_OK)
		{
			ret = STM_ER_FAILURE;
		}
		else
		{
			ret = STM_ER_OK;
		}
	}
	else 
	{
		ret = STM_ER_PARAM;
	}
	
    return ret;
}

