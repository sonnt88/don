/******************************************************************************
| FILE			:	stm_helper_client.h
| PROJECT		:	ADIT GEN 2
| SW-COMPONENT	:	OEDT STM HELPER CLIENT
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains STM helper client implementation
|------------------------------------------------------------------------------
| COPYRIGHT 	:	(c) 2011 Bosch
| HISTORY		:	   
|------------------------------------------------------------------------------
| Date		|	Modification			|	Author
|------------------------------------------------------------------------------
| 11.05.2011	| Initial revision			|	SUR2HI
| --.--.--	| ----------------		| ------------
| 11.08.2011  	| Lint Fix					| 	SRJ5KOR
| --.--.--  	| ----------------           | ------------

|*****************************************************************************/
#ifndef STM_HELPER_CLIENT_H
#define STM_HELPER_CLIENT_H

typedef unsigned long FG_Comp;
typedef FG_Comp *FG_ID;
typedef const FG_Comp *FG_ConstComp ;

//typedef int 		ID;

/** \typedef enum STM_ret
 *  \brief  Stream Manager Error Codes. */
typedef enum {
    STM_ER_MISSING_SSY = -0x7FF60000L,
    STM_ER_OK = 0,
    STM_ER_FAILURE,
    STM_ER_NULL_PTR,
    STM_ER_MEMORY,
    STM_ER_NOT_FOUND,
    STM_ER_TOO_SMALL,
    STM_ER_TMO_MTX,
    STM_ER_ALL_USED,
    STM_ER_FATAL,
    STM_ER_UNKNOWN,
    STM_ER_AVAILABLE,
    STM_ER_PARAM,
    STM_ER_ROUTE_TYPE,
    STM_ER_IN_USE
} STM_ret;


/** 
 * \enum FG_CMDINDEX
 * \brief Defines possible commands for components 
 */
typedef enum 
{
    FG_CMD_GETTYPE,                     /**< returns the type of the component */
    FG_CMD_START,                       /**< sets the component to the playstate */
    FG_CMD_STOP,                        /**< sets the component to the stopstate */
    FG_CMD_SEEK,                        /**< seeks to a specified position */
    FG_CMD_FLUSH,                       /**< flushes the components buffers */
    FG_CMD_GETSPEED,                    /**< returns the playback speed of the component */
    FG_CMD_SETSPEED,                    /**< sets the playback speed of the component */
    FG_CMD_EMPTYBUFFER,                 /**< processes the given buffer */
    FG_CMD_FILLBUFFER,                  /**< fills the given buffer */
    FG_CMD_SETSOURCEPATH,               /**< sets the filename for a source */
    FG_CMD_SETSINKPATH,                 /**< sets the filename for a sink */
    FG_CMD_RELEASE,                     /**< releases the component and its used hardware */
    FG_CMD_SETTRICKMODE,                /**< sets the trickmode variant for trickmode playback */    
    FG_CMD_SETCONFIG,                   /**< sets the database configuration. Database is handed over with call */
    FG_CMD_GETSTATE,                    /**< returns the components state */
    FG_CMD_GETMETADATA,                 /**< returns the metadata of the given component */
    FG_CMD_SETMETADATA,                 /**< sets the components metadata */
    FG_CMD_SETSTARTWATERMARK,           /**< sets watermark when the ring buffer starts to stream */
    FG_CMD_SETSTOPWATERMARK,            /**< sets watermark when the ring buffer stops to stream */
    FG_CMD_SETBITRATE,                  /**< sets the bitrate for encoding */
    FG_CMD_CHANGECODEC,                 /**< changes the codec for encoding */
    FG_CMD_SETTIMERFREQUENCY,           /**< sets the time update frequency */
    FG_CMD_SETMAXINPUTBUFFERSIZE,       /**< sets the maximum used input buffer size (AudioReader) */
    FG_CMD_SETLANGUAGE,                 /**< sets code page of container reader for Media TAG */
    FG_CMD_SETCHUNKSIZE,                /**< sets the chunksize for trickmodes */
    FG_CMD_GETSTREAMPROP,               /**< gets the stream property of an specific filter */
    FG_CMD_SETINPUTAUDIOPROP,           /**< sets the audio property (AudioReader) */
    FG_CMD_GETINPUTAUDIOPROP,           /**< gets the audio property (AudioReader) */
    FG_CMD_GETOUTPUTAUDIOPROP,          /**< fets the audio property (AudioRenderer) */
    FG_CMD_SETINPUTVIDEOPROP,           /**< sets the video property (VideoReader) */
    FG_CMD_GETINPUTVIDEOPROP,           /**< gets the video property (VideoReader) */
    FG_CMD_SETOUTPUTVIDEOPROP,          /**< sets the video property (VideoRenderer) */
    FG_CMD_GETOUTPUTVIDEOPROP,          /**< fets the video property (VideoRenderer) */
    FG_CMD_SETINPUTVOLUME,              /**< sets volume (AudioReader) */
    FG_CMD_SETOUTPUTVOLUME,             /**< sets volume (AudioRenderer) */
    FG_CMD_SETINPUTMLBADD,              /**< sets mlb address (AudioReader) */
    FG_CMD_GETINPUTMLBADD,              /**< gets mlb address (AudioReader) */
    FG_CMD_SETOUTPUTMLBADD,             /**< sets mlb address (AudioRenderer) */
    FG_CMD_GETOUTPUTMLBADD,             /**< gets mlb address (AudioRenderer) */
    FG_CMD_SETOUTPUTVIDEOFPS,           /**< set frames per second (VideoRenderer) */
    FG_CMD_SETOUTPUTVIDEOLAYER          /**< set display layer (VideoRenderer) */
} FG_CMDINDEX;


STM_ret STM_requestDBRoute ( FG_Comp p_route, FG_ID filterOrRoute  );
//Only Route ID is required on Linux side

STM_ret STM_freeRoute (FG_ConstComp route );

STM_ret STM_sendCommand( FG_ConstComp filterOrRoute, FG_CMDINDEX cmd);

//Only Filter and Cmd is required on Linux side further cmds will be added when required

#endif /* STM_HELPER_CLIENT_H */

