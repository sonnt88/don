


/******************************************************************************
 * Copyright (C) Blaupunkt GmbH, [year]
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ************************************************************************/

/************************************************************************/
/*! \file  oedt_Audio_TestTrace.c
 *\brief    oedt audio stet-cases

 *\author		CM-DI/PJ-GM32 - Resch

 *\par Copyright: 
 *(c) *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH

 *\par History:  

 **********************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|----------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_Audio_HelperTrace.h"
#include "util_lib.h"

/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/
#define AUDIO_TRACE_MAX_TRACE_SIZE      ((tU32) 512)
#define _OEDT_AUDIO_TRACE_SIGNATURE     ((tU16) 0xABBA)
/*****************************************************************
| typedefs (scope: modul-local)
|----------------------------------------------------------------*/


/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/
static tVoid OEDT_AudioTraceOut(tU32 u32Class,
                                TR_tenTraceLevel enTraceLevel,
                                tPCChar copchDescription,
                                tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4);

/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/
 /********************************************************************/ /**
  *  FUNCTION:      OEDT_AudioTraceOut
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u32Class           trace class
  *  @param         enTraceLevel       Message trace level
  *  @param         copchDescription   Additional info (only first 7 chars are used)
  *  @param         u32Par1            Trace message parameter
  *  @param         u32Par2            Trace message parameter
  *  @param         u32Par3            Trace message parameter
  *  @param         u32Par4            Trace message parameter            
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  ************************************************************************/
static tVoid OEDT_AudioTraceOut(tU32 u32Class,
                                TR_tenTraceLevel enTraceLevel,
                                tPCChar copchDescription,
                                tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{    
    tU8 au8Buf[1  * sizeof(tU16) +    /* signature */
               AUDIO_TRACE_MAX_TRACE_SIZE * sizeof(tChar) +  /* String */
               4  * sizeof(tU32)]    = {'\0'};
    
    tU32 u32Len;
    tInt n_data_cnt = 0;
    tU16 u16_signature = _OEDT_AUDIO_TRACE_SIGNATURE;
    TraceData tr_data = {0};
    
    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt],
                      &(u16_signature),
                      sizeof(tU16));
    n_data_cnt += 2;
    
    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt], 
                      &(u32Par1),
                      sizeof(tU32));
    n_data_cnt+=4;

    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt], 
                      &(u32Par2),
                      sizeof(tU32));
    n_data_cnt+=4;

    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt], 
                      &(u32Par3),
                      sizeof(tU32));
    n_data_cnt+=4;

    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt], 
                      &(u32Par4),
                      sizeof(tU32));
    
    n_data_cnt+=4;
    
    
    u32Len =  OSAL_u32StringLength(copchDescription);
    if(u32Len > AUDIO_TRACE_MAX_TRACE_SIZE){
        u32Len = AUDIO_TRACE_MAX_TRACE_SIZE;
    }
    
    OSAL_pvMemoryCopy(&au8Buf[n_data_cnt],
                      copchDescription,
                      u32Len);
    
    n_data_cnt += u32Len;
    
    au8Buf[n_data_cnt] = '\0';    
    n_data_cnt++;
    
    tr_data.uwLength = n_data_cnt;   /* Message length                    */
    tr_data.ubLevel  = enTraceLevel;    /* Trace level for the message       */
    tr_data.uhClass  = u32Class;    /* Trace class for the message       */
    tr_data.p_ubBuffer = (B*) au8Buf; /* The buffer containing the message */
    
    UTIL_traceb(&tr_data,au8Buf , n_data_cnt);
    
    return;
    
}

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/
/********************************************************************/ /**
 *  FUNCTION:      OEDT_AudioTraceUser1
 *
 *  @brief         Prints information trace message.
 *
 *  @param         enTraceLevel       trace level
 *  @param         sz_filename        file_name
 *  @param         u32_line_num       line_num
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter            
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 12.03.07 Carsten Resch
 *    Initial revision.
 ************************************************************************/

tVoid OEDT_AudioTrace(TR_tenTraceLevel enTraceLevel,
                      tPCChar sz_filename, 
                      tU32 u32_line_num,
                      tPCChar copchDescription,
                      tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{

    tChar sz_trace_out_str[AUDIO_TRACE_MAX_TRACE_SIZE] = "";

    OSALUTIL_s32SaveNPrintFormat(sz_trace_out_str, sizeof(sz_trace_out_str)-1,
                                 "%s (line: %d): %s", sz_filename, u32_line_num, copchDescription);
    sz_trace_out_str[AUDIO_TRACE_MAX_TRACE_SIZE-1] = '\0';
    
    OEDT_AudioTraceOut(TR_COMP_OSALTEST,
                       enTraceLevel,
                       sz_trace_out_str,
                       u32Par1,  u32Par2,  u32Par3,  u32Par4);
    return;
}


/*! @}*/
