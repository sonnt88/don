/*****************************************************************************
* FILE         : process3.cpp
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 3 for multi-process testing of Datapool 
*                This process read KDS deafult value
*              
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           |  comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 27.11.2015    | version 0.1               |  Initial version
* 03.08.2016    | version 0.2               |  add test 
*                                           |    u32DPStoreAllEndUserPoolsIfConfigEndUserStored
* 05.08.2016    | version 0.3               |  add test
*                                           |    u32DPSetBankAllEndUserPoolsIfConfigEndUserStored
*                                           |    u32DPSetDefaultEndUserPoolsIfConfigEndUserStored
*                                           |    u32DPCopyUserIfConfigEndUserStored
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DP_S_IMPORT_INTERFACE_FI
#include "dp_effd_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "oedt_DataPool_TestFuncs.h"


#define DP_ACCESS_ID_END_USER_APP  (tU16)0x00D8  //CCA_C_U16_APP_FC_USERMANAGER


#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
static tU32 u32DPSetEndUser(tU8 Pu8EndUser)
{
  tU32 Vu32ErrorCode=0;
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100000000;
  }
  else
  { /*set end user*/
	  if(DP_s32SetEndUser(Pu8EndUser,DP_ACCESS_ID_END_USER_APP)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200000000;
	  }
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=400000000;
	  }
  }
  return(Vu32ErrorCode);
}

static tU32 u32DPSetEndUserDefault(tU8 Pu8EndUser)
{
  tU32 Vu32ErrorCode=0;
  /* lock*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000;
  }
  else
  { /*set end user*/
	  if(DP_s32SetEndUserDefault(Pu8EndUser,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20000;
	  }	 
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
      Vu32ErrorCode|=40000;
	  }
  }
  return(Vu32ErrorCode);
}
//set end user
static tS32 s32DPCopyEndUser(tU8 Pu8EndUserFrom,tU8 Pu8EndUserTo)
{
  tS32 Vs32ErrorCode=DP_S32_NO_ERR;
  /* lock*/
  Vs32ErrorCode=DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000);
  if(Vs32ErrorCode==DP_S32_NO_ERR)  
  { /*set end user*/
	  Vs32ErrorCode=DP_s32CopyEndUser(Pu8EndUserFrom,Pu8EndUserTo,DP_ACCESS_ID_END_USER_APP,5000);
	  /*unlock  DP_U32_LOCK_MODE_END_USER*/
    DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000);
  }
  return(Vs32ErrorCode);
}
//helper function for load bank to current user
static tU32 u32OEDTDPLoadBankToCurrentEndUser(tU8 Vu8BankLoad)
{
  tU32                 Vu32ErrorCode=0;
  /* load bank to current user*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=10000000;
  }
  else
  { 
	  if(DP_s32LoadBankToCurrentEndUser(Vu8BankLoad,DP_ACCESS_ID_END_USER_APP,8000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=20000000;
	  }	
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=40000000;
    }
  }  
  return(Vu32ErrorCode);
}
//helper function for save current to bank
static tU32 u32OEDTDPSaveCurrentEndUserToBank(tU8 Vu8Bank)
{
  tU32                 Vu32ErrorCode=0;
  /* load bank to current user*/
  if(DP_s32Lock(DP_ACCESS_ID_END_USER_APP,DP_U32_LOCK_MODE_END_USER,5000)!=DP_S32_NO_ERR)
  {
    Vu32ErrorCode|=100000000;
  }
  else
  { 
	  if(DP_s32SaveCurrentEndUserToBank(Vu8Bank,DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
	  {
	    Vu32ErrorCode|=200000000;
	  }
    if(DP_s32Unlock(DP_ACCESS_ID_END_USER_APP,5000)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=400000000;
    }
  }  
  return(Vu32ErrorCode);
}

//helper function for check end user not stored
static tU32 u32OEDTDPCheckFileStored(tString PStrFile,tU8 Vu8EndUser,tBool VbDefault,tBool VbBank)
{
    char   VcFile[150];
    int    ViFile;
    tU32   Vu32ErrorCode=0;
    tU8    VubInc;
    tU8    VubIncBank;
     
    for(VubInc=0;VubInc<4;VubInc++)
    { /*check for end user extension 00,01,02 ..*/
      memset(VcFile,0,sizeof(VcFile));
      snprintf(VcFile,sizeof(VcFile),"%s%02x.dat",PStrFile,VubInc);
      // fprintf(stderr, "test %s\n",VcFile);
      ViFile = open(VcFile, O_RDONLY);
      if((ViFile<0)&&(VubInc!=Vu8EndUser)&&(VbDefault==FALSE))
      {
        Vu32ErrorCode|=2000+VubInc;
        fprintf(stderr, "open fails: %s\n",VcFile);
      }
      if(ViFile>=0)
      {
        if(VubInc==Vu8EndUser)
        {
          Vu32ErrorCode|=6000+VubInc;
          fprintf(stderr, "open success: %s\n",VcFile);
        }
        close(ViFile);
      }
      if(VbBank==TRUE)
      {/*check for end user bank extension 0000,0001,0002 ...*/
        for(VubIncBank=0;VubIncBank<4;VubIncBank++)
        {
          memset(VcFile,0,sizeof(VcFile));
          snprintf(VcFile,sizeof(VcFile),"%s%02x%02x.dat",PStrFile,VubInc,VubIncBank);
          //fprintf(stderr, "open fails: %s\n",VcFile);
          ViFile = open(VcFile, O_RDONLY);
          if((ViFile<0)&&(VubInc!=Vu8EndUser)&&(VbDefault==FALSE))
          {
            Vu32ErrorCode|=8000+VubInc;
            fprintf(stderr, "test %s\n",VcFile);
          }
          if(ViFile>=0)
          {
            if(VubInc==Vu8EndUser)
            {
              Vu32ErrorCode|=10000+VubInc;
              fprintf(stderr, "open success: %s\n",VcFile);
            }
            close(ViFile);
          }
        }
      }
    }
    return(Vu32ErrorCode);
}
#endif
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecord()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecord(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  tS32 VS32Size;
  tU8  Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantCodingRecord VmyElementRecord;

  memset(Vu8Record,0,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  //read record
  VS32Size=VmyElementRecord.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(VS32Size!=DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)
  {
    Vu32Return|=1;
  }
  else
  {/*check if record default */
    if(memcmp(&Vu8Record[0],&Vu8RecordKdsDefault[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
    {
      Vu32Return|=2;
    }
    else
    {/*check status*/
      if(VmyElementRecord.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
	    {
	      Vu32Return|=4;
      }
    }
  }
  #endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSRecordNoInit()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSRecordNoInit(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  tS32 VS32Size;
  tU8  Vu8Record[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE];
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantCodingRecord_NoInit VmyElementRecord;

  memset(Vu8Record,0,DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  //read record
  VS32Size=VmyElementRecord.s32GetData(&Vu8Record[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE);
  if(VS32Size!=DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)
  {
    Vu32Return|=1;
  }
  else
  {/*check if record default */
    if(memcmp(&Vu8Record[0],&Vu8RecordKdsDefault[0],DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE)!=0)
    {
      Vu32Return|=2;
    }
    else
    {/*check status*/
      if(VmyElementRecord.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
	    {
	      Vu32Return|=4;
      }
    }
  }
  #endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemGNSS()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemGNSS()
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantItemGNSS   VmyElementItem;
  tU8                                                    Vu8Value;
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32Return=1;
  }
  else
  {
    if(Vu8Value!=3)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSItemGNSSNoInit()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSItemGNSSNoInit()
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSVariantItemGNSS_NoInit   VmyElementItem;
  tU8                                                           Vu8Value;
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32Return=1;
  }
  else
  {
    if(Vu8Value!=3)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSWrongItem()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSWrongItem()
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementNoKDSVariantItemWrong       VmyElementItem;
  tU8                                                           Vu8Value;
  if(VmyElementItem.s32GetData(Vu8Value)!=DP_S32_NO_ERR)
  {
    Vu32Return=1;
  }
  else
  {
    if(Vu8Value!=22)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSu32()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSu32(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemAdcRangeMinTu32       VmyElementItem;
  tU32                                                                        Vu32Value;
  if(VmyElementItem.s32GetData(Vu32Value)!=DP_S32_NO_ERR)
  {
    Vu32Return=1;
  }
  else
  {
    if(Vu32Value!=0x4a4a4a4a)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSu16()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSu16(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemMinorVersionTu16       VmyElementItem;
  tU16                                                                         Vu16Value;
  if(VmyElementItem.s32GetData(Vu16Value)!=DP_S32_NO_ERR)
  {
    Vu32Return=1;
  }
  else
  {
    if(Vu16Value!=0x4a4a)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSWrongItem()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSstr(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementSensorConfigGyroItemTagTString      VmyElementItem;
  tChar                                                                 Vstr[10];
  if(VmyElementItem.s32GetData(&Vstr[0],sizeof(Vstr))==0)
  {
    Vu32Return=1;
  }
  else
  {
    if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetDefKDSWrongItem()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  27.11.2015
******************************************************************************/
tU32 u32DPGetDefKDSstrNoInit(void)
{
  tU32 Vu32Return=0;
  #ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
  dp_tclPddDpTestKdsDefaultTrElementKDSSensorConfigGyroItemTagNoInitTString      VmyElementItem;
  tChar                                                                          Vstr[10];
  if(VmyElementItem.s32GetData(&Vstr[0],sizeof(Vstr))==0)
  {
    Vu32Return=1;
  }
  else
  {
    if(strcmp("JJJJJJJJ",&Vstr[0])!=0)
    {
      Vu32Return=2;
    }
    else
    {/*check status*/
      if(VmyElementItem.u8GetElementStatus()!=DP_U8_ELEM_STATUS_INITVAL)
		  {
		    Vu32Return|=4;
		  }
    }
  }
  #endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPStoreAllEndUserPoolsIfConfigEndUserStored()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  03.08.2016
******************************************************************************/
tU32 u32DPStoreAllEndUserPoolsIfConfigEndUserStored(void)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSER
  dp_tclPddDpTestEndUserTestEndUsertU8           VmyTestElementU8;
  tU8                                            Vu8ValueSet;
  tU8                                            Vu8ValueGet;
  tU8                                            VubInc;

  //enable reload of shared memory
  DP_vEnableReloadEndUserSharedMemory();  
  /*set value for end user 0 to 9*/
  for(VubInc=0;VubInc<10;VubInc++)
  { /*set end user VubInc*/
    Vu8ValueSet=50;
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    /*store element*/
    Vu8ValueSet+=VubInc;
    if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=(10+VubInc);
    }
  }  
  if(Vu32ErrorCode==0)
  { /*set end user and read value value*/
    for(VubInc=0;VubInc<10;VubInc++)
    { /*set end user VubInc*/
      Vu32ErrorCode|=u32DPSetEndUser(VubInc);
      /*read element*/
      if(VmyTestElementU8.s32GetData(Vu8ValueGet)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=(8000+VubInc);
      }
      else
      {
        Vu8ValueSet=50+VubInc; 
        /*end user 1 should not be stored*/
        if(VubInc==1)
        {
          if(Vu8ValueGet==Vu8ValueSet)
          {
            Vu32ErrorCode|=1000;
          }
        }
        else if(Vu8ValueGet!=Vu8ValueSet)
        {
           Vu32ErrorCode|=(2000+VubInc);
           //fprintf(stderr, "Vu8ValueSet: %d, Vu8ValueGet: %d\n",Vu8ValueSet,Vu8ValueGet);
        }
      }
    }
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetBankAllEndUserPoolsIfConfigEndUserStored()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  05.08.2016
******************************************************************************/
tU32 u32DPSetBankAllEndUserPoolsIfConfigEndUserStored(void)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8          VmyTestElementU8;
  tU8                                                   Vu8ValueSet;
  tU8                                                   VubInc;
  tU8                                                   VubIncBank;

  /* !!! end user 0 should not be stored   !!!*/
  //enable reload of shared memory
  DP_vEnableReloadEndUserSharedMemory();  
  /*set value for end user 0 to 5*/
  for(VubInc=0;VubInc<5;VubInc++)
  { /*set end user VubInc*/
    Vu8ValueSet=50;
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    for(VubIncBank=0;VubIncBank<5;VubIncBank++)
    {
      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(VubIncBank+1);
      /*store element*/
      Vu8ValueSet+=VubInc;
      if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=(10+VubInc);
      }
      /*save to */
      Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(VubIncBank);
    }
  }  
  /* !!! check end user 0 not stored   !!!*/
  if(Vu32ErrorCode==0)
  { 
    Vu32ErrorCode=u32OEDTDPCheckFileStored("/var/opt/bosch/dynamic/pdd/datapool/effd/PddDpTestEndUserBank",0,FALSE,TRUE);    
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		u32DPSetDefaultEndUserPoolsIfConfigEndUserStored()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  05.08.2016
******************************************************************************/
tU32 u32DPSetDefaultEndUserPoolsIfConfigEndUserStored(void)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK
  dp_tclPddDpTestEndUserBankTestEndUserBanktU8          VmyTestElementU8;
  tU8                                                   Vu8ValueSet;
  tU8                                                   VubInc;
  tU8                                                   VubIncBank;

  /* !!! end user 2 should not be stored   !!!*/
  //enable reload of shared memory
  DP_vEnableReloadEndUserSharedMemory();  
  /*set value for end user 0 to 5*/
  for(VubInc=0;VubInc<5;VubInc++)
  { /*set end user VubInc*/
    Vu8ValueSet=50;
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    for(VubIncBank=0;VubIncBank<5;VubIncBank++)
    {
      Vu32ErrorCode|=u32OEDTDPLoadBankToCurrentEndUser(VubIncBank+1);
      /*store element*/
      Vu8ValueSet+=VubInc;
      if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
      {
        Vu32ErrorCode|=(10+VubInc);
      }
      /*save to */
      Vu32ErrorCode|=u32OEDTDPSaveCurrentEndUserToBank(VubIncBank);
    }
  }  
  /*set end user default*/
  for(VubInc=0;VubInc<5;VubInc++)
  {
     Vu32ErrorCode|=u32DPSetEndUserDefault(VubInc);
  }
  /* !!! check end user 2 not stored   !!!*/
  if(Vu32ErrorCode==0)
  { 
    Vu32ErrorCode=u32OEDTDPCheckFileStored("/var/opt/bosch/dynamic/pdd/datapool/effd/PddDpTestEndUserBank",2,TRUE,TRUE);    
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}

/*****************************************************************************
* FUNCTION:		u32DPCopyUserIfConfigEndUserStored()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  10.08.2016
******************************************************************************/
tU32 u32DPCopyUserIfConfigEndUserStored(void)
{
  tU32                 Vu32ErrorCode=0;
  #ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  #ifdef DP_U32_POOL_ID_PDDDPTESTENDUSERBANK
  dp_tclPddDpTestEndUserTestEndUsertU8          VmyTestElementU8;
  tU8                                           Vu8ValueSet;
  tU8                                           VubInc;

  /* !!! end user 3 should not be stored   !!!*/
  //enable reload of shared memory
  DP_vEnableReloadEndUserSharedMemory();  
  /*set value for end user 0 to 5*/
  for(VubInc=0;VubInc<5;VubInc++)
  { /*set end user VubInc*/
    Vu8ValueSet=0x50+VubInc;
    Vu32ErrorCode|=u32DPSetEndUser(VubInc);
    if(VmyTestElementU8.s32SetData(Vu8ValueSet)!=DP_S32_NO_ERR)
    {
      Vu32ErrorCode|=(10+VubInc);
    }      
  }  
  /*try to copy all to end user 3*/
  if(Vu32ErrorCode==0)
  { 
    for(VubInc=0;VubInc<5;VubInc++)
    {
       if(s32DPCopyEndUser(VubInc,3)!=DP_S32_NO_ERR)
       {
         Vu32ErrorCode|=(20+VubInc);
       }
    }
  }
  /* !!! check end user 3 not stored   !!!*/
  if(Vu32ErrorCode==0)
  { 
    Vu32ErrorCode=u32OEDTDPCheckFileStored("/var/opt/bosch/dynamic/pdd/datapool/effd/PddDpTestEndUser",3,FALSE,FALSE);    
  }
  #endif
  #endif
  return(Vu32ErrorCode);
}
/*****************************************************************************
* FUNCTION:		main()
* PARAMETER:    Option:
*               -d: delete element: -d[elementname]
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created  13.06.2014
******************************************************************************/
int main(int argc, char **argv)
{
   tU32                Vu32ReturnValue = 0;
   OSAL_tMQueueHandle  VmqHandleOEDTProcess = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle  VmqHandleTestProcess = OSAL_C_INVALID_HANDLE;
   TPoolMsg            VtMsg;
   tBool               VbEnd=FALSE;

   ET_TRACE_OPEN; 
   //for lint
   argc=(int) argv;
   //set init callback
   vSetInitCallback_EFFD();
   // Open MQ 
   //fprintf(stderr, "main() 3:start \n");
   if (OSAL_s32MessageQueueOpen(MQ_DP_TEST_PROCESS3_NAME,OSAL_EN_READWRITE, &VmqHandleTestProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   // Open MQ 
   if (OSAL_s32MessageQueueOpen(MQ_DP_OEDT_PROCESS3_NAME,OSAL_EN_READWRITE, &VmqHandleOEDTProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   if(Vu32ReturnValue!=0)
   {
     if ((VmqHandleTestProcess == OSAL_C_INVALID_HANDLE)||(VmqHandleOEDTProcess == OSAL_C_INVALID_HANDLE ))
	   {
	     Vu32ReturnValue=200000000;
	   }
   }
    //fprintf(stderr, "main() 3:start loop Vu32ReturnValue %d\n",Vu32ReturnValue);
   if (Vu32ReturnValue == 0)
   { // run test
	   tU32 u32Prio;
	   while(VbEnd==FALSE)
	   {
	     //fprintf(stderr, "main() 3:start wait \n");
	     if(OSAL_s32MessageQueueWait(VmqHandleTestProcess, (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	     {
         //fprintf(stderr, "main() 3:error wait message queue\n");
	       Vu32ReturnValue = 300000000;
		     VbEnd=TRUE;
	     }
	     else
	     {
	       //fprintf(stderr, "main() 3:wait done %d \n",VtMsg.eCmd);
         switch(VtMsg.eCmd)
		     {		   		      
		       case eCmdGetDefKDSRecord:
           {
             Vu32ReturnValue=u32DPGetDefKDSRecord();
           }break;
           case eCmdGetDefKDSRecordNoInit:
           {
             Vu32ReturnValue=u32DPGetDefKDSRecordNoInit();
           }break;
           case eCmdGetDefKDSItemGNSS:
           {
             Vu32ReturnValue=u32DPGetDefKDSItemGNSS();
           }break;
           case eCmdGetDefKDSItemGNSSNoInit:
           {
             Vu32ReturnValue=u32DPGetDefKDSItemGNSSNoInit();
           }break;
           case eCmdGetDefKDSWrongItem:
           {
             Vu32ReturnValue=u32DPGetDefKDSWrongItem();
           }break;
           case eCmdGetDefKDSu32:
           {
             Vu32ReturnValue=u32DPGetDefKDSu32();
           }break;
           case eCmdGetDefKDSu16:
           {
             Vu32ReturnValue=u32DPGetDefKDSu16();
           }break;
           case eCmdGetDefKDSstr:
           {
             Vu32ReturnValue=u32DPGetDefKDSstr();
           }break;
           case eCmdGetDefKDSstrNoInit:
           {
             Vu32ReturnValue=u32DPGetDefKDSstrNoInit();
           }break;
           case eCmdConfigSetUserDpEndUserNotStored:
           {
             //fprintf(stderr, "cmd: eCmdConfigSetUserDpEndUserNotStored, \n");
             Vu32ReturnValue=u32DPStoreAllEndUserPoolsIfConfigEndUserStored();     
           }break;
           case eCmdConfigSetBankDpEndUserNotStored:
           {
             //fprintf(stderr, "cmd: eCmdConfigSetBankDpEndUserNotStored, \n");
             Vu32ReturnValue=u32DPSetBankAllEndUserPoolsIfConfigEndUserStored();     
           }break;
           case eCmdConfigSetDefaultDpEndUserNotStored:
           {
             //fprintf(stderr, "cmd: eCmdConfigSetDefaultDpEndUserNotStored, \n");
             Vu32ReturnValue=u32DPSetDefaultEndUserPoolsIfConfigEndUserStored();     
           }break;
           case eCmdConfigCopyUserDpEndUserNotStored:
           {
             //fprintf(stderr, "cmd: eCmdConfigCopyUserDpEndUserNotStored, \n");
             Vu32ReturnValue=u32DPCopyUserIfConfigEndUserStored();     
           }break;
           case eCmdProcess3End:
		       {
			       //fprintf(stderr, "cmd: eCmdProcessEnd \n");
		         VbEnd=TRUE;
		       }break;
		       case eCmdResponse:
		         //fprintf(stderr, "cmd: eCmdResponse \n");
		       break;
		       default:
		       {
		         Vu32ReturnValue=400000000;
		       }break;
         }/*end switch */
         if(Vu32ReturnValue!=0)     fprintf(stderr, "main() 3:error code %d \n", Vu32ReturnValue);
         // send result to OEDT
         VtMsg.eCmd=eCmdResponse;
         VtMsg.u.u32ErrorCode=Vu32ReturnValue;
		     //fprintf(stderr, "main() 3:post message %d \n",VtMsg.eCmd);
         if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
         {
           Vu32ReturnValue = 500000000;
         }
		     else
		     {
		       Vu32ReturnValue = 0;
		     }
	     }
     }/*end while*/
   }
   //fprintf(stderr, "test end \n");
   if(Vu32ReturnValue!=0)
   {//post error
     VtMsg.eCmd=eCmdResponse;
     VtMsg.u.u32ErrorCode=Vu32ReturnValue;
     if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
       Vu32ReturnValue = 600000000;
     }
   }
   // Close MQs
   if (VmqHandleOEDTProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleOEDTProcess);
   }
   if (VmqHandleTestProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleTestProcess);
   }
   OSAL_vProcessExit();
   _exit(Vu32ReturnValue);
}

/************************ End of file ********************************/




