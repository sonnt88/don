/*****************************************************************************
* FILE         : process5.cpp
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 5 
*              
* AUTHOR(s)    : Andrea Bueter (BSOT)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 07.06.2016    | version 0.1               | Andrea Bueter (BSOT)
*               |                           | Initial version
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DP_S_IMPORT_INTERFACE_KDS_FI
#include "dp_kds_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "oedt_DataPool_TestFuncs.h"

/*****************************************************************************
* FUNCTION:		u32DPGetKDSValue()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 07.06.2016
******************************************************************************/
tU32 u32DPGetKDSValue(void)
{
  tU32 Vu32Return=0;
  #ifdef DP_U16_KDSADR_SWUPD_VARIANTINFO
  tU8   Vu8Buffer[DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE]={0};
  tS32  Vs32ReturnCode=DP_s32GetConfigItem("Wrong1","Wrong2",Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT)
  {
    Vu32Return=1;
  }
  Vs32ReturnCode=DP_s32GetConfigItem(NULL,NULL,Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE);
  if(Vs32ReturnCode!=DP_S32_ERR_WRONG_PARAM)
  {
    Vu32Return|=2;
  }
  Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","Wrong2",Vu8Buffer,DP_U8_KDSLEN_SWUPD_VARIANTINFO_COMPLETE,FALSE);
  if(Vs32ReturnCode!=DP_S32_ERR_NO_ELEMENT)
  {
    Vu32Return|=4;
  }
  Vs32ReturnCode=DP_s32GetConfigItem("SWUPD_VariantInfo","SoftwareVariantTag",Vu8Buffer,4);
  if(Vs32ReturnCode==DP_S32_ERR_NO_ELEMENT)
  {
    Vu32Return|=6;
  }  
  #endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		main()
* PARAMETER:    Option:
*               -d: delete element: -d[elementname]
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 07.06.2016
******************************************************************************/
int main(int argc, char **argv)
{
   tU32                Vu32ReturnValue = 0;
   OSAL_tMQueueHandle  VmqHandleOEDTProcess = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle  VmqHandleTestProcess = OSAL_C_INVALID_HANDLE;
   TPoolMsg            VtMsg;
   tBool               VbEnd=FALSE;

   ET_TRACE_OPEN; 
   //for lint
   argc=(int) argv;
   // Open MQ 
   //fprintf(stderr, "main() 5:start \n");
   if (OSAL_s32MessageQueueOpen(MQ_DP_TEST_PROCESS5_NAME,OSAL_EN_READWRITE, &VmqHandleTestProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   // Open MQ 
   if (OSAL_s32MessageQueueOpen(MQ_DP_OEDT_PROCESS5_NAME,OSAL_EN_READWRITE, &VmqHandleOEDTProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   if(Vu32ReturnValue!=0)
   {
     if ((VmqHandleTestProcess == OSAL_C_INVALID_HANDLE)||(VmqHandleOEDTProcess == OSAL_C_INVALID_HANDLE ))
	   {
	     Vu32ReturnValue=200000000;
	   }
   }
    //fprintf(stderr, "main() 5:start loop Vu32ReturnValue %d\n",Vu32ReturnValue);
   if (Vu32ReturnValue == 0)
   { // run test
	   tU32 u32Prio;
	   while(VbEnd==FALSE)
	   {
	     //fprintf(stderr, "main() 5:start wait \n");
	     if(OSAL_s32MessageQueueWait(VmqHandleTestProcess, (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	     {
         //fprintf(stderr, "main() 5:error wait message queue\n");
	       Vu32ReturnValue = 300000000;
		     VbEnd=TRUE;
	     }
	     else
	     {
         //fprintf(stderr, "main() 5:wait done %d \n",VtMsg.eCmd);
         switch(VtMsg.eCmd)
		     {		   		      
           case eCmdGetConfigItemWithoutDatapool:
           {
             //fprintf(stderr, "cmd: eCmdGetConfigItemWithoutDatapool \n");
             Vu32ReturnValue=u32DPGetKDSValue();
             //fprintf(stderr, "cmd: eCmdGetConfigItemWithoutDatapool %d \n",Vu32ReturnValue);
           }break;
           case eCmdProcess5End:
		       {
			       //fprintf(stderr, "cmd: eCmdProcessEnd \n");
		         VbEnd=TRUE;
		       }break;
		       case eCmdResponse:
		         //fprintf(stderr, "cmd: eCmdResponse \n");
		       break;
		       default:
		       {
		         Vu32ReturnValue=400000000;
		       }break;
         }/*end switch */
         if(Vu32ReturnValue!=0)     fprintf(stderr, "main() 5:error code %d \n", Vu32ReturnValue);
         // send result to OEDT
         VtMsg.eCmd=eCmdResponse;
         VtMsg.u.u32ErrorCode=Vu32ReturnValue;
		     //fprintf(stderr, "main() 5:post message %d \n",VtMsg.eCmd);
         if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
         {
           Vu32ReturnValue = 500000000;
         }
		     else
		     {
		       Vu32ReturnValue = 0;
		     }
	     }
     }/*end while*/
   }
   //fprintf(stderr, "test end \n");
   if(Vu32ReturnValue!=0)
   {//post error
     VtMsg.eCmd=eCmdResponse;
     VtMsg.u.u32ErrorCode=Vu32ReturnValue;
     if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
       Vu32ReturnValue = 600000000;
     }
   }
   // Close MQs
   if (VmqHandleOEDTProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleOEDTProcess);
   }
   if (VmqHandleTestProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleTestProcess);
   }
   OSAL_vProcessExit();
   _exit(Vu32ReturnValue);
}

/************************ End of file ********************************/




