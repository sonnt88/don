/*****************************************************************************
* FILE         : process1.cpp
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 1 for multi-process testing of Datapool
*              
* AUTHOR(s)    : Andrea Bueter (BSOT)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 13.06.2014    | version 0.1               | Andrea Bueter (BSOT)
* 22.09.2015    | version 0.2               | Andrea Bueter (BSOT)
*               |                           | Initial version
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osal_public.h"

#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"

#ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_spm_if.h"
#endif

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "oedt_DataPool_TestFuncs.h"


#ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
#define SRAM_THREAD_0 "SRAM_THREAD_0"
#define SRAM_THREAD_1 "SRAM_THREAD_1"
#define SRAM_THREAD_2 "SRAM_THREAD_2"
static tU32      vu32ErrorSRamAccess;
#endif

/*****************************************************************************
* FUNCTION:		u32DPInitUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 17.06.2014
******************************************************************************/
tU32 u32DPInitUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  dp_tclBaseElement    VmyDpElem1(
	    PcpElement,	                                 //pcElementName 
	    (tU8) 0x01,                                  //u8Version
	    sizeof(tU8),                                 //u16ElementLength
      TRUE,			                                   //bVarSize
      0x0666,							 		                     //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                             //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  
  if(DP_s32InitUndefElement(VcUserName,PcpPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1)!=DP_S32_ERR_ACCESS_RIGHT)
  {
    Vu32Return|=10000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPDeleteUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 13.06.2014
******************************************************************************/
tU32 u32DPDeleteUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  if(DP_s32DeleteUndefElement(VcUserName,PcpPoolName,PcpElement)!=DP_S32_NO_ERR)
  {
    Vu32Return|=10000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPSetUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 16.06.2014
******************************************************************************/
tU32 u32DPSetUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;
#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  tU8                  Vu8Write=0x22;

  if(DP_s32SetUndefElement(VcUserName,PcpPoolName,PcpElement,&Vu8Write,sizeof(Vu8Write))<DP_S32_NO_ERR)
  {
    Vu32Return|=10000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 16.06.2014
******************************************************************************/
tU32 u32DPGetUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;

#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  tU32                 Vu32Read;
  if(DP_s32GetUndefElement(VcUserName,PcpPoolName,PcpElement,(tU8*)&Vu32Read,sizeof(tU32))<DP_S32_NO_ERR)
  {
    Vu32Return|=10000000;
  }
  else
  {
    if(Vu32Read!=DP_TEST_VALUE_WRITE_GET_UNDEF_ELEMENT_SHARED)
	  {
	    Vu32Return|=20000000;
	  }
  } 
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPGetUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 16.06.2014
******************************************************************************/
tU32 u32DPAccessDeleteUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;

#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  tU32                 Vu32Read;
  if(DP_s32GetUndefElement(VcUserName,PcpPoolName,PcpElement,(tU8*)&Vu32Read,sizeof(tU32))>=DP_S32_NO_ERR)
  {
    Vu32Return|=10000000;
  }
  if(DP_s32DeleteUndefElement(VcUserName,PcpPoolName,PcpElement)==DP_S32_NO_ERR)
  {
    Vu32Return|=40000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPGetUndefElementShared()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 16.06.2014
******************************************************************************/
tU32 u32DPInitDeleteUndefElementShared(char* PcpElement, char* PcpPoolName)
{
  tU32 Vu32Return=0;

#ifdef DP_FEATURE_UNDEF_ELEMENT
  const char*          VcUserName="TestPerson4";
  dp_tclBaseElement    VmyDpElem1(
	    PcpElement,	                                 //pcElementName 
	    (tU8) 0x01,    				                       //u8Version
	     sizeof(tU8),					                       //u16ElementLength
      TRUE,						                             //bVarSize
      0x0666,							 		                     //u16AccessType														
      dp_tclBaseElement::eDpElementTypePersistent, //tElemType
      dp_tclBaseElement::eDpStoringTypeImmediately,//tStoringType
      0,							                             //u32StoringIntervall
      DP_DEFSET_USER);                             //u32DefSetMode    
  //set and get element
  tS32 Vs32Return=DP_s32InitUndefElement(VcUserName,PcpPoolName,eDpLocation_FILE_SYSTEM,&VmyDpElem1);
  if(Vs32Return!=DP_S32_NO_ERR)
  {
    Vu32Return|=10000000;
  }
  //delete element
  Vs32Return=DP_s32DeleteUndefElement(VcUserName,PcpPoolName,PcpElement);
  if(Vs32Return!=DP_S32_NO_ERR)
  {
    Vu32Return|=20000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpElement);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PcpPoolName);
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		vThreadForMultipleSramAccess0()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 22.09.2015
******************************************************************************/
tVoid vThreadForMultipleSramAccess0( void)
{
  #ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  int ViCounter;
  dp_tclSpmDpInternDataContinuousResets        oDataContinuousResets;
  for(ViCounter=0;ViCounter < 10;ViCounter++)
  {
    oDataContinuousResets.vSetData(0x11111111);
	  //fprintf(stderr, "vThreadForMultipleSramAccess0(): set 0x11111111 \n");
	  sleep(1);
  }
  OSAL_vThreadExit();
  #endif
}

/*****************************************************************************
* FUNCTION:		vThreadForMultipleSramAccess1()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 22.09.2015
******************************************************************************/
tVoid vThreadForMultipleSramAccess1(void)
{
  #ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  int ViCounter;
  dp_tclSpmDpInternDataContinuousResets        oDataContinuousResets;
  for(ViCounter=0;ViCounter < 10;ViCounter++)
  {
    oDataContinuousResets.vSetData(0x55555555);
	  //fprintf(stderr, "vThreadForMultipleSramAccess1(): set 0x55555555 \n");
	  sleep(1);
  }
  OSAL_vThreadExit();
  #endif
}

/*****************************************************************************
* FUNCTION:		vThreadForMultipleSramAccess2()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 22.09.2015
******************************************************************************/
tVoid vThreadForMultipleSramAccess2(void)
{
  #ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  int ViCounter;
  dp_tclSpmDpInternDataContinuousResets        oDataContinuousResets;
  tU32  Vu32ContinuousResets;
  for(ViCounter=0;ViCounter < 10;ViCounter++)
  { //read 0x11111111 or 0x55555555
	  Vu32ContinuousResets=0;
	  oDataContinuousResets.u8GetData(Vu32ContinuousResets);
	  if((Vu32ContinuousResets!=0x11111111)&&(Vu32ContinuousResets!=0x55555555))
	  {
	    fprintf(stderr, "vThreadForMultipleSramAccess2(): read not correct value 0x%x\n",Vu32ContinuousResets);
	    vu32ErrorSRamAccess|=(tU32)(ViCounter<100);
	  }
    sleep(1);
  }
  OSAL_vThreadExit();
  #endif
}

/*****************************************************************************
* FUNCTION:		u32DPStarts3ThreadsWithMultipleSRAMAccess()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 22.09.2015
******************************************************************************/
tU32 u32DPStarts3ThreadsWithMultipleSRAMAccess(void)
{
  tU32 Vu32Return=0;
  #ifdef DP_U32_POOL_ID_SPMDPINTERNDATA
  int ViCounter;
  dp_tclSpmDpInternDataContinuousResets        oDataContinuousResets;
  OSAL_trThreadAttribute                       VrAttr;
  OSAL_tThreadID                               VhThreadId[3];

  vu32ErrorSRamAccess=0;
  oDataContinuousResets.vSetData(0x11111111);

  VrAttr.szName = SRAM_THREAD_0;
  VrAttr.s32StackSize = 5000;
  VrAttr.u32Priority = 50;
  VrAttr.pfEntry = (OSAL_tpfThreadEntry) vThreadForMultipleSramAccess0;
  VrAttr.pvArg = (tPVoid) NULL;
  VhThreadId[0] = OSAL_ThreadSpawn(&VrAttr);
  if (VhThreadId[0] == OSAL_ERROR ) 
  {
    Vu32Return|=1;
	  fprintf(stderr, "u32DPStarts3ThreadsWithMultipleSRAMAccess(): OSAL_ThreadSpawn fails 'SRAM_THREAD_0' \n");
  }

  VrAttr.szName = SRAM_THREAD_1;
  VrAttr.pfEntry = (OSAL_tpfThreadEntry) vThreadForMultipleSramAccess1;
  VrAttr.pvArg = (tPVoid) NULL;
  VhThreadId[1] = OSAL_ThreadSpawn(&VrAttr);
  if (VhThreadId[1] == OSAL_ERROR ) 
  {
    Vu32Return|=2;
	  fprintf(stderr, "u32DPStarts3ThreadsWithMultipleSRAMAccess(): OSAL_ThreadSpawn fails 'SRAM_THREAD_1' \n");
  }

  VrAttr.szName = SRAM_THREAD_2;
  VrAttr.pfEntry = (OSAL_tpfThreadEntry) vThreadForMultipleSramAccess2;
  VrAttr.pvArg = (tPVoid) NULL;
  VhThreadId[2] = OSAL_ThreadSpawn(&VrAttr);
  if (VhThreadId[2] == OSAL_ERROR ) 
  {
    Vu32Return|=4;
	  fprintf(stderr, "u32DPStarts3ThreadsWithMultipleSRAMAccess(): OSAL_ThreadSpawn fails 'SRAM_THREAD_2' \n");
  }
  
  for (ViCounter=0;ViCounter<3;ViCounter++)
  {
	  if(VhThreadId[ViCounter]!=OSAL_ERROR)
	  {
      if(OSAL_s32ThreadJoin(VhThreadId[ViCounter] ,OSAL_C_TIMEOUT_FOREVER)!=OSAL_OK)
	    {
	      //Vu32Return|=100+ViCounter;
	      //fprintf(stderr, "u32DPStarts3ThreadsWithMultipleSRAMAccess(): OSAL_s32ThreadJoin fails\n");
	    }
	  }
  }
  Vu32Return|=vu32ErrorSRamAccess;
  #endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		main()
* PARAMETER:    Option:
*               -d: delete element: -d[elementname]
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 13.06.2014
******************************************************************************/
int main(int argc, char **argv)
{
   tU32                Vu32ReturnValue = 0;
   OSAL_tMQueueHandle  VmqHandleOEDTProcess = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle  VmqHandleTestProcess = OSAL_C_INVALID_HANDLE;
   TPoolMsg            VtMsg;
   tBool               VbEnd=FALSE;

   ET_TRACE_OPEN; 
   //for lint
   argc=(int) argv;
   // Open MQ 
   //fprintf(stderr, "main() 1:start \n");
   if (OSAL_s32MessageQueueOpen(MQ_DP_TEST_PROCESS_NAME,OSAL_EN_READWRITE, &VmqHandleTestProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   // Open MQ 
   if (OSAL_s32MessageQueueOpen(MQ_DP_OEDT_PROCESS_NAME,OSAL_EN_READWRITE, &VmqHandleOEDTProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   if(Vu32ReturnValue!=0)
   {
     if ((VmqHandleTestProcess == OSAL_C_INVALID_HANDLE)||(VmqHandleOEDTProcess == OSAL_C_INVALID_HANDLE ))
	 {
	   Vu32ReturnValue=200000000;
	 }
   }
   //fprintf(stderr, "main() 1:start loop Vu32ReturnValue %d\n",Vu32ReturnValue);
   if (Vu32ReturnValue == 0)
   { // run test
	   tU32 u32Prio;
	   while(VbEnd==FALSE)
	   {
	     //fprintf(stderr, "main() 1:start wait \n");
	     if(OSAL_s32MessageQueueWait(VmqHandleTestProcess, (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	     {
         //fprintf(stderr, "main() 1:error wait message queue\n");
	       Vu32ReturnValue = 300000000;
		     VbEnd=TRUE;
	     }
	     else
	     {
	       //fprintf(stderr, "main() 1:wait done %d \n",VtMsg.eCmd);
         switch(VtMsg.eCmd)
		     {		   
		       case eCmdInit:
		       {
		         //fprintf(stderr, "cmd: eCmdInit %s\n",VtMsg.u.tShared.strElement);
             Vu32ReturnValue=u32DPInitUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdDelete:
		       {
			       //fprintf(stderr, "cmd: eCmdDelete %s\n",VtMsg.u.tShared.strElement);
             Vu32ReturnValue=u32DPDeleteUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdSet:
		       {
			       //fprintf(stderr, "cmd: eCmdSet %s\n",VtMsg.u.tShared.strElement);
             Vu32ReturnValue=u32DPSetUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdGet:
           {
			       //fprintf(stderr, "cmd: eCmdGet %s\n",VtMsg.u.tShared.strElement);
             Vu32ReturnValue=u32DPGetUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdAccessDeleteElement:
		       {
		         //fprintf(stderr, "cmd: eCmdAccessDeleteElement %s\n",VtMsg.u.tShared.strElement);
			       Vu32ReturnValue=u32DPAccessDeleteUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdInitDeleteElement:
		       { 
			       //fprintf(stderr, "cmd: eCmdInitDeleteElement %s\n",VtMsg.u.tShared.strElement);
			       Vu32ReturnValue=u32DPInitDeleteUndefElementShared(VtMsg.u.tShared.strElement,VtMsg.u.tShared.strPool);
		       }break;
		       case eCmdSramAccessMultiThreads:
		       {
		         //fprintf(stderr, "cmd: eCmdSramAccessMultiThreads\n");
			       Vu32ReturnValue=u32DPStarts3ThreadsWithMultipleSRAMAccess();
		       }break;          
		       case eCmdProcess1End:
		       {
			       //fprintf(stderr, "cmd: eCmdProcessEnd \n");
		         VbEnd=TRUE;
		       }break;
		       case eCmdResponse:
		         //fprintf(stderr, "cmd: eCmdResponse \n");
		       break;
		       default:
		       {
		         Vu32ReturnValue=400000000;
		       }break;
         }/*end switch */
         if(Vu32ReturnValue!=0)     fprintf(stderr, "main() 1:error code %d \n", Vu32ReturnValue);
         // send result to OEDT
         VtMsg.eCmd=eCmdResponse;
         VtMsg.u.u32ErrorCode=Vu32ReturnValue;
		     //fprintf(stderr, "main() 1:post message %d \n",VtMsg.eCmd);
         if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
         {
           Vu32ReturnValue = 500000000;
         }
		     else
		     {
		       Vu32ReturnValue = 0;
		     }
	     }
     }/*end while*/
   }
   //fprintf(stderr, "test end \n");
   if(Vu32ReturnValue!=0)
   {//post error
     VtMsg.eCmd=eCmdResponse;
     VtMsg.u.u32ErrorCode=Vu32ReturnValue;
     if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
       Vu32ReturnValue = 600000000;
     }
   }
   // Close MQs
   if (VmqHandleOEDTProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleOEDTProcess);
   }
   if (VmqHandleTestProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleTestProcess);
   }
   OSAL_vProcessExit();
   _exit(Vu32ReturnValue);
}

/************************ End of file ********************************/




