/*****************************************************************************
* FILE         : process4.cpp
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 4 
*              
* AUTHOR(s)    : Andrea Bueter (BSOT)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 13.01.2016    | version 0.1               | Andrea Bueter (BSOT)
*               |                           | Initial version
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "oedt_DataPool_TestFuncs.h"


/*****************************************************************************
* FUNCTION:		main()
* PARAMETER:    Option:
*               -d: delete element: -d[elementname]
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 13.01.2016
******************************************************************************/
int main(int argc, char **argv)
{
   tU32                Vu32ReturnValue = 0;
   OSAL_tMQueueHandle  VmqHandleOEDTProcess = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle  VmqHandleTestProcess = OSAL_C_INVALID_HANDLE;
   TPoolMsg            VtMsg;
   tBool               VbEnd=FALSE;

   ET_TRACE_OPEN; 
   //for lint
   argc=(int) argv;
   // Open MQ 
   //fprintf(stderr, "main() 4:start \n");
   if (OSAL_s32MessageQueueOpen(MQ_DP_TEST_PROCESS4_NAME,OSAL_EN_READWRITE, &VmqHandleTestProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   // Open MQ 
   if (OSAL_s32MessageQueueOpen(MQ_DP_OEDT_PROCESS4_NAME,OSAL_EN_READWRITE, &VmqHandleOEDTProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   if(Vu32ReturnValue!=0)
   {
     if ((VmqHandleTestProcess == OSAL_C_INVALID_HANDLE)||(VmqHandleOEDTProcess == OSAL_C_INVALID_HANDLE ))
	   {
	     Vu32ReturnValue=200000000;
	   }
   }
    //fprintf(stderr, "main() 4:start loop Vu32ReturnValue %d\n",Vu32ReturnValue);
   if (Vu32ReturnValue == 0)
   { // run test
	   tU32 u32Prio;
	   while(VbEnd==FALSE)
	   {
	     //fprintf(stderr, "main() 4:start wait \n");
	     if(OSAL_s32MessageQueueWait(VmqHandleTestProcess, (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	     {
         //fprintf(stderr, "main() 4:error wait message queue\n");
	       Vu32ReturnValue = 300000000;
		     VbEnd=TRUE;
	     }
	     else
	     {
	       //fprintf(stderr, "main() 4:wait done %d \n",VtMsg.eCmd);
         switch(VtMsg.eCmd)
		     {		   	
           case eCmdProcess4End:
            //fprintf(stderr, "cmd: eCmdProcessEnd \n");
		         VbEnd=TRUE;
           break;
		       default:
		       {
		         Vu32ReturnValue=400000000;
		       }break;
         }/*end switch */
         if(Vu32ReturnValue!=0)     fprintf(stderr, "main() 4:error code %d \n", Vu32ReturnValue);
         // send result to OEDT
         VtMsg.eCmd=eCmdResponse;
         VtMsg.u.u32ErrorCode=Vu32ReturnValue;
		     //fprintf(stderr, "main() 4:post message %d \n",VtMsg.eCmd);
         if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
         {
           Vu32ReturnValue = 500000000;
         }
		     else
		     {
		       Vu32ReturnValue = 0;
		     }
	     }
     }/*end while*/
   }
   //fprintf(stderr, "test end \n");
   if(Vu32ReturnValue!=0)
   {//post error
     VtMsg.eCmd=eCmdResponse;
     VtMsg.u.u32ErrorCode=Vu32ReturnValue;
     if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
       Vu32ReturnValue = 600000000;
     }
   }
   // Close MQs
   if (VmqHandleOEDTProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleOEDTProcess);
   }
   if (VmqHandleTestProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleTestProcess);
   }
   OSAL_vProcessExit();
   _exit(Vu32ReturnValue);
}

/************************ End of file ********************************/




