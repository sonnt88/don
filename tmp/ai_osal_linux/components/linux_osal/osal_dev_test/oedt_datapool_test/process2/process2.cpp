/*****************************************************************************
* FILE         : process2.cpp
*
* SW-COMPONENT : OEDT_FrmWrk
*
* DESCRIPTION  : Process 2 for multi-process testing of Datapool 
*                This process simulate the application, which could change the end user
*              
* AUTHOR(s)    : Andrea Bueter (BSOT)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |                           | Author & comments
* --.--.--      | Initial revision          | ------------
*-----------------------------------------------------------------------------
* 28.01.2015    | version 0.1               | Andrea Bueter (BSOT)
*               |                           | Initial version
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"

#include "oedt_DataPool_TestFuncs.h"

/*****************************************************************************
* FUNCTION:		u32DPEndUserSetEndUserWithLock()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUserSetEndUserWithLock(tU8 Pu8EndUser)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  if(dpSrvIf.s32Lock(DP_U32_LOCK_MODE_END_USER)== DP_S32_NO_ERR)
  {
    if(dpSrvIf.s32SetEndUser(Pu8EndUser)!= DP_S32_NO_ERR)
    {/*error*/ 
	    Vu32Return=10000000;
    }
    if(dpSrvIf.s32Unlock()!= DP_S32_NO_ERR)
    {/*error*/
	    Vu32Return|=20000000;
    }
  }
  else
  {/*error*/ 
    Vu32Return|=30000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu8EndUser);
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserLock()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUserLock(void)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  if(dpSrvIf.s32Lock(DP_U32_LOCK_MODE_END_USER)!= DP_S32_NO_ERR)
  {
    Vu32Return|=30000000;
  }
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUserUnlock()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUserUnlock(void)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  if(dpSrvIf.s32Unlock()!= DP_S32_NO_ERR)
  {/*error*/
    Vu32Return|=20000000;
  }
#endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserSetEndUser()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUserSetEndUser(tU8 Pu8EndUser)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  if(dpSrvIf.s32SetEndUser(Pu8EndUser)!= DP_S32_NO_ERR)
  {/*error*/ 
    Vu32Return=10000000;
  } 
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu8EndUser);
#endif
  return(Vu32Return);
}
/*****************************************************************************
* FUNCTION:		u32DPEndUserGetEndUser()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUserGetEndUser(void)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  tU8         u8EndUser;
  if(dpSrvIf.s32GetEndUser(u8EndUser)!= DP_S32_NO_ERR)
  {/*error*/
    Vu32Return=10000000;
  }
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		u32DPEndUsersSetEndUserDefault()
* PARAMETER:  
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 28.01.2015
******************************************************************************/
tU32 u32DPEndUsersSetEndUserDefault(tU8 Pu8EndUser)
{
  tU32 Vu32Return=0;
#ifdef DP_U32_POOL_ID_DPENDUSERMODE 
  dp_tclSrvIf dpSrvIf;
  if(dpSrvIf.s32Lock(DP_U32_LOCK_MODE_END_USER)== DP_S32_NO_ERR)
  {
    if(dpSrvIf.s32SetEndUserDefault(Pu8EndUser)!= DP_S32_NO_ERR)
	  {/*error*/ 
      Vu32Return=40000000;
    }
    if(dpSrvIf.s32Unlock()!= DP_S32_NO_ERR)
    {/*error*/
      Vu32Return=20000000;
    }
  }
  else
  {/*error*/ 
    Vu32Return=10000000;
  }
#else
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu8EndUser);
#endif
  return(Vu32Return);
}

/*****************************************************************************
* FUNCTION:		main()
* PARAMETER:    Option:
*               -d: delete element: -d[elementname]
* RETURNVALUE:  
* TEST CASE:    
* DESCRIPTION:  
* HISTORY:		Created by Andrea Bueter(BSOT) 13.06.2014
******************************************************************************/
int main(int argc, char **argv)
{
   tU32                Vu32ReturnValue = 0;
   OSAL_tMQueueHandle  VmqHandleOEDTProcess = OSAL_C_INVALID_HANDLE;
   OSAL_tMQueueHandle  VmqHandleTestProcess = OSAL_C_INVALID_HANDLE;
   TPoolMsg            VtMsg;
   tBool               VbEnd=FALSE;
   tBool               VbEndTestExitWithoutOsalExit=FALSE;

   ET_TRACE_OPEN; 
   //for lint
   argc=(int) argv;
   // Open MQ 
   //fprintf(stderr, "main() 2 start \n");
   if (OSAL_s32MessageQueueOpen(MQ_DP_TEST_PROCESS2_NAME,OSAL_EN_READWRITE, &VmqHandleTestProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   // Open MQ 
   if (OSAL_s32MessageQueueOpen(MQ_DP_OEDT_PROCESS2_NAME,OSAL_EN_READWRITE, &VmqHandleOEDTProcess) == OSAL_ERROR)
   {
     Vu32ReturnValue=100000000;
   }
   if(Vu32ReturnValue!=0)
   {
     if ((VmqHandleTestProcess == OSAL_C_INVALID_HANDLE)||(VmqHandleOEDTProcess == OSAL_C_INVALID_HANDLE ))
	   {
	     Vu32ReturnValue=200000000;
	   }
   }
    //fprintf(stderr, "main() 2:start loop Vu32ReturnValue %d\n",Vu32ReturnValue);
   if (Vu32ReturnValue == 0)
   { // run test
	   tU32 u32Prio;
	   while(VbEnd==FALSE)
	   {
	     //fprintf(stderr, "main() 2: start wait \n");
	     if(OSAL_s32MessageQueueWait(VmqHandleTestProcess, (tPU8)&VtMsg, sizeof(TPoolMsg), &u32Prio,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
	     {
         //fprintf(stderr, "main() 2:error wait message queue\n");
	       Vu32ReturnValue = 300000000;
		     VbEnd=TRUE;
       }
	     else
	     {
	       //fprintf(stderr, "main() 2:wait done %d \n",VtMsg.eCmd);
         switch(VtMsg.eCmd)
		     {		   
		       case eCmdLock:
		       {
		         //fprintf(stderr, "cmd: eCmdLock \n");
             Vu32ReturnValue=u32DPEndUserLock();
           }break;
		       case eCmdUnlock:
		       {
		         //fprintf(stderr, "cmd: eCmdUnlock \n");
                 Vu32ReturnValue=u32DPEndUserUnlock();
		       }break;
		       case eCmdSetUser:
		       {
		         //fprintf(stderr, "cmd: eCmdSetUser %d\n",VtMsg.u.u8EndUser);
                 Vu32ReturnValue=u32DPEndUserSetEndUser(VtMsg.u.u8EndUser);
		       }break;
		       case eCmdSetUserWithLock:
		       {
		         //fprintf(stderr, "cmd: eCmdSetUserWithLock %d\n",VtMsg.u.u8EndUser);
                 Vu32ReturnValue=u32DPEndUserSetEndUserWithLock(VtMsg.u.u8EndUser);
		       }break;
		       case eCmdProcess2End:
		       {
			       //fprintf(stderr, "cmd: eCmdProcessEnd \n");
			       if(VtMsg.u.u8EndUser==1)
			       {
			         VbEndTestExitWithoutOsalExit=TRUE;
			       }
		         VbEnd=TRUE;
		       }break;
		       case eCmdResponse:
		         //fprintf(stderr, "cmd: eCmdResponse \n");
		       break;
		       default:
		       {
		         Vu32ReturnValue=400000000;
		       }break;
         }/*end switch */
         if(Vu32ReturnValue!=0)     fprintf(stderr, "main() 2:error code %d \n", Vu32ReturnValue);
         // send result to OEDT
         VtMsg.eCmd=eCmdResponse;
         VtMsg.u.u32ErrorCode=Vu32ReturnValue;
		     //fprintf(stderr, "main() 2:post message %d \n",VtMsg.eCmd);
         if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
         {
           Vu32ReturnValue = 500000000;
         }
		     else
		     {
		       Vu32ReturnValue = 0;
         }
       }
     }/*end while*/
   }
   //fprintf(stderr, "test end \n");
   if(Vu32ReturnValue!=0)
   {//post error
     VtMsg.eCmd=eCmdResponse;
     VtMsg.u.u32ErrorCode=Vu32ReturnValue;
     if (OSAL_s32MessageQueuePost(VmqHandleOEDTProcess, (tPCU8)&VtMsg,sizeof(TPoolMsg), OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) != OSAL_OK)
     {
       Vu32ReturnValue = 600000000;
     }
   }
   // Close MQs
   if (VmqHandleOEDTProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleOEDTProcess);
   }
   if (VmqHandleTestProcess != OSAL_C_INVALID_HANDLE)
   {
     OSAL_s32MessageQueueClose(VmqHandleTestProcess);
   }
   if(VbEndTestExitWithoutOsalExit==FALSE)
   {
     OSAL_vProcessExit();
   }
   _exit(Vu32ReturnValue);
}

/************************ End of file ********************************/




