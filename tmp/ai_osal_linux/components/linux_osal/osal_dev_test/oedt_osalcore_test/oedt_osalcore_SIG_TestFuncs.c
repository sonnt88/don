/******************************************************************************
 *FILE         : oedt_osalcore_SIG_TestFuncs.c
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file defines the  test cases for the testing
 *               the OSAL Signal Handling API.
 *               
 *AUTHOR       :Madhu Sudhan Swargam (RBEI/ECF5)
 *
 *COPYRIGHT    : (c) 2012, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : 04.12.12 .Initial version
 *             : Version 1.1  24-04-2013
 *             : Added  test cases for configuring all the Signals
 *             : and to check their status.   - MDH4COB
 *
 *****************************************************************************/
 
 /*****************************************************************
 * Helper Defines and Macros (scope: module-local)
 *******************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include <signal.h>
#include "oedt_osalcore_SIG_TestFuncs.h"
#include "OsalConf.h"
#define THREAD_ONE_ENABLE          0x00000001
#define THREAD_ONE_DISABLE         0x00000002
#define THREAD_ONE_COMPLETE        0x00000004
#define THREAD_ONE_ENABLE_TRUE     0x00000008
#define THREAD_ONE_DISABLE__TRUE   0x00000010
#define THREADCHECKLOOP            10
#define RandomCount                10
/*****************************************************************
* variable definition (scope: global)
******************************************************************/

static tBool ConfigCheck1			= FALSE;
static tBool ConfigCheck2			= FALSE;
static tBool ConfigCheck3			= FALSE;
static tU32 ConfigCheck_thread1  = 0;
static tU32 ConfigCheck_thread2  = 0;

OSAL_tEventHandle evHandle1 = 0;
OSAL_tEventHandle evHandle2 = 0;
OSAL_tEventHandle evHandle3 = 0;

/*****************************************************************
* function implementation (scope: global)
*****************************************************************/
// Test case's signal Handlers
static void TestSignalHandle1(int n_signal, siginfo_t *siginfo, void *ptr)
{
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"In Signal Handle By signal=%d To process=%d ",n_signal,siginfo->si_pid );
   ConfigCheck1=TRUE;
}
static void TestSignalHandle2(int n_signal, siginfo_t *siginfo, void *ptr)
{
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"In Signal Handle 2 By signal=%d To process=%d\n",n_signal,siginfo->si_pid );
   ConfigCheck2=TRUE;
}
static void TestSignalHandle3(int n_signal, siginfo_t *siginfo, void *ptr)
{
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"In Signal Handle 3 By signal=%d To process=%d\n",n_signal,siginfo->si_pid );
   ConfigCheck3=TRUE;
}
/*
 * definition used for installing the signal handlers
 */
static const struct sigaction test_Func1 = 
{
      .sa_sigaction = TestSignalHandle1,
      .sa_flags =  SA_SIGINFO ,
};
static const struct sigaction test_Func2 = 
{
      .sa_sigaction = TestSignalHandle2,
      .sa_flags =  SA_SIGINFO ,
};
static const struct sigaction test_Func3 = 
{
      .sa_sigaction = TestSignalHandle3,
      .sa_flags =  SA_SIGINFO ,
};


/*****************************************************************************
* FUNCTION    :  u32ConfigureSignalNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_001

* DESCRIPTION :  
*               a) Change the Signal Configuration for NULL Signal 
*
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigureSignalNULL(tVoid)
{
   tU32 u32Ret                   = 0;
   if(OSALConfigSignalHandle(SIGNALNULL,TRUE)!= OSAL_ERROR)
   {
      u32Ret+=100;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   u32ConfigureSignalInvalid()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_002

* DESCRIPTION :  
*             a) Change the Signal Configuration for Invalid Signal 
*			       	
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/


tU32 u32ConfigureSignalInvalid(tVoid)
{
   tU32 u32Ret                   = 0;
   if(OSALConfigSignalHandle(SIGNALINVALID,FALSE)!= OSAL_ERROR)
   {
     u32Ret+=100;
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   u32ConfigureSignalDoesntExist()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_003

* DESCRIPTION :  
*              a) Change the Signal Configuration which is not a Signal.
*			   
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigureSignalDoesntExist(tVoid)
{
   tU32 u32Ret                   = 0;
   if(OSALConfigSignalHandle(SIGNALDOESNTEXIST,FALSE)!= OSAL_ERROR)
   {
     u32Ret+=100;
   }
   return u32Ret;
}


/*****************************************************************************
* FUNCTION    :   u32GetStatusInvalid()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_004

* DESCRIPTION :  
*              a) Get the status for  Signal with invalid handler 
*			    	
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/

tU32 u32GetStatusInvalid(tVoid)
{
   tU32 u32Ret            = 0;
   if(OSALSignalHandlestatus(TESTSIGNAL1,NULL)==OSAL_OK)
   {
      u32Ret+=100;	
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   u32GetInvalidSignalStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_005

* DESCRIPTION :  
*				   a) Get the status for invalid Signal .
*			       	
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32GetInvalidSignalStatus(tVoid)
{
   tU32 u32Ret            = 0;
   if(OSALSignalHandlestatus(SIGNALINVALID,NULL)==OSAL_OK)
   {
      u32Ret+=100;	
   }
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32EnableTwiceCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_006

* DESCRIPTION :  
*              a) Enable the Signal Configuration for TESTSIGNAL2 Signal 
*              b) Again the TESTSIGNAL2 signal is enabled, if error occurs results in test case failure
*			       	
*			       	
* HISTORY     :   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32EnableTwiceCheck(tVoid)
{
   tU32 u32Ret               = 0;
   if(OSALConfigSignalHandle(TESTSIGNAL2,TRUE)== OSAL_OK)
   {
      if(OSALConfigSignalHandle(TESTSIGNAL2,TRUE)!= OSAL_OK)
      {
         u32Ret+=200;	
      }	
   }
   else
   u32Ret+=100;
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DisableTwiceCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_007

* DESCRIPTION :  
*           a) Disable the Signal Configuration for TESTSIGNAL2 Signal 
*           b) Again the TESTSIGNAL2 signal is Disabled, if error occurs results in test case failure
*           c)  Enable the Signal Configuration
*			       	
*			       	
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32DisableTwiceCheck(tVoid)
{
   tU32 u32Ret               = 0;
   if(OSALConfigSignalHandle(TESTSIGNAL2,FALSE)== OSAL_OK)
   {
      if(OSALConfigSignalHandle(TESTSIGNAL2,FALSE)!= OSAL_OK)
      {
         u32Ret+=200;	
      }	
   }
   else
   {
      u32Ret+=100;
   }
   OSALConfigSignalHandle(TESTSIGNAL2,TRUE);// keeping back the original OSAL Default Configuration 
   return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32ConfigureSignalCheckWithStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_008

* DESCRIPTION :  
*              a) Disable the Signal Configuration for TESTSIGNAL1  Signal 
*              b) Check whether the status is same or not, else returns failure
*			       	
*			       	
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigureSignalCheckWithStatus(tVoid)
{
   tU32 u32Ret                   = 0;
   tBool bStatus=TRUE;
   if(OSALConfigSignalHandle(TESTSIGNAL1,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL1,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=100;
      }
      else
      {         u32Ret+=200;
      }
   }
   else
   {      u32Ret+=300;
   }

   OSALConfigSignalHandle(TESTSIGNAL1,TRUE); // keeping back the original OSAL Default Configuration 
   return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32ConfigureCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_009

* DESCRIPTION :  
*              a) Disable the Signal Configuration for the signal
*              b) Assign one test case Signal Handler
*              c) Raise the disable signal to the Process
*              d) Activate the variable in Handler and check the Variable is updated or not
*              e) Else return Failure
*			       	
*			       	
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigureCheck(tVoid)
{
   tU32 u32Ret 				  = 0;
   tU32 u32LoopCount             = 0;
   struct sigaction Old_signalHandler;
   if(OSALConfigSignalHandle(TESTSIGNAL2,FALSE)!= OSAL_ERROR)
   {
      sigaction(TESTSIGNAL2, &test_Func1,&Old_signalHandler ); // Assining the User defined Signal Handler
      raise(TESTSIGNAL2);  // raising the signal to process to check the signal Handler functionality

   }
   else
   {
      u32Ret+=100;
   }
   u32LoopCount=0;
   while(ConfigCheck1==FALSE) // Waiting for the Signal Handler to update the Variable
      {
         OSAL_s32ThreadWait( 10 );
         u32LoopCount++;
         if(u32LoopCount>3)
         {
            break;
         }
      }
   if(u32LoopCount>3)
   {
      u32Ret+=200;
   }
   OSALConfigSignalHandle(TESTSIGNAL2,TRUE);// keeping back the original OSAL Default Configuration 
   return u32Ret;
}



/*****************************************************************************
* FUNCTION    :	   u32ConfigurTwiceDisableCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_010

* DESCRIPTION :  
*              a) Disable the Signal Configuration for the signal
*              b) Assign one test case Signal Handler
*              c) Raise the disabled signal to the Process
*              d) Update the Variable in the signal Handler
*              e) Again Disable the Signal Configuration
*              f)Raise the disabled signal to the Process
*              g) Update the Variable in the signal Handler
*              h)If both the signaler handlers update the Variable return Success		       	
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigurTwiceDisableCheck(tVoid)
{
   tU32 u32Ret 				  = 0;
   if(OSALConfigSignalHandle(TESTSIGNAL3,FALSE)!= OSAL_ERROR) //Disables TESTSIGNAL3 
   {
      sigaction(TESTSIGNAL3, &test_Func2,NULL ); // Assining the User defined Signal Handler
      raise(TESTSIGNAL3); // raising the signal to process to check the signal Handler functionality
   }
   else
   {
      u32Ret+=100;
   }
   OSAL_s32ThreadWait( 100 );
   if(OSALConfigSignalHandle(TESTSIGNAL3,FALSE)!= OSAL_ERROR) //Disables TESTSIGNAL3  
   {
      sigaction(TESTSIGNAL3, &test_Func3,NULL ); // Assining the User defined Signal Handler
      raise(TESTSIGNAL3); // raising the signal to process to check the signal Handler functionality
   }
   else
   {
      u32Ret+=200;
   }
   OSAL_s32ThreadWait( 100 );
   if(ConfigCheck2==FALSE || ConfigCheck3==FALSE)
   {
      u32Ret+=300;
   }
   OSALConfigSignalHandle(TESTSIGNAL3,TRUE);// keeping back the original OSAL Default Configuration 
   return u32Ret;
}
/************************************************************************************************
* FUNCTION    :   ConfigurCheckThread1()

* PARAMETER   :   none

* TEST CASE   :   Not a test case

* DESCRIPTION :  
*              a) Waits for THREAD_ONE_ENABLE event, then Check the Signal Status
*              b) If status is true Global variable is incremented and Post THREAD_ONE_ENABLE_TRUE
*              c) Waits for THREAD_ONE_DISABLE event, then Check the Signal Status
*              d) If status is False Global variable is incremented and Post THREAD_ONE_DISABLE__TRUE
*              e) The steps a,b,c,d are repeated in "for" loop for THREADCHECKLOOP times 
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*********************************************************************************************/
static void ConfigurCheckThread1(void *arg)
{
   tBool bStatus=TRUE;
   tU32 u32LoopCount;
   OSAL_tEventMask rResultMask = 0;
   for(u32LoopCount=0;u32LoopCount<THREADCHECKLOOP;u32LoopCount++)
   {
      // Waiting for THREAD_ONE_ENABLE event from thread 2
      if( OSAL_ERROR != OSAL_s32EventWait( evHandle1,
                                                THREAD_ONE_ENABLE,
                                                OSAL_EN_EVENTMASK_AND,
                                                OSAL_C_TIMEOUT_FOREVER,
                                                &rResultMask ) )  
      {
         if(THREAD_ONE_ENABLE == (rResultMask & THREAD_ONE_ENABLE))
         {
         // check the status and update the variable
            (tVoid)OSAL_s32EventPost(evHandle1,
                                             ~THREAD_ONE_ENABLE,
                                             OSAL_EN_EVENTMASK_AND);
            if(OSALSignalHandlestatus(TESTSIGNAL3,&bStatus)==OSAL_OK) 
            {
               if(bStatus==TRUE)
               {
                  ConfigCheck_thread1++;
               }
            }
         }
      }
      OSAL_s32ThreadWait( 10 ); 
      // Post the THREAD_ONE_ENABLE_TRUE to Thread 2
      (tVoid)OSAL_s32EventPost( evHandle2,THREAD_ONE_ENABLE_TRUE,OSAL_EN_EVENTMASK_OR ) ; 
      OSAL_s32ThreadWait( 10 ); 
      // Waiting for THREAD_ONE_DISABLE event from thread 2 
      if( OSAL_ERROR != OSAL_s32EventWait( evHandle1,
                                                   THREAD_ONE_DISABLE,
                                                   OSAL_EN_EVENTMASK_AND,
                                                   OSAL_C_TIMEOUT_FOREVER,
                                                   &rResultMask ) ) 
      {
         if(THREAD_ONE_DISABLE == (rResultMask & THREAD_ONE_DISABLE))
         {
         // check the status and update the variable
            (tVoid)OSAL_s32EventPost(evHandle1,
                                          ~THREAD_ONE_DISABLE,
                                          OSAL_EN_EVENTMASK_AND);
            if(OSALSignalHandlestatus(TESTSIGNAL3,&bStatus)==OSAL_OK) 
            {
               if(bStatus==FALSE)
               {
                  ConfigCheck_thread2++;
               }
            }
         }
      }
      OSAL_s32ThreadWait( 10 ); 
      // Post the THREAD_ONE_DISABLE__TRUE to Thread 2
      (tVoid)OSAL_s32EventPost( evHandle2,THREAD_ONE_DISABLE__TRUE,OSAL_EN_EVENTMASK_OR );
      OSAL_s32ThreadWait( 10 ); 
   }
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION    :	   ConfigurCheckThread2()

* PARAMETER   :    none

* TEST CASE   :    Not a Test Case

* DESCRIPTION :  
*              a) Enables the TESTSIGNAL3 and Post THREAD_ONE_ENABLE
*              b) Wair for THREAD_ONE_ENABLE_TRUE event.
*              c) When the Event is received Disables TESTSIGNAL3 and Post THREAD_ONE_DISABLE.
*              d) Waits for   THREAD_ONE_DISABLE__TRUE
*              e) Steps a,b,c,d are repeadted in for loop for 	THREADCHECKLOOP times
*              f) Event THREAD_ONE_COMPLETE is posted for the test case.
*			   
* HISTORY     : Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
static void ConfigurCheckThread2(void *arg)
{
   tU32 u32LoopCount;
   OSAL_tEventMask rResultMask = 0;
   for(  u32LoopCount=0;  u32LoopCount<THREADCHECKLOOP;  u32LoopCount++)
   {
      (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,TRUE);
      // Enable the TESTSIGNAL3 and post THREAD_ONE_ENABLE to thread 1
      (tVoid)OSAL_s32EventPost( evHandle1,THREAD_ONE_ENABLE,OSAL_EN_EVENTMASK_OR ) ;
      OSAL_s32ThreadWait( 10 ); 
      // Waits for the THREAD_ONE_ENABLE_TRUE event from thread1
      if( OSAL_ERROR != OSAL_s32EventWait( evHandle2,
                                                   THREAD_ONE_ENABLE_TRUE,
                                                   OSAL_EN_EVENTMASK_AND,
                                                   OSAL_C_TIMEOUT_FOREVER,
                                                   &rResultMask ) )
      {
         if(THREAD_ONE_ENABLE_TRUE == (rResultMask & THREAD_ONE_ENABLE_TRUE))
         {
            (tVoid)OSAL_s32EventPost(evHandle2,
                                       ~THREAD_ONE_ENABLE_TRUE,
                                       OSAL_EN_EVENTMASK_AND);
         }
      }
      OSAL_s32ThreadWait( 10 ); 
      (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,FALSE);
      // Enable the TESTSIGNAL3 and post THREAD_ONE_DISABLE to thread 1
      (tVoid)OSAL_s32EventPost( evHandle1,THREAD_ONE_DISABLE,OSAL_EN_EVENTMASK_OR );
      OSAL_s32ThreadWait( 10 ); 
      // Waits for the THREAD_ONE_DISABLE__TRUE event from thread1
      if( OSAL_ERROR != OSAL_s32EventWait( evHandle2,
                                                THREAD_ONE_DISABLE__TRUE,
                                                OSAL_EN_EVENTMASK_OR,
                                                OSAL_C_TIMEOUT_FOREVER,
                                                &rResultMask  ) )
      {
         if(THREAD_ONE_DISABLE__TRUE == (rResultMask & THREAD_ONE_DISABLE__TRUE))
         {
            (tVoid)OSAL_s32EventPost(evHandle2,
                                             ~THREAD_ONE_DISABLE__TRUE,
                                             OSAL_EN_EVENTMASK_AND);
         }
      }	
      OSAL_s32ThreadWait( 10 ); 
   }
   //Post the THREAD_ONE_COMPLETE event to Test case 
   (tVoid)OSAL_s32EventPost( evHandle3,THREAD_ONE_COMPLETE,OSAL_EN_EVENTMASK_OR ); 
   OSAL_vThreadExit();
}
/*****************************************************************************
* FUNCTION    :	   u32ConfigurSignalThreadsCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_011

* DESCRIPTION :  
*              a) Creates two Threads.
*              b) Thread 2 will enable the signal and wait for response
*              c) Thread 1 will Check the Status update variable and sends the response
*              d) Thread 2 will Disable the signal and wait for response
*              e) Thread 1 will Check the Status update variable and sends the response
*              f) the steps b,c,d,e are repeated for THREADCHECKLOOP times
*              g) Checks both Disabled and Enabled Variables
*              h) If the varibles are equal to 	THREADCHECKLOOP returns Success	       	
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32ConfigurSignalThreadsCheck(tVoid)
{
   OSAL_trThreadAttribute th_attr;
   tU32 u32Ret 				  = 0;
   ConfigCheck_thread1=0;
   ConfigCheck_thread2=0;
   OSAL_tEventMask rResultMask = 0;
   th_attr.u32Priority = OSAL_TIMER_THREAD_PRIORITY;
   th_attr.s32StackSize = 4*4096;
   th_attr.pvArg = NULL;
   /*Creation for events and threads used for Test case*/
   (tVoid)OSAL_s32EventCreate("Signal_Handler_test_1",&evHandle1 ) ;
   (tVoid)OSAL_s32EventCreate("Signal_Handler_test_2", &evHandle2) ;
   (tVoid)OSAL_s32EventCreate( "Signal_Handler_test_3",&evHandle3);
   th_attr.szName = (char*)"OEDT_Sig_Config_Check";
   th_attr.pfEntry = ConfigurCheckThread1;
   if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
   {
     u32Ret+=100;
   }
   th_attr.szName = (char*)"OEDT_Sig_Do_Config";
   th_attr.pfEntry = ConfigurCheckThread2;
   if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
   {
      u32Ret+=200;
   }
   OSAL_s32ThreadWait( 50 );  
   /* Waits for response from threads to complete thier Tasks*/
   if( OSAL_ERROR != OSAL_s32EventWait( evHandle3,
                                             THREAD_ONE_COMPLETE,
                                             OSAL_EN_EVENTMASK_AND,
                                             OSAL_C_TIMEOUT_FOREVER,
                                             &rResultMask ) )
   {
      if(THREAD_ONE_COMPLETE == (rResultMask & THREAD_ONE_COMPLETE))
      {
         (tVoid)OSAL_s32EventPost(evHandle3,
                                       ~THREAD_ONE_COMPLETE,
                                       OSAL_EN_EVENTMASK_AND);
      }
   }
   if(ConfigCheck_thread2 !=THREADCHECKLOOP && ConfigCheck_thread1!=THREADCHECKLOOP) // Check whether the Global variables are upto the limit
   {
      OEDT_HelperPrintf( TR_LEVEL_USER_1,"The values Thread1=%d, Thread2=%d are not equal to Max=%d",ConfigCheck_thread1,ConfigCheck_thread2,THREADCHECKLOOP);
      u32Ret+=100;
   }
   
   ConfigCheck_thread1=0;
   ConfigCheck_thread2=0;
   (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,TRUE);// keeping back the original OSAL Default Configuration 
   (tVoid)OSAL_s32EventDelete("Signal_Handler_test_1");
   (tVoid)OSAL_s32EventDelete("Signal_Handler_test_2");
   (tVoid)OSAL_s32EventDelete("Signal_Handler_test_3");
   return u32Ret;	   

}
/*****************************************************************************
* FUNCTION    :	   ConfigurRandomEnable()

* PARAMETER   :    none

* TEST CASE   :    Not a test case

* DESCRIPTION :  
*              a) Enables the signal TESTSIGNAL3 Randomly.    	
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
static void ConfigurRandomEnable(void *arg)
{
   tU32 i=0;
   for(i=0;i<RandomCount;i++)
   {
      (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,TRUE);//Enables the signal TESTSIGNAL3 Randomly. 
      OSAL_s32ThreadWait( 10 ); 
   }
}
/*****************************************************************************
* FUNCTION    :	   ConfigurRandomDisable()

* PARAMETER   :    none

* TEST CASE   :   Not a test Case

* DESCRIPTION :  
*              a) Disables the signal TESTSIGNAL3 Randomly.
*       	
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
static void ConfigurRandomDisable(void *arg)
{
   tU32 i=0;
   for(i=0;i<RandomCount;i++)
   {
      (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,FALSE);//Disables the signal TESTSIGNAL3 Randomly.
      OSAL_s32ThreadWait( 10 ); 
   }
}
/*****************************************************************************
* FUNCTION    :	   u32RandomConfigurSignal()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SIG_HNDL_012

* DESCRIPTION :  
*              a) Creates two Threads.
*              b) Thread 1 will enable the signal.
*              c) Thread 2 will Disable the signal.  	
*              d) To Check the traces Define in "OEDT_SIG_TRACE" in exception_handler.c
*			   
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Dec 04,2012
*
*******************************************************************************/
tU32 u32RandomConfigurSignal(tVoid)
{
   tU32 u32Ret 				  = 0;
   OSAL_trThreadAttribute th_attr;
   th_attr.u32Priority = OSAL_TIMER_THREAD_PRIORITY;
   th_attr.s32StackSize = 4*4096;
   th_attr.pvArg = NULL;
    /*Creation for threads used for Test case*/
   th_attr.szName = (char*)"OEDT_Sig_Check_Random1";
   th_attr.pfEntry = ConfigurRandomEnable;
   if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
   {
      u32Ret+=200;
   }
   th_attr.szName = (char*)"OEDT_Sig_Check_Random2";
   th_attr.pfEntry = ConfigurRandomDisable;
   if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
   {
      u32Ret+=100;
   }
   OSAL_s32ThreadWait( 500 ); 
 (tVoid)OSALConfigSignalHandle(TESTSIGNAL3,TRUE);// keeping back the original OSAL Default Configuration   
   return u32Ret;	     
}

/*****************************************************************************
* FUNCTION         :    u32StatusCheckOfSignals()

* PARAMETER        :    none

* RETURNVALUE      :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE        :    TU_OEDT_OSAL_CORE_SIG_HNDL_013

* DESCRIPTION      :    Checks the status of the signal, whether its enabled or not.

* AUTHOR           :    Manikandan Dhanasekaran (RBEI/ECF5Z).

*******************************************************************************/
tU32 u32StatusCheckOfSignals(tVoid)
{
   tU32 u32Ret     = 0;
   tBool bStatus   = TRUE;
   
   //For the signal SIGXFSZ
   if(OSALSignalHandlestatus(TESTSIGNAL1,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=100;
      }
   }
   
   //For the signal SIGSEGV
   if(OSALSignalHandlestatus(TESTSIGNAL4,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=200;
      }
   }

   //For the signal SIGILL
   if(OSALSignalHandlestatus(TESTSIGNAL5,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=300;
      }
   }

   //For the signal SIGFPE
   if(OSALSignalHandlestatus(TESTSIGNAL6,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=400;
      }
   }

   //For the signal SIGSYS
   if(OSALSignalHandlestatus(TESTSIGNAL7,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=500;
      }
   }

   //For the signal SIGBUS
   if(OSALSignalHandlestatus(TESTSIGNAL8,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=600;
      }
   }

   //For the signal SIGABRT
   if(OSALSignalHandlestatus(TESTSIGNAL9,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=700;
      }
   }

   //For the signal SIGPIPE
   if(OSALSignalHandlestatus(TESTSIGNAL10,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=800;
      }
   }

   //For the signal SIGXCPU
   if(OSALSignalHandlestatus(TESTSIGNAL11,&bStatus)==OSAL_OK) 
   {
      if(bStatus!=TRUE)
      {
         u32Ret+=900;
      }
   }
   /*Return error code*/   
   return u32Ret;
}


/*****************************************************************************
* FUNCTION         :    u32ConfigureCheckVariousSignals()

* PARAMETER        :    none

* RETURNVALUE      :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE        :    TU_OEDT_OSAL_CORE_SIG_HNDL_014

* DESCRIPTION      :    Does the Configuration check for various signals.

* AUTHOR           :    Manikandan Dhanasekaran (RBEI/ECF5Z).

*******************************************************************************/

tU32 u32ConfigureCheckVariousSignals(tVoid)
{
   tU32 u32Ret    = 0;
   tBool bStatus  = TRUE;
   
   //For the signal SIGXFSZ
   if( OSALConfigSignalHandle(TESTSIGNAL1,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL1,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=100;
      }
      else
      {
         u32Ret += 200;
      }
      OSALConfigSignalHandle(TESTSIGNAL1,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL1,TRUE) )
      {
         u32Ret += 300;
      }
   }

   //For the signal SIGUSR1
   if( OSALConfigSignalHandle(TESTSIGNAL2,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL2,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=400;
      }
      else
      {
         u32Ret += 500;
      }
      OSALConfigSignalHandle(TESTSIGNAL2,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL2,TRUE) )
      {
         u32Ret += 600;
      }
   }

   //For the signal SIGALRM
   if( OSALConfigSignalHandle(TESTSIGNAL3,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL3,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=700;
      }
      else
      {
         u32Ret += 800;
      }
      OSALConfigSignalHandle(TESTSIGNAL3,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL3,TRUE) )
      {
         u32Ret += 900;
      }
   }

   //For the signal SIGSEGV
   if( OSALConfigSignalHandle(TESTSIGNAL4,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL4,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=1000;
      }
      else
      {
         u32Ret += 1100;
      }
      OSALConfigSignalHandle(TESTSIGNAL4,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL4,TRUE) )
      {
         u32Ret += 1200;
      }
   }

   //For the signal SIGILL
   if( OSALConfigSignalHandle(TESTSIGNAL5,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL5,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=1300;
      }
      else
      {
         u32Ret += 1400;
      }
      OSALConfigSignalHandle(TESTSIGNAL5,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL5,TRUE) )
      {
         u32Ret += 1500;
      }
   }

   //For the signal SIGFPE
   if( OSALConfigSignalHandle(TESTSIGNAL6,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL6,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=1600;
      }
      else
      {
         u32Ret += 1700;
      }
      OSALConfigSignalHandle(TESTSIGNAL6,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL6,TRUE) )
      {
         u32Ret += 1800;
      }
   }

   //For the signal SIGSYS
   if( OSALConfigSignalHandle(TESTSIGNAL7,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL7,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=1900;
      }
      else
      {
         u32Ret += 2000;
      }
      OSALConfigSignalHandle(TESTSIGNAL7,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL7,TRUE) )
      {
         u32Ret += 2100;
      }
   }

   //For the signal SIGBUS
   if( OSALConfigSignalHandle(TESTSIGNAL8,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL8,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=2200;
      }
      else
      {
         u32Ret += 2300;
      }
      OSALConfigSignalHandle(TESTSIGNAL8,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL8,TRUE) )
      {
         u32Ret += 2400;
      }
   }

   //For the signal SIGABRT
   if( OSALConfigSignalHandle(TESTSIGNAL9,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL9,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=2500;
      }
      else
      {
         u32Ret += 2600;
      }
      OSALConfigSignalHandle(TESTSIGNAL9,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL9,TRUE) )
      {
         u32Ret += 2700;
      }
   }

   //For the signal SIGPIPE
   if( OSALConfigSignalHandle(TESTSIGNAL10,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL10,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=2800;
      }
      else
      {
         u32Ret += 2900;
      }
      OSALConfigSignalHandle(TESTSIGNAL10,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL10,TRUE) )
      {
         u32Ret += 3000;
      }
   }

   //For the signal SIGXCPU
   if( OSALConfigSignalHandle(TESTSIGNAL11,FALSE)!= OSAL_ERROR)
   {
      if(OSALSignalHandlestatus(TESTSIGNAL11,&bStatus)==OSAL_OK)
      {
         if(bStatus==TRUE)
            u32Ret+=3100;
      }
      else
      {
         u32Ret += 3200;
      }
      OSALConfigSignalHandle(TESTSIGNAL11,TRUE);
   }

   else
   {
      if( OSAL_ERROR == OSALConfigSignalHandle(TESTSIGNAL11,TRUE) )
      {
         u32Ret += 3300;
      }
   }
 
   /*Return error code*/
   return u32Ret;
}

/*End of Oedt_osalcore_SIG_TensFuncs.c*/
