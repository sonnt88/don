/******************************************************************************
 *FILE         : oedt_osalcore_IPC_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the IPC
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2008, Robert Bosch India Limited
 *
 *HISTORY      :
 *              ver1.4   - 22-04-2009
                lint info removal
				sak9kor

  *				ver1.3	 - 14-04-2009
                warnings removal
				rav8kor
 				 
 				 Version 1.2 , 10 - 03 - 2008
 *				 Removed Prototype for Input Feature, modified
 *				 Prototype for u32StartInit function
 *
 *				 Version 1.1 , 27 - 02 - 2008
 *				 Added several new Macros for 
 *				 Semaphore,Event and Message Queue Stress Test cases
 *				 Added Prototypes for cases TU_OEDT_OSAL_CORE_IPC_010 - 
 *				 TU_OEDT_OSAL_CORE_IPC_018
 *
 *				 Version 1.0 , 06- 02- 2008
 *				 Initial version - Tinoy Mathews( RBIN/EDI3 )	
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_IPC_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_IPC_TESTFUNCS_HEADER


/*Defines*/
#undef  PRIORITY
#define PRIORITY 					100
#undef INVALID_PID
#define INVALID_PID          ((tS32)-100)

/*Feature Defines - Choice Byte*/
#define FEATURE_TEST0         (tU8)0x00
#define FEATURE_TEST1         (tU8)0x01
#define FEATURE_TEST2         (tU8)0x02
#define FEATURE_TEST3         (tU8)0x03
#define FEATURE_TEST4         (tU8)0x04
#define FEATURE_TEST5         (tU8)0x05
#define FEATURE_TEST6         (tU8)0x06
#define FEATURE_TEST7         (tU8)0x07
#define FEATURE_TEST8         (tU8)0x08
#define FEATURE_TEST9         (tU8)0x09
#define FEATURE_TEST10		  (tU8)0x0A
#define FEATURE_TEST11		  (tU8)0x0B
#define FEATURE_TEST12		  (tU8)0x0C
#define FEATURE_TEST13		  (tU8)0x0D
#define FEATURE_TEST14		  (tU8)0x0E
#define FEATURE_TEST15        (tU8)0x0F
#define FEATURE_TEST16        (tU8)0x10
#define FEATURE_TEST17        (tU8)0x11
#define FEATURE_TEST18        (tU8)0x12

#define CLIENT_TO_SERVER       0x00000001
#define SERVER_TO_CLIENT       0x00000002
#define SERVER_TO_CLIENT_INIT  0x00000004

/*Error Code in the Custom Process - Error Byte*/
#define MAX_ERROR_CODE        (tU8)0xFF
#define MIN_ERROR_CODE        (tU8)0x00


#define SH_OSALRESOURCE_SIZE       76
#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS         10
#undef  NO_OF_WAIT_THREADS 
#define NO_OF_WAIT_THREADS         15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS        1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS       2
#undef  SRAND_MAX
#define SRAND_MAX                  (tU32)65535
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                   0x7FFFFFFF
#undef MAX_LENGTH

#define MAX_LENGTH                 3
#define MAX_OSAL_RESOURCES             (tU8)25
#define THREAD_NAME_LEN            256

#undef TOT_THREADS

#define TOT_THREADS                (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)

#undef  FIVE_SECONDS
#define FIVE_SECONDS               5000

#undef  MAX_PATTERNS
#define MAX_PATTERNS             (tU32)32
#undef  PATTERN_1
#define PATTERN_1              	 0x00000001	/*1*/
#undef  PATTERN_2
#define PATTERN_2              	 0x00000002	/*2*/
#undef  PATTERN_4
#define PATTERN_4             	 0x00000004	/*3*/
#undef  PATTERN_8
#define PATTERN_8            	 0x00000008	/*4*/
#undef  PATTERN_16 
#define PATTERN_16          	 0x00000010	/*5*/
#undef  PATTERN_32
#define	PATTERN_32        		 0x00000020	/*6*/
#undef  PATTERN_64 
#define PATTERN_64        		 0x00000040	/*7*/
#undef  PATTERN_128
#define PATTERN_128   			 0x00000080	/*8*/
#undef  PATTERN_256
#define PATTERN_256   			 0x00000100	/*9*/
#undef  PATTERN_512
#define PATTERN_512   			 0x00000200	/*10*/
#undef  PATTERN_1024
#define PATTERN_1024   			 0x00000400	/*11*/
#undef  PATTERN_2048
#define PATTERN_2048   			 0x00000800	/*12*/
#undef  PATTERN_4096
#define PATTERN_4096   			 0x00001000	/*13*/
#undef  PATTERN_8192
#define PATTERN_8192   			 0x00002000	/*14*/
#undef  PATTERN_16384
#define PATTERN_16384   	     0x00004000	/*15*/
#undef  PATTERN_32768
#define PATTERN_32768   		 0x00008000	/*16*/
#undef  PATTERN_65536
#define PATTERN_65536   		 0x00010000	/*17*/
#undef  PATTERN_131072
#define PATTERN_131072			 0x00020000 /*18*/
#undef  PATTERN_262144 
#define PATTERN_262144			 0x00040000 /*19*/
#undef  PATTERN_524288
#define PATTERN_524288			 0x00080000 /*20*/
#undef  PATTERN_1048576
#define PATTERN_1048576			 0x00100000	/*21*/
#undef  PATTERN_2097152
#define PATTERN_2097152          0x00200000	/*22*/
#undef  PATTERN_4194304
#define PATTERN_4194304          0x00400000	/*23*/
#undef  PATTERN_8388608
#define PATTERN_8388608          0x00800000	/*24*/
#undef  PATTERN_16777216
#define PATTERN_16777216		 0x01000000	/*25*/
#undef  PATTERN_33554432
#define PATTERN_33554432         0x02000000	/*26*/
#undef  PATTERN_67108864
#define PATTERN_67108864		 0x04000000	/*27*/
#undef  PATTERN_134217728
#define PATTERN_134217728        0x08000000	/*28*/
#undef  PATTERN_268435456
#define PATTERN_268435456        0x10000000	/*29*/
#undef  PATTERN_536870912
#define PATTERN_536870912        0x20000000	/*30*/
#undef  PATTERN_1073741824
#define PATTERN_1073741824		 0x40000000	/*31*/
#undef  PATTERN_2147483648
#define PATTERN_2147483648       0x80000000	/*32*/

#undef  MAX_MSG_LENGTH

#define MAX_MSG_LENGTH			 20
#undef  MAX_LEN
#define MAX_LEN                  20
#define MAX_MSGS                 20
#undef  MAX_NO_MESSAGES
#define MAX_NO_MESSAGES          20
#undef  MQ_PRIO0
#define MQ_PRIO0 				 0
#undef  MQ_PRIO1
#define MQ_PRIO1                 1
#undef  MQ_PRIO2
#define MQ_PRIO2 			     2
#undef  MQ_PRIO3
#define MQ_PRIO3                 3
#undef  MQ_PRIO4
#define MQ_PRIO4 				 4
#undef  MQ_PRIO5
#define MQ_PRIO5                 5
#undef  MQ_PRIO6            
#define MQ_PRIO6 				 6
#undef  MQ_PRIO7
#define MQ_PRIO7 				 7
#undef  INVAL_PRIO
#define INVAL_PRIO               20 /*>>7*/

#undef INIT_VAL

#define INIT_VAL                 0x00
#define SERVER_VAL               0xAA
#define CLIENT_VAL               0xBB

/*Data Structures*/
typedef struct SemaphoreTableEntryIPC_
{
	OSAL_tSemHandle hSem;
	tChar hSemName[MAX_LENGTH];
}SemaphoreTableEntryIPC;

typedef struct EventTableEntryIPC_
{
	OSAL_tEventHandle hEve;
	tChar hEventName[MAX_LENGTH];
}EventTableEntryIPC;

typedef struct MessageQueueTableEntryIPC_
{
	tChar coszName[MAX_LENGTH];
	tU32  u32MaxMessages;
	tU32  u32MaxLength;
	OSAL_tenAccess enAccess;
	OSAL_tMQueueHandle phMQ;
}MessageQueueTableEntryIPC;


/*Helper Functions*/
tU32  u32StartInit( tU8 );
tVoid vHelperPrintf( tVoid );
tU32  u32StartProcess( tVoid );
/*Helper Functions*/

/*Function Prototypes*/
tU32  u32SharedMemoryTestIPC( tVoid );
tU32  u32SemWaitInCustomProcessIPC( tVoid );
tU32  u32SemPostInCustomProcessMaxIPC( tVoid );
tU32  u32SemRedeleteIPC( tVoid );
tU32  u32SemDelOpenCounterNotZeroIPC( tVoid );
tU32  u32SemCreateSameNameIPC( tVoid );
tU32  u32EventCreateSameNameIPC( tVoid );
tU32  u32EventWaitPostIPC( tVoid );
tU32  u32EventRedeleteIPC( tVoid );
tU32  u32MQCreateSameNameIPC( tVoid );
tU32  u32MQRedeleteIPC( tVoid );
tU32  u32MQWaitForMSGIPC( tVoid );
tU32  u32MQPostMaxMSGIPC( tVoid );
tU32  u32SharedMemMapIPC( tVoid );
tU32  u32MSGPLIPC( tVoid );

tU32  u32SemaphoreStressTestIPC( tVoid );
tU32  u32EventStressTestIPC( tVoid );
tU32  u32MessageQueueStressTestIPC( tVoid );


#endif


