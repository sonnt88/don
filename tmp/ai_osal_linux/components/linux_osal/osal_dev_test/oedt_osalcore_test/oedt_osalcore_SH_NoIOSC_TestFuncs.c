/******************************************************************************
 *FILE          :oedt_osalcore_SH_NoIOSC_TestFuncs.c
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for 
 *               the shared Memory (NO IOSC) OSAL_CORE APIs
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY      : 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *			                copy of oedt_osalcore_SH_TestFuncs.c
 *
 *****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_osalcore_SH_common_TestFuncs.h"
#include "oedt_osalcore_SH_NoIOSC_TestFuncs.h"
#include "oedt_helper_funcs.h"


/*Global Variables*/
extern OSAL_tShMemHandle ghShared;
extern OSAL_tEventHandle gSHEventHandle;
extern OSAL_tEventMask   gevMask;
/*Global Variables*/

extern tU32 u32SHSharedMemoryCreateStatus( tVoid );
extern tU32 u32SHSharedMemoryOpenStatus( tVoid );
extern tU32 u32SHSharedMemoryCloseStatus( tVoid );

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCSharedMemoryCreateStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHSharedMemoryCreateStatus()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCSharedMemoryCreateStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_INVALIDVALUE: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_ALREADYEXISTS:
		 u32Ret = 2;
		 break;
	  case OSAL_E_NOSPACE:
		 u32Ret = 3;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 4;
		 break;
	  case OSAL_E_NAMETOOLONG:
		 u32Ret = 5;
		 break;
	  default:
		 u32Ret = 6;
   }//end of switch( u32status )

   return u32Ret; 
} 

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCSharedMemoryOpenStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHSharedMemoryOpenStatus()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCSharedMemoryOpenStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_BUSY: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_DOESNOTEXIST:
		 u32Ret = 1;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 2;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 3;
		 break;
	  default:
		 u32Ret = 4;
   }//end of switch( u32status )

   return u32Ret; 
} 
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCSharedMemoryCloseStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHSharedMemoryCloseStatus()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCSharedMemoryCloseStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_NOPERMISSION: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 2;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 3;
		 break;
	  default:
		 u32Ret = 4;
   }//end of switch( u32status )

   return u32Ret; 
} 


/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateDeleteNameNULL()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with Name NULL
 *				 b)check the error status returned
 *				 c)Delete the shared Memory with Name NULL
 *				 d)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_001
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateDeleteNameNULL()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateDeleteNameNULL(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Name NULL */
   SHHandle = OSAL_SharedMemoryCreate(OSAL_NULL, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)
   
   /* Delete the shared Memory with Name NULL */
   s32OSAL_Ret = OSAL_s32SharedMemoryDelete( OSAL_NULL );
   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
     
      u32status =  OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret += 0;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret += 10;
		    break;
	     case OSAL_E_DOESNOTEXIST:
		    u32Ret += 20;
		    break;
	     default:
		    u32Ret += 30;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )
   else
   {
      u32Ret += 50;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateNameVal()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_002
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateNameVal()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateNameVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;

   /* Create the shared Memory with vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  		/*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
	     		u32Ret += 50;
	  		}
	  }
   }
   else
   {
     	/*Check the error status returned */
     	u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateDeleteNameExceedNameLength()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with name exceed name length 32 
 *				 b)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_003
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateDeleteNameExceedNameLength()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateDeleteNameExceedNameLength(tVoid)
{
   tU32 u32status = 0;
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with name length 40*/
   SHHandle = OSAL_SharedMemoryCreate(SH_INVAL_NAME, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status =  OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_NAMETOOLONG:  /* Expected error */ 
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)

   /* Delete the shared Memory with name length 40*/
   s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_INVAL_NAME );
   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_DOESNOTEXIST:  /* Expected error */   
   	        u32Ret += 0;
		    break;
         case OSAL_E_NAMETOOLONG: 
            u32Ret += 10;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret += 20;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret += 30;
		    break;
	     default:
		    u32Ret += 40;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )
   else
   {
      u32Ret += 100;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) (Delete)

   return u32Ret ;


}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateNameSameNames()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)Create the shared Memory again with same name
 *  			 c)close and delete if shared Memory again with same name
 *				 d)close and delete first shared Memory 
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_004
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateNameSameNames()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateNameSameNames(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;
   OSAL_tShMemHandle SHHandle1 = 0;


   /* Create the shared Memory*/
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {

	  /* Create the shared Memory again with same name*/	
      SHHandle1 = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
	  if( OSAL_ERROR != SHHandle1 )
	  {
	     u32Ret = 60;
	     /*Close the handle */
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle1);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 100;
		 }
		 else
		 {
		 	/*Delete the shared Memory with Name*/
		 	s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
		 	if( OSAL_ERROR == s32OSAL_Ret )
		 	{
		    	u32Ret += 100;
		 	}
		}
	  }
	  else
	  {
      		/*Check the error status returned */
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_ALREADYEXISTS:  /* Expected error */  
		    u32Ret = 0;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 10;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 20;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 30;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 40;
		    break;
	     default:
		    u32Ret = 50;
      		}
	  }
	 	     
	  /*Close the handle */
	  s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 500;
	  }
	  else
	  {
	  /* Delete the shared Memory with Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 1000;
	  	   }
	  }
   }
   else
   {
      /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateSizeZero()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with Name NULL
 *				 b)check the error status returned
 *				 c)Delete the shared Memory with Name NULL
 *				 d)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_005
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateSizeZero()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateSizeZero(tVoid)
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Name SH_NAME */
   SHHandle = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_MIN);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)

   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCDeleteNameInval()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)close and delete shared Memory 
 *				 c)Try to delete invalid shared Memory  
 *				 d)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_006
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHDeleteNameInval()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCDeleteNameInval(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  	 /* Delete the shared Memory with Name */
	  	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	u32Ret += 50;
	  	 }
      
      /* Delete again the shared Memory with same Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
      if( OSAL_ERROR == s32OSAL_Ret )
      {
         	/*Check the error status returned */
         u32status = OSAL_u32ErrorCode();
         switch( u32status )
         {  
            case OSAL_E_DOESNOTEXIST:  /* Expected error */
		       u32Ret += 0;
		       break;
	        case OSAL_E_UNKNOWN:
		       u32Ret += 10;
		       break;
	        case OSAL_E_INVALIDVALUE:
		       u32Ret += 20;
		       break;
	        default:
		       u32Ret += 30;
         	}
   		}
   		else
   		{
      		u32Ret += 100;
   		}
      }
	}
    else
    {
       /* check the error status returned */
       u32Ret = u32SHSharedMemoryCreateStatus();
   	}
   	return u32Ret ;
}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCDeleteNameInUse()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory .
 *				 b)Delete shared Memory with out close.
 *				 c)Delete without close should pass.
 *				 d)On success of Delete, do a close to 
 *				 remove shared memory resources.
 *				 e)On success of close,do a reclose to
 *				 see if resources have actually been removed.
 *				 f)Return the error code.
 *
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_007
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHDeleteNameInUse()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCDeleteNameInUse(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /*Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {

	  /*Delete the shared Memory with Name, with out close*/
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
      if( OSAL_ERROR == s32OSAL_Ret )
      {
         /*Shared memory delete passed before a close*/
      u32Ret = 100;
   	  }
   	  else
   	  {
      	 /*Close the handle,remove shared memory resources*/
      	 s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	 	 {
	  		 /*Should not fail*/
	  		 u32Ret += 100;
	  	 }
	  	 else
	  	 {
	  	 	/*Attempt a Redelete*/
			if( OSAL_OK == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				u32Ret += 2000;
			}
	  	 	/*Attempt a reclose*/
	  	 	if( OSAL_OK == ( s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle) ) )
			{
				u32Ret += 1000;
			}
		 }
	  }
   }
   else
   {
     /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   /*Return the error code*/
   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCOpenCloseDiffModes()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)delete shared Memory with out close
  *				 c)check the error status returned
 *				 d)if not delete, close and delete the shared memory
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_008
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHOpenCloseDiffModes()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCOpenCloseDiffModes(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;
   OSAL_tShMemHandle SHHandle_RW = 0;
   OSAL_tShMemHandle SHHandle_W = 0;
   OSAL_tShMemHandle SHHandle_R = 0;
   OSAL_tShMemHandle SHHandle_Inval = 0;


   /*Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Open the shared memory with OSAL_EN_READWRITE) */ 
      SHHandle_RW = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE);
	  if(OSAL_ERROR == SHHandle_RW )
	  {
	     u32Ret = 100 + u32SHSharedMemoryOpenStatus();
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_READWRITE) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_RW);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret = (200 +u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_WRITEONLY) */ 
      SHHandle_W = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_WRITEONLY);
	  if(OSAL_ERROR == SHHandle_W )
	  {
	     u32Ret += (300 + u32SHSharedMemoryOpenStatus());
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_WRITEONLY) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_W);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += (400 + u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_READONLY) */ 
      SHHandle_R = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READONLY);
	  if(OSAL_ERROR == SHHandle_R )
	  {
	     u32Ret += (500 + u32SHSharedMemoryOpenStatus());
	  }
	  else
	  {
		 /* Close the shared memory with OSAL_EN_READONLY) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_R);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += (600 + u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_BINARY( invalid )) */ 
      SHHandle_Inval = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_BINARY);
	  if(OSAL_ERROR == SHHandle_Inval )
	  {
		 /*Check the error status returned */
		 u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_INVALIDVALUE: 		
			   u32Ret += 0;
			   break;
			case OSAL_E_DOESNOTEXIST:
			   u32Ret += 10;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 20;
			   break;
			case OSAL_E_BUSY:
			   u32Ret += 30;
			   break;
			default:
			   u32Ret += 40;
		   }
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_BINARY( invalid )) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_Inval);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
			/*Check the error status returned */
			u32status = OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION: 
				  u32Ret += 50;
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 60;
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 70;
				  break;
			   default:
				  u32Ret += 80;
			}
		 }
      }

      /*Shutdown - Close the handle*/
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 1000;
	  }
	  else
	  {
	  	 /*Delete the shared Memory with Name*/
	 	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	u32Ret += 1000;
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned*/
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCOpenNameNULL()
 *
 *DESCRIPTION	:
 *				 a)open the shared Memory name NULL
 *				 b)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_009
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHOpenNameNULL()
 *
 *****************************************************************************/
tU32  u32SHnoIOSCOpenNameNULL( tVoid )    
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;

   /* open the shared memory with OSAL_EN_READWRITE ) */ 
   SHHandle = OSAL_SharedMemoryOpen( OSAL_NULL, OSAL_EN_READWRITE);
   if(OSAL_ERROR == SHHandle )
   {
      /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE: 		
	  	    u32Ret = 0;
			break;
		 case OSAL_E_DOESNOTEXIST:
			u32Ret = 1;
			break;
		 case OSAL_E_UNKNOWN:
			u32Ret = 2;
			break;
		 case OSAL_E_BUSY:
			u32Ret = 3;
			break;
		 default:
			u32Ret = 4;
	  }//end of switch( u32status )
   }//end of if(OSAL_ERROR == SHHandle )
   else
   {
      u32Ret = 10;

   }//end of if-else (OSAL_ERROR == OSAL_EN_READWRITE )
   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCOpenNameInVal()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *   			 d)open after delete  shared Memory
 * 				 e)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_010
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHOpenNameInVal()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCOpenNameInVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with  vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Shutdown - Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  	/*Delete the shared Memory with Name */
	  	s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	if( OSAL_ERROR == s32OSAL_Ret )
	  	{
	    	 u32Ret += 50;
	  	}
	  }

	  /*Open the shared memory with OSAL_EN_READWRITE )*/ 
	  SHHandle = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE);
	  if(OSAL_ERROR == SHHandle )
	  {
	     /*Check the error status returned */
	     u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_DOESNOTEXIST: 	  	
		  	   u32Ret += 0;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret += 60;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 70;
			   break;
			case OSAL_E_BUSY:
			   u32Ret += 80;
			   break;
			default:
			   u32Ret += 90;
		 }
	  }
	  else
	  {
	     u32Ret += 100;

		 /*Attempt a Shutdown - Close the handle */
      	 s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	    	 u32Ret = 10;
	  	 }
	  	 else
	  	 {
	  		/*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
	    	 	u32Ret += 50;
	  		}
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCloseNameInVal()
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *   			 d)close after delete  shared Memory
 * 				 e)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_011
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCloseNameInVal()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCloseNameInVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with  vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  /* Delete the shared Memory with Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 50;
	  	 }
	  }

	  /*Close the shared memory*/ 
	   s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if(OSAL_ERROR == s32OSAL_Ret )
	  {
	     /*Check the error status returned */
	     u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_INVALIDVALUE: 	  	
		  	   u32Ret += 0;
			   break;
			case OSAL_E_NOPERMISSION:
			   u32Ret += 60;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 70;
			   break;
			default:
			   u32Ret += 90;
		 }
	  }
	  else
	  {
	     u32Ret += 100;

		 /*Shutdown - Delete the shared Memory with Name */
	  	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	 u32Ret += 50;
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned */
      u32Ret = u32SHSharedMemoryCreateStatus();
   }

   /*Return error code*/
   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateMaxSegment()
 *
 *DESCRIPTION	:
 *				 a)Create share memory segments
 *				 b)close and delete shared Memory segments
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_012
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHCreateMaxSegment()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateMaxSegment(tVoid)
{
   tChar SH_name[ SH_HARRAY ][ SH_HARRAY ];
   tS32 s32OSAL_Ret = 0;
   tS32 s32Count = 0; 
   tS32 s32Count1 = 0; 
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle[SH_HARRAY] = {0};


   OSAL_pvMemorySet(SH_name,0,sizeof(SH_name));

   /* Create  MAX No of shared Memory  */
   for( ; s32Count < SH_MAX; ++s32Count )
   {
   		 
      OSAL_s32PrintFormat( SH_name[ s32Count ], "SH_NAME%d", s32Count );

   	  /* Create the shared Memory with vaild Size */
      SHHandle[ s32Count ] = OSAL_SharedMemoryCreate( 
      												 SH_name[ s32Count ], 
      												 OSAL_EN_READWRITE, 
      												 SH_MEMSIZE 
      												);
   	  if( OSAL_ERROR  ==  SHHandle[ s32Count ] )
	  {
	     /* check the error status returned */
		 s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
		 if( s32OSAL_Ret  == (tS32)OSAL_E_NOSPACE )	
		 {										   
		    u32Ret = 10;
		 }
		 else
		 {
		    u32Ret = 50;
         }//end of if( OSAL_E_NOSPACE  == s32OSAL_Ret )
						 
		 break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )
   }

   --s32Count;

   /* close and Delete the shared Memory   */
   for( s32Count1 = s32Count; SH_MIN <= s32Count1; --s32Count1 )
   {
   		 
     /* close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle[ s32Count1 ]);
   	  if( OSAL_ERROR  == s32OSAL_Ret )
	  {
	     u32Ret += 100;
         break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )

   	  /* Create the shared Memory with vaild Size */
      s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_name[ s32Count1 ]);
   	  if( OSAL_ERROR  == s32OSAL_Ret )
	  {
	     u32Ret += 200;
         break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )
   }

   return u32Ret ;

}


/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryMapNonExistentHandle()
 *DESCRIPTION	:Try Memory Map on a non existent Shared Memory( Created in 
                 the system and then deleted ) and on an invalid shared memory
				 handle			  
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_013
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryMapNonExistentHandle()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryMapNonExistentHandle( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;
	OSAL_tShMemHandle hShared_ = INVAL_HANDLE;

	/*Scenario 1*/
	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : u32Ret = 100;break;
			case OSAL_E_NAMETOOLONG  : u32Ret = 200;break;
			case OSAL_E_UNKNOWN      : u32Ret = 300;break;
			case OSAL_E_ALREADYEXISTS: u32Ret = 400;break;
			case OSAL_E_NOSPACE      : u32Ret = 500;break;
			default                  : u32Ret = 600;
		}

		/*Reset the error code*/
		u32ErrorValue = OSAL_E_NOERROR;
	}
	else
	{
		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret = 700;break;
				case OSAL_E_UNKNOWN     : u32Ret = 800;break;
				case OSAL_E_NOPERMISSION: u32Ret = 900;break;
				default                 : u32Ret = 1000;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret = 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret = 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret = 1300;break;
					default                 : u32Ret = 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
				/*Attempt a Memory Map!*/ 
				if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                                          MAP_LENGTH,MAP_OFFSET ) )
				{
					/*Valid behaviour, Memory Map should fail,the error code
					must be OSAL_E_DOESNOTEXIST!*/
					u32ErrorValue = OSAL_u32ErrorCode( );

					if( OSAL_E_DOESNOTEXIST != u32ErrorValue )
					{
						u32Ret += 1500;
					}

					/*Reset the error code*/
					u32ErrorValue = OSAL_E_NOERROR;
				}
				else
				{
					/*Memory Map returned the pointer,Critical API failure!*/
					u32Ret += 10000;
				}
			}/*Delete successful*/
		}/*Close successful*/
	}/*Create successful*/
	/*Scenario 1*/

	/*Scenario 2*/
	/*Attempt a Memory Map on an invalid handle!*/ 
	if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared_,OSAL_EN_READWRITE,
				                              MAP_LENGTH,MAP_OFFSET ) )
	{
		/*Valid behaviour, Memory Map should fail,the error code
		must be OSAL_E_INVALIDVALUE!*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		if( OSAL_E_INVALIDVALUE != u32ErrorValue )
		{
			u32Ret += 2500;
		}

		/*Reset the error code*/
		u32ErrorValue = OSAL_E_NOERROR;
	}
	else
	{
		/*Memory Map returned the pointer,Critical API failure!*/
	 	u32Ret += 20000;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryMapLimitCheck()
 *DESCRIPTION	:Try Memory Map at the Limit os Shared Memory Size and
                 beyond that limit
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_014
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryMapLimitCheck()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryMapLimitCheck( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;

	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : 
			{
				
				return 100;
			}
			case OSAL_E_NAMETOOLONG  : 
			{
				
				return 200;
			}
			case OSAL_E_UNKNOWN      : 
			{
				
				return 300;
			}
			case OSAL_E_ALREADYEXISTS: 
			{
				
				return 400;
			}
			case OSAL_E_NOSPACE      :
			{
				
				 return 500;
			 }
			default                  : 
			{
				
				return 600;
			}
		}

		
	}
	else
	{
		/*At the end of Shared Memory*/
		if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	          ZERO_LENGTH,SH_MEMSIZE )  )
		{
			/*API failed!*/
			u32Ret += 900;
		}
		
		/*Beyond Limit of Shared Memory*/
		if( OSAL_NULL ==  OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	          MAP_LENGTH,SH_MEMSIZE )  )
		{
		   /*Correct Behaviour - Query the error code*/
		   u32ErrorValue = OSAL_u32ErrorCode( );
		   
		   if( 	OSAL_E_NOSPACE != u32ErrorValue )
		   {
		   		u32Ret += 1000;
		   }   
		}
		else
		{
			/*Critical API error!*/
			u32Ret += 10000;
		}

		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret += 700;break;
				case OSAL_E_UNKNOWN     : u32Ret += 800;break;
				case OSAL_E_NOPERMISSION: u32Ret += 950;break;
				default                 : u32Ret += 1050;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret += 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret += 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret += 1300;break;
					default                 : u32Ret += 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
		}/*Close successful*/
	}/*Create successful*/
	
	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryMapDiffAccessModes()
 *DESCRIPTION	:Try Memory Map on a existent Shared Memory,for different 
                 access modes
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_015
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryMapDiffAccessModes()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryMapDiffAccessModes( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;
	tPVoid ptr1                = OSAL_NULL;
	tPVoid ptr2                = OSAL_NULL;
	tPVoid ptr3                = OSAL_NULL;

	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : return 100;
			case OSAL_E_NAMETOOLONG  : return 200;
			case OSAL_E_UNKNOWN      : return 300;
			case OSAL_E_ALREADYEXISTS: return 400;
			case OSAL_E_NOSPACE      : return 500;
			default                  : return 600;
		}

		
	}
	else
	{
		/*MEMORY MAP WITH MODE OSAL_EN_READONLY*/
		if( OSAL_NULL == ( ptr1 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READONLY,
				                              	          MAP_LENGTH,MAP_OFFSET ) ) )
		{
		   /*Memory Map returned NULL*/
	 		u32Ret += 10000;	
		}
		
		/*MEMORY MAP WITH MODE OSAL_EN_WRITEONLY*/
		if( OSAL_NULL == ( ptr2 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_WRITEONLY,
				                              	 		  MAP_LENGTH,MAP_OFFSET ) ) )
	    {
		    /*Memory Map returned NULL*/
	 		u32Ret += 20000;	
		}

		/*MEMORY MAP WITH MODE OSAL_EN_READWRITE*/
		if( OSAL_NULL == ( ptr3 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	 		  MAP_LENGTH,MAP_OFFSET ) ) )
		{
		    /*Memory Map returned NULL*/
	 		u32Ret += 30000;	
		}

		/*Check Address Returned - Should not be dependant on Access Modes.*/
		if( ptr1 == ptr2 )
		{
			if( !( ptr2 == ptr3 ) )
			{
				/*ptr2 and ptr3 not equal!*/
				u32Ret += 40000;
			}
		}
		else
		{
			/*ptr1 and ptr2 not equal!*/
			u32Ret += 50000;
		}

		/*MEMORY MAP WITH MODE OSAL_INVALID_ACCESS*/
		if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,(OSAL_tenAccess)OSAL_INVALID_ACCESS,
				                              	 MAP_LENGTH,MAP_OFFSET ) )
		{
			/*Valid behaviour, Memory Map should fail,the error code
			must be OSAL_E_INVALIDVALUE!*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			if( OSAL_E_INVALIDVALUE != u32ErrorValue )
			{
				u32Ret += 800;
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Memory Map returned the pointer,Critical API failure!*/
	 		u32Ret += 60000;
		}

		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret += 700;break;
				case OSAL_E_UNKNOWN     : u32Ret += 800;break;
				case OSAL_E_NOPERMISSION: u32Ret += 950;break;
				default                 : u32Ret += 1050;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret += 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret += 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret += 1300;break;
					default                 : u32Ret += 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
		}/*Close successful*/
	}/*Create successful*/

	/*Return the error code*/
	return u32Ret;
}
		
	
/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryThreadAccess
 *DESCRIPTION	:Try to access Shared memory in different threads of control.
				 Should be able to address shared memory and access it.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_016
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryThreadAccess()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryThreadAccess( tVoid )
{
	/*DEFINITIONS*/

	/*General*/
	tU32 u32Ret                		= 0;
	/*Thread IDs*/
	OSAL_tThreadID tID_1            = 0;
	OSAL_tThreadID tID_2            = 0;
	OSAL_tThreadID tID_3            = 0;
	OSAL_tThreadID tID_4            = 0;
	/*Thread Attributes*/
	OSAL_trThreadAttribute trAtr_1  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_2  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_3  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_4  = {OSAL_NULL};
	/*Thread return error codes*/
	tU32 u32ThrEC_1            		= 0;
	tU32 u32ThrEC_2            		= 0;
	tU32 u32ThrEC_3            		= 0;
	tU32 u32ThrEC_4            		= 0;
	/*Shared memory Pointer*/
	tVoid * shptr                   = OSAL_NULL;
	gevMask = 0;
	/*INITIALIZATIONS*/

	/* FAN4HI: 2011 11 11 - Renamed threads to avoid overlap with test 000.021 */

	/*Fill the Thread "Thread_SH_NoIOSC_0_127" Attributes*/
	trAtr_1.szName                 	=  "Thread_SH_NoIOSC_0_127";
	trAtr_1.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_1.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_1.pfEntry                	=  Thread_SH_0_127;
	trAtr_1.pvArg                  	=  &u32ThrEC_1;

	/*Fill the Thread "Thread_SH_NoIOSC_128_255" Attributes*/
	trAtr_2.szName                 	=  "Thread_SH_NoIOSC_128_255";
	trAtr_2.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_2.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_2.pfEntry                	=  Thread_SH_128_255;
	trAtr_2.pvArg                  	=  &u32ThrEC_2;

	/*Fill the Thread "Thread_SH_NoIOSC_256_383" Attributes*/
	trAtr_3.szName                 	=  "Thread_SH_NoIOSC_256_383";
	trAtr_3.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_3.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_3.pfEntry                	=  Thread_SH_256_383;
	trAtr_3.pvArg                  	=  &u32ThrEC_3;

	/*Fill the Thread "Thread_SH_NoIOSC_384_511" Attributes*/
	trAtr_4.szName                 	=  "Thread_SH_NoIOSC_384_511";
	trAtr_4.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_4.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_4.pfEntry                	=  Thread_SH_384_511;
	trAtr_4.pvArg                  	=  &u32ThrEC_4;
								 

	/*EVENT CREATE*/
	if( OSAL_ERROR != OSAL_s32EventCreate( "SH_MEM_EVENT",&gSHEventHandle ) )
	{
		/*CREATE OSAL RESOURCES*/

		/*Create the Shared Memory*/
		if( OSAL_ERROR != ( ghShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512 ) ) )
		{
			/*Spawn Thread Thread_SH_0_127*/
			if( OSAL_ERROR != ( tID_1 = OSAL_ThreadSpawn( &trAtr_1 ) ) )
			{
				/*Spawn Thread Thread_SH_128_255*/
				if( OSAL_ERROR != ( tID_2 = OSAL_ThreadSpawn( &trAtr_2 ) ) )
				{
					/*Spawn Thread Thread_SH_256_383*/
					if( OSAL_ERROR != ( tID_3 = OSAL_ThreadSpawn( &trAtr_3 ) ) )
					{
						/*Spawn Thread Thread_SH_384_511*/
						if( OSAL_ERROR != ( tID_4 = OSAL_ThreadSpawn( &trAtr_4 ) ) )
						{
						    /*4 THREAD EVENT WAIT*/
							if( OSAL_ERROR == OSAL_s32EventWait( gSHEventHandle, 
											 					 PATTERN_THR1|PATTERN_THR2|PATTERN_THR3|PATTERN_THR4,
		                   					 					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					 					 &gevMask ) )
							{
								u32Ret += 6000;
							}	
							/*Clear the event*/
							OSAL_s32EventPost
							( 
							gSHEventHandle,
							~(gevMask),
						   OSAL_EN_EVENTMASK_AND
						    );
	
						}
						else
						{
							/*3 THREAD EVENT WAIT ( 1,2,3 Passed )*/
							OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1|PATTERN_THR2|PATTERN_THR3,
		                   					 	OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					 	&gevMask );

							/*Thread Thread_SH_384_511 spawn failed*/
							u32Ret += 1000;
						}
						/*Clear the event*/
						OSAL_s32EventPost
						( 
						gSHEventHandle,
						~(gevMask),
					   OSAL_EN_EVENTMASK_AND
					    );

					}
					else
					{
						/*2 THREAD EVENT WAIT( 1,2 Passed )*/
						OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1|PATTERN_THR2,
		                   					OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					&gevMask );

						/*Clear the event*/
						OSAL_s32EventPost
						( 
						gSHEventHandle,
						~(gevMask),
					   OSAL_EN_EVENTMASK_AND
					    );

						/*Thread Thread_SH_256_383 spawn failed*/
						u32Ret += 2000;
					} 
				}
				else
				{
					/*1 THREAD EVENT WAIT( 1 Passed )*/
					OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1,
		                   				OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   				&gevMask );
					/*Clear the event*/
					OSAL_s32EventPost
					( 
					gSHEventHandle,
					~(gevMask),
				   OSAL_EN_EVENTMASK_AND
				    );

					/*Thread Thread_SH_128_255 spawn failed*/
					u32Ret += 3000;
				}
			}
			else
			{
				/*Thread Thread_SH_0_127 spawn failed*/
				u32Ret += 4000;
			}
		}
		else
		{
			/*Shared Memory Create failed*/
			u32Ret += 10000;
		}

		/*Check for data in shared memory*/
		if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            							   ( SH_MEMSIZE_512-1 ),OFFSET_0 ) ) )
		{
			if( ( *( ( tChar * )shptr + 0 ) != 'a' )	|| ( *( ( tChar * )shptr + 127 ) != 'a' ) )
			{
				u32Ret += 127; /*Failure of first mapping*/
			}

			if( ( *( ( tChar * )shptr + 128 ) != 'b' )	|| ( *( ( tChar * )shptr + 255 ) != 'b' ) )
			{
				u32Ret += 254; /*Failure of second mapping*/
			}

			if( ( *( ( tChar * )shptr + 256 ) != 'c' )	|| ( *( ( tChar * )shptr + 383 ) != 'c' ) )
			{
				u32Ret += 381; /*Failure of third mapping*/
			}

			if( ( *( ( tChar * )shptr + 384 ) != 'd' )	|| ( *( ( tChar * )shptr + 510 ) != 'd' ) )
			{
				u32Ret += 507; /*Failure of fourth mapping*/
			}
		}

		
		/*REMOVE OSAL RESOURCES*/

		/*Delete Thread Thread_SH_0_127*/ 
		/* FAN4HI: 2011 11 11 - ThreadIDs in LSim might be < 0 */
		if( tID_1 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_1 ) )
			{
				u32Ret += 7000;
			}
		}

		/*Delete Thread Thread_SH_128_255*/ 
        if( tID_2 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_2 ) )
			{
				u32Ret += 8000;
			}
		}

		/*Delete Thread Thread_SH_256_383*/ 
        if( tID_3 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_3 ) )
			{
				u32Ret += 9000;
			}
		}

		/*Delete Thread Thread_SH_384_511*/ 
        if( tID_4 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_4 ) )
			{
				u32Ret += 11000;
			}
		}

		/*Close and Delete the Event*/
		if( OSAL_ERROR != OSAL_s32EventClose( gSHEventHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( "SH_MEM_EVENT" ) )
			{
				u32Ret += 12000;
			}
		}
		else
		{
			u32Ret += 13000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != ghShared )
		{
			if( OSAL_ERROR != OSAL_s32SharedMemoryClose( ghShared ) )
			{
				if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
				{
					u32Ret  += 14000;
				}
			}
			else
			{
				u32Ret += 15000;
			}
		}
		
		/*Reinitialize the Global Variables*/
		gSHEventHandle = 0;
		ghShared       = 0;	

	}
	else
	{
		/*Reinitialize the Global variables*/
		gSHEventHandle = 0;
		return 50000;
	}

	/*RETURN ERROR CODE*/
	return ( u32Ret+u32ThrEC_1+u32ThrEC_2+u32ThrEC_3+u32ThrEC_4 );

}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryScanMemory
 *DESCRIPTION	:Try to access Shared memory upto allocated length.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_017
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryScanMemory()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryScanMemory( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	tU32 u32Offset            = ZERO_OFFSET;
	#if DEBUG_MODE
	tU32 u32ECode             = OSAL_E_NOERROR;
	#endif
	tChar tcSeeker            = '\0';
	OSAL_tShMemHandle hShared = 0;
	tVoid * ptr               = OSAL_NULL;

	/*Create Shared Memory*/
	if( OSAL_ERROR != (tS32)( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512 ) ) )
	{
		/*Initialize the Shared Memory*/
		if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            	  					 ZERO_LENGTH,ZERO_OFFSET ) ) )
		{
			OSAL_pvMemorySet( ptr,'\0',SH_MEMSIZE_512 );
			OSAL_pvMemorySet( ptr,'A',( SH_MEMSIZE_512-1 ) );
		}
		else
		{
			/*Query reasons for Map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Restore resource*/
			if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
			{
			   if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			   {
			   		/*Query reasons for Delete failure*/
			   		#if DEBUG_MODE
					u32ECode = OSAL_u32ErrorCode( );
					#endif 		
			   }
			}
			else
			{
				/*Query reasons for Close failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}

			/*Return Immediately*/	
			return 10000;
		}
		
		/*Scan the Shared Memory*/
		while( u32Offset < ( SH_MEMSIZE_512-1 ) )
		{
			/*Scan Memory - Return address of location u32Offset bytes
			from Shared Memory Base*/
			if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            							 ZERO_LENGTH,u32Offset ) ) )
			{
				/*Access Shared Memory Location contents*/
				tcSeeker = *( (tChar *)ptr );
				/*Check for content*/
				if( tcSeeker != 'A' )
				{
					u32Ret += 100;
				}

			}
			else
			{
				/*Query reasons for map failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Set error code*/
				u32Ret += 10;
			}

			/*Increment offset*/
			u32Offset++;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
	}
	else
	{
		/*Return Immediately*/
		return 20000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCMemoryScanMemoryBeyondLimit
 *DESCRIPTION	:Try to access Shared memory upto allocated length.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_018
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHMemoryScanMemoryBeyondLimit()
 *
*****************************************************************************/
tU32 u32SHnoIOSCMemoryScanMemoryBeyondLimit( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			  = 0;
	#if DEBUG_MODE
	tVoid * ptr               = OSAL_NULL;
	tU32 u32ECode             = OSAL_E_NOERROR;
	tChar tcSeeker            = '\0';
	#endif
	OSAL_tShMemHandle hShared = 0;

	/*Create the Shared Memory*/
	if( OSAL_ERROR !=(tS32) ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE ) ) )
	{
		/*Map the shared memory*/
		//rav8kor - only commented and not removed as it can be used in debug mode
		//if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            						// ZERO_LENGTH,SH_SIZE-2 ) ) )
		if( OSAL_NULL != OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            						 ZERO_LENGTH,SH_SIZE-2 ) )
		{
		 #if DEBUG_MODE	
			tcSeeker = *( (tChar * )( ((tChar*)ptr) + SH_SIZE ) );
		 #endif
		}
		else
		{
			/*Query reason for Map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
			
			/*Update error code*/
			u32Ret += 10000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
 
	}
	else
	{
		/*Query reason for Shared Memory Create failure*/
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	

		/*Update error code*/
		u32Ret += 20000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCUnmapNonMappedAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error( Error code should be
				 OSAL_E_DOESNOTEXIST )
 *TEST CASE		:TU_OEDT_SH_019
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32UnmapNonMappedAddress()
 *
*****************************************************************************/
tU32 u32SHnoIOSCUnmapNonMappedAddress( tVoid )
{
	/*Declaration*/
	tU32 u32Ret = 0;
	tVoid * ptr = OSAL_NULL;

	/*Allocate memory dynamically*/
	ptr = OSAL_pvMemoryAllocate( (tU32)1 );

	/*Unmap a non mapped location*/
	if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( ptr, 1 ) )
	{
		/*Query the error code*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 5000;
		}
	}
	else
	{
		/* Error, unmap will pass with all non zero adderss 
		  this is known limitation of osal */
		//u32Ret += 10000;
	}

	if (ptr != OSAL_NULL) 
	{
		OSAL_vMemoryFree(ptr);
		ptr = OSAL_NULL;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCUnmapNULLAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error( Error code should be
				 OSAL_E_INVALIDVALUE )
 *TEST CASE		:TU_OEDT_SH_020
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32UnmapNULLAddress()
 *
*****************************************************************************/
tU32 u32SHnoIOSCUnmapNULLAddress( tVoid )
{
	/*Declaration*/
	tU32 u32Ret = 0;
   
	/*Unmap a non mapped location*/
	if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( OSAL_NULL, 1 ) )
	{
		/*Query the error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 5000;
		}
	}
	else
	{
		/*Error, unmap should not pass if address passed is NULL*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCUnmapMappedAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_021
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32UnmapMappedAddress()
 *
*****************************************************************************/
tU32 u32SHnoIOSCUnmapMappedAddress( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			  = 0;
	tVoid * ptr               = OSAL_NULL;
	#if DEBUG_MODE
	tU32 u32ECode             = OSAL_E_NOERROR;
	#endif
	OSAL_tShMemHandle hShared = 0;

	/*Create the shared memory*/
	if( OSAL_ERROR !=(tS32) ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE ) ) )
	{
		/*Map shared memory*/
		if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,SH_SIZE,0 ) ) )
		{
			/*Unmap shared memory already mapped*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( ptr,SH_SIZE ) )
			{
				/*Query reasons for Unmap failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Set error code*/
				u32Ret += 10000;
			}
		}
		else
		{
			/*Query reasons for map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Cannot Map Memory*/
			u32Ret += 5000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
	}
	else
	{
		/*Query reasons for Shared Memory Create failure*/
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Update error code*/
		u32Ret += 7000;
	}

	/*Return the error code*/
	return u32Ret;
}
			
/******************************************************************************
 *FUNCTION     :u32SHnoIOSCOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
 *
 * HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of u32SHOpenDoubleCloseOnce()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   tS32 s32ReturnValueThread = 0;
   OSAL_tShMemHandle handle_1 = 0;
   tPVoid map_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) Thread_SH_Memory;
   threadAttr.pvArg = &s32ReturnValueThread;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   handle_1 = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if (handle_1 != OSAL_ERROR)
   {
      if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
      {
         OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
         if ((map_1 = OSAL_pvSharedMemoryMap(handle_1, OSAL_EN_READWRITE, SH_SIZE, 0)) != 0)
         {
            if ((OSAL_pvMemorySet(map_1, 0, SH_SIZE) && OSAL_pvMemorySet(map_1, 1, SH_SIZE)))
            {
               if (OSAL_s32SharedMemoryUnmap(map_1, SH_SIZE) != OSAL_ERROR)
               {
                  if (OSAL_s32SharedMemoryClose(handle_1) != OSAL_ERROR)
                  {
                     if (OSAL_s32SharedMemoryDelete(SH_NAME) != OSAL_ERROR)
                     {
                        // test ok
                     }
                     else
                        s32ReturnValue += 700;
                  }
                  else
                     s32ReturnValue += 600;
               }
               else
                  s32ReturnValue += 500;
            }
            else
               s32ReturnValue += 400;
         }
         else
            s32ReturnValue += 300;
      }
      else
         s32ReturnValue += 200;
   }
   else
      s32ReturnValue += 100;

   return s32ReturnValue;
}

/******************************************************************************
 *FUNCTION		:u32SHnoIOSCCreateNameValLoop()
 *
 *DESCRIPTION	:
 *				 a) Create the shared Memory
 *				 b) check the error status returned 
 *				 c) close and delete shared Memory
 *         d) go to a) and run everything in a loop
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_002
 *
 *
 * HISTORY      :19.01.2012 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				       copy of u32SHCreateNameVal()
 *
 *****************************************************************************/
tU32 u32SHnoIOSCCreateNameValLoop(tVoid)
{
  tS32 s32OSAL_Ret = 0;
  tU32 u32Ret = 0;
  OSAL_tShMemHandle SHHandle = 0;
  int loop = 0;
   
  for (loop=0; loop< 5000; loop++)
  {
    s32OSAL_Ret = 0;
    u32Ret = 0;
    SHHandle = 0;
   
    /* Create the shared Memory with vaild Size */
    SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
    
    if( OSAL_ERROR != SHHandle )
    {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
      
      if( OSAL_ERROR == s32OSAL_Ret )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32SharedMemoryClose() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
        u32Ret = 10;
      }
      else
      {
        /*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
          OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32SharedMemoryDelete() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
          u32Ret += 50;
	  		}
      }
    }  
    else
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_SharedMemoryCreate() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
     	/*Check the error status returned */
     	u32Ret = u32SHSharedMemoryCreateStatus();
    }
  
    if (u32Ret != 0)
    {
      return u32Ret;
    }
  }
  
  return u32Ret;
}

