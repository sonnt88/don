/******************************************************************************
 *FILE         : oedt_osalcore_FPE_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the FPE OSAL API.
 *               
 *AUTHOR       : Ravindran P(RBEI/ECF1)
 *
 *
 *COPYRIGHT    : 2011, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : Version 1.0 , 31/01/2011
 *               Version 1.1 , 08/08/2011 - Anooj Gopi (RBEI/ECF1)
 *               - Added abort mode tests. Updated dual-thread tests.
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_helper_funcs.h"
#include "oedt_osalcore_FPE_TestFuncs.h"

/*****************************************************************
| Global Variables
|----------------------------------------------------------------*/

/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/

/*Defines*/
/* Signal for main thread */
#define FP_THREAD_EV_MAIN_SIGT1 0x0001
#define FP_THREAD_EV_MAIN_SIGT2 0x0002
#define FP_THREAD_EV_MAIN_SIG_T1T2 (FP_THREAD_EV_MAIN_SIGT1 | FP_THREAD_EV_MAIN_SIGT2)
/* Signal for Thread 1 */
#define FP_THREAD_EV_T1_SIG1      0x0002
#define FP_THREAD_EV_T1_SIG2      0x0004
/* Signal for Thread 2 */
#define FP_THREAD_EV_T2_SIG1      0x0008
#define FP_THREAD_EV_T2_SIG2      0x0010
/* single thread event */
#define FP_THREAD_EV_SINGLE       0x0011

#define FP_THREAD_EV_WAIT_TIMEOUT 2000 /* Value in msec */

typedef struct
{
   tU32              u32RetValT1;
   tU32              u32RetValT2;
   OSAL_tEventHandle hEventHandleMain;
   OSAL_tEventHandle hEventHandleT1;
   OSAL_tEventHandle hEventHandleT2;
}trMultiThreadArgs;

typedef struct
{
   tU32              u32RetVal;
   OSAL_tEventHandle hEventHandle;
}trSingleThreadArgs;

static tVoid vFPZeroDivAbortThread(tPVoid pvArg);
static tVoid vFPOverflowAbortThread(tPVoid pvArg);
static tVoid vFPUnderflowAbortThread(tPVoid pvArg);
static tVoid vFPInvalidAbortThread(tPVoid pvArg);
static tVoid vResetMultithread(tPVoid pvArg);
static tVoid vResetMultithreadExt(tPVoid pvArg);
static tVoid vSetMultithread(tPVoid pvArg);
static tVoid vSetMultithreadExt(tPVoid pvArg);
static tVoid vModeSetMultithread(tPVoid pvArg);
static tVoid vModeSetMultithreadExt(tPVoid pvArg);
static tVoid vDefaultModeCheckThread(tPVoid pvArg);
static tU32 u32FPESingleThreadTest( tCString , OSAL_tpfThreadEntry );
static tU32 u32FPEDualThreadTest( tCString, tCString, OSAL_tpfThreadEntry , OSAL_tpfThreadEntry );

/*****************************************************************************
* FUNCTION     :  vFPZeroDivAbortThread()
* PARAMETER    :  Pointer to thread argument structure.
* RETURNVALUE  :  none
* DESCRIPTION  :  Thread to test divide by zero exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vFPZeroDivAbortThread(tPVoid pvArg)
{
   trSingleThreadArgs *prTArgs = (trSingleThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */
   volatile tDouble dDividend=2.0;
   volatile tU32 u32Zero = 0;
   volatile tDouble dResult = 0.0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;

   OSAL_vFPEReset();

   if( OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode) )
   {
      u32Retval = 1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPZeroDivAbortThread: SetFPEMode failed\n");
   }
   else
   {
      /*Do FP divide by 0*/
      /*lint -e414 */
      dResult = dDividend/u32Zero;

      /* Control should not come here as we are in abort mode */
      u32Retval = 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPZeroDivAbortThread: Abort did not happen!!!");
   }

   /* Put the return value in the argument structure before signaling main thread */
   prTArgs->u32RetVal = u32Retval;

   /* Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prTArgs->hEventHandle,
         FP_THREAD_EV_SINGLE, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPZeroDivAbortThread: Event post to main failed");
      prTArgs->u32RetVal += 10;
   }

   /* Suppress warning*/
   (tVoid)dResult;

   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPZeroDivAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create divide by zero exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
tU32 u32CheckFPZeroDivAbort(tVoid)
{
   tU32 u32RetValue;

   u32RetValue = u32FPESingleThreadTest("FPE_ZeroDivAbort", vFPZeroDivAbortThread);

   if( u32RetValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPZeroDivAbort : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPZeroDivAbort : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32RetValue;
}

/*****************************************************************************
* FUNCTION     :  vFPOverflowAbortThread()
* PARAMETER    :  Pointer to thread argument structure.
* RETURNVALUE  :  none
* DESCRIPTION  :  Thread to test overflow exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vFPOverflowAbortThread(tPVoid pvArg)
{
   trSingleThreadArgs *prTArgs = (trSingleThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */
   volatile float fNo=2.0;
   volatile tDouble dNo=1e40;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;

   OSAL_vFPEReset();

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      u32Retval = 1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPOverflowAbortThread: SetFPEMode failed\n");
   }
   else
   {
      /*Do overflow operation*/
      /*lint -e736*/
      fNo = dNo;

      /* Control should not come here as we are in abort mode */
      u32Retval = 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPOverflowAbortThread: Abort did not happen!!!");
   }

   /* Put the return value in the argument structure before signaling main thread */
   prTArgs->u32RetVal = u32Retval;

   /* Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prTArgs->hEventHandle,
         FP_THREAD_EV_SINGLE, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPOverflowAbortThread: Event post to main failed");
      prTArgs->u32RetVal += 10;
   }

   /* Suppress warning*/
   (tVoid)fNo;

   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPOverflowAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Overflow exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
tU32 u32CheckFPOverflowAbort(tVoid)
{
   tU32 u32RetValue;

   u32RetValue = u32FPESingleThreadTest("FPOverflowAbort", vFPOverflowAbortThread);

   if( u32RetValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPOverflowAbort : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPOverflowAbort : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32RetValue;
}

/*****************************************************************************
* FUNCTION     :  vFPUnderflowAbortThread()
* PARAMETER    :  Pointer to thread argument structure.
* RETURNVALUE  :  none
* DESCRIPTION  :  Thread to test underflow exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vFPUnderflowAbortThread(tPVoid pvArg)
{
   trSingleThreadArgs *prTArgs = (trSingleThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */
   volatile float fNo=2.0;
   volatile tDouble dNo=1e40;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPUnderflowAbortThread: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      OSAL_vFPEReset();
      /*Do Underflow operation*/
      fNo = fNo/dNo;

      /* Control should not come here as we are in abort mode */
      u32Retval = 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPUnderflowAbortThread: Abort did not happen!!!");
   }

   /* Put the return value in the argument structure before signaling main thread */
   prTArgs->u32RetVal = u32Retval;

   /* Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prTArgs->hEventHandle,
         FP_THREAD_EV_SINGLE, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPUnderflowAbortThread: Event post to main failed");
      prTArgs->u32RetVal += 10;
   }

   /* Suppress warning*/
   (tVoid)fNo;

   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPUnderflowAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Underflow exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
tU32 u32CheckFPUnderflowAbort(tVoid)
{
   tU32 u32RetValue;

   u32RetValue = u32FPESingleThreadTest("FPUnderflowAbort", vFPUnderflowAbortThread);

   if( u32RetValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPUnderflowAbort : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPUnderflowAbort : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32RetValue;
}

/*****************************************************************************
* FUNCTION     :  vFPInvalidAbortThread()
* PARAMETER    :  Pointer to thread argument structure.
* RETURNVALUE  :  none
* DESCRIPTION  :  Thread to test invalid exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vFPInvalidAbortThread(tPVoid pvArg)
{
   trSingleThreadArgs *prTArgs = (trSingleThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */
   tDouble dNo = -20.0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;
   tDouble dVal = 0;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPInvalidAbortThread: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      OSAL_vFPEReset();
      /*Sqrt of -ve number creates a NaN*/
      dVal = sqrt(dNo);
      /*Comparison of a NaN with Number is a Invalid Operation*/
      if( dVal <= 0 )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"FPInvalidAbortThread: Invalid operation");
      }

      /* Control should not come here as we are in abort mode */
      u32Retval = 2;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPInvalidAbortThread: Abort did not happen!!!");
   }

   /* Put the return value in the argument structure before signaling main thread */
   prTArgs->u32RetVal = u32Retval;

   /* Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prTArgs->hEventHandle,
         FP_THREAD_EV_SINGLE, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPInvalidAbortThread: Event post to main failed");
      prTArgs->u32RetVal += 10;
   }

   /* suppress warning */
   (tVoid)dVal;

   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPInvalidAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Invalid exception in abort mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
tU32 u32CheckFPInvalidAbort(tVoid)
{
   tU32 u32RetValue;

   u32RetValue = u32FPESingleThreadTest("CheckFPInvalidAbort", vFPInvalidAbortThread);

   if( u32RetValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPInvalidAbort : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPInvalidAbort : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32RetValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPZeroDivNoAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create divide by zero exception in Non abort mode
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckFPZeroDivNoAbort(tVoid)
{
   tU32 u32ReturnValue = 0;
   volatile tDouble dDividend=2.0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   volatile tDouble dResult = 0.0;
   
   if( OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode) )
   {
      u32ReturnValue += 1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "vFPZeroDivNoAbort SetFPEMode failed\n");
   }
   else
   {
      OSAL_vFPEReset();
      /*Do FP divide by 0*/
      /*lint -e414 */
      dResult = dDividend/u32Zero;
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_DIVZERO != u8FPEStatus )
      {
         u32ReturnValue += 2;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "vFPZeroDivNoAbort:Get status Failed");
      }
      else
      {
         OSAL_vFPEReset();
         /*Reset the exceptions caused*/
         u8FPEStatus = OSAL_u8GetFPE();
         if(0 != u8FPEStatus)
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "vFPZeroDivNoAbort :Reset status Failed");
            u32ReturnValue += 4;
         }
      }
   }
   /* Suppress warning*/
   (tVoid)dResult;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPOverflowNoAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Overflow exception in Non abort mode
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckFPOverflowNoAbort(tVoid)
{
   volatile float fNo=2.0;
   volatile tDouble dNo=1e40;

   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;
   
   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      u32ReturnValue += 1;
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Overflow SetFPEMode failed\n");
   }
   else
   {
      OSAL_vFPEReset();
      /*Do overflow operation*/
      /*lint -e736*/
      fNo = dNo;
      u8FPEStatus = OSAL_u8GetFPE();
      /*Overflow operation may also reult in Inexact Operation.Hence check for both flags*/
      if( (OSAL_C_U8_FPE_INEXACT & u8FPEStatus) || (OSAL_C_U8_FPE_OVERFLOW & u8FPEStatus) )
      {
         OSAL_vFPEReset();
         u8FPEStatus = OSAL_u8GetFPE();
         if(0 != u8FPEStatus)
         {
            u32ReturnValue += 4;
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"OverflowNoAbort : GetFPE after reset Failed");
         }
      }
      else
      {
         u32ReturnValue += 2;
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"OverflowNoAbort : GetFPE Failed");
      }
   }
   /* Suppress warning*/
   (tVoid)fNo;
   return u32ReturnValue;
}
/*****************************************************************************
* FUNCTION     :  u32CheckFPUnderflowNoAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Underflow exception in Non abort mode
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckFPUnderflowNoAbort(tVoid)
{
   volatile float fNo=2.0;
   volatile tDouble dNo=1e40;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Underflow SetFPEMode failed\n");
      u32ReturnValue += 1;
   }
   else
   {
      OSAL_vFPEReset();
      /*Do Underflow operation*/
      fNo = fNo/dNo;
      u8FPEStatus = OSAL_u8GetFPE();
      /*Underflow operation may also reult in Inexact Operation.Hence check for both flags*/
      if( (OSAL_C_U8_FPE_INEXACT & u8FPEStatus) || (OSAL_C_U8_FPE_UNDERFLOW & u8FPEStatus))
      {
         OSAL_vFPEReset();
         u8FPEStatus = OSAL_u8GetFPE();
         if(0 != u8FPEStatus)
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"UnderflowNoAbort : GetFPE after reset Failed");
            u32ReturnValue += 4;
         }
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Underflow GetFPE : Failed");
         u32ReturnValue += 2;
      }
   }
   /* Suppress warning */
   (tVoid)fNo;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPInvalidNoAbort()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Create Invalid exception in Non abort mode
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckFPInvalidNoAbort(tVoid)
{
   tDouble dNo = -20.0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;
   tDouble dVal = 0;
   
   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Invalid SetFPEMode failed\n");
      u32ReturnValue += 1;
   }
   else
   {
      OSAL_vFPEReset();
      /*Sqrt of -ve number creates a NaN*/
      dVal = sqrt(dNo);
      /*Comparison of a NaN with Number is a Invalid Operation*/
      if( dVal <= 0 )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Try to create a Invalid operation");
      }
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_INVALID != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Invalid GetFPE : Failed");
         u32ReturnValue += 2;
      }
      else
      {
         OSAL_vFPEReset();
         u8FPEStatus = OSAL_u8GetFPE();
         if(0 != u8FPEStatus)
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Invalid : after reset Failed");
            u32ReturnValue += 4;
         }
      }
   }
   /* suppress warning */
   (tVoid)dVal;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckResetFPEFlag()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check if reset resets all the flags
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckResetFPEFlag(tVoid)
{
   volatile tDouble dDividend=2.0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;
   volatile tDouble dResult = 0;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckResetFPEFlag SetFPEMode failed\n");
      u32ReturnValue += 1;
   }
   else
   {
      OSAL_vFPEReset();
      dResult = dDividend/u32Zero;
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_DIVZERO != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckResetFPEFlag Div by Zero Failed");
         u32ReturnValue += 2;
      }
      else
      {
         OSAL_vFPEReset();
         u8FPEStatus = OSAL_u8GetFPE();
         /*Reset should clear all the flags*/
         if(0 != u8FPEStatus)
         {
             OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckResetFPEFlag : Failed");
             u32ReturnValue += 4;
         }
      }
   }
   /* Suppress warning */
   (tVoid)dResult;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  vResetMultithread()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  Void 
* DESCRIPTION  :  Thread1 of Reset test
*                 See header of u32CheckResetMultithreads() for more details
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
******************************************************************************/
static tVoid vResetMultithread(tPVoid pvArg)
{
   volatile tDouble dDividend=2.0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   volatile tDouble dResult = 0;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Step 1 : Set the FPE Mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ResetMultithread: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      /* Step 2 : Reset the FPE Flags */
      OSAL_vFPEReset();

      /* Step 3 : Make FP error */
      dResult = dDividend/u32Zero;

      /* Step 4 : Check the FPE flags */
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_DIVZERO != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ResetMultithread : GetFPE Failed");
         u32Retval = 2;
      }
      /* Step 5 : Wait till thread2 make FPE error i.e wait for sig1 from thread2 */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1,
               OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : Wait for sig1 failed");
         u32Retval = 3;
      }
   }

   /* If no error happened till this point continue with further tests */
   if(!u32Retval)
   {
      /* Step 6 : Reset the FPE flags of current thread  */
      OSAL_vFPEReset();

      /* Step 7 : Confirm whether reset API call reset the FPE flags correctly */
      u8FPEStatus = OSAL_u8GetFPE();
      if(0 != u8FPEStatus)
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ResetMultithread : GetFPE after reset Failed");
         u32Retval = 4;
      }
      /* Step 8 : Inform thread2 that FPE Reset has been called from this thread */
      else if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleT2,
            FP_THREAD_EV_T2_SIG1, OSAL_EN_EVENTMASK_OR ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ResetMultithread : sig1 post to failed");
         u32Retval = 5;
      }
      /* Step 9 : Wait till the other thread checks to see if its FPE flag has not affected
       * by above reset. (i.e Thread1 FPE reset should not affect thread2 FPE flags ) */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG2,
            OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : sig2 wait failed");
         u32Retval = 6;
      }

   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT1 = u32Retval;

   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : u32Retval %d ", u32Retval);

   /* Step 10 : Inform Parent Thread that Thread1 is going to exit */
   if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleMain,
         FP_THREAD_EV_MAIN_SIGT1, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : Event post to Main failed");
      prMTArgs->u32RetValT1 += 10;
   }

   /* Suppress warning */
   (tVoid)dResult;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  vResetMultithreadExt()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  Void 
* DESCRIPTION  :  Thread2 of Reset test
*                 See header of u32CheckResetMultithreads() for more details
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
******************************************************************************/
static tVoid vResetMultithreadExt(tPVoid pvArg)
{
   tDouble dNo = -20.0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU8 u8FPEStatus = 0;
   tDouble dVal = 0;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Step 1 : Set the FPE Mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
     OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt: SetFPEMode failed\n");
     u32Retval = 1;
   }
   else
   {
      /* Step 2 : Reset the FPE Flags */
      OSAL_vFPEReset();

      /* Step 3 : Make FPE error. Sqrt of -ve number creates a NaN */
      dVal = sqrt(dNo);

      /* Comparison of a NaN with Number is a Invalid Operation */
      if( dVal <= 0 )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Try to create a Invalid operation");
      }

      /* Step 4 : Check the FPE flags */
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_INVALID != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt : getfpe Failed");
         u32Retval = 2;
      }
      /* Step 5 : Signal the first thread to reset the FPE flags from there */
      /* We should send this signal irrespective of any previous error to avoid
       * thread1 to be stuck waiting for this signal */
      else if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1,
                  OSAL_EN_EVENTMASK_OR))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt : sig1 post failed");
         u32Retval = 3;
      }
      /* Step 6 : Wait till first thread resets the FPE flags from there */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT2, FP_THREAD_EV_T2_SIG1,
            OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt : sig1 wait failed");
         u32Retval = 4;
      }
      /* Step 7 : Check the FPE flags to see if its affected by the reset from first thread */
      else if(0 == OSAL_u8GetFPE())
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Reset from thread1 affected Thread2 FPE flags");
         u32Retval = 5;
      }
      else
      {
         /* Step 8 : Reset the FPE Flags */
         OSAL_vFPEReset();

         /* Step 9 : Signal the thread1 to exit */
         if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleT1,
               FP_THREAD_EV_T1_SIG2, OSAL_EN_EVENTMASK_OR))
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt : sig2 post failed");
            u32Retval = 6;
         }
      }
   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT2 = u32Retval;

   OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : u32Retval %d ", u32Retval);

   /* Step 10 : Inform Parent Thread that Thread2 is going to exit */
   if( OSAL_ERROR ==
         OSAL_s32EventPost( prMTArgs->hEventHandleMain, FP_THREAD_EV_MAIN_SIGT2, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithreadExt : Event post to main failed");
      prMTArgs->u32RetValT2 += 10;
   }

   /* Suppress warning*/
   (tVoid)dVal;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckResetMultithreads()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check reset of flags is thread specific
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
*
*                  Thread1           Thread2
*                 ---------------------------
*                  Set Mode          Set Mode
*                  Reset             Reset
*                  Error     **      Error
*                  Check             Check
*                  Wait   <------    Post
*                  Reset
*                  Check
*                  Post   ------>    Wait
*                                    Check (to see if T1 resets T2's flag)
*                                    Reset
*                  Wait   <------    Post
*                  Exit              Exit
******************************************************************************/
tU32 u32CheckResetMultithreads(tVoid)
{
   tU32 u32ReturnValue;

   /* Start the Dual thread test */
   u32ReturnValue = u32FPEDualThreadTest("FPE_Reset_MultiThread",
                                         "FPE_Reset_MultiThread_Ext",
                                         vResetMultithread, /* Thread 1 */
                                         vResetMultithreadExt); /* Thread 2 */
   if( u32ReturnValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckResetMultithreads: Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckResetMultithreads: Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckMultipleFPEFlags()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check if flags are set not affecting other flags
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
******************************************************************************/
tU32 u32CheckMultipleFPEFlags(tVoid)
{
   volatile tDouble dDividend = 2.0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   tDouble dNo = -20.0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;
   volatile tDouble dResult = 0;
   tDouble dVal = 0;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"MultipleFPEFlags: SetFPEMode failed\n");
      u32ReturnValue += 1;
   }
   else
   {
      OSAL_vFPEReset();
      /*Create FP Divide by zero*/
      dResult = dDividend/u32Zero;
      /*Sqrt of -ve number creates a NaN*/
      dVal = sqrt(dNo);
      /*Comparison of a NaN with Number is a Invalid Operation*/
      if( dVal <= 0 )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Try to create a Invalid operation");
      }
      u8FPEStatus = OSAL_u8GetFPE();
      /*Both flags must be set*/
      if( ( u8FPEStatus & OSAL_C_U8_FPE_DIVZERO ) && (u8FPEStatus & OSAL_C_U8_FPE_INVALID) )
      {
         OSAL_vFPEReset();
         u8FPEStatus = OSAL_u8GetFPE();
         if(0 != u8FPEStatus)
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"MultipleFPEFlags: FPE reset Failed");
            u32ReturnValue += 4;
         }
      }
      else
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"MultipleFPEFlags: getfpe Failed");
         u32ReturnValue += 2;
      }
   }
   /* Suppress warnings */
   (tVoid)dResult;
   (tVoid)dVal;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  vSetMultithread()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Thread1 of FP flag set test
*                 See header of u32CheckFPEBitSetMultithreaded() for more details
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
******************************************************************************/
static tVoid vSetMultithread(tPVoid pvArg)
{
   volatile tDouble dDividend = 2.0;
   volatile tDouble dResult = 0;
   volatile tU32 u32Zero = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Step 1 : Set the FPE Mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode( enFPEMode ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      /* Step 2 : Reset the FPE Flags */
      OSAL_vFPEReset();

      /* Step 3 : Wait Till thread2 set its mode and reset its flags */
      if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1,
          OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
      {
        OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: Sig1 wait failed");
        u32Retval = 2;
      }
   }

   /* If no error happened till this point continue with further tests */
   if(!u32Retval)
   {
      /* Step 4 : Make FP divide by zero error */
      dResult = dDividend/u32Zero;

      /* Step 5 : Post signal to thread2 to inform error has been made at thread1 */
      if( OSAL_ERROR ==
          OSAL_s32EventPost( prMTArgs->hEventHandleT2, FP_THREAD_EV_T2_SIG1, OSAL_EN_EVENTMASK_OR ))
      {
          OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: sig1 post to thread2 failed");
          u32Retval = 3;
      }
      /* Step 6 : Wait till Thread 2 makes FPE error */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG2,
          OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: sig2 wait failed");
         u32Retval = 4;
      }
      /* Step 7 : Check the FPE flags */
      else if( OSAL_C_U8_FPE_DIVZERO != OSAL_u8GetFPE() )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: getfpe  Failed");
         u32Retval = 5;
      }
      /* Step 8 : Send signal to thread2 to exit */
      else if( OSAL_ERROR ==
            OSAL_s32EventPost( prMTArgs->hEventHandleT2, FP_THREAD_EV_T2_SIG2, OSAL_EN_EVENTMASK_OR ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: sig2 post to thread2 failed");
         u32Retval = 6;
      }
   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT1 = u32Retval;

   /* Step 9 : Inform Parent Thread that Thread1 is going to exit */
   if( OSAL_ERROR ==
     OSAL_s32EventPost( prMTArgs->hEventHandleMain, FP_THREAD_EV_MAIN_SIGT1, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithread: Event post to main failed");
      prMTArgs->u32RetValT1 += 10;
   }

   /* Step 10 : Reset and exit */
   OSAL_vFPEReset();
   (tVoid)dResult;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  vSetMultithreadExt()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Thread2 of FP flag set test
*                 See header of u32CheckFPEBitSetMultithreaded() for more details
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
******************************************************************************/
static tVoid vSetMultithreadExt(tPVoid pvArg)
{
   tDouble dNo = -20.0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tDouble dVal = 0.0;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */
   
   /* Step 1 : Set the FPE Mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode( enFPEMode ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "SetMultithreadExt: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      /* Step 2 : Reset the FPE Flags */
      OSAL_vFPEReset();

      /* Step 3 : Send the signal to thread1 */
      if( OSAL_ERROR ==
          OSAL_s32EventPost( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1, OSAL_EN_EVENTMASK_OR))
      {
          OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithreadExt: Event post to sibling failed");
          u32Retval = 2;
      }
      /* Step 4: Wait till other thread make FP error */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT2,
            FP_THREAD_EV_T2_SIG1, OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, NULL ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithreadExt: sig1 wait failed");
         u32Retval = 3;
      }
   }

   /* If no error happened till this point continue with further tests */
   if(!u32Retval)
   {
      /* Step 5: Make FPE error. Sqrt of -ve number creates a NaN*/
      dVal = sqrt(dNo);
      /*Comparison of a NaN with Number is a Invalid Operation*/
      if( dVal <= 0 )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"Try to create a Invalid operation");
      }

      /* Step 6: Check the FPE flags to see if T1 flag messed up with T2 flag */
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_INVALID != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithreadExt: Check FPE Failed");
         u32Retval = 4;
      }
      /* Step 7: Send the signal to thread 1 to inform thread2 has made FPE error */
      else if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleT1,
            FP_THREAD_EV_T1_SIG2, OSAL_EN_EVENTMASK_OR))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithreadExt: Event post to sibling failed");
         u32Retval = 5;
      }
      /* Step 8: Wait till FP operations are done in the other thread*/
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT2,
            FP_THREAD_EV_T2_SIG2, OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, NULL ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"SetMultithreadExt: Event post failed");
         u32Retval = 6;
      }

      /* Step 9: Reset FPE flags */
      OSAL_vFPEReset();
   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT2 = u32Retval;

   /* Step 10: Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleMain,
         FP_THREAD_EV_MAIN_SIGT2, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "SetMultithreadExt: Event post to main failed");
      prMTArgs->u32RetValT2 += 10;
   }

   (tVoid)dVal;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPEBitSetMultithreaded()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check set of flags is thread specific
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
*
*                  Thread1           Thread2
*                 ---------------------------
*                  Set Mode          Set Mode
*                  Reset             Reset
*                  Wait   <------    Post
*                  Error     *
*                  Post   ------>    Wait
*                            *       Error
*                                    Check (see if T1 flag messed up with T2 flag)
*                  Wait   <------    Post
*                  Check (see if error in T2 affected T1's flag)
*                  Post   ------>    Wait
*                  Reset             Reset
*                  Exit              Exit
******************************************************************************/
tU32 u32CheckFPEBitSetMultithreaded(tVoid)
{
   tU32 u32ReturnValue;

   /* Start the Dual thread test */
   u32ReturnValue = u32FPEDualThreadTest("FPE_BitSet_MultiThread",
                                         "FPE_BitSet_MultiThread_Ext",
                                         vSetMultithread,
                                         vSetMultithreadExt);
   
   if( u32ReturnValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEBitSetMultithreaded : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEBitSetMultithreaded : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPEModeChange()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check switch over from Abort mode to other mode
* HISTORY      :  Created by Ravindran P(RBEI/ECF1) on 31 - 01- 2011
*              :  04/08/2011 Anooj Gopi(RBEI/ECF1) Updated with multiple events.
******************************************************************************/
tU32 u32CheckFPEModeChange(tVoid)
{
   volatile tDouble dDividend=2.0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   tU32 u32ReturnValue = 0;
   volatile tDouble dResult = 0.0;

   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeChange: SetFPEMode failed\n");
      u32ReturnValue += 1;
   }
   else
   {
      OSAL_vFPEReset();
      dResult = dDividend/u32Zero;
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_DIVZERO != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeChange: getfpe Failed");
         u32ReturnValue += 2;
      }
      else
      {
         OSAL_vFPEReset();
         enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;

         if(OSAL_OK != OSAL_s32SetFPEMode(enFPEMode))
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeChange: SetFPEMode failed\n");
            u32ReturnValue += 4;
         }
         else
         {
            OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeChange: SetFPEMode success \n");
            /* If we make an FPE error here, it will cause SIGFPE to be generated */
            /* Current SIGFPE handler will trigger system reboot on FPE error */
         }
      }
   }

   (tVoid)dResult;
   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  vModeSetMultithread()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Thread1 of Mode set test
*                 See header of u32CheckFPEModeSetMultithreaded() for more details
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vModeSetMultithread(tPVoid pvArg)
{
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_ABORT;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Reset the FPE Flags */
   OSAL_vFPEReset();

   /* Step 1 : Wait Till thread2 set its mode and reset its flags */
   if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1,
       OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithread: Sig1 wait failed");
      u32Retval = 1;
   }
   /* Step 2 : Set the FPE Mode as Abort */
   else if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithread: SetFPEMode failed\n");
      u32Retval = 2;
   }
   /* Step 3 : Post signal to thread2 to inform mode has been set at thread1 */
   else if( OSAL_ERROR ==
       OSAL_s32EventPost( prMTArgs->hEventHandleT2, FP_THREAD_EV_T2_SIG1, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithread : sig1 post to thread2 failed");
      u32Retval = 3;
   }
   /* Step 4 : Wait till Thread 2 confirms its FPE mode is not affected by thread1 */
   else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG2,
       OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, OSAL_NULL))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithread : sig2 wait failed");
      u32Retval = 4;
   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT1 = u32Retval;

   /* Step 5 : Inform Parent Thread that Thread1 is going to exit */
   if( OSAL_ERROR ==
     OSAL_s32EventPost( prMTArgs->hEventHandleMain, FP_THREAD_EV_MAIN_SIGT1, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithread : Event post to main failed");
      prMTArgs->u32RetValT1 += 10;
   }

   /* Step 6 : Reset and exit */
   OSAL_vFPEReset();
   //(tVoid)dResult;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  vModeSetMultithreadExt()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Thread2 of Mode set test
*                 See header of u32CheckFPEModeSetMultithreaded() for more details
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vModeSetMultithreadExt(tPVoid pvArg)
{
   volatile tDouble dDividend=2.0;
   volatile tDouble dResult=0;
   volatile tU32 u32Zero = 0;
   tU8 u8FPEStatus = 0;
   OSAL_tenFPEMode enFPEMode = OSAL_EN_U8_FPE_MODE_FPE_FLAG;
   trMultiThreadArgs *prMTArgs = (trMultiThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Step 1 : Set the FPE Mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode(enFPEMode))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ModeSetMultithreadExt: SetFPEMode failed\n");
      u32Retval = 1;
   }
   else
   {
      /* Step 2 : Reset the FPE Flags */
      OSAL_vFPEReset();

      /* Step 3 : Send the signal to thread1 so that it set its mode to abort */
      if( OSAL_ERROR ==
          OSAL_s32EventPost( prMTArgs->hEventHandleT1, FP_THREAD_EV_T1_SIG1, OSAL_EN_EVENTMASK_OR))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithreadExt: sig1 post failed");
         u32Retval = 2;
      }
      /* Step 4: Wait till other thread Set the FPE mode to Abort */
      else if( OSAL_ERROR == OSAL_s32EventWait( prMTArgs->hEventHandleT2,
            FP_THREAD_EV_T2_SIG1, OSAL_EN_EVENTMASK_OR, FP_THREAD_EV_WAIT_TIMEOUT, NULL ))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithreadExt: sig1 wait failed");
         u32Retval = 3;
      }
   }

   /* If no error happened till this point continue with further tests */
   if(!u32Retval)
   {
      /* Step 5 : Make FP error. if FPE mode is affected by other thread it will
       * cause SIGFPE to be generated, which will restarts the system */
      dResult = dDividend/u32Zero;

      /* Step 6: Check the FPE flags */
      u8FPEStatus = OSAL_u8GetFPE();
      if( OSAL_C_U8_FPE_DIVZERO != u8FPEStatus )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithreadExt: getfpe Failed");
         u32Retval = 4;
      }
      /* Step 7: Send the signal to thread 1 to inform thread2 has made FPE error */
      else if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleT1,
            FP_THREAD_EV_T1_SIG2, OSAL_EN_EVENTMASK_OR))
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ModeSetMultithreadExt: sig2 post failed");
         u32Retval = 5;
      }

      /* Step 8: Reset FPE flags */
      OSAL_vFPEReset();
   }

   /* Put the return value in the argument structure before signaling main thread */
   prMTArgs->u32RetValT2 = u32Retval;

   /* Step 9: Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prMTArgs->hEventHandleMain,
         FP_THREAD_EV_MAIN_SIGT2, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "ModeSetMultithreadExt: Event post to main failed");
      prMTArgs->u32RetValT2 += 10;
   }

   (tVoid)dResult;
   OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION     :  u32CheckFPEModeSetMultithreaded()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check Mode set is thread specific
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
*
*                  Thread1           Thread2
*                 ---------------------------
*                            *       Set Mode (Flag mode)
*                  Reset             Reset
*                  Wait   <------    Post
*                  Set Mode  *                (Abort mode)
*                  Post   ------>    Wait
*                                    Error (to see if we are still in Flag mode)
*                                    Check
*                  Wait   <------    Post
*                  Reset             Reset
*                  Exit              Exit
******************************************************************************/
tU32 u32CheckFPEModeMultithreaded( tVoid )
{
   tU32 u32ReturnValue;

   /* Start the Dual thread test */
   u32ReturnValue = u32FPEDualThreadTest("FPE_ModeSet_MultiThread",
                                         "FPE_BitSet_MultiThread_Ext",
                                         vModeSetMultithread,
                                         vModeSetMultithreadExt);

   if( u32ReturnValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeSetMultithreaded : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckFPEModeSetMultithreaded : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32ReturnValue;
}

/*****************************************************************************
* FUNCTION     :  vDefaultModeCheckThread()
* PARAMETER    :  Pointer to multi-thread argument structure.
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Function to test the default FPE Mode
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
static tVoid vDefaultModeCheckThread(tPVoid pvArg)
{
   trSingleThreadArgs *prTArgs = (trSingleThreadArgs *)pvArg;
   tU32 u32Retval = 0; /* Error value should not exceed two decimal digits */

   /* Since the default Mode is Abort Mode if we make any FPE error
    * it will cause system reboot. If the default mode is flag mode
    * we could create an FPE error to see is thread continue its execution */

   /* Put the return value in the argument structure before signaling main thread */
   prTArgs->u32RetVal = u32Retval;

   /* Signal the main thread */
   if( OSAL_ERROR == OSAL_s32EventPost( prTArgs->hEventHandle,
         FP_THREAD_EV_SINGLE, OSAL_EN_EVENTMASK_OR ))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "DefaultModeCheckThread: Event post to main failed");
      prTArgs->u32RetVal += 10;
   }

   OSAL_vThreadExit();
}

#if 0
/*****************************************************************************
* FUNCTION     :  u32CheckFPEModeSetMultithreaded()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  :  Check Mode set is thread specific
* HISTORY      :  Created by Anooj Gopi(RBEI/ECF1) on 08/08/2011
******************************************************************************/
tU32 u32CheckDefaultFPEMode(tVoid)
{
   tU32 u32RetValue;

   u32RetValue = u32FPESingleThreadTest("FPE_ModeSet_MultiThread", vDefaultModeCheckThread);

   if( u32RetValue == 0 )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckDefaultFPEMode : Passed\n");
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"CheckDefaultFPEMode : Failed\n");
   }

   /*Reset as this should not affect other test cases*/
   OSAL_vFPEReset();

   return u32RetValue;
}
#endif

/*****************************************************************************
* FUNCTION     : u32FPESingleThreadTest()
* PARAMETER    : coszThreadName - name of thread to be spawned
*              : pfThreadEntry        - Entry Function name of first thread
* RETURNVALUE  : tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  : The Generic function to spawn a thread to run test function
* HISTORY      : Created by Anooj Gopi(RBEI/ECF1) on 08 - 08 - 2011
******************************************************************************/
static tU32 u32FPESingleThreadTest( tCString coszThreadName, OSAL_tpfThreadEntry pfThreadEntry )
{
   trSingleThreadArgs     rTArgs = {0};
   OSAL_tThreadID         FPE_TID;
   OSAL_trThreadAttribute rThAttr = {0};
   tCString coszFPE_OEDTEvent = "FPE_OEDTEvent";
   tU32 u32RetValue = 0;

   /* Make the thread attributes ready */
   rThAttr.szName         = (tString) coszThreadName;
   rThAttr.pfEntry        = pfThreadEntry;
   rThAttr.pvArg          = (tPVoid) &rTArgs;
   rThAttr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr.s32StackSize   = 2048;

   /* Set the current thread mode opposite to default mode */
   if(OSAL_ERROR == OSAL_s32SetFPEMode(OSAL_EN_U8_FPE_MODE_FPE_FLAG))
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: SetFPEMode failed\n");
      u32RetValue = 1000;
   }
   /* Creating the required events */
   else if( OSAL_OK != OSAL_s32EventCreate( coszFPE_OEDTEvent, &rTArgs.hEventHandle ) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Event create failed \n");
      u32RetValue = 2000;
   }
   else if( (FPE_TID = OSAL_ThreadSpawn( &rThAttr ) ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Spawn failed \n");
      u32RetValue = 3000;
   }
   else if( OSAL_OK != OSAL_s32EventWait( rTArgs.hEventHandle, FP_THREAD_EV_SINGLE,
         OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER , OSAL_NULL) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: sig wait failed \n");
      u32RetValue = 4000;
   }
   else
   {
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Wait for Events over !!!" );
      OEDT_HelperPrintf((tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Thread ID : %d", FPE_TID );
      /* Update the results from thread */
      u32RetValue += rTArgs.u32RetVal;
   }

   /* Deallocate all the resources we have created  */
   /* Deallocate Main event if created */
   if( rTArgs.hEventHandle )
   {
      /* Close the Event handle */
      if( OSAL_OK != OSAL_s32EventClose( rTArgs.hEventHandle ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Event Close failed \n");
         u32RetValue += 10000;
      }

      /* Delete the handle. We have to do it even if the close fails */
      if( OSAL_OK != OSAL_s32EventDelete( coszFPE_OEDTEvent ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPESingleThreadTest: Event Delete failed \n");
         u32RetValue += 20000;
      }
   }

   /* Note the return value will have 7 digits of error codes
    * Digit 1, 2 and 3 for spawned thread errors
    * Digit 4 and 5 for error from this function */
   return u32RetValue;
}

/*****************************************************************************
* FUNCTION     : u32FPEDualThreadTest()
* PARAMETER    : coszThreadName1 - name of first thread
*              : coszThreadName2 - name of second thread
*              : pfThread1       - Entry Function name of first thread
*              : pfThread2       - Entry Function name of second thread
* RETURNVALUE  : tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION  : The Generic function where 2 threads are opened for all the
*              : test cases.
* HISTORY      : Created by Anooj Gopi(RBEI/ECF1) on 04 - 08 - 2011
******************************************************************************/
static tU32 u32FPEDualThreadTest( tCString coszThreadName1, tCString coszThreadName2,
                                  OSAL_tpfThreadEntry pfThread1, OSAL_tpfThreadEntry pfThread2 )
{
   tU32                   u32RetValue     = 0;
   OSAL_trThreadAttribute rThAttr1        = {0};
   OSAL_trThreadAttribute rThAttr2        = {0};
   OSAL_tThreadID         TID_1           = 0;
   OSAL_tThreadID         TID_2           = 0;
   trMultiThreadArgs rMTArgs = {0};
   tCString coszFPE_OEDTEvent_Main = "FPE_OEDTEvent_Main";
   tCString coszFPE_OEDTEvent_T1 = "FPE_OEDTEvent_T1";
   tCString coszFPE_OEDTEvent_T2 = "FPE_OEDTEvent_T2";

   /* Make the thread attributes ready */
   rThAttr1.szName         = (tString)coszThreadName1;
   rThAttr1.pfEntry        = pfThread1;
   rThAttr1.pvArg          = (tPVoid) &rMTArgs;
   rThAttr1.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr1.s32StackSize   = 2048;

   rThAttr2.szName         = (tString)coszThreadName2;
   rThAttr2.pfEntry        = pfThread2;
   rThAttr2.pvArg          = (tPVoid) &rMTArgs;
   rThAttr2.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   rThAttr2.s32StackSize   = 2048;

   /* Creating the required events */
   if( OSAL_OK != OSAL_s32EventCreate( coszFPE_OEDTEvent_Main, &rMTArgs.hEventHandleMain ) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE Main Event create failed \n");
      u32RetValue = 10000;
   }
   else if( OSAL_OK != OSAL_s32EventCreate( coszFPE_OEDTEvent_T1, &rMTArgs.hEventHandleT1 ) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE T1 Event create failed \n");
      u32RetValue = 20000;
   }
   else if( OSAL_OK != OSAL_s32EventCreate( coszFPE_OEDTEvent_T2, &rMTArgs.hEventHandleT2 ) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE T2 Event create failed \n");
      u32RetValue = 30000;
   }
   else if( (TID_1 = OSAL_ThreadSpawn( &rThAttr1 ) ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT Thread1 Spawn failed \n");
      u32RetValue = 40000;
   }
   else if( ( TID_2 = OSAL_ThreadSpawn( &rThAttr2 ) ) == OSAL_ERROR )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT Thread2 Spawn failed \n");
      u32RetValue = 50000;
   }
   else if( OSAL_OK != OSAL_s32EventWait( rMTArgs.hEventHandleMain, FP_THREAD_EV_MAIN_SIG_T1T2,
         OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, OSAL_NULL) )
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT Main thread wait failed \n");
      u32RetValue = 60000;
   }
   else
   {
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPEDualThreadTest: Wait for Events over !!!" );
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPEDualThreadTest: Thread ID1 : %d, Thread ID2: %d", TID_1, TID_2 );
      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1,"ResetMultithread : T1Retval : %d , T2Retval : %d",
            rMTArgs.u32RetValT1, rMTArgs.u32RetValT2);
      /* Update the results from threads to return value
       * Note: In u32RetValue, least significant 2 digits are for thread1
       * and next 2 digits are for Thread2 */
      u32RetValue += rMTArgs.u32RetValT1 + 100 * rMTArgs.u32RetValT2;

      OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPEDualThreadTest: Main Thread got value %d ", u32RetValue );
   }

   /* Deallocate all the resources we have created  */
   /* Deallocate Main event if created */
   if( rMTArgs.hEventHandleMain )
   {
      /* Close the Event handle */
      if( OSAL_OK != OSAL_s32EventClose( rMTArgs.hEventHandleMain ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT Main Event Close failed \n");
         u32RetValue += 100000;
      }

      /* Delete the handle. We have to do it even if the close fails */
      if( OSAL_OK != OSAL_s32EventDelete( coszFPE_OEDTEvent_Main ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT Main Event Delete failed \n");
         u32RetValue += 200000;
      }
   }

   /* Deallocate T1 event if created */
   if( rMTArgs.hEventHandleT1 )
   {
      /* Close the Event handle */
      if( OSAL_OK != OSAL_s32EventClose( rMTArgs.hEventHandleT1 ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT T1 Event Close failed \n");
         u32RetValue += 400000;
      }

      /* Delete the handle. We have to do it even if the close fails */
      if( OSAL_OK != OSAL_s32EventDelete( coszFPE_OEDTEvent_T1 ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT T1 Event Delete failed \n");
         u32RetValue += 800000;
      }
   }

   /* Deallocate T2 event if created */
   if( rMTArgs.hEventHandleT2 )
   {
      /* Close the Event handle */
      if( OSAL_OK != OSAL_s32EventClose( rMTArgs.hEventHandleT2 ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT T2 Event Close failed \n");
         u32RetValue += 1000000;
      }

      /* Delete the handle. We have to do it even if the close fails */
      if( OSAL_OK != OSAL_s32EventDelete( coszFPE_OEDTEvent_T2 ) )
      {
         OEDT_HelperPrintf( (tU8)TR_LEVEL_USER_1, "FPE_OEDT T2 Event Delete failed \n");
         u32RetValue += 2000000;
      }
   }

   /* Note the return value will have 7 digits of error codes
    * Digit 1 and 2 for thread1 errors
    * Digit 3 and 4 for thread2 errors
    * Digit 5, 6 and 7 for error from this function */
   return u32RetValue;
}
