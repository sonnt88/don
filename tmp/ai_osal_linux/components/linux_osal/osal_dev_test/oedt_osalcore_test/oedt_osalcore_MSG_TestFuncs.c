/******************************************************************************
 *FILE          :oedt_osalcore_MSG_TestFuncs.c
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for the MessagePool
 *               OSAL_CORE APIs
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       :
				ver 1.6 - 15.07.09  
				Removed create/delete as message pool is created during start up
				rav8kor 

 *              ver1.5   - 22-04-2009
                lint info removal
				sak9kor

  *				ver1.4	 - 14-04-2009
                warnings removal,added vDeleteExistingMPool and called from all test cases
				rav8kor
 				
 				31.01.2008 Rev 1.3 Tinoy Mathews( RBIN/ECM1 )
				 Added cases :
				 TU_OEDT_MSGPL_18 <-> TU_OEDT_MSGPL_029
  
 				 13.12.2007 Rev. 1.2 Tinoy Mathews (RBIN/ECM1)
				 Linted the code,updated case 017
 
 				 3.12.2007  Rev. 1.1 Tinoy Mathews (RBIN/EDI3)
				 Modified case TU_OEDT_MSGPL_017
 
 				 26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3) 
 *               Initial Revision.
 *****************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_osalcore_MSG_TestFuncs.h"
#include "oedt_helper_funcs.h"

/******************************************************************************
 *FUNCTION		:u32MSGPLCGetAbsolute_CurrentSize *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
 
 
tVoid u32MSGPLCGetAbsolute_CurrentSize( tVoid )
{
    tU32 u32RetSize = 0; 
    
	u32RetSize = (tU32)OSAL_s32MessagePoolGetAbsoluteSize();

    OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Absolute size of message pool = %d\n",
      					u32RetSize 
    				 );

    u32RetSize = (tU32)OSAL_s32MessagePoolGetCurrentSize();

    OEDT_HelperPrintf( 
    					TR_LEVEL_USER_1, 
      					"Current size of messag pool = %d\n", 
      					u32RetSize 
      				 );      
}


/******************************************************************************
 *FUNCTION		:u32MSGPLOpenStatus
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MSGPLMessagePoolOpenStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = (tU32) OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_INVALIDVALUE:  /* Expected error */
		 u32Ret = 1;
		 break;
	  case OSAL_E_ALREADYEXISTS:
		 u32Ret = 2;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 3;
		 break;
	  default:
		 u32Ret = 4;
   }//end of switch( s32OSAL_Ret )

   return u32Ret; 
} 


/******************************************************************************
 *FUNCTION		:u32MSGPLOpen
 *
 *DESCRIPTION	:
 *				 a) Create a message pool with size 5000 Bytes
 *				 b) Open the Message pool with Size 5000 
 *				 c) Close the Message pool if open
 *				    with Size 5000 Byte.
 *				 d) Close & Delete the Message pool, if create
 *				    with Size 5000 Byte

 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_001
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                15.07.09 - Ravindran P - Removed create as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLOpen( tVoid )
{
	tS32 s32OSAL_Ret = 0;
	tU32 u32Ret = 0;
	
	/* Open the Message pool */
	s32OSAL_Ret = OSAL_s32MessagePoolOpen();
	if( OSAL_ERROR == s32OSAL_Ret )
	{
		u32Ret = 50;
	}//end of if( OSAL_ERROR == s32OSAL_Ret )(Open)
	else
	{
		/* Close the Message pool, if open */
		s32OSAL_Ret = OSAL_s32MessagePoolClose();
		if( OSAL_ERROR == s32OSAL_Ret )
		{
			u32Ret = 100 ;
		}//end of if( OSAL_ERROR == s32OSAL_Ret )(close if open)   
	}

	
	
	return u32Ret;

}





/******************************************************************************
 *FUNCTION		:u32MSGPLCreateMessageNULL
 *
 *DESCRIPTION	:
 *				 a) Create a message in shared Memory
 *				    with pMessage = OSAL_NULL
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_002
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                 15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLCreateMessageNULL( tVoid )	   //check anoop
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
   
       
   
   u32Size = MSGPL_POOLSIZE_FIVE_TH;
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();
   }//end of if( OSAL_ERROR == s32OSAL_Ret ) (create MSGPL)
   else
   {
	  /* Check Absolute and Current Size*/
	  u32MSGPLCGetAbsolute_CurrentSize();

	  u32Size = MSGPL_MSG_SIZE;

	  /* Create the Message with Size NULL */
	  s32OSAL_Ret = OSAL_s32MessageCreate(OSAL_NULL, u32Size, 
	   										OSAL_EN_MEMORY_SHARED);

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	    /* check the error status returned */
	    s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
	    switch( s32OSAL_Ret )
	    {
	       case OSAL_E_INVALIDVALUE:  /* Expected error */		 
	          u32Ret = 0;
	          break;
	       case OSAL_E_NOSPACE:
	          u32Ret = 1;
	          break;
	  	   case OSAL_E_DOESNOTEXIST:
            u32Ret = 2;
            break;
         default:
            u32Ret = 3;
      }//end of switch( s32OSAL_Ret )
      
      if (s32OSAL_Ret != OSAL_E_INVALIDVALUE)
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessageCreate() failed, ErrorCode=%i ", s32OSAL_Ret );	    
      }
      
	  }//end of if( OSAL_ERROR == s32OSAL_Ret ) 
	  else
	  {
	     u32Ret = 10;
		 
      }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) 

	  /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) (create MSGPL)
   return u32Ret;
}  

/******************************************************************************
 *FUNCTION		:u32MSGPLCreateSizeZero
 *
 *DESCRIPTION	:Create a message in shared Memory
 *				 a) Create a message with size zero.
 *				 b) Delete if created
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_003
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                 15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLCreateSizeZero( tVoid ) 
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
   
   OSAL_trMessage OSAL_msg;
    
   
   
   u32Size = MSGPL_POOLSIZE_FIVE_TH;
   
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();
   }//end of if( OSAL_ERROR == s32OSAL_Ret ) (create MSGPL)
   else
   {

	  /* Get Absolute and Current Size*/
	  u32MSGPLCGetAbsolute_CurrentSize();

	  u32Size = 0;

	  /* Create the Message with Size NULL */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	   										OSAL_EN_MEMORY_SHARED );

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
	     s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
	     switch( s32OSAL_Ret )
	     {
	        case OSAL_E_NOSPACE:
	           u32Ret = 10;
	           break;
	        case OSAL_E_INVALIDVALUE:		 
	           u32Ret = 0;
		       break;
		    case OSAL_E_DOESNOTEXIST:
		       u32Ret = 20;
		       break;
		    default:
		       u32Ret = 30;
		 }//end of switch( s32OSAL_Ret )

    	 
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )(create)
	  else
	  {
    	 
	   //  u32Ret = 50; Mssage contains only header info 
		 /* Delete the Message */
	     s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
	     if( OSAL_ERROR == s32OSAL_Ret )
	     {
	        u32Ret += 100;    
	     }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)

	  }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create)

	  /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) (create MSGPL)
   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLCreateLocationInVal
 *
 *DESCRIPTION	:Create a message in shared Memory
 *				 a) Create a message with invalid location.
 *				 b) check the Absolute size and current size
 *					with out creating  the message  
 *				 c) Delete if created
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_004
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 				15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLCreateLocationInVal( tVoid )
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
   tU32 u32RetSize = 0;
   OSAL_trMessage OSAL_msg;
    
   
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();     
   }//end of  if( OSAL_ERROR == s32OSAL_Ret ) (Create MSGPL )
   else
   {
	  /* check Absolute and Current Size*/
	  u32MSGPLCGetAbsolute_CurrentSize();

      u32Size = MSGPL_MSG_SIZE;
	  /* Create the Message with invalid location */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	  										OSAL_EN_MEMORY_INVALID );	  

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
		 s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
		 switch( s32OSAL_Ret )
		 {
        case OSAL_E_INVALIDVALUE:		 
		       u32Ret = 0;
		       break;
		    case OSAL_E_NOSPACE:
		       u32Ret = 10;
		       break;
		  	case OSAL_E_DOESNOTEXIST:
		  	   u32Ret = 20;
           break;
        default:
           u32Ret = 30;
		 }//end of switch( s32OSAL_Ret )

     if (s32OSAL_Ret != OSAL_E_INVALIDVALUE)
     {
       OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessageCreate() failed, ErrorCode=%i ", s32OSAL_Ret );	    
     }

    	 u32RetSize = (tU32)OSAL_s32MessagePoolGetAbsoluteSize();

		 /* Absolute size and MSGPL_POOLSIZE_FIVE_TH should same */ 
		 if( MSGPL_MSG_SIZE == u32RetSize )
		 {
		    u32Ret += 50;

		 }//end of if( MSGPL_MSG_SIZE != u32RetSize )

	     
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )(create)
	  else
	  {
	     u32Ret = 100;
 
    	 u32RetSize = (tU32)OSAL_s32MessagePoolGetAbsoluteSize();

		 if( MSGPL_MSG_SIZE != u32RetSize )
		 {
		    u32Ret += 200;

		 }//end of if( MSGPL_MSG_SIZE != u32RetSize )


	     /* Delete the Message */
	     s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
	     if( OSAL_ERROR == s32OSAL_Ret )
	     {
	        u32Ret += 300;    
	     }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)

	  }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create)

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	    

   }//end of  if-else( OSAL_ERROR == s32OSAL_Ret ) (Create MSGPL )
   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetSMSize
 *
 *DESCRIPTION	:Create a message in shared Memory
 *				 a) Create a message 
 *				 b) Get the absolute size of the message pool
 *			        after creating a message in shared memory
 *				 c) Get the current size of the message pool after creating
 *			        a message in shared memory 
 *				 d) Delete the Message
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_005
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/

tU32 u32MSGPLGetSMSize(tVoid)
{
	
	tS32 s32OSAL_Ret = 0;
	tU32 u32Ret = 0;
	tU32 u32Size = 0;
	tU32 u32RetSize = 0;
	tU32 u32RetSizeAbsolute = 0;
	tU32 u32RetSizeAfterCreate = 0;
	tU32 u32RetSizeAfterDelete = 0;
	OSAL_trMessage OSAL_msg;
	
	/* Create the Message pool with Size 5000 */
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();
    if (u32Ret != 0)
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessagePoolOpen() failed, ErrorCode=%i ", OSAL_u32ErrorCode() );	    
    }
      

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)
   else
   {  
      /* Get Absolut and Current Size */
	  //u32MSGPLCGetAbsolute_CurrentSize();
	  u32RetSizeAbsolute = (tU32)OSAL_s32MessagePoolGetAbsoluteSize();
	  /*Return if there is no space to create message*/
	  if( u32RetSizeAbsolute < MSGPL_MSG_SIZE )
	  {
			if(OSAL_ERROR == OSAL_s32MessagePoolClose())
			{
				u32Ret += 5;    		
			}
			return 	u32Ret;
			 		
	  }
      OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Absolute size of message pool = %d\n",
      					u32RetSizeAbsolute 
    				 );
	  u32RetSize = (tU32)OSAL_s32MessagePoolGetCurrentSize();

      OEDT_HelperPrintf( 
    					TR_LEVEL_USER_1, 
      					"Current size of message pool = %d\n", 
      					u32RetSize 
      				 );      

	  u32Size = MSGPL_MSG_SIZE;
	  

	  /* Create the Message in shared Memory */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	  										OSAL_EN_MEMORY_SHARED );	  

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
	     s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
		 switch( s32OSAL_Ret )
		 {
		    case OSAL_E_INVALIDVALUE:		 
		       u32Ret = 10;
		       break;
		    case OSAL_E_NOSPACE:
		       u32Ret = 20;
		       break;
		    case OSAL_E_DOESNOTEXIST:
			   u32Ret = 30;
			   break;
			default:
			   u32Ret = 40;
		 }//end of switch( s32OSAL_Ret )
	     
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )(create)
	  else
	  {
	     
		 u32RetSizeAfterCreate = (tU32)OSAL_s32MessagePoolGetCurrentSize();

      	 OEDT_HelperPrintf( 
    					TR_LEVEL_USER_1, 
      					"Current size of message pool after create = %d\n", 
      					u32RetSizeAfterCreate 
      				 );      
		 if(u32RetSize <= u32RetSizeAfterCreate)
		 {
			u32Ret += 50;    	
		 }
		 /* Delete the Message */
		 s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 100;    
		 }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)
		 else
		 {
			u32RetSizeAfterDelete = (tU32)OSAL_s32MessagePoolGetCurrentSize();

      	 	OEDT_HelperPrintf( 
    					TR_LEVEL_USER_1, 
      					"Current size of message pool after Delete = %d\n", 
      					u32RetSizeAfterDelete 
      				 );
      				  
      	  if( u32RetSizeAfterDelete != u32RetSizeAfterCreate ) 
      	  {
             if( u32RetSizeAfterDelete != u32RetSizeAbsolute )
             {
                u32Ret += 200;    		
             }
      	  } 			      
		 }
	  }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create)

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  

   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)

   return u32Ret;


}


/******************************************************************************
 *FUNCTION		:u32MSGPLGetSMContent
 *
 *DESCRIPTION	:
 *				 a) Get a pointer to a message with unitialized
 *                  OSAL_trMessage structure
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_06
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLGetSMContent( tVoid )
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   tU32  u32status = 0;		 
   tPU8 MsgPtr = NULL;
   /*rav8kor - uninitialised declaration gives lint warning.hence invalid memory location is used*/
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};

   /* Create the Message pool with Size 5000 */
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();     
    if (u32Ret != 0)
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessagePoolOpen() failed, ErrorCode=%i ", OSAL_u32ErrorCode() );	    
    }
   }
   else
   {

      /* check Absolut and Current Size */
	  u32MSGPLCGetAbsolute_CurrentSize();     
      /* Get a pointer to a message with unitialized
      OSAL_trMessage structure*/
	  MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE );
	  if( OSAL_NULL == MsgPtr )
	  {
	     u32status = (tU32) OSAL_u32ErrorCode();
		 switch( u32status )
		 {
		    case OSAL_E_INVALIDVALUE:
		       u32Ret = 0;
		 	   break;
		    case OSAL_E_DOESNOTEXIST:		 
		       u32Ret = 1;
		       break;
		    default:
			   u32Ret = 2;
		 }//end of switch( s32OSAL_Ret )
     
     if (u32Ret != OSAL_E_INVALIDVALUE)
     {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_pu8MessageContentGet() failed, ErrorCode=%i ", OSAL_u32ErrorCode() );	    
     }

	  }//end of if( OSAL_NULL == MsgPtr )
	  else
	  {
	     u32Ret = 10;
	  }//end of if-else ( OSAL_NULL == MsgPtr )

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)

   return u32Ret ;
}  
/******************************************************************************
 *FUNCTION		:u32MSGPLGetSMContentDiffMode
 *
 *DESCRIPTION	:Create a message in shared Memory
 *				 a) Create the Message with Shared memory
 *				 b) To Get a pointer to a message with
 *		            invalid access parameter(OSAL_EN_BINARY)
 *				 c) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_WRITEONLY)
 *				 d) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_READONLY)
 *				 e) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_READWRITE)
 *				 f) Delete the Message
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_007
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *              :20.12.2007 Updated by Tinoy Mathews(RBIN/ECM1)
                 15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/
tU32 u32MSGPLGetSMContentDiffMode( tVoid )
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
   tU32 u32status = 0;
   tPU8 MsgPtr = NULL;
   OSAL_trMessage OSAL_msg;

     
   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();     
   }//end of if( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)
   else
   {

      /* check Absolut and Current Size */
	  u32MSGPLCGetAbsolute_CurrentSize();

	  u32Size = MSGPL_MSG_SIZE;

	  /* Create the Message in Shared memory */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	  									OSAL_EN_MEMORY_SHARED );	  

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
	     u32status = OSAL_u32ErrorCode();
	     switch( u32status )
	     {
	        case OSAL_E_INVALIDVALUE:		 
	           u32Ret = 1;
	           break;
	        case OSAL_E_NOSPACE:
	           u32Ret = 2;
	           break;
	  	    case OSAL_E_DOESNOTEXIST:
		       u32Ret = 3;
		       break;
		    default:
			   u32Ret = 4;
		 }//end of switch( u32status )
	     
	  }//if( OSAL_ERROR == s32OSAL_Ret ) (message create)
	  else
	  {
 /* OSAL_pu8MessageContentGet does not prove access mode  */

	     /* To Get a pointer to a message with
	  	 invalid access parameter(OSAL_EN_BINARY)*/

/*	     MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_BINARY );
	     if( OSAL_NULL == MsgPtr )
	     {
	   	    u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{
			   case OSAL_E_INVALIDVALUE:
		          u32Ret = 0;
				  break;
			   case OSAL_E_DOESNOTEXIST:		 
			      u32Ret = 15;
			      break;
			   default:
				  u32Ret = 25;
			}//end of switch( u32status )
		 }//end of if( OSAL_NULL == MsgPtr )
		 else
		 {
		    u32Ret = 35;
		 }*/

		 /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_WRITEONLY)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_WRITEONLY );
		 if( OSAL_NULL == MsgPtr )
		 {
		    u32status = OSAL_u32ErrorCode();
			switch( u32status )
			{
			   case OSAL_E_DOESNOTEXIST:		 
			      u32Ret += 10;
			      break;
			   case OSAL_E_INVALIDVALUE:
			      u32Ret += 20;
				  break;
			   default:
				  u32Ret += 30;
		    }//end of switch( u32status )
		 }//end of if( OSAL_NULL == MsgPtr )

		 /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_READONLY)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READONLY );
		 if( OSAL_NULL == MsgPtr )
		 {
		    OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Message pointer NULL in READ ONLY \n",
      					u32status 
    				 );   

		 }//end of if( OSAL_NULL == MsgPtr )
		  

		 /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_READWRITE)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE );
		 if( OSAL_NULL == MsgPtr )
		 {
		    OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Message pointer NULL in READWRITE \n",
      					u32status 
    				 );   

		 }//end of if( OSAL_NULL == MsgPtr )
		  
		 /* Delete the Message */
		 s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 5000;    

		 }//end of if( OSAL_ERROR == s32OSAL_Ret )(message Delete)

	   }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) (message create)

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 9000 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)

   return u32Ret ;
}  

/******************************************************************************
 *FUNCTION		:u32MSGPLGetLMSize
 *
 *DESCRIPTION	:Create a message in Local Memory
 *				 a) Create a message 
 *				 b) Get the absolute size of the message pool
 *			        after creating a message in Local memory
 *				 c) Get the current size of the message pool after creating
 *			        a message in Local memory 
 *				 d) Delete the Message
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_008
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 *****************************************************************************/

tU32 u32MSGPLGetLMSize(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
      
  
   OSAL_trMessage OSAL_msg;

   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();     
   }//end of if( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)
   else
   {
      /* Get Absolut and Current Size */
	 u32MSGPLCGetAbsolute_CurrentSize();
	    

	  u32Size = MSGPL_MSG_SIZE;

	  /* Create the Message with invalid location */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	  										OSAL_EN_MEMORY_LOCAL );	  

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
	     s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
		 switch( s32OSAL_Ret )
		 {
		    case OSAL_E_INVALIDVALUE:		 
		       u32Ret = 10;
		       break;
		    case OSAL_E_NOSPACE:
		       u32Ret = 20;
		       break;
		    case OSAL_E_DOESNOTEXIST:
			   u32Ret = 30;
			   break;
			default:
			   u32Ret = 40;
		 }//end of switch( s32OSAL_Ret )
	     
	  }//end of if( OSAL_ERROR == s32OSAL_Ret )
	  else
	  {
	     
		 /* Delete the Message */
		 s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 100;    
		 }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)
	  }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create)
	  

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 300 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)

   return u32Ret;

}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetLMContentDiffMode
 *
 *DESCRIPTION	:Create a message in Local Memory
 *				 a) Create the Message with Local memory
 *				 b) To Get a pointer to a message with
 *		            invalid access parameter(OSAL_EN_BINARY)
 *				 c) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_WRITEONLY)
 *				 d) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_READONLY)
 *				 e) To Get a pointer to a message with
 *		            valid access parameter(OSAL_EN_READWRITE)
 *				 f) Delete the Message
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_009
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MSGPLGetLMContentDiffMode( tVoid )
{
   tS32 s32OSAL_Ret = 0;
   
   tU32 u32Ret = 0;
   tU32 u32Size = 0;
   tU32 u32status = 0;
   tPU8 MsgPtr = NULL;
   OSAL_trMessage OSAL_msg;
   u32Size = MSGPL_POOLSIZE_FIVE_TH;

   s32OSAL_Ret = OSAL_s32MessagePoolOpen( );

   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
	  u32Ret = u32MSGPLMessagePoolOpenStatus();     
   }
   else
   {

      /* Get Absolut and Current Size */
	  u32MSGPLCGetAbsolute_CurrentSize();
	   
	  u32Size = MSGPL_MSG_SIZE;

	  /* Create the Message with Local memory */
	  s32OSAL_Ret = OSAL_s32MessageCreate( &(OSAL_msg), u32Size, 
	  									OSAL_EN_MEMORY_LOCAL );	  

	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     /* check the error status returned */
	     u32status = OSAL_u32ErrorCode();
		 
	     switch( u32status )
	     {
	        case OSAL_E_INVALIDVALUE:		 
	           u32Ret = 1;
	           break;
	        case OSAL_E_NOSPACE:
	           u32Ret = 2;
	           break;
	  	    case OSAL_E_DOESNOTEXIST:
		       u32Ret = 3;
		       break;
		    default:
			   u32Ret = 4;
		 }//end of switch( u32status )
	     
	  }
	  else
	  {
	     /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_WRITEONLY)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_WRITEONLY );
		 if( OSAL_NULL == MsgPtr )
		 {
		 	OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Message pointer NULL in WRITE ONLY \n",
      					u32status 
    				 );   
		 }
		 /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_READONLY)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READONLY );
		 if( OSAL_NULL == MsgPtr )
		 {
		    OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Message pointer NULL in READONLY \n",
      					u32status 
    				 ); 
    	  }
		  

		 /* To Get a pointer to a message with
	   	  valid access parameter(OSAL_EN_READWRITE)*/
		 MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE );
		 if( OSAL_NULL == MsgPtr )
		 {
		    OEDT_HelperPrintf( 
      					TR_LEVEL_USER_1, 
      					"Message pointer NULL in READWRITE \n",
      					u32status 
    				 ); 
		 }
		  
		 /* Delete the Message */
		 s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 5000;    

		 }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)

	   }

      /* Close the Message pool, if create with Size 5000 Byte */
	  s32OSAL_Ret = OSAL_s32MessagePoolClose(); 	
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 500 ;

	  }//end of if( OSAL_ERROR == s32OSAL_Ret )	(close MSGPL)
	  
	  
   }//end of if( OSAL_ERROR == s32OSAL_Ret )(create MSGPL)

   return u32Ret ;
}  
/******************************************************************************
 *FUNCTION		:u32MSGPLDeleteMessageNULL
 *
 *DESCRIPTION	:Create a message in shared Memory
 *				 a)  Try to delete a message with NULL
 *				    
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *
 *TEST CASE		:TU_OEDT_MSGPL_010
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MSGPLDeleteMessageNULL( tVoid )
{
   tU32 u32Ret = 0;
   tS32 s32OSAL_Ret = 0;
   /*rav8kor - uninitialised declaration gives lint warning.hence invalid memory location is used*/
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};
  
   /* Try to delete a message with invalid parameter */
   s32OSAL_Ret = OSAL_s32MessageDelete( OSAL_msg );
   if( OSAL_ERROR != s32OSAL_Ret )
   {
      u32Ret = 10;    

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Delete)

   /* check the error status returned */
   s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
   if ( s32OSAL_Ret != (tS32)OSAL_E_INVALIDVALUE )	   
   {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessageDelete() failed, ErrorCode=%i ", s32OSAL_Ret );	    
      u32Ret += 50; 
   }//end of if ( OSAL_E_INVALIDVALUE != s32OSAL_Ret )

   return u32Ret;
}


/******************************************************************************
 *FUNCTION		:u32MSGPLMsgCreateWithNoMSGPLInSH
 *DESCRIPTION	:Trying to Create a Message in Shared Memory without a 
 				 prior Message Pool open.Should fail with error code 
				 OSAL_E_DOESNOTEXIST.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_011
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
                15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 ******************************************************************************/
tU32 u32MSGPLMsgCreateWithNoMSGPLInSH( tVoid )
{
	/*Definitions*/
	tU32 u32Ret = 0;
	OSAL_trMessage OSAL_msg;

	/*Try to Create a Message in shared memory with no prior Message Pool Create*/
	if( OSAL_ERROR == OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED ) )
	{
		/*Collect and query the error code*/
		if( OSAL_E_DOESNOTEXIST !=  OSAL_u32ErrorCode( )  )
		{
			/*Wrong error code*/
			u32Ret = 1000;
		}
	}
	else
	{
		/*Message has been created, set error code*/
		//u32Ret = 20000;
		u32Ret = 0; /*open is done only once at system start for shm*/
		/*Attempt shutdown*/
		OSAL_s32MessageDelete( OSAL_msg );
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLMsgCreateWithNoMSGPLInLM
 *DESCRIPTION	:Create a Message in Local memory Memory without a 
 				 prior Message Pool Create.Should pass
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_012
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLMsgCreateWithNoMSGPLInLM( tVoid )
{
	/*Definitions*/
	OSAL_trMessage OSAL_msg;

	/*Try to Create a Message in local memory with no prior Message Pool Create*/
	if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL ) )
	{		
		/*Shutdown*/
		if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msg ) )
		{
			/*Message Delete failed,return immediately*/
			return 5000;
		}
	}
	else
	{
		/*Message Create has failed,return immediately*/
		return 10000;
	}

	/*Return the error code,success*/
	return 0;
}
/******************************************************************************
 *FUNCTION		:u32MSGPLCheckMSGPL
 *DESCRIPTION	:Call the Check Message Pool API on a Created Message Pool
 				 With Message Pool of Size 1 and 5000			
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_013
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLCheckMSGPL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret = 0;
    
	/*Create a Message Pool passing arguement value huge*/
	if( OSAL_ERROR != OSAL_s32MessagePoolOpen( ) )
	{
		/*Message Pool Check*/
		if( OSAL_ERROR == OSAL_s32CheckMessagePool( ) )
		{
			u32Ret += 20000;
		}

		/*Close the message Pool and delete it*/
		if( OSAL_ERROR != OSAL_s32MessagePoolClose( ) )
		{
			
		}
		else
		{
			/*Message Pool Close failed*/
			u32Ret += 40000;
		}
	}
	else
	{
		/*Message Pool Create failed*/
		u32Ret += 60000;
	}
	/*Create a Message Pool passing arguement value huge*/

	/*Return the error code*/
	return u32Ret;
}	

/******************************************************************************
 *FUNCTION		:u32MSGPLCheckMSGPLNoMSGPL
 *DESCRIPTION	:Try calling the CheckMessagePool API without a prior message 
				 pool create,should return OSAL_ERROR
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_014
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLCheckMSGPLNoMSGPL( tVoid )
{
	/*Call Message Pool Memory Check function,without Message Pool Create*/
	if( OSAL_ERROR != OSAL_s32CheckMessagePool( ) )
	{
		/*Return error immediately*/
		return 10000;
	}
	
	/*Return success*/
	return 0;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMessageSizeMsgInLM
 *DESCRIPTION	:Call the OSAL_u32GetMessageSize API on a message 
				 in Local Memory( LM )
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_015
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMessageSizeMsgInLM( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 	= 0;
	
	OSAL_trMessage OSAL_msg;
	
	/*Try to Create a Message in local memory with no prior Message Pool Open*/
	if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL ) )
	{		
		u32Ret = 10000;
		/*Get message size on a message in Local Memory*/
		if( MSGPL_MSG_SIZE !=  OSAL_u32GetMessageSize( OSAL_msg ) )
		{
			/*Wrong Size returned*/
			u32Ret = 10000;
		}
		
		/*Shutdown*/
		if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msg ) )
		{
			/*Set the error code*/
			u32Ret += 20000;
		}
	}
	
	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMessageSizeMsgInSM
 *DESCRIPTION	:Call the OSAL_u32GetMessageSize API on a message 
				 in Shared Memory( SM )
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_016
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMessageSizeMsgInSM( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 	= 0;
	OSAL_trMessage OSAL_msg;

	/*Open a message pool*/
	if( OSAL_ERROR != OSAL_s32MessagePoolOpen( ) )
	{
		/*Create a message in Shared Memory*/
		if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED ) )
		{
			if( MSGPL_MSG_SIZE !=  OSAL_u32GetMessageSize( OSAL_msg ) )
			{
				/*Wrong Size returned*/
				u32Ret = 10000;
			}

			/*Shutdown*/
			if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msg ) )
			{
				/*Set the error code*/
				u32Ret += 20000;
			}
		}
		else
		{
			/*Message Creation failed*/
			u32Ret += 30000;
		}

		/*Close the Message Pool*/
		if( OSAL_ERROR != OSAL_s32MessagePoolClose( ) )
		{
			
		}
		else
		{
			/*Message Pool Close failed*/
			u32Ret += 50000;
		}
	}
	else
	{
		/*Message Pool Open failed*/
		u32Ret += 80000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMessageSizeNoMsg
 *DESCRIPTION	:Try calling the OSAL_u32GetMessageSize API passing NULL
				 as arguement
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_017
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMessageSizeNoMsg( tVoid )
{
	/*Definitions*/
	/*rav8kor - uninitialised declaration gives lint warning.hence invalid memory location is used*/
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};


	/*Try Getting Message Size Message that was never created*/
	if( OSAL_NULL != OSAL_u32GetMessageSize( OSAL_msg ) )
	{
		/*Get Message Size passes for NULL parameter,return immediately*/
		return 10000;
	}

	/*Return success*/
	return 0;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMinimalSize
 *DESCRIPTION	:Get minimal size of the message pool
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_018
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMinimalSize( tVoid )
{
	/*Definitions*/
	tU32 u32Ret      = 0;
	tS32 s32Size     = 0;
	tS32 s32SizeNext = 0;
	OSAL_trMessage OSAL_msg;
	OSAL_trMessage OSAL_msgNext;

	/*Create a Message Pool*/
	if( OSAL_ERROR != OSAL_s32MessagePoolOpen( ) )
	{
		/*Create a message within in Pool( Shared Memory )*/
		if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED ) )
		{
			/*Query the Minimal Size of the Message Pool*/
			if( OSAL_ERROR == ( s32Size = OSAL_s32MessagePoolGetMinimalSize( ) ) )
			{
				/*Get minimal size failed*/
				u32Ret = 1000;
			}
			else
			{
				/*Create next message within the Pool( Shared Memory )*/
				if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msgNext,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED ) )
				{
					/*Query the Minimal Size of the Message Pool*/
					if( OSAL_ERROR == ( s32SizeNext = OSAL_s32MessagePoolGetMinimalSize( ) ) )
					{
						/*Get minimal size failed*/
						u32Ret = 1000;
					}
					else
					{
						/*Compare two sizes - s32SizeNext must be lesser than s32Size*/
						if( s32SizeNext >= s32Size )
						{
							/*Set error*/
							u32Ret += 10000;
						}
					}

					/*Delete the Created message*/
					if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msgNext ) )
					{
						u32Ret += 1500;
					}
				}
				else
				{
					/*Message Create failed*/
					u32Ret += 2000;
				}
			}

			/*Delete the message from MP*/
			if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msg ) )
			{
				u32Ret += 2500;
			}
		}
		else
		{
			/*Message Create failed*/
			u32Ret += 3000;
		}

		/*Close the Message Pool*/
		if( OSAL_ERROR != OSAL_s32MessagePoolClose( ) )
		{
						
		}
		else
		{
			/*Message Pool Close failed*/
			u32Ret += 50000;
		}
	}
	else
	{
		/*Message Pool Creation failed*/
		u32Ret += 80000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMinimalSizeCreatMSGLM
 *DESCRIPTION	:Get minimal size of the message pool after creating a message
				 in Local memory.The minimal size should not be affected
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_019
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
                 15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
 ******************************************************************************/
tU32 u32MSGPLGetMinimalSizeCreatMSGLM( tVoid )
{
	/*Definitions*/
	tU32 u32Ret      = 0;
	tS32 s32Size     = 0;
	tS32 s32SizeNext = 0;
	OSAL_trMessage OSAL_msg;
	OSAL_trMessage OSAL_msgNext;

	
	/*Open a Message Pool*/
	if( OSAL_ERROR != OSAL_s32MessagePoolOpen( ) )
	{
		/*Create a message within in Pool( Shared Memory )*/
		if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED ) )
		{
			/*Query the Minimal Size of the Message Pool*/
			if( OSAL_ERROR == ( s32Size = OSAL_s32MessagePoolGetMinimalSize( ) ) )
			{
				/*Get minimal size failed*/
				u32Ret = 1000;
			}
			else
			{
				/*Create next message within Local Memory*/
				if( OSAL_ERROR != OSAL_s32MessageCreate( &OSAL_msgNext,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL ) )
				{
					/*Query the Minimal Size of the Message Pool*/
					if( OSAL_ERROR == ( s32SizeNext = OSAL_s32MessagePoolGetMinimalSize( ) ) )
					{
						/*Get minimal size failed*/
						u32Ret = 1000;
					}
					else
					{
						/*Compare two sizes*/
						if( s32SizeNext != s32Size )
						{
							/*Set error*/
							u32Ret += 10000;
						}
					}

					/*Delete the Created message*/
					if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msgNext ) )
					{
						u32Ret += 1500;
					}
				}
				else
				{
					/*Message Create failed*/
					u32Ret += 2000;
				}
			}

			/*Delete the message from MP*/
			if( OSAL_ERROR == OSAL_s32MessageDelete( OSAL_msg ) )
			{
				u32Ret += 2500;
			}
		}
		else
		{
			/*Message Create failed*/
			u32Ret += 3000;
		}

		/*Close and Delete the Message Pool*/
		if( OSAL_ERROR != OSAL_s32MessagePoolClose( ) )
		{
			
		}
		else
		{
			/*Message Pool Close failed*/
			u32Ret += 50000;
		}
	}
	else
	{
		/*Message Pool Creation failed*/
		u32Ret += 80000;
	}

	/*Return the error code*/
	return u32Ret;
} 

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMinimalSizeNoMSGPL
 *DESCRIPTION	:Try calling the OSAL_s32MessagePoolGetMinimalSize API without
				 a previous Message Pool create
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_020
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMinimalSizeNoMSGPL( tVoid )
{
	
	 
	/*Call OSAL_s32MessagePoolGetMinimalSize without Message
	Pool Create*/
	if( OSAL_ERROR == OSAL_s32MessagePoolGetMinimalSize( ) )
	{
		if( OSAL_E_DOESNOTEXIST	 !=  OSAL_u32ErrorCode( )  )
		{
			/*Wrong error code,return immediately*/
			return 5000;
		}
	}
	else
	{
		/*OSAL_s32MessagePoolGetMinimalSize passed!*/
		return 10000;
	}

	/*Return success*/
	return 0;
}

/******************************************************************************
 *FUNCTION		:u32MSGPLGetMessageMaxSizeInvalid
 *DESCRIPTION	:Try calling the OSAL_u32GetMaxMessageSize API without
				 a previous Message Pool or Message Create,should return
				failure
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_021
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMessageMaxSizeInvalid( tVoid )
{
   
	/*Try getting the Maximum Message Size*/
	if( (tU32)OSAL_ERROR == OSAL_u32GetMaxMessageSize( ) )
	{
		if( OSAL_E_DOESNOTEXIST !=  OSAL_u32ErrorCode( )  )
		{
			/*Return immediately*/
			return 1000;
		}
	}
	else
	{
		/*Get Max Message Size passed!*/
		return 2000;	
	}

	/*Return error code*/
	return 0;
}
/******************************************************************************
 *FUNCTION		:u32MSGPLGetMessageMaxSizeValid
 *DESCRIPTION	:Call the OSAL_u32GetMaxMessageSize API with valid 
				 parameters				 
 *PARAMETER		:none
 *RETURNVALUE	:tU32, "0" on success  or "non-zero" value in case of error
 *TEST CASE		:TU_OEDT_MSGPL_022
 *HISTORY		:31-01-2008, Tinoy Mathews( RBIN/ECM1 )
 ******************************************************************************/
tU32 u32MSGPLGetMessageMaxSizeValid( tVoid )
{
	/*Declaraions*/
	tU32 u32Ret      = 0;
	/*Sizes*/
	tU32 u32Size_1   = 0;
	tU32 u32Size_2 	 = 0;
	/*Returns from MessageCreate*/
	tS32 s32Ret_1    = OSAL_E_NOERROR;
	tS32 s32Ret_2    = OSAL_E_NOERROR;
	tS32 s32Ret_3    = OSAL_E_NOERROR;
	/*Message Handles*/
	OSAL_trMessage OSAL_msg_1;
	OSAL_trMessage OSAL_msg_2;
	OSAL_trMessage OSAL_msg_3;

	/*OPen a message Pool*/
	if( OSAL_ERROR != OSAL_s32MessagePoolOpen( ) )
	{
		/*Query the Maximum Message Size*/
		if( (tU32)OSAL_ERROR != ( u32Size_1 = OSAL_u32GetMaxMessageSize( ) ) )
		{
			if( 0 != u32Size_1 )
			{
				/*Set error code*/
				u32Ret += 100;
			}
		}
		else
		{
			u32Ret += 300;
		}

		/*Create 3 messages in the Message Pool*/
		s32Ret_1 = OSAL_s32MessageCreate( &OSAL_msg_1,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED );
		s32Ret_2 = OSAL_s32MessageCreate( &OSAL_msg_2,MSGPL_MSG_SIZE-100,OSAL_EN_MEMORY_SHARED );
		s32Ret_3 = OSAL_s32MessageCreate( &OSAL_msg_3,MSGPL_MSG_SIZE-200,OSAL_EN_MEMORY_SHARED );

		if( OSAL_ERROR != s32Ret_1 )
		{
			if( OSAL_ERROR != s32Ret_2 )
			{
				if( OSAL_ERROR != s32Ret_3 )
				{
					if( (tU32)OSAL_ERROR == ( u32Size_2 = OSAL_u32GetMaxMessageSize( ) ) )
					{
						/*Get Message Max size failed*/
						u32Ret += 500;
					}
					else
					{
						 
						/*Max message size returns the maximum passed message 
						size plus the message Header Size*/
						if( MSGPL_MSG_SIZE >= u32Size_2 )
						{
							u32Ret += 400;
						}

					}
					
					/*Delete the Message*/
					OSAL_s32MessageDelete( OSAL_msg_3 );
				}
				else
				{
					/*Message 1 Create failed*/
					u32Ret += 700;
				}
				/*Delete the Message*/
				OSAL_s32MessageDelete( OSAL_msg_2 );
			}
			else
			{
				/*Message 2 Create failed*/
				u32Ret += 800;
			}
			/*Delete the Message*/
			OSAL_s32MessageDelete( OSAL_msg_1 );
		}
		else
		{
			/*Message 1 Create failed*/
			u32Ret += 900;
		}

		/*Close the Message Pool and Delete*/
		if( OSAL_ERROR != OSAL_s32MessagePoolClose( ) )
		{
			
		}
		else
		{
			/*Message Pool Close failed*/
			u32Ret += 50000;
		}
	}
	else
	{
		/*Message Pool Creation failed*/
		u32Ret += 1000;
	}

	/*Return the srror code*/
	return u32Ret;
}
                                                                                                                                                                                                                            

