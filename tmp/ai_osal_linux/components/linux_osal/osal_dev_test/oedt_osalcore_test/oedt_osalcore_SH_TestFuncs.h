/************************************************************************
 *FILE          :oedt_osalcore_SH_TestFuncs.h
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION   :This file contains function prototypes and some Macros that 
 *				 will be used in the file oedt_osalcore_SH_TestFuncs.c 
 *				 for OSAL_CORE APIs.
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       :
 *              ver1.4   - 22-04-2009
                lint info removal
				sak9kor

 *				ver1.3	 - 14-04-2009
                warnings removal
				rav8kor

 				 31.01.2008 Rev 1.2 Tinoy Mathews( RBIN/ECM1 )
 				 Added 6 Prototypes
 
 				 24.11.2007 Rev. 1.1 Tinoy Mathews ( RBIN/EDI3)
                 Added 3 prototypes for Memory Map OSAL API test cases.

 				:06.11.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3) 
 *         		 Initial Revision.
 ************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_SH_TESTFUNCS_HEADER
#define OEDT_SH_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define SH_SIZE 100
#define SH_NAME "SH_MEM"
#define SH_INVAL_NAME "-------|-------|-------|-------|-------|"   /* name length = 40 */

#define SH_MEMSIZE 1000
#define SH_MEMSIZE_512 		512
#undef  VALID_STACK_SIZE
#define VALID_STACK_SIZE 	2048
#define SH_HARRAY 9
#define SH_MAX 9
#define SH_MIN 0
#define INVAL_HANDLE -1
#define OSAL_INVALID_ACCESS 0x0200
#define MAP_OFFSET    20
#define MAP_LENGTH    30
#define LENGTH_127          127
#define ZERO_LENGTH   0
#define ZERO_OFFSET         0
#define OFFSET_0            0
#define OFFSET_128          128
#define OFFSET_256          256
#define OFFSET_384          384
#define OFFSET_511          511
#define OFFSET_800          800
#define PATTERN_THR1        0x00000001
#define PATTERN_THR2        0x00000002
#define PATTERN_THR3        0x00000004
#define PATTERN_THR4        0x00000008
#undef  THREE_SECONDS
#define THREE_SECONDS       3000

#undef 	DEBUG_MODE

#define DEBUG_MODE          0

#define VALID_STACK_SIZE        2048
#define SH_SIZE  100

/* Function prototypes of functions used 
				in file oedt_osalcore_SH_TestFuncs.c */
/*Test cases*/

tU32 u32SHCreateDeleteNameNULL(tVoid);							//TU_OEDT_SH_001
tU32 u32SHCreateNameVal(tVoid);									//TU_OEDT_SH_002
tU32 u32SHCreateDeleteNameExceedNameLength (tVoid);				//TU_OEDT_SH_003
tU32 u32SHCreateNameSameNames(tVoid);							//TU_OEDT_SH_004
tU32 u32SHCreateSizeZero(tVoid);								//TU_OEDT_SH_005
tU32 u32SHDeleteNameInval(tVoid);								//TU_OEDT_SH_006
tU32 u32SHDeleteNameInUse(tVoid);								//TU_OEDT_SH_007
tU32 u32SHOpenCloseDiffModes(tVoid);							//TU_OEDT_SH_008
tU32  u32SHOpenNameNULL( tVoid );								//TU_OEDT_SH_009
tU32 u32SHOpenNameInVal(tVoid);									//TU_OEDT_SH_010
tU32 u32SHCloseNameInVal(tVoid);								//TU_OEDT_SH_011
tU32 u32SHCreateMaxSegment(tVoid);								//TU_OEDT_SH_012
tU32 u32SHMemoryMapNonExistentHandle( tVoid );
tU32 u32SHMemoryMapLimitCheck( tVoid );
tU32 u32SHMemoryMapDiffAccessModes( tVoid );

tU32 u32SHMemoryThreadAccess( tVoid );							//TU_OEDT_SH_016
tU32 u32SHMemoryScanMemory( tVoid );
tU32 u32SHMemoryScanMemoryBeyondLimit( tVoid );
tU32 u32UnmapNonMappedAddress( tVoid );
tU32 u32UnmapNULLAddress( tVoid );
tU32 u32UnmapMappedAddress( tVoid );							//TU_OEDT_SH_021
tU32 u32SHOpenDoubleCloseOnce( tVoid );
tU32 u32SHCreateNameValLoop( tVoid );

tVoid Thread_SH_0_127( tVoid * ptr );				
tVoid Thread_SH_128_255( tVoid * ptr );
tVoid Thread_SH_256_383( tVoid * ptr );
tVoid Thread_SH_384_511( tVoid * ptr );

tU32 u32SHMemoryThreadAccess( tVoid );
void Thread_SH_Memory(tVoid* pvArg);

/*Test cases*/

#endif
