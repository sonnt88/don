/******************************************************************************
 *FILE          :oedt_osalcore_SH_TestFuncs.c
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for 
 *               the shared Memory OSAL_CORE APIs
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       :ver1.8   - 26-02-2013
                fix for oedt u32UnmapNonMappedAddress
				sja3kor								
 
 *              ver1.7   - 22-04-2009
                lint info removal
				sak9kor

  *								
  				ver1.6	 - 14-04-2009
                warnings removal
				rav8kor
 				 
 				 Version 1.5 
 				 Event clear update by  
*				 Anoop Chandran (RBEI\ECM1) 27/09/2008

 				 31.01.2008 Rev. 1.4 Tinoy Mathews( RBIN/ECM1 )
 				 Added cases:
 				 TU_OEDT_SH_016 <-> TU_OEDT_SH_021

 				 18.12.2007 Rev. 1.3 Tinoy Mathews( RBIN/ECM1 )
                 Updated case : TU_OEDT_SH_007
 
 				 4.12.2007 Rev.  1.2 Tinoy Mathews( RBIN/ECM1 )
                 Updated cases : 002,004,006,007,008,010,011
 				
 				:24.11.2007 Rev. 1.1 Tinoy Mathews( RBIN/EDI3)
				 Added cases 013,014,015( Memory Map OSAL API tests )
 
 				:06.11.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3) 
 *               Initial Revision.
 *****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

//#include "oedt_osalcore_SH_common_TestFuncs.h"
#include "oedt_osalcore_SH_TestFuncs.h"
#include "oedt_helper_funcs.h"


/*Global Variables*/
extern OSAL_tShMemHandle ghShared;
extern OSAL_tEventHandle gSHEventHandle;
extern OSAL_tEventMask   gevMask;
/*Global Variables*/

/******************************************************************************
 *FUNCTION		:u32SHSharedMemoryCreateStatus
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHSharedMemoryCreateStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_INVALIDVALUE: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_ALREADYEXISTS:
		 u32Ret = 2;
		 break;
	  case OSAL_E_NOSPACE:
		 u32Ret = 3;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 4;
		 break;
	  case OSAL_E_NAMETOOLONG:
		 u32Ret = 5;
		 break;
	  default:
		 u32Ret = 6;
   }//end of switch( u32status )

   return u32Ret; 
} 

/******************************************************************************
 *FUNCTION		:u32SHSharedMemoryOpenStatus
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHSharedMemoryOpenStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_BUSY: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_DOESNOTEXIST:
		 u32Ret = 1;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 2;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 3;
		 break;
	  default:
		 u32Ret = 4;
   }//end of switch( u32status )

   return u32Ret; 
} 
/******************************************************************************
 *FUNCTION		:u32SHSharedMemoryCloseStatus
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHSharedMemoryCloseStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_NOPERMISSION: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 2;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 3;
		 break;
	  default:
		 u32Ret = 4;
   }//end of switch( u32status )

   return u32Ret; 
} 


/******************************************************************************
 *FUNCTION		:u32SHCreateDeleteNameNULL
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with Name NULL
 *				 b)check the error status returned
 *				 c)Delete the shared Memory with Name NULL
 *				 d)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_001
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHCreateDeleteNameNULL(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Name NULL */
   SHHandle = OSAL_SharedMemoryCreate(OSAL_NULL, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)
   
   /* Delete the shared Memory with Name NULL */
   s32OSAL_Ret = OSAL_s32SharedMemoryDelete( OSAL_NULL );
   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
     
      u32status =  OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret += 0;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret += 10;
		    break;
	     case OSAL_E_DOESNOTEXIST:
		    u32Ret += 20;
		    break;
	     default:
		    u32Ret += 30;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )
   else
   {
      u32Ret += 50;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHCreateNameVal
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_002
 *
 *HISTORY		:
 *				 06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				 4.12.2007  Updated By Tinoy Mathews(RBIN/EDI3)
 *****************************************************************************/
tU32 u32SHCreateNameVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;

   /* Create the shared Memory with vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  		/*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
	     		u32Ret += 50;
	  		}
	  }
   }
   else
   {
     	/*Check the error status returned */
     	u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHCreateDeleteNameExceedNameLength
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with name exceed name length 32 
 *				 b)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_003
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHCreateDeleteNameExceedNameLength(tVoid)
{
   tU32 u32status = 0;
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with name length 40*/
   SHHandle = OSAL_SharedMemoryCreate(SH_INVAL_NAME, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status =  OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_NAMETOOLONG:  /* Expected error */ 
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)

   /* Delete the shared Memory with name length 40*/
   s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_INVAL_NAME );
   if( OSAL_ERROR == s32OSAL_Ret )
   {
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_DOESNOTEXIST:  /* Expected error */   
   	        u32Ret += 0;
		    break;
         case OSAL_E_NAMETOOLONG: 
            u32Ret += 10;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret += 20;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret += 30;
		    break;
	     default:
		    u32Ret += 40;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )
   else
   {
      u32Ret += 100;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret ) (Delete)

   return u32Ret ;


}
/******************************************************************************
 *FUNCTION		:u32SHCreateNameSameNames
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)Create the shared Memory again with same name
 *  			 c)close and delete if shared Memory again with same name
 *				 d)close and delete first shared Memory 
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_004
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
                 4.12.2007   Updated by Tinoy Mathews(RBIN/EDI3)
 *****************************************************************************/
tU32 u32SHCreateNameSameNames(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;
   OSAL_tShMemHandle SHHandle1 = 0;


   /* Create the shared Memory*/
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {

	  /* Create the shared Memory again with same name*/	
      SHHandle1 = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
	  if( OSAL_ERROR != SHHandle1 )
	  {
	     u32Ret = 60;
	     /*Close the handle */
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle1);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += 100;
		 }
		 else
		 {
		 	/*Delete the shared Memory with Name*/
		 	s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
		 	if( OSAL_ERROR == s32OSAL_Ret )
		 	{
		    	u32Ret += 100;
		 	}
		}
	  }
	  else
	  {
      		/*Check the error status returned */
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_ALREADYEXISTS:  /* Expected error */  
		    u32Ret = 0;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 10;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 20;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 30;
		    break;
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 40;
		    break;
	     default:
		    u32Ret = 50;
      		}
	  }
	 	     
	  /*Close the handle */
	  s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 500;
	  }
	  else
	  {
	  /* Delete the shared Memory with Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 1000;
	  	   }
	  }
   }
   else
   {
      /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHCreateSizeZero
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory with Name NULL
 *				 b)check the error status returned
 *				 c)Delete the shared Memory with Name NULL
 *				 d)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_005
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHCreateSizeZero(tVoid)
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Name SH_NAME */
   SHHandle = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_MIN);

   if( OSAL_ERROR == SHHandle )
   {
         
      /* check the error status returned */
     
      u32status = OSAL_u32ErrorCode();
      switch( u32status )
      {  
         case OSAL_E_INVALIDVALUE:  /* Expected error */
		    u32Ret = 0;
		    break;
	     case OSAL_E_ALREADYEXISTS:
		    u32Ret = 1;
		    break;
	     case OSAL_E_NOSPACE:
		    u32Ret = 2;
		    break;
	     case OSAL_E_UNKNOWN:
		    u32Ret = 3;
		    break;
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 4;
		    break;
	     default:
		    u32Ret = 5;
      }//end of switch( u32status )

   }//end of if( OSAL_ERROR == s32OSAL_Ret )(Create)
   else
   {
      u32Ret = 10;
   }//end of if-else( OSAL_ERROR == s32OSAL_Ret )(Create)

   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHDeleteNameInval
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)close and delete shared Memory 
 *				 c)Try to delete invalid shared Memory  
 *				 d)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_006
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				 4.12.2007   Updated by Tinoy Mathews(RBIN/EDI3)
 *****************************************************************************/
tU32 u32SHDeleteNameInval(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  	 /* Delete the shared Memory with Name */
	  	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	u32Ret += 50;
	  	 }
      
      /* Delete again the shared Memory with same Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
      if( OSAL_ERROR == s32OSAL_Ret )
      {
         	/*Check the error status returned */
         u32status = OSAL_u32ErrorCode();
         switch( u32status )
         {  
            case OSAL_E_DOESNOTEXIST:  /* Expected error */
		       u32Ret += 0;
		       break;
	        case OSAL_E_UNKNOWN:
		       u32Ret += 10;
		       break;
	        case OSAL_E_INVALIDVALUE:
		       u32Ret += 20;
		       break;
	        default:
		       u32Ret += 30;
         	}
   		}
   		else
   		{
      		u32Ret += 100;
   		}
      }
	}
    else
    {
       /* check the error status returned */
       u32Ret = u32SHSharedMemoryCreateStatus();
   	}
   	return u32Ret ;
}
/******************************************************************************
 *FUNCTION		:u32SHDeleteNameInUse
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory .
 *				 b)Delete shared Memory with out close.
 *				 c)Delete without close should pass.
 *				 d)On success of Delete, do a close to 
 *				 remove shared memory resources.
 *				 e)On success of close,do a reclose to
 *				 see if resources have actually been removed.
 *				 f)Return the error code.
 *
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_007
 *
 *HISTORY		:
 * 06.11.2007   Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *				 4.12.2007    Tinoy Mathews( RBIN/EDI3 )
 *				 Updated some checking error
 *               18.12.2007   Tinoy Mathews( RBIN/ECM1 )
 *				 Updated as per comments from MRK2HI
 *****************************************************************************/
tU32 u32SHDeleteNameInUse(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /*Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {

	  /*Delete the shared Memory with Name, with out close*/
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
      if( OSAL_ERROR == s32OSAL_Ret )
      {
         /*Shared memory delete passed before a close*/
      u32Ret = 100;
   	  }
   	  else
   	  {
      	 /*Close the handle,remove shared memory resources*/
      	 s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	 	 {
	  		 /*Should not fail*/
	  		 u32Ret += 100;
	  	 }
	  	 else
	  	 {
	  	 	/*Attempt a Redelete*/
			if( OSAL_OK == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				u32Ret += 2000;
			}
	  	 	/*Attempt a reclose*/
	  	 	if( OSAL_OK == ( s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle) ) )
			{
				u32Ret += 1000;
			}
		 }
	  }
   }
   else
   {
     /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   /*Return the error code*/
   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHOpenCloseDiffModes
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory 
 *				 b)delete shared Memory with out close
  *				 c)check the error status returned
 *				 d)if not delete, close and delete the shared memory
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_008
 *
 *HISTORY		:
 * 06.11.2007   Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				 4.12.2007    Tinoy Mathews( RBIN/EDI3 )
 *****************************************************************************/
tU32 u32SHOpenCloseDiffModes(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;
   OSAL_tShMemHandle SHHandle_RW = 0;
   OSAL_tShMemHandle SHHandle_W = 0;
   OSAL_tShMemHandle SHHandle_R = 0;
   OSAL_tShMemHandle SHHandle_Inval = 0;


   /*Create the shared Memory with Size SH_SIZE */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Open the shared memory with OSAL_EN_READWRITE) */ 
      SHHandle_RW = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE);
	  if(OSAL_ERROR == SHHandle_RW )
	  {
	     u32Ret = 100 + u32SHSharedMemoryOpenStatus();
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_READWRITE) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_RW);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret = (200 +u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_WRITEONLY) */ 
      SHHandle_W = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_WRITEONLY);
	  if(OSAL_ERROR == SHHandle_W )
	  {
	     u32Ret += (300 + u32SHSharedMemoryOpenStatus());
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_WRITEONLY) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_W);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += (400 + u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_READONLY) */ 
      SHHandle_R = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READONLY);
	  if(OSAL_ERROR == SHHandle_R )
	  {
	     u32Ret += (500 + u32SHSharedMemoryOpenStatus());
	  }
	  else
	  {
		 /* Close the shared memory with OSAL_EN_READONLY) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_R);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
		    u32Ret += (600 + u32SHSharedMemoryCloseStatus());
		 }
      }

      /*Open the shared memory with OSAL_EN_BINARY( invalid )) */ 
      SHHandle_Inval = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_BINARY);
	  if(OSAL_ERROR == SHHandle_Inval )
	  {
		 /*Check the error status returned */
		 u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_INVALIDVALUE: 		
			   u32Ret += 0;
			   break;
			case OSAL_E_DOESNOTEXIST:
			   u32Ret += 10;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 20;
			   break;
			case OSAL_E_BUSY:
			   u32Ret += 30;
			   break;
			default:
			   u32Ret += 40;
		   }
	  }
	  else
	  {
		 /*Close the shared memory with OSAL_EN_BINARY( invalid )) */ 
	     s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle_Inval);
		 if( OSAL_ERROR == s32OSAL_Ret )
		 {
			/*Check the error status returned */
			u32status = OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION: 
				  u32Ret += 50;
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 60;
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 70;
				  break;
			   default:
				  u32Ret += 80;
			}
		 }
      }

      /*Shutdown - Close the handle*/
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 1000;
	  }
	  else
	  {
	  	 /*Delete the shared Memory with Name*/
	 	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	u32Ret += 1000;
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned*/
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret ;

}
/******************************************************************************
 *FUNCTION		:u32SHOpenNameNULL
 *
 *DESCRIPTION	:
 *				 a)open the shared Memory name NULL
 *				 b)check the error status returned
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_009
 *
 *HISTORY		:
 * 06.11.2007   Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32  u32SHOpenNameNULL( tVoid )    
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;

   /* open the shared memory with OSAL_EN_READWRITE ) */ 
   SHHandle = OSAL_SharedMemoryOpen( OSAL_NULL, OSAL_EN_READWRITE);
   if(OSAL_ERROR == SHHandle )
   {
      /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE: 		
	  	    u32Ret = 0;
			break;
		 case OSAL_E_DOESNOTEXIST:
			u32Ret = 1;
			break;
		 case OSAL_E_UNKNOWN:
			u32Ret = 2;
			break;
		 case OSAL_E_BUSY:
			u32Ret = 3;
			break;
		 default:
			u32Ret = 4;
	  }//end of switch( u32status )
   }//end of if(OSAL_ERROR == SHHandle )
   else
   {
      u32Ret = 10;

   }//end of if-else (OSAL_ERROR == OSAL_EN_READWRITE )
   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHOpenNameInVal
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *   			 d)open after delete  shared Memory
 * 				 e)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_010
 *
 *HISTORY		:
 * 06.11.2007   Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				 4.12.2007    Tinoy Mathews( RBIN/EDI3 )
 *****************************************************************************/
tU32 u32SHOpenNameInVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with  vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if( OSAL_ERROR != SHHandle )
   {
      /*Shutdown - Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  	/*Delete the shared Memory with Name */
	  	s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	if( OSAL_ERROR == s32OSAL_Ret )
	  	{
	    	 u32Ret += 50;
	  	}
	  }

	  /*Open the shared memory with OSAL_EN_READWRITE )*/ 
	  SHHandle = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE);
	  if(OSAL_ERROR == SHHandle )
	  {
	     /*Check the error status returned */
	     u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_DOESNOTEXIST: 	  	
		  	   u32Ret += 0;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret += 60;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 70;
			   break;
			case OSAL_E_BUSY:
			   u32Ret += 80;
			   break;
			default:
			   u32Ret += 90;
		 }
	  }
	  else
	  {
	     u32Ret += 100;

		 /*Attempt a Shutdown - Close the handle */
      	 s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	    	 u32Ret = 10;
	  	 }
	  	 else
	  	 {
	  		/*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
	    	 	u32Ret += 50;
	  		}
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned */
     u32Ret = u32SHSharedMemoryCreateStatus();
   }

   return u32Ret;
}
/******************************************************************************
 *FUNCTION		:u32SHCloseNameInVal
 *
 *DESCRIPTION	:
 *				 a)Create the shared Memory
 *				 b)check the error status returned 
 *				 c)close and delete shared Memory
 *   			 d)close after delete  shared Memory
 * 				 e)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_011
 *
 *HISTORY		:
 * 06.11.2007   Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
				 4.12.2007    Tinoy Mathews( RBIN/EDI3 )
 *****************************************************************************/
tU32 u32SHCloseNameInVal(tVoid)
{
   tS32 s32OSAL_Ret = 0;
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle = 0;


   /* Create the shared Memory with  vaild Size */
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);

   if( OSAL_ERROR != SHHandle )
   {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret = 10;
	  }
	  else
	  {
	  /* Delete the shared Memory with Name */
	  s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  if( OSAL_ERROR == s32OSAL_Ret )
	  {
	     u32Ret += 50;
	  	 }
	  }

	  /*Close the shared memory*/ 
	   s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
	  if(OSAL_ERROR == s32OSAL_Ret )
	  {
	     /*Check the error status returned */
	     u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_INVALIDVALUE: 	  	
		  	   u32Ret += 0;
			   break;
			case OSAL_E_NOPERMISSION:
			   u32Ret += 60;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret += 70;
			   break;
			default:
			   u32Ret += 90;
		 }
	  }
	  else
	  {
	     u32Ret += 100;

		 /*Shutdown - Delete the shared Memory with Name */
	  	 s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  	 if( OSAL_ERROR == s32OSAL_Ret )
	  	 {
	     	 u32Ret += 50;
	  	 }
	  }
   }
   else
   {
      /*Check the error status returned */
      u32Ret = u32SHSharedMemoryCreateStatus();
   }

   /*Return error code*/
   return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHCreateMaxSegment
 *
 *DESCRIPTION	:
 *				 a)Create share memory segments
 *				 b)close and delete shared Memory segments
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_012
 *
 *HISTORY		:06.11.2007  Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32SHCreateMaxSegment(tVoid)
{
   tChar SH_name[ SH_HARRAY ][ SH_HARRAY ];
   tS32 s32OSAL_Ret = 0;
   tS32 s32Count = 0; 
   tS32 s32Count1 = 0; 
   tU32 u32Ret = 0;
   OSAL_tShMemHandle SHHandle[SH_HARRAY] = {0};


   OSAL_pvMemorySet(SH_name,0,sizeof(SH_name));

   /* Create  MAX No of shared Memory  */
   for( ; s32Count < SH_MAX; ++s32Count )
   {
   		 
      OSAL_s32PrintFormat( SH_name[ s32Count ], "SH_NAME%d", s32Count );

   	  /* Create the shared Memory with vaild Size */
      SHHandle[ s32Count ] = OSAL_SharedMemoryCreate( 
      												 SH_name[ s32Count ], 
      												 OSAL_EN_READWRITE, 
      												 SH_MEMSIZE 
      												);
   	  if( OSAL_ERROR  ==  SHHandle[ s32Count ] )
	  {
	     /* check the error status returned */
		 s32OSAL_Ret = (tS32) OSAL_u32ErrorCode();
		 if( s32OSAL_Ret  == (tS32)OSAL_E_NOSPACE )	
		 {										   
		    u32Ret = 10;
		 }
		 else
		 {
		    u32Ret = 50;
         }//end of if( OSAL_E_NOSPACE  == s32OSAL_Ret )
						 
		 break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )
   }

   --s32Count;

   /* close and Delete the shared Memory   */
   for( s32Count1 = s32Count; SH_MIN <= s32Count1; --s32Count1 )
   {
   		 
     /* close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle[ s32Count1 ]);
   	  if( OSAL_ERROR  == s32OSAL_Ret )
	  {
	     u32Ret += 100;
         break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )

   	  /* Create the shared Memory with vaild Size */
      s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_name[ s32Count1 ]);
   	  if( OSAL_ERROR  == s32OSAL_Ret )
	  {
	     u32Ret += 200;
         break;
	  }//end of if( OSAL_ERROR  == s32OSAL_Ret )
   }

   return u32Ret ;

}


/******************************************************************************
 *FUNCTION		:u32SHMemoryMapNonExistentHandle
 *DESCRIPTION	:Try Memory Map on a non existent Shared Memory( Created in 
                 the system and then deleted ) and on an invalid shared memory
				 handle			  
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_013
 *HISTORY		:24.11.2007  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tU32 u32SHMemoryMapNonExistentHandle( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;
	OSAL_tShMemHandle hShared_ = INVAL_HANDLE;

	/*Scenario 1*/
	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : u32Ret = 100;break;
			case OSAL_E_NAMETOOLONG  : u32Ret = 200;break;
			case OSAL_E_UNKNOWN      : u32Ret = 300;break;
			case OSAL_E_ALREADYEXISTS: u32Ret = 400;break;
			case OSAL_E_NOSPACE      : u32Ret = 500;break;
			default                  : u32Ret = 600;
		}

		/*Reset the error code*/
		u32ErrorValue = OSAL_E_NOERROR;
	}
	else
	{
		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret = 700;break;
				case OSAL_E_UNKNOWN     : u32Ret = 800;break;
				case OSAL_E_NOPERMISSION: u32Ret = 900;break;
				default                 : u32Ret = 1000;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret = 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret = 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret = 1300;break;
					default                 : u32Ret = 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
				/*Attempt a Memory Map!*/ 
				if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                                          MAP_LENGTH,MAP_OFFSET ) )
				{
					/*Valid behaviour, Memory Map should fail,the error code
					must be OSAL_E_DOESNOTEXIST!*/
					u32ErrorValue = OSAL_u32ErrorCode( );

					if( OSAL_E_DOESNOTEXIST != u32ErrorValue )
					{
						u32Ret += 1500;
					}

					/*Reset the error code*/
					u32ErrorValue = OSAL_E_NOERROR;
				}
				else
				{
					/*Memory Map returned the pointer,Critical API failure!*/
					u32Ret += 10000;
				}
			}/*Delete successful*/
		}/*Close successful*/
	}/*Create successful*/
	/*Scenario 1*/

	/*Scenario 2*/
	/*Attempt a Memory Map on an invalid handle!*/ 
	if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared_,OSAL_EN_READWRITE,
				                              MAP_LENGTH,MAP_OFFSET ) )
	{
		/*Valid behaviour, Memory Map should fail,the error code
		must be OSAL_E_INVALIDVALUE!*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		if( OSAL_E_INVALIDVALUE != u32ErrorValue )
		{
			u32Ret += 2500;
		}

		/*Reset the error code*/
		u32ErrorValue = OSAL_E_NOERROR;
	}
	else
	{
		/*Memory Map returned the pointer,Critical API failure!*/
	 	u32Ret += 20000;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHMemoryMapLimitCheck
 *DESCRIPTION	:Try Memory Map at the Limit os Shared Memory Size and
                 beyond that limit
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_014
 *HISTORY		:24.11.2007  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tU32 u32SHMemoryMapLimitCheck( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;

	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : 
			{
				
				return 100;
			}
			case OSAL_E_NAMETOOLONG  : 
			{
				
				return 200;
			}
			case OSAL_E_UNKNOWN      : 
			{
				
				return 300;
			}
			case OSAL_E_ALREADYEXISTS: 
			{
				
				return 400;
			}
			case OSAL_E_NOSPACE      :
			{
				
				 return 500;
			 }
			default                  : 
			{
				
				return 600;
			}
		}

		
	}
	else
	{
		/*At the end of Shared Memory*/
		if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	          ZERO_LENGTH,SH_MEMSIZE )  )
		{
			/*API failed!*/
			u32Ret += 900;
		}
		
		/*Beyond Limit of Shared Memory*/
		if( OSAL_NULL ==  OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	          MAP_LENGTH,SH_MEMSIZE )  )
		{
		   /*Correct Behaviour - Query the error code*/
		   u32ErrorValue = OSAL_u32ErrorCode( );
		   
		   if( 	OSAL_E_NOSPACE != u32ErrorValue )
		   {
		   		u32Ret += 1000;
		   }   
		}
		else
		{
			/*Critical API error!*/
			u32Ret += 10000;
		}

		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret += 700;break;
				case OSAL_E_UNKNOWN     : u32Ret += 800;break;
				case OSAL_E_NOPERMISSION: u32Ret += 950;break;
				default                 : u32Ret += 1050;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret += 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret += 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret += 1300;break;
					default                 : u32Ret += 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
		}/*Close successful*/
	}/*Create successful*/
	
	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHMemoryMapDiffAccessModes
 *DESCRIPTION	:Try Memory Map on a existent Shared Memory,for different 
                 access modes
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_015
 *HISTORY		:24.11.2007  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tU32 u32SHMemoryMapDiffAccessModes( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	tU32 u32ErrorValue         = OSAL_E_NOERROR;
	OSAL_tShMemHandle hShared  = 0;
	tPVoid ptr1                = OSAL_NULL;
	tPVoid ptr2                = OSAL_NULL;
	tPVoid ptr3                = OSAL_NULL;

	/*Create Shared Memory*/
	if( OSAL_ERROR == ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE ) ) )
	{
		/*Catch the error code*/
		u32ErrorValue = OSAL_u32ErrorCode( );

		/*Scan for the error code*/
		switch( u32ErrorValue )
		{
			case OSAL_E_INVALIDVALUE : return 100;
			case OSAL_E_NAMETOOLONG  : return 200;
			case OSAL_E_UNKNOWN      : return 300;
			case OSAL_E_ALREADYEXISTS: return 400;
			case OSAL_E_NOSPACE      : return 500;
			default                  : return 600;
		}

		
	}
	else
	{
		/*MEMORY MAP WITH MODE OSAL_EN_READONLY*/
		if( OSAL_NULL == ( ptr1 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READONLY,
				                              	          MAP_LENGTH,MAP_OFFSET ) ) )
		{
		   /*Memory Map returned NULL*/
	 		u32Ret += 10000;	
		}
		
		/*MEMORY MAP WITH MODE OSAL_EN_WRITEONLY*/
		if( OSAL_NULL == ( ptr2 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_WRITEONLY,
				                              	 		  MAP_LENGTH,MAP_OFFSET ) ) )
	    {
		    /*Memory Map returned NULL*/
	 		u32Ret += 20000;	
		}

		/*MEMORY MAP WITH MODE OSAL_EN_READWRITE*/
		if( OSAL_NULL == ( ptr3 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
				                              	 		  MAP_LENGTH,MAP_OFFSET ) ) )
		{
		    /*Memory Map returned NULL*/
	 		u32Ret += 30000;	
		}

		/*Check Address Returned - Should not be dependant on Access Modes.*/
		if( ptr1 == ptr2 )
		{
			if( !( ptr2 == ptr3 ) )
			{
				/*ptr2 and ptr3 not equal!*/
				u32Ret += 40000;
			}
		}
		else
		{
			/*ptr1 and ptr2 not equal!*/
			u32Ret += 50000;
		}

		/*MEMORY MAP WITH MODE OSAL_INVALID_ACCESS*/
		if( OSAL_NULL == OSAL_pvSharedMemoryMap( hShared,(OSAL_tenAccess)OSAL_INVALID_ACCESS,
				                              	 MAP_LENGTH,MAP_OFFSET ) )
		{
			/*Valid behaviour, Memory Map should fail,the error code
			must be OSAL_E_INVALIDVALUE!*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			if( OSAL_E_INVALIDVALUE != u32ErrorValue )
			{
				u32Ret += 800;
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Memory Map returned the pointer,Critical API failure!*/
	 		u32Ret += 60000;
		}

		/*Close the shared memory handle*/
		if( OSAL_ERROR == OSAL_s32SharedMemoryClose( hShared ) )
		{
			/*Catch the error code*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE: u32Ret += 700;break;
				case OSAL_E_UNKNOWN     : u32Ret += 800;break;
				case OSAL_E_NOPERMISSION: u32Ret += 950;break;
				default                 : u32Ret += 1050;  
			}

			/*Reset the error code*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Delete the shared memory from the system*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Catch the error code*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE: u32Ret += 1100;break;
					case OSAL_E_UNKNOWN     : u32Ret += 1200;break;
					case OSAL_E_DOESNOTEXIST: u32Ret += 1300;break;
					default                 : u32Ret += 1400;
				}
				
				/*Reset the error code*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
		}/*Close successful*/
	}/*Create successful*/

	/*Return the error code*/
	return u32Ret;
}
		
/******************************************************************************
 *FUNCTION		:u32SHMemoryScanMemory
 *DESCRIPTION	:Try to access Shared memory upto allocated length.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_017
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
*****************************************************************************/
tU32 u32SHMemoryScanMemory( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	tU32 u32Offset            = ZERO_OFFSET;
	#if DEBUG_MODE
	tU32 u32ECode             = OSAL_E_NOERROR;
	#endif
	tChar tcSeeker            = '\0';
	OSAL_tShMemHandle hShared = 0;
	tVoid * ptr               = OSAL_NULL;

	/*Create Shared Memory*/
	if( OSAL_ERROR != (tS32)( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512 ) ) )
	{
		/*Initialize the Shared Memory*/
		if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            	  					 ZERO_LENGTH,ZERO_OFFSET ) ) )
		{
			OSAL_pvMemorySet( ptr,'\0',SH_MEMSIZE_512 );
			OSAL_pvMemorySet( ptr,'A',( SH_MEMSIZE_512-1 ) );
		}
		else
		{
			/*Query reasons for Map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Restore resource*/
			if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
			{
			   if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			   {
			   		/*Query reasons for Delete failure*/
			   		#if DEBUG_MODE
					u32ECode = OSAL_u32ErrorCode( );
					#endif 		
			   }
			}
			else
			{
				/*Query reasons for Close failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}

			/*Return Immediately*/	
			return 10000;
		}
		
		/*Scan the Shared Memory*/
		while( u32Offset < ( SH_MEMSIZE_512-1 ) )
		{
			/*Scan Memory - Return address of location u32Offset bytes
			from Shared Memory Base*/
			if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            							 ZERO_LENGTH,u32Offset ) ) )
			{
				/*Access Shared Memory Location contents*/
				tcSeeker = *( (tChar *)ptr );
				/*Check for content*/
				if( tcSeeker != 'A' )
				{
					u32Ret += 100;
				}

			}
			else
			{
				/*Query reasons for map failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Set error code*/
				u32Ret += 10;
			}

			/*Increment offset*/
			u32Offset++;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
	}
	else
	{
		/*Return Immediately*/
		return 20000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32SHMemoryScanMemoryBeyondLimit
 *DESCRIPTION	:Try to access Shared memory upto allocated length.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_018
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
*****************************************************************************/
tU32 u32SHMemoryScanMemoryBeyondLimit( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			  = 0;
	#if DEBUG_MODE
	tVoid * ptr               = OSAL_NULL;
	tU32 u32ECode             = OSAL_E_NOERROR;
	tChar tcSeeker            = '\0';
	#endif
	OSAL_tShMemHandle hShared = 0;

	/*Create the Shared Memory*/
	if( OSAL_ERROR !=(tS32) ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE ) ) )
	{
		/*Map the shared memory*/
		//rav8kor - only commented and not removed as it can be used in debug mode
		//if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            						// ZERO_LENGTH,SH_SIZE-2 ) ) )
		if( OSAL_NULL != OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,
					            						 ZERO_LENGTH,SH_SIZE-2 ) )
		{
		 #if DEBUG_MODE	
			tcSeeker = *( (tChar * )( ((tChar*)ptr) + SH_SIZE ) );
		 #endif
		}
		else
		{
			/*Query reason for Map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
			
			/*Update error code*/
			u32Ret += 10000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
 
	}
	else
	{
		/*Query reason for Shared Memory Create failure*/
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	

		/*Update error code*/
		u32Ret += 20000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32UnmapNonMappedAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error( Error code should be
				 OSAL_E_DOESNOTEXIST )
 *TEST CASE		:TU_OEDT_SH_019
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
                 26.02.2013  updated correct errorcode sja3kor(RBEI/ECF5)
*****************************************************************************/
tU32 u32UnmapNonMappedAddress( tVoid )
{
	/*Declaration*/
	tU32 u32Ret = 0;
	tVoid * ptr = OSAL_NULL;

	/*Allocate memory dynamically*/
	ptr = OSAL_pvMemoryAllocate( (tU32)1 );

	/*Unmap a non mapped location*/
	if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( ptr, 1 ) )  //updated correct errorcode
	{

	}
	else
	{
		/* Error, unmap will pass with all non zero adderss 
		  this is known limitation of osal */
		//u32Ret += 10000;
	}

	if (ptr != OSAL_NULL) 
	{
		OSAL_vMemoryFree(ptr);
		ptr = OSAL_NULL;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32UnmapNULLAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error( Error code should be
				 OSAL_E_INVALIDVALUE )
 *TEST CASE		:TU_OEDT_SH_020
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
*****************************************************************************/
tU32 u32UnmapNULLAddress( tVoid )
{
	/*Declaration*/
	tU32 u32Ret = 0;
   
	/*Unmap a non mapped location*/
	if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( OSAL_NULL, 1 ) )
	{
		/*Query the error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 5000;
		}
	}
	else
	{
		/*Error, unmap should not pass if address passed is NULL*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/******************************************************************************
 *FUNCTION		:u32UnmapMappedAddress
 *DESCRIPTION	:Try to unmap a location that was never mapped
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_021
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
*****************************************************************************/
tU32 u32UnmapMappedAddress( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			  = 0;
	tVoid * ptr               = OSAL_NULL;
	#if DEBUG_MODE
	tU32 u32ECode             = OSAL_E_NOERROR;
	#endif
	OSAL_tShMemHandle hShared = 0;

	/*Create the shared memory*/
	if( OSAL_ERROR !=(tS32) ( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE ) ) )
	{
		/*Map shared memory*/
		if( OSAL_NULL != ( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,SH_SIZE,0 ) ) )
		{
			/*Unmap shared memory already mapped*/
			if( OSAL_ERROR == OSAL_s32SharedMemoryUnmap( ptr,SH_SIZE ) )
			{
				/*Query reasons for Unmap failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Set error code*/
				u32Ret += 10000;
			}
		}
		else
		{
			/*Query reasons for map failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Cannot Map Memory*/
			u32Ret += 5000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != OSAL_s32SharedMemoryClose( hShared ) )
		{
			if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
			{
				/*Query reasons for Delete failure*/
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				u32Ret  += 2000;
			}
		}
		else
		{
			/*Query reasons for Close failure*/
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			u32Ret += 3000;
		}
	}
	else
	{
		/*Query reasons for Shared Memory Create failure*/
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Update error code*/
		u32Ret += 7000;
	}

	/*Return the error code*/
	return u32Ret;
}
			

/******************************************************************************
 *FUNCTION     :u32SHOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *HISTORY      :07.07.2011  Dainius Ramanauskas
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
*****************************************************************************/
tU32 u32SHOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   tS32 s32ReturnValueThread = 0;
   OSAL_tShMemHandle handle_1 = 0;
   tPVoid map_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) Thread_SH_Memory;
   threadAttr.pvArg = &s32ReturnValueThread;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   handle_1 = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   if (handle_1 != OSAL_ERROR)
   {
      if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
      {
         OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
         if ((map_1 = OSAL_pvSharedMemoryMap(handle_1, OSAL_EN_READWRITE, SH_SIZE, 0)) != 0)
         {
            if ((OSAL_pvMemorySet(map_1, 0, SH_SIZE) && OSAL_pvMemorySet(map_1, 1, SH_SIZE)))
            {
               if (OSAL_s32SharedMemoryUnmap(map_1, SH_SIZE) != OSAL_ERROR)
               {
                  if (OSAL_s32SharedMemoryClose(handle_1) != OSAL_ERROR)
                  {
                     if (OSAL_s32SharedMemoryDelete(SH_NAME) != OSAL_ERROR)
                     {
                        // test ok
                     }
                     else
                        s32ReturnValue += 700;
                  }
                  else
                     s32ReturnValue += 600;
               }
               else
                  s32ReturnValue += 500;
            }
            else
               s32ReturnValue += 400;
         }
         else
            s32ReturnValue += 300;
      }
      else
         s32ReturnValue += 200;
   }
   else
      s32ReturnValue += 100;

   return s32ReturnValue;
}

/******************************************************************************
 *FUNCTION		:u32SHCreateNameValLoop()
 *
 *DESCRIPTION	:
 *				 a) Create the shared Memory
 *				 b) check the error status returned 
 *				 c) close and delete shared Memory
 *         d) go to a) and run everything in a loop
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *TEST CASE		:TU_OEDT_SH_002
 *
 *
 * HISTORY      :19.01.2012 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				                  copy of u32SHCreateNameVal()
 *
 *****************************************************************************/
tU32 u32SHCreateNameValLoop(tVoid)
{
  tS32 s32OSAL_Ret = 0;
  tU32 u32Ret = 0;
  OSAL_tShMemHandle SHHandle = 0;
  int loop = 0;
   
  for (loop=0; loop< 5000; loop++)
  {
    s32OSAL_Ret = 0;
    u32Ret = 0;
    SHHandle = 0;
   
    /* Create the shared Memory with vaild Size */
    SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
    
    if( OSAL_ERROR != SHHandle )
    {
      /*Close the handle */
      s32OSAL_Ret = OSAL_s32SharedMemoryClose(SHHandle);
      
      if( OSAL_ERROR == s32OSAL_Ret )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32SharedMemoryClose() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
        u32Ret = 10;
      }
      else
      {
        /*Delete the shared Memory with Name */
	  		s32OSAL_Ret = OSAL_s32SharedMemoryDelete( SH_NAME );
	  		if( OSAL_ERROR == s32OSAL_Ret )
	  		{
          OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32SharedMemoryDelete() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
          u32Ret += 50;
	  		}
      }
    }  
    else
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_SharedMemoryCreate() failed with error code '%i' in loop '%i'", OSAL_u32ErrorCode(), loop );
     	/*Check the error status returned */
     	u32Ret = u32SHSharedMemoryCreateStatus();
    }
  
    if (u32Ret != 0)
    {
      return u32Ret;
    }
  }
  
  return u32Ret;
}

