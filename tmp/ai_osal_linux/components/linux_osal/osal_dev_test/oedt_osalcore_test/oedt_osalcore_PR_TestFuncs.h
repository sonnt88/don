/******************************************************************************
 *FILE         : oedt_osalcore_PR_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the THREAD OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *              ver1.5   - 22-04-2009
                lint info removal
				sak9kor
 				
 				Version 1.4,31- 01- 2008
				Added case prototype 
				u32SpawnThreadInfiniteLoop
 				
 				Version 1.3,1- 1- 2008
				Changed the macro value
				MAX_THREADS from 300 to 100
 				
 				Version 1.2 , 4- 12- 2007
				Added 10 new prototypes 029 - 038
				(To test Process OSAL API Interface,
				and a few associated #defines)
				- Tinoy Mathews( RBIN/ECM1 )
 
 				Version 1.1 , 24- 11- 2007
 				Added 5 new prototypes 023 - 027
			    (To test ThreadWhoAmI and ThreadList OSAL APIs )
				- Tinoy Mathews( RBIN/EDI3 ) 
 
 				Version 1.0 , 26- 10- 2007
 *				Initial version - Tinoy Mathews( RBIN/EDI3 )	
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_PR_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_PR_TESTFUNCS_HEADER

/*Defines*/
#define INVALID_PRIORITY   160
#define INVALID_STACK_SIZE 1
#define VALID_STACK_SIZE     2048/*2KB, minimum is 1KB*/
#define FIVE_SECONDS         5000
#define SIX_SECONDS          6000
#define SEVEN_SECONDS        7000
#define TEN_SECONDS          10000
#define TWO_SECONDS          2000
#define MAX_THREADS          50
#define THREAD_NAME_SIZE     10
#define LIST_SIZE            300
#define EXCEPTION            0
#define PRIORITY             100
#define INVAL_PRIORITY       400
#define T_ENGINE_MAX_PROCESS 20
#define SIZE_INVAL           ((tS32)-15)
#define INVALID_PID          ((tS32)-100)
#define DEFAULT_PID          (tS32)0
#define KERNEL_MODE          0
#define  SHM_AREA "/shmarea"
#define  MYSHMSIZE 128

#define MESSAGEQUEUE_MP_CONTROL_MAX_MSG     10
#define MESSAGEQUEUE_MP_CONTROL_MAX_LEN     sizeof(tU32)
#define MESSAGEQUEUE_MP_P1_CONTROL_NAME     "MQ_P1_CTRL"
#define MESSAGEQUEUE_MP_P2_CONTROL_NAME     "MQ_P2_CTRL"
#define MESSAGEQUEUE_MP_RESPONSE_NAME       "MQ_RSPNS"


typedef enum
{
    MP_TEST_SPMVOLT = 1,
    MP_TEST_TESTRTC,
    MP_TEST_TESTMQ,
    MP_TEST_TESTMQ_IPC_PERF,
    MP_TEST_TESTMQ_IPC_CONTENT,
    MP_TEST_TESTMQ_OPEN_CLOSE_TWICE,

    MP_TEST_FFD_WRS,

    MP_TEST_KDS_WRITE_FULL,
    MP_TEST_KDS_READ_ENTRY,

    MP_TEST_MSGPOOL_OPEN,
    MP_TEST_MSGPOOL_CHECK_SIZE,

    MP_TEST_END
} MP_TEST_MESSAGE;

/*Helper Functions Declarations*/

/*Declarations*/
tU32  u32PRCreateThreadNameNULL( tVoid );
tU32  u32CreateThreadNameTooLong( tVoid );
tU32  u32CreateThreadWrongPriority( tVoid );
tU32  u32CreateThreadLowStack( tVoid );
tU32  u32CreateThreadEntryFuncNULL( tVoid );
tU32  u32DeleteThreadNotExisting( tVoid );
tU32  u32DeleteThreadWithNegID( tVoid );
tU32  u32CreateDeleteThreadValidParam( tVoid );
tU32  u32CreateTwoThreadSameName( tVoid );
tU32  u32ActivateThreadNonExisting( tVoid );
tU32  u32ActivateThreadWithNegID( tVoid );
tU32  u32ActivateThreadTwice( tVoid );
tU32  u32SuspendThreadNonExisting( tVoid );
tU32  u32SuspendThreadWithNegID( tVoid );
tU32  u32ResumeThreadNonExisting( tVoid );
tU32  u32ResumeThreadWithNegID( tVoid );
tU32  u32ChgPriorityThreadNonExisting( tVoid );
tU32  u32ChgPriorityThreadWithNegID( tVoid );
tU32  u32ChgPriorityThreadWithWrongVal( tVoid );
tU32  u32ReadTCBThreadNonExisting( tVoid );
tU32  u32ReadTCBThreadWithNegID( tVoid );
tU32  u32ReadTCBThreadDifferentStatus( tVoid );
tU32  u32MaxThreadCreate( tVoid );
tU32  u32ThreadWhoAmI( tVoid );
tU32  u32ThreadListWithListNULL( tVoid );
tU32  u32ThreadListWithListSizeNULL( tVoid );
tU32  u32ThreadListWithValidParam( tVoid );
tU32  u32AttainVariousThreadState( tVoid );
tU32  u32SpawnMultiProcesses( tVoid );
tU32  u32SpawnPerformanceProcess( tVoid );
tU32  u32ProcessSpawnInvalidParam( tVoid );
tU32  u32ProcessWhoAmI( tVoid );
tU32  u32ProcessSelfDelete( tVoid );
tU32  u32ProcessListValid( tVoid );
tU32  u32ProcessListInvalidList( tVoid );
tU32  u32ProcessListInvalidSize( tVoid );
tU32  u32GetPCBValidParam( tVoid );
tU32  u32QueryPCBInvalidID( tVoid );
tU32  u32QueryPCBInvalidAddress( tVoid );

tU32  u32SpawnThreadInfiniteLoop( tVoid );

#endif
