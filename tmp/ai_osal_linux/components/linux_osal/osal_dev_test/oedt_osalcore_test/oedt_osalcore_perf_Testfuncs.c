/**********************************************************FileHeaderBegin******
*
* FILE:        oedt_osalcore_perf_Testfuncs.c
*
* CREATED:     2009-08-27
*
* AUTHOR:      TMS Ramscheid 
*
* DESCRIPTION: functions to test performance of osalcore functions
*
* NOTES: -
*
* COPYRIGHT:  (c) 2009 Robert Bosch Car Multimedia GmbH
*
**********************************************************FileHeaderEnd*******/

/* TENGINE Header */
#include <extension/device.h>
#include <extension/tkcall.h>
#include <extension/clk.h>
#include "syscall.h"


/* OSAL Header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "TEngine_osal.h"


extern tS32 OSAL_s32MutexCreate(tCString coszName,
                             OSAL_tMtxHandle * phMutex,
                             tU32 u32Opt);
extern tS32 OSAL_s32MutexLock (OSAL_tMtxHandle hMutex, OSAL_tMSecond msec);
extern tS32 OSAL_s32MutexUnLock (OSAL_tMtxHandle hMutex);
extern tS32 OSAL_s32MutexDelete (tCString coszName);
extern tS32 OSAL_s32MutexClose (OSAL_tMtxHandle hMutex);
extern tBool bGetPerfCounter(tU64* pu64Counter,tU32* pu32Option);

#include "oedt_osalcore_perf_Testfuncs.h"

/************************************************************************ 
| variable definition (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable definition (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototype (scope: module-local) 
|-----------------------------------------------------------------------*/

#define OSAL_M_INSERT_T32(buf, Long) \
    (buf)[0] = (tU8) (((Long) >> 24) & 0xFF); \
    (buf)[1] = (tU8) (((Long) >> 16) & 0xFF); \
    (buf)[2] = (tU8) (((Long) >>  8) & 0xFF); \
    (buf)[3] = (tU8) (((Long)      ) & 0xFF);


#ifndef TSIM_OSAL

void clear_instruction_cache(void)
{
  unsigned int tmp = 0;
    __asm

    {
        MCR p15, 0, tmp, c7, c5, 0  // ;clear instruction cache
        MCR p15, 0, tmp, c8, c5, 0  // ;clear itlb
    }
}

#endif



/*****************************************************************************
 *
 * FUNCTION:    u32OsalCoreMeasureRuntime
 *
 * DESCRIPTION: 
 *              
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: none
 *
 * NOTES:       
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 27.08.09  |   create                               | Ram2Hi
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/

static tU32 u32OsalCoreMeasureRuntime( tVoid )
{
#ifndef TSIM_OSAL

#define MEASUREMENTS 8
#define SAMPLES      2000
#define NAME_LEN     30

    char cBuffer[1+NAME_LEN+12]; /* type (1) + string + 3*data (4) */
    UINT i,j;
    tS32 tempRes, res=0;
    tU32 u32Option =0;

    const char asName[MEASUREMENTS][NAME_LEN+1] = 
    { 
     /* 012345678901234567890123456789 */
       "PRC: td_get_otm overhead",        /* 1 */
       "PRC: tkse_get_otm",               /* 2 */
       "PRC: OSAL_ClockGetElapsedTime",   /* 3 */
       "PRC: SDI tkse_rea_dev",           /* 4 */
       "PRC: tkse_wai_sem/sig_sem",       /* 5 */
       "PRC: OSAL_SemWai+OSAL_SemPost",   /* 6 */ 
       "PRC: tkse_lco_mtx/unl_mtx",       /* 7 */
       "PRC: OSAL_MtxLock+OSAL_MtxUnLk",  /* 8 */ 
    };

    tU64 aiCounter[MEASUREMENTS *4];  /* 4 time values per measurement */
    UINT aiValid[MEASUREMENTS];       
    unsigned long alTimeDiff[MEASUREMENTS * 2]; /* 2 time diffs per measurement (with / without overhead) */

    /* variables to measure tkse_get_otm and OSAL_ClockGetElapsedTime*/
    SYSTIM rTim;
    OSAL_tMSecond osalTim;

    /* variables to measure "SDI read" */
    ID dd;

    /* variables to measure "OSAL_Sem" */
    T_CSEM rSem;
    tCString coszSemName = "TK_SEM";
    ID sd;
    OSAL_tSemHandle m_hSemaphore;
    tChar m_szSemName[]="OSAL_SEM";

    /* variables to measure "OSAL_Mtx" */
    OSAL_tMtxHandle m_hMutex0;
    tChar m_szMtxName0[]="OSAL_MT0";
    T_CMTX rMtx0;
    tCString coszMtxName0 = "TK_MT0";
    ID md0;

    for (i=0; i<MEASUREMENTS; ++i)
    {
      aiValid[i]=0;
      alTimeDiff[2*i]=0;
      alTimeDiff[2*i+1]=0;
    }


    /* td_get_otm is used to measure time before and after measured function call */
    /* TODO: exchange by performance counter */

    /* prepare measurement, "SDI read" */
    dd=tkse_opn_dev((UB*)"spmdrv",TD_READ);

    /* prepare measurement, "OSAL_Sem" */
    tempRes=OSAL_s32SemaphoreCreate(m_szSemName, &m_hSemaphore, 1);
    if (tempRes != OSAL_OK)
    {
      res=1; /* test fails */
    }


    rSem.sematr  = TA_TPRI | TA_DSNAME;
    rSem.exinf   = 0;
    rSem.isemcnt = 1;
    rSem.maxsem  = 65535;
    memcpy(&rSem.dsname[0],coszSemName,8);
    sd=tkse_cre_sem(&rSem);


    /* prepare measurement, "OSAL_Mx0" */
    tempRes=OSAL_s32MutexCreate(m_szMtxName0, &m_hMutex0, 0);
    if (tempRes != OSAL_OK)
    {
      res=1; /* test fails */
    }
    rMtx0.mtxatr  = TA_TPRI | TA_DSNAME;
    rMtx0.exinf   = 0;
    memcpy(&rMtx0.dsname[0],coszMtxName0,8);
    md0=tkse_cre_mtx(&rMtx0);


    for (i=0; i<SAMPLES; ++i)
    {
      /* 1) measure bGetPerfCounter overhead
            for DRAM and instruction cache measurment */

      (void) bGetPerfCounter (&aiCounter[0], &u32Option);
      clear_instruction_cache();
      (void) bGetPerfCounter (&aiCounter[1], &u32Option);
      (void) bGetPerfCounter (&aiCounter[2], &u32Option);
      (void) bGetPerfCounter (&aiCounter[3], &u32Option);


      /* 2) call tkse_get_otm via driver framework from DRAM and IC */

      (void) bGetPerfCounter (&aiCounter[4], &u32Option);
      clear_instruction_cache();
      (tVoid) tkse_get_otm(&rTim);
      (void) bGetPerfCounter (&aiCounter[5], &u32Option);

      (void) bGetPerfCounter (&aiCounter[6], &u32Option);
      (tVoid) tkse_get_otm(&rTim);
      (void) bGetPerfCounter (&aiCounter[7], &u32Option);


      /* 3) call OSAL_ClockGetElapsedTime from DRAM and IC */

      (void) bGetPerfCounter (&aiCounter[8], &u32Option);
      clear_instruction_cache();
      osalTim=OSAL_ClockGetElapsedTime();
      (void) bGetPerfCounter (&aiCounter[9], &u32Option);

      (void) bGetPerfCounter (&aiCounter[10], &u32Option);
      osalTim=OSAL_ClockGetElapsedTime();
      (void) bGetPerfCounter (&aiCounter[11], &u32Option);

      if (osalTim == 0)
      {
        res=2; /* test fails */
      }

      /* 4) call tkse_rea_dev via driver framework from DRAM and IC */

      (void) bGetPerfCounter (&aiCounter[12], &u32Option);
      clear_instruction_cache();
      tkse_rea_dev(dd,0,NULL,0,0);
      (void) bGetPerfCounter (&aiCounter[13], &u32Option);

      (void) bGetPerfCounter (&aiCounter[14], &u32Option);
      tkse_rea_dev(dd,0,NULL,0,0);
      (void) bGetPerfCounter (&aiCounter[15], &u32Option);

      /* 5) call tkse_wai_sem / tkse_sig_sem from DRAM and IC */

      (void) bGetPerfCounter (&aiCounter[16], &u32Option);
      clear_instruction_cache();
      tkse_wai_sem(sd, 1, TMO_FEVR);
      tkse_sig_sem(sd, 1);
      (void) bGetPerfCounter (&aiCounter[17], &u32Option);

      (void) bGetPerfCounter (&aiCounter[18], &u32Option);
      tkse_wai_sem(sd, 1, TMO_FEVR);
      tkse_sig_sem(sd, 1);
      (void) bGetPerfCounter (&aiCounter[19], &u32Option);


      /* 6) call semaphore via OSAL */

      (void) bGetPerfCounter (&aiCounter[20], &u32Option);
      clear_instruction_cache();
      OSAL_s32SemaphoreWait(m_hSemaphore, OSAL_C_U32_INFINITE);
      OSAL_s32SemaphorePost(m_hSemaphore);
      (void) bGetPerfCounter (&aiCounter[21], &u32Option);

      (void) bGetPerfCounter (&aiCounter[22], &u32Option);
      OSAL_s32SemaphoreWait(m_hSemaphore, OSAL_C_U32_INFINITE);
      OSAL_s32SemaphorePost(m_hSemaphore);
      (void) bGetPerfCounter (&aiCounter[23], &u32Option);


      /* 7) call mutex via tkse */

      (void) bGetPerfCounter (&aiCounter[24], &u32Option);
      clear_instruction_cache();
      tkse_loc_mtx(md0, TMO_FEVR);
      tkse_unl_mtx(md0);
      (void) bGetPerfCounter (&aiCounter[25], &u32Option);

      (void) bGetPerfCounter (&aiCounter[26], &u32Option);
      tkse_loc_mtx(md0, TMO_FEVR);
      tkse_unl_mtx(md0);
      (void) bGetPerfCounter (&aiCounter[27], &u32Option);


      /* 8) call mutex via OSAL */

      (void) bGetPerfCounter (&aiCounter[28], &u32Option);
      clear_instruction_cache();
      OSAL_s32MutexLock(m_hMutex0, OSAL_C_U32_INFINITE);
      OSAL_s32MutexUnLock(m_hMutex0);
      (void) bGetPerfCounter (&aiCounter[29], &u32Option);

      (void) bGetPerfCounter (&aiCounter[30], &u32Option);
      OSAL_s32MutexLock(m_hMutex0, OSAL_C_U32_INFINITE);
      OSAL_s32MutexUnLock(m_hMutex0);
      (void) bGetPerfCounter (&aiCounter[31], &u32Option);


        /* evaluate time elapsed during indirect and direct calls */
        for (j=0; j< MEASUREMENTS; ++j)
        {
          if ((aiCounter[4*j+1] > aiCounter[4*j]) && (aiCounter[4*j+3] > aiCounter[4*j+2]))
          {
            aiValid[j]++;
            alTimeDiff[2*j]   += aiCounter[4*j+1] - aiCounter[4*j];
            alTimeDiff[2*j+1] += aiCounter[4*j+3] - aiCounter[4*j+2];
          }
        }
    }

    /* cleanup */
    tkse_cls_dev(dd,0);
    OSAL_s32SemaphoreClose(m_hSemaphore);
    OSAL_s32SemaphoreDelete(m_szSemName);
    tkse_del_sem(sd);
    OSAL_s32MutexClose(m_hMutex0);
    OSAL_s32MutexDelete(m_szMtxName0);

    /* build average */
    for (j=0; j<MEASUREMENTS;++j)
    {
      alTimeDiff[2*j] = alTimeDiff[2*j]/aiValid[j];
      alTimeDiff[2*j+1] = alTimeDiff[2*j+1]/aiValid[j];
    }

    /* print results, cleartext is in system.trc */
    cBuffer[0] = 0x10;

    for (i=0; i<MEASUREMENTS; ++i)
    {
      strncpy(cBuffer+1, asName[i], NAME_LEN);   
      OSAL_M_INSERT_T32( cBuffer+NAME_LEN+1, alTimeDiff[2*i]); /* average: OSAL call from RAM */
      OSAL_M_INSERT_T32( cBuffer+NAME_LEN+5, alTimeDiff[2*i+1]); /* average: OSAL call from instruction cache */
      OSAL_M_INSERT_T32( cBuffer+NAME_LEN+9, (alTimeDiff[2*i] - alTimeDiff[2*i+1])); /* average: overhead of RAM access */
      LLD_vTrace((int)TR_COMP_SYSTEM, (int)TR_LEVEL_FATAL, cBuffer, sizeof(cBuffer));
    }

    /* done */
    cBuffer[0] = 0xfe;
    LLD_vTrace((int)TR_COMP_SYSTEM, (int)TR_LEVEL_FATAL, cBuffer, 1);

    return(res); /* 0 == test passed */

#else /* TSIM_OSAL */
  return(0);
#endif
}


/************************************************************************ 
|function prototype (scope: global) 
|-----------------------------------------------------------------------*/


/*****************************************************************************
 *
 * FUNCTION:    u32OsalCoreRuntimeReport
 *
 * DESCRIPTION: 
 *              
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: none
 *
 * NOTES:       
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 27.08.09  |   create                               | Ram2Hi
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/

tU32 u32OsalCoreRuntimeReport( tVoid )
{
tU32 res;

  /* execute measurement on core 0 */
  res = u32OsalCoreMeasureRuntime();

  /* test how runtime is affected ba parallel memory access */
#ifdef FOR_FUTURE_USE

  /* start parallel task on core 1 */
  startMemoryAccess(1);

  /* execute measurement on core 0 */
  res = u32OsalCoreMeasureRuntime();

  /* start parallel task on core 2 */
  startMemoryAccess(2);

  /* execute measurement on core 0 */
  res = u32OsalCoreMeasureRuntime();

  /* stop parallel tasks */
  stopMemoryAccess(2);
  stopMemoryAccess(1);

#endif

  return res;
}


/* End of File oedt_osalcore_perf_Testfuncs.c                                */

