/******************************************************************************
 *FILE         : oedt_osalcore_MQ_common_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the MESSAGE QUEUE OSAL API.
 *               
 *AUTHOR       : Anoop chandran(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *                         Initial version
 *****************************************************************************/
/*****************************************************************
| includes: 
|   1)system- and project- includes
|   2)needed interfaces from external components
|...3)internal and external interfaces from this component
|----------------------------------------------------------------*/


#include "osdevice.h"
#include "osal_if.h"

#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>


#ifndef OEDT_OSAL_CORE_MQ_COMMON_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_MQ_COMMON_TESTFUNCS_HEADER

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define MAX_MESG                    1
#define MAX_MESG3                    3
#define MAX_LEN                     20
#define MAX_EXC_LEN                 24
#define MESSAGEQUEUE_NAME 			"Message Queue"
#define MQ_MESSAGE_EXC_LEN			"-------|-------|-------|-------|"
#ifdef OSAL_SHM_MQ
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|---"
#else
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|-"
#endif
#define MQ_MESSAGE 					"----|----|----|----|"
#define MQ_MSG						"Message Queue Data"
#define MQ_PRIO0 					0
#define MQ_PRIO1                    1
#define MQ_PRIO2 					2
#define MQ_PRIO3                    3
#define MQ_PRIO4 					4
#define MQ_PRIO5                    5
#define MQ_PRIO6 					6
#define MQ_PRIO7 					7
#define INVAL_PRIO                  20 /*>>7*/
#define MQ_BUFF_SIZE				21
#define MQ_THR_NAME_1 				"MQ_Thread_1"
#define MQ_THR_NAME_2 				"MQ_Thread_2"
#define MQ_THR_NAME_NOTIFY_1 		"MQ_Thread_Notify1"
#define MQ_THR_NAME_NOTIFY_2 		"MQ_Thread_Notify2"
#define MQ_THR_NAME_NOTIFY_3 		"MQ_Thread_Notify3"
#define MQ_EVE_NAME1				"MQ_Event1"
#define MQ_EVE_NAME2				"MQ_Event2"
#define MQ_EVE1						1
#define MQ_EVE2						2
#define MQ_EVE3						4
#define MQ_EVE						7
#define MQ_TR_STACK_SIZE			2048
#define MQ_COUNT_MAX 				5
#define MQ_HARRAY 					20
#define MQ_MESSAGE_TO_POST          "In a message queue"
#define MESSAGE_20BYTES             "ABCDEFGHIJKLMNOPQRS"
#undef  MAX_LENGTH
#define MAX_LENGTH                  256
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                    0x7FFFFFFF
#undef  SRAND_MAX
#define SRAND_MAX                   (tU32)65535
#define MAX_MESSAGE_QUEUES          20
#define MAX_NO_MESSAGES             20
#undef  FIVE_SECONDS
#define FIVE_SECONDS                5000

#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS       10
#undef  NO_OF_WAIT_THREADS
#define NO_OF_WAIT_THREADS       15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS      1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS     2
#undef  TOT_THREADS
#define TOT_THREADS              (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)
/*rav8kor - error code for MQ wait*/
#define MQ_WAIT_RETURN_ERROR	  0
/*rav8kor - disable*/
#undef  DEBUG_MODE
#define DEBUG_MODE              0
#define RUN_TEST                5
#define VALID_STACK_SIZE        2048


/*****************************************************************
| variables (scope: module-global)
|----------------------------------------------------------------*/
extern OSAL_tEventHandle    MQEveHandle1;
extern OSAL_tEventHandle    MQEveHandle2;
extern OSAL_tEventHandle    MQEveHandle2_cb;
extern OSAL_tEventHandle    MQEveHandle3_cb;


/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/

/*Data Types*/
typedef struct MessageQueueTableEntry_
{
	tChar coszName[MAX_LENGTH];
	tU32  u32MaxMessages;
	tU32  u32MaxLength;
	OSAL_tenAccess enAccess;
	OSAL_tMQueueHandle phMQ;
}MessageQueueTableEntry; 

tVoid vMQCallBack2(void *Arg);
tVoid vMQCallBack3(void *Arg);

tVoid u32MQThread1(tPVoid pvArg);
tVoid u32MQThread2(tPVoid pvArg);
tVoid u32MQThreadNotify1(tPVoid pvArg);
tVoid u32MQThreadNotify2(tPVoid pvArg);
tVoid u32MQThreadNotify3(tPVoid pvArg);
void u32MQPostThread(void);
tVoid Post_Message(tVoid *ptr);
tVoid Wait_Message(tVoid *ptr);
tVoid Close_Message( tVoid *ptr );
tVoid Create_Message( tVoid *ptr );
void Thread_MQ_Memory(tVoid* pvArg);
tVoid vEntryFunction(void (*ThreadEntry) (void *), const tC8 *threadName, tU8 u8NoOfThreads, OSAL_trThreadAttribute tr_Attr[], OSAL_tThreadID tr_ID[]);


#endif


