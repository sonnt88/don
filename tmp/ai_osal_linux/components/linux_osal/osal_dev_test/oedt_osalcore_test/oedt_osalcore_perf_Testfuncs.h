/**********************************************************FileHeaderBegin******
*
* FILE:        oedt_osalcore_perf_Testfuncs.h
*
* CREATED:     2009-07-15
*
* AUTHOR:      TMS Ramscheid 
*
* DESCRIPTION: oedt test functions for osal_core runtime analysis
*
* NOTES: -
*
* COPYRIGHT:  (c) 2009 Robert Bosch Car Multimedia GmbH
*
**********************************************************FileHeaderEnd*******/


#if !defined (OEDT_OSALCORE_PERF_TESTFUNCS_HEADER)
  #define OEDT_OSALCORE_PERF_TESTFUNCS_HEADER
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 
#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototype (scope: global) 
|-----------------------------------------------------------------------*/

tU32 u32OsalCoreRuntimeReport( tVoid );

#ifdef __cplusplus
}
#endif


#else
  #error oedt_osalcore_perf_Testfuncs.h included several times
#endif 

/* End of File oedt_osalcore_perf_Testfuncs.h                                                    */


