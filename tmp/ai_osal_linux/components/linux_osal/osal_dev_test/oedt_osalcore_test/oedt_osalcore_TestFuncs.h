/******************************************************************************
 *FILE         : oedt_osalcore_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file contains common defines
 *
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *              Ver 1.6  -03-12-2012
 *              Included Test Case .h file for OSAL Signal Handler 
 *              SWM2KOR 
 				ver1.5   - 30-04-2009
                lint warning and info removal
				rav8kor

 				ver1.3   - 22-04-2009
                lint warning and info removal
				sak9kor

                Version 1.5, 27 - 02 - 2008
 *				Added Prototype for IPC testcases
 *				Added PROBLEM define
 *
 *
 *				Version 1.4 , 6 - 02 - 2008
 *				Added Prototype for IPC testcases
 *				- Tinoy Mathews( RBIN/ECM1 )
 * 
 *				Version 1.3, 2- 02- 2008
 *				Added defines EXCEPTION,HANG 
 *				and UNDOCUMENTED
 *				- Tinoy Mathews( RBIN/ECM1 )
 *
 *
 *				Version 1.2 , 27- 12- 2007
 *				Added the Test function Header for OSAL MUTEX
 *              API test Cases
 *				- Tinoy Mathews( RBIN/ECM1 )
 *
 *				Version 1.1 , 12- 12- 2007	
 *				Added Stress Test Semaphore,Stress Test Event
 *              Stress Test Message Queue defines 
 *				(Case Inclusion and Timeout)
 *				- Tinoy Mathews( RBIN/ECM1 )
 *
 *				Version 1.0 , 26- 10- 2007
 *					
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_COMMON_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_COMMON_TESTFUNCS_HEADER

/*Includes*/
#include "oedt_osalcore_PR_TestFuncs.h"
#include "oedt_osalcore_SM_TestFuncs.h"
#include "oedt_osalcore_EV_TestFuncs.h"
#include "oedt_osalcore_TM_TestFuncs.h"
#include "oedt_osalcore_SH_TestFuncs.h"
#include "oedt_osalcore_MSG_TestFuncs.h"
#include "oedt_osalcore_MQ_TestFuncs.h"
#include "oedt_osalcore_MTX_TestFuncs.h"
#include "oedt_osalcore_IPCom_TestFuncs.h"
//#include "oedt_osalcore_SH_NoIOSC_TestFuncs.h"
//#include "oedt_osalcore_MQ_NoIOSC_TestFuncs.h"
#include "oedt_osalcore_SIG_TestFuncs.h"  // Signal Handler File

/*Defines*/
/*STRESS_SEMAPHORE/STRESS_EVENT/STRESS_MQ - Define to decide whether or not to run the 
Stress semaphore test case/Stress event test case/Stress message queue test case.
If set to 1, the test case will run a max upto STRESS_SEMAPHORE_DURATION/STRESS_EVENT_DURATION/ 
STRESS_MQ_DURATION time units(ms), after which, the expected  behaviour of the case is that 
it should time out,in case system crashes in between, the STRESS_SEMAPHORE_DURATION/
STRESS_EVENT_DURATION/STRESS_MQ_DURATION time units(ms) frame, it is a failure of the 
Semaphore/Event/Message Queue OSAL API on a relative time frame. - tny1kor*/
#define STRESS_SEMAPHORE 0
#define STRESS_SEMAPHORE_DURATION 1800

#define STRESS_EVENT 0
#define STRESS_EVENT_DURATION 1800

#define STRESS_MQ 0
#define STRESS_MQ_DURATION 1800

#undef CPU_EXCEPTION
/*This define encapsulates those cases that generate CPU exception
 - tny1kor*/
#define CPU_EXCEPTION 0
/*This define encapsulates those cases that causes a straight run of 
subsequent cases to hang, which may be because the test case is still
in developmental stages or the test case goes ahead to test some
destructive scenario - tny1kor*/
#define HANG 0
/*These defines encapsulates those functions that are implemented in OSAL
but are not availiable in OSAL reference manuals,by default,these cases
will run - tny1kor*/
#define UNDOCUMENTED 1
/*This defines mask some test cases which are causing the system
left in inconsistent state as of now.If these cases behave inconsistently
now,protocol is measures will be taken to flex the test code to minimize
the system inconsistencies, such as minimizing the number of resources
that are left unfreed*/
#define PROBLEM 0

/*Helper Functions Declarations*/



#endif
