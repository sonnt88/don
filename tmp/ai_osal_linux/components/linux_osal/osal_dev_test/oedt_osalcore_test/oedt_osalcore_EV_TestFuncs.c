/******************************************************************************
 *FILE         : oedt_osalcore_EV_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the EVENT OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 
   *				ver1.5	 - 14-04-2009
                warnings removal
				rav8kor
 *					
 *				 Version 1.4 , 29- 10- 2008
 *		  		updated case
 *				TU_OEDT_OSAL_CORE_EVT_028
 *				 Anoop Chandran( RBIN/ECM1 )
 *				 Version 1.3 
 *				 Added new testcase TU_OEDT_OSAL_CORE_EVT_030 
 * 			 added trace for Stress Test by  
 *				 Anoop Chandran (RBEI\ECM1)
 *				 Version 1.3, 15- 10- 2008
 * 
 *				 Version 1.2 
 *				 Event clear update by  
 *				 Anoop Chandran (RBEI\ECM1)
 *				 Version 1.2, 7- 01- 2008 
 *
 *				 Added cases:
 *				 TU_OEDT_OSAL_CORE_EVT_029,030,031,032,033,034
 *				 Tinoy Mathews( RBIN/ECM1 )
 *
 *				 Version 1.1 , 13- 12- 2007
 *				 Linted the code,added an Event
 *               Stress Test Case,updated case
 *				 010.
 *				 Tinoy Mathews( RBIN/ECM1 )

 *               version 1.2, 22-04-2009
                 Removed lint info,
                 Sainath Kalpuri(RBEI/ECF1) 
 *
 *
*
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_helper_funcs.h"
#include "oedt_osalcore_EV_TestFuncs.h"

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1
#define OEDT_EVENT_DWC "OEDT_EVENT_DELETE_WITHOUT_CLOSE"



/***************************************************************************************
|Global Data 
|--------------------------------------------------------------------------------------*/
OSAL_tEventHandle  gevHandle        = 0;
OSAL_tEventHandle  gevHandle_       = 0;
OSAL_tEventMask    gevMask          = 0;
OSAL_tIODescriptor gRFDescriptor    = 0; /*RF - Ramdisk File*/

EventTableEntry    event_StressList[MAX_EVENTS];
OSAL_trThreadAttribute evepost_ThreadAttr[NO_OF_POST_THREADS];
OSAL_trThreadAttribute evewait_ThreadAttr[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute eveclose_ThreadAttr[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute evecreate_ThreadAttr[NO_OF_CREATE_THREADS];
OSAL_tThreadID evepost_ThreadID[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID evewait_ThreadID[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID eveclose_ThreadID[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID evecreate_ThreadID[NO_OF_CREATE_THREADS]           = {OSAL_NULL};
OSAL_tSemHandle baseSemHan                                        = 0;
tU32 u32GlobEveCounter                                            = 0;


/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/
/*****************************************************************************
* FUNCTION    :	   WaitPostThread
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Thread to test the Wait and Post event mechanisms
* HISTORY     :	   Created by Tinoy Mathews(RBIN/ECM1)  Jan 7 , 2008
*******************************************************************************/
tVoid WaitPostThread( const tVoid *pArg ) 
{
	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif
	
	/*Thread Action*/
	/*Wait for a while, to stress OSAL_s32EventWait*/
	OSAL_s32ThreadWait( ONE_SECOND );

	if( OSAL_ERROR == OSAL_s32EventPost(   *( OSAL_tEventHandle* )pArg ,
										   	  THREAD_POST_PATTERN,
										      OSAL_EN_EVENTMASK_OR ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	
	}

	/*Wait for the main thread to terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}	

/*****************************************************************************
* FUNCTION    :	   ThreadWaitPostTest
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Thread to test the Wait and Post event mechanisms
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tVoid ThreadWaitPostTest( tVoid *pArg )
{
	/*Thread Action*/
	/*Wait for a while, to stress OSAL_s32EventWait*/
	OSAL_s32ThreadWait( TWO_SECONDS );

	/*Call the helper function to print a message on the Trace*/
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Control in the Thread" );

	/*Thread Action*/

	if( OSAL_ERROR == OSAL_s32EventPost(   gevHandle ,
										   THREAD_POST_PATTERN,
										   OSAL_EN_EVENTMASK_OR ) )
	{
		*( (tU32 *)pArg ) += 500;
	}
	
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_One
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Creates file in Ramdisk, supervises the thread which does the 
                   file write operation and the file close, delete operations
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tVoid Thread_One( tVoid *pArg )
{
      
	/*Create a file in Ramdisk*/
	if( OSAL_ERROR == ( gRFDescriptor = OSAL_IOCreate( OEDT_FFS1_EVENTTEXT,OSAL_EN_READWRITE ) ) )
	{
		*( (tU32 *)pArg ) += 350;
	}
	else
	{
		 /*Wait for two seconds to confirm all the threads are waiting*/
		 OSAL_s32ThreadWait( TWO_SECONDS );

		 /*Post an event pattern to trigger Thread 2*/
		 if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,THREAD_TWO_EVENT,OSAL_EN_EVENTMASK_OR ) )
		 {
			 *( (tU32 *)pArg ) += 450;
		 }
		 
		 /*Wait for a response event from Thread 2*/
		 if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
			                				  THREAD_ONE_EVENT,
											  OSAL_EN_EVENTMASK_AND,
											  OSAL_C_TIMEOUT_FOREVER,
							                  &gevMask ) )
		{
			 *( (tU32 *)pArg ) += 1450;
		}
	/*Clear the event*/

		OSAL_s32EventPost
		( 
		gevHandle_,
		~(gevMask),
      OSAL_EN_EVENTMASK_AND
       );

		 /*Post an event pattern to trigger Thread 3*/
		 if( OSAL_ERROR ==OSAL_s32EventPost( gevHandle_,
		                    				 THREAD_THREE_EVENT,
		                    				 OSAL_EN_EVENTMASK_OR ) )
		 {
			 *( (tU32 *)pArg ) += 2450;
		 }

	}

	/*Wait for a response event from Main Thread*/
	if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
			           					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,
					   					 &gevMask ) )
	{
		*( (tU32 *)pArg ) += 4567;
	}

	/*Clear the event*/
		OSAL_s32EventPost
		( 
		gevHandle_,
		~(gevMask),
      OSAL_EN_EVENTMASK_AND
       );

	/*Post the init pattern*/
	OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR );

	/*Wait and terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Two
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Does the file write operation,communicates with the thread
                   which creates the file with the help of event mechanism
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tVoid Thread_Two( tVoid *pArg )
{
	/*Declarations*/
	tChar t2Buffer[10];
	tU8 u8Count = 10;
	/*Initialize the buffer*/
	OSAL_pvMemorySet( t2Buffer,0,10 );
	(tVoid)OSAL_szStringCopy( t2Buffer,"ThreadThr" );

	/*Wait for a signal from Thread 1*/
	if( OSAL_ERROR ==OSAL_s32EventWait( gevHandle_,
	                   					THREAD_TWO_EVENT,
					   					OSAL_EN_EVENTMASK_OR,
					   					OSAL_C_TIMEOUT_FOREVER,
					   					&gevMask ) )
	{
		*( (tU32 *)pArg ) += 550;
	}

	/*Write into Ramdisk file*/
	while( u8Count-- )
	{
		OSAL_s32IOWrite( gRFDescriptor,(tPCS8)t2Buffer,9 );
	}

	/*Post an event pattern to trigger Thread 1*/
	if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,THREAD_ONE_EVENT,OSAL_EN_EVENTMASK_OR ) )
	{
		*( (tU32 *)pArg ) += 1550;
	}

	/*Wait for a response event from Main Thread*/
	if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
			           					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,
					   					 &gevMask ) )
	{
		*( (tU32 *)pArg ) += 2550;
	}
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	gevHandle_,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Post the init pattern*/
	OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR );

	/*Wait and terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Three
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Does the file close and delete operations,communicates with
                   the main thread with the help of event mechanism
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
* 						Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tVoid Thread_Three( tVoid *pArg )
{
	/*Wait for a signal from Thread 1*/
	if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
	                   					 THREAD_THREE_EVENT,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,
					                     &gevMask ) )
	{
		*( (tU32 *)pArg ) += 300;
	}

	/*Close the file handle*/
	if( OSAL_ERROR == OSAL_s32IOClose( gRFDescriptor ) )
	{
		*( (tU32 *)pArg ) += 345;
	}
	else
	{
		/*Remove the file from Ramdisk*/
		if( OSAL_ERROR == OSAL_s32IORemove( OEDT_FFS1_EVENTTEXT ) )
		{
			*( (tU32 *)pArg ) += 478;
		}
	}

	/*Post an event pattern to trigger MainThread*/
	if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,
	                   					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT,
	                   					 OSAL_EN_EVENTMASK_OR ) )
	{
		*( (tU32 *)pArg ) += 3250;
	}

	/*Wait for a response event from Main Thread*/
	if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
			           					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,
					   					 &gevMask ) )
	{
		*( (tU32 *)pArg ) += 4256;
	}
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	gevHandle_,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Post the init pattern*/
	OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR );

	/*Wait and terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Del
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Waits for a pattern
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid Thread_Del(const tVoid *pvArg )
{
	/*Wait for Pattern*/
	OSAL_s32EventWait( *( OSAL_tEventHandle* )pvArg,
						THREAD_IN_USE,OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER,
						OSAL_NULL );

	/*Thread Wait*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   XOR_Thread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with XOR Flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid XOR_Thread(const tVoid * ptr )
{
	int n;
	tU32 u32MaskPattern = 0x00000001;

	#if DEBUG_MODE
	tU32 u32ECode       = OSAL_E_NOERROR; 
	#endif

	/*Loop Post 32 Times, 32 bit patterns*/
	for (n = 0; n < 32; n++)
	{
		/*Post the pattern to the event Field*/
		if( OSAL_ERROR == OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
							  				    u32MaskPattern,
							  					OSAL_EN_EVENTMASK_XOR ) )
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}	
		else
		{
			/*Update the Mask Pattern*/
			u32MaskPattern = u32MaskPattern<<1;
		}
			
	}

	/*Wait for Termination from the Spawning Process*/
	OSAL_s32ThreadWait( TWO_SECONDS );

}

/*****************************************************************************
* FUNCTION    :	   Replace_Thread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with Replace Flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid Replace_Thread(const tVoid * ptr )
{
	/*Definitions*/
	#if DEBUG_MODE
	tU32 u32ECode       = OSAL_E_NOERROR; 
	#endif
	
	if( OSAL_ERROR == OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
						  					REPLACE_PATTERN,
						  					OSAL_EN_EVENTMASK_REPLACE )	)
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	
	}

	/*Wait for Termination from the Spawning Process*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   ORWaitThread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with OR flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid ORWaitThread(const tVoid * ptr )
{
	
	tU32 u32MaskPattern = 0x00000001;
	#if DEBUG_MODE
	tU32 u32ECode       = OSAL_E_NOERROR; 
	#endif
	

	/*Post 1*/
	if( OSAL_ERROR != OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
						  					u32MaskPattern,
						  					OSAL_EN_EVENTMASK_OR )	)
	{
		/*Make 2*/
		u32MaskPattern  = u32MaskPattern<<1;
		/*Post 2*/
		if( OSAL_ERROR != OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
						  						u32MaskPattern,
						  						OSAL_EN_EVENTMASK_OR )	)		
		{
			/*Make 4*/
			u32MaskPattern  = u32MaskPattern<<1;
			/*Post 4*/			
			if( OSAL_ERROR == OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
						  							u32MaskPattern,
						  							OSAL_EN_EVENTMASK_OR )	)	
			{
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif	
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif		
		}	
	}
	else
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	
	}

	/*Post on Semaphore to signal main task*/
	OSAL_s32SemaphorePost( baseSemHan );

	/*Wait for Termination from the Spawning Process*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   NoBlockingThread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts the pattern EVENT_MASK_PATTERN to the event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid NoBlockingThread(const tVoid * ptr )
{
	/*Definitions*/
	#if DEBUG_MODE
	tU32 u32ECode    = OSAL_E_NOERROR;
	#endif

	/*Post the pattern EVENT_MASK_PATTERN*/
	if( OSAL_ERROR == OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,
						  					EVENT_MASK_PATTERN,
						  					OSAL_EN_EVENTMASK_OR ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}	
	
	/*Wait for some time*/
	OSAL_s32ThreadWait( THREE_SECONDS );
}



/*****************************************************************
| Test Function Definitions
|----------------------------------------------------------------*/

/*****************************************************************************
* FUNCTION    :	   u32CreateEventNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_001

* DESCRIPTION :  
				   1). Create Event with event name as NULL
			       2). If Event creation successful, close and delete the 
				       event from the system and report failure of the
					   event create OSAL API
				   3). If even creation fails, track the error code,
				       the API is successful only if the error code
					   OSAL_E_INVALIDVALUE is returned.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateEventNameNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventHandle evHandle    = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			OSAL_NULL, 
	                             			&evHandle
	                             		  ) )
	{
		/*Created a semaphore*/
		u32Ret = 1;

		/*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event Close failed*/
			u32Ret += 7;
		}
		else
		{
			/*Delete the event!*/
			if( OSAL_ERROR == OSAL_s32EventDelete( OSAL_NULL ) )
			{
				/*Event delete failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Success condition for the API is to return 
			OSAL_E_INVALIDVALUE*/
			u32Ret += 1000;
		}
		
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_002

* DESCRIPTION :  
				   1). Create event with handle as OSAL_NULL
				   2). If event is created ,set return error
				       and shutdown event(Close , delete)
				   3). If event creation fails,track the error code.
				       Only if the return error code is OSAL_E_INVALIDVALUE
					   can it be asserted that the API works correctly.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateEventHandleNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			OSAL_NULL 
	                                      ) )
	{
		/*Created the event*/
		u32Ret = 1;

		/*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( OSAL_NULL ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		if( OSAL_E_INVALIDVALUE !=  OSAL_u32ErrorCode( )  )
		{
			/*Success condition for the API is to return 
			OSAL_E_INVALIDVALUE*/
			u32Ret += 1000;
		}
		
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateEventNameMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_003

* DESCRIPTION :  
				   1). Create event with event name of length
				       32 characters.
				   2). If event is created,shutdown event(Close , delete).
				   3). If event creation fails,update the API failure
				       in the return error code.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateEventNameMAX( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME_MAX, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME_MAX ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateEventNameExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_004

* DESCRIPTION :  
				   1). Try to Create event with event name of length greater than
				       32 characters.
				   2). If event is created,update the return code of an API failure
					   do a shutdown on the event(Close , delete).
				   3). If event creation fails,query the error code, and check if
				       the error code is OSAL_E_NAMETOOLONG.Only if this is the
					   error code,can it be asserted that the API works correctly.
					   If error code is something else, update the return code of
					   an API failure.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateEventNameExceedsMAX( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME_EXCEEDS_MAX, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Created an event field!*/
		u32Ret = 1;

	    /*Attempt Shutdown activities! - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME_EXCEEDS_MAX ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		/*Event Creation failed - Expected behaviour*/
		if( OSAL_E_NAMETOOLONG != OSAL_u32ErrorCode( ) )
		{
			/*Returned code is not one expected*/
			u32Ret += 100;
		}
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateTwoEventsSameName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_005

* DESCRIPTION :  
				   1). Create event
				   2). If event is created,create another event with the same
				       name.
				       If the second event creation passes, report the
					   return error code, of an API failure.
					   If the second even creation fails,as the expected behaviour,
					   query the return code.If the return code is not 
					   OSAL_E_ALREADYEXISTS,report the return code of the API failure.
				   3). Wherever the event has been created, shoutdown activies
				       (close and delete the event) must be done.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateTwoEventsSameName( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		        = 0;
	OSAL_tEventHandle evHandle  = 0;
	OSAL_tEventHandle evHandle_	= 0;

	OSAL_s32EventDelete( EVENT_NAME );

	/*Create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	   	/*Try to Create an event with the same name*/
		if( OSAL_ERROR == OSAL_s32EventCreate(
		                                       EVENT_NAME,
											   &evHandle_
											 ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode( ) )
			{
				/*Error code is not as expected*/
				u32Ret += 100;
			}
		}
		else
		{
			/*Passing of event create API here is not an expected behaviour -
			  Event Create API failed!*/
			u32Ret += 200;

			/*Attempt shut down activities*/
			if( OSAL_ERROR == OSAL_s32EventClose( evHandle_ ) )
			{
				u32Ret += 50;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
				{
					u32Ret += 60;
				}
			}
		}

	   	/*Close and Delete the initially created event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 70;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 80;
			}
		}
	}
	else
	{
		/*Initial Event Creation failed*/
		u32Ret += 1000;
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateEventRegularName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_006

* DESCRIPTION :  
				   1). Create an event
				   2). If event is created,shutdown event(Close , delete).
				   3). If event creation fails,update the API failure
				       in the return error code.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateEventRegularName( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_007

* DESCRIPTION :  
				   1). Call the Delete Event API passing 
				       event name parameter OSAL_NULL
				   2). If the API fails, it is an expected
				       behaviour,query the error code,check if it
					   is OSAL_E_INVALIDVALUE,if not report the 
					   return error code of API failure.
				   3). If API passes, it is a critical error,
				       update the return error code.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelEventNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	
	if( OSAL_ERROR == OSAL_s32EventDelete( OSAL_NULL ) )
	{
		/*Failure of the API to return OSAL_E_NOERROR
		  is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 5000;
		}
	}
	else
	{
		/*Event delete API passed with OSAL_NULL parameter
		 - Wrong behaviour of the Delete Event API*/
		u32Ret += 10000;
	}
   
	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelEventNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_008

* DESCRIPTION :  
				   1). Create an event
				   2). If event is created,shutdown event(Close , delete).
				   3). If event creation fails,update the API failure
				       in the return error code.
				   4). If shutdown of the event in step 2 is completed,
				       the event is non existent within the system.
				       Do a Delete Event Call on this event's name.
				   5). Do a Delete Event Call on a name that was never
				       stored in the system.
				   6). If the Delete Event Call passes, it is Critical
				       API bug, notify the return error code.
				   7). If the Delete Event Call fails, it is an expected
				       behaviour, query the return error code.Only if it
					   is OSAL_E_DOESNOTEXIST, can it be asserted that the
					   API works correctly.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelEventNonExisting( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;

	
	/*Scenario 1*/
	/*Create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
			else
			{
			   	/*Attempt a delete on the non existing event!*/
				if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
				{
					/*Failure is an expected behaviour*/
					if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
					{
						u32Ret += 500;
					}	
				}
				else
				{
					/*Invalid behaviour of the Event Delete API*/
					u32Ret += 700;
				}	
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}
	/*Scenario 1*/


	/*Scenario 2*/
	/*Attempt a delete on the non existing event!*/
	if( OSAL_ERROR == OSAL_s32EventDelete( "UnknownEvent" ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 800;
		}
	}
	else
	{
		/*Invalid behaviour of the Event Delete API!*/
		u32Ret += 900;
	}
	/*Scenario 2*/


	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelEventNameExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_009

* DESCRIPTION :  
				   1). Call the Delete Event API passing 
				       event name length greater than 32 chars.
				   2). If the API fails, it is an expected
				       behaviour,query the error code,check if it
					   is OSAL_E_DOESNOTEXIST,if not report the 
					   return error code of API failure.
				   3). If API passes, it is a critical error,
				       update the return error code.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelEventNameExceedsMAX( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	
	if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME_EXCEEDS_MAX ) )
	{
		/*Failure of the API is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 5000;
		}
	}
	else
	{
		/*Event delete API passed!
		 - Wrong behaviour of the Delete Event API*/
		u32Ret += 10000;
	}
   
	/*Return error value*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32DelEventInUse()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_010

* DESCRIPTION :  
				   1). Create an event.
				   2). If event is created, do a delete on the event.
				   3). Event Delete should pass, if it fails, update the
				   	   failure in an error code.
				   4). Only if Event Delete passes, do a close, it is
				       expected that the Event close should delete
				       the event resources.
				   5). Do an Event Close again to verify that the
				       Event resources indeed have been removed,
				       apparently, the event close in this step 
				       should fail.    
				   5). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007

				   Updated by Tinoy Mathews( RBIN/ECM1 ), as per 
				   comments from MRK2HI Dec 11, 2007
*******************************************************************************/
tU32  u32DelEventInUse( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		         = 0;
	static OSAL_tEventHandle evHandle   = 0;
	OSAL_tThreadID tID           = 0;
	OSAL_trThreadAttribute trAtr = {OSAL_NULL};


	/*Create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
		
		/*Fill in Thread "Thread_Del_In_Use" attributes*/
	    trAtr.szName       = "Thread_Del_In_Use";
	    trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr.s32StackSize = 4096;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)Thread_Del;
		trAtr.pvArg        = &evHandle;

		/*Spawn a thread*/
		if( OSAL_ERROR != ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
		{
			/* better use a pthread_barrier for sync.. */
			OSAL_s32ThreadWait( ONE_SECOND );
			/*Try to delete the event in use*/
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Event delete will not fail while the event is
				still in use*/
				u32Ret += 1000;
			}
			else
			{
				/*Through an Event Close remove the event related 
				resources from the system - so since the event delete was
				done*/
				if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
				{
					/*Event Close should not fail*/
					u32Ret += 2000;
				}
				else
				{
					/*Attempt to Reclose the Event, to check if 
					the Event field has actually been removed from the 
					system*/
					if( OSAL_OK == OSAL_s32EventClose( evHandle ) )
					{
						/*Reclose should not pass*/
						u32Ret += 3000;
					}
				}
			}
			/*Delete the Thread*/
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
			{
				/*Thread Delete failed*/
				u32Ret += 150;
			}
		}
		else
		{
			/*Thread Spawn failed*/
			u32Ret += 197;
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/* cleanup unconditionally, ignore error */
	(void) OSAL_s32EventDelete( EVENT_NAME );

	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenEventExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_011

* DESCRIPTION :  
				   1). Create an event
				   2). If event is created,close the event,
				       followed by a reopen of event.
				   2). If event reopens, close the event and delete the
				       event from the system
				   3). If event creation fails,update the API failure
				       in the return error code.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenEventExisting( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;
	OSAL_s32EventDelete( EVENT_NAME );
	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Close the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			/*Open a created event*/
			if( OSAL_ERROR == OSAL_s32EventOpen( EVENT_NAME,&evHandle ) )
			{
				/*Open event failed*/
				u32Ret += 10;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
				{
					/*Close event failed*/
					u32Ret += 20;
				}
				else
				{
					/*Delete the event from the system*/
					if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
					{
						u32Ret += 50;
					}
				}
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenEventNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_012

* DESCRIPTION :  
				   1). Create an event.
				   2). If event is created,close the event,
				       followed by a reopen of event but with 
					   event name set to OSAL_NULL.
				       If event creation fails,update the Create
				       Event API failure in the return error code.
				   3). If event reopens,event open API has failed,
				       update the return error code.
				   4). If event reopen fails, it is an expected
				       behaviour.Check the return error code to see
					   if it is	OSAL_E_INVALIDVALUE.If it is not,
					   report the return error code of API failure.
				   5). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenEventNameNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Close the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			/*Open event passing OSAL_NULL as name parameter
			but a valid event handle*/
			if( OSAL_ERROR == OSAL_s32EventOpen( OSAL_NULL,&evHandle ) )
			{
				/*Failure is an expected behaviour*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 10;
				}
			}
			else
			{
				/*Event open with name as OSAL_NULL passed!
				  - Wrong behaviour*/
				u32Ret += 200;
			}

			/*Delete the valid event*/
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				u32Ret += 250;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 300;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_013

* DESCRIPTION :  
				   1). Create an event.
				   2). If event is created,close the event,
				       followed by a reopen of event but with 
					   event handle address set to OSAL_NULL.
				       If event creation fails,update the Create
				       Event API failure in the return error code.
				   3). If event reopens,event open API has failed,
				       update the return error code.
				   4). If event reopen fails, it is an expected
				       behaviour.Check the return error code to see
					   if it is	OSAL_E_INVALIDVALUE.If it is not,
					   report the return error code of API failure.
				   5). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenEventHandleNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;
	

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Close the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			/*Open the created event, but with a NULL handle address*/
			if( OSAL_ERROR == OSAL_s32EventOpen( EVENT_NAME,OSAL_NULL ) )
			{
				/*Failure is an expected behaviour*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( )  )
				{
					u32Ret += 10;
				}
			}
			else
			{
				/*Event open with event handle address as OSAL_NULL passed!
				  - Wrong behaviour*/
				u32Ret += 200;
				
			}

			/*Delete the valid event from the system*/
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				u32Ret += 250;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 300;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenEventNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_014

* DESCRIPTION :  
				   1). Create an event
				   2). If event is created,shutdown event(Close , delete).
				   3). If event creation fails,update the API failure
				       in the return error code.
				   4). If shutdown of the event in step 2 is completed,
				       the event is non existent within the system.
				       Do an Open Event Call on this event.
				   5). Do an Open Event Call on a name that was never
				       stored in the system.
				   6). If the Open Event Call passes, it is Critical
				       API bug, notify the return error code.
				   7). If the Open Event Call fails, it is an expected
				       behaviour, query the return error code.Only if it
					   is OSAL_E_DOESNOTEXIST, can it be asserted that the
					   API works correctly.
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenEventNonExisting( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		        = 0;
	OSAL_tEventHandle evHandle  = 0;
	OSAL_tEventHandle evHandle_ = 0;
	
	/*Scenario 1*/
	/*Create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    /*Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}

		/*Attempt an open on the non existing event!*/
		if( OSAL_ERROR == OSAL_s32EventOpen( EVENT_NAME,&evHandle ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 500;
			}
		}
		else
		{
			/*Invalid behaviour of the Event Open API*/
			u32Ret += 700;

			/*Since event opened, it must be believed that
			  the event must be closed and removed,for a 
			  shutdown!*/
			/*Close and Delete the event*/
			if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
			{
				/*Event close failed*/
				u32Ret += 800;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
				{
					/*Delete event failed*/
					u32Ret += 900;
				}
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 1000;	
	}
	/*Scenario 1*/


	/*Scenario 2*/
	/*Attempt an open on the non existing event!*/
	if( OSAL_ERROR == OSAL_s32EventOpen( "UnknownEvent",&evHandle_ ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 1100;
		}
	}
	else
	{
		/*Invalid behaviour of the Event Open API!*/
		u32Ret += 1300;

		/*Since event opened, it must be believed that
		the event must be closed and removed,for a 
		shutdown!*/
		/*Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle_ ) )
		{
			/*Event close failed*/
			u32Ret += 1500;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( "UnknownEvent" ) )
			{
				/*Delete event failed*/
				u32Ret += 1700;
			}
		}
	}
	/*Scenario 2*/


	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenEventNameExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_015

* DESCRIPTION :  
				   1). Call the Open Event API passing 
				       event name length greater than 32 chars.
				   2). If the API fails, it is an expected
				       behaviour,query the error code,check if it
					   is OSAL_E_DOESNOTEXIST,if not report the 
					   return error code of API failure.
				   3). If API passes, it is a critical error,
				       update the return error code.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenEventNameExceedsMAX( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		        = 0;
	OSAL_tEventHandle evHandle  = 0;
	
	if( OSAL_ERROR == OSAL_s32EventOpen( EVENT_NAME_EXCEEDS_MAX,&evHandle ) )
	{
		/*Failure of the API is an expected behaviour*/
		if( OSAL_E_NAMETOOLONG != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 5000;
		}
	}
	else
	{
		/*Event open API passed!
		 - Wrong behaviour of the Open Event API*/
		u32Ret += 10000;

		/*Attempt shutdown(close and delete) activities!*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			u32Ret += 300;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME_EXCEEDS_MAX ) )
			{
				u32Ret += 700;
			}
		}
	}
   
	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CloseEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_016

* DESCRIPTION :  
				   1).Call the close event API on OSAL_NULL.
				   2).If close event API fails, it is an expected behaviour,
				      query the error code to see if it is OSAL_E_INVALIDVALUE.
					  If it is not, report the returned error code as the API 
					  failure.
					  If the close event API passes, it is critical error,
					  report the returned error code.
				   3). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CloseEventHandleNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret = 0;

	/*Attempt calling close event API on OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32EventClose( OSAL_NULL ) )
	{
		/*Failure is an expected behaviour,Query the error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Close event API passes - Wrong behaviour*/
		u32Ret += 2000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CloseEventHandleInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_017

* DESCRIPTION :  
				   1).Call the close event API an invalid handle.
				   2).If close event API fails, it is an expected behaviour,
				      query the error code to see if it is OSAL_E_INVALIDVALUE.
					  If it is not, report the returned error code as the API 
					  failure.
					  If the close event API passes, it is critical error,
					  report the returned error code.
				   3). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CloseEventHandleInval( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                = 0;
	OSAL_tEventHandle evHandle = 0;

	/*Attempt calling close event API on OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Failure is an expected behaviour,Query the error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Close event API passes - Wrong behaviour*/
		u32Ret += 2000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetEventStatusHandleValid()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_018

* DESCRIPTION :  
				   1). Create an event.
				   2). If event is created,call the Get Event Status
				       API call.
					   If Get Event Status API fails,update the return
					   code of the API failure.
					   If event creation fails,update the API failure
				       in the return error code.
				   3). Shutdown event(Close , delete).
				   4). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetEventStatusHandleValid( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		       = 0;
	OSAL_tEventHandle evHandle = 0;
	OSAL_tEventMask   evMask   = MAX_32BIT;
	OSAL_tEventMask   evMask_  = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME, 
	                             			&evHandle 
	                                      ) )
	{ 
	    
		/*Get the event status of the created event field
		 - Event Status information availiable in evMask_ */
		if( OSAL_ERROR == OSAL_s32EventStatus( evHandle,evMask,&evMask_ ) )
		{
			/*Getting Event Status failed!*/
			u32Ret += 1000;
		}
		else
		{
			/*Out the current event flag pattern*/
			OEDT_HelperPrintf( TR_LEVEL_USER_1,"Event field Pattern : %u \n",evMask_ );
			
			/*Check for event field pattern*/
			if( INITIAL_EVENT_FIELD != evMask_ )
			{
				u32Ret += 1000;
			}
		}
	    
	    /*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetEventStatusHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_019

* DESCRIPTION :  
				   1). Try a Get Event Status call on OSAL_NULL parameter passed
				       for an event handle.
					   If the API fails,it is an expected behaviour,check if the
					   error code is OSAL_E_INVALIDVALUE,if not,update the 
					   return error code.
					   If the API passes,it is a critical API failure,report
					   the return error code.
				   2). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetEventStatusHandleNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret             = 0;
	OSAL_tEventMask	evMask  = MAX_32BIT;
	OSAL_tEventMask evMask_	= 0;

	/*Use the get event status call*/
	if( OSAL_ERROR == OSAL_s32EventStatus( OSAL_NULL,evMask,&evMask_ ) )
	{
		/*Behaviour expected*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Get Status passed for a NULL handle!-
		  Wrong Behaviour*/
		u32Ret += 10000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetEventStatusHandleInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_020

* DESCRIPTION :  
				   1). Try a Get Event Status call on an invalid event handle.
					   If the API fails,it is an expected behaviour,check if the
					   error code is OSAL_E_INVALIDVALUE,if not,update the 
					   return error code.
					   If the API passes,it is a critical API failure,report
					   the return error code.
				   2). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetEventStatusHandleInval( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                = 0;
	OSAL_tEventHandle evHandle = 0;
	OSAL_tEventMask	evMask     = MAX_32BIT;
	OSAL_tEventMask evMask_    = 0;

	/*Use the get event status call*/
	if( OSAL_ERROR == OSAL_s32EventStatus( evHandle,evMask,&evMask_ ) )
	{
		/*Behaviour expected*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Get Status passed for a NULL handle!-
		  Wrong Behaviour*/
		u32Ret += 10000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32PostEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_021

* DESCRIPTION :  
				   1). Try a Post on NULL instead of an event handle
					   If the API fails,it is an expected behaviour,check if the
					   error code is OSAL_E_INVALIDVALUE,if not,update the 
					   return error code.
					   If the API passes,it is a critical API failure,report
					   the return error code.
				   2). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32PostEventHandleNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                = 0;
   
	/*Use the post event call on OSAL_NULL instead of an event handle*/
	if( OSAL_ERROR == OSAL_s32EventPost( OSAL_NULL,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Behaviour expected*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Post passed for a NULL handle!-
		  Wrong Behaviour*/
		u32Ret += 10000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32PostEventHandleInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_022

* DESCRIPTION :  
				   1). Try a Post on an invalid event handle
					   If the API fails,it is an expected behaviour,check if the
					   error code is OSAL_E_INVALIDVALUE,if not,update the 
					   return error code.
					   If the API passes,it is a critical API failure,report
					   the return error code.
				   2). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32PostEventHandleInval( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                = 0;
	OSAL_tEventHandle evHandle = OSAL_C_INVALID_HANDLE;

	/*Use the post event call on an invalid event handle*/
	if( OSAL_ERROR == OSAL_s32EventPost( evHandle,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Behaviour expected*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Post passed for an invalid event handle!-
		  Wrong Behaviour*/
		u32Ret += 10000;
	}

	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitAndPostEventVal()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_023

* DESCRIPTION :  
				   1). Create an event
				   2). Spawn a thread from the main thread.
				       In the main thread, meanwhile, call wait for the
					   event pattern from the spawned off thread until it
					   posts.If wait fails, failure must be reported, if
					   post fails, failure must be reported.
				   3). If event is created in 1,shutdown 
				       event(Close , delete).
				   4). If event creation fails,update the API failure
				       in the return error code.
				   5). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitAndPostEventVal( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventMask EM            = 0;
	OSAL_tEventMask	evMask        = MAX_32BIT;
	OSAL_tEventMask evMask_       = MAX_32BIT;
	OSAL_tThreadID trID           = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			EVENT_NAME_GLOBAL, 
	                             			&gevHandle 
	                                      ) )
	{ 
		/*Fill in the thread Attributes*/
		trAtr.szName       = "EventWaitPostThread";
		trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr.s32StackSize = 4096;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)ThreadWaitPostTest;
		trAtr.pvArg        = &u32Ret;
		
		/*Spawn(Create + Activate) the thread*/	    
		if( OSAL_ERROR == ( trID = OSAL_ThreadSpawn( &trAtr ) ) )
		{
			u32Ret += 100;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventWait( gevHandle,
			                   					 THREAD_POST_PATTERN,
		                 	   					 OSAL_EN_EVENTMASK_AND,
		                 	   					 OSAL_C_TIMEOUT_FOREVER,
		                 	   					 &EM ) )
			{
				u32Ret += 200;
			}
			else
			{	
				/*Message from the main thread to Trace*/
				OEDT_HelperPrintf( TR_LEVEL_USER_1,"Control in the main Thread" );

				/*Remove the thread from the system*/
				if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
				{
					u32Ret += 300;
				}
			}

			/*Query the status of the event field pattern*/
			OSAL_s32EventStatus( gevHandle,evMask,&evMask_ );
		}


	    /*Shutdown activities - Close and Delete the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( gevHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME_GLOBAL ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}
	else
	{
		/*Event Creation failed*/
		u32Ret += 200;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitEventHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_024

* DESCRIPTION :  
				  
				   1). In the main thread, call wait(use OSAL_NULL
				       instead of the event handle) 
				       If wait fails, it is an expected behaviour,query
					   if the error code is OSAL_E_INVALIDVALUE,if not, the API
					   has failed , failure must be reported.

				   2). Return the error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitEventHandleNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventMask EM            = 0;

   	if( OSAL_ERROR == OSAL_s32EventWait( OSAL_NULL,
			                   			 THREAD_POST_PATTERN,
		                 	   			 OSAL_EN_EVENTMASK_AND,
		                 	   			 OSAL_C_TIMEOUT_FOREVER,
		                 	   			 &EM ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 268;
		}
	}
	else
	{	
		/*Event Wait passes for a handle set as OSAL_NULL!*/
		u32Ret += 345;	
	}
		
	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitEventHandleInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_025

* DESCRIPTION :  
   				  1). In the main thread, call wait(use an invalid 
   				      event handle) 
				      If wait fails, it is an expected behaviour,query
					  if the error code is OSAL_E_INVALIDVALUE,if not, the API
					  has failed , failure must be reported.

				   2).Return the error code.

			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitEventHandleInval( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventMask EM            = 0;		
	OSAL_tEventHandle evHandle    = 0;
   
	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,
			                   			 THREAD_POST_PATTERN,
		                 	   			 OSAL_EN_EVENTMASK_AND,
		                 	   			 OSAL_C_TIMEOUT_FOREVER,
		                 	   			 &EM ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 268;
		}
	}
	else
	{	
		/*Event Wait passes for an invalid handle!*/
		u32Ret += 345;	
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitEventWithoutTimeout()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_026

* DESCRIPTION :  
   				  1). In the main thread, call wait event API on valid event handle,
				      but, which will exhaust waiting without getting a post.
					  So the wait is set with a time out parameter 
					  OSAL_C_TIMEOUT_NOBLOCKING
   				  	  If wait fails, it is an expected behaviour,query
					  if the error code is OSAL_E_TIMEOUT,if not, the API
					  has failed , failure must be reported.

				   2).Return the error code.

			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitEventWithoutTimeout( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventMask EM            = 0;		
	OSAL_tEventHandle evHandle    = 0;
   
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		u32Ret += 100;
	}
	else
	{										
		/*Wait for 0 ms*/
		if( OSAL_ERROR == OSAL_s32EventWait( evHandle,
				                   			 THREAD_POST_PATTERN,
			                 	   			 OSAL_EN_EVENTMASK_AND,
		    	             	   			 OSAL_C_TIMEOUT_NOBLOCKING,
		        	         	   			 &EM ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 268;
			}
		}
		else
		{	
			/*Event Wait passes without timing out!*/
			u32Ret += 345;	
		}

		/*Shutdown the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitEventWithTimeout()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_027

* DESCRIPTION :  
   				  1). In the main thread, call wait event API on valid event handle,
				      but, which will exhaust waiting without getting a post.
					  So the wait is set with a time out parameter 
					  TWENTY_MILLISECONDS( defined as 20 )
   				  	  If wait fails, it is an expected behaviour,query
					  if the error code is OSAL_E_TIMEOUT,if not, the API
					  has failed , failure must be reported.

				   2).Return the error code.

			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitEventWithTimeout( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventMask EM            = 0;		
	OSAL_tEventHandle evHandle    = 0;
	OSAL_tMSecond   u32StartTime  = 0;
    OSAL_tMSecond   u32EndTime  = 0;
   
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		u32Ret += 100;
	}
	else
	{										
		/*Measure current time*/
		u32StartTime = OSAL_ClockGetElapsedTime( );
		
		/*Set Timeout for 20 ms*/
		if( OSAL_ERROR == OSAL_s32EventWait( evHandle,
				                   			 THREAD_POST_PATTERN,
			                 	   			 OSAL_EN_EVENTMASK_AND,
		    	             	   			 TWENTY_MILLISECONDS,
		        	         	   			 &EM ) )
		{
			/*Measure current time after a timout*/
			u32EndTime = OSAL_ClockGetElapsedTime( );

			/*Failure is an expected behaviour*/
			if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 268;
			}
			else
			{
				if( u32EndTime > u32StartTime )
				{
					if( TWENTY_MILLISECONDS > ( u32EndTime - u32StartTime ) )
					{
						/*Critical error in timeout!*/
						u32Ret += 456;
					}
				}
			}
		}
		else
		{	
			/*Event Wait passes without timing out!*/
			u32Ret += 345;	
		}

		/*Shutdown the event*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SyncEventMultipleThreads()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_028

* DESCRIPTION :  
				   Strategy:
				   Objective of this case is to synchronise 3 threads using one 
				   event field.A file in Ramdisk is the abstraction 
				   on which individual threads would be impacting upon.Using one 
				   event field, the following sequence will be attempted, 
				   Thread 1 : Creates the file first.
				   Thread 2 : Writes into the file.
				   Thread 3 : Closes and Removes the file.
				   As it is apparent, write cannot happen without create of file.
				   Close and remove must be after the write.
				   The Post from Thread 3 should release the wait from the main
				   Thread( or main task )
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32SyncEventMultipleThreads( tVoid )
{
	/*Declarations*/ 
	tU32 u32Ret                    = 0;
	OSAL_tThreadID tID_1           = 0;
	OSAL_tThreadID tID_2           = 0;
	OSAL_tThreadID tID_3           = 0;
   	OSAL_trThreadAttribute trAtr_1 = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_2 = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_3 = {OSAL_NULL};

	/*Global parameter initializations*/
	gevMask       = 0;
	gRFDescriptor = 0;		
	
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&gevHandle_ ) )
	{
		u32Ret += 100;
	}
	else
	{
	    /*Fill in thread 1 attributes*/
	    trAtr_1.szName       = "Thread_one";
	    trAtr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr_1.s32StackSize = 4096;
		trAtr_1.pfEntry      = (OSAL_tpfThreadEntry)Thread_One;
		trAtr_1.pvArg        = &u32Ret;

		/*Fill in thread 2 attributes*/
	    trAtr_2.szName       = "Thread_two";
	    trAtr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr_2.s32StackSize = 4096;
		trAtr_2.pfEntry      = (OSAL_tpfThreadEntry)Thread_Two;
		trAtr_2.pvArg        = &u32Ret;

		/*Fill in thread 3 attributes*/
	    trAtr_3.szName       = "Thread_three";
	    trAtr_3.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr_3.s32StackSize = 4096;
		trAtr_3.pfEntry      = (OSAL_tpfThreadEntry)Thread_Three;
		trAtr_3.pvArg        = &u32Ret;

		/*Spawn Thread 1*/
		if( OSAL_ERROR == ( tID_1 = OSAL_ThreadSpawn( &trAtr_1 ) ) )
		{
			u32Ret += 1000;
		}
		else
		{
			/*Spawn Thread 2*/
			if( OSAL_ERROR == ( tID_2 = OSAL_ThreadSpawn( &trAtr_2 ) ) )
			{
				u32Ret += 2000;
			}
			else
			{
				/*Spawn Thread 3*/
				if( OSAL_ERROR == ( tID_3 = OSAL_ThreadSpawn( &trAtr_3 ) ) )
				{
					u32Ret += 4000;
				}
				else
				{
					/*Wait for message from Thread 3*/
					if( OSAL_ERROR == OSAL_s32EventWait( gevHandle_,
			                		   					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT,
							           					 OSAL_EN_EVENTMASK_AND,
							           					 OSAL_C_TIMEOUT_FOREVER,
							           					 &gevMask ) )
					{
						u32Ret += 5000;
					}
					/*Clear the event*/
					
					OSAL_s32EventPost
					( 
					gevHandle_,
					~(gevMask),
		         OSAL_EN_EVENTMASK_AND
		          );
					/*Wait for a second*/
					OSAL_s32ThreadWait( ONE_SECOND );
					
					/*Post the Thread terminate event pattern,signal to all threads
					  to wait for termination from the main thread*/
					if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,
		                               					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
		                               					 OSAL_EN_EVENTMASK_OR ) )
					{
						u32Ret += 6000;
					}

					/*Wait on INIT EVENT PATTERN*/
					OSAL_s32EventWait( gevHandle_,
					                   INITIAL_EVENT_FIELD,
									   OSAL_EN_EVENTMASK_AND,
							           OSAL_C_TIMEOUT_FOREVER,
									   &gevMask );
					/*Clear the event*/

					OSAL_s32EventPost
					( 
					gevHandle_,
					~(gevMask),
		         OSAL_EN_EVENTMASK_AND
		          );
					/*Post the Thread terminate event pattern,signal to all threads
					  to wait for termination from the main thread*/
					if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,
		                               					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
		                               					 OSAL_EN_EVENTMASK_OR ) )
					{
						u32Ret += 6000;
					}

					OSAL_s32EventWait( gevHandle_,
					                   INITIAL_EVENT_FIELD,
									   OSAL_EN_EVENTMASK_AND,
							           OSAL_C_TIMEOUT_FOREVER,
									   &gevMask ); 
					/*Clear the event*/
					OSAL_s32EventPost
					( 
					gevHandle_,
					~(gevMask),
		         OSAL_EN_EVENTMASK_AND
		          );

					/*Post the Thread terminate event pattern,signal to all threads
					  to wait for termination from the main thread*/
					if( OSAL_ERROR == OSAL_s32EventPost( gevHandle_,
		                               					 THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
		                               					 OSAL_EN_EVENTMASK_OR ) )
					{
						u32Ret += 6000;
					}

					OSAL_s32EventWait( gevHandle_,
					                   INITIAL_EVENT_FIELD,
									   OSAL_EN_EVENTMASK_AND,
							           OSAL_C_TIMEOUT_FOREVER,
									   &gevMask );
					/*Clear the event*/
					OSAL_s32EventPost
					( 
					gevHandle_,
					~(gevMask),
		         OSAL_EN_EVENTMASK_AND
		          );
					
					/*Delete Thread 1*/
					if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_3 ) )
					{
						u32Ret += 6500;
					}
				 }
			   /*Delete thread 2*/
			   if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_2 ) )
			   {
					u32Ret += 7500;
			   }
			}
		  /*Delete Thread 3*/
		  if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_1 ) )
		  {
		  		u32Ret += 8500;
		  }
		}

		/*Shutdown the event - close and delete*/
		if( OSAL_ERROR == OSAL_s32EventClose( gevHandle_ ) )
		{
			/*Event close failed*/
			u32Ret += 7;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Delete event failed*/
				u32Ret += 10;
			}
		}
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32XORFlagPostEvent
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_029
* DESCRIPTION :    Waits for a Pattern of 0xFFFFFFFF posted using the XOR Flag
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tU32 u32XORFlagPostEvent( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 				  = 0;

	#if DEBUG_MODE
	tU32 u32ECode                 = OSAL_E_NOERROR;
	#endif

	OSAL_tEventHandle evHandle    = 0;
	OSAL_tEventMask   evMask      = MAX_32BIT;
	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
	gevMask = 0;
	/*Create an Event Field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	}
	
	/*Fill in the Thread Fields*/
	trAtr.szName       = "XOR_Thread";
	trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr.s32StackSize = 4096;
	trAtr.pfEntry      = (OSAL_tpfThreadEntry)XOR_Thread;
	trAtr.pvArg        = &evHandle;	 
	
	/*Create the Thread*/
	if( OSAL_ERROR == ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Return immediately*/
				return 15000;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Return immediately*/
			return 20000;
		}

		/*Return immediately*/
		return 25000;
	}

	/*Wait until all 32 patterns are obtained*/
	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,evMask,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,
					   					 &gevMask ) )
	{
		/*Event Wait failed*/
		u32Ret   = 1000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Delete the Thread*/
	if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
	{
		/*Thread Delete failed*/
		u32Ret  += 2000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Shut Down Event*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Event close failed*/
		u32Ret += 5000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			/*Event Delete failed*/
			u32Ret += 7000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReplaceFlagPostEvent
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_030
* DESCRIPTION :    Posts a pattern to replace the earlier event pattern
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tU32 u32ReplaceFlagPostEvent( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 				  = 0;

	#if DEBUG_MODE
	tU32 u32ECode                 = OSAL_E_NOERROR;
	#endif

	OSAL_tEventHandle evHandle    = 0;
	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
	gevMask = 0;
   
	/*Create an Event Field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	}
	
	/*Fill in the Thread Fields*/
	trAtr.szName       = "Replace_Thread";
	trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr.s32StackSize = 4096;
	trAtr.pfEntry      = (OSAL_tpfThreadEntry)Replace_Thread;
	trAtr.pvArg        = &evHandle;	 
	
	/*Create the Thread*/
	if( OSAL_ERROR == ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
	{
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Return immediately*/
				return 15000;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Return immediately*/
			return 20000;
		}

		/*Return immediately*/
		return 25000;
	}

	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,REPLACE_PATTERN,OSAL_EN_EVENTMASK_AND,
					   					 OSAL_C_TIMEOUT_FOREVER,&gevMask ) )
	{
		/*Event Wait failed*/
		u32Ret += 1800;
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}	
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );


	/*Delete the Thread*/
	if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
	{
		/*Thread Delete failed*/
		u32Ret  += 2000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Shut Down Event*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Event close failed*/
		u32Ret += 5000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			/*Event Delete failed*/
			u32Ret += 7000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32ORFlagWaitEvent
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_031
* DESCRIPTION :    Testing for the Wait Event OSAL API using the OR Wait 
				   flag.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 7,2008
*******************************************************************************/
tU32 u32ORFlagWaitEvent( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 				  = 0;
	OSAL_tEventHandle evHandle    = 0;
	OSAL_tEventMask   evMask      = 0x00000001;
	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
	tU32 u32eveMask               = MAX_32BIT;
	tU32 u32eveMask_			  = MAX_32BIT;

	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif
	gevMask = 0;
	/*Create a Global Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "GLOBAL_SEM",&baseSemHan,0 ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 500;
	}
	
	/*Create an Event*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
		
		/*Close and Delete the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( baseSemHan ) )
		{
			/*Semaphore Close failed*/
			u32Ret += 30000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "GLOBAL_SEM" ) )
			{
				/*Semaphore Delete failed*/
				u32Ret += 40000;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}	

		/*Return Immediately*/
		return 10000;
	}

	/*Fill in the Thread Fields*/
	trAtr.szName       = "ORWaitTest";
	trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr.s32StackSize = 4096;
	trAtr.pfEntry      = (OSAL_tpfThreadEntry)ORWaitThread;
	trAtr.pvArg        = &evHandle;

	/*Create the Thread*/
	if( OSAL_ERROR == ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
	{
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Return immediately*/
				return 15000;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Return immediately*/
			return 20000;
		}

		/*Close and Delete the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( baseSemHan ) )
		{
			/*Semaphore Close failed*/
			u32Ret += 30000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "GLOBAL_SEM" ) )
			{
				/*Semaphore Delete failed*/
				u32Ret += 40000;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}

		/*Return immediately*/
		return 25000;
	}

	/*Wait on Semaphore until posted by the thread*/
	OSAL_s32SemaphoreWait( baseSemHan,OSAL_C_TIMEOUT_FOREVER ); 

	/*Wait with OR flag option is obtained*/
	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,evMask,
					   					 OSAL_EN_EVENTMASK_OR,
					   					 OSAL_C_TIMEOUT_FOREVER,
					   					 &gevMask ) )
	{
		/*Wait failed*/
		u32Ret   = 1000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Delete the Thread*/
	if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
	{
		/*Thread Delete failed*/
		u32Ret  += 2000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Check the event flag status*/
	if( OSAL_ERROR != OSAL_s32EventStatus( evHandle,u32eveMask,&u32eveMask_ ) )
	{
		if( 0x00000006 != u32eveMask_ )
		{
			/*Not the expected Event field pattern*/
			u32Ret += 3000;
		}
	}
	else
	{
		/*Event Status failed*/
		u32Ret += 4000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Shut Down Event*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Event close failed*/
		u32Ret += 5000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			/*Event Delete failed*/
			u32Ret += 7000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Close and Delete the Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreClose( baseSemHan ) )
	{
		/*Semaphore Close failed*/
		u32Ret += 8000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "GLOBAL_SEM" ) )
		{
			/*Semaphore Delete failed*/
			u32Ret += 9000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32NonBlockingWaitEvent
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_032
* DESCRIPTION :    In the OSAL API for Event wait, use the
				   OSAL_C_TIMEOUT_NOBLOCKING flag.
				   Test following scenarios:
				   1).Post a pattern and Wait for that pattern
					  with OSAL_C_TIMEOUT_NOBLOCKING flag set.
					  - Wait should pass
				   2).Wait for a pattern that has not been posted
					  with OSAL_C_TIMEOUT_NOBLOCKING flag set.
					  - Wait should return OSAL_E_TIMEOUT
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 7,2008
*******************************************************************************/
tU32 u32NoBlockingEventWait( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 				  = 0;
	tU32 u32eveMask               = MAX_32BIT;
	tU32 u32eveMask_              = MAX_32BIT;
	tU32 u32TFlag                 = 0;
	OSAL_tEventHandle evHandle    = 0;
	tU32 u32EC                    = OSAL_E_NOERROR;

	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif

	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
	gevMask = 0;
	/*Create an Event in the system*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	

		/*Return Immediately*/
		return 10000;
	}

	/*Scenario 1*/
	/*Fill in thread attributes*/
	trAtr.szName       = "NoBlockingWaitTest";
	trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr.s32StackSize = 4096;
	trAtr.pfEntry      = (OSAL_tpfThreadEntry)NoBlockingThread;
	trAtr.pvArg        = &evHandle;

	/*Spawn thread*/
	if( OSAL_ERROR == ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
	{
		/*Set the error code*/
		u32Ret += 700;
	}
	else
	{
		/*Set the Flag to show thread was created and activated*/
		u32TFlag = 1;
		
		/*Wait until the pattern in the event field is
		EVENT_MASK_PATTERN.This is because as the subsequent Wait call
		has the OSAL_C_TIMEOUT_NOBLOCKING, it will not wait for any pattern*/
		do
		{
			/*Wait*/
			OSAL_s32ThreadWait( 30 );
			/*Get Status*/
			OSAL_s32EventStatus( evHandle,u32eveMask,&u32eveMask_ );

		}while( EVENT_MASK_PATTERN != u32eveMask_ );
		
		if( OSAL_ERROR == OSAL_s32EventWait( evHandle,EVENT_MASK_PATTERN,
						   					 OSAL_EN_EVENTMASK_AND,
						   					 OSAL_C_TIMEOUT_NOBLOCKING,
						   					 &gevMask ) )
		{
			/*Update the error code*/
			u32Ret += 1000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}	
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

   	/*Scenario 1*/

	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,PATTERN_2048,
						   				 OSAL_EN_EVENTMASK_AND,
						   				 OSAL_C_TIMEOUT_NOBLOCKING,
						   				 &gevMask ) )
	{
		/*Failure is an expected behaviour*/	
		/*Query the error code*/
		u32EC = OSAL_u32ErrorCode( );
		if( OSAL_E_TIMEOUT != u32EC )
		{
			/*Wrong error code*/
			u32Ret += 3000;
		} 
	}
	else
	{
		/*Wait passes!*/
		u32Ret += 4000;
	}
	/*Scenario 2*/
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Delete the thread*/
	if( u32TFlag )
	{
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
		{
			/*Update error code*/
			u32Ret += 4500;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}
			
	/*Shut Down Event*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Event close failed*/
		u32Ret += 5000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			/*Event Delete failed*/
			u32Ret += 7000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32BlockingDurationEventWait
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_033
* DESCRIPTION :    Test For the wait duration for a Blocking Wait for the
				   OSAL Event Wait API.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 7,2008
*******************************************************************************/
tU32 u32BlockingDurationEventWait( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tEventHandle evHandle = 0;
	OSAL_tMSecond u32StartTime = 0;
	OSAL_tMSecond u32EndTime   = 0;
	OSAL_tMSecond u32Duration  = 0;

	#if DEBUG_MODE
	tU32 u32ECode              = OSAL_E_NOERROR;
	#endif
	gevMask = 0;
	/*Create an Event field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
	   	#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif		

		/*Return Immediately*/
		return 10000;
	}

	/*Mark the Start Time*/
	u32StartTime = OSAL_ClockGetElapsedTime( );

	/*Do a Idle Wait for an Event Pattern*/
	if( OSAL_ERROR == OSAL_s32EventWait( evHandle,PATTERN_2048,
					   					 OSAL_EN_EVENTMASK_AND,
					   					 ONE_SECOND,
					   					 &gevMask ) )
	{
		/*Mark the End Time*/
		u32EndTime = OSAL_ClockGetElapsedTime( );

		/*Update Error Code*/
		if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
		else
		{
			/*Calculate Duration*/
			if( u32EndTime > u32StartTime )
			{
				u32Duration = u32EndTime - u32StartTime;

				/*Do Duration Test only if the error code is OSAL_E_TIMEOUT
				and EndTime > StartTime*/
				if(!( (( ONE_SECOND - THIRTY_MILLISECONDS ) <= u32Duration) && (u32Duration <= ( ONE_SECOND + THIRTY_MILLISECONDS )) ) )
				{
					/*Duration out of Range*/
					u32Ret += 30000;
				}
			}
			else
			{
				/*Overflow*/
				u32Ret += 20000;
			}
		}
	}
	else
	{
		/*Event Wait cannot pass!*/
		u32Ret += 1000;
	}
		/*Clear the event*/
		OSAL_s32EventPost
		( 
		evHandle,
		~(gevMask),
	   OSAL_EN_EVENTMASK_AND
	    );


	/*Shut Down Event*/
	if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
	{
		/*Event close failed*/
		u32Ret += 5000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
		{
			/*Event Delete failed*/
			u32Ret += 7000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32WaitPostEventAfterDelete
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_034
* DESCRIPTION :    Do a Wait and Post after deleting the Event
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 7,2008
*******************************************************************************/	
tU32 u32WaitPostEventAfterDelete( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   	  = 0;
	tU32 u32DelFlag               = 0;
	OSAL_tEventHandle evHandle 	  = 0;
	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};

	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif	
	gevMask = 0;
	/*Create an Event within the system*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&evHandle ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif	

		/*Return Immediately*/
		return 10000;
	}

	/*Delete the Event before close*/
	if( OSAL_ERROR != OSAL_s32EventDelete( EVENT_NAME ) )
	{
		/*Fill in the Thread Fields*/
		trAtr.szName       = "OperateAfterDel";
		trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr.s32StackSize = 4096;
		trAtr.pfEntry      = (OSAL_tpfThreadEntry)WaitPostThread;
		trAtr.pvArg        = &evHandle;	

		/*Spawn the Thread*/
		if( OSAL_ERROR != ( tID = OSAL_ThreadSpawn( &trAtr ) ) )
		{
			if( OSAL_ERROR == OSAL_s32EventWait( evHandle,
							   				     THREAD_POST_PATTERN,
					   		   					 OSAL_EN_EVENTMASK_AND,
					   		   					 OSAL_C_TIMEOUT_FOREVER,
					   		   					 &gevMask ) )
			{
				/*Event Wait failed*/
				u32Ret += 1000;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}

			/*Delete the thread*/ 
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
			{
				/*Thread Delete failed*/
				u32Ret  += 1500;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}
		else
		{
			/*Thread Spawn failed*/
			u32Ret += 2000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}

		/*Set the Delete Flag*/
		u32DelFlag = 1;
	}
	else
	{
		/*Event Delete failed*/
		u32Ret += 4000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Restore*/
	if( u32DelFlag )
	{
		/*Close the Event and Release resources*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event Close failed*/
			u32Ret += 5000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}
	else
	{
		/*Try to close the Event first and Delete - Normal Sequence*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event Close failed*/
			u32Ret += 5000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				/*Event Delete failed*/
				u32Ret += 6000;
				
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}
			

/******************************************************************************/
/*STRESS TEST EVENT OSAL API*/
/******************************************************************************/

/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tS32 s32RandomNoGenerate( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );
	
	/*Return the Positive Random Number*/
	return s32Rand;
}	

/*****************************************************************************
* FUNCTION    :	   vChangeThreadPrioRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Change Thread Priorities Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid vChangeThreadPrioRandom( tVoid )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandomTime()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Wait for Random time frame
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid vWaitRandomTime( tU32 u32TimeModulus )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32TimeModulus +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectEventIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Indexes Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tU8 u8SelectEventIndexRandom( tVoid )
{
	/*Declarations*/
	tU8  u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX; 
	/*Calculate the Event Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_EVENTS );
	/*Return the Event Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   u32SelectEventPatternRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Patterns Randomly 
				   - For Both Post and Wait
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tU32 u32SelectEventPatternRandom( tVoid )
{
	/*Declarations*/
	tS32 s32Rand           = RAND_MAX;
	tU32 u32evPatternIndex = 0xFFFFFFFF;
	tU32 u32evPattern      = 0x00000000;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Find calculate the event Pattern Index*/
	u32evPatternIndex = ( (tU32)s32Rand%MAX_PATTERNS );

	/*Return the True Pattern*/
	switch( u32evPatternIndex )
	{
		case 0: u32evPattern = PATTERN_1;			break;
		case 1:	u32evPattern = PATTERN_2;			break;
		case 2:	u32evPattern = PATTERN_4;			break;
		case 3:	u32evPattern = PATTERN_8;			break;
		case 4:	u32evPattern = PATTERN_16;			break;
		case 5:	u32evPattern = PATTERN_32;			break;
		case 6:	u32evPattern = PATTERN_64;			break;
		case 7:	u32evPattern = PATTERN_128;			break;
		case 8:	u32evPattern = PATTERN_256;			break;
		case 9:	u32evPattern = PATTERN_512;			break;
		case 10:u32evPattern = PATTERN_1024;		break;
		case 11:u32evPattern = PATTERN_2048;		break;
		case 12:u32evPattern = PATTERN_4096;		break;
		case 13:u32evPattern = PATTERN_8192;		break;
		case 14:u32evPattern = PATTERN_16384;		break;
		case 15:u32evPattern = PATTERN_32768;		break;
		case 16:u32evPattern = PATTERN_65536;		break;
		case 17:u32evPattern = PATTERN_131072;		break;
		case 18:u32evPattern = PATTERN_262144;		break;
		case 19:u32evPattern = PATTERN_524288;		break;
		case 20:u32evPattern = PATTERN_1048576;		break;
		case 21:u32evPattern = PATTERN_2097152;		break;
		case 22:u32evPattern = PATTERN_4194304;		break;
		case 23:u32evPattern = PATTERN_8388608;		break;
		case 24:u32evPattern = PATTERN_16777216;	break;
		case 25:u32evPattern = PATTERN_33554432;	break;
		case 26:u32evPattern = PATTERN_67108864;	break;
		case 27:u32evPattern = PATTERN_134217728;	break;
		case 28:u32evPattern = PATTERN_268435456;	break;
		case 29:u32evPattern = PATTERN_536870912;	break;
		case 30:u32evPattern = PATTERN_1073741824;	break;
		case 31:u32evPattern = PATTERN_2147483648;	break;
		default: break;
	}

	/*Return the pattern*/
	return u32evPattern;
}

/*****************************************************************************
* FUNCTION    :	   Post_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Event Thread 
				   - To post an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Post_EventThread( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;

	#if DEBUG_MODE
	tU32 u32eveMask  = MAX_32BIT;
	tU32 u32eveMask_ = MAX_32BIT;
	#endif
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		#if DEBUG_MODE
		/*Get Event Status at the Randomly Selected Index - Before Open*/
		OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
		#endif

		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressList[u8EventIndex].hEventName,&event_StressList[u8EventIndex].hEve ) )
		{
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Post_EventThread:EVENT %s open Successful",
				event_StressList[u8EventIndex].hEventName
			);
			
			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Open*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
			
			/*Select the Post Pattern Randomly*/
			u32PostPattern = u32SelectEventPatternRandom( );

			/*Post Randomly selected pattern to event pattern a Random Index*/
			if( OSAL_ERROR != OSAL_s32EventPost( event_StressList[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR ))
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Successful",
					event_StressList[u8EventIndex].hEventName
				);
					
			}
		   else
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Failed",
					event_StressList[u8EventIndex].hEventName
				);
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Post*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Close Event Field at the Randomly Selected Index*/
			if( u8SelectEventIndexRandom( ) < (MAX_EVENTS/2) )
			{
		   		vWaitRandomTime( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );	
		   		vWaitRandomTime( FIVE_SECONDS );
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Close*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Wait_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Event Thread 
				   - To Wait for an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Wait_EventThread( tVoid *ptr )
{ 
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32WaitPattern = 0x00000000;

	#if DEBUG_MODE
	tU32 u32eveMask     = MAX_32BIT;
	tU32 u32eveMask_    = MAX_32BIT;
	#endif
	gevMask = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		#if DEBUG_MODE
		/*Get Event Status at the Randomly Selected Index - Before Open*/
		OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
		#endif

		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressList[u8EventIndex].hEventName,&event_StressList[u8EventIndex].hEve ) )
		{
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Wait_EventThread:EVENT %s open Successful",
				event_StressList[u8EventIndex].hEventName
			);

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Open*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		 
			/*Select the Wait Pattern Randomly*/
			u32WaitPattern = u32SelectEventPatternRandom( );
		 

			/*Wait for a Pattern*/
			if(OSAL_OK ==
				OSAL_s32EventWait( 
								   event_StressList[u8EventIndex].hEve,
								   u32WaitPattern,
								   OSAL_EN_EVENTMASK_OR,
								   OSAL_C_TIMEOUT_FOREVER,
							   	&gevMask 
							  		 )
				)
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Released",
						event_StressList[u8EventIndex].hEventName
					);
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Release failed",
						event_StressList[u8EventIndex].hEventName
					);
				}
			/*Clear the event*/
			OSAL_s32EventPost
			( 
			event_StressList[u8EventIndex].hEve,
			~(gevMask),
		   OSAL_EN_EVENTMASK_AND
							  );

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Post*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Close Event Field at the randomly selected index*/
			if( u8SelectEventIndexRandom( ) < (MAX_EVENTS/2) )
			{
		   		vWaitRandomTime( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );	
		   		vWaitRandomTime( FIVE_SECONDS );
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Close*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Close_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Delete and Close an Event field at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Close_EventThread( tVoid *ptr )
{	
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;
	tU8  u8Count 		= 0;

	#if DEBUG_MODE
	tU32 u32eveMask     = MAX_32BIT;
	tU32 u32eveMask_    = MAX_32BIT;
	#endif
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select event index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHan, OSAL_C_TIMEOUT_FOREVER );

		/*Check if the event handle is invalid*/
		if( event_StressList[u8EventIndex].hEve != OSAL_C_INVALID_HANDLE )
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );
			
			#if DEBUG_MODE
			/*Query Event Status - Before Delete*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Delete the Event*/
			if( OSAL_s32EventDelete( event_StressList[u8EventIndex].hEventName ) == OSAL_OK )
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Close_EventThread:EVENT %s Delete Successful",
					event_StressList[u8EventIndex].hEventName
				);

				#if DEBUG_MODE
				/*Query Event Status - After Delete*/
				OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
				#endif

				/*Post Randomly 200 times*/
				while( u8Count < 200 )
				{
					/*Select the Post Pattern Randomly*/
					u32PostPattern = u32SelectEventPatternRandom( );

					/*Post a Pattern to event pattern a random index*/
					if (OSAL_OK == OSAL_s32EventPost( event_StressList[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR ))
					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Successful",
							event_StressList[u8EventIndex].hEventName
						);
							
					}
				   else
					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Failed",
							event_StressList[u8EventIndex].hEventName
						);
					}


					/*Increment the counter*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Delete the event resources*/
				if ( OSAL_OK != OSAL_s32EventClose( event_StressList[u8EventIndex].hEve ))
				{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Close Failed",
							event_StressList[u8EventIndex].hEventName
						);
				
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s Close Failed",
						event_StressList[u8EventIndex].hEventName
					);
					/*Invalidate the event handle for reuse*/
					event_StressList[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s handle invalid",
						event_StressList[u8EventIndex].hEventName
					);

				}
				#if DEBUG_MODE
				/*Query Event Status - After Close*/
				OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
				#endif
			
				/*Invalidate the event handle for reuse*/
				event_StressList[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;

			}/*Event Delete Success*/
		}/*Handle Valid*/
		else
		{
		   OSAL_s32SemaphorePost( baseSemHan );
		}	
	}/*Loop Infinitely*/
}

/*****************************************************************************
* FUNCTION    :	   Create_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Create an Event field at Random Event indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Create_EventThread( tVoid *ptr )
{ 
	 /*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 for(;;)   
   	 {
        /*Randomly modify the priority of the thread*/
		vChangeThreadPrioRandom( );

		/*Select event index randomly*/
      	u8Index = u8SelectEventIndexRandom();

      	/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHan, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (event_StressList[u8Index].hEve == OSAL_C_INVALID_HANDLE)
      	{
         	/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );

         	/*Create the event*/
         	if (OSAL_OK == OSAL_s32EventCreate( event_StressList[u8Index].hEventName,&event_StressList[u8Index].hEve ))
				{
					
			 		OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Created Successful",
						event_StressList[u8Index].hEventName
					);
						
				}
			   else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Create Failed",
						event_StressList[u8Index].hEventName
					);
				}

      	}
		else
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );
		}

		/*Wait randomly*/
      	vWaitRandomTime( FIVE_SECONDS );
     }
}

/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute 
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Interface to all Threads			       	
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid vEntryFunction( 
					  		void (*ThreadEntry) (void *),
					  		const tC8 *threadName,
                      		tU8 u8NoOfThreads, 
                      		OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[]
                    		)
{
	/*Declarations*/
	tU8  u8Index                        = 0;
	tChar aBuf[TOT_THREADS][MAX_LENGTH];

	OSAL_pvMemorySet(aBuf,0,sizeof(aBuf));

	/*Spawn the threads*/
	while( u8Index < u8NoOfThreads )
	{
		/*Catch the thread name into buffer element*/
		OSAL_s32PrintFormat( aBuf[u32GlobEveCounter],"%s_%d",threadName,u8Index );

		/*Fill in the thread attributes*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;
		tr_Attr[u8Index].szName       = aBuf[u32GlobEveCounter];
		tr_Attr[u8Index].pfEntry      = ThreadEntry;

		/*Spawn the thread*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			//do nothing
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobEveCounter++;
	}
}

/*****************************************************************************
* FUNCTION    :	   u32EventStressTest()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_035

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elasped Time.
				   2). Create 30 Event Fields in the system.
				   3). Create 10 Threads which does Post operation 
				       on 30 Events Randomly.
				   4). Create 15 Threads which does Wait operation 
				       on 30 Events Randomly.
				   5). Create 1 Thread which does Delete and Close 
				       operation on 30 Events Randomly.
				   6). Create 2 Threads which does a Re- Create on 
				       30 Events Randomly,if deleted earlier.
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete all the 28 Threads.
				   9). Return success.
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Event 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Event
				   API Interface is Robust.Timeout Time can be reconfigured in the 
				   file "oedt_osalcore_TestFuncs.h" under the define 
				   STRESS_EVENT_DURATION.Whether this case should be run 
				   can be configured in the same file as part of the define
				   STRESS_EVENT.
					   			       	
* HISTORY     :	  Created By Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tU32 u32EventStressTest( tVoid )
{
	/*Definitions*/
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = "\0";


	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "BASE_SEM",&baseSemHan, 1 );

	/*Create MAX_EVENTS number of events in the system*/
	while( u8Index < MAX_EVENTS )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_Event_%d",u8Index );
		/*Get the name into Event Name field*/
		(tVoid)OSAL_szStringCopy( event_StressList[u8Index].hEventName,acBuf );
		/*Create an Event in the system*/
		if( OSAL_ERROR == OSAL_s32EventCreate( event_StressList[u8Index].hEventName,&event_StressList[u8Index].hEve ) )
		{
			
			/*Return immediately with failure*/
			return (10000+u8Index);
		}
		/*Reinitialize the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment the index*/
		u8Index++;
	}

	/*Start all Post Threads*/
	vEntryFunction( Post_EventThread,"Post_Ev_Thread",NO_OF_POST_THREADS,evepost_ThreadAttr,evepost_ThreadID );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_EventThread,"Wait_Ev_Thread",NO_OF_WAIT_THREADS,evewait_ThreadAttr,evewait_ThreadID );
	/*Start the Close Thread*/
	vEntryFunction( Close_EventThread,"Close_Ev_Thread",NO_OF_CLOSE_THREADS,eveclose_ThreadAttr,eveclose_ThreadID );
	/*Start all Create Threads*/
	vEntryFunction( Create_EventThread,"Create_Ev_Thread",NO_OF_CREATE_THREADS,evecreate_ThreadAttr,evecreate_ThreadID );

	/*Wait for OSAL_C_TIMEOUT_FOREVER*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( evepost_ThreadID[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( evewait_ThreadID[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( eveclose_ThreadID[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}
		
	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( evecreate_ThreadID[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Reset the Global Thread Counter*/
	u32GlobEveCounter = 0;

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHan );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEM" );

	/*Return error code - Success if control reaches here*/
	return 0;
	
}	
/*****************************************************************************
* FUNCTION    :	   u32EventDeletewithoutclose()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_036

* DESCRIPTION :  
				   1). Create Event with event name as OEDT_EVENT_DWC
			      
			      2). If Event creation successful, Delete and then close the 
				       event from the system 
				       
				   3). Try to create Event with event name as OEDT_EVENT_DWC
			      
			      4). If Event creation successful, close and then Delete the 
				       event from the system 
			       	
* HISTORY     :	   Created by Anoop Chandran(RBIN/ECM1)  Oct 15,2008
*******************************************************************************/

tU32 u32EventDeletewithoutclose( tVoid )
{

	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tEventHandle evHandle    = 0;

	/*Try to create an event within the system*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			OEDT_EVENT_DWC, 
	                             			&evHandle
	                             		  ) )
	{

		/*Delete the event!*/
		if( OSAL_ERROR == OSAL_s32EventDelete( OEDT_EVENT_DWC ) )
		{
			/*Event delete failed*/
			u32Ret = 5;
		}
		else
		{
			/*Close the event*/
			if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
			{
				/*Event Close failed*/
				u32Ret += 10;
			}
			
		}
	}
	else
	{
		/*Event create failed*/

			u32Ret += 100;
	}
	/*Try to create the event again with same name*/
	if( OSAL_ERROR !=  OSAL_s32EventCreate( 
	                             			OEDT_EVENT_DWC, 
	                             			&evHandle
	                             		  ) )
	{
		/*Close the event!*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			/*Event Close failed*/
			u32Ret += 50;
		}
		else
		{
			/*Delete the event*/
			if( OSAL_ERROR == OSAL_s32EventDelete( OEDT_EVENT_DWC ) )
			{																	 
				/*Event Delete failed*/
				u32Ret += 200;
			}
			
		}
	
	}
	else
	{
		/*Event create failed*/
		u32Ret += 1000;
	}
	/*Return the error code*/
	return u32Ret;
}



