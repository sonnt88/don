/******************************************************************************
 *FILE         : oedt_osalcore_TM_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the TIMER OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *              Ver 1.4 -12-12-2012
 *				      Added test case to check the timer callback limit
 *                -SWM2KOR
 *              ver1.3   - 22-04-2009
                lint info removal
				sak9kor
  
*				ver1.2	 - 14-04-2009
                warnings removal
				rav8kor
 				 
 				 Version 1.1, 25- 03- 2008
 *               Added Prototypes:
 *				 u32TimerPerformanceMultiThread
 *               u32TimerPerformanceSingleThread
 
 				 Version 1.0 , 26- 10- 2007
 *					
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_TM_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_TM_TESTFUNCS_HEADER

/*Defines*/
#define CALLBACK_WAIT_TIME        1
#define DUMMY_WAIT                10
#define INVAL_VALUE               ((tS32)-1)

#define DELAY               	  2000
#define WAIT_FOR_DELETE           4000
#define LOAD_WAIT                 1
#define EIGHT_MILLI_SECONDS       8
#define TEN_MILLI_SECONDS         10
#define TWENTY_MILLI_SECONDS      20
#define PHASE_TIME         		  300
#define CYCLE_TIME         		  900

#undef  EVENT_NAME

#define EVENT_NAME                "Event_Timer"
#define TIMER_ONE_EVENT           0x00000001
#define TIMER_TWO_EVENT           0x00000002
#undef  EXCEPTION
#define EXCEPTION                 0
#undef  MUTEX
#define MUTEX                     "Mutex"
#define TOLERANCE_TIME            25
#undef  MAX_LENGTH
#define MAX_LENGTH                256
#define CALLBACK_COUNT            150
#define LOAD                      30
#define WAIT_TIME                 3000

/*Helper Functions Declarations*/

/*Declarations*/
tU32  u32CreateTimerWithCallbackNULL( tVoid );
tU32  u32DelTimerNonExistent( tVoid );
tU32  u32DelTimerWithHandleNULL( tVoid );
tU32  u32CreateDelTimer( tVoid );
tU32  u32SetTimeTimerStTimeOrIntToNULL( tVoid );
tU32  u32SetTimeTimerNonExistent( tVoid );
tU32  u32GetTimeTimerNonExistent( tVoid );
tU32  u32GetTimeTimerWithDiffParam( tVoid );
tU32  u32SetResetTwoTimers( tVoid );
tU32  u32GetElaspedTimeFromRef( tVoid );
tU32  u32SetOsalTimerInvalStruct( tVoid );
tU32  u32GetOsalTimerWithoutPrevSet( tVoid );
tU32  u32SetOsalTimerGetOsalTimer( tVoid );
tU32  u32GetOsalTimerNULL( tVoid );
tU32  u32CyclePhaseTimeCheck( tVoid );
tU32  u32TimerPerformanceMT_Time( tVoid );
tU32  u32TimerPerformanceST_Time( tVoid );
tU32  u32TimerPerformanceST_Counter( tVoid );
tU32  u32TimerPerformanceMT_Counter( tVoid );
tU32  u32ChangeSystemTimeTest( tVoid );
tU32  U32MaxCallHandleCheck(tVoid);

#endif
