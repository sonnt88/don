/************************************************************************
 *FILE          :oedt_osalcore_SH_TestFuncs.h
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION   :This file contains function prototypes and some Macros that 
 *				 will be used in the file oedt_osalcore_SH_TestFuncs.c 
 *				 for OSAL_CORE APIs.
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       : 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *                           Initial Version.
 ************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_SH_TESTFUNCS_HEADER
#define OEDT_SH_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define SH_SIZE 100
#define SH_NAME "SH_MEM"
#define SH_INVAL_NAME "-------|-------|-------|-------|-------|"   /* name length = 40 */

#define SH_MEMSIZE 1000
#define SH_MEMSIZE_512 		512
#undef  VALID_STACK_SIZE
#define VALID_STACK_SIZE 	2048
#define SH_HARRAY 9
#define SH_MAX 9
#define SH_MIN 0
#define INVAL_HANDLE -1
#define OSAL_INVALID_ACCESS 0x0200
#define MAP_OFFSET    20
#define MAP_LENGTH    30
#define LENGTH_127          127
#define ZERO_LENGTH   0
#define ZERO_OFFSET         0
#define OFFSET_0            0
#define OFFSET_128          128
#define OFFSET_256          256
#define OFFSET_384          384
#define OFFSET_511          511
#define OFFSET_800          800
#define PATTERN_THR1        0x00000001
#define PATTERN_THR2        0x00000002
#define PATTERN_THR3        0x00000004
#define PATTERN_THR4        0x00000008
#undef  THREE_SECONDS
#define THREE_SECONDS       3000

#undef 	DEBUG_MODE

#define DEBUG_MODE          0

#define VALID_STACK_SIZE        2048
#define SH_SIZE  100

/* Function prototypes of functions used 
	in file oedt_osalcore_SH_common_TestFuncs.c */


tVoid Thread_SH_0_127( tVoid * ptr );				
tVoid Thread_SH_128_255( tVoid * ptr );
tVoid Thread_SH_256_383( tVoid * ptr );
tVoid Thread_SH_384_511( tVoid * ptr );

tU32 u32SHMemoryThreadAccess( tVoid );
void Thread_SH_Memory(tVoid* pvArg);
				
#endif
