/******************************************************************************
 *FILE         : oedt_osalcore_PR_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the THREAD OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
				ver2.1   - 17-02-2016
                oedt fix for cfg3-1738
				dku6kor
 *              
 				ver2.0   - 03-05-2013
                Process path adaptation for multiple projects
				kgr1kor
            
				ver1.9   - 30-04-2009
                lint info removal
				rav8kor

 				ver1.8   - 22-04-2009
                lint info removal
				sak9kor

 *				ver1.7	 - 14-04-2009
                warnings removal,fix problems in test cases
				rav8kor

				 Version 1.6 
				 Event clear update and update the 
				 Test case TU_OEDT_OSAL_CORE_THRD_039  by  
				 Anoop Chandran (RBEI\ECM1) 27/09/2008

 				 Version 1.5, 31- 01- 2008
				 Added case	TU_OEDT_OSAL_CORE_THRD_039
				 - Tinoy Mathews( RBIN/ECM1 )
 
 				 Version 1.4, 1- 1- 2008
				 Updated the case TU_OEDT_OSAL_CORE_THRD_023
				 ( Reduced threads to be spawned from a limit of 300
				   to a limit of 100 - Target got Reset)
				 - Tinoy Mathews( RBIN/ECM1 )
 				 
 				 Version 1.3, 13- 12- 2007
				 Linted the code
				 - Tinoy Mathews( RBIN/ECM1 )
 
 				 Version 1.2 ,4- 12- 2007
				 Added 10 new cases 029 - 038
				 (To test the Process OSAL Interface)
				 - Tinoy Mathews( RBIN/ECM1 ) 
 
 				 Version 1.1 , 24- 11- 2007
 				 Added 5 new cases 023 - 027
				 (To test ThreadWhoAmI and ThreadList OSAL APIs )
				 - Tinoy Mathews( RBIN/EDI3 )
 					
 				 Version 1.0 , 26- 10- 2007
 *				 Initial version - Tinoy Mathews( RBIN/EDI3 )
                 	
 *
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include <sys/wait.h>
#include "oedt_helper_funcs.h"
#include "oedt_osalcore_PR_TestFuncs.h"
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include "oedt_Types.h"
//#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1

#define EVENT_MAX_THREADS         0x00000001
//#define ARRAY_DIM_MAX_THREADS          100






/*****************************************************************
Global variables
*****************************************************************/
OSAL_tSemHandle u32SemHandle_Who_Am_I = 0;
OSAL_tSemHandle u32SemThreadState     = 0;
OSAL_tEventHandle hEvMaxThreads       = 0;
tU8  u8Switch                         = 0;

pthread_mutex_t MaxThreadCreateMutex = PTHREAD_MUTEX_INITIALIZER;
tS32 s32MaxThreadCreateCounter       = 0;

/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/
/*Thread Body*/
tVoid Thread(tPVoid pvArg)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   OSAL_s32ThreadWait(3000);
}

/*Thread Body*/
/*lint -e818 */
tVoid Thread_Body(tVoid* pvArg )
{
   ((void)pvArg);
	/*rav8kor - LINT info supressed as pvArg cannot be const as per the OSAL call back declaration*/
   /*rav8kor - post event when the last thread is created */

   pthread_mutex_lock(&MaxThreadCreateMutex);
   s32MaxThreadCreateCounter++;
   pthread_mutex_unlock(&MaxThreadCreateMutex);

   if (s32MaxThreadCreateCounter == (MAX_THREADS))
   {
      OSAL_s32EventPost(hEvMaxThreads, EVENT_MAX_THREADS, OSAL_EN_EVENTMASK_OR);
   }
}
/*lint +e818 */

/*Thread Body*/
tVoid ThreadWhoAmI_Body( tPVoid pvArg )
{
	

	/*Get the Thread ID*/
	*(OSAL_tThreadID *)pvArg = OSAL_ThreadWhoAmI( );

	/*Post on the Semaphore*/
	OSAL_s32SemaphorePost( u32SemHandle_Who_Am_I );

	/*Wait for Termination*/
	OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*Thread Body*/
tVoid Thread_State_Test( tPVoid pvArg )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	
	/*Try to keep processor in run state*/
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Thread_State_Test\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Tries to simulate Thread States\n" );
	
				   
  	if( u8Switch )
	{
		/*Wait for a Post*/
		OSAL_s32SemaphoreWait( u32SemThreadState,OSAL_C_TIMEOUT_FOREVER );
	}
	
	/*Wait and enter delayed state*/
	OSAL_s32ThreadWait( TEN_SECONDS );
}

/*Infinite Thread*/
tVoid Infinite_Thread( tVoid * ptr )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	for(;;)
	{
	}
}

/*To Print thread states*/
tVoid OEDT_HelperPrintfAll(const OSAL_trThreadControlBlock* tCB,tString tStrArr )
{
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "TCB : %s\n",tStrArr );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread name             : %s\n",(tString)tCB->szName );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Initial Priority : %u\n",(tU32)tCB->u32Priority );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Current Priority : %u\n",(tU32)tCB->u32CurrentPriority );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Stack Size       : %d\n",(tS32)tCB->s32StackSize );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Used Stack Size  : %d\n",(tS32)tCB->s32UsedStack );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Start Time       : %u\n",(OSAL_tMSecond)tCB->startTime );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Slice Time       : %u\n",(OSAL_tMSecond)tCB->sliceTime );
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Run Time         : %u\n",(OSAL_tMSecond)tCB->runningTime );
#ifndef TSIM_OSAL
#if 0   // COMMENT OUT all Tests to make it compilable  OSR
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread Status           : %d\n",tCB->enStatus );
#endif   // COMMENT OUT all Tests to make it compilable  OSR
#endif /*#ifndef TSIM_OSAL*/
	OEDT_HelperPrintf( TR_LEVEL_USER_1, "Error code              : %u\n",(tU32)tCB->u32ErrorCode );

	return;	
}
/*To initialize Thread Control Block*/
tVoid Initialize_TCB( OSAL_trThreadControlBlock* tCB )
{
	/*Initialize TCB*/
	tCB->szName             = OSAL_NULL;
	tCB->u32Priority        = 0;
	tCB->u32CurrentPriority = 0;
	tCB->s32StackSize       = 0;
	tCB->s32UsedStack       = 0;
	tCB->startTime          = 0;
	tCB->sliceTime          = 0;
	tCB->runningTime        = 0;
	tCB->u32ErrorCode       = 0;

	return;
}
	

/*****************************************************************************
* FUNCTION    :	   u32PRCreateThreadNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_001

* DESCRIPTION :  
				   1). Call the thread create spawn with Thread name
				       set to NULL
				   2). Return the system state to initial state
			       3). Check error status and return error code
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32PRCreateThreadNameNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;


	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)OSAL_NULL; /*Thread name set to NULL*/
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		/*Set return error code*/
		u32Ret += 1000;

		/*Try to restore system state*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Error code expected to be OSAL_E_INVALIDVALUE*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
	}
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateThreadNameTooLong()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_002

* DESCRIPTION :  
				   1). Call the thread spawn API with Thread name
				       set to string wth length exceeding MAX size.
				   2). Return the system state to initial state
			       3). Check error status and return error code
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateThreadNameTooLong( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	tS8 tStr[55]                  = "Thread name set to very long to test name length limit";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr; /*Thread name set to a very long string*/
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		/*Set return error code*/
		u32Ret += 1000;

		/*Try to restore system state*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Error code expected to be OSAL_E_NAMETOOLONG*/
		if( OSAL_E_NAMETOOLONG != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
	}
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateThreadWrongPriority()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_003

* DESCRIPTION :  
				   1). Call the thread spawn API with Thread priority
				       set to 160.
				   2). If the thread is created, set the error code
				       and restore the system to initial state(delete
					   the thread)
				   3). If the thread creation fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateThreadWrongPriority( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	tS8 tStr[10]                  = "Threadxyz";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  INVALID_PRIORITY;/*Invalid Priority*/
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		/*Created the Thread with the wrong Priority*/
		u32Ret += 1000;

		/*Try to restore system state by deleting the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Error code expected to be OSAL_E_INVALIDVALUE*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
	}
		
	/* thread is created, default priority is used*/
	if(u32Ret == 1000) u32Ret = 0;
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateThreadLowStack()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_004

* DESCRIPTION :  
				   1). Call the thread spawn API with Thread stack set 
				       to 1 byte.
				   2). If the thread is created, set the error code
				       and restore the system to initial state(delete
					   the thread)
				   3). If the thread creation fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateThreadLowStack( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	tS8 tStr[10]                  = "Threadpqr";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  INVALID_STACK_SIZE;/*Undersized stack*/
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		/*Created the Thread with undersized stack*/
		u32Ret += 1000;

		/*Try to restore system state by deleting the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Error code expected to be OSAL_E_INVALIDVALUE*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
	}

	/* thread is created, default stack size is used*/
	if(u32Ret == 1000) u32Ret = 0;
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateThreadEntryFuncNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_005

* DESCRIPTION :  
				   1). Call the thread spawn API with Thread code segment
				       set to OSAL_NULL.
				   2). If the thread is created, set the error code
				       and restore the system to initial state(delete
					   the thread)
				   3). If the thread creation fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateThreadEntryFuncNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	tS8 tStr[10]                  = "Threadabc";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  OSAL_NULL;/*Thread code segment set to OSAL_NULL*/
	trAttr.pvArg                  =  OSAL_NULL;

	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		/*Created the Thread with thread body set to OSAL_NULL*/
		u32Ret += 1000;

		/*Try to restore system state by deleting the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Error code expected to be OSAL_E_INVALIDVALUE*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
	}
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DeleteThreadNotExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_006

* DESCRIPTION :  
				   1). Call the thread delete API with Thread which has
				       already been removed from the system.
				   2). If the thread delete passes, set the error code
				   3). If the thread delete fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DeleteThreadNotExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threaddef";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to re-delete the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadDelete( trID ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check if error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to delete a thread which never was created in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadDelete( trID_ ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}	

/*****************************************************************************
* FUNCTION    :	   u32DeleteThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_007

* DESCRIPTION :  
				   1). Call the thread delete API with Thread id
				       set to a negative value.
				   2). If the thread delete passes, the error code needs
				       to be updated indicating failure of the thread
					   delete API.
				   3). If the thread delete fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DeleteThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;

	/*Call the thread delete API*/
	if( OSAL_ERROR != OSAL_s32ThreadDelete( trID ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateDeleteThreadValidParam()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_008

* DESCRIPTION :  
				   1). Create a Thread and Delete it.
			       2). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateDeleteThreadValidParam( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	tS8 tStr[10]                  = "Threadfgh";

	/*Fill in the thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;  

	/*Create a thread*/
	if( OSAL_ERROR == ( trID = OSAL_ThreadSpawn( &trAttr ) ) )
	{
		/*Thread create failed*/
		u32Ret += 1000;
	}
	else
	{
		/*Delete the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateTwoThreadSameName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_009

* DESCRIPTION :  
				   1). Create a Thread.
				   2). If thread create fails, update the error code,
				       If the thread create passes, it is the expected behaviour.
				   3). Create another thread with the same name.
				   4). If thread create fails, it is the expected behaviour.
				       If the thread create passes, update the error code,
					   and restore the system state by deleting the thread
				   5). Delete the thread that was created in step 1. 
			       6). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateTwoThreadSameName( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID1          = 0;
	OSAL_tThreadID trID2          = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	tS8 tStr[10]                  = "Threadbcd";

	/*Fill in the thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;  

	/*Create a thread*/
	if( OSAL_ERROR == ( trID1 = OSAL_ThreadSpawn( &trAttr ) ) )
	{
		/*Thread create failed*/
		u32Ret += 1000;
	}
	else
	{
	   if( OSAL_ERROR != ( trID2 = OSAL_ThreadSpawn( &trAttr ) ) )
	   {
			/*Recreate happened!*/
			u32Ret += 1000;

			/*Delete the thread to restore the system state*/
			if( OSAL_ERROR == OSAL_s32ThreadDelete( trID2 ) )
			{
				u32Ret += 500;
			}
	   }
	   else
	   {
			if( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 2000;
			} 
	   }
	   
	   /*Delete the thread*/
	   if( OSAL_ERROR == OSAL_s32ThreadDelete( trID1 ) )
	   {
	   		/*Thread delete failed*/
			u32Ret += 3000;
	   }
	}
	   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ActivateThreadNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_010

* DESCRIPTION :  
				   1). Call the thread activate API with Thread which has
				       already been removed from the system.
				   2). If the thread activate passes, set the error code
				   3). If the thread activate fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ActivateThreadNonExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threadwxy";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to Activate the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadActivate( trID ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check is error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to Activate a thread which never was created in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadActivate( trID_ ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}	

/*****************************************************************************
* FUNCTION    :	   u32ActivateThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_011

* DESCRIPTION :  
				   1). Call the thread activate API with Thread id
				       set to a negative value.
				   2). If the thread activate passes, the error code needs
				       to be updated indicating failure of the thread
					   activate API.
				   3). If the thread activate fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ActivateThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;

	/*Call the thread activate API*/
	if( OSAL_ERROR != OSAL_s32ThreadActivate( trID ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ActivateThreadTwice()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_012

* DESCRIPTION :  
				   1). Create a Thread.
				   2). Activate the thread.
				   3). Reactivate the thread.
				   4). Delete the thread.
			       5). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ActivateThreadTwice( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	tS8 tStr[10]                  = "Threadklm";

	/*Fill in the thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;  

	/*Create a thread*/
	if( OSAL_ERROR == ( trID = OSAL_ThreadCreate( &trAttr ) ) )
	{
		/*Thread create failed*/
		u32Ret += 1000;
	}
	else
	{
		/*Activate the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadActivate( trID ) )
		{
			u32Ret += 1000;
		}
		else
		{
		   	/*Reactivate the thread*/
			if( OSAL_ERROR != OSAL_s32ThreadActivate( trID ) )
			{
				u32Ret += 2000;
			}
			else
			{
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					/*Wrong error code returned*/
					u32Ret += 3000;
				}
			}
		}
		
		/*Delete the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SuspendThreadNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_013

* DESCRIPTION :  
				   1). Call the thread suspend API with Thread which has
				       already been removed from the system.
				   2). If the thread suspend passes, set the error code
				   3). If the thread suspend fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32SuspendThreadNonExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threadnop";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to Suspend the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadSuspend( trID ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check if error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to Suspend a thread which never was created in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadSuspend( trID_ ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SuspendThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_014

* DESCRIPTION :  
				   1). Call the thread suspend API with Thread id
				       set to a negative value.
				   2). If the thread suspend passes, the error code needs
				       to be updated indicating failure of the thread
					   suspend API.
				   3). If the thread suspend fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32SuspendThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;

	/*Call the thread suspend API*/
	if( OSAL_ERROR != OSAL_s32ThreadSuspend( trID ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ResumeThreadNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_015

* DESCRIPTION :  
				   1). Call the thread resume API with Thread which has
				       already been removed from the system.
				   2). If the thread resume passes, set the error code
				   3). If the thread resume fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ResumeThreadNonExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threadrgh";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to Resume the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadResume( trID ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check if error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to Resume a thread which never was created in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadResume( trID_ ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ResumeThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_016

* DESCRIPTION :  
				   1). Call the thread resume API with Thread id
				       set to a negative value.
				   2). If the thread resume passes, the error code needs
				       to be updated indicating failure of the thread
					   resume API.
				   3). If the thread resume fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ResumeThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;

	/*Call the thread resume API*/
	if( OSAL_ERROR != OSAL_s32ThreadResume( trID ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ChgPriorityThreadNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_017

* DESCRIPTION :  
				   1). Call the thread change priority API with Thread which has
				       already been removed from the system.
				   2). If the thread change priority passes, set the error code
				   3). If the thread change priority fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ChgPriorityThreadNonExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threadplm";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to change the priority of the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadPriority( trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check if error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to change the priority of the thread which never was created 
	in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadPriority( trID_,OSAL_C_U32_THREAD_PRIORITY_LOWEST ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ChgPriorityThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_018

* DESCRIPTION :  
				   1). Call the thread change priority API with Thread id
				       set to a negative value.
				   2). If the thread change priority passes, the error code needs
				       to be updated indicating failure of the thread
					   change priority API.
				   3). If the thread change priority fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
                     Modified by Deepak Kumar (RBEI/ECF5) Feb 17, 2016   fix for CFG3-1738 
*******************************************************************************/
tU32  u32ChgPriorityThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;

	/*Call the thread change priority API*/
	if( OSAL_ERROR != OSAL_s32ThreadPriority( trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )            /* fix for CFG3-1738 */
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ChgPriorityThreadWithWrongVal()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_019

* DESCRIPTION :  
				   1). Create a Thread 
				   2). Try to change the thread Priority to an invalid value
				   3). If change possible, update the error code
				       If change not possible and error returned,track the 
				       error code.If the error code is OSAL_E_INVALIDVALUE,
				       behaviour is expected,else update the error code
				   4). Delete the thread
			       2). Return the error code
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ChgPriorityThreadWithWrongVal( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	tS8 tStr[10]                  = "Threadzxc";

	/*Fill in the thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;  

	/*Create a thread*/
	if( OSAL_ERROR == ( trID = OSAL_ThreadCreate( &trAttr ) ) )
	{
		/*Thread create failed*/
		u32Ret += 1000;
	}
	else
	{
		/*Call the thread change priority API*/
		if( OSAL_ERROR != OSAL_s32ThreadPriority( trID,INVALID_PRIORITY ) )
		{
			u32Ret += 1000;
		}
		else
		{
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{	
				u32Ret += 500;
			}
			
		}
		
		/*Run the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadActivate( trID ) )
		{
			u32Ret += 1500;
		}
		
		/*Delete the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 2000;
		}
	}
	   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReadTCBThreadNonExisting()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_020

* DESCRIPTION :  
				   1). Try to get the TCB of the Thread which has
				       already been removed from the system.
				   2). If the TCB retrieve API passes, set the error code
				   3). If the TCB retrieve API fails, check if the 
				       returned error code is OSAL_E_WRONGTHREAD
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ReadTCBThreadNonExisting( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	OSAL_tThreadID trID_          = 7;
	tS8 tStr[10]                  = "Threadqaz";
	OSAL_trThreadControlBlock tCB = {OSAL_NULL};

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/* Scenario 1*/
	/*Try to create the Thread within the system*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Error status Check*/
	if( OSAL_ERROR != trID )
	{
		
		/*Remove the thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
		{
			/*Thread delete failed*/
			u32Ret += 1000;
		}
		else
		{
			/*Try to get the TCB of the non existent thread*/
			if( OSAL_ERROR != OSAL_s32ThreadControlBlock( trID,&tCB ) )
			{
				u32Ret += 3000;
			}
			else
			{
				/*Check if error code is OSAL_E_WRONGTHREAD*/
				if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 4000;
				}
			}
		}
	}
	else
	{
	   u32Ret += 2000;
	}
	/* Scenario 1*/

	/* Scenario 2*/
	/*Try to get the TCB of a thread which never was created 
	in the system*/
	if( OSAL_ERROR != OSAL_s32ThreadControlBlock( trID_,&tCB ) )
	{
		u32Ret += 5000;
	}
	else
	{
		/*Check is error code is OSAL_E_WRONGTHREAD*/
		if( OSAL_E_WRONGTHREAD != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 6000;
		}
	}
	/* Scenario 2*/
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReadTCBThreadWithNegID()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_021

* DESCRIPTION :  
				   1). Call the thread TCB retrieve API with Thread id
				       set to a negative value.
				   2). If the thread TCB retrieve API passes, the error code needs
				       to be updated indicating failure of the thread
					   TCB retrieve API.
				   3). If the thread TCB retrieve API fails, check if the 
				       returned error code is OSAL_E_INVALIDVALUE
					   Only if the API returns this, can it be 
					   asserted that the API works correctly.
			       4). Return the error code.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ReadTCBThreadWithNegID( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tThreadID trID           = -1;
	OSAL_trThreadControlBlock tCB = {OSAL_NULL};

	/*Call the thread TCB retrieve API*/
	if( OSAL_ERROR != OSAL_s32ThreadControlBlock( trID,&tCB ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{	
			u32Ret += 500;
		}
	}
   			
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReadTCBThreadDifferentStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_022

* DESCRIPTION :  
				   1). Create and run the thread.
				   2). In the main thread, query the TCB.
				   3). Suspend the thread
				   4). In the main thread, query the TCB.
				   5). Resume the thread
				   6). In the main thread, query the TCB.
				   7). Change the thread priority
				   8). In the main thread, query the TCB.
				   9). Delete the thread.
			       
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ReadTCBThreadDifferentStatus( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_trThreadAttribute trAttr = {OSAL_NULL};
	OSAL_tThreadID trID           = 0;
	tS8 tStr[10]                  = "Threadlfj";
	tChar tStr_1[]                = "Thread Created";
	tChar tStr_2[]				  = "Thread Created and Run";
	tChar tStr_3[]				  = "Thread Suspended";
	tChar tStr_4[]				  =	"Thread Resumed";
	tChar tStr_5[]				  = "Thread Priority Changed";

	OSAL_trThreadControlBlock tCB = {OSAL_NULL};

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Create the thread*/
	if( OSAL_ERROR == (trID = OSAL_ThreadCreate( &trAttr ) ) )
	{
		u32Ret = 25;
	}
	else
	{
	    /*Call the thread TCB retrieve API*/
		if( OSAL_ERROR == OSAL_s32ThreadControlBlock( trID,&tCB ) )
		{
			u32Ret = 50;
		}
		else
		{
			/*TCB retrieved while thread created*/
			OEDT_HelperPrintfAll( &tCB,tStr_1 );
			/*Initialize the TCB*/
			Initialize_TCB( &tCB );
		}
		
		/*Activate the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadActivate( trID ) )
		{
			u32Ret = 75;
		}
		else
		{
		   	/*Call the thread TCB retrieve API*/
		  	if( OSAL_ERROR == OSAL_s32ThreadControlBlock( trID,&tCB ) )
		  	{
		  		u32Ret = 100;
		  	}
		  	else
		  	{
			 	/*TCB retrieved while thread created and run*/
			 	OEDT_HelperPrintfAll( &tCB,tStr_2 );
			 	/*Initialize the TCB*/
			 	Initialize_TCB( &tCB );
		  
				/*Suspend the thread*/
				if( OSAL_ERROR == OSAL_s32ThreadSuspend( trID ) )
				{
					u32Ret += 300;
				}
				else
				{/*THREAD SUCCESSFULLY SUSPENDED*/

	   				/*Call the thread TCB retrieve API*/
					if( OSAL_ERROR == OSAL_s32ThreadControlBlock( trID,&tCB ) )
					{
						u32Ret += 400;
					}
					else
					{	
						/*TCB retrieved while thread Suspended*/
						OEDT_HelperPrintfAll( &tCB,tStr_3 );
						/*Initialize the TCB*/
			    		Initialize_TCB( &tCB );
					}
		   			/*Resume the thread*/
					if( OSAL_ERROR == OSAL_s32ThreadResume( trID ) )
					{
						u32Ret += 500;
					}
					else
					{/*THREAD SUCCESSFULLY RESUMED*/

	   					/*Call the thread TCB retrieve API*/
						if( OSAL_ERROR == OSAL_s32ThreadControlBlock( trID,&tCB ) )
						{
							u32Ret += 600;
						}
						else
						{	
							/*TCB retrieved while thread resumed*/
							OEDT_HelperPrintfAll( &tCB,tStr_4 );
							/*Initialize the TCB*/
			        		Initialize_TCB( &tCB );
						}
					}/*THREAD SUCCESSFULLY RESUMED*/	
				}/*THREAD SUCCESSFULLY SUSPENDED*/

				/*Change the priority of the thread*/
				if( OSAL_ERROR == OSAL_s32ThreadPriority( trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST ) )
				{
					u32Ret += 700;
				}
				else
				{/*THREAD PRIORITY SUCCESSFULLY CHANGED AS PER OSAL*/

					/*Call the thread TCB retrieve API*/
					if( OSAL_ERROR == OSAL_s32ThreadControlBlock( trID,&tCB ) )
					{
						u32Ret += 600;
					}
					else
					{	
						/*Check for a priority change*/
						if( OSAL_C_U32_THREAD_PRIORITY_LOWEST != tCB.u32CurrentPriority )
						{
							/*Priority of thread not changed!*/
							u32Ret += 650;
						}
						/*TCB retrieved while thread priority changed*/
						OEDT_HelperPrintfAll( &tCB,tStr_5 );
						/*Initialize the TCB*/
			    		Initialize_TCB( &tCB );
					}

				}/*THREAD PRIORITY SUCCESSFULLY CHANGED AS PER OSAL*/

		    }/*THREAD SUCCESSFULLY CREATED AND RUN*/
			
			/*Delete the thread from the system*/
			if( OSAL_ERROR == OSAL_s32ThreadDelete( trID ) )
			{
				u32Ret += 700;
			}
		}/*THREAD SUCCESSFULLY ACTIVATED*/
	}/*THREAD SUCCESSFULLY CREATED*/

	/*Return the error code*/
	return u32Ret;
		
}

/*****************************************************************************
* FUNCTION    :	   u32MaxThreadCreate()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_023

* DESCRIPTION :    1). Verify maximum number of threads that can be spawned
                       in 
                       User mode   -  Test Case runs in context of a process
					   outside the resident unit( User Context )
                       Kernel mode -  Test Case runs as part of the resident
					   unit( Kernel Context )

					   Note : As per current implemetation of osalproc.cpp,
					   a target reset will happen at the 145th Thread Spawn
					   At this point of time there are 315 threads running 
					   on the target.
					   So thread numbers have been reduced to 100 threads.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/
tU32 u32MaxThreadCreate( tVoid )
{
	/*Definitions*/

	tU32 u32Count                         = 0;
	tU32 u32Ret                           = 0;
	OSAL_trThreadAttribute trAtrM         = {OSAL_NULL};
   s32MaxThreadCreateCounter = 0;
   hEvMaxThreads = 0;
   tS8 ps8ThreadName[16];

	if( OSAL_ERROR == OSAL_s32EventCreate("MaxThEve",&hEvMaxThreads) )
	{
		u32Ret = 1;
		return u32Ret;
	}
	/*Spawn the threads*/
    while(u32Count < MAX_THREADS )
	{	 
		/*Initialize thread fields*/
		//trAtrM.szName                 =  (tString)acThreadName[u32Count];
       sprintf((tString) ps8ThreadName, "%s%03u", "test21_", u32Count);
       trAtrM.szName                 =  (tString)ps8ThreadName;
	    trAtrM.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	    trAtrM.s32StackSize           =  VALID_STACK_SIZE;
	    trAtrM.pfEntry                =  Thread_Body;
	    trAtrM.pvArg                  =  &u32Count;

		/*Create and set the thread running*/
		if( OSAL_ERROR ==  OSAL_ThreadSpawn( &trAtrM )  )
		{
			u32Ret        = 10000 + u32Count;
			/*Check if error code has been set*/
			if( OSAL_E_NOERROR ==  OSAL_u32ErrorCode ( )  )
			{
				u32Ret += 10000;
			}
			break;
		}
		/*Increment the count*/
		u32Count++;

	}

	
	/*wait till all threads are exited with time out for 5 secs*/
	if( OSAL_ERROR == OSAL_s32EventWait( hEvMaxThreads,
			                				  EVENT_MAX_THREADS,
											  OSAL_EN_EVENTMASK_AND,
											  5000,
							                  OSAL_NULL ) )
	{
		u32Ret += 10;
		
	}
	

	if( OSAL_ERROR == OSAL_s32EventClose( hEvMaxThreads ) )
	{
		/*Event Close should not fail*/
		u32Ret += 20;
	}
	else
	{

		if( OSAL_ERROR == OSAL_s32EventDelete( "MaxThEve" ) )
		{
			/*Event delete failed*/
			u32Ret += 40;
		}
	}

	OSAL_s32ThreadWait( 100 );
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ThreadWhoAmI()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_024

* DESCRIPTION :    Test OSAL Thread API call OSAL_ThreadWhoAmI 
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/
tU32  u32ThreadWhoAmI( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                                       = 0;
	OSAL_tThreadID u32MainThreadID                    = 0;
	OSAL_tThreadID tID[MAX_THREADS]                            = {0};
	OSAL_tThreadID tIDWAI[MAX_THREADS]                         = {0};
	OSAL_trThreadAttribute trAtr                      = {OSAL_NULL};
	tU32 u32Count                                     = 0;
	tU32 u32Counter                                   = 0;
	tU32 u32ReCounter                                 = 0;
	tS32 s32SemaphoreCount                            = 0;
	tS8 ps8ThreadName[16];

	/*Call OSAL_ThreadWhoAmI( ) on main thread*/
	u32MainThreadID = OSAL_ThreadWhoAmI( );
	tID[0]          = u32MainThreadID;
	tIDWAI[0]       = u32MainThreadID;

	/*Create a semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreCreate( "Semaphore_Who_Am_I",&u32SemHandle_Who_Am_I,0 ) )
	{
		/*Spawn n Threads*/
		while( u32Count<10 )
		{
		   sprintf((tString) ps8ThreadName, "%s%03u", "test22_", u32Count);
		   trAtr.szName                 =  (tString)ps8ThreadName;
		   trAtr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	    	trAtr.s32StackSize           =  VALID_STACK_SIZE;
	    	trAtr.pfEntry                =  ThreadWhoAmI_Body;
	    	trAtr.pvArg                  =  &tIDWAI[u32Count];
	    
	    	/*Spawn threads*/
	    	if( OSAL_ERROR == ( tID[u32Count] = OSAL_ThreadSpawn( &trAtr ) ) )
			{
               u32Ret += 100;
               break;
			}

			u32Count++;

		}
		
		/*Get the status of the semaphore*/
		while( ( u32Count ) != (tU32)s32SemaphoreCount )
		{
			OSAL_s32SemaphoreGetValue( u32SemHandle_Who_Am_I,&s32SemaphoreCount );
			OSAL_s32ThreadWait( 20 );
		}
		
		/*CHECK FOR OSAL_ThreadWhoAmI*/
		/*Check ID returned by Thread Spawn and ID returned from OSAL_ThreadWhoAmI*/
		while( u32Counter < u32Count )
		{
			if( tID[u32Counter]^tIDWAI[u32Counter] )
			{
				u32Ret += u32Counter;
			}
			u32Counter++;
		}

		/*Reset u32Counter*/
		u32Counter = 1;

		/*Check for ID repetition in IDs returned by OSAL_ThreadWhoAmI*/
		while( u32ReCounter < u32Count )
		{
			while( u32Counter < u32Count )
			{
				if( !( tIDWAI[u32ReCounter] ^ tIDWAI[u32Counter] ) )
				{
					u32Ret += u32Counter;
				}
				u32Counter++;
			}
			u32ReCounter++;
			u32Counter = u32ReCounter+1;
		}
		/*CHECK FOR OSAL_ThreadWhoAmI*/

		/*Reset u32Counter*/
		u32Counter = 1;

		/*Delete the threads*/
		while( u32Counter < u32Count )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID[u32Counter] ) )
			{
			//	u32Ret += u32Counter;
			}
		    u32Counter++;
		}
		
		/*Close and Delete the Semphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( u32SemHandle_Who_Am_I ) )
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "Semaphore_Who_Am_I" ) )
			{
				u32Ret += 1200;
			}
		}
		
	}
	else
	{
		u32Ret += 10000;
	}

	OSAL_s32ThreadWait( 6000 );
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ThreadListWithListNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_025

* DESCRIPTION :    Call Thread List API with OSAL_NULL as array pointer. 
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/
tU32 u32ThreadListWithListNULL( tVoid )
{
	/*Definitions*/
	tS32 s32Length            = LIST_SIZE;	
	tS32 s32ReturnParam       = -1;

	/*Issue call to Thread List API*/
	s32ReturnParam = OSAL_s32ThreadList( OSAL_NULL,s32Length );

	if( s32ReturnParam > 0 )
	{
		return 10000;
	}
		
	/*Return success*/
	return 0;
}

/*****************************************************************************
* FUNCTION    :	   u32ThreadListWithListSizeNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_026

* DESCRIPTION :    Call the Thread List API with Maximum number of
                   Threads for list query as zero. 
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/
tU32 u32ThreadListWithListSizeNULL( tVoid )
{
	/*Definitions*/
	tS32 pas32List[LIST_SIZE] = {0};
	tU32 u32Count             = 0;
	tS32 s32ReturnParam       = -1;

	/*Issue call to Thread List API*/
	s32ReturnParam = OSAL_s32ThreadList( pas32List,(tS32)0 );
	
	/*Check for returned value, must be zero*/
	if( 0 != s32ReturnParam )
	{
		return 10000;
	}
	else
	{
		/*Check if the array got updated by the call to thread list*/
		while( u32Count < LIST_SIZE )
		{
			if( pas32List[u32Count] != 0 )
			{
				return 20000;
			}
		    u32Count++;
		}
	}
	
	/*Return success*/
	return 0;
}

/*****************************************************************************
* FUNCTION    :	   u32ThreadListWithValidParam()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_027

* DESCRIPTION :    Lists Threads from the Global Thread Table.
				   NOTE : This case assumes several threads to
				          have already started as part of system
						  initialization, if no threads are in the
						  system,the Thread Table contains no information,
						  and ThreadList OSAL API will return zero.		  
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/
tU32 u32ThreadListWithValidParam( tVoid )
{
	/*Definitions*/
	tS32 pas32List[LIST_SIZE] = {0};
	tS32 s32Length            = (tS32)LIST_SIZE;	
	tS32 s32ReturnParam       = -1;
	tS32 s32Count             = 0;
	tU32 u32Ret               = 0;

	/*Issue call to Thread List API*/
	s32ReturnParam = OSAL_s32ThreadList( pas32List,s32Length );

	/*Check for returned value*/
	if( -1 == s32ReturnParam )
	{
		return 10000;
	}
	else
	{
		if( s32ReturnParam > s32Length )
		{
			return 20000;
		}
		else
		{
			/*Array Check*/
			while( s32Count < s32ReturnParam )
			{
				if( pas32List[s32Count] == 0 )
				{
					u32Ret += (tU32)s32Count;
				}
				s32Count++;
			}
			
			/*No of Threads in list*/
			OEDT_HelperPrintf( TR_LEVEL_USER_1, "No of Threads in List : %d\n",s32ReturnParam );
		}
	}

	/*Return errorcode*/
	return u32Ret;
}
	
/*****************************************************************************
* FUNCTION    :	   u32AttainVariousThreadState()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_028

* DESCRIPTION :    Attain thread states :
                   OSAL_EN_THREAD_INITIALIZED,         
       			   OSAL_EN_THREAD_RUNNING,             
       			   OSAL_EN_THREAD_READY,               
       			   OSAL_EN_THREAD_PENDED,              
       			   OSAL_EN_THREAD_DELAYED,             
       			   OSAL_EN_THREAD_SUSPENDED,           
       			   OSAL_EN_THREAD_SUSPENDED_PENDED,    
       			   OSAL_EN_THREAD_SUSPENDED_DELAYED.
				   Check whether OSAL Implementation for
				   Thread Control Block keeps track of thread states
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Nov 21,2007
*******************************************************************************/

#if 0   // COMMENT OUT all Tests to make it compilable  OSR
tU32 u32AttainVariousThreadState( tVoid )
{
	tU32 u32Ret                   = 0;
#ifndef TSIM_OSAL
	/*Definitions*/
	
	OSAL_tThreadID tID            = 0;
	OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
	OSAL_trThreadControlBlock tCB =	{OSAL_NULL};

	
	/*Create the Semaphore*/
	if( OSAL_OK == OSAL_s32SemaphoreCreate( "Thread_State_Semaphore",
	                                              &u32SemThreadState,
												  0 ) )
	{
		u8Switch = 1;
	}	
	/*Create the Semaphore*/
	
	/*Fill in the thread attributes*/
	trAtr.szName                 =  "Thread_State";
	trAtr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr.s32StackSize           =  VALID_STACK_SIZE;
	trAtr.pfEntry                =  Thread_State_Test;
	trAtr.pvArg                  =  OSAL_NULL;	

	if( OSAL_ERROR == ( tID = OSAL_ThreadCreate( &trAtr ) ) )
	{
		/*Thread Create failed*/
		return 100;
	}
	else
	{
		/*THREAD STATE : INITIALISED*/
		if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
		{
			u32Ret += 200;
		}
		else
		{
			/*Check for Thread State*/
			if( OSAL_EN_THREAD_INITIALIZED != tCB.enStatus )
			{
				switch( tCB.enStatus )
				{
					case OSAL_EN_THREAD_RUNNING          :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 break;
					case OSAL_EN_THREAD_READY            :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 break;
					case OSAL_EN_THREAD_PENDED           :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 break;
					case OSAL_EN_THREAD_DELAYED          :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 break;
					case OSAL_EN_THREAD_SUSPENDED        :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 break;
					case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 break;
					case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 break;
					case OSAL_EN_THREAD_INITIALIZED:
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Initialized\n" );
						 break;
					case OSAL_EN_THREAD_PENDED_DELAYED:
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Pended and Delayed\n" );
						 break;
					case OSAL_EN_THREAD_INVALIDSTATUS:
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Invalid Status\n" );
						 break;
					default                              :
						 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 break;
				}
				u32Ret += 300;
			}
		}/*Thread Control Block call successful*/
		/*THREAD STATE : INITIALISED*/

		/*THREAD STATE : READY/RUNNING*/
		if( OSAL_ERROR == OSAL_s32ThreadActivate( tID ) )
		{
			u32Ret += 400;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
			{
				u32Ret += 200;
			}
			else
			{

				/*Check for Thread State*/
				/*rav8kor - thread can be in pended state as it is waiting for a post */
				if( ( OSAL_EN_THREAD_READY != tCB.enStatus ) && ( OSAL_EN_THREAD_RUNNING != tCB.enStatus )&& 
				    ( OSAL_EN_THREAD_PENDED != tCB.enStatus )
				  )
				{
					switch( tCB.enStatus )
					{
						case OSAL_EN_THREAD_INITIALIZED      :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 break;
					   	case OSAL_EN_THREAD_DELAYED          :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED        :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 break;
					    case OSAL_EN_THREAD_RUNNING:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Running\n" );
						 	 break;
					    case OSAL_EN_THREAD_READY:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Ready\n" );
						 	 break;
					    case OSAL_EN_THREAD_PENDED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Pended\n" );
						 	 break;
					    case OSAL_EN_THREAD_PENDED_DELAYED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Pended and Delayed\n" );
						 	 break;
					    case OSAL_EN_THREAD_INVALIDSTATUS:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Thread Pended and Delayed\n" );
						 	 break;
						default                              :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 break;
					}
					u32Ret += 500;
				}
			}/*Thread Control Block call successful*/
		}/*Thread Activate is successful*/
		/*THREAD STATE : READY/RUNNING*/

		if( u8Switch )
		{
			/*THREAD STATE : PENDED*/
			/* Main Thread is forced to wait, because, until the time the 
			tkernel semaphore wait call is made in the subthread,
			the subthread is still could be in the run state*/
			OSAL_s32ThreadWait( 500 ); 

			if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
			{
				u32Ret += 600;
			}
			else
			{
				/*Check for thread state*/
				if( OSAL_EN_THREAD_PENDED != tCB.enStatus )
				{
					switch( tCB.enStatus )
					{
						case OSAL_EN_THREAD_INITIALIZED      :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 break;
						case OSAL_EN_THREAD_RUNNING          :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 	 break;
						case OSAL_EN_THREAD_READY            :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 	 break;
						case OSAL_EN_THREAD_DELAYED          :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED        :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 break;
						case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 break;
						case OSAL_EN_THREAD_PENDED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 break;
						case OSAL_EN_THREAD_PENDED_DELAYED:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 break;
						case OSAL_EN_THREAD_INVALIDSTATUS:
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid State\n" );
						 	 break;
						default                              :
						 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 break;
					}
					u32Ret += 700;
				}
			}/*Thread Control Block call successful*/
			/*THREAD STATE : PENDED*/

			/*THREAD STATE : SUSPENDED + PENDED*/
			if( OSAL_ERROR == OSAL_s32ThreadSuspend( tID ) )
			{
				u32Ret += 800;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
				{
					u32Ret += 900;
				}
				else
				{
					/*Check for thread state*/
					if( OSAL_EN_THREAD_SUSPENDED_PENDED != tCB.enStatus )
					{
						switch( tCB.enStatus )
						{
							case OSAL_EN_THREAD_INITIALIZED      :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 	 break;
							case OSAL_EN_THREAD_RUNNING          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 	 	 break;
							case OSAL_EN_THREAD_READY            :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED           :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_DELAYED          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED        :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_PENDED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_INVALIDSTATUS:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid State\n" );
						 	 	 break;
							default                              :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 	 break;
						}
						u32Ret += 1000;
					}
				}/*Thread Control Block Call successful*/
			}/*Thread suspend successful*/
			/*THREAD STATE : SUSPENDED + PENDED*/

			/*THREAD STATE : SUSPENDED*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( u32SemThreadState ) )
			{
				u32Ret += 1100;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
				{
					u32Ret += 1200;
				}
				else
				{
					/*Check for thread state*/
					if( OSAL_EN_THREAD_SUSPENDED != tCB.enStatus )
					{
						switch( tCB.enStatus )
						{
							case OSAL_EN_THREAD_INITIALIZED      :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 	 break;
							case OSAL_EN_THREAD_RUNNING          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 	 	 break;
							case OSAL_EN_THREAD_READY            :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED           :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_DELAYED          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_INVALIDSTATUS:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid State\n" );
						 	 	 break;
							default                              :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 	 break;
						}
						u32Ret += 1300;
					}
				}/*Thread Control Block Call successful*/
			}/*Semaphore Post successful*/
			/*THREAD STATE : SUSPENDED*/

			/*THREAD STATE : DELAYED*/
			if( OSAL_ERROR == OSAL_s32ThreadResume( tID ) )
			{
				u32Ret += 1400;
			}
			else
			{
				/*Force the Main Thread to wait, because the resumed thread
				could go to running state and then to delayed state.So it 
				must be assured that the subthread is in delayed state*/
				OSAL_s32ThreadWait( 300 );

				if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
				{
					u32Ret += 1500;
				}
				else
				{
					/*Check for thread state*/
					if( OSAL_EN_THREAD_DELAYED != tCB.enStatus )
					{
						switch( tCB.enStatus )
						{
							case OSAL_EN_THREAD_INITIALIZED      :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 	 break;
							case OSAL_EN_THREAD_RUNNING          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 	 	 break;
							case OSAL_EN_THREAD_READY            :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED           :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED        :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED_DELAYED:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_INVALIDSTATUS:
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid State\n" );
						 	 	 break;
							default                              :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 	 break;
						}
						u32Ret += 1600;
					}
				}/*Thread Control Block Call successful*/
			}/*Thread Resume successful*/
			/*THREAD STATE : DELAYED*/

			/*THREAD STATE : SUSPENDED + DELAYED*/
			if( OSAL_ERROR == OSAL_s32ThreadSuspend( tID ) )
			{
				u32Ret += 1700;
			}
			else
			{
				if( OSAL_ERROR == OSAL_s32ThreadControlBlock( tID,&tCB ) )
				{
					u32Ret += 1800;
				}
				else
				{
					/*Check for thread state*/
					if( OSAL_EN_THREAD_SUSPENDED_DELAYED != tCB.enStatus )
					{
						switch( tCB.enStatus )
						{
							case OSAL_EN_THREAD_INITIALIZED      :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Initialized\n" );
						 	 	 break;
							case OSAL_EN_THREAD_RUNNING          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Running\n" );
						 	 	 break;
							case OSAL_EN_THREAD_READY            :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Ready\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED           :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_DELAYED          :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED        :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_PENDED :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Pended\n" );
						 	 	 break;
							case OSAL_EN_THREAD_SUSPENDED_DELAYED :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Suspended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_PENDED_DELAYED :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Pended and Delayed\n" );
						 	 	 break;
							case OSAL_EN_THREAD_INVALIDSTATUS :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid State\n" );
						 	 	 break;
							default                              :
						 	 	 OEDT_HelperPrintf( TR_LEVEL_USER_1, "Thread State : Invalid Status\n" );
						 	 	 break;
						}
						u32Ret += 1900;
					}
				}/*Thread Control Block Call successful*/
			} /*Thread Suspend successful*/
			/*THREAD STATE : SUSPENDED + DELAYED*/

			/*Resume the suspended thread*/
			if( OSAL_ERROR == OSAL_s32ThreadResume( tID ) )
			{
				u32Ret += 2000;
			}
			/*Resume the suspended thread*/
		}
		
		/*Delete the Thread from the system*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
		{
			u32Ret += 2100;
		}
	}

	if( u8Switch )
	{
		/*Close the Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( u32SemThreadState ) )
		{
			/*Delete the Semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "Thread_State_Semaphore" ) )
			{
				u32Ret += 2200;
			}
		}
		else
		{
			u32Ret += 2300;
		}
	}

	/*Reinitialize switch*/
	u8Switch = 0;

	/*Return the error code*/
#endif /* tsim */
	return u32Ret;
}

#endif   // COMMENT OUT all Tests to make it compilable  OSR

/*****************************************************************************
* FUNCTION    :	   u32ProcessSpawnValid()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_029
* DESCRIPTION :    Spawn Test Process and Delete.
				   Requires build of ProcOEDT_PF_Test.out and its placement in 
				   "/nor0" path as a prerequisite			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 GetProcessResponse(OSAL_tMQueueHandle mqHandle)
{
    tU32    u32Ret = 0;
    tU32    inmsg, procnum, procerr;

    if(OSAL_s32MessageQueueWait(mqHandle, (tPU8)&inmsg, sizeof(inmsg), OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
        u32Ret += 1000;
    procnum = inmsg >> 16;
    procerr = inmsg & 0x0000ffff;
    if(procerr != 0)
    {
        u32Ret += procerr * 10000;
    }

    return u32Ret;
}



tU32 u32SpawnMultiProcesses( tVoid )
{
	/*Definitions*/
	tU32   u32Ret                   = 0;
    OSAL_tMQueueHandle mqHandleP1   = OSAL_C_INVALID_HANDLE;
    OSAL_tMQueueHandle mqHandleP2   = OSAL_C_INVALID_HANDLE;
    OSAL_tMQueueHandle mqHandleR    = OSAL_C_INVALID_HANDLE;
    tU32    outmsg;
	OSAL_tProcessID	prID_1,prID_2   = INVALID_PID;
	OSAL_trProcessAttribute	prAtr   = {OSAL_NULL};
//pid_t  w1,w2;
//int    statusp1=0;
//int    statusp2=0;
	int    i;
	tU32   testset[] =
	{
        MP_TEST_SPMVOLT,
        MP_TEST_TESTRTC,
        MP_TEST_TESTMQ,
        MP_TEST_TESTMQ_IPC_PERF,
        MP_TEST_TESTMQ_IPC_CONTENT,
        MP_TEST_TESTMQ_OPEN_CLOSE_TWICE,

        MP_TEST_FFD_WRS,

        MP_TEST_KDS_WRITE_FULL,
        MP_TEST_KDS_READ_ENTRY,

        MP_TEST_MSGPOOL_OPEN,
        MP_TEST_MSGPOOL_CHECK_SIZE,

        MP_TEST_END
	};
	tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";

	// Create or open MQ to control first process
    if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_P1_CONTROL_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleP1) == OSAL_ERROR)
        if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_P1_CONTROL_NAME, OSAL_EN_WRITEONLY, &mqHandleP1) == OSAL_ERROR)
            u32Ret += 1;

    // Create or open MQ to control second process
    if(u32Ret == 0)
        if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_P2_CONTROL_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleP2) == OSAL_ERROR)
            if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_P2_CONTROL_NAME, OSAL_EN_WRITEONLY, &mqHandleP2) == OSAL_ERROR)
                u32Ret += 2;

    // Create or open MQ for both processes to send their responses
    if(u32Ret == 0)
        if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_RESPONSE_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR)
            if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_RESPONSE_NAME, OSAL_EN_READONLY, &mqHandleR) == OSAL_ERROR)
                u32Ret += 3;

    // Spawn first process
    if(u32Ret == 0)
    {
        prAtr.szCommandLine  = (char*)"bindcpu0";
        prAtr.szName         = "procp1_out.out";
        OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
        prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
        prAtr.u32Priority    = PRIORITY;
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
        if( OSAL_ERROR == ( prID_1 = OSAL_ProcessSpawn( &prAtr ) ) )
        {
            printf("spawn p1 error %d\n", OSAL_u32ErrorCode());
            u32Ret += 10;
        }
    }

    // Spawn second process
    if(u32Ret == 0)
    {
        prAtr.szCommandLine  = (char*)"bindcpu1";
        prAtr.szName         = "procp2_out.out";
        OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
        prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
        prAtr.u32Priority 	 = PRIORITY;
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
        if( OSAL_ERROR == ( prID_2 = OSAL_ProcessSpawn( &prAtr ) ) )
        {
            printf("spawn p2 error %d\n", OSAL_u32ErrorCode());
            u32Ret += 20;
        }
    }


    if(u32Ret == 0)
    {
        // Start all tests listed in testset[]
        for(i = 0; i < (int)(sizeof(testset)/sizeof(testset[0])); i++)
        {
            outmsg = testset[i];

            // Tell first process to start test
            if(OSAL_s32MessageQueuePost(mqHandleP1, (tPCU8)&outmsg, sizeof(outmsg), 2) != OSAL_OK)
                u32Ret += 100;
            // Tell second process to start test
            if(OSAL_s32MessageQueuePost(mqHandleP2, (tPCU8)&outmsg, sizeof(outmsg), 2) != OSAL_OK)
                u32Ret += 200;

            // Wait for processes to respond
            // TODO: All OSAL_s32MessageQueueWait use OSAL_C_TIMEOUT_FOREVER. So if one process gets stuck, the whole thing hangs
            u32Ret += GetProcessResponse(mqHandleR);
            u32Ret += GetProcessResponse(mqHandleR);
        }
    }


    // Close & delete MQs
    if(mqHandleP1 != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleP1) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_P1_CONTROL_NAME);

    if(mqHandleP2 != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleP2) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_P2_CONTROL_NAME);

    if(mqHandleR != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleR) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_RESPONSE_NAME);

    return u32Ret;
}


/*****************************************************************************
* FUNCTION    :      u32ProcessSpawnValid()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_029
* DESCRIPTION :    Spawn Test Process and Delete.
               Requires build of ProcOEDT_PF_Test.out and its placement in
               "/nor0" path as a prerequisite
* HISTORY     :      Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32SpawnPerformanceProcess( tVoid )
{
   /*Definitions*/
   tU32 u32Ret                       = 0;
   OSAL_tProcessID   prID            = INVALID_PID;
   OSAL_trProcessAttribute prAtr     = {OSAL_NULL};
   pid_t w;
   int status;
   tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";

   prAtr.szCommandLine  = (char*)"";
   prAtr.szName         = "procperf_out.out";
   OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
   prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
   OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
   prAtr.u32Priority     = PRIORITY;

   if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
   {
      printf("spawn error \n");
   }
   else
   {
      //printf("spawn ok \n");
   }



   w = waitpid(prID, &status, WUNTRACED | WCONTINUED);
   if (w == -1) {
      perror("waitpid perf");
   }

   /*Return the error code*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessSpawnInvalidParam()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_030
* DESCRIPTION :    Spawn a Process with invalid parameter
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessSpawnInvalidParam( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                   = 0;
	
	OSAL_tProcessID	prID          = INVALID_PID;
	OSAL_trProcessAttribute	prAtr = {OSAL_NULL};
	
	/*INVALID PRIORITY*/
	/*Fill in the ProcessAttributes*/ 
	prAtr.szCommandLine	 = 0;
	prAtr.szAppName	     = "TESTPROCESS";
	prAtr.szName		 = "/host/DEBUG/ProcOEDT_PF_Test.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
#ifndef TSIM_OSAL
	prAtr.u32Priority 	 = INVAL_PRIORITY;
#endif /*#ifndef TSIM_OSAL*/
	

	/*Spawn the LFS Process*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
	{
		/*Get the error code
		for sake of query, in future when more clarifications arrive as regards
		the true error code, code will be extended to catch the actual error*/
		
	}
	else
	{
		/*Process Spawn passed!*/
		u32Ret += 1000;
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32Ret += 100;
			
		}
	}
	/*INVALID PRIORITY*/

	/*INVALID PATH - Path to some binary never existent*/
	/*Fill in the ProcessAttributes*/ 
	prAtr.szCommandLine	 = 0;
	prAtr.szAppName	     = "LFSTEST";
	prAtr.szName		 = "/nand0/ProcDA.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
#ifndef TSIM_OSAL
	prAtr.u32Priority 	 = PRIORITY;
#endif /*#ifndef TSIM_OSAL*/

	/*Spawn the LFS Process*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
	{
		/*Get the error code
		for sake of query, in future when more clarifications arrive as regards
		the true error code, code will be extended to catch the actual error*/
		
	}
	else
	{
		/*Process Spawn passed! - critical failure*/
		u32Ret += 2000;
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32Ret += 200;
			
		}
	}
	/*INVALID PATH - Path to some binary never existent*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessWhoAmI
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_031
* DESCRIPTION :    Test for API OSAL_ProcessWhoAmI
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessWhoAmI( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	OSAL_tProcessID	prID = INVALID_PID;

	/*Call ProcessWhoAmI API*/
	prID = OSAL_ProcessWhoAmI( );

	/*Check for the ID returned*/
	if( INVALID_PID == prID )
	{
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessSelfDelete
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_032
* DESCRIPTION :    Try to delete the invoking process
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessSelfDelete( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	OSAL_tProcessID prID = INVALID_PID;
	
	prID = OSAL_ProcessWhoAmI( );
	
	/*Delete the currently running process*/
	if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
	{
		
		//Do nothing		;
	}
   	else
	{
		/*Process Delete interface returned success!*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessListValid
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_033
* DESCRIPTION :    Retrive a list of Processes running in a system.
				   ProcOEDT_PF_Test.out must be aviliable in /host/debug/ path
				   as a prerequisite before running the case.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/	
tU32 u32ProcessListValid( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 						= 0;
	tU32 u32Count                       = 0;
	tU32 u32ChangeCount                 = 0;
	tS32 as32List[T_ENGINE_MAX_PROCESS] = {
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
										  };
	tS32 s32Length 						=  T_ENGINE_MAX_PROCESS;
	tS32 s32ReturnedLength              = -1;
#if KERNEL_MODE
	OSAL_trProcessAttribute	prAtr       = {OSAL_NULL};
	OSAL_tProcessID prID                = INVALID_PID;
#endif


	/*Get number of processes registered in the system*/
	s32ReturnedLength = OSAL_s32ProcessList( as32List,s32Length );

	/*Check the returned length*/
	if( s32ReturnedLength > s32Length )
	{
		u32Ret += 1000;
	}

	/*Check for the Array modifications*/
	while( u32Count < T_ENGINE_MAX_PROCESS )
	{
		/*Find the number of Processes registered in the system*/
		if( DEFAULT_PID != as32List[u32Count] )
		{
		   ++u32ChangeCount;
		}

		/*Increment count*/
		++u32Count;
	}

	/*Check for Number of registered Processes returned by OSAL_s32ProcessList
	and the number of modified IDs in the Array list*/
	if( (tS32)u32ChangeCount != s32ReturnedLength )
	{
		/*No Process ID added to array*/
		u32Ret += 2000;
	}

	#if KERNEL_MODE
	/*Spawn a process*/
	/*Fill in the ProcessAttributes
	- Requires build of ProcOEDT_PF_Test and its placement in 
	"/host/DEBUG" path as a prerequisite*/ 
	prAtr.szAppName	     = "TESTPROCESS";
	prAtr.szCommandLine	 = 0;
	prAtr.szName		 = "/host/DEBUG/ProcOEDT_PF_Test.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
	prAtr.u32Priority 	 = PRIORITY;

	/*Reinitialize the array and temperory variables*/
	as32List[0] = DEFAULT_PID;as32List[1] = DEFAULT_PID;
	as32List[2] = DEFAULT_PID;as32List[3] = DEFAULT_PID;
	as32List[4] = DEFAULT_PID;as32List[5] = DEFAULT_PID;
	as32List[6] = DEFAULT_PID;as32List[7] = DEFAULT_PID;
	as32List[8] = DEFAULT_PID;as32List[9] = DEFAULT_PID;
	u32Count          = 0;
	u32ChangeCount    = 0;
	s32ReturnedLength = -1;

	/*Spawn a process - No check to see if process spawned or not
	as ProcessList API is not interested in whether Process spawned is
	actually spawned or not*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( (const)&prAtr ) ) )
	{
		u32ECode = OSAL_u32ErrorCode( );
	}
	else
	{
		/*Collect the Process List*/
		s32ReturnedLength = OSAL_s32ProcessList( as32List,s32Length );

		/*Query the return code*/
		if( s32ReturnedLength < 1 )
		{
			/*At least 1 process was registered*/
			u32Ret += 10000;
		}
	
		/*Scan the array*/
		while( u32Count < T_ENGINE_MAX_PROCESS )
		{
			/*Find the number of Processes registered in the system*/
			if( DEFAULT_PID != as32List[u32Count] )
			{
		   		++u32ChangeCount;
			}

			/*Increment count*/
			++u32Count;
		}
	
		/*Check the change count*/
		if( u32ChangeCount < 1 )
		{
			/*At least 1 process was registered*/
			u32Ret += 5000;
		} 	

		/*Compare ChangeCount and returned no of registered processes, they
		are expected to be the same*/
		if( (tS32)u32ChangeCount != s32ReturnedLength )
		{
			u32Ret += 3000;
		}

		/*Delete the Process*/
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32ECode = OSAL_u32ErrorCode( );
		}
	}
	#endif

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessListInvalidList
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_034
* DESCRIPTION :    Try using Process List with List as NULL.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessListInvalidList( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	tS32 s32Length       = T_ENGINE_MAX_PROCESS;
	tS32 s32ReturnLength = -1;

	/*Call the Process List API with NULL as list*/
    s32ReturnLength = OSAL_s32ProcessList( OSAL_NULL,s32Length ); 

	/*Query the status of s32ReturnLength*/
	if( s32ReturnLength > 0 )
	{
		u32Ret += 1000;
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32ProcessListInvalidSize
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_035
* DESCRIPTION :    Try using Process Size as -15
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessListInvalidSize( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                         = 0;
	tU32 u32Count                       = 0;
	tU32 u32ChangeCount                 = 0;
	tS32 s32Length                      = SIZE_INVAL;
	tS32 s32ReturnLength                = -1;
	tS32 as32List[T_ENGINE_MAX_PROCESS] = {
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
										  };

	/*Call the Process List API with NULL as list*/
    s32ReturnLength = OSAL_s32ProcessList( as32List,s32Length ); 

	/*Query the status of s32ReturnLength*/
	if( s32ReturnLength > 0 )
	{
		u32Ret += 1000;
	}

	/*Query the status of the array*/
	while( u32Count < T_ENGINE_MAX_PROCESS )
	{
		/*Check the Array content*/
		if( DEFAULT_PID == as32List[u32Count] )
		{
		   ++u32ChangeCount	;
		}

		/*Increment count*/
		++u32Count;
	}

	/*Check the change count , Array is not expected to change*/
	if( u32ChangeCount < T_ENGINE_MAX_PROCESS )
	{
		/*Array did change with an invalid(negative) size passed!*/
		u32Ret += 2000;
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32GetPCBValidParam
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_036
* DESCRIPTION :    Get the Process Control Block for a valid Process.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32GetPCBValidParam( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;
	OSAL_trProcessControlBlock prCB	= {OSAL_NULL};

	/*Get the current Process ID*/
	prID = OSAL_ProcessWhoAmI( );

	/*Get the Process Control Block for the Current Process*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,&prCB ) )
	{	
		u32Ret += 1000;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32QueryPCBInvalidID
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_037
* DESCRIPTION :    Try to Get the Process Control Block for an invalid Process ID
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32QueryPCBInvalidID( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;
	OSAL_tProcessID	prID_           = T_ENGINE_MAX_PROCESS+10;
	OSAL_trProcessControlBlock prCB	= {OSAL_NULL};

	/*Get the Process Control Block for a negative Process ID*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,&prCB ) )
	{	
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 50;
		}	
	}
	else
	{
		/*PCB Query Passed! - Should not pass*/
		u32Ret += 200;
	}

	/*Get the Process Control Block for a Process ID greater than max*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID_,&prCB ) )
	{	
		if( OSAL_E_WRONGPROCESS != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 150;
		}	
	}
	else
	{
		/*PCB Query Passed! - Should not pass*/
		u32Ret += 300;
	}
	
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32QueryPCBInvalidAddress
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_038
* DESCRIPTION :    Try to get the Process Control Block for 
				   a Process with address for Process Control Block structure as
				   NULL
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32QueryPCBInvalidAddress( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;

	/*Get the current Process ID*/
	prID = OSAL_ProcessWhoAmI( );

	/*Get the Process Control Block for the Current Process*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,OSAL_NULL ) )
	{	
	   if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 50;
		}
	}
	else
	{
		/*Call to Process Control Block should not pass!*/
		u32Ret += 100;
	}
	
	/*Return the error code*/
	return u32Ret;
}											

/*****************************************************************************
* FUNCTION    :	   u32SpawnThreadInfiniteLoop
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_039
* DESCRIPTION :    Spawn a Thread whose body has an infinite loop
* HISTORY     :	   
*					Thread Priority change from Midel to low by 
*					Anoop Chandran(RBEI\ECM1) 27/09/2008 					
*Created by Tinoy Mathews(RBIN/EDI3)  21.01.08
*******************************************************************************/											
tU32 u32SpawnThreadInfiniteLoop( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	OSAL_tThreadID tid            = 0;
	OSAL_trThreadAttribute trAttr  = {OSAL_NULL};

	/*Fill in the thread attributes*/
	trAttr.szName                 =  "Infinite_Thread";
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_LOWEST;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Infinite_Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Spawn the thread*/
	if( OSAL_ERROR == ( tid = OSAL_ThreadSpawn( &trAttr ) ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tid ) )
		{
			u32Ret += 2000;
		}
	}
	
	/*Return the error code*/
	return u32Ret;
}
