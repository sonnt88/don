/******************************************************************************
 *FILE         : oedt_osalcore_MTX_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the MUTEX OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 
 *              ver1.3   - 22-04-2009
                lint info removal
				sak9kor

 *				ver1.2	 - 14-04-2009
                warnings removal,fix problems in test cases
				rav8kor

 				Version 1.1 , 31- 01- 2008
 *				 Added case 025
 *				 Tinoy Mathews( RBIN/ECM1 )
 *
 *				 Version 1.0 , 27- 12- 2007
 *				 	
 *
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "TEngine_osal.h"

#include "oedt_helper_funcs.h"
#include "oedt_osalcore_MTX_TestFuncs.h"

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1

extern tS32 OSAL_s32MutexCreate(tCString coszName,OSAL_tMtxHandle * phMutex,tU32 u32Opt);
extern tS32 OSAL_s32MutexDelete(tCString coszName);
extern tS32 OSAL_s32MutexOpen(tCString coszName, OSAL_tMtxHandle * phMutex);
extern tS32 OSAL_s32MutexClose(OSAL_tMtxHandle hMutex);
extern tS32 OSAL_s32MutexUnLock(OSAL_tMtxHandle hMutex);
extern tS32 OSAL_s32MutexLock(OSAL_tMtxHandle hMutex, OSAL_tMSecond msec);

/*Global Variables*/
OSAL_tMtxHandle mtxHandleglob     			 = 0;
/*Semaphore to signal between OEDT task and Main thread*/
OSAL_tSemHandle mtxSemMain_To_OEDT           = 0;
/*Semaphore to signal between Main thread and Sub thread*/
OSAL_tSemHandle mtxSemSub_To_Main            = 0;
OSAL_tSemHandle mtxSemMain_To_Sub            = 0;

/*Data to be Protected*/
tU32 u32Data                                 = 0;

/*****************************************************************************
* FUNCTION    :	   u32CreateMTXHandleNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_001
* DESCRIPTION :    Try to Create a Mutex with Mutex Handle Address as OSAL_NULL
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CreateMTXHandleNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret = 0;

	/*Attempt to Create a Mutex with Mutex Handle address as OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexCreate( MUTEX_NAME,OSAL_NULL,MUTEX_PAYLOAD ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( )	)
		{
			/*Returned error code was not as expected*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Mutex Creation passed!*/
		u32Ret += 10000;

		/*Attempt to close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( OSAL_NULL ) )
		{
			/*Attempt to delete the Mutex*/
			if(OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ))
			{
				u32Ret += 100;	
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateMTXNameNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_002
* DESCRIPTION :    Try to Create a Mutex with Mutex Name as OSAL_NULL
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CreateMTXNameNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret               = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Attempt to Create a Mutex with Mutex Name as OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexCreate( OSAL_NULL,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( )	)
		{
			/*Returned error code was not as expected*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Mutex Creation passed!*/
		u32Ret += 10000;

		/*Attempt to close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Attempt to delete the Mutex*/
			if(OSAL_ERROR == OSAL_s32MutexDelete( OSAL_NULL ))
			{
				u32Ret += 100;	
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateMTXNameMAX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_003
* DESCRIPTION :    Try to Create a Mutex with Mutex Name upto maximum
			       31 characters.
				   Should create the Mutex
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CreateMTXNameMAX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret               = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create a Mutex with Mutex Name as OSAL_NULL*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME_MAX,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME_MAX ) )
			{
				/*Mutex Delete failed*/
				u32Ret += 200;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 100;
		}
	}
	else
	{
		/*Mutex Creation failed!*/
		u32Ret += 1000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateMTXNameExceedsMAX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_004
* DESCRIPTION :    Try to Create a Mutex with Mutex Name exceeding Maximum
                   Length.
				   Should return error code OSAL_E_NAMETOOLONG.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CreateMTXNameExceedsMAX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret               = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Attempt to Create a Mutex with Mutex Name as OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexCreate( MUTEX_NAME_EXCEEDS_MAX,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		if( OSAL_E_NAMETOOLONG != OSAL_u32ErrorCode( )	)
		{
			/*Returned error code was not as expected*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Mutex Creation passed!*/
		u32Ret += 10000;

		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if(OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME_EXCEEDS_MAX ) )
			{
				u32Ret += 100;
	
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateMTXNameExisting()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_005
* DESCRIPTION :    Try to Create a Mutex with Mutex Name of an already existing
                   Mutex.
				   Should return error code OSAL_E_ALREADYEXISTS.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CreateMTXNameExisting( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	OSAL_tMtxHandle mtxHandle  = 0;
	OSAL_tMtxHandle mtxHandle_ = 0;

	/*Attempt to Create a Mutex with Mutex Name as OSAL_NULL*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Mutex Create Successful*/

		/*Recreate the Mutex*/
		if( OSAL_ERROR == OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle_,MUTEX_PAYLOAD ) )
		{
			if( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 1000;
			}
		}
		else
		{
			/*Mutex Created! - Should not!*/
			u32Ret += 2000;

			/*Attempt Shutdown*/
			if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle_ ) )
			{
				/*Delete the Mutex*/
				if(OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ))
				{
					u32Ret += 100;
				}
			}
		}
		
		/*Do a Shutdown*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 3000;
			}
		}
		else
		{
			/*Cannot close the Mutex!*/
			u32Ret += 4000;
		}
	}
	else
	{
		/*Mutex Create has failed*/
		u32Ret += 5000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenMTXNameNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_006
* DESCRIPTION :    Try to Open a Mutex with Mutex Name as OSAL_NULL
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32  u32OpenMTXNameNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Open the Mutex with name set to OSAL_NULL*/
		if( OSAL_ERROR == OSAL_s32MutexOpen( OSAL_NULL,&mtxHandle ) )
		{
			/*Check for the correct error code*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 100;
			}
		}
		else
		{
			/*Mutex opened! - error*/
			u32Ret += 1000;

			/*Close the opened Mutex - Attempt Shutdown*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				u32Ret += 3000;
			}

		}

		/*Do a shutdown now*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 6000;
			}
		}
		else
		{
			/*Mutex close failed*/
			u32Ret += 450;
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32OpenMTXHandleNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_007
* DESCRIPTION :    Try to Open a Mutex with Mutex Handle as OSAL_NULL
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32  u32OpenMTXHandleNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Open the Mutex Handle set to OSAL_NULL*/
		if( OSAL_ERROR == OSAL_s32MutexOpen( MUTEX_NAME,OSAL_NULL ) )
		{
			/*Check for the correct error code*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 100;
			}
		}
		else
		{
			/*Mutex opened! - error*/
			u32Ret += 1000;

			/*Close the opened Mutex - Attempt Shutdown*/
			OSAL_s32MutexClose( OSAL_NULL );
			
		}

		/*Do a shutdown now*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 6000;
			}
		}
		else
		{
			/*Mutex close failed*/
			u32Ret += 450;
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenMTXNonExistent()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_008
* DESCRIPTION :    Try to Open a deleted Mutex.
				   Should return error code OSAL_E_DOESNOTEXIST.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32OpenMTXNonExistent( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Do a shutdown now*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Deletion of Mutex failed*/
				u32Ret += 6000;
			}
		}
		else
		{
			/*Mutex close failed*/
			u32Ret += 450;
		}

		/*Attempt a Mutex Open now - Should fail here*/
		if( OSAL_ERROR == OSAL_s32MutexOpen( MUTEX_NAME,&mtxHandle ) )
		{
			/*Check for the error code*/
			if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 2000;
			}
		}	
		else
		{
			/*Mutex open passed! - Should not*/
			u32Ret += 7000;

			/*Do Shutdown activities*/
			if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
			{
				if(OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ))
				{
					u32Ret += 100;	
				}
			}
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 9000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CloseMTXNonExistent()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_009
* DESCRIPTION :    Try to Close a deleted Mutex.
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CloseMTXNonExistent( tVoid )
{
	/*Definitions*/
	tU32 u32Ret				   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;		

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Do a shutdown now*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Deletion of Mutex failed*/
				u32Ret += 6000;
			}
		}
		else
		{
			/*Mutex close failed*/
			u32Ret += 450;
		}

		/*Attempt a Mutex Close now - Should fail here*/
		if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Check for the error code*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 2000;
			}
		}	
		else
		{
			/*Mutex close passed! - Should not*/
			u32Ret += 7000;

			/*Do Shutdown activities*/				
			if(OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ))
			{
				u32Ret += 100;
			}
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 9000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReCloseMTX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_010
* DESCRIPTION :    Try to ReClose a Mutex.
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32ReCloseMTX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;
	
	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Close the Mutex now*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Reclose the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				/*Query the error code*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
				{
					/*Wrong error code*/
					u32Ret += 1000;
				}
			}
			else
			{
				/*Reclose Passed, should not*/
				u32Ret += 2000;
			}

			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 5000;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 7000;
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CloseMTXHandleNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_011
* DESCRIPTION :    Try to Close a Mutex with handle OSAL_NULL
				   Should return error code OSAL_E_INVALIDVALUE.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32  u32CloseMTXHandleNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret = 0;

	/*Close Mutex passing OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexClose( OSAL_NULL ) )
	{
		/*Query the error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Mutex Close passed!*/
		u32Ret += 4000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CloseMTXBetweenLockUnlock()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_012
* DESCRIPTION :    Close a Mutex between a Lock and Unlock
				   Should return success
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32CloseMTXBetweenLockUnlock( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Lock the Mutex*/
		if( OSAL_ERROR == OSAL_s32MutexLock( mtxHandle,OSAL_C_TIMEOUT_FOREVER ) )
		{
			/*Lock failed*/
			u32Ret += 200;

			/*Close the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				u32Ret += 400;
			}
		}
		else
		{
			/*Close the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				/*Close failed*/
				u32Ret += 1000;
			}
			else
			{
				/*Open the Mutex*/
				if( OSAL_ERROR == OSAL_s32MutexOpen( MUTEX_NAME,&mtxHandle ) )
				{
					/*Open failed*/
					u32Ret += 2000;
				}
				else
				{
					/*Unlock the Mutex*/
					if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
					{
						/*Unlock the Mutex*/
						u32Ret += 5000;

						/*Close the Mutex*/
						if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
						{
							u32Ret += 5500;
						}
					}
					else
					{
						/*Close the Mutex*/
						if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
						{
							u32Ret += 8000;
						}
					}/*Unlock Mutex Successful*/
				}/*Open Mutex Successful*/
			}/*Close Mutex Successful*/
		}/*Lock Mutex Successful*/

		/*Delete the Mutex*/
		if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
		{
			/*Delete failed*/
			u32Ret += 9000;
		}
	}/*Create Mutex Successful*/
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}
		
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReDeleteMTX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_013
* DESCRIPTION :    Delete Mutex already deleted
				   Should return OSAL_E_DOESNOTEXIST on second delete
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32ReDeleteMTX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret				   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create a Mutex*/
    if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) ) 
	{
		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR != OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Redelete the Mutex*/
				if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
				{
					if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( )	)
					{
						u32Ret += 50;
					}
				}
				else
				{
					/*Redelete successful!*/
					u32Ret += 1000;
				}
			}
			else
			{
				/*Delete failed*/
				u32Ret += 3000;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 100;
		}
	}
	else
	{
		/*Mutex Create failed*/
		u32Ret += 200;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DeleteMTXNullHandle()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_014
* DESCRIPTION :    Try to Delete Mutex passing OSAL_NULL for handle
				   Should return OSAL_E_INVALIDVALUE
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32DeleteMTXNullHandle( tVoid )
{
	/*Definitions*/
	tU32 u32Ret = 0;

	/*Try Mutex Delete with parameter OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexDelete( OSAL_NULL ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( )	)
		{
			u32Ret += 10000;
		}
	}
	else
	{
		/*Mutex delete passed for OSAL_NULL!*/
		u32Ret += 20000;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32LockMTXNonExisting()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_015
* DESCRIPTION :    Lock a mutex which is not existing
				   Should return OSAL_E_INVALIDVALUE
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32  u32LockMTXNonExisting( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) ) 
	{
		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Delete failed*/
				u32Ret += 100;
			}
			else
			{
				/*Lock on the Mutex*/
				if( OSAL_ERROR == OSAL_s32MutexLock( mtxHandle,OSAL_C_TIMEOUT_FOREVER ) )
				{
					if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
					{
						/*Wrong error code*/
						u32Ret += 300;
					}
				}
				else
				{
					/*Mutex lock returned success! - Error*/
					u32Ret += 20000;
				}/*Failure of Mutex Lock*/
			}/*Success of Mutex Delete*/
		}/*Success of Mutex Close*/
		else
		{
			/*Mutex could not be closed*/
			u32Ret += 30000;
		}
	}/*Success of Mutex Create*/
	else
	{
		/*Mutex Create failed*/
		u32Ret += 10000;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReLockMTX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_016
* DESCRIPTION :    Lock a mutex which was already locked
				   Should return OSAL_E_UNKNOWN 
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/	
tU32 u32ReLockMTX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	OSAL_tMtxHandle mtxHandle  = 0;

	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Lock on the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexLock( mtxHandle,OSAL_C_TIMEOUT_FOREVER ) )
		{
			/*Lock on the Mutex which is already locked*/
			if( OSAL_ERROR == OSAL_s32MutexLock( mtxHandle,1000 ) )
			{
				if( OSAL_E_UNKNOWN != OSAL_u32ErrorCode( ) )
				{
					u32Ret += 10000;
				}
			}
			else
			{
				/*Lock returned success!*/
				u32Ret += 20000;

				/*Unlock the redundant lock*/
				if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
				{
					u32Ret += 100;
				}
			}
	  		
	  		/*Unlock on the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
			{
				u32Ret += 3000;
			}
		}
		else
		{
			/*Mutex Lock failed*/
			u32Ret += 4000;
		}

		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 7000;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 9000;
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 20000;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   MutexThread_Sub()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Does the Protected Operation
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tVoid MutexThread_Sub( tVoid * vptr )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	/*Lock on the global mutex*/
	OSAL_s32MutexLock( mtxHandleglob,OSAL_C_TIMEOUT_FOREVER );

	/*Critical section*/
	u32Data = u32Data+1;
	/*Critical section*/

	/*Unlock on the global mutex*/
	OSAL_s32MutexUnLock( mtxHandleglob );

	/*Post a response to Main Thread*/
	OSAL_s32SemaphorePost( mtxSemSub_To_Main );

	/*Wait for the Main Thread to Delete*/
	OSAL_s32ThreadWait( OEDT_OSALCORE_MTX_THREE_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   MutexThread_Main()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Spawns Sub Threads( Tasks )
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tVoid MutexThread_Main( tVoid * vptr )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	/*Definitions*/
	OSAL_tThreadID tid_sub[TOT_THREADS] 			  = {0};
	OSAL_trThreadAttribute thrAtr_sub[TOT_THREADS];
	tU8 u8Count                           			  = 0;
	tU8 u8SemFlag                                     = 0;
	tS32 s32SemStatus                                 = 0;
	tChar acBuf[TOT_THREADS][MAX_LENGTH]  			  = {"\0","\0","\0","\0",
														 "\0","\0","\0","\0",
														 "\0","\0"};

	/*Create a Semaphore
	To make possible synchronization between the Thread running the body
	MutexThread_Sub( Sub Thread ) and the Thread running the body MutexThread_Main
	( Main Thread )*/
	if( OSAL_ERROR != OSAL_s32SemaphoreCreate( "SUBTHREAD_TO_MAINTHREAD",&mtxSemSub_To_Main,0 ) )
	{
	   u8SemFlag = 1;
	}	

	/*Lock on the global mutex*/
	OSAL_s32MutexLock( mtxHandleglob,OSAL_C_TIMEOUT_FOREVER );

	/*Create TOT_THREADS Threads*/
	while( u8Count < TOT_THREADS )
	{
		/*Form the thread name*/
		OSAL_s32PrintFormat( acBuf[u8Count],"SubThread_%d",u8Count );

		/*Fill in the Thread attributes*/
		thrAtr_sub[u8Count].u32Priority  = PRIORITY_100;
		thrAtr_sub[u8Count].pvArg        = OSAL_NULL;
		thrAtr_sub[u8Count].s32StackSize = 2048;
		thrAtr_sub[u8Count].szName       = acBuf[u8Count];
		thrAtr_sub[u8Count].pfEntry      = MutexThread_Sub;

		/*Spawn the Thread*/
		tid_sub[u8Count] = OSAL_ThreadSpawn( &thrAtr_sub[u8Count] );

		/*Increment the count*/
		u8Count++;
	}

	/*Unlock on the global mutex*/
	OSAL_s32MutexUnLock( mtxHandleglob );

	/*Wait until semaphore count becomes 10*/
	do
	{
		/*Get the status of the semaphore meant for signalling from
		Sub threads to main thread*/
		OSAL_s32SemaphoreGetValue( mtxSemSub_To_Main,&s32SemStatus );

		/*Wait for 30 milliseconds for a change in semaphore status*/
		OSAL_s32ThreadWait( 30 );

	}while( s32SemStatus != TOT_THREADS ); 

	/*Delete all the subthreads*/
	u8Count = 0;
	while( u8Count < TOT_THREADS )
	{
		/*Delete the thread*/
		OSAL_s32ThreadDelete( tid_sub[u8Count] );
		/*Increment the count*/
		u8Count++;
	}

	/*Remove the Semaphore from the system*/
	if( u8SemFlag )
	{
		/*Delete the Semaphore( Main Thread to OEDT Thread Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemSub_To_Main ) )
		{
			/*Delete the Semaphore*/
			OSAL_s32SemaphoreDelete( "SUBTHREAD_TO_MAINTHREAD" );
		}
	}

	/*Post a response to OEDT Thread*/
	OSAL_s32SemaphorePost( mtxSemMain_To_OEDT );

	/*Wait for the subthreads to finish operation*/
	OSAL_s32ThreadWait( ONE_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   u32LockUnlockMTXValid()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_017
* DESCRIPTION :    Lock , Unlock a mutex on 11 threads
				   Should successfully complete Protected Operation gaurded
				   by the mutex.Success condition is, the protected resource
				   should not be corrupted.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/					
tU32 u32LockUnlockMTXValid( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			      	      = 0;
	tU8  u8Flag                           = 0;
	tU8  u8MutexFlag                      = 0;
	tU8  u8SemaphoreFlag                  = 0;
	tS8  tStr[12]                         = "Main_Thread";
	OSAL_tThreadID tid_mtx                = 0;
	OSAL_trThreadAttribute thrAtr_mtx     = {OSAL_NULL};

	/*Create a Semaphore
	To synchronize the OEDT Thread and Thread which runs the entry point 
	MutexThread_Main( Main Thread)*/
	if( OSAL_ERROR != OSAL_s32SemaphoreCreate( "MAINTHREAD_TO_OEDTTHREAD",&mtxSemMain_To_OEDT,0 ) )
	{
		/*Set Flag to High to show, semaphore got created*/
		u8SemaphoreFlag = 1;
	}

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandleglob,MUTEX_PAYLOAD ) )
	{
		/*Set the Mutex Flag*/
		u8MutexFlag = 1;

		/*Lock the Created Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexLock( mtxHandleglob,OSAL_C_TIMEOUT_FOREVER ) )
		{			
			/*Fill in thread attributes*/
			thrAtr_mtx.szName = (tString)tStr;
			/*Assign Thread Priority*/ 
			thrAtr_mtx.u32Priority  = PRIORITY_120;
			/*Assign Thread Stack Priority*/        
			thrAtr_mtx.s32StackSize = 2048;              
			/*Assign thread entry point*/
       	    thrAtr_mtx.pfEntry      = (OSAL_tpfThreadEntry)MutexThread_Main;      
			/*Assign thread arguement*/
      		thrAtr_mtx.pvArg        = OSAL_NULL;

			/*Spawn the Thread*/
			if( OSAL_ERROR != ( tid_mtx = OSAL_ThreadSpawn( &thrAtr_mtx ) ) )
			{
				/*Set the flag*/
				u8Flag = 1;
			}
			else
			{
				/*Set the error code*/
				u32Ret += 400;
			}	

			/*Unlock the Created Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandleglob ) )
			{
				/*Unlock failed*/
				u32Ret += 100;
			}
		}
		else
		{
			/*Mutex Lock failed*/
			u32Ret += 150;
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 200;
	}

	/*Delete the Main Thread*/
	if( u8Flag )
	{
		/*Wait for signal from Main Thread to OEDT Thread*/
		OSAL_s32SemaphoreWait( mtxSemMain_To_OEDT, OSAL_C_TIMEOUT_FOREVER );

		/*Delete the thread*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tid_mtx ) )
		{
			u32Ret += 1000;
		}
	}

	if( u8SemaphoreFlag )
	{
		/*Delete the Semaphore( Main Thread to OEDT Thread Semaphore )*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemMain_To_OEDT ) )
		{
			/*Delete the Semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "MAINTHREAD_TO_OEDTTHREAD" ) )
			{
				/*Cannot Delete the Semaphore*/
				u32Ret += 2500;
			}
		}
	}

	/*Delete the Mutex*/
	if( u8MutexFlag )
	{
		/*Close the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandleglob ) )
		{
			/*Delete the Mutex*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				u32Ret += 4000;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 5000;
		}
	}

	/*Check for Data*/
	if( u32Data != TOT_THREADS )
	{
		/*Data corrupted*/
		u32Ret += 10000;
	}

	/*Initialize the Global variables*/
	mtxHandleglob = 0;
	u32Data       = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32LockMTXHandleNull()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_018
* DESCRIPTION :    Try a lock on OSAL_NULL
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/	
tU32 u32LockMTXHandleNull( tVoid )
{
	/*Definitions*/
	tU32 u32Ret  			  = 0;

	/*Try call Mutex Lock on OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexLock( OSAL_NULL,OSAL_C_TIMEOUT_FOREVER ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Lock passed for OSAL_NULL*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32UnlockMTXHandleNull()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_019
* DESCRIPTION :    Try an Unlock on OSAL_NULL
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32UnlockMTXHandleNull( tVoid )
{
	/*Definitions*/
	tU32 u32Ret  			  = 0;

	/*Try call Mutex UnLock on OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32MutexUnLock( OSAL_NULL ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 100;
		}
	}
	else
	{
		/*Lock passed for OSAL_NULL*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReUnlockMTX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_020
* DESCRIPTION :    Try to ReUnlock on a Mutex
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32ReUnlockMTX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create a mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Lock the Created Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexLock( mtxHandle,OSAL_C_TIMEOUT_FOREVER )	)
		{
			/*Unlock the Mutex*/
			if( OSAL_ERROR != OSAL_s32MutexUnLock( mtxHandle ) )
			{
				/*ReUnlock the Mutex*/
				if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
				{
					if( OSAL_E_UNKNOWN != OSAL_u32ErrorCode( ) )
					{
						/*Wrong error code*/
						u32Ret += 200;
					}
				}
				else
				{
					/*ReUnlock Passed!*/
					u32Ret += 250;
				}
			}
			else
			{
				/*Mutex Unlock failed*/
				u32Ret += 300;
			}
		}
		else
		{
			/*Mutex could not be locked*/
			u32Ret += 500;
		}

		/*Mutex Close*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Mutex Delete*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Mutex Delete failed*/
				u32Ret += 600;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 700;
		}	
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32UnlockMTXBeforeLock()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_021
* DESCRIPTION :    Try Unlocking on a Mutex without locking the Mutex
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32UnlockMTXBeforeLock( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Unlock the Mutex*/
		if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
		{
			if( OSAL_E_UNKNOWN != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 200;
			}
		}
		else
		{
			/*Unlock Passed!*/
			u32Ret += 250;
		}

		/*Mutex Close*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			/*Mutex Delete*/
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Mutex Delete failed*/
				u32Ret += 600;
			}
		}
		else
		{
			/*Mutex Close failed*/
			u32Ret += 700;
		}
	}
	else
	{
		/*Mutex Create failed*/
		u32Ret += 10000;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DeleteMTXBeforeClose()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_022
* DESCRIPTION :    Should Pass.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32DeleteMTXBeforeClose( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Delete the Mutex - Should return success*/
		if( OSAL_ERROR != OSAL_s32MutexDelete( MUTEX_NAME ) )
		{
			/*Close the Mutex now and release resources*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				/*Mutex Close failed*/
				u32Ret += 8000;
			}
		}
		else
		{
			/*Mutex Delete failed*/
			u32Ret += 9000;
		}
	}
	else
	{
		/*Mutex Create failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32LockUnlockMTXAfterDelete()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_023
* DESCRIPTION :    Should Pass.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32LockUnlockMTXAfterDelete( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create the Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Delete the Mutex - Should return success*/
		if( OSAL_ERROR != OSAL_s32MutexDelete( MUTEX_NAME ) )
		{
			/*Lock the Mutex*/
			if( OSAL_ERROR != OSAL_s32MutexLock( mtxHandle,OSAL_C_TIMEOUT_FOREVER ) )
			{
				/*Unlock the Mutex Now*/
				if( OSAL_ERROR == OSAL_s32MutexUnLock( mtxHandle ) )
				{
					/*Mutex unlock failed*/
					u32Ret += 6000;
				}
			}
			else
			{
				/*Mutex lock failed*/
				u32Ret += 7000;
			}

			/*Close the Mutex now and release resources*/
			if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
			{
				/*Mutex Close failed*/
				u32Ret += 8000;
			}
		}
		else
		{
			/*Mutex Delete failed*/
			u32Ret += 9000;
		}
	}
	else
	{
		/*Mutex Create failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenCounterCheckMTX()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_024
* DESCRIPTION :    Mutex Create Valid, Mutex Open Valid, Mutex Close Valid, 
				   Mutex Close Valid,Mutex Close Once Again( Should fail ),
				   Mutex Delete.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 27,2007
*******************************************************************************/
tU32 u32OpenCounterCheckMTX( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			  = 0;
	OSAL_tMtxHandle mtxHandle = 0;

	/*Create a Mutex*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Open the Mutex now*/
		if( OSAL_ERROR != OSAL_s32MutexOpen( MUTEX_NAME, &mtxHandle ) )
		{
			/*Close the Mutex now*/
			if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
			{
				/*Close the Mutex once again*/
				if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
				{
					/*Close the Mutex a third time - Should fail here*/
					if( OSAL_ERROR == OSAL_s32MutexClose( mtxHandle ) )
					{
						/*Check the error code*/
						if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
						{
							u32Ret += 7000;
						}
					}
					else
					{
						/*Mutex Close passed! - Should not*/
						u32Ret += 7500;
					}
					
					/*Remove the mutex from the system*/
					if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME )	)
					{
						u32Ret += 7600;
					}	
				}
				else
				{
					/*Mutex close failed for the second time*/
					u32Ret += 8000;

					/*Cannot Shutdown as, Mutex Close failed*/
				} 
			}
			else
			{
				/*Mutex Close failed*/
				u32Ret += 8500;

				/*Cannot Shutdown as, Mutex Close failed!*/
			}
		}
		else
		{
			/*Mutex Open failed*/
			u32Ret += 9000;

			/*Mutex Open failed - Initiate shutdown*/
			if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
			{
				if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME )	)
				{
					/*Mutex Delete failed*/
					u32Ret += 9500;
				}
			}
			else
			{
				/*Mutex close failed*/
				u32Ret += 9600;
			}
		}
	}
	else
	{
		/*Mutex Creation failed*/
		u32Ret += 10000;
	}

	/*Return the error code*/

	return u32Ret;
}
/*Thread OwnerThread holds ownership of the mutex*/
tVoid OwnerThread(const tVoid * ptr )
{
	

	/*Own the Mutex*/
	if( OSAL_ERROR == OSAL_s32MutexLock( *( OSAL_tMtxHandle * )ptr,OSAL_C_TIMEOUT_FOREVER ) )
	{
		
		/*To avoid main thread from waiting forever*/
		OSAL_s32SemaphorePost( mtxSemSub_To_Main );
	}
	else
	{
		/*Post the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphorePost( mtxSemSub_To_Main ) )
		{
		/*Do nothing*/ 		
		}
	}

	/*Wait for signal from the main thread*/
	if( OSAL_ERROR != OSAL_s32SemaphoreWait( mtxSemMain_To_Sub,OSAL_C_TIMEOUT_FOREVER ) )
	{
		/*Unlock the Mutex now*/
		if( OSAL_ERROR == OSAL_s32MutexUnLock( *( OSAL_tMtxHandle * )ptr ) )
		{
		/*Do nothing*/
		}	
	}
	else
	{
	 /*Do nothing*/	
	}

	/*Post the Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphorePost( mtxSemSub_To_Main ) )
	{
	/*Do nothing*/
	}

	/*Wait for Thread Delete*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   u32LockUnlockMTXDiffThread()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MTX_025
* DESCRIPTION :    Create a Mutex in the Main Thread.
				   Lock the Mutex in a Sub Thread.
				   Try to Unlock the Mutex in the Main Thread
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 23,2008
*******************************************************************************/
tU32 u32LockUnlockMTXDiffThread( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			  			  = 0;
	OSAL_tMtxHandle mtxHandle 			  = 0;
	OSAL_tThreadID tid_mtx                = 0;
	OSAL_trThreadAttribute thrAtr_mtx     = {OSAL_NULL};
	
	/*Create a Semaphore - Use the semaphore as a Binary Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "BINARY_SEMAPHORE",&mtxSemSub_To_Main,0 ) )
	{
		/*Return Immediately*/
		return 10000;
	}
	/*Create a second Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "BINARY_SEMAPHORE_",&mtxSemMain_To_Sub,0 ) )
	{
		/*Close the Semaphore with the name BINARY_SEMAPHORE*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemSub_To_Main ) )
		{
			/*Delete the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "BINARY_SEMAPHORE" ) )
			{
			/*Do nothing*/
			}
		}
		else
		{
		/*Do nothing*/
		}

		/*Return immediately*/
		return 15000;
	}

	/*Create a Mutex within the system*/
	if( OSAL_ERROR != OSAL_s32MutexCreate( MUTEX_NAME,&mtxHandle,MUTEX_PAYLOAD ) )
	{
		/*Fill in thread attributes*/
		thrAtr_mtx.szName 		= "Priority_Inversion_Test";
		thrAtr_mtx.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_LOWEST;
		thrAtr_mtx.s32StackSize = 2048;              
       	thrAtr_mtx.pfEntry      = (OSAL_tpfThreadEntry)OwnerThread;      
        thrAtr_mtx.pvArg        = &mtxHandle;

		/*Spawn a Thread*/
		if( OSAL_ERROR != ( tid_mtx = OSAL_ThreadSpawn( &thrAtr_mtx ) ) )
		{
			 if( OSAL_ERROR != OSAL_s32SemaphoreWait( mtxSemSub_To_Main,OSAL_C_TIMEOUT_FOREVER ) )
			 {
				/*Unlock the Mutex*/
				if( OSAL_ERROR != OSAL_s32MutexUnLock( mtxHandle ) )
				{
					/*Mutex unlock should not succeed as ownership is with the
					OwnerThread*/
					u32Ret += 500;
				}
				else
				{
				  /*Do nothing*/	
				}
			 }
			 else
			 {
				/*Set error code*/
				u32Ret += 1000;
			 }

			 /*Post a message to the main thread*/
			 if( OSAL_ERROR == OSAL_s32SemaphorePost( mtxSemMain_To_Sub ) )
			 {
			 /*Do nothing*/
			 }

			/*Wait for a response from the Sub Thread*/
			if( OSAL_ERROR != OSAL_s32SemaphoreWait( mtxSemSub_To_Main,OSAL_C_TIMEOUT_FOREVER ) )
			{
				if( OSAL_ERROR == OSAL_s32ThreadDelete( tid_mtx ) )
				{
				/*Do nothing*/
				}
			}
		}
		else
		{
			 /*Update the error code*/
			 u32Ret += 2000;
		}

		/*Close and Delete the Mutex*/
		if( OSAL_ERROR != OSAL_s32MutexClose( mtxHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32MutexDelete( MUTEX_NAME ) )
			{
				/*Update the error code*/
				u32Ret += 2300;
			}
		}
		else
		{
			/*Update the error code*/
			u32Ret += 2500;
		}
	}
	else
	{
		/*Close the Semaphore named BINARY_SEMAPHORE*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemSub_To_Main ) )
		{
			/*Delete the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "BINARY_SEMAPHORE" ) )
			{
			/*Do nothing*/
			}
		}
		else
		{
		   /*Do nothing*/	
		}

		/*Close the Semaphore named BINARY_SEMAPHORE_*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemMain_To_Sub ) )
		{
			/*Delete the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "BINARY_SEMAPHORE_" ) )
			{
			/*Do nothing*/
			}
		}
		else
		{
		/*Do nothing*/
		}
		
		/*Return Immediately*/
		return 20000;
	}

	/*Close and Delete the OSAL Resources*/

	/*Close the Semaphore named BINARY_SEMAPHORE*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemSub_To_Main ) )
	{
		/*Delete the semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "BINARY_SEMAPHORE" ) )
		{
			u32Ret += 4500;
		}
	}
	else
	{
			u32Ret +=5000;
	}

	/*Close the Semaphore named BINARY_SEMAPHORE_*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( mtxSemMain_To_Sub ) )
	{
		/*Delete the semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "BINARY_SEMAPHORE_" ) )
		{
			 u32Ret += 6000;	
		}
	}
	else
	{
		u32Ret += 7000;
	}

	/*Return the error code*/
	return u32Ret;
}

