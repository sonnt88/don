/******************************************************************************
 *FILE         : oedt_osalcore_MQ_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the MESSAGE QUEUE OSAL API.
 *               
 *AUTHOR       : Anoop Chandran (RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *                          Initial version
 *****************************************************************************/


/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_osalcore_MQ_common_TestFuncs.h"
#include "oedt_osalcore_MQ_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
extern OSAL_tEventMask    gevMask;


/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

tU32 u32Tr1Ret;
tU32 u32Tr2Ret;
tU32 u32Tr1NtRet;
tU32 u32Tr2NtRet;
tU32 u32Tr3NtRet;
OSAL_tEventHandle MQEveHandle1;
OSAL_tEventHandle MQEveHandle2;
OSAL_tEventHandle MQEveHandle2_cb;
OSAL_tEventHandle MQEveHandle3_cb;
OSAL_tMQueueHandle MQHandle = 0;

OSAL_tSemHandle baseSem 									  	   = 0;
MessageQueueTableEntry mesgque_StressList[MAX_MESSAGE_QUEUES];
tU32 u32GlobMsgCounter										  	   = 0;
OSAL_trThreadAttribute msgqpost_ThreadAttr[NO_OF_POST_THREADS];
OSAL_trThreadAttribute msgqwait_ThreadAttr[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute msgqclose_ThreadAttr[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute msgqcreate_ThreadAttr[NO_OF_CREATE_THREADS];
OSAL_tThreadID msgqpost_ThreadID[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID msgqwait_ThreadID[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID msgqclose_ThreadID[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID msgqcreate_ThreadID[NO_OF_CREATE_THREADS]           = {OSAL_NULL};


/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/

void u32MQPostThread(void)
{
   OSAL_s32MessageQueuePost(MQHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 );
}

/******************************************************************************
 *FUNCTION		:vMQCallBack()
 *
 *DESCRIPTION	:
 *				 a)callback for notification
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/

tVoid vMQCallBack2(void *Arg)
{
   (void)Arg;
   OEDT_HelperPrintf( TR_LEVEL_USER_1, "DEBUG CALLBACK2: message notifier callback #2 was called" );
   
   if ( OSAL_ERROR == OSAL_s32EventPost ( MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_OR )) 
   {
     OEDT_HelperPrintf ( TR_LEVEL_FATAL, "ERROR CALLBACK2: sending event 'message notification' (callback #2 -> thread 2) failed" );
   }   
   OEDT_HelperPrintf( TR_LEVEL_USER_1, "DEBUG CALLBACK2: event 'message notification' was posted to thread 2" );   
}

tVoid vMQCallBack3(void *Arg)
{
   (void)Arg;
   OEDT_HelperPrintf( TR_LEVEL_USER_1, "DEBUG CALLBACK3: message notifier callback #3 was called" );
   
   if ( OSAL_ERROR == OSAL_s32EventPost ( MQEveHandle3_cb, MQ_EVE1, OSAL_EN_EVENTMASK_OR )) 
   {
     OEDT_HelperPrintf ( TR_LEVEL_FATAL, "ERROR CALLBACK3: sending event 'message notification' (callback #3 -> thread 3) failed" );
   }   
   OEDT_HelperPrintf( TR_LEVEL_USER_1, "DEBUG CALLBACK3: event 'message notification' was posted to thread 3" );   
}

/******************************************************************************
 *FUNCTION		:u32MQCreateStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQCreateStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_INVALIDVALUE: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 2;
		 break;
	  case OSAL_E_NOSPACE:
		 u32Ret = 3;
		 break;
	  case OSAL_E_ALREADYEXISTS:
		 u32Ret = 4;
		 break;
	  case OSAL_E_NAMETOOLONG:
		 u32Ret = 5;
		 break;
	  default:
		 u32Ret = 6;
   }//end of switch( u32status )
   return u32Ret; 
} 
/******************************************************************************
 *FUNCTION		:u32MQOpenStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQOpenStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_NOPERMISSION: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 1;
		 break;
	  case OSAL_E_BUSY:
		 u32Ret = 2;
		 break;
	  case OSAL_E_DOESNOTEXIST:
		 u32Ret = 3;
		 break;
	  case OSAL_E_UNKNOWN:
		 u32Ret = 4;
		 break;
	  default:
		 u32Ret = 5;
   }//end of switch( u32status )

   return u32Ret; 
} 

/******************************************************************************
 *FUNCTION		:u32MQCloseStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQCloseStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status = OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_UNKNOWN: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 2;
		 break;
	  default:
		 u32Ret = 3;
   }//end of switch( u32status )

   return u32Ret; 
} 
/******************************************************************************
 *FUNCTION		:u32MQDeleteStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQDeleteStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status =  OSAL_u32ErrorCode();
   switch( u32status )
   {  
      case OSAL_E_UNKNOWN: 
	     u32Ret = 1;
		 break;
	  case OSAL_E_DOESNOTEXIST:
		 u32Ret = 2;
		 break;
	  case OSAL_E_BUSY:
		 u32Ret = 3;
		 break;
	  case OSAL_E_INVALIDVALUE:
		 u32Ret = 4;
		 break;
	  default:
		 u32Ret = 5;
   }//end of switch( u32status )

   return u32Ret; 
} 

/******************************************************************************
 *FUNCTION		:u32MQPostStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQPostStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status =  OSAL_u32ErrorCode();
   switch( u32status )
   {  
		    case OSAL_E_MSGTOOLONG:  
			   u32Ret = 1;
			   break;
			case OSAL_E_QUEUEFULL:
			   u32Ret = 2;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret = 3;
			   break;
			case OSAL_E_NOPERMISSION:
			   u32Ret = 4;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 5;
			   break;
			case OSAL_E_BADFILEDESCRIPTOR:
			   u32Ret = 6;
			   break;
			default:
			   u32Ret = 7;
   }//end of switch( u32status )

   return u32Ret; 
} 
/******************************************************************************
 *FUNCTION		:u32MQWaitStatus()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tU32 u32MQWaitStatus( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   /* check the error status returned */
   u32status =  OSAL_u32ErrorCode();
   switch( u32status )
   {  
		    case OSAL_E_UNKNOWN:  
			   u32Ret = 1;
			   break;
			case OSAL_E_TIMEOUT:
			   u32Ret = 2;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 3;
			   break;
			default:
			   u32Ret = 4;
   }//end of switch( u32status )

   return u32Ret; 
} 

/******************************************************************************
 *FUNCTION		:u32MQThread1()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32MQThread1(tPVoid pvArg)
{
   tS32 s32Count				           = 0;
   OSAL_tMQueueHandle mqHandle             = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];

   u32Tr1Ret = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   OSAL_pvMemorySet(MQ_msg,0,sizeof(MQ_msg));
   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
   	     OSAL_ERROR 
   	     != 
   	     OSAL_s32MessageQueueOpen
   	     ( 
   	        MESSAGEQUEUE_NAME,
   	        OSAL_EN_WRITEONLY,
   	        &mqHandle 
   	     ) 
   	  ) 
   {
	  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
	  {
         OSAL_s32PrintFormat
         ( 
            MQ_msg[s32Count], 
            "MQ_MSG%d",
            MQ_PRIO2 
         );
	  
	     // Post a message into the message queue 
		 if ( 
		       OSAL_ERROR 
		       == 
		       OSAL_s32MessageQueuePost
		       ( 
		          mqHandle,
		          (tPCU8)MQ_msg[s32Count],
				  MAX_LEN,
				  (tU32)s32Count 
			   ) 
			) 
		 {

			u32Tr1Ret += (10 + u32MQPostStatus ()) ;

		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
		 OEDT_HelperPrintf
		 (  
		    TR_LEVEL_USER_1, 
		    "Message Send = %d\t%s\n", 
		    s32Count, 
		    MQ_msg[s32Count] 
		 );

	  }//end of  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
    
	  if (
	        OSAL_ERROR 
	        == 
	        OSAL_s32MessageQueueClose
	        ( 
	           mqHandle 
	        ) 
	     )
	  {
		 
		 /* Check the error status */
		 u32Tr1Ret += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {

	  u32Tr1Ret += (30 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if (
         OSAL_ERROR 
         == 
         OSAL_s32EventPost
         ( 
            MQEveHandle1,
            MQ_EVE1,
            OSAL_EN_EVENTMASK_OR 
         ) 
      ) 
   {
      u32Tr1Ret += 100;
   }
//   OSAL_s32ThreadWait(800);
   OSAL_vThreadExit();
}

/******************************************************************************
 *FUNCTION		:u32MQThread2()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32MQThread2(tPVoid pvArg)
{
   tS32 s32Count				           = 0;
   OSAL_tMQueueHandle mqHandle             = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];

   u32Tr2Ret = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   OSAL_pvMemorySet(MQ_msg,0,sizeof(MQ_msg));

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
		 OSAL_ERROR 
		 != 
		 OSAL_s32MessageQueueOpen
		 ( 
		  	MESSAGEQUEUE_NAME, 
			OSAL_EN_READONLY,
			&mqHandle
		 )
	  ) 
   {
	  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
	  {
  
	     /*Post a message into the message queue */
	     if (
			   MQ_WAIT_RETURN_ERROR
			   == 
			   OSAL_s32MessageQueueWait
			   (
			      mqHandle,
			      (tPU8)MQ_msg[ s32Count ],
			      MAX_LEN,
			      OSAL_NULL,
			      (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER
			   )
		    )
	     {
		    u32Tr2Ret += (10 + u32MQWaitStatus ()) ;

	     }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 
		 OEDT_HelperPrintf
		 ( 
		    TR_LEVEL_USER_1, 
		    "Message Received = %d\t%s\n", 
		    s32Count, 
		    MQ_msg[ s32Count ] 
		 );
	  
	  }//end of  for( ; MQ_COUNT_MAX > u32Count; ++u32Count )

	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueClose
			( 
			   mqHandle
			)
		 )
	  {
		 
		 /* Check the error status */
		 u32Tr2Ret += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {

	  u32Tr2Ret += (30 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32EventPost
		 ( 
		  	MQEveHandle1,
			MQ_EVE2,
			OSAL_EN_EVENTMASK_OR
		 )
	  ) 
   {
      u32Tr2Ret += 100;
   }
//   OSAL_s32ThreadWait(800);
   OSAL_vThreadExit();


}
/******************************************************************************
 *FUNCTION		:u32MQThreadNotify1()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32MQThreadNotify1(tPVoid pvArg)
{
    OSAL_tMQueueHandle mqHandle = 0;
  //  tChar MQ_msg[MQ_HARRAY] = MQ_MSG;

    u32Tr1NtRet = 0;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

    // Open the message queue  in OSAL_EN_WRITEONLY mode

    if(OSAL_ERROR != OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle))
    {

        OSAL_s32ThreadWait(2000);

        // Post a message into the message queue
/*        if(OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_msg, MAX_LEN, MQ_PRIO2))
        {   // Check the error status
            u32Tr1NtRet = (10 + u32MQPostStatus());
        }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD1: Message Send = %s", MQ_msg);*/

        // Close the message queue

        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {   /* Check the error status */
            u32Tr1NtRet += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

    }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

    else
    {   /* Check the error status */
        u32Tr1NtRet += (30 + u32MQOpenStatus());
    }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD1: event 'ready' & 'success' was sent to main thread");
    if(OSAL_ERROR == OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE1 | MQ_DONE_EVE1, OSAL_EN_EVENTMASK_OR))
    {
        u32Tr1NtRet += 100;
    }

    OEDT_HelperPrintf(TR_LEVEL_ERRORS, "DEBUG THREAD1: error sum of thread #1 is '%i'", u32Tr1NtRet);
    OSAL_vThreadExit();
}

/******************************************************************************
 *FUNCTION		:u32MQThreadNotify2()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32MQThreadNotify2(tPVoid pvArg)
{
    tU32 u32status = 0;
    OSAL_tMQueueHandle mqHandle = 0;
    u32Tr2NtRet = 0;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: thread started successfully");

    /*Open the message queue  in OSAL_EN_WRITEONLY mode*/
    if(OSAL_ERROR != OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle))
    {
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: message queue has been opened successfully");
        if(OSAL_ERROR == OSAL_s32EventCreate(MQ_EVE_NAME2_CB, &MQEveHandle2_cb))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 100;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: event 'message notification' was created successfully");
        }

        /*Register a callback for notification*/
        if(OSAL_ERROR == OSAL_s32MessageQueueNotify(mqHandle, (OSAL_tpfCallback)vMQCallBack2, OSAL_NULL ))
        {   /* Check the error status */
            u32status = OSAL_u32ErrorCode();
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32MessageQueueNotify() failed with error '%i'", u32status);
            switch(u32status)
            {
            case OSAL_E_BUSY:
                u32Tr2NtRet = 1;
                break;
            case OSAL_E_UNKNOWN:
                u32Tr2NtRet = 2;
                break;
            case OSAL_E_INVALIDVALUE:
                u32Tr2NtRet = 3;
                break;
            default:
                u32Tr2NtRet = 4;
            }//end of switch( u32status )
        }// end of if (OSAL_ERROR == OSAL_s32MessageQueueNotify())
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: message queue notifier callback #2 was activated");

        if(OSAL_ERROR == OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE2, OSAL_EN_EVENTMASK_OR))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: event post of Thread 2 -> Main failed");
            u32Tr2NtRet += 200;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: event 'ready' was sent to main thread");
        }

        if(OSAL_ERROR == OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_NOBLOCKING | 10000, &gevMask))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 300;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: received event 'message notification' from callback #2");
        }

        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle2_cb))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventClose() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 400;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: event 'message notification' was closed successfullly");
        }

        if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME2_CB))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventDelete() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 500;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: event 'message notification' was deleted successfullly");
        }

        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {   /* Check the error status */
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32MessageQueueClose() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: message queue was closed");
        }
    }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())
    else
    {   /* Check the error status */
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32MessageQueueOpen() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += (20 + u32MQOpenStatus());
    }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())

    if(OSAL_ERROR == OSAL_s32EventPost(MQEveHandle2, MQ_DONE_EVE2, OSAL_EN_EVENTMASK_OR))
    {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventPost() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += 800;
    }
    else
    {
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: event 'success' was posted to main thread");
    }

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: error sum of thread #2 is '%i'", u32Tr2NtRet);
    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD2: end of thread #2");
    OSAL_vThreadExit();
}

/******************************************************************************
 *FUNCTION		:u32MQThreadNotify3()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32MQThreadNotify3(tPVoid pvArg)
{
    tU32 u32status = 0;
    OSAL_tMQueueHandle mqHandle = 0;
    u32Tr3NtRet = 0;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: thread started successfully");

    /*Open the message queue  in OSAL_EN_WRITEONLY mode*/
    if(OSAL_ERROR != OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle))
    {
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: message queue has been opened successfully");

        if(OSAL_ERROR == OSAL_s32EventCreate(MQ_EVE_NAME3_CB, &MQEveHandle3_cb))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 100;
        }
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: event 'message notification' was created successfully");

        // we need some delay because thread 2 should be start the notifier
        OSAL_s32ThreadWait(500);

        /*Post a message into the message queue */
        if(OSAL_ERROR == OSAL_s32MessageQueueNotify(mqHandle, (OSAL_tpfCallback)vMQCallBack3, OSAL_NULL ))
        {
            // Check the error status
            u32status = OSAL_u32ErrorCode();
            if(u32status != OSAL_E_BUSY)
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32MessageQueueNotify() failed with unexpected error '%i'", u32status);
                u32Tr2NtRet = 1;
            }
            else
            {
                OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: message queue notifier callback #3 failed as expected, got a busy signal");
            }
        }// end of if (OSAL_ERROR == OSAL_s32MessageQueueNotify())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: message queue notifier callback #3 creation should fail, we didn't expect an ok");
            u32Tr2NtRet = 2;
        }

        if(OSAL_ERROR == OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE3, OSAL_EN_EVENTMASK_OR))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: event post of Thread 3 -> Main failed");
            u32Tr2NtRet += 200;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: event 'ready' was sent to main thread");
        }

        if(OSAL_ERROR == OSAL_s32EventWait(MQEveHandle3_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_NOBLOCKING | 1000, &gevMask))
        {
            u32status = OSAL_u32ErrorCode();
            if(u32status == OSAL_E_TIMEOUT)
            {
                OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: waiting for event 'message notification' failed with expected timeout");
            }
            else
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERRROR THREAD3: OSAL_s32EventWait() failed with unexpected error '%i'", u32status);
                u32Tr2NtRet += 350;
            }
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERRROR THREAD3: received event 'message notification' from callback #3");
            u32Tr2NtRet += 300;
        }

        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle3_cb))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32EventClose() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 400;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: event 'message notification' was closed successfullly");
        }

        if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME3_CB))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32EventDelete() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += 500;
        }
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: event 'message notification' was deleted successfullly");
        }

        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {   /* Check the error status */
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32MessageQueueClose() failed with error '%i'", OSAL_u32ErrorCode());
            u32Tr2NtRet += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: message queue was closed");
        }
    }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())
    else
    {   /* Check the error status */
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32MessageQueueOpen() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += (20 + u32MQOpenStatus());
    }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())

    OSAL_s32ThreadWait(1000);

    if(OSAL_ERROR == OSAL_s32EventPost(MQEveHandle2, MQ_DONE_EVE3, OSAL_EN_EVENTMASK_OR))
    {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD3: OSAL_s32EventPost() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += 800;
    }
    else
    {
        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: event 'success' was posted to main thread");
    }

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: error sum of thread #3 is '%i'", u32Tr2NtRet);
    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG THREAD3: end of thread #3");
    OSAL_vThreadExit();
}

/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tS32 s32RandomValGen( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Return the Positive Random Number*/
	return s32Rand;
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandTime()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Wait for Random time frame
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vWaitRandTime( tU32 u32TimeModulus )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32TimeModulus +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}

/*****************************************************************************
* FUNCTION    :	   vChangeThreadPrio()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Change Thread Priorities Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vChangeThreadPrio( tVoid )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectMsgQueIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Queue Indexes Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tU8 u8SelectMsgQueIndexRandom( tVoid )
{
	/*Declarations*/
	tU8  u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the Message Queue Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_MESSAGE_QUEUES );
	/*Return the Message Queue Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   u8SelectMessagePriority()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Priority for a message to
  				   be sent to a Message Queue.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
				   Updated By Tinoy Mathews( RBIN/ECM1 ) 20 Dec,2007
*******************************************************************************/
tU32 u32SelectMessagePriority( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = RAND_MAX;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Return the Message Priority*/
	return (tU32)( s32Rand%((tS32)MQ_PRIO7+1) );
}

#if 0 /*rav8kor comment*/
/*****************************************************************************
* FUNCTION    :	   vRandomizeString()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Randomize the string
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vRandomizeString( tChar *ptr,tU32 u32len )
{
	/*Declarations*/
	tU32 u32Index = 0;
	tU8  u8Rand;


	/*Encrypt the String*/
	while( u32Index < u32len )
	{
		u8Rand = (tU8)( s32RandomValGen( ) % 0x000000FF );
		
		/*Randomize the string*/
		*( ptr + u32Index ) = u8Rand;

	    /*Increment the index*/
	    u32Index++;
	}
}
#endif

/******************************************************************************
* FUNCTION    :	   Post_Message()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Message Thread
				   - To continually post Message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid Post_Message( tVoid *ptr )
{
	/*Declarations*/
	tU8 u8Index             = 0xFF;
	tU32 u32MessagePriority = 0xFFFFFFFF;
	tChar aBuf[MAX_LEN]		= "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vChangeThreadPrio( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectMsgQueIndexRandom( );

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		#if 0 /*rav8kor comment*/
		/*Randomize the String*/
		vRandomizeString( aBuf,MAX_LEN );
		#endif

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen(
								  		mesgque_StressList[u8Index].coszName,
								  		mesgque_StressList[u8Index].enAccess,
								  		&mesgque_StressList[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueuePost( mesgque_StressList[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );

			/*Close the Message Queue*/
			if( u8SelectMsgQueIndexRandom( ) < MAX_MESSAGE_QUEUES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressList[u8Index].phMQ );
				vWaitRandTime( FIVE_SECONDS );
			}
			else
			{
				vWaitRandTime( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressList[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/
}

/******************************************************************************
* FUNCTION    :	   Wait_Message()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Message Thread
				   - To continually Wait for
				   message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid Wait_Message( tVoid *ptr )
{
   	/*Declarations*/
	tChar aBuf[MAX_LEN];
	tU8 u8Index;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
	   /*Change Thread Priority Randomly*/
		vChangeThreadPrio( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectMsgQueIndexRandom( );

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen(
								  		mesgque_StressList[u8Index].coszName,
								  		mesgque_StressList[u8Index].enAccess,
								  		&mesgque_StressList[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueueWait( mesgque_StressList[u8Index].phMQ,
									  (tPU8)aBuf,
									  MAX_LEN,
									  OSAL_NULL,
									  OSAL_C_TIMEOUT_FOREVER );

			/*Close the Message Queue*/
			if( u8SelectMsgQueIndexRandom( ) < MAX_MESSAGE_QUEUES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressList[u8Index].phMQ );
				vWaitRandTime( FIVE_SECONDS );
			}
			else
			{
				vWaitRandTime( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressList[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/
}

/******************************************************************************
* FUNCTION    :	   Close_Message()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Close Message Thread
				   - To continually Close
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid Close_Message( tVoid *ptr )
{
   	/*Declarations*/
	tU8 u8Index;
	tU8 u8Count = 0;
	tChar aBuf[MAX_LEN]		 = "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/
	tU32  u32MessagePriority = 0xFFFFFFFF;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vChangeThreadPrio( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectMsgQueIndexRandom( );

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		/*Sync*/
		OSAL_s32SemaphoreWait( baseSem, OSAL_C_TIMEOUT_FOREVER );

		if( mesgque_StressList[u8Index].phMQ != OSAL_C_INVALID_HANDLE )
		{
			/*Sync*/
			OSAL_s32SemaphorePost( baseSem );

			/*Delete the message queue*/
			if( OSAL_s32MessageQueueDelete( mesgque_StressList[u8Index].coszName ) == OSAL_OK )
			{
				/*Random Delay*/
				vWaitRandTime( FIVE_SECONDS );

				/*Fire 100 posts*/
				while( u8Count < 100 )
				{
					/*Post the message into the message queue*/
					OSAL_s32MessageQueuePost( mesgque_StressList[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );
					u8Count++;
				}

				/*Reset the count*/
				u8Count = 0;

				/*Delete the message Queue*/
				OSAL_s32MessageQueueClose( mesgque_StressList[u8Index].phMQ );

				/*Reset the handle at the index*/
				mesgque_StressList[u8Index].phMQ = OSAL_C_INVALID_HANDLE;
			}
		}
		else
		{
		   OSAL_s32SemaphorePost( baseSem );
		}
	}
}

/******************************************************************************
* FUNCTION    :	   Create_Message()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Create Message Thread
				   - To continually Create
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid Create_Message( tVoid *ptr )
{
	 /*Declarations*/
	 tU8 u8Index;
     OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 for(;;)
   	 {
        /*Change Thread Priority Randomly*/
		vChangeThreadPrio( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectMsgQueIndexRandom( );

      	/*Sync*/
		OSAL_s32SemaphoreWait( baseSem, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (mesgque_StressList[u8Index].phMQ == OSAL_C_INVALID_HANDLE)
      	{
         	/*Sync*/
			OSAL_s32SemaphorePost( baseSem );

         	/*Create the Message Queue*/
         	OSAL_s32MessageQueueCreate(
										mesgque_StressList[u8Index].coszName,
										mesgque_StressList[u8Index].u32MaxMessages,
										mesgque_StressList[u8Index].u32MaxLength,
										mesgque_StressList[u8Index].enAccess,
										&mesgque_StressList[u8Index].phMQ
									  );
      	}
		/*Random Delay*/
		vWaitRandTime( FIVE_SECONDS );
     }
}

/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Interface to all Threads
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vEntryFunction(
  							void (*ThreadEntry) (void *),
					  		const tC8 *threadName,
                      		tU8 u8NoOfThreads,
                      		OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[]
                    		)
{
	/*Declarations*/
	tU8  u8Index                        = 0;
	tChar aBuf[TOT_THREADS][MAX_LENGTH];

    OSAL_pvMemorySet(aBuf,0,sizeof(aBuf));

	/*Spawn the threads*/
	while( u8Index < u8NoOfThreads )
	{
		/*Catch the thread name into buffer element*/
		OSAL_s32PrintFormat( aBuf[u32GlobMsgCounter],"%s_%d",threadName,u8Index );

		/*Fill in the thread attributes*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;
		tr_Attr[u8Index].szName       = aBuf[u32GlobMsgCounter];
		tr_Attr[u8Index].pfEntry      = ThreadEntry;

		/*Spawn the thread*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			//u32ECode += u8Index;
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobMsgCounter++;
	}
}


void Thread_MQ_Memory(tVoid* pvArg)
{
   tS32 s32ReturnValue = 0;
   OSAL_tMQueueHandle handle_2 = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };

   if ((OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_READWRITE, &handle_2)) != OSAL_ERROR)
   {
      if (OSAL_s32MessageQueuePost(handle_2, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
      {
         if (OSAL_s32MessageQueueWait(handle_2, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueClose(handle_2) != OSAL_ERROR)
            {
               // test ok
            }
            else
               s32ReturnValue =5000;
         }
         else
            s32ReturnValue = 3000;
      }
      else
         s32ReturnValue = 2000;
   }
   else
      s32ReturnValue = 1000;

   // thread return value
   *(tS32 *)pvArg = s32ReturnValue;
}

