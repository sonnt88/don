/******************************************************************************
 *FILE         : oedt_osalcore_SM_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the SEMAPHORE OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *          ver1.7   - 11-08-15
 *          CFG3-1351 : Oedt fix
				ver1.6   - 30-04-2009
                lint info removal
				rav8kor
  *             
  				ver1.5   - 22-04-2009
                lint info removal
				sak9kor

 *				ver1.4	 - 14-04-2009
                warnings removal
				rav8kor

 				Version 1.3 , 29- 10- 2008
 *				Update : SEMTEXT
 *				- Anoop Chandran( RBIN/ECM1 )
 *
 *					
 *				Version 1.2 , 8- 1- 2008
 *				Added case declarations:
 *				u32NoBlockingSemWait,
 *				u32BlockingDurationSemWait,
 *				u32WaitPostSemAfterDel
 *				- Tinoy Mathews( RBIN/ECM1 )
 *				
 *
 *				Version 1.1 , 11- 12- 2007 
 *				Added defines for Stress Test Case, Added Stress Test
 *				Case Prototype
 *				- Tinoy Mathews( RBIN/ECM1 )
 *
 *				Version 1.0 , 26- 10- 2007
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_SM_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_SM_TESTFUNCS_HEADER

/*Defines*/
#define SEMAPHORE_START_VALUE 	      1
#define SEMAPHORE_START_VALUE_MAX     65535    
#define SEMAPHORE_NAME 			      "Semaphore"
#define GLOBAL_SEMAPHORE_START_VALUE  1
#define GLOBAL_SEMAPHORE_START_VALUE_ 3
#define GLOBAL_SEMAPHORE_NAME 	      "GSemaphore"
#define GLOBAL_SEMAPHORE_NAME_        "Mutex"
#define SEMAPHORE_NAME_MAX            "Semaphore with name equal to 31"
#define SEMAPHORE_NAME_EXCEEDS_MAX	  "Semaphore with name exceeding 31 characters"
#define SEMTEXT						  OSAL_C_STRING_DEVICE_FFS"/semtext.txt"
#define THIRTY_MILLISECONDS           30
#define ONE_SECOND                    1000
#undef 	TWO_SECONDS
#define TWO_SECONDS                   2000
#define THREE_SECONDS                 3000
#undef  FIVE_SECONDS
#define FIVE_SECONDS                  5000
#define STRESS_WAIT                   10000
#define SEMAPHORE_COUNT_INVALID       ((tS32)-1)

#undef 	INIT_VAL

#define INIT_VAL                      (tU8)0x00 // Binary : 0000 0000
#define VALID_VAL                     (tU8)0x07	// Binary : 0000 0111
#define CF1_MASK                      (tU8)0x01 // Binary : 0000 0001
#define CF2_MASK					  (tU8)0x02	// Binary : 0000 0010
#define CF3_MASK					  (tU8)0x04	// Binary : 0000 0100
#define MAX_SEMAPHORES                (tU8)25
#define NO_OF_POST_THREADS            10
#define NO_OF_WAIT_THREADS            15
#define NO_OF_CLOSE_THREADS           1
#define NO_OF_CREATE_THREADS          2

#undef  SRAND_MAX
#define SRAND_MAX                     (tU32)65535
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                      0x7FFFFFFF
#undef 	MAX_LENGTH
#define MAX_LENGTH                    256
#define TOT_THREADS                   (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)

#undef DEBUG_MODE
#define DEBUG_MODE                    0

/*Data Types*/
typedef struct SemaphoreTableEntry_
{
	OSAL_tSemHandle hSem;
	tChar hSemName[MAX_LENGTH];
	tBool hToBeDeleted;
}SemaphoreTableEntry;

/*Declarations*/
tU32  u32CreateCloseDelSemVal( tVoid );
tU32  u32CreateSemNameNULL( tVoid )	;
tU32  u32CreateSemHandleNULL( tVoid );
tU32  u32CreateCloseDelSemNameMAX( tVoid );
tU32  u32CreateSemWithNameExceedsMAX( tVoid );
tU32  u32CreateTwoSemSameName( tVoid );
tU32  u32DelSemWithNULL( tVoid );
tU32  u32DelSemWithNonExistingName( tVoid );
tU32  u32DelSemCurrentlyUsed( tVoid );
tU32  u32OpenSemWithNameNULL( tVoid );
tU32  u32OpenSemWithHandleNULL( tVoid );
tU32  u32OpenSemWithNameExceedsMAX( tVoid );
tU32  u32OpenSemWithNonExistingName( tVoid );
tU32  u32CloseSemWithInvalidHandle( tVoid );
tU32  u32WaitPostSemValid( tVoid );
tU32  u32WaitSemWithInvalidHandle( tVoid );
tU32  u32ReleaseSemWithInvalidHandle( tVoid );
tU32  u32GetRegularSemValue( tVoid );
tU32  u32GetSemValueInvalidHandle( tVoid );
tU32  u32WaitPostSemValidExceedsMAX( tVoid );
tU32  u32AccessSemMpleThreads( tVoid );

tU32 u32NoBlockingSemWait( tVoid );
tU32 u32BlockingDurationSemWait( tVoid );
tU32 u32WaitPostSemAfterDel( tVoid );

tU32 u32SemaphoreStressTest( tVoid );

#endif
