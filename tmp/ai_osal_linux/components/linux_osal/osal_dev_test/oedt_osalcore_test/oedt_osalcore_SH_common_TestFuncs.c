/******************************************************************************
 *FILE          :oedt_osalcore_SH_TestFuncs.c
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file implements the individual test cases for 
 *               the shared Memory OSAL_CORE APIs
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY      : 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *                          Initial Version.
 *****************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "oedt_osalcore_SH_TestFuncs.h"
#include "oedt_helper_funcs.h"


/*Global Variables*/
OSAL_tShMemHandle ghShared  	 = 0;
OSAL_tEventHandle gSHEventHandle = 0;
extern OSAL_tEventMask    gevMask;
/*Global Variables*/
extern tU32 u32SHSharedMemoryCreateStatus( tVoid );
extern tU32 u32SHSharedMemoryOpenStatus( tVoid );
extern tU32 u32SHSharedMemoryCloseStatus( tVoid );
	
/******************************************************************************
 *FUNCTION		:Thread_SH_0_127
 *DESCRIPTION	:Thread access and Fills the bytes from 0 to 127 of Shared
				 Memory with character 'a'.
 *PARAMETER		:Address of the thread return variable
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tVoid Thread_SH_0_127( tVoid * ptr )
{
	/*Definitions*/
	tVoid *shptr = OSAL_NULL;

	/*Map to the Shared Memory Start Address*/
	if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            							LENGTH_127,OFFSET_0 ) ) )
	{
		/*Set Memory*/
		OSAL_pvMemorySet( shptr,'a',LENGTH_127+1 );
	}
	else
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr = 100;
	}

	/*Post Event Pattern*/
	if( OSAL_ERROR == OSAL_s32EventPost( gSHEventHandle,PATTERN_THR1,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr += 200;
	}

	/*Wait for Thread Delete*/
	OSAL_s32ThreadWait( THREE_SECONDS );
}

/******************************************************************************
 *FUNCTION		:Thread_SH_128_255
 *DESCRIPTION	:Thread access and Fills the bytes from 128 to 255 of Shared
				 Memory with character 'b'.
 *PARAMETER		:Address of the thread return variable
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tVoid Thread_SH_128_255( tVoid * ptr )
{
	/*Definitions*/
	tVoid *shptr = OSAL_NULL;

	/*Map to the Shared Memory Address at offset of 128*/
	if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            							LENGTH_127,OFFSET_128 ) ) )
	{
		/*Set Memory*/
		OSAL_pvMemorySet( shptr,'b',LENGTH_127+1 );
	}
	else
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr = 100;
	}

	/*Post Event Pattern*/
	if( OSAL_ERROR == OSAL_s32EventPost( gSHEventHandle,PATTERN_THR2,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr += 200;
	}

	/*Wait for Thread Delete*/
	OSAL_s32ThreadWait( THREE_SECONDS );
}

/******************************************************************************
 *FUNCTION		:Thread_SH_256_383
 *DESCRIPTION	:Thread access and Fills the bytes from 256 to 383 of Shared
				 Memory with character 'c'.
 *PARAMETER		:Address of the thread return variable
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tVoid Thread_SH_256_383( tVoid * ptr )
{
	/*Definitions*/
	tVoid *shptr = OSAL_NULL;

	/*Map to the Shared Memory Start Address*/
	if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            						   LENGTH_127,OFFSET_256 ) ) )
	{
		/*Set Memory*/
		OSAL_pvMemorySet( shptr,'c',LENGTH_127+1 );
	}
	else
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr = 100;
	}

	/*Post Event Pattern*/
	if( OSAL_ERROR == OSAL_s32EventPost( gSHEventHandle,PATTERN_THR3,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr += 200;
	}

	/*Wait for Thread Delete*/
	OSAL_s32ThreadWait( THREE_SECONDS );
}

/******************************************************************************
 *FUNCTION		:Thread_SH_384_511
 *DESCRIPTION	:Thread access and Fills the bytes from 384 to 511 of Shared
				 Memory with character 'd'.
 *PARAMETER		:Address of the thread return variable
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/EDI3 )
*****************************************************************************/
tVoid Thread_SH_384_511( tVoid * ptr )
{
	/*Definitions*/
	tVoid *shptr = OSAL_NULL;

	/*Map to the Shared Memory Start Address*/
	if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            							LENGTH_127,OFFSET_384 ) ) )
	{
		/*Set Memory*/
		OSAL_pvMemorySet( shptr,'\0',LENGTH_127+1 );
		OSAL_pvMemorySet( shptr,'d',LENGTH_127 );
	}
	else
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr = 100;
	}

	/*Post Event Pattern*/
	if( OSAL_ERROR == OSAL_s32EventPost( gSHEventHandle,PATTERN_THR4,OSAL_EN_EVENTMASK_OR ) )
	{
		/*Set error in case of failure*/
		*( tU32 * )ptr += 200;
	}

	/*Wait for Thread Delete*/
	OSAL_s32ThreadWait( THREE_SECONDS );
}

/******************************************************************************
 *FUNCTION		:u32SHMemoryThreadAccess
 *DESCRIPTION	:Try to access Shared memory in different threads of control.
				 Should be able to address shared memory and access it.
 *PARAMETER		:none
 *RETURNVALUE	:tU32, status value in case of error
 *TEST CASE		:TU_OEDT_SH_016
 *HISTORY		:21.01.2008  Tinoy Mathews( RBIN/ECM1 )
*****************************************************************************/
tU32 u32SHMemoryThreadAccess( tVoid )
{
	/*DEFINITIONS*/

	/*General*/
	tU32 u32Ret                		= 0;
	/*Thread IDs*/
	OSAL_tThreadID tID_1            = 0;
	OSAL_tThreadID tID_2            = 0;
	OSAL_tThreadID tID_3            = 0;
	OSAL_tThreadID tID_4            = 0;
	/*Thread Attributes*/
	OSAL_trThreadAttribute trAtr_1  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_2  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_3  = {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_4  = {OSAL_NULL};
	/*Thread return error codes*/
	tU32 u32ThrEC_1            		= 0;
	tU32 u32ThrEC_2            		= 0;
	tU32 u32ThrEC_3            		= 0;
	tU32 u32ThrEC_4            		= 0;
	/*Shared memory Pointer*/
	tVoid * shptr                   = OSAL_NULL;
	gevMask = 0;
	/*INITIALIZATIONS*/

	/*Fill the Thread "Thread_SH_0_127" Attributes*/
	trAtr_1.szName                 	=  "Thread_SH_0_127";
	trAtr_1.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_1.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_1.pfEntry                	=  Thread_SH_0_127;
	trAtr_1.pvArg                  	=  &u32ThrEC_1;

	/*Fill the Thread "Thread_SH_128_255" Attributes*/
	trAtr_2.szName                 	=  "Thread_SH_128_255";
	trAtr_2.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_2.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_2.pfEntry                	=  Thread_SH_128_255;
	trAtr_2.pvArg                  	=  &u32ThrEC_2;

	/*Fill the Thread "Thread_SH_256_383" Attributes*/
	trAtr_3.szName                 	=  "Thread_SH_256_383";
	trAtr_3.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_3.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_3.pfEntry                	=  Thread_SH_256_383;
	trAtr_3.pvArg                  	=  &u32ThrEC_3;

	/*Fill the Thread "Thread_SH_384_511" Attributes*/
	trAtr_4.szName                 	=  "Thread_SH_384_511";
	trAtr_4.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_4.s32StackSize           	=  VALID_STACK_SIZE;
	trAtr_4.pfEntry                	=  Thread_SH_384_511;
	trAtr_4.pvArg                  	=  &u32ThrEC_4;
								 

	/*EVENT CREATE*/
	if( OSAL_ERROR != OSAL_s32EventCreate( "SH_MEM_EVENT",&gSHEventHandle ) )
	{
		/*CREATE OSAL RESOURCES*/

		/*Create the Shared Memory*/
		if( OSAL_ERROR != ( ghShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512 ) ) )
		{
			/*Spawn Thread Thread_SH_0_127*/
			if( OSAL_ERROR != ( tID_1 = OSAL_ThreadSpawn( &trAtr_1 ) ) )
			{
				/*Spawn Thread Thread_SH_128_255*/
				if( OSAL_ERROR != ( tID_2 = OSAL_ThreadSpawn( &trAtr_2 ) ) )
				{
					/*Spawn Thread Thread_SH_256_383*/
					if( OSAL_ERROR != ( tID_3 = OSAL_ThreadSpawn( &trAtr_3 ) ) )
					{
						/*Spawn Thread Thread_SH_384_511*/
						if( OSAL_ERROR != ( tID_4 = OSAL_ThreadSpawn( &trAtr_4 ) ) )
						{
						    /*4 THREAD EVENT WAIT*/
							if( OSAL_ERROR == OSAL_s32EventWait( gSHEventHandle, 
											 					 PATTERN_THR1|PATTERN_THR2|PATTERN_THR3|PATTERN_THR4,
		                   					 					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					 					 &gevMask ) )
							{
								u32Ret += 6000;
							}	
							/*Clear the event*/
							OSAL_s32EventPost
							( 
							gSHEventHandle,
							~(gevMask),
						   OSAL_EN_EVENTMASK_AND
						    );
	
						}
						else
						{
							/*3 THREAD EVENT WAIT ( 1,2,3 Passed )*/
							OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1|PATTERN_THR2|PATTERN_THR3,
		                   					 	OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					 	&gevMask );

							/*Thread Thread_SH_384_511 spawn failed*/
							u32Ret += 1000;
						}
						/*Clear the event*/
						OSAL_s32EventPost
						( 
						gSHEventHandle,
						~(gevMask),
					   OSAL_EN_EVENTMASK_AND
					    );

					}
					else
					{
						/*2 THREAD EVENT WAIT( 1,2 Passed )*/
						OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1|PATTERN_THR2,
		                   					OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   					&gevMask );

						/*Clear the event*/
						OSAL_s32EventPost
						( 
						gSHEventHandle,
						~(gevMask),
					   OSAL_EN_EVENTMASK_AND
					    );

						/*Thread Thread_SH_256_383 spawn failed*/
						u32Ret += 2000;
					} 
				}
				else
				{
					/*1 THREAD EVENT WAIT( 1 Passed )*/
					OSAL_s32EventWait(	gSHEventHandle,PATTERN_THR1,
		                   				OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
						   				&gevMask );
					/*Clear the event*/
					OSAL_s32EventPost
					( 
					gSHEventHandle,
					~(gevMask),
				   OSAL_EN_EVENTMASK_AND
				    );

					/*Thread Thread_SH_128_255 spawn failed*/
					u32Ret += 3000;
				}
			}
			else
			{
				/*Thread Thread_SH_0_127 spawn failed*/
				u32Ret += 4000;
			}
		}
		else
		{
			/*Shared Memory Create failed*/
			u32Ret += 10000;
		}

		/*Check for data in shared memory*/
		if( OSAL_NULL != ( shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,
				            							   ( SH_MEMSIZE_512-1 ),OFFSET_0 ) ) )
		{
			if( ( *( ( tChar * )shptr + 0 ) != 'a' )	|| ( *( ( tChar * )shptr + 127 ) != 'a' ) )
			{
				u32Ret += 127; /*Failure of first mapping*/
			}

			if( ( *( ( tChar * )shptr + 128 ) != 'b' )	|| ( *( ( tChar * )shptr + 255 ) != 'b' ) )
			{
				u32Ret += 254; /*Failure of second mapping*/
			}

			if( ( *( ( tChar * )shptr + 256 ) != 'c' )	|| ( *( ( tChar * )shptr + 383 ) != 'c' ) )
			{
				u32Ret += 381; /*Failure of third mapping*/
			}

			if( ( *( ( tChar * )shptr + 384 ) != 'd' )	|| ( *( ( tChar * )shptr + 510 ) != 'd' ) )
			{
				u32Ret += 507; /*Failure of fourth mapping*/
			}
		}

		
		/*REMOVE OSAL RESOURCES*/

		/*Delete Thread Thread_SH_0_127*/ 
        /* FAN4HI: 2011 11 11 - ThreadIDs in LSim might be < 0 */
	    if( tID_1 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_1 ) )
			{
				u32Ret += 7000;
			}
		}

		/*Delete Thread Thread_SH_128_255*/ 
        if( tID_2 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_2 ) )
			{
				u32Ret += 8000;
			}
		}

		/*Delete Thread Thread_SH_256_383*/ 
		if( tID_3 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_3 ) )
			{
				u32Ret += 9000;
			}
		}

		/*Delete Thread Thread_SH_384_511*/ 
        if( tID_4 != OSAL_ERROR )
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_4 ) )
			{
				u32Ret += 11000;
			}
		}

		/*Close and Delete the Event*/
		if( OSAL_ERROR != OSAL_s32EventClose( gSHEventHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( "SH_MEM_EVENT" ) )
			{
				u32Ret += 12000;
			}
		}
		else
		{
			u32Ret += 13000;
		}

		/*Close and Delete the Shared Memory*/
		if( OSAL_ERROR != ghShared )
		{
			if( OSAL_ERROR != OSAL_s32SharedMemoryClose( ghShared ) )
			{
				if( OSAL_ERROR == OSAL_s32SharedMemoryDelete( SH_NAME ) )
				{
					u32Ret  += 14000;
				}
			}
			else
			{
				u32Ret += 15000;
			}
		}
		
		/*Reinitialize the Global Variables*/
		gSHEventHandle = 0;
		ghShared       = 0;	

	}
	else
	{
		/*Reinitialize the Global variables*/
		gSHEventHandle = 0;
		return 50000;
	}

	/*RETURN ERROR CODE*/
	return ( u32Ret+u32ThrEC_1+u32ThrEC_2+u32ThrEC_3+u32ThrEC_4 );

}

void Thread_SH_Memory(tVoid* pvArg)
{
   tS32 s32ReturnValue = 0;
   OSAL_tShMemHandle handle_2 = 0;
   tPVoid map_2 = 0;

   if ((handle_2 = OSAL_SharedMemoryOpen(SH_NAME, OSAL_EN_READWRITE)) != OSAL_ERROR)
   {
      if ((map_2 = OSAL_pvSharedMemoryMap(handle_2, OSAL_EN_READWRITE, SH_SIZE, 0)) != 0)
      {
         if ((OSAL_pvMemorySet(map_2, 0, SH_SIZE) && OSAL_pvMemorySet(map_2, 1, SH_SIZE)))
         {
            if (OSAL_s32SharedMemoryUnmap(map_2, SH_SIZE) != OSAL_ERROR)
            {
               if (OSAL_s32SharedMemoryClose(handle_2) != OSAL_ERROR)
               {
                  // test ok
               }
               else
                  s32ReturnValue =5000;
            }
            else
               s32ReturnValue = 4000;
         }
         else
            s32ReturnValue = 3000;
      }
      else
         s32ReturnValue = 2000;
   }
   else
      s32ReturnValue = 1000;

   // thread return value
   *(tS32 *)pvArg = s32ReturnValue;
}
