/******************************************************************************
 *FILE         : oedt_osalcore_SIG_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file defines the  test cases for the testing
 *               the OSAL Signal Handling API.
 *               
 *AUTHOR       : Madhu Sudhan Swargam (RBEI/ECF5)
 *
 *
 *COPYRIGHT    : 2012, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : Version 1.0 , 04 - 12- 2012
 *             : Version 1.1  24-04-2013
 *             : Added  test cases for configuring all the Signals
 *             : and to check their status.  - MDH4COB
 *               
 *
 *****************************************************************************/
#include <signal.h>

/*Defines*/
#ifndef OEDT_OSAL_CORE_SIG_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_SIG_TESTFUNCS_HEADER
#define SIGNALNULL NULL
#define SIGNALINVALID SIGUSR2
#define SIGNALDOESNTEXIST (SIGRTMAX+10)
#define TESTSIGNAL1 SIGXFSZ
#define TESTSIGNAL2 SIGUSR1
#define TESTSIGNAL3 SIGALRM
#define TESTSIGNAL4 SIGSEGV
#define TESTSIGNAL5 SIGILL
#define TESTSIGNAL6 SIGFPE
#define TESTSIGNAL7 SIGSYS
#define TESTSIGNAL8 SIGBUS
#define TESTSIGNAL9 SIGABRT 
#define TESTSIGNAL10 SIGPIPE
#define TESTSIGNAL11 SIGXCPU

/* Function prototypes of functions used 
            in file oedt_osalcore_SIG_TestFuncs.c */
/*Test cases*/

tU32 u32ConfigureSignalNULL(tVoid);                //TU_OEDT_OSAL_CORE_SIG_HNDL_001
tU32 u32ConfigureSignalInvalid(tVoid);             //TU_OEDT_OSAL_CORE_SIG_HNDL_002
tU32 u32ConfigureSignalDoesntExist(tVoid);         //TU_OEDT_OSAL_CORE_SIG_HNDL_003
tU32 u32GetStatusInvalid(tVoid);                   //TU_OEDT_OSAL_CORE_SIG_HNDL_004
tU32 u32GetInvalidSignalStatus(tVoid);             //TU_OEDT_OSAL_CORE_SIG_HNDL_005
tU32 u32EnableTwiceCheck(tVoid);                   //TU_OEDT_OSAL_CORE_SIG_HNDL_006
tU32 u32DisableTwiceCheck(tVoid);                  //TU_OEDT_OSAL_CORE_SIG_HNDL_007
tU32 u32ConfigureSignalCheckWithStatus(tVoid);     //TU_OEDT_OSAL_CORE_SIG_HNDL_008
tU32 u32ConfigureCheck(tVoid);                     //TU_OEDT_OSAL_CORE_SIG_HNDL_009
tU32 u32ConfigurTwiceDisableCheck(tVoid);          //TU_OEDT_OSAL_CORE_SIG_HNDL_010
tU32 u32ConfigurSignalThreadsCheck(tVoid);         //TU_OEDT_OSAL_CORE_SIG_HNDL_011
tU32 u32RandomConfigurSignal(tVoid);               //TU_OEDT_OSAL_CORE_SIG_HNDL_012
tU32 u32StatusCheckOfSignals(tVoid);               //TU_OEDT_OSAL_CORE_SIG_HNDL_013
tU32 u32ConfigureCheckVariousSignals(tVoid);       //TU_OEDT_OSAL_CORE_SIG_HNDL_014
#endif
