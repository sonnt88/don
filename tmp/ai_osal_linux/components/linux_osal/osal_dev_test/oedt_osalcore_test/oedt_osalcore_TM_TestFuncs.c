/******************************************************************************
 *FILE         : oedt_osalcore_TM_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the TIMER OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *                ver 1.9 -12-12-2012
                  added test case for timer call back handle limit -SWM2KOR
 *              ver1.8 - 07-09-09
				updates for release version - Haribabu
                
                ver1.7   - 04-05-2009
                lint info removal
				rav8kor

 				ver1.6   - 22-04-2009
                lint info removal
				sak9kor

  *				ver1.5	 - 14-04-2009
                warnings removal,fix test cases
				rav8kor
 *				 
 				Version 1.4 
 *				 Event clear update by  
 *				 Anoop Chandran (RBEI\ECM1) 27/09/2008
 *
 * 			 Version 1.3, 25- 03- 2008
 *               Updated case:
 *               u32SetTimeTimerStTimeOrIntToNULL
 *				 Added cases:
 *				 u32TimerPerformanceMultiThread
 *               u32TimerPerformanceSingleThread
 *
 *				 Version 1.2 , 1- 1- 2008
 *				 Tinoy Mathews( RBIN/ECM1 )
 *				 Updated error code in
 *				 TU_OEDT_OSAL_CORE_TMR_002
 *
 *				 Version 1.1 , 13- 12- 2007
 *				 Tinoy Mathews( RBIN/ECM1)
 *				 Updated : Linted the code.
 *
 *				 Version 1.0 , 26- 10- 2007
 *					
 *
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "OsalConf.h"

//#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"
#include "oedt_helper_funcs.h"
#include "oedt_osalcore_TM_TestFuncs.h"

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1
//#define OEDT_TM_CALLBACK_OVER 1

#define EVENT_TIMER_MT         0x00000001
#define MAX_TIMER_CALLBACK_CHECK 12

/*****************************************************************
| Global Variables
|----------------------------------------------------------------*/
OSAL_tTimerHandle tHandle_1            = 0;
OSAL_tTimerHandle tHandle_2            = 0;
OSAL_tEventHandle globalevHandle       = 0;
OSAL_tSemHandle   globalTimersemHandle = 0;
OSAL_tEventHandle hEvSyncTimerMT       = 0;
tU32 u32Counter                        = 1;
tU32 u32Count                          = 0;
tU32 u32CallBack_count                 = 0; //Used to check the timer call back execution
trOsalLock   TimerLimitCheckLock ;



OSAL_tSemHandle globalIndexsemHandle   = 0;	

OSAL_tThreadID tID[LOAD]                 = {0};
OSAL_tTimerHandle tmHandleArray[LOAD]    = {0};
tU32 u32CounterArray[LOAD]               = {0};
tU32 u32StartElapsedTime[LOAD]           = {0};
tU32 u32EndElapsedTime[LOAD]             = {0};
tS32 s32Drift[LOAD]                      = {0};
tU8  u8LimitCheck[LOAD]                  = {0};
tU32 globIndex                           = 0;
tU8  u8Multicast                         = 0;
tU8  u8TimerSwitch                       = 0;
OSAL_tMSecond u32StartTime               = 0;
OSAL_tMSecond u32EndTime                 = 0;
/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/
tU32 u32CheckTimeDiff(tU32 tU32Value, tU32 _u32StartTime, tU32 _u32EndTime)
{
   tU32 u32Ret = 0;
   if (_u32EndTime > _u32StartTime)
   {
      if ((_u32EndTime - _u32StartTime) < tU32Value && (_u32EndTime - _u32StartTime) > (tU32Value + 100))
      {
         u32Ret += 100;
      }
   }
   else
   {
      u32Ret += 100;
   }

   return u32Ret;
}

tU32 vPrintTime()
{
   tU32 u32Ret = 0;

   OSAL_trTimeDate rtcGetParam = { 0 };

   if (OSAL_ERROR == OSAL_s32ClockGetTime(&rtcGetParam))
   {
      u32Ret += 2;
   }
   else
   {
      //PRINTF("Time now %d:%d:%d\n", rtcGetParam.s32Hour, rtcGetParam.s32Minute, rtcGetParam.s32Second);
   }

   return u32Ret;

}

void oedt_vChangeTimeCallback(void* u32Idx)
{
   //PRINTF("   %s ENTER \n", __func__);
   ((void)u32Idx);
   u32EndTime = OSAL_ClockGetElapsedTime();
   vPrintTime();

   //PRINTF("   %s EXIT  \n", __func__);
}

tU32 vChangeTimeForward(tU32 u32Seconds)
{
   tU32 u32Ret = 0;

   OSAL_trTimeDate rtcSetParam = { 0 };
   OSAL_trTimeDate rtcGetParam = { 0 };

   /*Initialize Structure*/
   rtcSetParam.s32Second = u32Seconds;
   rtcSetParam.s32Minute = 0;
   rtcSetParam.s32Hour = 10;
   rtcSetParam.s32Day = 1;
   rtcSetParam.s32Month = 1;
   rtcSetParam.s32Year = 111;
   rtcSetParam.s32Daylightsaving = 0;
   rtcSetParam.s32Weekday = 0;
   rtcSetParam.s32Yearday = 1;
   rtcSetParam.s32Daylightsaving = 0;

   if (OSAL_ERROR == OSAL_s32ClockSetTime(&rtcSetParam))
   {
      /*Critical Error!*/
      u32Ret += 10000;
   }
   else
   {
      if (OSAL_ERROR == OSAL_s32ClockGetTime(&rtcGetParam))
      {
         /*Critical Error!*/
         u32Ret += 20000;
      }
      else
      {
         if ((rtcGetParam.s32Hour != 10) && (rtcGetParam.s32Day != 1) && (rtcGetParam.s32Month != 1)
                  && (rtcGetParam.s32Second != 10) && (rtcGetParam.s32Year != 111))
         {
            /*Critical Error!*/
            u32Ret += 30000;
         }
      }
   }
   return u32Ret;

}

tU32 u32SetTime(tVoid)
{
   tU32 u32Ret = 0;
   OSAL_trTimeDate rtcSetParam = { 0 };
   OSAL_trTimeDate rtcGetParam = { 0 };

   /*Initialize Structure*/
   rtcSetParam.s32Second = 0;
   rtcSetParam.s32Minute = 0;
   rtcSetParam.s32Hour = 10;
   rtcSetParam.s32Day = 1;
   rtcSetParam.s32Month = 1;
   rtcSetParam.s32Year = 111;
   rtcSetParam.s32Daylightsaving = 0;
   rtcSetParam.s32Weekday = 0;
   rtcSetParam.s32Yearday = 1;
   rtcSetParam.s32Daylightsaving = 0;

   if (OSAL_ERROR == OSAL_s32ClockSetTime(&rtcSetParam))
   {
      u32Ret += 10000;
   }
   else
   {
      if (OSAL_ERROR == OSAL_s32ClockGetTime(&rtcGetParam))
      {
         u32Ret += 20000;
      }
      else
      {
         if ((rtcGetParam.s32Hour != 10)        &&
                  (rtcGetParam.s32Day != 1)     &&
                  (rtcGetParam.s32Month != 1)   &&
                  (rtcGetParam.s32Year != 111))
         {
            u32Ret += 30000;
         }
      }
   }
   return u32Ret;
}

tU32 u32CreateTimer(tVoid)
{
   tU32 u32Ret = 0;
   OSAL_tTimerHandle hTimer;

   if (OSAL_ERROR != OSAL_s32TimerCreate(oedt_vChangeTimeCallback, 0, &hTimer))
   {
      u32StartTime = OSAL_ClockGetElapsedTime();

      if (OSAL_ERROR != OSAL_s32TimerSetTime(hTimer, (OSAL_tMSecond) (10*1000), 0))
      {
         u32Ret += vPrintTime();
         u32Ret += vChangeTimeForward(5);
         u32Ret += vPrintTime();
         OSAL_s32ThreadWait(12 * 1000);
         u32Ret += vPrintTime();
         u32Ret += u32CheckTimeDiff(10*1000, u32StartTime, u32EndTime);
         if (OSAL_ERROR != OSAL_s32TimerDelete(hTimer)) {
            // test OK
         } else {
            u32Ret += 2000;
         }
      }
      else
      {
         u32Ret += 3000;
      }
   }
   else
   {
      u32Ret += 4000;
   }

   return u32Ret;
}


/*Update the counter value*/
tVoid CounterCallBack( tPVoid argv )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
	u32Count++;
}

/*Is called over periodically with a defined time span*/
tVoid TimerCallback(tPVoid argv)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
	/*Simply wait for 1 ms*/
	OSAL_s32ThreadWait( CALLBACK_WAIT_TIME );
}

tVoid TimerCallback_1(tPVoid argv) 
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_1\n" );
}

tVoid TimerCallback_2(tPVoid argv) 
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_2\n" );
}

/*Thread_Timer_1*/
tVoid Thread_Timer_1( tPVoid argv )
{
	/*Declarations*/
	OSAL_tpfCallback Callback_1  = OSAL_NULL;
	OSAL_tMSecond msec           = 0;
	OSAL_tMSecond interval       = 0;

	/*Init*/
	Callback_1 = (OSAL_tpfCallback)TimerCallback_1;
   	
	if( OSAL_ERROR == OSAL_s32TimerCreate( Callback_1,OSAL_NULL,&tHandle_1 ) )
	{
		OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		/*Critical section*/
		*( (tU32 *)argv ) += 450;
		/*Critical section*/
		OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	else
	{
		/*Set*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_1,TEN_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 750;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );
	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_1,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		        /*Critical section*/
				*( (tU32 *)argv ) += 950;
				/*Critical section*/
		        OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		/*Reset*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_1,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 150;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_1,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		        /*Critical section*/
				*( (tU32 *)argv ) += 250;
				/*Critical section*/
		        OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		if( OSAL_ERROR == OSAL_s32TimerDelete( tHandle_1 ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 1150;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );		
		}
	}

	/*Signal the main thread of thread action completion*/
	if( OSAL_ERROR == OSAL_s32EventPost( globalevHandle,TIMER_ONE_EVENT,OSAL_EN_EVENTMASK_OR ) )
	{
	   OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	   /*Critical section*/
	   *( (tU32 *)argv ) += 2150;
	   /*Critical section*/
	   OSAL_s32SemaphorePost( globalTimersemHandle );			
	}
	
	OSAL_s32ThreadWait( DELAY );
}

/*Thread_Timer_2*/
tVoid Thread_Timer_2( tPVoid argv )
{
   /*Declarations*/
	OSAL_tpfCallback Callback_2  = OSAL_NULL;
	OSAL_tMSecond msec           = 0;
	OSAL_tMSecond interval       = 0;

	/*Init*/
	Callback_2 = (OSAL_tpfCallback)TimerCallback_2;
   	
	if( OSAL_ERROR == OSAL_s32TimerCreate( Callback_2,OSAL_NULL,&tHandle_2 ) )
	{
		OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	    /*Critical section*/
		*( (tU32 *)argv ) += 450;
		/*Critical section*/
	    OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	else
	{
		/*Set*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_2,TEN_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 750;
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_2,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	            /*Critical section*/
				*( (tU32 *)argv ) += 950;
				/*Critical section*/
	            OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		/*Reset*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_2,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 150;	
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_2,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	            /*Critical section*/
				*( (tU32 *)argv ) += 250;
				/*Critical section*/
	            OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		if( OSAL_ERROR == OSAL_s32TimerDelete( tHandle_2 ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 1150;		
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );
		}
	}

	/*Signal the main thread of thread action completion*/
	if( OSAL_ERROR == OSAL_s32EventPost( globalevHandle,TIMER_TWO_EVENT,OSAL_EN_EVENTMASK_OR ) )
	{
	   OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	   /*Critical section*/
	   *( (tU32 *)argv ) += 2150;			
	   /*Critical section*/
	   OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	
	OSAL_s32ThreadWait( DELAY );
}

/*Timer to test for Phase Time and Cycle Time duration*/
tVoid CyclePhaseCheck( tPVoid argv )
{
	/*Check the Counter*/
	if( u32Counter < 3 )
	{
		*( ( OSAL_tMSecond *)argv + u32Counter ) = OSAL_ClockGetElapsedTime( );
	}

	/*Increment the Counter*/
	u32Counter++;
}


/*****************************************************************************
* FUNCTION    :	   u32CreateTimerWithCallbackNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_001

* DESCRIPTION :  
				   1). Create Timer with the callback as OSAL_NULL
			       2). If Timer creation successful, delete the 
				       Timer from the system and report failure of the
					   timer create OSAL API
				   3). If timer creation fails, track the error code,
				       the API is successful only if the error code
					   OSAL_E_INVALIDVALUE is returned.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateTimerWithCallbackNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;

	/*Try to create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		  OSAL_NULL,
	                             		  OSAL_NULL, 
	                             		  &tmHandle ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Success condition for the API is to return 
			OSAL_E_INVALIDVALUE*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Created a Timer!*/
		u32Ret += 1500;

		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}	
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelTimerNonExistent()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_002

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Delete the Timer from the system.
				   3). Try deleting the same timer again.
				   4). If Timer delete successful, report failure of the
					   timer delete OSAL API in the returned error code
				   5). If timer delete fails, track the error code,
				       the API is successful only if the error code
					   OSAL_E_DOESNOTEXIST is returned.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelTimerNonExistent( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;
	OSAL_tTimerHandle tmHandle_   = 0;
	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)TimerCallback;

	/*Scenario 1*/
	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
		else
		{
			/*Try deleting the non existent Timer*/
			if( OSAL_ERROR ==  OSAL_s32TimerDelete( tmHandle ) )
			{
				/*Query the returned error code*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
				{
					/*Success condition for the API is to return 
					OSAL_E_DOESNOTEXIST*/
					u32Ret += 1000;
				}
			}
			else
			{
			   /*Timer redelete passed!*/
			   u32Ret += 1500;	
			}
		}
	}
	/*Scenario 1*/

	/*Scenario 2*/
	if( OSAL_ERROR ==  OSAL_s32TimerDelete( tmHandle_ ) )
	{
		/*Query the returned error code*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Success condition for the API is to return 
			OSAL_E_INVALIDVALUE*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Timer redelete passed!*/
		u32Ret += 2500;	
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
The timer callbacks uesd to update the timer call back check
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Oct 26,2012
*******************************************************************************/
tVoid TimerCallback_CHK11(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_11 called\n" );
}

tVoid TimerCallback_CHK10(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_10 called\n" );
}

tVoid TimerCallback_CHK9(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_9 called\n" );
}

tVoid TimerCallback_CHK8(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_8 called\n" );
}

tVoid TimerCallback_CHK7(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_7 called\n" );
}

tVoid TimerCallback_CHK6(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_6 called\n" );
}

tVoid TimerCallback_CHK5(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_5 called\n" );
}

tVoid TimerCallback_CHK4(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_4 called\n" );
}

tVoid TimerCallback_CHK3(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_3 called\n" );
}

tVoid TimerCallback_CHK2(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_2 called\n" );
}

tVoid TimerCallback_CHK1(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_1 called\n" );
}

tVoid TimerCallback_CHK0(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   OEDT_HelperPrintf( TR_LEVEL_USER_1,"Message : Timer_0 called\n" );
}

/*****************************************************************************
* FUNCTION    :	   u32MytestCasestemp()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_016

* DESCRIPTION :    Checks the Call Back Handles of timer when all are called at a single instant
			       	
* HISTORY     :	   Created by Madhu Sudhan Swargam (RBEI/ECF5)  Oct 26,2012
*******************************************************************************/
tU32  U32MaxCallHandleCheck( tVoid )
{
   tU32 u32Ret = 0;
   tU32 i;
   //temp_count=0;
 //  tU32 loop_count=0;
 //  char CallBackName[50]={0};
//   OSAL_tMSecond msec           = 0;
 //  OSAL_tMSecond interval       = 0;
   OSAL_tTimerHandle tHandle_CallBackCHK[MAX_TIMER_CALLBACK_CHECK]         ={0};
   //Creating and opening the Lock
   if(CreateOsalLock(&TimerLimitCheckLock, "TimerCallBckLmtChck") != OSAL_OK)
   {   
      NORMAL_M_ASSERT_ALWAYS();
   }
   if(OpenOsalLock(&TimerLimitCheckLock) != OSAL_OK)
   {   
      NORMAL_M_ASSERT_ALWAYS();   
   }
   OSAL_tpfCallback Callback_CHK[MAX_TIMER_CALLBACK_CHECK]={0};
   // Intasilizing the universal data
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count =0;
      UnLockOsal(&TimerLimitCheckLock);
   }
   /*Init*/
   Callback_CHK[0] = (OSAL_tpfCallback)TimerCallback_CHK0;
   Callback_CHK[1] = (OSAL_tpfCallback)TimerCallback_CHK1;
   Callback_CHK[2] = (OSAL_tpfCallback)TimerCallback_CHK2;
   Callback_CHK[3] = (OSAL_tpfCallback)TimerCallback_CHK3;
   Callback_CHK[4] = (OSAL_tpfCallback)TimerCallback_CHK4;
   Callback_CHK[5] = (OSAL_tpfCallback)TimerCallback_CHK5;
   Callback_CHK[6] = (OSAL_tpfCallback)TimerCallback_CHK6;
   Callback_CHK[7] = (OSAL_tpfCallback)TimerCallback_CHK7;
   Callback_CHK[8] = (OSAL_tpfCallback)TimerCallback_CHK8;
   Callback_CHK[9] = (OSAL_tpfCallback)TimerCallback_CHK9;
   Callback_CHK[10] = (OSAL_tpfCallback)TimerCallback_CHK10;
   Callback_CHK[11] = (OSAL_tpfCallback)TimerCallback_CHK11;
   // Creation the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      if( OSAL_ERROR == OSAL_s32TimerCreate( Callback_CHK[i],OSAL_NULL,&tHandle_CallBackCHK[i] ) )
      {
          u32Ret=100;
      }
   }
   // Setting the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_CallBackCHK[i],5,0 ) )
      {
          u32Ret=200;
      }
   }
   OSAL_s32ThreadWait( 50);
   // deleting the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      if( OSAL_ERROR == OSAL_s32TimerDelete( tHandle_CallBackCHK[i]) )
      {
         u32Ret=300;
      }
   }
   OSAL_s32ThreadWait( 500);
    if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      if(u32CallBack_count!=MAX_TIMER_CALLBACK_CHECK)
      {
         OEDT_HelperPrintf( TR_LEVEL_FATAL,"Bug not all timers are callback exccuted only=%d\n",u32CallBack_count );
         u32Ret=u32CallBack_count;
      }
      UnLockOsal(&TimerLimitCheckLock);
   }
   u32CallBack_count=0;
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32DelTimerWithHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_003

* DESCRIPTION :    Call the Timer Delete OSAL API with OSAL_NULL
                   parameter
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32DelTimerWithHandleNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret = 0;

	/*Issue a call to Timer delete OSAL API with OSAL_NULL parameter*/
	if( OSAL_ERROR == OSAL_s32TimerDelete( OSAL_NULL ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 250;
		}
	}
	else
	{
		/*Timer delete passed with NULL!*/
		u32Ret += 350;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateDelTimer()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_004

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Delete the Timer from the system.
				   3). Return error code( Incase create
				       fails or delete fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateDelTimer( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;
	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)TimerCallback;

	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SetTimeTimerStTimeOrIntToNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_005

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Set timer to different parameters:

				       a).NULL handle,non zero valid cycle time,non zero valid phase time.
				       b).Valid handle,zero cycle time,non zero valid phase time.
				       c).Valid handle,non zero valid cycle time,zero phase time.
					   d).Valid handle,zero cycle time,zero phase time.

				   3). Delete the Timer from the system.
				   4). Return error code( Incase create
				       fails or delete fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32SetTimeTimerStTimeOrIntToNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	tU32 u32Count_1               = 0;
	tU32 u32Count_2               = 0;

	OSAL_tTimerHandle tmHandle    = 0;
	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)CounterCallBack;

	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		
		/*Scenario 1 - Pass Timer handle as OSAL_NULL*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( OSAL_NULL,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 100;
			}
		}
		else
		{
			/*Set time passed for a handle set to OSAL_NULL!*/
			u32Ret += 1000;
		}
		/*Scenario 1*/

		/*Collect the Global Count Value*/
		u32Count_1 = u32Count;

		/*Scenario 2 - Valid timer handle,Zero phase time,Non zero valid cycle time*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,0,TEN_MILLI_SECONDS ) )
		{
			/*Failed*/
			u32Ret += 200;
		}
		/*Scenario 2 - Valid timer handle,Zero phase time,Non zero valid cycle time*/

		/*Wait for Time period greater than 10 milliseconds
		rav8kor - callback should not be called as making phase 0 stops the timer */
		OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );


        /*Collect the Global Count Value now*/
		u32Count_2 = u32Count;

		/*Check the counter for changes, if there are  changes, callback
		was executed.rav8kor  */
		if( u32Count_1 != u32Count_2 )
		{
			u32Ret += 250;
		}

		/*Reinitialize the Counters*/
		u32Count_1 = 0;
		u32Count_2 = 0;

		
		/*Collect the Global Count Value*/
		u32Count_1 = u32Count;

		/*Scenario 3 - Valid timer handle,Non zero phase time,Zero cycle time*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,0 ) )
		{
			/*Failed*/
			u32Ret += 300;
		}
		/*Scenario 3 - Valid timer handle,Non zero phase time,Zero cycle time*/

		/*Wait for Time period greater than 10 milliseconds
		Until this time the callback should execute atleast once*/
		OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );

        /*Collect the Global Count Value now*/
		u32Count_2 = u32Count;

		/*Check the counter for changes, if there are no changes, callback
		was not executed*/
		if( u32Count_1 == u32Count_2 )
		{
			u32Ret += 350;
		}

		/*Reinitialize the Counters*/
		u32Count_1 = 0;
		u32Count_2 = 0;

		/*Collect the Global Count Value*/
		u32Count_1 = u32Count;

		/*Scenario 4 - Valid timer handle,Zero phase time,Zero cycle time*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,0,0 ) )
		{
			/*Failed*/
			u32Ret += 400;
		}
		/*Scenario 4 - Valid timer handle,Zero cycle time,Zero phase time*/

		/*Wait for Time period greater than 10 milliseconds
		Until this time the callback should execute atleast once*/
		OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );

        /*Collect the Global Count Value now*/
		u32Count_2 = u32Count;

		/*Check the counter for changes, if there are no changes, callback
		was not executed*/
		if( u32Count_1 != u32Count_2 )
		{
			u32Ret += 350;
		}

		/*Reinitialize the Counters*/
		u32Count_1 = 0;
		u32Count_2 = 0;

		/*Collect the Global Count Value*/
		u32Count_1 = u32Count;

		/*Scenario 4 - Valid timer handle,Zero phase time,Zero cycle time*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			/*Failed*/
			u32Ret += 400;
		}
		/*Scenario 4 - Valid timer handle,Zero cycle time,Zero phase time*/

		/*Wait for Time period greater than 10 milliseconds
		Until this time the callback should execute atleast once*/
		OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );

        /*Collect the Global Count Value now*/
		u32Count_2 = u32Count;

		/*Check the counter for changes, if there are no changes, callback
		was not executed*/
		if( u32Count_1 == u32Count_2 )
		{
			u32Ret += 350;
		}
		
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}

	/*Reinitialize the global counter*/
    u32Count = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SetTimeTimerNonExistent()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_006

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Delete the Timer from the system.
				   3). Set Time for the Deleted Timer 
				       If Set Time API fails, it is an expected behaviour,
					   query the error code,it should be OSAL_E_DOESNOTEXIST,
					   if the Set Time API should pass.
					   If Set Time API passes, it is a critical error,
					   update the return error code.
				   4). Return error code( Incase create
				       fails or delete fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32SetTimeTimerNonExistent( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;

	#if EXCEPTION
	OSAL_tTimerHandle tmHandle_   = -7;
	#endif

	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)TimerCallback;

	/*Scenario 1*/
	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}
	
	/*Try to set time for a timer which has been deleted from 
	the system*/
	if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 200;
		}
	}
	else
	{
		/*Set Time API passes for a non existent timer!*/
		u32Ret += 300;
	}
	/*Scenario 1*/

	/*As of now, this section of code crashes the system......
	whether this section is to be kept or removed, needs clarification
	To support that this section should be removed is the fact that
	OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
	#if EXCEPTION 
	/*Scenario 2*/
	if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle_,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 400;
		}
	}
	else
	{
		/*Set Time API passes for a non existent timer!*/
		u32Ret += 500;
	}
	/*Scenario 2*/
	#endif

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetTimeTimerNonExistent()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_007

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Delete the Timer from the system.
				   3). Get Time for the Deleted Timer 
				       If Get Time API fails, it is an expected behaviour,
					   query the error code,it should be OSAL_E_DOESNOTEXIST,
					   if the Get Time API should pass.
					   If Get Time API passes, it is a critical error,
					   update the return error code.
				   4). Return error code( Incase create
				       fails or delete fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetTimeTimerNonExistent( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;

	#if EXCEPTION
	OSAL_tTimerHandle tmHandle_   = -7;
	#endif

	OSAL_tMSecond msec            =	TEN_MILLI_SECONDS;
	OSAL_tMSecond interval        = TEN_MILLI_SECONDS;
	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)TimerCallback;

	/*Scenario 1*/
	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}
	
	/*Try to get time for a timer which has been deleted from 
	the system*/
	if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,&msec,&interval ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 200;
		}
	}
	else
	{
		/*Get Time API passes for a non existent timer!*/
		u32Ret += 300;
	}
	/*Scenario 1*/

	/*As of now, this section of code crashes the system......
	whether this section is to be kept or removed, needs clarification
	To support that this section should be removed is the fact that
	OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
	#if EXCEPTION
	/*Scenario 2*/
	if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle_,&msec,&interval ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 400;
		}
	}
	else
	{
		/*Get Time API passes for a non existent timer!*/
		u32Ret += 500;
	}
	/*Scenario 2*/
	#endif

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetTimeTimerWithDiffParam()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_008

* DESCRIPTION :  
				   1). Create Timer with valid parameters.
				   2). Set timer using valid parameters.
				   3). Get timer using following invalid/valid paramters combinations
				       for Periodic Timer( Cyclic handler ) and Single Timer( Alarms )
					   a). Get Time using OSAL_NULL handle
		               b). Get Time using OSAL_C_INVALID_HANDLE as handle
		               c). Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE
		               d). Get Time using valid handle,OSAL_NULL(cycle time address),valid phase time address
                       e). Get Time using valid handle,valid cycle time address,OSAL_NULL(phase time address)
		               f). Get Time using valid handle,OSAL_NULL(cycle time address),OSAL_NULL(phase time address)
				       
				   3). Delete the Timer from the system.
				   4). Return error code( Incase create
				       fails or delete fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetTimeTimerWithDiffParam( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;

	#if EXCEPTION
	OSAL_tTimerHandle tmHandle_   = -7;
	#endif

	OSAL_tMSecond msec            = 0;
	OSAL_tMSecond interval        = 0; 
	OSAL_tpfCallback Callback     = OSAL_NULL;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)TimerCallback;

	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   OSAL_NULL, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		
		/*Set the Timer first 
		Timer Type  : Periodic Timer
		Description :
		This means, once the timer API to trigger timer is called, only after an interval(phase time),
		is the handler routine run,after this phase time, for every cycle time, the handler routine
		is run.
		TKernel API :
		Cyclic Handler
		*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			/*Set Time failed!*/
			u32Ret += 1000;
		}

		/* 1 ------ Get Time using OSAL_NULL handle*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( OSAL_NULL,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 500;
			}
		}
		else
		{
			/*Get Time passed for OSAL_NULL handle!*/
			u32Ret += 2000;
		}
		/* 1 ------ Get Time using OSAL_NULL handle*/


		/* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( OSAL_C_INVALID_HANDLE,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 550;
			}
		}
		else
		{
			/*Get Time passed for OSAL_C_INVALID_HANDLE handle!*/
			u32Ret += 2500;
		}
		/* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/


		/*As of now, this section of code crashes the system......
	    whether this section is to be kept or removed, needs clarification
	    To support that this section should be removed is the fact that
	    OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
		#if EXCEPTION
		/* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle_,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 600;
			}
		}
		else
		{
			/*Get Time passed for INVALID_HANDLE handle!*/
			u32Ret += 2600;
		}
		/* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
		#endif


		/* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
		            valid phase time address*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,&interval ) )
		{
			u32Ret += 700;
		}
		/* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
		            valid phase time address*/
		
		/* 5 ------ Get Time using valid handle,valid cycle time address,
		            OSAL_NULL(phase time address)*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,&msec,OSAL_NULL ) )
		{
			u32Ret += 800;
		}
		/* 5 ------ Get Time using valid handle,valid cycle time address,
		            OSAL_NULL(phase time address)*/
		
		/* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
					OSAL_NULL(phase time address)*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,OSAL_NULL ) )
		{
			u32Ret += 900;
		}
		/* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
					OSAL_NULL(phase time address)*/
		
		
		/*Set the Timer first 
		Timer Type  : Single Timer
		Description :
		This means, once the timer API to trigger timer is called,the handler routine is run
		immediately		
		TKernel API :
		Alarm
		*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,0 ) )
		{
			/*Set Time failed!*/
			u32Ret += 1050;
		}

		/* 1 ------ Get Time using OSAL_NULL handle*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( OSAL_NULL,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 560;
			}
		}
		else
		{
			/*Get Time passed for OSAL_NULL handle!*/
			u32Ret += 2060;
		}
		/* 1 ------ Get Time using OSAL_NULL handle*/


		/* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( OSAL_C_INVALID_HANDLE,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 570;
			}
		}
		else
		{
			/*Get Time passed for OSAL_C_INVALID_HANDLE handle!*/
			u32Ret += 2560;
		}
		/* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/


		/*As of now, this section of code crashes the system......
	    whether this section is to be kept or removed, needs clarification
	    To support that this section should be removed is the fact that
	    OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
		#if EXCEPTION
		/* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle_,&msec,&interval ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 660;
			}
		}
		else
		{
			/*Get Time passed for INVALID_HANDLE handle!*/
			u32Ret += 2650;
		}
		/* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
		#endif


		/* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
		            valid phase time address*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,&interval ) )
		{
			u32Ret += 760;
		}
		/* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
		            valid phase time address*/
		
		/* 5 ------ Get Time using valid handle,valid cycle time address,
		            OSAL_NULL(phase time address)*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,&msec,OSAL_NULL ) )
		{
			u32Ret += 860;
		}
		/* 5 ------ Get Time using valid handle,valid cycle time address,
		            OSAL_NULL(phase time address)*/
		
		/* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
					OSAL_NULL(phase time address)*/
		if( OSAL_ERROR == OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,OSAL_NULL ) )
		{
			u32Ret += 960;
		}
		/* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),
					OSAL_NULL(phase time address)*/
		

		
		
		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SetResetTwoTimers()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_009

* DESCRIPTION :  
				   
				   1). Create an Event in the main thread.
				   2). Spawn two threads from the main thread.
				       In each thread, do these activities:
					   a).Create a Timer.
					   b).Set Time for the Timer.
					   c).Get Timer for the Timer.
					   d).Reset Timer.
					   e).Get Timer.
					   f).Delete Timer.
					   g).Post an event signalling thread completion to
					      the main thread.( Upto this time, the main
						  thread waits for events from subthreads ).
				   3). Return error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32SetResetTwoTimers( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                     = 0;
	tU8  u8Flag                     = 0;
	OSAL_tEventMask   evMask        = 0;
	OSAL_tThreadID tID_1            = 0;
	OSAL_tThreadID tID_2            = 0;
	OSAL_trThreadAttribute trAtr_1	= {OSAL_NULL};
	OSAL_trThreadAttribute trAtr_2  = {OSAL_NULL};

	/*Create a mutex as threads to be spawned access 
	a common memory location*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( MUTEX,&globalTimersemHandle, 1 ) )
	{
		/*Semaphore creation failed! Critical section not protected!*/
		u32Ret = 10000;
	}
	else
	{
		/*Set flag to true*/
		u8Flag = 1;
	}

	/*Create the event field*/
	if( OSAL_ERROR == OSAL_s32EventCreate( EVENT_NAME,&globalevHandle ) )
	{
		/*Event Creation failed!*/
		u32Ret += 100;
	}
	else
	{
		/*Fill in Thread 1 attributes*/
		trAtr_1.szName       = "Thread_Timer_1";
		trAtr_1.u32Priority	 = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   		trAtr_1.s32StackSize = 4096;
		trAtr_1.pfEntry      = (OSAL_tpfThreadEntry)Thread_Timer_1;
		trAtr_1.pvArg        = &u32Ret;
		/*Fill in Thread 2 attributes*/

		/*Fill in Thread 1 attributes*/
		trAtr_2.szName       = "Thread_Timer_2";
		trAtr_2.u32Priority	 = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   		trAtr_2.s32StackSize = 4096;
		trAtr_2.pfEntry      = (OSAL_tpfThreadEntry)Thread_Timer_2;
		trAtr_2.pvArg        = &u32Ret;
		/*Fill in Thread 2 attributes*/

		/*Spawn the Thread 1*/
		if( OSAL_ERROR != ( tID_1 = OSAL_ThreadSpawn( &trAtr_1 ) ) )
		{
			 /*Spawn Thread 2*/
			 if( OSAL_ERROR != ( tID_2 = OSAL_ThreadSpawn( &trAtr_2 ) ) )
			 {
				/*Wait until signal from the two threads*/
				if( OSAL_ERROR == OSAL_s32EventWait( globalevHandle,TIMER_ONE_EVENT|TIMER_TWO_EVENT,
				                   					 OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,
								   					 &evMask ) )
				{
					/*Event Wait failed*/
					u32Ret += 200;
				}
		  		/*Clear the event*/
				OSAL_s32EventPost
				( 
				globalevHandle,
				~(evMask),
			   OSAL_EN_EVENTMASK_AND
			    );

			 }
			 else
			 {
				/*Thread 2 Spawn failed*/
				u32Ret += 300;
			 }
		}
		else
		{
			/*Thread 1 Spawn failed*/
			u32Ret += 400;
		}

		/*Signal from Thread obtained --- Terminate threads*/
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_1 ) )
		{
			u32Ret += 500;
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_2 ) )
			{
				u32Ret += 600;
			}
		}

		/*Close the event and delete it*/
		if( OSAL_ERROR != OSAL_s32EventClose( globalevHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32EventDelete( EVENT_NAME ) )
			{
				u32Ret += 700;
			}
		}
		else
		{
			u32Ret += 800;
		}
	}

	/*Close the semaphore and Delete it if it were created*/
	if( u8Flag )
	{
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( globalTimersemHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( MUTEX ) )
			{
				u32Ret += 20000;
			}
		}
		else
		{
			u32Ret += 15000;
		}
	}	

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32GetElaspedTimeFromRef()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_010

* DESCRIPTION :  
				   1). Call Clock elapsed API.
				   2). Wait for 1 millisecond.
				   3). Call Clock elasped API.
				   4). Find difference 3). - 1). ( only if 3). > 1). )
				   5). Check if this difference is atleast 1 millisecond,
				       If not report critical error.
				   6). Return error code.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32GetElaspedTimeFromRef( tVoid )
{
	/*Declaration*/
	tU32 u32Ret                    = 0;
	OSAL_tMSecond  _u32StartTime   = 0;
	OSAL_tMSecond  _u32EndTime     = 0;
	OSAL_tMSecond  _u32ElapsedTime = 0;


	/*Catch offset time - Reference Time*/
	_u32StartTime = OSAL_ClockGetElapsedTime( );

	OSAL_s32ThreadWait( DUMMY_WAIT );

	/*Catch offset time again*/
	_u32EndTime   = OSAL_ClockGetElapsedTime( );

	/*Compute the elapsed time*/
	if( _u32EndTime > _u32StartTime )
	{
		_u32ElapsedTime = _u32EndTime - _u32StartTime;
	}
	
	/*Elapsed Time must be atleast 1 millisecond*/
	if( _u32ElapsedTime < DUMMY_WAIT ) 
	{
		/*Critical Error!*/ 
		u32Ret += 10000;
	}

	/*Return error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32SetOsalTimerInvalStruct()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_011

* DESCRIPTION :  
				   1).Call the Set Time API with invalid parameters.
				      The API is expected to fail with error code
				      OSAL_E_INVALIDVALUE.
				      If either the API passes(critical error), or the
				      error code is different,report the return error code
				      of failure.
				   2).Return the error code. 
				   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32SetOsalTimerInvalStruct( tVoid )
{
	/*Declaration*/
	tU32 u32Ret                    = 0;
	OSAL_trTimeDate rtcInvalParam  = {INVAL_VALUE,
	                                  INVAL_VALUE,
	                                  INVAL_VALUE,
	                                  INVAL_VALUE,
	                                  INVAL_VALUE,
	                                  INVAL_VALUE,
	                                  INVAL_VALUE,
									  INVAL_VALUE,
									  2*INVAL_VALUE };

	/*Attempt to Set Time field with invalid values*/
	if( OSAL_ERROR == OSAL_s32ClockSetTime( &rtcInvalParam ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 500;
		}
	}
	else
	{
		/*Critical Error!*/
		u32Ret += 10000;
	}
	
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetOsalTimerWithoutPrevSet()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_012

* DESCRIPTION :  
				   1).Call the Get Time API without a previous set.
				      The API is expected to fail with error code
				      OSAL_E_UNKNOWN.
				      If either the API passes(critical error), or the
				      error code is different,report the return error code
				      of failure.
				   2).Return the error code. 
				   
				   Note : This case should not be run after running case
				          TU_OEDT_OSAL_CORE_TMR_013
				   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32GetOsalTimerWithoutPrevSet( tVoid )
{
	/*Declaration*/
	tU32 u32Ret                    = 0;
	OSAL_trTimeDate rtcParam       = {0};
	tU8* aWeekdays[] = {(tU8*)"SUN",(tU8*)"MON",(tU8*)"TUE",(tU8*)"WED",(tU8*)"THU",(tU8*)"FRI",(tU8*)"SAT"};


	/*Attempt to Get Time field without a previous set*/
	if( OSAL_ERROR == OSAL_s32ClockGetTime( &rtcParam ) )
	{
		/*rav8kor - get time should not succeed without previous set*/
		//u32Ret += 10000;
		if( OSAL_E_UNKNOWN != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 500;
		}
	}
	else
	{
		u32Ret += 10000;	
	}
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "%d - %d - %d  %d : %d : %d %s[%d]",rtcParam.s32Year,rtcParam.s32Month,rtcParam.s32Day,rtcParam.s32Hour,rtcParam.s32Minute,rtcParam.s32Second,aWeekdays[rtcParam.s32Weekday],rtcParam.s32Weekday);
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SetOsalTimerGetOsalTimer()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_013

* DESCRIPTION :  
				   1).Call the Set Time API with valid parameters.
				   2).Call the Get Time API to retrive the set time.
					  If either set or get fails,report the error code
					  of API failure
				   3).Return the error code. 
				   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32SetOsalTimerGetOsalTimer( tVoid )
{
	/*Declaration*/
	tU32 u32Ret                    = 0;
	OSAL_trTimeDate rtcSetParam    = {0};
	OSAL_trTimeDate rtcGetParam    = {0};
	tU8* aWeekdays[] = {(tU8*)"SUN",(tU8*)"MON",(tU8*)"TUE",(tU8*)"WED",(tU8*)"THU",(tU8*)"FRI",(tU8*)"SAT"};



	/*Initialize Structure*/
	rtcSetParam.s32Second         = 59;
    rtcSetParam.s32Minute         = 59;
    rtcSetParam.s32Hour           = 23;
    rtcSetParam.s32Day            = 28;
    rtcSetParam.s32Month          = 2;
    rtcSetParam.s32Year           = 2012 - 1900;
    rtcSetParam.s32Daylightsaving = 0;
    rtcSetParam.s32Weekday        = 0;
    rtcSetParam.s32Yearday        = 59;
    rtcSetParam.s32Daylightsaving = 0;

	OEDT_HelperPrintf(TR_LEVEL_FATAL, "%d - %d - %d  %d : %d : %d %s[%d]",rtcSetParam.s32Year,rtcSetParam.s32Month,rtcSetParam.s32Day,rtcSetParam.s32Hour,rtcSetParam.s32Minute,rtcSetParam.s32Second,aWeekdays[rtcSetParam.s32Weekday],rtcGetParam.s32Weekday);

	/*Attempt to Set Time field with invalid values*/
	if( OSAL_ERROR == OSAL_s32ClockSetTime( &rtcSetParam ) )
	{
		/*Critical Error!*/
		u32Ret += 10000;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32ClockGetTime( &rtcGetParam ) )
		{
			/*Critical Error!*/
			u32Ret += 20000;
		}
	}
	OEDT_HelperPrintf(TR_LEVEL_FATAL, "%d - %d - %d  %d : %d : %d %s[%d]",rtcGetParam.s32Year,rtcGetParam.s32Month,rtcGetParam.s32Day,rtcGetParam.s32Hour,rtcGetParam.s32Minute,rtcGetParam.s32Second,aWeekdays[rtcGetParam.s32Weekday],rtcGetParam.s32Weekday);
	
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetOsalTimerNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_014

* DESCRIPTION :  
				   1).Call the Get Time API passing OSAL_NULL.
				      The API is expected to fail with error code
				      OSAL_E_INVALIDVALUE.
				      If either the API passes(critical error), or the
				      error code is different,report the return error code
				      of failure.
				   2).Return the error code. 
				   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32GetOsalTimerNULL( tVoid )
{
	/*Declaration*/
	tU32 u32Ret = 0;

	/*Attempt to Get Time field without a previous set*/
	if( OSAL_ERROR == OSAL_s32ClockGetTime( OSAL_NULL ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 500;
		}
	}
	else
	{
		/*Critical Error!*/
		u32Ret += 10000;
	}
	
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CyclePhaseTimeCheck()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_015

* DESCRIPTION :  
				   1). Create Timer(use periodic timer) with valid parameters.
				   2). Get the Elapsed Time.
					   Store this Time in an array.
				   3). Set the Timer.
				   4). Wait in the main until timer has executed Twice.
				       In the Callback, increment a counter
					   This counter is the index for an array of
					   OSAL_tMSecond.
				   2). Delete the Timer from the system.
				   3). Return error code( Incase create,
				       delete,Set timer fails)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CyclePhaseTimeCheck( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tTimerHandle tmHandle    = 0;
	OSAL_tpfCallback Callback     = OSAL_NULL;
	OSAL_tMSecond au32TimeData[3] = {0};
	OSAL_tMSecond u32PhaseTime    = 0;
	OSAL_tMSecond u32CycleTime    = 0;

	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)CyclePhaseCheck;

	/*Create an timer within the system*/
	if( OSAL_ERROR == OSAL_s32TimerCreate( 
	                             		   Callback,
	                             		   au32TimeData, 
	                             		   &tmHandle ) )
	{
		/*Creation of Timer failed!*/
		u32Ret = 1;
	}
	else
	{
		au32TimeData[0] = OSAL_ClockGetElapsedTime( );
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tmHandle,PHASE_TIME,CYCLE_TIME ) )
		{
			/*Set Time failed!*/
			u32Ret += 1000;
		}

		/*Busy Wait*/
		while( 3 > u32Counter )
		{
			OSAL_s32ThreadWait( DELAY );
		}

		/*Shutdown activities - Delete the timer*/
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandle ) )
		{
			u32Ret += 10;
		}
	}

	/*Phase Time*/
	if( au32TimeData[1] > au32TimeData[0] )
	{
		u32PhaseTime = au32TimeData[1] - au32TimeData[0];
	}
	/*Cycle Time*/
	if( au32TimeData[2] > au32TimeData[1] )
	{
		u32CycleTime = au32TimeData[2] - au32TimeData[1];
	}

	/*Cycle Time check*/
	if( ( u32CycleTime < ( CYCLE_TIME - TOLERANCE_TIME ) ) ||
	    ( u32CycleTime > ( CYCLE_TIME + TOLERANCE_TIME ) ) )
	{
		/*Cycle Time had been set to PHASE_TIME*/
		u32Ret += 1000;
	}

	/*Phase Time Check*/
	if( ( u32PhaseTime > ( PHASE_TIME + TOLERANCE_TIME ) ) ||
	    ( u32PhaseTime < ( PHASE_TIME - TOLERANCE_TIME ) )  )
	{
		u32Ret += 2000;
	}

	/*Reinitialize Counter*/
	u32Counter = 1;

	/*Return the error code*/
	return u32Ret;
} 

/*****************************************************************************
* FUNCTION    :	   vCounterCallBack_()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCounterCallBack_( tPVoid pArg )
{
	/*Update the counter*/
	tU32 *pInt    	= OSAL_NULL;
    tU32 u32Index 	= 0;

	/*Collect the inputs*/
	pInt  = ( tU32* )pArg;
	*pInt = *pInt+1;
			  
	if( *pInt == CALLBACK_COUNT )
	{
		/*Calculate index*/
		u32Index = (tU32)( pInt - u32CounterArray );

		/*In case of rollback*/
		if( !u8LimitCheck[u32Index] )
		{
			/*Find Elapsed time*/
			u32EndElapsedTime[u32Index] = OSAL_ClockGetElapsedTime( );
	 
			/*Set the Limit check to indicate to the corresponding thread
			that that thread may post to the main thread*/
			u8LimitCheck[u32Index]      = 1;
		}
	}

}
 
/*****************************************************************************
* FUNCTION    :	   vPerformanceThread()
* PARAMETER   :    Pointer to unsigned 32 bit integer
* RETURNVALUE :    none
* DESCRIPTION :    Thread to Load the system			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vPerformanceThread( tVoid* pvArg )
{
	/*Declarations*/
	OSAL_tpfCallback Callback     = OSAL_NULL;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	/*Initialize the callback*/
	Callback = (OSAL_tpfCallback)vCounterCallBack_;

	if( OSAL_ERROR != OSAL_s32TimerCreate( 
	                             		   	Callback,
	                             		   	&u32CounterArray[globIndex], 
	                             		   	&tmHandleArray[globIndex] ) )
	{
		/*Find Elapsed time*/	
		u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );

		/*Start Timer*/
		OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TWENTY_MILLI_SECONDS );
	} 

	/*Post on the Semaphore*/
	OSAL_s32SemaphorePost( globalIndexsemHandle );
	 	
	/*Wait to see if main thread is ready to terminate the spawned threads
	This code is to keep the system load going on*/
	//while( !u8Multicast )
	{
		//OSAL_s32ThreadWait( LOAD_WAIT );
	}
	
   /*wait till all threads are exited with time out for 100 secs*/
	OSAL_s32EventWait( hEvSyncTimerMT,
			                				  EVENT_TIMER_MT,
											  OSAL_EN_EVENTMASK_AND,
											  100000,
							                  OSAL_NULL ) ; 


	/*Wait so that the main thread may actually delete the threads*/
	//OSAL_s32ThreadWait( DELAY );

}

/*****************************************************************************
* FUNCTION    :	   u32TimerPerformanceMT_Time()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    Experimental
* DESCRIPTION :    Create 30 timers in 30 threads and verify drift.
				   Drift :
				   When system gets loaded, a deviation may be noted in the
				   effective time taken by the timer to execute a callback 'n'			       	
				   number of times from the actual time.
				   Such a drift is undesirable to an application developer and
				   it has to be within a tolerance range of 0 - 1.If not,it is
				   a timer error.
				   
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tU32 u32TimerPerformanceMT_Time( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				   = 0;
	tU32 u32LoopCount              = 0;
	/*CALLBACK_COUNT Timer Callback calls will be made*/
	tU32 u32ExactTimerTime         = EIGHT_MILLI_SECONDS+TWENTY_MILLI_SECONDS*( CALLBACK_COUNT - 1 );
	tU32 u32ActualTimerTime        = 0;
	tChar acBuf[MAX_LENGTH]        = {"\0"};
	OSAL_trThreadAttribute trAtr[LOAD];

	/*Initilaise the array of thread attributes*/
	OSAL_pvMemorySet(trAtr,0,sizeof(trAtr));

	/*Create a Wait Post Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
							 					"SYNC_GLOB_INDEX_SEM",
				    							 &globalIndexsemHandle,
							                     0
						                     ) )
	{																	   
		/*Return error*/
		return 2;
	}
	if( OSAL_ERROR == OSAL_s32EventCreate("TimerMTEve",&hEvSyncTimerMT) )
	{
		return 3;
	}


	while( u32LoopCount < LOAD )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Timer_Thread_%u",(unsigned int)u32LoopCount );
		
		/*Fill in the Thread Attribute structure*/
		trAtr[u32LoopCount].pvArg        = OSAL_NULL;
		trAtr[u32LoopCount].s32StackSize = 2048;/*2 KB Minimum*/
		trAtr[u32LoopCount].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr[u32LoopCount].pfEntry      = vPerformanceThread;
		trAtr[u32LoopCount].szName       = acBuf;
				  
		/*Spawn*/
		if( OSAL_ERROR == ( tID[u32LoopCount] = OSAL_ThreadSpawn( &trAtr[u32LoopCount] ) ) )
		{
			/*Incase of error*/
			u32Ret = u32Ret + ( u32LoopCount+1 )*100;
		}
				
		/*Wait on the Global index sync semaphore variable*/
		OSAL_s32SemaphoreWait( globalIndexsemHandle,OSAL_C_TIMEOUT_FOREVER );

		/*Increment the Global Counter*/
		globIndex++;

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

		/*Increment Counter*/
		u32LoopCount++;	
	}
	
    /*Wait until all timers have reached the timer count*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		while( u8LimitCheck[u32LoopCount] != 1 )
		{
		}	 
		u32LoopCount++;
	}


    /*Initialize loop count*/
	u32LoopCount = 0;
	/*Delete all the timers from the system*/
	while( u32LoopCount < LOAD )
	{
		if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandleArray[u32LoopCount] ) )
		{
			u32Ret += 1000;
		}

		u32LoopCount++;
	}

	
    /*Initialize loop count*/
	u32LoopCount = 0;
	/*Say main thread is ready to exit the threads spawned with entry - vPerformanceThread */
	//u8Multicast = 1;
	OSAL_s32EventPost( hEvSyncTimerMT,EVENT_TIMER_MT,OSAL_EN_EVENTMASK_OR );
	

   /*rav8kor - commented as threads has to gracefully exit but not deleted*/
   /*	
	while( u32LoopCount < LOAD )
	{
		
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID[u32LoopCount] ) )
		{
			u32Ret = u32Ret + ( u32LoopCount+1 )*100;
		}

		
		u32LoopCount++;
	}*/

	/*Close the Semaphore and Remove it*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose(  globalIndexsemHandle ) )
	{
		/*Remove the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "SYNC_GLOB_INDEX_SEM" ) )
		{
			u32Ret += 150;
		}
	}
	else
	{
		u32Ret += 250;
	}

	if( OSAL_ERROR == OSAL_s32EventClose( hEvSyncTimerMT ) )
	{
		/*Event Close should not fail*/
		u32Ret += 20;
		
	}
	else
	{

		if( OSAL_ERROR == OSAL_s32EventDelete( "TimerMTEve" ) )
		{
			/*Event delete failed*/
			u32Ret += 40;
			
		}
	}



	/*Drift Computation*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		if( u32EndElapsedTime[u32LoopCount] > u32StartElapsedTime[u32LoopCount] ) 
		{
			u32ActualTimerTime = u32EndElapsedTime[u32LoopCount] - u32StartElapsedTime[u32LoopCount];
		}
		else
		{
			u32ActualTimerTime = ( 0xFFFFFFFF - u32StartElapsedTime[u32LoopCount] )	+ u32EndElapsedTime[u32LoopCount]+1;
		}

		 /*Trace out the drift*/	  
		OEDT_HelperPrintf( TR_LEVEL_USER_1, 
		     	           "Drift time( in milli seconds ) : %u\n",
		                   	u32ActualTimerTime-u32ExactTimerTime );
		/*Calculate the Drift*/
		s32Drift[u32LoopCount] = (tS32)( u32ActualTimerTime-u32ExactTimerTime );
	   	/*Increment the Loop*/
		u32LoopCount++;
	}


	/*Query Drift*/
	/*rav8kor - the drift can be max 2ms as there is a 1msec uncertainity while measuurement is 
	started and ended*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		if( ( tS32 )-2 <= s32Drift[u32LoopCount] )
		{
			if( !( s32Drift[u32LoopCount] <= ( tS32 )2 ) )
			{
				u32Ret += 5;
			}
		}
		else
		{
			u32Ret += 10;
		}

		/*Increment the counter*/
		u32LoopCount++;
	}
	
	/*Reinitialize all the global variables*/
	/*Thread ID,Timer Handle,Counter,Start Elapsed Time,End Elapsed Time,Drift,Limit Check*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		tID[u32LoopCount] 					= 0;
		tmHandleArray[u32LoopCount] 		= 0;
		u32CounterArray[u32LoopCount] 		= 0;
		u32StartElapsedTime[u32LoopCount] 	= 0;
		u32EndElapsedTime[u32LoopCount] 	= 0;
		s32Drift[u32LoopCount] 				= 0;
		u8LimitCheck[u32LoopCount] 			= 0;

		u32LoopCount++;
	}
	
	/*Reinitialize global variables*/
	globalIndexsemHandle   			  = 0;
    globIndex                         = 0;
    u8Multicast                       = 0;

	/*Wait until threads have finished*/
	OSAL_s32ThreadWait( WAIT_FOR_DELETE << 1 );
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32TimerPerformanceST_Time()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    Experimental
* DESCRIPTION :    Create 30 timers and verify drift.
				   Drift :
				   When system gets loaded, a deviation may be noted in the
				   effective time taken by the timer to execute a callback 'n'			       	
				   number of times from the actual time.
				   Such a drift is undesirable to an application developer and
				   it has to be within a tolerance range of 0 - 1.If not,it is
				   a timer error.
				   
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
#define OEDT_TIMER_10S 10
tU32 u32TimerPerformanceST_Time( tVoid )
{
	/*Declarations*/
	OSAL_tpfCallback Callback      = OSAL_NULL;
	tU32 u32Ret 				   = 0;
	tU32 u32LoopCount              = 0;
	/*CALLBACK_COUNT Timer Callback calls will be made*/
	tU32 u32ExactTimerTime         = EIGHT_MILLI_SECONDS+TWENTY_MILLI_SECONDS*( CALLBACK_COUNT - 1 );
	tU32 u32ActualTimerTime        = 0;
	/*Initialize the callback*/
	Callback 					   = (OSAL_tpfCallback)vCounterCallBack_;

 	

	/*Create the timers int he system*/
	while( globIndex < LOAD )
	{
		if( OSAL_ERROR != OSAL_s32TimerCreate( 
	    	                         		   	Callback,
	        	                     		   	&u32CounterArray[globIndex], 
	            	                 		   	&tmHandleArray[globIndex] ) )
		{
			/*Find Elapsed time*/	
			u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );

			/*Start Timer*/
			OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TWENTY_MILLI_SECONDS );
		}

		/*Increment the global counter*/
		globIndex++;
	}

 

	/*Wait until all timers have reached the timer count*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		while( u8LimitCheck[u32LoopCount] != 1 )
		{
			/*Wait and enter delayed state*/
			OSAL_s32ThreadWait( OEDT_TIMER_10S );	 
		}
		u32LoopCount++;
	}

	/*Delete all the timers from the system*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		if( OSAL_ERROR != (tS32)tmHandleArray[u32LoopCount] )
		{
			if( OSAL_ERROR == OSAL_s32TimerDelete( tmHandleArray[u32LoopCount] ) )
			{
				u32Ret += 30;
			}
		}
		u32LoopCount++;
	}

	/*Drift Computation*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		if( u32EndElapsedTime[u32LoopCount] > u32StartElapsedTime[u32LoopCount] ) 
		{
			u32ActualTimerTime = u32EndElapsedTime[u32LoopCount] - u32StartElapsedTime[u32LoopCount];
		}
		else
		{
			u32ActualTimerTime = ( 0xFFFFFFFF - u32StartElapsedTime[u32LoopCount] )	+ u32EndElapsedTime[u32LoopCount]+1;
		}

		 
		OEDT_HelperPrintf( TR_LEVEL_USER_1, 
		     	           "Drift time( in milli seconds ) : %u\n",
		                   u32ActualTimerTime-u32ExactTimerTime );

		s32Drift[u32LoopCount] = (tS32)( u32ActualTimerTime-u32ExactTimerTime );
		
		u32LoopCount++;
	}

	/*Query Drift*/
	/*rav8kor - the drift can be max 2ms as there is a 1msec uncertainity while measuurement is 
	started and ended*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		if( ( tS32 )-2 <= s32Drift[u32LoopCount] )
		{
			if( !( s32Drift[u32LoopCount] <= ( tS32 )2 ) )
			{
				u32Ret += 5;
			}
		}
		else
		{
			u32Ret += 10;
		}

		/*Increment the counter*/
		u32LoopCount++;
	}

	/*Reinitialize Parameters*/
	u32LoopCount = 0;
	while( u32LoopCount < LOAD )
	{
		s32Drift[u32LoopCount] = 0;
		u32StartElapsedTime[u32LoopCount] = 0;
		u32EndElapsedTime[u32LoopCount] = 0;
		tmHandleArray[u32LoopCount] = 0;
		u32CounterArray[u32LoopCount] = 0;
		u8LimitCheck[u32LoopCount] = 0;

		u32LoopCount++;
	}

	globIndex                         = 0;

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   vCriticalCounterCallBack_()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCriticalCounterCallBack_( tPVoid pArg )
{
	/*Update the counter*/
	tU32 *pInt    	= ( tU32* )pArg;
    /*Increment the counter*/
	*pInt = *pInt+1;
}
/*****************************************************************************
* FUNCTION    :	   u32TimerPerformanceST_Counter()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_016
* DESCRIPTION :    Create 30 timers and verify drift.				 
				   Drift computation is based on counter mechanism.
				   				   
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tU32 u32TimerPerformanceST_Counter( tVoid )
{
	/*Declarations*/
	OSAL_tpfCallback Callback       			  = OSAL_NULL;
	tU32 u32Ret 				     			  = 0;
	tU32 u32Variance                              = 0;
	tDouble tdPredictSetTimePart   	 			  = 0.0;
	tDouble tdPredictDelTimePart    			  = 0.0;
	tDouble tdVariance                            = 0.0;
	OSAL_tMSecond u32StartSetTime   			  = 0;
    OSAL_tMSecond u32EndSetTime     			  = 0;
	OSAL_tMSecond u32StartDelTime                 = 0;
	OSAL_tMSecond u32EndDelTime                   = 0;
	OSAL_tMSecond u32PredictSetTime               = 0;
	OSAL_tMSecond u32PredictDelTime               = 0;
	OSAL_tMSecond u32PredictedTimeForTimer[LOAD]  = {0};
	OSAL_tMSecond u32PredictedCountForTimer[LOAD] = {0};
	/*Initialize the callback*/
	Callback 					    			  = (OSAL_tpfCallback)vCriticalCounterCallBack_;

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Create all timers*/
	while( globIndex < LOAD )
	{
		/*Create a Timer*/
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	    		                         	 	Callback,
	        		                     		&u32CounterArray[globIndex], 
	            		                 		&tmHandleArray[globIndex] ) )
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment global index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;


	/** Predict time for triggering all the timers, for using in future **/
	/*Capture the elapsed time now*/
	u32StartSetTime   = OSAL_ClockGetElapsedTime( );
	/*Trigger all the timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );
		/*Increment global counter*/
		globIndex++;
	}
	/*Capture the elapsed time now*/
	u32EndSetTime     = OSAL_ClockGetElapsedTime( );
	/*Predict set time*/
	u32PredictSetTime = u32EndSetTime - u32StartSetTime;
	/** Predict time for triggering all the timers, for using in future **/	

	/*Initialize the Global counter*/
	globIndex    = 0;

	/** Predict time for deleting all the timers, for using in future **/
	/*Capture the elapsed time now*/
	u32StartDelTime   = OSAL_ClockGetElapsedTime( );
	/*Delete all the timers*/
	while( globIndex < LOAD )
	{
		/*Delete Timer*/
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		/*Increment global counter*/
		globIndex++;
	}
	/*Capture the elapsed time now*/
	u32EndDelTime     = OSAL_ClockGetElapsedTime( );
	/*Predict delete time*/
	u32PredictDelTime = u32EndDelTime - u32StartDelTime;
	/** Predict time for deleting all the timers, for using in future **/

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Initialize Timer Handle array and counter array*/
	while( globIndex < LOAD )
	{
		tmHandleArray[globIndex]   = 0;
		u32CounterArray[globIndex] = 0;

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Create all timers*/
	while( globIndex < LOAD )
	{
		/*Create a Timer*/
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	    		                         	 	Callback,
	        		                     		&u32CounterArray[globIndex], 
	            		                 		&tmHandleArray[globIndex] ) )
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment global index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Compute Average for one Predicted Set Time and one Predicted Del Time*/
	tdPredictSetTimePart = ( tDouble )u32PredictSetTime/( tDouble )LOAD;
	tdPredictDelTimePart = ( tDouble )u32PredictDelTime/( tDouble )LOAD;

	/*Update the Predicted time of run for each timer*/
	while( globIndex < LOAD )
	{
		/*Get the variance*/
		tdVariance = ( ( LOAD - ( globIndex + 1 ) )*tdPredictSetTimePart )+ ( globIndex*tdPredictDelTimePart );

		if(	( tDouble )( tdVariance - ( tU32 )tdVariance ) > 0.5 )
		{
			u32Variance = ( tU32 )tdVariance + 1;
		}
		else
		{
			u32Variance = ( tU32 )tdVariance;
		}

		u32PredictedTimeForTimer[globIndex] = WAIT_TIME + u32Variance;

		globIndex++;
		u32Variance = 0;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Update the Predicted count for each timer*/
	while( globIndex < LOAD )
	{
		u32PredictedCountForTimer[globIndex] = ( u32PredictedTimeForTimer[globIndex] - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS + 1;
		globIndex++;
	}
	
	/*Initialize the Global counter*/
	globIndex    = 0;		 

	/*Trigger all the timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );
		/*Increment global counter*/
		globIndex++;
	}

	/*Wait until the couter is ready*/
	OSAL_s32ThreadWait( WAIT_TIME );

   
	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Delete all the Timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		/*Increment global counter*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Verify the Counters*/
	while( globIndex < LOAD )
	{
		if( u32PredictedCountForTimer[globIndex] != u32CounterArray[globIndex] )
		{
			u32Ret += 1;
		}

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Initialize Timer Handle array and counter array*/
	while( globIndex < LOAD )
	{
		tmHandleArray[globIndex]   = 0;
		u32CounterArray[globIndex] = 0;

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   vCriticalCounterCallBack__()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCriticalCounterCallBack__( tPVoid pArg )
{
	/*Definition*/
	tU32* pInt    = ( tU32* )pArg;
	tU32 u32Index = (tU32)(pInt - u32CounterArray);

	/*Increment*/
	if( !u32EndElapsedTime[u32Index] )
	{
		*pInt = *pInt + 1;
	}

	/*Check*/
	if( u8TimerSwitch )
	{
		/*Calculate the End elapsed time*/
		u32EndElapsedTime[u32Index] = OSAL_ClockGetElapsedTime( );	
	}
}

/*****************************************************************************
* FUNCTION    :	   vPerformanceThread_()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread to Load the system			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vPerformanceThread_( tPVoid pArg )
{
 	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pArg);
 	/*Find Elapsed time*/	
	u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );

	/*Start Timer*/
	OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );

	/*Post on semaphore*/
	OSAL_s32SemaphorePost( globalIndexsemHandle );

	/*Keep the system load continued*/
	while( u8TimerSwitch != 1 )
	{
		OSAL_s32ThreadWait( LOAD_WAIT );
	}

	/*Do a wait also for the end elapsed time to be over*/
	while( !u32EndElapsedTime[globIndex] )
	{
		OSAL_s32ThreadWait( LOAD_WAIT );
	}

	/*Wait for 4 seconds*/
        /*	OSAL_s32ThreadWait( WAIT_FOR_DELETE );	 	*/
        /* Dont wait here */
}

/*****************************************************************************
* FUNCTION    :	   u32TimerPerformanceMT_Counter()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_017
* DESCRIPTION :    Create 30 timers in 30 threads and verify drift.
				   in terms of updates of the counter value, incremented
				   in the Timer callback, with the elapsed time as base.
				   
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tU32 u32TimerPerformanceMT_Counter( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				 = 0;
	OSAL_tpfCallback Callback    = OSAL_NULL;
	tChar acBuf[MAX_LENGTH]      = {"\0"};
	tU32 u32CountForTimers[LOAD] = {0};
	OSAL_trThreadAttribute trAtr[LOAD];

	/*Initilaise the array of thread attributes*/
	OSAL_pvMemorySet(trAtr,0,sizeof(trAtr));

	/*Initialize the Global counter*/
	globIndex 					= 0;
	/*Initialize the callback*/
	Callback 					= (OSAL_tpfCallback)vCriticalCounterCallBack__;


	/*Create a Wait Post Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
							 					"SYNC_GLOB_INDEX_SEM",
				    							 &globalIndexsemHandle,
							                     0
						                     ) )
	{																	   
		/*Return error*/
		return 2;
	}

	/*Create all the timers in the system*/
	while( globIndex < LOAD )
	{
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	        	  	                     		Callback,
	                	             			&u32CounterArray[globIndex], 
	                    	         			&tmHandleArray[globIndex] ) )	
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment the Global Index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Spawn Threads*/
	while( globIndex < LOAD )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Timer_Thread_%u",(unsigned int)globIndex );
		
		/*Fill in the Thread Attribute structure*/
		trAtr[globIndex].pvArg        = OSAL_NULL;
		trAtr[globIndex].s32StackSize = 2048;/*2 KB Minimum*/
		trAtr[globIndex].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr[globIndex].pfEntry      = vPerformanceThread_;
		trAtr[globIndex].szName       = acBuf; 
		
		/*Spawn the thread*/
		tID[globIndex] = OSAL_ThreadSpawn(  &trAtr[globIndex] );

	   	/*Wait on semaphore*/
		OSAL_s32SemaphoreWait( globalIndexsemHandle,OSAL_C_TIMEOUT_FOREVER );

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

	   	/*Increment the Global Index*/
		globIndex++;
	} 

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Wait until wait Time*/
	OSAL_s32ThreadWait( WAIT_TIME );
	
	/*Initialize switch - Relieve Thread Load*/
	u8TimerSwitch = 1;

	/*Wait until end elapsed times are updated*/
	while( globIndex < LOAD )
	{
		for(;;)
		{
			/*Wait until wait Time*/
			/* with out the below statement (Wait), the release version has the instruction 
			"beq v0,zero,0x9D070" which points to the same location. this is like endless loop*/
		        OSAL_s32ThreadWait( 0 );
		  	if((u32EndElapsedTime[globIndex]))
			{
			  break;
			}
 	
		} 	

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Delete Timers*/
	while( globIndex < LOAD )
	{
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Delete all the Threads*/
	while( globIndex < LOAD )
	{
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID[globIndex] ) )
		{
			u32Ret = u32Ret + ( globIndex + 1 )*100;
		}

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Compute the count values for every timer using the elapsed time*/
	while( globIndex < LOAD )
	{
		/*Compute the Count based on elapsed time*/
		if(  u32EndElapsedTime[globIndex] > u32StartElapsedTime[globIndex] )
		{
			u32CountForTimers[globIndex] = ( ( u32EndElapsedTime[globIndex] - u32StartElapsedTime[globIndex] ) - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS +1;
		}
		else
		{
			u32CountForTimers[globIndex] = 	( ( ( 0xFFFFFFFF - u32StartElapsedTime[globIndex] )	+ u32EndElapsedTime[globIndex]+1 ) - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS +1;
		}
			
		/*Increment counter*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Check counter increments*/
	while( globIndex < LOAD )
	{
		/*Check counter values*/
		if( u32CountForTimers[globIndex] != u32CounterArray[globIndex] )
		{
			u32Ret += 1;
		}

		/*Increment the count*/
		globIndex++;
	} 

	/*Close the Semaphore and Remove it*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose(  globalIndexsemHandle ) )
	{
		/*Remove the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "SYNC_GLOB_INDEX_SEM" ) )
		{
			u32Ret += 150;
		}
	}
	else
	{
		u32Ret += 250;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Reinitialize array*/
	while( globIndex < LOAD )
	{
		u32StartElapsedTime[globIndex] = 0;
		u32EndElapsedTime[globIndex]   = 0;
		tmHandleArray[globIndex] 	   = 0;
		u32CounterArray[globIndex] 	   = 0;

		globIndex++;
	}

	/*Initialize the Global counter and Timer Switch*/
	globIndex 					= 0;
	u8TimerSwitch               = 0;


	/*Wait until threads have finished*/
	OSAL_s32ThreadWait( WAIT_FOR_DELETE << 1 );
        
	/*Return the error code*/
	return u32Ret;
}
/*****************************************************************************
* FUNCTION    :    u32ChangeSystemTimeTest()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :
* DESCRIPTION :    set time to x, create timer with callback in 10s.
*                  While timer is running set system time to x+5s.
*                  Callback should end after 10s.

* HISTORY     :    Created by Dainius Ramanauskas(CM-AI/PJ-CF31)  Juni 01,2011
*******************************************************************************/
tU32 u32ChangeSystemTimeTest(tVoid)
{
   tU32 u32Ret = 0;

   u32Ret = u32SetTime();

   u32Ret = u32CreateTimer();

   return u32Ret;
}
