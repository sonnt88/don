/******************************************************************************
 *FILE         : oedt_osalcore_FPE_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file defines the  test cases for the testing
 *               the FPE OSAL API.
 *               
 *AUTHOR       : Ravindran P(RBEI/ECF1)
 *
 *
 *COPYRIGHT    : 2011, Robert Bosch Engg and Business Solutions India Limited
 *
 *HISTORY      : Version 1.0 , 31 - 01- 2011
 *               Version 1.1 , 08/08/2011 - Anooj Gopi (RBEI/ECF1)
 *               - Added abort mode tests. Updated dual-thread tests.
 *
 *****************************************************************************/

#ifndef OEDT_OSAL_CORE_FPE_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_FPE_TESTFUNCS_HEADER

#define FP_RESULT_MLT     0x0001
#define FP_RESULT_MLT_EXT 0x0002


/*Function declarations*/

tU32 u32CheckFPZeroDivAbort(tVoid);

tU32 u32CheckFPOverflowAbort(tVoid);

tU32 u32CheckFPUnderflowAbort(tVoid);

tU32 u32CheckFPInvalidAbort(tVoid);

tU32 u32CheckFPZeroDivNoAbort(tVoid);

tU32 u32CheckFPOverflowNoAbort(tVoid);

tU32 u32CheckFPUnderflowNoAbort(tVoid);

tU32 u32CheckFPInvalidNoAbort(tVoid);

tU32 u32CheckResetFPEFlag(tVoid);

tU32 u32CheckResetMultithreads(tVoid);

tU32 u32CheckMultipleFPEFlags(tVoid);

tU32 u32CheckFPEBitSetMultithreaded(tVoid);

tU32 u32CheckFPEModeChange(tVoid);

tU32 u32CheckFPEModeMultithreaded(tVoid);

#if 0
tU32 u32CheckDefaultFPEMode(tVoid);
#endif

#endif
