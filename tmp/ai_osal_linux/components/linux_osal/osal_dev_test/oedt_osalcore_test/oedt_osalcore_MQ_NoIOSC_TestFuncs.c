/******************************************************************************
 *FILE         : oedt_osalcore_MQ_NoIOSC_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the MESSAGE QUEUE (NO IOSC) OSAL API.
 *               
 *AUTHOR       : Anoop Chandran (RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *			                copy of oedt_osalcore_MQ_TestFuncs.c
 
 *             : 13.12.2012 sja3kor Added Oedt u32MQnoIOSCPostWaitMsgPriowise().
 *****************************************************************************/


/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_osalcore_MQ_common_TestFuncs.h"
#include "oedt_osalcore_MQ_NoIOSC_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
tU32 u32Tr1Ret2;
tU32 u32Tr2Ret2;
tU32 u32Tr1NtRet2;
tU32 u32Tr2NtRet2;
tU32 u32Tr3NtRet2;
OSAL_tEventHandle MQEveHandle_1;
OSAL_tEventHandle MQEveHandle_2;
extern OSAL_tMQueueHandle MQHandle;

OSAL_tSemHandle hbaseSem;
extern MessageQueueTableEntry mesgque_StressList[MAX_MESSAGE_QUEUES];
tU32 u32GlobMsgCount;
extern OSAL_trThreadAttribute msgqpost_ThreadAttr[NO_OF_POST_THREADS];
extern OSAL_trThreadAttribute msgqwait_ThreadAttr[NO_OF_WAIT_THREADS];
extern OSAL_trThreadAttribute msgqclose_ThreadAttr[NO_OF_CLOSE_THREADS];
extern OSAL_trThreadAttribute msgqcreate_ThreadAttr[NO_OF_CREATE_THREADS];
extern OSAL_tThreadID msgqpost_ThreadID[NO_OF_POST_THREADS];
extern OSAL_tThreadID msgqwait_ThreadID[NO_OF_WAIT_THREADS];
extern OSAL_tThreadID msgqclose_ThreadID[NO_OF_CLOSE_THREADS];
extern OSAL_tThreadID msgqcreate_ThreadID[NO_OF_CREATE_THREADS];
extern tU32 u32MQWaitStatus( tVoid );
extern tU32 u32MQOpenStatus( tVoid );
extern tU32 u32MQCreateStatus( tVoid );
extern tU32 u32MQCloseStatus( tVoid );
extern tU32 u32MQDeleteStatus( tVoid );
extern tU32 u32MQPostStatus( tVoid );
OSAL_tEventMask    gevMask2;

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/
/******************************************************************************
 *FUNCTION		:u32NoIoscMQThread1()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32NoIoscMQThread1(tPVoid pvArg)
{
   tS32 s32Count				           = 0;
   OSAL_tMQueueHandle MQHandle2             = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];

   u32Tr1Ret2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   OSAL_pvMemorySet(MQ_msg,0,sizeof(MQ_msg));
   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
   	     OSAL_ERROR 
   	     != 
   	     OSAL_s32MessageQueueOpen
   	     ( 
   	        IOSCMESSAGEQUEUE_NAME,
   	        OSAL_EN_WRITEONLY,
   	        &MQHandle2 
   	     ) 
   	  ) 
   {
	  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
	  {
         OSAL_s32PrintFormat
         ( 
            MQ_msg[s32Count], 
            "MQ_MSG%d",
            MQ_PRIO2 
         );
	  
	     /*Post a message into the message queue */
		 if ( 
		       OSAL_ERROR 
		       == 
		       OSAL_s32MessageQueuePost
		       ( 
		          MQHandle2,
		          (tPCU8)MQ_msg[s32Count],
				  MAX_LEN,
				  (tU32)s32Count 
			   ) 
			) 
		 {

			u32Tr1Ret2 += (10 + u32MQPostStatus ()) ;

		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
		 OEDT_HelperPrintf
		 (  
		    TR_LEVEL_USER_1, 
		    "Message Send = %d\t%s\n", 
		    s32Count, 
		    MQ_msg[s32Count] 
		 );

	  }//end of  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )

	  if (
	        OSAL_ERROR 
	        == 
	        OSAL_s32MessageQueueClose
	        ( 
	           MQHandle2 
	        ) 
	     )
	  {
		 
		 /* Check the error status */
		 u32Tr1Ret2 += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {

	  u32Tr1Ret2 += (30 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if (
         OSAL_ERROR 
         == 
         OSAL_s32EventPost
         ( 
            MQEveHandle_1,
            MQ_EVE1,
            OSAL_EN_EVENTMASK_OR 
         ) 
      ) 
   {
      u32Tr1Ret2 += 100;
   }
//   OSAL_s32ThreadWait(800);
   OSAL_vThreadExit();
}

/******************************************************************************
 *FUNCTION		:u32NoIoscMQThread2()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32NoIoscMQThread2(tPVoid pvArg)
{
   tS32 s32Count				           = 0;
   OSAL_tMQueueHandle MQHandle2             = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];

   u32Tr2Ret2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   OSAL_pvMemorySet(MQ_msg,0,sizeof(MQ_msg));

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
		 OSAL_ERROR 
		 != 
		 OSAL_s32MessageQueueOpen
		 ( 
		  	IOSCMESSAGEQUEUE_NAME, 
			OSAL_EN_READONLY,
			&MQHandle2
		 )
	  ) 
   {
	  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
	  {
  
	     /*Post a message into the message queue */
	     if (
			   MQ_WAIT_RETURN_ERROR
			   == 
			   OSAL_s32MessageQueueWait
			   (
			      MQHandle2,
			      (tPU8)MQ_msg[ s32Count ],
			      MAX_LEN,
			      OSAL_NULL,
			      (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER
			   )
		    )
	     {
		    u32Tr2Ret2 += (10 + u32MQWaitStatus ()) ;

	     }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 
		 OEDT_HelperPrintf
		 ( 
		    TR_LEVEL_USER_1, 
		    "Message Received = %d\t%s\n", 
		    s32Count, 
		    MQ_msg[ s32Count ] 
		 );
	  
	  }//end of  for( ; MQ_COUNT_MAX > u32Count; ++u32Count )

	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueClose
			( 
			   MQHandle2
			)
		 )
	  {
		 
		 /* Check the error status */
		 u32Tr2Ret2 += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {

	  u32Tr2Ret2 += (30 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32EventPost
		 ( 
		  	MQEveHandle_1,
			MQ_EVE2,
			OSAL_EN_EVENTMASK_OR
		 )
	  ) 
   {
      u32Tr2Ret2 += 100;
   }
//   OSAL_s32ThreadWait(800);
   OSAL_vThreadExit();


}
/******************************************************************************
 *FUNCTION		:u32NoIoscMQThreadNoIoscNotify1()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32NoIoscMQThreadNoIoscNotify1(tPVoid pvArg)
{
   OSAL_tMQueueHandle MQHandle2             = 0;
   tChar MQ_msg[ MQ_HARRAY ]              = MQ_MSG;

	u32Tr1NtRet2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
		 OSAL_ERROR 
		 != 
		 OSAL_s32MessageQueueOpen
		 ( 
		  	IOSCMESSAGEQUEUE_NAME, 
			OSAL_EN_WRITEONLY,
			&MQHandle2
		 )
	  ) 
   {
	  
	  /*Post a message into the message queue */
	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueuePost
			( 
			   MQHandle2, 
			   (tPCU8)MQ_msg,
			   MAX_LEN,
			   MQ_PRIO2 
			)
		 ) 
	  {
		 /* Check the error status */
		 u32Tr1NtRet2 = (10 + u32MQPostStatus ()) ;

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
	  OEDT_HelperPrintf
	  ( 
		 TR_LEVEL_USER_1, 
		 "Message Send = %s\n", 
		 MQ_msg
	  );

	  /*Close the message queue */

	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueClose
			( 
			   MQHandle2
			)
		 )
	  {
		 /* Check the error status */
		 u32Tr1NtRet2 += (20 + u32MQCloseStatus ()) ;
		 	

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {
	 /* Check the error status */
	  u32Tr1NtRet2 += (30 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32EventPost
		 ( 
		  	MQEveHandle_2,
			MQ_EVE1,
			OSAL_EN_EVENTMASK_OR
		 )
	  ) 
   {
      u32Tr1NtRet2 += 100;
   }
//   OSAL_s32ThreadWait(1000);
   OSAL_vThreadExit();


}

/******************************************************************************
 *FUNCTION		:vNoIoscMQCallBack()
 *
 *DESCRIPTION	:
 *				 a)callback for notification
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/

tVoid vNoIoscMQCallBack(tVoid)
{
   OEDT_HelperPrintf
   ( 
	  TR_LEVEL_USER_1, 
	  "This is the MQ Notify CallBack\n" 
   );
}

/******************************************************************************
 *FUNCTION		:u32NoIoscMQThreadNoIoscNotify2()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32NoIoscMQThreadNoIoscNotify2(tPVoid pvArg)
{
   tU32 u32status						   = 0;
   OSAL_tMQueueHandle MQHandle2             = 0;


   u32Tr2NtRet2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
		 OSAL_ERROR 
		 != 
		 OSAL_s32MessageQueueOpen
		 ( 
		  	IOSCMESSAGEQUEUE_NAME, 
			OSAL_EN_WRITEONLY,
			&MQHandle2
		 )
	  ) 
   {

	  /*Post a message into the message queue */
	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueNotify
			( 
			   MQHandle2, 
			   (OSAL_tpfCallback)vNoIoscMQCallBack,
			   OSAL_NULL
			)
		 ) 
	  {
		 /* Check the error status */
		 u32status =  OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
			case OSAL_E_BUSY:  
			   u32Tr2NtRet2 = 1;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Tr2NtRet2 = 2;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Tr2NtRet2 = 3;
			   break;
			default:
			   u32Tr2NtRet2 = 4;
		 }//end of switch( u32status )

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueNotify())

	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueClose
			( 
			   MQHandle2
			)
		 )
	  {
		 /* Check the error status */
		 u32Tr2NtRet2 += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {
	  /* Check the error status */
	  u32Tr2NtRet2 += (20 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32EventPost
		 ( 
		  	MQEveHandle_2,
			MQ_EVE2,
			OSAL_EN_EVENTMASK_OR
		 )
	  ) 
   {
      u32Tr2NtRet2 += 100;
   }
//   OSAL_s32ThreadWait(1000);
  OSAL_vThreadExit();


}
/******************************************************************************
 *FUNCTION		:u32NoIoscMQThreadNoIoscNotify3()
 *
 *DESCRIPTION	:
 *				 a)check the error status returned
 *
 *PARAMETER		:none
 *
 *RETURNVALUE	:tU32, status value in case of error
 *
 *HISTORY		:
 * 26.10.2007  Anoop Chandran (RBIN/EDI3)
 *				 Initial Revision.
 *****************************************************************************/
tVoid u32NoIoscMQThreadNoIoscNotify3(tPVoid pvArg)
{
   tU32 u32status						   = 0;
   OSAL_tMQueueHandle MQHandle2             = 0;


   u32Tr3NtRet2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

   if ( 
		 OSAL_ERROR 
		 != 
		 OSAL_s32MessageQueueOpen
		 ( 
		  	IOSCMESSAGEQUEUE_NAME, 
			OSAL_EN_WRITEONLY,
			&MQHandle2
		 )
	  ) 
   {

	  OSAL_s32ThreadWait(800);

	  /*Post a message into the message queue */
	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueNotify
			( 
			   MQHandle2, 
			   (OSAL_tpfCallback)vNoIoscMQCallBack,
			   OSAL_NULL
			)
		 ) 
	  {

		 u32status =  OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
			case OSAL_E_BUSY:  
			   u32Tr3NtRet2 = 0;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Tr3NtRet2 = 1;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Tr3NtRet2 = 2;
			   break;
			default:
			   u32Tr3NtRet2 = 3;

		 }//end of switch( u32status )

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueNotify())

	  if ( 
			OSAL_ERROR 
			== 
			OSAL_s32MessageQueueClose
			( 
			   MQHandle2
			)
		 )
	  {
		 
		 u32Tr3NtRet2 += (20 + u32MQCloseStatus ()) ;

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

   else
   {

	  u32Tr3NtRet2 += (100 + u32MQOpenStatus ()) ;

   }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
 
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32EventPost
		 ( 
		  	MQEveHandle_2,
			MQ_EVE3,
			OSAL_EN_EVENTMASK_OR
		 )
	  ) 
   {
      u32Tr2NtRet2 += 100;
   }
//   OSAL_s32ThreadWait(1000);
   OSAL_vThreadExit();


}

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCPostMsgExcMaxLen()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_001

* DESCRIPTION :  
				   1). Create Message Queue.
			       2). Try Posting a message size exceeding the max size 
				       of the message queue.
				   3). If posting successful, query the error code,
				       and update the returned error code.
				   4). Close the message queue.
			       5). Delete the message queue if close message queue 
			           successful
			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQuePostMsgExcMaxLen()
*
*******************************************************************************/
tU32  u32MQnoIOSCPostMsgExcMaxLen( tVoid )
{
   tU32 u32Ret                     = 0;
   tU32 u32status					= 0;
   OSAL_tMQueueHandle mqHandle     = 0;


   /*Create the Message Queue within the system*/
   if (
         OSAL_ERROR
         !=
         OSAL_s32MessageQueueCreate
         (
   			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			OSAL_EN_READWRITE,
			&mqHandle
		 )
	  )
   {
      /*Post a message into the message queue exceeding the defined
   	  maximum size for the message queue*/
	  if (
	        OSAL_ERROR
	        ==
	        OSAL_s32MessageQueuePost
	        (
	  		 mqHandle,
			 (tPCU8)MQ_MESSAGE_EXC_LEN,
			 MAX_EXC_LEN,
			 MQ_PRIO2
			)
		 )
	  {
		 /*Failure of Post - returned success for a length greater than
		 the message queue's limit*/
		 u32status =  OSAL_u32ErrorCode();
		 switch( u32status )
		 {
		    case OSAL_E_MSGTOOLONG:
			   u32Ret = 0;
			   break;
			case OSAL_E_QUEUEFULL:
			   u32Ret = 10;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret = 20;
			   break;
			case OSAL_E_NOPERMISSION:
			   u32Ret = 30;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 40;
			   break;
			case OSAL_E_BADFILEDESCRIPTOR:
			   u32Ret = 50;
			   break;
			default:
			   u32Ret = 60;
		 }//end of switch( u32status )
			/*Close the message queue*/
		 if (
			    OSAL_ERROR
			    ==
			    OSAL_s32MessageQueueClose
			    (
			       mqHandle
			    )
			)
		 {
		 	u32Ret += 70;
		 }//if (OSAL_ERROR == OSAL_s32MessageQueueClose())

		 /*Delete the message queue*/
		 if (
			   OSAL_ERROR
			   ==
			   OSAL_s32MessageQueueDelete
			   (
			      IOSCMESSAGEQUEUE_NAME
			   )
			)
		 {
			u32Ret += 80;

		 }//if (OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//if(OSAL_ERROR == OSAL_s32MessageQueuePost())

	  else
	  {
	     u32Ret  = 200;

	     /*Close the message queue*/
		 if (
		        OSAL_ERROR
			    ==
			    OSAL_s32MessageQueueClose
			    (
			 	   mqHandle
			 	)
			)
		 {
			u32Ret += 300;
		 }
		 /*Delete the message queue*/
		 if (
			   OSAL_ERROR
			   ==
			   OSAL_s32MessageQueueDelete
			   (
				  IOSCMESSAGEQUEUE_NAME
			   )
		   	)
		 {
		    u32Ret += 400;
		 }

	  }//if(OSAL_ERROR == OSAL_s32MessageQueuePost())

   }//	if (OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//	if -else(OSAL_ERROR != OSAL_s32MessageQueueCreate())


   /*Return the error code*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCCreateMsgQueWithHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_002

* DESCRIPTION :
				   1). Create Message Queue with handle set to NULL
				   2). If Message queue is created, set return error code,
				       close the message queue and delete it
				   3). If Message queue is not created, check for the expected
				       error code : OSAL_E_INVALIDVALUE.
					   If the expected error code is not returned, set return error
					   code.
				   4). Return the error code

* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32CreateMsgQueWithHandleNULL()
*
*******************************************************************************/
tU32  u32MQnoIOSCCreateMsgQueWithHandleNULL( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;


   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			OSAL_NULL
		 )
	  ) 
   {
	  /*Created a message queue for a NULL handle*/
	  u32Ret = 10;

	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR
	  	    ==
	  	    OSAL_s32MessageQueueClose
	  	    (
	  	       OSAL_NULL
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if (
		 	   OSAL_ERROR
		 	   ==
		 	   OSAL_s32MessageQueueDelete
		 	   (
		 	      IOSCMESSAGEQUEUE_NAME
		 	   )
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret = 1;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret = 2;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret = 3;
			break;
		 case OSAL_E_NAMETOOLONG:
			u32Ret = 4;
			break;
		 default:
			u32Ret = 5;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCCreateMsgExcMaxLen()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_003

* DESCRIPTION :  
*				   a) Created a message queue with a exceeding
*			    	   Max Name lenth
*				   b) Close and delete the message queue, if created.
*				   c) check the error status returned
*
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueCreateMsgExcMaxLen()
*
*******************************************************************************/
tU32  u32MQnoIOSCCreateMsgExcMaxLen( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MQ_MESSAGE_NAME_ECX,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  u32Ret = 10;
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret = 1;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret = 2;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret = 3;
			break;
		 case OSAL_E_INVALIDVALUE:
			u32Ret = 4;
			break;
		 default:
			u32Ret = 5;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCCreateDiffModes()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_004

* DESCRIPTION :  
*				   a) Created a message queues with Different Access
*			    	   Modes
*					   1.OSAL_EN_READWRITE
*					   2.OSAL_EN_READONLY
*					   3.OSAL_EN_WRITEONLY
*					   4.OSAL_EN_BINARY(Invalid Access Mode)
*				   b) Close and delete the message queues
*				   c) check the error status returned
*				      for OSAL_EN_BINARY(Invalid Access Mode)
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueCreateDiffModes()
*
*******************************************************************************/
tU32  u32MQnoIOSCCreateDiffModes( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret = 10;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret = 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_READONLY;

   /*Try to create the Message Queue with OSAL_EN_READONLY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 50;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 100;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret += ( 200 + u32MQCreateStatus() );
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_WRITEONLY;

   /*Try to create the Message Queue with OSAL_EN_WRITEONLY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 300;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 400;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret += ( 500 + u32MQCreateStatus() );
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_BINARY;

   /*Try to create the Message Queue with OSAL_EN_BINARY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 600;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 700;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE: 	  
		    u32Ret += 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret += 1000;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret += 2000;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret += 3000;
			break;
		 case OSAL_E_NAMETOOLONG:
			u32Ret += 4000;
			break;
		 default:
			u32Ret += 5000;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCCreateSameName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_005

* DESCRIPTION :  
*				   a) Created a message queue with a exceeding 
*			    	   Max Name lenth
*				   b) Close and delete the message queue, if created.
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueCreateSameName()
*
*******************************************************************************/
tU32  u32MQnoIOSCCreateSameName( tVoid )	
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  if ( 
	        OSAL_ERROR 
	        != 
	  	    /*Created a message queue with out deleting
	  	    previous*/
	        OSAL_s32MessageQueueCreate
	        (
			   IOSCMESSAGEQUEUE_NAME,
			   MAX_MESG,
			   MAX_LEN,
			   enAccess,
			   &mqHandle
			)
		 ) 
	  {
		 u32Ret = 10;

		 /*Close the message queue*/
	     if ( 
		  	   OSAL_ERROR 
		  	   == 
		  	   OSAL_s32MessageQueueClose
		  	   ( 
		  	      mqHandle 
		  	   )
		  	)
		 {
		    u32Ret += 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())( 2nd MQ)

		 else
		 {
		    /*Delete the message queue*/
			if ( 
				  OSAL_ERROR 
				  == 
				  OSAL_s32MessageQueueDelete
				  ( 
				     IOSCMESSAGEQUEUE_NAME 
				  ) 
			   )
			{
			   u32Ret += 50;
			}//end of if( OSAL_ERROR == OSAL_s32MessageQueueDelete())( 2nd MQ)

		 }//end of if-else( OSAL_ERROR == OSAL_s32MessageQueueClose())( 2nd MQ)

	  }//end of if ( OSAL_ERROR != OSAL_s32MessageQueueCreate())( 2nd MQ)
	  else
	  {
		 /* check the error status returned */
		 u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_ALREADYEXISTS:  	  
			   u32Ret = 0;
			   break;
		    case OSAL_E_UNKNOWN:
			   u32Ret = 10;
			   break;
			case OSAL_E_NOSPACE:
			   u32Ret = 20;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 30;
			   break;
			case OSAL_E_NAMETOOLONG:
			   u32Ret = 40;
			   break;
			default:
			   u32Ret = 50;
		 }//end of switch( u32status )

	  }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())( 2nd MQ)
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate()) 
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCDeleteNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_006

* DESCRIPTION :  
*				   b) delete the message queue with name NULL
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueDeleteNameNULL()
*
*******************************************************************************/
tU32 u32MQnoIOSCDeleteNameNULL( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;

   /*Delete the message queue*/
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32MessageQueueDelete
		 ( 
		 	OSAL_NULL 
		 ) 
	  )
   {
	  /* check the error status returned */
	  u32status =  OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE:  
		    u32Ret = 0;
			break;
		 case OSAL_E_DOESNOTEXIST:
			u32Ret = 1;
			break;
		 case OSAL_E_BUSY:
			u32Ret = 2;
			break;
		 case OSAL_E_UNKNOWN:
			u32Ret = 3;
			break;
		 default:
			u32Ret = 4;
	  }//end of switch( u32status )

   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

   else
   {
      u32Ret = 10;
   }
   return u32Ret; 
}
/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCDeleteNameInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_007

* DESCRIPTION :  
*				   a) Created a message queues 
*				   b) Close and delete the message queues
*				   b) delete the message queue which already deleted
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueDeleteNameInval()
*
*******************************************************************************/
tU32 u32MQnoIOSCDeleteNameInval( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret = 10;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret = 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

		 /*Delete the message queue which already deleted */
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
			u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_DOESNOTEXIST:  
				  u32Ret += 0;
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 50;
				  break;
			   case OSAL_E_BUSY:
				  u32Ret += 60;
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 70;
				  break;
			   default:
				  u32Ret += 80;
			}//end of switch( u32status )

		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())
		 else
		 {
			u32Ret += 100;
		 }

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   return u32Ret; 
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCDeleteWithoutClose()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_008

* DESCRIPTION :  
*				   a) Created a message queue. 
*				   b) Delete the message queues without close
*				   c) Deleting without a close should pass,
                      in case it fails, update the error code.
                   d) Close the Message Queue, it is supposed to
                      remove the resources as well, as a delete
                      has happened.
                   e) Try to reclose, this is expected to fail.
                   f) Return the error code. 
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueDeleteWithoutClose()
*
*******************************************************************************/
tU32 u32MQnoIOSCDeleteWithoutClose( tVoid )
{
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Delete the message queue without close */
	  if ( 
	   	    OSAL_ERROR 
		 	== 
		 	OSAL_s32MessageQueueDelete
		 	( 
		 	   IOSCMESSAGEQUEUE_NAME 
		 	) 
		 )
	  {
		  /*Delete should not fail*/
		  u32Ret = 50;	 
	  }
	  else
	  {
		 /*Close the message queue, and remove message queue resources*/
		 if ( 
		  	   OSAL_ERROR 
		  	   == 
		  	   OSAL_s32MessageQueueClose
		  	   ( 
		  	      mqHandle 
		  	   )
		  	)
		 {
		  	/*Message Queue Close is not supposed to fail*/
		  	u32Ret += 100;
		 }
		 else
		 {
			/*Try a reclose*/			  
			if( OSAL_OK == ( OSAL_s32MessageQueueClose( mqHandle ) ) )
			{
				/*Message Queue Reclose should not pass
				Queue was not successfully removed from the system*/
				u32Ret += 1000;
			}
		 }
      }
   }
   else
   {
      u32Ret = u32MQCreateStatus();
   }

   return u32Ret; 
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCOpenDiffMode()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_009

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueOpenDiffMode()
*
*******************************************************************************/
tU32 u32MQnoIOSCOpenDiffMode( tVoid ) 
{
   tU32 u32status 				   = 0;
   tU32 u32Ret					   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   tChar MQ_Buffer[MQ_BUFF_SIZE]   = { 0 };
   
   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue */
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
      /*Post a message into the message queue */
	  if ( 
	        OSAL_ERROR 
	        == 
	        OSAL_s32MessageQueuePost
	        ( 
	  		 mqHandle, 
			 (tPCU8)MQ_MESSAGE,
			 MAX_LEN,
			 MQ_PRIO2 
			)
		 ) 
	  {

	     u32Ret += (100 + u32MQPostStatus ()) ;
		 
	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
	  
	  /* Wait for Message */
	  if (
	  	    MQ_WAIT_RETURN_ERROR
	  	    == 
	        OSAL_s32MessageQueueWait
			(
			   mqHandle,
			   (tPU8)MQ_Buffer,
			   MAX_LEN,
			   OSAL_NULL,
			   (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING

			)
		 )
	  {
		 u32Ret += 	(200 + u32MQWaitStatus ()) ;
		 
	  }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())

	  /* Compare the message */
	  if (
	        OSAL_s32StringCompare ( MQ_MESSAGE, MQ_Buffer )
		 )
	  {
	     u32Ret += 300 ;
	     
	  }// end of if (! s32StringCompare())

	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle
	  	    )
	  	 )
	  {
	     u32Ret += 10;
		 
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	   
	  /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

	  if ( 
		    OSAL_ERROR 
		    != 
		    OSAL_s32MessageQueueOpen
		    ( 
		  	   IOSCMESSAGEQUEUE_NAME, 
			   OSAL_EN_WRITEONLY,
			   &mqHandle
			)
		 ) 
	  {
		 OSAL_pvMemorySet( MQ_Buffer, '\0', MQ_BUFF_SIZE );

		 /*Post a message into the message queue */
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueuePost
			   ( 
			  	  mqHandle, 
				  (tPCU8)MQ_MESSAGE,
				  MAX_LEN,
				  MQ_PRIO2 
			   )
			) 
		 {

			u32Ret += (400 + u32MQPostStatus ()) ;
			
		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())

		/*wait has to fail as handle has only write only access*/
		 if (
			   MQ_WAIT_RETURN_ERROR
			   != 
			   OSAL_s32MessageQueueWait
			   (
			      mqHandle,
			      (tPU8)MQ_Buffer,
			      MAX_LEN,
			      OSAL_NULL,
			      (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING
			   )
			)
		 {
			u32Ret += (500 + u32MQWaitStatus ()) ;
			

		 }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 else
		 {
			u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION:  
				  u32Ret += 0;
				  
				  break;
			   case OSAL_E_QUEUEFULL:
				  u32Ret += 40;
				  
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 50;
				  
				  break;
			   case OSAL_E_MSGTOOLONG:
				  u32Ret += 60;
				  
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 70;
				  
				  break;
			   case OSAL_E_BADFILEDESCRIPTOR:
				  u32Ret += 80;
				  
				  break;
			   default:
				  u32Ret += 90;
			}//end of switch( u32status )

		 }
		 
		 /*Close the message queue*/
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueueClose
			   ( 
			  	  mqHandle
			   )
			)
		 {
		 
		  	u32Ret = 700;
			

		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

	  else
	  {

		 u32Ret += (800 + u32MQPostStatus ()) ;
		 

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())

	  /*Open the message queue  in OSAL_EN_READONLY mode*/

	  if ( 
		    OSAL_ERROR 
		    != 
		    OSAL_s32MessageQueueOpen
		    ( 
		  	   IOSCMESSAGEQUEUE_NAME, 
			   OSAL_EN_READONLY,
			   &mqHandle
			)
		 ) 
	  {
		 OSAL_pvMemorySet( MQ_Buffer, '\0', MQ_BUFF_SIZE );

		 /*Post a message into the message queue.This has to fail as  handle only has read only access*/
		 if ( 
		       OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueuePost
			   ( 
			      mqHandle, 
				  (tPCU8)MQ_MESSAGE,
				  MAX_LEN,
				  MQ_PRIO2 
			   )
			) 
		 {

		    u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION:  
				  u32Ret += 0;
				  
				  break;
			   case OSAL_E_QUEUEFULL:
				  u32Ret += 40;
				  
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 50;
				  
				  break;
			   case OSAL_E_MSGTOOLONG:
				  u32Ret += 60;
				  
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 70;
				  
				  break;
			   case OSAL_E_BADFILEDESCRIPTOR:
				  u32Ret += 80;
				  
				  break;
			   default:
				  u32Ret += 90;
			}//end of switch( u32status )

		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
		 else
		 {
		    u32Ret +=1000;
			
		 }
		 if (
			   MQ_WAIT_RETURN_ERROR
			   == 
			   OSAL_s32MessageQueueWait
			   (
				  mqHandle,
				  (tPU8)MQ_Buffer,
				  MAX_LEN,
				  OSAL_NULL,
				  (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING

			   )
			)
		 {
			u32Ret += (2000 + u32MQWaitStatus ()) ;
			

		 }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 /*Close the message queue*/
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueueClose
			   ( 
			      mqHandle
			   )
			)
		 {
		    u32Ret = 4000;
			
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
	  }
	  else
	  {

		 u32Ret += (5000 + u32MQPostStatus ()) ;
		 

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
		 /*Delete the message queue*/
	  
	  if ( 
		 	OSAL_ERROR 
		 	== 
		 	OSAL_s32MessageQueueDelete
		 	( 
		 	   IOSCMESSAGEQUEUE_NAME 
		 	) 
		 )
	  {
	
		 u32Ret = 6000;
		 
	  
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
	  
  
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())


   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCTwoThreadMsgPost()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_010

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueTwoThreadMsgPost()
*			    
*******************************************************************************/
tU32 u32MQnoIOSCTwoThreadMsgPost( tVoid )
{
   tU32 u32Ret					   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   OSAL_tThreadID Thread1ID 	   = 0;
   OSAL_tThreadID Thread2ID 	   = 0;
   OSAL_trThreadAttribute  attr1   = { 0 };
   OSAL_trThreadAttribute  attr2   = { 0 };
	gevMask2 = 0;
   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MQ_COUNT_MAX,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  if (
		    OSAL_ERROR
			==
	        OSAL_s32EventCreate
	        (
	           MQ_EVE_NAME1,
		       &MQEveHandle_1
	        )
		 )
	  {
	     u32Ret = 1;   

	  }
	  /* Setting attributes for Thread 1 */

	  attr1.szName = MQ_THR_NAME_1;
	  attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr1.s32StackSize = MQ_TR_STACK_SIZE;
	  attr1.pfEntry = u32NoIoscMQThread1; 
	  attr1.pvArg = OSAL_NULL;

	  /* Setting attributes for Thread 2 */
	  attr2.szName = MQ_THR_NAME_2;
	  attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr2.s32StackSize = MQ_TR_STACK_SIZE;
	  attr2.pfEntry = u32NoIoscMQThread2; 
	  attr2.pvArg = OSAL_NULL;
	   
	  
	  
	  /* Spawn the Thread MQ_Thread_1 */
	  Thread1ID = 
	     OSAL_ThreadSpawn(&attr1);

	  if (
	        OSAL_ERROR
	  	    ==
		    Thread1ID
	   	 )
	  {
	     u32Ret += 2;   
	  }//if (OSAL_ERROR == Thread1ID )
	  else
	  { 
		  /* Spawn the Thread MQ_Thread_2 */
		  Thread2ID = 
		     OSAL_ThreadSpawn(&attr2);

		  if (
			    OSAL_ERROR
				==
				Thread2ID
		   	 )
		  {
		     u32Ret += 3;   
		  }//if (OSAL_ERROR == Thread2ID )
	      else
		  {
		      /* Wait for an event */
			  if ( 
			  	    OSAL_ERROR 
			  	    == 
			  	    OSAL_s32EventWait
			  	    ( 
			  	       MQEveHandle_1,
					   MQ_EVE1|MQ_EVE2,
			  	       OSAL_EN_EVENTMASK_AND,
			  	       OSAL_C_TIMEOUT_FOREVER ,
					   &gevMask2
					)
			  	 )
			  {
				 u32Ret += 4;
			  }//end of if ( OSAL_ERROR == OSAL_s32EventWait())
			  
				/*Clear the event*/
				OSAL_s32EventPost
				( 
				MQEveHandle_1,
				~(gevMask2),
			   OSAL_EN_EVENTMASK_AND
			    );
#if 0			  
			  /* Delete Thread 1*/
			  if ( 
			  	    OSAL_ERROR 
			  	    == 
			  	    OSAL_s32ThreadDelete
			  	    ( 
			  	       Thread2ID 
			  	    )
			  	 )
			  {
				 /* Check the error status */
				 u32Ret += 5;
			  }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())

#endif
		  }//if-else (OSAL_ERROR == Thread2ID )
		  /* Delete Thread 2*/
#if 0
		  if ( 
		  	    OSAL_ERROR 
		  	    == 
		  	    OSAL_s32ThreadDelete
		  	    ( 
		  	       Thread1ID 
		  	    )
		  	 )
		  {
			 /* Check the error status */
			 u32Ret += 6;
		  }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
#endif
	  }//if-else (OSAL_ERROR == Thread1ID )
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
		 /* Check the error status */
		 u32Ret += (20 + u32MQCloseStatus ()) ;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      IOSCMESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		    /* Check the error status */
		    u32Tr1NtRet2 += (30 + u32MQDeleteStatus ()) ;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32EventClose
	  	    ( 
	  	       MQEveHandle_1
			)
	  	 )
	  {
		 u32Ret += 30;
	  }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
	  else
	  {
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32EventDelete
		 	   ( 
		 	      MQ_EVE_NAME1 
		 	   ) 
		 	)
		 {
		    /* Check the error status */
		    u32Ret += 40;
		 }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())


	  }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   u32Ret +=
     		u32Tr1Ret2
   			+
   			u32Tr2Ret2;
   			  
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCThreadMsgNotify()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_011

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueThreadMsgNotify()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCThreadMsgNotify( tVoid )
{
   tU32 u32Ret					   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
   OSAL_tThreadID Thread1ID 	   = 0;
   OSAL_tThreadID Thread2ID 	   = 0;
   OSAL_tThreadID Thread3ID 	   = 0;
   OSAL_trThreadAttribute  attr1   = { 0 };
   OSAL_trThreadAttribute  attr2   = { 0 };
   OSAL_trThreadAttribute  attr3   = { 0 };
	gevMask2 = 0;
   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			IOSCMESSAGEQUEUE_NAME,
			MAX_MESG3,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  if (
		    OSAL_ERROR
			==
	        OSAL_s32EventCreate
	        (
	           MQ_EVE_NAME2,
		       &MQEveHandle_2
	        )
		 )
	  {
	     u32Ret = 1;   

	  }
	  /* Setting attributes for Thread 1 */

	  attr1.szName = MQ_THR_NAME_NOTIFY_1;
	  attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr1.s32StackSize = MQ_TR_STACK_SIZE;
	  attr1.pfEntry = u32NoIoscMQThreadNoIoscNotify1; 
	  attr1.pvArg = OSAL_NULL;

	  /* Setting attributes for Thread 2 */
	  attr2.szName = MQ_THR_NAME_NOTIFY_2;
	  attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr2.s32StackSize = MQ_TR_STACK_SIZE;
	  attr2.pfEntry = u32NoIoscMQThreadNoIoscNotify2; 
	  attr2.pvArg = OSAL_NULL;
	   
	  /* Setting attributes for Thread 2 */
	  attr3.szName = MQ_THR_NAME_NOTIFY_3;
	  attr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr3.s32StackSize = MQ_TR_STACK_SIZE;
	  attr3.pfEntry = u32NoIoscMQThreadNoIoscNotify3; 
	  attr3.pvArg = OSAL_NULL;

	  /* Spawn the Thread MQ_Thread_1 */
        Thread1ID = OSAL_ThreadSpawn(&attr1);

        if(OSAL_ERROR == Thread1ID)
	  {
	     u32Ret = 1;   
	  }//if (OSAL_ERROR == Thread1ID )
	  else
        {   /* Spawn the Thread MQ_Thread_2 */
            Thread2ID = OSAL_ThreadSpawn(&attr2);

            if(OSAL_ERROR == Thread2ID)
		  {
		     u32Ret = 2;   
		  }//if (OSAL_ERROR == Thread2ID )

		  else
		  {
                Thread3ID = OSAL_ThreadSpawn(&attr3);

                if(OSAL_ERROR == Thread3ID)
			  {
			     u32Ret = 3;   
			  }//if (OSAL_ERROR == Thread3ID )
			  else
			  {
				  /* Wait for an event */
				  if ( 
				  	    OSAL_ERROR 
				  	    == 
				  	    OSAL_s32EventWait
				  	    ( 
				  	       MQEveHandle_2,
						   MQ_EVE1|MQ_EVE2|	MQ_EVE3,
				  	       OSAL_EN_EVENTMASK_AND,
				  	       OSAL_C_TIMEOUT_FOREVER ,
						   &gevMask2
						)
				  	 )
				  {
					 u32Ret = 4;
				  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
					/*Clear the event*/
					OSAL_s32EventPost
					( 
					MQEveHandle_2,
					~(gevMask2),
				   OSAL_EN_EVENTMASK_AND
				    );

			  }//if-else (OSAL_ERROR == Thread1ID )
		  }//if-else (OSAL_ERROR == Thread1ID )
	  }//if-else (OSAL_ERROR == Thread1ID )

	  /*Close the message queue*/
        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
	  {
		 /* Check the error status */
		 u32Ret += (20 + u32MQCloseStatus ()) ;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if (OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ))
		 {
		    /* Check the error status */
		    u32Ret += (30 + u32MQDeleteStatus ()) ;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle2))
	  {
		 u32Ret += 30;
	  }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
	  else
	  {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: event 'ready' + 'success' closed");
            if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME2))
		 {
		    /* Check the error status */
		    u32Ret += 40;
		 }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())
	  }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())


   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())


    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum without subthreads %i", u32Ret);
    OSAL_s32ThreadWait(23000);

    u32Ret += u32Tr1NtRet2 + u32Tr2NtRet2 + u32Tr3NtRet2;

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum with subthreads %i", u32Ret);
   	  
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCQueryStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_012

* DESCRIPTION :  
*				   a)Created a message queue.
*				   b)Query Status of message queue.
*				   c)Post a message into the message queue.
				   d)Query Status of message queue.
*				   e)Wait for Message.
                   f)Query Status of message queue.
				   g)Close the message queue.
				   h)Query Status of message queue.
				   i)Delete the message queue.
				   j)Query Status of message queue( Should return 
				     OSAL_ERROR = OSAL_E_BADFILEDESCRIPTOR ).
*				   			       	
* HISTORY     :	   
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueQueryStatus()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCQueryStatus( tVoid )
{
	/*Definitions*/
	tU32 u32Ret             = 0;
	tU32 u32ErrorValue      = OSAL_E_NOERROR;
	tU32 u32MaxMessage      = 0;
	tU32 u32MaxLength       = 0;
	tU32 u32Message         = 0;
	tU8  au8Buffer[MAX_LEN]	= {0};
	tU8  u8Switch           = 0;
	OSAL_tMQueueHandle hMQ  = 0;


	/*Create a Message Queue in the system*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		/*Catch the error*/
		u32ErrorValue =	OSAL_u32ErrorCode( );

		/*Query the error*/
		switch( u32ErrorValue )
		{
			case OSAL_E_NOPERMISSION:  return 100;
			case OSAL_E_INVALIDVALUE:  return 200;
			case OSAL_E_NOSPACE:       return 300;
			case OSAL_E_ALREADYEXISTS: return 400;
			case OSAL_E_NAMETOOLONG:   return 500;
			case OSAL_E_UNKNOWN:       return 600;
			default:                   return 700;		
		}
	}
	else
	{
		/*Query the Message Queue Status after Message Queue Create*/
		if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );	

			/*Query the error*/
			switch( u32ErrorValue )
			{
				case OSAL_E_BADFILEDESCRIPTOR:
					 u32Ret += 800;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 900;
					 break;
				default:
					 u32Ret += 1000;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Create\n" );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum number of messages possible: %u\n",
			 					 u32MaxMessage );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum size of a message possible: %u\n",
			 					 u32MaxLength );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Message after a Create : %u\n",
			 					 u32Message );
		}

		/*Post a Message into Message Box*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,
								  					OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1,
								  					MQ_PRIO2 ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			/*Query the error*/
			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 1100;
					 break;
				case OSAL_E_MSGTOOLONG:
					 u32Ret += 1200;
					 break;
				case OSAL_E_BADFILEDESCRIPTOR:
					 u32Ret += 1300;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 1400;
					 break;
				case OSAL_E_QUEUEFULL:
					 u32Ret += 1450;
					 break;
				case OSAL_E_NOPERMISSION:
				     u32Ret += 1475;
					 break;
				default:
					 u32Ret += 1500;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Set the switch parameter*/
			u8Switch = 1;
			
			/*Query Message Queue Status*/
			if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );	

				/*Query the error*/
				switch( u32ErrorValue )
				{
					case OSAL_E_BADFILEDESCRIPTOR:
						 u32Ret += 1600;
						 break;
					case OSAL_E_UNKNOWN:
						 u32Ret += 1700;
						 break;
					default:
						 u32Ret += 1800;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Post\n" );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum number of messages possible: %u\n",
			 					 u32MaxMessage );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum size of a message possible: %u\n",
			 					 u32MaxLength );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Message after a Post: %u\n",
			 					 u32Message );
			}
		}

		if( u8Switch )
		{
			/*Wait for a Message in the Message Box*/
			if( MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait( hMQ,au8Buffer,
		                                            	MAX_LEN,OSAL_NULL,
		                                            	OSAL_C_TIMEOUT_FOREVER ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				/*Query the error code*/
				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE:
					 	 u32Ret += 1900;
					 	 break;
					case OSAL_E_TIMEOUT:
					 	 u32Ret += 2000;
					 	 break;
					case OSAL_E_UNKNOWN:
					 	 u32Ret += 2100;
					 	 break;
					default:
					 	 u32Ret += 2200;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
				/*Query Message Queue Status*/
				if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              		  &u32MaxLength,
		                            				  		  &u32Message ) )
				{
					/*Catch the error*/
					u32ErrorValue = OSAL_u32ErrorCode( );	

					/*Query the error*/
					switch( u32ErrorValue )
					{
						case OSAL_E_BADFILEDESCRIPTOR:
						 	 u32Ret += 2300;
						 	 break;
						case OSAL_E_UNKNOWN:
						 	 u32Ret += 2400;
						 	 break;
						default:
							 u32Ret += 2500;
					}

					/*Clear the error value*/
					u32ErrorValue = OSAL_E_NOERROR;
				}
				else
				{
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Wait\n" );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Maximum number of messages possible: %u\n",
			 					 	   u32MaxMessage );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Maximum size of a message possible: %u\n",
			 					 	   u32MaxLength );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Message after a wait : %u\n",
			 					 	   u32Message );
				}/*Message Status successful*/
			}/*Wait successful*/
		}/*Switch*/

		/*Close the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( hMQ ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 2600;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 2700;
					 break;
				default:
					 u32Ret += 2800;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Query Message Queue Status - after close has to fail*/
			if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				/*Query the error*/
				switch( u32ErrorValue )
				{
					case OSAL_E_BADFILEDESCRIPTOR:
						 u32Ret += 0;
						 break;
					case OSAL_E_INVALIDVALUE:
						 u32Ret += 0;
						 break;
					default:
						 u32Ret += 3100;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
			 	u32Ret += 3000;

			}
		}

		/*Delete the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 3200;
					 break;
				case OSAL_E_DOESNOTEXIST:
					 u32Ret += 3300;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 3400;
					 break;
				default:
					 u32Ret += 3500;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}

	}

	/*Reset the switch*/
	u8Switch = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCQueryStatusMMParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_013
* DESCRIPTION :
*				   Message Queue Status with Maximum number of messages(MM)
*				   parameter as OSAL_NULL.
*
*				   Reference Manual says on OSAL_s32MessageQueueStatus:
*				   This function gives status informations about a message box,
*				   opened before. If one of parameters (not the first) is
*				   OSAL_NULL, the value of this respective parameter is not
*				   returned.
*				   So in the above scenario the API OSAL_s32MessageQueueStatus
*				   should not fail, but pass
*
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueQueryStatusMMParamNULL()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCQueryStatusMMParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tPU32 pu32MM            = OSAL_NULL;
	tU32 u32MaxLength       = 0;
	tU32 u32Message         = 0;
	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													pu32MM,
		           		                                &u32MaxLength,
		            	                				&u32Message ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;

}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCQueryStatusMLParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_014
* DESCRIPTION :
*				   Message Queue Status with Maximum Message Length (ML)
*				   parameter as OSAL_NULL.
*
*				   Reference Manual says on OSAL_s32MessageQueueStatus:
*				   This function gives status informations about a message box,
*				   opened before. If one of parameters (not the first) is
*				   OSAL_NULL, the value of this respective parameter is not
*				   returned.
*				   So in the above scenario the API OSAL_s32MessageQueueStatus
*				   should not fail, but pass
*
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueQueryStatusMLParamNULL()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCQueryStatusMLParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tU32 u32MaxMessage      = 0;
  	tPU32 pu32ML            = OSAL_NULL;
	tU32 u32Message         = 0;
	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													&u32MaxMessage,
		           		                                pu32ML,
		            	                				&u32Message ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCQueryStatusCMParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_015
* DESCRIPTION :
*				   Message Queue Status with Current number of messages(CM)
*				   parameter as OSAL_NULL.
*
*				   Reference Manual says on OSAL_s32MessageQueueStatus:
*				   This function gives status informations about a message box,
*				   opened before. If one of parameters (not the first) is
*				   OSAL_NULL, the value of this respective parameter is not
*				   returned.
*				   So in the above scenario the API OSAL_s32MessageQueueStatus
*				   should not fail, but pass
*
*
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueQueryStatusCMParamNULL()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCQueryStatusCMParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tU32 u32MaxMessage      = 0;
	tU32 u32MaxLength       = 0;
	tPU32 pu32CM            = OSAL_NULL;
    OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													&u32MaxMessage,
		           		                                &u32MaxLength,
		            	                				pu32CM ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCQueryStatusAllParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_016
* DESCRIPTION :    Try calling OSAL_s32MessageQueueStatus with all
*				   parameters as OSAL_NULL.Should return the error
*				   code OSAL_E_BADFILEDESCRIPTOR.
*
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQueQueryStatusAllParamNULL()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCQueryStatusAllParamNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret             = 0;


	/*Call MsgQueQueryStatus API*/
	if( OSAL_ERROR == OSAL_s32MessageQueueStatus( OSAL_NULL,OSAL_NULL,
												  OSAL_NULL,OSAL_NULL ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_BADFILEDESCRIPTOR != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Message Queue Status cannot pass with NULL parameters*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCPostMsgInvalPrio()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_017
* DESCRIPTION :    Try calling OSAL_s32MessageQueuePost, with invalid
				   priority value.
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQuePostMsgInvalPrio()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCPostMsgInvalPrio( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			= 0;

	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif

	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		/*Post a Message with invalid priority*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,
								  					OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1,
								  					INVAL_PRIO ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 1000;
			}
		}
		else
		{
			/*Message cannot be posted!*/
			u32Ret += 2000;
		}

		/*Close the Message Queue*/
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		{
			/*Delete the Message Queue*/
			if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
			{
				u32Ret += 3000;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}
		else
		{
			u32Ret += 4000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}

	}
	else
	{
		/*Message Queue Create failed*/
		u32Ret += 5000;
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Return the error code*/
	return u32Ret;

}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCPostMsgBeyondQueLimit()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_018
* DESCRIPTION :    Try to Post a Message Queue beyond the limit of
                   maximum number of messages it can hold
* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MsgQuePostMsgBeyondQueLimit()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCPostMsgBeyondQueLimit( tVoid )
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	tU8  u8Count = 0;

	#if DEBUG_MODE
	tU32 u32ECode           = OSAL_E_NOERROR;
	#endif

	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	}

	/*Post 5 messages in the Message Queue*/
	do
	{
		/*Plus 1 to the length to accomodate the NULL string terminator*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,
								  					OSAL_u32StringLength( MESSAGE_20BYTES )+1,
								  					MQ_PRIO2 ) )
		{
			/*Update the error code*/
			u32Ret += 100;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}

		/*Increment Count*/
		u8Count++;

	}while( MQ_COUNT_MAX != u8Count );

	/*Post it one extra time - Should fail*/
	/*System Crash, Success are API failures*/
	if( OSAL_ERROR != OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,
							  					OSAL_u32StringLength( MESSAGE_20BYTES )+1,
							  					MQ_PRIO2 ) )
	{
		/*Update the error code*/
		u32Ret += 1000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Close the Message Queue and Delete*/
	if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
	{
		/*Delete the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
		{
			u32Ret += 3000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}
	else
	{
		u32Ret += 4000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCStressTest()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_019

* DESCRIPTION :    1). Set the Seed for Random function based on
                       Elasped Time.
				   2). Create 20 Message Queues in the system.
				   3). Create 10 Threads which does Post operation
				       on 20 Message Queues Randomly.
				   4). Create 15 Threads which does Wait operation
				       on 20 Message Queues Randomly.
				   5). Create 1 Thread which does Delete and Close
				       operation on 20 Message Queues Randomly.
				   6). Create 2 Threads which does a Re- Create on
				       20 Message Queues Randomly,if deleted earlier.
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete all the 28 Threads.
				   9). Return success.

				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Message Queue
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Message Queue
				   API Interface is Robust.Timeout Time can be reconfigured in the
				   file "oedt_osalcore_TestFuncs.h" under the define
				   STRESS_MQ_DURATION.Whether this case should be run
				   can be configured in the same file as part of the define
				   STRESS_MQ.

* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of u32MessageQueueStressTest()
*			       	
*******************************************************************************/
tU32 u32MQnoIOSCStressTest( tVoid )
{
	/*Definitions*/
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = "\0";

	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "B_SEM",&hbaseSem, 1 );

	/*Create MAX_MESSAGE_QUEUES number of messages in the system*/
	while( u8Index < MAX_MESSAGE_QUEUES )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_MessageQueue_%d",u8Index );

		/*Get the name into Message Name field*/
		(tVoid)OSAL_szStringCopy( mesgque_StressList[u8Index].coszName,acBuf );
		/*Fill in the length of a message unit*/
		mesgque_StressList[u8Index].u32MaxLength   = MAX_LEN;
		/*Fill in the number of message units in the message queue*/
		mesgque_StressList[u8Index].u32MaxMessages = MAX_NO_MESSAGES;
		/*Set the access mode to the message queue*/
		mesgque_StressList[u8Index].enAccess       = OSAL_EN_READWRITE;

		/*Create a Message Queue within the system*/
		if( OSAL_ERROR == OSAL_s32MessageQueueCreate(
													  mesgque_StressList[u8Index].coszName,
													  mesgque_StressList[u8Index].u32MaxMessages,
													  mesgque_StressList[u8Index].u32MaxLength,
													  mesgque_StressList[u8Index].enAccess,
													  &mesgque_StressList[u8Index].phMQ
													 ) )
		{

			/*Return immediately with failure*/
			return (10000+u8Index);
		}
		/*Reinitialize the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment the index*/
		u8Index++;
	}

	/*Start all Post Threads*/
	vEntryFunction( Post_Message,"Post_Msgq_Thr",NO_OF_POST_THREADS,msgqpost_ThreadAttr,msgqpost_ThreadID );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_Message,"Wait_Msgq_Thr",NO_OF_WAIT_THREADS,msgqwait_ThreadAttr,msgqwait_ThreadID );
	/*Start the Close Thread*/
	vEntryFunction( Close_Message,"Close_Msgq_Thr",NO_OF_CLOSE_THREADS,msgqclose_ThreadAttr,msgqclose_ThreadID );
	/*Start all Create Threads*/
	vEntryFunction( Create_Message,"Create_Msgq_Thr",NO_OF_CREATE_THREADS,msgqcreate_ThreadAttr,msgqcreate_ThreadID );

	/*Wait for OSAL_C_TIMEOUT_FOREVER*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( msgqpost_ThreadID[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( msgqwait_ThreadID[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( msgqclose_ThreadID[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( msgqcreate_ThreadID[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Reset the Global Thread Counter*/
	u32GlobMsgCount = 0;

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( hbaseSem );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEM" );

	/*Return error code - Success if control reaches here*/
	return 0;
}

void u32NoIoscMQPostThread(void)
{
   OSAL_tMQueueHandle mqHandle = 0;
   OSAL_s32MessageQueueOpen(IOSCMESSAGEQUEUE_NAME,OSAL_EN_READWRITE,&mqHandle);
   OSAL_s32MessageQueuePost(mqHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 );
   OSAL_s32MessageQueueClose(mqHandle);
}


/*****************************************************************************
* FUNCTION    :   u32MQnoIOSCSimplePostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue.
               2). Post to Message Queue
               3). Wait on Message Queue
               4). Go to 1). and do Test again n-Times
               5). Close and Delete Message Queue

* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of MsgQueSimplePostWaitPerfTest()
*			       	
*******************************************************************************/

tU32 u32MQnoIOSCSimplePostWaitPerfTest(tVoid )
{
   tU32 u32Ret = 0;
   OSAL_tMQueueHandle mqHandle = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   int loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(IOSCMESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &mqHandle))
   {

      while (loop < RUN_TEST)
      {
         if (OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ))
         {
            u32Ret += loop+1000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 1200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(IOSCMESSAGEQUEUE_NAME)) u32Ret += 1300;
            return u32Ret;
         }
         if( 0 == OSAL_s32MessageQueueWait(mqHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
         {
            u32Ret += loop+2000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 2200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(IOSCMESSAGEQUEUE_NAME)) u32Ret += 2300;
            return u32Ret;
         }
         loop++;
      }
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(IOSCMESSAGEQUEUE_NAME)) u32Ret += 300;
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   u32MQnoIOSCThreadPostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue
               2). Create Thread
               3). Wait on Message Queue
               4). Thread should post to Message Queue
               5). Go to 2). and do Test again n-Times
               6). Close and Delete Message Queue

* HISTORY     :
* 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
*				 copy of MsgQueThreadPostWaitPerfTest()
*			       	
*******************************************************************************/

tU32 u32MQnoIOSCThreadPostWaitPerfTest(tVoid )
{
   OSAL_trThreadAttribute threadAttr = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   tS8 ps8ThreadName[16];
   tU32 u32Ret = 0;
   tU32 u32Loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(IOSCMESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &MQHandle))
   {
      while (u32Loop < RUN_TEST)
      {
         sprintf((tString) ps8ThreadName, "%s%03u", "MQ_TH_", u32Loop);
         threadAttr.u32Priority = 0;
         threadAttr.szName = (tString) ps8ThreadName;
         threadAttr.pfEntry = (OSAL_tpfThreadEntry) u32NoIoscMQPostThread;
         threadAttr.pvArg = OSAL_NULL;
         threadAttr.s32StackSize = VALID_STACK_SIZE;

         if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
         {

            if (OSAL_ERROR != OSAL_s32MessageQueueWait(MQHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
            {
               u32Loop++;
            }
            else
            {
               u32Ret += u32Loop + 2000;
               break;
            }
         }
         else
         {
            u32Ret += u32Loop + 4000;
            break;
         }
      } // while
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(MQHandle))
      u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(IOSCMESSAGEQUEUE_NAME))
      u32Ret += 300;
   return u32Ret;
}


void NoIoscThread_MQ_Memory(tVoid* pvArg)
{
   tS32 s32ReturnValue = 0;
   OSAL_tMQueueHandle handle_2 = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };

   if ((OSAL_s32MessageQueueOpen(IOSCMESSAGEQUEUE_NAME, OSAL_EN_READWRITE, &handle_2)) != OSAL_ERROR)
   {
      if (OSAL_s32MessageQueuePost(handle_2, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
      {
         if (OSAL_s32MessageQueueWait(handle_2, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueClose(handle_2) != OSAL_ERROR)
            {
               // test ok
            }
            else
               s32ReturnValue =5000;
         }
         else
            s32ReturnValue = 3000;
      }
      else
         s32ReturnValue = 2000;
   }
   else
      s32ReturnValue = 1000;

   // thread return value
   *(tS32 *)pvArg = s32ReturnValue;
}


/******************************************************************************
 * FUNCTION    :u32MQnoIOSCOpenDoubleCloseOnce
 * DESCRIPTION :Open Twice, Close once and use sh memory with threads
 * PARAMETER   :none
 * RETURNVALUE :tU32, status value in case of error
 * TEST CASE   :
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
 * HISTORY     :
 * 02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				 copy of u32MQOpenDoubleCloseOnce()
 *			       	
 *****************************************************************************/
tU32 u32MQnoIOSCOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   tS32 s32ReturnValueThread = 0;
   OSAL_tMQueueHandle handle_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) NoIoscThread_MQ_Memory;
   threadAttr.pvArg = &s32ReturnValueThread;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   if ((OSAL_s32MessageQueueCreate(IOSCMESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &handle_1) != OSAL_ERROR))
   {
      if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
      {
         OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
         if (OSAL_s32MessageQueuePost(handle_1, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueWait(handle_1, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
            {
               if (OSAL_s32MessageQueueClose(handle_1) != OSAL_ERROR)
               {
                  if (OSAL_s32MessageQueueDelete(IOSCMESSAGEQUEUE_NAME) != OSAL_ERROR)
                  {
                     // test ok
                  }
                  else
                     s32ReturnValue += 700;
               }
               else
                  s32ReturnValue += 500;
            }
            else
               s32ReturnValue += 400;
         }
         else
            s32ReturnValue += 300;
      }
      else
         s32ReturnValue += 200;
   }
   else
      s32ReturnValue += 100;


   return s32ReturnValue;
}

/*****************************************************************************
* FUNCTION    :	   u32MQnoIOSCTimeoutCheck()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Run MessageQueueWait with timeout
*                  Set +/- drifted time via subthreads Thread_SetTimePlus, Thread_SetTimeMinus
* HISTORY     :	   05. June 2012, Martin Langer (PJ-AI/CF-33)
*******************************************************************************/
tVoid Thread_SetTimePlus( tVoid *pArg )
{
    OSAL_trTimeDate rtcSetParam = { 0 };
    OSAL_trTimeDate rtcGetParam = { 0 };
    ((void)pArg);
    OSAL_s32ThreadWait( TWO_SECONDS );
    
    if (OSAL_ERROR == OSAL_s32ClockGetTime(&rtcGetParam))
    {
        /*Critical Error!*/
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32ClockGetTime failed with errror code '%i'", OSAL_u32ErrorCode() );
    }
    else
    {
        OSAL_s32ThreadWait( TWO_SECONDS );
        
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Minute         '%i' (before)", rtcGetParam.s32Minute );
        
        rtcSetParam.s32Second           = rtcGetParam.s32Second;
        rtcSetParam.s32Minute           = rtcGetParam.s32Minute + 5;
        rtcSetParam.s32Hour             = rtcGetParam.s32Hour;
        rtcSetParam.s32Day              = rtcGetParam.s32Day;
        rtcSetParam.s32Month            = rtcGetParam.s32Month;
        rtcSetParam.s32Year             = rtcGetParam.s32Year;
        rtcSetParam.s32Daylightsaving   = rtcGetParam.s32Daylightsaving;
        rtcSetParam.s32Weekday          = rtcGetParam.s32Weekday;
        rtcSetParam.s32Yearday          = rtcGetParam.s32Yearday;
        rtcSetParam.s32Daylightsaving   = rtcGetParam.s32Daylightsaving;

        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Second         '%i'", rtcSetParam.s32Second );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Minute         '%i'", rtcSetParam.s32Minute );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Hour           '%i'", rtcSetParam.s32Hour );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Day            '%i'", rtcSetParam.s32Day );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Month          '%i'", rtcSetParam.s32Month );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Year           '%i'", rtcSetParam.s32Year );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Daylightsaving '%i'", rtcSetParam.s32Daylightsaving );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Weekday        '%i'", rtcSetParam.s32Weekday );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Yearday        '%i'", rtcSetParam.s32Yearday );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Daylightsaving '%i'", rtcSetParam.s32Daylightsaving );
        
        OSAL_s32ThreadWait( TWO_SECONDS );
    
        if (OSAL_ERROR == OSAL_s32ClockSetTime(&rtcSetParam))
        {
            /*Critical Error!*/
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32ClockSetTime failed with errror code '%i'", OSAL_u32ErrorCode() );
        }
    }
    
	OSAL_s32ThreadWait( TWO_SECONDS );
}

tVoid Thread_SetTimeMinus( tVoid *pArg )
{
    OSAL_trTimeDate rtcSetParam = { 0 };
    OSAL_trTimeDate rtcGetParam = { 0 };
    ((void)pArg);
    OSAL_s32ThreadWait( TWO_SECONDS );
    
    if (OSAL_ERROR == OSAL_s32ClockGetTime(&rtcGetParam))
    {
        /*Critical Error!*/
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32ClockGetTime failed with errror code '%i'", OSAL_u32ErrorCode() );
    }
    else
    {
        OSAL_s32ThreadWait( TWO_SECONDS );
        
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Minute         '%i' (before)", rtcGetParam.s32Minute );
        
        rtcSetParam.s32Second           = rtcGetParam.s32Second;
        rtcSetParam.s32Minute           = rtcGetParam.s32Minute - 10;
        rtcSetParam.s32Hour             = rtcGetParam.s32Hour;
        rtcSetParam.s32Day              = rtcGetParam.s32Day;
        rtcSetParam.s32Month            = rtcGetParam.s32Month;
        rtcSetParam.s32Year             = rtcGetParam.s32Year;
        rtcSetParam.s32Daylightsaving   = rtcGetParam.s32Daylightsaving;
        rtcSetParam.s32Weekday          = rtcGetParam.s32Weekday;
        rtcSetParam.s32Yearday          = rtcGetParam.s32Yearday;
        rtcSetParam.s32Daylightsaving   = rtcGetParam.s32Daylightsaving;

        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Second         '%i'", rtcSetParam.s32Second );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Minute         '%i'", rtcSetParam.s32Minute );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Hour           '%i'", rtcSetParam.s32Hour );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Day            '%i'", rtcSetParam.s32Day );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Month          '%i'", rtcSetParam.s32Month );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Year           '%i'", rtcSetParam.s32Year );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Daylightsaving '%i'", rtcSetParam.s32Daylightsaving );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Weekday        '%i'", rtcSetParam.s32Weekday );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Yearday        '%i'", rtcSetParam.s32Yearday );
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: rtcSetParam.s32Daylightsaving '%i'", rtcSetParam.s32Daylightsaving );
        
        OSAL_s32ThreadWait( TWO_SECONDS );
    
        if (OSAL_ERROR == OSAL_s32ClockSetTime(&rtcSetParam))
        {
            /*Critical Error!*/
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32ClockSetTime failed with errror code '%i'", OSAL_u32ErrorCode() );
        }
    }
    
	OSAL_s32ThreadWait( TWO_SECONDS );
}

tU32 u32MQnoIOSCTimeoutCheck( tVoid )
{
	/*Definitions*/
	tU32 u32Ret             = 0;
	tU8  au8Buffer[MAX_LEN]	= {0};
	OSAL_tMQueueHandle hMQ  = 0;
    OSAL_trTimeDate rtcSetParam = { 0 };    
	OSAL_tThreadID tID_1           = 0;
	OSAL_tThreadID tID_2           = 0;
   	OSAL_trThreadAttribute trAtr_1 = {OSAL_NULL};
   	OSAL_trThreadAttribute trAtr_2 = {OSAL_NULL};
  	OSAL_tMSecond timestampStart   = 0; 
	OSAL_tMSecond timestampEnd     = 0;
	OSAL_tMSecond deltaTime        = 0;

    /*Initialize Structure*/
    rtcSetParam.s32Second = 0;
    rtcSetParam.s32Minute = 20;
    rtcSetParam.s32Hour = 10;
    rtcSetParam.s32Day = 1;
    rtcSetParam.s32Month = 1;
    rtcSetParam.s32Year = 111;
    rtcSetParam.s32Daylightsaving = 0;
    rtcSetParam.s32Weekday = 0;
    rtcSetParam.s32Yearday = 1;
    rtcSetParam.s32Daylightsaving = 0;

    if (OSAL_ERROR == OSAL_s32ClockSetTime(&rtcSetParam))
    {
        /*Critical Error!*/
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32ClockSetTime() failed with errror code '%i'", OSAL_u32ErrorCode() );
    }

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME, MQ_COUNT_MAX,MAX_LEN, OSAL_EN_READWRITE,&hMQ ) )
	{
        /*Critical Error!*/
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueCreate() failed with errror code '%i'", OSAL_u32ErrorCode() );
        return 1;
	}
    
    /*Fill in thread attributes*/
    trAtr_1.szName       = "OEDT_SetTimePlus";
	trAtr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_1.s32StackSize = 4096;
	trAtr_1.pfEntry      = (OSAL_tpfThreadEntry)Thread_SetTimePlus;
	trAtr_1.pvArg        = &u32Ret;

	/*Set Time in Subthread 1*/
	if( OSAL_ERROR == ( tID_1 = OSAL_ThreadSpawn( &trAtr_1 ) ) )
	{
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_ThreadSpawn() failed with errror code '%i'", OSAL_u32ErrorCode() );
		u32Ret += 2;
	}

   	/*Get the elapsed Time*/
 	timestampStart = OSAL_ClockGetElapsedTime();

	/*Wait for a Message in the Message Box*/
	if( MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait( hMQ,au8Buffer, MAX_LEN,OSAL_NULL, 10000 ) )
	{
		/*Catch the error*/
		if (OSAL_E_TIMEOUT != OSAL_u32ErrorCode() )
        {
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueWait() failed with unexpected errror code '%i'", OSAL_u32ErrorCode() );
            u32Ret += 4;
        }
        else
        {
		 	timestampEnd = OSAL_ClockGetElapsedTime();
        }
	}

	/* check elapsed time */
	if(timestampEnd > timestampStart )
	{
		deltaTime = timestampEnd-timestampStart;
	}
	else
	{
		deltaTime = ( 0xFFFFFFFF - timestampStart ) + timestampEnd+1;
	}
    if (deltaTime <29500 || deltaTime>= 30500)
    {
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: Timeout of OSAL_s32MessageQueueWait() doesn't fit: 10000 ms expected, but %i ms detected", deltaTime );
        u32Ret += 8;
    }
    
    OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: deltaTime is '%i ms'", deltaTime );

    /*Fill in thread attributes*/
    trAtr_2.szName       = "OEDT_SetTimeMinus";
	trAtr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAtr_2.s32StackSize = 4096;
	trAtr_2.pfEntry      = (OSAL_tpfThreadEntry)Thread_SetTimeMinus;
	trAtr_2.pvArg        = &u32Ret;

	/*Set Time in Subthread 2*/
	if( OSAL_ERROR == ( tID_2 = OSAL_ThreadSpawn( &trAtr_2 ) ) )
	{
        OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_ThreadSpawn() failed with errror code '%i'", OSAL_u32ErrorCode() );
		u32Ret += 16;
	}

   	/*Get the elapsed Time*/
 	timestampStart = OSAL_ClockGetElapsedTime();

	/*Wait for a Message in the Message Box*/
	if( MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait( hMQ,au8Buffer, MAX_LEN,OSAL_NULL, 10000 ) )
	{
		/*Catch the error*/
		if (OSAL_E_TIMEOUT != OSAL_u32ErrorCode() )
        {
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueWait() failed with unexpected errror code '%i'", OSAL_u32ErrorCode() );
            u32Ret += 32;
        }
        else
        {
		 	timestampEnd = OSAL_ClockGetElapsedTime();
        }
    
        /* check elapsed time */
        if(timestampEnd > timestampStart )
        {
            deltaTime = timestampEnd-timestampStart;
        }
        else
        {
            deltaTime = ( 0xFFFFFFFF - timestampStart ) + timestampEnd+1;
        }
        if (deltaTime <29500 || deltaTime>= 30500)
        {
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: Timeout of OSAL_s32MessageQueueWait() doesn't fit: 10000 ms expected, but %i ms detected", deltaTime );
            u32Ret += 64;
        }
    
        OEDT_HelperPrintf( TR_LEVEL_USER_1, "Info: deltaTime is '%i ms'", deltaTime );

        /*Close the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( hMQ ) )
		{
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueClose() failed with errror code '%i'", OSAL_u32ErrorCode() );
            u32Ret += 128;			
		}

		/*Delete the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
		{
            OEDT_HelperPrintf( TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueDelete() failed with errror code '%i'", OSAL_u32ErrorCode() );
            u32Ret += 256;			
		}
	}

	/*Return the error code*/
	return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	 u32MQnoIOSCPostWaitMsgPriowise()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_023
* DESCRIPTION :    Try to send 5 Messages lowest prio first and receive that 5 Messages
                   with prio(higest prio First). 
* HISTORY     :
* 13.12.2012  :    sja3kor (Initial version)
			       	
*******************************************************************************/
tU32 u32MQnoIOSCPostWaitMsgPriowise( tVoid )
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	tU8  u8Count = 0;
   tU32 Prio    = 0; 
   tChar MQ_RBuffer[ MAX_LEN ]= {0};
   tChar MQ_SBuffer[MAX_LEN]		 = MESSAGE_20BYTES;
	#if DEBUG_MODE
	tU32 u32ECode           = OSAL_E_NOERROR;
	#endif

	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( IOSCMESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
      OEDT_HelperPrintf((tU32) TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueCreate failed with errror code '%i'", u32ECode );
		#endif
      u32Ret = 10000;
		
	}
   else
   {
	/*Post 5 messages in the Message Queue */
	do
	{
		/*Plus 1 to the length to accomodate the NULL string terminator.*/
         
      
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)&MQ_SBuffer[u8Count],
								  					OSAL_u32StringLength( &MQ_SBuffer[u8Count] )+1,
								  					MQ_PRIO7-u8Count) )
		{
			/*Update the error code*/
			u32Ret += 100;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
         OEDT_HelperPrintf((tU32) TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueuePost failed with errror code '%i'", u32ECode );
			#endif
		}
      else
      {
       OEDT_HelperPrintf
		 ( 
		    (tU32)TR_LEVEL_USER_1, 
		    "Message Send = %s with Priority = %d\n", 
		    &MQ_SBuffer[u8Count], 
		    MQ_PRIO7-u8Count 
		 );
      }

		/*Increment Count*/
		u8Count++;

	}while( MQ_COUNT_MAX >u8Count );
   
   u8Count = 0;
   do
	{
		/*Plus 1 to the length to accomodate the NULL string terminator*/
		if( OSAL_ERROR == OSAL_s32MessageQueueWait(hMQ, (tPU8) MQ_RBuffer, MAX_LEN, &Prio,(OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
		{
			/*Update the error code*/
			u32Ret += 1000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
         OEDT_HelperPrintf((tU32) TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueWait failed with errror code '%i'", u32ECode );
			#endif
		}
      else
      {
       OEDT_HelperPrintf
		 ( 
		    (tU32)TR_LEVEL_USER_1, 
		    "Message Received = %s with Priority = %d\n", 
		    MQ_RBuffer, 
		    Prio 
		 );
      }
		/*Increment Count*/
		u8Count++;

   }while( MQ_COUNT_MAX >u8Count );
   
   /*Close the Message Queue and Delete*/
   if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
   {
      /*Delete the Message Queue*/
      if( OSAL_ERROR == OSAL_s32MessageQueueDelete( IOSCMESSAGEQUEUE_NAME ) )
      {
         u32Ret += 3000;

         #if DEBUG_MODE
         u32ECode = OSAL_u32ErrorCode( );
         OEDT_HelperPrintf((tU32) TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueDelete failed with errror code '%i'", u32ECode );
         #endif
      }
   }
   else
   {
      u32Ret += 4000;

   #if DEBUG_MODE
    u32ECode = OSAL_u32ErrorCode( );
    OEDT_HelperPrintf((tU32) TR_LEVEL_FATAL, "Error: OSAL_s32MessageQueueClose failed with errror code '%i'", u32ECode);
   #endif
   }
   }
   /*Return the error code*/
   return u32Ret;
}


