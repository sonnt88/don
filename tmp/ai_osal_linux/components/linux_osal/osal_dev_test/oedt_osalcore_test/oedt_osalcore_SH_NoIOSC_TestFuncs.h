/************************************************************************
 *FILE          :oedt_osalcore_SH_NoIOSC_TestFuncs.h
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION   :This file contains function prototypes and some Macros that 
 *				 will be used in the file oedt_osalcore_SH_TestFuncs.c 
 *				 for OSAL_CORE APIs.
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *				            copy of oedt_osalcore_SH_TestFuncs.h
 *
 ************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_SH_NOIOSC_TESTFUNCS_HEADER
#define OEDT_SH_NOIOSC_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define SH_SIZE 100
#define SH_NAME "NOIOSC_SH_MEM"
#define SH_INVAL_NAME "-------|-------|-------|-------|-------|"   /* name length = 40 */

#define SH_MEMSIZE 1000
#define SH_MEMSIZE_512 		512
#undef  VALID_STACK_SIZE
#define VALID_STACK_SIZE 	2048
#define SH_HARRAY 9
#define SH_MAX 9
#define SH_MIN 0
#define INVAL_HANDLE -1
#define OSAL_INVALID_ACCESS 0x0200
#define MAP_OFFSET    20
#define MAP_LENGTH    30
#define LENGTH_127          127
#define ZERO_LENGTH   0
#define ZERO_OFFSET         0
#define OFFSET_0            0
#define OFFSET_128          128
#define OFFSET_256          256
#define OFFSET_384          384
#define OFFSET_511          511
#define OFFSET_800          800
#define PATTERN_THR1        0x00000001
#define PATTERN_THR2        0x00000002
#define PATTERN_THR3        0x00000004
#define PATTERN_THR4        0x00000008
#undef  THREE_SECONDS
#define THREE_SECONDS       3000

#undef 	DEBUG_MODE

#define DEBUG_MODE          0

#define VALID_STACK_SIZE        2048
#define SH_SIZE  100

/* Function prototypes of functions used 
   in file oedt_osalcore_SH_NoIOSC_TestFuncs.c */


/*Test cases*/

tU32 u32SHnoIOSCCreateDeleteNameNULL(tVoid);							//TU_OEDT_SH_001
tU32 u32SHnoIOSCCreateNameVal(tVoid);									//TU_OEDT_SH_002
tU32 u32SHnoIOSCCreateDeleteNameExceedNameLength (tVoid);				//TU_OEDT_SH_003
tU32 u32SHnoIOSCCreateNameSameNames(tVoid);							    //TU_OEDT_SH_004
tU32 u32SHnoIOSCCreateSizeZero(tVoid);								    //TU_OEDT_SH_005
tU32 u32SHnoIOSCDeleteNameInval(tVoid);								    //TU_OEDT_SH_006
tU32 u32SHnoIOSCDeleteNameInUse(tVoid);								    //TU_OEDT_SH_007
tU32 u32SHnoIOSCOpenCloseDiffModes(tVoid);							    //TU_OEDT_SH_008
tU32 u32SHnoIOSCOpenNameNULL( tVoid );							    	//TU_OEDT_SH_009
tU32 u32SHnoIOSCOpenNameInVal(tVoid);									//TU_OEDT_SH_010
tU32 u32SHnoIOSCCloseNameInVal(tVoid);								    //TU_OEDT_SH_011
tU32 u32SHnoIOSCCreateMaxSegment(tVoid);								//TU_OEDT_SH_012
tU32 u32SHnoIOSCMemoryMapNonExistentHandle( tVoid );
tU32 u32SHnoIOSCMemoryMapLimitCheck( tVoid );
tU32 u32SHnoIOSCMemoryMapDiffAccessModes( tVoid );
tU32 u32SHnoIOSCMemoryThreadAccess( tVoid );							//TU_OEDT_SH_016
tU32 u32SHnoIOSCMemoryScanMemory( tVoid );
tU32 u32SHnoIOSCMemoryScanMemoryBeyondLimit( tVoid );					// not active !
tU32 u32SHnoIOSCUnmapNonMappedAddress( tVoid );
tU32 u32SHnoIOSCUnmapNULLAddress( tVoid );
tU32 u32SHnoIOSCUnmapMappedAddress( tVoid );							//TU_OEDT_SH_021
tU32 u32SHnoIOSCOpenDoubleCloseOnce( tVoid );
tU32 u32SHnoIOSCCreateNameValLoop(tVoid);

/*Test cases*/

#endif
