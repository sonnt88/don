/******************************************************************************
 *FILE         : oedt_osalcore_MTX_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the MUTEX OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :
 *              ver1.3   - 22-04-2009
                lint info removal
				sak9kor

 *				ver1.2	 - 14-04-2009
                warnings removal
				rav8kor
 
 				Version 1.1 , 31- 01- 2008
 *				 Added prototype for case
 *				 u32LockUnlockMTXDiffThread
 *
 *				 Version 1.0 , 27- 12- 2007
 *					
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_MTX_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_MTX_TESTFUNCS_HEADER

#define MUTEX_NAME               "IMAMutex"
#define MUTEX_NAME_MAX           "IMAMutexUntil31CharactersLength"
#define MUTEX_NAME_EXCEEDS_MAX   "IMAMutexMoreThan31CharactersLength"
#define MUTEX_PAYLOAD             0
#undef  MAX_LENGTH
#define MAX_LENGTH                256
//#define EIGHT_SECONDS             8000
//#define FOUR_SECONDS              4000
//#undef  THREE_SECONDS
#define OEDT_OSALCORE_MTX_THREE_SECONDS             3000
#undef  TWO_SECONDS
#define TWO_SECONDS               2000
#define ONE_SECONDS               1000
#define PRIORITY_120              (tU32)120
#define PRIORITY_100              (tU32)100

#undef 	TOT_THREADS

#define TOT_THREADS               10
/*rav8kor - debug mode disabled*/
#undef  DEBUG_MODE
#define DEBUG_MODE                0

tU32 u32CreateMTXHandleNULL( tVoid );
tU32 u32CreateMTXNameNULL( tVoid );
tU32 u32CreateMTXNameMAX( tVoid );
tU32 u32CreateMTXNameExceedsMAX( tVoid );
tU32 u32CreateMTXNameExisting( tVoid );

tU32 u32OpenMTXNameNULL( tVoid );
tU32 u32OpenMTXHandleNULL( tVoid );
tU32 u32OpenMTXNonExistent( tVoid );
tU32 u32CloseMTXNonExistent( tVoid );
tU32 u32ReCloseMTX( tVoid );
tU32 u32CloseMTXHandleNULL( tVoid );
tU32 u32CloseMTXBetweenLockUnlock( tVoid );
tU32 u32ReDeleteMTX( tVoid );
tU32 u32DeleteMTXNullHandle( tVoid );
tU32 u32LockMTXNonExisting( tVoid );
tU32 u32ReLockMTX( tVoid );
tU32 u32LockUnlockMTXValid( tVoid );
tU32 u32LockMTXHandleNull( tVoid );
tU32 u32UnlockMTXHandleNull( tVoid );
tU32 u32ReUnlockMTX( tVoid );
tU32 u32UnlockMTXBeforeLock( tVoid );
tU32 u32DeleteMTXBeforeClose( tVoid );
tU32 u32LockUnlockMTXAfterDelete( tVoid );
tU32 u32OpenCounterCheckMTX( tVoid );

tU32 u32LockUnlockMTXDiffThread( tVoid );

#endif
