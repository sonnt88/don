/******************************************************************************
 *FILE         : processMain.c
 *
 *
 *SW-COMPONENT : Test Process
 *
 *
 *DESCRIPTION  : Contains the body of a Custom Test process to aid
 *               in osal IPC feature test
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2008, Robert Bosch India Limited
 *
 *HISTORY      :
 *				 
 *				ver1.5	 - 30-04-2009
                warnings removal
				rav8kor

 				ver1.4	 - 14-04-2009
                warnings removal
				rav8kor

 				 Version 1.3
 *				 Event clear update by  
 *				 Anoop Chandran (RBEI\ECM1) 27/09/2008
 *
 * 		    Version 1.2
 *				 Tinoy Mathews( RBEI/ECM1 )
 *				 Modified the feature access mechanism to a notification
 *				 ( Event Model ) mechanism placed within the body of a new 
 *				 thread	"vCommonInterface"
 *
 *				 Version 1.1
 *				 Tinoy Mathews( RBIN/ECM1 )
 *				 Added updates related to 
 *				 oedt_osalcore_IPC_TestFuncs.c( ver 1.1 )
 *
 *				 Version 1.0
 *				 Initial version - Tinoy Mathews( RBIN/EDI3 )	
 *
 *****************************************************************************/

#include <extension/outer.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "processMain.h"
#include "oedt_helper_funcs.h"



/*GLOBAL DATA*/
FPTR fptr[FUNCTION_PTR_ARRAY_SIZE]  = {
								   		vUpdateSHErrorFieldDiffProcessCtxt, /*00*/
								   		vWaitOnSemaphore,					/*01*/										
										vPostOnSemaphoreMoreThanMax,		/*02*/
										vDelSemaphore,						/*03*/
										vDelSemOpenCounter,					/*04*/
										vCreateSemSameName,					/*05*/
										vCreateEventSameName,				/*06*/
										vPostEventPattern,					/*07*/
										vDeleteEvent,						/*08*/
										vMQSameName,						/*09*/
										vMQDelete,							/*10*/
										vMQPost,							/*11*/
										vMQPostBeyonMax,					/*12*/
										vMQMapSharedMemoryBeyondMax,		/*13*/
										vMSGPLCreateMSG,					/*14*/
										vSemStressTest,						/*15*/
										vEventStressTest,					/*16*/
										vMessageQueueStressTest,			/*17*/
										NULL									 
							  	      };

									
OSAL_tShMemHandle sh_Handle_pr    = 0;
OSAL_tEventHandle   eveIPC  	  = 0;
SH_DATA_PTR       sh_data_pointer = {NULL,NULL};
/*Check to ascertain the Event Open and Shared Memory open
in successful together always*/
tU8 u8Check                       = 0x00;


/*Stress Test Semaphore*/
OSAL_tShMemHandle shHandleIPC   	  = 0;
OSAL_tSemHandle baseSemHandleIPC  = 0;
tU32 u32GlobCounterIPC            = 0;
tU8 * shptr_osalrsc                     = OSAL_NULL;

SemaphoreTableEntryIPC sem_StressListIPC[MAX_OSAL_RESOURCES];
EventTableEntryIPC     event_StressListIPC[MAX_OSAL_RESOURCES];
MessageQueueTableEntryIPC mesgque_StressListIPC[MAX_OSAL_RESOURCES];

OSAL_trThreadAttribute post_ThreadAttrIPC[NO_OF_POST_THREADS];
OSAL_trThreadAttribute wait_ThreadAttrIPC[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute close_ThreadAttrIPC[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute create_ThreadAttrIPC[NO_OF_CREATE_THREADS];
OSAL_tThreadID post_ThreadIDIPC[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID wait_ThreadIDIPC[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID close_ThreadIDIPC[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID create_ThreadIDIPC[NO_OF_CREATE_THREADS]           = {OSAL_NULL};

OSAL_tEventMask    gevMask;

  
/*********************************************************************************
Function name 	    : main
Feature number 	    : 0
Purpose       	    : Entry Point of the Process ProcOEDT_PF_Test
Furthur Information : 
					  COMMUNICATION MODEL:
					  1). The Process ProcOEDT_PF_Test will act as a SERVER
					  2). The Process ProcOEDT_PF will act as a CLIENT
					  3). ProcOEDT_PF will send a signal to ProcOEDT_PF_Test
					  4). ProcOEDT_PF_Test will serve the signal by calling the 
					  	  appropriate functionality
					  5). ProcOEDT_PF_Test loops infinitely and waits 
					  	  for signal from ProcOEDT_PF to provide for the 
					  	  next service
						
Created By          : Tinoy Mathews( RBIN/ECM1 ),29- 02- 2008
*********************************************************************************/
void vStartApp(void)
{
	OSAL_trThreadAttribute trAttr;
	OSAL_tThreadID trID = 0;
	tS8 tStr[20]        = "TEST_PROCESS_THREAD";

	/*Initialize thread attributes*/
	trAttr.szName                 =  (tString)tStr;
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  (OSAL_tpfThreadEntry)vCommonInterface;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Create Thread*/
	trID = OSAL_ThreadSpawn( &trAttr );

	/*Wait infinitly*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Remove the Thread from system*/
	OSAL_s32ThreadDelete( trID );

	
  	
}

/*********************************************************************************
Function name 	   : vCommonInterface
Purpose       	   : Thread Body which calls the Feature test functions
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vCommonInterface( tVoid * ptr )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	/*Open the IPC Event*/
  	if( OSAL_ERROR == OSAL_s32EventOpen( "EVE_IPC", &eveIPC ) )
  	{
		/*If the Event Open fails, then the Custom Process expects
		that a non IPC function is spawning the Process,in that
		case, call a custom function*/
		vCustomFunction( );

		/*Return*/
		return;
	}
  
  	//while( 1 )
	/*loop continuously and wait for events*/
	for(;;)
  	{
  		/*Wait for a response from Client( ProcOEDT_PF )*/
		OSAL_s32EventWait( 
							eveIPC,
							CLIENT_TO_SERVER,
							OSAL_EN_EVENTMASK_AND,
							OSAL_C_TIMEOUT_FOREVER, 
							&gevMask 
						 );
		
		/*Clear the event*/
		OSAL_s32EventPost
		( 
		eveIPC,
	  ~(gevMask),
	   OSAL_EN_EVENTMASK_AND
	    );

  	    /*Open the Shared Memory*/
  		if( OSAL_ERROR != ( sh_Handle_pr = OSAL_SharedMemoryOpen( "SH_DATA_REP",OSAL_EN_READWRITE ) ) )
  		{
  			/*Map the shared memory - Upto the Choice byte*/
  			sh_data_pointer.sh_ptr_CB = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,2,0 ); 

  			/*Run the functionality only if the Choice byte is in the 
			bounds of the Function Pointer Array*/
  			if( *( sh_data_pointer.sh_ptr_CB ) <= ( FUNCTION_PTR_ARRAY_SIZE-1 ) )
			{
  				/*Choose the test functionality to run*/
  				fptr[*( sh_data_pointer.sh_ptr_CB )]( );
			}

  			/*Close the shared memory*/
  			OSAL_s32SharedMemoryClose( sh_Handle_pr );
   		}

		/*Send Response( Notify ) to the Client that the Server completed 
		one service*/
		OSAL_s32EventPost( 
							eveIPC,
							SERVER_TO_CLIENT,
							OSAL_EN_EVENTMASK_OR 
						 );
	   
	}

	
}

/*********************************************************************************
Function name 	   : vUpdateSHErrorFieldDiffProcessCtxt
Feature number 	   : 0
Purpose       	   : Update Shared Memory and verify whether the update is 
						 reflected in the Spawning Process
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vUpdateSHErrorFieldDiffProcessCtxt( tVoid )
{	
	/*Map to Error byte*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	/*Update error code*/
	*( sh_data_pointer.sh_ptr_EB ) = 0xFF;
}


/*********************************************************************************
Function name 	   : vWaitOnSemaphore
Feature number 	   : 1
Purpose       	   : Wait on a Semaphore Created in the Process
						 ProcOEDT_PF
Created By           : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vWaitOnSemaphore( tVoid )
{
	/*Declarations*/
	OSAL_tSemHandle semWait = 0;

	/*Map to Error byte*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

	/*Open the Wait Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreOpen( "WAIT_SEM",&semWait ) )	
	{
		/*Wait on the Wait Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreWait( semWait,OSAL_C_TIMEOUT_FOREVER ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 5;
		}

		/*Close the Wait Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semWait ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 20;			
		}
	}
	else
	{
		*( sh_data_pointer.sh_ptr_EB ) += 10;
	}
}	

/*********************************************************************************
Function name 	   : vPostOnSemaphoreMoreThanMax
Feature number 	   : 2
Purpose       	   : Post on a Semaphore Created in the Process
						 ProcOEDT_PF with maximum count 65535 once in the
						 Custom Process.The Post in the Custom Process
						 should fail.
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vPostOnSemaphoreMoreThanMax( tVoid )
{
	/*Declarations*/
	OSAL_tSemHandle semPost = 0;

	/*Map to Error byte*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

	/*Open the Post Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreOpen( "POST_SEM",&semPost ) )	
	{
		/*Post once on the Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphorePost( semPost ) )
		{
		   *( sh_data_pointer.sh_ptr_EB ) += 100;		
		}

		/*Close the Post Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semPost ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 20;			
		}
	}
	else
	{
		*( sh_data_pointer.sh_ptr_EB ) += 10;
	}
}

/*********************************************************************************
*Function name 	   : vDelSemaphore
*Feature number    : 3
*Purpose       	   : Delete the Semaphore created created in process
						 ProcOEDT_PF in the custom process
*Created By        : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vDelSemaphore( tVoid )
{
	/*Declarations*/
	OSAL_tSemHandle semTest = 0;

	/*Map to Error byte*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

	/*Open the Wait Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreOpen( "TEST_SEM",&semTest ) )	
	{
		/*Close the Wait Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semTest ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 20;			
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "TEST_SEM" ) )
			{
				*( sh_data_pointer.sh_ptr_EB ) += 15;
			}
		}							
	}
	else
	{
		*( sh_data_pointer.sh_ptr_EB ) += 10;
	}
}

/*********************************************************************************
Function name 	   : vDelSemOpenCounter
Feature number 	   : 4
Purpose       	   : Delete the Semaphore created created in process
						 ProcOEDT_PF in the custom process
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vDelSemOpenCounter( tVoid )
{
	/*Declarations*/
	OSAL_tSemHandle semDel = 0;

	/*Map to Error byte*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

	/*Open the Del Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreOpen( "DEL_SEM",&semDel ) )	
	{
		/*Close the Wait Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semDel ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 20;			
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "DEL_SEM" ) )
			{
				*( sh_data_pointer.sh_ptr_EB ) += 15;
			}
		}							
	}
	else
	{
		*( sh_data_pointer.sh_ptr_EB ) += 10;
	}
   
}			

/*********************************************************************************
Function name 	   : vCreateSemSameName
Feature number 	   : 5
Purpose       	   : Try to Create a semaphore with the same name as the one 
					 created in ProcOEDT_PF,it should fail with return error
						 OSAL_E_ALREADYEXISTS
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vCreateSemSameName( tVoid )
{
   /*Declarations*/	
   tU32 u32ECode             = OSAL_E_NOERROR;
   OSAL_tSemHandle semCommon = 0;

   /*Map to Error byte*/
   sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

   /*Create a Semaphore with the same name as in ProcOEDT_PF*/
   if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "COMMON_SEM",&semCommon,1 ) )
   {
	   /*Query the error code*/
	   u32ECode = OSAL_u32ErrorCode( );
	   if( OSAL_E_ALREADYEXISTS != u32ECode )
	   {
		   *( sh_data_pointer.sh_ptr_EB ) += 5;			
	   }   	 					
   }
   else
   {
		*( sh_data_pointer.sh_ptr_EB ) += 10;

		/*Attempt to close the Semaphore*/
		OSAL_s32SemaphoreClose( semCommon );
   }
}

/*********************************************************************************
Function name 	   : vCreateEventSameName
Feature number 	   : 6
Purpose       	   : Try to Create an Event with the same name as the one 
						 created in ProcOEDT_PF,it should fail with return error
						 OSAL_E_ALREADYEXISTS
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vCreateEventSameName( tVoid )
{
   /*Declarations*/	
   tU32 u32ECode = OSAL_E_NOERROR;
   OSAL_tEventHandle eventCommon = 0;

   /*Map to Error byte*/
   sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

   /*Create an Event with the same name as in ProcOEDT_PF*/
   if( OSAL_ERROR == OSAL_s32EventCreate( "COMMON_EVENT",&eventCommon ) )
   {
		/*Query the error code*/
	   u32ECode = OSAL_u32ErrorCode( );
	   if( OSAL_E_ALREADYEXISTS != u32ECode )
	   {
		   *( sh_data_pointer.sh_ptr_EB ) += 5;			
	   }				
   }
   else
   {
		*( sh_data_pointer.sh_ptr_EB ) += 10;

		/*Attempt to close the Semaphore*/
		OSAL_s32EventClose( eventCommon );
   }
}

/*********************************************************************************
Function name 	   : vPostEventPattern
Feature number 	   : 7
Purpose       	   : Post a Pattern to an event created in process ProcOEDT_PF
						 and which is waiting in ProcOEDT_PF process
Created By           : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vPostEventPattern( tVoid )
{
   /*Declarations*/	
   OSAL_tEventHandle eventPost = 0;

   /*Map to Error byte*/
   sh_data_pointer.sh_ptr_EB   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

   /*Open the event field*/
   if( OSAL_ERROR != OSAL_s32EventOpen( "WAITPOST_EVENT",&eventPost ) )
   {
		/*Post a pattern to free the wait in ProcOEDT_PF*/
		OSAL_s32EventPost( eventPost,0x00000004,OSAL_EN_EVENTMASK_OR );

		/*Close the Event Field*/
		if( OSAL_ERROR == OSAL_s32EventClose( eventPost ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 5;		
		}
   }
   else
   {
		*( sh_data_pointer.sh_ptr_EB ) += 10;
   }	
}

/*********************************************************************************
Function name 	   : vDeleteEvent
Feature number 	   : 8
Purpose       	   : Delete an Event Created in process ProcOEDT_PF in the 
						 Custom process
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vDeleteEvent( tVoid )
{
   /*Declarations*/	
   OSAL_tEventHandle eventDel = 0;

   /*Map to Error byte*/
   sh_data_pointer.sh_ptr_EB   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

   /*Open the event field*/
   if( OSAL_ERROR != OSAL_s32EventOpen( "DEL_EVENT",&eventDel ) )
   {
		/*Close the Event Field*/
		if( OSAL_ERROR == OSAL_s32EventClose( eventDel ) )
		{
			*( sh_data_pointer.sh_ptr_EB ) += 5;		
		}
		else
		{
			/*Delete the Event*/
			if( OSAL_ERROR == OSAL_s32EventDelete( "DEL_EVENT" ) )
			{
				*( sh_data_pointer.sh_ptr_EB ) += 50;			
			}
		}
   }
   else
   {
		*( sh_data_pointer.sh_ptr_EB ) += 100;
   }	
}

/*********************************************************************************
Function name 	   : vMQSameName
Feature number 	   : 9
Purpose       	   : Create a message queue with same name as the one 
						 created in Process ProcOEDT_PF
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/			
tVoid vMQSameName( tVoid )
{
	/*Declarations*/
	
	OSAL_tMQueueHandle mqIPC = 0;

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	
	/*Create the message queue*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( "MQ_IPC",MAX_MSGS,MAX_MSG_LENGTH,OSAL_EN_READWRITE,&mqIPC ) )
	{
		if( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode( ) )
		{
			 *( sh_data_pointer.sh_ptr_EB ) += 5;
		}
	}
	else
	{
		*( sh_data_pointer.sh_ptr_EB ) += 10;		

		/*Attempt to Close the Message Queue*/	
		OSAL_s32MessageQueueClose( mqIPC );
	}
}

/*********************************************************************************
Function name 	   : vMQDelete
Feature number 	   : 10
Purpose       	   : Delete a message queue which was created in ProcOEDT_PF
						 before ProcOEDT_PF deletes it.
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/			
tVoid vMQDelete( tVoid )
{
	/*Declarations*/
	OSAL_tMQueueHandle mqIPC = 0;

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	
	/*Open the message queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueOpen( "MQ_IPC",OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Close the message queue*/
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC ) )
		{
			/*Delete the message queue*/
			if( OSAL_ERROR == OSAL_s32MessageQueueDelete( "MQ_IPC" ) )
			{
				/*Update error code*/
				*( sh_data_pointer.sh_ptr_EB ) += 20;
			}
		}
		else
		{
			/*Update error code*/
			*( sh_data_pointer.sh_ptr_EB ) += 15;							
		}
	}
	else
	{
		/*Update error code*/
		*( sh_data_pointer.sh_ptr_EB ) += 10;				
	}
}


/*********************************************************************************
Function name 	   : vMQPost
Feature number 	   : 11
Purpose       	   : Post a message to Process ProcOEDT_PF
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/			
tVoid vMQPost( tVoid )
{
	/*Declarations*/
	tU8  mqBuffer[20]        = "ABCDEFGHIJKLMNOPQRS";
	OSAL_tMQueueHandle mqIPC = 0;

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	
	/*Open the message queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueOpen( "MQ_IPC",OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Post a message to the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( mqIPC,mqBuffer,20,1 ) )
		{
			/*Update error code*/
			sh_data_pointer.sh_ptr_EB += 10;
		}	
		
		/*Close the  Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( mqIPC ) )
		{
			/*Update error code*/
			sh_data_pointer.sh_ptr_EB += 20;	
		}
	}
	else
	{
		/*Update error code*/
		sh_data_pointer.sh_ptr_EB += 30;
	}
}

/*********************************************************************************
Function name 	   : vMQPostBeyonMax
Feature number 	   : 12
Purpose       	   : Post a message to Process ProcOEDT_PF
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/			
tVoid vMQPostBeyonMax( tVoid )
{
	/*Declarations*/
	tU8  mqBuffer[20]        = "ABCDEFGHIJKLMNOPQRS";
	OSAL_tMQueueHandle mqIPC = 0;

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	
	/*Open the message queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueOpen( "MQ_IPC",OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Post a message to the Message Queue - This
		Post is expected to fail*/
		if( OSAL_ERROR != OSAL_s32MessageQueuePost( mqIPC,mqBuffer,20,1 ) )
		{
			/*Update error code*/
			sh_data_pointer.sh_ptr_EB += 10;
		}	
		
		/*Close the  Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( mqIPC ) )
		{
			/*Update error code*/
			sh_data_pointer.sh_ptr_EB += 20;	
		}
	}
	else
	{
		/*Update error code*/
		sh_data_pointer.sh_ptr_EB += 30;
	}
}


/*********************************************************************************
Function name 	   : vMQMapSharedMemoryBeyondMax
Feature number 	   : 13
Purpose       	   : Map in Shared Memory beyond the Shared Memory size limit
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/			
tVoid vMQMapSharedMemoryBeyondMax( tVoid )
{
	/*Declarations*/

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );

	/*Try to map beyond the size of shared memory*/
	if( OSAL_NULL != OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1000 ) )
	{
		/*Update the error code*/
		sh_data_pointer.sh_ptr_EB += 200;	
	}
}

/*********************************************************************************
Function name 	   : vMSGPLCreateMSG
Feature number 	   : 14
Purpose       	   : Create a Message within a Message Pool that had been
						 created in process ProcOEDT_PF
Created By         : Tinoy Mathews( RBIN/ECM1 ),20- 02- 2008
*********************************************************************************/
tVoid vMSGPLCreateMSG( tVoid )
{
	/*Declarations*/
	OSAL_trMessage msg;

	/*Map to the error byte of the shared memory*/
	sh_data_pointer.sh_ptr_EB  	   = ( tU8 * )OSAL_pvSharedMemoryMap( sh_Handle_pr,OSAL_EN_READWRITE,0,1 );
	
	/*Open the Message Pool*/
	if( OSAL_ERROR == OSAL_s32MessagePoolOpen( ) )
	{
		sh_data_pointer.sh_ptr_EB += 10;
	}

	/*Create a Message within the Message Pool*/
	if( OSAL_ERROR == OSAL_s32MessageCreate( &msg,1000,OSAL_EN_MEMORY_SHARED ) )
	{
		/*Update the error code*/
		sh_data_pointer.sh_ptr_EB += 20;
	}

	/*Close the Message Pool*/
	if( OSAL_ERROR == OSAL_s32MessagePoolClose( ) )
	{
		sh_data_pointer.sh_ptr_EB += 30;
	}
}

 
/*STRESS TEST SEMAPHORE*/

/*GENERIC FUNCTIONS*/
/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tS32 s32RandomNoGenerate( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );
	
	/*Return the Positive Random Number*/
	return s32Rand;
}
/*****************************************************************************
* FUNCTION    :	   vSelectThreadPriorityRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Random Priority Select
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vSelectThreadPriorityRandom( )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand     = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    tU8
* DESCRIPTION :    Helper Function - Random Select Semaphore Index
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tU8 u8SelectIndexRandom( )
{
	/*Declarations*/
	tU8 u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand	    = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX; 
	/*Calculate the Semaphore Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_OSAL_RESOURCES );
	/*Return the Semaphore Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandom()
* PARAMETER   :    tU32
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Random Wait Function.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vWaitRandom(tU32 u32ModVal )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand        = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32ModVal +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}
/*GENERIC FUNCTIONS*/

/*SEMAPHORE SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   Post_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Post 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Sem( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	//while( 1 )
	for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );

		/*Randomly Select the Index*/
		u8Index = u8SelectIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressListIPC[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{
			/*Post on the opened semaphore*/
			OSAL_s32SemaphorePost( smphrHandle );

			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32SemaphoreClose ( smphrHandle );
			}
			else
			{
				OSAL_s32SemaphoreClose ( smphrHandle );
				vWaitRandom( FIVE_SECONDS );
			}
		}
	}	
}

/*****************************************************************************
* FUNCTION    :	   Wait_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Wait 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Sem( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
   //while( 1 )
    for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );

		/*Randomly Select the Index*/
		u8Index = u8SelectIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressListIPC[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{
			/*Post on the opened semaphore*/
			OSAL_s32SemaphoreWait( smphrHandle,OSAL_C_TIMEOUT_FOREVER );

			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32SemaphoreClose ( smphrHandle );
			}
			else
			{
				OSAL_s32SemaphoreClose ( smphrHandle );
				vWaitRandom( FIVE_SECONDS );
			}
		}
	}
}

/*****************************************************************************
* FUNCTION    :	   Close_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Delete and Close 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Sem( tVoid *vptr )
{
  	/*Declarations*/
	tU8 u8Index  = 0;
	tU8 u8Count  = 0;
	tU8 u8Offset = 0;
	tBool dFlag  = FALSE;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	//while( 1 )
	for(;;)
	{
		/*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Randomly select the semaphore index*/
		u8Index = u8SelectIndexRandom( );
		/*Calculate offset within Shared Memory*/
		u8Offset = (tU8)(u8Index*3);

		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		/*Update the To be deleted flag*/
		if( 'F' == *( shptr_osalrsc + ( u8Offset + 2 ) ) )
		{
			dFlag = FALSE;
		}
		else
		{
			dFlag = TRUE;
		}


		if( ( dFlag != TRUE ) && (sem_StressListIPC[u8Index].hSem != OSAL_C_INVALID_HANDLE ) )
		{
			/*Set the to be deleted flag*/
			dFlag = TRUE;
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'T';

			OSAL_s32SemaphorePost( baseSemHandleIPC );
		
			/*Do a Delete on the specific semaphore at the randomly selected index*/
			if( OSAL_s32SemaphoreDelete( sem_StressListIPC[u8Index].hSemName )	== OSAL_OK )
			{
				/*Post 100 times on a deleted Semaphore*/
				while( u8Count < 100 )
				{
					/*Post on the Semaphore*/
					OSAL_s32SemaphorePost( sem_StressListIPC[u8Index].hSem );
					/*Increment count*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Close the Semaphore*/
				OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );

				/*Invalidate the handle*/
				sem_StressListIPC[u8Index].hSem = OSAL_C_INVALID_HANDLE;
			}

			/*Semaphore at this index already deleted*/
			dFlag = FALSE;
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		}
		else
		{
			OSAL_s32SemaphorePost( baseSemHandleIPC );
		}
	}
}

/*****************************************************************************
* FUNCTION    :	   Create_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Create 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Sem( tVoid *vptr )
{
	/*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	 //while (1)   
	 for(;;)
   	 {
        /*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Select semaphore index randomly*/
      	u8Index = u8SelectIndexRandom();

      	/*Check whether handle to the corresponding index is invalid*/
      	if (sem_StressListIPC[u8Index].hSem == OSAL_C_INVALID_HANDLE)
      	{
         	/*Create the semaphore*/
         	OSAL_s32SemaphoreCreate (sem_StressListIPC[u8Index].hSemName, &sem_StressListIPC[u8Index].hSem, 0);         
      	}
		/*Wait randomly*/
      	vWaitRandom( FIVE_SECONDS );
    }
}
/*SEMAPHORE SPECIFIC*/

/*EVENT SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   u32SelectEventPatternRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Patterns Randomly 
				   - For Both Post and Wait
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tU32 u32SelectEventPatternRandom( tVoid )
{
	/*Declarations*/
	tS32 s32Rand           = RAND_MAX;
	tU32 u32evPatternIndex = 0xFFFFFFFF;
	tU32 u32evPattern      = 0x00000000;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Find calculate the event Pattern Index*/
	u32evPatternIndex = ( (tU32)s32Rand%MAX_PATTERNS );

	/*Return the True Pattern*/
	switch( u32evPatternIndex )
	{
		case 0: u32evPattern = PATTERN_1;			break;
		case 1:	u32evPattern = PATTERN_2;			break;
		case 2:	u32evPattern = PATTERN_4;			break;
		case 3:	u32evPattern = PATTERN_8;			break;
		case 4:	u32evPattern = PATTERN_16;			break;
		case 5:	u32evPattern = PATTERN_32;			break;
		case 6:	u32evPattern = PATTERN_64;			break;
		case 7:	u32evPattern = PATTERN_128;			break;
		case 8:	u32evPattern = PATTERN_256;			break;
		case 9:	u32evPattern = PATTERN_512;			break;
		case 10:u32evPattern = PATTERN_1024;		break;
		case 11:u32evPattern = PATTERN_2048;		break;
		case 12:u32evPattern = PATTERN_4096;		break;
		case 13:u32evPattern = PATTERN_8192;		break;
		case 14:u32evPattern = PATTERN_16384;		break;
		case 15:u32evPattern = PATTERN_32768;		break;
		case 16:u32evPattern = PATTERN_65536;		break;
		case 17:u32evPattern = PATTERN_131072;		break;
		case 18:u32evPattern = PATTERN_262144;		break;
		case 19:u32evPattern = PATTERN_524288;		break;
		case 20:u32evPattern = PATTERN_1048576;		break;
		case 21:u32evPattern = PATTERN_2097152;		break;
		case 22:u32evPattern = PATTERN_4194304;		break;
		case 23:u32evPattern = PATTERN_8388608;		break;
		case 24:u32evPattern = PATTERN_16777216;	break;
		case 25:u32evPattern = PATTERN_33554432;	break;
		case 26:u32evPattern = PATTERN_67108864;	break;
		case 27:u32evPattern = PATTERN_134217728;	break;
		case 28:u32evPattern = PATTERN_268435456;	break;
		case 29:u32evPattern = PATTERN_536870912;	break;
		case 30:u32evPattern = PATTERN_1073741824;	break;
		case 31:u32evPattern = PATTERN_2147483648;	break;
		default:break;
	}

	/*Return the pattern*/
	return u32evPattern;
}
/*****************************************************************************
* FUNCTION    :	   Post_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Event Thread 
				   - To post an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Eve( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	
	//while( 1 )
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

	
		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressListIPC[u8EventIndex].hEventName,&event_StressListIPC[u8EventIndex].hEve ) )
		{
						
			/*Select the Post Pattern Randomly*/
			u32PostPattern = u32SelectEventPatternRandom( );

			/*Post Randomly selected pattern to event pattern a Random Index*/
			OSAL_s32EventPost( event_StressListIPC[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR );

			/*Close Event Field at the Randomly Selected Index*/
			if( u8SelectIndexRandom( ) < (MAX_OSAL_RESOURCES/2) )
			{
		   		vWaitRandom( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );	
		   		vWaitRandom( FIVE_SECONDS );
			}

		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Wait_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Event Thread 
				   - To Wait for an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Eve( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32WaitPattern = 0x00000000;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
   	
	//while( 1 )
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

		
		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressListIPC[u8EventIndex].hEventName,&event_StressListIPC[u8EventIndex].hEve ) )
		{
		 
			/*Select the Wait Pattern Randomly*/
			u32WaitPattern = u32SelectEventPatternRandom( );
		 

			/*Wait for a Pattern*/
			OSAL_s32EventWait( 
							   event_StressListIPC[u8EventIndex].hEve,
							   u32WaitPattern,
							   OSAL_EN_EVENTMASK_OR,
							   OSAL_C_TIMEOUT_FOREVER,
							   &gevMask 
							  );
			/*Clear the event*/
			OSAL_s32EventPost
			( 
			event_StressListIPC[u8EventIndex].hEve,
			~(gevMask),
		   OSAL_EN_EVENTMASK_AND
		    );


			/*Close Event Field at the randomly selected index*/
			if( u8SelectIndexRandom( ) < (MAX_OSAL_RESOURCES/2) )
			{
		   		vWaitRandom( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );	
		   		vWaitRandom( FIVE_SECONDS );
			}

		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Close_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Delete and Close an Event field at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Eve( tVoid *ptr )
{	
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;
	tU8  u8Count 		= 0;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	//while( 1 )
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select event index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

		/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		/*Check if the event handle is invalid*/
		if( event_StressListIPC[u8EventIndex].hEve != OSAL_C_INVALID_HANDLE )
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );
			
			/*Delete the Event*/
			if( OSAL_s32EventDelete( event_StressListIPC[u8EventIndex].hEventName ) == OSAL_OK )
			{

				/*Post Randomly 200 times*/
				while( u8Count < 200 )
				{
					/*Select the Post Pattern Randomly*/
					u32PostPattern = u32SelectEventPatternRandom( );

					/*Post a Pattern to event pattern a random index*/
					OSAL_s32EventPost( event_StressListIPC[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR );			

					/*Increment the counter*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Delete the event resources*/
				OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );

							
				/*Invalidate the event handle for reuse*/
				event_StressListIPC[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;

			}/*Event Delete Success*/
		}/*Handle Valid*/
		else
		{
		   OSAL_s32SemaphorePost( baseSemHandleIPC );
		}	
	}/*Loop Infinitely*/
}

/*****************************************************************************
* FUNCTION    :	   Create_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Create an Event field at Random Event indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Eve( tVoid *ptr )
{
	 /*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 //while (1)   
	 for(;;)
   	 {
        /*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Select event index randomly*/
      	u8Index = u8SelectIndexRandom();

      	/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (event_StressListIPC[u8Index].hEve == OSAL_C_INVALID_HANDLE)
      	{
         	/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

         	/*Create the event*/
         	OSAL_s32EventCreate( event_StressListIPC[u8Index].hEventName,&event_StressListIPC[u8Index].hEve );
      	}
		else
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );
		}

		/*Wait randomly*/
      	vWaitRandom( FIVE_SECONDS );
     }
}
/*EVENT SPECIFIC*/

/*MESSAGE QUEUE SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   u8SelectMessagePriority()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Priority for a message to 
  				   be sent to a Message Queue.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
				   Updated By Tinoy Mathews( RBIN/ECM1 ) 20 Dec,2007
*******************************************************************************/
static tU32 u32SelectMessagePriority( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = RAND_MAX;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Return the Message Priority*/
	return (tU32)( s32Rand%((tS32)MQ_PRIO7+1) );
}
/******************************************************************************
* FUNCTION    :	   Post_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Message Thread 
				   - To continually post Message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tU8 u8Index             = 0xFF;
	tU32 u32MessagePriority = 0xFFFFFFFF;
	tChar aBuf[MAX_LEN]		= "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	//while( 1 )
	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen( 
								  		mesgque_StressListIPC[u8Index].coszName,
								  		mesgque_StressListIPC[u8Index].enAccess,
								  		&mesgque_StressListIPC[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueuePost( mesgque_StressListIPC[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );

			/*Close the Message Queue*/
			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				vWaitRandom( FIVE_SECONDS );
			}
			else
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/
}

/******************************************************************************
* FUNCTION    :	   Wait_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Message Thread 
				   - To continually Wait for 
				   message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tChar aBuf[MAX_LEN];
	tU8 u8Index;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	//while( 1 )
	for(;;)
	{
	   /*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen( 
								  		mesgque_StressListIPC[u8Index].coszName,
								  		mesgque_StressListIPC[u8Index].enAccess,
								  		&mesgque_StressListIPC[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueueWait( mesgque_StressListIPC[u8Index].phMQ,
									  (tPU8)aBuf,
									  MAX_LEN,
									  OSAL_NULL,
									  OSAL_C_TIMEOUT_FOREVER );

			/*Close the Message Queue*/
			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				vWaitRandom( FIVE_SECONDS );
			}
			else
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/	
}

/******************************************************************************
* FUNCTION    :	   Close_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Close Message Thread 
				   - To continually Close  
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tU8 u8Index;
	tU8 u8Offset 			 = 0;
	tU8 u8Count = 0;
	tChar aBuf[MAX_LEN]		 = "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/
	tU32  u32MessagePriority = 0xFFFFFFFF;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	//while( 1 )
	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Calculate offset within Shared Memory*/
		u8Offset = (tU8)(u8Index*3);

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		/*Sync*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		if( (  *( shptr_osalrsc + ( u8Offset + 2 ) ) == 'F' ) && ( mesgque_StressListIPC[u8Index].phMQ != OSAL_C_INVALID_HANDLE ) )
		{
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'T';
				
			/*Sync*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

			/*Delete the message queue*/
			if( OSAL_s32MessageQueueDelete( mesgque_StressListIPC[u8Index].coszName ) == OSAL_OK )
			{
				/*Random Delay*/
				vWaitRandom( FIVE_SECONDS );

				/*Fire 100 posts*/
				while( u8Count < 100 )
				{
					/*Post the message into the message queue*/
					OSAL_s32MessageQueuePost( mesgque_StressListIPC[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );	
					u8Count++;
				}

				/*Reset the count*/
				u8Count = 0;

				/*Delete the message Queue*/
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				
				/*Reset the handle at the index*/
				mesgque_StressListIPC[u8Index].phMQ = OSAL_C_INVALID_HANDLE;
			}

			/*Update the to be deleted field in Shared memory*/
			 *( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';
		}
		else
		{
		   OSAL_s32SemaphorePost( baseSemHandleIPC );
		}	
	}		
}

/******************************************************************************
* FUNCTION    :	   Create_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Create Message Thread 
				   - To continually Create 
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Msg( tVoid *ptr )
{
	 /*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 //while (1)
	 for(;;)   
   	 {
        /*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

      	/*Sync*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (mesgque_StressListIPC[u8Index].phMQ == OSAL_C_INVALID_HANDLE)
      	{
         	/*Sync*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

         	/*Create the Message Queue*/
         	OSAL_s32MessageQueueCreate(
										mesgque_StressListIPC[u8Index].coszName,
										mesgque_StressListIPC[u8Index].u32MaxMessages,
										mesgque_StressListIPC[u8Index].u32MaxLength,
										mesgque_StressListIPC[u8Index].enAccess,
										&mesgque_StressListIPC[u8Index].phMQ	
									  );
      	}
		/*Random Delay*/
		vWaitRandom( FIVE_SECONDS );
     }
}
/*MESSAGE QUEUE SPECIFIC*/

/*GENERAL PORTAL FUNCTION*/
/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute 
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Portal for Thread Spawn			       	
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vEntryFunction( 
					  		void (*ThreadEntry) (void *),
					  		const tC8 *threadName,
                      		tU8 u8NoOfThreads, 
                      		OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[] 
                    )
{
	/*Definitions*/
	tU8 u8Index                                         = 0;
	
	tChar acBufThreadName[TOT_THREADS][THREAD_NAME_LEN];
	                                                       	
	OSAL_pvMemorySet(acBufThreadName, 0, sizeof(acBufThreadName));


	/*Spawn the required number of tasks*/
	while( u8Index < u8NoOfThreads )
	{
		/*Fill in the Thread Name*/
		OSAL_s32PrintFormat( acBufThreadName[u32GlobCounterIPC],"%s_%d",threadName,u8Index );

		/*Fill in the Thread Attribute structure*/
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;/*2 KB Minimum*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pfEntry      = ThreadEntry;
		tr_Attr[u8Index].szName       =	acBufThreadName[u32GlobCounterIPC];

		/*Spawn*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			// Do nothing
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobCounterIPC++;
	}
}
/*GENERAL PORTAL FUNCTION*/

/*********************************************************************************
Function name 	   : vSemStressTest
Feature number 	   : 15
Purpose       	   : Stress Test Semaphore
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vSemStressTest( tVoid )   						
{
	/*Declarations*/
	tU8 u8Index 				  = 0;
	tU8 u8Offset                  = 0;
	tU8 u8PostThreadCounter       = 0;
	tU8 u8WaitThreadCounter       = 0;
	tU8 u8CloseThreadCounter      = 0;
	tU8 u8CreateThreadCounter     = 0;

	/*Open the Base Semaphore*/
	OSAL_s32SemaphoreOpen( "BASE_SEMAPHORE",&baseSemHandleIPC );

	/*Open Shared Memory to collect the Semaphore names*/
	shHandleIPC = OSAL_SharedMemoryOpen( "SH_OSALRESOURCE_IPC",OSAL_EN_READWRITE );

	/*Map to the base address of osal resource shared memory*/
	shptr_osalrsc = OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

	/*Fill the semaphore names into a global array*/
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Calculate the offset from base*/
		u8Offset = (tU8)(u8Index*3);

		sem_StressListIPC[u8Index].hSemName[0] = *( shptr_osalrsc + u8Offset ); 		
		sem_StressListIPC[u8Index].hSemName[1] = *( shptr_osalrsc + ( u8Offset+1 ) );
		sem_StressListIPC[u8Index].hSemName[2] = '\0';

		/*Open the test semaphores*/
		OSAL_s32SemaphoreOpen( 
							   sem_StressListIPC[u8Index].hSemName,
							   &sem_StressListIPC[u8Index].hSem
							 );

		/*Increment Index*/
		u8Index++;
	}

	/*Post on the IPC Semaphore*/
	OSAL_s32EventPost( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_OR 
					 );

	/*Call the Portal function to Post Sem*/
	vEntryFunction( Post_Thread_Sem,"Post_Thread_Sem_CP",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Call the Portal function to Wait Sem*/
	vEntryFunction( Wait_Thread_Sem,"Wait_Thread_Sem_CP",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Call the Portal function to Delete Sem*/
	vEntryFunction( Close_Thread_Sem,"Close_Thread_Sem_CP",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Call the Portal function to Create Sem*/
	vEntryFunction( Create_Thread_Sem,"Create_Thread_Sem_CP",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

	/*Wait infinitely*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Reset the Global Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment Counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment Counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment Counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment Counter*/
		u8CreateThreadCounter++;
	}

	/*Close the 25 Semaphores*/
	u8Index = 0;
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Attempt to close the Semaphore*/
		OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );
		u8Index++;
	}


	/*Close the Base Semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );

	/*Close the Shared Memory holding names*/
	OSAL_s32SharedMemoryClose( shHandleIPC );

	/*Return to main*/
	return;
} 

/*********************************************************************************
Function name 	   : vEventStressTest
Feature number 	   : 16
Purpose       	   : Stress Test Event
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vEventStressTest( tVoid )   						
{
	/*Declarations*/
	tU8 u8Index 				  = 0;
	tU8 u8Offset                  = 0;
	tU8 u8PostThreadCounter       = 0;
	tU8 u8WaitThreadCounter       = 0;
	tU8 u8CloseThreadCounter      = 0;
	tU8 u8CreateThreadCounter     = 0;

	/*Open the Base Semaphore*/
	OSAL_s32SemaphoreOpen( "BASE_SEMAPHORE",&baseSemHandleIPC );

	/*Open Shared Memory to collect the Event names*/
	shHandleIPC = OSAL_SharedMemoryOpen( "SH_OSALRESOURCE_IPC",OSAL_EN_READWRITE );

	/*Map to the base address of osal resource shared memory*/
	shptr_osalrsc = OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

	/*Fill the Event names into a global array*/
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Calculate the offset from base*/
		u8Offset =(tU8)(u8Index*3);

		event_StressListIPC[u8Index].hEventName[0] = *( shptr_osalrsc + u8Offset ); 		
		event_StressListIPC[u8Index].hEventName[1] = *( shptr_osalrsc + ( u8Offset+1 ) );
		event_StressListIPC[u8Index].hEventName[2] = '\0';

		/*Open the test events*/
		OSAL_s32EventOpen( 
							 event_StressListIPC[u8Index].hEventName,
							 &event_StressListIPC[u8Index].hEve
						  );

		/*Increment Index*/
		u8Index++;
	}

	/*Post on the IPC Semaphore*/
	OSAL_s32EventPost( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_OR 
					 );

	/*Call the Portal function to Post Event*/
	vEntryFunction( Post_Thread_Eve,"Post_Thread_Eve_CP",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Call the Portal function to Wait Event*/
	vEntryFunction( Wait_Thread_Eve,"Wait_Thread_Eve_CP",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Call the Portal function to Delete Event*/
	vEntryFunction( Close_Thread_Eve,"Close_Thread_Eve_CP",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Call the Portal function to Create Event*/
	vEntryFunction( Create_Thread_Eve,"Create_Thread_Eve_CP",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

	/*Wait infinitely*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Reset the Global Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment Counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment Counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment Counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment Counter*/
		u8CreateThreadCounter++;
	}

	/*Close the 25 Events*/
	u8Index = 0;
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Attempt to close the Event*/
		OSAL_s32EventClose( event_StressListIPC[u8Index].hEve );
		u8Index++;
	}


	/*Close the Base Semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );

	/*Close the Shared Memory holding names*/
	OSAL_s32SharedMemoryClose( shHandleIPC );

	/*Return to main*/
	return;
}  
 
/*********************************************************************************
Function name 	   : vMessageQueueStressTest
Feature number 	   : 17
Purpose       	   : Stress Test Message Queue
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vMessageQueueStressTest( tVoid )   						
{
	/*Declarations*/
	tU8 u8Index 				  = 0;
	tU8 u8Offset                  = 0;
	tU8 u8PostThreadCounter       = 0;
	tU8 u8WaitThreadCounter       = 0;
	tU8 u8CloseThreadCounter      = 0;
	tU8 u8CreateThreadCounter     = 0;

	/*Open the Base Semaphore*/
	OSAL_s32SemaphoreOpen( "BASE_SEMAPHORE",&baseSemHandleIPC );

	/*Open Shared Memory to collect the Message Queue names*/
	shHandleIPC = OSAL_SharedMemoryOpen( "SH_OSALRESOURCE_IPC",OSAL_EN_READWRITE );

	/*Map to the base address of osal resource shared memory*/
	shptr_osalrsc = OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

	/*Fill the Message queue names into a global array*/
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Calculate the offset from base*/
		u8Offset = (tU8)(u8Index*3);

		mesgque_StressListIPC[u8Index].coszName[0] = *( shptr_osalrsc + u8Offset ); 		
		mesgque_StressListIPC[u8Index].coszName[1] = *( shptr_osalrsc + ( u8Offset+1 ) );
		mesgque_StressListIPC[u8Index].coszName[2] = '\0';

		/*Open the message queue*/
		OSAL_s32MessageQueueOpen( 
							 		mesgque_StressListIPC[u8Index].coszName,
									OSAL_EN_READWRITE,
							 		&mesgque_StressListIPC[u8Index].phMQ
						  		);

		/*Increment Index*/
		u8Index++;
	}

	/*Post on the IPC Semaphore*/
	OSAL_s32EventPost( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_OR 
					 );

	/*Call the Portal function to Post*/
	vEntryFunction( Post_Thread_Msg,"Post_Thread_Msg_CP",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Call the Portal function to Wait*/
	vEntryFunction( Wait_Thread_Msg,"Wait_Thread_Msg_CP",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Call the Portal function to Delete*/
	vEntryFunction( Close_Thread_Msg,"Close_Thread_Msg_CP",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Call the Portal function to Create*/
	vEntryFunction( Create_Thread_Msg,"Create_Thread_Msg_CP",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

	/*Wait infinitely*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Reset the Global Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment Counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment Counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment Counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment Counter*/
		u8CreateThreadCounter++;
	}

	/*Close the 25 Message Queues*/
	u8Index = 0;
	while( u8Index < MAX_OSAL_RESOURCES )
	{
		/*Attempt to close the Semaphore*/
		OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ);
		u8Index++;
	}


	/*Close the Base Semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );

	/*Close the Shared Memory holding names*/
	OSAL_s32SharedMemoryClose( shHandleIPC );

	/*Return to main*/
	return;
} 

/*********************************************************************************
Function name 	   : vCustomFunction
Feature number 	   : XX
Purpose       	   : Customizable function
Created By         : Tinoy Mathews( RBIN/ECM1 ),5- 02- 2008
*********************************************************************************/
tVoid vCustomFunction( tVoid )
{
	/*TODO*/

	/*Return*/
	return;
}

