/******************************************************************************
 *FILE         : processMain.h
 *
 *
 *SW-COMPONENT : Test Process
 *
 *
 *DESCRIPTION  : Contains the body of a Custom Test process to aid
 *               in osal IPC feature test
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2008, Robert Bosch India Limited
 *
 *HISTORY      : 
					versiopn 1.4
					rav8kor - warnings removal
					versiopn 1.3
					rav8kor - warnings removal
 				  Version 1.2
 *				 Tinoy Mathews( RBEI/ECM1 )
 *				 Added function prototype for
 *				 "vCommonInterface"
 *
 *				 Version 1.1
 *				 Tinoy Mathews( RBIN/ECM1 )
 *				 Added updates related to 
 *				 oedt_osalcore_IPC_TestFuncs.c( ver 1.1 )
 *
 *				 Version 1.0
 *				 Initial version - Tinoy Mathews( RBIN/EDI3 )	
 *
 *****************************************************************************/


/*Defines*/
#define SH_OSALRESOURCE_SIZE       76
#define NO_OF_POST_THREADS         10
#define NO_OF_WAIT_THREADS         15
#define NO_OF_CLOSE_THREADS        1
#define NO_OF_CREATE_THREADS       2
#define SRAND_MAX                  (tU32)65535
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                   0x7FFFFFFF

#define MAX_LENGTH                 3
#define MAX_OSAL_RESOURCES             (tU8)25
#define THREAD_NAME_LEN            256
#define TOT_THREADS                (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)
#define FIVE_SECONDS               5000
#define MAX_MSG_LENGTH			   20
#define MAX_MSGS                   20
#define MAX_LEN                    20


#define MAX_PATTERNS             (tU32)32
#define PATTERN_1              	 0x00000001	/*1*/
#define PATTERN_2              	 0x00000002	/*2*/
#define PATTERN_4             	 0x00000004	/*3*/
#define PATTERN_8            	 0x00000008	/*4*/
#define PATTERN_16          	 0x00000010	/*5*/
#define	PATTERN_32        		 0x00000020	/*6*/
#define PATTERN_64        		 0x00000040	/*7*/
#define PATTERN_128   			 0x00000080	/*8*/
#define PATTERN_256   			 0x00000100	/*9*/
#define PATTERN_512   			 0x00000200	/*10*/
#define PATTERN_1024   			 0x00000400	/*11*/
#define PATTERN_2048   			 0x00000800	/*12*/
#define PATTERN_4096   			 0x00001000	/*13*/
#define PATTERN_8192   			 0x00002000	/*14*/
#define PATTERN_16384   	     0x00004000	/*15*/
#define PATTERN_32768   		 0x00008000	/*16*/
#define PATTERN_65536   		 0x00010000	/*17*/
#define PATTERN_131072			 0x00020000 /*18*/
#define PATTERN_262144			 0x00040000 /*19*/
#define PATTERN_524288			 0x00080000 /*20*/
#define PATTERN_1048576			 0x00100000	/*21*/
#define PATTERN_2097152          0x00200000	/*22*/
#define PATTERN_4194304          0x00400000	/*23*/
#define PATTERN_8388608          0x00800000	/*24*/
#define PATTERN_16777216		 0x01000000	/*25*/
#define PATTERN_33554432         0x02000000	/*26*/
#define PATTERN_67108864		 0x04000000	/*27*/
#define PATTERN_134217728        0x08000000	/*28*/
#define PATTERN_268435456        0x10000000	/*29*/
#define PATTERN_536870912        0x20000000	/*30*/
#define PATTERN_1073741824		 0x40000000	/*31*/
#define PATTERN_2147483648       0x80000000	/*32*/

#define MQ_PRIO0 					0
#define MQ_PRIO1                    1
#define MQ_PRIO2 					2
#define MQ_PRIO3                    3
#define MQ_PRIO4 					4
#define MQ_PRIO5                    5
#define MQ_PRIO6 					6
#define MQ_PRIO7 					7
#define INVAL_PRIO                  20 /*>>7*/
#define SERVER_VAL               	0xAA
#define CLIENT_VAL               	0xBB

#define CLIENT_TO_SERVER       		0x00000001
#define SERVER_TO_CLIENT       		0x00000002
#define SERVER_TO_CLIENT_INIT  		0x00000004

#define FUNCTION_PTR_ARRAY_SIZE     19

#define VALID_STACK_SIZE     		2048/*2KB, minimum is 1KB*/



/*Data structures*/
typedef tVoid (*FPTR)(tVoid);

typedef struct
{
	tU8 * sh_ptr_CB;/*Pointer to data holding the function number*/
	tU8 * sh_ptr_EB;/*Pointer to error code for errors in the function body*/
}SH_DATA_PTR;

typedef struct SemaphoreTableEntryIPC_
{
	OSAL_tSemHandle hSem;
	tChar hSemName[MAX_LENGTH];
}SemaphoreTableEntryIPC;

typedef struct EventTableEntryIPC_
{
	OSAL_tEventHandle hEve;
	tChar hEventName[MAX_LENGTH];
}EventTableEntryIPC;

typedef struct MessageQueueTableEntryIPC_
{
	tChar coszName[MAX_LENGTH];
	tU32  u32MaxMessages;
	tU32  u32MaxLength;
	OSAL_tenAccess enAccess;
	OSAL_tMQueueHandle phMQ;
}MessageQueueTableEntryIPC;

/*Function Declarations*/
tVoid vCommonInterface( tVoid * ptr );

tVoid vUpdateSHErrorFieldDiffProcessCtxt( tVoid );
tVoid vWaitOnSemaphore( tVoid );
tVoid vPostOnSemaphoreMoreThanMax( tVoid );
tVoid vDelSemaphore( tVoid );
tVoid vDelSemOpenCounter( tVoid );
tVoid vCreateSemSameName( tVoid );
tVoid vCreateEventSameName( tVoid );
tVoid vPostEventPattern( tVoid );
tVoid vDeleteEvent( tVoid );
tVoid vMQSameName( tVoid );
tVoid vMQDelete( tVoid );
tVoid vMQPost( tVoid );
tVoid vMQPostBeyonMax( tVoid );
tVoid vMQMapSharedMemoryBeyondMax( tVoid );
tVoid vMSGPLCreateMSG( tVoid );

tVoid vSemStressTest( tVoid );
tVoid vEventStressTest( tVoid );
tVoid vMessageQueueStressTest( tVoid );

tVoid vCustomFunction( tVoid );






