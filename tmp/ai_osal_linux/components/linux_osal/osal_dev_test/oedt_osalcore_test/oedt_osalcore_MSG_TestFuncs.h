/************************************************************************
 *FILE          :oedt_osalcore_MSG_TestFuncs.h
 *
 *SW-COMPONENT  :OEDT_FrmWrk 
 *
 *DESCRIPTION   :This file contains function prototypes and some Macros that 
 *				 will be used in the file oedt_osalcore_MSG_TestFuncs.c 
 *				 for OSAL_CORE APIs.
 *
 *AUTHOR        :Anoop Chandran (RBIN/EDI3) 
 *
 *COPYRIGHT     :(c) 1996 - 2000 Blaupunkt Werke GmbH
 *
 *HISTORY       :
 *              ver1.5   - 22-04-2009
                lint info removal
				sak9kor


  *				ver1.4	 - 14-04-2009
                warnings removal
				rav8kor
 				 
 				 31.01.2008 Rev. 1.3 Tinoy Mathews( RBIN/ECM1 )
				 Added new test cases' prototypes
 				 TU_OEDT_MSGPL_018 <->TU_OEDT_MSGPL_029
					
 				 13.12.2007 Rev. 1.2 Tinoy Mathews (RBIN/ECM1)
				 Linted the code
 				
 				 3.12.2007 	Rev. 1.1 Tinoy Mathews (RBIN/EDI3)
 				 Added define "EXCEPTION"

 				 26.10.2007 Rev. 1.0 Anoop Chandran (RBIN/EDI3) 
 *         		 Initial Revision.
 ************************************************************************/
   

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_MEGPL_TESTFUNCS_HEADER
#define OEDT_MSGPL_TESTFUNCS_HEADER

/* Macro definition used in the code */

#define MSGPL_POOLSIZE_FIVE_TH 5000
#define MSGPL_POOLSIZE_SIX_TH 6000

#define MSGPL_MSG_SIZE 1000
#define MSGPL_INVAL_LOCATION 1000 // According to osmsg.h

#define MSGPL_FLAG_UP 1
#define MSGPL_MAX 20
#define MSGPL_MIN 0
#undef  EXCEPTION
#define EXCEPTION 0


/* Function prototypes of functions used 
				in file oedt_Illumination _TestFuns.c */
/*Test cases*/

tU32 u32MSGPLCreateSizeNULL( tVoid );                   //TU_OEDT_MSGPL_001
tU32 u32MSGPLCreateDiffSize( tVoid );                   //TU_OEDT_MSGPL_002
tU32 u32MSGPLCreateWithoutDelete( tVoid ); 				//TU_OEDT_MSGPL_003
tU32 u32MSGPLOpen( tVoid );								//TU_OEDT_MSGPL_004
tU32 u32MSGPLOpenWithoutCreate( tVoid );  				//TU_OEDT_MSGPL_005
tU32 u32MSGPLDeleteAfterDelete( tVoid );				//TU_OEDT_MSGPL_006
tU32 u32MSGPLCreateMessageNULL( tVoid );				//TU_OEDT_MSGPL_007
tU32 u32MSGPLCreateSizeZero( tVoid ); 					//TU_OEDT_MSGPL_008
tU32 u32MSGPLCreateLocationInVal(tVoid);				//TU_OEDT_MSGPL_009
tU32 u32MSGPLGetSMSize(tVoid);							//TU_OEDT_MSGPL_010
tU32 u32MSGPLCreateSMMaxMessages( tVoid );				//TU_OEDT_MSGPL_011
tU32 u32MSGPLGetSMContent( tVoid );						//TU_OEDT_MSGPL_012
tU32 u32MSGPLGetSMContentDiffMode( tVoid );				//TU_OEDT_MSGPL_013
tU32 u32MSGPLGetLMSize(tVoid);							//TU_OEDT_MSGPL_014
tU32 u32MSGPLGetLMContentDiffMode( tVoid );				//TU_OEDT_MSGPL_015
tU32 u32MSGPLDeleteMessageNULL( tVoid );				//TU_OEDT_MSGPL_016
tU32 u32MSGPLDeleteWithoutClose( tVoid ); 				//TU_OEDT_MSGPL_017

tU32 u32MSGPLMsgCreateWithNoMSGPLInSH( tVoid );			//TU_OEDT_MSGPL_018
tU32 u32MSGPLMsgCreateWithNoMSGPLInLM( tVoid );
tU32 u32MSGPLCheckMSGPL( tVoid );
tU32 u32MSGPLCheckMSGPLNoMSGPL( tVoid );
tU32 u32MSGPLGetMessageSizeMsgInLM( tVoid );
tU32 u32MSGPLGetMessageSizeMsgInSM( tVoid );
tU32 u32MSGPLGetMessageSizeNoMsg( tVoid );
tU32 u32MSGPLGetMinimalSize( tVoid );
tU32 u32MSGPLGetMinimalSizeCreatMSGLM( tVoid );
tU32 u32MSGPLGetMinimalSizeNoMSGPL( tVoid );
tU32 u32MSGPLGetMessageMaxSizeInvalid( tVoid );
tU32 u32MSGPLGetMessageMaxSizeValid( tVoid );			//TU_OEDT_MSGPL_029


tU32 u32MSGPLMessagePoolOpenStatus(tVoid);



/*Test cases*/

#endif
