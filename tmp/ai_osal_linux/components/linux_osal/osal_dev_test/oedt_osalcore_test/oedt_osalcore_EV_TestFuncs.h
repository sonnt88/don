/******************************************************************************
 *FILE         : oedt_osalcore_EV_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the EVENT OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 
  *				ver1.5	 - 14-04-2009
                warnings removal
				rav8kor
   
  				Version 1.4 , 29- 10- 2008
 *		  		updated macro  OEDT_FFS1_EVENTTEXT
 *				 Anoop Chandran( RBIN/ECM1 )
 *				 Version 1.3 
 *				 Added new testcase TU_OEDT_OSAL_CORE_EVT_030 
 *				 Anoop Chandran (RBEI\ECM1)
 *
 *                                Version 1.2
 *				 Added cases to test for XOR and REPLACE Flags for
 *				 OSAL Event Post API,Added cases to test for
 *				 No Blocking Wait for the OSAL Event Wait API.
 *				 Tinoy Mathews, 7- 01- 2008
 *
 *				 Version 1.1 
 *				 Added Stress Test Defines
 *				 Tinoy Mathews, 13- 12- 2007
 *
 *               Version 1.2
 *               Removed lint info
 *               Sainath Kalpuri, 22-04-2009
 *
 *****************************************************************************/
#ifndef OEDT_OSAL_CORE_EV_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_EV_TESTFUNCS_HEADER

/*Defines*/
#define EVENT_NAME 			     "EventTest"
#define EVENT_NAME_GLOBAL        "GlobalEvent"
#define EVENT_NAME_MAX 			 "Event named to the max length31"
#define EVENT_NAME_EXCEEDS_MAX   "Event named to the length greater than max"
#define MUTEX                    "Mutex"
#define MAX_32BIT                0xFFFFFFFF

#undef  INIT_VAL

#define INIT_VAL                 0x00000000
#define INITIAL_EVENT_FIELD      0x00000000
#define EVENT_MASK_PATTERN       0x00000001
#define THREAD_POST_PATTERN      0x00000001 
#define THREAD_IN_USE            0x00101010
#undef  ONE_SECOND 
#define ONE_SECOND               1000
#undef  TWO_SECONDS    
#define TWO_SECONDS              2000
#undef  THREE_SECONDS
#define THREE_SECONDS            3000
#undef  FIVE_SECONDS
#define FIVE_SECONDS             5000
#define TWENTY_MILLISECONDS      20
#undef  THIRTY_MILLISECONDS
#define THIRTY_MILLISECONDS      30
#define OEDT_FFS1_EVENTTEXT				 OSAL_C_STRING_DEVICE_FFS1"/eventtxt.txt"
#define THREAD_ONE_EVENT         0x00000001
#define THREAD_TWO_EVENT         0x00000002
#define THREAD_THREE_EVENT       0x00000004
#define THREAD_FINAL_EVENT       0x00000008
#define TERMINATE_THREADS	     0x00000010

#define MAX_PATTERNS             (tU32)32
#define PATTERN_1              	 0x00000001	/*1*/
#define PATTERN_2              	 0x00000002	/*2*/
#define PATTERN_4             	 0x00000004	/*3*/
#define PATTERN_8            	 0x00000008	/*4*/
#define PATTERN_16          	 0x00000010	/*5*/
#define	PATTERN_32        		 0x00000020	/*6*/
#define PATTERN_64        		 0x00000040	/*7*/
#define PATTERN_128   			 0x00000080	/*8*/
#define PATTERN_256   			 0x00000100	/*9*/
#define PATTERN_512   			 0x00000200	/*10*/
#define PATTERN_1024   			 0x00000400	/*11*/
#define PATTERN_2048   			 0x00000800	/*12*/
#define PATTERN_4096   			 0x00001000	/*13*/
#define PATTERN_8192   			 0x00002000	/*14*/
#define PATTERN_16384   	     0x00004000	/*15*/
#define PATTERN_32768   		 0x00008000	/*16*/
#define PATTERN_65536   		 0x00010000	/*17*/
#define PATTERN_131072			 0x00020000 /*18*/
#define PATTERN_262144			 0x00040000 /*19*/
#define PATTERN_524288			 0x00080000 /*20*/
#define PATTERN_1048576			 0x00100000	/*21*/
#define PATTERN_2097152          0x00200000	/*22*/
#define PATTERN_4194304          0x00400000	/*23*/
#define PATTERN_8388608          0x00800000	/*24*/
#define PATTERN_16777216		 0x01000000	/*25*/
#define PATTERN_33554432         0x02000000	/*26*/
#define PATTERN_67108864		 0x04000000	/*27*/
#define PATTERN_134217728        0x08000000	/*28*/
#define PATTERN_268435456        0x10000000	/*29*/
#define PATTERN_536870912        0x20000000	/*30*/
#define PATTERN_1073741824		 0x40000000	/*31*/
#define PATTERN_2147483648       0x80000000	/*32*/
#define REPLACE_PATTERN          0x55555555

#undef  SRAND_MAX
#define SRAND_MAX                (tU32)65535
/*rav8kor - commented as macro available in osansi.c*/
//#define RAND_MAX                 0x7FFFFFFF
#define MAX_EVENTS               30
#undef  MAX_LENGTH
#define MAX_LENGTH               256
#undef  DEBUG_MODE 
/*rav8kor - disable debug mode*/
#define DEBUG_MODE               0

#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS       10
#undef  NO_OF_WAIT_THREADS
#define NO_OF_WAIT_THREADS       15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS      1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS     2
#undef  TOT_THREADS
#define TOT_THREADS              (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)

/*Data Types*/
typedef struct EventTableEntry_
{
	OSAL_tEventHandle hEve;
	tChar hEventName[MAX_LENGTH];
}EventTableEntry;

/*Declarations*/
tU32  u32CreateEventNameNULL( tVoid );
tU32  u32CreateEventHandleNULL( tVoid );
tU32  u32CreateEventNameMAX( tVoid );
tU32  u32CreateEventNameExceedsMAX( tVoid );	
tU32  u32CreateTwoEventsSameName( tVoid );
tU32  u32CreateEventRegularName( tVoid );
tU32  u32DelEventNULL( tVoid );
tU32  u32DelEventNonExisting( tVoid );
tU32  u32DelEventNameExceedsMAX( tVoid );
tU32  u32DelEventInUse( tVoid );
tU32  u32OpenEventExisting( tVoid );	
tU32  u32OpenEventNameNULL( tVoid );
tU32  u32OpenEventHandleNULL( tVoid );
tU32  u32OpenEventNonExisting( tVoid );
tU32  u32OpenEventNameExceedsMAX( tVoid );
tU32  u32CloseEventHandleNULL( tVoid );
tU32  u32CloseEventHandleInval( tVoid );
tU32  u32GetEventStatusHandleValid( tVoid );
tU32  u32GetEventStatusHandleNULL( tVoid );
tU32  u32GetEventStatusHandleInval( tVoid );
tU32  u32PostEventHandleNULL( tVoid );
tU32  u32PostEventHandleInval( tVoid );
tU32  u32WaitAndPostEventVal( tVoid );
tU32  u32WaitEventHandleNULL( tVoid );
tU32  u32WaitEventHandleInval( tVoid );
tU32  u32WaitEventWithoutTimeout( tVoid );
tU32  u32WaitEventWithTimeout( tVoid );
tU32  u32SyncEventMultipleThreads( tVoid );

tU32  u32XORFlagPostEvent( tVoid );
tU32  u32ReplaceFlagPostEvent( tVoid );
tU32  u32ORFlagWaitEvent( tVoid );
tU32  u32NoBlockingEventWait( tVoid );
tU32  u32BlockingDurationEventWait( tVoid );
tU32  u32WaitPostEventAfterDelete( tVoid );

tU32  u32EventStressTest( tVoid );
tU32 u32EventDeletewithoutclose( tVoid );
#endif


