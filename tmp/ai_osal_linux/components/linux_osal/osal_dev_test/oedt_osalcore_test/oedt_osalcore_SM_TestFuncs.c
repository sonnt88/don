/******************************************************************************
 *FILE         : oedt_osalcore_SM_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the SEMAPHORE OSAL API.
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 
 *              ver1.8   - 30-04-2009
                warnings removal
				rav8kor

 				ver1.7   - 22-04-2009
                lint info removal
				sak9kor
 
 *				ver1.6	 - 14-04-2009
                warnings removal
				rav8kor

 				Version 1.5 , 29- 10- 2008

 *				 update: TU_OEDT_OSAL_CORE_SEM_015
 *				 Anoop Chandran(RBIN/ECM1) 
 *
 *				Version 1.4 
 *				Event clear update by  
 *				Anoop Chandran (RBEI\ECM1) 27/09/2008
 * 
 *				 Version 1.3 , 8- 1- 2008
 *				 Added new cases 
 *				 u32NoBlockingSemWait,
 *				 u32BlockingDurationSemWait,
 *				 u32WaitPostSemAfterDel
 *				 Tinoy Mathews( RBIN/ECM1)  
 *
 *				 Version 1.2 , 1- 1- 2008
 *				 Changed the expected error code for
 *				 case TU_OEDT_OSAL_CORE_SEM_011 from
 *				 Does not exist to invalid value.
 *			     Changed the error code for
 *				 TU_OEDT_OSAL_CORE_SEM_020 from
 *				 Unknown to Queuefull
 *				 Tinoy Mathews( RBIN/ECM1) 
 *
 *				 Version 1.1 , 11- 12- 2007
 *				 Added case 022( Stress Test Semaphore ),
 *				 Linted the code,updated case 009
 *				 Tinoy Mathews( RBIN/ECM1) 
 *
 *				 Version 1.0 , 26- 10- 2007
 *				 
 *				 
 *					
 *
 *****************************************************************************/
		 

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"

#include "oedt_osalcore_SM_TestFuncs.h"
#include "oedt_helper_funcs.h"




/******************************************************************
|Global variables
|----------------------------------------------------------------*/
OSAL_tSemHandle globalsemHandle     = 0;
OSAL_tSemHandle globalsemHandle_    = 0;
OSAL_tSemHandle baseSemHandle                     = 0;
tU8  u8Byte                         = INIT_VAL;
tU32 u32ThreadReturnCode            = 0;
tU32 u32CFRC_1                      = 0;
tU32 u32CFRC_2                      = 0;
tU32 u32CFRC_3                      = 0;

SemaphoreTableEntry sem_StressList[MAX_SEMAPHORES];
OSAL_trThreadAttribute post_ThreadAttr[NO_OF_POST_THREADS];
OSAL_trThreadAttribute wait_ThreadAttr[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute close_ThreadAttr[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute create_ThreadAttr[NO_OF_CREATE_THREADS];
OSAL_tThreadID post_ThreadID[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID wait_ThreadID[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID close_ThreadID[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID create_ThreadID[NO_OF_CREATE_THREADS]           = {OSAL_NULL};
tU32 u32GlobCounter                                            = 0;


/*****************************************************************
| Helper Functions and Macros  (scope: module-local)
|----------------------------------------------------------------*/

/*ThreadWaitPost*/
tVoid ThreadSemTest(const tVoid *pvArg )
{
	/*Declarations*/
	tChar ThreadBuffer[10];
	tU8 u8Count          = 100;

	/*Initialize thread return code and Thread status*/
	u32ThreadReturnCode  = 0;
	
	/*Initialize ThreadBuffer*/
	OSAL_pvMemorySet( ThreadBuffer,'\0',10 );
	(tVoid)OSAL_szStringCopy( ThreadBuffer,"Threaddat" );

	/*Write into Ramdisk*/
	while( u8Count-- )
	{
		if( OSAL_ERROR == OSAL_s32IOWrite(
										 	*((OSAL_tIODescriptor*)pvArg),
										 	(tPCS8)ThreadBuffer,
										 	10
									  	  ) )
		{
			/*Write to device failed*/
			u32ThreadReturnCode += 2;
		}
	}
	
	if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle ) )
	{
		u32ThreadReturnCode = 3;
	}
	
    /*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*Thread 1*/
tVoid ControlFlow_1( tVoid *pvArg )
{
	/*Initialize the return value*/
	u32CFRC_1 = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	/*Wait on semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER ) )
	{
		u32CFRC_1 = 1;
	}
	else
	{
		/*Wait for 2 seconds*/
		OSAL_s32ThreadWait( TWO_SECONDS );

		/*Wait on Mutex*/
		if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER ) )
		{
			u32CFRC_1 += 100;
		}
		else
		{
			/*Critical section*/
			u8Byte |= CF1_MASK;
			/*Critical section*/

			/*Post on Mutex*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle_ ) )
			{
				u32CFRC_1 += 200;
			}
		}

		/*Post on semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle ) )
		{
			u32CFRC_1 =2;
		}
	}

	/*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*Thread 2*/
tVoid ControlFlow_2( tVoid *pvArg )
{
	/*Initialize return value*/
	u32CFRC_2 = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

	/*Wait on semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER ) )
	{
		u32CFRC_2 = 1;
	}
	else
	{
		/*Wait for 2 seconds*/
		OSAL_s32ThreadWait( TWO_SECONDS );

		/*Wait on Mutex*/
		if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER ) )
		{
			u32CFRC_2 += 100;
		}
		else
		{
			/*Critical section*/
			u8Byte |= CF2_MASK;
			/*Critical section*/

			/*Post on Mutex*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle_ ) )
			{
				u32CFRC_2 += 200;
			}
		}

		/*Post on semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle ) )
		{
			u32CFRC_2 = 2;
		}
	}

	/*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*Thread 3*/
tVoid ControlFlow_3( tVoid *pvArg )
{

	/*Initialize return value*/
	u32CFRC_3 = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	/*Wait on Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER ) )
	{
		u32CFRC_3 = 1;
	}
	else
	{
		/*Wait for 2 seconds*/
		OSAL_s32ThreadWait( TWO_SECONDS );

		/*Wait on Mutex*/
		if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER ) )
		{
			u32CFRC_3 += 100;
		}
		else
		{
			/*Critical section*/
			u8Byte |= CF3_MASK;
			/*Critical section*/

			/*Post on Mutex*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle_ ) )
			{
				u32CFRC_3 += 200;
			}
		}

		/*Post on semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphorePost( globalsemHandle ) )
		{
			u32CFRC_3 = 2;
		}
	}

	/*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( FIVE_SECONDS );
}


/*****************************************************************************
* FUNCTION    :	   u32CreateCloseDelSemVal()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_001

* DESCRIPTION :  
				   1). Create Semaphore
			       2). Close semaphore if Create semaphore successful
			       3). Delete semaphore if close semaphore successful
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateCloseDelSemVal( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateSemNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_002

* DESCRIPTION :  
				   1). Create Semaphore with name as OSAL_NULL
				   2). If semaphore creation successful,set return error
				       and shutdown semaphore(Close , delete)
				   3). If semaphore creation fails,track the error code.
				       Only if the return error code is OSAL_E_INVALIDVALUE
					   can it be asserted that the API works correctly.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateSemNameNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		      = 0;
	OSAL_tSemHandle	semHandle = 0;

	/*Create the semaphore with semaphore name set to NULL*/
	if( OSAL_ERROR != OSAL_s32SemaphoreCreate( 
	                             			 	OSAL_NULL, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Created semaphore*/
		u32Ret = 1;

		/*Close the created semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			u32Ret += 10;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if(	OSAL_ERROR == OSAL_s32SemaphoreDelete( OSAL_NULL ) )
			{
				u32Ret = 100;
			}
		}
	}
	else
	{
		if(	OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 200;
		}
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateSemHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_003

* DESCRIPTION :  
				   1). Create Semaphore with handle as OSAL_NULL
				   2). If semaphore creation successful,set return error
				       and shutdown semaphore(Close , delete)
				   3). If semaphore creation fails,track the error code.
				       Only if the return error code is OSAL_E_INVALIDVALUE
					   can it be asserted that the API works correctly.
				   4). Return the error code
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateSemHandleNULL( tVoid )	
{
	/*Declarations*/
	tU32 u32Ret 		      = 0;
	//tU32 u32ErrorStorage      = OSAL_E_NOERROR;

	/*Create the semaphore with semaphore handle set to OSAL_NULL*/
	if( OSAL_ERROR != OSAL_s32SemaphoreCreate( 
	                             			 	SEMAPHORE_NAME, 
	                             				OSAL_NULL, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Created semaphore*/
		u32Ret = 1;

		/*Close the created semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( OSAL_NULL ) )
		{
			u32Ret += 10;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if(	OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				u32Ret = 100;
			}
		}
	}
	else
	{
		if(	OSAL_E_INVALIDVALUE !=  OSAL_u32ErrorCode( )  )
		{
			u32Ret += 200;
		}
	}

	/*Return error value*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateCloseDelSemNameMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_004

* DESCRIPTION :  
				   1). Create Semaphore(with name equal to 32 chars)
			       2). Close semaphore if Create semaphore successful
			       3). Delete semaphore if close semaphore successful
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateCloseDelSemNameMAX( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME_MAX, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME_MAX ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateSemWithNameExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_005

* DESCRIPTION :  
				   1). Try to create Semaphore(with name exceeding 32 chars).
				   2). If semaphore gets created, notify the return error code 
					   and:
			           Close semaphore.
			           Delete semaphore.
				   3). If semaphore	creation fails, check the returned error
				       code.Only if the error code is OSAL_E_NAMETOOLONG, can it
					   be asserted that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateSemWithNameExceedsMAX( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Try to create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME_EXCEEDS_MAX, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore - expected behaviour*/
		if( OSAL_E_NAMETOOLONG != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
		
	}
	else
	{
		/*Created semaphore within the system! - API failed!*/
		u32Ret += 2000;
		
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME_EXCEEDS_MAX ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateTwoSemSameName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_006

* DESCRIPTION :  
				   1). Create Semaphore
				   2). Create Semaphore with same name as above.
				   3). If semaphore re-creation successful, report error
				       into return value,and close and delete the semaphore.
					   If semaphore re-creation fails,query the error
					   code and check if it is same as OSAL_E_ALREADYEXISTS
					   Only if it is OSAL_E_ALREADYEXISTS,does the API 
					   behaves correctly.
			       5). Close semaphore if Create semaphore in 1 successful
			       6). Delete semaphore if close semaphore in 5 successful
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CreateTwoSemSameName( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle1    = 0;
	OSAL_tSemHandle semHandle2    = 0;

	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle1, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 500;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreCreate(
		                                           SEMAPHORE_NAME,
												   &semHandle2,
												   SEMAPHORE_START_VALUE
												 ) )
		{
			/*Expected behaviour*/
			if( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 1000;
			}
		}
		else
		{
			/*Re - created semaphore with the same name!*/
			u32Ret += 2000;

			/*Close the re - semaphore invalidating the handle*/
			if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle2 ) )
			{
				/*Close of re - created semaphore failed*/
				u32Ret += 3000;
			}
			else
			{
				/*Remove the re-created semaphore from the system*/
				if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
				{
					/*Delete of semaphore failed*/
					u32Ret += 4000;
				}
			}
		}

		/*Close the initially created semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle1 ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 6000;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 7000;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelSemWithNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_007

* DESCRIPTION :  
				   1). Try a Delete semaphore with OSAL_NULL.
				   2). If Delete passes, report to the return code of
				       the API failure.
				   3). If Delete fails, analyse the error code.Only if the 
				       error code is OSAL_E_INVALIDVALUE, can it be asserted
					   that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelSemWithNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;

	/*Call the semaphore delete API passing OSAL_NULL*/
	if( OSAL_ERROR == OSAL_s32SemaphoreDelete( OSAL_NULL ) )
	{
		/*Check for the correct return value*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 500;
		}
	}
	else
	{
		/*Semaphore delete passes with OSAL_NULL!*/
		u32Ret += 1000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelSemWithNonExistingName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_008

* DESCRIPTION :  
				   1). Create Semaphore.
			       2). Close semaphore if Create semaphore successful.
			       3). Delete semaphore if close semaphore successful.
				   4). Delete the semaphore deleted in step 3.
				   5). If Delete fails, analyse the error code, only
				       if the error code is OSAL_E_DOESNOTEXIST, can it 
				       be asserted that the API works correctly.
				   6). If Delete passes, report the return code, regarding
				       the API failure.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32DelSemWithNonExistingName( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}

		/*Attempt to delete an already deleted(non existent) semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
		{
			/*Analyse the error code*/
			if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
			{
				u32Ret += 1000;
			}
		}
		else
		{
			/*Delete passed for a non existent semaphore!!*/
			u32Ret += 2000;
		}
	}
	/*Scenario 1*/
	

	/*Scenario 2*/
	if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "WhoAmI" ) )
	{
		/*Analyse the error code*/
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 3000;
		}
	}
	else
	{
		/*Delete passed for a non existent semaphore!!*/
		u32Ret += 4000;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32DelSemCurrentlyUsed()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_009

* DESCRIPTION :  
				   1). Create Semaphore.
			       2). Try to Delete semaphore while semaphore is still in use.
				   3). If Delete passes,then the behaviour is correct,if it
				   fails, then report error.
				   4). If delete passes, close the semaphore to reclaim 
				   semaphore resources.
				   			       			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007

				   Updated by Tinoy Mathews( RBIN/ECM1 ) as per comments
				   from MRK2HI  Dec 11, 2007
*******************************************************************************/
tU32  u32DelSemCurrentlyUsed( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Remove the semaphore from the system while semaphore is yet to be 
		closed(in use)*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
		{
			/*Removal succeeded before a close!*/
			u32Ret += 2000;
		}
		else
		{
		    /*If delete passes( successful behaviour ), remove resources
		    with a close*/
		    if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		    {
				 /*Close of semaphore failed!*/
				 u32Ret += 3000;
		    }
		    else
		    {
		   		 /*On success of close try a reclose to see if semaphore is 
		   		 actually deleted from the system*/
		   		 if( OSAL_OK == OSAL_s32SemaphoreClose( semHandle ) )
				 {
					 u32Ret += 4000;
				 }
		    }
		}
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenSemWithNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_010

* DESCRIPTION :  
				   1). Create Semaphore
			       2). Close semaphore if Create semaphore successful
				   3). Delete semaphore if close successful.
				   3). Open the semaphore passing the semaphore name
				       as OSAL_NULL
				   4). If Open successful, report the return code, 
				       of the API failure.
				   5). If Open failed,query the error code, only if
				       the error code is OSAL_E_INVALIDVALUE, can
					   it be asserted that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenSemWithNameNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;
	OSAL_tSemHandle semHandle_    = 0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			

			/*Open the semaphore passing semaphore name as OSAL_NULL*/
			if( OSAL_ERROR == OSAL_s32SemaphoreOpen( OSAL_NULL,&semHandle ) )
			{
				/*Failure is an expected behaviour*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
				{
					/*Incorrect error code!*/
					u32Ret += 1000;
				}
			}
			else
			{
				/*API passed with OSAL_NULL named semaphore!*/
				u32Ret += 2000;

				/*Close the semaphore which got opened*/
				if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
				{
					u32Ret += 3000;
				}
				else
				{
					/*Remove the OSAL_NULL named semaphore*/
					OSAL_s32SemaphoreDelete( OSAL_NULL );
				}
			}
			
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Scenario 2*/
	/*Open the semaphore passing name as OSAL_NULL, on a handle
	that was never used for create/open in the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreOpen( OSAL_NULL,&semHandle_ ) )
	{
		/*Failure is an expected behaviour*/
		if(	OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Incorrect error code*/ 
			u32Ret += 4000;
		}
	}
	else
	{
		/*API passed with OSAL_NULL named semaphore!*/
		u32Ret += 5000;

		/*Close the semaphore which got opened*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle_ ) )
		{
			u32Ret += 6000;
		}
		else
		{
			/*Remove the OSAL_NULL named semaphore*/
			OSAL_s32SemaphoreDelete( OSAL_NULL );
		}
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenSemWithHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_011

* DESCRIPTION :  
				   1). Create Semaphore
			       2). Close semaphore if Create semaphore successful
				   3). Delete semaphore if close successful.
				   3). Open the semaphore passing the semaphore handle 
				       as NULL
				   4). If Open successful, report the return code, 
				       of the API failure.
				   5). If Open failed,query the error code, only if
				       the error code is OSAL_E_INVALIDVALUE, can
					   it be asserted that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenSemWithHandleNULL( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			
			/*Open the semaphore passing semaphore handle as OSAL_NULL*/
			if( OSAL_ERROR == OSAL_s32SemaphoreOpen( SEMAPHORE_NAME,OSAL_NULL ) )
			{
				/*Failure is an expected behaviour*/
				if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
				{
					/*Incorrect error code!*/
					u32Ret += 1000;
				}
			}
			else
			{
				/*API passed with OSAL_NULL semaphore handle!*/
				u32Ret += 2000;

				/*Close the semaphore which got opened*/
				if( OSAL_ERROR == OSAL_s32SemaphoreClose( OSAL_NULL ) )
				{
					u32Ret += 3000;
				}
				
			}
			
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Scenario 2*/
	/*Open the semaphore passing handle as OSAL_NULL, on a semaphore name
	that was never used for create/open in the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreOpen( "Unknown",OSAL_NULL ) )
	{
		/*Failure is an expected behaviour*/
		if(	OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Incorrect error code*/ 
			u32Ret += 4000;
		}
	}
	else
	{
		/*API passed with OSAL_NULL named semaphore!*/
		u32Ret += 5000;

		/*Close the semaphore which got opened*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( OSAL_NULL ) )
		{
			u32Ret += 6000;
		}
		else
		{
			/*Remove the OSAL_NULL named semaphore*/
			OSAL_s32SemaphoreDelete( "Unknown" );
		}
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32OpenSemWithNameExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_012

* DESCRIPTION :  
				   
				   1). Open the semaphore passing a semaphore name
				       with length greater than 32 characters.
				   2). If Open successful, report the return code, 
				       of the API failure.
				   3). If Open failed,query the error code, only if
				       the error code is OSAL_E_DOESNOTEXIST, can
					   it be asserted that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenSemWithNameExceedsMAX( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Try to open a semaphore passing semaphore name greater than MAX*/
	if( OSAL_ERROR == OSAL_s32SemaphoreOpen( SEMAPHORE_NAME_EXCEEDS_MAX,&semHandle ) )
	{
		if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code!*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Semaphore open with name exceeding max chars returned success!*/
		u32Ret += 2000;

		/*Attempt shutdown activities!!*/

		/*Close the semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			u32Ret += 3000;
		}
		else
		{
			/*Delete the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME_EXCEEDS_MAX ) )
			{
				u32Ret += 4000;
			}
		}

		/*Attempt shutdown activities!!*/
	}

	/*Return error code*/
	return u32Ret;
}
		
/*****************************************************************************
* FUNCTION    :	   u32OpenSemWithNonExistingName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_013

* DESCRIPTION :  
				   1). Create Semaphore
			       2). Close semaphore if Create semaphore successful
				   3). Delete semaphore if close successful.
				   3). Open the semaphore passing that was deleted from
				       the system.
				   4). If Open successful, report the return code, 
				       of the API failure.
				   5). If Open failed,query the error code, only if
				       the error code is OSAL_E_DOESNOTEXIST, can
					   it be asserted that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32OpenSemWithNonExistingName( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
			else
			{
				/*Open the semaphore passing semaphore with a non existent name*/
				if( OSAL_ERROR == OSAL_s32SemaphoreOpen( SEMAPHORE_NAME,&semHandle ) )
				{
					/*Failure is an expected behaviour*/
					if( OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
					{
						/*Incorrect error code!*/
						u32Ret += 1000;
					}
				}
				else
				{
					/*API passed with a deleted semaphore!*/
					u32Ret += 2000;

					/*Close the semaphore which got opened*/
					if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
					{
						u32Ret += 3000;
					}
					else
					{
						/*Remove the semaphore*/
						if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
						{
							u32Ret += 3500;
						}
					}
				}
			}
		}
	}
	/*Scenario 1*/

	/*Scenario 2*/
	/*Open the semaphore, on a semaphore name
	that was never used for create/open in the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreOpen( "NeverKnownToSystem",&semHandle ) )
	{
		/*Failure is an expected behaviour*/
		if(	OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode( ) )
		{
			/*Incorrect error code*/ 
			u32Ret += 4000;
		}
	}
	else
	{
		/*API passed with a semaphore whose name never was known to the system!*/
		u32Ret += 5000;

		/*Close the semaphore which got opened*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			u32Ret += 6000;
		}
		else
		{
			/*Remove the semaphore which was never known to the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "NeverKnownToSystem" ) )
			{
				u32Ret += 7000;
			}
		}
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}
	
/*****************************************************************************
* FUNCTION    :	   u32CloseSemWithInvalidHandle()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_014

* DESCRIPTION :  
				   1). Try a close semaphore with an invalid semaphore
				       handle.
				   2). If close passes, report to the return code of
				       the API failure.
				   3). If close fails, analyse the error code.Only if the 
				       error code is OSAL_E_INVALIDVALUE, can it be asserted
					   that the API works correctly.
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32CloseSemWithInvalidHandle( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;

	/*Call the semaphore close API passing invalid handle*/
	if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
	{
		/*Check for the correct return value*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 500;
		}
	}
	else
	{
		/*Semaphore close passes with invalid handle!*/
		u32Ret += 1000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitPostSemValid()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_015

* DESCRIPTION :  
				   1). Examine the Wait and Post functionalities of
				       a semaphore.(Using threads)
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tU32  u32WaitPostSemValid( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                       = 0;
	tU8  u8MainCount                  = 100;
	OSAL_tIODescriptor FDsemtext 	    = 0;
	OSAL_trThreadAttribute trAttr     = {OSAL_NULL};
	OSAL_tThreadID tID                = 0;
	tChar MainThreadBuffer[10];

	/*Initialize the global semaphore value and global id*/
	globalsemHandle = 0;

	/*Copy String into MainThreadBuffer*/
	OSAL_pvMemorySet( MainThreadBuffer,'\0',10 );
	(tVoid)OSAL_szStringCopy( MainThreadBuffer,"MainThdat" );

	/*Create a file in Ramdisk*/
	if( OSAL_ERROR == ( FDsemtext = OSAL_IOCreate( SEMTEXT, OSAL_EN_READWRITE ) ) )
	{
		/*File creation failed!*/
		u32Ret += 100;
	}
	else
	{
		/*Create a semaphore within the system*/
		if( OSAL_ERROR == OSAL_s32SemaphoreCreate( GLOBAL_SEMAPHORE_NAME,
	                                           &globalsemHandle,
	                                           GLOBAL_SEMAPHORE_START_VALUE ) )
		{
			u32Ret += 500;
		}
		else
		{
			/*Decrement the semaphore count*/
			if( OSAL_ERROR == OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER ) )
			{
				u32Ret += 600;
			}
			else
			{
				/*Fill in thread attributes*/
				trAttr.szName       = "SemaphoreWPTest";
				trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
				trAttr.s32StackSize	= 2048;/*2MB stack*/
				trAttr.pfEntry      = (OSAL_tpfThreadEntry)ThreadSemTest;
				trAttr.pvArg        = &FDsemtext;

				/*Create and Run a thread of control*/
				if( OSAL_ERROR == ( tID = OSAL_ThreadSpawn( &trAttr ) ) )
				{
					/*Thread create and activate failed*/
					u32Ret += 1000;
				}
				else
				{

					/*Wait for the signal from the thread*/
					if( OSAL_ERROR == OSAL_s32SemaphoreWait( 
				                                         globalsemHandle,
				                                         OSAL_C_TIMEOUT_FOREVER ) )
					{
						u32Ret += 2000;
					}

					/*Delete the thread*/
					if( OSAL_ERROR == OSAL_s32ThreadDelete( tID ) )
					{
						u32Ret += 3000;
					}

					/*Write into file in Ramdisk from main thread now*/
					while( u8MainCount-- )
					{
						/*Write into file*/
						if( OSAL_ERROR == OSAL_s32IOWrite( FDsemtext,(tPCS8)MainThreadBuffer,10 ) )
						{
							u32Ret += 2;
						}
					}
				}
			}

			/*Close the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreClose( globalsemHandle ) )
			{
				u32Ret += 4000;
			}
			else
			{
				/*Delete the semaphore*/
				if( OSAL_ERROR == OSAL_s32SemaphoreDelete( GLOBAL_SEMAPHORE_NAME ) )
				{
					u32Ret += 5000;
				}
			}
		}

		/*Close the file on Ramdisk*/
		if( OSAL_ERROR == OSAL_s32IOClose( FDsemtext ) )
		{
			u32Ret += 6000;
		}
		else
		{
			/*Remove the file from Ramdisk*/
			if( OSAL_ERROR == OSAL_s32IORemove( SEMTEXT ) )
			{
				u32Ret += 7000;
			}
		}
	}

	/*Return the error code*/
	return u32Ret+u32ThreadReturnCode;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitSemWithInvalidHandle()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_016

* DESCRIPTION :  
				   1). Wait on a semaphore with invalid handle
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitSemWithInvalidHandle( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	OSAL_tSemHandle semHandle     = 0;
	OSAL_tSemHandle semHandle_    = 0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
			else
			{
			  	/*Wait on a non existent handle(invalidated)*/
			 	if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle,TWO_SECONDS ) )
			 	{
					/*Failure is an expected behaviour*/
					if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
					{
						u32Ret += 100;
					}   
				}
				else
				{
					/*Wait API succeeds on a non existent semaphore handle! 
					- Incorrect behaviour*/
					u32Ret += 500;
				}
			}	
		}
	}
	/*Scenario 1*/

	/*Scenario 2*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle_,TWO_SECONDS ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 300;
		}
	}
	else
	{
		/*Wait API succeeds on a non existent semaphore handle! 
		- Incorrect behaviour*/
		u32Ret += 1000;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ReleaseSemWithInvalidHandle()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_017

* DESCRIPTION :  
				   1). Post on a semaphore with invalid handle
					   If the Post API passes,report the 
					   return code regarding the post API 
					   failure
					   If the Post API fails, it is the expected 
					   behaviour, analyse the error code, if the
					   error code is OSAL_E_INVALIDVALUE,then the 
					   API behaves correctly.
					  
					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32ReleaseSemWithInvalidHandle( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   =  0;
	OSAL_tSemHandle semHandle     =  0;
	OSAL_tSemHandle semHandle_    =  0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
			else
			{
				/*Post on a non existent handle(invalidated)*/
				if( OSAL_ERROR == OSAL_s32SemaphorePost( semHandle ) )
				{
					/*Failure is an expected behaviour*/
					if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
					{
						u32Ret += 100;
					}   
				}
				else
				{
					/*Posting on a non existing handle successful!
			 		- Incorrect behaviour*/
					u32Ret += 1000;
				}
			}	
		}
	}
	/*Scenario 1*/

	/*Scenario 2*/
	if( OSAL_ERROR == OSAL_s32SemaphorePost( semHandle_ ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 300;
		}
	}
	else
	{
		/*Posting on a non existent handle successful!
		- Incorrect behaviour*/
		u32Ret += 2000;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32GetRegularSemValue()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_018

* DESCRIPTION :  
				   1). Create Semaphore( WITH SEMAPHORE COUNT 1 )
				   2). Get semaphore count at Time when semaphore is 
				       created.
				   3). Get semaphore count after a wait call is issued
				       on the semaphore.
				   4). Get the semaphore count after a post call is
				       issued on the semaphore.
			       2). Close semaphore if Create semaphore successful
			       3). Delete semaphore if close semaphore successful
			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetRegularSemValue( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	tS32 s32Count                 = SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle     = 0;

	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Get the value of the semaphore count*/
		if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle,&s32Count ) )
		{
			u32Ret += 100;
		}
		else
		{
			/*Do a simple wait on semaphore - Take one resource instance*/
			if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_FOREVER ) )
			{
				u32Ret += 200;
			}
			else
			{
				/*Get the value of semaphore count now*/
				s32Count = SEMAPHORE_COUNT_INVALID;/*Invalidate the previous count value*/
				if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle,&s32Count ) )
				{
					/*Get value call failed*/
					u32Ret += 300;
				}
				else
				{
					/*Check for the semaphore count*/
					if( 0 != s32Count )
					{
						u32Ret += 400;
					}
				}
			}

			/*Do a simple post on semaphore - Return the resource instance*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( semHandle ) )
			{
				u32Ret += 500;
			}
			else
			{
				/*Get the value of semaphore count now*/
				s32Count = SEMAPHORE_COUNT_INVALID;/*Invalidate the previous count value*/
				if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle,&s32Count ) )
				{
					/*Get value call failed*/
					u32Ret += 600;
				}
				else
				{
					/*Check for the semaphore count*/
					if( SEMAPHORE_START_VALUE != s32Count )
					{
						u32Ret += 700;
					}
				}
			}
		}
		
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}	

/*****************************************************************************
* FUNCTION    :	   u32GetSemValueInvalidHandle()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_019

* DESCRIPTION :  
				   1). Create Semaphore.
			       2). Close semaphore if Create semaphore successful.
			       3). Delete semaphore if close semaphore successful.
				   4). Try to get the value of the semaphore.
				   5). If get value API returns success, update the
				       return error code the failure of the API.
				   6). If get value API fails, it is as per expected
				       behaviour,analyse the returned error code and
					   check if it is OSAL_E_INVALIDVALUE.If not, return
					   with error.
					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32GetSemValueInvalidHandle( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   =  0;
	tS32 s32Count                 =  SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle     =  0;
	OSAL_tSemHandle semHandle_    =  0;

	
	/*Scenario 1*/
	/*Create the Semaphore within the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
	                             				SEMAPHORE_NAME, 
	                             				&semHandle, 
	                             				SEMAPHORE_START_VALUE 
	                                         ) )
	{
		/*Failed to create semaphore*/
		u32Ret = 1;
	}
	else
	{
		/*Close the semaphore invalidating the handle*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Close of semaphore failed*/
			u32Ret = 2;
		}
		else
		{
			/*Remove the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Delete of semaphore failed*/
				u32Ret = 3;
			}
			else
			{
				/*Get semaphore count on a non existent handle(invalidated)*/
				if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle,&s32Count ) )
				{
					/*Failure is the correct behaviour of the API*/
					if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
					{
						u32Ret += 100;
					}   
				}
				else
				{
					/*Get value successful with a non existent semaphore!
			 		- Incorrect behaviour*/
					u32Ret += 200;
				}
			}
		}
	}
	/*Scenario 1*/

	/*Scenario 2*/
	s32Count = 	SEMAPHORE_COUNT_INVALID;/*Invalidate the count value*/
	if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle_,&s32Count ) )
	{
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 300;
		}
	}
	else
	{
		/*Get value successful with a non existent semaphore!
		 - Incorrect behaviour*/
		 u32Ret += 400;
	}
	/*Scenario 2*/

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32WaitPostSemValidExceedsMAX()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_020

* DESCRIPTION :  
				   1). Create Semaphore(with semaphore count 1).
				   2). Issue a wait on the semaphore.
				   3). Issue a post on the semaphore.
				   4). Issue a post again on the semaphore.
					   If post successful, update the return
					   code of the post API failure
					   If post fails, it is a correct behaviour,
					   analyse if the error code is OSAL_E_UNKNOWN.
					   Only if the error code is this, can it be asserted
					   that the API works correctly

			       5). Close semaphore if Create semaphore successful.
			       6). Delete semaphore if close semaphore successful.
				   
					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32  u32WaitPostSemValidExceedsMAX( tVoid )
{
	/*Declarations*/
	tU32 u32Ret               = 0;
	tS32 s32Count             = SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle = 0;
	
	/*Create a semaphore in the system*/	
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE_MAX ) )
	{
		u32Ret = 1;
	}
	else
	{
		/*Call the semaphore wait API*/
		if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_FOREVER )	)
		{
			u32Ret = 2;
		}
		else
		{
			/*Call the semaphore post API*/
			if( OSAL_ERROR == OSAL_s32SemaphorePost( semHandle ) )
			{
				u32Ret = 3;
			}
			else
			{
				/*Call the semaphore get value API*/
				if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( semHandle,&s32Count ) )
				{
					u32Ret = 4;
				}
				else
				{
					/*Check for correctness*/
					if( SEMAPHORE_START_VALUE_MAX != s32Count )
					{
						u32Ret = 500;
					}
				}

				/*Call the semaphore post API once more*/
				if( OSAL_ERROR == OSAL_s32SemaphorePost( semHandle ) )
				{
					/*Failure is the correct behaviour*/
					if( OSAL_E_QUEUEFULL != OSAL_u32ErrorCode( ) )
					{
						/*Wrong error code returned*/
						u32Ret += 1000;
					}
				}
				else
				{
					/*Post once again successful! - Wrong behaviour
					  Semaphore has a max resource count of 1!*/
					  u32Ret += 2000;
				}
			}
		}

		/*Close  the semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
			/*Semaphore close failed*/
			u32Ret += 3000;
		}
		else
		{
			/*Delete the semaphore from the system*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
				/*Semaphore delete failed*/
				u32Ret += 4000;
			}
		}
	}

	/*Return the error code*/
	return u32Ret;
}
				
/*****************************************************************************
* FUNCTION    :	   u32AccessSemMpleThreads()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_021

* DESCRIPTION :  
				   1). Create Semaphore(with semaphore count 3).
				   2). Create three threads,each of the threads
				       first waits then posts on the semaphore.
			       3). Delete the three threads
			       4). Delete semaphore and close semaphore
				   
					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tU32 u32AccessSemMpleThreads( tVoid )
{
	/*Declarations*/
	tU32 u32Ret               		= 0;
	tS32 s32Count                   = SEMAPHORE_COUNT_INVALID;
    tS32 s32loop                    = 0;
	OSAL_tThreadID tID_1      		= 0;
	OSAL_tThreadID tID_2      		= 0;
	OSAL_tThreadID tID_3      		= 0;
	OSAL_trThreadAttribute trAttr_1 = {OSAL_NULL};
	OSAL_trThreadAttribute trAttr_2 = {OSAL_NULL};
	OSAL_trThreadAttribute trAttr_3 = {OSAL_NULL};

	/*Initialize the global semaphore handle*/
	globalsemHandle  = 0;
	globalsemHandle_ = 0;
	/*Reset data*/
	u8Byte = INIT_VAL;


	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( GLOBAL_SEMAPHORE_NAME_,
	                                           &globalsemHandle_,
	                                           GLOBAL_SEMAPHORE_START_VALUE ) )
	{
		u32Ret = 2000;
	}
	else
	{
		/*Create a semaphore within the system*/
		if( OSAL_ERROR == OSAL_s32SemaphoreCreate( GLOBAL_SEMAPHORE_NAME,
		                                           &globalsemHandle,
	    	                                       GLOBAL_SEMAPHORE_START_VALUE_ ) )
		{
			u32Ret += 500;
		}
		else
		{
			/*Fill in thread 1 attributes*/
			trAttr_1.szName       = "ControlFlow_1";
			trAttr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
			trAttr_1.s32StackSize = 2048;/*2MB stack*/
			trAttr_1.pfEntry      = (OSAL_tpfThreadEntry)ControlFlow_1;
			trAttr_1.pvArg        = OSAL_NULL;

			/*Fill in thread 2 attributes*/
			trAttr_2.szName       = "ControlFlow_2";
			trAttr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
			trAttr_2.s32StackSize = 2048;/*2MB stack*/
			trAttr_2.pfEntry      = (OSAL_tpfThreadEntry)ControlFlow_2;
			trAttr_2.pvArg        = OSAL_NULL;

			/*Fill in thread 3 attributes*/
			trAttr_3.szName       = "ControlFlow_3";
			trAttr_3.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
			trAttr_3.s32StackSize = 2048;/*2MB stack*/
			trAttr_3.pfEntry      = (OSAL_tpfThreadEntry)ControlFlow_3;
			trAttr_3.pvArg        = OSAL_NULL;

			/*Create and Run a thread of control*/
			if( OSAL_ERROR == ( tID_1 = OSAL_ThreadSpawn( &trAttr_1 ) ) )
			{
				/*Thread 1 create and activate failed*/
				u32Ret += 1000;
			}
			else
			{
				if( OSAL_ERROR == ( tID_2 = OSAL_ThreadSpawn( &trAttr_2 ) ) )
				{
					/*Thread 2 create and activate failed*/
					u32Ret += 2000;
				}
				else
				{
			  		if( OSAL_ERROR == ( tID_3 = OSAL_ThreadSpawn( &trAttr_3 ) ) )
					{
						/*Thread 3 create and activate failed*/
						u32Ret += 3000;
					}
					else
					{
						/*Busy wait*/
						do
						{
					  		/*Set semaphore value*/
					  		s32Count =  SEMAPHORE_COUNT_INVALID;
					  
					  		/*Wait for threads to execute*/
					  		OSAL_s32ThreadWait( TWO_SECONDS );

					  		/*Get semaphore value*/
					  		if( OSAL_ERROR == OSAL_s32SemaphoreGetValue( globalsemHandle,&s32Count ) )
					  		{
					  			u32Ret += 250;
					  		}
                            
                            s32loop++;
							
						}while( GLOBAL_SEMAPHORE_START_VALUE_ != s32Count && s32loop < 10);					
					
						/*Delete the Thread 3*/
						if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_3 ) )
						{
							u32Ret += 4000;
						}
					}

					/*Delete the Thread 2*/
					if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_2 ) )
					{
						u32Ret += 5000;
					}
				}

				/*Delete the Thread 1*/
				if( OSAL_ERROR == OSAL_s32ThreadDelete( tID_1 ) )
				{
					u32Ret += 6000;
				}
			}

			/*Close the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreClose( globalsemHandle ) )
			{	
				/*Close failed*/
				u32Ret += 7000;
			}
			else
			{
				/*Delete the semaphore*/
				if( OSAL_ERROR == OSAL_s32SemaphoreDelete( GLOBAL_SEMAPHORE_NAME ) )
				{
					/*Delete failed*/
					u32Ret += 8000;
				}
			}
		}
	   /*Close the semaphore*/
	   if( OSAL_ERROR == OSAL_s32SemaphoreClose( globalsemHandle_ ) )
	   {	
	   		/*Close failed*/
			u32Ret += 7000;
	   }
	   else
	   {
			/*Delete the semaphore*/
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( GLOBAL_SEMAPHORE_NAME_ ) )
			{
				/*Delete failed*/
				u32Ret += 8000;
			}
		}
	}

	/*Data check*/
	if( VALID_VAL != u8Byte )
	{
		u32Ret += 10000;
	}

	/*Return the error code*/
	return (u32Ret+u32CFRC_1+u32CFRC_2+u32CFRC_3);
}

/*****************************************************************************
* FUNCTION    :	   u32NoBlockingSemWait()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_022
* DESCRIPTION :    In the OSAL API for Semaphore wait, use the
				   OSAL_C_TIMEOUT_NOBLOCKING flag.
				   Test following scenarios:
				   1).Wait with OSAL_C_TIMEOUT_NOBLOCKING 
				      flag set with initial semaphore count as 1 
				      - Wait should pass
				   2).Wait on Semaphore value of zero( count value = 0 )
					  with OSAL_C_TIMEOUT_NOBLOCKING flag set.
					  - Wait should return OSAL_E_TIMEOUT				   				   					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 8,2008
*******************************************************************************/
tU32 u32NoBlockingSemWait( tVoid )
{
	/*Definitions*/
	tU32 u32Ret   			  = 0;
	tS32 s32SemCount          = SEMAPHORE_COUNT_INVALID; 

	#if DEBUG_MODE
	tU32 u32ECode 			  = OSAL_E_NOERROR;
	#endif
	
	OSAL_tSemHandle semHandle = 0; 

	/*Create a Semaphore in the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	}
	
	/*Wait on this Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle, OSAL_C_TIMEOUT_NOBLOCKING )	)
	{
		/*Wait on Semaphore should not fail, as one instance of the resource 
		is still availiable*/
		u32Ret += 1000;
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}
	else
	{
		/*Get Semaphore Status*/
		if( OSAL_ERROR != OSAL_s32SemaphoreGetValue( semHandle,&s32SemCount ) )
		{
			if( INIT_VAL == s32SemCount )
			{
				if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle, OSAL_C_TIMEOUT_NOBLOCKING )	)
				{
					/*Failure is an expected behaviour*/
					if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
					{
						/*Wrong error code*/
						u32Ret += 1500;
					}
				}
				else
				{
					/*Semaphore Wait cannot pass!*/
					u32Ret += 1700;
				}
			}
			else
			{
				/*Semaphore Count Value Incorrect*/
				u32Ret += 1800;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
			
			/*Get Semaphore Value Failed*/ 	
			u32Ret += 2000;
		}
	}

	/*Close and Delete the Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( semHandle ) )
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
			
			/*Update the error code*/
			u32Ret += 2500;
		}
	}
	else
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
		
		/*Update the error code*/
		u32Ret += 3000;
	}
	
	/*Return the error code*/
	return u32Ret;	
}

/*****************************************************************************
* FUNCTION    :	   u32BlockingDurationSemWait()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_023
* DESCRIPTION :    Test For the wait duration for a Blocking Wait for the
				   OSAL Semaphore Wait API. 				   				   					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 8,2008
*******************************************************************************/
tU32 u32BlockingDurationSemWait( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;

	#if DEBUG_MODE
	tU32 u32ECode 			   = OSAL_E_NOERROR;
	#endif

	OSAL_tSemHandle semHandle  = 0;
	OSAL_tMSecond u32StartTime = 0;  
	OSAL_tMSecond u32EndTime   = 0;  
	OSAL_tMSecond u32Duration  = 0;  

	/*Create the Semaphore with initial semaphore count as 0*/
    if( OSAL_ERROR == OSAL_s32SemaphoreCreate( SEMAPHORE_NAME,&semHandle,0 ) )
    {
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	} 

	/*Mark the Start Time*/
	u32StartTime = OSAL_ClockGetElapsedTime( );

	/*Wait on the Semaphore with semaphore count as zero and wait 
	time as one second*/
	if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle,ONE_SECOND ) )
	{
		/*Mark the End Time*/
		u32EndTime = OSAL_ClockGetElapsedTime( );

		if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 500;
		}
		else
		{
			if( u32EndTime > u32StartTime )
			{
				/*Calculate duration of wait*/
				u32Duration = u32EndTime - u32StartTime;

				if(!( (( ONE_SECOND - THIRTY_MILLISECONDS ) <= u32Duration)&&(u32Duration <= ( ONE_SECOND + THIRTY_MILLISECONDS ) ) ) )
				{
					/*Wait duration in error*/
					u32Ret += 600;
				} 
			}
			else
			{
				/*Overflow*/
				u32Ret += 700;
			}
		}
	}
	else
	{
		/*Wait cannot pass!*/
		u32Ret += 1000;
	}
	
	/*Close the Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( semHandle ) )
	{
		/*Delete the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME )	)
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif	

			/*Update error code*/
			u32Ret += 2000;
		}
	}
	else
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Update error code*/
		u32Ret += 3000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32WaitPostSemAfterDel()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_024
* DESCRIPTION :    Wait and Post on a Semaphore after delete 				   				   					   			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 8,2008
*******************************************************************************/
tU32 u32WaitPostSemAfterDel( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			   = 0;
	tU32 u32DelFlag 		   = 0;

	#if DEBUG_MODE
	tU32 u32ECode 			   = OSAL_E_NOERROR;
	#endif

	OSAL_tSemHandle semHandle  = 0;

	/*Create a Semaphore in the system*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( SEMAPHORE_NAME, &semHandle,SEMAPHORE_START_VALUE ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
		
		/*Return Immediately*/
		return 10000;
	}
	
	/*Delete the Semaphore from the system*/
	if( OSAL_ERROR != OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
	{
		/*Wait on the Semaphore - Should pass*/	
		if( OSAL_ERROR != OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_FOREVER ) ) 
		{
			if( OSAL_ERROR != OSAL_s32SemaphorePost( semHandle ) )
			{
				if( OSAL_ERROR != OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_FOREVER ) )
				{
					if( OSAL_ERROR == OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_NOBLOCKING ) )
					{
						if( OSAL_E_TIMEOUT != OSAL_u32ErrorCode( ) )
						{
							/*Wrong error code*/
							u32Ret += 1000;
						}
					}
					else
					{
						/*Wait Should not pass here!*/
						u32Ret += 1200;
					}
				}
				else
				{
					#if DEBUG_MODE
					u32ECode = OSAL_u32ErrorCode( );
					#endif

					/*Wait failed!*/
					u32Ret += 1500;
				}
			}
			else
			{
				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif

				/*Post failed*/
				u32Ret += 1700;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Wait failed*/
			u32Ret += 1800;
		}

		/*Set the delete flag*/
		u32DelFlag = 1;
	}
	else
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Semaphore delete failed*/
		u32Ret += 1900;
	}

	/*Restore*/
	if( u32DelFlag )
	{
		/*Close the Semaphore and release resources*/
		if( OSAL_ERROR == OSAL_s32SemaphoreClose( semHandle ) )
		{
		 	#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif	

			/*Update the error code*/
			u32Ret += 2000;
		}
	}
	else
	{
		/*Close the Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semHandle ) )
		{
			if( OSAL_ERROR == OSAL_s32SemaphoreDelete( SEMAPHORE_NAME ) )
			{
			 	#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif	

				/*Update the error code*/
				u32Ret += 2300;
			}
		}
		else
		{
			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif

			/*Update the error code*/
			u32Ret += 2500;
		}
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
STRESS TEST SEMAPHORE
******************************************************************************
******************************************************************************
FUNCTION    :	   vSelectThreadPriorityRandom()
PARAMETER   :    none
RETURNVALUE :    none
DESCRIPTION :    Helper Function - Random Priority Select
HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid vSelectThreadPriorityRandom( )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand     = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectSemaphoreIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    tU8
* DESCRIPTION :    Helper Function - Random Select Semaphore Index
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tU8 u8SelectSemaphoreIndexRandom( )
{
	/*Declarations*/
	tU8 u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand	    = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX; 
	/*Calculate the Semaphore Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_SEMAPHORES );
	/*Return the Semaphore Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandom()
* PARAMETER   :    tU32
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Random Wait Function.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid vWaitRandom(tU32 u32ModVal )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand        = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32ModVal +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}

/*****************************************************************************
* FUNCTION    :	   Post_Thread()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Post 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid Post_Thread( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );
		OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread");

		/*Randomly Select the Index*/
		u8Index = u8SelectSemaphoreIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressList[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{

			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread:Semaphore open Successful");
			/*Post on the opened semaphore*/
			if (OSAL_OK == OSAL_s32SemaphorePost( smphrHandle ))
			{
				OEDT_HelperPrintf(TR_LEVEL_USER_4," Post_Thread:Semaphore posted");
			}

			if( u8SelectSemaphoreIndexRandom( ) < MAX_SEMAPHORES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Post_Thread:Semaphore close failed");
				
				}
			}
			else
			{
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread:Semaphore close failed");
				
				}
				vWaitRandom( FIVE_SECONDS );
			}
		}
		else
		{
				OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread:Semaphore open failed");
			
		}
	
	}	
}

/*****************************************************************************
* FUNCTION    :	   Wait_Thread()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Wait 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid Wait_Thread( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );
		OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread");

		/*Randomly Select the Index*/
		u8Index = u8SelectSemaphoreIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressList[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{
			
			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore open Successful");
			
			/*Post on the opened semaphore*/
			OSAL_s32SemaphoreWait( smphrHandle,OSAL_C_TIMEOUT_FOREVER );

			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore wait release");

			if( u8SelectSemaphoreIndexRandom( ) < MAX_SEMAPHORES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore close failed");
				
				}
			}
			else
			{
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore close failed");
				
				}
				vWaitRandom( FIVE_SECONDS );
			}
		}
	else
	{
			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore open failed");
		
	}
	}
}

/*****************************************************************************
* FUNCTION    :	   Close_Thread()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Delete and Close 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid Close_Thread( tVoid *vptr )
{
	/*Declarations*/
	tU8 u8Index;
	tU8 u8Count = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	
	for(;;)
	{
		/*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Randomly select the semaphore index*/
		u8Index = u8SelectSemaphoreIndexRandom( );
		OEDT_HelperPrintf(TR_LEVEL_USER_4,"Close_Thread");

		OSAL_s32SemaphoreWait( baseSemHandle, OSAL_C_TIMEOUT_FOREVER );

		if( ( sem_StressList[u8Index].hToBeDeleted != TRUE ) && (sem_StressList[u8Index].hSem != OSAL_C_INVALID_HANDLE ) )
		{
			/*Set the to be deleted flag*/
			sem_StressList[u8Index].hToBeDeleted = TRUE;
			OSAL_s32SemaphorePost( baseSemHandle );
		
			/*Do a Delete on the specific semaphore at the randomly selected index*/
			if( OSAL_s32SemaphoreDelete( sem_StressList[u8Index].hSemName )	== OSAL_OK )
			{
				
				OEDT_HelperPrintf(TR_LEVEL_USER_4,"Close_Thread:Semaphore Delete Successful");
				/*Post 100 times on a deleted Semaphore*/
				while( u8Count < 100 )
				{
					/*Post on the Semaphore*/
					OSAL_s32SemaphorePost( sem_StressList[u8Index].hSem );
					/*Increment count*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Close the Semaphore*/
				OSAL_s32SemaphoreClose( sem_StressList[u8Index].hSem );

				/*Invalidate the handle*/
				sem_StressList[u8Index].hSem = OSAL_C_INVALID_HANDLE;
			}
			/*Semaphore at this index already deleted*/
			sem_StressList[u8Index].hToBeDeleted = FALSE;
		}
		else
		{
			OSAL_s32SemaphorePost( baseSemHandle );
		}
	}
}

/*****************************************************************************
* FUNCTION    :	   Create_Thread()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Create 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tVoid Create_Thread( tVoid *vptr )
{
	/*Declarations*/
	 tU8 u8Index;
     OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	 for(;;)   
   	 {
        /*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );
		OEDT_HelperPrintf(TR_LEVEL_USER_4,"Create_Thread");

		/*Select semaphore index randomly*/
      	u8Index = u8SelectSemaphoreIndexRandom();

      	/*Check whether handle to the corresponding index is invalid*/
      	if (sem_StressList[u8Index].hSem == OSAL_C_INVALID_HANDLE)
      	{
         	/*Create the semaphore*/
         	if (OSAL_s32SemaphoreCreate (sem_StressList[u8Index].hSemName, &sem_StressList[u8Index].hSem, 0) != OSAL_OK)
      		{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Create thread:Semaphore Create failed");
      		
      		}
				else
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Create thread:Semaphore Create Successful");
		
				}
         	       
      	}
		/*Wait randomly*/
      	vWaitRandom( FIVE_SECONDS );
    }
}

/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute 
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Portal for Thread Spawn			       	
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vEntryFunction( 
					  		void (*ThreadEntry) (void *),
					  		const tC8 *threadName,
                      		tU8 u8NoOfThreads, 
                      		OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[] 
                    )
{
	/*Definitions*/
	tU8 u8Index                                    = 0;
	//tU32 u32ECode                                  = 0;
	tChar acBufThreadName[TOT_THREADS][MAX_LENGTH];

	OSAL_pvMemorySet(acBufThreadName,0,sizeof(acBufThreadName));
	/*Spawn the required number of tasks*/
	while( u8Index < u8NoOfThreads )
	{
		/*Fill in the Thread Name*/
		OSAL_s32PrintFormat( acBufThreadName[u32GlobCounter],"%s_%d",threadName,u8Index );

		/*Fill in the Thread Attribute structure*/
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;/*2 KB Minimum*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pfEntry      = ThreadEntry;
		tr_Attr[u8Index].szName       =	acBufThreadName[u32GlobCounter];

		/*Spawn*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			//u32ECode += u8Index;
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobCounter++;
	}
}
/*****************************************************************************
* FUNCTION    :	   u32SemaphoreStressTest()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_SEM_025

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elasped Time.
				   2). Create a Base Semaphore.
				   2). Create 25 Semaphores in the system.
				   3). Create 10 Threads which does Post operation 
				       on 25 Semaphores Randomly.
				   4). Create 15 Threads which does Wait operation 
				       on 25 Semaphores Randomly.
				   5). Create 1 Thread which does Delete and Close 
				       operation on 25 Semaphores Randomly.
				   6). Create 2 Threads which does a Re- Create on 
				       25 Semaphores Randomly,if deleted earlier.
				   7). Set a Wait OSAL_C_TIMEOUT_FOREVER.
				   8). Close the Base Semaphore.
				   9). Delete the Base Semaphore.
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Semaphore 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Semaphore
				   API Interface is Robust.Timeout Time can be reconfigured in the 
				   file "oedt_osalcore_TestFuncs.h" under the define 
				   STRESS_SEMAPHORE_DURATION.Whether this case should be run 
				   can be configured in the same file as part of the define
				   STRESS_SEMAPHORE.
					   			       	
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
//#define OEDT_SM_1700000S 1700000
tU32 u32SemaphoreStressTest( tVoid )	
{
	/*Definitions*/
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = {"\0"};

	/*Get Base for the seed*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Initialise a random seed - Subsequent returns from 
	OSAL_s32Random will be influenced by the seed value*/
	OSAL_vRandomSeed( (tU32)elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "BASE_SEMAPHORE",&baseSemHandle, 1 );

	OEDT_HelperPrintf(TR_LEVEL_USER_4,"BASE_SEMAPHORE Created");

	/*Create all the MAX_SEMAPHORES semaphores*/
	while( u8Index<MAX_SEMAPHORES )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_Sem_%d",u8Index );
		/*Copy the semaphore name to Semaphore Table List*/
		(tVoid)OSAL_szStringCopy( sem_StressList[u8Index].hSemName,acBuf );
		/*Create the Semaphore*/
		OSAL_s32SemaphoreCreate( sem_StressList[u8Index].hSemName,&sem_StressList[u8Index].hSem,0 );
		/*Set the flag*/
		sem_StressList[u8Index].hToBeDeleted = FALSE;
		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment Counter*/
		u8Index++;
	}

	/*Invoke the Post threads*/
	vEntryFunction( Post_Thread,"Post_Thread",NO_OF_POST_THREADS,post_ThreadAttr,post_ThreadID );
	/*Invoke the Wait threads*/
	vEntryFunction( Wait_Thread,"Wait_Thread",NO_OF_WAIT_THREADS,wait_ThreadAttr,wait_ThreadID );
	/*Invoke the Close thread*/
	vEntryFunction( Close_Thread,"Close_Thread",NO_OF_CLOSE_THREADS,close_ThreadAttr,close_ThreadID );
	/*Invoke the Create threads*/
	vEntryFunction( Create_Thread,"Create_Thread",NO_OF_CREATE_THREADS,create_ThreadAttr,create_ThreadID );

	/*Reset the Global Counter*/
	u32GlobCounter = 0;

	/*Wait slot for Crash - OSAL_C_TIMEOUT_FOREVER days.*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( post_ThreadID[u8PostThreadCounter] );
		/*Increment Counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( wait_ThreadID[u8WaitThreadCounter] );
		/*Increment Counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( close_ThreadID[u8CloseThreadCounter] );
		/*Increment Counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( create_ThreadID[u8CreateThreadCounter] );
		/*Increment Counter*/
		u8CreateThreadCounter++;
	}

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandle );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

	/*Return the error code*/
	return 0;
}
