/******************************************************************************
 *FILE         : oedt_osalcore_IPC_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the OSAL Interprocess Communication
 *               
 *AUTHOR       : Tinoy Mathews(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 
				ver 1.7 - 15.07.09 -  
				Removed create/delete as message pool is created during start up
				rav8kor 

 *              ver1.6   - 22-04-2009
                lint info removal
				sak9kor
 *				
 *				ver1.5	 - 14-04-2009
                warnings removal
				rav8kor
 
 				Version 1.4 
 * 			 Added trace for Stress Test by  
 *				 Anoop Chandran (RBEI\ECM1)
 *				 15- 10- 2008

 *				 Version 1.3 
 *				 Event clear update by  
 *				 Anoop Chandran (RBEI\ECM1) 27/09/2008
 *
 *				 Version 1.2 , 10 - 03 - 2008
 *				 Modified the communication strategy for IPC to a 
 *				 notification strategy using OSAL Events
 *
 *				 Version 1.1 , 27 - 02 - 2008
 *				 Added cases TU_OEDT_OSAL_CORE_IPC_010 - 
 *				 TU_OEDT_OSAL_CORE_IPC_018
 *				 Tinoy Mathews( RBIN/ECM1 )
 *
 *				 Initial Version 1.0 , 6 - 02 - 2008
 *				 Tinoy Mathews( RBIN/ECM1 )

				
 *
 *	             				
 *****************************************************************************/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "oedt_helper_funcs.h"
#include "oedt_osalcore_IPCom_TestFuncs.h"

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1

//Switch
//Define this switch if test process is to be run from /nor0/
//Comment this switch if test process is to be run from /host/Debug*/
#define NOR_RESIDENT


extern OSAL_tEventMask    gevMask;

/*GLOBAL VARIABLES*/

/*IPC Event*/
OSAL_tEventHandle eveIPC          = 0;    

/*IPC Event Tracker*/
/*0x00 - Event not in System
  0xFF - Event in System
*/
tU8 u8IPCEventTracker        	  = 0x00; 

/*Feature Select and Error Byte Shared Memory Handle*/
OSAL_tShMemHandle shHandle        = 0;    

/*Feature Select and Error Byte Shared Memory Tracker*/
/*0x00 - Shared Memory not in System
  0xFF - Shared Memory in System
*/
tU8 u8FeatureErrorSHTracker       = 0x00;

/*Custom Process ID initialization*/
OSAL_tProcessID	prID          	  = INVALID_PID;

/*Custom Process attributes*/ 
#ifdef NOR_RESIDENT
OSAL_trProcessAttribute	prAtr 	  = {
							  		  "/nand0/ProcOEDT_PF_Test.out",     /*szName*/
									  PRIORITY,						    /*u32Priority*/								 	
								 	  "TEST_PROCESS",					/*szAppName*/
									  0,								/*szCommandLine*/
                                      " "/* CGroup path*/
								    };

/* Nor0 can be used in case Nand0 is not available */
OSAL_trProcessAttribute	prAtr1 	  = {
							  		  "/nor0/ProcOEDT_PF_Test.out",     /*szName*/
									  PRIORITY,						    /*u32Priority*/								 	
								 	  "TEST_PROCESS",					/*szAppName*/
									  0,								/*szCommandLine*/
                                      " "/* CGroup path*/
								    };

#else
OSAL_trProcessAttribute	prAtr 	  = {
							  		  "/host/DEBUG/ProcOEDT_PF_Test.out",/*szName*/
									  PRIORITY,						    /*u32Priority*/								 	
								 	  "TEST_PROCESS",					/*szAppName*/
									  0,								/*szCommandLine*/
                                      " "/* CGroup path*/
								    };
#endif

/*Process Existance Tracker*/
/*0x00 - Process not in System
  0xFF - Process in System
*/
tU8 u8ProcessTracker              = 0x00;

								
OSAL_tShMemHandle shHandleIPC   	  = 0;
OSAL_tSemHandle baseSemHandleIPC  = 0;
tU32 u32GlobCounterIPC            = 0;
tU8 * shptr                       = OSAL_NULL;
tU8 * shptr_osalrsc               = OSAL_NULL;



SemaphoreTableEntryIPC sem_StressListIPC[MAX_OSAL_RESOURCES];
EventTableEntryIPC     event_StressListIPC[MAX_OSAL_RESOURCES];
MessageQueueTableEntryIPC mesgque_StressListIPC[MAX_OSAL_RESOURCES];

OSAL_trThreadAttribute post_ThreadAttrIPC[NO_OF_POST_THREADS];
OSAL_trThreadAttribute wait_ThreadAttrIPC[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute close_ThreadAttrIPC[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute create_ThreadAttrIPC[NO_OF_CREATE_THREADS];
OSAL_tThreadID post_ThreadIDIPC[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID wait_ThreadIDIPC[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID close_ThreadIDIPC[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID create_ThreadIDIPC[NO_OF_CREATE_THREADS]           = {OSAL_NULL};

/*****************************************************************************
* FUNCTION    :	   vHelperPrintf()
* PARAMETER   :    none
* RETURNVALUE :    none   
* DESCRIPTION :    Sends Traces to indicate reasons possible for Process 
				   Spawn Failure
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 29, 2008
*******************************************************************************/
tVoid vHelperPrintf( tVoid )
{
	/*Printfs*/
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Process Spawn Failed!\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Build library     -:- osal_e_dev_test_osalcore\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Generate out file -:- ProcOEDT_PF_Test\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Copy the ProcOEDT_PF_Test out file to /host/DEBUG path\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Copy the ProcOEDT_PF_Test out file to %_SWNAVIROOT%/bin/DRAGON/tengine/saphire/debug/Processes \n" );
    OEDT_HelperPrintf( TR_LEVEL_USER_1,"Function returning immediately with failure,initiating shutdown..." );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Known reasons for Failure:\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"1). Build process above unsuccessful\n" );
	OEDT_HelperPrintf( TR_LEVEL_USER_1,"2). 10 Processes exist in the system\n" );

	/*Return*/
	return;
}

/*****************************************************************************
* FUNCTION    :	   u32StartInit()
* PARAMETER   :    Feature Byte
* RETURNVALUE :    Unsigned 32 bit integer 
                   = 0 if initialization successful
                   > 0 if initialization fails   
* DESCRIPTION :    Initialize Data Structures
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 29, 2008
*******************************************************************************/
tU32 u32StartInit( tU8 u8FeatureByte )
{
	/*Declarations*/
	tU32 u32Ret = 0;
	/*Check if IPC Event is already created*/
	if( !u8IPCEventTracker )
	{
		/*Create the IPC Event*/
		if( OSAL_ERROR != OSAL_s32EventCreate( "EVE_IPC",&eveIPC ) )
		{
		   /*Initialize the IPC Event Tracker*/
		   u8IPCEventTracker = 0xFF;

		   /*Create the Feature Select and Error Byte Shared Memory*/
		   if( !u8FeatureErrorSHTracker )
		   {
		   		/*Create the Feature Select and Error Select Shared Memory*/
				if( OSAL_ERROR != ( shHandle = OSAL_SharedMemoryCreate( "SH_DATA_REP",OSAL_EN_READWRITE,2 ) ) )
				{
					/*Initialize Feature Select and Error Byte Shared Memory Tracker*/
					u8FeatureErrorSHTracker = 0xFF;
				}
				else
				{
					/*Shut down Event,Leave Initialization complete
					or none at all*/
					if( OSAL_ERROR != OSAL_s32EventClose( eveIPC ) )
					{
						OSAL_s32EventDelete( "EVE_IPC" );
					}
					/*Set the IPC event tracker to suggest event non existent*/
					u8IPCEventTracker = 0x00;
					/*Reinitialize event handle*/
					eveIPC = 0;

					/*Reinitialize Shared Memory handle*/
					shHandle = 0;

					/*Update error*/
					u32Ret += 200;
			    }
		   }
		}
		else
		{
			/*Reinitialize event handle*/
			eveIPC = 0;

			/*Update the error code*/
			u32Ret += 100;
		}	 	
	}

	/*Enter Feature Byte*/
	if( OSAL_NULL != ( shptr =  ( tU8* )OSAL_pvSharedMemoryMap( shHandle,OSAL_EN_READWRITE,2,0 ) ) )
	{
		/*Initialize Shared Memory*/
		*( shptr + 0 ) = u8FeatureByte; /*Choice byte*/
		*( shptr + 1 ) = MIN_ERROR_CODE; /*Error byte*/
	}
	else
	{
		return ( 2+u32Ret );
	}
	
	/*Return error*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32StartProcess()
* PARAMETER   :    none
* RETURNVALUE :    Unsigned 32 bit integer 
                   = 0 if initialization
                   > 0 if initialization fails   
* DESCRIPTION :    Start Process ProcOEDT_PF_Test
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 29, 2008
*******************************************************************************/
tU32 u32StartProcess( tVoid )
{
	
	
	/*Spawn the Custom Process*/
	if( !u8ProcessTracker )
	{
		if( OSAL_ERROR != ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
		{
			/*Update Process Tracker*/
			u8ProcessTracker = 0xFF;
			/*Signal the Server*/
			OSAL_s32EventPost( eveIPC,CLIENT_TO_SERVER,OSAL_EN_EVENTMASK_OR );			
			/*Return Success*/	
			return 0;
		}
		else
		{	
			/*  Nor0 can be used in case if nand0 is not available */
			if( OSAL_ERROR != ( prID = OSAL_ProcessSpawn( &prAtr1 ) ) )
			{
				/*Update Process Tracker*/
				u8ProcessTracker = 0xFF;
				/*Signal the Server*/
				OSAL_s32EventPost( eveIPC,CLIENT_TO_SERVER,OSAL_EN_EVENTMASK_OR );			
				/*Return Success*/	
				return 0;
			}
			else
			{	
			/*Clean contents in Process ID*/
			prID = 0;
			/*Query reason for Process Spawn failure*/
			OSAL_u32ErrorCode( );			
			/*If Process Spawn fails, Trace out messages*/
			vHelperPrintf( );
			/*Return with failure*/
			return 100;
		}
	}
	}
	else
	{
		/*Process is already existent Post to Server*/
		OSAL_s32EventPost( eveIPC,CLIENT_TO_SERVER,OSAL_EN_EVENTMASK_OR );			
	}

	/*Return success*/
	return 0;
}
	

/*****************************************************************************
* FUNCTION    :	   u32SharedMemoryTestIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_001
* DESCRIPTION :    Test whether the Non Resident Common memory allocated by
				   OSAL Shared Memory Create API call is accessible across 
				   Processes without Non Resident Common Memory corruption                   		       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SharedMemoryTestIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST0 ) )
		return 1;

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
		return 3;

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 ); 		

		/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 ); 		

	/*Query Error Field*/
	if( 0xFF != *( shptr + 1 ) )
	{
		u32Ret += 200;
	}

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SemWaitInCustomProcessIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_002
* DESCRIPTION :    Wait in the body of a Custom Process on a Semaphore Created
                   in the Process ProcOEDT_PF,with initial count of 1                   		       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SemWaitInCustomProcessIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	OSAL_tSemHandle   semWait	  = 0;
	tS32 s32SemaphoreCount        = -1;
	tU8 u8ErrorFromServer         = 0;
	gevMask = 0;
	
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST1 ) )
		return 1;

	/*Create Wait Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "WAIT_SEM",&semWait,1 ) )
	{
		/*Return Immediately*/
		return 3;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{
		/*Close the Wait Semaphore and Delete it*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semWait ) )
		{
			OSAL_s32SemaphoreDelete( "WAIT_SEM" );
		}
		/*Return Immediately*/ 
		return 4;
	}
	
	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
					 		
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
					 		
	/*Check the status of the Wait Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreGetValue( semWait,&s32SemaphoreCount ) )
	{
		if( 0 != s32SemaphoreCount )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 6;
	}
	
	/*Close and Delete the Wait Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( semWait ) )
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "WAIT_SEM" ) )
		{
			u32Ret += 7;
		}
	}
	else
	{
		u32Ret += 8;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SemPostInCustomProcessMaxIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_003
* DESCRIPTION :    Create a Semaphore in process ProcOEDT_PF with count 65535
				   Post once more in the Custom Process, this post should 
				   fail                   		       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SemPostInCustomProcessMaxIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	OSAL_tSemHandle   semPost	  = 0;
	tU8 u8ErrorFromServer         = 0;

	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST2 ) )
		return 1;

	/*Create the Post Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "POST_SEM",&semPost,65535 ) )
	{
		/*Return Immediately*/
		return 3;
	}
	
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Wait Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semPost ) )
		{
			OSAL_s32SemaphoreDelete( "POST_SEM" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );

	/*Close and Delete the Wait Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( semPost ) )
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "POST_SEM" ) )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 6;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;
	
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SemRedeleteIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_004
* DESCRIPTION :    Create a Semaphore in process ProcOEDT_PF with count 65535
				   Post once more in the Custom Process, this post should 
				   fail                   		       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SemRedeleteIPC( tVoid )
{
	/*Declarations*/
	OSAL_tSemHandle   semTest	  = 0;
	tU32 u32Ret 			      = 0;
	tU8 u8ErrorFromServer         = 0;
	gevMask = 0;
	
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST3 ) )
		return 1;
		
	/*Create a Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "TEST_SEM",&semTest,0 ) )
	{
		/*Return Immediately*/
		return 3;
	}
	
	/*Close the Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreClose( semTest ) )
	{
		return 4;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Wait Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semTest ) )
		{
			OSAL_s32SemaphoreDelete( "TEST_SEM" );
		}
		/*Return Immediately*/ 
		return 5;
	}
		
	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
		
	/*Try to delete the Test Semaphore*/
	if( OSAL_ERROR != OSAL_s32SemaphoreDelete( "TEST_SEM" ) )
	{
		/*Update the error code*/
		u32Ret += 6;	
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;
	
	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SemDelOpenCounterNotZeroIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_005
* DESCRIPTION :    Try to Delete the Semaphore in the Custom Process while the
				   Open Counter for the Semaphore is still not zero
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SemDelOpenCounterNotZeroIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	OSAL_tSemHandle   semDel	  = 0;
	tU8 u8ErrorFromServer         = 0;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST4 ) )
		return 1;
	
	/*Create Del Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "DEL_SEM",&semDel,0 ) )
	{
		/*Return Immediately*/
		return 3;
	}
	
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Wait Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semDel ) )
		{
			OSAL_s32SemaphoreDelete( "DEL_SEM" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );

	/*Close and Delete the Del Semaphore - A close here should remove
	semaphore resource in case at the server side operations are correct*/
	if( OSAL_ERROR == OSAL_s32SemaphoreClose( semDel ) )
	{
		u32Ret += 5;

		/*Try a dummy Delete, in case delete does not happen with a close*/
		OSAL_s32SemaphoreDelete( "DEL_SEM" );

	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SemCreateSameNameIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_006
* DESCRIPTION :    Create a Semaphore in the Process ProcOEDT_PF and in also
				   in the Custom Process.The Semaphore Creation in the custom
				   process with common name should fail.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32SemCreateSameNameIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	tU8 u8ErrorFromServer         = 0;
	OSAL_tSemHandle   semCommon	  = 0;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST5 ) )
		return 1;

	/*Create Wait Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "COMMON_SEM",&semCommon,1 ) )
	{
		/*Return Immediately*/
		return 3;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Wait Semaphore*/
		if( OSAL_ERROR != OSAL_s32SemaphoreClose( semCommon ) )
		{
			OSAL_s32SemaphoreDelete( "COMMON_SEM" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
	
	if( OSAL_ERROR != OSAL_s32SemaphoreClose( semCommon ) )
	{
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "COMMON_SEM" ) )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 6;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32EventCreateSameNameIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_007
* DESCRIPTION :    Create an Event with the same name in the custom process
				   as in ProcOEDT_PF, creation of the event in custom process
				   must fail
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32EventCreateSameNameIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	tU8 u8ErrorFromServer         = 0;
	OSAL_tEventHandle   evHandle  = 0;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST6 ) )
		return 1;


	/*Create Common Event*/
	if( OSAL_ERROR == OSAL_s32EventCreate( "COMMON_EVENT",&evHandle ) )
	{
		/*Return Immediately*/
		return 3;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			OSAL_s32EventDelete( "COMMON_EVENT" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
			
	/*Close and Delete the Event*/
	if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( "COMMON_EVENT" ) )
		{
			u32Ret += 5;
		}
	}
	else
	{
		u32Ret += 6;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32EventWaitPostIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_008
* DESCRIPTION :    Wait for a Pattern in Process ProcOEDT_PF posted from
				   a custom process
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32EventWaitPostIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 			      = 0;
	tU8 u8ErrorFromServer         = 0;
	OSAL_tEventHandle   evHandle  = 0;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST7 ) )
		return 1;
	

	/*Create Wait Semaphore*/
	if( OSAL_ERROR == OSAL_s32EventCreate( "WAITPOST_EVENT",&evHandle ) )
	{
		/*Return Immediately*/
		return 3;
	}
	
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			OSAL_s32EventDelete( "WAITPOST_EVENT" );
		}
		/*Return Immediately*/ 
		return 4;
	}
		
	/*Wait for a specific pattern*/
	OSAL_s32EventWait( evHandle,0x00000004,
					   OSAL_EN_EVENTMASK_AND,
					   OSAL_C_TIMEOUT_FOREVER,
					   &gevMask );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	evHandle,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
		
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
			
	/*Close and Delete the Event*/
	if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
	{
		if( OSAL_ERROR == OSAL_s32EventDelete( "WAITPOST_EVENT" ) )
		{
			u32Ret += 400;
		}
	}
	else
	{
		u32Ret += 600;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;
   
	/*Return error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32EventRedeleteIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_009
* DESCRIPTION :    Try to delete an event in ProcOEDT_PF, which has already
				   been deleted in the custom process
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 6, 2008
*******************************************************************************/
tU32 u32EventRedeleteIPC( tVoid )
{
	/*Declarations*/
	
	tU32 u32Ret 			      = 0;
	OSAL_tEventHandle   evHandle  = 0;
	tU8 u8ErrorFromServer         = 0;
	gevMask = 0;

	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST8 ) )
		return 1;
	
	
	/*Create Wait Semaphore*/
	if( OSAL_ERROR == OSAL_s32EventCreate( "DEL_EVENT",&evHandle ) )
	{
		/*Return Immediately*/
		return 3;
	}
	else
	{
		/*Close the Event Handle*/
		if( OSAL_ERROR == OSAL_s32EventClose( evHandle ) )
		{
			u32Ret += 4;
		}
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32EventClose( evHandle ) )
		{
			OSAL_s32EventDelete( "DEL_EVENT" );
		}
		/*Return Immediately*/ 
		return 5;
	}
	
   	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );

	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	if( OSAL_ERROR != OSAL_s32EventDelete( "DEL_EVENT" ) )
	{
		u32Ret += 6;
		/*Restore*/
		OSAL_s32EventClose( evHandle );
		OSAL_s32EventDelete( "DEL_EVENT" );
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32MQCreateSameNameIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_010
* DESCRIPTION :    Create a Message Queue in Process ProcOEDT_PF and then
				   try to create the same named Message queue in the Custom 
				   Process.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
*******************************************************************************/
tU32 u32MQCreateSameNameIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	OSAL_tMQueueHandle mqIPC      = 0;
	tU8 u8ErrorFromServer         = 0;	
	gevMask = 0; 
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST9 ) )
		return 1;
	
	/*Create a Message Queue in ProcOEDT_PF Process*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( "MQ_IPC",MAX_MSGS,MAX_MSG_LENGTH,OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Return the error code*/
		return 3;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC )  )
		{
			OSAL_s32MessageQueueDelete( "MQ_IPC" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );

	/*Remove the Message Queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC ) )
	{
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( "MQ_IPC" ) )
		{
			u32Ret += 5;
		}
	}
	else
	{
		/*Update the error code*/
		u32Ret += 6;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32MQRedeleteIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_011
* DESCRIPTION :    Try to delete a message queue which has already been deleted
				   in a custom process.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
*******************************************************************************/
tU32 u32MQRedeleteIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	OSAL_tMQueueHandle mqIPC      = 0;
	tU8 u8ErrorFromServer         = 0;	
	gevMask = 0; 
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST10 ) )
		return 1;
	
	/*Create a Message Queue in ProcOEDT_PF Process*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( "MQ_IPC",MAX_MSGS,MAX_MSG_LENGTH,OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Return the error code*/
		return 3;
	}
	else
	{
		/*Close the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( mqIPC ) )
		{
			/*Attempt to remove the message queue from the system*/
			OSAL_s32MessageQueueDelete( "MQ_IPC" );
			/*Return immediately*/
			return 4;
		}
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC )  )
		{
			OSAL_s32MessageQueueDelete( "MQ_IPC" );
		}
		/*Return Immediately*/ 
		return 5;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
		
	/*Remove the Message Queue - Confirm a successful deletion of message queue
	within the custom process, otherwise do not remove */
	if( 0 == *( shptr + 1 ) )
	{
		if( OSAL_ERROR != OSAL_s32MessageQueueDelete( "MQ_IPC" ) )
		{
			/*Update error code*/
			u32Ret += 6;
		}
	}
	else
	{
		OEDT_HelperPrintf( TR_LEVEL_USER_1,"Redelete cancelled......Custom Process Operation failed" );
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32MQWaitForMSGIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_012
* DESCRIPTION :    Wait for a message from a custom process.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
*******************************************************************************/
tU32 u32MQWaitForMSGIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	tU8  u8Count                  = 0;
	tU8 mqBuffer[20]              = {'\0'};
	tU8 keyString[20]             = "ABCDEFGHIJKLMNOPQRS";
	OSAL_tMQueueHandle mqIPC      = 0;
	tU8 u8ErrorFromServer         = 0;	
	gevMask = 0;

	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST11 ) )
		return 1;
	
   		 
	/*Create a Message Queue in ProcOEDT_PF Process*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( "MQ_IPC",MAX_MSGS,MAX_MSG_LENGTH,OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Return the error code*/
		return 3;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC )  )
		{
			OSAL_s32MessageQueueDelete( "MQ_IPC" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	
	/*Wait for the receipt of a message*/
	OSAL_s32MessageQueueWait( mqIPC,mqBuffer,20,OSAL_NULL,OSAL_C_TIMEOUT_FOREVER );

	/*Verify the Message*/
	while( u8Count < 20 )
	{
		if( mqBuffer[u8Count] != keyString[u8Count] )
		{
			u32Ret += u8Count;
		}

		/*Increment Count*/
		u8Count++;
	}
	
	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );	
		
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
	
	/*Remove the Message Queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC ) )
	{
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( "MQ_IPC" ) )
		{
			u32Ret += 5;
		}
	}
	else
	{
		/*Update the error code*/
		u32Ret += 6;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MQPostMaxMSGIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_013
* DESCRIPTION :    Post the message queue to maximum number of messages.
				   In the Custom Process, try to post beyond this maximum 
				   value.This post should fail.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
*******************************************************************************/
tU32 u32MQPostMaxMSGIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;	
	tU8  u8Count                  = 0;
	tU8 mqBuffer[20]              = "ABCDEFGHIJKLMNOPQRS";
	OSAL_tMQueueHandle mqIPC      = 0;
	tU8 u8ErrorFromServer         = 0;	
	gevMask =0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST12 ) )
		return 1;	
		 
	/*Create a Message Queue in ProcOEDT_PF Process*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( "MQ_IPC",MAX_MSGS,MAX_MSG_LENGTH,OSAL_EN_READWRITE,&mqIPC ) )
	{
		/*Return the error code*/
		return 3;
	}

			
	/*Post the message queue until limit*/
	while( u8Count < 20 )
	{
		/*Post a message into the message queue*/
		OSAL_s32MessageQueuePost( mqIPC,mqBuffer,20,1 );

		/*Increment the count*/
		u8Count++;
	}

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{		
		/*Close and Delete the Common Event */
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC )  )
		{
			OSAL_s32MessageQueueDelete( "MQ_IPC" );
		}
		/*Return Immediately*/ 
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );	
	
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
	
	/*Remove the Message Queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueClose( mqIPC ) )
	{
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( "MQ_IPC" ) )
		{
			u32Ret += 800;
		}
	}
	else
	{
		/*Update the error code*/
		u32Ret += 900;
	}

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32SharedMemMapIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_014
* DESCRIPTION :    Create a Shared memory in Process ProcOEDT_PF, later map
				   Shared Memory beyond the limit in a Custom Process which 
				   should fail.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
*******************************************************************************/
tU32 u32SharedMemMapIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	tU8 u8ErrorFromServer         = 0;	
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST13 ) )
		return 1;

	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
		return 4;

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;  

	/*Return error code*/
	return u32Ret;

}

/*****************************************************************************
* FUNCTION    :	   u32MSGPLIPC()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_015
* DESCRIPTION :    P1 - ProcOEDT_PF
				   P2 - Custom Process
				   Create Message Pool( MP1 - In Shared memory ) in P1, 
				   Create a message in MP1,Close MP1 in P1, Spawn P2 in P1 
				   and wait for a signal from P2.In P2,Open the Message Pool, 
				   Create a Message in MP1,Check MP1 Memory,Get MP1 minimal 
				   size,Close the Message Pool,Signal P1.
     			   Release wait in P1, Open MP1 in P1,Check MP1 Memory,
     			   ,Get MP1 minimal size, Compare the minimal sizes,
     			   Delete MP1.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Feb 20, 2008
15.07.09 - Ravindran P - Removed create/delete as message pool is created during start up 
*******************************************************************************/
tU32 u32MSGPLIPC( tVoid )
{
	/*Declarations*/
	tU32 u32Ret                   = 0;
	tS32 s32MinimalSize_2         = 0;
	tS32 s32MinimalSize_3         = 0;
	tU8 u8ErrorFromServer         = 0;	
	OSAL_trMessage msg;
	gevMask = 0;
	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST14 ) )
		return 1;

		/*Create a Message Pool in Shared Memory*/
	if( OSAL_ERROR == OSAL_s32MessagePoolOpen( ) )
	{
		/*Return immediately*/
		return 3;
	}

	OSAL_s32MessagePoolGetMinimalSize( );

	/*Create a Message in the Message Pool*/
	if( OSAL_ERROR == OSAL_s32MessageCreate( &msg,1000,OSAL_EN_MEMORY_SHARED ) )
	{
		u32Ret += 4;
	}
	
	/*Store the Minimal Size of the Message Pool*/
	s32MinimalSize_2 = OSAL_s32MessagePoolGetMinimalSize( );

	/*Close the Message Pool*/
	if( OSAL_ERROR == OSAL_s32MessagePoolClose( ) )
	{
		u32Ret += 5;
	}
	 
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{
		return 4;
	}

	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );
	/*Open the Message Pool*/
	OSAL_s32MessagePoolOpen( );
	
	/*Store the Minimal Size of Message Pool now*/
	s32MinimalSize_3 = OSAL_s32MessagePoolGetMinimalSize( );

	/*Compare the two minimal sizes*/
	if( s32MinimalSize_2 == s32MinimalSize_3 )
	{
		/*Should be different*/
		u32Ret += 2000;
	}

	/*Close the message Pool*/
	OSAL_s32MessagePoolClose( );

	/*Collect the error code if any from the 
	Server Process ProcOEDT_PF_Test*/
	u8ErrorFromServer = *( shptr+1 );
	u32Ret += u8ErrorFromServer;
		
	/*Return error code*/
	return u32Ret;
}

/*STRESS TESTS*/

/*GENERIC*/
/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tS32 s32RandomNoGenerate( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );
	
	/*Return the Positive Random Number*/
	return s32Rand;
}
/*****************************************************************************
* FUNCTION    :	   vSelectThreadPriorityRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Random Priority Select
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vSelectThreadPriorityRandom( )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand     = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    tU8
* DESCRIPTION :    Helper Function - Random Select Semaphore Index
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tU8 u8SelectIndexRandom( )
{
	/*Declarations*/
	tU8 u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand	    = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX; 
	/*Calculate the Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_OSAL_RESOURCES );
	/*Return the Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandom()
* PARAMETER   :    tU32
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Random Wait Function.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vWaitRandom(tU32 u32ModVal )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	do
	{
		s32Rand        = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32ModVal +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}
/*GENERIC*/

/*SEMAPHORE SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   Post_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Post 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Sem( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	
	for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );

		/*Randomly Select the Index*/
		u8Index = u8SelectIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressListIPC[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread:Semaphore open Successful");

			/*Post on the opened semaphore*/
		  if (OSAL_OK == OSAL_s32SemaphorePost( smphrHandle ))
		  {
		  		OEDT_HelperPrintf(TR_LEVEL_USER_4," Post_Thread:Semaphore posted");
		  }

			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Post_Thread:Semaphore close failed");
				
				}
			}
			else
			{
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Post_Thread:Semaphore close failed");
				
				}
				vWaitRandom( FIVE_SECONDS );
			}
		}
		else
		{
				OEDT_HelperPrintf(TR_LEVEL_USER_4,"Post_Thread:Semaphore open failed");
			
		}

	}	
}

/*****************************************************************************
* FUNCTION    :	   Wait_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Wait 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Sem( tVoid *vptr )
{
	/*Definitions*/
	tU8 u8Index                 = 0;
	OSAL_tSemHandle smphrHandle = 0;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	
	for(;;)
	{
		/*Randomly Select Current Thread Priority*/
		vSelectThreadPriorityRandom( );

		/*Randomly Select the Index*/
		u8Index = u8SelectIndexRandom( );

		/*Open the semaphore received at the index*/
		if( OSAL_s32SemaphoreOpen( sem_StressListIPC[u8Index].hSemName,&smphrHandle ) == OSAL_OK )
		{
			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore open Successful");
			
			/*Post on the opened semaphore*/
			OSAL_s32SemaphoreWait( smphrHandle,OSAL_C_TIMEOUT_FOREVER );
			OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore wait release");

			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				vWaitRandom( FIVE_SECONDS );
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore close failed");
				
				}
			}
			else
			{
				if (OSAL_s32SemaphoreClose ( smphrHandle ) != OSAL_OK)
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore close failed");
				
				}
				vWaitRandom( FIVE_SECONDS );
			}
		}
		else
		{
				OEDT_HelperPrintf(TR_LEVEL_USER_4,"Wait_Thread:Semaphore open failed");
			
		}

	}
}

/*****************************************************************************
* FUNCTION    :	   Close_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Delete and Close 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Sem( tVoid *vptr )
{
	/*Declarations*/
	tU8 u8Index  = 0;
	tU8 u8Count  = 0;
	tU8 u8Offset = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);
	
	for(;;)
	{
		/*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Randomly select the semaphore index*/
		u8Index = u8SelectIndexRandom( );
		/*Calculate offset within Shared Memory*/
		u8Offset = (tU8)(u8Index*3);

		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		if( (  *( shptr_osalrsc + ( u8Offset + 2 ) ) == 'F' ) && ( sem_StressListIPC[u8Index].hSem != OSAL_C_INVALID_HANDLE ) )
		{
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'T';

			OSAL_s32SemaphorePost( baseSemHandleIPC );
		
			/*Do a Delete on the specific semaphore at the randomly selected index*/
			if( OSAL_s32SemaphoreDelete( sem_StressListIPC[u8Index].hSemName )	== OSAL_OK )
			{
				OEDT_HelperPrintf(TR_LEVEL_USER_4,"Close_Thread:Semaphore Delete Successful");

				/*Post 100 times on a deleted Semaphore*/
				while( u8Count < 100 )
				{
					/*Post on the Semaphore*/
					OSAL_s32SemaphorePost( sem_StressListIPC[u8Index].hSem );
					/*Increment count*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Close the Semaphore*/
				OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );

				/*Invalidate the handle*/
				sem_StressListIPC[u8Index].hSem = OSAL_C_INVALID_HANDLE;
			}

			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		}
		else
		{
			OSAL_s32SemaphorePost( baseSemHandleIPC );
		}
	}
}

/*****************************************************************************
* FUNCTION    :	   Create_Thread_Sem()
* PARAMETER   :    tVoid Pointer
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - To Create 25 Threads Randomly.
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Sem( tVoid *vptr )
{
	/*Declarations*/
	 tU8 u8Index;
     OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(vptr);

	 for(;;)   
   	 {
        /*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Select semaphore index randomly*/
      	u8Index = u8SelectIndexRandom();

      	/*Check whether handle to the corresponding index is invalid*/
      	if (sem_StressListIPC[u8Index].hSem == OSAL_C_INVALID_HANDLE)
      	{
         	/*Create the semaphore*/
         	if 
         	(
         		OSAL_OK == 
         		OSAL_s32SemaphoreCreate 
         		(
         			sem_StressListIPC[u8Index].hSemName, 
         			&sem_StressListIPC[u8Index].hSem, 
         			0
         		)
				)
      		{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Create thread:Semaphore Create failed");
      		
      		}
				else
				{
					OEDT_HelperPrintf(TR_LEVEL_USER_4," Create thread:Semaphore Create Successful");
		
				}

			
      	}
		/*Wait randomly*/
      	vWaitRandom( FIVE_SECONDS );
    }
}
/*SEMAPHORE SPECIFIC*/


/*EVENT SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   u32SelectEventPatternRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Patterns Randomly 
				   - For Both Post and Wait
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tU32 u32SelectEventPatternRandom( tVoid )
{
	/*Declarations*/
	tS32 s32Rand           = RAND_MAX;
	tU32 u32evPatternIndex = 0xFFFFFFFF;
	tU32 u32evPattern      = 0x00000000;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Find calculate the event Pattern Index*/
	u32evPatternIndex = ( (tU32)s32Rand%MAX_PATTERNS );

	/*Return the True Pattern*/
	switch( u32evPatternIndex )
	{
		case 0: u32evPattern = PATTERN_1;			break;
		case 1:	u32evPattern = PATTERN_2;			break;
		case 2:	u32evPattern = PATTERN_4;			break;
		case 3:	u32evPattern = PATTERN_8;			break;
		case 4:	u32evPattern = PATTERN_16;			break;
		case 5:	u32evPattern = PATTERN_32;			break;
		case 6:	u32evPattern = PATTERN_64;			break;
		case 7:	u32evPattern = PATTERN_128;			break;
		case 8:	u32evPattern = PATTERN_256;			break;
		case 9:	u32evPattern = PATTERN_512;			break;
		case 10:u32evPattern = PATTERN_1024;		break;
		case 11:u32evPattern = PATTERN_2048;		break;
		case 12:u32evPattern = PATTERN_4096;		break;
		case 13:u32evPattern = PATTERN_8192;		break;
		case 14:u32evPattern = PATTERN_16384;		break;
		case 15:u32evPattern = PATTERN_32768;		break;
		case 16:u32evPattern = PATTERN_65536;		break;
		case 17:u32evPattern = PATTERN_131072;		break;
		case 18:u32evPattern = PATTERN_262144;		break;
		case 19:u32evPattern = PATTERN_524288;		break;
		case 20:u32evPattern = PATTERN_1048576;		break;
		case 21:u32evPattern = PATTERN_2097152;		break;
		case 22:u32evPattern = PATTERN_4194304;		break;
		case 23:u32evPattern = PATTERN_8388608;		break;
		case 24:u32evPattern = PATTERN_16777216;	break;
		case 25:u32evPattern = PATTERN_33554432;	break;
		case 26:u32evPattern = PATTERN_67108864;	break;
		case 27:u32evPattern = PATTERN_134217728;	break;
		case 28:u32evPattern = PATTERN_268435456;	break;
		case 29:u32evPattern = PATTERN_536870912;	break;
		case 30:u32evPattern = PATTERN_1073741824;	break;
		case 31:u32evPattern = PATTERN_2147483648;	break;
		default: break;
	}

	/*Return the pattern*/
	return u32evPattern;
}
/*****************************************************************************
* FUNCTION    :	   Post_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Event Thread 
				   - To post an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Eve( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

	
		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressListIPC[u8EventIndex].hEventName,&event_StressListIPC[u8EventIndex].hEve ) )
		{
						
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Post_EventThread:EVENT %s open Successful",
				event_StressListIPC[u8EventIndex].hEventName
			);

			/*Select the Post Pattern Randomly*/
			u32PostPattern = u32SelectEventPatternRandom( );

			/*Post Randomly selected pattern to event pattern a Random Index*/
			if( OSAL_s32EventPost( event_StressListIPC[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR ) )
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Successful",
					event_StressListIPC[u8EventIndex].hEventName
				);
					
			}
		   else
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Failed",
					event_StressListIPC[u8EventIndex].hEventName
				);
			}
			
				
			

			/*Close Event Field at the Randomly Selected Index*/
			if( u8SelectIndexRandom( ) < (MAX_OSAL_RESOURCES/2) )
			{
		   		vWaitRandom( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );	
		   		vWaitRandom( FIVE_SECONDS );
			}

		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Wait_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Event Thread 
				   - To Wait for an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Eve( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32WaitPattern = 0x00000000;
	gevMask = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
   	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

		
		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressListIPC[u8EventIndex].hEventName,&event_StressListIPC[u8EventIndex].hEve ) )
		{
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Wait_EventThread:EVENT %s open Successful",
				event_StressListIPC[u8EventIndex].hEventName
			);
			/*Select the Wait Pattern Randomly*/
			u32WaitPattern = u32SelectEventPatternRandom( );
		 

			/*Wait for a Pattern*/
			if(
					OSAL_OK ==
					OSAL_s32EventWait( 
									   event_StressListIPC[u8EventIndex].hEve,
									   u32WaitPattern,
									   OSAL_EN_EVENTMASK_OR,
									   OSAL_C_TIMEOUT_FOREVER,
									   &gevMask 
									  )
				)

				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Released",
						event_StressListIPC[u8EventIndex].hEventName
					);
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Release failed",
						event_StressListIPC[u8EventIndex].hEventName
					);
				}

			/*Clear the event*/
			OSAL_s32EventPost
			( 
				event_StressListIPC[u8EventIndex].hEve,
				~(gevMask),
			   OSAL_EN_EVENTMASK_AND
			 );
			/*Close Event Field at the randomly selected index*/
			if( u8SelectIndexRandom( ) < (MAX_OSAL_RESOURCES/2) )
			{
		   		vWaitRandom( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve );	
		   		vWaitRandom( FIVE_SECONDS );
			}

		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Close_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Delete and Close an Event field at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Eve( tVoid *ptr )
{	
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;
	tU8  u8Count 		= 0;
	tU8 u8Offset = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select event index Randomly*/
		u8EventIndex = u8SelectIndexRandom( );

		/*Calculate offset within Shared Memory*/
		u8Offset = (tU8)(u8EventIndex*3);

		/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		/*Check if the event handle is invalid*/
		if( (  *( shptr_osalrsc + ( u8Offset + 2 ) ) == 'F' ) && ( event_StressListIPC[u8EventIndex].hEve != OSAL_C_INVALID_HANDLE ) )
		{
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'T';

			/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );
			
			/*Delete the Event*/
			if( OSAL_s32EventDelete( event_StressListIPC[u8EventIndex].hEventName ) == OSAL_OK )
			{

				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Close_EventThread:EVENT %s Delete Successful",
					event_StressListIPC[u8EventIndex].hEventName
				);

				/*Post Randomly 200 times*/
				while( u8Count < 200 )
				{
					/*Select the Post Pattern Randomly*/
					u32PostPattern = u32SelectEventPatternRandom( );

					/*Post a Pattern to event pattern a random index*/
					if( 
							OSAL_OK ==
							OSAL_s32EventPost
							( 
								event_StressListIPC[u8EventIndex].hEve,
								u32PostPattern,
								OSAL_EN_EVENTMASK_OR 
							)
							
						)			

					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Successful",
							event_StressListIPC[u8EventIndex].hEventName
						);
							
					}
				   else
					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Failed",
							event_StressListIPC[u8EventIndex].hEventName
						);
					}


					/*Increment the counter*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Delete the event resources*/
				if( OSAL_OK == OSAL_s32EventClose( event_StressListIPC[u8EventIndex].hEve ))
				{
					
					
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Close Failed",
							event_StressListIPC[u8EventIndex].hEventName
						);
				
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s Close Failed",
						event_StressListIPC[u8EventIndex].hEventName
					);
				/*Invalidate the event handle for reuse*/
				event_StressListIPC[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;

					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s handle invalid",
						event_StressListIPC[u8EventIndex].hEventName
					);

				}

			 }

			 /*Update the to be deleted field in Shared memory*/
			 *( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		}/*Handle Valid*/
		else
		{
		   OSAL_s32SemaphorePost( baseSemHandleIPC );
		}	
	}/*Loop Infinitely*/
}

/*****************************************************************************
* FUNCTION    :	   Create_Thread_Eve()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Create an Event field at Random Event indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Eve( tVoid *ptr )
{
	 /*Declarations*/
	 tU8 u8Index;

	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	 for(;;)   
   	 {
        /*Randomly modify the priority of the thread*/
		vSelectThreadPriorityRandom( );

		/*Select event index randomly*/
      	u8Index = u8SelectIndexRandom();

      	/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (event_StressListIPC[u8Index].hEve == OSAL_C_INVALID_HANDLE)
      	{
         	/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

         	/*Create the event*/
         	if( 
         			OSAL_OK == 
         			OSAL_s32EventCreate
         			( 
         				event_StressListIPC[u8Index].hEventName,
         				&event_StressListIPC[u8Index].hEve
         			)
					)

				{
					
			 		OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Created Successful",
						event_StressListIPC[u8Index].hEventName
					);
						
				}
			   else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Create Failed",
						event_StressListIPC[u8Index].hEventName
					);
				}


							
      	}
		else
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );
		}

		/*Wait randomly*/
      	vWaitRandom( FIVE_SECONDS );
     }
}
/*EVENT SPECIFIC*/

/*MESSAGE QUEUE SPECIFIC*/
/*****************************************************************************
* FUNCTION    :	   u8SelectMessagePriority()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Priority for a message to 
  				   be sent to a Message Queue.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
				   Updated By Tinoy Mathews( RBIN/ECM1 ) 20 Dec,2007
*******************************************************************************/
static tU32 u32SelectMessagePriority( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = RAND_MAX;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Return the Message Priority*/
	return (tU32)( s32Rand%((tS32)MQ_PRIO7+1) );
}
/******************************************************************************
* FUNCTION    :	   Post_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Message Thread 
				   - To continually post Message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Post_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tU8 u8Index             = 0xFF;
	tU32 u32MessagePriority = 0xFFFFFFFF;
	tChar aBuf[MAX_LEN]		= "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	
	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen( 
								  		mesgque_StressListIPC[u8Index].coszName,
								  		mesgque_StressListIPC[u8Index].enAccess,
								  		&mesgque_StressListIPC[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueuePost( mesgque_StressListIPC[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );

			/*Close the Message Queue*/
			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				vWaitRandom( FIVE_SECONDS );
			}
			else
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/
}

/******************************************************************************
* FUNCTION    :	   Wait_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Message Thread 
				   - To continually Wait for 
				   message at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Wait_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tChar aBuf[MAX_LEN];
	tU8 u8Index;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
	   /*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Open the message queue*/
		if( OSAL_s32MessageQueueOpen( 
								  		mesgque_StressListIPC[u8Index].coszName,
								  		mesgque_StressListIPC[u8Index].enAccess,
								  		&mesgque_StressListIPC[u8Index].phMQ
									) == OSAL_OK )
		{
			/*Post the message into the message queue*/
			OSAL_s32MessageQueueWait( mesgque_StressListIPC[u8Index].phMQ,
									  (tPU8)aBuf,
									  MAX_LEN,
									  OSAL_NULL,
									  OSAL_C_TIMEOUT_FOREVER );

			/*Close the Message Queue*/
			if( u8SelectIndexRandom( ) < MAX_OSAL_RESOURCES/2 )
			{
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				vWaitRandom( FIVE_SECONDS );
			}
			else
			{
				vWaitRandom( FIVE_SECONDS );
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
			}
		}/*Success of Open Message Queue*/
	}/*Loop Infinitely*/	
}

/******************************************************************************
* FUNCTION    :	   Close_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Close Message Thread 
				   - To continually Close  
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Close_Thread_Msg( tVoid *ptr )
{
	/*Declarations*/
	tU8 u8Index;
	tU8 u8Count 			 = 0;
	tU8 u8Offset 			 = 0;
	tChar aBuf[MAX_LEN]		 = "aBc1#$%tYuHjkLpA!FG";/*20 bytes*/
	tU32  u32MessagePriority = 0xFFFFFFFF;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

		/*Calculate offset within Shared Memory*/
		u8Offset = (tU8)(u8Index*3);

		/*Select Priority for the Message to be Send to the Selected Message Queue*/
		u32MessagePriority = u32SelectMessagePriority( );

		/*Sync*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

		if( (  *( shptr_osalrsc + ( u8Offset + 2 ) ) == 'F' ) && ( mesgque_StressListIPC[u8Index].phMQ != OSAL_C_INVALID_HANDLE ) )
		{
			/*Update the to be deleted field in Shared memory*/
			*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'T';

			/*Sync*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

			/*Delete the message queue*/
			if( OSAL_s32MessageQueueDelete( mesgque_StressListIPC[u8Index].coszName ) == OSAL_OK )
			{
				/*Random Delay*/
				vWaitRandom( FIVE_SECONDS );

				/*Fire 100 posts*/
				while( u8Count < 100 )
				{
					/*Post the message into the message queue*/
					OSAL_s32MessageQueuePost( mesgque_StressListIPC[u8Index].phMQ,(tPCU8)aBuf,MAX_LEN,u32MessagePriority );	
					u8Count++;
				}

				/*Reset the count*/
				u8Count = 0;

				/*Delete the message Queue*/
				OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ );
				
				/*Reset the handle at the index*/
				mesgque_StressListIPC[u8Index].phMQ = OSAL_C_INVALID_HANDLE;
			}

			/*Update the to be deleted field in Shared memory*/
			 *( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';
		}
		else
		{
		   OSAL_s32SemaphorePost( baseSemHandleIPC );
		}	
	}		
}

/******************************************************************************
* FUNCTION    :	   Create_Thread_Msg()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Create Message Thread 
				   - To continually Create 
				   message queues at Random Message Queue Indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
static tVoid Create_Thread_Msg( tVoid *ptr )
{
	 
	 /*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 for(;;)   
   	 {
        /*Change Thread Priority Randomly*/
		vSelectThreadPriorityRandom( );

		/*Select the Index of the Message Queue*/
		u8Index = u8SelectIndexRandom( );

      	/*Sync*/
		OSAL_s32SemaphoreWait( baseSemHandleIPC, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (mesgque_StressListIPC[u8Index].phMQ == OSAL_C_INVALID_HANDLE)
      	{
         	/*Sync*/
			OSAL_s32SemaphorePost( baseSemHandleIPC );

         	/*Create the Message Queue*/
         	OSAL_s32MessageQueueCreate(
										mesgque_StressListIPC[u8Index].coszName,
										mesgque_StressListIPC[u8Index].u32MaxMessages,
										mesgque_StressListIPC[u8Index].u32MaxLength,
										mesgque_StressListIPC[u8Index].enAccess,
										&mesgque_StressListIPC[u8Index].phMQ	
									  );
      	}
		/*Random Delay*/
		vWaitRandom( FIVE_SECONDS );
     }
}
/*MESSAGE QUEUE SPECIFIC*/

/*GENERAL PORTAL FUNCTION*/
/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute 
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Portal for Thread Spawn			       	
* HISTORY     :	   Ported from the file "oedt_osal_semaphore_stress.c" 
				   DI_ARION_PSW_7.6V01, 
				   /di_osal/nucleus/nucleus_osal/osal_dev_test 
				   - by Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
static tVoid vEntryFunction( 
					  		void (*ThreadEntry) (void *),
					  		const tC8 *threadName,
                      		tU8 u8NoOfThreads, 
                      		OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[] 
                    )
{
	/*Definitions*/
	tU8 u8Index                                         = 0;
	tChar acBufThreadName[TOT_THREADS][THREAD_NAME_LEN];

	OSAL_pvMemorySet( acBufThreadName,0,sizeof(acBufThreadName) );
	
	/*Spawn the required number of tasks*/
	while( u8Index < u8NoOfThreads )
	{
		/*Fill in the Thread Name*/
		OSAL_s32PrintFormat( acBufThreadName[u32GlobCounterIPC],"%s_%d",threadName,u8Index );

		/*Fill in the Thread Attribute structure*/
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;/*2 KB Minimum*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pfEntry      = ThreadEntry;
		tr_Attr[u8Index].szName       =	acBufThreadName[u32GlobCounterIPC];

		/*Spawn*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			//Do nothing
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobCounterIPC++;
	}
}
/*GENERAL PORTAL FUNCTION*/

/*SEMAPHORE STRESS TEST*/
/*****************************************************************************
* FUNCTION    :	   u32SemaphoreStressTestIPC()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_016

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elapsed Time.
				   2). Create a Base Semaphore( Required for
				   	   synchronization for deleting the semaphore
				   	   resource across two Processes ).
				   2). Create 25 Semaphores in the system.
				   3). Create 10 Threads which does Post operation 
				       on 25 Semaphores Randomly for the Process
					   ProcOEDT_PF
				   4). Create 15 Threads which does Wait operation 
				       on 25 Semaphores Randomly for the Process
					   ProcOEDT_PF
				   5). Create 1 Thread which does Delete and Close 
				       operation on 25 Semaphores Randomly for the 
					   Process ProcOEDT_PF
				   6). Create 2 Threads which does a Re- Create on 
				       25 Semaphores Randomly,if deleted earlier, for
					   the Process ProcOEDT_PF
				   7). Do shutdown activities( Thread Shutdown,Semaphore
				       shutdown )
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Semaphore 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Semaphore
				   API Interface is Robust at the IPC level.Timeout Time can be 
				   reconfigured in the file "oedt_osalcore_TestFuncs.h" under the define 
				   STRESS_SEMAPHORE_DURATION.Whether this case should be run 
				   can be configured in the same file as part of the define
				   STRESS_SEMAPHORE.Target must be essentialy restarted to
				   actually terminate this case.
					   			       	
* HISTORY     :	   Ported from the file "oedt_osalcore_SM_TestFuncs.c" 
				   - by Tinoy Mathews( RBIN/ECM1 ) 20 Feb ,2008
*******************************************************************************/
tU32 u32SemaphoreStressTestIPC( tVoid )	
{
	/*Definitions*/
	/*Seed*/
	OSAL_tMSecond elapsedTime     = 0;
	/*Array index*/
	tU8 u8Index                   = 0;
	/*Keeps track of Post thread count*/
	tU8 u8PostThreadCounter       = 0;
	/*Keeps track of Wait thread count*/
	tU8 u8WaitThreadCounter       = 0;
	/*Keeps track of Close thread count*/
	tU8 u8CloseThreadCounter      = 0;
	/*Keeps track of Create thread count*/
	tU8 u8CreateThreadCounter     = 0;
	/*Offset within Shared Memory from Base*/
	tU8 u8Offset                  = 0;
	/*Return code*/
	tU32 u32Ret                   = 0;
	/*Modifier to Semaphore names to be created*/
	tChar tcIndex                 = 'A';
	/*Temp buffer to store Semaphore names*/
	tChar acBuf[MAX_LENGTH]       = {"\0"};
	gevMask = 0;
	/*Get Base for the seed*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Initialise a random seed - Subsequent returns from 
	OSAL_vRandomSeed will be influenced by the seed value*/
	OSAL_vRandomSeed( (tU32)elapsedTime%SRAND_MAX );

	/*Create the Base Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "BASE_SEMAPHORE",&baseSemHandleIPC, 1 ) )
	{
		/*Return immediately*/
		return 60000;
	}

	/*Create this Shared Memory to linearly store
	: Created Semaphore Names
	: To be deleted or to be not deleted status field for the created semaphore*/

	if( OSAL_ERROR == ( shHandleIPC = OSAL_SharedMemoryCreate( 
															"SH_OSALRESOURCE_IPC",
															 OSAL_EN_READWRITE,
															 SH_OSALRESOURCE_SIZE
										 				     ) ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Return immediately*/
		return 50000;
	}
	else
	{
   		/*Get the base address of the OSAL Resource Shared memory*/
		shptr_osalrsc = ( tU8* )OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

		/*Initialize the Shared memory*/
		OSAL_pvMemorySet( shptr_osalrsc,0,SH_OSALRESOURCE_SIZE );

	}

	/*Create all the MAX_OSAL_RESOURCES semaphores*/
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		/*Compute offset in shared memory*/
		u8Offset = (tU8)(u8Index*3);
		
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"S%c",tcIndex );

		/*Copy the semaphore name to Semaphore Table List*/
		(tVoid)OSAL_szStringCopy( sem_StressListIPC[u8Index].hSemName,acBuf );

		/*Shared Memory memory map:
  		Assume that the OSAL Resource is a Semaphore
  		Semaphore Resource Name will start with letter 'S', followed by an Alphabet
  		So We can have Semaphore Resource names as "SA","SB","SC" and so on
  		Extensions of this map can be names like "SAA","SAB","SAC" and so on
  		But as of now since only 25 osal resources will be managed by a Process
	    across threads, 2 bytes will suffice

  		Semaphore Resource Status can take either 
  		'T'( In case resource is to be deleted by a Process )
  		'F'( In case resource is not yet to be deleted by a Process )
		The purpose of a Semaphore Resource Status byte is that
		it acts like a pure binary semaphore in context of threads
		running at different process context.....
  		
		Overall, the custom process will read the shared memory as:
		SA(T/F)SB(T/F)SC(T/F)...............SW(T/F)
  		*/

		/*Update the name in shared memory*/
		*( shptr_osalrsc + u8Offset )  		  = *( acBuf + 0 );
		*( shptr_osalrsc + ( u8Offset+1 ) )   = *( acBuf + 1 );
		
		/*Create the Semaphore*/
		OSAL_s32SemaphoreCreate( sem_StressListIPC[u8Index].hSemName,&sem_StressListIPC[u8Index].hSem,0 );

		/*Update the status of the semaphore resource in shared memory*/
		*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

		/*Increment Counter*/
		u8Index++;
		/*Increment Character Index*/
		tcIndex++;
	}

	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST15 ) )
	{
		
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the IPC Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Semaphores*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
			OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );
			OSAL_s32SemaphoreDelete( sem_StressListIPC[u8Index].hSemName );	

			/*Increment Index*/
			u8Index++;
		}

		/*Return with failure*/
		return 40000;
	}
			
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the IPC Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Semaphores*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
			OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );
			OSAL_s32SemaphoreDelete( sem_StressListIPC[u8Index].hSemName );	

			/*Increment Index*/
			u8Index++;
		}

		return 30000;
	}
	
	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );	   
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );	   
	/*Invoke the Post threads*/
	vEntryFunction( Post_Thread_Sem,"Post_Thread_Sem",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Invoke the Wait threads*/
	vEntryFunction( Wait_Thread_Sem,"Wait_Thread_Sem",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Invoke the Close thread*/
	vEntryFunction( Close_Thread_Sem,"Close_Thread_Sem",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Invoke the Create threads*/
	vEntryFunction( Create_Thread_Sem,"Create_Thread_Sem",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

   	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
		 
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
	/*Reset the Global Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment Counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment Counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment Counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment Counter*/
		u8CreateThreadCounter++;
	}

	/*Shutdown the Base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );
	OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

	
	/*Shutdown the IPC Shared Memory*/
	OSAL_s32SharedMemoryClose( shHandleIPC );
	OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );

	

	/*Try to Shutdown 25 Semaphores*/
	u8Index = 0;
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		OSAL_s32SemaphoreClose( sem_StressListIPC[u8Index].hSem );
		OSAL_s32SemaphoreDelete( sem_StressListIPC[u8Index].hSemName );	

		/*Increment Index*/
		u8Index++;
	}


	/*Return the error code*/
	return u32Ret;
}
/*SEMAPHORE STRESS TEST*/

/*EVENT STRESS TEST*/
/*****************************************************************************
* FUNCTION    :	   u32EventStressTestIPC()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_017

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elasped Time.
				   2). Create 25 Event Fields in the Process 
				   	   ProcOEDT_PF
				   3). Create 10 Threads which does Post operation 
				       on 25 Events Randomly in Process ProcOEDT_PF
				   4). Create 15 Threads which does Wait operation 
				       on 25 Events Randomly in Process ProcOEDT_PF
				   5). Create 1 Thread which does Delete and Close 
				       operation on 25 Events Randomly in Process 
				       ProcOEDT_PF
				   6). Create 2 Threads which does a Re- Create on 
				       25 Events Randomly,if deleted earlier
					   in Process ProcOEDT_PF
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete all the 28 Threads.
				   9). Return success.
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Event 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Event
				   API Interface is Robust at the IPC pov.Timeout Time can be 
				   reconfigured in the file "oedt_osalcore_TestFuncs.h" 
				   under the define STRESS_EVENT_DURATION.Whether this case 
				   should be run can be configured in the same file as part 
				   of the define STRESS_EVENT.
					   			       	
* HISTORY     :	  Created By Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tU32 u32EventStressTestIPC( tVoid )
{
	/*Definitions*/
	/*Seed*/
	OSAL_tMSecond elapsedTime 	  = 0;
	/*Array index*/
	tU8 u8Index               	  = 0;
	/*Count number of Post Threads*/
	tU8 u8PostThreadCounter   	  = 0;
	/*Count number of Wait Threads*/
	tU8 u8WaitThreadCounter   	  = 0;
	/*Count number of Close Threads*/
	tU8 u8CloseThreadCounter  	  = 0;
	/*Count number of Create Threads*/
	tU8 u8CreateThreadCounter 	  = 0;
	/*Modifier to Semaphore names to be created*/
	tChar tcIndex             	  = 'A';
	/*Offset within Shared Memory from Base*/
	tU8 u8Offset                  = 0;
   	/*Temp Buffer*/
	tChar acBuf[MAX_LENGTH]   	  = {"\0"};
	gevMask = 0;

	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "BASE_SEMAPHORE",&baseSemHandleIPC, 1 ) )
	{
		/*Return immediately*/
		return 60000;
	}

	/*Create this Shared Memory to linearly store
	: Created Event Names
	: To be deleted or to be not deleted status field for the created event*/

	if( OSAL_ERROR == ( shHandleIPC = OSAL_SharedMemoryCreate( 
															"SH_OSALRESOURCE_IPC",
															 OSAL_EN_READWRITE,
															 SH_OSALRESOURCE_SIZE
										 				     ) ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Return immediately*/
		return 50000;
	}
	else
	{
   		/*Get the base address of the OSAL Resource Shared memory*/
		shptr_osalrsc = ( tU8* )OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

		/*Initialize the Shared memory*/
		OSAL_pvMemorySet( shptr_osalrsc,0,SH_OSALRESOURCE_SIZE );

	}

	/*Create MAX_OSAL_RESOURCES number of events in the system*/
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		/*Compute offset in shared memory*/
		u8Offset = (tU8)(u8Index*3);
		
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"E%c",tcIndex );

		/*Copy the event name to Event Table List*/
		(tVoid)OSAL_szStringCopy( event_StressListIPC[u8Index].hEventName,acBuf );

		/*Shared Memory memory map:
  		Assume that the OSAL Resource is an Event
  		Event Resource Name will start with letter 'E', followed by an Alphabet
  		So We can have Event Resource names as "EA","EB","EC" and so on
  		Extensions of this map can be names like "EAA","EAB","EAC" and so on
  		But as of now since only 25 osal resources will be managed by a Process
	    across threads, 2 bytes will suffice

  		Event Resource Status can take either 
  		'T'( In case resource is to be deleted by a Process )
  		'F'( In case resource is not yet to be deleted by a Process )
		The purpose of a Event Resource Status byte is that
		it acts like a pure binary semaphore in context of threads
		running at different process context.....
  		
		Overall, the custom process will read the shared memory as:
		EA(T/F)EB(T/F)EC(T/F)...............EW(T/F)
  		*/

		/*Update the name in shared memory*/
		*( shptr_osalrsc + u8Offset )  		  = *( acBuf + 0 );
		*( shptr_osalrsc + ( u8Offset+1 ) )   = *( acBuf + 1 );
		
		/*Create the Event*/
		OSAL_s32EventCreate( event_StressListIPC[u8Index].hEventName,&event_StressListIPC[u8Index].hEve );

		/*Update the status of the event resource in shared memory*/
		*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

		/*Increment Counter*/
		u8Index++;
		/*Increment Character Index*/
		tcIndex++;
	}

	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST16 ) )
	{
		
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the Event name Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Events*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
		    OSAL_s32EventClose( event_StressListIPC[u8Index].hEve );
			OSAL_s32EventDelete( event_StressListIPC[u8Index].hEventName );	

			/*Increment Index*/
			u8Index++;
		}

		/*Return with failure*/
		return 40000;
	}
			
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the IPC Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Events*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
		    OSAL_s32EventClose( event_StressListIPC[u8Index].hEve );
			OSAL_s32EventDelete( event_StressListIPC[u8Index].hEventName );	

			/*Increment Index*/
			u8Index++;
		}

		return 30000;
	}


	   /*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );	
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
					 );	
	/*Start all Post Threads*/
	vEntryFunction( Post_Thread_Eve,"Post_Ev_Thread",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_Thread_Eve,"Wait_Ev_Thread",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Start the Close Thread*/
	vEntryFunction( Close_Thread_Eve,"Close_Ev_Thread",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Start all Create Threads*/
	vEntryFunction( Create_Thread_Eve,"Create_Ev_Thread",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

		/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );
   
	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );

	/*Reset the Global Thread Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}
		
	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}
	

	/*Shutdown the Base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );
	OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

   
	/*Shutdown the IPC Shared Memory*/
	OSAL_s32SharedMemoryClose( shHandleIPC );
	OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );


	/*Try to Shutdown 25 Events*/
	u8Index = 0;
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		OSAL_s32EventClose( event_StressListIPC[u8Index].hEve );
		OSAL_s32EventDelete( event_StressListIPC[u8Index].hEventName );	

		/*Increment Index*/
		u8Index++;
	}

	/*Return error code - Success if control reaches here*/
	return 0;
	
}
/*EVENT STRESS TEST*/

/*MESSAGE QUEUE STRESS TEST*/
/*****************************************************************************
* FUNCTION    :	   u32MessageQueueStressTestIPC()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_IPC_018

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elasped Time.
				   2). Create 25 Message Queues in the Process 
				   	   ProcOEDT_PF
				   3). Create 10 Threads which does Post operation 
				       on 25 Message Queues Randomly in Process ProcOEDT_PF
				   4). Create 15 Threads which does Wait operation 
				       on 25 Message Queues Randomly in Process ProcOEDT_PF
				   5). Create 1 Thread which does Delete and Close 
				       operation on 25 Message Queues Randomly in Process 
				       ProcOEDT_PF
				   6). Create 2 Threads which does a Re- Create on 
				       25 Message Queues Randomly,if deleted earlier
					   in Process ProcOEDT_PF
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete the Allocated OSAL Resources( Semaphores,Shared
					   Memory,Message Queues,Threads )
				   9). Return success.
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Message Queue 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Message 
				   Queue API Interface is Robust at the IPC Point of View.Timeout 
				   Time can be reconfigured in the file "oedt_osalcore_TestFuncs.h" 
				   under the define STRESS_MQ_DURATION.Whether this case 
				   should be run can be configured in the same file as part 
				   of the define STRESS_MQ.
					   			       	
* HISTORY     :	  Created By Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tU32 u32MessageQueueStressTestIPC( tVoid )
{
	/*Definitions*/
	/*Seed*/
	OSAL_tMSecond elapsedTime 	  = 0;
	/*Array index*/
	tU8 u8Index               	  = 0;
	/*Count number of Post Threads*/
	tU8 u8PostThreadCounter   	  = 0;
	/*Count number of Wait Threads*/
	tU8 u8WaitThreadCounter   	  = 0;
	/*Count number of Close Threads*/
	tU8 u8CloseThreadCounter  	  = 0;
	/*Count number of Create Threads*/
	tU8 u8CreateThreadCounter 	  = 0;
	/*Modifier to Message Queue names to be created*/
	tChar tcIndex             	  = 'A';
	/*Offset within Shared Memory from Base*/
	tU8 u8Offset                  = 0;
	/*Temp Buffer*/
	tChar acBuf[MAX_LENGTH]   	  = {"\0"};

	gevMask = 0;
	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( "BASE_SEMAPHORE",&baseSemHandleIPC, 1 ) )
	{
		/*Return immediately*/
		return 60000;
	}

	/*Create this Shared Memory to linearly store
	: Created Message Queue Names
	: To be deleted or to be not deleted status field for the created 
	  Message Queue*/

	if( OSAL_ERROR == ( shHandleIPC = OSAL_SharedMemoryCreate( 
															"SH_OSALRESOURCE_IPC",
															 OSAL_EN_READWRITE,
															 SH_OSALRESOURCE_SIZE
										 				     ) ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Return immediately*/
		return 50000;
	}
	else
	{
   		/*Get the base address of the OSAL Resource Shared memory*/
		shptr_osalrsc = ( tU8* )OSAL_pvSharedMemoryMap( shHandleIPC,OSAL_EN_READWRITE,0,0 );

		/*Initialize the Shared memory*/
		OSAL_pvMemorySet( shptr_osalrsc,0,SH_OSALRESOURCE_SIZE );

	}


	/*Create MAX_OSAL_RESOURCES number of Message Queues in the system*/
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		/*Compute offset in shared memory*/
		u8Offset = (tU8)(u8Index*3);
		
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"M%c",tcIndex );

		/*Copy the Message Queue name to Message Queue Table List field for name*/
		(tVoid)OSAL_szStringCopy( mesgque_StressListIPC[u8Index].coszName,acBuf );

		/*Shared Memory memory map:
  		Assume that the OSAL Resource is a Message Queue
  		Message Queue Resource Name will start with letter 'M', followed by an Alphabet
  		So We can have Message Queue Resource names as "MA","MB","MC" and so on
  		
  		Message Queue Resource Status can take either 
  		'T'( In case resource is to be deleted by a Process )
  		'F'( In case resource is not yet to be deleted by a Process )
		The purpose of a Message Queue Resource Status byte is that
		it acts like a pure binary semaphore in context of threads
		running at different process context.....
  		
		Overall, the custom process will read the shared memory as:
		MA(T/F)MB(T/F)MC(T/F)...............MW(T/F)
  		*/

		/*Update the name in shared memory*/
		*( shptr_osalrsc + u8Offset )  		  = *( acBuf + 0 );
		*( shptr_osalrsc + ( u8Offset+1 ) )   = *( acBuf + 1 );
		
		/*Fill in the length of a message unit*/
		mesgque_StressListIPC[u8Index].u32MaxLength   = MAX_LEN;
		/*Fill in the number of message units in the message queue*/
		mesgque_StressListIPC[u8Index].u32MaxMessages = MAX_NO_MESSAGES;
		/*Set the access mode to the message queue*/
		mesgque_StressListIPC[u8Index].enAccess       = OSAL_EN_READWRITE;

		/*Create a Message Queue within the system*/
		OSAL_s32MessageQueueCreate(
									mesgque_StressListIPC[u8Index].coszName,
									mesgque_StressListIPC[u8Index].u32MaxMessages,
									mesgque_StressListIPC[u8Index].u32MaxLength,
									mesgque_StressListIPC[u8Index].enAccess,
									&mesgque_StressListIPC[u8Index].phMQ	
								  );

		/*Update the status of the Message Queue resource in shared memory*/
		*( shptr_osalrsc + ( u8Offset + 2 ) ) = 'F';

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

		/*Increment Counter*/
		u8Index++;
		/*Increment Character Index*/
		tcIndex++;
	}

	/*Initialize all the required common Data Structures*/
	if( u32StartInit( FEATURE_TEST17 ) )
	{
		
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the IPC Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Message Queues*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
		    OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ);
			OSAL_s32MessageQueueDelete( mesgque_StressListIPC[u8Index].coszName );	

			/*Increment Index*/
			u8Index++;
		}

		/*Return with failure*/
		return 40000;
	}
			
	/*Start Process ProcOEDT_PF_Test*/
	if( u32StartProcess( ) )
	{
		/*Shutdown the Base semaphore*/
		OSAL_s32SemaphoreClose( baseSemHandleIPC );
		OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

		/*Shutdown the IPC Shared Memory*/
		OSAL_s32SharedMemoryClose( shHandleIPC );
		OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );
	
	  
		/*Shutdown 25 Message Queues*/
		while( u8Index<MAX_OSAL_RESOURCES )
		{
		    OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ);
			OSAL_s32MessageQueueDelete( mesgque_StressListIPC[u8Index].coszName );	

			/*Increment Index*/
			u8Index++;
		}

		return 30000;
	}
	
	/*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT_INIT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );

	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
	/*Start all Post Threads*/
	vEntryFunction( Post_Thread_Msg,"Post_Msg_Thread",NO_OF_POST_THREADS,post_ThreadAttrIPC,post_ThreadIDIPC );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_Thread_Msg,"Wait_Msg_Thread",NO_OF_WAIT_THREADS,wait_ThreadAttrIPC,wait_ThreadIDIPC );
	/*Start the Close Thread*/
	vEntryFunction( Close_Thread_Msg,"Close_Msg_Thread",NO_OF_CLOSE_THREADS,close_ThreadAttrIPC,close_ThreadIDIPC );
	/*Start all Create Threads*/
	vEntryFunction( Create_Thread_Msg,"Create_Mag_Thread",NO_OF_CREATE_THREADS,create_ThreadAttrIPC,create_ThreadIDIPC );

	   /*Wait for Response from the server and then proceed*/
	OSAL_s32EventWait( 
						eveIPC,
						SERVER_TO_CLIENT,
						OSAL_EN_EVENTMASK_AND,
						OSAL_C_TIMEOUT_FOREVER, 
						&gevMask
					 );


	/*Clear the event*/
	OSAL_s32EventPost
	( 
	eveIPC,
	~(gevMask),
   OSAL_EN_EVENTMASK_AND
    );
	/*Reset the Global Thread Counter*/
	u32GlobCounterIPC = 0;

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( post_ThreadIDIPC[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( wait_ThreadIDIPC[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( close_ThreadIDIPC[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}
		
	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( create_ThreadIDIPC[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Shutdown the Base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHandleIPC );
	OSAL_s32SemaphoreDelete( "BASE_SEMAPHORE" );

	

	/*Shutdown the IPC Shared Memory*/
	OSAL_s32SharedMemoryClose( shHandleIPC );
	OSAL_s32SharedMemoryDelete( "SH_OSALRESOURCE_IPC" );

	/*Try to Shutdown 25 Message Queues*/
	u8Index = 0;
	while( u8Index<MAX_OSAL_RESOURCES )
	{
		OSAL_s32MessageQueueClose( mesgque_StressListIPC[u8Index].phMQ);
		OSAL_s32MessageQueueDelete( mesgque_StressListIPC[u8Index].coszName );	

		/*Increment Index*/
		u8Index++;
	}

	/*Return error code - Success if control reaches here*/
	return 0;
	
}
/*MESSAGE QUEUE STRESS TEST*/
	
