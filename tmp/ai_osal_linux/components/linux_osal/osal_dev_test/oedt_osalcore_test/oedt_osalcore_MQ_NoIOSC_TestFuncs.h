/******************************************************************************
 *FILE         : oedt_osalcore_MQ_NoIOSC_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the MESSAGE QUEUE OSAL API.
 *               
 *AUTHOR       : Anoop chandran(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      :02.09.2011 Martin Langer (Fa. ESE, CM-AI/PJ-CF33)
 *			               copy of oedt_osalcore_MQ_TestFuncs.h
 *             :13.12.2012 added u32MQnoIOSCPostWaitMsgPriowise 
                           function prototype.
 *****************************************************************************/
/*****************************************************************
| includes: 
|   1)system- and project- includes
|   2)needed interfaces from external components
|...3)internal and external interfaces from this component
|----------------------------------------------------------------*/


#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	

#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>
#include "osal_if.h"


#ifndef OEDT_OSAL_CORE_MQ_NOIOSC_TESTFUNCS_HEADER
#define OEDT_OSAL_CORE_MQ_NOIOSC_TESTFUNCS_HEADER

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define MAX_MESG                    1
#define MAX_MESG3                    3
#define MAX_LEN                     20
#define MAX_EXC_LEN                 24
#define IOSCMESSAGEQUEUE_NAME 			"NOIOSC_Message Queue"
#define MQ_MESSAGE_EXC_LEN			"-------|-------|-------|-------|"
#ifdef OSAL_SHM_MQ
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|---"
#else
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|-"
#endif
#define MQ_MESSAGE 					"----|----|----|----|"
#define MQ_MSG						"Message Queue Data"
#define MQ_PRIO0 					0
#define MQ_PRIO1                    1
#define MQ_PRIO2 					2
#define MQ_PRIO3                    3
#define MQ_PRIO4 					4
#define MQ_PRIO5                    5
#define MQ_PRIO6 					6
#define MQ_PRIO7 					7
#define INVAL_PRIO                  20 /*>>7*/
#define MQ_BUFF_SIZE				21
#define MQ_THR_NAME_1 				"MQ_Thread_1"
#define MQ_THR_NAME_2 				"MQ_Thread_2"
#define MQ_THR_NAME_NOTIFY_1 		"MQ_Thread_Notify1"
#define MQ_THR_NAME_NOTIFY_2 		"MQ_Thread_Notify2"
#define MQ_THR_NAME_NOTIFY_3 		"MQ_Thread_Notify3"
#define MQ_EVE_NAME1				"MQ_Event1"
#define MQ_EVE_NAME2				"MQ_Event2"
#define MQ_EVE1						1
#define MQ_EVE2						2
#define MQ_EVE3						4
#define MQ_EVE						7
#define MQ_TR_STACK_SIZE			2048
#define MQ_COUNT_MAX 				5
#define MQ_HARRAY 					20
#define MQ_MESSAGE_TO_POST          "In a message queue"
#define MESSAGE_20BYTES             "ABCDEFGHIJKLMNOPQRS"
#undef  MAX_LENGTH
#define MAX_LENGTH                  256
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                    0x7FFFFFFF
#undef  SRAND_MAX
#define SRAND_MAX                   (tU32)65535
#define MAX_MESSAGE_QUEUES          20
#define MAX_NO_MESSAGES             20
#undef  TWO_SECONDS    
#define TWO_SECONDS                 2000
#undef  FIVE_SECONDS
#define FIVE_SECONDS                5000

#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS          10
#undef  NO_OF_WAIT_THREADS
#define NO_OF_WAIT_THREADS          15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS         1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS        2
#undef  TOT_THREADS
#define TOT_THREADS                 (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)
/*rav8kor - error code for MQ wait*/
#define MQ_WAIT_RETURN_ERROR	    0
/*rav8kor - disable*/
#undef  DEBUG_MODE
#define DEBUG_MODE                  0
#define RUN_TEST                    5
#define VALID_STACK_SIZE            2048

/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/

/*Test cases*/
tU32 u32MQnoIOSCPostMsgExcMaxLen( tVoid );				     //TU_OEDT_OSAL_CORE_MSGQ_001
tU32 u32MQnoIOSCCreateMsgQueWithHandleNULL( tVoid );		 //TU_OEDT_OSAL_CORE_MSGQ_002
tU32 u32MQnoIOSCCreateMsgExcMaxLen( tVoid );				 //TU_OEDT_OSAL_CORE_MSGQ_003
tU32 u32MQnoIOSCCreateDiffModes( tVoid );				     //TU_OEDT_OSAL_CORE_MSGQ_004
tU32 u32MQnoIOSCCreateSameName( tVoid );					 //TU_OEDT_OSAL_CORE_MSGQ_005
tU32 u32MQnoIOSCDeleteNameNULL( tVoid );					 //TU_OEDT_OSAL_CORE_MSGQ_006
tU32 u32MQnoIOSCDeleteNameInval( tVoid );					 //TU_OEDT_OSAL_CORE_MSGQ_007
tU32 u32MQnoIOSCDeleteWithoutClose( tVoid );				 //TU_OEDT_OSAL_CORE_MSGQ_008
tU32 u32MQnoIOSCOpenDiffMode( tVoid );					     //TU_OEDT_OSAL_CORE_MSGQ_009
tU32 u32MQnoIOSCTwoThreadMsgPost( tVoid );				     //TU_OEDT_OSAL_CORE_MSGQ_010
tU32 u32MQnoIOSCThreadMsgNotify( tVoid );					 //TU_OEDT_OSAL_CORE_MSGQ_011
tU32 u32MQnoIOSCQueryStatus( tVoid );                        //TU_OEDT_OSAL_CORE_MSGQ_012/*tny1kor*/

tU32 u32MQnoIOSCQueryStatusMMParamNULL( tVoid );			 //TU_OEDT_OSAL_CORE_MSGQ_013/*tny1kor*/
tU32 u32MQnoIOSCQueryStatusMLParamNULL( tVoid );			 //TU_OEDT_OSAL_CORE_MSGQ_014/*tny1kor*/
tU32 u32MQnoIOSCQueryStatusCMParamNULL( tVoid );			 //TU_OEDT_OSAL_CORE_MSGQ_015/*tny1kor*/   

tU32 u32MQnoIOSCQueryStatusAllParamNULL( tVoid );            //TU_OEDT_OSAL_CORE_MSGQ_016/*tny1kor*/   
tU32 u32MQnoIOSCPostMsgInvalPrio( tVoid );				     //TU_OEDT_OSAL_CORE_MSGQ_017/*tny1kor*/   
tU32 u32MQnoIOSCPostMsgBeyondQueLimit( tVoid );              //TU_OEDT_OSAL_CORE_MSGQ_018/*tny1kor*/   

tU32 u32MQnoIOSCStressTest( tVoid );                         //TU_OEDT_OSAL_CORE_MSGQ_019/*tny1kor*/
tU32 u32MQnoIOSCTimeoutCheck( tVoid );

tU32 u32MQnoIOSCSimplePostWaitPerfTest( tVoid );
tU32 u32MQnoIOSCThreadPostWaitPerfTest( tVoid );
tU32 u32MQnoIOSCOpenDoubleCloseOnce( tVoid );
tU32 u32MQnoIOSCPostWaitMsgPriowise( tVoid );              //TU_OEDT_OSAL_CORE_MSGQ_023/*sja3kor*/  

/*Test cases*/

#endif


