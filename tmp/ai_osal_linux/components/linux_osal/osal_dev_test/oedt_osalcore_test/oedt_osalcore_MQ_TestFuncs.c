/******************************************************************************
 *FILE         : oedt_osalcore_MQ_TestFuncs.c
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file implements the  test cases for the testing
 *               the MESSAGE QUEUE OSAL API.
 *               
 *AUTHOR       : Anoop Chandran (RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : 

*              Version 1.9 , 29- 15- 2012
*              Updated by : Madhu Kiran Ramachandra (RBEI/ECF5)
*              Update     : Added new case "u32MsgQueTimeoutTest"

 
 *              ver1.8   - 22-04-2009
                lint info removal
				sak9kor
 *  			ver1.7	 - 14-04-2009
                warnings removal
				rav8kor
	
 				 Version 1.6 , 29- 10- 2008
 * 				 Updated by : Anoop Chandran( RBIN/ECM1)
 *               Update     : TU_OEDT_OSAL_CORE_MSGQ_010,011
 *
 *		   	 Version 1.5 
 *			    Event clear update by  
 *				 Anoop Chandran (RBEI\ECM1) 27/09/2008
 *
 *				 Version 1.4 , 25- 030 2008
 *				 Updated by : Tinoym Mathews( RBIN/ECM1 ) 
 *				 Update     : Added cases
 *				              u32MsgQueQueryStatusMMParamNULL
 *							  u32MsgQueQueryStatusMLParamNULL
 *							  u32MsgQueQueryStatusCMParamNULL
 *
 *				 Version 1.3, 8- 1- 2008
 *				 Updated by : Tinoy Mathews( RBIN/ECM1 ) 
 *				 Update     : Added case 
 *							  u32MsgQueQueryStatusAllParamNULL,
 *				              u32MsgQuePostMsgInvalPrio,
 *							  u32MsgQuePostMsgBeyondQueLimit.
 *
 *				 Version 1.2, 17- 12- 2007
 *				 Updated by : Tinoy Mathews( RBIN/ECM1 )
 *				 Update     : Added Stress Test Case,
 *							  Modified Case 008
 *
 *				 Version 1.1 , 24- 11- 2007
 * 				 Updated by : Tinoy Mathews(RBIN/EDI3)
 *               Update     : Added new case "u32MsgQueQueryStatus"	
 *					
 *  Version 1.0 , 09- 11- 2007
 *					
 *					
 *
 *****************************************************************************/


/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "oedt_osalcore_MQ_common_TestFuncs.h"
#include "oedt_osalcore_MQ_TestFuncs.h"
#include "oedt_helper_funcs.h"

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
extern tU32 u32Tr1Ret;
extern tU32 u32Tr2Ret;
extern tU32 u32Tr1NtRet;
extern tU32 u32Tr2NtRet;
extern tU32 u32Tr3NtRet;
extern OSAL_tEventHandle MQEveHandle1;
extern OSAL_tEventHandle MQEveHandle2;
extern OSAL_tMQueueHandle MQHandle;

extern OSAL_tSemHandle baseSem;
extern MessageQueueTableEntry mesgque_StressList[MAX_MESSAGE_QUEUES];
extern tU32 u32GlobMsgCounter;
extern OSAL_trThreadAttribute msgqpost_ThreadAttr[NO_OF_POST_THREADS];
extern OSAL_trThreadAttribute msgqwait_ThreadAttr[NO_OF_WAIT_THREADS];
extern OSAL_trThreadAttribute msgqclose_ThreadAttr[NO_OF_CLOSE_THREADS];
extern OSAL_trThreadAttribute msgqcreate_ThreadAttr[NO_OF_CREATE_THREADS];
extern OSAL_tThreadID msgqpost_ThreadID[NO_OF_POST_THREADS];
extern OSAL_tThreadID msgqwait_ThreadID[NO_OF_WAIT_THREADS];
extern OSAL_tThreadID msgqclose_ThreadID[NO_OF_CLOSE_THREADS];
extern OSAL_tThreadID msgqcreate_ThreadID[NO_OF_CREATE_THREADS];
extern tU32 u32MQWaitStatus( tVoid );
extern tU32 u32MQOpenStatus( tVoid );
extern tU32 u32MQCreateStatus( tVoid );
extern tU32 u32MQCloseStatus( tVoid );
extern tU32 u32MQDeleteStatus( tVoid );
extern tU32 u32MQPostStatus( tVoid );

extern OSAL_tEventMask    gevMask;

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!! Further each test has to be atomic (stand alone)!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/*****************************************************************************
* FUNCTION    :	   u32MsgQuePostMsgExcMaxLen()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_001

* DESCRIPTION :  
				   1). Create Message Queue.
			       2). Try Posting a message size exceeding the max size 
				       of the message queue.
				   3). If posting successful, query the error code,
				       and update the returned error code.
				   4). Close the message queue.
			       5). Delete the message queue if close message queue 
			           successful
			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32  u32MsgQuePostMsgExcMaxLen( tVoid )
{
   tU32 u32Ret                     = 0;
   tU32 u32status					= 0;
   OSAL_tMQueueHandle mqHandle     = 0;


   /*Create the Message Queue within the system*/
   if (
         OSAL_ERROR
         !=
         OSAL_s32MessageQueueCreate
         (
   			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			OSAL_EN_READWRITE,
			&mqHandle
		 )
	  )
   {
      /*Post a message into the message queue exceeding the defined
   	  maximum size for the message queue*/
	  if (
	        OSAL_ERROR
	        ==
	        OSAL_s32MessageQueuePost
	        (
	  		 mqHandle,
			 (tPCU8)MQ_MESSAGE_EXC_LEN,
			 MAX_EXC_LEN,
			 MQ_PRIO2
			)
		 )
	  {
		 /*Failure of Post - returned success for a length greater than
		 the message queue's limit*/
		 u32status =  OSAL_u32ErrorCode();
		 switch( u32status )
		 {
		    case OSAL_E_MSGTOOLONG:
			   u32Ret = 0;
			   break;
			case OSAL_E_QUEUEFULL:
			   u32Ret = 10;
			   break;
			case OSAL_E_UNKNOWN:
			   u32Ret = 20;
			   break;
			case OSAL_E_NOPERMISSION:
			   u32Ret = 30;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 40;
			   break;
			case OSAL_E_BADFILEDESCRIPTOR:
			   u32Ret = 50;
			   break;
			default:
			   u32Ret = 60;
		 }//end of switch( u32status )
			/*Close the message queue*/
		 if (
			    OSAL_ERROR
			    ==
			    OSAL_s32MessageQueueClose
			    (
			       mqHandle
			    )
			)
		 {
		 	u32Ret += 70;
		 }//if (OSAL_ERROR == OSAL_s32MessageQueueClose())

		 /*Delete the message queue*/
		 if (
			   OSAL_ERROR
			   ==
			   OSAL_s32MessageQueueDelete
			   (
			      MESSAGEQUEUE_NAME
			   )
			)
		 {
			u32Ret += 80;

		 }//if (OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//if(OSAL_ERROR == OSAL_s32MessageQueuePost())

	  else
	  {
	     u32Ret  = 200;

	     /*Close the message queue*/
		 if (
		        OSAL_ERROR
			    ==
			    OSAL_s32MessageQueueClose
			    (
			 	   mqHandle
			 	)
			)
		 {
			u32Ret += 300;
		 }
		 /*Delete the message queue*/
		 if (
			   OSAL_ERROR
			   ==
			   OSAL_s32MessageQueueDelete
			   (
				  MESSAGEQUEUE_NAME
			   )
		   	)
		 {
		    u32Ret += 400;
		 }

	  }//if(OSAL_ERROR == OSAL_s32MessageQueuePost())

   }//	if (OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//	if -else(OSAL_ERROR != OSAL_s32MessageQueueCreate())


   /*Return the error code*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32CreateMsgQueWithHandleNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_002

* DESCRIPTION :
				   1). Create Message Queue with handle set to NULL
				   2). If Message queue is created, set return error code,
				       close the message queue and delete it
				   3). If Message queue is not created, check for the expected
				       error code : OSAL_E_INVALIDVALUE.
					   If the expected error code is not returned, set return error
					   code.
				   4). Return the error code

* HISTORY     :
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32  u32CreateMsgQueWithHandleNULL( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;


   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			OSAL_NULL
		 )
	  ) 
   {
	  /*Created a message queue for a NULL handle*/
	  u32Ret = 10;

	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR
	  	    ==
	  	    OSAL_s32MessageQueueClose
	  	    (
	  	       OSAL_NULL
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if (
		 	   OSAL_ERROR
		 	   ==
		 	   OSAL_s32MessageQueueDelete
		 	   (
		 	      MESSAGEQUEUE_NAME
		 	   )
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {
	     case OSAL_E_INVALIDVALUE:
		    u32Ret = 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret = 1;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret = 2;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret = 3;
			break;
		 case OSAL_E_NAMETOOLONG:
			u32Ret = 4;
			break;
		 default:
			u32Ret = 5;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32MsgQueCreateMsgExcMaxLen()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_003

* DESCRIPTION :  
*				   a) Created a message queue with a exceeding
*			    	   Max Name lenth
*				   b) Close and delete the message queue, if created.
*				   c) check the error status returned
*
* HISTORY     :
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32  u32MsgQueCreateMsgExcMaxLen( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MQ_MESSAGE_NAME_ECX,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  u32Ret = 10;
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_NAMETOOLONG:
		    u32Ret = 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret = 1;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret = 2;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret = 3;
			break;
		 case OSAL_E_INVALIDVALUE:
			u32Ret = 4;
			break;
		 default:
			u32Ret = 5;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueCreateDiffModes()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_004

* DESCRIPTION :  
*				   a) Created a message queues with Different Access
*			    	   Modes
*					   1.OSAL_EN_READWRITE
*					   2.OSAL_EN_READONLY
*					   3.OSAL_EN_WRITEONLY
*					   4.OSAL_EN_BINARY(Invalid Access Mode)
*				   b) Close and delete the message queues
*				   c) check the error status returned
*				      for OSAL_EN_BINARY(Invalid Access Mode)
*			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32  u32MsgQueCreateDiffModes( tVoid )
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret = 10;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret = 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_READONLY;

   /*Try to create the Message Queue with OSAL_EN_READONLY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 50;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 100;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret += ( 200 + u32MQCreateStatus() );
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_WRITEONLY;

   /*Try to create the Message Queue with OSAL_EN_WRITEONLY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 300;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 400;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret += ( 500 + u32MQCreateStatus() );
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   enAccess         = OSAL_EN_BINARY;

   /*Try to create the Message Queue with OSAL_EN_BINARY mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 600;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 700;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
	  /* check the error status returned */
	  u32status = OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE: 	  
		    u32Ret += 0;
			break;
	     case OSAL_E_UNKNOWN:
			u32Ret += 1000;
			break;
		 case OSAL_E_NOSPACE:
			u32Ret += 2000;
			break;
		 case OSAL_E_ALREADYEXISTS:
			u32Ret += 3000;
			break;
		 case OSAL_E_NAMETOOLONG:
			u32Ret += 4000;
			break;
		 default:
			u32Ret += 5000;
	  }//end of switch( u32status )

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}
/*****************************************************************************
* FUNCTION    :	   u32MsgQueCreateSameName()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_005

* DESCRIPTION :  
*				   a) Created a message queue with a exceeding 
*			    	   Max Name lenth
*				   b) Close and delete the message queue, if created.
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32  u32MsgQueCreateSameName( tVoid )	
{
   tU32 u32Ret 		               = 0;
   tU32 u32status				   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   

   /*Try to create the Message Queue within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  if ( 
	        OSAL_ERROR 
	        != 
	  	    /*Created a message queue with out deleting
	  	    previous*/
	        OSAL_s32MessageQueueCreate
	        (
			   MESSAGEQUEUE_NAME,
			   MAX_MESG,
			   MAX_LEN,
			   enAccess,
			   &mqHandle
			)
		 ) 
	  {
		 u32Ret = 10;

		 /*Close the message queue*/
	     if ( 
		  	   OSAL_ERROR 
		  	   == 
		  	   OSAL_s32MessageQueueClose
		  	   ( 
		  	      mqHandle 
		  	   )
		  	)
		 {
		    u32Ret += 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())( 2nd MQ)

		 else
		 {
		    /*Delete the message queue*/
			if ( 
				  OSAL_ERROR 
				  == 
				  OSAL_s32MessageQueueDelete
				  ( 
				     MESSAGEQUEUE_NAME 
				  ) 
			   )
			{
			   u32Ret += 50;
			}//end of if( OSAL_ERROR == OSAL_s32MessageQueueDelete())( 2nd MQ)

		 }//end of if-else( OSAL_ERROR == OSAL_s32MessageQueueClose())( 2nd MQ)

	  }//end of if ( OSAL_ERROR != OSAL_s32MessageQueueCreate())( 2nd MQ)
	  else
	  {
		 /* check the error status returned */
		 u32status = OSAL_u32ErrorCode();
		 switch( u32status )
		 {  
		    case OSAL_E_ALREADYEXISTS:  	  
			   u32Ret = 0;
			   break;
		    case OSAL_E_UNKNOWN:
			   u32Ret = 10;
			   break;
			case OSAL_E_NOSPACE:
			   u32Ret = 20;
			   break;
			case OSAL_E_INVALIDVALUE:
			   u32Ret = 30;
			   break;
			case OSAL_E_NAMETOOLONG:
			   u32Ret = 40;
			   break;
			default:
			   u32Ret = 50;
		 }//end of switch( u32status )

	  }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())( 2nd MQ)
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret += 20;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret += 50;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate()) 
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   /*Return error value*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueDeleteNameNULL()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_006

* DESCRIPTION :  
*				   b) delete the message queue with name NULL
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32MsgQueDeleteNameNULL( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;

   /*Delete the message queue*/
   if ( 
		 OSAL_ERROR 
		 == 
		 OSAL_s32MessageQueueDelete
		 ( 
		 	OSAL_NULL 
		 ) 
	  )
   {
	  /* check the error status returned */
	  u32status =  OSAL_u32ErrorCode();
	  switch( u32status )
	  {  
	     case OSAL_E_INVALIDVALUE:  
		    u32Ret = 0;
			break;
		 case OSAL_E_DOESNOTEXIST:
			u32Ret = 1;
			break;
		 case OSAL_E_BUSY:
			u32Ret = 2;
			break;
		 case OSAL_E_UNKNOWN:
			u32Ret = 3;
			break;
		 default:
			u32Ret = 4;
	  }//end of switch( u32status )

   }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

   else
   {
      u32Ret = 10;
   }
   return u32Ret; 
}
/*****************************************************************************
* FUNCTION    :	   u32MsgQueDeleteNameInval()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_007

* DESCRIPTION :  
*				   a) Created a message queues 
*				   b) Close and delete the message queues
*				   b) delete the message queue which already deleted
*				   c) check the error status returned 
*			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32MsgQueDeleteNameInval( tVoid )
{
   tU32 u32status = 0;
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
	  	 u32Ret = 10;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		 	u32Ret = 20;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

		 /*Delete the message queue which already deleted */
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
			u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_DOESNOTEXIST:  
				  u32Ret += 0;
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 50;
				  break;
			   case OSAL_E_BUSY:
				  u32Ret += 60;
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 70;
				  break;
			   default:
				  u32Ret += 80;
			}//end of switch( u32status )

		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())
		 else
		 {
			u32Ret += 100;
		 }

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   return u32Ret; 
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueDeleteWithoutClose()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_008

* DESCRIPTION :  
*				   a) Created a message queue. 
*				   b) Delete the message queues without close
*				   c) Deleting without a close should pass,
                      in case it fails, update the error code.
                   d) Close the Message Queue, it is supposed to
                      remove the resources as well, as a delete
                      has happened.
                   e) Try to reclose, this is expected to fail.
                   f) Return the error code. 
*			       	
* HISTORY     :	   
* 09.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
* 17.12.2007 Tinoy Mathews( RBIN/ECM1 )
*                Updated as per comments from MRK2HI
* 20.12.2007 Tinoy Mathews( RBIN/ECM1 )
*
*******************************************************************************/
tU32 u32MsgQueDeleteWithoutClose( tVoid )
{
   tU32 u32Ret = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  /*Delete the message queue without close */
	  if ( 
	   	    OSAL_ERROR 
		 	== 
		 	OSAL_s32MessageQueueDelete
		 	( 
		 	   MESSAGEQUEUE_NAME 
		 	) 
		 )
	  {
		  /*Delete should not fail*/
		  u32Ret = 50;	 
	  }
	  else
	  {
		 /*Close the message queue, and remove message queue resources*/
		 if ( 
		  	   OSAL_ERROR 
		  	   == 
		  	   OSAL_s32MessageQueueClose
		  	   ( 
		  	      mqHandle 
		  	   )
		  	)
		 {
		  	/*Message Queue Close is not supposed to fail*/
		  	u32Ret += 100;
		 }
		 else
		 {
			/*Try a reclose*/			  
			if( OSAL_OK == ( OSAL_s32MessageQueueClose( mqHandle ) ) )
			{
				/*Message Queue Reclose should not pass
				Queue was not successfully removed from the system*/
				u32Ret += 1000;
			}
		 }
      }
   }
   else
   {
      u32Ret = u32MQCreateStatus();
   }

   return u32Ret; 
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueOpenDiffMode()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_009

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 12.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
*
*******************************************************************************/
tU32 u32MsgQueOpenDiffMode( tVoid ) 
{
   tU32 u32status 				   = 0;
   tU32 u32Ret					   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   tChar MQ_Buffer[MQ_BUFF_SIZE]   = { 0 };
   
   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue */
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MAX_MESG,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
      /*Post a message into the message queue */
	  if ( 
	        OSAL_ERROR 
	        == 
	        OSAL_s32MessageQueuePost
	        ( 
	  		 mqHandle, 
			 (tPCU8)MQ_MESSAGE,
			 MAX_LEN,
			 MQ_PRIO2 
			)
		 ) 
	  {

	     u32Ret += (100 + u32MQPostStatus ()) ;
		 
	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
	  
	  /* Wait for Message */
	  if (
	  	    MQ_WAIT_RETURN_ERROR
	  	    == 
	        OSAL_s32MessageQueueWait
			(
			   mqHandle,
			   (tPU8)MQ_Buffer,
			   MAX_LEN,
			   OSAL_NULL,
			   (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING

			)
		 )
	  {
		 u32Ret += 	(200 + u32MQWaitStatus ()) ;
		 
	  }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())

	  /* Compare the message */
	  if (
	        OSAL_s32StringCompare ( MQ_MESSAGE, MQ_Buffer )
		 )
	  {
	     u32Ret += 300 ;
	     
	  }// end of if (! s32StringCompare())

	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle
	  	    )
	  	 )
	  {
	     u32Ret += 10;
		 
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	   
	  /*Open the message queue  in OSAL_EN_WRITEONLY mode*/

	  if ( 
		    OSAL_ERROR 
		    != 
		    OSAL_s32MessageQueueOpen
		    ( 
		  	   MESSAGEQUEUE_NAME, 
			   OSAL_EN_WRITEONLY,
			   &mqHandle
			)
		 ) 
	  {
		 OSAL_pvMemorySet( MQ_Buffer, '\0', MQ_BUFF_SIZE );

		 /*Post a message into the message queue */
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueuePost
			   ( 
			  	  mqHandle, 
				  (tPCU8)MQ_MESSAGE,
				  MAX_LEN,
				  MQ_PRIO2 
			   )
			) 
		 {

			u32Ret += (400 + u32MQPostStatus ()) ;
			
		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())

		/*wait has to fail as handle has only write only access*/
		 if (
			   MQ_WAIT_RETURN_ERROR
			   != 
			   OSAL_s32MessageQueueWait
			   (
			      mqHandle,
			      (tPU8)MQ_Buffer,
			      MAX_LEN,
			      OSAL_NULL,
			      (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING
			   )
			)
		 {
			u32Ret += (500 + u32MQWaitStatus ()) ;
			

		 }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 else
		 {
			u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION:  
				  u32Ret += 0;
				  
				  break;
			   case OSAL_E_QUEUEFULL:
				  u32Ret += 40;
				  
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 50;
				  
				  break;
			   case OSAL_E_MSGTOOLONG:
				  u32Ret += 60;
				  
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 70;
				  
				  break;
			   case OSAL_E_BADFILEDESCRIPTOR:
				  u32Ret += 80;
				  
				  break;
			   default:
				  u32Ret += 90;
			}//end of switch( u32status )

		 }
		 
		 /*Close the message queue*/
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueueClose
			   ( 
			  	  mqHandle
			   )
			)
		 {
		 
		  	u32Ret = 700;
			

		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueOpen())

	  else
	  {

		 u32Ret += (800 + u32MQPostStatus ()) ;
		 

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())

	  /*Open the message queue  in OSAL_EN_READONLY mode*/

	  if ( 
		    OSAL_ERROR 
		    != 
		    OSAL_s32MessageQueueOpen
		    ( 
		  	   MESSAGEQUEUE_NAME, 
			   OSAL_EN_READONLY,
			   &mqHandle
			)
		 ) 
	  {
		 OSAL_pvMemorySet( MQ_Buffer, '\0', MQ_BUFF_SIZE );

		 /*Post a message into the message queue.This has to fail as  handle only has read only access*/
		 if ( 
		       OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueuePost
			   ( 
			      mqHandle, 
				  (tPCU8)MQ_MESSAGE,
				  MAX_LEN,
				  MQ_PRIO2 
			   )
			) 
		 {

		    u32status =  OSAL_u32ErrorCode();
			switch( u32status )
			{  
			   case OSAL_E_NOPERMISSION:  
				  u32Ret += 0;
				  
				  break;
			   case OSAL_E_QUEUEFULL:
				  u32Ret += 40;
				  
				  break;
			   case OSAL_E_UNKNOWN:
				  u32Ret += 50;
				  
				  break;
			   case OSAL_E_MSGTOOLONG:
				  u32Ret += 60;
				  
				  break;
			   case OSAL_E_INVALIDVALUE:
				  u32Ret += 70;
				  
				  break;
			   case OSAL_E_BADFILEDESCRIPTOR:
				  u32Ret += 80;
				  
				  break;
			   default:
				  u32Ret += 90;
			}//end of switch( u32status )

		 }// end of if (OSAL_ERROR == OSAL_s32MessageQueuePost())
		 else
		 {
		    u32Ret +=1000;
			
		 }
		 if (
			   MQ_WAIT_RETURN_ERROR
			   == 
			   OSAL_s32MessageQueueWait
			   (
				  mqHandle,
				  (tPU8)MQ_Buffer,
				  MAX_LEN,
				  OSAL_NULL,
				  (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING

			   )
			)
		 {
			u32Ret += (2000 + u32MQWaitStatus ()) ;
			

		 }// end of if (MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait())
		 /*Close the message queue*/
		 if ( 
			   OSAL_ERROR 
			   == 
			   OSAL_s32MessageQueueClose
			   ( 
			      mqHandle
			   )
			)
		 {
		    u32Ret = 4000;
			
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
    	 
	  }
	  else
	  {

		 u32Ret += (5000 + u32MQPostStatus ()) ;
		 

	  }// end of if (OSAL_ERROR == OSAL_s32MessageQueueOpen())
		 /*Delete the message queue*/
	  
	  if ( 
		 	OSAL_ERROR 
		 	== 
		 	OSAL_s32MessageQueueDelete
		 	( 
		 	   MESSAGEQUEUE_NAME 
		 	) 
		 )
	  {
	
		 u32Ret = 6000;
		 
	  
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
	  
  
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())


   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueTwoThreadMsgPost()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_010

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 12.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tU32 u32MsgQueTwoThreadMsgPost( tVoid )
{
   tU32 u32Ret					   = 0;
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   OSAL_tThreadID Thread1ID 	   = 0;
   OSAL_tThreadID Thread2ID 	   = 0;
   OSAL_trThreadAttribute  attr1   = { 0 };
   OSAL_trThreadAttribute  attr2   = { 0 };
	gevMask = 0;
   /*Try to create the Message Queue with OSAL_EN_READWRITE mode
   within the system*/
   if ( 
         OSAL_ERROR 
         != 
		 /*Created a message queue with a exceeding 
		 Max Name lenth*/
         OSAL_s32MessageQueueCreate
         (
			MESSAGEQUEUE_NAME,
			MQ_COUNT_MAX,
			MAX_LEN,
			enAccess,
			&mqHandle
		 )
	  ) 
   {
	  if (
		    OSAL_ERROR
			==
	        OSAL_s32EventCreate
	        (
	           MQ_EVE_NAME1,
		       &MQEveHandle1
	        )
		 )
	  {
	     u32Ret = 1;   

	  }
	  /* Setting attributes for Thread 1 */

	  attr1.szName = MQ_THR_NAME_1;
	  attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr1.s32StackSize = MQ_TR_STACK_SIZE;
	  attr1.pfEntry = u32MQThread1; 
	  attr1.pvArg = OSAL_NULL;

	  /* Setting attributes for Thread 2 */
	  attr2.szName = MQ_THR_NAME_2;
	  attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	  attr2.s32StackSize = MQ_TR_STACK_SIZE;
	  attr2.pfEntry = u32MQThread2; 
	  attr2.pvArg = OSAL_NULL;
	   
	  
	  
	  /* Spawn the Thread MQ_Thread_1 */
	  Thread1ID = 
	     OSAL_ThreadSpawn(&attr1);

	  if (
	        OSAL_ERROR
	  	    ==
		    Thread1ID
	   	 )
	  {
	     u32Ret += 2;   
	  }//if (OSAL_ERROR == Thread1ID )
	  else
	  { 
		  /* Spawn the Thread MQ_Thread_2 */
		  Thread2ID = 
		     OSAL_ThreadSpawn(&attr2);

		  if (
			    OSAL_ERROR
				==
				Thread2ID
		   	 )
		  {
		     u32Ret += 3;   
		  }//if (OSAL_ERROR == Thread2ID )
	      else
		  {
		      /* Wait for an event */
			  if ( 
			  	    OSAL_ERROR 
			  	    == 
			  	    OSAL_s32EventWait
			  	    ( 
			  	       MQEveHandle1,
					   MQ_EVE1|MQ_EVE2,
			  	       OSAL_EN_EVENTMASK_AND,
			  	       OSAL_C_TIMEOUT_FOREVER ,
					   &gevMask
					)
			  	 )
			  {
				 u32Ret += 4;
			  }//end of if ( OSAL_ERROR == OSAL_s32EventWait())
			  
				/*Clear the event*/
				OSAL_s32EventPost
				( 
				MQEveHandle1,
				~(gevMask),
			   OSAL_EN_EVENTMASK_AND
			    );
#if 0			  
			  /* Delete Thread 1*/
			  if ( 
			  	    OSAL_ERROR 
			  	    == 
			  	    OSAL_s32ThreadDelete
			  	    ( 
			  	       Thread2ID 
			  	    )
			  	 )
			  {
				 /* Check the error status */
				 u32Ret += 5;
			  }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())

#endif
		  }//if-else (OSAL_ERROR == Thread2ID )
		  /* Delete Thread 2*/
#if 0
		  if ( 
		  	    OSAL_ERROR 
		  	    == 
		  	    OSAL_s32ThreadDelete
		  	    ( 
		  	       Thread1ID 
		  	    )
		  	 )
		  {
			 /* Check the error status */
			 u32Ret += 6;
		  }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
#endif
	  }//if-else (OSAL_ERROR == Thread1ID )
	  /*Close the message queue*/
	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32MessageQueueClose
	  	    ( 
	  	       mqHandle 
	  	    )
	  	 )
	  {
		 /* Check the error status */
		 u32Ret += (20 + u32MQCloseStatus ()) ;
	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
	  else
	  {
		 /*Delete the message queue*/
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32MessageQueueDelete
		 	   ( 
		 	      MESSAGEQUEUE_NAME 
		 	   ) 
		 	)
		 {
		    /* Check the error status */
		    u32Tr1NtRet += (30 + u32MQDeleteStatus ()) ;
		 }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

	  }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

	  if ( 
	  	    OSAL_ERROR 
	  	    == 
	  	    OSAL_s32EventClose
	  	    ( 
	  	       MQEveHandle1
			)
	  	 )
	  {
		 u32Ret += 30;
	  }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
	  else
	  {
		 if ( 
		 	   OSAL_ERROR 
		 	   == 
		 	   OSAL_s32EventDelete
		 	   ( 
		 	      MQ_EVE_NAME1 
		 	   ) 
		 	)
		 {
		    /* Check the error status */
		    u32Ret += 40;
		 }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())


	  }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())

   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
   else
   {
      u32Ret = u32MQCreateStatus();
   }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

   u32Ret +=
     		u32Tr1Ret
   			+
   			u32Tr2Ret;
   			  
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueSingleThreadMsgNotify()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_011

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 12.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tU32 u32MsgQueSingleThreadMsgNotify( tVoid )
{
    tU32 u32Ret                     = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
    OSAL_tThreadID Thread1ID        = 0;
    OSAL_trThreadAttribute attr1    = { 0 };
    gevMask                         = 0;

    /*Try to create the Message Queue with OSAL_EN_READWRITE mode within the system*/
    if(OSAL_ERROR
            != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle))
    { /*Created a message queue with a exceeding Max Name lenth*/
        if(OSAL_ERROR == OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
            u32Ret = 1;
        }

        /* Setting attributes for Thread 1 */
        attr1.szName = MQ_THR_NAME_NOTIFY_1;
        attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        attr1.s32StackSize = MQ_TR_STACK_SIZE;
        attr1.pfEntry = u32MQThreadNotify1;
        attr1.pvArg = OSAL_NULL;

        /* Spawn the Thread MQ_Thread_1 */
        Thread1ID = OSAL_ThreadSpawn(&attr1);

        if(OSAL_ERROR == Thread1ID)
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_ThreadSpawn() failed with error '%i'", OSAL_u32ErrorCode());
            u32Ret = 2;
        }//if (OSAL_ERROR == Thread1ID )
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG: OSAL_s32MessageQueuePost() will be executed");

            if(OSAL_ERROR
                    == OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST)
                            + 1, MQ_PRIO1 ))
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueuePost() failed with error '%i'", OSAL_u32ErrorCode());
                u32Ret += 200;
            }

            if(OSAL_ERROR
                    == OSAL_s32EventWait(MQEveHandle2, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 10000, &gevMask))
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
                u32Ret = 4;
            }//end of if ( OSAL_ERROR == OSAL_s32EventWait())

            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: OSAL_s32ThreadDelete() will be executed");
            /* Delete Thread 1*/
            if(OSAL_ERROR == OSAL_s32ThreadDelete(Thread1ID))
            {
                /* Check the error status */
                if(OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
                {
                    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32ThreadDelete() failed with error %i", OSAL_u32ErrorCode());
                    u32Ret += 7;
                }
            }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
        }//if-else (OSAL_ERROR == Thread1ID )

        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: OSAL_s32MessageQueueClose() will be executed");
        /*Close the message queue*/
        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {
            /* Check the error status */
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
            u32Ret += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
        else
        {
            /*Delete the message queue*/
            if(OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME))
            {
                /* Check the error status */
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueDelete() failed with error %i", OSAL_u32ErrorCode());
                u32Ret += (30 + u32MQDeleteStatus());
            }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: OSAL_s32EventClose() will be executed");
        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle2))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose() failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 30;
        }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: OSAL_s32EventDelete() will be executed");
            if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME2))
            {
                /* Check the error status */
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete() failed with error %i", OSAL_u32ErrorCode());
                u32Ret += 40;
            }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())
        }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())
    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
    else
    {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueCreate() failed with error %i", OSAL_u32ErrorCode());
        u32Ret = u32MQCreateStatus();
    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum in main thread is '%i'", u32Ret);
    OSAL_s32ThreadWait(1000);
    u32Ret += u32Tr1NtRet;
    return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueSingleThreadMsgNotifyCallback()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_011

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 12.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tU32 u32MsgQueSingleThreadMsgNotifyCallback( tVoid )
{
    tU32 u32Ret                     = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
    OSAL_tThreadID Thread1ID        = 0;
    OSAL_trThreadAttribute attr2    = { 0 };
    gevMask                         = 0;

    /*Try to create the Message Queue with OSAL_EN_READWRITE mode within the system*/
    if(OSAL_ERROR
            != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle))
    /*Created a message queue with a exceeding Max Name lenth*/
    {
        if(OSAL_ERROR == OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
            u32Ret = 1;
        }

        /* Setting attributes for Thread 2 */
        attr2.szName = MQ_THR_NAME_NOTIFY_2;
        attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        attr2.s32StackSize = MQ_TR_STACK_SIZE;
        attr2.pfEntry = u32MQThreadNotify2;
        attr2.pvArg = OSAL_NULL;

        /* Spawn the Thread MQ_Thread_1 */
        Thread1ID = OSAL_ThreadSpawn(&attr2);

        if(OSAL_ERROR == Thread1ID)
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_ThreadSpawn() failed with error '%i'", OSAL_u32ErrorCode());
            u32Ret = 2;
        }//if (OSAL_ERROR == Thread1ID )
        else
        {
            if(OSAL_ERROR
                    == OSAL_s32EventWait(MQEveHandle2, MQ_READY_EVE2, OSAL_EN_EVENTMASK_AND, 10000, &gevMask))
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
                u32Ret = 4;
            }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: received 'ready' event from thread #2");

            if(OSAL_ERROR
                    == OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST)
                            + 1, MQ_PRIO1 ))
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32MessageQueuePost() failed with error '%i'", OSAL_u32ErrorCode());
                u32Ret += 200;
            }
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: message for testing notification callback was sent");

            if(OSAL_ERROR
                    == OSAL_s32EventWait(MQEveHandle2, MQ_DONE_EVE2, OSAL_EN_EVENTMASK_AND, 10000, &gevMask))
            {
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
                u32Ret = 4;
            }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: event 'success' received");

            /* Delete Thread 1*/
            if(OSAL_ERROR == OSAL_s32ThreadDelete(Thread1ID))
            {
                /* Check the error status */
                if(OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
                {
                    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32ThreadDelete() failed with error %i", OSAL_u32ErrorCode());
                    u32Ret += 7;
                }
            }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: deletion of thread #2 was done");
        }//if-else (OSAL_ERROR == Thread1ID )

        /*Close the message queue*/
        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {
            /* Check the error status */
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
            u32Ret += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: message queue was closed");
            /*Delete the message queue*/
            if(OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME))
            {
                /* Check the error status */
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32MessageQueueDelete() failed with error %i", OSAL_u32ErrorCode());
                u32Ret += (30 + u32MQDeleteStatus());
            }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: message queue was deleted");
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle2))
        {
            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32EventClose() failed with error %i", OSAL_u32ErrorCode());
            u32Ret += 30;
        }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: event 'success' was closed");
            if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME2))
            {
                /* Check the error status */
                OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32EventDelete() failed with error %i", OSAL_u32ErrorCode());
                u32Ret += 40;
            }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: event 'success' was deleted");
        }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())
    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
    else
    {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: OSAL_s32MessageQueueCreate() failed with error %i", OSAL_u32ErrorCode());
        u32Ret = u32MQCreateStatus();
    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum in main thread is '%i'", u32Ret);
    u32Ret += u32Tr2NtRet;
    return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueThreadMsgNotify()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_011

* DESCRIPTION :  
*				   a)Created a message queues 
*				   b)Post a message into the message queue 
*				   c)Wait for Message
*				   d)Compare the message 
*				   e)Close the message queue
*				   f)open the message Queue in Diff mode
*				   g)Repete the above procedure		
*				   h)Delete the message queues
*			       	
* HISTORY     :	   
* 12.10.2007 Anoop Chandran (RBIN/EDI3)
*				 Initial Revision.
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tU32 u32MsgQueThreadMsgNotify( tVoid )
{
    tU32 u32Ret                     = 0;
    OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
    OSAL_tThreadID Thread1ID        = 0;
    OSAL_tThreadID Thread2ID        = 0;
    OSAL_tThreadID Thread3ID        = 0;
    OSAL_trThreadAttribute attr1    = { 0 };
    OSAL_trThreadAttribute attr2    = { 0 };
    OSAL_trThreadAttribute attr3    = { 0 };
    gevMask                         = 0;

    /*Try to create the Message Queue with OSAL_EN_READWRITE mode
     within the system*/
    if(OSAL_ERROR != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle))
    {
        if(OSAL_ERROR == OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2))
        {
            u32Ret = 1;

        }

        /* Setting attributes for Thread 1 */
        attr1.szName = MQ_THR_NAME_NOTIFY_1;
        attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        attr1.s32StackSize = MQ_TR_STACK_SIZE;
        attr1.pfEntry = u32MQThreadNotify1;
        attr1.pvArg = OSAL_NULL;

        /* Setting attributes for Thread 2 */
        attr2.szName = MQ_THR_NAME_NOTIFY_2;
        attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        attr2.s32StackSize = MQ_TR_STACK_SIZE;
        attr2.pfEntry = u32MQThreadNotify2;
        attr2.pvArg = OSAL_NULL;

        /* Setting attributes for Thread 3 */
        attr3.szName = MQ_THR_NAME_NOTIFY_3;
        attr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        attr3.s32StackSize = MQ_TR_STACK_SIZE;
        attr3.pfEntry = u32MQThreadNotify3;
        attr3.pvArg = OSAL_NULL;

        /* Spawn the Thread MQ_Thread_1 */
        Thread1ID = OSAL_ThreadSpawn(&attr1);

        if(OSAL_ERROR == Thread1ID)
        {
            u32Ret = 1;
        }//if (OSAL_ERROR == Thread1ID )
        else
        {   /* Spawn the Thread MQ_Thread_2 */
            Thread2ID = OSAL_ThreadSpawn(&attr2);

            if(OSAL_ERROR == Thread2ID)
            {
                u32Ret = 2;
            }//if (OSAL_ERROR == Thread2ID )

            else
            {
                Thread3ID = OSAL_ThreadSpawn(&attr3);

                if(OSAL_ERROR == Thread3ID)
                {
                    u32Ret = 3;
                }//if (OSAL_ERROR == Thread3ID )
                else
                {
                    if(OSAL_ERROR ==
                            OSAL_s32EventWait(MQEveHandle2, MQ_READY_EVE1 | MQ_READY_EVE2 | MQ_READY_EVE3, OSAL_EN_EVENTMASK_AND, 10000, &gevMask))
                    {
                        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR MAIN: wait for event 'ready' failed with error '%i'", OSAL_u32ErrorCode());
                        u32Ret = 4;
                    }

                    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: all threads are ready, let's post a mesage now");

                    if(OSAL_ERROR ==
                            OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST) + 1, MQ_PRIO1 ))
                    {
                        u32Ret += 200;
                    }

                    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: wait for event 'success'");
                    if(OSAL_ERROR ==
                            OSAL_s32EventWait(MQEveHandle2, MQ_DONE_EVE1 | MQ_DONE_EVE2 | MQ_DONE_EVE3, OSAL_EN_EVENTMASK_AND, 10000, &gevMask))
                    {
                        u32Ret = 4;
                    }
                    else
                    {
                        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: close message queue", u32Ret);
                    }

                    // cleanup event
                    OSAL_s32EventPost(MQEveHandle2, ~(gevMask), OSAL_EN_EVENTMASK_AND);

                    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: start deleting threads 3, 2, 1", u32Ret);

                    OSAL_s32ThreadWait(10000);

                    if(OSAL_ERROR == OSAL_s32ThreadDelete(Thread3ID))
                    {

                        if(OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
                        {
                            OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: delete thread #3 failed with error %i", OSAL_u32ErrorCode());
                            u32Ret += 5;
                        }
                    }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())

                }//if-else (OSAL_ERROR == Thread1ID )

                /* Delete Thread 2*/
                if(OSAL_ERROR == OSAL_s32ThreadDelete(Thread2ID))
                {
                    /* Check the error status */
                    if(OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
                    {
                        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: delte thread #2 failed with error %i", OSAL_u32ErrorCode());
                        u32Ret += 6;
                    }
                }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
            }//if-else (OSAL_ERROR == Thread1ID )

            /* Delete Thread 1*/
            if(OSAL_ERROR == OSAL_s32ThreadDelete(Thread1ID))
            {
                /* Check the error status */
                if(OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
                {
                    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: delte thread #1 failed with error %i", OSAL_u32ErrorCode());
                    u32Ret += 7;
                }
            }//end of if ( OSAL_ERROR == OSAL_s32ThreadDelete())
        }//if-else (OSAL_ERROR == Thread1ID )

        OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: deletion of threads finished", u32Ret);

        /*Close the message queue*/
        if(OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle))
        {
            /* Check the error status */
            u32Ret += (20 + u32MQCloseStatus());
        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose() %i", u32Ret)
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: message queue closed", u32Ret);

            /*Delete the message queue*/
            if(OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME))
            {
                /* Check the error status */
                u32Ret += (30 + u32MQDeleteStatus());
            }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueDelete())

        }//end of if ( OSAL_ERROR == OSAL_s32MessageQueueClose())

        if(OSAL_ERROR == OSAL_s32EventClose(MQEveHandle2))
        {
            u32Ret += 30;
        }//end of if ( OSAL_ERROR == OSAL_s32EventClose())
        else
        {
            OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: event 'ready' + 'success' closed");
            if(OSAL_ERROR == OSAL_s32EventDelete(MQ_EVE_NAME2))
            {
                /* Check the error status */
                u32Ret += 40;
            }//end of if ( OSAL_ERROR == OSAL_s32EventDelete())
        }//end of if-else ( OSAL_ERROR == OSAL_s32EventClose())


    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())
    else
    {
        u32Ret = u32MQCreateStatus();
    }//end of if-else ( OSAL_ERROR != OSAL_s32MessageQueueCreate())


    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum without subthreads %i", u32Ret);
    OSAL_s32ThreadWait(23000);

    u32Ret += u32Tr1NtRet + u32Tr2NtRet + u32Tr3NtRet;

    OEDT_HelperPrintf(TR_LEVEL_USER_1, "DEBUG MAIN: error sum with subthreads %i", u32Ret);

    return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueQueryStatus()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_012

* DESCRIPTION :  
*				   a)Created a message queue.
*				   b)Query Status of message queue.
*				   c)Post a message into the message queue.
				   d)Query Status of message queue.
*				   e)Wait for Message.
                   f)Query Status of message queue.
				   g)Close the message queue.
				   h)Query Status of message queue.
				   i)Delete the message queue.
				   j)Query Status of message queue( Should return 
				     OSAL_ERROR = OSAL_E_BADFILEDESCRIPTOR ).
*				   			       	
* HISTORY     :	   
* 24.11.2007 Tinoy Mathews (RBIN/EDI3)

*******************************************************************************/
tU32 u32MsgQueQueryStatus( tVoid )
{
	/*Definitions*/
	tU32 u32Ret             = 0;
	tU32 u32ErrorValue      = OSAL_E_NOERROR;
	tU32 u32MaxMessage      = 0;
	tU32 u32MaxLength       = 0;
	tU32 u32Message         = 0;
	tU8  au8Buffer[MAX_LEN]	= {0};
	tU8  u8Switch           = 0;
	OSAL_tMQueueHandle hMQ  = 0;


	/*Create a Message Queue in the system*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		/*Catch the error*/
		u32ErrorValue =	OSAL_u32ErrorCode( );

		/*Query the error*/
		switch( u32ErrorValue )
		{
			case OSAL_E_NOPERMISSION:  return 100;
			case OSAL_E_INVALIDVALUE:  return 200;
			case OSAL_E_NOSPACE:       return 300;
			case OSAL_E_ALREADYEXISTS: return 400;
			case OSAL_E_NAMETOOLONG:   return 500;
			case OSAL_E_UNKNOWN:       return 600;
			default:                   return 700;		
		}
    
    OEDT_HelperPrintf(TR_LEVEL_FATAL,"Error: OSAL_s32MessageQueueCreate() failed, ErrorCode=%i ", u32ErrorValue );	    
	}
	else
	{
		/*Query the Message Queue Status after Message Queue Create*/
		if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );	

			/*Query the error*/
			switch( u32ErrorValue )
			{
				case OSAL_E_BADFILEDESCRIPTOR:
					 u32Ret += 800;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 900;
					 break;
				default:
					 u32Ret += 1000;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Create\n" );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum number of messages possible: %u\n",
			 					 u32MaxMessage );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum size of a message possible: %u\n",
			 					 u32MaxLength );
			 OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Message after a Create : %u\n",
			 					 u32Message );
		}

		/*Post a Message into Message Box*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,
								  					OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1,
								  					MQ_PRIO2 ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			/*Query the error*/
			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 1100;
					 break;
				case OSAL_E_MSGTOOLONG:
					 u32Ret += 1200;
					 break;
				case OSAL_E_BADFILEDESCRIPTOR:
					 u32Ret += 1300;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 1400;
					 break;
				case OSAL_E_QUEUEFULL:
					 u32Ret += 1450;
					 break;
				case OSAL_E_NOPERMISSION:
				     u32Ret += 1475;
					 break;
				default:
					 u32Ret += 1500;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Set the switch parameter*/
			u8Switch = 1;
			
			/*Query Message Queue Status*/
			if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );	

				/*Query the error*/
				switch( u32ErrorValue )
				{
					case OSAL_E_BADFILEDESCRIPTOR:
						 u32Ret += 1600;
						 break;
					case OSAL_E_UNKNOWN:
						 u32Ret += 1700;
						 break;
					default:
						 u32Ret += 1800;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Post\n" );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum number of messages possible: %u\n",
			 					 u32MaxMessage );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Maximum size of a message possible: %u\n",
			 					 u32MaxLength );
			 	OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 					"Message after a Post: %u\n",
			 					 u32Message );
			}
		}

		if( u8Switch )
		{
			/*Wait for a Message in the Message Box*/
			if( MQ_WAIT_RETURN_ERROR == OSAL_s32MessageQueueWait( hMQ,au8Buffer,
		                                            	MAX_LEN,OSAL_NULL,
		                                            	OSAL_C_TIMEOUT_FOREVER ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				/*Query the error code*/
				switch( u32ErrorValue )
				{
					case OSAL_E_INVALIDVALUE:
					 	 u32Ret += 1900;
					 	 break;
					case OSAL_E_TIMEOUT:
					 	 u32Ret += 2000;
					 	 break;
					case OSAL_E_UNKNOWN:
					 	 u32Ret += 2100;
					 	 break;
					default:
					 	 u32Ret += 2200;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
				/*Query Message Queue Status*/
				if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              		  &u32MaxLength,
		                            				  		  &u32Message ) )
				{
					/*Catch the error*/
					u32ErrorValue = OSAL_u32ErrorCode( );	

					/*Query the error*/
					switch( u32ErrorValue )
					{
						case OSAL_E_BADFILEDESCRIPTOR:
						 	 u32Ret += 2300;
						 	 break;
						case OSAL_E_UNKNOWN:
						 	 u32Ret += 2400;
						 	 break;
						default:
							 u32Ret += 2500;
					}

					/*Clear the error value*/
					u32ErrorValue = OSAL_E_NOERROR;
				}
				else
				{
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,"Queue Status after Wait\n" );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Maximum number of messages possible: %u\n",
			 					 	   u32MaxMessage );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Maximum size of a message possible: %u\n",
			 					 	   u32MaxLength );
			 		OEDT_HelperPrintf( TR_LEVEL_USER_1,
			 						   "Message after a wait : %u\n",
			 					 	   u32Message );
				}/*Message Status successful*/
			}/*Wait successful*/
		}/*Switch*/

		/*Close the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueClose( hMQ ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 2600;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 2700;
					 break;
				default:
					 u32Ret += 2800;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}
		else
		{
			/*Query Message Queue Status - after close has to fail*/
			if( OSAL_ERROR == OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,
		                                              &u32MaxLength,
		                            				  &u32Message ) )
			{
				/*Catch the error*/
				u32ErrorValue = OSAL_u32ErrorCode( );

				/*Query the error*/
				switch( u32ErrorValue )
				{
					case OSAL_E_BADFILEDESCRIPTOR:
						 u32Ret += 0;
						 break;
					case OSAL_E_INVALIDVALUE:
						 u32Ret += 0;
						 break;
					default:
						 u32Ret += 3100;
				}

				/*Clear the error value*/
				u32ErrorValue = OSAL_E_NOERROR;
			}
			else
			{
			 	u32Ret += 3000;

			}
		}

		/*Delete the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
		{
			/*Catch the error*/
			u32ErrorValue = OSAL_u32ErrorCode( );

			switch( u32ErrorValue )
			{
				case OSAL_E_INVALIDVALUE:
					 u32Ret += 3200;
					 break;
				case OSAL_E_DOESNOTEXIST:
					 u32Ret += 3300;
					 break;
				case OSAL_E_UNKNOWN:
					 u32Ret += 3400;
					 break;
				default:
					 u32Ret += 3500;
			}

			/*Clear the error value*/
			u32ErrorValue = OSAL_E_NOERROR;
		}

	}

	/*Reset the switch*/
	u8Switch = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueQueryStatusMMParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_013
* DESCRIPTION :
				   Message Queue Status with Maximum number of messages(MM)
				   parameter as OSAL_NULL.

				   Reference Manual says on OSAL_s32MessageQueueStatus:
				   This function gives status informations about a message box,
				   opened before. If one of parameters (not the first) is
				   OSAL_NULL, the value of this respective parameter is not
				   returned.
				   So in the above scenario the API OSAL_s32MessageQueueStatus
				   should not fail, but pass

* HISTORY     :	   Created By Tinoy Mathews (RBIN/EDI3) 25.03.2008
*******************************************************************************/
tU32 u32MsgQueQueryStatusMMParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tPU32 pu32MM            = OSAL_NULL;
	tU32 u32MaxLength       = 0;
	tU32 u32Message         = 0;
	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													pu32MM,
		           		                                &u32MaxLength,
		            	                				&u32Message ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;

}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueQueryStatusMLParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_014
* DESCRIPTION :
				   Message Queue Status with Maximum Message Length (ML)
				   parameter as OSAL_NULL.

				   Reference Manual says on OSAL_s32MessageQueueStatus:
				   This function gives status informations about a message box,
				   opened before. If one of parameters (not the first) is
				   OSAL_NULL, the value of this respective parameter is not
				   returned.
				   So in the above scenario the API OSAL_s32MessageQueueStatus
				   should not fail, but pass

* HISTORY     :	   Created By Tinoy Mathews (RBIN/EDI3) 25.03.2008
*******************************************************************************/
tU32 u32MsgQueQueryStatusMLParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tU32 u32MaxMessage      = 0;
  	tPU32 pu32ML            = OSAL_NULL;
	tU32 u32Message         = 0;
	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													&u32MaxMessage,
		           		                                pu32ML,
		            	                				&u32Message ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueQueryStatusCMParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_015
* DESCRIPTION :
				   Message Queue Status with Current number of messages(CM)
				   parameter as OSAL_NULL.

				   Reference Manual says on OSAL_s32MessageQueueStatus:
				   This function gives status informations about a message box,
				   opened before. If one of parameters (not the first) is
				   OSAL_NULL, the value of this respective parameter is not
				   returned.
				   So in the above scenario the API OSAL_s32MessageQueueStatus
				   should not fail, but pass

* HISTORY     :	   Created By Tinoy Mathews (RBIN/EDI3) 25.03.2008
*******************************************************************************/
tU32 u32MsgQueQueryStatusCMParamNULL( tVoid )
{
	tU32  u32Ret            = 0;
	tU32 u32MaxMessage      = 0;
	tU32 u32MaxLength       = 0;
	tPU32 pu32CM            = OSAL_NULL;
    OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue in the system*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		  if( OSAL_OK != OSAL_s32MessageQueueStatus( hMQ,
	  													&u32MaxMessage,
		           		                                &u32MaxLength,
		            	                				pu32CM ) )
		  {
				u32Ret += 150;
		  }

		  /*Close the message Queue*/
		  if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		  {
				/*Delete the message queue*/
				if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
				{
					u32Ret += 140;
				}
		  }
		  else
		  {
			u32Ret += 130;
		  }
	}
	else
	{
		u32Ret += 100;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQueQueryStatusAllParamNULL()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_016
* DESCRIPTION :    Try calling OSAL_s32MessageQueueStatus with all
				   parameters as OSAL_NULL.Should return the error
				   code OSAL_E_BADFILEDESCRIPTOR.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 8 Jan,2008
*******************************************************************************/
tU32 u32MsgQueQueryStatusAllParamNULL( tVoid )
{
	/*Definitions*/
	tU32 u32Ret             = 0;


	/*Call MsgQueQueryStatus API*/
	if( OSAL_ERROR == OSAL_s32MessageQueueStatus( OSAL_NULL,OSAL_NULL,
												  OSAL_NULL,OSAL_NULL ) )
	{
		/*Failure is an expected behaviour*/
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			/*Wrong error code*/
			u32Ret += 1000;
		}
	}
	else
	{
		/*Message Queue Status cannot pass with NULL parameters*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32MsgQuePostMsgInvalPrio()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_017
* DESCRIPTION :    Try calling OSAL_s32MessageQueuePost, with invalid
				   priority value.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 8 Jan,2008
*******************************************************************************/
tU32 u32MsgQuePostMsgInvalPrio( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 			= 0;

	#if DEBUG_MODE
	tU32 u32ECode                 =	OSAL_E_NOERROR;
	#endif

	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue*/
	if( OSAL_ERROR != OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		/*Post a Message with invalid priority*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,
								  					OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1,
								  					INVAL_PRIO ) )
		{
			/*Failure is an expected behaviour*/
			if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
			{
				/*Wrong error code*/
				u32Ret += 1000;
			}
		}
		else
		{
			/*Message cannot be posted!*/
			u32Ret += 2000;
		}

		/*Close the Message Queue*/
		if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
		{
			/*Delete the Message Queue*/
			if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
			{
				u32Ret += 3000;

				#if DEBUG_MODE
				u32ECode = OSAL_u32ErrorCode( );
				#endif
			}
		}
		else
		{
			u32Ret += 4000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}

	}
	else
	{
		/*Message Queue Create failed*/
		u32Ret += 5000;
		
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Return the error code*/
	return u32Ret;

}

/*****************************************************************************
* FUNCTION    :	   u32MsgQuePostMsgBeyondQueLimit()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_018
* DESCRIPTION :    Try to Post a Message Queue beyond the limit of
                   maximum number of messages it can hold
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 13 Jan,2008
*******************************************************************************/
tU32 u32MsgQuePostMsgBeyondQueLimit( tVoid )
{
	/*Definitions*/
	tU32 u32Ret  = 0;
	tU8  u8Count = 0;

	#if DEBUG_MODE
	tU32 u32ECode           = OSAL_E_NOERROR;
	#endif

	OSAL_tMQueueHandle hMQ  = 0;

	/*Create a Message Queue*/
	if( OSAL_ERROR == OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,
												  MQ_COUNT_MAX,MAX_LEN,
	                            				  OSAL_EN_READWRITE,&hMQ ) )
	{
		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif

		/*Return Immediately*/
		return 10000;
	}

	/*Post 5 messages in the Message Queue*/
	do
	{
		/*Plus 1 to the length to accomodate the NULL string terminator*/
		if( OSAL_ERROR == OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,
								  					OSAL_u32StringLength( MESSAGE_20BYTES )+1,
								  					MQ_PRIO2 ) )
		{
			/*Update the error code*/
			u32Ret += 100;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}

		/*Increment Count*/
		u8Count++;

	}while( MQ_COUNT_MAX != u8Count );

	/*Post it one extra time - Should fail*/
	/*System Crash, Success are API failures*/
	if( OSAL_ERROR != OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,
							  					OSAL_u32StringLength( MESSAGE_20BYTES )+1,
							  					MQ_PRIO2 ) )
	{
		/*Update the error code*/
		u32Ret += 1000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Close the Message Queue and Delete*/
	if( OSAL_ERROR != OSAL_s32MessageQueueClose( hMQ ) )
	{
		/*Delete the Message Queue*/
		if( OSAL_ERROR == OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME ) )
		{
			u32Ret += 3000;

			#if DEBUG_MODE
			u32ECode = OSAL_u32ErrorCode( );
			#endif
		}
	}
	else
	{
		u32Ret += 4000;

		#if DEBUG_MODE
		u32ECode = OSAL_u32ErrorCode( );
		#endif
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32MessageQueueStressTest()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_MSGQ_019

* DESCRIPTION :    1). Set the Seed for Random function based on
                       Elasped Time.
				   2). Create 20 Message Queues in the system.
				   3). Create 10 Threads which does Post operation
				       on 20 Message Queues Randomly.
				   4). Create 15 Threads which does Wait operation
				       on 20 Message Queues Randomly.
				   5). Create 1 Thread which does Delete and Close
				       operation on 20 Message Queues Randomly.
				   6). Create 2 Threads which does a Re- Create on
				       20 Message Queues Randomly,if deleted earlier.
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete all the 28 Threads.
				   9). Return success.

				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Message Queue
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Message Queue
				   API Interface is Robust.Timeout Time can be reconfigured in the
				   file "oedt_osalcore_TestFuncs.h" under the define
				   STRESS_MQ_DURATION.Whether this case should be run
				   can be configured in the same file as part of the define
				   STRESS_MQ.

* HISTORY     :	  Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tU32 u32MessageQueueStressTest( tVoid )
{
	/*Definitions*/
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = "\0";


	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "B_SEM",&baseSem, 1 );

	/*Create MAX_MESSAGE_QUEUES number of messages in the system*/
	while( u8Index < MAX_MESSAGE_QUEUES )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_MessageQueue_%d",u8Index );

		/*Get the name into Message Name field*/
		(tVoid)OSAL_szStringCopy( mesgque_StressList[u8Index].coszName,acBuf );
		/*Fill in the length of a message unit*/
		mesgque_StressList[u8Index].u32MaxLength   = MAX_LEN;
		/*Fill in the number of message units in the message queue*/
		mesgque_StressList[u8Index].u32MaxMessages = MAX_NO_MESSAGES;
		/*Set the access mode to the message queue*/
		mesgque_StressList[u8Index].enAccess       = OSAL_EN_READWRITE;

		/*Create a Message Queue within the system*/
		if( OSAL_ERROR == OSAL_s32MessageQueueCreate(
													  mesgque_StressList[u8Index].coszName,
													  mesgque_StressList[u8Index].u32MaxMessages,
													  mesgque_StressList[u8Index].u32MaxLength,
													  mesgque_StressList[u8Index].enAccess,
													  &mesgque_StressList[u8Index].phMQ
													 ) )
		{

			/*Return immediately with failure*/
			return (10000+u8Index);
		}
		/*Reinitialize the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment the index*/
		u8Index++;
	}

	/*Start all Post Threads*/
	vEntryFunction( Post_Message,"Post_Msgq_Thr",NO_OF_POST_THREADS,msgqpost_ThreadAttr,msgqpost_ThreadID );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_Message,"Wait_Msgq_Thr",NO_OF_WAIT_THREADS,msgqwait_ThreadAttr,msgqwait_ThreadID );
	/*Start the Close Thread*/
	vEntryFunction( Close_Message,"Close_Msgq_Thr",NO_OF_CLOSE_THREADS,msgqclose_ThreadAttr,msgqclose_ThreadID );
	/*Start all Create Threads*/
	vEntryFunction( Create_Message,"Create_Msgq_Thr",NO_OF_CREATE_THREADS,msgqcreate_ThreadAttr,msgqcreate_ThreadID );

	/*Wait for OSAL_C_TIMEOUT_FOREVER*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( msgqpost_ThreadID[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( msgqwait_ThreadID[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( msgqclose_ThreadID[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( msgqcreate_ThreadID[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Reset the Global Thread Counter*/
	u32GlobMsgCounter = 0;

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( baseSem );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEM" );

	/*Return error code - Success if control reaches here*/
	return 0;
}

/*****************************************************************************
* FUNCTION    :   MsgQueSimplePostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue.
               2). Post to Message Queue
               3). Wait on Message Queue
               4). Go to 1). and do Test again n-Times
               5). Close and Delete Message Queue

* HISTORY     :
* 03.03.2011   Dainius Ramanauskas (CM-AI/PJ-CF31)
*******************************************************************************/

tU32 MsgQueSimplePostWaitPerfTest(tVoid )
{
   tU32 u32Ret = 0;
   OSAL_tMQueueHandle mqHandle = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   int loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &mqHandle))
   {

      while (loop < RUN_TEST)
      {
         if (OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ))
         {
            u32Ret += loop+1000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 1200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 1300;
            return u32Ret;
         }
         if( 0 == OSAL_s32MessageQueueWait(mqHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
         {
            u32Ret += loop+2000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 2200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 2300;
            return u32Ret;
         }
         loop++;
      }
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 300;
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   MsgQueThreadPostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue
               2). Create Thread
               3). Wait on Message Queue
               4). Thread should post to Message Queue
               5). Go to 2). and do Test again n-Times
               6). Close and Delete Message Queue

* HISTORY     :
* 03.03.2011   Dainius Ramanauskas (CM-AI/PJ-CF31)
*******************************************************************************/

tU32 MsgQueThreadPostWaitPerfTest(tVoid )
{
   OSAL_trThreadAttribute threadAttr = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   tS8 ps8ThreadName[16];
   tU32 u32Ret = 0;
   tU32 u32Loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &MQHandle))
   {
      while (u32Loop < RUN_TEST)
      {
         sprintf((tString) ps8ThreadName, "%s%03u", "MQ_TH_", u32Loop);
         threadAttr.u32Priority = 0;
         threadAttr.szName = (tString) ps8ThreadName;
         threadAttr.pfEntry = (OSAL_tpfThreadEntry) u32MQPostThread;
         threadAttr.pvArg = OSAL_NULL;
         threadAttr.s32StackSize = VALID_STACK_SIZE;

         if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
         {

            if (OSAL_ERROR != OSAL_s32MessageQueueWait(MQHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
            {
               u32Loop++;
            }
            else
            {
               u32Ret += u32Loop + 2000;
               break;
            }
         }
         else
         {
            u32Ret += u32Loop + 4000;
            break;
         }
      } // while
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(MQHandle))
      u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME))
      u32Ret += 300;
   return u32Ret;
}

/******************************************************************************
 *FUNCTION     :u32MQOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *HISTORY      :07.07.2011  Dainius Ramanauskas
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
*****************************************************************************/
tU32 u32MQOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   tS32 s32ReturnValueThread = 0;
   OSAL_tMQueueHandle handle_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) Thread_MQ_Memory;
   threadAttr.pvArg = &s32ReturnValueThread;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   if ((OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &handle_1) != OSAL_ERROR))
   {
      if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
      {
         OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
         if (OSAL_s32MessageQueuePost(handle_1, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueWait(handle_1, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
            {
               if (OSAL_s32MessageQueueClose(handle_1) != OSAL_ERROR)
               {
                  if (OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME) != OSAL_ERROR)
                  {
                     // test ok
                  }
                  else
                     s32ReturnValue += 700;
               }
               else
                  s32ReturnValue += 500;
            }
            else
               s32ReturnValue += 400;
         }
         else
            s32ReturnValue += 300;
      }
      else
         s32ReturnValue += 200;
   }
   else
      s32ReturnValue += 100;


   return s32ReturnValue;
}

/*****************************************************************************
* FUNCTION    :	   helper function for u32MsgQueNotifyEvent()
******************************************************************************/
void u32ThreadForSendingMessage( void* pArg )
{
  OSAL_tMQueueHandle mqhandle = 0;
  u32Tr1Ret = 0;
  ((void)pArg);
  if ( OSAL_s32MessageQueueOpen( MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY,	&mqhandle) == OSAL_OK )
  {
    if ( OSAL_s32MessageQueuePost(mqhandle,(tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1, MQ_PRIO1 ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueuePost() failed with error '%i'", OSAL_u32ErrorCode());
      u32Tr1Ret = 1000;
    }
    
  } else {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueueOpen() failed with error '%i'", OSAL_u32ErrorCode());
    u32Tr1Ret += 2000;  
  }

  if ( OSAL_s32MessageQueueClose (mqhandle) == OSAL_ERROR )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
    u32Tr1Ret += 4000;
  }
  
  OSAL_vThreadExit();
}



/*****************************************************************************
* FUNCTION    :	  u32MsgQueNotifyEvent()
* PARAMETER   :   none
* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :   see https://hi-cmts.apps.intranet.bosch.com:8443/browse/CF3PF-760
* HISTORY     :   Martin Langer (ESE, CM-AI/PJ-CF33) on Jan 9th, 2011
*******************************************************************************/
tU32 u32MsgQueNotifyEvent( tVoid )
{
  tU32 u32Ret = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  OSAL_tEventHandle hEvent = 0;
  OSAL_trThreadAttribute  attr = { 0 };
  OSAL_tThreadID ThreadID = 0;
  trMqEventInf rEvent;

  u32Tr1Ret = 0;

  if ( OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess,	&MQHandle ) == OSAL_OK )
  {

    if ( OSAL_s32EventCreate( "TestName", &hEvent ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
      u32Ret = 1;
    } else {

      rEvent.coszName = "TestName";
      rEvent.mask = 0x11001100;
      rEvent.enFlags = OSAL_EN_EVENTMASK_OR;

      if ( OSAL_s32MessageQueueNotify( MQHandle, (OSAL_tpfCallback)0xffffffff, (tPVoid)&rEvent ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueNotify() failed with error '%i'", OSAL_u32ErrorCode());
        u32Ret += 2;
      }

      // message queue is prepared, start posting a msg in subthread to get an event in this main thread

      attr.szName = MQ_THR_NAME_NOTIFY_2;
      attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      attr.s32StackSize = MQ_TR_STACK_SIZE;
      attr.pfEntry = u32ThreadForSendingMessage;
      attr.pvArg = OSAL_NULL;

      ThreadID = OSAL_ThreadSpawn( &attr );
      if ( ThreadID == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_ThreadSpawn() failed with error '%i'", OSAL_u32ErrorCode());
        u32Ret += 64;
      }

      if ( OSAL_s32EventWait ( hEvent, rEvent.mask, rEvent.enFlags, OSAL_C_TIMEOUT_NOBLOCKING | 10000  , &gevMask ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += 128;
      }

      if ( OSAL_s32EventClose ( hEvent ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose() failed with error %i", OSAL_u32ErrorCode());
        u32Ret += 4;
      }

      if ( OSAL_s32EventDelete( "TestName" ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete() failed with error %i", OSAL_u32ErrorCode());
        u32Ret += 8;
      }
    }

    if ( OSAL_s32MessageQueueNotify( MQHandle, (OSAL_tpfCallback)0xffffffff, NULL ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueNotify() failed with error '%i'", OSAL_u32ErrorCode());
      u32Ret += 512;
    }

    if ( OSAL_s32MessageQueueClose ( MQHandle ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 16;
    }

    if ( OSAL_s32MessageQueueDelete ( MESSAGEQUEUE_NAME ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueDelete() failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 32;
    }

    if ( OSAL_s32ThreadDelete( ThreadID ) == OSAL_ERROR )
    {
      // don't throw an error if thread finished itself
      if (OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: delete thread '%i' failed with error %i", ThreadID, OSAL_u32ErrorCode());
        u32Ret += 256;
      }
    }
  }

  if (u32Tr1Ret != 0)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: received error '%i' from subthread u32ThreadForSendingMessage()", u32Tr1Ret);
    u32Ret += u32Tr1Ret;
  }
  return u32Ret;
}



/******************************************************************************
 *FUNCTION      :u32MsgQueNotifyRemove
 *
 *DESCRIPTION   :Creates a message queue and an event
 *               Registers a callback for notification that will set event
 *                  flags when triggered
 *               Posts a message to the queue and checks if the callback was
 *                  executed by waiting for an event.
 *               Cleans message queue and event
 *               Posts a message to the queue and checks if the callback was
 *                  executed again by waiting for an event.
 *               Removes that callback
 *               Posts a message to the queue and checks if the callback was
 *                  executed by waiting for an event. While waiting for the
 *                  event an error of OSAL_E_TIMEOUT is expected
 *               Closes & deletes the event and the message queue
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tU32   0 on success or
 *                      error code in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 03 05
 *****************************************************************************/
tU32 u32MsgQueNotifyRemove(tVoid)
{
    tU32                retval      = 0;
    OSAL_tMQueueHandle  mqhandle    = 0;
    OSAL_tEventMask     evmask      = 0;
    tU8                 msgbuffer[MAX_LEN];

    if((OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &mqhandle) == OSAL_ERROR))
    {
        retval += 1;
    }
    else
    {
        if(OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2_cb) == OSAL_ERROR)
        {
            retval += 2;
        }
        else
        {
            // Register a callback
            if(OSAL_s32MessageQueueNotify(mqhandle, vMQCallBack2, NULL) == OSAL_ERROR)
            {
                retval += 4;
            }

            // Trigger the callback by posting a message
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 8;
            }

            // Check if the callback was executed by checking the event flags
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) == OSAL_ERROR)
            {
                retval += 16;
            }



            // Clear event flags
            OSAL_s32EventPost(MQEveHandle2_cb, 0, OSAL_EN_EVENTMASK_AND);
            // Clear message queue
            OSAL_s32MessageQueueWait(mqhandle, msgbuffer, sizeof(msgbuffer), NULL, OSAL_C_TIMEOUT_FOREVER);

            // Try to trigger the callback again
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 32;
            }

            // Check if the callback was executed again by checking the event flags
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) == OSAL_ERROR)
            {
                retval += 64;
            }



            // Clear event flags
            OSAL_s32EventPost(MQEveHandle2_cb, 0, OSAL_EN_EVENTMASK_AND);
            // Clear message queue
            OSAL_s32MessageQueueWait(mqhandle, msgbuffer, sizeof(msgbuffer), NULL, OSAL_C_TIMEOUT_FOREVER);

            // Remove the callback
            if(OSAL_s32MessageQueueNotify(mqhandle, NULL, NULL) == OSAL_ERROR)
            {
                retval += 128;
            }

            // Try to trigger the callback
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 256;
            }

            // Check if the callback was executed by checking the event flags
            // Since the callback was removed, an error is expected
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) != OSAL_ERROR)
            {
                retval += 512;
            }
            else
            {
                tU32    status = OSAL_u32ErrorCode();
                if(status != OSAL_E_TIMEOUT)
                    retval += 1024;
            }

            // Clean up event
            if(OSAL_s32EventClose(MQEveHandle2_cb) == OSAL_ERROR)
            {
                retval += 2048;
            }
            if(OSAL_s32EventDelete(MQ_EVE_NAME2) == OSAL_ERROR)
            {
                retval += 4096;
            }
        }

        // Clean up message queue
        if(OSAL_s32MessageQueueClose(mqhandle) == OSAL_ERROR)
        {
            retval += 8192;
        }
        if(OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME) == OSAL_ERROR)
        {
            retval += 16384;
        }
    }

    return retval;
}
/******************************************************************************
*FUNCTION      :u32MsgQueTimeoutTest
*
*DESCRIPTION : This is a test for timeout feature to call OSAL_s32MessageQueueWait
*
*            1. Creates a message queue
*            2. Calls a Wait on the message queue with a specific timeout value
*            3. If a timeout occours then test is passed
*
*PARAMETER     :none
*
*RETURNVALUE   :tU32   0 on success or
*                      error code in case of an error
*
*HISTORY:      :Created by rmm1kor on 29/05/2012
*****************************************************************************/

tU32 u32MsgQueTimeoutTest(tVoid)
{
   tS32 u32RetVal = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle= 0;
   tU8 u8Buf[MAX_LEN];
   // Create Message queue
   
   if(OSAL_OK == (OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,
                                             MQ_COUNT_MAX,
                                             MAX_LEN,
                                             enAccess,
                                             &mqHandle)))
   {
      // Call a wait on the message queue with finite timeout value. This is expected to timeout.
      if(0 == OSAL_s32MessageQueueWait(mqHandle, u8Buf, MAX_LEN, OSAL_NULL, MSG_QUEUE_TIMEOUT_VAL))
      {
         if(OSAL_u32ErrorCode()== OSAL_E_TIMEOUT) // It timed-out Test case is passed
         {
            u32RetVal = 0;
            OEDT_HelperPrintf(TR_LEVEL_USER_4, "Sucess:  Wait on message queue timedout as expected.");
         }
         else
         {
            u32RetVal = 2;
            OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: Message queue Wait failed expected error OSAL_E_TIMEOUT but got : ",OSAL_u32ErrorCode());
         }
      }
      else
      {
         u32RetVal += 4;
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: Message queue Wait Passed !!!! Expcted to FAIL !!! ");
      }

      if(OSAL_s32MessageQueueClose(mqHandle)==OSAL_ERROR)
      {
         u32RetVal += 8;
         OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: closing message queue failed in u32MsgQueTimeoutTest");
      }

      if(OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)== OSAL_ERROR)
      {
         u32RetVal += 16;
         OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: OSAL_s32MessageQueueDelete failed in u32MsgQueTimeoutTest");
      }
   }
   else
   {
      u32RetVal += 32;
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueCreate Failed !!!");
   }
   return u32RetVal;
}
