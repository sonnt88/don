
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "OsalConf.h"
#include "osal_if.h"
#include "Linux_osal.h"
#include "unistd.h"
#include "process_cd1.h"
#include <sys/mman.h>
#include <fcntl.h>

#include <stdio.h>


/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define OCDTR(TL,...) if(g_iTraceLevel >= (tInt)(TL)) vTracePrintf(__VA_ARGS__)


#define OEDT_CD_LOWORD(U32) ((tU16)((U32)&(0xFFFFUL)))
#define OEDT_CD_HIWORD(U32) ((tU16)((U32) >> 16))

#define OEDT_CD_8TO32(_3_,_2_,_1_,_0_) (((tU32)(_3_) << 24) |\
                                       ((tU32)(_2_) << 16) |\
                                       ((tU32)(_1_) << 8) |\
                                       ((tU32)(_0_)))

#define OEDT_CD_ARSIZE(_X_) (sizeof(_X_) / sizeof((_X_)[0]))

#define OEDT_CD_RELOFFSETFROMPLAYINFO(PI) ((tU16)PI.rRelTrackAdr.u8MSFMinute \
                                           * 60 \
                                           + (tU16)PI.rRelTrackAdr.u8MSFSecond)

#define OEDT_CD_MS2SECS(msf) (((tU32)(msf).u8MSFMinute * 60UL) \
                                                    + (tU32)(msf).u8MSFSecond)

#define OEDT_CD_NOTI32(HW,LW) (((tU32)(OSAL_C_U16_NOTI_ ## HW)<<16)\
                               | ((tU32)(OSAL_C_U16_ ## LW)))

#define OEDT_CD_IGNORE32 0xFFFFFFFF

/*#define OEDT_CD_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24) )*/

//#define OEDT_CD_MIN(X,Y) ((X) < (Y) ? (X) : (Y))

#define OEDT_CD_TRACE_DEVICE            "/dev/trace"
#define OEDT_CD_TRACE_PERMISSION        OSAL_EN_READWRITE
#define OEDT_CD_MAX_TRACE_STR_SIZE      256

/*****************************************************************
| const definition (scope: module-local)
|----------------------------------------------------------------*/
#define OEDT_CD_MAX_ERR_STR_SIZE       128  /*error text buffer size*/
static const tU32 g_cu32BPS           = 2048; /*Bytes per sector*/
static const tU32 g_cu32ErrTxtFixSize = 11; /*formatted output of "0x%08X: "*/
static const tInt c0               = (tInt)TR_LEVEL_FATAL;
static const tInt c1               = (tInt)TR_LEVEL_ERRORS;
static const tInt c2               = (tInt)TR_LEVEL_SYSTEM_MIN;
static const tInt c3               = (tInt)TR_LEVEL_SYSTEM;
/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/
static tInt g_iTraceLevel = (tInt)TR_LEVEL_USER_4; //TR_LEVEL_ERRORS;


 /********************************************************************/ /**
  *  FUNCTION:      tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tChar *pchFormat, ...){
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u8_trace_level     TTFIS trace level
  *  @param         tChar *pchFormat   VA Format string
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  * 13-April-2009| Lint warnings removed | Jeryn Mathew(RBEI/ECF1)
  ************************************************************************/
#define OEDT_HELPER_MAX_TRACE_STR_SIZE                       ((tInt) 256)
#define _OEDT_TTFIS_ODTDEV_ID         0x1a
#define _OEDT_TTFIS_HELPER_TRACE_ID   0x02


static tVoid OEDT_HelperPrintf(tU8 u8_trace_level, tPCChar pchFormat, ...){
    
    tChar pcBuffer[OEDT_HELPER_MAX_TRACE_STR_SIZE+2];
    va_list argList = {0}; 
    tInt noChar = 0;
    va_start(argList, pchFormat);
    pcBuffer[0] = _OEDT_TTFIS_ODTDEV_ID;
    pcBuffer[1] = _OEDT_TTFIS_HELPER_TRACE_ID;
    
    noChar = vsnprintf(&pcBuffer[2], 255, pchFormat, argList);
    pcBuffer[OEDT_HELPER_MAX_TRACE_STR_SIZE-1] = '\0';  //last buffer entry
    pcBuffer[noChar+2] = '\0'; //end of the real msg
    // TTFIS tracing
	TR_core_uwTraceOut((tU32)noChar+3,TR_COMP_OSALTEST,(TR_tenTraceLevel)u8_trace_level,(tU8*)pcBuffer);
    va_end( argList );
    return;
}

/********************************************************FunctionHeaderBegin***
* vSend2TTFis()
* sends a message to the configured output device
***********************************************************FunctionHeaderEnd***/
static tVoid vSend2TTFis(tPCS8 ps8Msg, tU32 u32Len)
{
  OSAL_trIOWriteTrace rMsg2Trace;
  OSAL_tIODescriptor  fdTrace;

  rMsg2Trace.u32Class    = TR_COMP_OSALTEST;
  rMsg2Trace.u32Level    = TR_LEVEL_FATAL;
  rMsg2Trace.u32Length   = u32Len;
  rMsg2Trace.pcos8Buffer = ps8Msg;

  fdTrace = OSAL_IOOpen(OEDT_CD_TRACE_DEVICE, OEDT_CD_TRACE_PERMISSION);

  if(fdTrace != OSAL_ERROR)
  {
    OSAL_s32IOWrite(fdTrace, (tPCS8)&rMsg2Trace, sizeof(rMsg2Trace));
    OSAL_s32IOClose(fdTrace);
  }
}

static va_list getArgList(va_list* a)
{
  (void)a;
  return *a;
}

/*********************************************************FunctionHeaderBegin***
* vTracePrintf
* TTFIS output in printf-style
***********************************************************FunctionHeaderEnd***/
static tVoid vTracePrintf(tPCChar pchFormat, ...)
{
  tChar pcBuffer[OEDT_CD_MAX_TRACE_STR_SIZE];
  va_list argList = {0};
  tU32 u32Len;
  va_start(argList, pchFormat);
  
  memset(pcBuffer, 0, sizeof(pcBuffer));
  pcBuffer[0] = 0x1A; // COMMAND_ID
  pcBuffer[1] = 0x02; //oedt.trc:1a:(6,1)==0x1a,(7,1)==0x02: INFO: %s(8,1,I,255)
   (void)vsnprintf(&pcBuffer[2], 250, pchFormat, argList);
  u32Len = (tU32)strlen(&pcBuffer[2]) + 2;
  vSend2TTFis((tS8*)pcBuffer, u32Len);
  va_end(argList);
}



/*****************************************************************************
* FUNCTION:     vTraceHex
* PARAMETER:    Buffer, Length of buffer, number of Bytes printed per Line
* RETURNVALUE:  result
* DESCRIPTION:  output hex dump to TTFIS
******************************************************************************/
static tVoid vTraceHex(const tU8 *pu8Buf, tInt iBufLen, tInt iBytesPerLine)
{
  tInt iBpl = iBytesPerLine;
  tInt iL, iC;
  char c;
  static char szTmp[32];
  static char szTxt[256];
  if(NULL == pu8Buf)
  {
    return;
  } //if(NULL == pu8Buf)


  for(iL = 0 ; iL < iBufLen ; iL += iBpl)
  {
    sprintf(szTxt, "%04X:",(unsigned int)(iL));
    for(iC = 0 ; iC < iBpl ; iC++)
    {
      sprintf(szTmp, " %02X",(unsigned int)pu8Buf[iL + iC]);
      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)

    strcat(szTxt, " ");
    for(iC = 0 ; iC < iBpl ; iC++)
    {
      c = (char)pu8Buf[iL + iC];
      if((c < 0x20) || (c >= 0x80))
      {
        c = '.';
      }
      sprintf(szTmp, "%c",(unsigned char)c);

      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)
    OCDTR(c3, "%s", szTxt);
  } //for(i = 0 ; i < iBufLen ; i += iBpl)
}


/*****************************************************************************
* FUNCTION:     pGetErrTxt
* PARAMETER:    void
* RETURNVALUE:  const char* of Osal-Error-text
* DESCRIPTION:  output Error text of last Osal -Error
******************************************************************************/
static const char* pGetErrTxt()
{
  return(const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}

/*****************************************************************************
* FUNCTION:     vPrintOsalResult
* PARAMETER:    Description
*               OSAL-return value
*               internal loop counter
* RETURNVALUE:  void
* DESCRIPTION:  Traces error or success
******************************************************************************/
static tVoid vPrintOsalResult(const char* pcszDescription,
                              tS32 s32Ret,
                              int iCount)
{
  if(g_iTraceLevel >= (tInt)TR_LEVEL_ERRORS)
  {
    tBool bOK = (s32Ret != OSAL_ERROR);
    if(bOK)
    {
      OCDTR(c2, "OEDT_CD [%s][%4d] SUCCESS", pcszDescription, iCount);
    }
    else //if(bOK)
    {
      OCDTR(c1, "OEDT_CD [%s][%4d] ERROR [%s]",
            pcszDescription,
            iCount,
            pGetErrTxt());
    } //else //if(bOK)
  } //if(g_iTraceLevel)
}

//used by test without cd - An Error-Code is treated as Success!
/*****************************************************************************
* FUNCTION:     vPrintOsalResultNoCD
* PARAMETER:    Description
*               OSAL-return value
*               internal loop counter
* RETURNVALUE:  void
* DESCRIPTION:  Traces error or success
*               This version treaded an OSAL-ERROR as "successfull tested"
******************************************************************************/
static tVoid vPrintOsalResultNoCD(const char* pcszDescription,
                                  tS32 s32Ret,
                                  int iCount)
{
  if(g_iTraceLevel >= (tInt)TR_LEVEL_ERRORS)
  {
    tBool bOK = (s32Ret != OSAL_ERROR);
    if(bOK)
    {
      OCDTR(c1, "OEDT_CD [%s][%4d] SUCCESS (== test f a i l e d)",
            pcszDescription, iCount);
    }
    else //if(bOK)
    {
      OCDTR(c2, "OEDT_CD [%s][%4d] ERROR (== test successfully) [%s]",
            pcszDescription,
            iCount,
            pGetErrTxt());
    } //else //if(bOK)
  } //if(g_iTraceLevel)
}


/*****************************************************************************
* FUNCTION:     vGetOsalErrorText
* PARAMETER:    Text Buffer
*               size of Text Buffer
* RETURNVALUE:  void
* DESCRIPTION:  copies error informations into given Text Buffer
******************************************************************************/
static void vGetOsalErrorText(tString pszErrorText, tU32 u32ErrorTextBufferSize)
{
  tU32     u32ErrorCode;
  tCString pcszErrorTxt;
  tU32     u32NeededBufferSize;

  u32ErrorCode = OSAL_u32ErrorCode();
  pcszErrorTxt = OSAL_coszErrorText(u32ErrorCode);

  if(g_cu32ErrTxtFixSize < u32ErrorTextBufferSize)
  {
    if(NULL != pcszErrorTxt)
    {
      u32NeededBufferSize = OSAL_u32StringLength(pcszErrorTxt) +
                            g_cu32ErrTxtFixSize;
      if(u32NeededBufferSize > u32ErrorTextBufferSize)
      {
        OSAL_s32PrintFormat(pszErrorText,
                            "0x%08X: <%s>",
                            (unsigned int)u32ErrorCode,
                            (const char*)pcszErrorTxt);
      }
      else //if(u32NeededBufferSize > u32ErrorTextBufferSize)
      {
        OSAL_s32PrintFormat(pszErrorText,
                            "0x%08X: <%s>",
                            (unsigned int)u32ErrorCode,
                            (const char*)pcszErrorTxt);
      } //else //if(u32NeededBufferSize > u32ErrorTextBufferSize)
    }
    else //if(NULL != pcszErrorTxt)
    {
      OSAL_s32PrintFormat(pszErrorText,
                          "0x%08X: <no error text available>",
                          (unsigned int)u32ErrorCode);
    } //else //if(NULL != pcszErrorTxt)
  }
  else//if(g_cu32ErrTxtFixSize >= u32ErrorTextBufferSize)
  {
  } //else//if(g_cu32ErrTxtFixSize >= u32ErrorTextBufferSize)

}

/******************************************************************************
 *FUNCTION     :  vEject
 *PARAMETER    :  void
 *RETURNVALUE  :  void
 *DESCRIPTION  :  send eject command without checking for errors
 *****************************************************************************/
static void vEject(void)
{
  OSAL_tIODescriptor hCDctrl;
  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR != hCDctrl)
  {
    //tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
    s32Arg = 0;
    OCDTR(c2, "vEject (EJECTMEDIA)");
    (void)OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
    (void)OSAL_s32IOClose(hCDctrl);
  } //if(OSAL_ERROR != hCDctrl)
}

/******************************************************************************
 *FUNCTION     :  vLoad
 *PARAMETER    :  void
 *RETURNVALUE  :  void
 *DESCRIPTION  :  send closedoor command without error-check
 *****************************************************************************/
static void vLoad(void)
{
  OSAL_tIODescriptor hCDctrl;

  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR != hCDctrl)
  {
    tInt iEmergencyExit = 50;
    tS32 s32Fun, s32Arg;
    s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
    s32Arg = 0;
    OCDTR(c2, "vLoad (CLOSEDOOR)");
    while(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg)
          &&
          (iEmergencyExit-- > 0))
    {
      (void)OSAL_s32ThreadWait(100);
    }

    (void)OSAL_s32IOClose(hCDctrl);
  } //if(OSAL_ERROR != hCDctrl)
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_NOCD
 *DESCRIPTION  :  This is no test. 
 *                Prints "REMOVE CD" and waits until cd is removed
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_NOCD(void)
{
  tU32 u32ResultBitMask = 0;
  OSAL_tIODescriptor hCDctrl;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;

  startMS = OSAL_ClockGetElapsedTime();


  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hCDctrl)
  {
    u32ResultBitMask |= 0x00000001;
  }
  else //if(OSAL_ERROR == hCDctrl)
  {
    tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    tBool bCDInserted;
    tInt iCount=0;
    //GETLOADERINFO - wait while cd is removed
    do
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= 0x00000002;
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_NO_MEDIA:
          bCDInserted = FALSE;
          break;
        case OSAL_C_U8_MEDIA_INSIDE:
          //EJECTMEDIA
          {
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
            s32Arg = 0;
            (void)OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);
          }//EJECTMEDIA
          /*lint -fallthrough */
        case OSAL_C_U8_EJECT_IN_PROGRESS:
          /*lint -fallthrough */
        case OSAL_C_U8_MEDIA_IN_SLOT:
          /*lint -fallthrough */
        default:
          bCDInserted = TRUE;
          if((iCount % 6) == 0)
          {
            OCDTR(c0, "%s", "***********************************");
            OCDTR(c0, "%s", "* REMOVE CD                       *");
            OCDTR(c0, "%s", "***********************************");
          }
          (void)OSAL_s32ThreadWait(500);
          break;

        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)

      iCount++;
      diffMS = OSAL_ClockGetElapsedTime() - startMS;
    }
    while(bCDInserted && (diffMS < (60 * 30 * 1000)));//GETLOADERINFO

    (void)OSAL_s32IOClose(hCDctrl);
  } //else //if(OSAL_ERROR == hCDctrl)

  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA01
 *DESCRIPTION  :  Prints "INSERT UDF CD"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
static tU32 u32WaitForCD(const char *pcszTxt)
{
  tU32 u32ResultBitMask = 0;
  OSAL_tIODescriptor hCDctrl;
  tBool bCDWasEjected = FALSE;


  /* open device */
  hCDctrl = OSAL_IOOpen(OSAL_C_STRING_DEVICE_CDCTRL, OSAL_EN_WRITEONLY);
  if(OSAL_ERROR == hCDctrl)
  {
    u32ResultBitMask |= 0x00000001;
  }
  else //if(OSAL_ERROR == hCDctrl)
  {
    tS32 s32Ret;
    tS32 s32Fun, s32Arg;
    tBool bCDInserted;
    tInt iCount = 0;
    //GETLOADERINFO - wait while cd is inserted
    do
    {
      OSAL_trLoaderInfo rInfo;

      rInfo.u8LoaderInfoByte = 0;
      s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
      s32Arg = (tS32)&rInfo;
      s32Ret = OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg);

      if(s32Ret == OSAL_ERROR)
      {
        u32ResultBitMask |= 0x00000002;
        bCDInserted = FALSE;
      }
      else //if(s32Ret == OSAL_ERROR)
      {
        switch(rInfo.u8LoaderInfoByte)
        {
        case OSAL_C_U8_MEDIA_INSIDE:
          bCDInserted = TRUE;
          break;

        case OSAL_C_U8_EJECT_IN_PROGRESS:
          /*lint -fallthrough */
        case OSAL_C_U8_MEDIA_IN_SLOT:
          {
            tInt iEmergencyExit = 20;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
            s32Arg = 0;
            while(OSAL_OK != OSAL_s32IOControl(hCDctrl, s32Fun, s32Arg)
                  &&
                  (iEmergencyExit-- > 0))
            {
              (void)OSAL_s32ThreadWait(500);
            }
          }
          /*lint -fallthrough */
        case OSAL_C_U8_NO_MEDIA:
          /*lint -fallthrough */
        default:
          bCDWasEjected = TRUE;
          bCDInserted = FALSE;
          if((iCount % 16) == 0)
          {
            OCDTR(c0, "%s", "***********************************");
            OCDTR(c0, "%s", pcszTxt);
            OCDTR(c0, "%s", "***********************************");
          }
          (void)OSAL_s32ThreadWait(500);
        } //switch(rInfo.u8LoaderInfoByte)
      } //else //if(s32Ret == OSAL_ERROR)
      iCount++;
    }
    while(!bCDInserted);//GETLOADERINFO

    (void)OSAL_s32IOClose(hCDctrl);
  } //else //if(OSAL_ERROR == hCDctrl)

  if(bCDWasEjected)
  {
    (void)OSAL_s32ThreadWait(10000);
  }
  else //if(bCDWasEjected)
  {
    (void)OSAL_s32ThreadWait(1000);
  } //else //if(bCDWasEjected)

  return u32ResultBitMask;
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA01
 *DESCRIPTION  :  Prints "INSERT CD MASCA 01"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA01(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 01' (UDF 1.02) *");
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA02
 *DESCRIPTION  :  Prints "INSERT CD MASCA 02"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA02(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 02' (ISO9660) *");
}

/******************************************************************************
 *FUNCTION     :  OEDT_CD_TPRINT_MASCA03
 *DESCRIPTION  :  Prints "INSERT CD MASCA 03"
 *PARAMETER    :  void
 *RETURNVALUE  :  0
 *****************************************************************************/
tU32 OEDT_CD_TPRINT_MASCA03(void)
{
  return u32WaitForCD("* INSERT CD 'MASCA 03' (CDDA) *");
}




void TraceOut(const char* cBuffer)
{
  char au8Buf[OSAL_C_U32_MAX_PATHLENGTH];
  tU16 u16Len;
  memset(au8Buf,' ',OSAL_C_U32_MAX_PATHLENGTH);
  au8Buf[0] = 0xf1;
  u16Len = strlen(cBuffer);
  if(u16Len > 250)
    u16Len = 250;

  au8Buf[u16Len+1] = '\0';
  memcpy(&au8Buf[1],cBuffer,u16Len);

  LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,u16Len+1);
}


/*****************************************************************************/
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*                            OEDT_CD_T001                          */
/*****************************************************************************/

#define OEDT_CD_T001_COUNT                                              1
#define OEDT_CD_T001_DEVICE_CTRL_NAME          OSAL_C_STRING_DEVICE_CDCTRL
#define OEDT_CD_T001_DEVICE_AUDIO_NAME         OSAL_C_STRING_DEVICE_CDAUDIO

#define OEDT_CD_T001_RESULT_OK_VALUE                            0x00000000
#define OEDT_CD_T001_OPEN_RESULT_ERROR_BIT                      0x00000001
#define OEDT_CD_T001_CLOSE_RESULT_ERROR_BIT                     0x80000000

/******************************************************************************
 *FUNCTION     :  OEDT_CD_T001
 *
 *DESCRIPTION  :  open/close CDCTRL
 *
 *PARAMETER    :  void
 *             
 *RETURNVALUE  :  0
 *
 *HISTORY      :  
 *                Initial Revision.
 *****************************************************************************/
static tU32 OEDT_CD_T001(void)
{
  static tChar szError[OEDT_CD_MAX_ERR_STR_SIZE+1];
  tU32 u32ResultBitMask           = OEDT_CD_T001_RESULT_OK_VALUE;
  OSAL_tIODescriptor hCDAudio = OSAL_ERROR;
  OSAL_tIODescriptor hCDCtrl  = OSAL_ERROR;
  tS32 s32Ret;
  int  iCount;
  OSAL_tMSecond startMS;
  OSAL_tMSecond diffMS;

  OCDTR(c2, "PROCCD1 tU32 OEDT_CD_T001(void)");


  startMS = OSAL_ClockGetElapsedTime();
  for(iCount = 0; iCount < OEDT_CD_T001_COUNT; iCount++)
  {
    /* CDCTRL open device */
    hCDCtrl = OSAL_IOOpen(OEDT_CD_T001_DEVICE_CTRL_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDCtrl)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "PROCCD1: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T001_DEVICE_CTRL_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDCtrl)
    {
      OCDTR(c2, "PROCCD1: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T001_DEVICE_CTRL_NAME, (unsigned int)hCDCtrl, iCount);
    } //else //if(OSAL_ERROR == hCDCtrl)

    /* CDAUDIO open device */
    hCDAudio = OSAL_IOOpen(OEDT_CD_T001_DEVICE_AUDIO_NAME, OSAL_EN_WRITEONLY);
    if(OSAL_ERROR == hCDAudio)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "PROCCD1: ERROR Open <%s> (count %d): %s",
            OEDT_CD_T001_DEVICE_AUDIO_NAME, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_OPEN_RESULT_ERROR_BIT;
    }
    else //if(OSAL_ERROR == hCDAudio)
    {
      OCDTR(c2, "PROCCD1: SUCCESS Open <%s> == 0x%08X, (count %d)",
            OEDT_CD_T001_DEVICE_AUDIO_NAME, (unsigned int)hCDAudio, iCount);
    } //else //if(OSAL_ERROR == hCDAudio)



    {
      int i;
      for(i = 0; i < 3 ; i++)
      {
        printf("-------------- ProcCD1 WAIT %d----------- \n", i);
        OCDTR(c2, "PROCCD1: WAIT %d", i);
        OSAL_s32ThreadWait(100);
      }
    }


    //close cdctrl
    s32Ret = OSAL_s32IOClose(hCDCtrl);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "PROCCD1: ERROR CLOSE CDCTRL handle 0x%08X (count %d): %s",
            (unsigned int)hCDCtrl, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "PROCCD1: "
            "SUCCESS CLOSE handle CDCTRL %u (count %d)",
            (unsigned int)hCDCtrl, iCount);
    } //else //if(OSAL_OK != s32Ret)

    //close cdaudio
    s32Ret = OSAL_s32IOClose(hCDAudio);
    if(OSAL_OK != s32Ret)
    {
      vGetOsalErrorText(szError, OEDT_CD_MAX_ERR_STR_SIZE);
      OCDTR(c2, "PROCCD1: ERROR CLOSE handle CDAUDIO 0x%08X (count %d): %s",
            (unsigned int)hCDAudio, iCount, szError);
      u32ResultBitMask |= OEDT_CD_T001_CLOSE_RESULT_ERROR_BIT;
    }
    else //if(OSAL_OK != s32Ret)
    {
      OCDTR(c2, "PROCCD1: "
            "SUCCESS CLOSE handle AUDIO %u (count %d)",
            (unsigned int)hCDAudio, iCount);
    } //else //if(OSAL_OK != s32Ret)
  } //for(iCount = 0; iCount < OEDT_CD_T001_COUNT; iCount++)
  diffMS = OSAL_ClockGetElapsedTime() - startMS;
  OCDTR(c2, "PROCCD1: T001 Time for %u loops: %u ms",
        (unsigned int)iCount,
        (unsigned int)diffMS);

  if(OEDT_CD_T001_RESULT_OK_VALUE != u32ResultBitMask)
  {
    OCDTR(c1, "PROCCD1: T001 bit coded ERROR: 0x%08X",
          (unsigned int)u32ResultBitMask);
  } //if(OEDT_CD_T001_RESULT_OK_VALUE != u32ResultBitMask)

  return u32ResultBitMask;
}



int main(void)
{
    tS32 s32ReturnValue                 = 0;


    printf("\n-------------- ProcCD1 startet ----------- \n");

    TraceOut("-------------------------------------------");
    TraceOut("        force loading of libshared_cd_so.so");
    TraceOut("-------------------------------------------");


    OEDT_CD_T001();


    printf("-------------- OK. Process will never end-- \n");
    while(1)
    {
      OSAL_s32ThreadWait(10000);
      //TraceOut("-------------Loop-----------------------");
    }




    printf("-------------- ProcCD1 startet ----------- \n");

    TraceOut("-------------------------------------------");
    TraceOut("        process CD1 started");
    TraceOut("        TURN ON TRACES - I will wait 10secs!");
    TraceOut("-------------------------------------------");

    OSAL_s32ThreadWait(8000);

    TraceOut("-------------------------------------------");
    TraceOut("        Start Test");
    TraceOut("-------------------------------------------");

    OEDT_CD_T001();


    TraceOut("-------------------------------------------");
    TraceOut("        Test done - wait 5 secs");
    TraceOut("-------------------------------------------");

    OSAL_s32ThreadWait(5000);

/*
    while(1)
    {
      OSAL_s32ThreadWait(10000);
      printf("-------------- ProcCD1 Loop ----------- \n");
      //TraceOut("-------------Loop-----------------------");
    }
*/

    TraceOut("-------------------------------------------");
    TraceOut("               CD P1 returns now!");
    TraceOut("-------------------------------------------");
    _exit(s32ReturnValue);
}

