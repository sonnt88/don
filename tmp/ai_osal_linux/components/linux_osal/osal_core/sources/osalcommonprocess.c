/*****************************************************************************
| FILE:         osalcommonprocess.c
| PROJECT:      PF NGA
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Startup-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 12.10.10  | Initial revision           | OSC2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

extern void vStartApp(int argc, char **argv);

tS32 s32NrOfArg = 0;
tS32 s32Arg[10] = {0,0,0,0,0,0,0,0,0,0};

OSAL_DECL tS32 main(tS32 cPar, tString aPar[]) 
{
    int i;
    if(cPar > 1)
    {
	   /* first parameter(commandline) already stored in OSAL -> commandline */
       TraceString("argc: %d arg[0]:%s\n", cPar,aPar[0]);	
       for(i=1; i < cPar; i++)
       {
          s32Arg[i-1] = atoi(aPar[i]);
          TraceString("argv[%d]: %s converted:%d\n", i, aPar[i], s32Arg[i-1]);
          s32NrOfArg++;
       }
    }

	/* check for current process if libunwind can be used */   
  //  vCheckLibUnwind();

    vStartApp(cPar,aPar); 
    OSAL_vProcessExit();
    return 0;
}

