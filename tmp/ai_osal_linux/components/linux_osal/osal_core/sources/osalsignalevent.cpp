/*****************************************************************************
| FILE:         osalevent.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Event-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#ifdef SIGNAL_EV_HDR

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include "ostrace.h"

#include <signal.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
 
#define  LINUX_C_U32_EVENT_ID                    ((tU32)0x45564E54)

#define  LINUX_C_EVENT_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH - 1)


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

tS32  s32EventTableCreate(tVoid);
tS32  s32EventTableDeleteEntries(void);
tBool bCleanUpEventofContext(void);

static trEventElement* tEventTableGetFreeEntry(tVoid);

static trEventElement* tEventTableSearchEntryByName(tCString coszName);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableCreate
*
* DESCRIPTION: This function creates the Event Table List. If
*              there isn't space it returns a error code.
*
* PARAMETER:   none
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableCreate(tVoid)
{
   tU32 tiIndex;
   tS32 s32ReturnValue = OSAL_OK;

   pOsalData->EventTable.u32UsedEntries      = 0;
   pOsalData->EventTable.u32MaxEntries       = pOsalData->u32MaxNrEventElements;

   for(tiIndex = 0; tiIndex < pOsalData->EventTable.u32MaxEntries; tiIndex++)
   {
      pOsalData->rEventElement[tiIndex].u32EventID = LINUX_C_U32_EVENT_ID;
      pOsalData->rEventElement[tiIndex].bIsUsed = FALSE;
      pOsalData->rEventElement[tiIndex].u32Index = tiIndex;
   }

   return s32ReturnValue;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableGetFreeEntry
*
* DESCRIPTION: this function goes through the Event List and returns the
*              first unused EventElement.
*
*
* PARAMETER:   tVoid
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static  trEventElement* tEventTableGetFreeEntry(tVoid)
{
   trEventElement *pCurrent  = pOsalData->rEventElement;
   tU32 used = pOsalData->EventTable.u32UsedEntries;

   if (used < pOsalData->EventTable.u32MaxEntries)
   {
      pCurrent = &pOsalData->rEventElement[used];
      pOsalData->EventTable.u32UsedEntries++;
   } else {
        /* search an entry with !bIsUsed */
        while ( pCurrent < &pOsalData->rEventElement[used]
                && ( pCurrent->bIsUsed == TRUE) )
        {
            pCurrent++;
        }
        if(pCurrent >= &pOsalData->rEventElement[used])
        {
            pCurrent = NULL; /* not found */
        }
   }
   return pCurrent;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableSearchEntryByName
*
* DESCRIPTION: this function goes through the Event List and returns the
*              EventElement with the given name or NULL if all the List has
*              been checked without success.
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static trEventElement *tEventTableSearchEntryByName(tCString coszName)
{
   trEventElement *pCurrent  = pOsalData->rEventElement;
   tU32 used = pOsalData->EventTable.u32MaxEntries;

   /* search an entry with coszName and the current pid */
   while ( pCurrent < &pOsalData->rEventElement[used]
         && ( pCurrent->bIsUsed == FALSE
      ||  (OSAL_s32StringCompare(coszName, pCurrent->szName))) )
   {
      pCurrent++;
   }
   if(pCurrent >= &pOsalData->rEventElement[used])
   {
      pCurrent = NULL;
   }
   return pCurrent;
}

/*****************************************************************************
*
* FUNCTION:    bEventTableDeleteEntryByName
*
* DESCRIPTION: this function goes through the Event List deletes the
*              EventElement with the given name
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************
static tBool bEventTableDeleteEntryByName(tCString coszName)
{
   trEventElement *pCurrent = EventTable.ptrHeader;

   while( (pCurrent!= OSAL_NULL)
       && (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0) )
   {
      pCurrent = pCurrent->pNext;
   }
   if(pCurrent != OSAL_NULL)
   {
      if(pCurrent->bIsUsed == FALSE )
     {
           pCurrent->bIsUsed = 0;
           pCurrent->bToDelete = 0;
           pCurrent->u16ContextID = 0;
           pCurrent->u16OpenCounter = 0;
           pCurrent->u32mask = 0;
           memset(&pCurrent->rEventGroup,0,sizeof(T_CFLG));
           memset(pCurrent->szName,0,OSAL_C_U32_MAX_NAMELENGTH);
           pCurrent->hLock = 0;
           return TRUE;
     }
   }

   return FALSE;
}*/

/*****************************************************************************
*
* FUNCTION:    bCleanUpEventofContext
*
* DESCRIPTION: this function goes through the Event List deletes the
*              Event and EventElement with the given context from list
*
* PARAMETER:   tU16 Context ID
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool bCleanUpEventofContext(void)
{
   trEventElement *pCurrent = &pOsalData->rEventElement[0];
   tBool bResult = TRUE;
   tU32 used = pOsalData->EventTable.u32UsedEntries;

   while (pCurrent < &pOsalData->rEventElement[used])
   {
      if( OSAL_s32EventDelete( pCurrent->szName ) != OSAL_OK )
      {
         bResult = FALSE;
      }
      pCurrent++;
   }
   return bResult;
}


/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableDeleteEntries
*
* DESCRIPTION: This function deletes all elements from OSAL table,
*
* PARAMETER:
*   u16ContextID                  index to distinguish caller context
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableDeleteEntries(void)
{
   trEventElement *pCurrentEntry = &pOsalData->rEventElement[0];
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;
   tU32 used = pOsalData->EventTable.u32UsedEntries;


   if(LockOsal(&pOsalData->EventTable.rLock) == OSAL_OK)
   {
      /* search the whole table */
      while ( pCurrentEntry < &pOsalData->rEventElement[used])
      {
         if (pCurrentEntry->bIsUsed == TRUE)
         {
               s32DeleteCounter++;
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = s32DeleteCounter;
         }
         else
         {
            pCurrentEntry++;
         }
      }

      UnLockOsal(&pOsalData->EventTable.rLock);
   }
   return(s32ReturnValue);
}

void vTraceEventInfo(trEventElement *pCurrentEntry,tU8 Type,tU32 u32Level,tU32 u32Error,tCString coszName)
{
    char au8Buf[26 + OSAL_C_U32_MAX_NAMELENGTH + 8];
    tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
    tS32 s32Len;

    if((Type == POST_OPERATION)||(Type == WAIT_OPERATION))
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO2);
    }
    else
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO);
    }
    OSAL_M_INSERT_T8(&au8Buf[1],(tU8)Type);
    bGetThreadNameForTID(&au8Buf[2],8,(int)u32Val);
    OSAL_M_INSERT_T32(&au8Buf[10],u32Val);
 
    if(pCurrentEntry)
    {
        OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry);
        OSAL_M_INSERT_T32(&au8Buf[18],pCurrentEntry->u32EventBitField);
    }
    else
    {
        OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
        OSAL_M_INSERT_T32(&au8Buf[18],u32Val);
    }
    OSAL_M_INSERT_T32(&au8Buf[22],u32Error);

    s32Len = 26;

    if(pCurrentEntry)
    {
       if(Type == POST_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32PostMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32PostFlag);
          s32Len += 8;
       }
       else if(Type == WAIT_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32WaitMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32WaitEnFlags);
          s32Len += 8;
       }
    }
    else
    {
          OSAL_M_INSERT_T32(&au8Buf[26],u32Error);
          OSAL_M_INSERT_T32(&au8Buf[30],u32Error);
    }

    u32Val = 0;
    if(coszName)
    {
      u32Val = strlen(coszName);
      if( u32Val > OSAL_C_U32_MAX_NAMELENGTH )
      {
         u32Val = OSAL_C_U32_MAX_NAMELENGTH;
      }
      memcpy(&au8Buf[s32Len],coszName,u32Val);
      au8Buf[s32Len + OSAL_C_U32_MAX_NAMELENGTH -1] = 0;
    }

    s32Len += u32Val;
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG, u32Level,au8Buf,s32Len);
}

/*****************************************************************************
*
* FUNCTION: u32ProcessRecIpcSignal
*
* DESCRIPTION: this function forwards a signal from a different process to an 
*              process internel thread
*
* PARAMETER:   tU32   signal number
*              tS32   additional signal information (contain index of related 
*                     OSAL event object)
*
* RETURNVALUE: u32ReturnValue   OSAL error code
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tU32 u32ProcessRecIpcSignal(tS32 s32Sig,siginfo_t* pInfo)
{
   tS32 s32Signal = (tS32)0xffffffff;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   union sigval value = {0};
   trEventElement *pCurrentEntry = NULL;
#ifdef SIGNAL_TRACE_EVENT
   TraceString("u32ProcessRecIpcSignal %d in Process:%d",s32Sig,getpid());
#endif
   s32Signal = s32Sig + 4;
   if(s32Signal != (tS32)0xffffffff)
   {
      /* get Index of t */
      pCurrentEntry = &pOsalData->rEventElement[pInfo->si_value.sival_int];
#ifdef SIGNAL_TRACE_EVENT
      TraceString("Forward 0x%x for %d Tsk:%d ",pCurrentEntry->u32PrcSndMask,pCurrentEntry,pCurrentEntry->s32RecTsk);
#endif
      /* we use a lock to change pCurrentEntry->u32PrcSndMask for the unusual case 
         if this event is used from more than one other process */
      if( LockOsal( &pOsalData->EventTable.rPrcLock) == OSAL_OK )
      {
         value.sival_int = pCurrentEntry->u32PrcSndMask;
         UnLockOsal(&pOsalData->EventTable.rPrcLock);
      }
      for(;;)
      {
         if(pthread_sigqueue(pCurrentEntry->s32RecTsk,s32Signal,value) == -1)
         {
            if(errno != EAGAIN)
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
               TraceString("Forward signal Tsk:%d sigqueue error %d for %s to PRC:%d",pCurrentEntry->s32RecTsk,errno,pCurrentEntry->szName,pCurrentEntry->s32RecProc);
               break;
            }
            TraceString("Tsk:%d sigqueue retry for %s to PRC:%d",pCurrentEntry->s32RecTsk,pCurrentEntry->szName,pCurrentEntry->s32RecProc);
            OSAL_s32ThreadWait(100);
         }
         else
         {
            break;
         }
      }//for(;;)
   }
   return u32ErrorCode;
}

/*****************************************************************************
*
* FUNCTION: u32SendSignal
*
* DESCRIPTION: this function sends the signal for the relatedevent 
*
* PARAMETER:   trEventElement pointer to OSAL event object
*              tS32           signal to send
*              tU32           additional information sent with signal
*
* RETURNVALUE: u32ReturnValue   OSAL error code
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tU32 u32SendSignal(trEventElement *pCurrentEntry,tS32 s32Signal, tU32 mask )
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tS32 s32Ret;
   union sigval value = {0};

    /* store event mask to post */
    value.sival_int = mask;
    /* use loop to handle full signal receiver queue */
    for(;;)
    {
       if (pCurrentEntry->s32RecProc == (tU32)OSAL_ProcessWhoAmI()) /* check for receiver is in same process as sender */
       {
#ifdef SIGNAL_TRACE_EVENT
          TraceString("Tsk:%d pthread_sigqueue post signal %d with 0x%x for %s to Task:%d,",pthread_self(),s32Signal,mask,pCurrentEntry->szName,pCurrentEntry->s32RecTsk);
#endif
          /* send mask to receiver task */
          s32Ret = pthread_sigqueue(pCurrentEntry->s32RecTsk,s32Signal,value);
       }
       else
       {
#ifdef SIGNAL_TRACE_EVENT
          TraceString("Tsk:%d sigqueue post signal %d with 0x%x for %s to PRC:%d",pCurrentEntry->s32RecTsk,s32Signal,mask,pCurrentEntry->szName,pCurrentEntry->s32RecProc);
#endif
          if( LockOsal( &pOsalData->EventTable.rPrcLock) == OSAL_OK )
          {
             /* store mask in OSAL event struct */
             pCurrentEntry->u32PrcSndMask |= mask;
             UnLockOsal(&pOsalData->EventTable.rPrcLock);
          }
          /* overwrite  value.sival_int -> send index of OSAL struct to 
             process of the receiver task */
          value.sival_int = pCurrentEntry->u32Index;
          s32Ret = sigqueue(pCurrentEntry->s32RecProc,s32Signal-4,value);
       }
       if(s32Ret == -1)
       {
          if(errno != EAGAIN)
          {
             u32ErrorCode = u32ConvertErrorCore(errno);
             TraceString("Tsk:%d sigqueue error %d for %s to PRC:%d",pCurrentEntry->s32RecTsk,errno,pCurrentEntry->szName,pCurrentEntry->s32RecProc);
             break;
          }
          TraceString("Tsk:%d sigqueue retry for %s to PRC:%d",pCurrentEntry->s32RecTsk,pCurrentEntry->szName,pCurrentEntry->s32RecProc);
          OSAL_s32ThreadWait(100);
       }
       else
       {
           break;
       }
    }// end for(;;)
    return u32ErrorCode;
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventCreate
*
* DESCRIPTION: this function creates an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to create.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventCreate( tCString coszName,
                          OSAL_tEventHandle* phEvent )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   /* The name is not NULL */
   if (coszName && phEvent) /* FIX 15/10/2002 */
   {
      /* The Name is not too long */
      if( OSAL_u32StringLength( coszName ) <= (tU32)LINUX_C_EVENT_MAX_NAMELENGHT )
      {

         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            /* The name is not already in use*/
            pCurrentEntry = tEventTableSearchEntryByName( coszName );
            if(pCurrentEntry == OSAL_NULL)
            {
               pCurrentEntry = tEventTableGetFreeEntry();
               if (pCurrentEntry!= OSAL_NULL)
               {
               //      tU32 u32Count = 0;
                     /* success */
                     pCurrentEntry->s32RecProc = -1;
                     pCurrentEntry->bIsUsed   = TRUE;
                     pCurrentEntry->bToDelete = FALSE;
                     pCurrentEntry->u16OpenCounter   = 1;
                     pCurrentEntry->u32EventBitField = 0;
                     pCurrentEntry->bTraceFlg = pOsalData->bEvTaceAll;
                     if(pOsalData->szEventName[0] != 0)
                     {
                        if(!strncmp(coszName,pOsalData->szEventName,strlen(coszName)))
                        {
                           pCurrentEntry->bTraceFlg = TRUE;
                        }
                     }
                     pCurrentEntry->u32PrcSndMask = 0;
                     (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                               coszName, OSAL_C_U32_MAX_NAMELENGTH );

                     tEvHandle* pTemp = (tEvHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&EvMemPoolHandle);
                     if(pTemp)
                     {
                         pTemp->pEntry   = pCurrentEntry;
                         pTemp->s32Tid   = OSAL_ThreadWhoAmI();
            /*             pCurrentEntry->rEvQDat[0].s32Tid      = OSAL_ThreadWhoAmI();
                         pCurrentEntry->rEvQDat[0].rData.mask  = 0;
                         pCurrentEntry->rEvQDat[0].rData.Flags = -1;
                         for(u32Count=1;u32Count<MAX_EVENT_OPEN;u32Count++)
                         {
                            pCurrentEntry->rEvQDat[u32Count].rData.mask  = 0;
                            pCurrentEntry->rEvQDat[u32Count].rData.Flags = -1;
                            pCurrentEntry->rEvQDat[u32Count].s32Tid      = 0;
                         }*/
                         *phEvent = (OSAL_tEventHandle)pTemp;
                         s32ReturnValue = OSAL_OK;
                         pOsalData->u32EvtResCount++;
                         if(pOsalData->u32MaxEvtResCount < pOsalData->u32EvtResCount)pOsalData->u32MaxEvtResCount = pOsalData->u32EvtResCount;
                         if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
                         {
                            vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_DATA,u32ErrorCode,coszName);
                         }
                     }
                     else
                     {
                        FATAL_M_ASSERT_ALWAYS();
                        u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
                     }
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }
             UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode == OSAL_E_ALREADYEXISTS)
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventDelete
*
* DESCRIPTION: this function removes an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventDelete(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
//   tU32 u32Count = 0;

   if (coszName)
   {
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
         pCurrentEntry = tEventTableSearchEntryByName( coszName );
         if( pCurrentEntry != OSAL_NULL )
         {
            if( pCurrentEntry->u16OpenCounter == 0 )
            {
                  pCurrentEntry->bIsUsed = FALSE;
                  if(pCurrentEntry->bWaitFlag)
                  {
                     TraceString("Event %s try to delete by Task %d during wait !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
                     /* send signal to stop waiting */
              //       u32SendSignal(pCurrentEntry,OSAL_SIGNAL_EVENT_REPLACE,0);
                     OSAL_s32ThreadWait(500);
                  }
                  pCurrentEntry->s32RecProc = -1;
                  s32ReturnValue = OSAL_OK;
                  pOsalData->u32EvtResCount--;

            /*      tS32 s32Val = OSAL_ThreadWhoAmI();
                  for(u32Count=0;u32Count<MAX_EVENT_OPEN;u32Count++)
                  {
                         if(pCurrentEntry->rEvQDat[u32Count].s32Tid == s32Val)
                         {
                             pCurrentEntry->rEvQDat[u32Count].s32Tid      = 0;
                             pCurrentEntry->rEvQDat[u32Count].rData.mask  = 0;
                             pCurrentEntry->rEvQDat[u32Count].rData.Flags = -1;
                             break;
                         }
                  }*/
            }
            else
            {
               pCurrentEntry->bToDelete = TRUE;
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
           u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->EventTable.rLock);
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
      {
         vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_DATA,u32ErrorCode,coszName);
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventOpen
*
* DESCRIPTION: this function returns a valid handle to an OSAL event
*              already created.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventOpen(tCString coszName, OSAL_tEventHandle* phEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
 //  tU32 u32Count = 0;

   /* Parameter check */
   if( coszName && phEvent )
   {
      if( OSAL_u32StringLength(coszName) <= (tU32)LINUX_C_EVENT_MAX_NAMELENGHT )
      {
         /* Lock the event table */
         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            pCurrentEntry = tEventTableSearchEntryByName(coszName);

            if (pCurrentEntry != OSAL_NULL)
            {
               if( !pCurrentEntry->bToDelete )
               {
                  pCurrentEntry->u16OpenCounter++;

                  tEvHandle* pTemp = (tEvHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&EvMemPoolHandle);
                  if(pTemp)
                  {
/*                         for(u32Count=1;u32Count<MAX_EVENT_OPEN;u32Count++)
                         {
                             if(pCurrentEntry->rEvQDat[u32Count].s32Tid == 0)break;
                         }
                         pCurrentEntry->rEvQDat[u32Count].s32Tid = OSAL_ThreadWhoAmI();*/

                         pTemp->pEntry   = pCurrentEntry;
                         pTemp->s32Tid   = OSAL_ThreadWhoAmI();
                         *phEvent = (OSAL_tEventHandle)pTemp;
                         s32ReturnValue = OSAL_OK;
                  }
                  else
                  {
                      FATAL_M_ASSERT_ALWAYS();
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_BUSY;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }
            /* unlock the Event table */
            UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_DATA,u32ErrorCode,coszName);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventClose
*
* DESCRIPTION: this function closes an OSAL event.
*
* PARAMETER:   hEvent (I)
*                 event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventClose(OSAL_tEventHandle hEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
 //  tU32 u32Count = 0;

   /* Parameter check */
   if( hEvent )
   {
      /* Lock the event table */
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
          pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;

         if((pCurrentEntry != OSAL_NULL )&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
            if( pCurrentEntry->u16OpenCounter > 0 )
            {
               pCurrentEntry->u16OpenCounter--;
               s32ReturnValue = OSAL_OK;
               // @@@@ kos2hi: insert POSIX delete mechanism
               if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
               {
                     pCurrentEntry->bIsUsed = FALSE;
                     if(pCurrentEntry->bWaitFlag)
                     {
                        TraceString("Event %s try to delete by Task %d during wait !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
                        /* send signal to stop waiting */
                  //      u32SendSignal(pCurrentEntry,OSAL_SIGNAL_EVENT_REPLACE,0);
                        OSAL_s32ThreadWait(500);
                     }
                     pCurrentEntry->bToDelete = FALSE;
                     pCurrentEntry->s32RecProc = -1;
                     pOsalData->u32EvtResCount--;

         /*            tS32 s32Val = ((tEvHandle*)hEvent)->s32Tid ;
                     for(u32Count=0;u32Count<MAX_EVENT_OPEN;u32Count++)
                     {
                         if(pCurrentEntry->rEvQDat[u32Count].s32Tid == s32Val)
                         {
                             pCurrentEntry->rEvQDat[u32Count].s32Tid      = 0;
                             pCurrentEntry->rEvQDat[u32Count].rData.mask  = 0;
                             pCurrentEntry->rEvQDat[u32Count].rData.Flags = -1;
                             break;
                         }
                     }*/
               }
               if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&EvMemPoolHandle,(tEvHandle*)hEvent) == OSAL_ERROR)
               {    NORMAL_M_ASSERT_ALWAYS();  }
            }
            else
            {
               u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;  // todo Why not OSAL_E_DOESNOTEXIST;
         }
         /* UnLock the Event table */
         UnLockOsal( &pOsalData->EventTable.rLock );
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA)&&(pCurrentEntry)))
      {
         vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_DATA,u32ErrorCode,pCurrentEntry->szName);
      }
   }

   return( s32ReturnValue );
}

tU32 u32CheckCondition(trEventElement* pCurrentEntry,
                       OSAL_tEventMask   mask,
                       OSAL_tenEventMaskFlag enFlags )
{
  tU32 u32Mask = 0;
  /* check if return condition is full filled */
//  TraceString("u32CheckCondition mask: 0x%x EventBitField  0x%x start",mask,pCurrentEntry->u32EventBitField);
  switch (enFlags)
  {
    case OSAL_EN_EVENTMASK_AND:
         u32Mask = ( ( pCurrentEntry->u32EventBitField & mask ) == mask);
        break;
    case OSAL_EN_EVENTMASK_OR:
         u32Mask = ( pCurrentEntry->u32EventBitField & mask);
        break;                              // this is only for the trace message end
    default:
         NORMAL_M_ASSERT_ALWAYS();
        break;
  }
// TraceString("u32CheckCondition mask: 0x%x EventBitField  0x%x return %d ",mask,pCurrentEntry->u32EventBitField,u32Mask);
  return u32Mask;
}

tU32 u32SignalSetBitFields(trEventElement *pCurrentEntry,
                         OSAL_tEventMask   mask,
                         tU32 u32Signal,
                         tS32 s32Flag)
{
    tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(u32Signal)
   {
//   TraceString("vSignalSetBitFields Signal:0x%x EventBitField 0x%x start",u32Signal,pCurrentEntry->u32EventBitField);
      switch(u32Signal)
      {
       case OSAL_SIGNAL_EVENT_AND:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & mask;
           break;
       case OSAL_SIGNAL_EVENT_OR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField | mask;
           break;
       case OSAL_SIGNAL_EVENT_XOR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField ^ mask;
           break;
       case OSAL_SIGNAL_EVENT_REPLACE:
            pCurrentEntry->u32EventBitField = mask;
           break;
       default:
            u32ErrorCode = OSAL_E_INVALIDVALUE;
          break;
      }
//      TraceString("vSignalSetBitFields Signal:0x%x EventBitField 0x%x end",u32Signal,pCurrentEntry->u32EventBitField);
   }
   else
   {
//      TraceString("vSignalSetBitFields Flag:0x%x EventBitField 0x%x start",s32Flag,pCurrentEntry->u32EventBitField);
      switch( s32Flag )
      {
       case OSAL_EN_EVENTMASK_AND:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & mask;
           break;
       case OSAL_EN_EVENTMASK_OR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField | mask;
           break;
       case OSAL_EN_EVENTMASK_XOR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField ^ mask;
           break;
       case OSAL_EN_EVENTMASK_REPLACE:
            pCurrentEntry->u32EventBitField = mask;
           break;
       default:
            u32ErrorCode = OSAL_E_INVALIDVALUE;
          break;
      }
//      TraceString("vSignalSetBitFields Flag:0x%x EventBitField 0x%x end",s32Flag,pCurrentEntry->u32EventBitField);
   }
   return u32ErrorCode;
}


#define TEST2

/*****************************************************************************
*
* FUNCTION: OSAL_s32EventWait
*
* DESCRIPTION: This function waits for an OSAL event to occur,
*              where an event occurrence means the link operation
*              (given by enFlags) between the EventField present
*              in the EventGroup structure and the provided EventMask
*              matches. Allowed link operation are AND/OR
*              So the event resets the calling thread only if within
*              the requested timeout one of the following conditions
*              is verified:
*                 EventMask || EventField is TRUE or
*                 EventMask && EventField is true
*              depending on the requested link operation
*
*
* PARAMETER:   hEvent (I)
*                 event handle.
*              mask (I)
*                 event mask.
*              enFlag (I)
*                 event flag.
*              msec (I)
*                 waiting time.
*              pResultMask (->O)
*                 pointer to the previous event mask.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventWait(OSAL_tEventHandle      hEvent,
                       OSAL_tEventMask        mask,
                       OSAL_tenEventMaskFlag  enFlags,
                       OSAL_tMSecond          msec,
                       OSAL_tEventMask        *pResultMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
   OSAL_tMSecond s32Timeout = msec;
   struct timespec tim = {0,0};
   siginfo_t rInfo;
   OSAL_tEventMask u32Mask = 0;
   tBool bRepeat = TRUE;
   tU32  u32RetryCnt = 0;
   sigset_t rOldSet = {0};
   tBool bChecked = FALSE;

   /* parameter check */
   if((hEvent) && (hEvent != OSAL_C_INVALID_HANDLE)
    &&((enFlags == OSAL_EN_EVENTMASK_AND)||(enFlags == OSAL_EN_EVENTMASK_OR) ))
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         pCurrentEntry->u32WaitMask = mask;
         pCurrentEntry->u32WaitEnFlags = (tU32)enFlags;
         if( pCurrentEntry->bWaitFlag == TRUE )
         {
            u32ErrorCode = OSAL_E_EVENTINUSE;
            NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
         }
         else
         {
            pCurrentEntry->bWaitFlag = TRUE;
            /* step 1 check for first wait call */
            if(pCurrentEntry->s32RecProc == -1)
            {
               /* add new signals to mask of calling task first to avoid loosing a signal */
               sigemptyset(&pCurrentEntry->rSet);
               sigaddset(&pCurrentEntry->rSet, OSAL_SIGNAL_EVENT_AND);
               sigaddset(&pCurrentEntry->rSet, OSAL_SIGNAL_EVENT_OR);
               sigaddset(&pCurrentEntry->rSet, OSAL_SIGNAL_EVENT_XOR);
               sigaddset(&pCurrentEntry->rSet, OSAL_SIGNAL_EVENT_REPLACE);
               if (pthread_sigmask( SIG_BLOCK, &pCurrentEntry->rSet, &rOldSet) != 0)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
               LockOsal( &pOsalData->EventTable.rLock );
               /* enter receiver task data */
               pCurrentEntry->s32RecProc = OSAL_ProcessWhoAmI();
               pCurrentEntry->s32RecTsk = pthread_self();

               /* check if event is already set */
               if(pCurrentEntry->bUpdate == TRUE)
               {
                  /* check if return condition is full filled */
                  u32Mask = u32CheckCondition(pCurrentEntry,mask,enFlags );
                  pCurrentEntry->bUpdate = FALSE;
                  bChecked = TRUE;
#ifdef SIGNAL_TRACE_EVENT
                  TraceString("Tsk:%d Event Wait bUpdate First Time PRC=%d 0x%x for %s \0",pthread_self(),OSAL_ProcessWhoAmI(),mask,pCurrentEntry->szName);
#endif
               }
#ifdef SIGNAL_TRACE_EVENT
               else
               {
                  TraceString("Tsk:%d Event Wait first time PRC=%d 0x%x for %s \0",pthread_self(),OSAL_ProcessWhoAmI(),mask,pCurrentEntry->szName);
               }
#endif
               /* check  if Post message is called at the same time*/
               if(pCurrentEntry->bPost == TRUE)
               {
                   pCurrentEntry->bUpdate = TRUE;
               }
               UnLockOsal( &pOsalData->EventTable.rLock );
            }
            /* when receiver has send an event to itself */
            if((pCurrentEntry->bUpdate == TRUE)||(pCurrentEntry->u32EventBitField))
            {
               if(!bChecked)
               {
#ifdef SIGNAL_TRACE_EVENT
                  TraceString("Tsk:%d Event Wait bUpdate PRC=%d 0x%x for %s \0",pCurrentEntry->s32RecTsk,pCurrentEntry->s32RecProc,mask,pCurrentEntry->szName);
#endif
                  /* check if return condition is full filled */
                  u32Mask = u32CheckCondition(pCurrentEntry,mask,enFlags );
                  pCurrentEntry->bUpdate = FALSE;
               }
            }

            /* step 2 check if wait condition is not fullfilled */
            if(u32Mask == 0)
            {
               if(s32Timeout != OSAL_C_TIMEOUT_FOREVER)
               {
                  /* prepare timeout value if needed */
                  if(s32Timeout)
                  {
                     /* check for minimum resolution time*/
                     if(s32Timeout < pOsalData->u32TimerResolution)
                     {
                        s32Timeout = pOsalData->u32TimerResolution;
                     }
                     /* Calculate the relative timeout value by adding the timeout argument with current time */
                     tim.tv_sec  += s32Timeout/1000;
                     tim.tv_nsec += (s32Timeout%1000)*1000000;
                     if(tim.tv_nsec >= 1000000000)
                     {
                        tim.tv_sec += 1;
                        tim.tv_nsec -= 1000000000;
                     }
                  }
               }
               /* start while loop until mask condition reached */
               do {
#ifndef NO_SIGNALAWARENESS
                     /* start while loop to handle other diturbing signals */
                     do {
#endif
                        /* for OSAL_C_TIMEOUT_FOREVER to decide which API has to be used*/
                        if(s32Timeout == OSAL_C_TIMEOUT_FOREVER)
                        {
                           /* use sigwaitinfo */
#ifdef SIGNAL_TRACE_EVENT
                           TraceString("Tsk:%d sigwait 0x%x for %s \0",pCurrentEntry->s32RecTsk,mask,pCurrentEntry->szName);
#endif
                           s32ReturnValue = sigwaitinfo(&pCurrentEntry->rSet,&rInfo);
                        }
                        else
                        {
                            /* use sigtimedwait */
#ifdef SIGNAL_TRACE_EVENT
                           TraceString("Tsk:%d sigtimedwait 0x%x for %s with timeout %d msec \0",pCurrentEntry->s32RecTsk,mask,pCurrentEntry->szName,s32Timeout);
#endif 
                           s32ReturnValue = sigtimedwait(&pCurrentEntry->rSet,&rInfo,&tim);
                        }
                        if(s32ReturnValue != -1)
                        {
 #ifdef SIGNAL_TRACE_EVENT
                           TraceString("Tsk:%d received Signal %d for %s \0",pCurrentEntry->s32RecTsk,s32ReturnValue,pCurrentEntry->szName);
#endif
                           /* check for  right signal */
                           if((s32ReturnValue >= OSAL_SIGNAL_EVENT_REPLACE)&&(s32ReturnValue <= OSAL_SIGNAL_EVENT_AND))
                           {
                              /* evaluate incoming signal and store related bitmask */
                              u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,rInfo.si_value.sival_int/*mask*/,(OSAL_tenEventMaskFlag)s32ReturnValue/*Signal*/,0);

#ifdef TEST1
                              /* signal request for pending signals before return to app*/
                              s32Timeout  = 0;
                              tim.tv_sec  = 0;
                              tim.tv_nsec = 0;
                              if(pCurrentEntry->bTraceFlg == TRUE)
                              {
                                  TraceString("OSAL_s32EventWait %d retry Bitfield Status: 0x%x for Event %s",u32RetryCnt,pCurrentEntry->u32EventBitField,pCurrentEntry->szName);
                              }
                              u32RetryCnt++;
                              /* limit mx number of signals for one wait to 5*/
                              if(u32RetryCnt > 5) bRepeat = FALSE;
#endif

#ifdef TEST2
                              /* check for further pending signals */
                              sigset_t sigset;
                              if((sigpending(&sigset) == 0 )&&(u32RetryCnt < 5))
                              {
                                 if((sigismember(&sigset, OSAL_SIGNAL_EVENT_OR)==1) || (sigismember(&sigset, OSAL_SIGNAL_EVENT_AND) == 1))
                                 {
                                    if(pCurrentEntry->bTraceFlg == TRUE)
                                    {
                                       TraceString("OSAL_s32EventWait %d retry Bitfield Status: 0x%x for Event %s",u32RetryCnt,pCurrentEntry->u32EventBitField,pCurrentEntry->szName);
                                    }
                                    /* catch next signal */
                                 }
                                 else
                                 {
                                     bRepeat = FALSE;
                                 }
                                 u32RetryCnt++;
                              }
                              else
                              {
                                 /* leave signal handling while loop*/
                                 bRepeat = FALSE;
                              }
#endif
                           }
                           else
                           {
                              TraceString("Tsk:%d received unexpected Signal %d for %s \0",pCurrentEntry->s32RecTsk,s32ReturnValue,pCurrentEntry->szName);
                           }
                        }
                        else if(errno == EAGAIN)
                        {
#ifdef TEST1
                           if(u32RetryCnt == 1)
#endif
                           {
#ifdef SIGNAL_TRACE_EVENT
                              TraceString("Tsk:%d sigtimedwait timeout \0",pCurrentEntry->s32RecTsk);
#endif
                              u32ErrorCode = OSAL_E_TIMEOUT;
                           }
                           /* leave signal handling while loop*/
                           bRepeat = FALSE;
                        }
                        else if(errno != EINTR)
                        {
#ifdef TEST1
                           if(u32RetryCnt > 1)
#endif
                           {
                              /* signal queue wait is failed not because of signal */
                              u32ErrorCode = u32ConvertErrorCore(errno);
                              if(errno == EINVAL)
                              {  TraceString("Wrong Timeout value for event"); }
                           }
                           /* leave signal handling while loop*/
                           bRepeat = FALSE;
                        }
                        /* We are here because of signal. lets try again. */
#ifndef NO_SIGNALAWARENESS
                     } while(bRepeat == TRUE);
#endif
                     /* prove if object is still in use */
                     if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
                     {
                        u32ErrorCode = OSAL_E_TIMEOUT;
                        u32Mask++;// leave do while loop
                        NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
                     }
                     /* check if return condition is full filled, but further signals pending */
                     if((u32Mask = u32CheckCondition(pCurrentEntry,mask,enFlags )) == 0)
                     {
                        if((pCurrentEntry->bTraceFlg == TRUE)&&(OSAL_E_TIMEOUT != u32ErrorCode))
                        {
                           vTraceEventInfo(pCurrentEntry,WAIT_OPERATION_STEP,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
                        }
                     }
               }while((0 == u32Mask) && (OSAL_E_TIMEOUT != u32ErrorCode));
            }// if(u32Mask == 0)
#ifdef SIGNAL_TRACE_EVENT
            else
            {
               TraceString("Tsk:%d condition fullfilled without signal \0",pCurrentEntry->s32RecTsk);
            }
#endif
            pCurrentEntry->bWaitFlag = FALSE;
         }
      }
      else // if( pCurrentEntry )
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }//if( pCurrentEntry )
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode != OSAL_E_TIMEOUT)
      {
         tCString coszName;
         if(pCurrentEntry)coszName = pCurrentEntry->szName;
         else coszName = "NO_NAME";
         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      else
      {
         if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE)&&(!pOsalData->bEvTaceAllWithoutTimeout))
         {
            vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
      }
   }
   else
   {
      s32ReturnValue = (tS32)OSAL_OK;
      if(pCurrentEntry)//lint
      {
         if( pResultMask != NULL )
         {
            *pResultMask =mask & pCurrentEntry->u32EventBitField;

            if(pCurrentEntry->bTraceFlg == TRUE)
            {
               OSAL_M_INSERT_T8(&cBuffer[0],(tU8)OSAL_TSK_FLG_INFO3);
               snprintf(&cBuffer[1],99,"OSAL_s32EventWait return with result mask 0x%x for Event %s",*pResultMask,pCurrentEntry->szName);
               LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG,TR_LEVEL_FATAL,cBuffer,100);
            }
         }
         if(pCurrentEntry->bTraceFlg == TRUE )
         {
            vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventPost
*
* DESCRIPTION: this function set an event in OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventPost( OSAL_tEventHandle hEvent,
                        OSAL_tEventMask   mask,
                        OSAL_tenEventMaskFlag enFlags )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
   tS32 s32Signal = 0;
   tS32 s32Tsk = (tS32)pthread_self();
   tBool bSendSignal = TRUE;

   /* parameter check */
   if( (hEvent) && (hEvent != OSAL_C_INVALID_HANDLE) )
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         pCurrentEntry->u32PostMask = mask;
         pCurrentEntry->u32PostFlag = (tU32)enFlags;

         /* check for no active receiver task , OSAL_s32EventWait was not called yet */
         if(pCurrentEntry->s32RecProc == -1) 
         {
            pCurrentEntry->bPost = TRUE;
            /* store event mask info and Lock, because we don' t know if wait 
               is called by receiver task at the same time*/
            LockOsal( &pOsalData->EventTable.rLock );
            /* check if in OSAL_s32EventWait s32RecProc is set before lock was taken*/
            if(pCurrentEntry->s32RecProc == -1)
            {
#ifdef SIGNAL_TRACE_EVENT
               TraceString("Tsk:%d signal 0x%x store for %s \0",s32Tsk, mask,pCurrentEntry->szName);
#endif
               u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,mask,0,enFlags );
               pCurrentEntry->bUpdate = TRUE;
               bSendSignal = FALSE;
            }
#ifdef SIGNAL_TRACE_EVENT
            else
            {
               TraceString("Tsk:%d signal 0x%x not stored for %s \0",s32Tsk, mask,pCurrentEntry->szName);
            }
#endif
            pCurrentEntry->bPost = FALSE;
            UnLockOsal( &pOsalData->EventTable.rLock );
            s32ReturnValue = OSAL_OK;
         }
         else if(pCurrentEntry->s32RecTsk == s32Tsk)/* check for clear event */
         {
            /* Lock not needed, because we know post is called by receiver task */
            u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,mask,0,enFlags );
            pCurrentEntry->bUpdate = TRUE; //for check of an event sent to myself*/
            bSendSignal = FALSE;
            s32ReturnValue = OSAL_OK;
#ifdef SIGNAL_TRACE_EVENT
            TraceString("Tsk:%d clear event 0x%x for %s \0",s32Tsk,mask,pCurrentEntry->szName);
#endif
         }
         else 
         {
            bSendSignal = TRUE;
         }
         if(bSendSignal == TRUE)
         {
            switch( enFlags )
            {
                case OSAL_EN_EVENTMASK_AND:
                     s32Signal = OSAL_SIGNAL_EVENT_AND;
                    break;
                case OSAL_EN_EVENTMASK_OR:
                     s32Signal = OSAL_SIGNAL_EVENT_OR;
                    break;
                case OSAL_EN_EVENTMASK_XOR:
                     s32Signal = OSAL_SIGNAL_EVENT_XOR;
                    break;
                case OSAL_EN_EVENTMASK_REPLACE:
                     s32Signal = OSAL_SIGNAL_EVENT_REPLACE;
                    break;
                default:
                     u32ErrorCode = OSAL_E_INVALIDVALUE;
                    break;
            }
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               if((u32ErrorCode = u32SendSignal(pCurrentEntry,s32Signal,mask )) == OSAL_E_NOERROR)
               {
                  s32ReturnValue = OSAL_OK;
               }
            }
         }//if(bSendSignal == TRUE)
      }
      else // if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed))
      {
         u32ErrorCode = OSAL_E_DOESNOTEXIST;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE ))
      {
         if(pOsalData->bEvTaceAllWithoutTimeout)
         {
            if((mask == 0xffffffff)&&(enFlags == OSAL_EN_EVENTMASK_AND))
            {
                /* clear of a timeout is happened */
            }
            else
            {
               vTraceEventInfo(pCurrentEntry,POST_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
            }
         }
         else
         {
            vTraceEventInfo(pCurrentEntry,POST_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventStatus
*
* DESCRIPTION: this function returns the status of an OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventStatus(OSAL_tEventHandle hEvent,
                         OSAL_tEventMask   mask,
                         OSAL_tEventMask   *pMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   if( (hEvent) && (hEvent != OSAL_C_INVALID_HANDLE) )
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         /* check for status directly after post -> force changing bitmask */
         if(pCurrentEntry->s32RecTsk != pthread_self())
         {
              pthread_yield();
         }

         *pMask = (mask & pCurrentEntry->u32EventBitField);
         s32ReturnValue = OSAL_OK;
      }
      else
      {
         s32ReturnValue = OSAL_ERROR;
         u32ErrorCode   = OSAL_E_DOESNOTEXIST;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE ))
      {
         vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
      }
   }
   return( s32ReturnValue );
}




#ifdef __cplusplus
}
#endif

#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
