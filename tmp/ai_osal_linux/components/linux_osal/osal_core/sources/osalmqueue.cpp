/*****************************************************************************
| FILE:         osalmqueue.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Message Queue-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
| 13.12.12  | Fix for OSAL MQ priority   | sja3kor
|           |  issue.(CFAGII-497)        | 
| 17.12.12  | Fix for MMS CFAGII-501.    |sja3kor 
|           | Boundary check condition   |
|           | changed.                   |          
| --.--.--  | ----------------           | -------, -----
| 16.01.13  |Fix for Wrong timeout for   |sja3kor 
|           |Linux messagequeue post/wait|
|           |CFAGII-506                  |          
|21.01.13   |Fix for MMS GMNGA48007      |SWM2KOR        
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"


#ifdef __cplusplus
extern "C" {
#endif



/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define  LINUX_C_U32_MQUEUE_ID                   ((tU32)0x51554555) 
#define  USER_HANDLE_MAGIC                         ((tU32)0x75686D71) 

#define  NOTATHREAD                                  ((tS32)-1)

#ifdef OSAL_SHM_MQ
#define  LINUX_C_MQ_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH + 2)
#else
#define  LINUX_C_MQ_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH)
#endif


//#define NOTEMPTY_PATTERN  0x01
//#define BLOCKING_PATTERN  0x10
//#define LINUX_MQ
#define NR_OF_REC_MQ 60  // MAX 53 navigation


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
typedef struct{
     int  u32MqIdx;
     tU32 u32MemIdx;
     tU32 u32PrcIdx;
     tU32 u32PrcCnt;
}trResToPidMap2;


/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
tS32 s32Pid = 0;
tBool bTimOutMqSvActive = FALSE;
pthread_once_t  SvSecTimer_is_initialized = PTHREAD_ONCE_INIT;

static trResToPidMap2* prLiMqMap=NULL;
static tS32* pMqRefHandle= NULL;


static tBool bTraceHandle = TRUE;
static trMqueueElement* arRecMq[NR_OF_REC_MQ];
tU32 u32RecMqCount = 0;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
extern uintptr_t* pu32GetSharedBaseAdress(void);
extern tS32 s32GetPrcIdx(tU32 u32PrcId);
uintptr_t GetMemPoolOffset(trHandleMpf* pHandle, uintptr_t* pU32Mem);

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
tS32 s32MqueueTableCreate(tVoid);
tS32 s32MqueueTableDeleteEntries(void);

static trMqueueElement* tMqueueTableSearchEntryByName(tCString coszName);

struct msgblock
{
    /*lint -esym(754,msgblock::u16MagicField) msgblock::u16MagicField is not referenced PQM_authorized_435*/       
     tU16 u16MagicField;   /*identify status of the following block OSAL_MSG_MAGIC     -> valid message
                                                                   OSAL_DEL_MSG_MAGIC -> deleted message
                                                                   OSAL_INV_MSG_MAGIC -> marker for begin of guard intervall */
     tU16 u16MsgOffset;    /* offset after OSAL_MSG_uHeader of the message for begin of message 1-11 bytes */
     /*lint -esym(754,msgblock::s32Offset) msgblock::s32Offset is not referenced PQM_authorized_435*/       
     tS32 s32Offset;       /* offset in the pool */
     /*lint -esym(754,msgblock::nsize) msgblock::nsize is not referenced PQM_authorized_435*/       
     tU32 nsize;           /* size in blocks need for the message*/
} ;/*lint !e612 */


void InitRecMqToList(void)
{
   int i = 0;
   for(i=0;i<NR_OF_REC_MQ;i++)
   {
         arRecMq[i] = NULL;
   }
}

void vAddRecMqToList(trMqueueElement* pEntry)
{
   int i = 0;
      for(i=0;i<NR_OF_REC_MQ;i++)
      {
         if(arRecMq[i] == NULL)
         {
            arRecMq[i] = pEntry;
            u32RecMqCount++;
            // TraceString("RECMQ PID:%d Nr:%d",getpid(),i);
            break;
         }
      }
      if(i == (NR_OF_REC_MQ-1))
      {
         TraceString("RECMQ Increase arRecMq Current Size:%d",NR_OF_REC_MQ);
      }
}

void vRemRecMqFromList(trMqueueElement* pEntry)
{
   int i = 0;
      for(i=0;i<NR_OF_REC_MQ;i++)
      {
         if(arRecMq[i] == pEntry)
         {
           arRecMq[i] = NULL;
           u32RecMqCount--;
           break;
         }
      }
}


void vCheckMqTimeout(void* Val)
{
    ((void)Val);
   if(bTimOutMqSvActive == TRUE)
   {
      int i,j = 0;
      tU32 u32Time = OSAL_ClockGetElapsedTime()/SV_RESOLUTION;
      tU32 u32Val = 0xA3A3A3A3;
      for(i=0;i<NR_OF_REC_MQ;i++)
      {
         if(arRecMq[i])
         {
            j++;
            if((arRecMq[i]->u32Timeout)&&(arRecMq[i]->u32Timeout < u32Time))
            {
               if(arRecMq[i]->u32Timeout != OSAL_C_TIMEOUT_FOREVER)
               {
                    /* Post trigger */
                    if(mq_send(prLiMqMap[arRecMq[i]->u32EntryIdx].u32MqIdx,
                                 (const char*)&u32Val,
                                  sizeof(tU32),
                                  0) == -1)
                   {
                      TraceString("Send SV Trigger to %s failed errno %d",arRecMq[i]->szName,errno);
                   }
               }
           }
        }
        if(j == (int)u32RecMqCount)break;
      }
   }
}

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
/*
20 00 00 01   27 00 00 00   02 00 00 45   00 00 00 00   00 00 00 00   70 00 ff ff   01 00 02 00   00 00 00 00   01 00 00 00
00 01 02 03   04       07   08       11   12       15   16       19   20       23   24    26 27   28       31   32       35
src   dest    size          versi co ty   s-sub d-sub   Time__Stamp   srvid regid   fncid Op act  cmdctr s sctr data
|     |       |                      |                                |             |     |
|     |       0x27 = 39 byte         type = 0x45 = ServiceData        |             |     Opcode 0x2= MethodStart
|     0x100= CCA_C_U16_APP_DIAGLOG                                    |             Function-ID 0x1= SaveTestResult
0x20= CCA_C_U16_APP_KBD                                               0x70= CCA_C_U16_SRV_DIAGLOG (ccaservice.h)
*/
tBool bFilterMsg(OSAL_trMessage* handle)
{
    tU16 u16Src,u16Dst,u16serv,u16Func;
    uintptr_t u32Offset;

    if((char)(handle->enLocation) == OSAL_EN_MEMORY_LOCAL)
    {
        u32Offset = (uintptr_t)(handle->u32Offset);
    }
    else
    {
       msgblock* ptr = (msgblock*)((uintptr_t)pu32GetSharedBaseAdress()+ handle->u32Offset);
       u32Offset = (uintptr_t)(ptr) + sizeof(msgblock)+ (tU32)ptr->u16MsgOffset ;
    }

    if(pOsalData->bTraceAllCca ==TRUE)
    {
       return TRUE;
    }

    if(pOsalData->u16CheckSrc != 0xffff)
    {
      memcpy(&u16Src,(void*)u32Offset,sizeof(tU16));
      if(pOsalData->u16CheckSrc == u16Src)
      {
        return TRUE;
      }
    }
    if(pOsalData->u16CheckDst != 0xffff)
    {
      memcpy(&u16Dst,(void*)(u32Offset + sizeof(tU16)),sizeof(tU16));
      if(pOsalData->u16CheckDst == u16Dst)
      {
        return TRUE;
      }
    }
    if(pOsalData->u16ServId != 0xffff)
    {
      memcpy(&u16serv,(void*)(u32Offset + 20),sizeof(tU16));
      if(pOsalData->u16ServId == u16serv)
      {
        return TRUE;
      }
    }
    if(pOsalData->u16FuncId != 0xffff)
    {
      memcpy(&u16Func,(void*)(u32Offset + 24),sizeof(tU16));
      if(pOsalData->u16FuncId == u16Func)
      {
        return TRUE;
      }
    }
    return FALSE;
}

void vTraceCcaMsg(OSAL_trMessage* handle, tU32 MsgSize,trMqueueElement *pCurrentEntry,tCString coszName)
{
   if(pCurrentEntry == NULL)// called by OSAL message API 
   {
      if(((char)(handle->enLocation) == OSAL_EN_MEMORY_LOCAL)
       ||((char)(handle->enLocation) == OSAL_EN_MEMORY_SHARED))
      {
         if(bFilterMsg(handle))
         {
            /* valid handle */
            TraceString("%s Task:%d Handle:%p u32Offset:%d Location:%d",
                                coszName,
                                OSAL_ThreadWhoAmI(),
                                handle,
                                handle->u32Offset,
                                handle->enLocation);
         }
      }
      else
      {
            /* invalid handle */
            TraceString("%s Task:%d Defect Handle:%p",
                               coszName,
                               OSAL_ThreadWhoAmI(),
                               handle);
      }
   }
   else
   {
     if((MsgSize == 8) /* size of CCA message */
      &&(pStdMQEl[pCurrentEntry->u32EntryIdx].u32Marker != 0xffffffff)) //magic for exclude of supervision
     {
        /* CCA message queue */
        if(((char)(handle->enLocation) == OSAL_EN_MEMORY_LOCAL)
         ||((char)(handle->enLocation) == OSAL_EN_MEMORY_SHARED))
        {
           if(bFilterMsg(handle))
           {
              /* valid handle */
               TraceString("%s Task:%d Mbx:%s Handle:%p u32Offset:%d Location:%d",
                                  coszName,
                                  OSAL_ThreadWhoAmI(),
                                  pCurrentEntry->szName,
                                  handle,
                                  handle->u32Offset,
                                  handle->enLocation);
           }
        }
        else
        {
              /* invalid handle */
              TraceString("%s Task:%d Mbx:%s Defect Handle:%p",
                                  coszName,
                                  OSAL_ThreadWhoAmI(),
                                  pCurrentEntry->szName,
                                  handle);
        }
     }
   }
}

tVoid vMqueueLiMqMapInit(tVoid)
{
    tU32    i;

    for(i = 0; i < pOsalData->u32MaxNrMqElements; i++)
    {
        prLiMqMap[i].u32MqIdx = -1;
        pMqRefHandle[i] = -1;
    }
}

void vResetEntry(tU32 u32Index)
{
    memset(&pMsgQEl[u32Index],0,sizeof(trMqueueElement));
    pMsgQEl[u32Index].u32MqueueID   = LINUX_C_U32_MQUEUE_ID;
    pMsgQEl[u32Index].u32EntryIdx   = u32Index;
    memset(&pStdMQEl[u32Index],0,sizeof(trStdMsgQueue));
}
 
/*****************************************************************************
 *
 * FUNCTION:    s32MqueueTableCreate
 *
 * DESCRIPTION: This function creates the Mqueue Table List. If
 *              there isn't space it returns a error code.
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
 tS32 s32MqueueTableCreate(tVoid)
{
   tU32 tiIndex;

   for( tiIndex=0;  tiIndex <  pOsalData->u32MaxNrProcElements ; tiIndex++ )
   {
      hMQCbPrc[tiIndex] = 0;
   } 
   pOsalData->MqueueTable.u32UsedEntries      = 0;
   pOsalData->MqueueTable.u32MaxEntries       = pOsalData->u32MaxNrMqElements;

   for( tiIndex=0;  tiIndex <  pOsalData->MqueueTable.u32MaxEntries ; tiIndex++ )
   {
      pMsgQEl[tiIndex].u32MqueueID   = LINUX_C_U32_MQUEUE_ID;
      pMsgQEl[tiIndex].bIsUsed       = FALSE;
      pMsgQEl[tiIndex].bTraceCannel  = FALSE;
      pMsgQEl[tiIndex].u16Type       = 0;
      pMsgQEl[tiIndex].u32EntryIdx   = tiIndex;
   }
   return OSAL_OK;
}

/*void CheckandCorrectMqStaticValues(void)
{
   tU32 tiIndex;
   TraceString("Task %d detects Overwritten MQ data\n",OSAL_ThreadWhoAmI());
   for( tiIndex=0;  tiIndex <  pOsalData->u32MaxNrMqElements; tiIndex++ )
   {
      if(pMsgQEl[tiIndex].u32MqueueID != LINUX_C_U32_MQUEUE_ID)
      {
          vWritePrintfErrmem("MQ Magic for Index %d overwritten with 0x%x \n",tiIndex,pMsgQEl[tiIndex].u32MqueueID);
          TraceString("MQ Magic for Index %d overwritten with 0x%x \n",tiIndex,pMsgQEl[tiIndex].u32MqueueID);
          pMsgQEl[tiIndex].u32MqueueID   = LINUX_C_U32_MQUEUE_ID;
      }
      if(pMsgQEl[tiIndex].u32EntryIdx != tiIndex)
      {
          vWritePrintfErrmem("MQ Index for Index %d overwritten with 0x%x \n",tiIndex,pMsgQEl[tiIndex].u32EntryIdx);
          TraceString("MQ Index for Index %d overwritten with 0x%x \n",tiIndex,pMsgQEl[tiIndex].u32EntryIdx);
          pMsgQEl[tiIndex].u32EntryIdx = tiIndex;
      }
   }
}*/

/*****************************************************************************
 *
 * FUNCTION:    tMqueueTableGetFreeEntry
 *
 * DESCRIPTION: this function goes through the Mqueue List and returns the
 *              first MqueueElement.In case no entries are available a new
 *              defined number of entries is attempted to be added.
 *              In case of success the first new element is returned, otherwise
 *              a NULL pointer is returned.
 *
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trMqueueElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static  trMqueueElement* tMqueueTableGetFreeEntry(tVoid)
{
   trMqueueElement *pCurrent  = &pMsgQEl[0];
   tU32 u32Ret;
   for(u32Ret=0;u32Ret<pOsalData->u32MaxNrMqElements;u32Ret++)
   {
         if(pCurrent->bIsUsed == FALSE)break;
         else pCurrent++;
   }
   if(u32Ret == pOsalData->u32MaxNrMqElements)
   {
      return NULL;
   }
   else 
   {
      pOsalData->MqueueTable.u32UsedEntries++;
      return pCurrent;
   }
}


void vCleanUpMqofContext(void)
{
   tS32 s32OwnPid = OSAL_ProcessWhoAmI();
   uintptr_t s32MqHandle = 0;
   tS32 s32OpnCount = 0;
   tU32 i;
   char cName[LINUX_C_MQ_MAX_NAMELENGHT] = {0};
   trMqueueElement *pCurrentEntry = NULL;
   trStdMsgQueue* pQueue;
   for(i=0;i<pOsalData->u32MqHandles;i++)
   {
      s32MqHandle = (uintptr_t)pvGetFirstElementForPid(&MqMemPoolHandle,(tS32)s32OwnPid);
      if(s32MqHandle)
      {
         if(((tMQUserHandle*)s32MqHandle)->u32Magic == USER_HANDLE_MAGIC)
         {
             /* cleanup queue */
             pCurrentEntry = ((tMQUserHandle*)s32MqHandle)->pOrigin; /*lint !e613 *//* pointer already checked*/ 
             if(cName[0] == 0)
             {
                 strncpy(cName,pCurrentEntry->szName,LINUX_C_MQ_MAX_NAMELENGHT);
             }
             pQueue = &pStdMQEl[pCurrentEntry->u32EntryIdx];
             s32OpnCount =  pQueue->u16OpenCounter;

             TraceString("OSAL_s32MessageQueueClose for %s",pCurrentEntry->szName);
             if(OSAL_s32MessageQueueClose(s32MqHandle) == OSAL_ERROR)
             {
                break;
             }
             if((s32OpnCount-1) == 0)
             {
                 OSAL_s32MessageQueueDelete(cName);
             }
         }
         else
         {
             /* defect handle in pool */
             TraceString(" MQ pvGetFirstElementForPid gives handle without USER_HANDLE_MAGIC");
             break;
         }
      }
      else
      {
          /* last handle for PID was found */
          break;
      }
   }
}



/*****************************************************************************
 *
 * FUNCTION:     tMqueueTableSearchEntryByName
 *
 * DESCRIPTION:  return the pointer to a NU_MESSAGE_QUEUE with a given name or NULL
 *               in the case a NU_MESSAGE_QUEUE with this name doen't exist
 *
 * PARAMETER:    pName
 *
 * RETURNVALUE:  pointer to the searched NU_MESSAGE_QUEUE or NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trMqueueElement *tMqueueTableSearchEntryByName(tCString coszName)
{
   trMqueueElement *pCurrent = &pMsgQEl[0];
   tU32 u32Loop;
   for(u32Loop = 0; u32Loop < pOsalData->u32MaxNrMqElements; u32Loop++)
   {
      if((pCurrent->bIsUsed != 0)
        &&(OSAL_s32StringCompare(coszName, (tString)pCurrent->szName) == 0))break;
        pCurrent++;
    }
    if(u32Loop >= pOsalData->u32MaxNrMqElements)
    {
      pCurrent = NULL;
    }
    return pCurrent;
}
/*
static tU32 tMqueueTableSearchIndexByName(tCString coszName)
{
   trMqueueElement *pCurrent = &pMsgQEl[0];
   tU32 u32Loop;
   for(u32Loop = 0; u32Loop < pOsalData->u32MaxNrMqElements; u32Loop++)
   {
      if((pCurrent->bIsUsed != 0)
        &&(OSAL_s32StringCompare(coszName, (tString)pCurrent->szName) == 0))break;
        pCurrent++;
    }
    if(u32Loop >= pOsalData->u32MaxNrMqElements)
    {
      u32Loop = 0;
    }
    return u32Loop;
}*/

/*****************************************************************************
 *
 * FUNCTION:     tMqueueTableSearchEntryByHandle
 *
 * DESCRIPTION:  return the pointer to a NU_MESSAGE_QUEUE with a given handle or NULL
 *               in the case a NU_MESSAGE_QUEUE with this handle doen't exist
 *
 * PARAMETER:    pBox
 *
 * RETURNVALUE:  pointer to the searched NU_MESSAGE_QUEUE or NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
 /*
static trMqueueElement* tMqueueTableSearchEntryByHandle(OSAL_tMQueueHandle handle)
{
   trMqueueElement *pCurrent = pMsgQEl;
   pCurrent = pCurrent + handle;

   if(pCurrent->bIsUsed == FALSE )pCurrent = NULL;
   return pCurrent;

   return(pCurrent);
}

*/

/*****************************************************************************
*
* FUNCTION:    bSharedMemoryTableDeleteEntryByName
 *
 * DESCRIPTION: this function goes throught the Semaphor List deletes the
 *              Semaphor Element with the given name 
 *
 * PARAMETER:   coszName (I)
 *                 event name wanted.
 *
 * RETURNVALUE: tBool  
 *                TRUE = success FALSE=failed
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
*****************************************************************************
static tBool bMqueueTableDeleteEntryByName(tCString coszName)
{
   trMqueueElement *pCurrent = MqueueTable.ptrHeader;

   while( (pCurrent!= OSAL_NULL)
       && (pCurrent->pBasicInfo != NULL) && (OSAL_s32StringCompare(coszName,(tString)pCurrent->pBasicInfo->szName) != 0) )
   {
      pCurrent = pCurrent->pNext;
   }

   if(pCurrent!= OSAL_NULL)
   {
      if(pCurrent->bIsUsed == FALSE )
     {
         pCurrent->u32MqueueID    = 0;
         pCurrent->nuNotEmptyMQEvent.exinf  = 0;
         pCurrent->nuNotEmptyMQEvent.flgatr = 0;
         pCurrent->nuNotEmptyMQEvent.iflgptn = 0;
         pCurrent->idNotEmptyFlg  = 0;
         pCurrent->bIsUsed        = 0;
         pCurrent->bToDelete      = 0;
         pCurrent->u16ContextID   = 0;
         pCurrent->u16OpenCounter = 0;
         pCurrent->pNext          = 0;
         memset((void*)&pCurrent->nuMqueue,0,sizeof(TE_MESSAGE_QUEUE));
         memset(pCurrent->pBasicInfo->szName,0,LINUX_C_MQ_MAX_NAMELENGHT);
         return TRUE;
     }
   }

   return FALSE;
}*/

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    s32MqueueTableDeleteEntries
 *
 * DESCRIPTION: This function deletes all elements from OSAL table,
 *
 * PARAMETER:
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 *
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32MqueueTableDeleteEntries()
{
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;
   trMqueueElement *pCurrentEntry = pMsgQEl;

   if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
   {
      /* search the used table */
      while( pCurrentEntry < &pMsgQEl[pOsalData->MqueueTable.u32UsedEntries] )
      {
         if( (pCurrentEntry->bIsUsed == TRUE) )
         {
            s32DeleteCounter++;
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = s32DeleteCounter;
         }
         pCurrentEntry++;
      }
      UnLockOsal(&pOsalData->MqueueTable.rLock);
   }
   return(s32ReturnValue);
}


void vTraceMqInfo(tMQUserHandle* pCurrentEntry,tU8 Type,tBool bErrMem,tU32 u32Error,tCString coszName)
{
#ifdef SHORT_TRACE_OUTPUT
   char au8Buf[23 + LINUX_C_MQ_MAX_NAMELENGHT+1]={0,}; 
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   OSAL_M_INSERT_T8(&au8Buf[0],Type);
   bGetThreadNameForTID(&au8Buf[1],8,(tS32)u32Temp);
   OSAL_M_INSERT_T32(&au8Buf[9],u32Temp);
   if(pCurrentEntry)
   {  
       OSAL_M_INSERT_T32(&au8Buf[13],(tU32)pCurrentEntry->pOrigin);
       OSAL_M_INSERT_T32(&au8Buf[17],(tU32)pCurrentEntry);   
       OSAL_M_INSERT_T16(&au8Buf[21],(tU16)pCurrentEntry->enAccess);
       strncpy (&au8Buf[23],((trMqueueElement*)(pCurrentEntry->pOrigin))->szName,LINUX_C_MQ_MAX_NAMELENGHT);
   }
   else
   {  
      u32Temp = 0;
      OSAL_M_INSERT_T32(&au8Buf[13],u32Error);
      OSAL_M_INSERT_T32(&au8Buf[17],u32Error);
      OSAL_M_INSERT_T16(&au8Buf[21],(tU16)u32Temp);
      if(coszName)strncpy (&au8Buf[23],coszName,LINUX_C_MQ_MAX_NAMELENGHT);
   }
   u32Temp=strlen(&au8Buf[23]);
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_FATAL,au8Buf,u32Temp+23);
  
   if((pOsalData->bLogError)&&(bErrMem)/*(u32Error != OSAL_E_NOERROR)*/)
   {
      if(!pCurrentEntry)
      {
         vWriteToErrMem(OSAL_C_TR_CLASS_SYS_MQ,au8Buf,u32Temp+23,0);
      }
      else
      {
         if(!((trMqueueElement*)pCurrentEntry->pOrigin)->bTraceCannel)vWriteToErrMem(OSAL_C_TR_CLASS_SYS_MQ,au8Buf,u32Temp+23,0);
      }
   }
#else
   char au8Buf[251]={0,}; 
   char name[TRACE_NAME_SIZE];
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   au8Buf[0] = OSAL_STRING_OUT;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);

   if(coszName == NULL)coszName ="Unknown";
   switch(Type)
   {
     case OSAL_MQ_CREATE:
          if(pCurrentEntry)
          {
#ifdef NO_PRIxPTR
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Creates MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Creates MQ Handle 0x%" PRIxPTR " -> User Handle 0x%" PRIxPTR " Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#endif
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Creates MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
          }
         break;
     case OSAL_TSK_DELETE_MQ:
          if(pCurrentEntry)
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) deleted MQ Name:%s",
                      name,(unsigned int)u32Temp,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) deleted MQ Name:%s",
                      name,(unsigned int)u32Temp,coszName);
          }
         break;
     case OSAL_TSK_CONNECT_TO_MQ:
          if(pCurrentEntry)
          {
#ifdef NO_PRIxPTR
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) connected to MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) connected to MQ Handle 0x%" PRIxPTR " -> User Handle 0x%" PRIxPTR " Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#endif
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) connected to MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
          }
         break;
     case OSAL_TSK_DISCONNECT_MQ:
          if(pCurrentEntry)
          {
#ifdef NO_PRIxPTR
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) disconnected from MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) disconnected from MQ Handle 0x%" PRIxPTR " -> User Handle 0x%" PRIxPTR " Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#endif
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) disconnected from MQ Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
          }
         break;
     case OSAL_MQ_STATUS:
          if(pCurrentEntry)
          {
#ifdef NO_PRIxPTR
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Message Queue Status Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Message Queue Status Handle 0x%" PRIxPTR " -> User Handle 0x%" PRIxPTR " Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->pOrigin,(uintptr_t)pCurrentEntry,pCurrentEntry->enAccess,((trMqueueElement*)(pCurrentEntry->pOrigin))->szName);
#endif
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Message Queue Status Handle 0x%x -> User Handle 0x%x Access:%d MQ Name:%s",
                      name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
          }
         break;
     default:
         snprintf(&au8Buf[1],250,"Unhandled Trace ID:%d",Type);
         break;
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
   if((pOsalData->bLogError)&&(bErrMem))
   {
      trMqueueElement*  pEntry = NULL;
      if(pCurrentEntry)
      {
         pEntry = pCurrentEntry->pOrigin;
      }
      if((pEntry)&&(pEntry->bTraceCannel == FALSE))
      {
          vWritePrintfErrmem(au8Buf);
      }
      else
      {
          vWritePrintfErrmem(au8Buf);
      }
   }
#endif
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueueCreate
 *
 * DESCRIPTION: Create a MQ with a given max number of messages, each of a
 *              given MaxLenght, with given access right
 *
 * PARAMETER:
 *
 *   u16ContextID                index to distinguish caller context
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tU32 u32GenerateHandle(OSAL_tenAccess enAccess,trMqueueElement *pCurrentEntry,OSAL_tMQueueHandle* phMQ)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  
  /* create user handle */
  tMQUserHandle* pHandle = (tMQUserHandle *)OSAL_pvMemPoolFixSizeGetBlockOfPool(&MqMemPoolHandle);
  if(pHandle)
  {
     pHandle->enAccess = enAccess;
     pHandle->pOrigin  = pCurrentEntry;
     pHandle->u32Magic = USER_HANDLE_MAGIC;
     pHandle->s32TaskId = OSAL_ThreadWhoAmI();
     *phMQ = (OSAL_tMQueueHandle)pHandle;
  }
  else
  {
      u32ErrorCode = OSAL_E_NOSPACE;
  }
  return u32ErrorCode;
}



tS32 OSAL_s32MessageQueueCreate(tCString coszName,
                                tU32 u32MaxMessages,
                                tU32 u32MaxLength,
                                OSAL_tenAccess enAccess,
                                OSAL_tMQueueHandle* phMQ)
{
   tS32 s32ReturnValue= OSAL_ERROR;
   tU32 u32ErrorCode=OSAL_E_NOERROR;
   trMqueueElement *pCurrentEntry = NULL;
   tU32 u32Loop = 0;
   struct rlimit limit;

   if(s32Pid == 0)
   {
      s32Pid = getpid();
   }
   if(prLiMqMap == NULL)
   {
	   prLiMqMap    = (trResToPidMap2*)malloc(pOsalData->u32MaxNrMqElements * sizeof(trResToPidMap2));
	   pMqRefHandle = (tS32*)malloc(pOsalData->u32MaxNrMqElements * sizeof(tS32));
	   if((prLiMqMap == NULL)||(pMqRefHandle == NULL))
	   {
	      FATAL_M_ASSERT_ALWAYS();
	   }
	   vMqueueLiMqMapInit();
   }
   
#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
   return OSAL_s32ShMemMessageQueueCreate(coszName, u32MaxMessages, u32MaxLength,enAccess,phMQ);
#endif
   
   /* The name is not NULL */
   /* Parameter check */

   if( coszName && phMQ && (enAccess <= OSAL_EN_READWRITE) )
   {
      /* The Name is not too long */
      if( OSAL_u32StringLength(coszName) < LINUX_C_MQ_MAX_NAMELENGHT )
      {
         if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
         {
            pCurrentEntry = tMqueueTableSearchEntryByName(coszName);
            if( pCurrentEntry == OSAL_NULL )
            {
               if((pCurrentEntry = tMqueueTableGetFreeEntry())!= OSAL_NULL )
               {
  /*                  if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
                  {
                     CheckandCorrectMqStaticValues();
                  }*/
                  /* find Index */
                  if(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx != -1)
                  {
                     TraceString("MQ(%s): Create->rLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx != -1", coszName);
                     FATAL_M_ASSERT_ALWAYS();
                  }
                  tU32 u32Retry = 0;
                  /* check if linux resource is available*/
                  char buf[LINUX_C_MQ_MAX_NAMELENGHT+1];
                  mq_attr osalmq_attr = {0};
                  osalmq_attr.mq_maxmsg = u32MaxMessages;
                  osalmq_attr.mq_msgsize = u32MaxLength;
                  buf[0] = '/';
                  strncpy(&buf[1],coszName,LINUX_C_MQ_MAX_NAMELENGHT);
                  do
                  {
                     s32ReturnValue = mq_open(buf, O_RDWR | O_CREAT | O_EXCL, OSAL_ACCESS_RIGTHS, &osalmq_attr);
                     if(s32ReturnValue == -1 )
                     {
                        u32ErrorCode = u32ConvertErrorCore(errno);
                        switch(errno)
                        {
                           case EEXIST:
                                if((s32ReturnValue = mq_open(buf, O_RDWR , OSAL_ACCESS_RIGTHS, NULL)) == -1)
                                {
                                   bGetThreadNameForTID(&buf[0],8, OSAL_ThreadWhoAmI());
                                   TraceString("MQ(%s): Create->Open by TID:%d(%s) failed Error:%d", coszName, OSAL_ThreadWhoAmI(),buf, errno);
                                   u32ErrorCode = u32ConvertErrorCore(errno);
                                   FATAL_M_ASSERT_ALWAYS();
                                }
                                else
                                {
                                   u32ErrorCode = OSAL_E_NOERROR;
//                                   vWritePrintfErrmem("MQ(%s): Create->Open by TID:%d(%s) failed Error:%d", coszName, OSAL_ThreadWhoAmI(),buf, errno);
                                   TraceString("MQ(%s): Create->Open by TID:%d(%s) failed Error:%d", coszName, OSAL_ThreadWhoAmI(),buf, errno);
                                }
                                u32Retry += 2;
                               break;
                           case ENFILE:
                                if(getrlimit(RLIMIT_MSGQUEUE,&limit) == 0)
                                {
                                   TraceString("MQ(%s)-> ENFILE Limit Cur:%d Max:%d", coszName, limit.rlim_cur, limit.rlim_max);
                                   vWritePrintfErrmem("MQ(%s)-> ENFILE Limit Cur:%d Max:%d \n", coszName, limit.rlim_cur, limit.rlim_max);
                                }
                                else
                                {
                                   TraceString("MQ(%s): Create failed get RLIMIT_MSGQUEUE failed Error:%d", coszName, errno);
                                }
                                if(u32Retry == 0)
                                {
                                   u32ErrorCode = OSAL_E_NOERROR;
                                   limit.rlim_cur = pOsalData->u32MqResource;
                                   limit.rlim_max = pOsalData->u32MqResource;
                                   if(setrlimit(RLIMIT_MSGQUEUE,&limit) == -1)
                                   {
                                      TraceString("ENFILE Set Limit of MQ for Prc failed");
                                      vWritePrintfErrmem("ENFILE Set Limit of MQ for Prc failed \n");
                                   }
                                }
                                u32Retry++;
                               break;
                           case EMFILE:
                                if(getrlimit(RLIMIT_MSGQUEUE,&limit) == 0)
                                {
                                   TraceString("MQ(%s)-> EMFILE Limit Cur:%d Max:%d",coszName, limit.rlim_cur, limit.rlim_max);
                                   vWritePrintfErrmem("MQ(%s)-> EMFILE Limit Cur:%d Max:%d \n", coszName, limit.rlim_cur, limit.rlim_max);
                               }
                                else
                                {
                                   TraceString("MQ(%s): Create failed get RLIMIT_MSGQUEUE failed Error:%d", coszName, errno);
                                }
                                if(u32Retry == 0)
                                {
                                   u32ErrorCode = OSAL_E_NOERROR;
                                   limit.rlim_cur = pOsalData->u32MqResource;
                                   limit.rlim_max = pOsalData->u32MqResource;
                                   if(setrlimit(RLIMIT_MSGQUEUE,&limit) == -1)
                                   {
                                      TraceString("EMFILE Set Limit of MQ for Prc failed");
                                      vWritePrintfErrmem("EMFILE Set Limit of MQ for Prc failed \n");
                                   }
                                }
                                u32Retry++;
                               break;
                           case ENOSPC:
                                TraceString("MQ(%s): Create->Open failed  ENOSPC check /proc/sys/fs/mqueue/queues_max", coszName);
                                vWritePrintfErrmem("MQ(%s): Create->Open failed  ENOSPC check /proc/sys/fs/mqueue/queues_max\n", coszName);
                                u32Retry += 2;
                               break;
                          case EINVAL:
                                TraceString("MQ(%s): Create->Open failed  EINVAL mq_maxmsg:%d mq_msgsize:%d check /proc/sys/fs/mqueue", coszName,osalmq_attr.mq_maxmsg,osalmq_attr.mq_msgsize);
                                vWritePrintfErrmem("MQ(%s): Create->Open failed  EINVAL mq_maxmsg:%d mq_msgsize:%d check /proc/sys/fs/mqueue", coszName,osalmq_attr.mq_maxmsg,osalmq_attr.mq_msgsize);
                                u32Retry += 2;
                               break;
                          default:
                                TraceString("MQ(%s): Create->Open failed  Unexpected Error:%d", coszName, errno);
                                vWritePrintfErrmem("MQ(%s): Create->Open failed  Unexpected Error:%d \n", coszName, errno);
                                u32Retry += 2;
                                FATAL_M_ASSERT_ALWAYS();
                               break;
                        }//switch
                     }//if(s32ReturnValue == -1 )
                  }while(u32Retry == 1);
                  if(u32ErrorCode == OSAL_E_NOERROR)
                  {
                     if(s32OsalGroupId)
                     {
                        if(fchown(s32ReturnValue,(uid_t)-1,s32OsalGroupId) == -1)
                        {
                           vWritePrintfErrmem("MQ(%s): OSAL_s32MessageQueueCreate -> fchown error %d \n",coszName,errno);
                        }
                        if(fchmod(s32ReturnValue, OSAL_ACCESS_RIGTHS) == -1)
                        {
                           vWritePrintfErrmem("MQ(%s): OSAL_s32MessageQueueCreate -> fchmod error %d \n",coszName,errno);
                        }
                     }
                     pCurrentEntry->bIsUsed = TRUE;
                     pCurrentEntry->bTraceCannel = pOsalData->bMqTaceAll;
                      if(pOsalData->szMqName[0] != 0)
                     {
                        if(!strncmp(coszName,pOsalData->szMqName,strlen(pOsalData->szMqName)))
                        {
                           pCurrentEntry->bTraceCannel = TRUE;
                        }
                        if(!strncmp(commandline,pOsalData->szMqName,strlen(pOsalData->szMqName)))
                        {
                           pCurrentEntry->bTraceCannel = TRUE;
                        }
                     }
                     pOsalData->u32MqResCount++;
                     if(pOsalData->u32MaxMqResCount < pOsalData->u32MqResCount)
                     {
                         pOsalData->u32MaxMqResCount = pOsalData->u32MqResCount;
                         if(pOsalData->u32MaxMqResCount > (pOsalData->u32MaxNrMqElements*9/10))
                         {
                            pOsalData->u32NrOfChanges++;
                         }
                     }
                     pCurrentEntry->pcallback = NULL;
                     pCurrentEntry->pcallbackarg = NULL;
                     pCurrentEntry->callbacktid = NOTATHREAD;
                     pCurrentEntry->callbackpid = NOTATHREAD;
                     pCurrentEntry->callbackpididx = NOTATHREAD;
                     pCurrentEntry->bNotify = FALSE;
#ifdef USE_EVENT_AS_CB
                     pCurrentEntry->hEvent = (OSAL_tEventHandle) 0;
                     pCurrentEntry->mask = (OSAL_tEventMask) 0;
                     pCurrentEntry->enFlags = (OSAL_tenEventMaskFlag) 0;
#endif
                     pCurrentEntry->u16Type = MQ_STD;
                     pCurrentEntry->bOverflow = FALSE;
                     pCurrentEntry->s32PID = s32Pid;
                     /* export the handle */
                     if((u32ErrorCode = u32GenerateHandle(enAccess, pCurrentEntry, phMQ)) != OSAL_E_NOERROR)
                     {
                        TraceString("MQ(%s): u32GenerateHandle Error:%d", coszName, u32ErrorCode);
                        FATAL_M_ASSERT_ALWAYS();
                     }

                     prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx = (tU32)s32ReturnValue;
                     pMqRefHandle[pCurrentEntry->u32EntryIdx] = s32ReturnValue;
                     prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx = s32Pid;
                     prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt = 1;
                     (void)OSAL_szStringNCopy((tString)pCurrentEntry->szName,
                                               coszName,
                                               LINUX_C_MQ_MAX_NAMELENGHT);
                     pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages   = u32MaxMessages;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].MaxLength     = u32MaxLength;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage = 0;
                     /* init the other MQ fields */
                     pStdMQEl[pCurrentEntry->u32EntryIdx].bToDelete = FALSE;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u16OpenCounter = 1;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecTsk = 0;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecPrc = 0;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].s32LiHandle = s32ReturnValue;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u32IdxMPT    = 0;
                     if(!strcmp("CSMALU_MQ",coszName))
                     {
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing = MEASUREMENT_SWITCHOFF;
                     }
                     else
                     {
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing = MSG_PROCESSING_INACTIVE;
                     }
                     for( u32Loop=0; u32Loop < OSAL_C_MSG_TIME_ARR_SIZE; u32Loop++ )
                     {
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32MsgPrcTim[u32Loop]   = 0;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32MsgTimStamp[u32Loop] = 0;
                     }
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u32IdxLMPT    = 0;
                     for( u32Loop=0; u32Loop < OSAL_C_LONG_MSG_TIME_ARR_SIZE; u32Loop++ )
                     {
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32LongMsgPrcTim[u32Loop]   = 0;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32LongMsgTimStamp[u32Loop] = 0;
                     }
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u16WaitCounter = 0;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].bBlocked = FALSE;
                     pStdMQEl[pCurrentEntry->u32EntryIdx].u32Marker = 0;
                  }//if(u32ErrorCode == OSAL_E_NOERROR)
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
               }//if((pCurrentEntry = tMqueueTableGetFreeEntry())!= OSAL_NULL )
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }//if( pCurrentEntry == OSAL_NULL )
            /* UnLock the table */
            UnLockOsal(&pOsalData->MqueueTable.rLock);
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32ErrorCode != OSAL_E_NOERROR )||(phMQ == NULL))
   {
      s32ReturnValue = OSAL_ERROR;
      if(u32ErrorCode == OSAL_E_ALREADYEXISTS)
      {
        if((pCurrentEntry&&(pCurrentEntry->bTraceCannel == TRUE))||(u32OsalSTrace & 0x00000020))
        {
           vTraceMqInfo(NULL,OSAL_MQ_CREATE,FALSE,u32ErrorCode,coszName);
        }
      }
      else
      {
           vTraceMqInfo(NULL,OSAL_MQ_CREATE,TRUE,u32ErrorCode,coszName);
      }
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }
   else
   {
      if((pCurrentEntry->bTraceCannel == TRUE)||(u32OsalSTrace & 0x00000020)||(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_DATA))) /*lint !e613 pCurrentEntry already checked */
      {
         vTraceMqInfo((tMQUserHandle*)*phMQ,OSAL_MQ_CREATE,FALSE,u32ErrorCode,coszName);
         if(bTraceHandle)
         {
             TraceString("Cre MQ:%s OSAL Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",coszName,phMQ,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,/*lint !e613 pointer already checked */
                      GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)phMQ),
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,/*lint !e613 pointer already checked */
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);/*lint !e613 pointer already checked */
         }
      }
      s32ReturnValue= OSAL_OK;
   }
   return( s32ReturnValue );
}

tU32 u32GetValidMqHandle(trMqueueElement *pCurrentEntry)
 {
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tS32 s32Val = 0;
   
   if((prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx >= 0)
   &&(prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx == (tU32)s32Pid))
   {
     prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt++;
   }
   else
   {
       char buf[LINUX_C_MQ_MAX_NAMELENGHT+1];
       OSAL_szStringCopy(&buf[0], "/");
       OSAL_szStringCopy(&buf[1], pCurrentEntry->szName);
       s32Val = mq_open(buf, O_RDWR , OSAL_ACCESS_RIGTHS, NULL);
       if (s32Val == -1)
       {
         u32ErrorCode = u32ConvertErrorCore(errno);
         TraceString("OSALMQ mq_open %s failed with %d",pCurrentEntry->szName,errno);
       }
       else
       {
           prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx  = (tU32)s32Val;
           pMqRefHandle[pCurrentEntry->u32EntryIdx] = s32Val;
           prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx = s32Pid;
           prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt = 1;
       }
   }
   return u32ErrorCode;
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueueOpen
 *
 * DESCRIPTION: this function opens an existing OSAL event
 *
 * PARAMETER:   coszName (I)
 *                 event name to create.
 *              phEvent (->O)
 *                 pointer to the event handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MessageQueueOpen (tCString coszName,
                               OSAL_tenAccess enAccess,
                               OSAL_tMQueueHandle* phMQ)
{
   tS32 s32ReturnValue=OSAL_ERROR;
   tU32 u32ErrorCode=OSAL_E_NOERROR;
   trMqueueElement *pCurrentEntry = NULL;

   /* Make handle invalid */
   if(phMQ)
   {
        *phMQ=OSAL_C_INVALID_HANDLE;
   }

   if(prLiMqMap == NULL)
   {
	   prLiMqMap    = (trResToPidMap2*)malloc(pOsalData->u32MaxNrMqElements * sizeof(trResToPidMap2));
	   pMqRefHandle = (tS32*)malloc(pOsalData->u32MaxNrMqElements * sizeof(tS32));
	   if((prLiMqMap == NULL)||(pMqRefHandle == NULL))
	   {
	      FATAL_M_ASSERT_ALWAYS();
	   }
	   vMqueueLiMqMapInit();
   }
   
#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
   return OSAL_s32ShMemMessageQueueOpen(coszName,enAccess,phMQ);
#endif
   if( coszName && phMQ && (enAccess <= OSAL_EN_READWRITE) )
   {
      if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
      {
         pCurrentEntry = tMqueueTableSearchEntryByName(coszName);
         if(pCurrentEntry != NULL)
         {
/*            if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
            {
               CheckandCorrectMqStaticValues();
            }*/
            trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
            if(pTemp->bToDelete)
            {  
//               u32ErrorCode = OSAL_E_NOPERMISSION;
                vWritePrintfErrmem("TID:%d MQ Open for %s Delete Marked Queue OpenCnt:%d \n",
                                   OSAL_ThreadWhoAmI(),pCurrentEntry->szName,pTemp->u16OpenCounter);
            }//if( pCurrentEntry!= OSAL_NULL )
          //  else
            {
               u32ErrorCode = u32GetValidMqHandle(pCurrentEntry);
               if(u32ErrorCode == OSAL_E_NOERROR)
               {
                  pTemp->u16OpenCounter++;
               }
               else
               {
                   NORMAL_M_ASSERT_ALWAYS();
               }
               u32ErrorCode = u32GenerateHandle(enAccess,pCurrentEntry,phMQ);
            }
         }// if(pCurrentEntry != NULL)
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }//if( pCurrentEntry!= OSAL_NULL )
      }
      UnLockOsal(&pOsalData->MqueueTable.rLock);
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32ErrorCode != OSAL_E_NOERROR )||(phMQ == NULL))
   {
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
        if((pCurrentEntry&&(pCurrentEntry->bTraceCannel == TRUE))||(u32OsalSTrace & 0x00000020))
        {
           vTraceMqInfo(NULL,OSAL_TSK_CONNECT_TO_MQ,FALSE,u32ErrorCode,coszName);
        }
      }
      else
      {
         vTraceMqInfo(NULL,OSAL_TSK_CONNECT_TO_MQ,TRUE,u32ErrorCode,coszName);
      }
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if((pCurrentEntry->bTraceCannel == TRUE)||(u32OsalSTrace & 0x00000020)||(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_DATA)))/*lint !e613 pointer already checked */
      {
         vTraceMqInfo((tMQUserHandle*)*phMQ,OSAL_TSK_CONNECT_TO_MQ,FALSE,u32ErrorCode,coszName);
         if(bTraceHandle)
         {
             TraceString("Opn MQ:%s OSAL Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",coszName,phMQ,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,/*lint !e613 pointer already checked */
                      GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)phMQ),
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,/*lint !e613 pointer already checked */
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);/*lint !e613 pointer already checked */
         }
      }
     s32ReturnValue = OSAL_OK;
   }
   return( s32ReturnValue );
}




/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueueClose
 *
 * DESCRIPTION: this function creates an OSAL event.
 *
 * PARAMETER:   coszName (I)
 *                 event name to create.
 *              phEvent (->O)
 *                 pointer to the event handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tU32 u32CheckForDelete(trMqueueElement *pCurrentEntry)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt == 0)
   {
      trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];

      if( !pTemp->u16OpenCounter && pTemp->bToDelete )
      {
          if(pCurrentEntry->bSvActive == TRUE)
          {
             vRemRecMqFromList(pCurrentEntry);
             pCurrentEntry->u32Timeout = 0;
             pCurrentEntry->bSvActive  = FALSE;
          }
          pOsalData->u32MqResCount--;
          char buf[LINUX_C_MQ_MAX_NAMELENGHT+1];
          buf[0] = '/';
          strncpy(&buf[1],pCurrentEntry->szName,LINUX_C_MQ_MAX_NAMELENGHT);
          if(mq_unlink(buf) == -1)
          {
             TraceString("OSALMQ unlink %d failed with errno %d",buf,errno);
             u32ErrorCode = u32ConvertErrorCore(errno);
          }
          vResetEntry(pCurrentEntry->u32EntryIdx);
      }
   }//if(pCurrentEntry->uTypeMqInfo.pMqueue->u32LiPrcOpnCnt[u32count] == 0)
   pCurrentEntry->bIsUsed = FALSE;
   return u32ErrorCode;
}

static void vPrintMqHandleIssue(OSAL_tMQueueHandle hMQueue)
{
   if(pOsalData->bLogError)
   {
      vWritePrintfErrmem("OSALMQ memstart:0x%x memend:0x%x Handle:0x%x \n", MqMemPoolHandle.u32memstart,MqMemPoolHandle.u32memend,hMQueue);
      NORMAL_M_ASSERT_ALWAYS();
   }
   TraceString("OSALMQ memstart:0x%x memend:0x%x Handle:0x%x", MqMemPoolHandle.u32memstart,MqMemPoolHandle.u32memend,hMQueue);
}

tU32 u32CheckMqHandle(OSAL_tMQueueHandle hMQueue)
{
   if((hMQueue <= MqMemPoolHandle.u32memstart)||(hMQueue >= MqMemPoolHandle.u32memend))
   {
      if(MqMemPoolHandle.pMem)
      {
         trOSAL_MPF* pPtr = (trOSAL_MPF*)MqMemPoolHandle.pMem;
         if(pPtr->u32ErrCnt == 0)
         {
           vPrintMqHandleIssue(hMQueue);
           return OSAL_E_INVALIDVALUE;
         }
         else
         {
            /* possible that Handle Memory was allocated via malloc */
            if(( hMQueue == 0)||((tU32)hMQueue == OSAL_C_INVALID_HANDLE))
            {
               vPrintMqHandleIssue(hMQueue);
               return OSAL_E_INVALIDVALUE;
            }
         }
      }
      else
      {
         vPrintMqHandleIssue(hMQueue);
         return OSAL_E_INVALIDVALUE;
      }
   }

   if(((tMQUserHandle*)hMQueue)->u32Magic != USER_HANDLE_MAGIC)
   {
      vPrintMqHandleIssue(hMQueue);
      return OSAL_E_BADFILEDESCRIPTOR;
   }
   return OSAL_E_NOERROR;
}


tS32 OSAL_s32MessageQueueClose (OSAL_tMQueueHandle phMQueue)
{
  tS32 s32ReturnValue = OSAL_ERROR;
  tU32 u32ErrorCode   = u32CheckMqHandle(phMQueue);
  trMqueueElement *pCurrentEntry = NULL;
 
#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
  return OSAL_s32ShMemMessageQueueClose(phMQueue);
#endif

  if(u32ErrorCode == OSAL_E_NOERROR)
  {
     if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
     {
        pCurrentEntry = ((tMQUserHandle*)phMQueue)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
        if( pCurrentEntry != OSAL_NULL )
        {
/*           if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
           {
               CheckandCorrectMqStaticValues();
           }*/
           trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
           if(pTemp->u16OpenCounter > 0 )
           {
               pTemp->u16OpenCounter--;
               if(s32Pid != (tS32)prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx)
               {
                  u32ErrorCode = OSAL_E_DOESNOTEXIST;
               }
               if(prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt > 0)
               {
                     prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt--;
               }
               if(prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt == 0)
               {
                  if(mq_close(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx) == -1)
                  {
                     u32ErrorCode = u32ConvertErrorCore(errno);
                  }
                  prLiMqMap[pCurrentEntry->u32EntryIdx].u32MemIdx = 0;
                  prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx = 0;
                  prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt = 0;
                  prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx  = -1;
                  pMqRefHandle[pCurrentEntry->u32EntryIdx] = -1;
               }
               if( !pTemp->u16OpenCounter && pTemp->bToDelete )
               {
                  u32ErrorCode = u32CheckForDelete(pCurrentEntry);
                  pTemp->bToDelete = FALSE;
                  if(u32ErrorCode == OSAL_E_NOERROR)
                  {
                     if((pCurrentEntry->bTraceCannel == TRUE)||(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_DATA)))
                     {
                         vTraceMqInfo((tMQUserHandle*)phMQueue,
                                    OSAL_TSK_DISCONNECT_MQ,
                                    TR_LEVEL_DATA,
                                    u32ErrorCode,
                                    pCurrentEntry->szName);
                         if(bTraceHandle)
                         { 
                            TraceString("Del MQ:%s OSAL Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",pCurrentEntry->szName,&phMQueue,
                            prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                            GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)&phMQueue),
                            prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,
                            prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);
                         }
                      }
                  }//if(u32ErrorCode == OSAL_E_NOERROR)
               }//if( !pTemp->u16OpenCounter && pTemp->bToDelete )
               /*destroy magic*/
               ((tMQUserHandle*)phMQueue)->u32Magic = 0;/*lint !e613 *//* pointer already checked*/ 
               if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&MqMemPoolHandle,(void*)phMQueue) == OSAL_ERROR)
               {    NORMAL_M_ASSERT_ALWAYS();  }
               s32ReturnValue = OSAL_OK;
            }
            else
            {
               TraceString("OSALMQ Unexpected Open counter 0 for MQ %s",pCurrentEntry->szName);
               u32ErrorCode = OSAL_E_UNKNOWN;
            }
        }
        else
        {
           u32ErrorCode = OSAL_E_INVALIDVALUE;
        }
        UnLockOsal(&pOsalData->MqueueTable.rLock);
     }
  }

  if( u32ErrorCode != OSAL_E_NOERROR )
  {
     tCString szName = "";
     if(pCurrentEntry)
     {
        szName = pCurrentEntry->szName;
     }
      vTraceMqInfo(NULL,OSAL_TSK_DISCONNECT_MQ,TRUE,u32ErrorCode,szName);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF,u32ErrorCode);
  }
  else
  {
     if((pCurrentEntry->bTraceCannel == TRUE)||(u32OsalSTrace & 0x00000020)||(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_DATA)))/*lint !e613 pointer already checked */
     {
         vTraceMqInfo((tMQUserHandle*)phMQueue,OSAL_TSK_DISCONNECT_MQ,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pointer already checked */
         if(bTraceHandle)
         {
            TraceString("Cls MQ:%s OSAL Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",pCurrentEntry->szName,&phMQueue,/*lint !e613 pointer already checked */
                        prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,/*lint !e613 pointer already checked */
                        GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)&phMQueue),
                        prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,/*lint !e613 pointer already checked */
                        prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);/*lint !e613 pointer already checked */
         }
      }
      phMQueue = 0;
  }
  return( s32ReturnValue );
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueueDelete
 *
 * DESCRIPTION: this function destroys an OSAL event.
 *
 * PARAMETER:   coszName (I)
 *                 event name to create.
 *              phEvent (->O)
 *                 pointer to the event handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MessageQueueDelete(tCString coszName)
{
  tS32 s32ReturnValue=OSAL_ERROR;
  tU32 u32ErrorCode=OSAL_E_NOERROR;
  trMqueueElement *pCurrentEntry = NULL;

#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
   return OSAL_s32ShMemMessageQueueDelete(coszName);
#endif
  
  if( coszName )
  {
    pCurrentEntry = tMqueueTableSearchEntryByName(coszName);
    if(pCurrentEntry)
    {
/*       if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
       {
          CheckandCorrectMqStaticValues();
       }*/
       if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
       {
           trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
           pTemp->bToDelete = TRUE;
           if(pTemp->u16OpenCounter == 0)
           {
              u32ErrorCode = u32CheckForDelete(pCurrentEntry);
           }
           UnLockOsal(&pOsalData->MqueueTable.rLock);
       }
    }
    else
    {
       u32ErrorCode = OSAL_E_DOESNOTEXIST;
    }
  }
  else
  {
       u32ErrorCode = OSAL_E_INVALIDVALUE;
  }

  if( u32ErrorCode != OSAL_E_NOERROR )
  {
      vTraceMqInfo(NULL,OSAL_TSK_DELETE_MQ,TRUE,u32ErrorCode,coszName);
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
  }
  else
  {
      if((pCurrentEntry->bTraceCannel == TRUE)||(u32OsalSTrace & 0x00000020)||(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MQ,TR_LEVEL_DATA)))/*lint !e613 pointer already checked */
      {
         vTraceMqInfo(NULL,OSAL_TSK_DELETE_MQ,FALSE,u32ErrorCode,coszName);
         if(bTraceHandle)
         {
            TraceString("Del MQ:%s OSAL Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",coszName,0,
            prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,/*lint !e613 pointer already checked */
            GetMemPoolOffset(&MqMemPoolHandle, 0),
            prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,/*lint !e613 pointer already checked */
            prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);/*lint !e613 pointer already checked */
         }
      }
      s32ReturnValue = OSAL_OK;
  }

  return( s32ReturnValue );
}

/*****************************************************************************
 *
 * FUNCTION: u32ExecuteCallback
 *
 * DESCRIPTION: Exeecute callba.
 *
 * PARAMETER:   trMqueueElement* pointer to message queue element
 *
 * ERRORS:      OSAL_E_NOPERMISSION  access denied for callback code
 *
 * RETURNVALUE: u32Ret
 *                 it is the function return value:
 *                 - OSAL_E_NOERROR if everything goes right;
 *                 - OSAL_E_NOPERMISSION otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.12.07  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
void vExecuteCallback(void* pvArg)
{
  trMqueueElement* pCurrentEntry = (trMqueueElement*)pvArg;
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  if((pCurrentEntry)
   &&(pCurrentEntry->u32MqueueID == LINUX_C_U32_MQUEUE_ID)
   &&(pCurrentEntry->bNotify == TRUE))
  {
#ifdef MQ_NOTIFY_VIA_OSAL
#else
     struct sigevent s = { 0 };
     s.sigev_notify = SIGEV_THREAD;
     s.sigev_notify_function = (void (*)(sigval_t))vExecuteCallback;
     s.sigev_notify_attributes = NULL;
     s.sigev_value.sival_ptr = pCurrentEntry;
     if(mq_notify((mqd_t)rLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, &s) == -1)
     {
       //  FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = u32ConvertErrorCore(errno);
     }
#endif

     pCurrentEntry->pcallback(pCurrentEntry->pcallbackarg);

     if((pCurrentEntry->bTraceCannel == TRUE)
#ifndef MQ_NOTIFY_VIA_OSAL
         ||(u32ErrorCode != OSAL_E_NOERROR)
#endif
         )
     {
#ifdef SHORT_TRACE_OUTPUT
        char au8Buf[22 + LINUX_C_MQ_MAX_NAMELENGHT];
        tU32 Temp = (tU32)OSAL_ThreadWhoAmI();
        OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_CALLBACK);
#ifdef MQ_NOTIFY_VIA_OSAL
        bGetThreadNameForTID(&au8Buf[1],8,OSAL_ThreadWhoAmI());
#else
        memcpy(&au8Buf[1],"LISYSTEM",8);
#endif
        OSAL_M_INSERT_T32(&au8Buf[9], Temp);
        OSAL_M_INSERT_T32(&au8Buf[13],(tU32)vExecuteCallback);
        OSAL_M_INSERT_T32(&au8Buf[17], u32ErrorCode);
        Temp = strlen(pCurrentEntry->szName);
        memcpy (&au8Buf[21],pCurrentEntry->szName,Temp);
        LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_DATA,au8Buf,21+Temp);
#else
        char au8Buf[251];
        tU32 Temp = (tU32)OSAL_ThreadWhoAmI();
        char name[TRACE_NAME_SIZE];
        bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,OSAL_ThreadWhoAmI());
        au8Buf[0] = OSAL_STRING_OUT;
#ifdef NO_PRIxPTR
        snprintf(&au8Buf[1],250," Task:%s(ID:%d) Executes callback 0x%x Error:0x%x for MQ:%s",
                  name,(unsigned int)Temp,(uintptr_t)vExecuteCallback,(unsigned int)u32ErrorCode,pCurrentEntry->szName);
#else
        snprintf(&au8Buf[1],250," Task:%s(ID:%d) Executes callback 0x%" PRIxPTR " Error:0x%x for MQ:%s",
                  name,(unsigned int)Temp,(uintptr_t)vExecuteCallback,(unsigned int)u32ErrorCode,pCurrentEntry->szName);
#endif
        LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_DATA,au8Buf,strlen(&au8Buf[1])+1);
#endif
     }
  }
}

tS32 u32GetSigRtMinId(tS32 s32PrcId)
{
   char Buffer[64];
   int fd;
   char* pTmp;
   tS32 s32Ret = -1;
   snprintf(Buffer,64,"%s/OSAL/Processes/%d",VOLATILE_DIR,s32PrcId);
   if((fd = open(Buffer, O_RDONLY|O_CLOEXEC,OSAL_ACCESS_RIGTHS)) != -1)
   {
      if(read(fd,Buffer,64) > 0)
      {
         pTmp = strstr(Buffer,":");
         if(pTmp)
         {
            pTmp++;
            s32Ret = atoi(pTmp);
         }
      }
      close(fd);
   }
   return s32Ret;
}


void vTriggerSigRtMin(tS32 s32ReqPid, char* Name)
{
   int i;
   
   if((i = u32GetSigRtMinId(s32ReqPid)) == -1)
   {
      i = SIG_BACKTRACE;
   }
   /* get callstack of receiver */
   if(kill(s32ReqPid,i) == -1)
   {   
      if(errno == EPERM)
      {
         vWritePrintfErrmem("%s Do Kill via OSAL Term Task due permissions ",Name);
         vSendCallStackGenFromTerm(s32ReqPid,MBX_GEN_CS_USR2);
      }
      else
      {
         vWritePrintfErrmem("%s Kill SIG_BACKTRACE failed Error:%d ",Name,errno);
      }
   }
}

static void vReactOnMqOverflow(trMqueueElement* pCurrentEntry)
{
  tU32 u32Val1[OSAL_C_MSG_TIME_ARR_SIZE];
  tU32 u32Val2[OSAL_C_MSG_TIME_ARR_SIZE];
//  tU32 u32Val3[OSAL_C_MSG_TIME_ARR_SIZE];
  int i;
  char NameS[16];
  char NameR[16];
  tU32 u32CurrentIdx;

  trStdMsgQueue* pStdMsgQEntry = &pStdMQEl[pCurrentEntry->u32EntryIdx];
  if(pStdMsgQEntry->u32Marker == 0)
  {
     tU32 u32OverflowTime = 0;
     if(pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing != MEASUREMENT_SWITCHOFF)
     {
        u32OverflowTime = pOsalData->u32Seconds - pStdMsgQEntry->u32MsgTimStamp[pStdMsgQEntry->u32IdxMPT];
     }
     u32CurrentIdx = pStdMsgQEntry->u32IdxMPT;
     pStdMsgQEntry->u32Marker++;
     NameS[15] = 0;
     NameR[15] = 0;
     bGetThreadNameForTID(&NameS[0], 16,OSAL_ThreadWhoAmI());
     bGetThreadNameForTID(&NameR[0], 16,pStdMsgQEntry->u32RecTsk);
     /* format string and write to errmem */
     vWritePrintfErrmem("Message Queue %s overflow after %d Sec Send Task:%s Receiver Task:%s blocked for %d Sec \n",
                        pCurrentEntry->szName,
                        pOsalData->u32Seconds,
                        NameS,
                        NameR,
                        u32OverflowTime);
     if((pCurrentEntry->bOverflow == FALSE)&& (pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing != MEASUREMENT_SWITCHOFF))
     {
        /* prepare right sequence */
        for(i= 0;i<OSAL_C_MSG_TIME_ARR_SIZE;i++)
        {
           u32CurrentIdx++;
           if(u32CurrentIdx == OSAL_C_MSG_TIME_ARR_SIZE)
           {
              u32CurrentIdx = 0;
           } 
           u32Val1[i] = pStdMsgQEntry->u32MsgTimStamp[u32CurrentIdx];
           u32Val2[i] = pStdMsgQEntry->u32MsgPrcTim[u32CurrentIdx];
//           u32Val3[i] = pStdMsgQEntry->u32MsgCount[u32CurrentIdx];
        }
        vWritePrintfErrmem("Last Msg Processing times  Start:%d -> %d sec ,Start:%d -> %d sec,Start:%d -> %d sec,Start:%d -> %d sec,Start:%d -> %d sec \n",
                           u32Val1[0],u32Val2[0],u32Val1[1],u32Val2[1],u32Val1[2],u32Val2[2],u32Val1[3],u32Val2[3],u32Val1[4],u32Val2[4]);
        vWritePrintfErrmem("Last Msg Processing times  Start:%d -> %d sec ,Start:%d -> %d sec,Start:%d -> %d sec,Start:%d -> %d sec,Start:%d -> %d sec \n",
                           u32Val1[5],u32Val2[5],u32Val1[6],u32Val2[6],u32Val1[7],u32Val2[7],u32Val1[8],u32Val2[8],u32Val1[9],u32Val2[9]);

        for(i= 0;i<OSAL_C_LONG_MSG_TIME_ARR_SIZE;i++)
        {
           u32CurrentIdx = pStdMsgQEntry->u32IdxLMPT;
           u32CurrentIdx++;
           if(u32CurrentIdx == OSAL_C_LONG_MSG_TIME_ARR_SIZE)
           {
              u32CurrentIdx = 0;
           }
           u32Val1[i] = pStdMsgQEntry->u32LongMsgTimStamp[u32CurrentIdx];
           u32Val2[i] = pStdMsgQEntry->u32LongMsgPrcTim[u32CurrentIdx];
        }
        vWritePrintfErrmem("Long Msg Processing times  Start:%d -> %d sec ,Start:%d -> %d sec,Start:%d -> %d sec ,Start:%d -> %d sec,Start:%d -> %d sec \n",
                           u32Val1[5],u32Val2[5],u32Val1[6],u32Val2[6],u32Val1[7],u32Val2[7],u32Val1[8],u32Val2[8],u32Val1[9],u32Val2[9]);

        if(pOsalData->u32OverflowPid != 0)
        {
           vWritePrintfErrmem("MQ Handle MQ overflow simultanously");
        }
        else
        {
           pOsalData->u32OverflowPid = pStdMsgQEntry->u32RecPrc;
           pOsalData->u32OverflowTid = pStdMsgQEntry->u32RecTsk;
        }

        vTriggerSigRtMin(pStdMsgQEntry->u32RecPrc,pCurrentEntry->szName);

        pCurrentEntry->bOverflow = TRUE;
     }
  }
  /* to ensure callstack generation completed */
  OSAL_s32ThreadWait(1000);
  pStdMsgQEntry->u32Marker = 0;
}

void vRestoreMq(trMqueueElement* pCurrentEntry)
{
   if(pCurrentEntry->u32BFD_Count == 3)
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   if(pCurrentEntry->u32BFD_Count < 3)
   {
   if(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx != pMqRefHandle[pCurrentEntry->u32EntryIdx])
   {
      TraceString("OSALMQ %s(%d) TID:%d Restore Linux MQ %s -> Descriptor %d was invalid new %d Index:%d",
                      commandline,
                      OSAL_ProcessWhoAmI(),
                      OSAL_ThreadWhoAmI(),
                      pCurrentEntry->szName,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, 
                      pMqRefHandle[pCurrentEntry->u32EntryIdx],
                      pCurrentEntry->u32EntryIdx);
      vWritePrintfErrmem("OSALMQ %s(%d) TID:%d Restore Linux MQ %s -> Descriptor %d was invalid new %d Index:%d \n",
                      commandline,
                      OSAL_ProcessWhoAmI(),
                      OSAL_ThreadWhoAmI(),
                      pCurrentEntry->szName,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, 
                      pMqRefHandle[pCurrentEntry->u32EntryIdx],
                      pCurrentEntry->u32EntryIdx);   
   }
   else
   {
      TraceString("OSALMQ TID:%d Cannot Restore Linux MQ %s -> Descriptor %d was invalid new %d Index:%d",
                      OSAL_ThreadWhoAmI(),
                      pCurrentEntry->szName,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, 
                      pMqRefHandle[pCurrentEntry->u32EntryIdx],
                      pCurrentEntry->u32EntryIdx);
      vWritePrintfErrmem("OSALMQ TID:%d Cannot Restore Linux MQ %s -> Descriptor %d was invalid new %d Index:%d \n",
                      OSAL_ThreadWhoAmI(),
                      pCurrentEntry->szName,
                      prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, 
                      pMqRefHandle[pCurrentEntry->u32EntryIdx],
                      pCurrentEntry->u32EntryIdx);   
   }
   }
   pCurrentEntry->u32BFD_Count++;
}


void vPostToCallbackQueue(trMqueueElement* pCurrentEntry)
{
   tU32 u32Error = OSAL_E_NOERROR;
   tOsalMbxMsg rMsgRecBuffer;
   rMsgRecBuffer.rOsalMsg.Cmd = MBX_MQ_NOTIFY;
   rMsgRecBuffer.rOsalMsg.ID  = pCurrentEntry->u32EntryIdx;

   if(hMQCbPrc[pCurrentEntry->callbackpididx] == 0)
   {
     hMQCbPrc[pCurrentEntry->callbackpididx] = GetPrcLocalMsgQHandle(pCurrentEntry->callbackpid);
   /*   TraceString("OSALMQ TID:%d %s vPostToCallbackQueue install CB MQ Handle:0x%x",
                   OSAL_ThreadWhoAmI(), pCurrentEntry->szName,hMQCbPrc[pCurrentEntry->callbackpididx]);*/
   }
   if(OSAL_s32MessageQueuePost(hMQCbPrc[pCurrentEntry->callbackpididx],
                              (tPCU8)&rMsgRecBuffer, 
                              sizeof(tOsalMbxMsg),
                              0) == OSAL_ERROR)
   {
      u32Error = OSAL_u32ErrorCode();
      TraceString("OSALMQ TID:%d %s vPostToCallbackQueue Error:0x%x Handle:0x%x",
                   OSAL_ThreadWhoAmI(), pCurrentEntry->szName,u32Error,hMQCbPrc[pCurrentEntry->callbackpididx]);
      /* check for block callback handler task */
      if((u32Error == OSAL_E_TIMEOUT)||(u32Error == OSAL_E_QUEUEFULL))
      {
         tU32 u32MaxMessages = 0;
         tU32 u32MaxLength = 0;
         tU32 u32Messages = 0;
         (void)OSAL_s32MessageQueueStatus(hMQCbPrc[pCurrentEntry->callbackpididx],&u32MaxMessages,&u32MaxLength,&u32Messages);

         vWritePrintfErrmem("OSALMQ %s seems to be blocked -> MaxMsg:%d ActMsg:%d \n",
                            prProcDat[pCurrentEntry->callbackpididx].szMqName,u32MaxMessages,u32Messages);
         trStdMsgQueue* pStdMsgQEntry = &pStdMQEl[pCurrentEntry->u32EntryIdx];
         int sig = 0;
         if((sig = u32GetSigRtMinId(pStdMsgQEntry->u32RecPrc)) == -1)
         {
            sig = SIG_BACKTRACE;
         }
         /* get callstack of receiver */
         if(kill(pStdMsgQEntry->u32RecPrc,sig) == -1)
         {   
            if(errno == EPERM)
            {
               vWritePrintfErrmem("MQ:%s Do Kill via OSAL Term Task due permissions ",pCurrentEntry->szName);
               vSendCallStackGenFromTerm(pStdMsgQEntry->u32RecPrc,MBX_GEN_CS_USR2);
            }
            else
            {
                vWritePrintfErrmem("MQ:%s Kill SIG_BACKTRACE failed Error:%d ",pCurrentEntry->szName,errno);
            }
         }
      }
   }

   /* do not set an error code when using callback system doesn't work, because message is 
      completely delivered to receiver and will be detected by timeout */
   if(u32Error != OSAL_E_NOERROR)
   {
      if((u32Error != OSAL_E_TIMEOUT)&&(u32Error != OSAL_E_TIMEOUT))
      {
         vWritePrintfErrmem("OSALMQ %s Reset Error Code:%d from CB HDR system \n",
                            prProcDat[pCurrentEntry->callbackpididx].szMqName,u32Error);
      }
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, OSAL_E_NOERROR);

#ifdef USE_OSAL_CB_HDR_SIGNAL
      if(u32Error == OSAL_E_QUEUEFULL)
      {
         //tS32 s32Tid = pStdMQEl[((tMQUserHandle*phMQCbPrc[pCurrentEntry->callbackpididx])->pOrigin->u32EntryIdx].u32RecTsk;
         tS32 s32Pid = pStdMQEl[((tMQUserHandle*phMQCbPrc[pCurrentEntry->callbackpididx])->pOrigin->u32EntryIdx].u32RecPrc;
         /* check for callback handler task */
         if(s32Pid)
         {
               kill(s32Pid, OSAL_CB_HDR_SIGNAL);
         }
      }
#endif
   }
}


tU32 u32GemTmoStruct(struct timespec *tim,OSAL_tMSecond msec)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   if(!clock_gettime(CLOCK_REALTIME, tim))
   {
        /* If the Timers option is supported, the timeout shall be based on the CLOCK_REALTIME clock; 
          if the Timers option is not supported, the timeout shall be based on the system clock as returned by the time() function.*/
       tim->tv_sec += msec / 1000 ;
       tim->tv_nsec += (msec%1000)*1000000;
       if(tim->tv_nsec >= 1000000000)
       {
          tim->tv_sec += 1;
          tim->tv_nsec = tim->tv_nsec - 1000000000;
       }
   }
   else
   {
      u32ErrorCode = u32ConvertErrorCore(errno);
   }
   return u32ErrorCode;
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueuePost
 *
 * DESCRIPTION: Sends a message of given length and priority to a MessageQueue.
 *
 * PARAMETER:   hMQ       MessageQueue handle
 *              pcou8Msg  const pointer to the buffer containing the message
 *              u32Length message length in bytes
 *              u32Prio   message priority
 *
 * ERRORS:      OSAL_E_BADFILEDESCRIPTOR  hMQ is not a valid handle
 *              OSAL_E_INVALIDVALUE       prio exceeds the boundary
 *              OSAL_E_MSGTOOLONG         length exceeds the field MaxLength
 *                                        defined in creation
 *              OSAL_E_QUEUEFULL          the MQ is full
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 * 13.12.12  | Fix for OSAL MQ priority issue         |sja3kor 
 * --.--.--  | ----------------                       | -----
 * 16.01.13  |Fix for Wrong timeout for Linux message |sja3kor
 *           | queue post/wait                        |
 *****************************************************************************/
tS32 OSAL_s32MessageQueuePost( OSAL_tMQueueHandle hMQ,
                               tPCU8 pcou8Msg,
                               tU32  u32Length,
                               tU32  u32Prio)
{
   tS32 s32ReturnValue = OSAL_OK;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ); /* step 1 user handle check */
   trMqueueElement *pCurrentEntry = NULL;
   tU32 u32ActualMessage = 0xffffffff;
   struct timespec tim = {0,0};
   tU32 u32Prio_Li = 0;                      //u32Prio_Li variable indicates linux specific priority.
   tU32 msec = 0;
   tU32 u32Temp;
   struct mq_attr mqstat;
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];
   trStdMsgQueue* pTemp;


#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
    return OSAL_s32ShMemMessageQueuePost(hMQ,pcou8Msg,u32Length,u32Prio);
#endif
 
   if(u32ErrorCode == OSAL_E_NOERROR) 
   {
      /* check permissions*/
      if(((tMQUserHandle*)hMQ)->enAccess == OSAL_EN_READONLY)
      {
         u32ErrorCode = OSAL_E_NOPERMISSION;
      }
   }
   if(u32ErrorCode == OSAL_E_NOERROR) 
   {
      /* step 2 uparameter check */
      pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin; /*lint !e613 *//* pointer already checked*/ 
      if(pCurrentEntry != OSAL_NULL )
      {
/*         if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
         {
            CheckandCorrectMqStaticValues();
         }*/

         pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
         if( u32Prio < (OSAL_C_U32_MQUEUE_PRIORITY_LOWEST + 1) )
         {  
             /* check if queue isn't deleted */
            if(u32Length > pTemp->MaxLength)
            {
               u32ErrorCode = OSAL_E_MSGTOOLONG;
            }
         /*      else if(u32Length < pTemp->MaxLength)
               {
                   TRACE_LINE_INFO;
                   memcpy(cResMsgBuffer,pcou8Msg,u32Length);
                   pcSendMsg = (const tU8*)&cResMsgBuffer[0];
               }*/
            else if((pCurrentEntry->bIsUsed == FALSE)||(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx == -1))
            {
               u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }
            /*else if(pElement->rStdMsgQueue.bToDelete == TRUE)
            {
                 u32ErrorCode = OSAL_E_NOPERMISSION;
            }*/
            else
            {
            }

            if(u32ErrorCode == OSAL_E_NOERROR) 
            {
               msec = pOsalData->u32MqPostTmo;
               /* step 3 check for full message queue and wait , when queue is greater minimum size*/
               if((pTemp->ActualMessage == pTemp->MaxMessages)&&(pTemp->MaxMessages > 1))
               {
#ifdef BLOCKING_TIMEOUT
                  if(msec == 0)msec = BLOCKING_TIMEOUT;
                  pTemp->bBlocked = TRUE;
                  if(pOsalData->u32NoMqBlockEntry & 0x01)
                  {
                     /* no entries required */
                  }
                  else
                  {
                     u32Temp = (tU32)OSAL_ThreadWhoAmI();
                     bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,OSAL_ThreadWhoAmI());
                     vWritePrintfErrmem("Task:%s(ID:%d) block activated at MQ: Handle 0x%x Name:%s \n",
                                        name,(unsigned int)u32Temp, (uintptr_t)pCurrentEntry,pCurrentEntry->szName); /* lint !e613 */ /* u32ErrorCode ensures that hMQ is a valid pointer value */
                  }
                  if(pOsalData->u32NoMqBlockEntry & 0x10)
                  {
                     vReactOnMqOverflow(pCurrentEntry);
                  }
#endif //BLOCKING_TIMEOUT
               }
               if(msec)
               {
                  u32ErrorCode = u32GemTmoStruct(&tim,msec);
               }
            }
            if(u32ErrorCode == OSAL_E_NOERROR) 
            {
               if(pCurrentEntry->bTraceCannel == TRUE)
               {
                   TraceString("OSALMQ TID:%d Start Send Timeout:%d msec MQ:%s",OSAL_ThreadWhoAmI(),msec,pCurrentEntry->szName);
               }
               /* converted the OSAL priority to linux specific priority(for OSAL : 0:higest prio 7:lowest prio
                  for Linux: 0:lowest prio 7:highest prio)*/
               u32Prio_Li = OSAL_C_U32_MQUEUE_PRIORITY_LOWEST - u32Prio ;
               if(pOsalData->bCheckCcaMsg)
               {
                   vTraceCcaMsg((OSAL_trMessage*)pcou8Msg,u32Length,pCurrentEntry,"OSAL_s32MessageQueuePost");/*lint !e826 !e1773*/
               }
#ifndef NO_SIGNALAWARENESS
               while(1)
               {
#endif
                  /* reset error code for retry */
                  u32ErrorCode = OSAL_E_NOERROR;
#ifdef MQ_NOTIFY_VIA_OSAL
                  if(pCurrentEntry->bNotify == TRUE)
                  {
                     if((s32ReturnValue = mq_getattr(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat)) == 0)
                     {
                         if(mqstat.mq_curmsgs == 0)pCurrentEntry->bTriggerCb = TRUE;
                         pTemp->ActualMessage = u32ActualMessage = mqstat.mq_curmsgs;
                     }
                     else
                     {
                         TraceString("OSALMQ Send mq_getattr errno:%d %s",errno,pCurrentEntry->szName);
                         vWritePrintfErrmem("OSALMQ Send mq_getattr errno:%d %s \n",errno,pCurrentEntry->szName);
                         if(errno == EBADF)
                         {
                            vRestoreMq(pCurrentEntry);
                            if((s32ReturnValue = mq_getattr(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat)) == -1)
                            {
                               TraceString("OSALMQ Send mq_getattr errno:%d %s",errno,pCurrentEntry->szName);
                               vWritePrintfErrmem("OSALMQ Send mq_getattr errno:%d %s \n",errno,pCurrentEntry->szName);
                            }
                         }
                     }
                  }
#endif
                  if(msec)
                  {
                     s32ReturnValue = mq_timedsend((mqd_t)prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                                                   (const char*)pcou8Msg,//pcSendMsg,
                                                   u32Length,//pTemp->MaxLength, 
                                                   u32Prio_Li,
                                                   &tim);
                  }
                  else
                  {
                     s32ReturnValue = mq_send(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                                              (const char*)pcou8Msg,//pcSendMsg, 
                                              u32Length,//pTemp->MaxLength,
                                              u32Prio_Li);
                  }
                  if(s32ReturnValue != -1)
                  {
                      if(u32ActualMessage == 0xffffffff)
                      {
                         u32ActualMessage = pTemp->ActualMessage++;
                      }
#ifndef NO_SIGNALAWARENESS
                      break;
#endif
                  }
                  else if(errno != EINTR)
                  {
                     /* message queue wait is failed not because of signal 
                        check queue overflow */
                     if((EAGAIN == errno)||(ETIMEDOUT == errno))
                     {
                         if(mq_getattr(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat) == 0)
                         {
                            if(mqstat.mq_curmsgs < mqstat.mq_maxmsg)
                            {
                               TraceString("OSALMQ %s retry Status Cur:%d Max:%d \n",
                                                  pCurrentEntry->szName,mqstat.mq_curmsgs,mqstat.mq_maxmsg);
                               vWritePrintfErrmem("OSALMQ %s retry Status Cur:%d Max:%d \n",
                                                  pCurrentEntry->szName,mqstat.mq_curmsgs,mqstat.mq_maxmsg);
                               return OSAL_s32MessageQueuePost(hMQ,pcou8Msg,u32Length,u32Prio);
                            }
                            pTemp->ActualMessage = u32ActualMessage = mqstat.mq_curmsgs;
                         }
                         else
                         {
                            TraceString("OSALMQ after Send mq_getattr errno:%d %s \n",errno,pCurrentEntry->szName);
                            vWritePrintfErrmem("OSALMQ after Send mq_getattr errno:%d %s \n",errno,pCurrentEntry->szName);
                         }
                         u32ErrorCode = OSAL_E_QUEUEFULL;
                         vReactOnMqOverflow(pCurrentEntry);
                         break;
                     }
                     else
                     {
                        u32ErrorCode = u32ConvertErrorCore(errno);
                        if(errno == EBADF)
                        {
                           TraceString("OSALMQ mq_send Task:%d restores Linux File Descriptor Handle:%d",
                                       OSAL_ThreadWhoAmI(),prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx);
                           vWritePrintfErrmem("OSALMQ mq_send Task:%d restores Linux File Descriptor Handle:%d\n",
                                              OSAL_ThreadWhoAmI(),prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx);
                           vRestoreMq(pCurrentEntry);
                        }
                        break;
                     }
                  }
                  /* We are here because of signal. lets try again. */
#ifndef NO_SIGNALAWARENESS
               }// end while
#endif
            } //if(u32ErrorCode == OSAL_E_NOERROR)
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_DOESNOTEXIST;
      }
   }//if(u32ErrorCode == OSAL_E_NOERROR) 

#ifdef MQ_NOTIFY_VIA_OSAL
   if((u32ErrorCode == OSAL_E_NOERROR)&&(pCurrentEntry)&&(pCurrentEntry->bTriggerCb == TRUE)
    &&(pCurrentEntry->callbackpididx < (tS32)pOsalData->u32MaxNrProcElements)
    && (pCurrentEntry->pcallback )&&(pCurrentEntry->callbackpididx >= 0))
   {
        pCurrentEntry->bTriggerCb = FALSE;
        if( pCurrentEntry->callbackpid == OSAL_ProcessWhoAmI()) // check if process is the same
        {
           vExecuteCallback(pCurrentEntry);
        }
        else
        {
           vPostToCallbackQueue(pCurrentEntry);
        }
   }
#endif
#ifdef USE_EVENT_AS_CB
   if((u32ActualMessage == 1)&&(pCurrentEntry)&&(pCurrentEntry->hEvent))
   {
      OSAL_s32EventPost(pCurrentEntry->hEvent, pCurrentEntry->mask, pCurrentEntry->enFlags);
   }
#endif

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      if((u32ErrorCode != OSAL_E_TIMEOUT)||
        ((pCurrentEntry)&&(pCurrentEntry->bTraceCannel == TRUE))||(u32OsalSTrace & 0x00000020))
      {
#ifdef SHORT_TRACE_OUTPUT
          u32Temp = (tU32)OSAL_ThreadWhoAmI();
          OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_SEND);
          bGetThreadNameForTID(&au8Buf[1],8,(tS32)u32Temp);
          OSAL_M_INSERT_T32(&au8Buf[9],(tU32)u32Temp);
          OSAL_M_INSERT_T32(&au8Buf[13],(tU32)&hMQ);
          /* u32ActualMessage  */
          OSAL_M_INSERT_T32(&au8Buf[19],u32ErrorCode);
          OSAL_M_INSERT_T32(&au8Buf[23],u32Prio);
          if(pCurrentEntry)
          {
            OSAL_M_INSERT_T16(&au8Buf[17],(tU16)u32ActualMessage);
            u32Temp = strlen(pCurrentEntry->szName);
            memcpy (&au8Buf[27],pCurrentEntry->szName,u32Temp);
          }
          else
          {
            u32Temp = strlen("Unknown");
            memcpy (&au8Buf[27],"Unknown",u32Temp);
          }
          LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,u32Temp+27);
          if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_TIMEOUT)&&(u32OsalSTrace & ~0x00000020))
          {
               vWriteToErrMem(OSAL_C_TR_CLASS_SYS_MQ,au8Buf,u32Temp+27,0);
          }
#else
          u32Temp = (tU32)OSAL_ThreadWhoAmI();
          au8Buf[0] = OSAL_STRING_OUT;
          bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,u32Temp);
          if(pCurrentEntry)
          {
#ifdef NO_PRIxPTR
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Send      to MQ: User Handle 0x%x MQ Name:%s Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin, (((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32Prio);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Send      to MQ: User Handle 0x%" PRIxPTR " MQ Name:%s Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin, (((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32Prio);
#endif
          }
          else
          {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Send      to MQ: User Handle 0x%x MQ Name:%s Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(unsigned int)0, "Unknown",(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32Prio);
          }
          LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
          if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_TIMEOUT)&&(u32OsalSTrace & ~0x00000020))
          {
                 vWritePrintfErrmem(au8Buf);
          }
#endif
          if(bTraceHandle)
          {
             if(pCurrentEntry)
             {
                TraceString("Snd MQ:%s OSAL User Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",pCurrentEntry->szName,hMQ,
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)&hMQ),
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);
             }
             else
             {
                TraceString("Snd MQ:? OSAL User Handle:0x%x Linux Handle:? Offset:0x%x Pid:? Count:?",hMQ,GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)&hMQ));
             }
          }
      }
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }
   else
   {
       if((pCurrentEntry->bTraceCannel == TRUE)||(u32OsalSTrace & 0x00000020))/*lint !e613 pCurrentEntry already checked */
       {
#ifdef SHORT_TRACE_OUTPUT
           u32Temp = (tU32)OSAL_ThreadWhoAmI();
           OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_SEND);
           bGetThreadNameForTID(&au8Buf[1],8,(tS32)u32Temp);
           OSAL_M_INSERT_T32(&au8Buf[9],u32Temp);
           OSAL_M_INSERT_T32(&au8Buf[13],(tU32)&hMQ);
           OSAL_M_INSERT_T16(&au8Buf[17],(tU16)u32ActualMessage);
           OSAL_M_INSERT_T32(&au8Buf[19],u32ErrorCode);
           OSAL_M_INSERT_T32(&au8Buf[23],u32Prio);
           u32Temp = (tU32)strlen(pCurrentEntry->szName);/*lint !e613 *//* pointer already checked*/ 
           memcpy (&au8Buf[27],pCurrentEntry->szName,u32Temp);/*lint !e613 *//* pointer already checked*/ 
           LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, /*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,au8Buf,u32Temp+27);
#else
           u32Temp = (tU32)OSAL_ThreadWhoAmI();
           au8Buf[0] = OSAL_STRING_OUT;
           bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,u32Temp);
#ifdef NO_PRIxPTR
           snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Send      to MQ: User Handle 0x%x MQ Name:%s Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(intptr_t)((tMQUserHandle*)hMQ)->pOrigin, (((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32Prio);
#else
           snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Send      to MQ: User Handle 0x%" PRIxPTR " MQ Name:%s Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(intptr_t)((tMQUserHandle*)hMQ)->pOrigin, (((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32Prio);
#endif
           LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
#endif
       }
   }
   return( s32ReturnValue );
}



void vStartMeasureProcessingTime(trStdMsgQueue* pStdMsgQEntry)
{
   if(pStdMsgQEntry->u32MessagePrcessing != MEASUREMENT_SWITCHOFF)
   {
      pStdMsgQEntry->u32MessagePrcessing = MSG_PROCESSING_ACTIVE;
      pStdMsgQEntry->u32IdxMPT++;
      if(pStdMsgQEntry->u32IdxMPT == OSAL_C_MSG_TIME_ARR_SIZE)
      {   pStdMsgQEntry->u32IdxMPT = 0;   }
      pStdMsgQEntry->u32MsgTimStamp[pStdMsgQEntry->u32IdxMPT] = pOsalData->u32Seconds;
      pStdMsgQEntry->u32MsgCount[pStdMsgQEntry->u32IdxMPT] = pStdMsgQEntry->ActualMessage;
   }
}

void vStopMeasureProcessingTime(trStdMsgQueue* pStdMsgQEntry,trMqueueElement *pCurrentEntry)
{
   if(pStdMsgQEntry->u32MessagePrcessing != MEASUREMENT_SWITCHOFF)
   {
      char name[16];
      if((pStdMsgQEntry->u32MessagePrcessing == MSG_PROCESSING_ACTIVE)&&(pOsalData->u32Seconds))
      {
         pStdMsgQEntry ->u32MsgPrcTim[pStdMsgQEntry ->u32IdxMPT] = pOsalData->u32Seconds - pStdMsgQEntry->u32MsgTimStamp[pStdMsgQEntry->u32IdxMPT];
         if(pStdMsgQEntry ->u32MsgPrcTim[pStdMsgQEntry ->u32IdxMPT] > 2)
         {
            pStdMsgQEntry->u32LongMsgPrcTim[pStdMsgQEntry->u32IdxLMPT] = pStdMsgQEntry->u32MsgPrcTim[pStdMsgQEntry->u32IdxMPT];
            pStdMsgQEntry ->u32LongMsgTimStamp[pStdMsgQEntry->u32IdxLMPT] = pStdMsgQEntry->u32MsgTimStamp[pStdMsgQEntry->u32IdxMPT];
            bGetThreadNameForTID(&name[0],16,(tS32)pStdMsgQEntry ->u32RecTsk);
            TraceString("Task:%s(ID:%u) Message Processing needs %u seconds for MQ:%s",
                      name,pStdMsgQEntry->u32RecTsk,pStdMsgQEntry->u32LongMsgPrcTim[pStdMsgQEntry->u32IdxLMPT],pCurrentEntry->szName);
            /* store only long runners */
            pStdMsgQEntry ->u32IdxLMPT++;
            if(pStdMsgQEntry ->u32IdxLMPT == OSAL_C_LONG_MSG_TIME_ARR_SIZE)
            {   pStdMsgQEntry ->u32IdxLMPT = 0;   }
         }
      }
      pStdMsgQEntry->u32MessagePrcessing = MSG_PROCESSING_INACTIVE;
   }
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32MessageQueueWait
 *
 * DESCRIPTION: Query a message from  a MessageQueue. The obtained Message is
 *              the one with the higher priority existing in the MessageQueue
 *              and the actual length of the copied message is defined by the
 *              function param and not by the message length.
 *
 * PARAMETER:   hMQ       MessageQueue handle
 *              pu8Buffer pointer to the output buffer for the message copy
 *              u32Length actual length to e copied in bytes
 *              pu32Prio  pointer to the actual priority of the copied message
 *              msec      timeout
 *
 * ERRORS:      OSAL_E_BADFILEDESCRIPTOR  hMQ is not a valid handle
 *              OSAL_E_MSGTOOLONG         length exceeds the field MaxLength
 *                                        defined in creation
 *              OSAL_E_TIMEOUT            the timeout is reached without
 *                                        any message in the MQueue
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 * 13.12.12  | Fix for OSAL MQ priority issue         |sja3kor 
 * 17.12.12  | Fix for MMS CFAGII-501.Boundary check  |sja3kor
 *           |  condition changed.                    | 
 * --.--.--  | ----------------                       | -------, -----
 * 16.01.13  |Fix for Wrong timeout for               |sja3kor 
 *           |Linux messagequeue post/wait CFAGII-506 | 
 * 21.01.13  | Integrating GMNGA-48007 Fix            | SWM2KOR   
 *****************************************************************************/
tS32 OSAL_s32MessageQueueWait( OSAL_tMQueueHandle hMQ,
                               tPU8 pu8Buffer,
                               tU32 u32Length,
                               tPU32 pu32Prio,
                               OSAL_tMSecond msec)
{
   tU32 u32CopiedBytes = 0;
   tS32 s32Val = 0;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ); /* step 1 user handle check */
   tU32 u32ActualPrio = 0xffffffff;
   trMqueueElement *pCurrentEntry = NULL;
   tU32 u32ActualMessage = 0xffffffff;
   trStdMsgQueue* pStdMsgQEntry = NULL;
   struct timespec tim ={0,0};
   struct mq_attr mqstat = {0};
   tU32 u32Temp = 0;
   char au8Buf[251];
//  tPU8 pRecMsg = pu8Buffer;
//  char cResMsgBuffer[1024];
   char name[TRACE_NAME_SIZE];

#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
    return OSAL_s32ShMemMessageQueueWait(hMQ,pu8Buffer,u32Length,pu32Prio,msec);
#endif

   if(u32ErrorCode == OSAL_E_NOERROR) 
   {
      /* check permissions*/
      if(((tMQUserHandle*)hMQ)->enAccess == OSAL_EN_WRITEONLY) 
      {
          u32ErrorCode = OSAL_E_NOPERMISSION;
      }
   }
  if(u32ErrorCode == OSAL_E_NOERROR)
  {
     pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
     if(pCurrentEntry!= OSAL_NULL )
     {
/*        if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
        {
           CheckandCorrectMqStaticValues();
        }*/
        pStdMsgQEntry = &pStdMQEl[pCurrentEntry->u32EntryIdx];
        if(pStdMsgQEntry->bToDelete == TRUE)
        {
           msec = OSAL_C_TIMEOUT_NOBLOCKING;
        }
        pStdMsgQEntry->u16WaitCounter++;

       /*     if(u32Length > pStdMsgQEntry->MaxLength)
            {
               u32ErrorCode = OSAL_E_MSGTOOLONG;
            }
            else if(u32Length < pStdMsgQEntry->MaxLength)
            {
                memcpy(cResMsgBuffer,pu8Buffer,u32Length);
                pRecMsg = (tU8*)&cResMsgBuffer[0];
            }*/

        if(pStdMsgQEntry ->u32RecTsk == 0)
        {
           pStdMsgQEntry->u32RecTsk = OSAL_ThreadWhoAmI();
           pStdMsgQEntry->u32RecPrc = OSAL_ProcessWhoAmI();
        }
        /* Stop message processing time measurement */
        vStopMeasureProcessingTime(pStdMsgQEntry,pCurrentEntry);

        if(msec == OSAL_C_TIMEOUT_NOBLOCKING)
        {
           if(mq_getattr((mqd_t)prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat) == 0)
           {
              if(mqstat.mq_curmsgs == 0)
              {
                 u32ErrorCode = OSAL_E_TIMEOUT;
              }
              else
              {
                 /* message is in the queue we can access with timeout forever */
                 msec = OSAL_C_TIMEOUT_FOREVER;
              }
              pStdMsgQEntry->ActualMessage = u32ActualMessage = mqstat.mq_curmsgs;
           }
           else
           {
               TraceString("OSALMQ wait mq_getattr %s failed errno:%d \n",pCurrentEntry->szName,errno);
               vWritePrintfErrmem("OSALMQ wait mq_getattr %s failed errno:%d \n",pCurrentEntry->szName,errno);
               u32ErrorCode = u32ConvertErrorCore(errno);
           }
        }
        else if(msec != OSAL_C_TIMEOUT_FOREVER)
        {
           u32ErrorCode = u32GemTmoStruct(&tim,msec);
        }
        if(u32ErrorCode == OSAL_E_NOERROR)
        {
#ifndef NO_SIGNALAWARENESS
           while(1)
           {
#endif
               /* reset error code for retry */
               u32ErrorCode = OSAL_E_NOERROR;
               /* store old message content */
               u32Temp = *((tPU32)pu8Buffer);/*lint !e826 PQM_authorized_529 */
               if(msec == OSAL_C_TIMEOUT_FOREVER)
               {
                  pCurrentEntry->u32Timeout = msec;
                  s32Val = mq_receive((mqd_t)prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                                      (char*)pu8Buffer,//pRecMsg,
                                      (size_t)u32Length,//pStdMsgQEntry->MaxLength, 
                                      (unsigned int*)pu32Prio);
               }
               else
               {
                  /* check for MQ supervison for clock monotonic behavior */
                  if(bTimOutMqSvActive == TRUE)
                  {
                    pCurrentEntry->u32Timeout = (OSAL_ClockGetElapsedTime() + msec)/SV_RESOLUTION;
                    /* check if queue has to beadded to supervision list */
                    if(pCurrentEntry->bSvActive == FALSE)
                    {
                       pCurrentEntry->bSvActive = TRUE;
                       vAddRecMqToList(pCurrentEntry);
                       if(u32LocalTimerSig == 0)
                       {
                          TraceString("Cannot start MQ supervision for PID:%d ", getpid());
                       }
                       else
                       { 
                          /* start timeout supervision timer if required */
                          pthread_once(&SvSecTimer_is_initialized,&SetupSupervisionTimer);
                       }
                    }
                  }
                  s32Val =  mq_timedreceive((mqd_t)prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                                            (char*)pu8Buffer,//pRecMsg,
                                            u32Length, //pStdMsgQEntry->MaxLength, 
                                            (unsigned int*)pu32Prio,
                                            &tim);
               }
               if(pCurrentEntry->bSvActive == TRUE)
               {
                  /* check for time changed trigger */
                  if((s32Val == sizeof(tU32))&&(*((tPU32)pu8Buffer) == 0xA3A3A3A3)) /*lint !e826 PQM_authorized_529 */ /* used buffer is always size of tU32*/
                  {
                      TraceString("OSALMQ received trigger for MQ:%s ", pCurrentEntry->szName);
                    //  vWritePrintfErrmem("OSALMQ received trigger for MQ:%s \n", pCurrentEntry->szName);
                      u32ErrorCode = OSAL_E_INTERRUPT;
                      errno = EINTR;
                      /* restore old message buffer content */
                      *((tPU32)pu8Buffer) = u32Temp;/*lint !e826 PQM_authorized_529 */
                      if(msec != OSAL_C_TIMEOUT_FOREVER)
                      {
                         tU32 u32Time = OSAL_ClockGetElapsedTime()/SV_RESOLUTION;
                         /* check for real timeout */
                         if(pCurrentEntry->u32Timeout <= u32Time)
                         {
                            /* check for another message is availbale*/
                            if(mqstat.mq_curmsgs == 0)
                            {
                               TraceString("OSALMQ Timeout via Trigger for %s",pCurrentEntry->szName);
                               u32ErrorCode = OSAL_E_TIMEOUT;
                               break;
                            }
                         }
                         else
                         {
                            if(msec)
                            {
                               msec = pCurrentEntry->u32Timeout - u32Time;
                               /* calculate new timeout */ 
                               (void)u32GemTmoStruct(&tim,msec);
                            }
                         }
                      }
                  }
               }
               if(s32Val != -1)
               {
                  if(mq_getattr(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat) == 0)
                  {
                     pStdMsgQEntry->ActualMessage = u32ActualMessage = mqstat.mq_curmsgs;
                  }
                  pCurrentEntry->u32Timeout = 0;
                  if(pu32Prio != OSAL_NULL)
                  {
                     /*mq_receive API gives the linux specific prio(returns here pu32Prio )(for OSAL : 0:higest prio 7:lowest prio
                       for Linux: 0:lowest prio 7:highest prio)So,converted prio back to OSAL specific priority */
                     *pu32Prio = OSAL_C_U32_MQUEUE_PRIORITY_LOWEST - *pu32Prio ;
                  }
                  u32CopiedBytes = (tU32)s32Val;
                  if(u32ErrorCode != OSAL_E_INTERRUPT)
                  {
#ifndef NO_SIGNALAWARENESS
                     break;
#endif
                  }
               }
               else if(errno != EINTR)
               {
                  /* message queue wait is failed not because of signal */
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  if(errno == EBADF)
                  {
                      TraceString("OSALMQ mq_receive Task:%d restores Linux File Descriptor Handle:%d",OSAL_ThreadWhoAmI(),prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx);
                      vWritePrintfErrmem("OSALMQ mq_receive Task:%d restores Linux File Descriptor Handle:%d\n",OSAL_ThreadWhoAmI(),prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx);
                      vRestoreMq(pCurrentEntry);
                  }
                  else
                  {
                     if(OSAL_E_TIMEOUT != u32ErrorCode)
                     {
                        vWritePrintfErrmem("OSALMQ mq_receive EntryIdx:%d errno:%d  %s \n",
                                           pCurrentEntry->u32EntryIdx,errno,pCurrentEntry->szName);
                     }
                     break;
                  }
               }
#ifndef NO_SIGNALAWARENESS
               /* We are here because of signal. lets try again. */
           }// end while
#endif
        }// if(u32ErrorCode == OSAL_E_NOERROR)
#ifdef MQ_NOTIFY_VIA_OSAL
        if(u32ErrorCode == OSAL_E_TIMEOUT)
        {
           pCurrentEntry->bTriggerCb = TRUE;/*lint !e613 when u32ErrorCode == OSAL_E_TIMEOUT valid pCurrentEntry*/
        }
#endif
        if(u32CopiedBytes)
        {
           /* Start measurement of message processing times */
           vStartMeasureProcessingTime(pStdMsgQEntry);
           
           if(pOsalData->bCheckCcaMsg)
           {
              vTraceCcaMsg((OSAL_trMessage*)pu8Buffer,u32Length,pCurrentEntry,"OSAL_s32MessageQueueWait");/*lint !e826 */
           }
#ifdef BLOCKING_TIMEOUT
           if((pStdMsgQEntry->bBlocked)&&(pStdMsgQEntry->ActualMessage < pStdMsgQEntry->MaxMessages))
           {
              if(pOsalData->u32NoMqBlockEntry & 0x01)
              {
                /* no entries required*/
              }
              else
              {
                 u32Temp = (tU32)OSAL_ThreadWhoAmI();
                 bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,OSAL_ThreadWhoAmI());
 #ifdef NO_PRIxPTR
                  vWritePrintfErrmem("Task:%s(ID:%d) block released at MQ: Handle 0x%x Name:%s \n",
                                     name,(unsigned int)u32Temp, (uintptr_t)pCurrentEntry, pCurrentEntry->szName);/* lint !e613 */ /* u32ErrorCode ensures that hMQ is a valid pointer value */
 #else
                 vWritePrintfErrmem("Task:%s(ID:%d) block released at MQ: Handle 0x%" PRIxPTR " Name:%s \n",
                                     name,(unsigned int)u32Temp, (uintptr_t)pCurrentEntry, pCurrentEntry->szName);/* lint !e613 */ /* u32ErrorCode ensures that hMQ is a valid pointer value */
#endif
              }
              pStdMsgQEntry->bBlocked = FALSE;
           }
#endif
        } //if(u32CopiedBytes)
     }
     else//if( pCurrentEntry!= OSAL_NULL )
     {
        u32ErrorCode = OSAL_E_INVALIDVALUE;
     }//if( pCurrentEntry!= OSAL_NULL )
  }//if(u32ErrorCode == OSAL_E_NOERROR)



  if( u32ErrorCode != OSAL_E_NOERROR )
  {
     if((u32ErrorCode != OSAL_E_TIMEOUT)||
        ((pCurrentEntry)&&(pCurrentEntry->bTraceCannel == TRUE))||(u32OsalSTrace & 0x00000020))
     {
#ifdef SHORT_TRACE_OUTPUT
         u32Temp = (tU32)OSAL_ThreadWhoAmI();
         OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_RECEIVE);
         bGetThreadNameForTID(&au8Buf[1],8,(tS32)u32Temp);
         OSAL_M_INSERT_T32(&au8Buf[9],(tU32)u32Temp);
         OSAL_M_INSERT_T32(&au8Buf[13],(tU32)&hMQ);
          /* u32ActualMessage  */
         OSAL_M_INSERT_T32(&au8Buf[19],u32ErrorCode);
         OSAL_M_INSERT_T32(&au8Buf[23],u32ActualPrio);
         if(pCurrentEntry)
         {
            OSAL_M_INSERT_T16(&au8Buf[17],(tU16)u32ActualMessage);
            u32Temp = strlen(pCurrentEntry->szName);
            memcpy (&au8Buf[27],pCurrentEntry->szName,u32Temp);
         }
         else
         {
            u32Temp = strlen("Unknown");
            memcpy (&au8Buf[27],"Unknown",u32Temp);
         }
         LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,u32Temp+27);
         if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_TIMEOUT)&&(u32OsalSTrace & ~0x00000020))
         {
                 vWriteToErrMem(OSAL_C_TR_CLASS_SYS_MQ,au8Buf,u32Temp+27,0);
         }
#else
         u32Temp = (tU32)OSAL_ThreadWhoAmI();
         au8Buf[0] = OSAL_STRING_OUT;
         bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,OSAL_ThreadWhoAmI());
         if(pCurrentEntry)
         {
#ifdef NO_PRIxPTR
            snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Receive from MQ: User Handle 0x%x Name:%s(33,32) Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin,(((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32ActualPrio);
#else
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Receive from MQ: User Handle 0x%" PRIxPTR " Name:%s(33,32) Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin,(((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32ActualPrio);
#endif
         }
         else
         {
             snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Receive from MQ: User Handle 0x%x Name:%s(33,32) Msg Count:%d Error:0x%x Prio:%d",
                      name,(unsigned int)u32Temp,(unsigned int)u32ErrorCode,"Unknown",(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32ActualPrio);
         }
         LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
         if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_TIMEOUT)&&(u32OsalSTrace & ~0x00000020))
         {
            vWritePrintfErrmem(au8Buf);
         }
#endif
         if(bTraceHandle)
         {
             if(pCurrentEntry)
             {
                TraceString("Rec MQ:%s OSAL User Handle:0x%x Linux Handle:%d Offset:0x%x Pid:%d Count:%d",pCurrentEntry->szName,hMQ,
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,
                GetMemPoolOffset(&MqMemPoolHandle,(uintptr_t*)&hMQ),
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcIdx,
                prLiMqMap[pCurrentEntry->u32EntryIdx].u32PrcCnt);
             }
             else
             {
                TraceString("Rec MQ:? OSAL User Handle:0x%x Linux Handle:? Offset:0x%x Pid:? Count:?",hMQ,GetMemPoolOffset(&MqMemPoolHandle, (uintptr_t*)&hMQ));
             }
         }
     }
     vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
#ifndef OSAL_NOWAIT_INVALIDVALUE
     if (u32ErrorCode == OSAL_E_INVALIDVALUE) {
        TraceString("Rec MQ: Wait a second...");
        sleep(1);
     }
#endif
  }
  else
  {
     if(((pCurrentEntry) &&(pCurrentEntry->bTraceCannel == TRUE))||(u32OsalSTrace & 0x00000020))
     {
#ifdef SHORT_TRACE_OUTPUT
        u32Temp = (tU32)OSAL_ThreadWhoAmI();
        OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_RECEIVE);
        bGetThreadNameForTID(&au8Buf[1],8,(tS32)u32Temp);
        OSAL_M_INSERT_T32(&au8Buf[9],(tU32)u32Temp);
        OSAL_M_INSERT_T32(&au8Buf[13],(tU32)&hMQ);
        OSAL_M_INSERT_T16(&au8Buf[17],(tU16)u32ActualMessage);
        OSAL_M_INSERT_T32(&au8Buf[19],u32ErrorCode);
        OSAL_M_INSERT_T32(&au8Buf[23],u32ActualPrio);
        u32Temp = (tU32)strlen(pCurrentEntry->szName);/*lint !e613 *//* pointer already checked*/ 
        memcpy (&au8Buf[27],pCurrentEntry->szName,u32Temp);/*lint !e613 *//* pointer already checked*/ 
        LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, /*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,au8Buf,u32Temp+27);
#else
        u32Temp = (tU32)OSAL_ThreadWhoAmI();
        au8Buf[0] = OSAL_STRING_OUT;
        bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,OSAL_ThreadWhoAmI());
#ifdef NO_PRIxPTR
        snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Receive from MQ: User Handle 0x%x Name:%s(33,32) Msg Count:%d Error:0x%x Prio:%d",
                 name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin,(((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32ActualPrio);
#else
        snprintf(&au8Buf[1],250,"Task:%s(ID:%d) Receive from MQ: User Handle 0x%" PRIxPTR " Name:%s(33,32) Msg Count:%d Error:0x%x Prio:%d",
                 name,(unsigned int)u32Temp,(uintptr_t)((tMQUserHandle*)hMQ)->pOrigin,(((tMQUserHandle*)hMQ)->pOrigin)->szName,(unsigned int)u32ActualMessage,(unsigned int)u32ErrorCode,(unsigned int)u32ActualPrio);
#endif
        LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
#endif
     }
  }
  return((tS32)u32CopiedBytes );
}

tS32 OSAL_s32MessageQueueWaitMonotonic(OSAL_tMQueueHandle hMQ,
                                       tPU8 pu8Buffer,
                                       tU32 u32Length,
                                       tPU32 pu32Prio,
                                       OSAL_tMSecond msec)
{ 
   tS32 s32Ret;
   OSAL_tMSecond rVal[2];
   while(1)
   {
      rVal[0] = OSAL_ClockGetElapsedTime();
      if((s32Ret = OSAL_s32MessageQueueWait(hMQ,pu8Buffer,u32Length,pu32Prio,msec)) > 0 )
      {
		 /* valid Message received leave the loop*/
         break;
      }
	  if(OSAL_u32ErrorCode() != OSAL_E_TIMEOUT)
	  {
		  /* No timeout error leave the loop */
		  break;
	  }
	  else
	  {
         rVal[1] = OSAL_ClockGetElapsedTime();
	     if((rVal[1] - rVal[0]) >= msec)
	     {
			 /* correct timeout is happened  leave the loop */
	         break;
		 }
		 TraceString("OSALMQ Invalid timeout %d instaead of %d",rVal[1] - rVal[0],msec);
		 /* invalid timeout retry calculate new timeout */
		 msec -= (rVal[1] - rVal[0]);
	  }
   }
   return s32Ret;
}

tS32 OSAL_s32MessageQueuePriorityWait(OSAL_tMQueueHandle hMQ,  
                                      tPU8 pu8Buffer, tU32 u32Length, tPU32 pu32Prio, 
                                      OSAL_tMSecond msec, tU32 u32ThresholdPrio)
{
   ((tVoid)hMQ); // satisfy lint
   ((tVoid)pu8Buffer); // satisfy lint
   ((tVoid)u32Length); // satisfy lint
   ((tVoid)pu32Prio); // satisfy lint
   ((tVoid)msec); // satisfy lint
   ((tVoid)u32ThresholdPrio); // satisfy lint
   return 0;
}

void vTraceMqNotify(trMqueueElement *pCurrentEntry,tU32 u32ErrorCode)
{
   char au8Buf[101];
   tU32 Temp = (tU32)OSAL_ThreadWhoAmI();
#ifdef SHORT_TRACE_OUTPUT
   OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_MQ_REGISTER);
   bGetThreadNameForTID(&au8Buf[1],8,(tS32)Temp);
   OSAL_M_INSERT_T32(&au8Buf[9],(tU32)Temp);
   OSAL_M_INSERT_T32(&au8Buf[17],u32ErrorCode);
   if(pCurrentEntry)
   {
      OSAL_M_INSERT_T32(&au8Buf[13],(tU32)pCurrentEntry->pcallback);
      Temp = strlen(pCurrentEntry->szName);
      memcpy (&au8Buf[21],pCurrentEntry->szName,Temp);
   }
   else
   {
      Temp =0;
      OSAL_M_INSERT_T32(&au8Buf[13],Temp);
      au8Buf[21] = '\0';
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,Temp+21);
#else
   char name[TRACE_NAME_SIZE];
   au8Buf[0] = OSAL_STRING_OUT;;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)Temp);
#ifdef NO_PRIxPTR
    snprintf(&au8Buf[1],100," Task:%s(ID:%d) Register callback 0x%x Error:0x%x for MQ:%s",
             name,(unsigned int)Temp,(uintptr_t)pCurrentEntry->pcallback,(unsigned int)u32ErrorCode,pCurrentEntry->szName);
#else
    snprintf(&au8Buf[1],100," Task:%s(ID:%d) Register callback 0x%" PRIxPTR " Error:0x%x for MQ:%s",
             name,(unsigned int)Temp,(uintptr_t)pCurrentEntry->pcallback,(unsigned int)u32ErrorCode,pCurrentEntry->szName);
#endif
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
#endif
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32MessageQueueNotify
 *
 * DESCRIPTION: this function informs the calling Thread as soon as an empty
 *              message box receives a new message.
 *
 * PARAMETER:   OSAL_tMQueueHandle hMQ: message queue handle
 *              OSAL_tpfCallback pCallback: callback function
 *              tPVoid pvArg: parameter of the callback function
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MessageQueueNotify(OSAL_tMQueueHandle hMQ,
                                OSAL_tpfCallback pCallback,
                                tPVoid pvArg)
{
   tS32 s32RetVal = OSAL_OK;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ);
   tS32 s32TID = NOTATHREAD;
   trMqueueElement *pCurrentEntry = NULL;

#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
   return OSAL_s32ShMemMessageQueueNotify( hMQ,pCallback,pvArg);
#endif
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
       /* check if callback handler task for this process already exists */  
      tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
      if(s32PidEntry == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         /* check if callback handler task for this process already exists */  
         s32StartCbHdrTask(s32PidEntry);
      }
      pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
      if( pCurrentEntry!= OSAL_NULL )
      {
/*         if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
         {
            CheckandCorrectMqStaticValues();
         }*/

         if(LockOsal(&pOsalData->MqueueTable.rLock ) == OSAL_OK)
         {
            /* check if queue isn't deleted */
            if(pCurrentEntry->bIsUsed)
            {
               s32TID = (tS32)OSAL_ThreadWhoAmI();
#ifdef USE_EVENT_AS_CB
               if((uintptr_t)pCallback == 0xffffffff)
               {
                  if(pvArg != NULL)
                  {
                     trMqEventInf* pEvent = (trMqEventInf*)pvArg;
                     if(OSAL_s32EventOpen(pEvent->coszName,&pCurrentEntry->hEvent) != OSAL_OK)
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode=OSAL_E_DOESNOTEXIST;
                     }
                     else
                     {
                        pCurrentEntry->mask    = pEvent->mask;
                        pCurrentEntry->enFlags = pEvent->enFlags;
                     }
                  }
                  else
                  {
                     if(OSAL_s32EventClose(pCurrentEntry->hEvent) != OSAL_OK)
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
                        pCurrentEntry->hEvent = 0;
                     }
                  }
               }
               else//if((pCallback == NULL )&&(pvArg != NULL))
#endif
               {
                  if( pCallback == NULL )
                  {
                     if( pvArg == NULL )
                     {
                        if( pCurrentEntry->pcallback != NULL )
                        {
                           if(pCurrentEntry->callbacktid == s32TID )
                           {
#ifdef MQ_NOTIFY_VIA_OSAL
                              pCurrentEntry->bNotify = FALSE;
#else
                              if (mq_notify((mqd_t)rLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, NULL) != -1)
                              {
#endif
                                 /*force dispatch to disable callback */
                                 OSAL_s32ThreadWait(10);
                                 pCurrentEntry->pcallback = NULL;
                                 pCurrentEntry->callbacktid  = NOTATHREAD;
                                 s32RetVal = OSAL_OK;
#ifdef MQ_NOTIFY_VIA_OSAL
#else
                              }
                              else
                              {
                                 u32ErrorCode = u32ConvertErrorCore(errno);
                              }
#endif
                           }
                           else
                           {
                              s32RetVal=OSAL_ERROR;
                              u32ErrorCode=OSAL_E_BUSY;
                           }
                        }
                        else
                        {
                           s32RetVal=OSAL_ERROR;
                           pCurrentEntry->callbacktid  = NOTATHREAD;
                        }
                     }
                     else
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode=OSAL_E_INVALIDVALUE;
                     }
                  }
                  else
                  {
                     if( pCurrentEntry->pcallback == NULL )
                     {
                        pCurrentEntry->callbackpid  = OSAL_ProcessWhoAmI();
                        pCurrentEntry->callbacktid  = s32TID;
                        pCurrentEntry->pcallback    = pCallback;
                        pCurrentEntry->pcallbackarg = pvArg;
#ifdef MQ_NOTIFY_VIA_OSAL
                        pCurrentEntry->callbackpididx = s32GetPrcIdx(pCurrentEntry->callbackpid);
                        if(pCurrentEntry->callbackpididx == -1)
                        {
                          FATAL_M_ASSERT_ALWAYS();
                        }
                        pCurrentEntry->bNotify = TRUE;
#else
                        struct sigevent s = { 0 };
                        s.sigev_notify = SIGEV_THREAD;
                        s.sigev_notify_function = (void (*)(sigval_t))vExecuteCallback;
                        s.sigev_notify_attributes = NULL;
                        s.sigev_value.sival_ptr = pCurrentEntry;
                        if (mq_notify((mqd_t)rLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx, &s) != -1)
                        {
                           s32RetVal = OSAL_OK;
                        }
                        else
                        {
                           u32ErrorCode = u32ConvertErrorCore(errno);
                        }
#endif
                     }
                     else
                     {
                        s32RetVal = OSAL_ERROR;
                        u32ErrorCode = OSAL_E_BUSY;
                     }
                  }//if( pCallback == NULL )
               }//if((pCallback == NULL )&&(pvArg != NULL))
            }//if(pCurrentEntry->bIsUsed)
            UnLockOsal(&pOsalData->MqueueTable.rLock);
         }
     }
     else //if( pCurrentEntry!= OSAL_NULL )
     {
        u32ErrorCode = OSAL_E_INVALIDVALUE;
     }//if( pCurrentEntry!= OSAL_NULL )
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32RetVal = OSAL_ERROR;
      vTraceMqNotify(pCurrentEntry,u32ErrorCode);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if((pCurrentEntry->bTraceCannel)||(u32OsalSTrace & 0x00000020)) /*lint !e613 pointer already checked */
      {
        vTraceMqNotify(pCurrentEntry,u32ErrorCode);
      }
   }
   return(s32RetVal);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MessageQueueStatus
 *
 * DESCRIPTION: this function informs the calling Thread as soon as an empty
 *              message box receives a new message.
 *
 * PARAMETER:   OSAL_tMQueueHandle hMQ: message queue handle
 *              tPU32 pu32MaxMessage:   pointer to the number of messages or
 *                                      OSAL_NULL,
 *              tPU32 pu32MaxLength:    pointer to the number to the max
 *                                      length of the message or OSAL_NULL,
 *               tPU32 pu32Message:     pointer to the number of the current
 *                                      existing messages or OSAL_NULL,
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MessageQueueStatus( OSAL_tMQueueHandle hMQ,
                                 tPU32 pu32MaxMessage,
                                 tPU32 pu32MaxLength,
                                 tPU32 pu32Message)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ);
   trMqueueElement *pCurrentEntry = NULL;

#ifdef OSAL_SHM_MQ_FOR_LINUX_MQ
   return OSAL_s32ShMemMessageQueueStatus( hMQ,pu32MaxMessage,pu32MaxLength,pu32Message);
#endif

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      /* handle is already checked */
      pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
      if( pCurrentEntry!= OSAL_NULL )
      {
/*         if((pCurrentEntry->u32EntryIdx >= pOsalData->MqueueTable.u32UsedEntries)||(pCurrentEntry->u32MqueueID != LINUX_C_U32_MQUEUE_ID))
         {
            CheckandCorrectMqStaticValues();
         }*/
         /* check if queue isn't deleted */
         if(pCurrentEntry->bIsUsed)
         {
            trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
            struct mq_attr mqstat = {0};
            if(mq_getattr(prLiMqMap[pCurrentEntry->u32EntryIdx].u32MqIdx,&mqstat) == 0)
            {
               if( pu32MaxMessage != OSAL_NULL )
                         *pu32MaxMessage = pTemp->MaxMessages = mqstat.mq_maxmsg;
               if( pu32MaxLength  != OSAL_NULL )
                         *pu32MaxLength = mqstat.mq_msgsize;
               if( pu32Message    != OSAL_NULL )
                         *pu32Message     = pTemp->ActualMessage = mqstat.mq_curmsgs;
                  s32ReturnValue = OSAL_OK;
            }
            else
            {
                u32ErrorCode = u32ConvertErrorCore(errno);
            }
         }
         else
         {
            u32ErrorCode =OSAL_E_DOESNOTEXIST;
         }
      }
      else
      {
         u32ErrorCode=OSAL_E_BADFILEDESCRIPTOR;
      } 
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      if(pCurrentEntry)
      {
           vTraceMqInfo((tMQUserHandle*)hMQ,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
      else
      {
           vTraceMqInfo(NULL,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
   }
   else
   {
      if((pCurrentEntry->bTraceCannel)||(u32OsalSTrace & 0x00000020))/*lint !e613 pointer already checked */
      {
         vTraceMqInfo((tMQUserHandle*)hMQ,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
   }
   return( s32ReturnValue );
}


//#define TRACE_STEPS
#define WAIT_FOR_READ    0x001
#define WAIT_FOR_WRITE   0x010
//#define WAIT_FOR_ACCESS  0x100
/*****************************************************************************
 *
 * FUNCTION: OSAL_s32CreateRingBuffer
 *
 * DESCRIPTION: this function creates a ring buffer element
 *
 * PARAMETER:   tCString            : ring buffer name
 *              tU32                : ring buffer size
 *              OSAL_tenAccess      : ring buffer access rights
 *              OSAL_tMQueueHandle* : address of ring buffer handle
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
#define  LINUX_C_U32_RING_BUFFER_ID                    ((tU32)0x524E4742)
 tS32 OSAL_s32CreateRingBuffer(tCString coszName, tU32 u32Size,OSAL_tenAccess enAccess,OSAL_tMQueueHandle* pHandle)
{
   OSAL_tShMemHandle hShMem = (OSAL_tShMemHandle)OSAL_ERROR;
   OSAL_tEventHandle hEvent = (OSAL_tEventHandle)OSAL_ERROR;
   OSAL_tSemHandle hSem = (OSAL_tSemHandle)OSAL_ERROR;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)malloc(sizeof(tOsalRgBufHandle));
   tU32 u32Lenght = 0;
   tOsalRgBufDat* pRngBufDat= NULL;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
 
   if(pRngBufHandle)
   {
      if((pHandle)&&(u32Size)&&(coszName)&&(enAccess <= OSAL_EN_READWRITE)&&(strlen(coszName) < LINUX_C_MQ_MAX_NAMELENGHT))
      {
          /* ensure 4 Byte alignment */
          u32Lenght = u32Size % sizeof(tU32);
          if(u32Lenght)
          {
             u32Lenght = (sizeof(tU32) - u32Lenght) + u32Size + sizeof(tOsalRgBufDat);
             TraceString("Align Rng Buffer Memory");
          }			
          else			
          {
            u32Lenght = u32Size + sizeof(tOsalRgBufDat);
          }
			  
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"RG%s",coszName);
         hShMem = OSAL_SharedMemoryCreate(Name,enAccess,u32Lenght);
         if((s32ReturnValue = OSAL_s32EventCreateOpt(Name, &hEvent, CONSUME_EVENT|WAIT_MULTIPLE_TSK)) == OSAL_OK)
         {
            s32ReturnValue = OSAL_s32SemaphoreCreate(Name,&hSem,1);
         }
         if((hShMem != (OSAL_tShMemHandle)OSAL_ERROR)&&(s32ReturnValue != OSAL_ERROR))
         {
            pRngBufDat = (tOsalRgBufDat*)OSAL_pvSharedMemoryMap(hShMem,enAccess,u32Lenght,0);
            if(pRngBufDat)
            {
               pRngBufDat->u32Size          = u32Lenght;
               pRngBufDat->u32ReadOffset    = 0;
               pRngBufDat->u32WriteOffset   = 0;;
               pRngBufDat->u32OpnCnt        = 1;
               pRngBufDat->u32MemFree       = u32Size;
               pRngBufDat->u32ActualMessage = 0;               
               pRngBufDat->u32RBlock          = FALSE;               
               pRngBufDat->u32WBlock          = FALSE;               
               pRngBufDat->u32RBlockPost      = FALSE;               
               pRngBufDat->u32WBlockPost      = FALSE;               
               
               s32ReturnValue = strlen(coszName);
               if(s32ReturnValue >= (tS32)LINUX_C_MQ_MAX_NAMELENGHT)s32ReturnValue = LINUX_C_MQ_MAX_NAMELENGHT-1;
               strncpy(pRngBufDat->szName,coszName,s32ReturnValue);
               pRngBufDat->szName[LINUX_C_MQ_MAX_NAMELENGHT-1] = 0;
               
               pRngBufHandle->StartAdress = (uintptr_t)pRngBufDat;
               pRngBufHandle->MemStart    = pRngBufHandle->StartAdress + sizeof(tOsalRgBufDat);
               pRngBufHandle->MemEnd      = pRngBufHandle->MemStart + u32Size;
               pRngBufHandle->hEvent      = hEvent;
               pRngBufHandle->hLockSem    = hSem;
               pRngBufHandle->hShMem      = hShMem;
               pRngBufHandle->u32Magic    = LINUX_C_U32_RING_BUFFER_ID;
               pRngBufHandle->enAccess    = enAccess;
            //   (void)OSAL_s32EventPost(hEvent,WAIT_FOR_ACCESS,OSAL_EN_EVENTMASK_OR);

               s32ReturnValue = OSAL_OK;
               *pHandle = (OSAL_tMQueueHandle)pRngBufHandle;
            }
#ifdef TRACE_STEPS
            TraceString("Create Start:0x%x End:0x%x WriteOffset:%d ReadOffset:%d Free:%d Open:%d",
                        (tU32)pRngBufHandle->MemStart,pRngBufHandle->MemEnd,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,pRngBufDat->u32MemFree,pRngBufDat->u32OpnCnt);
#endif                        
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
         vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_NOSPACE;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   
   if(s32ReturnValue == OSAL_ERROR)
   {
      snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"RG%s",coszName);
      if(hShMem != (OSAL_tShMemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SharedMemoryClose(hShMem);
         (void)OSAL_s32SharedMemoryDelete(Name);
      }
      if(hEvent != (OSAL_tEventHandle)OSAL_ERROR)
      {
         (void)OSAL_s32EventClose(hEvent);
         (void)OSAL_s32EventDelete(coszName);
      }
      if(hSem != (OSAL_tSemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SemaphoreClose(hSem);
         (void)OSAL_s32SemaphoreDelete(coszName);
      }
   }
   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(coszName == NULL)coszName ="Unknown";
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Create RingBuffer Handle 0x%x Access:%d Error:0x%x Name:%s",
                   name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Create RingBuffer Handle 0x%" PRIxPTR " Access:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#endif
   }
   return s32ReturnValue;
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32OpenRingBuffer
 *
 * DESCRIPTION: this function opens an existing ring buffer element
 *
 * PARAMETER:   tCString            : ring buffer name
 *              OSAL_tenAccess      : ring buffer access rights
 *              OSAL_tMQueueHandle* : address of ring buffer handle
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32OpenRingBuffer(tCString coszName,OSAL_tenAccess enAccess,OSAL_tMQueueHandle* pHandle)
{
   OSAL_tShMemHandle hShMem = (OSAL_tShMemHandle)OSAL_ERROR;
   OSAL_tEventHandle hEvent = (OSAL_tEventHandle)OSAL_ERROR;
   OSAL_tSemHandle hSem = (OSAL_tSemHandle)OSAL_ERROR;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)malloc(sizeof(tOsalRgBufHandle));
   tOsalRgBufDat*   pRngBufDat = NULL;
   void* pRet= NULL;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
   
   if(pRngBufHandle)
   {
      if((pHandle)&&(coszName)&&(enAccess <= OSAL_EN_READWRITE))
      {
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"RG%s",coszName);
         hShMem = OSAL_SharedMemoryOpen(Name,enAccess);
         if((s32ReturnValue = OSAL_s32EventOpen(Name,&hEvent)) == OSAL_OK)
         {
            s32ReturnValue = OSAL_s32SemaphoreOpen(Name,&hSem);
         }
         if((hShMem != (OSAL_tShMemHandle)OSAL_ERROR)&&(s32ReturnValue != OSAL_ERROR))
         {
            pRngBufDat = (tOsalRgBufDat*)OSAL_pvSharedMemoryMap(hShMem,enAccess,sizeof(tOsalRgBufDat),0);
            pRet = OSAL_pvSharedMemoryMap(hShMem,enAccess,pRngBufDat->u32Size,0);
            if(pRet)
            {
               pRngBufDat->u32OpnCnt++;
 
               pRngBufHandle->StartAdress = (uintptr_t)pRet;
               pRngBufHandle->MemStart    = pRngBufHandle->StartAdress + sizeof(tOsalRgBufDat);
               pRngBufHandle->MemEnd      = pRngBufHandle->MemStart + pRngBufDat->u32Size - sizeof(tOsalRgBufDat);
               pRngBufHandle->hEvent      = hEvent;
               pRngBufHandle->hLockSem    = hSem;
               pRngBufHandle->hShMem      = hShMem;
               pRngBufHandle->u32Magic    = LINUX_C_U32_RING_BUFFER_ID;
               pRngBufHandle->enAccess    = enAccess;
               
               s32ReturnValue = OSAL_OK;
               *pHandle = (OSAL_tMQueueHandle)pRngBufHandle;
           /*    TraceString("Open Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Open:%d",
               (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,pRngBufDat->u32MemFree,pRngBufDat->u32OpnCnt);*/
            }
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
         vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_NOSPACE;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   
   if(s32ReturnValue == OSAL_ERROR)
   {
      snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"RG%s",coszName);
      if(hShMem != (OSAL_tShMemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SharedMemoryClose(hShMem);
         (void)OSAL_s32SharedMemoryDelete(Name);
      }
      if(hEvent != (OSAL_tEventHandle)OSAL_ERROR)
      {
         (void)OSAL_s32EventClose(hEvent);
         (void)OSAL_s32EventDelete(coszName);
      }
      if(hSem != (OSAL_tSemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SemaphoreClose(hSem);
         (void)OSAL_s32SemaphoreDelete(coszName);
      }
   }
   
   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Open RingBuffer Handle 0x%x Access:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Open RingBuffer Handle 0x%" PRIxPTR " Access:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#endif
   }
   return s32ReturnValue;
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32CloseRingBuffer
 *
 * DESCRIPTION: this function closes an existing ring buffer element
 *
 * PARAMETER:  OSAL_tMQueueHandle  : ring buffer handle
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32CloseRingBuffer(OSAL_tMQueueHandle Handle)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   char ObjName[LINUX_C_MQ_MAX_NAMELENGHT] = {0};
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)Handle;
   tOsalRgBufDat* pRngBufDat = NULL;
   
   if(pRngBufHandle && (Handle != (tU32)OSAL_ERROR) && (pRngBufHandle->u32Magic == LINUX_C_U32_RING_BUFFER_ID))
   {
      pRngBufDat = (tOsalRgBufDat*)pRngBufHandle->StartAdress;
    /*     TraceString("Write Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Open:%d",
                     (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,pRngBufDat->u32MemFree,pRngBufDat->u32OpnCnt);*/
      if(pRngBufDat)
      { 
          strncpy(ObjName,pRngBufDat->szName,LINUX_C_MQ_MAX_NAMELENGHT);
          pRngBufDat->u32OpnCnt--;
          if(pRngBufDat->u32OpnCnt == 0)
          { 
           // OSAL_s32DeleteRingBuffer(pRngBufDat->szName);
          }
      }
      s32ReturnValue = OSAL_s32SharedMemoryClose(pRngBufHandle->hShMem);
      s32ReturnValue = OSAL_s32EventClose(pRngBufHandle->hEvent);
      s32ReturnValue = OSAL_s32SemaphoreClose(pRngBufHandle->hLockSem);
      free((void*)Handle);
      if(s32ReturnValue != OSAL_OK)u32ErrorCode = OSAL_u32ErrorCode();
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   
   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(ObjName[0] == 0)strncpy(ObjName,"Unknown",strlen("Unknown"));
      if(s32ReturnValue != OSAL_OK)u32ErrorCode = OSAL_u32ErrorCode();
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Close RingBuffer Handle 0x%x Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,name);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Close RingBuffer Handle 0x%" PRIxPTR " Error:0x%x Name:%s",
                   name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,name);
#endif
   }
   return s32ReturnValue;
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32DeleteRingBuffer
 *
 * DESCRIPTION: this function deletes an existing ring buffer element
 *
 * PARAMETER:   tCString            : ring buffer name
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32DeleteRingBuffer(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tS32 s32ErrorCode = OSAL_E_NOERROR;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];

   snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"RG%s",coszName);
   (void)OSAL_s32SharedMemoryDelete(Name);
   s32ReturnValue = OSAL_s32SharedMemoryDelete(Name);
   s32ReturnValue = OSAL_s32EventDelete(Name);
   s32ReturnValue = OSAL_s32SemaphoreDelete(Name);
   if(OSAL_E_DOESNOTEXIST == OSAL_u32ErrorCode())
   {
      s32ReturnValue = OSAL_OK;
   }

   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
      if(s32ReturnValue == OSAL_ERROR)s32ErrorCode = OSAL_u32ErrorCode();
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Delete RingBuffer Error:%d Name:%s",
                  name,(unsigned int)u32Temp,s32ErrorCode,coszName);
   }

   return s32ReturnValue;
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_u32WriteToRingBuffer
 *
 * DESCRIPTION: this function writes to an existing ring buffer element
 *
 * PARAMETER:  OSAL_tMQueueHandle  : ring buffer handle
 *             char*               : buffer to copy to ringbuffer    
 *             tU32                : size of the buffer
 *             tU32                : timeout value
 *
 * RETURNVALUE: number of written bytes.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tU32 OSAL_u32WriteToRingBuffer(OSAL_tMQueueHandle Handle,char* ps8Buffer, tU32 Size,tU32 u32TimeOut)
{
   tS32 s32ReturnValue = OSAL_OK;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)Handle;
   tOsalRgBufDat* pRngBufDat = NULL;
   tBool bTrigger = FALSE;
   
   if(pRngBufHandle && (Handle != (tU32)OSAL_ERROR) && (pRngBufHandle->u32Magic == LINUX_C_U32_RING_BUFFER_ID))
   {
      if(pRngBufHandle->enAccess == OSAL_EN_READONLY)
      {
         Size = 0;
         u32ErrorCode = OSAL_E_NOPERMISSION;
         s32ReturnValue = OSAL_ERROR;
      }
      else
      {
         pRngBufDat = (tOsalRgBufDat*)pRngBufHandle->StartAdress;
         tU32 Size1,Size2;
         uintptr_t* pTmp;
         tU32 u32Mask = 0;
         /* check if enough space available otherwise wait */
         while(1)
         {
            if(pRngBufDat->u32MemFree < (Size+(2*sizeof(tU32))))
            {
               (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                pRngBufDat->u32WBlock++;
               (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                // TraceString("Write block Free:%d Open:%d Block%d",pRngBufDat->u32MemFree);
               if(pRngBufDat->u32WBlockPost)
               {
                  s32ReturnValue = OSAL_s32EventWait(pRngBufHandle->hEvent,WAIT_FOR_WRITE,OSAL_EN_EVENTMASK_OR,u32TimeOut,&u32Mask);
                  pRngBufDat->u32WBlockPost = 0 ;
                  (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                  pRngBufDat->u32WBlock++;
                  (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                  if(s32ReturnValue == OSAL_ERROR)
                  {
                     u32ErrorCode = OSAL_E_TIMEOUT;
                     s32ReturnValue = OSAL_ERROR;
                     TraceString("Write RngBuf:%s timeout:%d  %d memory free",pRngBufDat->szName,u32TimeOut,pRngBufDat->u32MemFree);
                     Size = 0;
                     break;
                  }
                  else
                  {
                     if(pRngBufDat->u32MemFree > (Size+(2*sizeof(tU32))))
                     {
                        break;
                     }
                  }
               }
               else
               {
                  (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                   pRngBufDat->u32WBlock++;
                  (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                   // repeat
               }
            }
            else
            {
               break;
            }
         }
#ifdef TRACE_STEPS
         TraceString("Write1 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                     (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                     pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
         /* check if memory is now ensured */
         if(s32ReturnValue == OSAL_OK)
         {
            tU32 u32ReleasedMem = 0;
            (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
            Size1 = pRngBufHandle->MemEnd - (pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset);     
            /* check for storing size info */
            if(Size1 < sizeof(tU32))
            {
               pRngBufDat->u32WriteOffset = 0;
               u32ReleasedMem += Size1;
            }
            pTmp = (uintptr_t*)(pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset);
#ifdef TRACE_STEPS
            TraceString("Write2 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                        (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                        pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
            tU32* pu32Tmp = (tU32*)pTmp;
            *pu32Tmp = Size;
            pu32Tmp++;
            u32ReleasedMem += sizeof(tU32);
            pRngBufDat->u32WriteOffset += sizeof(tU32);
            if((pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset + Size) <= pRngBufHandle->MemEnd)
            {
               memcpy((char*)(pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset),ps8Buffer,Size);
               pRngBufDat->u32WriteOffset += Size;
               if((pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset) == pRngBufHandle->MemEnd)
               {
                  pRngBufDat->u32WriteOffset = 0;
               }
            } 
            else
            {
               Size1 = pRngBufHandle->MemEnd - (pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset);
               memcpy((char*)(pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset),ps8Buffer,Size1);
               pRngBufDat->u32WriteOffset = 0;
               Size2 = Size -Size1;
               memcpy((char*)(pRngBufHandle->MemStart),(char*)(ps8Buffer+Size1),Size2);
               pRngBufDat->u32WriteOffset += Size2;
            }
            u32ReleasedMem +=Size;
         
            /* calculate 4 Byte alignment */       
            Size1 = Size % sizeof(tU32);
            if(Size1)
            {
                Size2 = sizeof(tU32) - Size1;
                u32ReleasedMem += Size2;
                if((pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset + Size2) <= pRngBufHandle->MemEnd)
                {
                   pRngBufDat->u32WriteOffset += Size2;
                   if((pRngBufHandle->MemStart + pRngBufDat->u32WriteOffset) == pRngBufHandle->MemEnd)
                   {
                      pRngBufDat->u32WriteOffset = 0;
                   }
                }
                else
                {
				   TraceString("Write Alignment not expected");
                   pRngBufDat->u32WriteOffset = 0;
                }
            }
            pRngBufDat->u32MemFree -= u32ReleasedMem;
            pRngBufDat->u32ActualMessage++;
            if(pRngBufDat->u32ActualMessage == 1)
            {
               bTrigger = TRUE;
            }
#ifdef TRACE_STEPS
            TraceString("Write3 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                        (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                        pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
            if(pRngBufDat->u32RBlock > 0)
            { 
               s32ReturnValue = OSAL_s32EventPost(pRngBufHandle->hEvent,WAIT_FOR_READ,OSAL_EN_EVENTMASK_OR);
               pRngBufDat->u32RBlockPost++;
            }
            (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
        }
         else
         {
            u32ErrorCode = OSAL_E_NOSPACE;
            s32ReturnValue = OSAL_ERROR;
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      s32ReturnValue = OSAL_ERROR;
   }
   

   if((s32ReturnValue == OSAL_OK)&&(bTrigger == TRUE)&&(pRngBufDat)&&(pRngBufDat->bNotify == TRUE)
    &&(pRngBufDat->callbackpididx < (tS32)pOsalData->u32MaxNrProcElements)&&(pRngBufDat->pcallback )&&(pRngBufDat->callbackpididx >= 0))
   {
        if(pRngBufDat->callbackpid == OSAL_ProcessWhoAmI()) // check if process is the same
        {
             pRngBufDat->pcallback(pRngBufDat->pcallbackarg);
        }
        else
        {
           tOsalMbxMsg Msg;
           Msg.rOsalMsg.Cmd    = MBX_RB_NOTIFY;
           Msg.rOsalMsg.ID     = pRngBufDat->callbackpid;
           Msg.rOsalMsg.pFunc  = pRngBufDat->pcallback;
           Msg.rOsalMsg.pArg   = pRngBufDat->pcallbackarg;

           if(hMQCbPrc[pRngBufDat->callbackpididx] == 0)
           {
              hMQCbPrc[pRngBufDat->callbackpididx] = GetPrcLocalMsgQHandle(pRngBufDat->callbackpid);
           }
           if(OSAL_s32MessageQueuePost(hMQCbPrc[pRngBufDat->callbackpididx],(tPCU8)&Msg,sizeof(tOsalMbxMsg),0) == OSAL_ERROR)
           {
             NORMAL_M_ASSERT_ALWAYS();
           }
        }
   }

   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tString coszName = NULL;
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      tU32 u32Val1 = 0;
      tU32 u32Val2 = 0;
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   
      if(pRngBufDat)
      {
         coszName = pRngBufDat->szName;
         u32Val1 = pRngBufDat->u32MemFree;
         u32Val2 = pRngBufDat->u32ActualMessage;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Write To RingBuffer Handle 0x%x Bytes:%d Error:0x%x Name:%s Mem:%d Msg:%d",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)Size,u32ErrorCode,coszName,u32Val1,u32Val2);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Write To RingBuffer Handle 0x%" PRIxPTR " Bytes:%d Error:0x%x Name:%s Mem:%d Msg:%d",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)Size,u32ErrorCode,coszName,u32Val1,u32Val2);
#endif
  }

  return Size;   
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_u32ReadFromRingBuffer
 *
 * DESCRIPTION: this function reads from an existing ring buffer element
 *
 * PARAMETER:  OSAL_tMQueueHandle  : ring buffer handle
 *             char*               : buffer to copy to ringbuffer    
 *             tU32                : size of the buffer
 *             tU32                : timeout value
 *
 * RETURNVALUE: number of read bytes.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tU32 OSAL_u32ReadFromRingBuffer(OSAL_tMQueueHandle Handle,char* ps8Buffer, tU32 BufSize,tU32 u32TimeOut)
{
   tS32 s32ReturnValue = OSAL_OK;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tU32 Size = 0;
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)Handle;
   tOsalRgBufDat* pRngBufDat = NULL;
   tU32 u32Mask = 0;

   if(pRngBufHandle && (Handle != (tU32)OSAL_ERROR) && (pRngBufHandle->u32Magic == LINUX_C_U32_RING_BUFFER_ID))
   {
      if(pRngBufHandle->enAccess == OSAL_EN_WRITEONLY)
      {
         Size = 0;
         u32ErrorCode = OSAL_E_NOPERMISSION;
         s32ReturnValue = OSAL_ERROR;
      }
      else
      {
         pRngBufDat = (tOsalRgBufDat*)pRngBufHandle->StartAdress;
         /* check for available data*/
         while(1)
         {
            if(pRngBufDat->u32ActualMessage == 0) 
            {
               if(u32TimeOut == OSAL_C_TIMEOUT_NOBLOCKING)
               {
                  s32ReturnValue = OSAL_ERROR;
                  u32ErrorCode = OSAL_E_TIMEOUT;
                  Size = 0;
                  break;
               }
               else
               {
                  (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                  pRngBufDat->u32RBlock++;
                  (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                  if((pRngBufDat->u32RBlockPost)&&(pRngBufDat->u32ActualMessage == 0))
                  {
                      s32ReturnValue = OSAL_s32EventWait(pRngBufHandle->hEvent,WAIT_FOR_READ,OSAL_EN_EVENTMASK_OR,u32TimeOut,&u32Mask);
                      pRngBufDat->u32RBlockPost = 0;
                      (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                      pRngBufDat->u32RBlock--;
                      (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                      if(s32ReturnValue == OSAL_ERROR)
                      {
                         s32ReturnValue = OSAL_ERROR;
                         u32ErrorCode = OSAL_E_TIMEOUT;
                         Size = 0;
                         TraceString("Read RngBuf:%s timeout %d -> %d messages in buffer Memfree:%d",pRngBufDat->szName,u32TimeOut,pRngBufDat->u32ActualMessage, pRngBufDat->u32MemFree);
                         break;
                      }
                      else
                      {
                         u32ErrorCode = OSAL_E_NOERROR;
                      }
                   }
                   else
                   {
                      (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                      pRngBufDat->u32RBlock--;
                      (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
                       //repeat
                   }
               }
            }
            else
            {
                break;
            }
         }
         if(s32ReturnValue == OSAL_OK)
         {
            tU32 u32ReleasedMem = 0;
            (void)OSAL_s32SemaphoreWait(pRngBufHandle->hLockSem,OSAL_C_TIMEOUT_FOREVER);
            tU32 Size1,Size2;
            uintptr_t* pTmp;
            Size1 = pRngBufHandle->MemEnd - (pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset);
            /* check for gap at end of memory */
            if(Size1 < sizeof(tU32))
            {
               u32ReleasedMem += Size1;
               pRngBufDat->u32ReadOffset = 0;
            }
            pTmp = (uintptr_t*)(pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset);

#ifdef TRACE_STEPS
            TraceString("Read1 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                        (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                        pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
            /* Get size info , go to data and upddate management data */
            tU32* pu32Tmp = (tU32*)pTmp;
            Size = *pu32Tmp;
            pu32Tmp++;
            u32ReleasedMem += sizeof(tU32);
            pRngBufDat->u32ReadOffset += sizeof(tU32);
   
            if(BufSize < Size)
            {
               TraceString("Read Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                           (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                            pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
               TraceString("Incomplete msg from ringbuffer due Buffer Size < Size",BufSize,Size);  
               OSAL_vProcessExit();
            }
#ifdef TRACE_STEPS
            TraceString("Read2 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                        (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                        pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
            if((pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset + Size) <= pRngBufHandle->MemEnd)
            {
               memcpy(ps8Buffer,(char*)(pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset),Size);
               pRngBufDat->u32ReadOffset += Size;
               u32ReleasedMem            += Size;
               if((pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset) == pRngBufHandle->MemEnd)
               {
                  pRngBufDat->u32ReadOffset = 0;
               }
            } 
            else
            {
               Size1 = pRngBufHandle->MemEnd - (pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset);
               memcpy(ps8Buffer,(char*)(pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset),Size1);
               pRngBufDat->u32ReadOffset = 0;
               u32ReleasedMem += Size1;
               Size2 = Size -Size1;
               memcpy((char*)(ps8Buffer+Size1),(char*)(pRngBufHandle->MemStart),Size2);
               u32ReleasedMem            += Size2;
               pRngBufDat->u32ReadOffset += Size2;
            }
            /* calculate 4 Byte alignment */
            Size1 = Size % sizeof(tU32);
            if(Size1)
            {
               Size2 = sizeof(tU32) - Size1;
               u32ReleasedMem += Size2;
               if((pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset + Size2) <= pRngBufHandle->MemEnd)
               {
                  pRngBufDat->u32ReadOffset += Size2;
                  if((pRngBufHandle->MemStart + pRngBufDat->u32ReadOffset) == pRngBufHandle->MemEnd)
                  {
                     pRngBufDat->u32ReadOffset = 0;
                  }
               }
               else
               {
                   TraceString("Read Alignment not expected");
                  pRngBufDat->u32ReadOffset = 0;
               }
            }
            pRngBufDat->u32ActualMessage--;
			pRngBufDat->u32MemFree += u32ReleasedMem;
#ifdef TRACE_STEPS
            TraceString("Read3 Start:0x%x WriteOffset:%d ReadOffset:%d Free:%d Msg:%d Open:%d ",
                        (tU32)pRngBufHandle->MemStart,pRngBufDat->u32WriteOffset,pRngBufDat->u32ReadOffset,
                         pRngBufDat->u32MemFree,pRngBufDat->u32ActualMessage,pRngBufDat->u32OpnCnt);
#endif
            if(pRngBufDat->u32WBlock > 0)
            { 
               s32ReturnValue = OSAL_s32EventPost(pRngBufHandle->hEvent,WAIT_FOR_WRITE,OSAL_EN_EVENTMASK_OR);
               pRngBufDat->u32WBlockPost++;
            }
            (void)OSAL_s32SemaphorePost(pRngBufHandle->hLockSem);
         }
         else
         {
            u32ErrorCode = OSAL_E_TIMEOUT;
            s32ReturnValue = OSAL_ERROR;
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      s32ReturnValue = OSAL_ERROR;
   }

   
   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tString coszName = NULL;
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      tU32 u32Val1 = 0;
	  tU32 u32Val2 = 0;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      
      if(pRngBufDat)
      {
         coszName = pRngBufDat->szName;
         u32Val1 = pRngBufDat->u32MemFree;
         u32Val2 = pRngBufDat->u32ActualMessage;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Read from RingBuffer Handle 0x%x Bytes:%d Error:0x%x Name:%s Mem:%d Msg:%d",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)Size,u32ErrorCode,coszName,u32Val1,u32Val2);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) Read from RingBuffer Handle 0x%" PRIxPTR " Bytes:%d Error:0x%x Name:%s Mem:%d Msg:%d",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)Size,u32ErrorCode,coszName,u32Val1,u32Val2);
#endif
   }
   return Size;   
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_u32RingBufferNotify
 *
 * DESCRIPTION: this function enter a callback function an existing ring buffer 
 *              element for notify for available message
 *
 * PARAMETER:  OSAL_tMQueueHandle  : ring buffer handle
 *             OSAL_tpfCallback    : callback function for the ringbuffer    
 *             tPVoid              : argument for callback function
 *
  * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
*
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_u32RingBufferNotify(OSAL_tMQueueHandle Handle,OSAL_tpfCallback pCallback,tPVoid pvArg)
{
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)Handle;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tOsalRgBufDat* pRngBufDat = NULL;
   
   /* check if callback handler task for this process already exists */  
   tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
   if(s32PidEntry == OSAL_ERROR)
   {
       NORMAL_M_ASSERT_ALWAYS();
   }
   else
   {
      /* check if callback handler task for this process already exists */  
      s32StartCbHdrTask(s32PidEntry);
   }
   if(pRngBufHandle && (Handle != (tU32)OSAL_ERROR)  && (pRngBufHandle->u32Magic == LINUX_C_U32_RING_BUFFER_ID))
   {
      pRngBufDat = (tOsalRgBufDat*)pRngBufHandle->StartAdress;
      if(pRngBufDat)
      {
         pRngBufDat->callbackpid    = OSAL_ProcessWhoAmI();
         pRngBufDat->callbackpididx = s32GetPrcIdx(pRngBufDat->callbackpid);
         if(pRngBufDat->callbackpididx == -1)
         {
           FATAL_M_ASSERT_ALWAYS();
         }
         pRngBufDat->pcallback      = pCallback;
         pRngBufDat->pcallbackarg   = pvArg;
         pRngBufDat->bNotify        = TRUE;
         s32ReturnValue             = OSAL_OK;
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_BADFILEDESCRIPTOR;
   }
   
   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tString coszName = NULL;
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if((s32ReturnValue != OSAL_OK))vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      
      if(pRngBufDat)
      {
         coszName = pRngBufDat->szName;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) RingBuffer Notify Handle 0x%x Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,coszName);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) RingBuffer Notify Handle 0x%" PRIxPTR " Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,coszName);
#endif
   }
   return s32ReturnValue;   
   
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32RingBufferStatus
 *
 * DESCRIPTION: this function enter a callback function an existing ring buffer 
 *              element for notify for available message
 *
 * PARAMETER:  OSAL_tMQueueHandle  : ring buffer handle
 *             tPU32               : Adress to store number of elements in the ringbuffer   
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.04.16  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32RingBufferStatus( OSAL_tMQueueHandle Handle,tPU32 pu32Message)
{
   tOsalRgBufHandle* pRngBufHandle = (tOsalRgBufHandle*)Handle;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tOsalRgBufDat* pRngBufDat = NULL;

   if(pRngBufHandle && (Handle != (tU32)OSAL_ERROR)  && (pRngBufHandle->u32Magic == LINUX_C_U32_RING_BUFFER_ID))
   {
      pRngBufDat = (tOsalRgBufDat*)pRngBufHandle->StartAdress;
      if(pRngBufDat)
      {
         if( pu32Message    != OSAL_NULL )
            *pu32Message     = pRngBufDat->u32ActualMessage;
         s32ReturnValue = OSAL_OK;
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      u32ErrorCode=OSAL_E_BADFILEDESCRIPTOR;
   } 

   if((u32OsalSTrace & 0x00000200)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tString coszName = NULL;
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      tU32 u32ActualMessage = 0xffffffff;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pRngBufDat)
      {
         coszName = pRngBufDat->szName;
         u32ActualMessage = pRngBufDat->u32ActualMessage;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
      if((s32ReturnValue != OSAL_OK))vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
#ifdef NO_PRIxPTR
      TraceString("OSALRNG_BUF Task:%s(ID:%d) RingBuffer Status Handle 0x%x Error:0x%x Msg:%d Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,u32ActualMessage,coszName);
#else
      TraceString("OSALRNG_BUF Task:%s(ID:%d) RingBuffer Status Handle 0x%" PRIxPTR " Error:0x%x Msg:%d Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,u32ActualMessage,coszName);
#endif
   }
   return( s32ReturnValue );
}


#ifdef OSAL_SHM_MQ

static tS32 s32StoreToMsgBox( trMqueueElement *pMq,
                              tMQUserHandle*  pHandle,
                              tU32            MsgPriority,
                              tU16            MsgSize,
                              tPCVoid         pMsgBuffer )
{
   MsgHdr *pStartHdr, *pEmptyHdr, *pPriorityHdr, *prTemp = NULL;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 MsgLoop, MsgOffset;
   tBool bEmptyFound = FALSE;
   trStdMsgQueue *pMqueue = &pStdMQEl[pMq->u32EntryIdx];

   /*********************************************/
   /* Search an Emty Box in the MsgBox */
   /************************************ ********/
   MsgLoop = pMqueue->MaxMessages;
   /* Bytes Offset between two Messages */
   MsgOffset = sizeof(struct MsgHdr);
   MsgOffset += (pMqueue->MaxLength );
   /* from the base of MsgBox untill an empty item is found */
   pStartHdr = (MsgHdr*)pHandle->pAdress;
   if (!pStartHdr) 
   {
       NORMAL_M_ASSERT_ALWAYS();
       return (OSAL_ERROR);
   }
   pEmptyHdr = pStartHdr;
   while( MsgLoop-- )
   {
      /* if the item is empty then break */
      if( pEmptyHdr->msgsize == 0 )
      {
         bEmptyFound = TRUE;
         break;
      }
      /* else go to the next item */
      pEmptyHdr = (MsgHdr *)((tPU8)pEmptyHdr + MsgOffset); /*lint !e826 */
   }

   /****************************************************************/
   /* if the item is really empty --> store the incoming message */
   /****************************************************************/
   if( bEmptyFound )
   {
      tPCU8 pRet;
      /* Fill the MsgHdr and copy from the incoming buffer the message */
      pEmptyHdr->msgsize   = MsgSize;
      pEmptyHdr->NextIndex = (uintptr_t)-1;
      pRet = (tPCU8)OSAL_pvMemoryCopy (&pEmptyHdr->Message[0], pMsgBuffer, MsgSize);
      if( pRet )
      {
         /* update the header list on priority base */
         pPriorityHdr = (MsgHdr *)pMqueue->PrioMsgListOffset[MsgPriority];
         if(pPriorityHdr == (MsgHdr*)-1)  // emty?
         {  
            pMqueue->PrioMsgListOffset[MsgPriority] = (uintptr_t)pEmptyHdr - (uintptr_t)pStartHdr;
         }
         else
         {
            // calculate the real memory offset, now
            pPriorityHdr = (MsgHdr *)((uintptr_t)pStartHdr + (uintptr_t)pPriorityHdr);
            /* look for last element in priority list */
            while( pPriorityHdr->NextIndex != (uintptr_t)-1 )
            {
               prTemp = (MsgHdr *)((uintptr_t)pStartHdr + (uintptr_t)(pPriorityHdr->NextIndex));
               pPriorityHdr = prTemp;
            }
            pPriorityHdr->NextIndex = (uintptr_t)pEmptyHdr - (uintptr_t)pStartHdr;
         }
         s32ReturnValue = OSAL_OK;
      }
   }
   return( s32ReturnValue );
}

/*****************************************************************************
*
* FUNCTION:    GetMsgFromMsgBox
*
* DESCRIPTION: This function get a message contentof an OSAL message queue based on IOSC
*
* PARAMETER:     trStdMsgQueue*  pointer to MQ management structure
*                         tU32  search priotrity for message
*                         tU32* pointer to store message size info
*
* RETURNVALUE: void*  pointer to message
* HISTORY:
* Date      |   Modification                         | Authors
* 03.06.10  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static void* GetMsgFromMsgBox ( trMqueueElement *pMq,
                                tMQUserHandle*  pHandle,
                                tU32 SearchedPrio,
                                tPU32 pActualSize )
{
   MsgHdr *pPriorityHdr;
   tPVoid pMessage = NULL;
   MsgHdr* prTemp;
   *pActualSize = 0;
   /* go in the Priority organised Header List to the searched priority */
   pPriorityHdr = (MsgHdr *)(pStdMQEl[pMq->u32EntryIdx].PrioMsgListOffset[SearchedPrio]);
   /* if the list is not empty copy ActualSize and address of the message */
   if(pPriorityHdr != (MsgHdr*)-1 )
   {
      /* calculate the real memory pointer, now */
      prTemp = (MsgHdr*)pHandle->pAdress; // get start adr of whole msg mem      
      if (!prTemp) 
      {
          NORMAL_M_ASSERT_ALWAYS();
          return (NULL);
      }
      prTemp = (MsgHdr*)((uintptr_t)prTemp + (uintptr_t)pPriorityHdr);
      *pActualSize    = prTemp->msgsize;
      pMessage        = prTemp->Message;
      prTemp->msgsize = 0;
      /* update priority list */
      pStdMQEl[pMq->u32EntryIdx].PrioMsgListOffset[SearchedPrio] = prTemp->NextIndex;

   }
   return( pMessage );
}

tS32 OSAL_s32ShMemMessageQueueCreate(tCString coszName, tU32 u32MaxMessages, tU32 u32MaxLength,OSAL_tenAccess enAccess, OSAL_tMQueueHandle* pHandle)
{
   OSAL_tSemHandle hSem = (OSAL_tSemHandle)OSAL_ERROR;
   OSAL_tEventHandle hEvent = (OSAL_tEventHandle)OSAL_ERROR;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tU32 u32Loop;
   tU32 u32Lenght = u32MaxMessages *(u32MaxLength + sizeof(MsgHdr));
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
   trMqueueElement* pCurrentEntry;
   OSAL_tShMemHandle hShMem= (OSAL_tShMemHandle)OSAL_ERROR;
 
   if((pHandle)&&(u32MaxMessages)&&(u32MaxLength)&&(coszName)&&(enAccess <= OSAL_EN_READWRITE))
   {
      /* The Name is not too long */
      if( OSAL_u32StringLength(coszName) < (LINUX_C_MQ_MAX_NAMELENGHT))
      {
         if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
         {
            snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
            pCurrentEntry = tMqueueTableSearchEntryByName(Name);
            if( pCurrentEntry == OSAL_NULL )
            {
               if((pCurrentEntry = tMqueueTableGetFreeEntry())!= OSAL_NULL )
               {
                  hShMem = OSAL_SharedMemoryCreate(Name,enAccess,u32Lenght);
                  if((s32ReturnValue = OSAL_s32EventCreateOpt(Name, &hEvent, CONSUME_EVENT|WAIT_MULTIPLE_TSK)) == OSAL_OK)
                  {
                     s32ReturnValue = OSAL_s32SemaphoreCreate(Name,&hSem,1);
                  }
                  if((hShMem != (OSAL_tShMemHandle)OSAL_ERROR)&&(s32ReturnValue != OSAL_ERROR))
                  {
                     if((u32ErrorCode = u32GenerateHandle(enAccess, pCurrentEntry, pHandle)) != OSAL_E_NOERROR)
                     {
                        TraceString("MQ(%s): u32GenerateHandle Error:%d", coszName, u32ErrorCode);
                        FATAL_M_ASSERT_ALWAYS();
                     }
                     else
                     {
                        ((tMQUserHandle*)*pHandle)->pAdress = OSAL_pvSharedMemoryMap(hShMem,enAccess,u32Lenght,0);
                        ((tMQUserHandle*)*pHandle)->hShMem  = hShMem;
                        ((tMQUserHandle*)*pHandle)->hLockSem = hSem;
                        ((tMQUserHandle*)*pHandle)->hEvent  = hEvent;
                            
                        pCurrentEntry->pcallback = NULL;
                        pCurrentEntry->pcallbackarg = NULL;
                        pCurrentEntry->callbacktid = NOTATHREAD;
                        pCurrentEntry->callbackpid = NOTATHREAD;
                        pCurrentEntry->callbackpididx = NOTATHREAD;
                        pCurrentEntry->bNotify = FALSE;
                        pCurrentEntry->bIsUsed = TRUE;
#ifdef USE_EVENT_AS_CB
                        pCurrentEntry->hEvent = (OSAL_tEventHandle) 0;
                        pCurrentEntry->mask = (OSAL_tEventMask) 0;
                        pCurrentEntry->enFlags = (OSAL_tenEventMaskFlag) 0;
#endif
                        pCurrentEntry->u16Type = MQ_STD;
                        pCurrentEntry->bOverflow = FALSE;
                        pCurrentEntry->s32PID = s32Pid;
                        (void)OSAL_szStringNCopy((tString)pCurrentEntry->szName,
                                                Name,
                                                LINUX_C_MQ_MAX_NAMELENGHT);
                        pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages   = u32MaxMessages;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].MaxLength     = u32MaxLength;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage = 0;
                        /* init the other MQ fields */
                        pStdMQEl[pCurrentEntry->u32EntryIdx].bToDelete = FALSE;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u16OpenCounter = 1;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecTsk = 0;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecPrc = 0;
                        for( u32Loop=0; u32Loop < OSAL_C_U32_MQUEUE_PRIORITY_LOWEST+1; u32Loop++ )
                        {
                           pStdMQEl[pCurrentEntry->u32EntryIdx].PrioMsgListOffset[u32Loop] = -1;
                        }
                     //   pStdMQEl[pCurrentEntry->u32EntryIdx].PayLoad = ( (u32MaxLength-1)>>2 )<<2;;
                        if(!strcmp("CSMALU,_MQ",coszName))
                        {
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing = MEASUREMENT_SWITCHOFF;
                        }
                        else
                        {
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32MessagePrcessing = MSG_PROCESSING_INACTIVE;
                        }
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32IdxMPT    = 0;
                        for( u32Loop=0; u32Loop < OSAL_C_MSG_TIME_ARR_SIZE; u32Loop++ )
                        {
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32MsgPrcTim[u32Loop]   = 0;
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32MsgTimStamp[u32Loop] = 0;
                        }
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32IdxLMPT    = 0;
                        for( u32Loop=0; u32Loop < OSAL_C_LONG_MSG_TIME_ARR_SIZE; u32Loop++ )
                        {
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32LongMsgPrcTim[u32Loop]   = 0;
                           pStdMQEl[pCurrentEntry->u32EntryIdx].u32LongMsgTimStamp[u32Loop] = 0;
                        }
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u16WaitCounter = 0;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].bBlocked = FALSE;
                        pStdMQEl[pCurrentEntry->u32EntryIdx].u32Marker = 0;
                        s32ReturnValue = OSAL_OK;
                     }
                  }
                  else                
                  {
                      u32ErrorCode = OSAL_E_UNKNOWN;
                  }
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }
            /* UnLock the table */
            UnLockOsal(&pOsalData->MqueueTable.rLock);
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   
   if(s32ReturnValue == OSAL_ERROR)
   {
      if(hShMem != (OSAL_tShMemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SharedMemoryClose(hShMem);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32SharedMemoryDelete(Name);
      }
      if(hSem != (OSAL_tSemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SemaphoreClose(hSem);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32SemaphoreDelete(coszName);
      }
      if(hEvent != (OSAL_tEventHandle)OSAL_ERROR)
      {
         (void)OSAL_s32EventClose(hEvent);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32EventDelete(coszName);
      }
   }
   if((u32OsalSTrace & 0x00000020)||(s32ReturnValue != OSAL_OK))
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
	  
      if(s32ReturnValue != OSAL_OK)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );

      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);  
      if(coszName == NULL)coszName ="Unknown";
#ifdef NO_PRIxPTR
      TraceString("OSALMQ Task:%s(ID:%d) Create MQ Handle 0x%x Access:%d Error:0x%x Name:%s",
                   name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#else
      TraceString("OSALMQ Task:%s(ID:%d) Create MQ Handle 0x%" PRIxPTR " Access:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#endif
   }
   return s32ReturnValue;
}

tS32 OSAL_s32ShMemMessageQueueOpen(tCString coszName,OSAL_tenAccess enAccess,OSAL_tMQueueHandle* pHandle)
{
   OSAL_tShMemHandle hShMem = (OSAL_tShMemHandle)OSAL_ERROR;
   OSAL_tSemHandle hSem     = (OSAL_tSemHandle)OSAL_ERROR;
   OSAL_tEventHandle hEvent = (OSAL_tEventHandle)OSAL_ERROR;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
   trMqueueElement* pCurrentEntry = NULL;
   
   if((pHandle)&&(coszName)&&(enAccess <= OSAL_EN_READWRITE))
   {
      if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
      {
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         pCurrentEntry = tMqueueTableSearchEntryByName(Name);
         if(pCurrentEntry != NULL)
         {
            hShMem = OSAL_SharedMemoryOpen(Name,enAccess);
            if((s32ReturnValue = OSAL_s32EventOpen(Name, &hEvent)) == OSAL_OK)
            {
               s32ReturnValue = OSAL_s32SemaphoreOpen(Name,&hSem);
            }
            if((hShMem != (OSAL_tShMemHandle)OSAL_ERROR)&&(s32ReturnValue != OSAL_ERROR))
            {
               pStdMQEl[pCurrentEntry->u32EntryIdx].u16OpenCounter++;
            }
            else
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            if((u32ErrorCode = u32GenerateHandle(enAccess,pCurrentEntry,pHandle)) == OSAL_E_NOERROR)
            {
               ((tMQUserHandle*)*pHandle)->pAdress = OSAL_pvSharedMemoryMap(hShMem,enAccess,pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages * pStdMQEl[pCurrentEntry->u32EntryIdx].MaxLength,0);
               ((tMQUserHandle*)*pHandle)->hShMem  = hShMem;
               ((tMQUserHandle*)*pHandle)->hEvent  = hEvent;
               ((tMQUserHandle*)*pHandle)->hLockSem= hSem;
               s32ReturnValue = OSAL_OK;
            } 
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
            vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
         }//if( pCurrentEntry!= OSAL_NULL )
         UnLockOsal(&pOsalData->MqueueTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   
   if(s32ReturnValue == OSAL_ERROR)
   {
      if(hShMem != (OSAL_tShMemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SharedMemoryClose(hShMem);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32SharedMemoryDelete(Name);
      }
      if(hSem != (OSAL_tSemHandle)OSAL_ERROR)
      {
         (void)OSAL_s32SemaphoreClose(hSem);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32SemaphoreDelete(coszName);
      }
      if(hEvent != (OSAL_tEventHandle)OSAL_ERROR)
      {
         (void)OSAL_s32EventClose(hEvent);
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         (void)OSAL_s32EventDelete(coszName);
      }
   }
   
   if((u32OsalSTrace & 0x00000020)||(s32ReturnValue != OSAL_OK))
   {
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALMQ Task:%s(ID:%d) Open MQ Handle 0x%x Access:%d Error:0x%x Name:%s",
                  Name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#else
      TraceString("OSALMQ Task:%s(ID:%d) Open MQ Handle 0x%" PRIxPTR " Access:%d Error:0x%x Name:%s",
                  Name,(unsigned int)u32Temp,(uintptr_t)pHandle,enAccess,u32ErrorCode,coszName);
#endif
   }
   return s32ReturnValue;
}

tS32 OSAL_s32ShMemMessageQueueDelete(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
   trMqueueElement* pCurrentEntry = NULL;

   if(coszName)
   {
      if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
      { 
         snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
         pCurrentEntry = tMqueueTableSearchEntryByName(Name);
         if(pCurrentEntry != NULL)
         {
            pStdMQEl[pCurrentEntry->u32EntryIdx].bToDelete = TRUE;
            snprintf(Name,LINUX_C_MQ_MAX_NAMELENGHT-1,"MQ%s",coszName);
            s32ReturnValue = OSAL_s32SharedMemoryDelete(Name);
            s32ReturnValue = OSAL_s32SemaphoreDelete(Name);
            s32ReturnValue = OSAL_s32EventDelete(Name);
         
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].u16OpenCounter == 0)
            {
               pCurrentEntry->bIsUsed = FALSE;
            }
            else
            {
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->MqueueTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32OsalSTrace & 0x00000020)||(s32ReturnValue != OSAL_OK))
   {
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      if(s32ReturnValue != OSAL_OK)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
      TraceString("OSALMQ Task:%s(ID:%d) Delete MQ Error:%d Name:%s",
                  Name,(unsigned int)u32Temp,u32ErrorCode,coszName);
   }

   return s32ReturnValue;
}

tS32 OSAL_s32ShMemMessageQueueClose(OSAL_tMQueueHandle Handle)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = u32CheckMqHandle(Handle);
   char ObjName[LINUX_C_MQ_MAX_NAMELENGHT] = {0};
   trMqueueElement* pCurrentEntry;
   char Name[LINUX_C_MQ_MAX_NAMELENGHT];
   
  if(u32ErrorCode == OSAL_E_NOERROR)
  {
     if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
     {
        pCurrentEntry = ((tMQUserHandle*)Handle)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
        if( pCurrentEntry != OSAL_NULL )
        {
           trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
           if(pTemp->u16OpenCounter > 0 )
           {
               pTemp->u16OpenCounter--;
           }
           ((tMQUserHandle*)Handle)->u32Magic = 0;/*lint !e613 *//* pointer already checked*/ 
           s32ReturnValue = OSAL_s32SharedMemoryClose(((tMQUserHandle*)Handle)->hShMem);
           s32ReturnValue = OSAL_s32EventClose(((tMQUserHandle*)Handle)->hEvent);
           s32ReturnValue = OSAL_s32SemaphoreClose(((tMQUserHandle*)Handle)->hLockSem);
           if((pTemp->u16OpenCounter == 0)&&(pTemp->bToDelete))
           {
             pCurrentEntry->bIsUsed = FALSE;
             s32ReturnValue = OSAL_s32SharedMemoryDelete(pCurrentEntry->szName);
             s32ReturnValue = OSAL_s32EventDelete(pCurrentEntry->szName);
             s32ReturnValue = OSAL_s32SemaphoreDelete(pCurrentEntry->szName);
           }
 
           if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&MqMemPoolHandle,(void*)Handle) == OSAL_ERROR)
           {    NORMAL_M_ASSERT_ALWAYS();  }
           s32ReturnValue = OSAL_OK;
        }
        else
        {
           u32ErrorCode = OSAL_E_INVALIDVALUE;
        }
        UnLockOsal(&pOsalData->MqueueTable.rLock);
     }
   }
    
   if((u32OsalSTrace & 0x00000020)||(s32ReturnValue != OSAL_OK))
   {
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(ObjName[0] == 0)strncpy(ObjName,"Unknown",strlen("Unknown"));
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
#ifdef NO_PRIxPTR
      TraceString("OSALMQ Task:%s(ID:%d) Close MQ Handle 0x%x Error:0x%x Name:%s",
                  Name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,Name);
#else
      TraceString("OSALMQ Task:%s(ID:%d) Close MQ Handle 0x%" PRIxPTR " Error:0x%x Name:%s",
                   Name,(unsigned int)u32Temp,(uintptr_t)Handle,u32ErrorCode,Name);
#endif
   }
   return s32ReturnValue;
}


tS32 OSAL_s32ShMemMessageQueuePost(OSAL_tMQueueHandle Handle,tPCU8 pcou8Msg, tU32 u32Length,tU32 u32Prio)
{
   tS32 s32ReturnValue = OSAL_OK;
   tU32 u32ErrorCode   = u32CheckMqHandle(Handle); /* step 1 user handle check */
   trMqueueElement *pCurrentEntry = NULL;
   tU32 u32Count = 0;
   char name[TRACE_NAME_SIZE];
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   tU32 u32Mask = 0;
  
   
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      pCurrentEntry = ((tMQUserHandle*)Handle)->pOrigin;
      if( u32Prio < (OSAL_C_U32_MQUEUE_PRIORITY_LOWEST + 1) )
      {  
        /* check if queue isn't deleted */
        if(u32Length > pStdMQEl[pCurrentEntry->u32EntryIdx].MaxLength)
        {
           u32ErrorCode = OSAL_E_MSGTOOLONG;
        }
        else if((pCurrentEntry->bIsUsed == FALSE))
        {
           u32ErrorCode = OSAL_E_DOESNOTEXIST;
        }
        /*else if(pStdMQEl[pCurrentEntry->u32EntryIdx].bToDelete == TRUE)
        {
           u32ErrorCode = OSAL_E_NOPERMISSION;
        }*/
	  }
	  else
	  {
		 u32ErrorCode = OSAL_E_INVALIDVALUE;
	  }
   }
   if((u32ErrorCode == OSAL_E_NOERROR)&&(pCurrentEntry)/*for stupid lint*/)
   {
      if(((tMQUserHandle*)Handle)->enAccess == OSAL_EN_READONLY)
      {
         u32ErrorCode = OSAL_E_NOPERMISSION;
         s32ReturnValue = OSAL_ERROR;
      }
      else
      {
         /* check if enough space available otherwise wait */
         while(1)
         {
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage == pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages)
            {
               (void)OSAL_s32SemaphoreWait(((tMQUserHandle*)Handle)->hLockSem,OSAL_C_TIMEOUT_FOREVER);
               pStdMQEl[pCurrentEntry->u32EntryIdx].u32WBlock++;
               (void)OSAL_s32SemaphorePost(((tMQUserHandle*)Handle)->hLockSem);
               if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage == pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages)
               {
                  s32ReturnValue = OSAL_s32EventWait(((tMQUserHandle*)Handle)->hEvent,WAIT_FOR_WRITE,OSAL_EN_EVENTMASK_OR,BLOCKING_TIMEOUT,&u32Mask);
                  if(s32ReturnValue == OSAL_ERROR)
                  {
                     u32ErrorCode = OSAL_E_QUEUEFULL;
                     s32ReturnValue = OSAL_ERROR;
                     TraceString("Post1 ShMem MQ:%s timeout:%d  ",pCurrentEntry->szName,OSAL_C_TIMEOUT_FOREVER);
                     break;
                  }
                  else
                  {
                     if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage < pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages)
                     {
                        break;
                     }
                     TraceString("Post2 Return+Retry ShMem MQ:%s mask:0x%x timeout %d -> %d messages in Queue",pCurrentEntry->szName,u32Mask,BLOCKING_TIMEOUT,pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage);
                  }
               }
               else
               {
                  if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage < pStdMQEl[pCurrentEntry->u32EntryIdx].MaxMessages)
                  {
                       break;
                  }
				  //wrong signal repeat
                  TraceString("Post4 Return+Retry ShMem MQ:%s mask:0x%x timeout %d -> %d messages in Queue",pCurrentEntry->szName,u32Mask,BLOCKING_TIMEOUT,pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage);
                  (void)OSAL_s32SemaphoreWait(((tMQUserHandle*)Handle)->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                  pStdMQEl[pCurrentEntry->u32EntryIdx].u32WBlock--;
                  (void)OSAL_s32SemaphorePost(((tMQUserHandle*)Handle)->hLockSem);
               }
            }
            else
            {
               break;
            }
         }
         /* check if memory is now ensured */
         if(s32ReturnValue == OSAL_OK)
         {
            (void)OSAL_s32SemaphoreWait(((tMQUserHandle*)Handle)->hLockSem,OSAL_C_TIMEOUT_FOREVER);
            pStdMQEl[pCurrentEntry->u32EntryIdx].u32WBlock--;
            if(s32StoreToMsgBox(pCurrentEntry,(tMQUserHandle*)Handle,u32Prio,u32Length,pcou8Msg) != OSAL_OK)
            {
               TraceString("OSALMQ s32StoreToMsgBox failed");
            }
            else
            {
               pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage++;
               if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage == 1)
               {
                    pCurrentEntry->bTriggerCb = TRUE;
               }
               u32Count = pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage;
               if(pStdMQEl[pCurrentEntry->u32EntryIdx].u32RBlock > 0)
               { 
                    s32ReturnValue = OSAL_s32EventPost(((tMQUserHandle*)Handle)->hEvent,WAIT_FOR_READ,OSAL_EN_EVENTMASK_OR);
               }
            }
            (void)OSAL_s32SemaphorePost(((tMQUserHandle*)Handle)->hLockSem);
         }
      }
   }
   else
   {
       s32ReturnValue = OSAL_ERROR;
   }
   

   if((u32ErrorCode == OSAL_E_NOERROR)&&(pCurrentEntry)&&(pCurrentEntry->bTriggerCb == TRUE)
    &&(pCurrentEntry->callbackpididx < (tS32)pOsalData->u32MaxNrProcElements)
    && (pCurrentEntry->pcallback )&&(pCurrentEntry->callbackpididx >= 0))
   {
        pCurrentEntry->bTriggerCb = FALSE;
        if( pCurrentEntry->callbackpid == OSAL_ProcessWhoAmI()) // check if process is the same
        {
           vExecuteCallback(pCurrentEntry);
        }
        else
        {
           vPostToCallbackQueue(pCurrentEntry);
        }
   }

    
   if((u32OsalSTrace & 0x00000020)||(s32ReturnValue != OSAL_OK))
   {
      tString coszName = NULL;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);

      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   
      if(pCurrentEntry)
      {
         coszName = pCurrentEntry->szName;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALMQ Task:%s(ID:%d) Write To MQ Handle 0x%x Bytes:%d Count:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)u32Length,u32Count,u32ErrorCode,coszName);
#else
      TraceString("OSALMQ Task:%s(ID:%d) Write To MQ Handle 0x%" PRIxPTR " Bytes:%d Count:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)u32Length,u32Count,u32ErrorCode,coszName);
#endif
  }

  return s32ReturnValue;   
}

tS32 OSAL_s32ShMemMessageQueueWait(OSAL_tMQueueHandle Handle,tPU8 pu8Buffer, tU32 u32Length, tPU32 pu32Prio,OSAL_tMSecond u32TimeOut)
{
   tU32 u32ErrorCode   = u32CheckMqHandle(Handle); /* step 1 user handle check */
   trMqueueElement *pCurrentEntry = NULL;
   tU32 u32CopiedBytes = 0;
   void* pMsg = NULL;
   tU32 u32ActualPrio = 0;            
   char name[TRACE_NAME_SIZE];
   tS32 s32ReturnValue = OSAL_OK;
   tU32 u32Mask = 0;
   ((void)u32Length);
//   tU32 Start,End;
   
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      if(((tMQUserHandle*)Handle)->enAccess == OSAL_EN_WRITEONLY)
      {
         u32ErrorCode = OSAL_E_NOPERMISSION;
      }
      else
      {
         pCurrentEntry = ((tMQUserHandle*)Handle)->pOrigin;
         /* check for available data*/
         while(1)
         {
            u32ErrorCode = OSAL_E_NOERROR;
            /* check for available data*/
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage == 0) 
            {
               if(u32TimeOut == OSAL_C_TIMEOUT_NOBLOCKING)
               {
                  s32ReturnValue = OSAL_ERROR;
                  u32ErrorCode = OSAL_E_TIMEOUT;
                  break;
               }
               else
               {
                  /* wait for post trigger or timeout */
                  (void)OSAL_s32SemaphoreWait(((tMQUserHandle*)Handle)->hLockSem,OSAL_C_TIMEOUT_FOREVER);
                  pStdMQEl[pCurrentEntry->u32EntryIdx].u32RBlock++;
                  (void)OSAL_s32SemaphorePost(((tMQUserHandle*)Handle)->hLockSem);
                  if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage == 0) 
                  {
                     u32CopiedBytes++;
   //                  Start = OSAL_ClockGetElapsedTime();
                     s32ReturnValue = OSAL_s32EventWait(((tMQUserHandle*)Handle)->hEvent,WAIT_FOR_READ,OSAL_EN_EVENTMASK_OR,u32TimeOut,&u32Mask);
     //                End = OSAL_ClockGetElapsedTime();
                     if((s32ReturnValue == OSAL_ERROR)/*&&((End - Start) < u32TimeOut)*/)
                     {
                        s32ReturnValue = OSAL_ERROR;
                        u32ErrorCode = OSAL_E_TIMEOUT;
                        TraceString("Wait1 ShMem MQ:%s timeout %d -> %d messages in Queue",pCurrentEntry->szName,u32TimeOut,pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage);
                        break;
                     }
                     else
                     {
                        if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage > 0)
                        {
                           break;
                        }
      /*            if((End - Start)<u32TimeOut)
                        {
                           TraceString("Wait2 Return+Retry:%d ShMem MQ:%s mask:0x%x timeout %d time:%d -> %d messages in Queue",
                                        u32CopiedBytes,pCurrentEntry->szName,u32Mask,u32TimeOut,End - Start,pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage);
                        }*/
                        // wrong signal repeat						
                     }
                  }
                  else
                  {
                      break;
                  }
               }
            }
            else
            {
                break;
            }
         }
         u32CopiedBytes = 0;
         if(u32ErrorCode == OSAL_E_NOERROR)
         {
            (void)OSAL_s32SemaphoreWait(((tMQUserHandle*)Handle)->hLockSem,OSAL_C_TIMEOUT_FOREVER);
            pStdMQEl[pCurrentEntry->u32EntryIdx].u32RBlock = 0;
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecTsk == 0)
            {
               pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecTsk = OSAL_ThreadWhoAmI();
                pStdMQEl[pCurrentEntry->u32EntryIdx].u32RecPrc = OSAL_ProcessWhoAmI();
            }
            /* Stop message processing time measurement */
            vStopMeasureProcessingTime(&pStdMQEl[pCurrentEntry->u32EntryIdx],pCurrentEntry);
            
            u32ErrorCode = OSAL_E_INPROGRESS;
            for( u32ActualPrio = 0; u32ActualPrio < (OSAL_C_U32_MQUEUE_PRIORITY_LOWEST + 1); u32ActualPrio ++ )
            {
                pMsg = GetMsgFromMsgBox (pCurrentEntry,((tMQUserHandle*)Handle),u32ActualPrio,&u32CopiedBytes);
                if(pMsg)
                {
                   memcpy(pu8Buffer,(char*)pMsg,u32CopiedBytes);
                   pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage--;
                   u32ActualPrio =  pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage;
                   if (pu32Prio != OSAL_NULL)
                   {
                      *pu32Prio = u32ActualPrio;
                   }
                   u32ErrorCode = OSAL_E_NOERROR;
                   break;
                }
            }
            
            /* Start measurement of message processing times */
            vStartMeasureProcessingTime(&pStdMQEl[pCurrentEntry->u32EntryIdx]);
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].u32WBlock > 0)
            { 
               s32ReturnValue = OSAL_s32EventPost(((tMQUserHandle*)Handle)->hEvent,WAIT_FOR_WRITE,OSAL_EN_EVENTMASK_OR);
            }
            if(pStdMQEl[pCurrentEntry->u32EntryIdx].ActualMessage > 0)
            {
               s32ReturnValue = OSAL_s32EventPost(((tMQUserHandle*)Handle)->hEvent,WAIT_FOR_READ,OSAL_EN_EVENTMASK_OR);
            }
			
            (void)OSAL_s32SemaphorePost(((tMQUserHandle*)Handle)->hLockSem);
         }
         else
         {
            u32ErrorCode = OSAL_E_TIMEOUT;
            s32ReturnValue = OSAL_ERROR;
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32OsalSTrace & 0x00000020)||((u32CopiedBytes == 0)&&(u32ErrorCode != OSAL_E_TIMEOUT)))
   {   
      tString coszName = NULL;
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      
      if(pCurrentEntry)
      {
         coszName = pCurrentEntry->szName;
      }
      if(coszName == NULL)coszName =(tString)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
#ifdef NO_PRIxPTR
      TraceString("OSALMQ Task:%s(ID:%d) Read from MQ Handle 0x%x Bytes:%d Count:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)u32CopiedBytes,u32ActualPrio,u32ErrorCode,coszName);
#else
      TraceString("OSALMQ Task:%s(ID:%d) Read from MQc Handle 0x%" PRIxPTR " Bytes:%d Count:%d Error:0x%x Name:%s",
                  name,(unsigned int)u32Temp,(uintptr_t)Handle,(tU32)u32CopiedBytes,u32ActualPrio,u32ErrorCode,coszName);
#endif
   }
   return u32CopiedBytes;   
}


tS32 OSAL_s32ShMemMessageQueueNotify(OSAL_tMQueueHandle hMQ,OSAL_tpfCallback pCallback,tPVoid pvArg)
{
   tS32 s32RetVal = OSAL_OK;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ);
   tS32 s32TID = NOTATHREAD;
   trMqueueElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
       /* check if callback handler task for this process already exists */  
      tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
      if(s32PidEntry == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         /* check if callback handler task for this process already exists */  
         s32StartCbHdrTask(s32PidEntry);
      }
      pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
      if( pCurrentEntry!= OSAL_NULL )
      {
         if(LockOsal(&pOsalData->MqueueTable.rLock ) == OSAL_OK)
         {
            /* check if queue isn't deleted */
            if(pCurrentEntry->bIsUsed)
            {
               s32TID = (tS32)OSAL_ThreadWhoAmI();
#ifdef USE_EVENT_AS_CB
               if((uintptr_t)pCallback == 0xffffffff)
               {
                  if(pvArg != NULL)
                  {
                     trMqEventInf* pEvent = (trMqEventInf*)pvArg;
                     if(OSAL_s32EventOpen(pEvent->coszName,&pCurrentEntry->hEvent) != OSAL_OK)
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode=OSAL_E_DOESNOTEXIST;
                     }
                     else
                     {
                        pCurrentEntry->mask    = pEvent->mask;
                        pCurrentEntry->enFlags = pEvent->enFlags;
                     }
                  }
                  else
                  {
                     if(OSAL_s32EventClose(pCurrentEntry->hEvent) != OSAL_OK)
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
                        pCurrentEntry->hEvent = 0;
                     }
                  }
               }
               else//if((pCallback == NULL )&&(pvArg != NULL))
#endif

               {
 	
                  if( pCallback == NULL )
                  {
                     if( pvArg == NULL )
                     {
                        if( pCurrentEntry->pcallback != NULL )
                        {
                           if(pCurrentEntry->callbacktid == s32TID )
                           {
                              pCurrentEntry->bNotify = FALSE;
                              /*force dispatch to disable callback */
                              OSAL_s32ThreadWait(10);
                              pCurrentEntry->pcallback = NULL;
                              pCurrentEntry->callbacktid  = NOTATHREAD;
                              s32RetVal = OSAL_OK;
                           }
                           else
                           {
                              s32RetVal=OSAL_ERROR;
                              u32ErrorCode=OSAL_E_BUSY;
                           }
                        }
                        else
                        {
                           s32RetVal=OSAL_ERROR;
                           pCurrentEntry->callbacktid  = NOTATHREAD;
                        }
                     }
                     else
                     {
                        s32RetVal=OSAL_ERROR;
                        u32ErrorCode=OSAL_E_INVALIDVALUE;
                     }
                  }
                  else
                  {
                     if( pCurrentEntry->pcallback == NULL )
                     {
                        pCurrentEntry->callbackpid  = OSAL_ProcessWhoAmI();
                        pCurrentEntry->callbacktid  = s32TID;
                        pCurrentEntry->pcallback    = pCallback;
                        pCurrentEntry->pcallbackarg = pvArg;
                        pCurrentEntry->callbackpididx = s32GetPrcIdx(pCurrentEntry->callbackpid);
                        if(pCurrentEntry->callbackpididx == -1)
                        {
                          FATAL_M_ASSERT_ALWAYS();
                        }
                        pCurrentEntry->bNotify = TRUE;
                     }
                     else
                     {
                        s32RetVal = OSAL_ERROR;
                        u32ErrorCode = OSAL_E_BUSY;
                     }
                  }//if( pCallback == NULL )
               }//if((pCallback == NULL )&&(pvArg != NULL))
            }//if(pCurrentEntry->bIsUsed)
            UnLockOsal(&pOsalData->MqueueTable.rLock);
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32RetVal = OSAL_ERROR;
      vTraceMqNotify(pCurrentEntry,u32ErrorCode);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if((pCurrentEntry->bTraceCannel)||(u32OsalSTrace & 0x00000020)) /*lint !e613 pointer already checked */
      {
        vTraceMqNotify(pCurrentEntry,u32ErrorCode);
      }
   }
   return(s32RetVal);
}

tS32 OSAL_s32ShMemMessageQueueStatus( OSAL_tMQueueHandle hMQ,tPU32 pu32MaxMessage,tPU32 pu32MaxLength,tPU32 pu32Message)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckMqHandle(hMQ);
   trMqueueElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      /* handle is already checked */
      pCurrentEntry = ((tMQUserHandle*)hMQ)->pOrigin;/*lint !e613 *//* pointer already checked*/ 
      if( pCurrentEntry!= OSAL_NULL )
      {
         /* check if queue isn't deleted */
         if(pCurrentEntry->bIsUsed)
         {
            trStdMsgQueue* pTemp = &pStdMQEl[pCurrentEntry->u32EntryIdx];
            if( pu32MaxMessage != OSAL_NULL )
                  *pu32MaxMessage = pTemp->MaxMessages;
            if( pu32MaxLength  != OSAL_NULL )
                  *pu32MaxLength = pTemp->MaxLength;
            if( pu32Message    != OSAL_NULL )
                  *pu32Message     = pTemp->ActualMessage;
            s32ReturnValue = OSAL_OK;
         }
         else
         {
            u32ErrorCode =OSAL_E_DOESNOTEXIST;
         }
      }
      else
      {
         u32ErrorCode=OSAL_E_BADFILEDESCRIPTOR;
      } 
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      if(pCurrentEntry)
      {
           vTraceMqInfo((tMQUserHandle*)hMQ,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
      else
      {
           vTraceMqInfo(NULL,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
   }
   else
   {
      if((pCurrentEntry->bTraceCannel)||(u32OsalSTrace & 0x00000020))/*lint !e613 pointer already checked */
      {
         vTraceMqInfo((tMQUserHandle*)hMQ,OSAL_MQ_STATUS,TR_LEVEL_FATAL,u32ErrorCode,NULL);
      }
   }
   return( s32ReturnValue );
}

#endif

#ifdef __cplusplus
}
#endif

 
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
