/*****************************************************************************
| FILE:         osalcallback.c
| PROJECT:      Platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) IOSC Connection-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 28.02.13  | Fixed lint warnings.       | mdh4cob
| 11.11.13  | Adding new CallBack for u32|
|           | parameter as the Argument  | SWM2KOR 
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#ifdef __cplusplus
extern "C" {
#endif


#include "exception_handler.h"
#include "Linux_osal.h"
#include "ostrace.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
typedef void (*OSAL_tpfCallback2) ( tPVoid, tPVoid );

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static tS32 s32Pid = OSAL_ProcessWhoAmI();
tS32 s32LocalCbHdrTid = -1;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
OSAL_tMQueueHandle hMQCbPrc[MAX_LINUX_PROC];

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

extern CbExc            pCbExc;

void vTraceCallbackExecution(trMqueueElement* pElement,tU32 u32ErrorCode)
{
#ifdef SHORT_TRACE_OUTPUT
    char au8Buf[22+OSAL_C_U32_MAX_NAMELENGTH];
    tU32 Temp = (tU32)OSAL_ThreadWhoAmI();

    OSAL_M_INSERT_T8(&au8Buf[0], (tU8)OSAL_MQ_CALLBACK);
    bGetThreadNameForTID(&au8Buf[1],8, (tS32)Temp);
    OSAL_M_INSERT_T32(&au8Buf[9], Temp);
    OSAL_M_INSERT_T32(&au8Buf[13],(tU32)pElement->pcallback);
    OSAL_M_INSERT_T32(&au8Buf[17], u32ErrorCode);
    Temp = strlen(pElement->szName);
    memcpy (&au8Buf[21], pElement->szName, Temp);
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_DATA,au8Buf,21+Temp);
#else
    char au8Buf[251];
    char name[TRACE_NAME_SIZE];
    tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
    au8Buf[0] = OSAL_STRING_OUT;
    bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
#ifdef NO_PRIxPTR
    snprintf(&au8Buf[1],250,"Task:%s (ID:%d) Executes callback 0x%x Error:0x%x for MQ:%s",
             name,(unsigned int)u32Temp,(uintptr_t)pElement->pcallback,(unsigned int)u32ErrorCode,pElement->szName);
#else
    snprintf(&au8Buf[1],250,"Task:%s (ID:%d) Executes callback 0x%" PRIxPTR " Error:0x%x for MQ:%s",
             name,(unsigned int)u32Temp,(uintptr_t)pElement->pcallback,(unsigned int)u32ErrorCode,pElement->szName);
#endif
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_MQ, TR_LEVEL_DATA,au8Buf,strlen(&au8Buf[1])+1);
#endif
}
/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/


/*****************************************************************************
*
* FUNCTION:    GetPrcLocalMsgQHandle
*
* DESCRIPTION: This function is the callbackhandler task for initial process
*
* PARAMETER:     tU32  Pid, 
*
* RETURNVALUE:   tU32  Handle
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.09.11  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
OSAL_tMQueueHandle GetPrcLocalMsgQHandle(tU32 u32PrcId)
{
  tU32 u32Count;

  for(u32Count = 0; u32Count < pOsalData->u32MaxNrProcElements; u32Count++)
  {
     if(prProcDat[u32Count].u32Pid == u32PrcId)break;
  }
  if(u32Count < pOsalData->u32MaxNrProcElements)
  {
     if(hMQCbPrc[u32Count] == 0)
     {
         if(OSAL_s32MessageQueueOpen(prProcDat[u32Count].szMqName,
                                     OSAL_EN_READWRITE,
                                     &hMQCbPrc[u32Count]) == OSAL_ERROR)
         {
             NORMAL_M_ASSERT_ALWAYS();
             hMQCbPrc[u32Count] = 0;
             return 0;
         }
     }
     return hMQCbPrc[u32Count];
  }
  else 
  {
     TraceString("GetPrcLocalMsgQHandle No match for MQ Queue found");
     return 0;
  }
}

tS32 s32GetPrcIdx(tU32 u32PrcId)
{
  tU32 u32Count;

  for(u32Count = 0; u32Count < pOsalData->u32MaxNrProcElements; u32Count++)
  {
     if(prProcDat[u32Count].u32Pid == u32PrcId)break;
  }
  if(u32Count < pOsalData->u32MaxNrProcElements)
  {
     return u32Count;
  }
  else 
  {
     return -1;
  }
}

/*****************************************************************************
*
* FUNCTION:    u32ExecuteLiCb
*
* DESCRIPTION: This function is the callbackhandler task for initial process
*
* PARAMETER:     tS32               TskId, 
*                tS32               Pid, 
*                OSAL_tpfCallback   callback address
*                tPVoid             callback argument
*                tU8                String
*
* RETURNVALUE:   tU32  error code
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.09.11  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tU32 u32ExecuteLiCb( tS32 s32TskId, tS32 s32Prc, OSAL_tpfCallback pcallback, tPVoid pcallbackarg, tU8 au8String[] )
{
   tU32 u32Ret = OSAL_E_NOERROR; 
   ((void)s32TskId);
   ((void)s32Prc);

   void *paged_addr;
   unsigned char vec[1];

   paged_addr = ALIGN_VOIDP(pcallback);
   
   //if(dev_null_descr != -1)
   //if(write(dev_null_descr, paged_addr, EXEC_PAGESIZE) == EXEC_PAGESIZE)

   if(mincore(paged_addr, EXEC_PAGESIZE, vec) == 0)
   {
      /*if( pcallback == NULL )
     {
         printf("No callback function available \n");
         u32Ret = OSAL_E_DOESNOTEXIST;
     }
     else*/
     if( au8String == NULL )
     {
        pcallback( pcallbackarg );
     }
     else
     {
        OSAL_tpfCallback2 pcallback2 = (OSAL_tpfCallback2)pcallback;
        pcallback2( pcallbackarg, au8String );
     }
   }
   else
   {
      u32Ret = OSAL_E_NOPERMISSION;
      TraceString("Illegal callback adress !!!");
   }
   return u32Ret;
}
/*****************************************************************************
*
* FUNCTION:    u32ExecuteLiu32ParmCB
*
* DESCRIPTION: This function is the callbackhandler for U32 paramater as argument
*
* PARAMETER:   
*                OSAL_tu32ParmCallbackFunc   callback address
*                tU32             callback argument
*
* RETURNVALUE:   tU32  error code
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.09.11  | Initial revision                       | SWM2KOR
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tU32 u32ExecuteLiu32ParmCB( OSAL_tu32ParmCallbackFunc pcallback, tU32 pcallbackarg )
{
   tU32 u32Ret = OSAL_E_NOERROR; 
   if( pcallback == NULL )
   {
      u32Ret = OSAL_E_DOESNOTEXIST;
   }
   else 
   {
      pcallback( pcallbackarg );
   }
   return u32Ret;
}


#ifdef USE_OSAL_CB_HDR_SIGNAL
jmp_buf env;

void sigaction_jumpfunc(int n_signal, siginfo_t *siginfo, void *ptr)
{
    ((void)n_signal);
    ((void)siginfo);
    ((void)ptr);
    longjmp(env,1);
}
#endif

#ifdef OSAL_EXIT_SIGNAL
void vBroadCastSignal(int Signal, int Val)
{
   int fd,pid;
   DIR *pDir;
   struct dirent *pDEntry;
   char path[128];
   pDir = opendir(VOLATILE_DIR"/OSAL/Processes");
   while( NULL != (pDEntry = readdir( pDir )) )
   {
      if(pDEntry->d_name[0] != '.')
      {
         snprintf(path,128,"/proc/%s",pDEntry->d_name);
         fd = open(path,O_RDONLY);
         if(fd != -1)
         {
            pid = atoi(pDEntry->d_name);
            if((pid > 0)&&(pid != getpid()))
            {
               //if(kill(pid, OSAL_EXIT_SIGNAL) < 0)
               sigval value;
               value.sival_int = Val;
               if(sigqueue(pid,Signal,value)< 0)
               {
                  TraceString("Send Signal %d OSAL_EXIT_SIGNAL for Process %d failed errno:%d",Signal,pid,errno);
               }
            }
            close(fd);
         }
      }
   }
   closedir(pDir);
}
#endif

/*****************************************************************************
*
* FUNCTION:    vNewCallbackHandler
*
* DESCRIPTION: This function is the callbackhandler task for further processes
*
* PARAMETER:     VP Task argument
*
* RETURNVALUE:   none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.06.10  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vNewCallbackHandler(void* pArg)
{
   tBool bTerminate = FALSE; 
   tOsalMbxMsg rMsg = {0};
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tU32 u32Tid = OSAL_ThreadWhoAmI();  
   uintptr_t u32_prc_count = (uintptr_t) pArg;
   trMqueueElement* pElement = NULL;
#ifdef USE_OSAL_CB_HDR_SIGNAL
   struct sigaction act_sigjmp;
   tBool bLockActive = FALSE;
#endif

   /* check for current process if libunwind can be used */   
   vCheckLibUnwind();

   snprintf(&prProcDat[u32_prc_count].szMqName[0], OSAL_C_U32_MAX_NAMELENGTH, "CB_HDR_LI_%d", (int)u32_prc_count );
   if(OSAL_s32MessageQueueCreate(prProcDat[u32_prc_count].szMqName,
                                 MAX_MSG_CBHDR,
                                 sizeof(tOsalMbxMsg),
                                 OSAL_EN_READWRITE,
                                 &hMQCbPrc[u32_prc_count]) == OSAL_ERROR)
   {
      if(OSAL_u32ErrorCode() != OSAL_E_ALREADYEXISTS)
      {
         NORMAL_M_ASSERT_ALWAYS();
      /* avoid long runner */
         bTerminate = TRUE;
      }
   }

   /* Trigger task is ready */
   OSAL_tSemHandle hSem = 0;
   char cNameBuf[OSAL_C_U32_MAX_NAMELENGTH];
   snprintf(cNameBuf,OSAL_C_U32_MAX_NAMELENGTH,"CB%d_SYNC_SEM",(int)u32Tid);
   if(OSAL_OK == OSAL_s32SemaphoreOpen(cNameBuf,&hSem))
   {
      if(OSAL_s32SemaphorePost(hSem)== OSAL_ERROR)
      {
        NORMAL_M_ASSERT_ALWAYS();
      }
      OSAL_s32SemaphoreClose(hSem);
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }


   /* set s32LocalCbHdrTid when MQ resources are created to ensure the availability of the resource */
   s32LocalCbHdrTid = (tS32)u32Tid;

#ifdef USE_OSAL_CB_HDR_SIGNAL
   sigset_t set;
   sigemptyset(&set);
   sigaddset(&set, OSAL_CB_HDR_SIGNAL);
   pthread_sigmask(SIG_UNBLOCK, &set, NULL);

   act_sigjmp.sa_sigaction = sigaction_jumpfunc;
   act_sigjmp.sa_flags = SA_SIGINFO | SA_RESTART;
   if(sigaction(OSAL_CB_HDR_SIGNAL, &act_sigjmp,NULL) != 0)
   {
       TraceString("MQ:%s sigaction failed errno:%d",prProcDat[u32_prc_count].szMqName,errno);

   }
   if(setjmp(env) != 0)
   {
      vWritePrintfErrmem("MQ:%s blocked TID:%d returns from longjump",prProcDat[u32_prc_count].szMqName,u32Tid);
   }
   if(bLockActive)
   {
      vWritePrintfErrmem("MQ:%s returned from longjump unlock if necessary",prProcDat[u32_prc_count].szMqName);
      if((rMsg.rOsalMsg.Cmd == MBX_CB_PRM)||(rMsg.rOsalMsg.Cmd == MBX_CB_PRM2))
      {
          UnLockOsal( &pOsalData->PrmTableLock );
      }
      else if( rMsg.rOsalMsg.Cmd == MBX_MQ_NOTIFY )
      {
          UnLockOsal(&pOsalData->MqueueTable.rLock);
      }
      else
      {
         TraceString("MQ:%s no unlock necessary",prProcDat[u32_prc_count].szMqName);
      }
   }
#endif

   while(bTerminate == FALSE)
   {
      pElement = &pMsgQEl[0];
      if(OSAL_s32MessageQueueWait(hMQCbPrc[u32_prc_count],
                                  (tPU8)&rMsg,
                                   sizeof(tOsalMbxMsg),
                                   0,
                                   OSAL_C_TIMEOUT_FOREVER ) > 0)  
      {
         if((rMsg.rOsalMsg.Cmd == MBX_CB_PRM)||(rMsg.rOsalMsg.Cmd == MBX_CB_PRM2))
         {
            if( rMsg.rPrmMsg.u32Pid != (tU32)s32Pid ) // check if process is the same
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            else
            {
               /* ensure that no change for PRM camback is happend during execution*/
               LockOsal( &pOsalData->PrmTableLock );
#ifdef USE_OSAL_CB_HDR_SIGNAL
               bLockActive = TRUE;
#endif
              // execute callback
               u32ErrorCode = u32ExecuteLiCb( 0, // no taskid needed
                                            rMsg.rPrmMsg.u32Pid,
                                            (OSAL_tpfCallback)rMsg.rPrmMsg.u32CallFun,
                                            (tPVoid)&rMsg.rPrmMsg.u32Param1,
                                            (rMsg.rOsalMsg.Cmd == MBX_CB_PRM2) ? rMsg.rPrmMsg.au8UUID : NULL );
               UnLockOsal( &pOsalData->PrmTableLock );
#ifdef USE_OSAL_CB_HDR_SIGNAL
               bLockActive = FALSE;
#endif
               if(pCbExc)
               {
                  pCbExc(s32Pid,u32Tid, rMsg.rPrmMsg.u32Param2);
               }
             #if 0
               if(u32ErrorCode == OSAL_E_NOERROR)
               {
                   if(pElement->bTraceCannel == TRUE)vTraceCallbackExecution(pElement,u32ErrorCode);
               }
               else
               {
                   vTraceCallbackExecution(pElement,u32ErrorCode);
               }
              #endif
            }
         }
         else if( rMsg.rOsalMsg.Cmd == MBX_MQ_NOTIFY )
         {
            pElement = pElement + rMsg.rOsalMsg.ID;

            if(pElement->pcallback)
            {
               /* check for Callback of own Process*/
               if(s32Pid != pElement->callbackpid)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
               else
               {
                  if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
                  {
#ifdef USE_OSAL_CB_HDR_SIGNAL
                     bLockActive = TRUE;
#endif
                     u32ErrorCode = u32ExecuteLiCb(0,
                                                   0,
                                                   pElement->pcallback,
                                                   pElement->pcallbackarg,
                                                   NULL );
                     if(u32ErrorCode == OSAL_E_NOERROR)
                     {
                        if(pElement->bTraceCannel == TRUE)vTraceCallbackExecution(pElement, u32ErrorCode);
                     }
                     else
                     {
                        vTraceCallbackExecution(pElement, u32ErrorCode);
                     }
                     UnLockOsal(&pOsalData->MqueueTable.rLock);
#ifdef USE_OSAL_CB_HDR_SIGNAL
                     bLockActive = FALSE;
#endif
                  }
               }
            }//if(pElement->pcallback)
         }
         else if(rMsg.rOsalMsg.Cmd == MBX_RB_NOTIFY)
         {
             if(s32Pid ==  (tS32)rMsg.rOsalMsg.ID) 
             {
                rMsg.rOsalMsg.pFunc(rMsg.rOsalMsg.pArg);
             }
             else
             {
                NORMAL_M_ASSERT_ALWAYS();
             }
         }
         else if(rMsg.rOsalMsg.Cmd == MBX_CB_ARG)
         {
             if(rMsg.rCBArgMsg.u32Pid != (tU32)s32Pid)
             {
                 NORMAL_M_ASSERT_ALWAYS();
             }
             else
             {
                 u32ErrorCode = u32ExecuteLiCb(0,
                                               rMsg.rCBArgMsg.u32Pid,
                                               (OSAL_tpfCallback)rMsg.rCBArgMsg.u32CallFun,
                                               (tPVoid)rMsg.rCBArgMsg.au8Arg,
                                               NULL);
             }
         }
         /* Call Back Mechanism for U32 Parameter as Argument */
         else if(rMsg.rOsalMsg.Cmd ==(tU32) MBX_CB_U32PARM)
         {
            if(rMsg.ru32ParmCBMsg.u32Pid != (tU32)s32Pid)
            {
                NORMAL_M_ASSERT_ALWAYS();
            }
            else
            {
               u32ErrorCode = u32ExecuteLiu32ParmCB((OSAL_tu32ParmCallbackFunc)rMsg.ru32ParmCBMsg.u32CallFun,
                                                    rMsg.ru32ParmCBMsg.u32Param1);
            }  
         }
         else if(rMsg.rOsalMsg.Cmd == MBX_STRACE)
         {
           tS32 i,Pid = OSAL_ProcessWhoAmI();
           u32OsalSTrace = rMsg.rOsalMsg.ID;
           if(u32OsalSTrace & 0x00000002)
           {
               for(i=0;i<OSAL_EN_DEVID_LAST;i++)
               {
                  if(OSAL_EN_DEVID_TRACE == i)
                  {
                     if(u32OsalSTrace & 0x10000000)
                     {
                        pOsalData->rDevTrace[i].cActive = 1;
                        pOsalData->rDevTrace[i].s32Pid  = Pid;
                     }
                  }
                  else
                  {
                        pOsalData->rDevTrace[i].cActive = 1;
                        pOsalData->rDevTrace[i].s32Pid  = Pid;
                  }
               }
           }
           else
           {
               for(i=0;i<OSAL_EN_DEVID_LAST;i++)
               {
                  pOsalData->rDevTrace[i].cActive = 0;
                  pOsalData->rDevTrace[i].s32Pid  = 0;
               }
           }
         }
         else if(rMsg.rOsalMsg.Cmd == MBX_TERM_TSK)
         {
             bTerminate = TRUE;
             break;
         }
         else
         {
            /* unknown message received */


            NORMAL_M_ASSERT_ALWAYS();
         }
      }
   }

#ifdef OSAL_EXIT_SIGNAL
   vBroadCastSignal(OSAL_EXIT_SIGNAL,u32_prc_count);
#endif
   OSAL_s32MessageQueueClose(hMQCbPrc[u32_prc_count]);
   hMQCbPrc[u32_prc_count] = 0;
   OSAL_s32MessageQueueDelete(prProcDat[u32_prc_count].szMqName);
}
#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalcallback.c
|-----------------------------------------------------------------------*/
