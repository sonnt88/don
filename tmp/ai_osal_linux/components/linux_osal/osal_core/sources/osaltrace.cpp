/************************************************************************
| FILE:         osaltrace.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------
| DESCRIPTION:  Assistance functions for OSAL trace
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 11.03.09  | MMS 223874 :               | JEV1KOR 
|           | Corrections in FileCopy()  |
| --.--.--  | ----------------           | -------, -----
| 31.03.09  | Made new TTFis command      | anc3kor
|            |    to Format SDCard            |
| --.--.--  | ----------------           | -------, -----
|************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include <dlfcn.h>

#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif


extern dbg_func pCoreDbgFunc;


/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/
    
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static int fd_log = -1;

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/


/************************************************************************ 
|function implementation (scope: module-local) 
|-----------------------------------------------------------------------*/
/*****************************************************************************
*
* FUNCTION:    LLD_bOpenTrace
*
* DESCRIPTION: This functions gets an trace handle for OSAL using applications                
*
* PARAMETER:   none
*
* RETURNVALUE: tBool
*                 it is the function return value: 
*                 - TRUE if everything goes right;
*                 - FALSE otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool LLD_bOpenTrace()
{
   return TRUE;
}

pFuncIsClassSelected        IsClassSelected = NULL;
OSAL_DECL pFuncTraceOut               TraceOut = NULL;
pFuncTraceBinOutput         TraceBinOutput = NULL;
pFunc_chan_acess_bRegChan   chan_acess_bRegChan = NULL;
pFunc_chan_acess_bUnRegChan chan_acess_bUnRegChan = NULL;
OSAL_DECL pFunc_get_blockmode_status  get_blockmode_status = NULL;

void* pHandle = NULL;
static  tBool bAditTraceLoadDone = FALSE;

void InitOsalTrace(void)
{
   tU32 i;
   pOsalData->u32TraceLevel = 2;

   for(i=0;i< 50;i++)
   {
      pOsalData->u32Class[i] = 0xffffffff;
   }
}


tBool LoadTrace(void)
{
   tS32 s32Pid =  OSAL_ProcessWhoAmI();
//   char *error;
   bAditTraceLoadDone = TRUE;
   dlerror();    /* Clear any existing error */
   pHandle = dlopen("libtrace.so", RTLD_NOW );
   if(pHandle == NULL)
   {
       return FALSE;
   }
   else
   {
      IsClassSelected       = (pFuncIsClassSelected)(dlsym(pHandle, (const char*)"TR_core_bIsClassSelected"));/*lint !e611 */;
      TraceOut              = (pFuncTraceOut)(dlsym(pHandle, (const char*)"TR_core_uwTraceOut"));/*lint !e611 */
      TraceBinOutput        = (pFuncTraceBinOutput)(dlsym(pHandle, (const char*)"TR_core_uwTraceBinOutput"));/*lint !e611 */
      chan_acess_bRegChan   = (pFunc_chan_acess_bRegChan)(dlsym(pHandle, (const char*)"TR_chan_acess_bRegChan"));/*lint !e611 */
      chan_acess_bUnRegChan = (pFunc_chan_acess_bUnRegChan)(dlsym(pHandle, (const char*)"TR_chan_acess_bUnRegChan"));/*lint !e611 */
      get_blockmode_status  = (pFunc_get_blockmode_status)(dlsym(pHandle, (const char*)"TR_get_blockmode_status"));/*lint !e611 */
         
      if((!IsClassSelected)||(!TraceOut)||(!TraceBinOutput)||(!chan_acess_bRegChan)||(!chan_acess_bUnRegChan)||(!get_blockmode_status))
      {
         dlclose(pHandle);
         return FALSE;
      }
      if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_COMPONENT))
      {
         tS32 s32PidEntry = s32FindProcEntry(s32Pid);
         if(s32PidEntry == OSAL_ERROR)
         {
           //NORMAL_M_ASSERT_ALWAYS();
           TraceString("Loaded Trace for unknown PID OSAL during load?");
         }
         else
         {
            TraceString("Loaded Trace PID:%d %s",s32Pid,prProcDat[s32PidEntry].pu8CommandLine);
         }
      }
      return TRUE;
   }
}
                                                                      
 

/*****************************************************************************
*
* FUNCTION:    LLD_vCloseTrace
*
* DESCRIPTION: This functions closes the trace handle for OSAL using 
*              applications, if it isn' used furthermore
*
* PARAMETER:   none
*
* RETURNVALUE: none
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tVoid LLD_vCloseTrace(void)
{
    if(fd_log != -1)
    {
       close(fd_log);
    }
}

tBool bCheckTraceActive(tU32 u32Class, tU32 u32Level)
{
   tBool bRet = FALSE;
   tU32 i;
   // evalute global trace level first
   if(u32Level <= pOsalData->u32TraceLevel)
   {
      bRet = TRUE;
   }

   for(i=0;i< pOsalData->u32NrOfClass;i++)
   {
      if((pOsalData->u32Class[i])&&(pOsalData->u32Class[i] == u32Class))
      {
         if(u32Level <= pOsalData->u32Level[i])
         {
            bRet = TRUE;
         }
         else
         {
             // override global trace level if specific class was set to lower level
             bRet = FALSE;
         }
         break;
      }
   }
   return bRet;
}


/*****************************************************************************
*
* FUNCTION:    LLD_bIsTraceActive
*
* DESCRIPTION: This functions proves if the required trace class/level is 
*              switched on                
*
* PARAMETER:   tU32 u32Class    required trace class
*              tU32 u32Level    required trace level
*
* RETURNVALUE: tBool
*                 it is the function return value: 
*                 - TRUE if class/level is selected
*                 - FALSE otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
{
   if((bAditTraceLoadDone == FALSE))
   {
       LoadTrace();
   }
   if(IsClassSelected)
   {
      return IsClassSelected((tU16)u32Class,(TR_tenTraceLevel)u32Level);
   }
   else
   {
      return bCheckTraceActive(u32Class,u32Level);
   }
}


void vWriteTraceRawLog(int fd,tU32 u32Class,void* pvData, tU32 u32Length)
{
   tU32 datalen = 0;
   tU32 len = u32Length;
   char cDest[4*1024];
   char Destination[4];
   tU8* pData = (tU8*)&u32Class;
   
   if(fd)
   {
      tU32 i, j;
      datalen = strlen("DATA:");
      strncpy(&cDest[0],"DATA:",datalen);
      sprintf(Destination,"%02x", *pData);  
      cDest[datalen+3] = Destination[0];
      cDest[datalen+4] = Destination[1];
 	  pData++;
 
      sprintf(Destination,"%02x", *pData);  
      cDest[datalen] = Destination[0];
      cDest[datalen+1] = Destination[1];
      cDest[datalen+2] = ' ';
	  strcpy(&cDest[datalen+5]," 00 00 00 ");
	  
      j=datalen + 15;
      for(i=0;i<len;i++)
      {
        sprintf(Destination,"%02x",*((tU8*)pvData+i));  
        cDest[j] = Destination[0];
        j++;
        cDest[j] = Destination[1];
        j++;
        cDest[j] = ' ';
        j++;
      }
      cDest[j] = 0x0d;
      j++;
      cDest[j] = 0x0a;
      write(fd,cDest,j+1);
   }
}

   
/*****************************************************************************
*
* FUNCTION:    LLD_vTrace
*
* DESCRIPTION: This functions generates trace output for OSAL using applications                
*
* PARAMETER:   tU32 u32Class    required trace class
*              tU32 u32Level    required trace level
*              void* pvData     pointer to trace data
*              tU32 u32Length   trace data length
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
{
    if((bAditTraceLoadDone == FALSE))
    {
       LoadTrace();
    }
    
    if(TraceOut != NULL)
    {
       TraceOut(u32Length,u32Class,(TR_tenTraceLevel)u32Level,(tU8*)pvData); /*lint !e1773 */
    }
 
    if((TraceOut == NULL)||(pOsalData->u32OsalTrace))
    {
       tBool bDoTrace = bCheckTraceActive(u32Class,u32Level);
       if(bDoTrace == TRUE)
       {
          char Prefix[50];
          tU32 u32Time = OSAL_ClockGetElapsedTime();
          tS32 s32Size = snprintf(Prefix,50,"[%02d:%02d.%02d.%03d|%05d|%u] ",
                               u32Time / (60 * 60 * 1000) % 24,
                               u32Time / (60 * 1000) % 60,
                               u32Time / 1000 % 60,
                               u32Time % 1000,u32Class,u32Level);
          char* pTmp = ((char*)pvData+1);/*lint !e1773 */
          write(1, Prefix,s32Size);
          write(1, pTmp,u32Length-1);
          write(1, "\n",1);

          if(pOsalData->u32WritToFile)
          {
             if(fd_log == -1)
             {
                fd_log = open(&pOsalData->szPathTraceLog[0],O_RDWR | O_CREAT | O_APPEND ,OSAL_ACCESS_RIGTHS);
             }
             if(fd_log != -1)
             {
                if(pOsalData->u32WritToFile == 2)
                {
                   /* do original trace to allow trace via file@card in TTFIS */
                   vWriteTraceRawLog(fd_log,u32Class,(void*)pvData,u32Length);/*lint !e1773 */
                }
                else
                {
                   write(fd_log, Prefix,s32Size);
                   write(fd_log, pTmp, u32Length);
                   write(fd_log, "\n",1);
                }
             }
		  }
       }
    }
}



/*****************************************************************************
*
* FUNCTION:    LLD_vRegTraceCallback
*
* DESCRIPTION: This functions registers the OSAL Callback function to the
*              trace device
*
* PARAMETER:    TR_tenTraceChan enTraceChannel,   Channel for register
*               OSAL_tpfCallback pfCallback       Callback to register
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void LLD_vRegTraceCallback( TR_tenTraceChan enTraceChannel, OSAL_tpfCallback pfCallback )
{
  if(chan_acess_bRegChan == NULL)
  {
    LoadTrace();
  }
  if(chan_acess_bRegChan != NULL)
  {
     chan_acess_bRegChan(enTraceChannel,pfCallback);
  }
}



void LLD_vUnRegTraceCallback( TR_tenTraceChan enTraceChannel)
{
  if(chan_acess_bUnRegChan == NULL)
  {
     LoadTrace();
  }
  if(chan_acess_bUnRegChan != NULL)
  {
     chan_acess_bUnRegChan(enTraceChannel, NULL);
  }
}



void TraceString(const char* cBuffer,...)
{
  tU16 u16Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  u16Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,cBuf,u16Len+1);
}

void TraceStringLevel(tU32 u32Level, const char* cBuffer,...)
{
  tU16 u16Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  u16Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  LLD_vTrace(TR_COMP_OSALCORE, u32Level,cBuf,u16Len+1);
}

void TraceStringClassLevel(tU32 u32Class,tU32 u32Level, const char* cBuffer,...)
{
  tU16 u16Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  u16Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  LLD_vTrace(u32Class, u32Level,cBuf,u16Len);
}


void TraceToConsole(const char* cBuffer,...)
{
   OSAL_tVarArgList argList; /*lint -e530 */
   char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
   tU16 u16Len = 0;
   u16Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[0], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
   if(u16Len > (OSAL_C_U32_MAX_PATHLENGTH-1))u16Len = OSAL_C_U32_MAX_PATHLENGTH-1;
   write(1,cBuf,u16Len);
}


void vCoreCallbackHandler(void* pvBuffer )
{
   if(pCoreDbgFunc == NULL)
   {
      if(u32LoadSharedObject((void*)pCoreDbgFunc,pOsalData->rLibrary[EN_SHARED_OSAL2].cLibraryNames) != OSAL_E_NOERROR)/*lint !e611 otherwise linker warning */
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
  }

  if(pCoreDbgFunc)
  {
      pCoreDbgFunc(pvBuffer );
  }
}

/*****************************************************************************
*
* FUNCTION:    vRegisterSysCallback
*
* DESCRIPTION: Register OSAL Callback
*
* PARAMETER:   none
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vRegisterSysCallback(void)
{
   LLD_vRegTraceCallback((TR_tenTraceChan)TR_TTFIS_LINUX_OSALCORE,(OSAL_tpfCallback)&vCoreCallbackHandler);
}

/*****************************************************************************
*
* FUNCTION:    vUnregisterSysCallback
*
* DESCRIPTION: Unregister OSAL Callback
*
* PARAMETER:   none
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vUnregisterSysCallback (void)
{
}

static long tickspersec = sysconf(_SC_CLK_TCK);

tU32 u32ScanPidTaskTidStat(char* Path,tBool bErrMemEntry,tS32 s32Idx)
{
   FILE* fdstr = NULL;
   char Buffer[246]; //needed for /proc/<PID>/tsak/<TID>/wchan & /proc/<PID>/tsak/<TID>/stat
   tU32 u32Ret = 0;
   char wchan[100];
   int fd = -1;

   OSAL_szStringCopy(Buffer,Path);
   OSAL_szStringConcat(Buffer,"/wchan");
   if((fd = open(Buffer, O_RDONLY,0)) != -1)
   {
       memset(wchan,0,100);
       read(fd,wchan,100);
       close(fd);
   }
   OSAL_szStringCopy(Buffer,Path);
   OSAL_szStringConcat(Buffer,"/stat");

   fdstr = fopen(Buffer, "r");
   if(fdstr)
   {
     int Dummy;
     long int lDummy;
     unsigned uDummy;
     long unsigned luDummy;
     long long unsigned lluDummy;
     int TID   = 0;
     int PPID  = 0;
     char cTskName[PATH_MAX] = {0};
     char cStatus = '?';
     char* pPolicy = (char*)"Unknown";/*lint !e1773 */  /*otherwise linker warning */
     long unsigned UTim = 0;
     long unsigned KTim = 0;
     long long unsigned StartTim = 0;
     long int cuTim = 0;
     long int csTim = 0;
     long int Prio  = 0;
     long int Nice  = 0;
     unsigned rtPrio = 0;
     unsigned uPolicy= 0;
     fscanf(fdstr,"%d ",&TID);     // 1. TID
     fscanf(fdstr,"%s ",&cTskName[0]);// 2. Taskname
     fscanf(fdstr,"%c ",&cStatus); // 3. %c ne character from the string "RSDZTW" where R is running, S is sleeping in an interruptible wait, D is waiting 
                                 //   in uninterruptible disk sleep, Z is zombie, T is traced or stopped (on a signal), and W is paging.*/
     fscanf(fdstr,"%d ",&PPID );   // 4. %d The PID of the parent.
     fscanf(fdstr,"%d ",&Dummy);   // 5.
     fscanf(fdstr,"%d ",&Dummy);   // 6.
     fscanf(fdstr,"%d ",&Dummy);   // 7.
     fscanf(fdstr,"%d ",&Dummy);   // 8.
     fscanf(fdstr,"%u ",&uDummy);  // 9.
     fscanf(fdstr,"%lu ",&luDummy);// 10.
     fscanf(fdstr,"%lu ",&luDummy);// 11.
     fscanf(fdstr,"%lu ",&luDummy);// 12.
     fscanf(fdstr,"%lu ",&luDummy);// 13.
     fscanf(fdstr,"%lu ",&UTim);   // 14 %ld Amount of time that this process's waited-for children have been scheduled in user mode, 
                                  //    measured in clock ticks (divide by sysconf(_SC_CLK_TCK)).(See also times(2).) This includes guest time, cguest_time 
     fscanf(fdstr,"%lu ",&KTim);   // 15. %ld Amount of time that this process's waited-for children have been scheduled in kernel mode,
                                   //     measured in clock ticks (divide by sysconf(_SC_CLK_TCK)). */
     fscanf(fdstr,"%ld ",&cuTim);  // 16.
     fscanf(fdstr,"%ld ",&csTim);  // 17.
     fscanf(fdstr,"%ld ",&Prio);   /* 18.priority  this is the negated scheduling priority, minus one; that is, a number in the range -2 to -100,
                                      corresponding to real-time priorities 1 to 99. For processes running under a non-real-time scheduling policy, this is 
                                      the raw nice value (setpriority(2)) as represented in the kernel. The kernel stores nice values as numbers in the range
                                      0 (high) to 39 (low), corresponding to the user-visible nice range of -20 to 19. */
     fscanf(fdstr,"%ld ",&Nice);   // 19. The nice value (see setpriority(2)), a value in the range 19 (low priority) to -20 (high priority). t_priority %u (since Linux 2.5.19; was %lu before Linux 2.6.22) 
     fscanf(fdstr,"%ld ",&lDummy); // 20.
     fscanf(fdstr,"%ld ",&lDummy);   // 21.
     fscanf(fdstr,"%llu ",&StartTim); /* 22.The time the process started after system boot. In kernels before Linux 2.6, this value was expressed in jiffies. 
                                            Since Linux 2.6, the value is expressed in clock ticks (divide by sysconf(_SC_CLK_TCK)).*/

     fscanf(fdstr,"%lu ",&luDummy);   // 23.
     fscanf(fdstr,"%ld ",&lDummy);   // 24.
     fscanf(fdstr,"%lu ",&luDummy);   // 25.
     fscanf(fdstr,"%lu ",&luDummy);   // 26.
     fscanf(fdstr,"%lu ",&luDummy);   // 27.
     fscanf(fdstr,"%lu ",&luDummy);   // 28.
     fscanf(fdstr,"%lu ",&luDummy);   // 29.
     fscanf(fdstr,"%lu ",&luDummy);   // 30.
     fscanf(fdstr,"%lu ",&luDummy);   // 31.
     fscanf(fdstr,"%lu ",&luDummy);   // 32.
     fscanf(fdstr,"%lu ",&luDummy);   // 33.
     fscanf(fdstr,"%lu ",&luDummy);   // 34.
     fscanf(fdstr,"%lu ",&luDummy);   // 35.
     fscanf(fdstr,"%lu ",&luDummy);   // 36.
     fscanf(fdstr,"%lu ",&luDummy);   // 37.
     fscanf(fdstr,"%lu ",&luDummy);    // 38.
     fscanf(fdstr,"%d ",&Dummy);    // 39.
     fscanf(fdstr,"%u ",&rtPrio);  /* 40. Real-time scheduling priority, a number in the range 1 to 99 for processes scheduled under a real-time policy, or 0,
                                          for non-real-time processes */
     fscanf(fdstr,"%u ",&uPolicy);    // 41
     fscanf(fdstr,"%llu ",&lluDummy);  // 42
     fscanf(fdstr,"%lu ",&luDummy);   // 43
     fscanf(fdstr,"%ld ",&lDummy);    // 44
     switch(uPolicy)
     {
        case SCHED_OTHER:
             pPolicy = (char*)"SCHED_OTHER";/*lint !e1773 */  /*otherwise linker warning */
            break;
        /*	  case SCHED_BATCH:
             pPolicy = "SCHED_BATCH";
            break;
        case SCHED_IDLE:
             pPolicy = "SCHED_IDLE";
            break;*/
        case SCHED_FIFO:
             pPolicy = (char*)"SCHED_FIFO";/*lint !e1773 */  /*otherwise linker warning */
            break;
        case SCHED_RR:
             pPolicy = (char*)"SCHED_RR";/*lint !e1773 */  /*otherwise linker warning */
            break;
        default:
            break;
     }
     if(bErrMemEntry == FALSE)
     {
        TraceString("PID:%s PPID:%d TID:%d %s Status:%c Prio:%ld RTPrio:%u Policy:%s Nice:%ld stopped at:%s UserTime:%.10f sec  KernelTime:%.10f sec Started after %.10f ",
                    Path,PPID,TID,&cTskName[0], cStatus,Prio,rtPrio,pPolicy,Nice,wchan,(((double)UTim) / tickspersec),(((double)KTim) / tickspersec),(((double)StartTim) / tickspersec));
     }
     else
     {
        vWritePrintfErrmem("PID:%s PPID:%d TID:%d %s Status:%c Prio:%ld RTPrio:%u Policy:%s Nice:%ld stopped at:%s UserTime:%f sec  KernelTime:%f sec Started after %f \n",
                           Path,PPID,TID,&cTskName[0], cStatus,Prio,rtPrio,pPolicy,Nice,wchan, (((double)UTim) / tickspersec),(((double)KTim) / tickspersec),(((double)StartTim) / tickspersec));
     }
     if(s32Idx != -1)
     {
         switch(s32Idx)
         {
         case 1: 
             u32Ret = TID;
             break;
         case 3:
             u32Ret = (tU32)cStatus;
             break;
         case 4:
             u32Ret = PPID;
             break;
         case 18:
             u32Ret = Prio;
             break;
         case 19:
             u32Ret = Nice;
             break;
         case 40:
             u32Ret = rtPrio;
             break;
         default:
             break;
         }
     }
     fclose(fdstr);
   }
   return u32Ret;
}

/*****************************************************************************
*
* FUNCTION:     vReadFile
*
* DESCRIPTION: read file content
*
* PARAMETER:    void*  file name as string
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vReadFile(const void* pvBuffer,tBool bWriteErrmem)
{
#define LINE_LENGTH  250
  tS32 s32Val;
  tS32 fd;
  int size,i;
  int offset = 0;
  char cTraceBuf[LINE_LENGTH+5];
  tU16 u16OpenMode = 0;//O_TEXT;
  unsigned char* pBuffer = NULL;
  tBool bIgnoreSize= FALSE;

    if((fd = open((const char*)pvBuffer, O_RDONLY,u16OpenMode)) > OSAL_OK)
    {
        size = s32GetFileSize(fd);
        if(size == OSAL_ERROR)
        {
             size = 100000;
             bIgnoreSize = TRUE;
        }
        if((pBuffer = (unsigned char*)OSAL_pvMemoryAllocate((tU32)size)) != 0)
        {
          memset(pBuffer,0,(tU32)size);
          if((s32Val = lseek(fd,0,SEEK_SET)) < OSAL_OK)
          {
             TraceString((const char*)"Seek File Error");
             s32Val = -1;
          }
          else
          {
             if((s32Val = read( fd,pBuffer,(tU32)size)) != size)
             {  
                 if(bIgnoreSize != TRUE)
                 {
                     TraceString((const char*)"Read File Error");
                     s32Val = -1;
                 }
             }
        }
          
        if(s32Val >= 0)
        {
            OSAL_M_INSERT_T8(&cTraceBuf[0],OSAL_STRING_OUT);
            while(size)
            {
                /* search next line if available*/
                for (i = 0; i<LINE_LENGTH ;i++)
                {
                  if((*(pBuffer+offset+i) == /*EOF*/0xff) || (*(pBuffer+offset+i) == 0x0a/*EOL*/)) 
                  {
                     i++;
                     break;
                  }
                  if(offset+i > s32Val)break;
                }
                memcpy(&cTraceBuf[1],pBuffer+offset,(tU32)i);
                LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,&cTraceBuf[0],(tU32)i);
                if(bWriteErrmem)
                {
                    vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&cTraceBuf[0],i,OSAL_STRING_OUT);
                }
                offset += (i);
                if(offset >= s32Val)break;
          }// end while
        }
        else
        {
            TraceString((const char*)"Read File Error , Try it again");
        }
        OSAL_vMemoryFree(pBuffer);
      }
      else
      {
         TraceString((const char*)"Allocate Memory Error");
      }
      close(fd);
    }
    else
    {
     TraceString((const char*)"Open File Error");
    }
}




#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
