/*****************************************************************************
| FILE:         osallsof.c
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation for the "OSAL_GET_RESOURCES FILES"
|               trace command. It lists all open files on the Linux system by
|               traversing the /proc file system.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#define PROC_PREFIX	"/proc"
#define MAX_PNAMELEN	32
typedef unsigned char           tBool;
extern void TraceString(const char *buf);

static int isProcessDir(struct dirent *de)
{
	char *c = de->d_name;

   while (*c) {
      if ( !isdigit(*c))
         return 0;
      c++;
   }
	return 1;
}

static int fetchProcname(const char *path, char *buf, int buflen)
{
   int rc = -1;
	char format[32];
	FILE *fp = fopen(path, "r");
	if (fp) {
		(void) snprintf(format, sizeof(format), "Name: %%%us", buflen);
		(void) fscanf(fp, format, buf);
		buf[buflen] = 0;
		(void) fclose(fp);
		rc = 0;
	}
	return rc;
}

static char *type2String(unsigned mode)
{
	char *o = "?";

	if (S_ISDIR(mode))
		o = "DIR";
	else if (S_ISCHR(mode))
		o = "CHR";
	else if (S_ISREG(mode))
		o = "REG";
	else if (S_ISBLK(mode))
		o = "BLOCK";
	else if (S_ISFIFO(mode))
		o = "FIFO";
	else if (S_ISSOCK(mode))
		o = "SOCK";
	else if (S_ISLNK(mode))
		o = "LINK";
	return o;
}

static char *dev2String(dev_t st_dev, char *buf, size_t buflen)
{
	(void) snprintf(buf, buflen, "%u,%u", major(st_dev), minor(st_dev));
	return buf;
}

static char *fd2String(const char *name, mode_t st_mode, char *buf, size_t buflen)
{
	char m;

	if ( (st_mode & 0600) == 0200)
		m = 'w';
	else if ( (st_mode & 0600) == 0400)
		m = 'r';
	else if ( (st_mode & 0777) == 0)
		m = ' ';
	else
		m = 'u';

	(void) snprintf(buf, buflen, "%s%c", name, m);
	return buf;
}

static int printEntry(char *cmd, char *pid, const struct dirent *dentry,
	const struct stat *sb, const struct stat *lsb, const char *path)
{
	char devspec[11];
	char fdspec[5];
	char trBuf[200];
	int n;

	n = snprintf(trBuf, sizeof(trBuf), "%16.16s %5.5s %4d %4.4s %5.5s %10.10s %10u %10u %s",
		cmd, pid, sb->st_uid,
		fd2String(dentry->d_name, sb->st_mode, fdspec, sizeof(fdspec)),
		type2String(lsb->st_mode),
		dev2String(lsb->st_dev, devspec, sizeof(devspec)),
		(unsigned int)lsb->st_size,(unsigned int)lsb->st_ino, path);
	TraceString(trBuf);
	return n;
}

static void printDir(char *path, char *cmd, char *pid, const char *dirspec)
{
	struct stat sb, lsb;
	struct dirent dentry;
	int n;

	(void) memset(&sb, 0, sizeof(sb));
	(void) memset(&lsb, 0, sizeof(lsb));
	(void) memset(&dentry, 0, sizeof(dentry));
	(void) snprintf(dentry.d_name,(int)sizeof(dentry.d_name), dirspec);
	n = readlink(path, path, PATH_MAX );
	path[n] = 0;
	(void) lstat(path, &lsb);
	(void) printEntry(cmd, pid, &dentry, &sb, &lsb, path);
}

static int listOpenFiles(struct dirent *dproc, tBool flag)
{
	DIR *dp;
	int n;
	char cmd[MAX_PNAMELEN];
	struct dirent dentry, *dresult;
	struct stat sb, linksb;
	char path[PATH_MAX];
	(void) flag;  /* for later use: all, no kthreads etc. */

	snprintf(path, sizeof(path), "%s/%s/status", PROC_PREFIX, dproc->d_name);
	(void) fetchProcname(path, cmd, MAX_PNAMELEN);

	/* print working- and root-dir, they will also prevent unmount */
	snprintf(path, sizeof(path), "%s/%s/cwd", PROC_PREFIX, dproc->d_name);
	printDir(path, cmd, dproc->d_name, "cwd");
	snprintf(path, sizeof(path), "%s/%s/root", PROC_PREFIX, dproc->d_name);
	printDir(path, cmd, dproc->d_name, "rtd");

	snprintf(path, sizeof(path), "%s/%s/fd", PROC_PREFIX, dproc->d_name);
	dp = opendir(path);

	if (dp) {
		do {
			if ( !readdir_r(dp, &dentry, &dresult) && dresult) {

				(void) snprintf(path, sizeof(path), "%s/%s/fd/%s", PROC_PREFIX, dproc->d_name, dentry.d_name);
				if (!stat(path, &sb) && !S_ISDIR(sb.st_mode) ) {

					n = readlink(path, path, sizeof(path) );
                    if(n >= PATH_MAX)n = PATH_MAX-1;
					path[n] = 0;
					(void) memset(&linksb, 0, sizeof(linksb));
					(void) lstat(path, &linksb);
					(void) printEntry(cmd, dproc->d_name, &dentry, &sb, &linksb, path);
				}
			}
		} while(dresult);
		closedir(dp);
	}
	return 0;
}

void vTraceOpenFiles(tBool flag)
{
	DIR *dp;
	struct dirent dentry, *dresult;

	dp = opendir(PROC_PREFIX);
	if (dp) {
		do {
			if ( !readdir_r(dp, &dentry, &dresult) && dresult) {
				if (isProcessDir(&dentry)) {
					listOpenFiles(&dentry, flag);
				}
			}
		} while(dresult);
	   closedir(dp);
	} else {
		TraceString("cannot opendir(/proc): not mounted?");
	}
}
