/******************************************************************************
| FILE:         osaltime.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Timer-Functions.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/* Linux API */
#include "Linux_osal.h"

#include "ostrace.h"

#ifdef SET_ERRMEM_TIME
#include "adit-components/errmem_lib.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif 

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

//#define TIMERDEBUG
//#define LINUX_C_MINIMUM_INTERVAL                 1     /*in milliseconds */
#define LINUX_C_U32_TIMER_ID                      ((tU32)0x54494D45)

#define TIM_MSEC_PER_SEC      1000
#define TIM_SEC_PER_MIN       60
#define TIM_MIN_PER_HR        60
#define TIM_HR_PER_DAY        24
#define TIM_DAYS_PER_WEEK     7
#define TIM_DAYS_YEAR         365
#define TIM_MONTH_PER_YEAR    12

#define TIM_MSEC_PER_DAY      (tU64)(24*60*60*1000)
#define TIM_LEAP_YEAR_PACK    (365+365+365+366)

#define TIM_OSAL_YEAR_BASE    1900
#define TIM_SYS_YEAR_BASE     1985



/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/*
 * callback threads are waiting until an entry is put into the array
 */
struct callback
{
   pthread_cond_t    cond;
   pthread_mutex_t   mutex;
   tU32              numReady;
   tU32              lastIndex;
   trTimerElement*   osalTimer[5];
};

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static struct callback cbHandler;
static pthread_once_t timer_is_initialized = PTHREAD_ONCE_INIT;
static pthread_once_t SecTimer_is_initialized = PTHREAD_ONCE_INIT;
static trTimerElement*  pTimerArray[TIMER_STORAGE_SIZE];
static tU32 u32NextWriteEntry= 0;
static tU32 u32NextReadEntry = 0;
static tU32 u32PendingCallbacks = 0;
tU32 u32MaxCbHdr = 0;

tU32 u32LocalTimerSig = 0;
tS32 s32LocalTimerTid = -1;

enum{
  ENTER_DO,
  ENTER_DIS_ERROR,
  ENTER_STORE,
  ENTER_TIM_EXE,
//  ENTER_TIM2_EXE,
//  ENTER_RETRY,
  ENTER_CB_EXE
}eEntryType;

//#define CHECK_SEQ
#ifdef CHECK_SEQ
#define SEQ_MAX  200
tU32 Entry[SEQ_MAX];
uintptr_t Data[SEQ_MAX];
int NextEntry = 0;
tU32 EntrNr = 0;

void vEnterSeq(tU32 NewEntry,uintptr_t NewDat)
{
   Entry[NextEntry] = NewEntry;
   Data[NextEntry]  = NewDat;
   NextEntry++;
   if(NextEntry == SEQ_MAX)NextEntry=0;
   EntrNr++;
}

void vPrintSequenz(void)
{
  int i;
  if(EntrNr > SEQ_MAX)
  {
     TraceString("%d Entries of Sequenz lost",EntrNr-SEQ_MAX);
    EntrNr = SEQ_MAX;
  }
  for(i=0;i<SEQ_MAX;i++)
  {
    switch(Entry[i])
    {
    case ENTER_DO:
         TraceString("Enter do while with %d pending callbacks",Data[i]);
        break;
    case ENTER_DIS_ERROR:
         TraceString("Check if Tim task can execute %d pending callbacks",Data[i]);
        break;
    case ENTER_STORE:
         TraceString("Element 0x%x stored in intermediate storage",Data[i]);
        break;
    case ENTER_TIM_EXE:
         TraceString("Tim task executes callback for 0x%x",Data[i]);
        break;
    case ENTER_TIM2_EXE:
         TraceString("Tim task executes second callback for 0x%x",Data[i]);
        break;
    case ENTER_RETRY:
         TraceString("Tim task initiates retry for 0x%x",Data[i]);
        break;
    case ENTER_CB_EXE:
         TraceString("CB HDR task executes callback for 0x%x",Data[i]);
        break;
    default:
         TraceString("Unknown Entry");
        break;
    }
  }
}
#else
void vEnterSeq(tU32 Entry,tU32 NewDat){((void)Entry);((void)NewDat);}
void vPrintSequenz(void){}
#endif

OSAL_trTimeDate rSysTimeBase  = {0,0,0,1,1,85,0,0,0};
tU8 aDaysOfMonth[13]= {0,31,28,31,30,31,30,31,31,30,31,30,31}; /*0 appended as first element as month can start from 1*/
tU8 aDaysOfMonthLeapYr[13]= {0,31,29,31,30,31,30,31,31,30,31,30,31};

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
tBool bNativeTimeHldr         = TRUE;
tU64 u64SetTimeUTCElapsed     = 0;
tU64 u64SetTimeSystemElapsed  = 0;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static trTimerElement* tTimerTableGetFreeEntry(tVoid);
static trTimerElement* tTimerCheckHandle(OSAL_tTimerHandle handle);

tS32 s32TimerTableCreate(tVoid);
void vCleanUpTimeofContext(void);


static tS32 s32CalculateLeapyrDays(OSAL_trTimeDate* pcorSetTime);
static tBool bTimeTillStartYr(OSAL_trTimeDate* pcorSetTime,tU64* u64Result);
static tBool bTimeFromStartYr(OSAL_trTimeDate* pcorSetTime,tU64* u64Result);
static tBool bNotLessthan(OSAL_trTimeDate* rOperand1,OSAL_trTimeDate* rOperand2 );
static tBool bConvertmSecsToDate(tU64 mseconds,OSAL_trTimeDate* prCurrTimeDate);
static tBool bConvertDateTomSecs(OSAL_trTimeDate* prCurrTimeDate, tU64* mseconds);
static void TimerCallBack(void *arg);

/****************************************************************************
*                                                                           *
* FUNCTIONS                                                                 *
                    bConvertmSecsToDate                                     *
*                                                                           *
* DESCRIPTION                                                               *
*      Converts the Milliseconds value to corresponding date                *

* INPUTS                                                                    *
* mseconds                                                                  *

* OUTPUTS                                                                   *
*                                                                           *
* tBool: FALSE- Error
         TRUE - Success                                                     *
*                                                                           *
****************************************************************************/
static tBool bConvertmSecsToDate(tU64 mseconds,OSAL_trTimeDate* prCurrTimeDate)
{
   tBool bRetval = TRUE;
   tBool bDaysltYear = FALSE;
   tBool bDaysltMonth = FALSE;
   tU32 u32CurrSecond=0;
   tU32 u32CurrMinute=0;
   tU32 u32CurrHour=0;
   tU32 u32CurrDay=0;
   tU32 u32CurrMonth=1;
   tU32 u32CurrYear=0;
   tU32 u32CurrWeekday=0;
   tU32 u32CurrYearday=0;
   tU32 u32CurrYearTemp=0;
   tU32 u32elapsedSeconds=0;
   tU32 u32elapsedMinutes=0;
   tU32 u32elapsedHours=0;
   tU32 u32elapsedDays=0;
   tU32 u32QuadYrs=0;
   tU32 u32extraDays=0;
   tU32 u32i;

   if (prCurrTimeDate != OSAL_NULL)
   {
      /*Extract the time information from the elapsed Time */
      u32elapsedSeconds   = (tU32)(mseconds / TIM_MSEC_PER_SEC);
      u32CurrSecond      = u32elapsedSeconds   % TIM_SEC_PER_MIN; /*SS of Time stamp*/
      u32elapsedMinutes    = u32elapsedSeconds   / TIM_SEC_PER_MIN;
      u32CurrMinute      = u32elapsedMinutes   % TIM_MIN_PER_HR;  /*MM of Time stamp*/
      u32elapsedHours      = u32elapsedMinutes   / TIM_MIN_PER_HR;
      u32CurrHour       = u32elapsedHours    % TIM_HR_PER_DAY;  /*HH of Time stamp*/
      u32elapsedDays      = u32elapsedHours    / TIM_HR_PER_DAY;
      u32CurrWeekday    = ((u32elapsedDays % TIM_DAYS_PER_WEEK)+1)%7; /*Day Starts with Mon (1) on 1-1-1900*/

      /*Stripout the QuadYear packs*/
      u32extraDays    = u32elapsedDays%TIM_LEAP_YEAR_PACK;
      u32QuadYrs       = (u32elapsedDays/TIM_LEAP_YEAR_PACK)*4;    
      /*Get the actual Year to check whether it is leapyear or not*/
      u32CurrYearTemp = u32QuadYrs + TIM_SYS_YEAR_BASE;

      while( bDaysltYear != TRUE )
      {
         if(((u32CurrYearTemp%4) == 0) &&( ((u32CurrYearTemp%100) != 0) ||((u32CurrYearTemp%400) == 0)) )
         {
            if ( u32extraDays >=(TIM_DAYS_YEAR+1))
            {
               u32extraDays -= TIM_DAYS_YEAR+1;
               u32CurrYearTemp++;
            }
            else
            {
               bDaysltYear = TRUE;
            }
         }
         else
         {
            if ( u32extraDays >=(TIM_DAYS_YEAR))
            {
               u32extraDays -= TIM_DAYS_YEAR;
               u32CurrYearTemp++;
            }
            else
            {
               bDaysltYear = TRUE;
            }
         }
      }

      u32CurrYear = u32CurrYearTemp - TIM_OSAL_YEAR_BASE; /*YYYY of Time Stamp*/
      u32CurrYearday = u32extraDays+1; /*Got u32Yearday*/

       /*Calculating Month of the current year and Day of Last(current) Month*/
      if(((u32CurrYear%4) == 0 ) &&(((u32CurrYear%100) != 0)||((u32CurrYear%400) == 0 )))
      {
         for(u32i=1;(u32i<=TIM_MONTH_PER_YEAR)&&(bDaysltMonth == FALSE);u32i++)
         {
           if(u32extraDays >= aDaysOfMonthLeapYr[u32i])
           {
              u32extraDays = u32extraDays - aDaysOfMonthLeapYr[u32i];
              u32CurrMonth++;
           }
           else
           {
              bDaysltMonth = TRUE;
           }
         }

      }
      else
      {
         for(u32i=1;(u32i<=TIM_MONTH_PER_YEAR)&&(bDaysltMonth == FALSE);u32i++)
         {
           if(u32extraDays >= aDaysOfMonth[u32i])
           {
              u32extraDays = u32extraDays - aDaysOfMonth[u32i];
              u32CurrMonth++;
           }
           else
           {
              bDaysltMonth = TRUE;
           }
         }
      }
      u32CurrDay = u32extraDays+1;
      prCurrTimeDate->s32Second    = (tS32)u32CurrSecond;
      prCurrTimeDate->s32Minute   = (tS32)u32CurrMinute;
      prCurrTimeDate->s32Hour    = (tS32)u32CurrHour;
      prCurrTimeDate->s32Day      = (tS32)u32CurrDay;
      prCurrTimeDate->s32Month   = (tS32)u32CurrMonth;
      prCurrTimeDate->s32Year    = (tS32)u32CurrYear;
      prCurrTimeDate->s32Weekday   = (tS32)u32CurrWeekday;
      prCurrTimeDate->s32Yearday   = (tS32)u32CurrYearday;
      prCurrTimeDate->s32Daylightsaving   = 0; /*TODO:Calculate based on the Variant and Time Zone correspondingly*/
	                                           /* 0: no DST, 1: DST, -1: information not available */
#ifdef TIMERDEBUG
     TraceString("M2D Years = %d , Months = %d, Days = %d, Hours = %d, Minutes = %d, Seconds = %d",
                 prCurrTimeDate->s32Year,prCurrTimeDate->s32Month,prCurrTimeDate->s32Day,
                 prCurrTimeDate->s32Hour,prCurrTimeDate->s32Minute,prCurrTimeDate->s32Second);
#endif

   }
   else
   {
         bRetval=FALSE;
   }
   return bRetval;

}


/****************************************************************************
*                                                                           *
* FUNCTIONS                                                                 *
                    bConvertDateTomSecs                                     *
*                                                                           *
* DESCRIPTION                                                               *
*      Calulate the total elaspsed milliseconds till date                   *

* INPUTS                                                                    *
* mseconds                                                                  *

* OUTPUTS                                                                   *
*                                                                           *
* tBool: FALSE- Error
         TRUE - Success                                                     *
*                                                                           *
****************************************************************************/
static tBool bConvertDateTomSecs( OSAL_trTimeDate* prCurrTimeDate, tU64* pu64mseconds)
{
	tBool bResult = TRUE;
    tU64 u64msecTillStartYr = 0;
    tU64 u64msecFromStartYr = 0;
    tU64 u64elapsedmsec = 0;
    if( TRUE == bTimeTillStartYr(prCurrTimeDate,&u64msecTillStartYr))
    {
#ifdef TIMERDEBUG
       TraceString("LI:bTimeTillStartYr Success with %llu ms",u64msecTillStartYr );
#endif            
       if( TRUE == bTimeFromStartYr(prCurrTimeDate,&u64msecFromStartYr))
       {
#ifdef TIMERDEBUG
          TraceString("LI:bTimeFromStartYr Success with %llu ms",u64msecFromStartYr );
#endif
          u64elapsedmsec = u64msecTillStartYr + u64msecFromStartYr;
          *pu64mseconds = u64elapsedmsec;

#ifdef TIMERDEBUG
          TraceString("LI:SetTime Elapsed MilliSeconds = %llu ms",u64elapsedmsec);
#endif
       }
       else
       {
          bResult = FALSE;
#ifdef TIMERDEBUG                
          TraceString("LI:bTimeFromStartYr Falied");
#endif                
       }
    }
    else
    {
       bResult = FALSE;
#ifdef TIMERDEBUG            
       TraceString("LI:bTimeTillStartYr Failed");
#endif            
    }

  return bResult;

}



/****************************************************************************
*                                                                           *
* FUNCTIONS                                                                 *
  bNotLessthan                                                               *
*                                                                           *
* DESCRIPTION                                                               *
*      Compares the value of the two OSAL time structures passed            *
*      True if rOperand1 > rOperand2                                        *

* INPUTS                                                                    *
*                                                                           *
*       OSAL_trTimeDate* - rOperand1,rOperand2                              *

* OUTPUTS                                                                   *
*                                                                           *
*      tBool: FALSE- Error
              TRUE - Success                                                *
*                                                                           *
****************************************************************************/
static tBool bNotLessthan(OSAL_trTimeDate* rOperand1,OSAL_trTimeDate* rOperand2 )
{
   tBool bDiffFlag = TRUE;
   if(rOperand1->s32Year < rOperand2->s32Year)
   {
      bDiffFlag = FALSE;
   }
   else if(rOperand1->s32Year == rOperand2->s32Year)
   {
      if(rOperand1->s32Month < rOperand2->s32Month)
      {
         bDiffFlag = FALSE;
      }
      else if(rOperand1->s32Month ==  rOperand2->s32Month)
      {
         if( rOperand1->s32Day < rOperand2->s32Day )
         {
            bDiffFlag = FALSE;
         }
         else if( rOperand1->s32Day == rOperand2->s32Day )
         {
            if(rOperand1->s32Hour < rOperand2->s32Hour)
            {
               bDiffFlag = FALSE;
            }
            else if(rOperand1->s32Hour == rOperand2->s32Hour)
            {
               if(rOperand1->s32Minute < rOperand1->s32Minute)
               {
                  bDiffFlag = FALSE;
               }
               else if(rOperand1->s32Minute == rOperand2->s32Minute)
               {
                  if(rOperand1->s32Second < rOperand2->s32Second)
                  {
                     bDiffFlag = FALSE;
                  }
               }
            }
         }
      }
   }
return(bDiffFlag);
}

/************************************************************************
* FUNCTION                                                              *
  s32CalculateLeapyrDays
*                                                                       *
* DESCRIPTION                                                           *
*      Find the no of leap years from the Default system time to
       the set time                                                     *
* INPUTS                                                                *
*      rSetGpsTime,Set time in OSAL time format                         *
* OUTPUTS                                                               *
*      NO of leap years
************************************************************************/
static tS32 s32CalculateLeapyrDays(OSAL_trTimeDate* pcorSetTime)
{
   tU16 u16SysTimYr =(tU16)(TIM_OSAL_YEAR_BASE+rSysTimeBase.s32Year);
   tU16 u16SetTimYr = (tU16)(TIM_OSAL_YEAR_BASE+pcorSetTime->s32Year);
   tS32 s32NoLeapYrs = 0;

   if( u16SetTimYr < u16SysTimYr)
   {
      s32NoLeapYrs = -1;
   }
   else
   {
 /*Check for leap year only if it is not the system time year*/
      if( u16SetTimYr > u16SysTimYr )
      {
         while( u16SysTimYr < u16SetTimYr)
            {
                /*Check for leap year*/
               if(((u16SysTimYr%4) == 0) &&( ((u16SysTimYr%100) != 0) ||
                 ((u16SysTimYr%400) == 0)) )
                 {
                     s32NoLeapYrs++;
                 }
                 u16SysTimYr++;
            }
      }
       }
    return(s32NoLeapYrs);
}

/******************************************************************************
* FUNCTION                                                                    *
  bTimeTillStartYr
*                                                                             *
* DESCRIPTION                                                                 *
*      Calculate the offset time between the start year of the set time
       and the default system year.
       eg:Set time 10:15:00 , 20.10.2010
       Here the offset between  Jan1,2010 and Jan1, 1985(the default
       system time year for TEngine)is computed.
       Here the system time is  assumed to be started from Jan1.The function
       also can be reused for other default years apart from 1985 but the day
       has to be Jan1.                                                        *
* INPUTS                                                                      *
*      pcorSetTime - Set time in OSAL time format                             *
* OUTPUTS                                                                     *
*      u64Result - Offset Time in ms
       tBool - TRUE,if calculation is success else FALSE
*******************************************************************************/
static tBool bTimeTillStartYr(OSAL_trTimeDate* pcorSetTime,tU64* u64Result)
{
   tS32 s32NoLeapYrDays = 0;
   tU32 u32NoDays = 0;
   tBool bRetVal = TRUE;

   if( FALSE == bNotLessthan(pcorSetTime,&rSysTimeBase) )
   {
      bRetVal = FALSE;
   }
   else
   {
      u32NoDays = (tU32)((pcorSetTime->s32Year - rSysTimeBase.s32Year)*TIM_DAYS_YEAR);
      s32NoLeapYrDays = s32CalculateLeapyrDays(pcorSetTime);
      
      if( s32NoLeapYrDays < 0 )
      {
         bRetVal = FALSE;
      }
      else
      {
         u32NoDays += (tU32)s32NoLeapYrDays;
         *u64Result = ((tU64)u32NoDays)*TIM_MSEC_PER_DAY;
      }
   }
    return bRetVal;
}

/******************************************************************************
* FUNCTION                                                                    *
  TimeFromStartYr
*                                                                             *
* DESCRIPTION                                                                 *
*      Calculate the offset time between the set date/Time
       and Jan1,of the set date/time
       eg:Set time 10:15:00 , 20.10.2010
       Here the offset between  10:15:00,20.10.2010 and
       Jan1,2010 is computed.

* INPUTS                                                                      *
*      pcorSetTime - Set time in OSAL time format                             *
* OUTPUTS                                                                     *
*      u64Result - Offset Time in ms
       tBool - TRUE,if calculation is success else FALSE
*******************************************************************************/
static tBool bTimeFromStartYr(OSAL_trTimeDate* pcorSetTime,tU64* u64Result)
{
   tU8 u8idx = 0;
   tU8* pau8DaysOfMonth = aDaysOfMonth;
   tBool bRetVal = TRUE;
   tU16 u16Days = 0;

   if(FALSE == bNotLessthan(pcorSetTime,&rSysTimeBase))
   {
      bRetVal = FALSE;
   }
   else
   {
      if((((pcorSetTime->s32Year+TIM_OSAL_YEAR_BASE) % 4) == 0) &&
        ((((pcorSetTime->s32Year+TIM_OSAL_YEAR_BASE) % 100) != 0)||
        (((pcorSetTime->s32Year+TIM_OSAL_YEAR_BASE) % 400) == 0)) )
         {
            pau8DaysOfMonth = aDaysOfMonthLeapYr;
         }

/*Reminder and borrow logic is not done as default system time 00:00:00,01.01 */
         *u64Result  = (tU64)((tU32)(pcorSetTime->s32Second - rSysTimeBase.s32Second)*(tU64)(TIM_MSEC_PER_SEC));
         *u64Result += (tU64)((tU32)(pcorSetTime->s32Minute - rSysTimeBase.s32Minute)*(tU64)(TIM_MSEC_PER_SEC*TIM_SEC_PER_MIN));
         *u64Result += (tU64)((tU32)(pcorSetTime->s32Hour   - rSysTimeBase.s32Hour)  *(tU64)(TIM_MSEC_PER_SEC*TIM_SEC_PER_MIN*TIM_MIN_PER_HR));

         u16Days =(tU16)(pcorSetTime->s32Day - rSysTimeBase.s32Day);
         for( u8idx = 0;u8idx < pcorSetTime->s32Month ;u8idx++)
         {
            u16Days += *(pau8DaysOfMonth + u8idx);
         }
         
         *u64Result += ((tU64)u16Days)*TIM_MSEC_PER_DAY;
         
   }
return( bRetVal );
}


/******************************************************************************
 * FUNCTION:      CallBackExecute
 *
 * DESCRIPTION:   
 *
 * PARAMETER:
 *                trTimerElement*  OSAL timer element pointer
 *               
 *
 * RETURNVALUE:   none 
 *
 *
 * HISTORY:
 * Date       |   Modification                         | Authors
 * 04.12.02 ??|   Initial revision                     | 
 * 04.11.02   |   prepare for "auto deletion"          | kos2hi
 * 
 ******************************************************************************/
void CallBackExecute(const trTimerElement* pCurrentEntry)
{
#ifdef MEASURE_CALLBACKS
   OSAL_tMSecond rtimes[2];
   rtimes[0] = OSAL_ClockGetElapsedTime();
#endif
   pCurrentEntry->pfOsalCallback(pCurrentEntry->pvArgOfOsalCallback);
#ifdef MEASURE_CALLBACKS
   tS32 TestErrorcode;
   tU16 u16Pid = (tU16)getpid();
   rtimes[1] = OSAL_ClockGetElapsedTime();
   if((TestErrorcode = rtimes[1] - rtimes[0]) > 20)
   {
      u16Pid = OSAL_ProcessWhoAmI();
#ifdef SHORT_TRACE_OUTPUT
      char BufferTim[20];
      OSAL_M_INSERT_T8(&BufferTim[0],OSAL_ERROR_TIMER_DURATION);
      OSAL_M_INSERT_T32(&BufferTim[1],(tU32)pCurrentEntry->hTimer);
      OSAL_M_INSERT_T16(&BufferTim[5],u16Pid);
      OSAL_M_INSERT_T32(&BufferTim[7],(tU32)TestErrorcode);
      OSAL_M_INSERT_T32(&BufferTim[11],(tU32)pCurrentEntry->idTask);
      OSAL_M_INSERT_T32(&BufferTim[15],(tU32)pCurrentEntry->pfOsalCallback);
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_ERROR,BufferTim,19);
#else
      char au8Buf[251]={0}; 
      au8Buf[0] = OSAL_STRING_OUT;
#ifdef NO_PRIxPTR
      snprintf(&au8Buf[1],250,"!!! Callback Duration overflow PID:%d Time:%d ms Timer ID:%x Task:%d Callback Adress:0x%x",
              (unsigned int)u16Pid,TestErrorcode,(uintptr_t)pCurrentEntry->hTimer,pCurrentEntry->idTask,
              (uintptr_t)pCurrentEntry->pfOsalCallback);
#else
      snprintf(&au8Buf[1],250,"!!! Callback Duration overflow PID:%d Time:%d ms Timer ID:%" PRIxPTR " Task:%d Callback Adress:0x%" PRIxPTR "",
              (unsigned int)u16Pid,TestErrorcode,(uintptr_t)pCurrentEntry->hTimer,pCurrentEntry->idTask,
              (uintptr_t)pCurrentEntry->pfOsalCallback);
#endif
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_ERROR,&au8Buf[1],strlen(&au8Buf[1])+1);
#endif
   }
#endif
}
/*
 * getCallback returns the next timers entry for callback execution
 * called with locked cbHandler.mutex
 */
static trTimerElement* getCallBack(void)
{
   trTimerElement* pElement = NULL;
   tU32 i = 0;
   tU32 checked;
   /*
    * cannot rely on that lastIndex is always valid: search for entry
    * think of: several timers expire and their callbacks where put
    * and then the cb threads are scheduled
    */
   if(u32MaxCbHdr != 1)
   {
      i = cbHandler.lastIndex; 
      for (checked = 0; (cbHandler.osalTimer[i] == 0)
       && (checked < u32MaxCbHdr) ; checked++)
      {
         i ++;
         if ( (i >= u32MaxCbHdr))
         i = 0;
      }
   }

   if (cbHandler.osalTimer[i] != NULL)
   {
      pElement = cbHandler.osalTimer[i];
      cbHandler.osalTimer[i] = NULL;
      cbHandler.numReady--;
   }

   return pElement;
}

/*
 * putCallBack stores the pElement and signals cbHandler.cond
 * locks cbHandler.mutex itself
 */
static tU32 putCallBack(trTimerElement* pElement)
{
   tU32 i = 0;
   tU32 checked;
   pthread_mutex_lock(&cbHandler.mutex);
   
   if(u32MaxCbHdr > 1)
   {
      i = cbHandler.lastIndex;
      for (checked = 0; (cbHandler.osalTimer[i] != 0)
       && (checked < u32MaxCbHdr) ; checked++)
      {
         i ++;
         if ( (i >= u32MaxCbHdr))
         i = 0;
      }
   }

   if (NULL == cbHandler.osalTimer[i])
   {
      cbHandler.osalTimer[i] = pElement;
      cbHandler.numReady++;
      cbHandler.lastIndex = i;
      pthread_cond_signal(&cbHandler.cond);
   }
   else
   { 
       i = u32MaxCbHdr;
   }
   
   pthread_mutex_unlock(&cbHandler.mutex);
   return i;
}

void vPrepareCallbackExecution(trTimerElement* pElement)
{
   /* check if timer isn't already switched off */
   if(pElement->bIsSetting == TRUE)
   {
      if((pElement->bTraceOn)||(u32OsalSTrace & 0x00000008))
      {  
          TraceString("OSALTIM PID:%d Timer CallBackExecute element 0x%x TMO:%d PER:%d",
                      pElement->Pid,pElement,pElement->u32Timeout,pElement->u32Interval);
      }
      CallBackExecute(pElement);
   }
   else
   {
      TraceString("OSALTIM PID:%d Timer CallBackExecute element 0x%x TMO:%d PER:%d is already stopped",
                  pElement->Pid,pElement,pElement->u32Timeout,pElement->u32Interval);
   }
}

/*
 * we have 1..u32MaxCbHdr TimerCallBack-Threads
 * each is waiting for a timerEvent that puts a trTimerElement into
 * the array cbHandler.osalTimer[] and signals the condition
 */
static void TimerCallBack(void *arg)
{
   trTimerElement* pElement = NULL;
   tU32 s32Ref = 0;
   (void) arg;
   for (;;)
   {
      pthread_mutex_lock(&cbHandler.mutex);
      while (s32Ref >= cbHandler.numReady)
      {
         pthread_cond_wait(&cbHandler.cond, &cbHandler.mutex);
      }

      pElement = getCallBack();
      pthread_mutex_unlock(&cbHandler.mutex);

      if (pElement && pElement->u32TimerID == LINUX_C_U32_TIMER_ID)
      {
          vEnterSeq(ENTER_CB_EXE,(uintptr_t)pElement);
          vPrepareCallbackExecution(pElement);
      }
   }
}


tBool bCheckPending( int sig) 
{
    sigset_t timersigset;
    tBool bRet = FALSE;
    if( sigpending( &timersigset ) == 0 )
    {
       if( sigismember( &timersigset, sig ) )
       {
             bRet = TRUE;
       }
       else
       {
#ifdef SIGNAL_TRACE
        TraceString("OSAL Time no pending signal");
#endif
       }
    }
    return bRet;
}


void vExecutePendingCallbacks(void)
{
   if(u32PendingCallbacks)
   {
      while(1)
      {
         if(pTimerArray[u32NextReadEntry])
         {
            vEnterSeq(ENTER_TIM_EXE,(uintptr_t)pTimerArray[u32NextReadEntry]);
            vPrepareCallbackExecution(pTimerArray[u32NextReadEntry]);
            pTimerArray[u32NextReadEntry] = NULL;
            u32NextReadEntry++;
            /* check for turn around _*/
            if(u32NextReadEntry == TIMER_STORAGE_SIZE)u32NextReadEntry = 0;
            u32PendingCallbacks--;
            if(u32PendingCallbacks == 0)break;
         }
         else
         {
            TRACE_LINE_INFO;
            break;
         }
      }//while(1)
   }//if(u32PendingCallbacks)
}

/*
 * TimerThread sits in an endless loop waiting for signals from
 * expired timers. The trTimerElement is pushed into an array
 * where "callback" threads wait on for calling the timer callbacks.
 */
static void TimerThread(void *arg)
{
   sigset_t set;
   siginfo_t info;
   int sig;
   tS32 s32Ret;
#ifdef SIGNAL_TRACE_TIMER
   union sigval value;
   char szName[80];
   struct timespec timeout;
#endif
   (void) arg;

   /* check for current process if libunwind can be used */   
   vCheckLibUnwind();

   sigemptyset(&set);
   sigaddset(&set, pOsalData->u32TimSignal);
   sigaddset(&set, pOsalData->u32TimSignal-1);
   sigaddset(&set, pOsalData->u32TimSignal-2);

   for(s32Ret = 0;s32Ret < TIMER_STORAGE_SIZE;s32Ret++)
   {
       pTimerArray[s32Ret] = NULL;
   }
#ifdef TIMEOUT_USED
   if(bCheckPending(pOsalData->u32TimSignal))
   {
      TraceString("OSAL Time OSAL_TIMER_SIGNAL is pending");
   }
   timeout.tv_sec  = 10;
   timeout.tv_nsec = 0;
#endif

   s32LocalTimerTid = OSAL_ThreadWhoAmI();

   while (1) 
   {
#ifdef TIMEOUT_USED
      sig = sigtimedwait(&set, &info,&timeout);
      if(sig == -1)
      {
         if(errno == EAGAIN)
         {
            TraceString("OSAL Time timeout");
         }
         else if(errno == EINTR)
         {
            TraceString("OSAL Time wrong signal caught");
         }
      }
#else
      sig = sigwaitinfo(&set, &info);
#endif

      if (pOsalData->u32TimSignal == (tU32)sig)
      {
         trTimerElement* pElement = (trTimerElement*)info.si_ptr;
         if (pElement && pElement->u32TimerID == LINUX_C_U32_TIMER_ID)
         {
            if((pElement->bTraceOn)||(u32OsalSTrace & 0x00000008))
            {
               TraceString("OSALTIM PID:%d Timer sigwaitinfo rec element 0x%x TMO:%d PER:%d",
                           pElement->Pid,pElement,pElement->u32Timeout,pElement->u32Interval);
            }
            info.si_ptr = NULL;

            vEnterSeq(ENTER_DO,u32PendingCallbacks);

            if(u32MaxCbHdr == 0)
            {
               s32Ret = u32MaxCbHdr;
            }
            else
            {
               s32Ret = putCallBack(pElement);
               pthread_yield();
            }
            if((s32Ret == (tS32)u32MaxCbHdr) || u32PendingCallbacks)
            {
               vEnterSeq(ENTER_DIS_ERROR,u32PendingCallbacks);
               /* check if element has to be stored */
               if(s32Ret == (tS32)u32MaxCbHdr)
               {
                  /* check if further timer signal are pending */
                  if(bCheckPending(pOsalData->u32TimSignal))
                  {
                     /* check for space in intermediate storage */
                     if(u32PendingCallbacks == TIMER_STORAGE_SIZE)
                     {
                        /* no further space available we have to do the callback here */
                        NORMAL_M_ASSERT_ALWAYS(); 
                        vExecutePendingCallbacks();
                        vPrepareCallbackExecution(pElement);
                     }
                     else
                     {
                        vEnterSeq(ENTER_STORE,(uintptr_t)pElement);
                        /* store element in intermediate storage */
                        pTimerArray[u32NextWriteEntry] = pElement;
                        u32NextWriteEntry++;
                        u32PendingCallbacks++;
                        /* check for turn around _*/
                        if(u32NextWriteEntry == TIMER_STORAGE_SIZE)u32NextWriteEntry = 0;
                     }                       
                  }
                  else//if(bCheckPending(pOsalData->u32TimSignal))
                  {
                     /* all callback threads occupied with callback and no pending signal
                       so execute callback here  */
                     vExecutePendingCallbacks();
                     vEnterSeq(ENTER_TIM_EXE,(uintptr_t)pElement);
                     vPrepareCallbackExecution(pElement);
                  }
               }// if(s32Ret == u32MaxCbHdr)
            }// if((s32Ret == u32MaxCbHdr) || u32PendingCallbacks)
         }
         else
         {
           TraceString("OSAL Time sigwaitinfo received signal without data");
         }
      }
      else if((pOsalData->u32TimSignal-1)== (tU32)sig)
      {
         vCleanUpTimeofContext();
         break;
      }
      else if((pOsalData->u32TimSignal-2)== (tU32)sig)
      {
         tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
         if(s32PidEntry == OSAL_ERROR)
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
         else
         {
            s32StartCbHdrTask(s32PidEntry);
         }
      }
      else
      {
#ifndef TIMEOUT_USED
         if(sig == -1)
         {
             sig = errno;
             switch(sig)
             {
             case ENOSYS:
                  /* The functions sigwaitinfo() and sigtimedwait() are not supported 
                     by this implementation. */
                 TraceString("OSAL Time PID:%d sigwaitinfo failed error %s",getpid(),"ENOSYS");
                 break;
             case EAGAIN:
                  /* No signal specified by set was generated within the specified timeout period. */
                  TraceString("OSAL Time PID:%d sigwaitinfo failed error %s",getpid(),"EAGAIN");
                 break;
             case EINTR:
                  /* The wait was interrupted by an unblocked, caught signal. It will 
                     be documented in system documentation whether this error will cause 
                     these functions to fail. */
                   TraceString("OSAL Time PID:%d sigwaitinfo failed error %s",getpid(),"EINTR");
                break;
             case EINVAL:
                 /* The timeout argument specified a tv_nsec value less than zero or greater 
                    than or equal to 1000 million. */
                  TraceString("OSAL Time PID:%d sigwaitinfo failed error %s",getpid(),"EINVAL");
                 break;
             default:
                  TraceString("OSAL Time PID:%d sigwaitinfo failed error %s",getpid(),"???");
                 break;
             }
         }
         else
         {
            TraceString("OSAL Time sigwaitinfo receives wrong signal %d",sig);
         }
#endif
      }
   }// end while
}


/* function is called by application task 
   mask has to be set in timer task itself */
static void setupTimerThread(void)
{
   tS32 t;
   OSAL_trThreadAttribute th_attr;
   tChar  tName[OSAL_C_U32_MAX_NAMELENGTH];
   cbHandler.numReady = 0;
   cbHandler.lastIndex = 0;

   if(u32PrcExitStep == 0)
   {
   if(LockOsal(&pOsalData->TimerTable.rLock) == OSAL_OK)
   {
      pthread_cond_init(&cbHandler.cond, NULL);
      pthread_mutex_init(&cbHandler.mutex, NULL);

      th_attr.u32Priority = OSAL_TIMER_THREAD_PRIORITY;
      th_attr.s32StackSize = 64*1024;//minimum stack size
      th_attr.pvArg = NULL;
      th_attr.szName = tName;

      if(u32MaxCbHdr != 0)
      {
         th_attr.pfEntry = TimerCallBack;
         for (t=0; t<(tS32)u32MaxCbHdr; t++) 
         {
            cbHandler.osalTimer[t] = NULL;
            snprintf(tName, sizeof(tName), "%dCB%d",getpid(), t);
            if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
            {
               NORMAL_M_ASSERT_ALWAYS();
               snprintf(tName, sizeof(tName), "Error:%d",(unsigned int)OSAL_u32ErrorCode());
               TraceString(tName);
            }
         }
      }

      th_attr.pfEntry = TimerThread;
      snprintf(tName, sizeof(tName), "Timer-%d",getpid());
      th_attr.szName = tName;
      if (OSAL_ThreadSpawn(&th_attr) == OSAL_ERROR) 
      {
         NORMAL_M_ASSERT_ALWAYS();
         snprintf(tName, sizeof(tName), "Error:%d",(unsigned int)OSAL_u32ErrorCode());
         TraceString(tName);
      }
	  /* force dispatching to allow thread startup */
      OSAL_s32ThreadWait(1);
   }
   }
   UnLockOsal(&pOsalData->TimerTable.rLock);
}


/*****************************************************************************
 *
 * FUNCTION:    s32TimerTableCreate
 *
 * DESCRIPTION: This function creates the Timer Table List. If 
 *              there isn't space it returns a error code.                
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 09.01.02  |   Initial revision                     | 
 *****************************************************************************/
tS32 s32TimerTableCreate(tVoid)
{  
   tU32 tiIndex;

   pOsalData->TimerTable.u32UsedEntries   = 0;
   pOsalData->TimerTable.u32MaxEntries       = pOsalData->u32MaxNrTimerElements;

   for(tiIndex=0;  tiIndex <  pOsalData->TimerTable.u32MaxEntries ; tiIndex++)      
   {
      pTimerEl[tiIndex].u32TimerID = LINUX_C_U32_TIMER_ID;
      pTimerEl[tiIndex].bIsUsed = FALSE;
   }
   
   return OSAL_OK; 
}


/*****************************************************************************
 *
 * FUNCTION:    tTimerTableGetFreeEntry
 *
 * DESCRIPTION: this function goes through the TimerList and returns the
 *              first TimerElement.In case no entries are available a new 
 *              defined number of entries is attempted to be added. 
 *              In case of success the first new element is returned, otherwise
 *              a NULL pointer is returned. 
 *              
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trTimerElement*  
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 16.11.01  |   Initial revision                     | 
 * 09.01.02  |   First   release                      | 
 *****************************************************************************/
static  trTimerElement* tTimerTableGetFreeEntry(tVoid)
{
   trTimerElement *pCurrent  = pTimerEl;
   tU32 used = pOsalData->TimerTable.u32UsedEntries;

   if (used < pOsalData->TimerTable.u32MaxEntries)
   {
      pCurrent = &pTimerEl[used];
      pOsalData->TimerTable.u32UsedEntries++;
   }
   else
   {
        while ((pCurrent < &pTimerEl[used])
            && (pCurrent->bIsUsed == TRUE))
        {
            pCurrent++;
        }

        if(pCurrent >= &pTimerEl[used])
        {
           pCurrent = NULL;
        }
   }
   return pCurrent;
}

/*****************************************************************************
 *
 * FUNCTION:     tTimerCheckHandle
 *
 *
 * DESCRIPTION: this function checks if the handle seems to be valid
 *
 * PARAMETER:   handle (I)
 *                Timer handle wanted..
 *
 * RETURNVALUE: trTimerElement*  
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 16.11.01  |   Initial revision                     | 
 * 09.01.02  |   First   release                      | 
 *****************************************************************************/
static trTimerElement* tTimerCheckHandle(OSAL_tTimerHandle handle)
{
   trTimerElement *pCurrent = (trTimerElement*)handle;
   
   /* Check casted pointer */
   if (pCurrent && ((uintptr_t)pCurrent!=OSAL_C_INVALID_HANDLE))
   {
      /* check valid range for handle memory */
      if((pCurrent <= &pTimerEl[0])&&(pCurrent >= &pTimerEl[pOsalData->TimerTable.u32UsedEntries]))
      {
         if(pOsalData->bLogError)
         {
            vWritePrintfErrmem("OSALTIM memstart:0x%x memend:0x%x Handle:0x%x \n",&pTimerEl[0],&pTimerEl[pOsalData->TimerTable.u32UsedEntries],pCurrent);
         }
         TraceString("OSALTIM memstart:0x%x memend:0x%x Handle:0x%x", &pTimerEl[0],&pTimerEl[pOsalData->TimerTable.u32UsedEntries],pCurrent);
         pCurrent = OSAL_NULL;
      }
      else
      {
         if( ( pCurrent->u32TimerID != LINUX_C_U32_TIMER_ID) || 
             ( pCurrent->bIsUsed == FALSE) )
         {
            if(pOsalData->bLogError)
            {
                vWritePrintfErrmem("OSALTIM Invalid Handle:0x%x Used:%d \n", pCurrent,pCurrent->bIsUsed);
            }
            TraceString("OSALTIM Invalid Handle:0x%x Used:%d",  pCurrent,pCurrent->bIsUsed);
            pCurrent = OSAL_NULL;
         }
      }
   }
   return(pCurrent);
}


/*****************************************************************************
*
* FUNCTION:    bStopAllTimer
*
* DESCRIPTION: this function deletes the
*              Timer Element with the given context from list
*
* PARAMETER:
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vCleanUpTimeofContext(void)
{
   trTimerElement *pCurrent = &pTimerEl[0];
   tU32 used = pOsalData->TimerTable.u32UsedEntries;
   tS32 s32OwnPid = OSAL_ProcessWhoAmI();

   while (pCurrent < &pTimerEl[used])
   {
      if((pCurrent->hTimer)&&(pCurrent->Pid == s32OwnPid))
      {
        if(pCurrent->bIsSetting)
        {
           if(OSAL_s32TimerSetTime((OSAL_tSemHandle)pCurrent,0,0) != OSAL_OK)
           {
              TraceString("OSAL_s32TimerSetTime for 0x%x failed",pCurrent);
           }
           pCurrent->bIsSetting = FALSE;
        }
        if(OSAL_s32TimerDelete((OSAL_tTimerHandle)pCurrent) != OSAL_OK)
        {
           TraceString("OSAL_s32TimerDelete for 0x%x failed",pCurrent);
        }
      }
      pCurrent++;
   }
}

/*****************************************************************************
 *
 * FUNCTION:    OSAL_u32TimerGetResolution
 *
 * DESCRIPTION: This Function returns the time resolution of the timer in Hz.
 *              A resolution of 100 Hz, for example, means that the minimum 
 *              distinguishable time interval represents 10 milliseconds. It
 *              is assumed that the resolution of all timers is identical.
 * 
 * PARAMETER:  nil
 *
 *              
 * RETURNVALUE: Time resolution
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 09.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tU32 OSAL_u32TimerGetResolution(tVoid)
{
   struct timespec res;
   tU32 hertz = 100;

   if (0 == clock_getres(CLOCK_REALTIME, &res))
   {
      /* clock_getres likely returns 1ns resolution, that is of no use!
       * to prevent a divide by zero: return 100hz
       * TODO: how to detect the timer resolution at runtime?
       */
      if (res.tv_nsec > 1000000)
      {
         hertz = 1000 / (res.tv_nsec / 1000000);
      }
   }
   return hertz;
}


tVoid vCountSeconds(void* id)
{
  (tVoid)id; /* satisfy lint*/
  pOsalData->u32Seconds++;
  return;
}

void SetupSecTimer(void)
{
   if(OSAL_s32TimerCreate(vCountSeconds,
                         NULL, 
                         &pOsalData->hSecTimer) == OSAL_OK)
   {
      if(OSAL_s32TimerSetTime(pOsalData->hSecTimer,
                              1000,
                              1000) == OSAL_ERROR)
      {
          NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
       NORMAL_M_ASSERT_ALWAYS();
   }
}

void LinuxCbHdr(union sigval Arg)
{
#ifdef DEBUG_LINUX_OSAL
   printf("OSAL Core Tim LinuxCbHdr Entry %x \n",Arg.sival_ptr);
#endif
   trTimerElement* pElement = (trTimerElement*)Arg.sival_ptr;

   if(pElement)
   {
      if(pElement->u32TimerID == LINUX_C_U32_TIMER_ID)
      {
         if(pElement->bTraceOn)
         {  
            TraceString("PID:%d Timer CallBackExecute element 0x%x TMO:%d PER:%d",
                        getpid(),pElement,pElement->u32Timeout,pElement->u32Interval);
         }
         CallBackExecute(pElement);
      }
   }
}

void vTraceSpecialTimerStatus(void)
{
    tS32 Index = s32FindProcEntry(getpid());
    if(Index == OSAL_ERROR)
    {
       TraceString("PID:%d OSAL Timer without signals active",getpid());
    }
    else
    {
       TraceString("PID:%d OSAL Timer without signals active for %s",getpid(),prProcDat[Index].pu8AppName);
    }
}

void vTraceTimerApi(trTimerElement *pCurrentEntry,tU8 u8OpCode,tU32 u32ErrorCode)
{
#ifdef SHORT_TRACE_OUTPUT
   char au8Buf[30];
   tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
   if((u8OpCode == DELETE_OPERATION)||(u8OpCode == CREATE_OPERATION))
   {
      OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_TIM_INFO);
      OSAL_M_INSERT_T8(&au8Buf[1],(tU8)u8OpCode);
      bGetThreadNameForTID(&au8Buf[2],8,(tS32)u32Val);
      OSAL_M_INSERT_T32(&au8Buf[10],u32Val);
      if(pCurrentEntry)
      {   OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry); }
      else
      {
          u32Val = 0;
          OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
      }
      OSAL_M_INSERT_T32(&au8Buf[18],u32ErrorCode);
      u32Val = 22;
   }
   else
   {
      if(u8OpCode == OSAL_TIM_GET_INFO)
      {
          OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TIM_GET_INFO);
      }
      else if(u8OpCode == OSAL_TIM_SET_INFO)
      {
          OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TIM_SET_INFO);
      }
      if(pCurrentEntry)
      {
         OSAL_M_INSERT_T32(&au8Buf[1],(tU32)pCurrentEntry);
         OSAL_M_INSERT_T32(&au8Buf[9],pCurrentEntry->u32Timeout);
         OSAL_M_INSERT_T32(&au8Buf[13],pCurrentEntry->u32Interval);
      }
      else
      {
         OSAL_M_INSERT_T32(&au8Buf[1],u32ErrorCode);
         OSAL_M_INSERT_T32(&au8Buf[9],u32ErrorCode);
         OSAL_M_INSERT_T32(&au8Buf[13],u32ErrorCode);
      }
      OSAL_M_INSERT_T32(&au8Buf[5],(tU32)u32ErrorCode);
      OSAL_M_INSERT_T32(&au8Buf[17],u32Val);
      u32Val = 21;
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_TIM, TR_LEVEL_FATAL,au8Buf,u32Val);
   if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_NOERROR))
   {
      vWriteToErrMem(OSAL_C_TR_CLASS_SYS_TIM,au8Buf,u32Val,0);
   }
#else
      char au8Buf[251];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      char name[TRACE_NAME_SIZE];
      au8Buf[0] = OSAL_STRING_OUT;
      switch(u8OpCode)
      {
      case CREATE_OPERATION:
#ifdef NO_PRIxPTR
           snprintf(&au8Buf[1],250,"OSALTIM Timer Create of Task:%s(%d) Timer:0x%x ErrorCode:0x%x ",
                    name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32ErrorCode);
#else
           snprintf(&au8Buf[1],250,"OSALTIM Timer Create of Task:%s(%d) Timer:0x%" PRIxPTR " ErrorCode:0x%x ",
                    name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32ErrorCode);
#endif
          break;
      case DELETE_OPERATION:
#ifdef NO_PRIxPTR
           snprintf(&au8Buf[1],250,"OSALTIM Timer Delete of Task:%s(%d) Timer:0x%x ErrorCode:0x%x ",
                    name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32ErrorCode);
#else
           snprintf(&au8Buf[1],250,"OSALTIM Timer Delete of Task:%s(%d) Timer:0x%" PRIxPTR " ErrorCode:0x%x ",
                    name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32ErrorCode);
#endif
          break;
      case OSAL_TIM_GET_INFO:
           if(pCurrentEntry)
           {
#ifdef NO_PRIxPTR
              snprintf(&au8Buf[1],250,"OSALTIM Timer GetTime of Task:%s(%d) Timer:0x%x Elapse:%d Intervall:%d ErrorCode:0x%x ",
                       name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32Timeout,
                       (unsigned int)pCurrentEntry->u32Interval,(unsigned int)u32ErrorCode);
#else
              snprintf(&au8Buf[1],250,"OSALTIM Timer GetTime of Task:%s(%d) Timer:0x%" PRIxPTR " Elapse:%d Intervall:%d ErrorCode:0x%x ",
                       name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32Timeout,
                       (unsigned int)pCurrentEntry->u32Interval,(unsigned int)u32ErrorCode);
#endif
           }
           else
           {
#ifdef NO_PRIxPTR
              snprintf(&au8Buf[1],250,"OSALTIM Timer GetTime of Task:%s(%d) Timer:0x%x Elapse:%d Intervall:%d ErrorCode:0x%x ",
                       name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,0,0,(unsigned int)u32ErrorCode);
#else
              snprintf(&au8Buf[1],250,"OSALTIM Timer GetTime of Task:%s(%d) Timer:0x%" PRIxPTR " Elapse:%d Intervall:%d ErrorCode:0x%x ",
                       name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,0,0,(unsigned int)u32ErrorCode);
#endif
           } 
          break;
      case OSAL_TIM_SET_INFO:
           if(pCurrentEntry)
           {
#ifdef NO_PRIxPTR
               snprintf(&au8Buf[1],250,"OSALTIM Timer SetTime of Task:%s(%d) Timer:0x%x Elapse:%d Intervall:%d ErrorCode:0x%x ",
                        name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32Timeout,
                        (unsigned int)pCurrentEntry->u32Interval,(unsigned int)u32ErrorCode);
#else
               snprintf(&au8Buf[1],250,"OSALTIM Timer SetTime of Task:%s(%d) Timer:0x%" PRIxPTR " Elapse:%d Intervall:%d ErrorCode:0x%x ",
                        name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32Timeout,
                        (unsigned int)pCurrentEntry->u32Interval,(unsigned int)u32ErrorCode);
#endif
           }
           else
           {
#ifdef NO_PRIxPTR
               snprintf(&au8Buf[1],250,"OSALTIM Timer SetTime of Task:%s(%d) Timer:0x%x Elapse:%d Intervall:%d ErrorCode:0x%x ",
                        name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,0,0,(unsigned int)u32ErrorCode);
#else
               snprintf(&au8Buf[1],250,"OSALTIM Timer SetTime of Task:%s(%d) Timer:0x%" PRIxPTR " Elapse:%d Intervall:%d ErrorCode:0x%x ",
                        name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,0,0,(unsigned int)u32ErrorCode);
#endif
           }
          break;
      default:
           snprintf(&au8Buf[1],250,"OSALTIM Unknown Timer function %d",u8OpCode);
          break;
      }
      LLD_vTrace(OSAL_C_TR_CLASS_SYS_TIM,TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
#endif
}

/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32TimerCreate
 *
 * DESCRIPTION: this function creates a timer. After expiry of a time interval   
 *              the specified parameter is called. After creation of the timer
 *              no time interval is set
 *               
 * PARAMETER:   OSAL_Callback pCallback
 *                 pointer to a callback function
 *              
 *              tPVoid
 *                 argument for the callback function
 *
 *              phTimer 
 *                 pointer to the timer handle.
 *
 *
 *              
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 05.12.01  |   Initial revision                     | 
 * 14.04.03  | rename function;                       | CM-CR/EES4-kos2hi
 *           | add parameter to identify              |    
 *           | application context                    | 
 *****************************************************************************/
tS32 OSAL_s32TimerCreate(OSAL_tpfCallback pCallback, 
                         tPVoid pvArg, 
                         OSAL_tTimerHandle* phTimer)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trTimerElement *pCurrentEntry = NULL;

   /* check if OSAL timer task is required */
   if(u32LocalTimerSig)
   {
      if(pthread_once(&timer_is_initialized, setupTimerThread) != 0)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         if(s32LocalTimerTid == -1)
         {
            do{
              OSAL_s32ThreadWait(1);
             // TraceString("Wait for Task Timer-%d",getpid());
            }while(s32LocalTimerTid == -1);
         }
      }
   }
   else
   {
       pthread_once(&timer_is_initialized, vTraceSpecialTimerStatus);
   }

   /* Parameter check */
   if((phTimer) && (pCallback))
   {
      if(LockOsal(&pOsalData->TimerTable.rLock) == OSAL_OK)
      {
         /*Get the first free entry in the timer table */   
         pCurrentEntry = tTimerTableGetFreeEntry();        
         /* Check if the timer table is not full */
         if(pCurrentEntry != OSAL_NULL)
         {     
             sigevent evp; 
            memset(&evp,0,sizeof(sigevent));
            if(u32LocalTimerSig)
            {
               evp.sigev_notify            = SIGEV_SIGNAL;
               evp.sigev_signo             = pOsalData->u32TimSignal;
               evp.sigev_value.sival_ptr   = pCurrentEntry;
             //  evp.sigev_notify_function   = NULL;
             //  evp.sigev_notify_attributes = NULL;
            }
            else
            {
               evp.sigev_notify            = SIGEV_THREAD; //A notification function shall be called to perform notification
             //  evp.sigev_signo             = 0; // no signal to be generated
               evp.sigev_value.sival_ptr   = pCurrentEntry;
               evp.sigev_notify_function   = LinuxCbHdr;
             //  evp.sigev_notify_attributes = NULL;
            }
            
            /*  If the value of the CLOCK_REALTIME clock is adjusted while an absolute timer
                based on that clock is armed, then the expiration of the timer will be
                appropriately adjusted.  Adjustments to the CLOCK_REALTIME clock have no
                effect on relative timers based on that clock. -> CLOCK_MONOTONIC*/

            while(1)
            {
               s32ReturnValue = timer_create(CLOCK_MONOTONIC, &evp, &pCurrentEntry->hTimer); 
               if(s32ReturnValue == 0)
               {
                   break;
               }
               else
               {
                  /* check for Temporary error during kernel allocation of timer structures */
                  if(errno == EAGAIN)
                  {
                     WRITE_LINE_INFO;
                     /* force dispatching */
                     OSAL_s32ThreadWait(10);
                  }
                  else
                  {
                      break;
                  }
               }
            }
            if(s32ReturnValue != -1)
            {
               /* valid handle is proved at function begin */ 
               *phTimer = (OSAL_tTimerHandle)pCurrentEntry; /*lint !e613 pointer already checked */
               pOsalData->u32TimResCount++;
               if(pOsalData->u32MaxTimResCount < pOsalData->u32TimResCount)
               {
                  pOsalData->u32MaxTimResCount = pOsalData->u32TimResCount;
                  if(pOsalData->u32MaxTimResCount > (pOsalData->u32MaxNrTimerElements*9/10))
                  {
                     pOsalData->u32NrOfChanges++;
                  }
               }
               pCurrentEntry->u32Timeout = 0;
               pCurrentEntry->u32Interval = 0;
               pCurrentEntry->bIsUsed    = TRUE;
               pCurrentEntry->pvArgOfOsalCallback = pvArg;
               pCurrentEntry->pfOsalCallback = pCallback;
               pCurrentEntry->bIsSetting = FALSE;
               pCurrentEntry->bTraceOn = FALSE;
               pCurrentEntry->idTask = OSAL_ThreadWhoAmI();
               pCurrentEntry->Pid = OSAL_ProcessWhoAmI();
               s32ReturnValue = OSAL_OK;
               if(pOsalData->s32CheckTimPid == pCurrentEntry->Pid)
               {
                  pCurrentEntry->bTraceOn = TRUE;
               }
            }
            else
            {
                u32ErrorCode = u32ConvertErrorCore(errno);
            }
         }
         else
         {
             u32ErrorCode = OSAL_E_NOSPACE;
         }
         UnLockOsal(&pOsalData->TimerTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if(u32ErrorCode != OSAL_E_NOERROR)                    
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      vTraceTimerApi(pCurrentEntry,CREATE_OPERATION,u32ErrorCode);
   }
   else
   {
      if((pOsalData->bTraceTimerAll)||(pCurrentEntry->bTraceOn == TRUE)||(u32OsalSTrace & 0x00000008))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceTimerApi(pCurrentEntry,CREATE_OPERATION,u32ErrorCode);
      }
   }
   
   return( s32ReturnValue );
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32TimerDelete
 *
 * DESCRIPTION: this function delete a timer. 
 *
 * PARAMETER:   hTimer (I)
 *                 timer handle to be removed.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 05.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tS32 OSAL_s32TimerDelete(OSAL_tTimerHandle hTimer)
{
   tS32 s32ReturnValue=OSAL_ERROR;
   tU32 u32ErrorCode=OSAL_E_NOERROR;
   trTimerElement *pCurrentEntry = NULL;

   if (hTimer && ((tU32)hTimer != OSAL_C_INVALID_HANDLE))
   {
      if(LockOsal(&pOsalData->TimerTable.rLock) == OSAL_OK)
      {
         pCurrentEntry = tTimerCheckHandle(hTimer);
         if( pCurrentEntry != OSAL_NULL )
         {
            if(timer_delete(pCurrentEntry->hTimer) == OSAL_OK)
            {
               pCurrentEntry->bIsSetting = FALSE;
               pCurrentEntry->hTimer = NULL;
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = OSAL_OK;
               pOsalData->u32TimResCount--;
            }
            else
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE; //OSAL_E_DOESNOTEXIST; // see requirement OSAL_INVALIDVALUE -> OEDT
         }
         UnLockOsal(&pOsalData->TimerTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if(u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      s32ReturnValue = OSAL_ERROR;
      vTraceTimerApi(pCurrentEntry,DELETE_OPERATION,u32ErrorCode);
   }
   else
   {
      if((pOsalData->bTraceTimerAll)||(pCurrentEntry->bTraceOn == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceTimerApi(pCurrentEntry,DELETE_OPERATION,u32ErrorCode);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32TimerGetTime
 *
 * DESCRIPTION: With this function the remaining time interval of a timer up
 *              to the calling of callback function can be determined.
 *
 * PARAMETER:  OSAL_tTimerHandle hTimer(I)
 *                Timer handle to get time;
 *  
 *             OSAL_tMSecond msec
 *                remaining time in ms or OSAL_NULL;
 *
 *             OSAL_tMSecond interval
 *                repetition Interval in ms or OSAL_NULL 
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 06.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tS32 OSAL_s32TimerGetTime(OSAL_tTimerHandle hTimer,
                          OSAL_tMSecond* pMSec,
                          OSAL_tMSecond* pInterval)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trTimerElement *pCurrentEntry = NULL;

   if (hTimer && ((tU32)hTimer != OSAL_C_INVALID_HANDLE))
   {
      pCurrentEntry = tTimerCheckHandle(hTimer);
      if( pCurrentEntry != OSAL_NULL )
      {
         if(FALSE == pCurrentEntry->bIsSetting)
         {
             if(pMSec)*pMSec = 0;
             if(pInterval)*pInterval = 0;
         }
         else
         {
            itimerspec rTime;
            if(timer_gettime(pCurrentEntry->hTimer,&rTime) == OSAL_OK)
            {
               if(pMSec)
               {
                  *pMSec = rTime.it_value.tv_sec * 1000;
                  *pMSec += rTime.it_value.tv_nsec/1000000L;
               }
               if(pInterval)
               {
                  *pInterval = rTime.it_interval.tv_sec * 1000;
                  *pInterval += rTime.it_interval.tv_nsec/1000000L;
               }
               s32ReturnValue = OSAL_OK;
            }
            else
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
            }

        }
     }
     else
     {
        u32ErrorCode = OSAL_E_DOESNOTEXIST;
     }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      s32ReturnValue = OSAL_ERROR;
      vTraceTimerApi(pCurrentEntry,OSAL_TIM_GET_INFO,u32ErrorCode);
   }
   else
   {
      if((pOsalData->bTraceTimerAll)||(pCurrentEntry->bTraceOn)||(u32OsalSTrace & 0x00000008))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceTimerApi(pCurrentEntry,OSAL_TIM_GET_INFO,u32ErrorCode);
      }
      s32ReturnValue = OSAL_OK;
   }
   return( s32ReturnValue );
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32TimerSetTime
 *
 * DESCRIPTION: With this function the timer is started ("wound up").The time
 *              interval up to the first time triggering of the call back 
 *              function is relative to the current point of time. Through the     
 *              specification of an additional interval the call back function 
 *              can be triggered periodically. If the timer is active, i.e. a 
 *              trigger time point id defined, which has not been reached yet,
 *              the time point can be changed by renewed calling of this 
 *              function or deleted by specific 0 as second parameter
 *                
 * PARAMETER:  
 *             OSAL_tTimerHandle hTimer,
 *             OSAL_tMSecond msec,
 *             OSAL_tMSecond interval
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 06.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tS32 OSAL_s32TimerSetTime(OSAL_tTimerHandle hTimer,
                          OSAL_tMSecond msec,
                          OSAL_tMSecond interval)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trTimerElement *pCurrentEntry = NULL;

   if (hTimer && ((tU32)hTimer != OSAL_C_INVALID_HANDLE))
   {
      if(LockOsal(&pOsalData->TimerTable.rLock) == OSAL_OK)
      {
         pCurrentEntry = tTimerCheckHandle(hTimer);
         if( pCurrentEntry != OSAL_NULL )
         {
            if((msec < pOsalData->u32TimerResolution)&&(msec > 0))msec = pOsalData->u32TimerResolution;

            itimerspec rNewTime = {{0,0L},{0,0L}};
            if (interval)
            {
               rNewTime.it_interval.tv_sec  = interval / 1000;
               rNewTime.it_interval.tv_nsec = (long)(interval % 1000) * (1000000L);
            }
            if(msec)
            {
               rNewTime.it_value.tv_sec  = msec / 1000;
               rNewTime.it_value.tv_nsec = (long)(msec % 1000) * (1000000L);
            }
            s32ReturnValue = timer_settime( pCurrentEntry->hTimer,
                                            0, // no TIMER_ABSTIME
                                            &rNewTime, 
                                            NULL);
            /* The timer_settime() function sets the time until the next expiration of the timer 
               specified by timerid from the it_value member of the value argument and arm the 
               timer if the it_value member of value is non-zero. If the specified timer was 
               already armed when timer_settime() is called, this call resets the time until next 
               expiration to the value specified. If the it_value member of value is zero, the 
               timer is disarmed. The effect of disarming or resetting a timer on pending 
               expiration notifications is unspecified. */
            if(s32ReturnValue != 0)  // timer_settime returns 0 or -1
            {
               TraceString("OSALTIM timer_settime timer:0x%x failed errno:%d by PID:%d TID:%d",
                           pCurrentEntry,errno,OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI());
               vWritePrintfErrmem("OSALTIM timer_settime timer:0x%x failed errno:%d by PID:%d TID:%d \n",
                                   pCurrentEntry,errno,OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI());
               u32ErrorCode = u32ConvertErrorCore(errno);
               /* check for unspecified behaviour */
               if(u32ErrorCode != OSAL_E_NOERROR)
               {
                  /*force dispatch */
                  pthread_yield();
                  /* setting timer failed ,but no error code is available 
                     shouldn't happen, but in special case the behaviour is undefined */
                  s32ReturnValue = timer_settime( pCurrentEntry->hTimer,
                                                  0, // no TIMER_ABSTIME
                                                  &rNewTime, 
                                                  NULL);
                  if(s32ReturnValue != 0)  // timer_settime returns 0 or -1
                  {
                     TraceString("OSALTIM timer_settime timer:0x%x failed errno:%d by PID:%d TID:%d",
                                  pCurrentEntry,errno,OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI());
                     u32ErrorCode = u32ConvertErrorCore(errno);
                     if(u32ErrorCode == OSAL_E_NOERROR)
                     {
                        NORMAL_M_ASSERT_ALWAYS();
                        /* setting timer failed twice, but no error code is available */
                        u32ErrorCode = OSAL_E_UNKNOWN;
                     }
                     else
                     {
                        /* setting timer failed twice, but error code is available */
                        NORMAL_M_ASSERT_ALWAYS();
                     }
                  }
                  else
                  {
                      /* setting timer the second time was successful */
                      NORMAL_M_ASSERT_ALWAYS();
                  }
               }/* end check for unspecified behaviour */
            }
            if(s32ReturnValue == 0)  // timer_settime returns 0 for success
            {
               pCurrentEntry->u32Timeout  = msec;
               pCurrentEntry->u32Interval = interval;
               if(msec == 0)   // it_value == 0 disarms the timer
               {
                  pCurrentEntry->bIsSetting = FALSE;
               }
               else
               {
                  pCurrentEntry->bIsSetting = TRUE;
               }
            }
         }
         else /* if( pCurrentEntry != OSAL_NULL )*/
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->TimerTable.rLock);
      }
   }
   else/* if hTimer*/
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }


   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      s32ReturnValue = OSAL_ERROR;
      vTraceTimerApi(pCurrentEntry,OSAL_TIM_SET_INFO,u32ErrorCode);
   }   
   else
   {
      if((pOsalData->bTraceTimerAll)||(pCurrentEntry->bTraceOn)||(u32OsalSTrace & 0x00000008))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceTimerApi(pCurrentEntry,OSAL_TIM_SET_INFO,u32ErrorCode);
      }
   }
 
  return( s32ReturnValue );
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_ClockGetElapsedTime;
 *
 * DESCRIPTION: This Function returns the elapsed time since the start of the 
 *              system through OSAL_Boot() in milliseconds 
 * 
 * PARAMETER:  nil
 *
 *              
 * RETURNVALUE: time in milliseconds
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 09.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
OSAL_tMSecond OSAL_ClockGetElapsedTime(tVoid)
{
   timeval oCur;
   struct timespec ts;
   tU32 ms;

   if ( clock_gettime(CLOCK_MONOTONIC, &ts))
   {
      TraceString("OSAL_ClockGetElapsedTime failed errno:%d",errno);
      gettimeofday(&oCur, NULL);
      ms = oCur.tv_sec * 1000 + oCur.tv_usec / 1000;
   }
   else
   {
      ms = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
   }
   return ms /*- pOsalData->u32OsalStartTimeInMs*/;
}

tU64 OSAL_u64GetMicroSeconds(tVoid)
{
   timeval oCur;
   struct timespec ts;
   tU64 ms;

   if ( clock_gettime(CLOCK_MONOTONIC, &ts))
   {
      TraceString("OSAL_u64GetMicroSeconds failed errno:%d",errno);
      gettimeofday(&oCur, NULL);
      ms = oCur.tv_sec * (tU64)1000000 + oCur.tv_usec / 1;
   }
   else
   {
      ms = ts.tv_sec * (tU64)1000000 + ts.tv_nsec / 1000;
   }
   return ms /*- pOsalData->u32OsalStartTimeInMs*/;
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32ClockGetTime;
 *
 * DESCRIPTION: This Function reads the current system time  
 *
 * PARAMETER:  OSAL_trTimeDate* prCurrentTime   pointer to timing
 *
 *              
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 09.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tS32 OSAL_s32ClockGetTime(OSAL_trTimeDate* prCurrentTime)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   struct tm mytm;
   time_t secSinceEpoch;
   OSAL_trTimeDate trCurrTimeDate;
   tU64 u64GetTimeUTCElapsedms= 0;
   tU64 u64GetTimeSystemElapsedms = 0;

   if( prCurrentTime != OSAL_NULL)     
   { 
      if( bNativeTimeHldr != TRUE)
      {
         u64GetTimeSystemElapsedms    = OSAL_ClockGetElapsedTime();
         u64GetTimeUTCElapsedms       = (u64SetTimeUTCElapsed*((tU32)1000)) + ( u64GetTimeSystemElapsedms - u64SetTimeSystemElapsed );
#ifdef TIMERDEBUG
         TraceString("LI:u64SetTimeUTCElapsed = %llu",u64SetTimeUTCElapsed);
         TraceString("LI:u64SetTimeSystemElapsed = %llu",u64SetTimeSystemElapsed);
         TraceString("LI:u64GetTimeSystemElapsedms = %llu",u64GetTimeSystemElapsedms);
         TraceString("LI:u32GetTimeUTCElapsed = %llu",u64GetTimeUTCElapsedms);
#endif
         if(pOsalData->u32UseOsalTime)
         {
            u64GetTimeUTCElapsedms += pOsalData->s32OsalInternOffset;
         }
         if(TRUE == bConvertmSecsToDate(u64GetTimeUTCElapsedms,&trCurrTimeDate))
         {
            /* Copy The Time to requested structure */
            prCurrentTime->s32Second         = trCurrTimeDate.s32Second;
            prCurrentTime->s32Minute         = trCurrTimeDate.s32Minute;
            prCurrentTime->s32Hour           = trCurrTimeDate.s32Hour;
            prCurrentTime->s32Day            = trCurrTimeDate.s32Day;
            prCurrentTime->s32Month          = trCurrTimeDate.s32Month;
            prCurrentTime->s32Year           = trCurrTimeDate.s32Year;
            prCurrentTime->s32Weekday        = trCurrTimeDate.s32Weekday;
            prCurrentTime->s32Yearday        = trCurrTimeDate.s32Yearday;
            prCurrentTime->s32Daylightsaving = trCurrTimeDate.s32Daylightsaving;
            s32ReturnValue=OSAL_OK;
#ifdef TIMERDEBUG
            TraceString("Get Years = %d , Months = %d, Days = %d, Hours = %d, Minutes = %d, Seconds = %d",
                        prCurrentTime->s32Year,prCurrentTime->s32Month,
                        prCurrentTime->s32Day,prCurrentTime->s32Hour,
                        prCurrentTime->s32Minute,prCurrentTime->s32Second);
#endif
         }
         else
         {
            u32ErrorCode = OSAL_E_UNKNOWN;
#ifdef TIMERDEBUG
            vWritePrintfErrmem("Conversion Failed! ElapsedTime to Date %llu \n",u64GetTimeUTCElapsedms);
#endif
         }
      }
      else
      {
         secSinceEpoch = time(NULL);
         if(pOsalData->u32UseOsalTime)
         {
            secSinceEpoch += pOsalData->s32OsalInternOffset;
         }
#ifdef TIMERDEBUG
         TraceString("LI:GetNative Hldr Seconds = %ld",secSinceEpoch );
         TraceString("LI:GetpOsalData->hSecTimerHandle = %d",pOsalData->hSecTimer);
#endif
         if( NULL != gmtime_r(&secSinceEpoch, &mytm))
         {
            prCurrentTime->s32Second         = mytm.tm_sec;
            prCurrentTime->s32Minute         = mytm.tm_min;
            prCurrentTime->s32Hour           = mytm.tm_hour;
            prCurrentTime->s32Day            = mytm.tm_mday;
            prCurrentTime->s32Month          = mytm.tm_mon + 1;
            prCurrentTime->s32Year           = mytm.tm_year;
            prCurrentTime->s32Weekday        = mytm.tm_wday;
            prCurrentTime->s32Yearday        = mytm.tm_yday;
            prCurrentTime->s32Daylightsaving = mytm.tm_isdst; /* do we have locale info? */
            s32ReturnValue = OSAL_OK;
         }
         else
         {
#ifdef TIMERDEBUG
            TraceString("LI:Getgmtime_r failed");
#endif
             u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }	

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
        /* Is called very often , trace will come after setting the UTC time */
      if(/*LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_TIM,TR_LEVEL_DATA)||*/(u32OsalSTrace & 0x00000008))
      {
         if((prCurrentTime))
         {
            TraceString("UTC Time Get: %d.%d.%d Hour:%d Min:%d Sec:%d",
                        prCurrentTime->s32Day,
                        prCurrentTime->s32Month,
                        prCurrentTime->s32Year,
                        prCurrentTime->s32Hour,
                        prCurrentTime->s32Minute,
                        prCurrentTime->s32Second);
         }
      }
   }

    return s32ReturnValue;
}

void vSetTimeToErrmem(tU64 secSinceEpoch)
{
#ifdef SET_ERRMEM_TIME
   int fd = open("/dev/errmem", O_WRONLY);
   if(fd)
   {
     tU64 msecSinceEpoch = secSinceEpoch;
     msecSinceEpoch = msecSinceEpoch*1000;
     if(ioctl(fd, IOCTL_ERRMEM_SET_EPOCH_MS, &msecSinceEpoch) ==  -1)
     {
       TraceString("Set Time to Errmem ioctl failed errno %d",errno);
     }
     else
     {
       TraceString("Set Time to Errmem ioctl succeeded %u Seconds  %llu MilliSeconds",secSinceEpoch,msecSinceEpoch);
     }
     close(fd);
   }
   else
   {
      TraceString("Set Time to Errmem open failed errno %d",errno);
   }
#else
   ((void)secSinceEpoch);
#endif
}

/*****************************************************************************
 *
 * FUNCTION:    OSAL_ClockSetTime;
 *
 * DESCRIPTION: This Function sets the current system time  
 *
 * PARAMETER:  OSAL_trTimeDate* prCurrentTime   new timing
 *
 *              
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 09.12.01  |   Initial revision                     | TB/DC/AP/MS
 *****************************************************************************/
tS32 OSAL_s32ClockSetTime(const OSAL_trTimeDate* pcorSetTime)
{  
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   char u8Buffer[100];
   tU64 u64mseconds = 0;
   OSAL_trTimeDate trCurrTimeDate;
   OSAL_trTimeDate* prCurrTimeDate = OSAL_NULL;

   if( pcorSetTime != OSAL_NULL)
   {
       if(pcorSetTime->s32Year >= (2037-1900))
      {
         trCurrTimeDate.s32Second          =   pcorSetTime->s32Second;
         trCurrTimeDate.s32Minute            =   pcorSetTime->s32Minute;
         trCurrTimeDate.s32Hour              =   pcorSetTime->s32Hour;
         trCurrTimeDate.s32Day               =   pcorSetTime->s32Day;
         trCurrTimeDate.s32Month             =   pcorSetTime->s32Month;
         trCurrTimeDate.s32Year              =   pcorSetTime->s32Year;
         trCurrTimeDate.s32Weekday           =   pcorSetTime->s32Weekday;
         trCurrTimeDate.s32Yearday           =   pcorSetTime->s32Yearday;
         trCurrTimeDate.s32Daylightsaving    =   pcorSetTime->s32Daylightsaving;//TODO
         prCurrTimeDate = &trCurrTimeDate;
         if (TRUE == bConvertDateTomSecs(prCurrTimeDate, &u64mseconds))
         {
            bNativeTimeHldr = FALSE;
            u64SetTimeUTCElapsed = (u64mseconds/(tU32)1000);
#ifdef TIMERDEBUG
            TraceString("LI:SetNewHldr u64mseconds= %llu ms",u64mseconds);
            TraceString("LI:SetNewHldr u64SetTimeUTCElapsed= %llu s",u64SetTimeUTCElapsed);
#endif
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
         
         u64SetTimeSystemElapsed = OSAL_ClockGetElapsedTime();

         vSetTimeToErrmem(u64SetTimeUTCElapsed);         
         
         /*Since the time is beyond System Time Handling range,
          * System Time is not updated in this case*/
#ifdef TIMERDEBUG
            TraceString("LI:SetNewHldr u64SetTimeSystemElapsed= %llu s",u64SetTimeSystemElapsed);
#endif
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               s32ReturnValue = OSAL_OK;
            }
      }
      else /*Native Time Handler*/
      {
         struct tm objTM;
         struct timespec ts;
         time_t secSinceEpoch;

         objTM.tm_sec       = pcorSetTime->s32Second;
         objTM.tm_min       = pcorSetTime->s32Minute;
         objTM.tm_hour      = pcorSetTime->s32Hour;
         objTM.tm_mday      = pcorSetTime->s32Day;
         objTM.tm_mon       = pcorSetTime->s32Month - 1;
         objTM.tm_year      = pcorSetTime->s32Year;
         objTM.tm_wday      = pcorSetTime->s32Weekday;
         objTM.tm_yday      = pcorSetTime->s32Yearday;

         switch (pcorSetTime->s32Daylightsaving)
         {
           case 0:
           case 1:
                objTM.tm_isdst = pcorSetTime->s32Daylightsaving;	
               break;
           default:
                objTM.tm_isdst    =  0;
               break;
         }
         secSinceEpoch = mktime( &objTM );
#ifdef TIMERDEBUG
         TraceString("LI:SetNative Hldr Seconds = %ld",secSinceEpoch);
#endif
         if( -1 == secSinceEpoch)
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
         else
         {
            bNativeTimeHldr = TRUE;
            pOsalData->hSecTimer = secSinceEpoch;
#ifdef TIMERDEBUG
            TraceString("LI:SetpOsalData->hSecTimerHandle = %u",pOsalData->hSecTimer );
#endif
            pthread_once(&SecTimer_is_initialized,&SetupSecTimer);
            if(pOsalData->u32UseOsalTime)
            {
               if((-1 == pcorSetTime->s32Second)||(-1 == pcorSetTime->s32Minute)||(-1 == pcorSetTime->s32Hour)||
                  (-1 == pcorSetTime->s32Day)||(-1 == pcorSetTime->s32Month)||(-1 == pcorSetTime->s32Year)||
                  (-1 == pcorSetTime->s32Weekday)||(-1 == pcorSetTime->s32Yearday))
               {
                  u32ErrorCode = OSAL_E_INVALIDVALUE;
               }
               else
               {
                  time_t secSinceEpochCur = time(NULL);		
                  pOsalData->s32OsalInternOffset = secSinceEpoch - secSinceEpochCur;
                  s32ReturnValue = OSAL_OK;
               }
            }
            else			
            {
               ts.tv_sec = secSinceEpoch;
               ts.tv_nsec = 0;
               if ( clock_settime(CLOCK_REALTIME, &ts) == -1)
               {
#ifdef TIMERDEBUG
                  TraceString("LI:clock_settime failed = %u",ts.tv_sec);
#endif             
                  u32ErrorCode = u32ConvertErrorCore(errno);
               }
               else
               {
                  tU64 u64Second = secSinceEpoch;
                  vSetTimeToErrmem(u64Second);
                  s32ReturnValue = OSAL_OK;
               }
            }
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      s32ReturnValue = OSAL_ERROR;
      if(pcorSetTime)
      {
          char Name[OSAL_C_U32_MAX_NAMELENGTH];
          tS32 Pid = OSAL_ProcessWhoAmI();
          tS32 Tid = OSAL_ThreadWhoAmI();
          if(!bGetTaskName(Pid,Tid,Name))
          {
              strncpy(Name,"Unknown",strlen("Unknown"));
          }
          snprintf(u8Buffer,100,"Set UTC Time: %d.%d.%d Hour:%d Min:%d Sec:%d DayLight:%d failed Error:%d Task:%s \n",
                  pcorSetTime->s32Day,
                  (pcorSetTime->s32Month -1),
                  pcorSetTime->s32Year,
                  pcorSetTime->s32Hour,
                  pcorSetTime->s32Minute,
                  pcorSetTime->s32Second,
                  pcorSetTime->s32Daylightsaving,
                  (unsigned int)u32ErrorCode,
                  Name);
      }
      else
      {
         snprintf(u8Buffer,100,"Set UTC Time Error: %d \n",(unsigned int)u32ErrorCode);
      }
      TraceString(u8Buffer);
      vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&u8Buffer[0],strlen(u8Buffer),OSAL_STRING_OUT);
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }
   else
   {
      if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_TIM,TR_LEVEL_DATA)||(u32OsalSTrace & 0x00000008))
      {
          char Name[OSAL_C_U32_MAX_NAMELENGTH];
          tS32 Pid = OSAL_ProcessWhoAmI();
          tS32 Tid = OSAL_ThreadWhoAmI();
          if(!bGetTaskName(Pid,Tid,Name))
          {
              strncpy(Name,"Unknown",strlen("Unknown"));
          }
          TraceString("UTC Time Set: %d.%d.%d Hour:%d Min:%d Sec:%d by Task:%s",
                     pcorSetTime->s32Day,
                     pcorSetTime->s32Month,
                     pcorSetTime->s32Year,
                     pcorSetTime->s32Hour,
                     pcorSetTime->s32Minute,
                     pcorSetTime->s32Second,
                     Name);/*lint !e613 pcorSetTime already checked */

         OSAL_trTimeDate rCurrentTime;
         if(OSAL_s32ClockGetTime(&rCurrentTime) == OSAL_OK)
         {
            TraceString("UTC Time Get: %d.%d.%d Hour:%d Min:%d Sec:%d",
                     rCurrentTime.s32Day,
                     rCurrentTime.s32Month,
                     rCurrentTime.s32Year,
                     rCurrentTime.s32Hour,
                     rCurrentTime.s32Minute,
                     rCurrentTime.s32Second);
         }
         else
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
   }
   return s32ReturnValue;
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
