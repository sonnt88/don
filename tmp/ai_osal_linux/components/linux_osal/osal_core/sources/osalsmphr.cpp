/*****************************************************************************
| FILE:         osalsmphr.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Semaphore-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 04.03.2013| Fixed Lint warnings        | mdh4cob
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define LINUX_C_U32_SEMAPHORE_MAGIC                 ((tU32)0x53454D41)

#ifdef OSAL_SHM_MQ
#define  LINUX_C_SEM_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH + 2)
#else
#define  LINUX_C_SEM_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH)
#endif

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/


tS32 s32SemaphoreTableCreate(tVoid);

tS32 s32SemaphoreTableDeleteEntries(void);

static trSemaphoreElement* tSemaphoreTableGetFreeEntry(tVoid);

static trSemaphoreElement* tSemaphoreTableSearchEntryByName(tCString coszName);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    s32SemaphoreTableDeleteEntries
 *
 * DESCRIPTION: This function deletes all elements from OSAL table
 *
 * PARAMETER:
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32SemaphoreTableDeleteEntries(void)
{
   trSemaphoreElement *pCurrentEntry = pSemEl;
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;

   if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
   {
      /* search the whole table */
      while( pCurrentEntry < &pSemEl[pOsalData->SemaphoreTable.u32MaxEntries])
      {
         if(pCurrentEntry->bIsUsed == TRUE)
         {
            s32DeleteCounter++;

            s32ReturnValue = DelSyncObj(pCurrentEntry->hSemaphore);

           if( s32ReturnValue == OSAL_E_NOERROR )
           {
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = s32DeleteCounter;
           }
           else
           {
               s32ReturnValue = OSAL_ERROR;
               break;
           }
         }
         else
            pCurrentEntry++;
      }
      UnLockOsal(&pOsalData->SemaphoreTable.rLock);
   }

   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION:    s32SemaphoreTableCreate
 *
 * DESCRIPTION: This function creates the Semaphore Table List. If
 *              there isn't space it returns a error code.
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32SemaphoreTableCreate(tVoid)
{
   tU32 tiIndex;

   pOsalData->SemaphoreTable.u32UsedEntries   = 0;
   pOsalData->SemaphoreTable.u32MaxEntries       = pOsalData->u32MaxNrSemaphoreElements;

   for( tiIndex=0; tiIndex <  pOsalData->SemaphoreTable.u32MaxEntries ; tiIndex++ )
   {
       pSemEl[tiIndex].u32SemaphoreID = LINUX_C_U32_SEMAPHORE_MAGIC;
       pSemEl[tiIndex].bIsUsed = FALSE;
   }
#ifdef SEMAPHORE_WARN_LEVEL
   pOsalData->u32WarnLevel  = SEMAPHORE_WARN_LEVEL;
#endif
   return OSAL_OK;
}


/*****************************************************************************
 *
 * FUNCTION:    tSemaphoreTableGetFreeEntry
 *
 * DESCRIPTION: this function goes through the Semaphore List and returns the
 *              first SemaphoreElement.In case no entries are available a new
 *              defined number of entries is attempted to be added.
 *              In case of success the first new element is returned, otherwise
 *              a NULL pointer is returned.
 *
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trSemaphoreElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static  trSemaphoreElement* tSemaphoreTableGetFreeEntry(tVoid)
{
   trSemaphoreElement *pCurrent  = &pSemEl[0];
   tU32 used = pOsalData->SemaphoreTable.u32UsedEntries;

   if (used < pOsalData->SemaphoreTable.u32MaxEntries)
   {
      pCurrent = &pSemEl[used];
      pOsalData->SemaphoreTable.u32UsedEntries++;
   } else {
      /* search an entry with !bIsUsed, used == MaxEntries */
      while ( pCurrent <= &pSemEl[used]
         && ( pCurrent->bIsUsed == TRUE) )
      {
         pCurrent++;
      }
      if(pCurrent >= &pSemEl[used])
      {
         pCurrent = NULL; /* not found */
      }
   }

   return(pCurrent);
}


/*****************************************************************************
 *
 * FUNCTION:    tSemaphoreTableSearchEntryByName
 *
 * DESCRIPTION: this function goes through the Semaphore List and returns the
 *              SemaphoreElement with the given name or NULL if all the List has
 *              been checked without success.
 *
 * PARAMETER:   coszName (I)
 *                 Semaphore name wanted.
 *
 * RETURNVALUE: trSemaphoreElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trSemaphoreElement *
tSemaphoreTableSearchEntryByName(tCString coszName)
{
   trSemaphoreElement *pCurrent = &pSemEl[0];
   tU32 used = pOsalData->SemaphoreTable.u32UsedEntries;

   while( (pCurrent < &pSemEl[used])
          && ((pCurrent->bIsUsed == FALSE )
              || (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0)) )
   {
      pCurrent++;
   }
   if (pCurrent >= &pSemEl[used]) {
      pCurrent = NULL;
   }
   return(pCurrent);
}

/*****************************************************************************
*
* FUNCTION:    bCleanUpSemaphoreofContext
*
* DESCRIPTION: this function goes through the Thread List deletes the
*              Semaphore and SemaphoreElement with the given context from list
*
* PARAMETER:   to be of used the context is the "getpid()": when the process
*              exits we could clean up "stale" resources
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vCleanUpSemaphoreofContext(void)
{
   tS32 s32OwnPid = OSAL_ProcessWhoAmI();
   uintptr_t s32SemHandle;
   tS32 s32OpnCount = 0;
   tU32 i;
   trSemaphoreElement *pCurrentEntry = NULL;
   char cName[LINUX_C_SEM_MAX_NAMELENGHT] = {0};
   for(i=0;i<pOsalData->u32SemHandles;i++)
   {
      s32SemHandle = (uintptr_t)pvGetFirstElementForPid(&SemMemPoolHandle,(tU32)s32OwnPid);
      if(s32SemHandle)
      {
         if(((tSemHandle*)s32SemHandle)->s32Tid > 0)
         {
            /* cleanup semaphore */
            pCurrentEntry = ((tSemHandle*)s32SemHandle)->pEntry; /*lint !e613 *//* pointer already checked*/ 
            if(cName[0] == 0)
            {
               strncpy(cName,pCurrentEntry->szName,LINUX_C_SEM_MAX_NAMELENGHT);
               cName[LINUX_C_SEM_MAX_NAMELENGHT-1] = '\0';
            }
            s32OpnCount =  pCurrentEntry->u16OpenCounter;
            if(((tSemHandle*)s32SemHandle)->s32Cnt > 0)
            {
               OSAL_s32SemaphorePost((OSAL_tSemHandle)s32SemHandle);
            }
            TraceString("OSAL_s32SemaphoreClose for %s",pCurrentEntry->szName);
            if(OSAL_s32SemaphoreClose((OSAL_tSemHandle)s32SemHandle) == OSAL_ERROR)
            {
               break;
            }
            if((s32OpnCount-1) == 0)
            {
               OSAL_s32SemaphoreDelete(cName);
            }
         }
         else
         {
            /* defect handle in pool */
            TraceString(" Semaphore pvGetFirstElementForPid gives handle without TID");
            break;
         }
      }
      else
      {
          /* last handle for PID was found */
          break;
      }
   }
}


/*****************************************************************************
 *
 * FUNCTION:     u32CheckSemaphoreHandle
 *
 *
 * DESCRIPTION: this function checks the handle for validity
 *
 * PARAMETER:   handle (I)
 *                Semaphore handle
 *
 * RETURNVALUE: tU32  Error Code
 *                 
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static void vPrintSemHandleIssue(OSAL_tSemHandle hSem)
{
  if(pOsalData->bLogError)
  {
     vWritePrintfErrmem("OSALSEM memstart:0x%x memend:0x%x Handle:0x%x \n", SemMemPoolHandle.u32memstart,SemMemPoolHandle.u32memend,hSem);
     NORMAL_M_ASSERT_ALWAYS();
  }
  TraceString("OSALSEM memstart:0x%x memend:0x%x Handle:0x%x ", SemMemPoolHandle.u32memstart,SemMemPoolHandle.u32memend,hSem);
}
static tU32 u32CheckSemaphoreHandle(OSAL_tSemHandle hSem)
{
   if((hSem <= SemMemPoolHandle.u32memstart)||(hSem >= SemMemPoolHandle.u32memend))
   {
      if(SemMemPoolHandle.pMem)
      {
         trOSAL_MPF* pPtr = (trOSAL_MPF*)SemMemPoolHandle.pMem;
         if(pPtr->u32ErrCnt == 0)
         {
            vPrintSemHandleIssue(hSem);
            return OSAL_E_INVALIDVALUE;
         }
         else
         {
            /* possible that Handle Memory was allocated via malloc */
            if(( hSem == 0)||((tU32)hSem == OSAL_C_INVALID_HANDLE))
            {
               vPrintSemHandleIssue(hSem);
               return OSAL_E_INVALIDVALUE;
            }
         }     
      }
      else
      {
        vPrintSemHandleIssue(hSem);
        return OSAL_E_INVALIDVALUE;
      }
   }
   return OSAL_E_NOERROR;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
void vTraceSemInfo(trSemaphoreElement *pCurrentEntry,tU8 Type,tBool bErrMem,tU32 u32Error,tCString coszName)
{
#ifdef SHORT_TRACE_OUTPUT
    char au8Buf[24 + LINUX_C_SEM_MAX_NAMELENGHT];
    tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
    OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_SEM_INFO);
    OSAL_M_INSERT_T8(&au8Buf[1],(tU8)Type);
    bGetThreadNameForTID(&au8Buf[2],8,(tS32)u32Val);
    OSAL_M_INSERT_T32(&au8Buf[10],u32Val);

    if(pCurrentEntry)
    {
      OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry->hSemaphore);
      tS32 s32Val = -1;
      if(pCurrentEntry->hSemaphore == (tU32)-1)
      {
          pCurrentEntry->u16SemValue = 0xffff;
      }
      else
      {
         if(GetSyncObjVal(pCurrentEntry->hSemaphore,&s32Val) == OSAL_E_NOERROR)
         {
             pCurrentEntry->u16SemValue = (tU16)s32Val;
         }
      }
      OSAL_M_INSERT_T16(&au8Buf[18],pCurrentEntry->u16SemValue);
    }
    else
    {
      OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
      OSAL_M_INSERT_T16(&au8Buf[18],(tU16)u32Val);
    }
      OSAL_M_INSERT_T32(&au8Buf[20],u32Error);
    u32Val = 0;
    if(coszName)
    {
      u32Val = strlen(coszName);
      if( u32Val > LINUX_C_SEM_MAX_NAMELENGHT )
      {
         u32Val = LINUX_C_SEM_MAX_NAMELENGHT;
      }
      memcpy (&au8Buf[24],coszName,u32Val);
      au8Buf[24 + LINUX_C_SEM_MAX_NAMELENGHT -1] = 0;
    }
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_SEM, TR_LEVEL_FATAL,au8Buf,u32Val+24);
    if((pOsalData->bLogError)&&(bErrMem))
    {
       if(!pCurrentEntry)
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_SEM,au8Buf,u32Val+24,0);
       }
       else
       {
          if(!pCurrentEntry->bTraceSem)vWriteToErrMem(OSAL_C_TR_CLASS_SYS_SEM,au8Buf,u32Val+24,0);
       }
    }
#else
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];
   tS32 s32Val=0;
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   au8Buf[0] = OSAL_STRING_OUT;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
   tCString Operation;
   switch(Type)
   {
     case CREATE_OPERATION:
         Operation = "CREATE_OPERATION";
         break;
     case DELETE_OPERATION:
         Operation = "DELETE_OPERATION";
         break;
     case OPEN_OPERATION:
         Operation = "OPEN_OPERATION";
         break;
     case CLOSE_OPERATION:
         Operation = "CLOSE_OPERATION";
         break;
     case WAIT_OPERATION:
         Operation = "WAIT_OPERATION";
         break;
     case POST_OPERATION:
         Operation = "POST_OPERATION";
         break;
     case STATUS_OPERATION:
         Operation = "STATUS_OPERATION";
         break;
     default:
         Operation = "Unknown";
         break;
   }
   if(coszName == NULL)coszName ="Unknown";
   if(pCurrentEntry)
   {
      if(GetSyncObjVal(pCurrentEntry->hSemaphore,&s32Val) == OSAL_E_NOERROR)
      {
         pCurrentEntry->u16SemValue = (tU16)s32Val;
      } 
      snprintf(&au8Buf[1],250,"Semaphor %s Task:%s(%d) Handle:0x%x Count:%d Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry->hSemaphore,pCurrentEntry->u16SemValue,(unsigned int)u32Error,coszName);
   }
   else
   {
       snprintf(&au8Buf[1],250,"Semaphor %s Task:%s(%d) Handle:0x%x Count:%d Error:0x%x Name:%s",
                Operation,name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_SEM,TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
   if((pOsalData->bLogError)&&(bErrMem))
   {
       vWriteToErrMem(OSAL_C_TR_CLASS_SYS_SEM,au8Buf,strlen(&au8Buf[1])+1,OSAL_STRING_OUT);
   }
#endif
}

tU32 u32GenerateSemaphoreHandle(trSemaphoreElement *pCurrentEntry,OSAL_tSemHandle* phSemaphore)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tSemHandle* pTemp;
   pTemp = (tSemHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&SemMemPoolHandle);
   if(pTemp)
   {
      pTemp->pEntry   = pCurrentEntry;
      pTemp->s32Tid   = OSAL_ThreadWhoAmI();
      pTemp->s32Cnt   = 0;
      *phSemaphore = (OSAL_tSemHandle)pTemp;
   }
   else
   {
      u32ErrorCode = OSAL_E_NOSPACE;
   }
   return u32ErrorCode;
}

/*****************************************************************************
 * FUNCTION: OSAL_s32SemaphoreCreate
 *
 *
 * DESCRIPTION:   this function creates an OSAL Semaphore.
 *
 * PARAMETERS:
 *       coszName (I)
 *          name of semaphore
 *       phSemaphore (O)
 *          pseudo-handle for semaphore
 *       uCount (I)
 *          initial value of semaphore
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SemaphoreCreate(tCString coszName,
                             OSAL_tSemHandle * phSemaphore,
                             tU32 uCount)
{
   return(OSAL_s32SemaphoreCreate_Opt(coszName,
                                      phSemaphore,
                                      uCount,
                                       0));
}

/*****************************************************************************
 * FUNCTION: OSAL_s32SemaphoreCreate_Cntxt_Opt
 *
 *
 * DESCRIPTION:   this function create a OSAL Semaphore.
 *
 * PARAMETERS:
 *       coszName (I)
 *          name of semaphore
 *       phSemaphore (O)
 *          pseudo-handle for semaphore
 *       uCount (I)
 *          initial value of semaphore
 *       u16ContextID (I)
 *          context identification
 *       u16Option (I)
 *          Semaphore options 0 -> default
 *                            1 -> semaphore could not locked twice from the same thread 
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SemaphoreCreate_Opt(tCString coszName,
                                       OSAL_tSemHandle * phSemaphore,
                                       tU32 uCount,
                                       tU16 u16Option)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trSemaphoreElement *pCurrentEntry = NULL;

   if( phSemaphore )
   {
      if( coszName )  /* FIX 15/10/2002 */
      {
         if( OSAL_u32StringLength(coszName) < (tU32)LINUX_C_SEM_MAX_NAMELENGHT )
         {
            if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
            {
               pCurrentEntry = tSemaphoreTableSearchEntryByName(coszName);
               if( pCurrentEntry == OSAL_NULL )
               {
                  pCurrentEntry = tSemaphoreTableGetFreeEntry ();
                  if( pCurrentEntry != OSAL_NULL )
                  {
                     if((pCurrentEntry->hSemaphore = CreSyncObj(coszName,uCount)) != (tU32)OSAL_ERROR)
                     {
                        pCurrentEntry->bIsUsed        = TRUE;
                        pCurrentEntry->bToDelete      = FALSE;
                        pCurrentEntry->u16OpenCounter = 1;
                        pCurrentEntry->u16SemMaxValue = 0xffff;
                        pCurrentEntry->u16SemValue    = (tU16)uCount+1;
                        pCurrentEntry->bTraceSem      = pOsalData->bSemTaceAll;
                        pCurrentEntry->bCheckSem      = FALSE;
                        pCurrentEntry->s32PID         = OSAL_ProcessWhoAmI();
                        pCurrentEntry->u16Option      = u16Option;
                        pCurrentEntry->u32LockCount   = 0;
                        if(pOsalData->szCheckSemName[0] != 0)
                        {
                           if(!strncmp(coszName,pOsalData->szCheckSemName,strlen(pOsalData->szCheckSemName)))
                           {
                              pCurrentEntry->bCheckSem = TRUE;
                           }
                        }
                        if(pOsalData->szSemName[0] != 0)
                        {
                           if(!strncmp(coszName,pOsalData->szSemName,strlen(pOsalData->szSemName)))
                           {
                              TraceString("OSALSEM Found Trace Sem %s Count:%u ",coszName,uCount);
                              pCurrentEntry->bTraceSem = TRUE;
                           }
                        }

                        (void)OSAL_szStringNCopy ((tString)pCurrentEntry->szName,
                                          coszName,LINUX_C_SEM_MAX_NAMELENGHT);
                        if((u32ErrorCode = u32GenerateSemaphoreHandle(pCurrentEntry,phSemaphore)) == OSAL_E_NOERROR)
                        {
                            if(uCount == 0)
                            {
                              ((tSemHandle*)*phSemaphore)->s32Cnt = 1;
                            }
                            s32ReturnValue = OSAL_OK;
                            pOsalData->u32SemResCount++;
                        }
#ifdef SEMAPHORE_WARN_LEVEL
extern void vTraceSCB(tBool bUsedOnly);
extern void vGetTopInfo(tBool bErrMemEntry);
                        if(pOsalData->u32SemResCount == pOsalData->u32WarnLevel)
                        {
                            if(pOsalData->u32WarnLevel == SEMAPHORE_WARN_LEVEL)
                            {
                               vGetTopInfo(FALSE);
                            }
                            pOsalData->u32WarnLevel += 50;
                            vTraceSCB(TRUE);
                        }
#endif
                        if(pOsalData->u32MaxSemResCount < pOsalData->u32SemResCount)
                        {
                            pOsalData->u32MaxSemResCount = pOsalData->u32SemResCount;
                            if(pOsalData->u32MaxSemResCount > (pOsalData->u32MaxNrSemaphoreElements*9/10))
                            {
                               pOsalData->u32NrOfChanges++;
                            }
                        }
                     }
                     else
                     {
                         FATAL_M_ASSERT_ALWAYS();
                         u32ErrorCode = u32ConvertErrorCore(errno);
                     }
                  }
                  else
                  {
                     u32ErrorCode = OSAL_E_NOSPACE;
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_ALREADYEXISTS;
               }
             UnLockOsal(&pOsalData->SemaphoreTable.rLock);
            }
         }
         else
         {
           u32ErrorCode = OSAL_E_NAMETOOLONG;
         }//if( OSAL_u32StringLength(coszName) < LINUX_C_SEM_MAX_NAMELENGHT )
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      } // if( coszName )
   }/*if(u32ErrorCode == OSAL_E_NOERROR)*/
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_ALREADYEXISTS)
      {
        if((pCurrentEntry&&(pCurrentEntry->bTraceSem == TRUE))||(u32OsalSTrace & 0x00000040))
        {
           vTraceSemInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
        }
      }
      else
      {
         vTraceSemInfo(pCurrentEntry,CREATE_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_SEM,TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphoreDelete()
 *
 * DESCRIPTION: this function removes an OSAL Semaphore.
 *
 * PARAMETER:   coszName (I)
 *                 semaphore name to be removed.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SemaphoreDelete (tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trSemaphoreElement *pCurrentEntry = NULL;

   if(coszName)
   {
      if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
      {
         pCurrentEntry =  tSemaphoreTableSearchEntryByName (coszName);
         if( pCurrentEntry != OSAL_NULL )
         {
            if( pCurrentEntry->u16OpenCounter == 0 )
            {
               if(pCurrentEntry->bTraceSem == TRUE)
               {
                  tS32 s32Val;
                  if(GetSyncObjVal(pCurrentEntry->hSemaphore,&s32Val)== OSAL_E_NOERROR)
                  {
                     TraceString("Delete Semaphore %s with count %d",pCurrentEntry->szName,s32Val);
                  }
               }
               s32ReturnValue = DelSyncObj(pCurrentEntry->hSemaphore);
               if(s32ReturnValue == OSAL_E_NOERROR)
               {
                  pCurrentEntry->bIsUsed = FALSE;
                  s32ReturnValue = OSAL_OK;
                  pOsalData->u32SemResCount--;
               }
               else
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  s32ReturnValue = OSAL_ERROR;
               }
            }
            else
            {
               pCurrentEntry->bToDelete = TRUE;
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->SemaphoreTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      vTraceSemInfo(pCurrentEntry,DELETE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_SEM,TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,DELETE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphoreOpen()
 *
 * DESCRIPTION: this function returns a valid handle to an OSAL Semaphore
 *              already created.
 *
 * PARAMETER:   coszName (I)
 *                 semaphore name to be removed.
 *              phSemaphore (->O)
 *                 pointer to the semaphore handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SemaphoreOpen (tCString coszName, OSAL_tSemHandle * phSemaphore)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trSemaphoreElement *pCurrentEntry = NULL;

   if( coszName && phSemaphore )  /* FIX 15/10/2002 */
   {
      if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
      {
         pCurrentEntry =  tSemaphoreTableSearchEntryByName (coszName);
         if( pCurrentEntry != OSAL_NULL )
         {
            if(!pCurrentEntry->bToDelete )
            {
               pCurrentEntry->u16OpenCounter++;
               if((u32ErrorCode = u32GenerateSemaphoreHandle(pCurrentEntry,phSemaphore)) == OSAL_E_NOERROR)
               {
                  s32ReturnValue = OSAL_OK;
               }
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->SemaphoreTable.rLock);
      }
      else
      {
         u32ErrorCode = OSAL_E_BUSY;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
         if((pCurrentEntry&&(pCurrentEntry->bTraceSem == TRUE))||(u32OsalSTrace & 0x00000040))
         {
            vTraceSemInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,coszName);
         }
      }
      else
      {
         vTraceSemInfo(pCurrentEntry,OPEN_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_SEM,TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */ 
      }
   }
   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphoreClose()
 *
 * DESCRIPTION: this function closes an OSAL Semaphore.
 *
 * PARAMETER:   hSemaphore (I)
 *                 semaphore handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 ******************************************************************************/
tS32 OSAL_s32SemaphoreClose (OSAL_tSemHandle hSemaphore)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = u32CheckSemaphoreHandle(hSemaphore);
   trSemaphoreElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
      {
         pCurrentEntry = ((tSemHandle*)hSemaphore)->pEntry;
         if((pCurrentEntry != OSAL_NULL )
          &&(pCurrentEntry->u32SemaphoreID == LINUX_C_U32_SEMAPHORE_MAGIC)
          &&( pCurrentEntry->bIsUsed == TRUE))
         {
            if( pCurrentEntry->u16OpenCounter > 0 )
            {
               pCurrentEntry->u16OpenCounter--;
               s32ReturnValue = OSAL_OK;
               if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&SemMemPoolHandle,(tSemHandle*)hSemaphore) == OSAL_ERROR)
               {    NORMAL_M_ASSERT_ALWAYS();  }
               if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
               {
                  if(pCurrentEntry->bTraceSem == TRUE)
                  {
                     tS32 s32Val;
                     if(GetSyncObjVal(pCurrentEntry->hSemaphore,&s32Val)== OSAL_E_NOERROR)
                     {
                        TraceString("Delete Semaphore %s with count %d",pCurrentEntry->szName,s32Val);
                     }
                  }
                  if((u32ErrorCode = DelSyncObj(pCurrentEntry->hSemaphore)) == OSAL_E_NOERROR )
                  {
                     pCurrentEntry->bIsUsed = FALSE;
                     pCurrentEntry->bToDelete = FALSE;
                     s32ReturnValue = OSAL_OK;
                     pOsalData->u32SemResCount--;
                  }
                  else
                  {
                     u32ErrorCode = u32ConvertErrorCore(errno);
                     s32ReturnValue = OSAL_ERROR;
                  }
               }
            }
            else
            {
                u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
         UnLockOsal(&pOsalData->SemaphoreTable.rLock);
      }
   }
   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceSemInfo(pCurrentEntry,CLOSE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_SEM,TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,CLOSE_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphoreGetValue()
 *
 * DESCRIPTION: this function retrieves the value of the lock.
 *
 * PARAMETER:   hSemaphore (I)
 *                 semaphore handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SemaphoreGetValue (OSAL_tSemHandle hSemaphore, tPS32 ps32Value)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = u32CheckSemaphoreHandle(hSemaphore);;
   trSemaphoreElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      pCurrentEntry = ((tSemHandle*)hSemaphore)->pEntry;
      if((pCurrentEntry != OSAL_NULL )
       &&(pCurrentEntry->u32SemaphoreID == LINUX_C_U32_SEMAPHORE_MAGIC)
       &&( pCurrentEntry->bIsUsed == TRUE))
      {
         tS32 s32Val = -1;
         if(GetSyncObjVal(pCurrentEntry->hSemaphore,&s32Val) == OSAL_E_NOERROR)
         {
            pCurrentEntry->u16SemValue = (tU16)s32Val;
            *ps32Value = pCurrentEntry->u16SemValue;
            s32ReturnValue = OSAL_OK;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceSemInfo(pCurrentEntry,STATUS_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,STATUS_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }

   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphorePost()
 *
 * DESCRIPTION:   this function release a semaphore
 *
 *
 * PARAMETER:   hSemaphore (I)
 *                 semaphore handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *****************************************************************************/
tS32 OSAL_s32SemaphorePost (OSAL_tSemHandle hSemaphore)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = u32CheckSemaphoreHandle(hSemaphore);;
   trSemaphoreElement *pCurrentEntry=NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      pCurrentEntry = ((tSemHandle*)hSemaphore)->pEntry;
      if((pCurrentEntry != OSAL_NULL )
       &&(pCurrentEntry->u32SemaphoreID == LINUX_C_U32_SEMAPHORE_MAGIC)
       &&(pCurrentEntry->bIsUsed == TRUE))
      {
         if(pCurrentEntry->u16Option == 1)
         {
            if(pCurrentEntry->s32TID == OSAL_ThreadWhoAmI())
            {
               if(pCurrentEntry->u32LockCount > 1)
               {  
                  pCurrentEntry->u32LockCount--;
                  return OSAL_OK;
               }
               else
               {
                  pCurrentEntry->s32TID = 0;
                  pCurrentEntry->u32LockCount = 0;
               }
            }
         }
         u32ErrorCode = RelSyncObj(pCurrentEntry->hSemaphore);
         if(u32ErrorCode == OSAL_E_NOERROR)
         {
            ((tSemHandle*)hSemaphore)->s32Cnt -= 1;
            pCurrentEntry->u16SemValue++;
            s32ReturnValue = OSAL_OK;
         }
      }
      else
      {
          u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceSemInfo(pCurrentEntry,POST_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,POST_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SemaphoreWait()
 *
 * DESCRIPTION:   This function waits for an OSAL semaphore
 *
 *
 * PARAMETER:     hSemaphore (I)
 *                   semaphore handle.
 *                msec (I)
 *                   max blocking timeout.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *****************************************************************************/
tS32 OSAL_s32SemaphoreWait (OSAL_tSemHandle hSemaphore, OSAL_tMSecond msec)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = u32CheckSemaphoreHandle(hSemaphore);
   trSemaphoreElement *pCurrentEntry = NULL;
   OSAL_tMSecond s32Timeout = msec;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      pCurrentEntry = ((tSemHandle*)hSemaphore)->pEntry;
      if((pCurrentEntry != OSAL_NULL )
       &&(pCurrentEntry->u32SemaphoreID == LINUX_C_U32_SEMAPHORE_MAGIC)
       &&(pCurrentEntry->bIsUsed == TRUE))
      {
         if(pCurrentEntry->u16Option == 1)
         {
            if((pCurrentEntry->s32TID == OSAL_ThreadWhoAmI())&&(pCurrentEntry->u32LockCount))
            {
               pCurrentEntry->u32LockCount++;
               return OSAL_OK;
            }
         }
         if((pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))
         {
             TraceString("OSALSEM TID:%d Start Wait %d msec Sem %s",OSAL_ThreadWhoAmI(),msec,pCurrentEntry->szName);
         }
         if((s32Timeout < pOsalData->u32TimerResolution)&&(s32Timeout != OSAL_C_TIMEOUT_NOBLOCKING))
         {
            s32Timeout = pOsalData->u32TimerResolution;
         }
         pCurrentEntry->u16SemValue--;
         if(pCurrentEntry->bCheckSem)
         {
             u32ErrorCode = WaiSyncObj( pCurrentEntry->hSemaphore,pOsalData->u32CheckSemTmo);
             if(u32ErrorCode == OSAL_E_TIMEOUT)
             {
                char Name[16];
                int i;
                bGetThreadNameForTID(&Name[2],8,pCurrentEntry->s32TID);
                vWritePrintfErrmem("Semaphore %s blocked for %d msec from Task %d %s \n",
                         pCurrentEntry->szName,
                         pOsalData->u32CheckSemTmo,
                         pCurrentEntry->s32TID,
                         Name);
                if((i = u32GetSigRtMinId(pCurrentEntry->s32PID)) == -1)
                {
                   i = SIG_BACKTRACE;
                }
                /* get callstack of owner */
                if(kill(pCurrentEntry->s32PID,i) == -1)
                {   
                   TraceString("SEM:%s Kill SIG_BACKTRACE failed Error:%d ",pCurrentEntry->szName,errno);
                }
                u32ErrorCode = WaiSyncObj( pCurrentEntry->hSemaphore, s32Timeout );
             }
         }
         else
         {
            u32ErrorCode = WaiSyncObj( pCurrentEntry->hSemaphore, s32Timeout );
         }
         if(pCurrentEntry->u16Option == 1)
         {
            pCurrentEntry->s32TID = OSAL_ThreadWhoAmI();
            pCurrentEntry->u32LockCount++;
         }
         if(u32ErrorCode == OSAL_E_NOERROR)
         {
            ((tSemHandle*)hSemaphore)->s32Cnt += 1;
            s32ReturnValue = OSAL_OK;
         }
         else
         {
            pCurrentEntry->u16SemValue++;
            s32ReturnValue = OSAL_ERROR;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      if(u32ErrorCode != OSAL_E_TIMEOUT)
      {
         vTraceSemInfo(pCurrentEntry,WAIT_OPERATION,TRUE,u32ErrorCode,coszName); 
      }
      else
      {
         if((pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */
         {
            vTraceSemInfo(pCurrentEntry,WAIT_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
         }
      }
   }
   else
   {
      if((pCurrentEntry->bTraceSem == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceSemInfo(pCurrentEntry,WAIT_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return s32ReturnValue;
}

/*void vPrintSemHandles(void)
{
  trOSAL_MPF* pPool = (trOSAL_MPF*)SemMemPoolHandle.pMem;
  trSemaphoreElement *pCurrentEntry = NULL;
  uintptr_t *pMem;
  tS32 s32Pid;
  tSemHandle* pTmp;
  tU32 i;

  if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
  {
     for(i = 0;i<pPool->mpfcnt;i++)
     {
        pMem = (uintptr_t*)((uintptr_t)SemMemPoolHandle.u32memstart + (i * pPool->blfsz));
        s32Pid = *pMem;
        pMem++;
        if(*pMem != FREEMARKER)
        {
           pMem++;
           pTmp = (tSemHandle*)pMem;
           pCurrentEntry = ((tSemHandle*)pTmp)->pEntry;
           if(pCurrentEntry->u32SemaphoreID == LINUX_C_U32_SEMAPHORE_MAGIC)
           {
              TraceString("OSAL_SEM PID:%d TID:%d Name:%s",s32Pid, pTmp->s32Tid,pCurrentEntry->szName);
           }
           else
           {
              TraceString("OSAL_SEM Wrong Element address PID:%d",s32Pid);
           }
         }
     }
     UnLockOsal(&pOsalData->SemaphoreTable.rLock);
  }
}*/

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
