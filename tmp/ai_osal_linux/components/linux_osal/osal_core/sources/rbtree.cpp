/*****************************************************************************
| FILE:         rbtee.cpp
| PROJECT:      Platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) red-FALSE tree implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 19.06.2013| Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#ifdef RBTREE_SEARCH

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
//#define TRACE_RBTREE
//#define CHECK_ALL_ENTRIES
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
 

/*****************************************************************************
 *
 * FUNCTION:    vPrintAllNodes
 *
 * DESCRIPTION: This function print content of all (occuoied and free) nodes 
 *              from the OSAL shared memory
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
void vPrintAllNodes(trRBTreeData* pDat)
{
   trElementNode *x = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
   trElementNode* pElements = (trElementNode*)(pDat->OffsetData + (intptr_t)pDat);
   unsigned int i,j=0;
   
   TraceString("Head NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d",
            pDat->HeadOffset,x->key,x->u32Index,x->bRed,x->Offsetl,x->Offsetr,x->Offsetp);


   for(i = 0;i < pDat->u32MaxElements;i++)
   {
        x = (trElementNode*)&pElements[i];
        if(x->u32Index != (tU32)-1)
        {
          TraceString( "NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d Offsetl:0x%x Offsetr:0x%x Offsetp:0x%x",
                   (intptr_t)x - (intptr_t)pDat,x->key,x->u32Index,x->bRed,x->Offsetl,x->Offsetr,x->Offsetp);
          j++;
        }
   }
   trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
   TraceString("End NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d Offsetl:0x%x Offsetr:0x%x Offsetp:0x%x",
            (intptr_t)z - (intptr_t)pDat,z->key,z->u32Index,z->bRed,z->Offsetl,z->Offsetr,z->Offsetp);
   TraceString("Counter:%d Count Check:%d",pDat->u32UsedElements,j);
}

void vPrintAllNodesToErrmem(trRBTreeData* pDat)
{
   trElementNode *x = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
   trElementNode* pElements = (trElementNode*)(pDat->OffsetData + (intptr_t)pDat);
   unsigned int i;
   
   vWritePrintfErrmem("Head NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d \n",
            pDat->HeadOffset,x->key,x->u32Index,x->bRed,x->Offsetl,x->Offsetr,x->Offsetp);


   for(i = 0;i < pDat->u32MaxElements;i++)
   {
        x = (trElementNode*)&pElements[i];
        if(x->u32Index != (tU32)-1)
        {
          vWritePrintfErrmem( "NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d Offsetl:0x%x Offsetr:0x%x Offsetp:0x%x \n",
                   (intptr_t)x - (intptr_t)pDat,x->key,x->u32Index,x->bRed,x->Offsetl,x->Offsetr,x->Offsetp);
        }
   }
   trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
   vWritePrintfErrmem("End NodeOffset:x:0x%x key:%d u32Index:%d bRed:%d Offsetl:0x%x Offsetr:0x%x Offsetp:0x%x \n",
            (intptr_t)z - (intptr_t)pDat,z->key,z->u32Index,z->bRed,z->Offsetl,z->Offsetr,z->Offsetp);
}


void vCkeckAllEntries(trRBTreeData* pDat)
{
#ifdef CHECK_ALL_ENTRIES
   trElementNode *x,*y;
   trElementNode* pElements = (trElementNode*)(pDat->OffsetData + (intptr_t)pDat);
   trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
   tS32 NodeOffset;
   tU32 i;

   for(i = 0;i < pDat->u32MaxElements;i++)
   {
      x = (trElementNode*)&pElements[i];
      if(x->u32Index != (intptr_t)-1)
      {
         y = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
         if(x->key != y->key)
         {
            while(y != z)
            {
               NodeOffset = (x->key < y->key) ? y->Offsetl : y->Offsetr;
               y = (trElementNode*)(NodeOffset + (intptr_t)pDat);
               if(x->key == y->key)break;
            }
            if(y == z)
            {
               TraceString("Cannot find Node Offset:x:0x%x key:%d u32Index:%d bRed:%d Offsetl:0x%x Offsetr:0x%x Offsetp:0x%x",
                        (intptr_t)x- (intptr_t)pDat,x->key,x->u32Index,x->bRed,x->Offsetl,x->Offsetr,x->Offsetp);
            }
         }
      }
   }
#else
   ((void)pDat);
#endif
}

/*****************************************************************************
 *
 * FUNCTION:    vInitNodes
 *
 * DESCRIPTION: This function initialize all nodes and set the head and end node
 *              of the RB tree
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
void vInitNodes(tU32 NrOfEl,trRBTreeData* pDat,trElementNode* pElements,trOsalLock* pLock)
{
#ifdef TRACE_RBTREE
   TraceString("vInitStartNodes pElements:0x%x",pElements);
#endif

   unsigned int i;
   pDat->OffsetData  = (intptr_t)pElements - (intptr_t)pDat;
   if(pLock)pDat->OffSetLock  = (intptr_t)pLock - (intptr_t)pDat;
   else pDat->OffSetLock  = 0;
   pDat->u32MaxElements = NrOfEl-1;
   
   for(i=0;  i < pDat->u32MaxElements; i++)
   {
       pElements[i].u32Index = OSAL_C_INVALID_HANDLE;
   } 
   
   /* fill head&end node */
   pDat->MinOffset = (intptr_t)&pElements[0] - (intptr_t)pDat;
   trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
   pDat->MaxOffset = (intptr_t)z - (intptr_t)pDat;

   pDat->HeadOffset  = pDat->MaxOffset;
   
   z->key        = (tS32)OSAL_C_INVALID_HANDLE;
   z->bRed       = FALSE; 
   z->Offsetl = (intptr_t)z - (intptr_t)pDat;
   z->Offsetr = (intptr_t)z - (intptr_t)pDat;
   z->Offsetp = 0;
   z->u32Index   = OSAL_C_INVALID_HANDLE;
}

/*****************************************************************************
 *
 * FUNCTION:    pGetFreeNodeElement
 *
 * DESCRIPTION: This function search  a free node in the OSAL shared memory
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: trElementNode*   address of a available node or NULL in error case
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
trElementNode* pGetFreeNodeElement(trRBTreeData* pDat)
{
#ifdef TRACE_RBTREE
   TraceString("pGetFreeNodeElement ");
#endif

   tU32 u32Count;
   trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
   for(u32Count=0;  u32Count < (pDat->u32MaxElements); u32Count++)
   {
      if(pElements[u32Count].u32Index == OSAL_C_INVALID_HANDLE)break;
   }
   if(u32Count < (pDat->u32MaxElements))return (trElementNode*)&pElements[u32Count];
   else return NULL;
}


/*****************************************************************************
 *
 * FUNCTION:    rotateLeft
 *
 * DESCRIPTION: This function accomplish a rotation around a of a given node 
 *              in the left direction
 *
 * PARAMETER:   trElementNode*   address of a available node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static tBool rotateLeft(trRBTreeData* pDat,trElementNode *x)
{
#ifdef TRACE_RBTREE
   TraceString( "rotateLeft for node:0x%x ",(intptr_t)x-(intptr_t)pDat);
#endif
    tBool bCalcNewHead = FALSE;
    trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
    trElementNode *tmp,*y,*z;
    if(x)
    {
       z = (trElementNode*)&pElements[pDat->u32MaxElements];
       // rotate node x to left
       y = (trElementNode*)(x->Offsetr + (intptr_t)pDat);

       if(y != z)
       {
          // establish x->right link
          x->Offsetr = y->Offsetl;
          if((trElementNode*)(y->Offsetl + (intptr_t)pDat) != z) 
          {
             tmp = (trElementNode*)(y->Offsetl + (intptr_t)pDat);
             tmp->Offsetp = (intptr_t)x - (intptr_t)pDat;
          }

          // establish y->parent link
          if (y != z)
          {
             y->Offsetp = x->Offsetp;
          }
          if (x->Offsetp) 
          {
#ifdef TRACE_RBTREE
             TraceString("rotateLeft node:0x%x ",(intptr_t)x - (intptr_t)pDat);
#endif
             tmp = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
             if (x == (trElementNode*)(tmp->Offsetl + (intptr_t)pDat))
             {
                tmp->Offsetl = (intptr_t)y - (intptr_t)pDat;
             }
             else
             {
                tmp->Offsetr = (intptr_t)y - (intptr_t)pDat;
             }
          }
          else 
          {
             pDat->HeadOffset = (intptr_t)y - (intptr_t)pDat;
             bCalcNewHead = TRUE;
#ifdef TRACE_RBTREE
             TraceString("rotateLeft new head node:0x%x ",pDat->HeadOffset);
#endif
          }
          // link x and y
          y->Offsetl = (intptr_t)x - (intptr_t)pDat;
          if (x != z)
          {
             x->Offsetp = (intptr_t)y - (intptr_t)pDat;
          }
       }
       else
       {
#ifdef TRACE_RBTREE
            TraceString( "rotateLeft node:0x%x not possible y == z",(intptr_t)x- (intptr_t)pDat);
#endif
       }

    }
    else
    {
#ifdef TRACE_RBTREE
       TraceString( "rotateLeft node:0x%x not possible ",x);
#endif
    }
    return bCalcNewHead;
}

/*****************************************************************************
 *
 * FUNCTION:    rotateRight
 *
 * DESCRIPTION: This function accomplish a rotation around a of a given node 
 *              in the right direction
 *
 * PARAMETER:   trElementNode*   address of a available node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static tBool rotateRight(trRBTreeData* pDat,trElementNode *x) 
{
#ifdef TRACE_RBTREE
   TraceString("rotateRight for node:0x%x ",(intptr_t)x-(intptr_t)pDat);
#endif
   tBool bCalcNewHead = FALSE;
   trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
   trElementNode *tmp,*y,*z;
   if(x)
   {
      z = (trElementNode*)&pElements[pDat->u32MaxElements];
      // rotate node x to right
      y = (trElementNode*)(x->Offsetl + (intptr_t)pDat);

      if(y != z)
      {
         // establish x->left link
         x->Offsetl = y->Offsetr;
         if ((trElementNode*)(y->Offsetr + (intptr_t)pDat) != z)
         {
            tmp = (trElementNode*)(y->Offsetr + (intptr_t)pDat);
            tmp->Offsetp = (intptr_t)x - (intptr_t)pDat;
         }

         // establish y->parent link
         if (y != z)
         {
            y->Offsetp = x->Offsetp;
         }
         if (x->Offsetp) 
         {
#ifdef TRACE_RBTREE
            TraceString("rotateRight node:0x%x ",(intptr_t)x - (intptr_t)pDat);
#endif
            tmp = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
            if (x == (trElementNode*)(tmp->Offsetr + (intptr_t)pDat))
            {
               tmp->Offsetr = (intptr_t)y - (intptr_t)pDat;
            }
            else
            {
               tmp->Offsetl = (intptr_t)y - (intptr_t)pDat;
            }
         }
         else 
         {
            pDat->HeadOffset = (intptr_t)y - (intptr_t)pDat;
            bCalcNewHead = TRUE;
#ifdef TRACE_RBTREE
            TraceString( "rotateRight new head node:0x%x ",pDat->HeadOffset);
#endif
         }
         // link x and y
         y->Offsetr = (intptr_t)x - (intptr_t)pDat;
         if (x != z) 
         {
            x->Offsetp = (intptr_t)y - (intptr_t)pDat;
         }
      }
      else
      {
#ifdef TRACE_RBTREE
          TraceString( "rotateRight node:0x%x not possible y == z",(intptr_t)x- (intptr_t)pDat);
#endif
      }
   }
   else
   {
#ifdef TRACE_RBTREE
      TraceString( "rotateRight node:0x%x not possible ",x);
#endif
   }
   return bCalcNewHead;
}

/*****************************************************************************
 *
 * FUNCTION:    vCheckBalanceInsert
 *
 * DESCRIPTION: This function ensures a balanced tree after insert a new node
 *              1. Every node is either red or black.
 *              2. Every leaf (NIL) is black.
 *              3. If a node is red, then both its children are black.
 *              4. Every simple path from a node to a descendant leaf contains the same number of black nodes.
 *
 * PARAMETER:   trElementNode*   address of a available node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static void vCheckBalanceInsert(trRBTreeData* pDat,trElementNode *x)
{
#ifdef TRACE_RBTREE
   TraceString("vCheckBalanceInsert for node:0x%x ",(intptr_t)x-(intptr_t)pDat);
   tU32 Indx = 0;
#endif
    // maintain red-FALSE tree balance
    // after inserting node x
    trElementNode* head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
    trElementNode* xp  = NULL;
    trElementNode* xpp = NULL;
    if(x->Offsetp)
    {
       xp = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
       if (xp->bRed == TRUE)
       {
          xpp = (trElementNode*)(xp->Offsetp + (intptr_t)pDat);
       }
       else
       {
          xpp = xp;
       }
    }
    else
    {
       x = head;
       xp = x;   // satify lint
       xpp = xp; // satify lint
    }
    trElementNode *y   = 0;

    // check red-FALSE properties
    while ((x != head) && (xp->bRed == TRUE)) 
    {
#ifdef TRACE_RBTREE
        TraceString("vCheckBalanceInsert start x:0x%x xp:0x%x xpp:0x%x",
                    (intptr_t)x-(intptr_t)pDat,(intptr_t)xp-(intptr_t)pDat,(intptr_t)xpp-(intptr_t)pDat);
#endif
        // we have a violation
        if (x->Offsetp == xpp->Offsetl)
        {
            y = (trElementNode*)(xpp->Offsetr + (intptr_t)pDat);
            if (y->bRed == TRUE) 
            {
               // uncle is RED
               xp->bRed = FALSE;
               y->bRed = FALSE;
               xpp->bRed = TRUE;
               x = xpp;
#ifdef TRACE_RBTREE
               Indx = 1;
#endif
            }
            else
            {
               // uncle is black
               if(x == (trElementNode*)(xp->Offsetr + (intptr_t)pDat))
               {
                  // make x a left child
                  x = xp;
                  if( rotateLeft(pDat,x) == TRUE)
                  {
                     head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                  }
               }
               if(x->Offsetp)
               {
                  xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
                  if(xp->Offsetp)
                  {
                     xpp = (trElementNode*)(xp->Offsetp + (intptr_t)pDat);
                  }
                  else
                  {
                     xpp = xp;
                  }
               }
               else
               {
                  xp = x;
               }
               // recolor bRed and rotate
               xp->bRed = FALSE;
               xpp->bRed = TRUE;
               if(rotateRight(pDat,xpp) == TRUE)
               {
                  head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
               }
#ifdef TRACE_RBTREE
               Indx = 2;
#endif
            }
        }
        else
        {
            // mirror image of above code
            y = (trElementNode*)(xpp->Offsetl + (intptr_t)pDat);
            if (y->bRed == TRUE) 
            {
                // uncle is RED
                xp->bRed = FALSE;
                y->bRed = FALSE;
                xpp->bRed = TRUE;
                x = xpp;
#ifdef TRACE_RBTREE
                Indx = 4;
#endif
            }
            else
            {
               // uncle is BLACK
               if (x == (trElementNode*)(xp->Offsetl + (intptr_t)pDat))
               {
                  x = xp;
                  if(rotateRight(pDat,x) == TRUE)
                  {
                     head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                  }
               }
               if(x->Offsetp)
               {
                  xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
                  if(xp->Offsetp)
                  {
                     xpp = (trElementNode*)(xp->Offsetp + (intptr_t)pDat);
                  }
                  else
                  {
                     xpp = xp;
                  }
               }
               else
               {
                  xp = x;
               }
               xp->bRed = FALSE;
               xpp->bRed = TRUE;
               if(rotateLeft(pDat,xpp) == TRUE)
               {
                  head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
               }
#ifdef TRACE_RBTREE
               Indx = 5;
#endif
            }
        }
        // set x->parent and x->parant-parent again when x has changed
        if(x->Offsetp)
        {
           xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
           if(xp->Offsetp)
           {
              xpp = (trElementNode*)(xp->Offsetp + (intptr_t)pDat);
           }
           else
           {
              xpp = xp;
           }
        }
        else
        {
           x = xp;
        }
 #ifdef TRACE_RBTREE
        TraceString("vCheckBalanceInsert round Idx:%d",Indx);
        TraceString("vCheckBalanceInsert x:0x%x xp:0x%x xpp:0x%x y:0x%x",
                   (intptr_t)x-(intptr_t)pDat,(intptr_t)xp-(intptr_t)pDat,(intptr_t)xpp-(intptr_t)pDat,(intptr_t)y-(intptr_t)pDat);
#endif
    }
    //head node always is black
    ((trElementNode*)(pDat->HeadOffset + (intptr_t)pDat))->bRed = FALSE;
#ifdef TRACE_RBTREE
   TraceString("vCheckBalanceInsert for node:0x%x finished",(intptr_t)x-(intptr_t)pDat);
#endif
}

/*****************************************************************************
 *
 * FUNCTION:    vInsertElement
 *
 * DESCRIPTION: This function inserts a node with the given key and data
 *
 * PARAMETER:   tS32   key of the new node
 *              tS32   data for the new node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
void vInsertElement(trRBTreeData* pDat,tS32 key,tS32 s32Data)
{
#ifdef TRACE_RBTREE
   TraceString("vInsertElement with key:%d and Data:%d head:0x%x",key,s32Data,pDat->HeadOffset);
#endif
   if((pDat)&&(!pDat->bInValidRbTree))
   {

    intptr_t NodeOffset;
//    trThreadElement *pCurrent = NULL;
    trElementNode* c = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat); //set head node
    trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
    trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
    trElementNode *p, *x = NULL;
    pDat->bChangeInProgress = TRUE;
//#ifdef TRACE_RBTREE_CHECK
    tU32 u32Count = 0;
//#endif
    // allocate node for data and insert in tree

    // find future parent
    p = 0;
    while (c != z)
    {
          /* each key can only exist one time */
          if (key == c->key)
          {
             x = pSearchElement(pDat,key,TRUE);
             if(x)
             {
                if(x->u32Index < pDat->u32MaxElements)
                {
                 //   pCurrent = &pDat->rThreadElement[x->u32Index];
                 //   vWritePrintfErrmem("vInsertElement found double key:%d for Task %s \n",key,pCurrent->szName);
                    vWritePrintfErrmem("vInsertElement found double key:%d \n",key);
                    break;
                }
             }
             vWritePrintfErrmem("vInsertElement found double key:%d for not identified Task \n",key);
             x = c;
             break;
          }
          p = c;
          NodeOffset = (key < c->key) ? c->Offsetl : c->Offsetr;
          c = (trElementNode*)(NodeOffset + (intptr_t)pDat);
//#ifdef TRACE_RBTREE_CHECK
          u32Count++;
          if(u32Count > pDat->u32UsedElements)
          {
             pDat->bInValidRbTree = TRUE;
         vPrintAllNodes(pDat);
         //    vPrintAllNodesToErrmem(pDat);
             NORMAL_M_ASSERT_ALWAYS();
             break;
          }
//#endif
    }

//#ifdef TRACE_RBTREE_CHECK
    if(u32Count < pDat->u32MaxElements+1)
    {
//#endif
       if(x == NULL)
       {
          pDat->u32UsedElements++;
          // setup new node, otherwise overwrite node
          x = pGetFreeNodeElement(pDat);
       }
       if(x == NULL)
       {
         vPrintAllNodes(pDat);
        // vPrintAllNodesToErrmem(pDat);
         OSAL_s32ThreadWait(1000);
         FATAL_M_ASSERT_ALWAYS();
       }
       else
       {
          x->key        = key;
          x->bRed       = TRUE; 
          x->Offsetl = (intptr_t)z - (intptr_t)pDat;
          x->Offsetr = (intptr_t)z - (intptr_t)pDat;
          /* when parent exist set pointer*/
          if(p)
          {
             x->Offsetp = (intptr_t)p - (intptr_t)pDat;
          }
          else
          {
             x->Offsetp = 0;
          }
          x->u32Index   = (tU32)s32Data;
#ifdef TRACE_RBTREE
         TraceString( "vInsertElement New Element x:0x%x with data:%d  p:0x%x ",
                     (intptr_t)x-(intptr_t)pDat,x->u32Index,x->Offsetp);
#endif
       }
       // insert node in tree
       if(p) 
       {
          if(key < p->key)
          {
             p->Offsetl = (intptr_t)x - (intptr_t)pDat;
          }
          else
          {
             p->Offsetr = (intptr_t)x - (intptr_t)pDat;
          }
       } 
       else 
       {
          /* set x as new head */
          pDat->HeadOffset = (intptr_t)x - (intptr_t)pDat;
#ifdef TRACE_RBTREE
          TraceString("vInsertElement new head:0x%x ",pDat->HeadOffset);
#endif
       }
       /* check balance */
       vCheckBalanceInsert(pDat,x);
#ifdef TRACE_RBTREE
       vCkeckAllEntries(pDat);
       vPrintAllNodes(pDat);
//   OSAL_s32ThreadWait(100);
       TraceString("vInsertElement with key:%d and Data:%d finished %d Elements",key,s32Data,pDat->u32UsedElements);
#endif
//#ifdef TRACE_RBTREE_CHECK
    }
//#endif
    pDat->bChangeInProgress = FALSE;
  }//if(!pDat->bInValidRbTree)

}

/*****************************************************************************
 *
 * FUNCTION:    vCheckBalanceRemove
 *
 * DESCRIPTION: This function ensures a balanced tree after removing a node
 *              1. Every node is either red or black.
 *              2. Every leaf (NIL) is black.
 *              3. If a node is red, then both its children are black.
 *              4. Every simple path from a node to a descendant leaf contains the same number of black nodes.
 *
 * PARAMETER:   trElementNode*   address of a available node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static void vCheckBalanceRemove(trRBTreeData* pDat,trElementNode *x) 
{
#ifdef TRACE_RBTREE
   TraceString("vCheckBalanceRemove for node:0x%x",(intptr_t)x-(intptr_t)pDat);
   tU32 Indx = 0;
#endif
    /* maintain red-FALSE tree balance after deleting node x */
    trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
    trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
    trElementNode* head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
    trElementNode *xp   = 0;
    trElementNode *wr = 0;
    trElementNode *wl = 0;
    trElementNode *w = 0;
    while ((x != head) && (x->bRed == FALSE))
    {
        xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
#ifdef TRACE_RBTREE
        TraceString("vCheckBalanceRemove next round for node x:0x%x xp:0x%x",(intptr_t)x-(intptr_t)pDat,(intptr_t)xp-(intptr_t)pDat);
#endif
        if (x == (trElementNode*)(xp->Offsetl + (intptr_t)pDat))
        {
           w = (trElementNode*)(xp->Offsetr + (intptr_t)pDat);
           if ((w != z)&&(w->bRed == TRUE))
           {
              w->bRed = FALSE;
              xp->bRed = TRUE;
              if(rotateLeft(pDat,xp) == TRUE)
              {
                  head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
              }
              if(x->Offsetp)
              {
                  xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
              }
              else
              {
                  xp = x;
              }
              w =(trElementNode*)(xp->Offsetr + (intptr_t)pDat);
           }
           wl = (trElementNode*)(w->Offsetl + (intptr_t)pDat);
           wr = (trElementNode*)(w->Offsetr + (intptr_t)pDat);
           if(/*(wl != z)&&(wr != z)&&*/(wl->bRed == FALSE)&&(wr->bRed == FALSE))
           {
               w->bRed = TRUE;
               x = xp;
#ifdef TRACE_RBTREE
               Indx = 1;
#endif
           }
           else
           {
              //if((wr !=z)&&(wl != z)&&(wr != z)
              {
                 if(wr->bRed == FALSE)
                 {
                    wl->bRed = FALSE;
                    w->bRed = TRUE;
                    if(rotateRight (pDat,w) == TRUE)
                    {
                       head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                    }
                    if(x->Offsetp)
                    {
                        xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
                    }
                    else
                    {
                        xp = x;
                    }
                    w = (trElementNode*)(xp->Offsetr + (intptr_t)pDat);
                 }
                 w->bRed = xp->bRed;
                 xp->bRed = FALSE;
                 wr->bRed = FALSE;
                 if( rotateLeft(pDat,xp) == TRUE)
                 {
                    head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                 }
                 x = head;
#ifdef TRACE_RBTREE
                 Indx = 2;
#endif
              }
        /*      else
              {
#ifdef TRACE_RBTREE
                 Indx = 3;
#endif 
                 break;
              }//if(wr !=z)*/
           }
        }
        else //if (x == (trElementNode*)(xp->Offsetl + (intptr_t)pDat))
        {
           w = (trElementNode*)(xp->Offsetl + (intptr_t)pDat);
          // if(w != z)
           {
              if (w->bRed == TRUE)
              {
                 w->bRed = FALSE;
                 xp->bRed = TRUE;
                 if(rotateRight (pDat, xp) == TRUE)
                 {
                     head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                 }
                 if(x->Offsetp)
                 {
                    xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
                 }
                 else
                 {
                    xp = x;
                 }
                 w = (trElementNode*)(xp->Offsetl + (intptr_t)pDat);
              }
              wl = (trElementNode*)(w->Offsetl + (intptr_t)pDat);
              wr = (trElementNode*)(w->Offsetr + (intptr_t)pDat);
              if(/*(wl !=z)&&(wr !=z)&&*/(wl->bRed == FALSE)&&(wr->bRed == FALSE))
              {
                 w->bRed = TRUE;
                 x = xp;
#ifdef TRACE_RBTREE
                 Indx = 4;
#endif
              }
              else
              {
            //     if((w !=z)&&(wl != z)&&(wr != z))
                 {
                    if((wl->bRed == FALSE))
                    {
                        wr->bRed = FALSE;
                        w->bRed = TRUE;
                        if(rotateLeft(pDat, w) == TRUE)
                        {
                          head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                        }
                        if(x->Offsetp)
                        {
                           xp  = (trElementNode*)(x->Offsetp + (intptr_t)pDat);
                        }
                        else
                        {
                            xp = x;
                        }
                        w = (trElementNode*)(xp->Offsetl + (intptr_t)pDat);
                    }
                    w->bRed = xp->bRed;
                    xp->bRed = FALSE;
                    wl->bRed = FALSE;
                    if(rotateRight(pDat, xp) == TRUE)
                    {
                       head = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
                    }
                    x = head;
#ifdef TRACE_RBTREE
                    Indx = 5;
#endif
                 }
            /*     else
                 {
#ifdef TRACE_RBTREE
                    Indx = 6;
#endif 
                    break;
                 }*/
              }
           }
 /*          else
           {
#ifdef TRACE_RBTREE
             Indx = 7;
#endif  
             break;
           }*/
       }
#ifdef TRACE_RBTREE
        TraceString("vCheckBalanceRemove x:0x%x xp:0x%x wl:0x%x wr:0x%x w:0x%x",
                 (intptr_t)x-(intptr_t)pDat,(intptr_t)xp-(intptr_t)pDat,(intptr_t)wl-(intptr_t)pDat,(intptr_t)wr-(intptr_t)pDat,(intptr_t)w-(intptr_t)pDat);
#endif

        /* check for changed head regarding while condition */
        if(x == head)
        {
#ifdef TRACE_RBTREE
           TraceString("vCheckBalanceRemove Idx:%d head:0x%x",Indx,(intptr_t)head-(intptr_t)pDat);
#endif
        }
    }
    head->bRed = FALSE;
#ifdef TRACE_RBTREE
   TraceString("vCheckBalanceRemove for node:0x%x finished",(intptr_t)x-(intptr_t)pDat);
#endif
}

/*****************************************************************************
 *
 * FUNCTION:    vRemoveElement
 *
 * DESCRIPTION: This function removes a node with the given key 
 *
 * PARAMETER:   tS32   key of the new node
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
void vRemoveElement(trRBTreeData* pDat,tS32 key)
{
#ifdef TRACE_RBTREE
   TraceString("vRemoveElement key:%d ",key);
#endif
   tBool bFound = FALSE;

   if((pDat)&&(!pDat->bInValidRbTree))
   {

   pDat->bChangeInProgress = TRUE;

   tS32 NodeOffset;
   trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
   trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
   trElementNode *del = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
   trElementNode *x, *y, *yp;


   if(del == z)
   {
      TraceString("vRemoveElement %d FATAL ERRROR head==z",key);
   }
   else
   {
     /* to identify head node*/
     bFound = TRUE;
     /* check for head element */
     if(key != del->key)
     {
        bFound = FALSE;
        while(del != z)
        {
           NodeOffset = (key < del->key) ? del->Offsetl : del->Offsetr;
           del = (trElementNode*)(NodeOffset + (intptr_t)pDat);
           if(key == del->key)
           {
              bFound = TRUE;
              break;
           }
        }
      }
      else
      {
         /* head node should be removed */
         if(pDat->u32UsedElements == 1)
         {
            del->u32Index   = OSAL_C_INVALID_HANDLE;
            del->key        = 0;
            del->bRed       = FALSE; 
            del->Offsetl = 0;
            del->Offsetr = 0;
            del->Offsetp = 0;
            pDat->MaxOffset = (intptr_t)z - (intptr_t)pDat;
            pDat->HeadOffset = pDat->MaxOffset;
            pDat->u32UsedElements--;
            bFound = FALSE;
         }
         else
         {
           if((del->Offsetl == ((intptr_t)z-(intptr_t)pDat))
            ||(del->Offsetr == ((intptr_t)z-(intptr_t)pDat)))
           {
              if(del->Offsetl == ((intptr_t)z-(intptr_t)pDat))
              {
#ifdef TRACE_RBTREE
                TraceString("vRemoveElement  node:0x%x left node = z",(intptr_t)del-(intptr_t)pDat);
#endif
                 x = (trElementNode*)(del->Offsetr + (intptr_t)pDat);
                 x->Offsetp = 0;
              }
              else
              {
#ifdef TRACE_RBTREE
                TraceString("vRemoveElement  node:0x%x right node = z",(intptr_t)del-(intptr_t)pDat);
#endif
                 x = (trElementNode*)(del->Offsetl + (intptr_t)pDat);
                 x->Offsetp = 0;
              }
              x->bRed = FALSE;
              pDat->HeadOffset = (intptr_t)x - (intptr_t)pDat;
              del->u32Index   = OSAL_C_INVALID_HANDLE;
              del->key        = 0;
              del->bRed       = FALSE; 
              del->Offsetl = 0;
              del->Offsetr = 0;
              del->Offsetp = 0;
              /* no further action necessary*/
              bFound = FALSE;
              pDat->u32UsedElements--;
            }
         }
      }
   }


   if(bFound == TRUE)
   {
           if((del->Offsetl == ((intptr_t)z-(intptr_t)pDat))
            &&(del->Offsetr == ((intptr_t)z-(intptr_t)pDat)))
           {
#ifdef TRACE_RBTREE
                TraceString("vRemoveElement node:0x%x with 2 end nodes",(intptr_t)del-(intptr_t)pDat);
#endif
                 x = (trElementNode*)(del->Offsetp + (intptr_t)pDat);
                 if(x->Offsetr == ((intptr_t)del - (intptr_t)pDat))
                 {
                    x->Offsetr = (intptr_t)z-(intptr_t)pDat;
                 }
                 else
                 {
                    x->Offsetl = (intptr_t)z-(intptr_t)pDat;
                 }
              del->u32Index   = OSAL_C_INVALID_HANDLE;
              del->key        = 0;
              del->bRed       = FALSE; 
              del->Offsetl = 0;
              del->Offsetr = 0;
              del->Offsetp = 0;
              /* no further action necessary*/
              bFound = FALSE;
              pDat->u32UsedElements--;
            }
   }
/*   trElementNode* head =  (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
   if(pDat->u32UsedElements > 3)
   {
   if((head->Offsetl == ((intptr_t)z-(intptr_t)pDat))
    ||(head->Offsetr == ((intptr_t)z-(intptr_t)pDat)))
   {
      if(head->Offsetr == ((intptr_t)z-(intptr_t)pDat))
      {
#ifdef TRACE_RBTREE
         TraceString("vRemoveElement extra rotateRight for head node:0x%x ",(intptr_t)head-(intptr_t)pDat);
#endif
         if(rotateRight(head))
            {
                  y = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
            if(y->bRed == TRUE)
            {
               x = (trElementNode*)(y->Offsetl + (intptr_t)pDat);
               x->bRed = FALSE;               
               x = (trElementNode*)(y->Offsetr + (intptr_t)pDat);
               x->bRed = FALSE;               
                  y->bRed = FALSE;
#ifdef TRACE_RBTREE
               TraceString("vRemoveElement recolor for head node:0x%x ",(intptr_t)y-(intptr_t)pDat);
#endif
            }
         }
      }
      else
      {
#ifdef TRACE_RBTREE
         TraceString("vRemoveElement extra rotateLeft for head node:0x%x ",(intptr_t)head-(intptr_t)pDat);
#endif
         if(rotateLeft(head))
         {
            y = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
            if(y->bRed == TRUE)
            {
               x = (trElementNode*)(y->Offsetl + (intptr_t)pDat);
               x->bRed = FALSE;               
               x = (trElementNode*)(y->Offsetr + (intptr_t)pDat);
               x->bRed = FALSE;               
               y->bRed = FALSE;               
#ifdef TRACE_RBTREE
               TraceString("vRemoveElement recolor for head node:0x%x ",(intptr_t)y-(intptr_t)pDat);
#endif
            }
         }
     }
#ifdef TRACE_RBTREE
      vPrintAllNodes();
#endif
   }
   }

   * check if node was found */
   if(bFound == TRUE)
   {
      pDat->u32UsedElements--;
#ifdef TRACE_RBTREE
      TraceString("vRemoveElement node for key:%d found",key);
#endif
      if(((trElementNode*)(del->Offsetl + (intptr_t)pDat) == z)
       ||((trElementNode*)(del->Offsetr + (intptr_t)pDat) == z))
      {
         // y has an end nodes as a child
         y = del;
         if(y->Offsetp)
         {
            yp = (trElementNode*)(y->Offsetp + (intptr_t)pDat);
         }
         else
         {
            yp = y;
         }
      }
      else
      {
#ifdef TRACE_RBTREE
         TraceString("vRemoveElement find tree successor for key:%d",key);
#endif
         // find tree successor with an end node as a child
         yp = 0;
         y = (trElementNode*)(del->Offsetr + (intptr_t)pDat);
         while ((trElementNode*)(y->Offsetl + (intptr_t)pDat) != z)
         {
            yp = y;
            y = (trElementNode*)(y->Offsetl + (intptr_t)pDat);
        }
#ifdef TRACE_RBTREE
  //       snprintf(au8Buf,100, "vRemoveElement found tree successor for key:%d",key);
   //      TraceString(au8Buf);
#endif
      }
      // x is y's only child
      if((trElementNode*)(y->Offsetl + (intptr_t)pDat) != z)
      {
         x = (trElementNode*)(y->Offsetl + (intptr_t)pDat);
      }
      else
      {
         x = (trElementNode*)(y->Offsetr + (intptr_t)pDat);
      }

      // remove y from the parent chain
      x->Offsetp = y->Offsetp;
      if (y->Offsetp)
      {
         yp = (trElementNode*)(y->Offsetp + (intptr_t)pDat);
         if (y == (trElementNode*)(yp->Offsetl + (intptr_t)pDat))
         {
            yp->Offsetl = (intptr_t)x - (intptr_t)pDat;
         }
         else
         {
            yp->Offsetr = (intptr_t)x - (intptr_t)pDat;
         }
      }
      else
      {
         /* set x as new head */
         pDat->HeadOffset = (intptr_t)x - (intptr_t)pDat;
         /* set pointer to old subtree */
         x->Offsetr = y->Offsetr;
         /* set end pointer to old parent of x */
         if(yp)
         {
             yp->Offsetl = (intptr_t)z - (intptr_t)pDat;
         }
#ifdef TRACE_RBTREE
         TraceString("old:0x%x new:0x%x",(intptr_t)yp - (intptr_t)pDat,(intptr_t)x - (intptr_t)pDat);
#endif
      }
      if (y != del)
      {
         del->key      = y->key;
         del->u32Index = y->u32Index;
      }
      if((y->bRed == FALSE)&&(x!=z))
      {
         vCheckBalanceRemove(pDat,x);
      }

      y->u32Index   = OSAL_C_INVALID_HANDLE;
      y->key        = 0;
      y->bRed       = FALSE; 
      y->Offsetl = 0;
      y->Offsetr = 0;
      y->Offsetp = 0;

   }
   else //if(del != NULL)
   {
#ifdef TRACE_RBTREE
      TraceString("vRemoveElement search key:%d failed",key);
#endif
    }
#ifdef TRACE_RBTREE
   vCkeckAllEntries(pDat);
   vPrintAllNodes(pDat);
//   OSAL_s32ThreadWait(100);
    TraceString( "vRemoveElement key:%d finished %d Elements",key,pDat->u32UsedElements); 
#endif

   pDat->bChangeInProgress = FALSE;
   }//if(!pDat->bInValidRbTree)
}

trElementNode* pSearchElementWithLock(trRBTreeData* pDat, tS32 key)
{
//    char au8Buf[150];
#ifdef TRACE_RBTREE
 //  snprintf(au8Buf,100, "pSearchElementWithLock with key:%d ",key);
 //  TraceString(au8Buf);
#endif
    intptr_t NodeOffset;
    trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
    trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
    trElementNode *current = NULL;
    trOsalLock*  pLock = (trOsalLock*)((intptr_t)pDat + pDat->OffSetLock);
 

  //  if( LockOsalTmo( &pDat->ThreadTable.rLock,3000) == OSAL_OK )
    if( LockOsal(pLock) == OSAL_OK )
    {
       current = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);
       /* check for head element */
       if(key == current->key)
       {
          UnLockOsal(pLock);
          return current;
       }
       while(current != z)
       {
          if(key == current->key)
          {
             UnLockOsal(pLock);
             return current;
          }
          else 
          {
             NodeOffset = (key < current->key) ? current->Offsetl : current->Offsetr;
             if(NodeOffset == 0)
             {
                pDat->bInValidRbTree = TRUE;
                vWritePrintfErrmem("RBTree Overwritten NodeOffset:0x%x key:%d Offsetl:0x%x Offsetr:0x%x\n",
                                   NodeOffset,current->key,current->Offsetl,current->Offsetr);
                vPrintAllNodesToErrmem(pDat);
                return NULL;
             }
             if((NodeOffset > pDat->MaxOffset) 
              ||(NodeOffset < pDat->MinOffset))
             {
                pDat->bInValidRbTree = TRUE;
                vWritePrintfErrmem("RBTree Overwritten NodeOffset:0x%x key:%d Offsetl:0x%x Offsetr:0x%x\n",
                                   NodeOffset,current->key,current->Offsetl,current->Offsetr);
                vPrintAllNodesToErrmem(pDat);
                return NULL;
             }
             current = (trElementNode*)(NodeOffset + (intptr_t)pDat);
          }
       }
       if(current == z)
       {
#ifdef TRACE_RBTREE
          TraceString("pSearchElementWithLock %d failed ,because current == z detected  Tid:%d",key,OSAL_ThreadWhoAmI());
          vPrintAllNodes(pDat);
       OSAL_vProcessExit();
 //         OSAL_s32ThreadWait(1000);
#endif
       }
       UnLockOsal(pLock);
    }
 /*   else
    {
        snprintf(au8Buf,100,"Lock failed after:%d ms",3000);
        TraceString(au8Buf);
    }*/
#ifdef TRACE_RBTREE
    OSAL_vProcessExit();
#endif
    return NULL;
}


/*****************************************************************************
 *
 * FUNCTION:    pSearchElement
 *
 * DESCRIPTION: This function searches a node with the given key 
 *
 * PARAMETER:   tS32   key of the new node
 *
 * RETURNVALUE: trElementNode* addres of the found node or NULL in error case
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 02.07.13  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
trElementNode* pSearchElement(trRBTreeData* pDat, tS32 key,tBool bLocked)
{
  // char au8Buf[150];
#ifdef TRACE_RBTREE
 //  snprintf(au8Buf,100, "pSearchElement with key:%d ",key);
 //  TraceString(au8Buf);
#endif
    intptr_t NodeOffset;
 //    tU32 u32Count = 0;
    trElementNode* pElements = (trElementNode*)((intptr_t)pDat + pDat->OffsetData);
    trElementNode* z    = (trElementNode*)&pElements[pDat->u32MaxElements];
    trElementNode *current = (trElementNode*)(pDat->HeadOffset + (intptr_t)pDat);

    if((pDat == NULL)||(pDat->u32UsedElements == 0))return NULL;

    /* check for head element */
    if(key == current->key)return current;

    /* when tree change in progress and access is done from code without lock */
    if((pDat->bChangeInProgress == TRUE)&&(bLocked == FALSE))
    {
       if(pDat->OffSetLock != 0)
       {
         return pSearchElementWithLock(pDat,key);
       }
       else
       {
          return NULL;
       }
    }

    while(current != z)
    {
        /*  check for insert/remove is happened */
        if((pDat->bChangeInProgress == TRUE)&&(bLocked == FALSE))
        {
           if(pDat->OffSetLock != 0)
           {
              return pSearchElementWithLock(pDat, key);
           }
           else
           {
              return NULL;
           }
        }
        if(key == current->key)
        {
            return current;
        }
        else 
        {
            NodeOffset = (key < current->key) ? current->Offsetl : current->Offsetr;
            if(NodeOffset == 0)
            {
               pDat->bInValidRbTree = TRUE;
               vWritePrintfErrmem("RBTree Overwritten NodeOffset:0x%x key:%d Offsetl:0x%x Offsetr:0x%x\n",
                                   NodeOffset,current->key,current->Offsetl,current->Offsetr);
               vPrintAllNodesToErrmem(pDat);
               return NULL;
            }
            if((NodeOffset > pDat->MaxOffset) 
             ||(NodeOffset < pDat->MinOffset))
            {
                pDat->bInValidRbTree = TRUE;
                vWritePrintfErrmem("RBTree Overwritten NodeOffset:0x%x key:%d Offsetl:0x%x Offsetr:0x%x\n",
                                   NodeOffset,current->key,current->Offsetl,current->Offsetr);
                vPrintAllNodesToErrmem(pDat);
                return NULL;
            }
            current = (trElementNode*)(NodeOffset + (intptr_t)pDat);
        }
 //       u32Count++;
    }
    /*element not found
      check for insert/remove is happened and no lock is used (set/get error)*/
    if((current == z)&&(bLocked == FALSE))
    {
#ifdef TRACE_RBTREE
  //     TraceString("pSearchElement -> pSearchElementWithLock ,because current == z detected");
#endif
       if(pDat->OffSetLock != 0)
       {
          return pSearchElementWithLock(pDat, key);
       }
       else
       {
          return NULL;
       }
    }
    else
    {
#ifdef TRACE_RBTREE
       TraceString("pSearchElement %d -> current == z detected",key);
       vPrintAllNodes(pDat);
  //     OSAL_vProcessExit();
  //     OSAL_s32ThreadWait(500);
#endif
    }
    return NULL;
}



#ifdef __cplusplus
}
#endif

#endif
/************************************************************************
|end of file rbtree.cpp
|-----------------------------------------------------------------------*/



