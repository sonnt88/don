/******************************************************************************
| FILE:            osaldbg.cpp
| PROJECT:        Platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION: This is the implementation file for the OSAL for
|                  Floating Point Exception handling in Linux
|------------------------------------------------------------------------------
| COPYRIGHT: (c) 2011 Robert Bosch Engineering and Business Solutions India Limited
| HISTORY:        
| Date          | Modification                     | Author
| 03.08.2011  | Initial revision                | Anooj Gopi (RBEI/ECF1)
| 26.09.2011  | __USE_GNU macro is removed  | Sudharsanan Sivagnanam (RBEI/ECF1)     |
| --.--.--     | ----------------                | -------, -----
| 17.07.2014  | Exsting osal implementation | sja3kor(RBEI/ECF5)
|                 | "write EM_Trace to usb" is  |
|                 | extended for gen3 security. | 
|                 | Enabled only on                 |
|                 | authorised unlock.             |
|                 |[gen3 security feature]        |
| --.--.--     | ----------------                | -------, -----
| 25.07.2014  | New osal implementation      | sja3kor(RBEI/ECF5)
|                 | "write download trace to usb| 
|                 | on authorised unlock  " is  |
|                 | is added.[gen3 security feature] | 
|*****************************************************************************/ 

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#include<mntent.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

#ifdef FRAMEBUFFER_TEST
#include "osal_testdata.h"
#include <linux/fb.h>
#endif
#ifdef AM_BIN
#include "automounter_types.h"
#include "automounter_api.h"
#include "automounter_api_ctrl.h"
#include "automounter_api_info.h"
#include "automounter_api_events.h"
#endif

//#include <list.h>
struct list_head {
            struct list_head *next, *prev; };

//#include <linux/rbtree.h>
struct rb_node {
    unsigned long  __rb_parent_color;
    struct rb_node *rb_right;
    struct rb_node *rb_left;} __attribute__((aligned(sizeof(long))));

struct posix_msg_tree_node {
    struct rb_node             rb_node;
    struct list_head          msg_list;
    int                            priority;};


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define ER_MEM_CLASS 0x111A
#define ER_MEM_ASSERT_EXCEP  0x111B
#define TR_DEFAULT_STRING_CLASS  0x111D
#define ERMEM_CLASS_LENGTH 2
#define ER_MEM_NEW_LINE 0x111C
#define DYN_MEM_ALLOC_SIZE 255



/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static TR_tenTraceClass old_tr_class = TR_LAST_CLASS;
static tU16 old_entry_type = 0;
static OSAL_trTimeDate     old_Time_stamp = {0,0,0,0,0,0,0,0,0}; 

static OSAL_trSystemLoadTaskInfo tr_sys_load_task_table[OSAL_MAX_NUM_SYS_LOAD_TASKS];
static tBool OSAL_b_isLoadTaskInfoInit;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
extern pFuncTraceOut               TraceOut;
extern pFunc_get_blockmode_status  get_blockmode_status;

/************************************************************************
|function prototype
|-----------------------------------------------------------------------*/


#if !defined (OSAL_GEN3_SIM) /*TARGET CODE GEN3*/
#ifdef PRM_LIBUSB_CONNECTION
extern tVoid vUSBPWRCallbackHandler(void* pvBuffer );
#endif
#endif
extern tU32 u32GetUsedRegistrySize(void);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
int TestFramebuffer(void)
{
#ifdef FRAMEBUFFER_TEST
    int fbfd = 0;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize = 0;
    char *fbp = 0;
    int x = 0, y = 0;
    long int location = 0;

    /* Open the file for reading and writing */
    fbfd = open("/dev/fb0", O_RDWR);
    if (!fbfd) 
    {
        TraceString("Error: cannot open framebuffer device.\n");
        return OSAL_ERROR;
    }
    TraceString("The framebuffer device was opened successfully.\n");

    /* Get fixed screen information */
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) 
    {
      TraceString("Error reading fixed information errno:%d\n",errno);
      return OSAL_ERROR;
    }

    /* Get variable screen information */
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) 
    {
      TraceString("Error reading variable information. errno:%d\n",errno);
      return OSAL_ERROR;
    }

    /* Figure out the size of the screen in bytes */
    screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;
    TraceString("ScreenSize: %d Bytes \n",screensize);

    /* Map the device to memory */
    fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE,
                             MAP_SHARED, fbfd, 0);
    if (fbp == MAP_FAILED) 
    {
        TraceString("Error: failed to map framebuffer device to memory.errno:%d\n",errno);
      return OSAL_ERROR;
    }
    TraceString("The framebuffer device was mapped to memory successfully.\n");

    for (y = 0; y < 240; y++)
    {
        for (x = 0; x < 320; x++)
        {
//x = 100; y = 100; // Where we are going to put the pixel 

            /* Figure out where in memory to put the pixel */
            location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/4) + (y+vinfo.yoffset) * finfo.line_length;
            *(fbp + location) = 255; //Paint a black pixel.
        }
    }

     x = 100, y = 100;         // Where we are going to put the pixel
 
     // Figure out where in memory to put the pixel
     for ( y = 100; y < 200; y++ )
          for ( x = 100; x < 300; x++ ) {
 
                location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) +
                              (y+vinfo.yoffset) * finfo.line_length;
 
                if ( vinfo.bits_per_pixel == 32 ) {
                     *(fbp + location) = 100;          // Some blue
                     *(fbp + location + 1) = 15+(x-100)/2;      // A little green
                     *(fbp + location + 2) = 200-(y-100)/5;     // A lot of red
                     *(fbp + location + 3) = 0;        // No transparency
                } else  { //assume 16bpp
                     int b = 10;
                     int g = (x-100)/6;      // A little green
                     int r = 31-(y-100)/16;     // A lot of red
                     unsigned short int t = r<<11 | g << 5 | b;
                     *((unsigned short int*)(fbp + location)) = t; /*lint -e826*/
                }

          }
#ifdef TEST
		  int idx = 0;
     for ( y = 300; y < 540; y++ )
          for ( x = 300; x < 780; x++ ) {
 
                location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) +
                              (y+vinfo.yoffset) * finfo.line_length;

                *(fbp + location)= Wollmilchsau_480_240RGB565_Data[idx];
                idx++;
          }
#endif
    munmap(fbp, screensize);
    close(fbfd);
#endif
    return 0;
}

void vSetEmptyPoolInvestigation(tU32 u32Val)
{
    pOsalData->rMsgPoolStruct.u32InvestigatePool = u32Val;
}

/******************************************************FunctionHeaderBegin******
 * FUNCTION     : OSAL_vSetAssertMode
 * CREATED      : 2005-08-28
 * AUTHOR        : TMS-Mrh
 * DESCRIPTION : -
 * SYNTAX        : tVoid OSAL_vSetAssertMode(teAssertMode mode)
 * ARGUMENTS    : mode     ASSERTMODE_RESET, ASSERTMODE_SUSPEND,
 *                              ASSERTMODE_TASKSUSPEND or ASSERTMODE_TASKNOACTION
 * RETURN VALUE: -
 *
 * NOTES         : Set Assert mode to one the above values. The assert mode determines system
 *                    behaviour in case of assert. In addition, the behaviour depends on whether
 *                    the software was built in release or debug mode.
 *******************************************************FunctionHeaderEnd******/
void OSAL_vSetAssertMode( teAssertMode mode )
{
    switch( mode )
    {
        case ASSERTMODE_RESET:
        case ASSERTMODE_SUSPEND:
        case ASSERTMODE_TASKSUSPEND: 
        case ASSERTMODE_TASKNOACTION: 
            pOsalData->u32AssertMode = (tU32)mode;
            break;

    default: 
         /* Invalid Assert-mode, sno no good Idea to do an assert here ... */
             //NORMAL_M_ASSERT_ALWAYS();
#ifdef DEBUG
         pOsalData->u32AssertMode = (tU32)ASSERTMODE_TASKNOACTION;
#else
         pOsalData->u32AssertMode = (tU32)ASSERTMODE_RESET;
#endif

            break;
    }
}

/******************************************************FunctionHeaderBegin******
 * FUNCTION     : OSAL_vGetAssertMode
 * CREATED      : 2005-10-05
 * AUTHOR        : TMS-Mrh
 * DESCRIPTION : -
 * SYNTAX        : tVoid OSAL_vGetAssertMode(teAssertMode mode)
 * ARGUMENTS    : -
 *
 * RETURN VALUE: mode     ASSERTMODE_RESET, ASSERTMODE_SUSPEND, or
 *                              ASSERTMODE_TASKSUSPEND
 * NOTES         : Get Assert mode which is one the three above to determine system
 *                    behaviour in case of assert. In addition, it depends on whether
 *                    the software was built in release or debug mode.
 *******************************************************FunctionHeaderEnd******/
teAssertMode OSAL_vGetAssertMode(void)
{
     return (teAssertMode)pOsalData->u32AssertMode;
}

/*****************************************************************************
*
* FUNCTION: vTraceTCB
*
* DESCRIPTION: this function traces info data for all OSAL tasks 
*
* PARAMETER:    tU8
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vTraceTCB(tBool bUsedOnly)
{
    tU32 slen;
    tU8 au8Buf[MAX_TRACE_SIZE];
    trThreadElement *pCurrent = &pThreadEl[0];
    tU32 u32Count = 0;

    if(LockOsal(pThreadLock) == OSAL_OK)
    {
        tU16 u16LintDummy = 0;

        do
        {
            memset(&au8Buf[0],'\0',MAX_TRACE_SIZE);
            /* prepare Task data */
            OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_CORE_DATA_PROC);
            OSAL_M_INSERT_T32(&au8Buf[1],(tU32)pCurrent->kernel_tid);
            OSAL_M_INSERT_T32(&au8Buf[5],(uintptr_t)pCurrent->pfOsalEntry);
            OSAL_M_INSERT_T16(&au8Buf[9], u16LintDummy);
            OSAL_M_INSERT_T32(&au8Buf[11],(tU32)pCurrent->pid);
            OSAL_M_INSERT_T32(&au8Buf[15],pCurrent->u32Priority);
            OSAL_M_INSERT_T32(&au8Buf[19],pCurrent->tid);
            OSAL_M_INSERT_T32(&au8Buf[23],pCurrent->u32TaskStack);
            slen = strlen(pCurrent->szName);
            memcpy (&au8Buf[27],pCurrent->szName,slen);

            if(bUsedOnly)
            {
                if(pCurrent->bIsUsed)LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+27);
            }
            else
            {
                LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL, au8Buf, slen + 27);
            }
            /* get next Task data */
            pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
        }while (pCurrent <= &pThreadEl[pOsalData->ThreadTable.u32MaxEntries]);
        UnLockOsal(pThreadLock);
    }
    TraceString("Number of Max  Tasks:%d",pOsalData->u32MaxNrThreadElements);
    TraceString("Number of Used Tasks:%d",u32Count);
}

/*****************************************************************************
*
* FUNCTION: vTraceECB
*
* DESCRIPTION: this function traces info data for all OSAL Events
*
* PARAMETER:
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
#ifndef SEM_EV

void vTraceECB(tBool bUsedOnly)
{
    tU32 slen;
    tU8 au8Buf[MAX_TRACE_SIZE];
    trEventElement *pCurrent = &pEventEl[0];
    tU32 used = pOsalData->EventTable.u32UsedEntries;
    tU32 u32Count = 0;

    if(LockOsal(&pOsalData->EventTable.rLock) == OSAL_OK)
    {
        do
        {
            memset(&au8Buf[0],' ',MAX_TRACE_SIZE);
            slen = 0;
            OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_DATA_EV);
            if(pCurrent->s32RecProc != -1)
            { 
                OSAL_M_INSERT_T32(&au8Buf[1],(tU32)pCurrent->s32RecProc);
            }
            else 
            { 
                OSAL_M_INSERT_T32(&au8Buf[1],slen);
            }
            OSAL_M_INSERT_T8 (&au8Buf[5],pCurrent->bToDelete);
            OSAL_M_INSERT_T16(&au8Buf[6],(tU16)pCurrent->s32RecTsk);
            OSAL_M_INSERT_T16(&au8Buf[8],pCurrent->u16OpenCounter);
            OSAL_M_INSERT_T32(&au8Buf[10],pCurrent->u32EventBitField);
            slen = strlen(pCurrent->szName);
            memcpy(&au8Buf[14],pCurrent->szName,slen+1);
            if(bUsedOnly)
            {
                if(pCurrent->bIsUsed)LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+14);
            }
            else
            {
                LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+14);
            }
            pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
        } while ( pCurrent < &pEventEl[used]);

        UnLockOsal(&pOsalData->EventTable.rLock);
    }
    TraceString("Number of Max  Events:%d",pOsalData->u32MaxNrEventElements);
    TraceString("Number of current Used Events:%d",u32Count);
    TraceString("Number of Maximum Used Events:%d",pOsalData->u32EvtResCount);
}
#else
void vTraceECB(tBool bUsedOnly)
{
    trEventElement *pCurrent = &pEventEl[0];
    tU32 used = pOsalData->EventTable.u32UsedEntries;
    tU32 u32Count = 0;
    ((void)bUsedOnly);

    if(LockOsal(&pOsalData->EventTable.rLock) == OSAL_OK)
    {
        do
        {
            if(pCurrent->bIsUsed)
            {
                 TraceString("Event 0x%x (Lock ID:%d) PID:%d OpenCount:%d Bitfield:0x%x Name:%s",
                                 pCurrent,pCurrent->hLock,pCurrent->s32PID,pCurrent->u16OpenCounter,
                                 pCurrent->u32WaitMask,pCurrent->szName);
            }
            pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
        } while ( pCurrent < &pEventEl[used]);

        UnLockOsal(&pOsalData->EventTable.rLock);
    }
    TraceString("Number of Max  Events:%d",pOsalData->u32MaxNrEventElements);
    TraceString("Number of current Used Events:%d",u32Count);
    TraceString("Number of Maximum Used Events:%d",pOsalData->u32EvtResCount);
}
#endif

void vSetTraceFlagForEvent(tCString coszName, tBool bVal)
{
    trEventElement *pCurrent = &pEventEl[0];
    tU32 used = pOsalData->EventTable.u32UsedEntries;
    tU32  u32Found = 0;

    if (OSAL_NULL == coszName)
    {
        while(pCurrent < &pEventEl[used])
        {
            pCurrent->bTraceFlg = bVal;
            pCurrent++;
        }
    }
    else
    {
        while(pCurrent < &pEventEl[used])
        {
            if (pCurrent < &pEventEl[used]) 
            {
                 if(!strncmp(coszName,(tString)pCurrent->szName,strlen(coszName)))
                 {
                     pCurrent->bTraceFlg = bVal;
                     u32Found++;
                 }
            }
            pCurrent++;
        }
        if(u32Found)
        {
             TraceString("OSALEV %d matching Events found for %s",u32Found,coszName);
        }
        else
        {
             TraceString("OSALEV Event:%s not found for logging",coszName);
        }
    }
}

/*****************************************************************************
*
* FUNCTION: vTraceMQCB
*
* DESCRIPTION: this function traces info data for all OSAL message queues 
*
* PARAMETER:    
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vTraceMQCB(tBool bUsedOnly)
{
    trMqueueElement *pCurrent = pMsgQEl;
    tU32 i,u32Count = 0;
    ((void)bUsedOnly);

    if(LockOsal(&pOsalData->MqueueTable.rLock ) == OSAL_OK)
    {
        for(i=0;i<pOsalData->u32MaxNrMqElements;i++)
        {
            if(pCurrent->bIsUsed)
            {
                 trStdMsgQueue* pTemp = &pStdMQEl[pCurrent->u32EntryIdx];
                 TraceString("MQ 0x%x (Handle:%d) PID:%d OpenCount:%d ActMsg:%d Name:%s",
                                 pCurrent,pTemp->s32LiHandle,pCurrent->s32PID,pTemp->u16OpenCounter,
                                 pTemp->ActualMessage,pCurrent->szName);
            }
            if(i<pOsalData->u32MaxNrMqElements-1)pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
      }
      TraceSpecificPoolInfo(&MqMemPoolHandle);
      UnLockOsal(&pOsalData->MqueueTable.rLock);
    }
    TraceString("Number of Max  MQs:%d",pOsalData->u32MaxNrMqElements);
    TraceString("Number of Used MQs:%d",u32Count);
}

/*****************************************************************************
*
* FUNCTION: vGetMsgQueueStatus
*
* DESCRIPTION: this function traces Status for all OSAL message queues 
*
* PARAMETER:    
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
tS32 s32GetAttr( char * name, struct mq_attr* attr)
{
    mqd_t mqd;
    char q[256] = "/";
    strcat( q, name);
    mqd = mq_open( q, O_RDONLY);
    if (mqd == (mqd_t) -1)
    {
        return -1;
    }
    if (mq_getattr(mqd,attr) == -1)
    {
        return -2;
    }
    mq_close( mqd );
    
    return 0;
}

void vGetMsgQueueStatus()
{
     trMqueueElement *pCurrent = &pMsgQEl[0];
     trStdMsgQueue    *pMq = &pStdMQEl[0];
     int qcount = 0;
     long summaxbytes = 0;
     tU8 au8Buf[MAX_TRACE_SIZE];
     tU32 slen,i;
     struct mq_attr attr;

    if(LockOsal(&pOsalData->MqueueTable.rLock ) == OSAL_OK)
    {
        for(i=0;i<pOsalData->u32MaxNrMqElements;i++)
        {
            if(pCurrent->bIsUsed)          
            {
                memset(&au8Buf[0],'\0',MAX_TRACE_SIZE);
                memset(&attr,0,sizeof(attr));

                if( s32GetAttr(pCurrent->szName, &attr) == -1)
                {
                     TraceString("Incorrect Queue Data");
                }
                OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_DATA_MQ_STAT);
                OSAL_M_INSERT_T32(&au8Buf[1],(tU32)attr.mq_maxmsg);
                OSAL_M_INSERT_T32(&au8Buf[5],(tU32)attr.mq_msgsize);
                OSAL_M_INSERT_T32(&au8Buf[9],(tU32)attr.mq_curmsgs);
                slen = strlen(pCurrent->szName);
                memcpy (&au8Buf[13],pCurrent->szName,slen+1);
                LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+13);
/*                      Since Linux 3.5:
                             bytes = attr.mq_maxmsg * sizeof(struct msg_msg) +
                                        min(attr.mq_maxmsg, MQ_PRIO_MAX) *
                                                sizeof(struct posix_msg_tree_node)+
                                                             * For overhead *
                                        attr.mq_maxmsg * attr.mq_msgsize;
                                                             * For message data */
                summaxbytes +=  (pMq->MaxMessages * sizeof(struct msg_msg *) 
                                     + OSAL_MIN(pMq->MaxMessages, MQ_PRIO_MAX) * sizeof(struct posix_msg_tree_node)
                                     + pMq->MaxMessages * pMq->MaxLength);
                qcount++;
            }
            pCurrent++;
            pMq++;
     }
     UnLockOsal(&pOsalData->MqueueTable.rLock);
     
     TraceString("Memory for OSAL MQs (Count:%d) needed:%d Bytes  --------------------------------------------------------------",
                 (int)qcount,(int)summaxbytes);

     qcount = 0;
     summaxbytes = 0;

     DIR *pDir;
     struct dirent *pDEntry;
     long cursize = 0;
     pDir = opendir("/dev/mqueue");
     while( NULL != (pDEntry = readdir( pDir )) )
     {
         if(pDEntry->d_name[0] != '.')
         {
            if( s32GetAttr( pDEntry->d_name, &attr) == 0)
            {
              qcount++;
              cursize = attr.mq_maxmsg * sizeof(struct msg_msg *) + attr.mq_maxmsg * attr.mq_msgsize;
              summaxbytes += cursize;

              memset(&au8Buf[0],'\0',MAX_TRACE_SIZE);
              OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_DATA_MQ_STAT);
              OSAL_M_INSERT_T32(&au8Buf[1],(tU32)attr.mq_maxmsg);
              OSAL_M_INSERT_T32(&au8Buf[5],(tU32)attr.mq_msgsize);
              OSAL_M_INSERT_T32(&au8Buf[9],(tU32)attr.mq_curmsgs);
              slen = strlen(pDEntry->d_name);
              memcpy (&au8Buf[13],pDEntry->d_name,slen+1);
              LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+13);
            }
         }
     }
     closedir( pDir );
     TraceString("Memory for all MQs (Count:%d) needed:%d Bytes  --------------------------------------------------------------",
                 (int)qcount,(int)summaxbytes);
    }
}


tU32 u32GetMqResources(tU32* pCount)
{
     tU32 qcount = 0;
     tU32 summaxbytes = 0;
     struct mq_attr attr;

     DIR *pDir;
     struct dirent *pDEntry;
     long cursize = 0;
     pDir = opendir("/dev/mqueue");
     if(pDir)
     {
         while( NULL != (pDEntry = readdir( pDir )) )
         {
             if(pDEntry->d_name[0] != '.')
             {
                 if( s32GetAttr( pDEntry->d_name, &attr) == 0)
                 {
                     qcount++;
                     cursize = attr.mq_maxmsg * sizeof(struct msg_msg *) + attr.mq_maxmsg * attr.mq_msgsize;
                     summaxbytes += cursize;
                 }
             }
         }
         closedir( pDir );
     }
     *pCount = qcount;
     return summaxbytes;
}

void vGetMsgQueueMaxFillLevels(void)
{
#ifdef MAX_MSG_COUNT
     trMqueueElement *pCurrent = pMsgQEl;
     tU8 au8Buf[MAX_TRACE_SIZE];
     tU32 slen;
 
     while (pCurrent < &pMsgQEl[pOsalData->MqueueTable.u32UsedEntries] )
     {
          memset(&au8Buf[0],'\0',MAX_TRACE_SIZE);

          OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_MQ_FILL_LEVEL);
          OSAL_M_INSERT_T32(&au8Buf[1],pCurrent->pMqueue->u32MaxMsgCount);
          slen = strlen(pCurrent->szName);
          memcpy (&au8Buf[5],pCurrent->szName,slen+1);
          LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,slen+13);

          pCurrent++;
     }
#else
     TraceString((const char*)"MaxFillLevel Supervison not activ");
#endif
}

void vSetTraceFlagForChannel(tCString coszName, tBool bVal)
{
    trMqueueElement *pCurrent = pMsgQEl;
    tU32  u32Found = 0;

    if (OSAL_NULL == coszName)
    {
        while (pCurrent < &pMsgQEl[pOsalData->MqueueTable.u32UsedEntries] )
        {
              pCurrent->bTraceCannel = bVal;
              pCurrent++;
        }
        pOsalData->bMqTaceAll = bVal;
    }
    else
    {
        while(pCurrent < &pMsgQEl[pOsalData->MqueueTable.u32UsedEntries])
        {
            if (pCurrent < &pMsgQEl[pOsalData->MqueueTable.u32UsedEntries]) 
            {
                 if(!strncmp(coszName,(tString)pCurrent->szName,strlen(coszName)))
                 {
                     pCurrent->bTraceCannel = bVal;
                     u32Found++;
                 }
            }
            pCurrent++;
        }
        if(u32Found)
        {
             TraceString("OSALMQ %d matching Message Queue found for %s",u32Found,coszName);
        }
        else
        {
             TraceString("OSALMQ MQ:%s not found for logging",coszName);
        }
    }
}

void vSetTraceFlagForShMem(tCString coszName, tBool bVal)
{
    trSharedMemoryElement *pCurrent = &pShMemEl[0];
    tU32 used = pOsalData->SharedMemoryTable.u32UsedEntries;
    tU32  u32Found = 0;

    if (OSAL_NULL == coszName)
    {
        while(pCurrent < &pShMemEl[used])
        {
            pCurrent->bIsMapped = bVal;
            pCurrent++;
        }
        pOsalData->bShMemTaceAll = bVal;
    }
    else
    {
        while(pCurrent < &pShMemEl[used])
        {
            if (pCurrent < &pShMemEl[used]) 
            {
                 if(!strncmp(coszName,(tString)pCurrent->szName,strlen(coszName)))
                 {
                     pCurrent->bIsMapped = bVal;
                     u32Found++;
                 }
            }
            pCurrent++;
        }
        if(u32Found)
        {
             TraceString("OSALSHM %d matching Shared Memories found for %s",u32Found,coszName);
        }
        else
        {
             TraceString("OSALSHM Shared Memory:%s not found for logging",coszName);
        }
    }
}

/*****************************************************************************
*
* FUNCTION: vTraceShMem
*
* DESCRIPTION: this function traces info data for all OSAL shared memory areas
*
* PARAMETER:    
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vTraceShMem(tBool bUsedOnly)
{
    tU32 u32Count = 0;
    trSharedMemoryElement *pCurrent = pShMemEl;
    ((void)bUsedOnly);
    if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
    {
          do
          {
                if(pCurrent->bIsUsed)
                {
                     TraceString("ShMem 0x%x Size:%d PID:%d OpenCount:%d Adress:%d Name:%s",
                                     pCurrent,pCurrent->u32SharedMemorySize,pCurrent->s32PID,pCurrent->u16OpenCounter,
                                     pCurrent->pvSharedMemoryStartPointer,pCurrent->szName);
                }
                pCurrent++;
                if(pCurrent->bIsUsed)u32Count++;
            } while (pCurrent < &pShMemEl[pOsalData->SharedMemoryTable.u32UsedEntries]);
            UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
    }
    TraceString("Number of Max  Shared Memory:%d",pOsalData->u32MaxNrSharedMemElements);
    TraceString("Number of Used Shared Memory:%d",u32Count);
}

/*****************************************************************************
*
* FUNCTION: vTraceSCB
*
* DESCRIPTION: this function traces info data for all OSAL Semaphores
*
* PARAMETER:
*
* RETURNVALUE: void
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vTraceSCB(tBool bUsedOnly)
{
    trSemaphoreElement *pCurrent = &pSemEl[0];
    tU32 used = pOsalData->SemaphoreTable.u32UsedEntries;
    tU32 u32Count = 0;
    ((void)bUsedOnly);

    if(LockOsal(&pOsalData->SemaphoreTable.rLock) == OSAL_OK)
    {
        do
        {
            if(pCurrent->bIsUsed)
            {
                TraceString("Sem 0x%x (Lock ID:%d) PID:%d OpenCount:%d SemValue:%d Name:%s",
                                pCurrent,pCurrent->hSemaphore,pCurrent->s32PID,pCurrent->u16OpenCounter,
                                pCurrent->u16SemValue,pCurrent->szName);
            }
            pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
        } while (pCurrent < &pSemEl[used]);

        UnLockOsal(&pOsalData->SemaphoreTable.rLock);
    }
  TraceString("Number of Max  Semaphores:%d",pOsalData->u32MaxNrSemaphoreElements);
  TraceString("Number of Used Semaphores:%d",u32Count);
}

void vSetTraceFlagForSem(tCString coszName, tBool bVal)
{
    trSemaphoreElement *pCurrent = &pSemEl[0];
    tU32 used = pOsalData->SemaphoreTable.u32UsedEntries;
    tU32  u32Found = 0;

    if (OSAL_NULL == coszName)
    {
        while(pCurrent < &pSemEl[used])
        {
            pCurrent->bTraceSem = bVal;
            pCurrent++;
        }
        pOsalData->bSemTaceAll = bVal;
    }
    else
    {
        while(pCurrent < &pSemEl[used])
        {
            if(pCurrent < &pSemEl[used]) 
            {
                 if(!strncmp(coszName,(tString)pCurrent->szName,strlen(coszName))) 
                 {
                     pCurrent->bTraceSem = bVal;
                     u32Found++;
                 }
            }
            pCurrent++;
        }
        if(u32Found)
        {
             TraceString("OSALSEM %d matching Semaphores found for %s",u32Found,coszName);
        }
        else
        {
             TraceString("OSALSEM Semaphore:%s not found for logging",coszName);
        }
    }
}


void vTraceMCB(tBool bUsedOnly)
{
    trMutexElement *pCurrent = &pMutexEl[0];
    tU32 used = pOsalData->MutexTable.u32UsedEntries;
    tU32 u32Count = 0;
    ((void)bUsedOnly);

    if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
    {
        do
        {
            if(pCurrent->bIsUsed)
            {
                TraceString("Mtx 0x%x (Lock ID:%d) PID:%d OpenCount:%d Name:%s",
                                pCurrent,&pCurrent->hMutex,pCurrent->s32PID,pCurrent->u16OpenCounter,pCurrent->szName);
            }
            pCurrent++;
            if(pCurrent->bIsUsed)u32Count++;
        } while (pCurrent < &pMutexEl[used]);

        UnLockOsal(&pOsalData->SemaphoreTable.rLock);
    }
  TraceString("Number of Max  Mutexes:%d",pOsalData->u32MaxNrMutexElements);
  TraceString("Number of Used Mutexes:%d",u32Count);
}

void vSetTraceFlagForMtx(tCString coszName, tBool bVal)
{
    trMutexElement *pCurrent = &pMutexEl[0];
    tU32 used = pOsalData->MutexTable.u32UsedEntries;
    tU32  u32Found = 0;

    if (OSAL_NULL == coszName)
    {
        while(pCurrent < &pMutexEl[used])
        {
            pCurrent->bTraceMut = bVal;
            pCurrent++;
        }
        pOsalData->bMutexTaceAll = bVal;
    }
    else
    {
        while(pCurrent < &pMutexEl[used])
        {
            if(pCurrent < &pMutexEl[used]) 
            {
                 if(!strncmp(coszName,(tString)pCurrent->szName,strlen(coszName))) 
                 {
                     pCurrent->bTraceMut = bVal;
                     u32Found++;
                 }
            }
            pCurrent++;
        }
        if(u32Found)
        {
             TraceString("OSALMTX %d matching Mutex found for %s",u32Found,coszName);
        }
        else
        {
             TraceString("OSALMTX Mutex:%s not found for logging",coszName);
        }
    }
}


/************************************************************************
*                                                                                              *
* FUNCTIONS                                                                                 *
*                                                                                              *
*        vTraceTimCB                                                                        *
*                                                                                              *
* DESCRIPTION                                                                              *
*      Trace all OSAL Timer data                                                      *
*                                                                                              *
* CALLS                                                                                      *
*                                                                                              *
* INPUTS                                                                                     *
*                                                                                              *
* OUTPUTS                                                                                    *
*                                                                                              *
* HISTORY:                                                                                  *
* Date        |    Modification                                 | Authors             *
* 16.07.05  |    Initial revision                            | MRK2HI              *
************************************************************************/
void vTraceTimCB(tBool bUsedOnly)
{
    trTimerElement *pCurrent = &pTimerEl[0];
    tU8 au8Buf[80];
    tU32 u32Count = 0;
    if(LockOsal(&pOsalData->TimerTable.rLock) == OSAL_OK)
    {
        while (pCurrent < &pTimerEl[pOsalData->TimerTable.u32UsedEntries])
        {
          if(!bUsedOnly || pCurrent->bIsUsed)
          {
              OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_DATA_TIM);
              OSAL_M_INSERT_T32(&au8Buf[1],(tU32)pCurrent->Pid);
              OSAL_M_INSERT_T32(&au8Buf[5],(tU32)pCurrent->idTask);
              OSAL_M_INSERT_T32(&au8Buf[9],(uintptr_t)pCurrent->pfOsalCallback);
              OSAL_M_INSERT_T8 (&au8Buf[13],(tU32)pCurrent->bIsSetting);
              OSAL_M_INSERT_T32(&au8Buf[14],pCurrent->u32Timeout);
              OSAL_M_INSERT_T32(&au8Buf[18],pCurrent->u32Interval);
              LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,sizeof(au8Buf));
          }
          pCurrent++;
          if(pCurrent->bIsUsed)u32Count++;
        }
        UnLockOsal(&pOsalData->TimerTable.rLock);
  } 
  TraceString("Number of Max  Timer:%d",pOsalData->u32MaxNrTimerElements);
  TraceString("Number of Used Timer:%d",u32Count);
  
  struct timespec ts;

  if(!clock_getres(CLOCK_MONOTONIC,&ts))
  {    TraceString("Timer Resolution CLOCK_MONOTONIC:%d nsec",ts.tv_nsec); }
  else
  {    TraceString("Timer CLOCK_MONOTONIC not supported"); }
  if(!clock_getres(CLOCK_MONOTONIC_COARSE,&ts))
  {    TraceString("Timer Resolution CLOCK_MONOTONIC_COARSE:%d nsec",ts.tv_nsec); }
  else
  {    TraceString("Timer CLOCK_MONOTONIC_COARSE not supported"); }
  if(!clock_getres(CLOCK_MONOTONIC_RAW,&ts))
  {    TraceString("Timer Resolution CLOCK_MONOTONIC_RAW:%d nsec",ts.tv_nsec); }
  else
  {    TraceString("Timer CLOCK_MONOTONIC_RAW not supported"); }
  if(!clock_getres(CLOCK_REALTIME,&ts))
  {  TraceString("Timer Resolution CLOCK_REALTIME:%d nsec",ts.tv_nsec); }
  else
  {  TraceString("Timer CLOCK_REALTIME not supported"); }
  if(!clock_getres(CLOCK_REALTIME_COARSE,&ts))
  {  TraceString("Timer Resolution CLOCK_REALTIME_COARSE:%d nsec",ts.tv_nsec); }
  else
  {  TraceString("Timer CLOCK_REALTIME_COARSE not supported"); }
  if(!clock_getres(CLOCK_PROCESS_CPUTIME_ID,&ts))
  {  TraceString("Timer Resolution CLOCK_PROCESS_CPUTIME_ID:%d nsec",ts.tv_nsec); }
  else
  {  TraceString("Timer CLOCK_PROCESS_CPUTIME_ID not supported"); }
  if(!clock_getres(CLOCK_THREAD_CPUTIME_ID,&ts))
  {  TraceString("Timer Resolution CLOCK_THREAD_CPUTIME_ID:%d nsec",ts.tv_nsec); }
  else
  {  TraceString("Timer CLOCK_THREAD_CPUTIME_ID not supported"); }
}

void vActivateTimerTrace(tU8 bOn,tS32 s32Tid,tU32 u32Timeout, tU32 u32Interval)
{
  trTimerElement *pCurrent = &pTimerEl[0];
  tU16 u16Found = 0;
  tU32 used = pOsalData->TimerTable.u32UsedEntries;

  if((s32Tid == 0)&&(u32Timeout == 0)&&(u32Interval == 0))
  {
     pOsalData->bTraceTimerAll = bOn;
     u16Found++;
  }
  else
  {
      while(pCurrent < &pTimerEl[used])
      {
          if(s32Tid)
          {
              if(pCurrent->idTask == s32Tid)
              {
                 /*check if all timer of task ID should be marked */
                 if((u32Timeout)||(u32Interval))
                 {
                     /* only defined timer of task is marked */
                     if((pCurrent->u32Timeout  == u32Timeout)
                     &&(pCurrent->u32Interval == u32Interval))
                     {
                         pCurrent->bTraceOn = bOn;
                         u16Found++;
                     }
                 }
                 else
                 {
                     /* all timer of task will be marked */
                     pCurrent->bTraceOn = bOn;
                     u16Found++;
                 }
              }
          }
          else
          {
              if((pCurrent->u32Timeout  == u32Timeout)
              &&(pCurrent->u32Interval == u32Interval))
              {
                  pCurrent->bTraceOn = bOn;
                  u16Found++;
              }
          }
          pCurrent++;
      }//while
    }

    TraceString("%d Timer found for Task %d Timeout %d ms Interval %d ms",
                (tS32)u16Found, 
                 s32Tid,
                (tS32)u32Timeout,
                (tS32)u32Interval);
}

void vActivatePidTimerTrace(tU8 bOn,tU32 u32Pid)
{
    trTimerElement *pCurrent = &pTimerEl[0];
    tU16 u16Found = 0;
    tU32 used = pOsalData->TimerTable.u32UsedEntries;
    while(pCurrent < &pTimerEl[used])
    {
        if(pCurrent->Pid == (tS32)u32Pid)
        {
             pCurrent->bTraceOn = bOn;
             u16Found++;
        }
        pCurrent++;
    }
    TraceString("vActivatePidTimerTrace %d timer for process %d found",u16Found,u32Pid);
}


void vShowCurrentResourceSitutaion(void)
{
    tU32 u32Count = 0;
    tU32 u32Idx;

    for(u32Idx=0;u32Idx < MAX_OSAL_LOCK ;u32Idx++)
    {
      if(prOsalLock[u32Idx].rInfo.bUsed == TRUE)
      {
    /*     TraceString("Sem %s in OSAL Count:%d Used Flag:%d",
                    pOsalData->rLock[u32Idx].pInfo.cName,
                    pOsalData->rLock[u32Idx].pInfo.u32Count,
                    pOsalData->rLock[u32Idx].pInfo.bUsed);*/
         u32Count++;
      }
    }
    TraceString("Sem Count via OSAL:%d of %d",
              u32Count,MAX_OSAL_LOCK);


     TraceString("OSAL Event Current:%d Max:%d ",
                pOsalData->u32EvtResCount,pOsalData->u32MaxEvtResCount);
     TraceString("OSAL Semaphor Current:%d Max:%d ",
                pOsalData->u32SemResCount,pOsalData->u32MaxSemResCount);
     TraceString("OSAL Shared Memory Current:%d Max:%d ",
                pOsalData->u32ShMResCount,pOsalData->u32MaxShMResCount);
     TraceString("OSAL Message Queue Current:%d Max:%d ",
                pOsalData->u32MqResCount,pOsalData->u32MaxMqResCount);
     TraceString("OSAL Timer Current:%d Max:%d ",
                pOsalData->u32TimResCount,pOsalData->u32MaxTimResCount);
     TraceString("OSAL Tasks Current:%d Max:%d ",
                pOsalData->u32TskResCount,pOsalData->u32MaxTskResCount);
}



/*****************************************************************************
*
* FUNCTION:     FileCopy
*
* DESCRIPTION: File Copy function
*
* PARAMETER:    tString  sources file
*                  tString  destination file
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* 11.03.09  | MMS 223874 : Minor corrections to        | JEV1KOR
*              | ensure write returns during error.      |
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
int FileCopy(tString strsrc,tString strdest)
{
  tS32  s32RetValue = 0;
  tS32  fd_in_file  = 0;
  tS32  fd_out_file = 0;
  int    size          = 0;
  int    written      = 0;
  int    BytesToRead = 0;
  tU16  u16OpenMode = 0;
  char* pBuffer = NULL;
  DIR*  pDir = NULL;
  int    ReadOk=1;
  int    WriteOk=1;
 
  /* prove for directory */
  pDir = opendir(strsrc);
  if(pDir)
  {
        closedir(pDir);
        return OSAL_E_NOACCESS;
  }

  /* source is a file */
  fd_in_file = open(strsrc, O_RDONLY, u16OpenMode);
  if(fd_in_file == -1)
  {
        TraceString((const char*)"Open Source File Error");
        return OSAL_ERROR;
  }
  fd_out_file = open(strdest,O_CREAT | O_RDWR | O_TRUNC,S_IRUSR|S_IWUSR);
  if(fd_out_file == -1)
  {
        close(fd_in_file);
        TraceString((const char*)"Open Destination File Error");
        return OSAL_ERROR;
  }
  close(fd_out_file);
  size = s32GetFileSize(fd_in_file);

  pBuffer = (char *)OSAL_pvMemoryAllocate(OSALCORE_FILECOPY_BUFFERSIZE);

  if(pBuffer)
  {
     /* Start reading file */
     OSAL_tMSecond rtimes[2] = {0};

     while(written < size)
     {
         BytesToRead = size-written;  
         if(BytesToRead > OSALCORE_FILECOPY_BUFFERSIZE)
              BytesToRead = OSALCORE_FILECOPY_BUFFERSIZE;

         TraceString("Read from file %s %d Bytes",strsrc,BytesToRead);
         if((s32RetValue = lseek(fd_in_file, written, SEEK_SET)) != written)
         {
             TraceString("Seek File %s Error -> returns 0x%x", strdest, s32RetValue);
             ReadOk = 0;
         }
         else
         {
             rtimes[0] = OSAL_ClockGetElapsedTime();
             s32RetValue = read(fd_in_file, pBuffer, (tU32)BytesToRead);
             rtimes[1] = OSAL_ClockGetElapsedTime();
             TraceString("Read %d bytes in %d ms",s32RetValue,rtimes[1] - rtimes[0]);

             if(s32RetValue != BytesToRead) 
             {
                 ReadOk = 0;
                 TraceString((const char*)"Read Error Buffer 1");
                 OSAL_vMemoryFree(pBuffer);
                 close(fd_in_file); 
                 return OSAL_ERROR;
             }
         }
         if(ReadOk != 1)
         {
            TraceString((const char*)"Read file from file system failed");//MMS 223874: Correction to read error msg
            break; //MMS 223874: Break out of While-loop in case of error
         }

         TraceString("Write to file %s %d Bytes", strdest, BytesToRead);
         fd_out_file = open(strdest, O_RDWR|O_APPEND ,S_IRUSR|S_IWUSR);
         if(fd_out_file != -1)
         {
             if((s32RetValue = lseek(fd_out_file, written, SEEK_SET)) != written)
             {
                  TraceString("Seek File %s Error -> returns 0x%x", strdest, s32RetValue);
                  WriteOk = 0;
             }
             else
             {
                  rtimes[0] = OSAL_ClockGetElapsedTime();
                  s32RetValue = write(fd_out_file,pBuffer,(tU32)BytesToRead);
                  rtimes[1] = OSAL_ClockGetElapsedTime();
                  TraceString("Wrote %d bytes in %d ms",s32RetValue,rtimes[1] - rtimes[0]);
                  if(s32RetValue != BytesToRead)
                  {
                        TraceString("Write File %s Error -> %d Bytes of %d Bytes written",
                        strdest,s32RetValue,BytesToRead);
                        WriteOk = 0;
                  }
             }
             close(fd_out_file);
             if(WriteOk != 1)
             {
                  TraceString((const char*)"Write to file failed");
                  break;
             }
         }
         else
         {
              TraceString("Open file %s for writing failed",strdest);
              break; //MMS 223874: Break out of While-loop in case of error
         }
         written += BytesToRead;

         if(written >= size)
              break;
      } //while
  }

  OSAL_vMemoryFree(pBuffer);  /*lint -e429 */
  close(fd_in_file);
  return OSAL_OK;
}



/*****************************************************************************
*
* FUNCTION:     vMkDir
*
* DESCRIPTION: create a directory          
*
* PARAMETER:    void*  directory as string
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vMkDir(void* pvBuffer)
{
  if(mkdir((char*)pvBuffer,0777) != OSAL_OK)
  {
    TraceString((const char*)"MK DIR Error");
  }
}


/*****************************************************************************
*
* FUNCTION:     u32Read_Rem_Dir
*
* DESCRIPTION: reads or deletes a directory (file) content
*
* PARAMETER:    void*  directory as string
*                  tU8     operation code
*                  tU32    return value
*
* RETURNVALUE: tU32    Number of files
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
tU32 u32Read_Rem_Dir(void* pvBuffer,tU8 u8Val,tU32* pu32Content)
{
  struct dirent*    pDirEnt=NULL;
  char Buffer[150];
  int Len,Len2;
  char ContentAvailable = 0;
  tU32 u32FileCounter = 0;
  DIR* dir;
#ifdef TSIM_OSAL
  char tmpDevice[10];
  tU16 count = 0;
  char *pos = 0;
#endif
#ifdef TSIM_OSAL
  memset (tmpDevice, 0, 10);
  pos = (char*)pvBuffer;
  while(*pos != '\0')
  {
      if (*pos == '/')
      {
          count++;
          if (count == 2)
              break;
      }
      pos++;
  }
  if (count == 1)
  {
      memcpy((void*)tmpDevice, pvBuffer, strlen((const char*)pvBuffer));
  }
  else if (count == 2)
  {
      memcpy((void*)tmpDevice, pvBuffer, (tU32)((pos) - (char*)(pvBuffer)));
  }
#endif
  if((*((const char*)pvBuffer) == '/')&&(*(((const char*)pvBuffer+1)) == '\0'))
  {  
        dir = opendir((const char*)"/sda1");
        if(dir)closedir(dir);
        TraceString((const char*)"Scan all Mountpoints");
  }

  dir = opendir((char*)pvBuffer);
  if(dir)
  {
     chdir((char*)pvBuffer); 
     pDirEnt = readdir((DIR*)dir);
     while(pDirEnt)
     {
        tS32 fd;
         
        if(pDirEnt->d_name[0] != '.')
        {
            /* recursive operation */
          if(u8Val)
          { 
              if(u8Val == 1)
              {
                  if(remove((char*)pDirEnt->d_name) == OSAL_OK)
                  {
                         TraceString("File %s removed", pDirEnt->d_name );
                  }
                  else
                  {
                         TraceString("Directory %s cannot removed", pDirEnt->d_name );
                         /* try to remove directory */                
                         Len = (int)strlen((const char*)pvBuffer);
                         memcpy(&Buffer[0],pvBuffer,(tU32)Len);
                         Buffer[Len] = '/';
                         Len2 = (int)strlen(pDirEnt->d_name);
                         memcpy(&Buffer[Len+1],pDirEnt->d_name,(tU32)Len2);
                         Buffer[Len+1+Len2] = '\0';
                         u32Read_Rem_Dir(&Buffer[0],1,NULL);
                        remove((char*)&Buffer[0]);
                 }
            }
            else if(u8Val == 2)
            {
                tS32 s32RetValue = OSAL_OK;
                tS32 s32CurrentPos;
                if(pu32Content)
                {
                     DIR* pDir = NULL;
                     /* step 1: prove for directory */
                     Len = (tS32)strlen((const char*)pvBuffer);
                     memcpy(&Buffer[0],pvBuffer,(tU32)Len);
                     Buffer[Len] = '/';
                     Len2 = (tS32)strlen(pDirEnt->d_name);
                     memcpy(&Buffer[Len+1],pDirEnt->d_name,(tU32)Len2);
                     Buffer[Len+1+Len2] = '\0';
                     if((pDir = opendir(&Buffer[0])) != NULL)
                     {
                         closedir(pDir);
                         TraceString(&Buffer[0]);
                         u32FileCounter += u32Read_Rem_Dir(&Buffer[0],2,pu32Content);
                     }
                     else
                     {
                         if((fd = open(&Buffer[0], O_RDONLY ,0)) > 0)
                         {
                             u32FileCounter ++;
                             if((s32CurrentPos = lseek(fd,0,SEEK_CUR)) < OSAL_OK)
                             {
                                TraceString((const char*)"Seek operation SEEK_CUR failed");
                             }
                             else
                             {
                                if((s32RetValue = lseek(fd,0,SEEK_END)) < OSAL_OK)
                                {
                                    TraceString((const char*)"Seek operation SEEK_END failed");
                                }
                                else
                                {
                                    *pu32Content += (tU32)(s32RetValue - s32CurrentPos);
                                    if(lseek(fd,s32CurrentPos,SEEK_SET) < OSAL_OK)
                                    {
                                      TraceString((const char*)"Seek operation SEEK_SET failed");
                                    }
                                }
                             }
                             close(fd);
                         }
                         else
                         {
                             snprintf(Buffer, sizeof(Buffer),"open operation failed for %s", (char*)pDirEnt->d_name );
                             TraceString(Buffer);
                         }
                     }
                  }
            }
        }
        else
        {
            ContentAvailable = 1;
            memset(&Buffer[0],' ',(tU32)150);
            Len = (int)strlen(pDirEnt->d_name);
            if(Len > 130) Len = 130;
            memcpy(&Buffer[0],pDirEnt->d_name,(tU32)Len);
         
            struct stat rStatBuf;
            if(stat(pDirEnt->d_name,&rStatBuf) != -1)
            {
                  if(S_ISREG(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[Len],sizeof(Buffer)-Len, " Size: %d Bytes",(int) rStatBuf.st_size);
                  }
                  else if(S_ISDIR(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"Directory");
                  }
                  else if(S_ISCHR(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"Character device");
                  }
                  else if(S_ISBLK(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"Block device");
                  }
                  else if(S_ISFIFO(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"FIFO ");
                  }
                  else if(S_ISLNK(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"Symbolic Link %d Bytes",(int) rStatBuf.st_size);
                  }
                  else if(S_ISSOCK(rStatBuf.st_mode))
                  {
                         snprintf(&Buffer[50],100,"Socket");
                  }
                  else
                  {
                         snprintf(&Buffer[50],100,"Unknown");
                  }
            }
            else
            {
                snprintf(&Buffer[50],100,"stat call failed");
            }
            TraceString(Buffer);
         }
      }
      pDirEnt = readdir(dir);
    }
    if(u32FileCounter)
    {
          TraceString( "Number of Files: %d", (int)u32FileCounter);
    }
    closedir(dir);
    if((!ContentAvailable)&&(!u8Val)) TraceString((const char*)"Empty Directory"); 
  }
  else
  {
     TraceString((const char*)"Cannot Open Directory -> prove for correct name"); 
  }
  return u32FileCounter;
}

/*****************************************************************************
*
* FUNCTION:     vRmFileSelection
*
* DESCRIPTION: deletes directory files with wildcards
*
* PARAMETER:    void*  directory as string
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vRmFileSelection(void* pvBuffer)
{
  struct dirent*    pDirEnt=NULL;
  DIR* dir;
  char* cDir = (char*)pvBuffer;
  char* cFileString = NULL;
  char*     cString = NULL;
  char* cString2 = NULL;
  tBool bDeleteAll = FALSE;
  tBool bDelete;

  if(cDir)
  {
     cFileString = strrchr(cDir, '/');
     if(cFileString)
     {
        *cFileString++ = 0;
     }
  }

  if(cFileString)
  {
      cString = strrchr(cFileString, '*');
      if(cString)
      {
          /* Character found */
          cString2 = cString;
          cString2--;
          cString2--;
          if(!strncmp(cString2,"*.*",3))
          {
              bDeleteAll = TRUE;
          }
          else
          {
              cString2 = cString;         
              cString2++;
              /* prove if wild card is last character */
              if(*cString2 == 0)
              {
                 /* wild card is last character */
                 *cString = 0;
              }
              else
              {
                 /* wild card is first character */
                 cFileString++;
              }
          }
      }
  }

  if(cDir)
  {
     dir = opendir((char*)cDir);
     if(dir)
     {
         chdir((char*)cDir);
         pDirEnt = readdir((DIR*)dir);

         while(pDirEnt)
         {     
             if(pDirEnt->d_name[0] != '.')
             {
                 if(bDeleteAll)
                 {
                     bDelete = TRUE;
                 }
                 else
                 {
                     bDelete = FALSE;
                     if(cFileString)
                     {
                         if(strstr(pDirEnt->d_name, cFileString))
                         {            
                             bDelete = TRUE;
                         }
                     }
                 }
                 if(bDelete == TRUE)
                 {
                     if(remove((char*)pDirEnt->d_name) == OSAL_OK)
                     {
                         TraceString("File %s removed", pDirEnt->d_name );
                     }
                 }
             }
             pDirEnt = readdir(dir);
         }
         closedir(dir);
     }
     else
     {
        TraceString((const char*)"Cannot Open Directory -> prove for correct name"); 
     }
  }
}


static tBool bTerminateStackCheck = FALSE;
static tS32 s32StackPid = 0;

void TriggerStackCheckAll(void  *pvArg)
{
    struct dirent*    pDirEnt=NULL;
    DIR* dir;
    int Pid = 0;
    uintptr_t Intervall = (uintptr_t)pvArg;
    bTerminateStackCheck = FALSE;

    while(bTerminateStackCheck == FALSE)
    {
        if(s32StackPid != 0)
        {
             kill(s32StackPid,OSAL_STACK_SIGNAL);
             OSAL_s32ThreadWait(Intervall);
        }
        else
        {
      dir = opendir(VOLATILE_DIR"/OSAL/Processes");
      if(dir)
      {
                chdir(VOLATILE_DIR"/OSAL/Processes"); 
                pDirEnt = readdir((DIR*)dir);
                while(pDirEnt)
                {
                    if(pDirEnt->d_name[0] != '.')
                    {
                        Pid = atoi(pDirEnt->d_name);
                        kill(Pid,OSAL_STACK_SIGNAL);
                        OSAL_s32ThreadWait(100);
                    }
                }
                 pDirEnt = readdir(dir);
             }
             closedir(dir);
             if(!Intervall)break;
        }
    }
}

void vCreateStackCheckerTask(int Intervall)
{
    OSAL_trThreadAttribute  attr;
    intptr_t Val = (intptr_t)Intervall;
    /* Term Task call PRM , so vInitOsalIO should run before */
    attr.szName = (tString)"StackChecker";/*lint !e1773 */  /*otherwise linker warning */
    attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
    attr.s32StackSize = minStackSize;
    attr.pfEntry = (OSAL_tpfThreadEntry)TriggerStackCheckAll;
    attr.pvArg = (void*)Val;
    if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
    {
        TraceString("Start StackChecker Task");
    }
    else
    {
        bTerminateStackCheck = TRUE;
        TraceString("Stop StackChecker Task");
    }
}


void TriggerStackCheck(int Pid,int Intervall)
{
  if((Pid != 0)&&(Intervall == 0))
  {
      kill(Pid,OSAL_STACK_SIGNAL);
  }
  else
  {
      s32StackPid = Pid;
      vCreateStackCheckerTask(Intervall);
  }
}



/*****************************************************************************
*
* FUNCTION:     vRmDir
*
* DESCRIPTION: removes a directory            
*
* PARAMETER:    void*  directory as string
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vRmDir(void* pvBuffer)
{
  u32Read_Rem_Dir(pvBuffer,1,NULL);

  if(rmdir((const char*)pvBuffer) != OSAL_OK)
  {
    TraceString((const char*)"RM DIR Error");
  }
}

/*****************************************************************************
*
* FUNCTION:     vRmFile
*
* DESCRIPTION: removes a file            
*
* PARAMETER:    void*  file name as string
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vRmFile(void* pvBuffer)
{
    if(remove((char*)pvBuffer) != OSAL_OK)
    {      TraceString((const char*)"Remove File Error");    }
    else
    {        TraceString((const char*)"Remove File OK");        }
}


/*****************************************************************************
*
* FUNCTION:     vCopyFile
*
* DESCRIPTION: Copy file from one location to another location                 
*
* PARAMETER:    void*  comma seperated files
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vCopyFile(void* pvBuffer)
{
 char* srcdest;
 char* srcsrc = (char*)pvBuffer;

 srcdest = strrchr(srcsrc, ',');
 if(srcdest)
 {
    *srcdest++ = 0;
    /* seperate parameters */
    FileCopy(srcsrc,srcdest);
 }
 else
 {
    TraceString((const char*)"Wrong Copy Command");
 }
}

/*****************************************************************************
*
* FUNCTION:     vStartProc
*
* DESCRIPTION: starts a user mode process              
*
* PARAMETER:    void*  path to Process
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vStartProc(void* pvBuffer,tU8 u8Type)
{
    OSAL_trProcessAttribute prAtr = {0};
    char cNameBuffer[100];
    
    snprintf(cNameBuffer, sizeof(cNameBuffer),"%s",(char*)pvBuffer);
    cNameBuffer[sizeof(cNameBuffer)-1] = 0;
    switch (u8Type)
    {
    case PRC_TYPE_USERPRC:
        prAtr.szCommandLine = NULL;
        prAtr.szCGroup_path = NULL;
        prAtr.szAppName     = &cNameBuffer[0];
        prAtr.szName         = (char*) pvBuffer;

        if(OSAL_ProcessSpawn(&prAtr) == OSAL_ERROR)
        {
          TraceString((const char*)"Start Process failed");
        }
        else
        {
          TraceString((const char*)"Start Process succeeded");
        }
        break;
    default:
          TraceString((const char*)"Not supported option");
          break;
    }

}



void vCopyDirFiles(const char* cSrcDir,char* cDestDir)
{
 struct dirent*    pDirEnt=NULL;
 DIR* dir =0;
 char* pcNameBufferSrc  = (char*)malloc(OSAL_C_U32_MAX_PATHLENGTH);
 char* pcNameBufferDest = (char*)malloc(OSAL_C_U32_MAX_PATHLENGTH);

 if((pcNameBufferSrc)&&(pcNameBufferDest))
 {
    dir = opendir(cSrcDir);
    if(dir)
    {
        pDirEnt = readdir((DIR*)dir);
        while(pDirEnt)
        {
           if(pDirEnt->d_name[0] != '.')
           {
              memset(pcNameBufferSrc,'\0',OSAL_C_U32_MAX_PATHLENGTH);
              memset(pcNameBufferDest,'\0',OSAL_C_U32_MAX_PATHLENGTH);

              strcat(pcNameBufferSrc,cSrcDir);
              strcat(pcNameBufferSrc,"/");
              strcat(pcNameBufferSrc,pDirEnt->d_name);
      
              strcat(pcNameBufferDest,cDestDir);
              strcat(pcNameBufferDest,"/");
              strcat(pcNameBufferDest,pDirEnt->d_name);

              /* remove file if existing */
             remove((char*)pcNameBufferDest);
             if(FileCopy(pcNameBufferSrc,pcNameBufferDest) == (tS32)OSAL_E_NOACCESS)
             {
                /* it's an subdirectory */
                TraceString((const char*)"Create Subdirectory");
                vMkDir(pcNameBufferDest);
                /* try to copy subdirectory */
                vCopyDirFiles(pcNameBufferSrc,pcNameBufferDest);
             }
             /* see traces in download case */
           }
           pDirEnt = readdir(dir);
        }
        closedir(dir);
        TraceString((const char*)"Copy Dir End");
     }
  }
  if(pcNameBufferSrc)free(pcNameBufferSrc);
  if(pcNameBufferDest)free(pcNameBufferDest);
}


void vCopyDir(void* pvBuffer)
{
 char* srcdest;
 char* srcsrc = (char*)pvBuffer;

 srcdest = strrchr(srcsrc, ',');
 if(srcdest)
 {
    *srcdest++ = 0;
    vMkDir((char*)srcdest);
    /* separate parameters */
    vCopyDirFiles(srcsrc,srcdest);
 }
 else
 {
    TraceString((const char*)"Wrong Copy Parameter");
 }
}

void vCheckOsalPrc(tBool bPrintExisting)
{
    char szName[64];
    int fd1,fd2;
    struct dirent*    pDirEntt=NULL;
    DIR* dirtask =0;
    char szEntry[100] = {0};
    char* pTmp;
    char Buffer[256];

    snprintf(szName,64,"%s/OSAL/Processes",VOLATILE_DIR);
    if((fd1 = open(szName,O_RDONLY,OSAL_ACCESS_RIGTHS)) != -1)
    {
        dirtask = opendir(szName);
        if(dirtask)
        {
            pDirEntt = readdir((DIR*)dirtask);
            while(pDirEntt)
            {
                if((*pDirEntt->d_name>= 0x30 /* 0 */)&&(*pDirEntt->d_name <= 0x39 /* 9 */ ))
                {
                    memset(Buffer,0,256);
                    snprintf(szName,64,"%s/OSAL/Processes/%s",VOLATILE_DIR,pDirEntt->d_name);
                    if((fd2 = open(szName,O_RDONLY,OSAL_ACCESS_RIGTHS)) != -1)
                    {
                        (void)read(fd2,Buffer,256);
                        close(fd2);

                        pTmp = strstr(Buffer,"/opt");
                        if(!pTmp)
                        {
                             pTmp = strstr(Buffer,"./"); /* for processes started via script */
                        }
                        if(pTmp)
                        {
                            snprintf(szName,64,"/proc/%s",pDirEntt->d_name);
                            if((fd2 = open(szEntry,O_RDONLY,OSAL_ACCESS_RIGTHS)) != -1)
                            {
                                TraceString("Process %s disappeared !!!",pTmp);
                                close(fd2);
                            }
                            else
                            {
                                if(bPrintExisting)
                                {
                                    TraceString("Active Process %s  ",pTmp);
                                }
                            }
                        }
                        else
                        {
                            TraceString("strstr failed %s ",Buffer);
                        }
                    }
                    else
                    {
                        TraceString("open failed %s ",szName);
                    }
                }
                else
                {
                     if(pDirEntt->d_name[0] != '.')
                     {
                         TraceString("Unexpected entry %s", pDirEntt->d_name);
                     }
                }
                pDirEntt = readdir(dirtask);
            }//while(pDirEntt)
            closedir(dirtask);
        }//if(dirtask)
        close(fd1);
    }
}


//#define TEST_CODE_ACTIVE
#ifdef TEST_CODE_ACTIVE
void RegTest(void)
{
  OSAL_tIODescriptor hRegDevice = 0;
  OSAL_trErgReq rErgReqeust;
  memset(&rErgReqeust,0,sizeof(OSAL_trErgReq));
  strncpy(&rErgReqeust.cKeyName[0],"DATE",strlen("DATE"));
  strncpy(&rErgReqeust.cErgFilePath[0],"/dev/root/var/opt/bosch/dynamic/ffs/registry/NAVAPP.ERG",OSAL_C_U32_MAX_PATHLENGTH);
  hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY , OSAL_EN_READWRITE );
  if(OSAL_s32IOControl(hRegDevice, OSAL_C_S32_IOCTRL_BUILD_ERG, (uintptr_t)&rErgReqeust.cErgFilePath[0]) == OSAL_ERROR)
  {
        TraceString("OSAL_C_S32_IOCTRL_BUILD_ERG failed");
  }

  if(OSAL_s32IOControl(hRegDevice, OSAL_C_S32_IOCTRL_GET_ERGVAL, (uintptr_t)&rErgReqeust) == OSAL_ERROR)
  {
        TraceString("OSAL_C_S32_IOCTRL_GET_ERGVAL failed");
  }
  else
  {
        if(rErgReqeust.u32ResultType == 0xffffffff)
        {
            TraceString("OSAL_C_S32_IOCTRL_GET_ERGVAL no valid entry");
        }
        else
        {
          TraceString(rErgReqeust.cResultString);
        }
  }
  OSAL_s32IOClose(hRegDevice);
}

int Test=0;


int Start = TRUE;
void vSetTimeTask(void *pvArg)
{
     ((void)pvArg);
     OSAL_trTimeDate rTime;
     int count = 0;
while(count < 100)
{
     if(Start == TRUE)
     {
         rTime.s32Second = 00;
         rTime.s32Minute = 10;
         rTime.s32Hour = 12;
         rTime.s32Day = 15;
         rTime.s32Month = 6;
         rTime.s32Year = 109;
         rTime.s32Weekday = 0;
         rTime.s32Yearday = 0;
         rTime.s32Daylightsaving = 0;
         TraceString("Set Time OSAL_s32ClockSetTime");
         OSAL_s32ClockSetTime(&rTime);
         Start = FALSE;
     }
     else
     {
         OSAL_s32ClockGetTime(&rTime);
         if(rTime.s32Minute < 59)
         {
              rTime.s32Minute++;
         }
         else if(rTime.s32Hour < 59)
         {
              rTime.s32Hour++;
         }
         TraceString("Set Time OSAL_s32ClockSetTime");
         OSAL_s32ClockSetTime(&rTime);
         count++;
     }
     OSAL_s32ThreadWait(1000);
}
}

void StartTest(void)
{
             OSAL_trThreadAttribute  attr;
             attr.szName = (tString)"vSetTimeTask";/*lint !e1773 */  /*otherwise linker warning */
              attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
                attr.s32StackSize = minStackSize;
                attr.pfEntry = (OSAL_tpfThreadEntry)vSetTimeTask;
                attr.pvArg = NULL;
                if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
                {
                }
}

tVoid vMQTestCallBack(void *Arg)
{
    (void)Arg;
    TraceString("PID:%d Enter Function %s",OSAL_ProcessWhoAmI(),"vMQTestCallBack");
    NORMAL_M_ASSERT_ALWAYS();
    OSAL_s32ThreadWait(100000);
}


void vTestTask(tPVoid pArg )
{
    OSAL_tMQueueHandle mqHandle;

    if(OSAL_s32MessageQueueCreate("TEST_LONGJUMP",10,16,OSAL_EN_READWRITE, &mqHandle) == OSAL_ERROR)
    {
      TraceIOString("OSAL_s32MessageQueueOpen Notify failed"); 
    }
    if(OSAL_s32MessageQueueNotify(mqHandle, (OSAL_tpfCallback)vMQTestCallBack, OSAL_NULL ) == OSAL_ERROR)
    {    
      TraceIOString("OSAL_s32MessageQueueNotify failed"); 
    }
    while(1)
    {
        unsigned char Buffer[16];
        OSAL_s32MessageQueueWait(mqHandle,Buffer,16,0,OSAL_C_TIMEOUT_FOREVER);
        OSAL_s32ThreadWait(0);
    }
}


void TestLongjump()
{
  OSAL_trThreadAttribute  attr;
  OSAL_tThreadID ThreadID;
 
  tS32 s32Index = 3;
  tS32 s32Val=0,n;

  OSAL_tMQueueHandle mqHandle;
  if(OSAL_s32MessageQueueOpen("TEST_LONGJUMP", OSAL_EN_READWRITE, &mqHandle) == OSAL_ERROR)
  {
      TraceIOString("OSAL_s32MessageQueueOpen Notify failed"); 
  }
  char Buffer[16];
  tU32 u32Message;
  while(s32Val < 200)
  {
     
      for(;;)
      {
         OSAL_s32MessageQueueStatus(mqHandle,NULL,NULL,&u32Message);
         if(u32Message == 0)break;
         OSAL_s32ThreadWait(50);
      }


     if(OSAL_s32MessageQueuePost(mqHandle,(tPCU8)Buffer,16,0)  == OSAL_ERROR)
     {
          TraceIOString("OSAL_s32MessageQueuePost failed"); 
     }
     OSAL_s32ThreadWait(20);
     s32Val++;
  }
  
}


void vTriggerSigRtMinToSpecPrc(void* pArg)
{
    char path[128];
    int fd,pid = 0;
    DIR *pDir;
    struct dirent *pDEntry;
    char cReadBuffer[4096];
    char* Name = (char*)pArg;

    TraceString("vTriggerSigRtMinToSpecPrc Start");

    OSAL_s32ThreadWait(40000);
    pDir = opendir("/run/OSAL/Processes");
    TraceString("Search for %s ",Name);
    while( NULL != (pDEntry = readdir( pDir )) )
    {
        if(pDEntry->d_name[0] != '.')
        {
            snprintf(path,128,"/run/OSAL/Processes/%s",pDEntry->d_name);
            fd = open(path,O_RDONLY);
            if(fd != -1)
            {
                if(read(fd,cReadBuffer,4096) != -1)
                {
                     if(strstr(cReadBuffer,Name))
                     {
                          pid = atoi(pDEntry->d_name);
                          TraceString("Found process %s with ID %d failed errno:%d",Name,pid);
                          close(fd);
                          break;
                     }
                }
                close(fd);
            }
        }
    }
    closedir(pDir);

    if(pid > 0)
    {
         while(1)
         {
             if(kill(pid, SIG_BACKTRACE) < 0)
             {
                  TraceString("kill SIG_BACKTRACE for Process %d failed errno:%d",pid);
             }
             OSAL_s32ThreadWait(3000);
         }
    }
}
void StartSigRtMinSpecTest(char* Name)
{
    OSAL_trThreadAttribute  attr;
    attr.szName = (tString)"SigRtMinSpec";/*lint !e1773 */  /*otherwise linker warning */
    attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
    attr.s32StackSize = minStackSize;
    attr.pfEntry = (OSAL_tpfThreadEntry)vTriggerSigRtMinToSpecPrc;
    attr.pvArg = (void*)Name;
    if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
    {
    }
}


void vTriggerSigRtMinToAllPrc(void* pArg)
{
    char path[128];
    int fd,pid;
    DIR *pDir;
    struct dirent *pDEntry;

    TraceString("vTriggerSigRtMinToAllPrc Start");

    OSAL_s32ThreadWait(10000);
    while(1)
    {
        pDir = opendir("/run/OSAL/Processes");
        if(pDir)
        {
            while( NULL != (pDEntry = readdir( pDir )) )
            {
              if(pDEntry->d_name[0] != '.')
              {
                  snprintf(path,128,"/proc/%s",pDEntry->d_name);
                  fd = open(path,O_RDONLY);
                  if(fd != -1)
                  {
                      pid = atoi(pDEntry->d_name);
                      if((pid > 0)&&(pid != getpid()))
                      {
                    /*      if(kill(pid, SIGUSR2) < 0)
                          {
                                TraceString("kill SIGUSR2 for Process %d failed errno:%d",pid,errno);
                          }
                          OSAL_s32ThreadWait(3);*/
    
                          if(kill(pid, SIG_BACKTRACE) < 0)
                          {
                              TraceString("kill SIG_BACKTRACE for Process %d failed errno:%d",pid,errno);
                          }
                          else
                          {
                              TraceString("kill SIG_BACKTRACE for Process %d ",pid);
                          }
                          OSAL_s32ThreadWait(10000);
                      }
                      close(fd);
                  }
              }
            }
            rewinddir(pDir);
            closedir(pDir);
        }
        else
        {
          TraceString("cannot open /run/OSAL/Processes  %d",errno);
          OSAL_s32ThreadWait(1000);
        }
    }

    TraceString("vTriggerSigRtMinToAllPrc End");
}

void StartSigRtMinTest(void)
{
    OSAL_trThreadAttribute  attr;
    attr.szName = (tString)"vTriggerSigRtMinToAllPrc";/*lint !e1773 */  /*otherwise linker warning */
    attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
    attr.s32StackSize = minStackSize;
    attr.pfEntry = (OSAL_tpfThreadEntry)vTriggerSigRtMinToAllPrc;
    attr.pvArg = NULL;
    if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
    {
    }
}


void  vCheckRemount(void)
{
     OSAL_tIODescriptor PrmDesc,fd;
     PrmDesc = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY);
     if( PrmDesc != OSAL_ERROR )
     {
         OSAL_trRemountData rRemountData;
         rRemountData.u16AppID = OSAL_C_U16_DAPI_APPID;                /* ID of the application                    */
         rRemountData.szPath    = "/var/opt/bosch/navdata";
         rRemountData.szOption = "rw";         
         if(OSAL_s32IOControl(PrmDesc, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
         {
             OSAL_s32ThreadWait(15000);
             if((fd = OSAL_IOOpen("/dev/cryptnav/Test.txt", OSAL_EN_READWRITE )) == OSAL_ERROR)
             {
                 if((fd = OSAL_IOCreate("/dev/cryptnav/Test.txt", OSAL_EN_READWRITE)) != OSAL_ERROR)
                 {
                      OSAL_s32IOWrite(fd,(tPCS8)"Dummy",5);
                 }
                 OSAL_s32IOClose(fd);
             }
             else
             {
                OSAL_s32IOClose(fd);
                OSAL_s32IORemove("/dev/cryptnav/Test.txt");
             }
             rRemountData.szOption = "ro";         
             if(OSAL_s32IOControl(PrmDesc, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
             {
             }
         }
         OSAL_s32IOClose(PrmDesc);
     }
}

tCString PrcName = "fc_internetapplication";

 
void TestExtDir2(void)
{
   OSAL_tIODescriptor hDevice;
   OSAL_trIOCtrlExt2Dir rDir;
   OSAL_trIOCtrlExt2Dirent  rEntry[10];
   int i;
   OSAL_trIOCtrlExt2Dirent* pEntry;
    rDir.s32Cookie       = 0;
    rDir.u32NbrOfEntries = 10;
    rDir.u32NbrEntries   = 0;
    rDir.pDirent         = &rEntry[0];
   
 	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_FFS, OSAL_EN_READONLY );
    if(hDevice != OSAL_ERROR)
	{
	   while(1)
	   {
	      if(OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_FIOREADDIREXT2, (intptr_t)&rDir) == OSAL_OK)
	      {
  	         for(i=0;i<rDir.u32NbrEntries;i++)
		     {
				pEntry = &rDir.pDirent[i];
			    TraceString("ExtDir2 Entry %d %s",i,pEntry->s8Name);
			 }
		  }
		  else
		  {
		     break;
		  }
		  if(rDir.u32NbrEntries < rDir.u32NbrOfEntries)break;
	   }
	   OSAL_s32IOClose(hDevice);
	}
}
 
#endif

/*****************************************************************************
*
* FUNCTION:     vDisplayManual
*
* DESCRIPTION: Displays all commands via osal_core.trt              
*
* PARAMETER:    none
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vDisplayManual(void)
{
     OSAL_trTimeDate rCurrentTime;
     if(OSAL_s32ClockGetTime(&rCurrentTime) == OSAL_OK)
     {
         TraceString("OSAL UTC Time: %d.%d.%d Hour:%d Min:%d Sec:%d",
                         rCurrentTime.s32Day,rCurrentTime.s32Month,rCurrentTime.s32Year,
                         rCurrentTime.s32Hour,rCurrentTime.s32Minute,rCurrentTime.s32Second);
     }

     TraceString((const char*)"------------------------------- OSAL Debug Support -------------------------------");
     TraceString((const char*)"OSAL_LI_GET_RESOURCES <Type>            -> Trace out OSAL Resources Info, selection via TAB");
     TraceString((const char*)"OSAL_LI_GET_MQ_STATUS                   -> Trace out OSAL Message Queue Status");
     TraceString((const char*)"OSAL_LI_GET_MQ_MAX_COUNT                -> Trace out OSAL MQ maximum load, support has to be activated");
     TraceString((const char*)"OSAL_LI_GET_MSGQPOOL_INFO               -> Trace out mempool stati of OSAL core");
     TraceString((const char*)"OSAL_LI_GET_RESOURCE_INFO               -> Trace out currently allocated OS resources");
     TraceString((const char*)"OSAL_LI_GET_SYSHEAP                     -> Trace out Linux Memory Info");
     TraceString((const char*)"OSAL_LI_GET_TOP                         -> Trace out Linux Top Info");
     TraceString((const char*)"OSAL_LI_GET_PS        <PID>             -> Trace out various Linux proc file system Infos for specific process, 0 for all processes");
     TraceString(                                                                        "Status: R is running, S is sleeping in an interruptible wait, D is waiting in uninterruptible");
     TraceString(                                                                        "disk sleep, Z is zombie, T is traced or stopped (on a signal), and W is paging");
     TraceString((const char*)"                                                     process, 0 for all processes");
     TraceString((const char*)"OSAL_LI_GET_PRC_MEM_PID  <PID>          -> Trace out process memory for given PID");
     TraceString((const char*)"OSAL_LI_GET_PRC_MEM_NAME <name>         -> Trace out process memory for given name pattern");
     TraceString((const char*)"OSAL_LI_SET_SHELL_CMD  <String>         -> give a shell caommand to Linux system -> ; symbol used as seperator");
     TraceString((const char*)"OSAL_LI_GET_OSALPRC_INFO                -> get OSAL process data");
     TraceString((const char*)"OSAL_LI_GET_SIGNAL_MASK  <Pattern> <SIG>-> Checks for a specific signal Pattern like SigBlk for Signal bit");
     TraceString((const char*)"OSAL_LI_GET_CALLSTACKS_BY_NAME  <Name>  -> Trigger callstack generation via process name");
     TraceString((const char*)"OSAL_LI_GET_CALLSTACKS_BY_PID  <PID>    -> Trigger callstack generation via process ID ");

     TraceString((const char*)"OSAL_LI_SET_STRACE_PID <PID> <Level>    -> Activates OSAL strace for specified process with specilied level");
     TraceString((const char*)"OSAL_LI_SET_ASSERT_MODE                 -> Set OSAL ASSERT mode. ASSERT_MODE selection via TAB");
     TraceString((const char*)"OSAL_LI_GET_ASSERT_MODE                 -> Get OSAL ASSERT mode");
     TraceString((const char*)"OSAL_LI_TEST_ASSERT                     -> Test OSAL ASSERT behaviour. ASSERT_TYPE selection via TAB");
     TraceString((const char*)"OSAL_LI_SET_TRACE_MQ <switch> <MQ>      -> Switch on/off traces for defined OSAL message queue (name)");
     TraceString((const char*)"OSAL_LI_SET_TRACE_SEM <switch> <MQ>     -> Switch on/off traces for defined OSAL Semaphore (name)");
     TraceString((const char*)"OSAL_LI_SET_TRACE_EVENT <switch> <MQ>   -> Switch on/off traces for defined OSAL Event (name)");
     TraceString((const char*)"OSAL_LI_SET_TRACE_SHMEM <switch> <MQ>   -> Switch on/off traces for defined OSAL Shared_Memory (name)");
     TraceString((const char*)"OSAL_LI_SET_TRACE_TIM <switch> <TID> <Timeout> <Interval>  ");    
     TraceString((const char*)"                                        -> Switch on/off traces for defined OSAL Timer of Task & Timeout & Interval");
     TraceString((const char*)"OSAL_LI_SET_TRACE_TIM_PRC <switch> <PID>-> Switch on/off traces of OSAL Timer for specified process");    
     TraceString((const char*)"OSAL_LI_SET_DEV_TRACE <PID> <device>    -> Switch on/off traces of OSAL device with PID (0=all processes)-> OSAL_LI_SET_DEV_TRACE 0 /dev/gpio");
     TraceString((const char*)"OSAL_MQ_SET_SV_CCA <switch>             -> Switch on/Off supervision of defect CCA messages in system");
     TraceString((const char*)"OSAL_MQ_SET_FILTER_CCA <src> <dst> <ServId> <FuncId>");
     TraceString((const char*)"                                        -> Switch on/Off traces for specified CCA messages 65535 for not set");

     TraceString((const char*)"OSAL_LI_FS_COPY_DIR <source,dest>     -> copy a directory from source to destination '/host/test,/nor0/test'");
     TraceString((const char*)"OSAL_LI_FS_MK_DIR <path>                 -> create a new dir '/nor0/TestDir'");
     TraceString((const char*)"OSAL_LI_FS_RM_DIR <path to dir>        -> remove a dir '/nor0/TestDir'");
     TraceString((const char*)"OSAL_LI_FS_READ_DIR <path to dir>     -> read the directory and display contents '/nor0/TestDir'");
     TraceString((const char*)"OSAL_LI_FS_READ_DIR_SIZE <path to dir> -> read the directory content size ");
     TraceString((const char*)"OSAL_LI_FS_READ_FILE <path to file>  -> read the file and display contents '/nor0/Testfile.txt'");
     TraceString((const char*)"OSAL_LI_FS_COPY_FILE <source,dest>    -> copy a file from source to destination '/host/file.txt,/nor0/file.txt'");
     TraceString((const char*)"OSAL_LI_FS_RM_FILE <path to file>     -> remove a file from file system '/nor0/file.txt'");
     TraceString((const char*)"OSAL_LI_FS_RM_FILESELECTION <path to files> -> remove a file from file system with wildcards'/nor0/*.*'");
     TraceString((const char*)"OSAL_LI_FS_READ_DIR_SIZE <path to dir> -> determines from directory occupied memory ");

     TraceString((const char*)"OSAL_LI_STACK_CHECK <PID> <intervall> -> start stack measurement for PID (0=all processes) and intervall/");
     TraceString((const char*)"OSAL_LI_START_PROC <path to bin>      -> create a new USER Process with path below /opt/bosch/processes/");

     TraceString((const char*)"OSAL_LI_LOAD_TASK_SPAWN     <Prio> <Load> <sleep_periode>  ");
     TraceString((const char*)"                     while:  <Prio>=Threads priority     <Load>=System load in %%  <sleep_periode>= duration of sleep periode (ms)");
     TraceString((const char*)"OSAL_LI_LOAD_TASK_KILL <LoadTaskID>  ->  <LoadTaskID> (from 'OSAL_GET_LOAD_TASK_STATUS')");
     TraceString((const char*)"OSAL_LI_LOAD_TASK_STATUS_GET            -> Display the status of all load tasks");
     TraceString((const char*)"OSAL_LI_REDUCE_MEMORY <kb> <kb> <Seconds>");
     TraceString((const char*)"                                             -> Reduce Memory size to x MB with Memory Allocation of y Bytes intervall of z seconds");


     TraceString((const char*)"MSGPOOL_LI_BLOCKS                          -> Trace CCA message list ");
     TraceString((const char*)"MSGPOOL_LI_ABS_SIZE                        -> Trace the absolute size of the CCA message pool");
     TraceString((const char*)"MSGPOOL_LI_CUR_SIZE                        -> Trace the current size of the CCA message pool");
     TraceString((const char*)"MSGPOOL_LI_MIN_SIZE                        -> Trace the minmal size of the CCA message pool");
     TraceString((const char*)"MSGPOOL_LI_MAX_SIZE                        -> Trace the maximal CCA message size");
     TraceString((const char*)"OSAL_LI_TRACE_REG <SELECT>              -> Trace registry content from path selected via TAB");
     TraceString((const char*)"OSAL_LI_TRACE_REG_PRC <Subdir>         -> Trace registry content from subdir path below /LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS");
     TraceString((const char*)"OSAL_LI_GET_PRM_INFO                      -> Traces current stati of PRM");
     TraceString((const char*)"----------------------------------------------------------------------------------");

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
     TraceString("OSAL internal Error Memory is used");
#else
     TraceString("ADIT Error Memory via IF Lib is used");
#ifdef SET_ERRMEM_TIME
     TraceString("OSAL is setting time for ADIT Error Memory");
#endif
#endif
     if(TraceOut)
     {
        TraceString("ADIT Trace via IF Lib is used");
     }
     else
     {
        TraceString("OSAL internal Trace solution is used");
     }
#ifdef AM_BIN                
     TraceString("ADIT Automounter via IF Lib is used");
#endif
#ifdef VOLATILE_DIR                
     TraceString("As volatile directory %s is used",VOLATILE_DIR);
#endif
#ifdef PRM_LIBUSB_CONNECTION                
     TraceString("PRM USB Lib is active");
#endif
}


void vGetResourceData(int Val)
{
  switch (Val) 
  {
     case 0x0a:    // // Task Data requested
          vTraceTCB(1);
         break;
     case 0x0b:    // Event Data requested
          vTraceECB(1);
         break;
     case 0x0c:  // Semaphore Data requested
          vTraceSCB(1);
         break;
     case 0x0d:  // Timer Data requested
          vTraceTimCB(1);
         break;
     case 0x0f: // Message Queue Data requested
             vTraceMQCB(0); 
         break;
     case 0x10: // Mutex Queue Data requested
     //        vTraceMTXCB(0); 
         break;
     case 0x11: // Shared Memory data
          vTraceShMem(0);
         break;
     case 0x12:
          vTraceOpenFiles(0);
         break;
     case 0x0e: // MemPool printed by TraceMQCB
         // fallthrough
     default:
             TraceString((const char*)"Invalid resource Type");
          break;
    }
}


void vSetFilterForCcaMsg(tU8* pu8Buffer)
{
/* set filter elements */
    pOsalData->u16CheckSrc = 0x100 * (*pu8Buffer);
    pu8Buffer++;
    pOsalData->u16CheckSrc += *pu8Buffer;
    pu8Buffer++;

    pOsalData->u16CheckDst = 0x100 * (*pu8Buffer);
    pu8Buffer++;
    pOsalData->u16CheckDst += *pu8Buffer;
    pu8Buffer++;

    pOsalData->u16ServId = 0x100 * (*pu8Buffer);
    pu8Buffer++;
    pOsalData->u16ServId += *pu8Buffer;
    pu8Buffer++;

    pOsalData->u16FuncId = 0x100 * (*pu8Buffer);
    pu8Buffer++;
    pOsalData->u16FuncId += *pu8Buffer;


    TraceString("CCA Filter Src:%d Dest:%d Srv:%d, Func:%d",
                pOsalData->u16CheckSrc,
                pOsalData->u16CheckDst,
                pOsalData->u16ServId,
                pOsalData->u16FuncId);
}


void PrintMemorySizes(void)
{
    tU32 u32Tsk,u32Ev,u32Sem,u32ShM,u32Tim,u32Mq;
    tU32 i;
    for(i=0;i<pOsalData->u32MaxNrProcElements;i++)
    {
         if(prProcDat[i].u32Pid)
         {
             TraceString("PID:%d Name:%s Prio:%d MQ:%s",
                             prProcDat[i].u32Pid,
                             prProcDat[i].pu8AppName,
                             prProcDat[i].u32Priority,
                             prProcDat[i].szMqName);
          }
    }

    u32Ev  = pOsalData->u32MaxNrEventElements * sizeof(trEventElement);
    u32Tsk = pOsalData->u32MaxNrThreadElements * sizeof(trThreadElement) + (pOsalData->u32MaxNrThreadElements+1) * sizeof(trElementNode);
    u32ShM = pOsalData->u32MaxNrSharedMemElements * sizeof(trSharedMemoryElement);
    u32Sem = pOsalData->u32MaxNrSemaphoreElements * sizeof(trSemaphoreElement);
    u32Tim = pOsalData->u32MaxNrTimerElements * sizeof(trTimerElement);
    u32Mq  = pOsalData->u32MaxNrMqElements * sizeof(trMqueueElement) + pOsalData->u32MaxNrMqElements * sizeof(trStdMsgQueue);

    TraceString("OSAL Shared Mem %d -> Task:%d | Events: %d | Semaphores:%d | MQ:%d | Timer:%d | ShMem:%d",
                    sizeof(trGlobalOsalData), u32Tsk,u32Ev,u32Sem,u32Mq,u32Tim,u32ShM);
    
    vTracePoolInfo(&DescMemPoolHandle);
    vTracePoolInfo(&FileMemPoolHandle);
    vTracePoolInfo(&MqMemPoolHandle);
    vTracePoolInfo(&EvMemPoolHandle);
    vTracePoolInfo(&SemMemPoolHandle);
}

tBool bTermTaskFlag    = FALSE;

typedef struct {
OSAL_tMSecond awake_time;
OSAL_tMSecond sleep_time;
tU32 u32Prio;
tU32 u32CPU;
}trConfData;
 
static trConfData rData[2];

void vOsalSystemLoadFunc(void* pArg)
{
      OSAL_tMSecond last_timestamp  = 0;
      OSAL_tMSecond new_timestamp    = 0;
      OSAL_tMSecond curr_awake_time = 0;
      OSAL_tThreadID s32Tid = OSAL_ThreadWhoAmI();
  //    cpu_set_t set;

      trConfData* pData = (trConfData*)pArg;
      if(pData)
      {
            TraceString("Started Task Awake:%d Sleep:%d Prio:%d",pData->awake_time,pData->sleep_time,pData->u32Prio);
            if(OSAL_s32ThreadPriority(s32Tid,pData->u32Prio) == OSAL_OK)
            {
                 TraceString("Set Priority to %d succeeded",pData->u32Prio);
            }
            else
            {
                 TraceString("Set Priority to %d succeeded",pData->u32Prio);
            }

    /*        CPU_ZERO(&set); 
          CPU_SET(pData->u32CPU, &set );
          if(pthread_setaffinity_np(pthread_self(), sizeof( cpu_set_t ), &set) != 0)
          {
                TraceString("Bind Task to CPU failed errno:%d",errno);
          }
            else
            {
                TraceString("Bind Task to CPU failed errno:%d",errno);
            }*/

            while(bTermTaskFlag == FALSE)
            {
                TraceString("Sleep for %d msec",pData->sleep_time);
                OSAL_s32ThreadWait((tU32)pData->sleep_time);
                last_timestamp = OSAL_ClockGetElapsedTime();

                do {
                     new_timestamp = OSAL_ClockGetElapsedTime();
                     if(new_timestamp >= last_timestamp)
                     {
                          curr_awake_time = (new_timestamp - last_timestamp);
                     }
                     else
                     {
                          curr_awake_time = (last_timestamp - new_timestamp);
                     }
                }while((curr_awake_time < pData->awake_time) && (bTermTaskFlag == FALSE));
            }
      }
}


void StartSystemLoad(OSAL_tMSecond awake_time , OSAL_tMSecond sleep_time, char cPrio, char cNrTsk)
{
     OSAL_trThreadAttribute tr_thread_attr; 
     if(cNrTsk > 0)
     {
         rData[0].awake_time = awake_time;
         rData[0].sleep_time = sleep_time;
         rData[0].u32Prio     = (tU32)cPrio;
         rData[0].u32CPU      = CPU0;
         tr_thread_attr.u32Priority  = (tU32)70;
         tr_thread_attr.s32StackSize = 64*1024;
         tr_thread_attr.pfEntry        = (OSAL_tpfThreadEntry) vOsalSystemLoadFunc;
         tr_thread_attr.pvArg          = (void*)&rData[0];
         tr_thread_attr.szName         = (tString)"OSAL_TEST_LOAD1";//lint !e1773: Attempt to cast away const (or volatile)
         if(OSAL_ThreadSpawn(&tr_thread_attr) == OSAL_ERROR)
         {
            TraceString("OSAL_ThreadSpawn failed");
         }
         if(cNrTsk > 1)
         {
             rData[1].awake_time = awake_time;
             rData[1].sleep_time = sleep_time;
             rData[1].u32Prio     = (tU32)cPrio;
             rData[1].u32CPU      = CPU1;
             tr_thread_attr.u32Priority  = (tU32)70;
             tr_thread_attr.s32StackSize = 64*1024;
             tr_thread_attr.pfEntry        = (OSAL_tpfThreadEntry) vOsalSystemLoadFunc;
             tr_thread_attr.pvArg          = (void*)&rData[1];
             tr_thread_attr.szName         = (tString)"OSAL_TEST_LOAD2";//lint !e1773: Attempt to cast away const (or volatile)
             if(OSAL_ThreadSpawn(&tr_thread_attr) == OSAL_ERROR)
             {
                 TraceString("OSAL_ThreadSpawn failed");
             }
         }
     }
     else
     {
         bTermTaskFlag = TRUE;
     }

}



/*****************************************************************************
*
* FUNCTION:     vSysCallbackHandler
*
* DESCRIPTION: Handling of OSAL Callbacks              
*
* PARAMETER:    void* pvBuffer    command via trace
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
typedef union
{
  tU8 u8Array[4];
  tU32 u32Val;
}tuConverter;

static tU32 u32Tid;
static tU32 u32Timeout;
static tU32 u32Interval;
static tuConverter rConvert;


void vSysCallbackHandler(void* pvBuffer )
{
    tPU8 pu8Buffer;
    pu8Buffer = (tPU8)pvBuffer;
    teAssertMode eAssertMode;
    int i;

    switch ((tS32)pu8Buffer[2]) 
    {
      case OSAL_CORE_RESOURCE_DATA: 
             vGetResourceData((tS32)pu8Buffer[3]);
            break;
      case OSAL_MK_DIR:
             vMkDir(&pu8Buffer[3]);          
            break;
      case OSAL_RM_DIR:
             vRmDir(&pu8Buffer[3]);          
            break;
      case OSAL_COPY_FILE:
             vCopyFile(&pu8Buffer[3]);      
            break;
      case OSAL_COPY_DIR:
             vCopyDir(&pu8Buffer[3]);
            break;
      case OSAL_RM_FILE:
             vRmFile(&pu8Buffer[3]);      
            break;
      case OSAL_READ_FILE:
             vReadFile(&pu8Buffer[3],FALSE);      
            break;
      case OSAL_RM_SELECTED_FILES:
             vRmFileSelection(&pu8Buffer[3]);
            break;
      case OSAL_READ_DIR:  
             u32Read_Rem_Dir(&pu8Buffer[3],0,NULL);
            break;
      case OSAL_DIR_CONTENT_SIZE: 
            {
              tU32 u32Content = 0;
              tU32 u32Return = 0;
              TraceString((const char*)"Start investigation of directory, wait for a moment...");
              u32Return = u32Read_Rem_Dir(&pu8Buffer[3],2,&u32Content);
              TraceString("Directory contains %d Files with %d Bytes",(tS32)u32Return,(tS32)u32Content);
            }
          break;
     case OSAL_START_PROC:
            vStartProc(&pu8Buffer[3],PRC_TYPE_USERPRC);      
          break;
     case OSAL_MANUAL:
            vDisplayManual();
          break;
     case OSAL_CORE_DATA_MQ_STAT:
          vGetMsgQueueStatus();
         break;
     case OSAL_CORE_MQ_FILL_LEVEL:
            vGetMsgQueueMaxFillLevels();
          break;
     case OSAL_CORE_TRACE_MQ: 
              if(pu8Buffer[4] == 0)vSetTraceFlagForChannel(NULL,pu8Buffer[3]);
            else vSetTraceFlagForChannel((char*)&pu8Buffer[4],pu8Buffer[3]);
          break;
     case OSAL_MQ_SET_SV_CCA: 
            pOsalData->bCheckCcaMsg = pu8Buffer[3];
            pOsalData->bTraceAllCca = pu8Buffer[3];
          break;
     case OSAL_MQ_SET_FILTER_CCA: 
            pOsalData->bCheckCcaMsg = pu8Buffer[3];
            if(pOsalData->bCheckCcaMsg)
            {
                vSetFilterForCcaMsg(&pu8Buffer[4]);
            }
          break;
     case OSAL_CORE_TRACE_SEM: 
            if(pu8Buffer[4] == 0)vSetTraceFlagForSem(NULL,pu8Buffer[3]);
            else vSetTraceFlagForSem((char*)&pu8Buffer[4],pu8Buffer[3]);
          break;
     case OSAL_CORE_TRACE_MTX:
            if(pu8Buffer[4] == 0)vSetTraceFlagForMtx(NULL,pu8Buffer[3]);
            else vSetTraceFlagForMtx((char*)&pu8Buffer[4],pu8Buffer[3]);
          break;
     case OSAL_CORE_TRACE_FLG: 
            if(pu8Buffer[4] == 0)vSetTraceFlagForEvent(NULL,pu8Buffer[3]);
            else vSetTraceFlagForEvent((char*)&pu8Buffer[4],pu8Buffer[3]);
          break;
     case OSAL_CORE_TRACE_TIM: 
            rConvert.u8Array[0] = pu8Buffer[7];
            rConvert.u8Array[1] = pu8Buffer[6];
            rConvert.u8Array[2] = pu8Buffer[5];
            rConvert.u8Array[3] = pu8Buffer[4];
            u32Tid = rConvert.u32Val;
            rConvert.u8Array[0] = pu8Buffer[11];
            rConvert.u8Array[1] = pu8Buffer[10];
            rConvert.u8Array[2] = pu8Buffer[9];
            rConvert.u8Array[3] = pu8Buffer[8];
            u32Timeout = rConvert.u32Val;
            rConvert.u8Array[0] = pu8Buffer[15];
            rConvert.u8Array[1] = pu8Buffer[14];
            rConvert.u8Array[2] = pu8Buffer[13];
            rConvert.u8Array[3] = pu8Buffer[12];
            u32Interval = rConvert.u32Val;
            vActivateTimerTrace(pu8Buffer[3],u32Tid,u32Timeout,u32Interval);
          break;
     case OSAL_CORE_TRACE_TIM_PRC:
            rConvert.u8Array[3] = pu8Buffer[4];
            rConvert.u8Array[2] = pu8Buffer[5];
            rConvert.u8Array[1] = pu8Buffer[6];
            rConvert.u8Array[0] = pu8Buffer[7];
            vActivatePidTimerTrace(pu8Buffer[3],rConvert.u32Val);
          break;

     case OSAL_CORE_TRACE_SHMEM:
            vSetTraceFlagForShMem((char*)&pu8Buffer[4],pu8Buffer[3]);
          break;
     case OSAL_GET_MSGQPOOL_INFO:
            if(LockOsal(&pOsalData->MqueueTable.rLock) == OSAL_OK)
            {
                TraceSpecificPoolInfo(&MqMemPoolHandle);
                UnLockOsal(&pOsalData->MqueueTable.rLock);
            }
          break;
     case RESOURCE_INFO:
            vShowCurrentResourceSitutaion();
          break;
     case OSAL_SET_ASSERT_MODE:
            OSAL_vSetAssertMode((teAssertMode)pu8Buffer[3]);
            {             
                eAssertMode = OSAL_vGetAssertMode();
                OSAL_M_INSERT_T8(pu8Buffer, 0x20);
                OSAL_M_INSERT_T8(pu8Buffer+1, (char)(eAssertMode));
                LLD_vTrace(TR_CLASS_ASSERT, TR_LEVEL_FATAL,pu8Buffer,2);
            }
          break;
     case OSAL_GET_ASSERT_MODE:
            {             
                eAssertMode = OSAL_vGetAssertMode();
                OSAL_M_INSERT_T8(pu8Buffer, 0x20);
                OSAL_M_INSERT_T8(pu8Buffer+1, (char)(eAssertMode));
                LLD_vTrace(TR_CLASS_ASSERT, TR_LEVEL_FATAL,pu8Buffer,2);
            }
          break;
     case OSAL_TEST_ASSERT:
            if (pu8Buffer[3] == 0)
            {
                  FATAL_M_ASSERT_ALWAYS();
            }
            else
            {
                  NORMAL_M_ASSERT_ALWAYS();
            }
          break;
     case OSAL_SET_EMPTY_POOL_CHECK:
            vSetEmptyPoolInvestigation(pu8Buffer[3]);
          break;
     case OSAL_STACK_CHECK:
            rConvert.u8Array[0] = pu8Buffer[6];
            rConvert.u8Array[1] = pu8Buffer[5];
            rConvert.u8Array[2] = pu8Buffer[4];
            rConvert.u8Array[3] = pu8Buffer[3];
            u32Tid = rConvert.u32Val;
            rConvert.u8Array[0] = pu8Buffer[10];
            rConvert.u8Array[1] = pu8Buffer[9];
            rConvert.u8Array[2] = pu8Buffer[8];
            rConvert.u8Array[3] = pu8Buffer[7];
            u32Interval = rConvert.u32Val;
            if((u32Interval < 1000)&&(u32Interval != 0))u32Interval=1000;
            TraceString("Start Stack Supervision for PID:%u with Intervall:%u",u32Tid,u32Interval);
            TriggerStackCheck((int)u32Tid,(int)u32Interval);
          break;
     case OSAL_SET_SHELL_CMD:
             char szCommand[210];
             OSAL_szStringCopy(szCommand,(const char*)&pu8Buffer[3]);
             OSAL_szStringConcat(szCommand, "\n");
             for(i=0;i<200;i++)
             {
                  if(szCommand[i]=='\n')break;
                  if((szCommand[i]==';')&&(szCommand[i+1]!=';'))szCommand[i]= ' ';/*lint !e661 PQM_authorized_530 */
             }
             TraceString(szCommand);
             system(szCommand);
            break;
     case OSAL_GET_OSAL_PROC_INFO:
             PrintMemorySizes();
            break;
     case OSAL_SET_OSAL_STRACE:
            {
              tS32 s32PidEntry,u32Pid,u32Level;
              rConvert.u8Array[0] = pu8Buffer[6];
              rConvert.u8Array[1] = pu8Buffer[5];
              rConvert.u8Array[2] = pu8Buffer[4];
              rConvert.u8Array[3] = pu8Buffer[3];
              u32Pid = rConvert.u32Val;
              rConvert.u8Array[0] = pu8Buffer[10];
              rConvert.u8Array[1] = pu8Buffer[9];
              rConvert.u8Array[2] = pu8Buffer[8];
              rConvert.u8Array[3] = pu8Buffer[7];
              u32Level = rConvert.u32Val;
              TraceString("OSAL STRACE for PID:%d with 0x%x",u32Pid,u32Level);

              /* check if callback handler task for this process already exists */  
              OSAL_tMQueueHandle hMq = GetPrcLocalMsgQHandle(u32Pid);
              if(hMq == 0)
              {
                 if(u32Pid == OSAL_ProcessWhoAmI())
                 {
                    TraceString("Ensure Callback handler task is started");
                    s32PidEntry = s32FindProcEntry(u32Pid);
                    if(s32PidEntry == OSAL_ERROR)
                    {
                       NORMAL_M_ASSERT_ALWAYS();
                    }
                    else
                    {
                       s32StartCbHdrTask(s32PidEntry);
                    }
                 }
                 else
                 {
                    TraceString("Start Callback handler task via Timer Task");
                    union sigval value = {0};
                    value.sival_int = u32Level;
                    sigqueue(u32Pid,pOsalData->u32TimSignal-2,value);
                 }
                 OSAL_s32ThreadWait(1000);
                 hMq = GetPrcLocalMsgQHandle(u32Pid);
              }
              tOsalMbxMsg rMsg = {0};
              rMsg.rOsalMsg.Cmd = MBX_STRACE;
              rMsg.rOsalMsg.ID = u32Level;
              if(OSAL_s32MessageQueuePost(hMq, (tPCU8)&rMsg, sizeof(tOsalMbxMsg), 0) == OSAL_ERROR)
              {        
                 TraceString("Cannot start OSAL STRACE");
              }
            }
            break;
#if !defined (OSAL_GEN3_SIM) /*TARGET CODE GEN3*/
#ifdef PRM_LIBUSB_CONNECTION
     case OSAL_USB_UV_ALLACTIVE:
     case OSAL_USB_UV_ALLINACTIVE:
     case OSAL_USB_UV_ACTIVE:
     case OSAL_USB_UV_INACTIVE:
     case OSAL_USB_OC_ACTIVE:
     case OSAL_USB_OC_INACTIVE:
     case OSAL_USB_SIG_UNDEF:
          vUSBPWRCallbackHandler(pvBuffer);
          break;
#endif
#endif
     case OSAL_FB_CHECK:
            TestFramebuffer();
          break;
     case OSAL_BLOCK_CPU:
            OSAL_tMSecond awake_time,sleep_time;
            rConvert.u8Array[0] = pu8Buffer[6];
            rConvert.u8Array[1] = pu8Buffer[5];
            rConvert.u8Array[2] = pu8Buffer[4];
            rConvert.u8Array[3] = pu8Buffer[3];
            awake_time = rConvert.u32Val;
            rConvert.u8Array[0] = pu8Buffer[10];
            rConvert.u8Array[1] = pu8Buffer[9];
            rConvert.u8Array[2] = pu8Buffer[8];
            rConvert.u8Array[3] = pu8Buffer[7];
            sleep_time = rConvert.u32Val;
            StartSystemLoad(awake_time , sleep_time, pu8Buffer[11],pu8Buffer[12]);
            break;
     case OSAL_CHECK_SIGNAL:
            if(pu8Buffer[4] <= 64)
            {
                 tCString szType;
                 if(pu8Buffer[3] == 0)szType = "SigBlk:";
                 else if(pu8Buffer[3] == 1)szType = "SigCgt:";
                 else szType = "SigIgn:";
                 vCheckSignalMask(pu8Buffer[4],szType,TRUE);
            }
            else
            {
                 TraceString("Invalid signal number was given");
            }
            break;
     case OSAL_CALLSTACKS_BY_NAME:
            TriggerCallstackGenerationByName((char*)&pu8Buffer[3]);
            break;    
     case OSAL_CALLSTACKS_BY_PID:
            rConvert.u8Array[0] = pu8Buffer[7];
            rConvert.u8Array[1] = pu8Buffer[6];
            rConvert.u8Array[2] = pu8Buffer[5];
            rConvert.u8Array[3] = pu8Buffer[4];
            TriggerCallstackGenerationByPid(rConvert.u32Val);
            break;    
     default: // do nothing
              TraceString((const char*)"Unknown Command for LINUX OSAL");
          break;
    }
}


static tBool bIsNewTime(const trErrmemEntry* entry)
{
  return(entry->rEntryTime.s32Day != old_Time_stamp.s32Day ||
            entry->rEntryTime.s32Month != old_Time_stamp.s32Month ||
            entry->rEntryTime.s32Year != old_Time_stamp.s32Year ||
            entry->rEntryTime.s32Hour != old_Time_stamp.s32Hour ||
            entry->rEntryTime.s32Minute != old_Time_stamp.s32Minute ||
            entry->rEntryTime.s32Second != old_Time_stamp.s32Second);
}

static tVoid vSaveCurrentTime(const trErrmemEntry* entry)
{
     old_Time_stamp.s32Year = entry->rEntryTime.s32Year;
     old_Time_stamp.s32Day = entry->rEntryTime.s32Day;
     old_Time_stamp.s32Month = entry->rEntryTime.s32Month;
     old_Time_stamp.s32Hour = entry->rEntryTime.s32Hour;
     old_Time_stamp.s32Minute= entry->rEntryTime.s32Minute;
     old_Time_stamp.s32Second = entry->rEntryTime.s32Second;
}

static tVoid vPrintSeparatorLine(tVoid)
{
/* it's only class ER_MEM_NEW_LINE, level ER_MEM_NEW_LINE and no data, but TraceOsalIO requires buf and size for output */
    tU8 buf[] = {0};
    /* This will output the separator line "---------------"*/
    LLD_vTrace(ER_MEM_NEW_LINE,TR_LEVEL_FATAL,buf,1);
}


tS32 s32PrintErrMemString(tU32 fd,char* cString)
{
     tU32 datalen = 0;
     char cDest[4*1024];
     char Destination[4];
     char cBuffer[240];
     tS32 s32Return = OSAL_OK;
     if(fd)
     {
       tU32 i,j;
       datalen = strlen("DATA:18 00 00 64 30 F1 ");
       strncpy((char*)&cDest[0],"DATA:18 00 00 64 30 F1 ",datalen);
       j=datalen;
       datalen = strlen(cString);
       strncpy((char*)cBuffer,cString,240);
       for(i=0;i< datalen;i++)
       {
          sprintf(Destination,"%02x", cBuffer[i]);  
                  cDest[j] = Destination[0];
                  j++;
                  cDest[j] = Destination[1];
                  j++;
                  cDest[j] = ' ';
                  j++;
       }
       cDest[j] = 0x0d;
       j++;
       cDest[j] = 0x0a;
       if(OSAL_s32IOWrite(fd,(tPCS8)cDest,j+1) != (tS32)(j+1))
       {
         s32Return = OSAL_ERROR;
       }
     }
     else
     {
        TraceString(cString);
     }
     return s32Return;
}


tU32 u32GetSharedMemSize(void)
{
    struct dirent*    pDirEnt=NULL;
    DIR* dir =0;
    tU32 u32Ret = 0;
    tCString DirPath = (tCString)"/dev/shm/";
    char cFilePath[128];
    struct stat rStat;

    strncpy(&cFilePath[0],DirPath,strlen(DirPath));
    dir = opendir(DirPath);
    if(dir)
    {
        pDirEnt = readdir((DIR*)dir);
        while(pDirEnt)
        {
            if(pDirEnt->d_name[0])
            {
                memset(cFilePath,0,128);
                strncpy(&cFilePath[0],DirPath,strlen(DirPath));
                strncat(cFilePath,pDirEnt->d_name,strlen(pDirEnt->d_name));
                if(stat(cFilePath,&rStat) == 0)
                {
                    u32Ret += rStat.st_size;
             //      TraceString("SHMEM %s Size:%d",cFilePath,rStat.st_size);
                }
            }
            pDirEnt = readdir(dir);
        }
        closedir(dir);
    }
    return u32Ret;
}


tBool bGetVersionInfo(char* pBuffer)
{
    tBool bRet = FALSE;
    int fd;
    char  ReadBuf[1024];
    char *pStart, *pEnd;
    tS32 size = 0;
    if((fd = open("/opt/bosch/base/registry/BuildVersion.reg", O_RDONLY|O_CLOEXEC,OSAL_ACCESS_RIGTHS)) != -1)
    {
        if(read(fd,ReadBuf,1024) > 0)
        {
            /* "BUILDVERSION_LABEL"="GMG3G_LINUX_15.0B039" */
            pStart = strstr(ReadBuf,"BUILDVERSION_LABEL");
            if(pStart)
            {
                pStart = pStart+strlen("BUILDVERSION_LABEL")+2;
                pEnd = strstr(pStart,"\n");
                if(pEnd)
                {
                    size = pEnd-pStart;
                    strncpy(pBuffer,pStart,size);
                    bRet = TRUE;
                }
            }
        }
        close(fd);
    }
    return bRet;
}

void bGetKernelVersionInfo(char* pBuffer)
{
    int fd;
    char  ReadBuf[128];
    system("uname -r > /tmp/KernelInfo.txt");
    strncpy(pBuffer,"Kernel Version:",128);

    if((fd = open("/tmp/KernelInfo.txt", O_RDONLY|O_CLOEXEC,OSAL_ACCESS_RIGTHS)) != -1)
    {
        if(read(fd,ReadBuf,128) < 0)
        {
            strncat(pBuffer,"Unknown",128);
        }
        else
        {
          ReadBuf[127] = '\0'; //ensure 0 terminatied string
          strncat(pBuffer,ReadBuf,128);
        }
        close(fd);
        remove("/tmp/KernelInfo.txt");
    }
    else
    {
        strncpy(pBuffer,"uname -r > /tmp/KernelInfo.txt failed ",128);
    }
}

/*****************************************************************************
*
* FUNCTION:     vTraceErrmem
*
* DESCRIPTION: trace out Error Memory              
*
* PARAMETER:    none
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
* 04.02.09  | Error memory o/p formatting                | SRJ5KOR
* --.--.--  | ----------------                              | -----
* 09.03.09  | MMS 225865 Fix                                             | SRJ5KOR
* --.--.--  | ----------------                              | -----
* 06.04.09  | changes to em_trace output                         | SRJ5KOR
* --.--.--  | ----------------                              | -----
* 09.04.09  |added TR_DEFAULT_STRING_CLASS                        | SRJ5KOR
* --.--.--  | ----------------                              | -----
*****************************************************************************/
tS32 s32TraceErrmem(tBool bExtended,OSAL_tIODescriptor fd)
{
     tS32 s32Return = OSAL_OK;
#define WHITE_SPACE 0x20
     OSAL_tIODescriptor d = 0;
     trErrmemEntry entry = {0};
     TR_tenTraceClass tr_class = TR_LAST_CLASS;
     tU32 len = 0;
     tU32 datalen = 0;
     tU8 au8Buf_err_mem[40] = {0};
     char cDest[4*1024];
     char Destination[4];

     entry.u16Entry = 0;
     entry.u16EntryCount = 0;
     entry.eEntryType = (tenErrmemEntryType)0;
     memset(&entry.rEntryTime,0,sizeof(entry.rEntryTime)); 
     entry.u16Align = 0;
          /* print SW version */
      
     s32PrintErrMemString(fd,(char*)"OSAL_EM_TRACE Start"); /*lint !e1773 */ /* otherwise compiler error */
    
     char cBuffer[150];
     memset(cBuffer,0,150);
     if(!bGetVersionInfo(cBuffer))
     {
         s32PrintErrMemString(fd,(char*)"OSAL 15.0"); /*lint !e1773 */ /* otherwise compiler error */
     }
     else
     {
         s32PrintErrMemString(fd,cBuffer);
     }
     bGetKernelVersionInfo(cBuffer);
     s32PrintErrMemString(fd,cBuffer);
    
     snprintf(cBuffer,150,"From %d OSAL Processes %d currently in use (Max used:%d)",pOsalData->u32MaxNrProcElements,(int)pOsalData->u32PrcResCount,(int)pOsalData->u32MaxPrcResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Events %d currently in use (Max used:%d)",pOsalData->u32MaxNrEventElements,(int)pOsalData->u32EvtResCount,(int)pOsalData->u32MaxEvtResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Threads %d currently in use (Max used:%d)",pOsalData->u32MaxNrThreadElements,(int)pOsalData->u32TskResCount,(int)pOsalData->u32MaxTskResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Message Queues %d currently in use (Max used:%d)",pOsalData->u32MaxNrMqElements,(int)pOsalData->u32MqResCount,(int)pOsalData->u32MaxMqResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Semaphores %d currently in use (Max used:%d)",pOsalData->u32MaxNrSemaphoreElements,(int)pOsalData->u32SemResCount,(int)pOsalData->u32MaxSemResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Timer %d currently in use (Max used:%d)",pOsalData->u32MaxNrTimerElements,(int)pOsalData->u32TimResCount,(int)pOsalData->u32MaxTimResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Shared Memory %d currently in use (Max used:%d)",pOsalData->u32MaxNrSharedMemElements,(int)pOsalData->u32ShMResCount,(int)pOsalData->u32MaxShMResCount);
     s32PrintErrMemString(fd,cBuffer);
     snprintf(cBuffer,150,"From %d OSAL Mutex %d currently in use (Max used:%d)",pOsalData->u32MaxNrMutexElements,(int)pOsalData->u32MutResCount,(int)pOsalData->u32MaxMutResCount);
     s32PrintErrMemString(fd,cBuffer);

     vGetPoolInfo(&DescMemPoolHandle,cBuffer,150);
     s32PrintErrMemString(fd,cBuffer);
     vGetPoolInfo(&FileMemPoolHandle,cBuffer,150);
     s32PrintErrMemString(fd,cBuffer);
     vGetPoolInfo(&MqMemPoolHandle,cBuffer,150);
     s32PrintErrMemString(fd,cBuffer);
     vGetPoolInfo(&EvMemPoolHandle,cBuffer,150);
     s32PrintErrMemString(fd,cBuffer);
     vGetPoolInfo(&SemMemPoolHandle,cBuffer,150);
     s32PrintErrMemString(fd,cBuffer);

     snprintf(cBuffer,150,"From %d Bytes registry %d currently in use (%d Bytes wasted) allocated:%d Bytes ",pOsalData->u32RegistryMemSize - pOsalData->u32RegistryLookUpMemSize,u32GetUsedRegistrySize(),pOsalData->u32LostRegBytes,pOsalData->u32RegistryMemSize);
     s32PrintErrMemString(fd,cBuffer);
     datalen = u32GetMqResources(&len);
     snprintf(cBuffer,150,"%d Bytes are used for %d LINUX  message queues",datalen,len);
     s32PrintErrMemString(fd,cBuffer);

     len = u32GetSharedMemSize();
     snprintf(cBuffer,150,"Shared Memory in tmpfs  /dev/shm %d kb ",(int)len/1000);
     s32PrintErrMemString(fd,cBuffer);

     snprintf(cBuffer,150,"%d of %d internal OSAL lock objects are in use",u32UsedSyncObj(),MAX_OSAL_LOCK);
     s32PrintErrMemString(fd,cBuffer);

     unsigned int TraceBlockMode = 0;
     unsigned int ProxyBlockMode = 0;
     if(get_blockmode_status)
     {
         get_blockmode_status(&TraceBlockMode,&ProxyBlockMode);
         snprintf(cBuffer,150,"Current BLOCKMODE Status{TRACE:%d  PROXY:%d }",TraceBlockMode,ProxyBlockMode);
         s32PrintErrMemString(fd,cBuffer);
     }
     len = 0;

     /* now look for errmem content */
     d = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
     if ((d != 0) && (d != OSAL_ERROR))
     {
         while( OSAL_s32IORead(d, (tPS8)(&entry), sizeof(entry)) > 0 )
         {
                if((entry.u16EntryLength > 0)&& (entry.u16EntryLength <=ERRMEM_MAX_ENTRY_LENGTH))
                {
                     /* set special trace for begin of assert and exception 
                          0x00df refers to TR_CLASS_EXCEPTION and 
                          0x0402 refers to TR_CLASS_ASSERT, 
                          For both type there is no difference with respect to EM_TRACE 
                          hence only one class code is used*/
                     if(((entry.au8EntryData[0] == 0xdf)&&(entry.au8EntryData[1]==0x00))
                          ||((entry.au8EntryData[0] == 0x02) && (entry.au8EntryData[1] == 0x04)))
                     {
                          /* Header output with newline */
                          tr_class = (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP;  /* header class */

                          /* Add separator line at begin of assert/exception output */
                          if (old_tr_class != (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP)/*lint !e650 */ /* PQM_authorized_435 otherwise compiler warning */
                          {                          
                                if(fd)
                                {
                                  int i;
                                  for(i=0;i<100;i++)
                                  { cDest[i] = '-';}
                                  cDest[100] = 0x0a;
                                  datalen = strlen("DATA:11 1C 00 00 00 00 DA \n");
                                  strncpy(&cDest[101],"DATA:11 1C 00 00 00 00 DA \n",datalen);
                                  if(OSAL_s32IOWrite(fd,(tPCS8)cDest,101+datalen) != (int)(101+datalen)) s32Return = OSAL_ERROR;
                                }
                                else
                                {
                                  vPrintSeparatorLine();
                                }
                          }
                     }
                     else
                     {
                          /* Header output */
                          tr_class = (TR_tenTraceClass)ER_MEM_CLASS;  /* header class */
                     }

                     /* test if new "EM-Entry"-message shall be traced out */
                     /* new "EM-Entry"-message with class and time shall be traced out only if entry trace class, type or time changes */
                     if ((old_tr_class != tr_class) || (old_entry_type != entry.u16Entry) || bIsNewTime(&entry))
                     {
                         /* Add separator line at end of assert/exception output */
                         if ((old_tr_class == (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP) && (tr_class != (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP))  /*lint !e650 */ /* PQM_authorized_435 otherwise compiler warning */
                         {
                             if(fd)
                             {
                                int i;
                                for(i=0;i<100;i++)
                                { cDest[i] = '-';}
                                cDest[100] = 0x0a;
                                datalen = strlen("DATA:11 1C 00 00 00 00 DA \n");
                                strncpy(&cDest[101],"DATA:11 1C 00 00 00 00 DA \n",datalen);
                                if(OSAL_s32IOWrite(fd,(tPCS8)cDest,101+datalen) != (int)(101+datalen)) s32Return = OSAL_ERROR;
                             }
                             else
                             {
                                vPrintSeparatorLine();
                             }
                          }
                          /* save current entry class, type and time */
                          vSaveCurrentTime(&entry);
                          old_tr_class = tr_class;
                          old_entry_type = entry.u16Entry;

                          /* create new "EM-Entry"-message */
                          len = 12;
                          OSAL_M_INSERT_T16( &au8Buf_err_mem[0], entry.u16Entry );
                          OSAL_M_INSERT_T16( &au8Buf_err_mem[2], entry.u16EntryCount );
                          OSAL_M_INSERT_T16( &au8Buf_err_mem[4], (tU16)entry.eEntryType);
                          au8Buf_err_mem[6]  = (tU8)entry.rEntryTime.s32Day;
                          au8Buf_err_mem[7]  = (tU8)entry.rEntryTime.s32Month;
                          /* Year is offset from 1900 since we want only 2 fields 
                                reduce the year to less than 100 value */
                          while (entry.rEntryTime.s32Year >= 100)
                          {
                                entry.rEntryTime.s32Year -= 100;
                          }
                          au8Buf_err_mem[8]  = (tU8)entry.rEntryTime.s32Year ;
                          au8Buf_err_mem[9]  = (tU8)entry.rEntryTime.s32Hour;
                          au8Buf_err_mem[10] = (tU8)entry.rEntryTime.s32Minute;
                          au8Buf_err_mem[11] = (tU8)entry.rEntryTime.s32Second;
                          if(fd)
                          {
                                tU32 i,j;
                                j = strlen("DATA:");
                                datalen = strlen("DATA:11 10 00 00 00 ");
                                strncpy(&cDest[0],"DATA:11 10 00 00 00 ",datalen);
                                if(tr_class == (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP)cDest[4+j] = 'b'; /*lint !e650 */ /*PQM_authorized_435 otherwise compiler warning */
                                else                                         cDest[4+j] = 'a';
                                j=datalen;
                                for(i=0;i< len;i++)
                                {
                                  sprintf(Destination,"%02x", au8Buf_err_mem[i]);  
                                  cDest[j] = Destination[0];
                                  j++;
                                  cDest[j] = Destination[1];
                                  j++;
                                  cDest[j] = ' ';
                                  j++;
                                }
                                cDest[j] = 0x0d;
                                j++;
                                cDest[j] = 0x0a;
                                if(OSAL_s32IOWrite(fd,(tPCS8)cDest,j+1) != (tS32)(j+1)) s32Return = OSAL_ERROR;
                          }
                          else
                          { 
                              /* output new "EM-Entry"-message */
                               LLD_vTrace(tr_class,TR_LEVEL_FATAL, au8Buf_err_mem,len);
                          }
                     }

                /* If bExtended == FALSE and entry.au8EntryData[2] == 10 (Line Feed -- \r)
                     message will not be printed */
                if ((bExtended) || (entry.au8EntryData[2] != 10))
                {
                     len = 0;
                     /* First 2 bytes of message string contains Class code*/
                     if((entry.u16EntryLength > 0)&& (entry.u16EntryLength <=ERRMEM_MAX_ENTRY_LENGTH))
                     {
                         len = entry.u16EntryLength - ERMEM_CLASS_LENGTH;
                     }
                     if(len > 0)
                     {
                          /* original trace output */
                           if(fd)
                           {
                                 tU32 i, j;
                                datalen = strlen("DATA:");
                                strncpy(&cDest[0],"DATA:",datalen);
                                sprintf(Destination,"%02x", entry.au8EntryData[1]);  
                                cDest[datalen] = Destination[0];
                                cDest[datalen+1] = Destination[1];
                                cDest[datalen+2] = ' ';
                                sprintf(Destination,"%02x", entry.au8EntryData[0]);  
                                cDest[datalen+3] = Destination[0];
                                cDest[datalen+4] = Destination[1];
                                strcpy(&cDest[datalen+5]," 00 00 00 ");
                                j=datalen + 15;
                                for(i=2;i<len+2;i++)
                                {
                                  sprintf(Destination,"%02x", entry.au8EntryData[i]);  
                                  cDest[j] = Destination[0];
                                  j++;
                                  cDest[j] = Destination[1];
                                  j++;
                                  cDest[j] = ' ';
                                  j++;
                                }
                                cDest[j] = 0x0d;
                                j++;
                                cDest[j] = 0x0a;
                                if(OSAL_s32IOWrite(fd,(tPCS8)cDest,j+1) != (tS32)(j+1)) s32Return = OSAL_ERROR;
                          }
                          else
                          {
                             /* if the data contains a valid trace class */
                             tr_class = (TR_tenTraceClass) ((entry.au8EntryData[0])+ ((entry.au8EntryData[1]) << 8));
                             LLD_vTrace(tr_class,TR_LEVEL_FATAL, &entry.au8EntryData[2],len);
                          }
                                /* ram2hi, 16.06.09: test if entry.au8EntryData holds a string
                                    (reworked "MMS Fix - 225865 else part added") 

                                    the above test for a valid trace class sometimes gives a wrong TRUE
                                    e.g. for string data "timeout" == tr_class 0x6974h UTIL_IsClassSelected returns TRUE
                                    in this case the string output was missing

                                    by now it's too late to correct the cause - there is no reliable way to decide whether 
                                    an entry that is ment to be interpreted, or ment to be simply output as string    

                                    now I assume that an errmem string always contains minimum 5 valid characters
                                    while an entry to be interpreted contains values below 0x20 (white space) too

                                */

                                if ( (    (entry.au8EntryData[0] >= WHITE_SPACE) 
                                        && (entry.au8EntryData[1] >= WHITE_SPACE)
                                        && (entry.au8EntryData[2] >= WHITE_SPACE)
                                        && (entry.au8EntryData[3] >= WHITE_SPACE) 
                                        && (entry.au8EntryData[4] >= WHITE_SPACE)
                                      )
                                      || (! (LLD_bIsTraceActive(tr_class,TR_LEVEL_FATAL)))
                                    )
                                {
                                if(fd)
                                {
                                     tU32 i, j;
                                     datalen = strlen("DATA:11 1D 00 00 00 ");
                                     strncpy(&cDest[0],"DATA:11 1D 00 00 00 ",datalen);
                                     j= datalen + 15;
                                     for(i=2;i< (tU32)(entry.u16EntryLength+2);i++)
                                     {
                                        sprintf(Destination,"%02x", entry.au8EntryData[i]);  
                                        cDest[j] = Destination[0];
                                        j++;
                                        cDest[j] = Destination[1];
                                        j++;
                                        cDest[j] = ' ';
                                        j++;
                                     }
                                     cDest[j] = 0x0d;
                                     j++;
                                     cDest[j] = 0x0a;
                                     if(OSAL_s32IOWrite(fd,(tPCS8)cDest,j+1) != (tS32)(j+1)) s32Return = OSAL_ERROR;
                                }
                                else
                                {
                                   LLD_vTrace(TR_DEFAULT_STRING_CLASS,TR_LEVEL_FATAL, (unsigned char*)entry.au8EntryData,entry.u16EntryLength);
                                }

                            }
                     }
              }
            }
         }
         OSAL_s32IOClose(d);
     }
     else
     {
         snprintf(cBuffer,150,"Cannot connect to Errmem Daemon");
         s32PrintErrMemString(fd,cBuffer);
     }
     /* if the last err mem entry is a assert then separator line at the end will 
     not be drawn above, so do it here */
     if ((tr_class == (TR_tenTraceClass)TR_CLASS_ASSERT  || tr_class == (TR_tenTraceClass)TR_CLASS_EXCEPTION ) /*lint !e650  PQM_authorized_435 otherwise compiler warning */
                                     && old_tr_class == (TR_tenTraceClass)ER_MEM_ASSERT_EXCEP) /*lint !e650  PQM_authorized_435 otherwise compiler warning */
     {
          LLD_vTrace(ER_MEM_NEW_LINE,TR_LEVEL_FATAL, (unsigned char*)au8Buf_err_mem,len);
     }
     old_Time_stamp.s32Day = 0;
     old_Time_stamp.s32Month = 0;
     old_Time_stamp.s32Year = 0;
     old_Time_stamp.s32Hour = 0;
     old_Time_stamp.s32Minute = 0;
     old_Time_stamp.s32Second = 0;
     old_tr_class = TR_LAST_CLASS; 
     if(fd)
     {
          cDest[0] = 0x0d;
          cDest[1] = 0x0a;
          cDest[2] = ' ';
          cDest[3] = 'p';
          cDest[4] = '-';
          cDest[5] = 0;
         OSAL_s32IOWrite(fd,(tPCS8)cDest,6);
     }
     s32PrintErrMemString(fd,(char*)"OSAL_EM_TRACE done"); /*lint !e1773 */ /* otherwise compiler error */
     return s32Return;
}


tBool bSystemRemount(const char* pMountPath,const char* pcOption)
{
    tBool bRet = FALSE;
    int ret;
    char szCommand[100];

    OSAL_szStringCopy(szCommand,"mount -o remount,");
    OSAL_szStringConcat(szCommand,pcOption);
    OSAL_szStringConcat(szCommand," ");
    OSAL_szStringConcat(szCommand,pMountPath);
    OSAL_szStringConcat(szCommand, " \n");
//    if(pcOption == "ro")
    {
        ret = system("sync");
        TraceString("Pid:%d Sync Status:%x",getpid(),ret);
    }
    ret = system(szCommand);
    TraceString("Pid:%d Mount Status:%x",getpid(),ret);
    if(ret != -1)
    {
        bRet = TRUE;
    }
    return bRet;
}

#ifdef AM_BIN
static error_code_t  request_result = RESULT_OK;
static tBool bCallbackDone = FALSE;
static void remount_done_callback(int request_id, error_code_t result, const char *error_message)
{
    //we passed -1 here because we are not sending more than one request at a time. So we know from which
    //request the answer comes.
    (void)request_id;
    //we are extracting the message from the result code for now
    (void)error_message;
    if(result == RESULT_OK)
    {
         TraceString("remount_done_callback succeeded ");
    }
    else
    {
         TraceString("remount_done_callback failed Error:%d %s",result,error_message );
    }
    request_result=result;
    bCallbackDone = TRUE;
}
#endif

tBool bRemount(char* Path,const char* pcOption)
{
    tBool bRet = FALSE;

    if(Path)
    {
        char* pMountPath = (char*)Path;
        TraceString("Remount %s with %s",pMountPath,pcOption);

        if(!strncmp(Path,"/dev/",strlen("/dev/")))
        {
            pMountPath++;
            pMountPath++;
            pMountPath++;
            pMountPath++;
        }
#ifdef AM_BIN
        if(!strncmp(pMountPath,"/media",strlen("/media")))
        {
            int ReqId= 0;
            if(automounter_api_remount_partition_by_mountpoint(pMountPath,pcOption,ReqId,remount_done_callback) != RESULT_OK)
            {
              TraceString("Remount for %s failed",pMountPath);
            }
            else
            {
                do{
                     OSAL_s32ThreadWait(200);
                }while(bCallbackDone == FALSE);

                /*reset for next request */
                bCallbackDone = FALSE;
                if(request_result == RESULT_OK)
                {
                    bRet = TRUE;
                }
            }
        }
        else
        {
            bRet = bSystemRemount(pMountPath,pcOption);
        }
#else
        bRet = bSystemRemount(pMountPath,pcOption);
#endif
    }
    return bRet;
}


/*****************************************************************************
*
* FUNCTION:     vEraseErrmem
*
* DESCRIPTION: erase Error Memory              
*
* PARAMETER:    none
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vEraseErrmem(tS32 s32Val)
{
     OSAL_tIODescriptor d;
     tS32 s32Size = s32Val;
     d = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
     if ((d != 0) && (d != OSAL_ERROR))
     {
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
         if(OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_ERRMEM_SET_BE,(uintptr_t) &s32Size) != OSAL_OK)
         {
             TraceString(" Set Errmem Backend %d failed",s32Size);
         }
#endif
         if( OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,(uintptr_t)&s32Size) == OSAL_ERROR )
     {
             TraceString("OSAL_EM_ERASE error!");
         }
         OSAL_s32IOClose(d);
     }

     unsigned int TraceBlockMode = 0;
     unsigned int ProxyBlockMode = 0;
     if(get_blockmode_status)
	 {
		 get_blockmode_status(&TraceBlockMode,&ProxyBlockMode);
         vWritePrintfErrmem("After EM_ERASE BLOCKMODE status {TRACE:%d  PROXY:%d }",TraceBlockMode,ProxyBlockMode);
	 }

     (void)OSAL_s32IORemove(OSAL_C_STRING_DEVICE_FFS"/errmemflag.dnl");
     (void)OSAL_s32IORemove("/dev/root/var/opt/bosch/persistent/errmemflag.dnl");

     TraceString("OSAL_EM_ERASE done");
}


void vWriteErrmemToMassstorage(char* Path)
{
  OSAL_tIODescriptor file = 0;
  int i;
  tS32 s32Result = OSAL_OK;
  char cFilepath[100];
  char cFileNr[4];
  trPrmMsg rMsg;
  OSAL_tMQueueHandle hMQ;

  rMsg.s32ResID     = RES_ID_SYSINFO;
  rMsg.u32EvSignal = SIGNAL_SYSINFO;

  if(OSAL_s32MessageQueueOpen(LINUX_PRM_REC_MQ,OSAL_EN_READWRITE,&hMQ)==OSAL_ERROR)//"LI_PRM_REC_MQ"
  {
      NORMAL_M_ASSERT_ALWAYS();          
  }
  else
  {
        rMsg.s32ResID     = RES_ID_SYSINFO;
        rMsg.u32EvSignal = SIGNAL_SYSINFO;
  }


  /* check for running errmem copy */
  if(pOsalData->fderrmem == 0)
  {
     strcpy(cFilepath,pOsalData->szErrMemDumpPath);
     OSAL_szStringConcat(cFilepath,"/Errmem100.pro");
     if(pOsalData->szErrMemDumpPath[0] != 0)
     {
        /* create file for errmem download */
        if(pOsalData->fderrmem = OSAL_IOCreate(cFilepath, OSAL_EN_READWRITE ) == OSAL_ERROR)
        {
            TraceString("Create File %s with %d",cFilepath,OSAL_u32ErrorCode());
        }
     }
     else
     {
        if(!bRemount(Path,"rw"))
        {
             TraceString("Remount for ErrMem Dnl %s failed",Path);
        }
//      rMsg.u32Val = ERROR_MOUNT_FAILED;
//      OSAL_s32MessageQueuePost(hMQ,(tPCU8)&rMsg,sizeof(rMsg),6);

        /* check for older files on media*/
        for(i=1;i<100;i++)
        { 
          cFilepath[0] = '\0';
          OSAL_szStringCopy(cFilepath,Path);
          OSAL_szStringConcat(cFilepath,"/Errmem");
          sprintf(cFileNr,"%d",i);
          OSAL_szStringConcat(cFilepath,&cFileNr[0]);
          OSAL_szStringConcat(cFilepath,".pro");
          if((pOsalData->fderrmem = OSAL_IOOpen(&cFilepath[0], OSAL_EN_READWRITE)) == OSAL_ERROR)
          {
              /* create file for errmem download */
              pOsalData->fderrmem = OSAL_IOCreate(&cFilepath[0], OSAL_EN_READWRITE );
              break;
          }
          else
          {
              OSAL_s32IOClose(pOsalData->fderrmem);
          }
        }/* end for... */
     }
     /* check for valid file handle */
     if(pOsalData->fderrmem != OSAL_ERROR)
     {
          rMsg.u32Val = WRITE_ERRMEM_START;
          OSAL_s32MessageQueuePost(hMQ,(tPCU8)&rMsg,sizeof(rMsg),6);

          if((s32Result = s32TraceErrmem(FALSE,pOsalData->fderrmem)) == OSAL_ERROR)
          {
                TraceString("s32TraceErrmem finished with error");
          }

          /* ensure that data are in sync */
          system("sync \n");

          OSAL_s32ThreadWait(3000);

          if(pOsalData->szErrMemDumpPath[0] == 0)
          {
             if(!bRemount(Path,"r"))
             {
                 TraceString("Remount for ErrMem Dnl %s failed",Path);
             }
          }

          /* Check for erasing errmem */
          if(s32Result == OSAL_OK)
          {
              rMsg.u32Val = WRITE_ERRMEM_SUCCESS;
              OSAL_s32MessageQueuePost(hMQ,(tPCU8)&rMsg,sizeof(rMsg),6);

              cFilepath[0] = '\0';
              OSAL_szStringCopy(cFilepath,Path);
              OSAL_szStringConcat(cFilepath,"/em_erase.ini");
              file = OSAL_IOOpen(cFilepath, OSAL_EN_READONLY);
              if(file != OSAL_ERROR)
              {
                  rMsg.u32Val = ERASE_ERRMEM_START;
                  OSAL_s32MessageQueuePost(hMQ,(tPCU8)&rMsg,sizeof(rMsg),6);
                  vEraseErrmem(0);
             //     vEraseErrmem(1);
             //     vEraseErrmem(2);
                  OSAL_s32IOClose(file);
              }
          }
          else
          {
                rMsg.u32Val = WRITE_ERRMEM_FAILED;
                OSAL_s32MessageQueuePost(hMQ,(tPCU8)&rMsg,sizeof(rMsg),6);
          }
      }
      OSAL_s32IOClose(pOsalData->fderrmem);
      pOsalData->fderrmem = 0;
  }
  OSAL_s32MessageQueueClose(hMQ);
}


void vStartErrmemWriterTsk(void *pvArg)
{
    char* szPath = (char*)pvArg;
    vWriteErrmemToMassstorage(szPath);
}



/*****************************************************************************
*
* FUNCTION:     vDevMediaNotiHandler
*
* DESCRIPTION: This functions is called by PRM to check if errmem download or other 
*                  special activities has to be done
*
* PARAMETER:  tU32* address to media status information
*                 tU8 [] array of maximal PRM_SIZE_OF_UUID_MSG (64) bytes which contains a uuid
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
static char szPath[PRM_SIZE_OF_UUID_MSG + 64];      // static array because address is given as  parameter to an asynchrounus task

void vDevMediaNotiHandler( tU32 *pu32ModeChangeInfo, tU8 au8String[] )
{
    tU16 u16Status = LOWORD(*pu32ModeChangeInfo);
    char PathBuffer[PRM_SIZE_OF_UUID_MSG + 64]; 
    tBool bAllow = TRUE;
    
    if(u16Status != OSAL_C_U16_MEDIA_EJECTED)
    {
        OSAL_tIODescriptor fd = 0;
        /* prepare access*/
        strcpy(&PathBuffer[0],"/dev/media/");     /* prepare access via OSAL device /dev/media where each changeable device is mounted */
        strcat(PathBuffer,(char*)&au8String[0]); /* add the detected path to the medium identified by specific UUID*/
        strcat(PathBuffer,"/errmem.ini");          /* add specific file name in the root of the medium */

        /*check for ErrMemWriter.ini(marker file).If it is available then don't allow to 
          write emtrace to usb.It is gen3 security feature.File ErrMemWriter.ini(marker file)
          is created by ALD for disabling the feature "write errmem2usb"*/
        if((fd = OSAL_IOOpen("/dev/root/var/opt/bosch/dynamic/ErrMemWriter.ini",OSAL_EN_READONLY)) != OSAL_ERROR )
        {
            OSAL_s32IOClose(fd);
            bAllow = FALSE;
            if((fd = OSAL_IOOpen("/dev/root/tmp/ErrMemWriter.ini",OSAL_EN_READONLY)) != OSAL_ERROR )
            {
               bAllow = TRUE;
            }
            TraceString("Errmem Dump not allowed due security reason !!!");
        }
 
        if(bAllow == TRUE)
        {
            if((fd = OSAL_IOOpen(PathBuffer,OSAL_EN_READONLY)) != OSAL_ERROR )
            {
                OSAL_s32IOClose(fd);
                OSAL_trThreadAttribute  attr;
                attr.szName = (tString)"ErrMemWriter";/*lint !e1773 */  /*otherwise linker warning */
                attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
                attr.s32StackSize = minStackSize;
                attr.pfEntry = (OSAL_tpfThreadEntry)vStartErrmemWriterTsk;
                memset(szPath,0,PRM_SIZE_OF_UUID_MSG + 64);
                strcpy(szPath,"/dev/media/");
                strcat(szPath,(char*)&au8String[0]);
                attr.pvArg = szPath;
                if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
                {
                }
            }
        }
         
        /* check utility SO  for other media related purposes */
        if(pMediaFunc == NULL)
        {
           if(u32LoadSharedObject((void*)pMediaFunc,pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames) == OSAL_E_NOTSUPPORTED)/*lint !e611 */  /*otherwise linker warning */
           {
              TraceString("Load error for %s",pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames);
           }
        }
        if(pMediaFunc)pMediaFunc((tCString)au8String);
        
        /* for ADIT automounter version 2 we have to checked trace/DLT oproxy */
        if(pOsalData->u32AmVersion == 2)
        {
            char szCommand[100];
            tBool bReadWrite = FALSE;
            OSAL_trProcessAttribute prAtr    = {OSAL_NULL};
            int ret,s32NewPid;
            strcpy(&PathBuffer[0],"/dev/media/");
            strcat(PathBuffer,(char*)&au8String[0]);

            mkdir("/tmp/trace", OSAL_FILE_ACCESS_RIGTHS );

            strcat(PathBuffer,"/proxy_cnfg.cfg");
            if((fd = OSAL_IOOpen(PathBuffer,OSAL_EN_READONLY)) != OSAL_ERROR )
            {
              OSAL_s32IOClose(fd);
              PathBuffer[strlen(PathBuffer)-15] = 0;
              // create symbolic link for trace always */
              OSAL_szStringCopy(szCommand,"ln -s ");
              OSAL_szStringConcat(szCommand,PathBuffer);
              OSAL_szStringConcat(szCommand, " /tmp/trace/proxy \n");
              ret = system(szCommand);  /* should look like    ln -s  /dev/media/xxxx /tmp/trace/proxy  */
              TraceString("system(create link for trace) returns %d",ret);
              // STEP 1 make stick RW
              if(!bRemount(PathBuffer,"rw"))
              {
                  TraceString("Remount for Trace Proxy Dnl %s failed",PathBuffer);
 //                 vWritePrintfErrmem("Remount for Trace Proxy Dnl %s failed \n",PathBuffer);
              }
              bReadWrite = TRUE;


              // STEP 2 start trace binary */
              prAtr.szCommandLine  = (char*)"--attach";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.szName            = (char*)"trace_ctrl";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.szAppName        = (char*)"/bin/trace_ctrl";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.u32Priority     = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
              if( OSAL_ERROR == ( s32NewPid = OSAL_ProcessSpawn(&prAtr)))
              {
                  TraceString("OSAL_ProcessSpawn trace_ctrl failed errno %d",errno);
              }
              else
              {
                  TraceString("OSAL_ProcessSpawn trace_ctrl PID %d",s32NewPid);
              }
//              ret = system("/bin/trace_ctrl --attach ");
//              TraceString("system(/bin/trace_ctrl --attach)  returns %d",ret);

            }
            strcpy(&PathBuffer[0],"/dev/media/");
            strcat(PathBuffer,(char*)&au8String[0]);
            strcat(PathBuffer,"/dlt_logstorage.conf");
            if((fd = OSAL_IOOpen(PathBuffer,OSAL_EN_READONLY)) != OSAL_ERROR )
            {
              OSAL_s32IOClose(fd);
              // STEP 1 make stick RW
              PathBuffer[strlen(PathBuffer)-20] = 0;
              if(bReadWrite == FALSE)
              {
                  if(!bRemount(PathBuffer,"rw"))
                  {
                      TraceString("Remount for DLT Proxy Dnl %s failed",PathBuffer);
//                      vWritePrintfErrmem("Remount for DLT Proxy Dnl %s failed",PathBuffer);
                  }
              }
              // STEP 2 create symbolic link */
              OSAL_szStringCopy(szCommand,"ln -s ");
              OSAL_szStringConcat(szCommand,PathBuffer);
              OSAL_szStringConcat(szCommand, " /tmp/dltlogsdev1 \n ");
              ret = system(szCommand);    /* should look like ln -s  /dev/media/xxxx /tmp/dltlogsdev1 */
              TraceString("system(create link for DLT) returns %d",ret);
              // STEP 3 start dlt binary */
              prAtr.szCommandLine  = (char*)"-dam";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.szName            = (char*)"dlt-logstorage-ctrl";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.szAppName        = (char*)"/usr/bin/dlt-logstorage-ctrl";/*lint !e1773 PQM_authorized_multi_525*/
              prAtr.u32Priority     = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
              if( OSAL_ERROR == ( s32NewPid = OSAL_ProcessSpawn(&prAtr)))
              {
                  TraceString("OSAL_ProcessSpawn dlt-logstorage-ctrlapp failed errno %d",errno);
              }
              else
              {
                  TraceString("OSAL_ProcessSpawn dlt-logstorage-ctrlapp PID %d",s32NewPid);
              }
//              ret = system("dlt-logstorage-ctrlapp 1 1 &\n");
//              TraceString("system(dlt-logstorage-ctrlapp 1 1) returns %d",ret);
          }  
        }
    }
}

void RegisterForMedia(void)
{
    OSAL_tIODescriptor fd;
    OSAL_trNotifyDataExt2 rNotify2;

    // register callback function
    fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY);
    if( fd != OSAL_ERROR )
    {
        rNotify2.u16AppID = OSAL_C_U16_OTHERAPP_APPID;
        rNotify2.ResourceName = "/dev/media";
        rNotify2.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
        rNotify2.pCallbackExt2 = vDevMediaNotiHandler;

        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (uintptr_t)&rNotify2) == OSAL_ERROR)
        {
             NORMAL_M_ASSERT_ALWAYS();
        }
        OSAL_s32IOClose(fd);
    }
}

/*****************************************************************************
*
* FUNCTION:     vOsalTraceOutRegistry
*
* DESCRIPTION: This functions traces out a registry file and enters the keys 
*                  into the OSAL registry device
*
* PARAMETER:    const tChar* szBaseName Name
*
* RETURNVALUE: tBool
*                      it is the function return value: 
*                      - TRUE if everything goes right;
*                      - FALSE otherwise.
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vOsalTraceOutRegistry(const char* szBaseName)
{
    static tU32 u32Count_recursive_call = 0;
    tU32 u32Len1,u32Len2;
    OSAL_trIOCtrlDir        rDir;
    OSAL_trIOCtrlDir        rDirV;
    OSAL_trIOCtrlRegistry rReg;
    OSAL_tIODescriptor fd;
    unsigned char* pu8Buffer = (unsigned char*)OSAL_pvMemoryAllocate(DYN_MEM_ALLOC_SIZE);
    tS32* ps32Data = (tS32*)OSAL_pvMemoryAllocate(DYN_MEM_ALLOC_SIZE);
    char* ps8Name  = (char*)OSAL_pvMemoryAllocate(DYN_MEM_ALLOC_SIZE);
    
    if(pu8Buffer && ps32Data && ps8Name)
    {
    /*Tracing out when the function is recursively called for the 10th time*/
    u32Count_recursive_call++;
    if(!(u32Count_recursive_call % 10))
        TraceString("vOsalTraceOutResistry is recursively called for 10th time");
 
    fd = OSAL_IOOpen(szBaseName, OSAL_EN_READONLY);
    if (fd != OSAL_ERROR)
    {

        /* First, recursively show all the subkeys */
        rDir.fd = fd;
        rDir.s32Cookie = 0;

        while (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGREADDIR, (uintptr_t)&rDir) != OSAL_ERROR)
        {
            memset((void*)&pu8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
            (void)OSAL_szStringCopy(ps8Name, szBaseName);
            (void)OSAL_szStringConcat(ps8Name, "/");
            (void)OSAL_szStringConcat(ps8Name, (tString)rDir.dirent.s8Name);
            OSAL_s32ThreadWait(10);
            pu8Buffer[0] = 0x80;
            u32Len1 = strlen((char*)rDir.dirent.s8Name);
            if(u32Len1 >= (DYN_MEM_ALLOC_SIZE-1))u32Len1 = DYN_MEM_ALLOC_SIZE - 2;
            memcpy(&pu8Buffer[1],rDir.dirent.s8Name,u32Len1);
            pu8Buffer[u32Len1+1] = '\0';
          
            LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,pu8Buffer,1            /*for 0x70*/
                                                                                + sizeof(tU32)
                                                                                + u32Len1 /*len of string*/
                                                                                + 1);
          vOsalTraceOutRegistry( (tString)ps8Name);
        }

        TraceString(szBaseName);

        /* Now show all the values of this key */
        rDirV.fd = fd;
        rDirV.s32Cookie = 0;

        while (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGENUMVALUE, (uintptr_t)&rDirV) != OSAL_ERROR)
        {
            rReg.pcos8Name = rDirV.dirent.s8Name;
            rReg.ps8Value  = (tU8*)ps32Data;
            rReg.u32Size    = DYN_MEM_ALLOC_SIZE;

            if (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (uintptr_t)&rReg) != OSAL_ERROR)
            {
                if (rReg.s32Type == OSAL_C_S32_VALUE_S32)
                {
                    tU32 u32Temp = *(/*lint -e(826) pointer conversion is ok */(tU32*)(rReg.ps8Value));
                    memset((void*)&pu8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
                    pu8Buffer[0] = 0x70;
                    OSAL_M_INSERT_T32(&pu8Buffer[1],u32Temp);
                    u32Len1 = strlen((const char*)rReg.pcos8Name);
                    if(u32Len1 >= (DYN_MEM_ALLOC_SIZE-5))u32Len1 = DYN_MEM_ALLOC_SIZE -6;
                    memcpy(&pu8Buffer[5],rReg.pcos8Name,u32Len1);
                    pu8Buffer[u32Len1+5] = '\0';
                    LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,pu8Buffer,1            /*for 0x70*/
                                                                            + sizeof(tU32)
                                                                            + u32Len1 /*len of string*/
                                                                            + 1     /*for '\0'*/);
                }
                else if (rReg.s32Type == OSAL_C_S32_VALUE_STRING)
                {
                    memset((void*)&pu8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
                    pu8Buffer[0] = 0x71;
                    u32Len1 = strlen((const char*)rReg.pcos8Name);
                    memcpy(&pu8Buffer[1],rReg.pcos8Name,u32Len1);
                    pu8Buffer[1+u32Len1]    = ' ';
                    pu8Buffer[1+u32Len1+1] = ':';
                    pu8Buffer[1+u32Len1+2] = ' ';
                    u32Len2 = strlen(( char*)rReg.ps8Value);
                    if(u32Len2 >= (DYN_MEM_ALLOC_SIZE - (1+u32Len1+4)))
                    {
                       TraceString("Next output is truncated" );
                       u32Len2 = DYN_MEM_ALLOC_SIZE - (1+u32Len1+4);
                    }
                    memcpy(&pu8Buffer[1+u32Len1+3],(char*)rReg.ps8Value,u32Len2);
                    pu8Buffer[1+u32Len1+3+u32Len2] = '\0';
                    LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,pu8Buffer,1+u32Len1+3+u32Len2+1);
                }
                else
                {
                    memset((void*)&pu8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
                    pu8Buffer[0] = 0x73;
                    u32Len1 = strlen((const char*)rReg.pcos8Name);
                    memcpy(&pu8Buffer[1],rReg.pcos8Name,u32Len1);
                    if(u32Len1 >= (DYN_MEM_ALLOC_SIZE -1))u32Len1 = DYN_MEM_ALLOC_SIZE - 2;
                    pu8Buffer[u32Len1+1] = '\0';
                    LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,pu8Buffer,1+u32Len1+1);
                }
            }
        }
 
        if (OSAL_s32IOClose(fd) == OSAL_ERROR)
        {
             memset((void*)&pu8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
             pu8Buffer[0] = 0x82;
             u32Len1 = strlen((char*)rDir.dirent.s8Name);
             memcpy(&pu8Buffer[1],rDir.dirent.s8Name,u32Len1);
             pu8Buffer[u32Len1+1] = '\0';
             LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,pu8Buffer,1+u32Len1+1);
        }
    }
    /*Decreasing the count (for tracing out purposes) */
    u32Count_recursive_call--;
    }
    /*Freeing the memory during every return of the function*/
    if(ps8Name)OSAL_vMemoryFree(ps8Name);
    if(ps32Data)OSAL_vMemoryFree(ps32Data);
    if(pu8Buffer)OSAL_vMemoryFree(pu8Buffer);
}

void vOsalTraceOutRegistryDir(const char* szBaseName)
{
    tU32 u32Len1;
    OSAL_trIOCtrlDir    rDir;
    OSAL_tIODescriptor fd;
    unsigned char u8Buffer[DYN_MEM_ALLOC_SIZE];
    char    s8Name[DYN_MEM_ALLOC_SIZE];
    
    fd = OSAL_IOOpen(szBaseName, OSAL_EN_READONLY);
    if (fd != OSAL_ERROR)
    {
            /* First, recursively show all the subkeys */
            rDir.fd = fd;
            rDir.s32Cookie = 0;
 
            while (OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGREADDIR, (uintptr_t)&rDir) != OSAL_ERROR)
            {
                 memset((void*)&u8Buffer[0],0,DYN_MEM_ALLOC_SIZE);
                 (void)OSAL_szStringCopy(s8Name, szBaseName);
                 (void)OSAL_szStringConcat(s8Name, "/");
                 (void)OSAL_szStringConcat(s8Name, (tString)rDir.dirent.s8Name);
                 u8Buffer[0] = 0x80;
                 u32Len1 = strlen((char*)rDir.dirent.s8Name);
                 if(u32Len1 >= (DYN_MEM_ALLOC_SIZE-1))u32Len1 = DYN_MEM_ALLOC_SIZE - 2;
                 memcpy(&u8Buffer[1],rDir.dirent.s8Name,u32Len1);
                 u8Buffer[u32Len1+1] = '\0';
                 LLD_vTrace(TR_COMP_OSALIO,TR_LEVEL_FATAL,u8Buffer,1            /*for 0x70*/
                                + sizeof(tU32)
                                + u32Len1 /*len of string*/
                                + 1);
            }
            OSAL_s32IOClose(fd);
    }
}


/*****************************************************************************
*
* FUNCTION:     vOsalSystemLoadTask(OSAL_trSystemLoadTaskInfo *pTaskInfo ){
*
* DESCRIPTION: Generates system load with the given parameters (n_system_load, n_sleep_duration)
*
* PARAMETER:    OSAL_trSystemLoadTaskInfo *pTaskInfo
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 tVoid vOsalSystemLoadTask(OSAL_trSystemLoadTaskInfo *pTaskInfo ){
      
      OSAL_tMSecond last_timestamp = 0;
      OSAL_tMSecond new_timestamp = 0;
      
      OSAL_tMSecond desired_awake_time = 0;
      OSAL_tMSecond curr_awake_time     = 0;

      float fl_dummy = (float)0;

      fl_dummy = ((float)pTaskInfo->n_sleep_duration * (float)pTaskInfo->n_system_load);
      fl_dummy = fl_dummy / (float) (100-pTaskInfo->n_system_load);
      
      desired_awake_time = (OSAL_tMSecond) fl_dummy;

      if((fl_dummy - desired_awake_time) >= 0.5){
            desired_awake_time++;
      }

      last_timestamp = OSAL_ClockGetElapsedTime();
      while(pTaskInfo->b_terminate_task == FALSE){
            /*Sleep duration */
            OSAL_s32ThreadWait((tU32)pTaskInfo->n_sleep_duration);
            new_timestamp = OSAL_ClockGetElapsedTime();
            if(new_timestamp >= last_timestamp){
                 pTaskInfo->ui_sleep_time += (new_timestamp - last_timestamp);
            }else{
                 pTaskInfo->ui_sleep_time += (last_timestamp - new_timestamp);
            }
            last_timestamp = new_timestamp;
            curr_awake_time = 0;
            
            while((curr_awake_time <  desired_awake_time) &&
                    (pTaskInfo->b_terminate_task == FALSE)){
                 
                 new_timestamp = OSAL_ClockGetElapsedTime();
                 if(new_timestamp >= last_timestamp){
                      curr_awake_time = (new_timestamp - last_timestamp);
                 }else{
                      curr_awake_time = (last_timestamp - new_timestamp);
                 }
            }

            if(new_timestamp >= last_timestamp){
                 pTaskInfo->ui_awake_time += (new_timestamp - last_timestamp);
            }else{
                 pTaskInfo->ui_awake_time += (last_timestamp - new_timestamp);
            }
            last_timestamp = new_timestamp;
      }
      return;
 }

/*****************************************************************************
*
* FUNCTION:     vOsalInitLoadTaskInfoEntry(int n_idx){
*
* DESCRIPTION: Initializes the desired entry in tr_sys_load_task_table[]
*
* PARAMETER:    int n_idx - idx in table
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 void vOsalInitLoadTaskInfoEntry(int n_idx){

      tChar szTaskNum[4] = {'\0'};
      if((n_idx >= 0) &&
          ((tU16)n_idx < OSAL_MAX_NUM_SYS_LOAD_TASKS)){
            
            tr_sys_load_task_table[n_idx].b_entry_used         = FALSE;

            tr_sys_load_task_table[n_idx].n_priority            = 0;
            tr_sys_load_task_table[n_idx].n_system_load        = 0;
            tr_sys_load_task_table[n_idx].n_sleep_duration    = 0;
            tr_sys_load_task_table[n_idx].n_thread_id          = 0;
            tr_sys_load_task_table[n_idx].ui_awake_time        = 0;
            tr_sys_load_task_table[n_idx].ui_sleep_time        = 0;
            tr_sys_load_task_table[n_idx].b_terminate_task    = TRUE;
            
            sprintf(szTaskNum,"%02d",n_idx);
            strcpy(&tr_sys_load_task_table[n_idx].szTaskName[0],
                     OSAL_LOAD_TASK_BASE_NAME);
            strcat(&tr_sys_load_task_table[n_idx].szTaskName[0],
                     szTaskNum);
            
      }else{ /*if((n_task_id >= 0) &&*/
            TraceString("vOsalInitLoadTaskInfoEntry: Argument 'n_idx=%d' is out of Range (0-%d) ", 
                      n_idx, 
                      (OSAL_MAX_NUM_SYS_LOAD_TASKS-1));
      }/*if((n_task_id >= 0) &&*/
      return;
 }
 

/*****************************************************************************
*
* FUNCTION:      void vOsalInitLoadTaskInfo(void){
*
* DESCRIPTION: Initializes the  tr_sys_load_task_table[] table
*
* PARAMETER:    none
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 void vOsalInitLoadTaskInfo(void){
      tU16 i=0;

      for (i=0; i< OSAL_MAX_NUM_SYS_LOAD_TASKS; i++){
            vOsalInitLoadTaskInfoEntry(i);
      }
      return;
}



/*****************************************************************************
*
* FUNCTION:        void vOsalDisplayLoadTaskStatus(void){
*
* DESCRIPTION: Displays the status of all system load tasks
*
* PARAMETER:    none
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 void vOsalDisplayLoadTaskStatus(void){

      unsigned int n_real_load = 0;
      tU16 i = 0;
      float fl_load_stat = 0;
      TraceString("  Load task status function");


      /* Init the task info struct if not done */
      if(OSAL_b_isLoadTaskInfoInit == FALSE){
            vOsalInitLoadTaskInfo();
            OSAL_b_isLoadTaskInfoInit = TRUE;
      }      
      
      for(i=0; i< OSAL_MAX_NUM_SYS_LOAD_TASKS;i++){

            if(tr_sys_load_task_table[i].b_entry_used == TRUE){
                 if((tr_sys_load_task_table[i].ui_awake_time + tr_sys_load_task_table[i].ui_sleep_time) != 0){
                      n_real_load = (tr_sys_load_task_table[i].ui_awake_time) * 100;
                      n_real_load = n_real_load / (tr_sys_load_task_table[i].ui_awake_time + tr_sys_load_task_table[i].ui_sleep_time);

                      fl_load_stat = ((float)tr_sys_load_task_table[i].ui_awake_time) * 100;
                      fl_load_stat = fl_load_stat / (tr_sys_load_task_table[i].ui_awake_time + tr_sys_load_task_table[i].ui_sleep_time);
                 }else{
                      n_real_load = 0;
                      fl_load_stat = 0;
                 }
                 
                 TraceString("LoadTaskID: %02d - Prio: %02d - target load: %d %% - real load: %3.2f %%", 
                            i,
                            tr_sys_load_task_table[i].n_priority,
                            tr_sys_load_task_table[i].n_system_load,
                            fl_load_stat);
            }/*if(tr_sys_load_task_table[i].b_entry_used = TRUE){*/
      } /*for(i=0; i< OSAL_MAX_NUM_SYS_LOAD_TASKS;i++){*/
      

      return;
      
 }
 


/*****************************************************************************
*
* FUNCTION:         void vOsalKillLoadTask(int n_task_id){
*
* DESCRIPTION: Kills the specified load task
*
* PARAMETER:    int n_task_id - idx of load task
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 void vOsalKillLoadTask(int n_task_id){
      OSAL_trThreadControlBlock tr_thread_ctrl;
      tInt n_wait_cnt = 0;
      tS32 s32_osal_ret = 0;
      
      TraceString("  Thread %d will be killed", n_task_id);
      /* Init the task info struct if not done */
      if(OSAL_b_isLoadTaskInfoInit == FALSE){
            vOsalInitLoadTaskInfo();
            OSAL_b_isLoadTaskInfoInit = TRUE;
      }

      if((n_task_id >= 0) &&
          ((tU16)n_task_id < OSAL_MAX_NUM_SYS_LOAD_TASKS)){
            
            if(tr_sys_load_task_table[n_task_id].b_entry_used == TRUE){

                 tr_sys_load_task_table[n_task_id].b_terminate_task = TRUE;
                 /* Now wait until thread is really terminated */

                 n_wait_cnt = 0;
                 s32_osal_ret = OSAL_s32ThreadControlBlock( tr_sys_load_task_table[n_task_id].n_thread_id,
                                                                          &tr_thread_ctrl);
                 while((s32_osal_ret != OSAL_ERROR) &&
                         (n_wait_cnt < 30)){
                      
                      OSAL_s32ThreadWait(500);
                      n_wait_cnt++;      
                      s32_osal_ret = OSAL_s32ThreadControlBlock( tr_sys_load_task_table[n_task_id].n_thread_id,
                                                                                &tr_thread_ctrl);
                 }     
                 
                 if(s32_osal_ret != OSAL_ERROR){
                      TraceString("vOsalKillLoadTask: Task %d did not terminate within 15 s ", 
                                 n_task_id);
                 }
                 
                 OSAL_s32ThreadDelete(tr_sys_load_task_table[n_task_id].n_thread_id);
                 
                 vOsalInitLoadTaskInfoEntry(n_task_id );

            }else{ /*if(tr_sys_load_task_table[n_task_id].b_entry_used == TRUE){*/
                 TraceString("vOsalKillLoadTask: Task %d was not spawned ", 
                            n_task_id);
            }/*if(tr_sys_load_task_table[n_task_id].b_entry_used == TRUE){*/
            

      }else{ /*if((n_task_id >= 0) &&*/
            TraceString("vOsalKillLoadTask: Argument 'n_task_id=%d' is out of Range (0-%d) ", 
                      n_task_id, 
                      (OSAL_MAX_NUM_SYS_LOAD_TASKS-1));
      }/*if((n_task_id >= 0) && */
      
      
      return;
 }
 


/*****************************************************************************
*
* FUNCTION:      void vOsalSpawnLoadTask(int n_priority, int n_system_load, int n_sleep_periode){
*
* DESCRIPTION: Spawns a load task with the given parameters
*
* PARAMETER:    int n_priority         -  Priority
*                  int n_system_load     -  desired system load
*                    int n_sleep_periode -  sleep periode
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.07.06  | Initial revision                              | Resch, Carsten
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
 void vOsalSpawnLoadTask(int n_priority, int n_system_load, int n_sleep_periode){
      tBool b_entry_found = FALSE;
      tU16 i = 0;

      OSAL_trThreadAttribute tr_thread_attr; 
      
      TraceString(" Spawn Thread with prio: %d and sys load %d %%", n_priority, n_system_load);
      
      /* Init the task info struct if not done */
      if(OSAL_b_isLoadTaskInfoInit == FALSE){
            vOsalInitLoadTaskInfo();
            OSAL_b_isLoadTaskInfoInit = TRUE;
      }

      /* Try to find a new entry */
      while((b_entry_found == FALSE) &&
              (i < OSAL_MAX_NUM_SYS_LOAD_TASKS)){
            if(tr_sys_load_task_table[i].b_entry_used  == FALSE){
                 b_entry_found = TRUE;
                 tr_sys_load_task_table[i].b_entry_used = TRUE;
                 tr_sys_load_task_table[i].b_terminate_task    = FALSE;
                 tr_sys_load_task_table[i].n_sleep_duration = n_sleep_periode;
                 tr_sys_load_task_table[i].n_system_load = n_system_load;
                 tr_sys_load_task_table[i].n_priority = n_priority;

                 tr_thread_attr.u32Priority  = (tU32)tr_sys_load_task_table[i].n_priority;
                 tr_thread_attr.s32StackSize = 2048;
                 tr_thread_attr.pfEntry        = (OSAL_tpfThreadEntry) vOsalSystemLoadTask;
                 tr_thread_attr.pvArg          = (void*)(&tr_sys_load_task_table[i]);
                 tr_thread_attr.szName         = tr_sys_load_task_table[i].szTaskName;

                 tr_sys_load_task_table[i].n_thread_id = OSAL_ThreadSpawn(&tr_thread_attr);
                 if(tr_sys_load_task_table[i].n_thread_id == OSAL_ERROR){
                      TraceString("Failed to spawn worker task: err=%d", tr_sys_load_task_table[i].n_thread_id);
                      
                      vOsalInitLoadTaskInfoEntry(i );
                      
                 } /*if(tr_sys_load_task_table[i].n_thread_id == OSAL_ERROR){*/
            }else{ /*if(tr_sys_load_task_table[i].b_entry_used  == FALSE){*/
                 i++;
            }/*if(tr_sys_load_task_table[i].b_entry_used  == FALSE){*/
      } /*while((b_entry_found == FALSE) &&*/
                 
      if(b_entry_found == FALSE)
      {
            TraceString("Max number of load tasks (%d) already spawned", OSAL_MAX_NUM_SYS_LOAD_TASKS);
      }/*if(b_entry_found == TRUE){*/

      return;
 }

void vGetTopInfo(tBool bErrMemEntry)
{
    system("top -n 1 -b > /var/opt/bosch/dynamic/osaltop.log");
    if(bErrMemEntry)
    {
        vReadFile("/var/opt/bosch/dynamic/osaltop.log",TRUE);/*lint !e1773 */  /*otherwise linker warning */
    }
    else
    {
        vReadFile("/var/opt/bosch/dynamic/osaltop.log",FALSE);/*lint !e1773 */  /*otherwise linker warning */
    }
    system("rm /var/opt/bosch/dynamic/osaltop.log");
}

char* pcFindPattern(char* szName, char* cBuffer,tU32 u32Length)
{
  char* pTemp = cBuffer;
  char* pRet = NULL;

      while(1)
      {
          /* search name */
          if(*pTemp == 0x03 /*ETX*/)break;
          if(*pTemp == *szName)
          {
              if(!strncmp(szName,pTemp,u32Length))/*lint !e530 */
              {
                  pRet = pTemp;
              }
              break;
         }
         pTemp++;
      }// end while
  return pRet;
}

static int  PageSize;
void vTaskInfoForProc(char* pName,char* szPrcPath,tBool bErrMemEntry)
{
    struct dirent*    pDirEntt=NULL;
    DIR* dirtask =0;
    char szEntry[100];
    ((void)pName);

    dirtask = opendir(szPrcPath);
    if(dirtask)
    {
      pDirEntt = readdir((DIR*)dirtask);
      while(pDirEntt)
      {
         if(pDirEntt->d_name[0] != '.')
         {
             OSAL_szStringCopy(szEntry,szPrcPath);
             OSAL_szStringConcat(szEntry,"/");
             OSAL_szStringConcat(szEntry,pDirEntt->d_name);
                          
             u32ScanPidTaskTidStat(szEntry,bErrMemEntry,-1);
         }
         pDirEntt = readdir(dirtask);
      }//while(pDirEntt)
      rewinddir(dirtask);
      closedir(dirtask);
    }//if(dirtask)
}


void vMemInfoForProc(char* pName,tBool bErrMemEntry)
{
    char szPrcPath[50];
    FILE* fdstr = NULL;
    long int VmSize = 0;
    long int VmRSS  = 0;
    long int Share  = 0;
    long int Text    = 0;
    long int Lib     = 0;
    long int data    = 0;

    snprintf(szPrcPath,50,"/proc/%s",pName);
    chdir(szPrcPath);
    fdstr = fopen("statm", "r");
    if(fdstr)
    {
        fscanf(fdstr,"%lu ",&VmSize);      // 1. TID
        fscanf(fdstr,"%lu ",&VmRSS);      // 1. TID
        fscanf(fdstr,"%lu ",&Share);      // 1. TID
        fscanf(fdstr,"%lu ",&Text);      // 1. TID
        fscanf(fdstr,"%lu ",&Lib);      // 1. TID
        fscanf(fdstr,"%lu ",&data);      // 1. TID
        fclose(fdstr);
    
/*                     size         (1) total program size
                                      (same as VmSize in /proc/[pid]/status)
                        resident    (2) resident set size
                                      (same as VmRSS in /proc/[pid]/status)
                        share        (3) shared pages (i.e., backed by a file)
                        text         (4) text (code)
                        lib          (5) library (unused in Linux 2.6)
                        data         (6) data + stack*/
        if(bErrMemEntry == FALSE)
        {
             TraceString("PID:%s VmSize:%d kB VmRSS:%d kB SharedMem:%d kB Code:%d kB Lib:%d kB Stack&Data:%d kB",
                             pName,VmSize*PageSize/1024,VmRSS*PageSize/1024,Share*PageSize/1024, 
                             Text*PageSize/1024,Lib*PageSize/1024,data*PageSize/1024);
        }
        else
        {
             vWritePrintfErrmem("PID:%s VmSize:%d kB VmRSS:%d kB SharedMem:%d kB Code:%d kB Lib:%d kB Stack&Data:%d kB \n",
                                      pName,VmSize*PageSize/1024,VmRSS*PageSize/1024,Share*PageSize/1024, 
                                      Text*PageSize/1024,Lib*PageSize/1024,data*PageSize/1024);
        }
    }
}

void vProcTaskInfo(char* pcPid, tBool bErrMemEntry)
{
    struct dirent*    pDirEntp=NULL;
    DIR* dirproc =0;
    char szPrcPath[50];
    tS32 s32Pid = *((tS32*)(pcPid));/*lint !e826 */
    PageSize = getpagesize();

    dirproc = opendir("/proc");
    if(dirproc)
    {
        if(s32Pid == 0)
        {
            pDirEntp = readdir((DIR*)dirproc);
            while(pDirEntp)
            {
                if((*pDirEntp->d_name>= 0x30 /* 0 */)&&(*pDirEntp->d_name < 0x39 /* 9 */ ))
                {
                    vMemInfoForProc(pDirEntp->d_name, bErrMemEntry);
        
                    OSAL_szStringCopy(szPrcPath,"/proc/");
                    OSAL_szStringConcat(szPrcPath,pDirEntp->d_name);
                    OSAL_szStringConcat(szPrcPath,"/task");
                    vTaskInfoForProc(pDirEntp->d_name,szPrcPath,bErrMemEntry);
                }
             pDirEntp = readdir(dirproc);
         }//while(pDirEntp)
        }
        else
        {
            char cName[64];
            snprintf(cName,64,"%d",s32Pid);
 
            vMemInfoForProc(cName, bErrMemEntry);

            snprintf(szPrcPath,50,"/proc/%d/task",s32Pid);
            vTaskInfoForProc(cName,szPrcPath,bErrMemEntry);
        }
         closedir(dirproc);
    }//if(dirproc)
}


void vTriggerPrc(void)
{
    struct dirent*    pDirEntp=NULL;
    DIR* dirproc =0;
    char szPrcPath[50];
    char ProcessPath[128];
    char ProcessName[64];
    FILE* fdstr = NULL;
    int Pid = 0;

    dirproc = opendir("/proc");
    if(dirproc)
    {
        pDirEntp = readdir((DIR*)dirproc);
        while(pDirEntp)
        {
            if((*pDirEntp->d_name>= 0x30 /* 0 */)&&(*pDirEntp->d_name < 0x39 /* 9 */ ))
            {
                OSAL_szStringCopy(szPrcPath,"/proc/");
                OSAL_szStringConcat(szPrcPath,pDirEntp->d_name);
                OSAL_szStringConcat(szPrcPath,"/cmdline");
                fdstr = fopen(szPrcPath, "r");
                if(fdstr != NULL)
                {
                    memset(ProcessPath,0,128);
                    fscanf(fdstr,"%s",&ProcessPath[0]);
                    fclose(fdstr);
                    if(!strncmp(ProcessPath,"/opt/bosch",10))
                    {
                        OSAL_szStringCopy(szPrcPath,"/proc/");
                        OSAL_szStringConcat(szPrcPath,pDirEntp->d_name);
                        OSAL_szStringConcat(szPrcPath,"/comm");
                        fdstr = fopen(szPrcPath, "r");
                        if(fdstr != NULL)
                        {
                            fscanf(fdstr,"%s",&ProcessName[0]);
                            fclose(fdstr);
                            Pid = atoi(pDirEntp->d_name);
                            TraceString(ProcessName);
                            kill(Pid,-19);
                            kill(Pid,-18);
                        }
                    }
                }
            }
            pDirEntp = readdir(dirproc);
        }//while(pDirEntp)
    }
    closedir(dirproc);
}


tS32 s32GetPidFromName(char* szName, tU32 u32Len)
{
    struct dirent*    pDirEnt=NULL;
    DIR* dir =0;
    char szCommand[200];
    tU32 size;
    char *pBuffer,*pTemp;
    tS32 fd,s32Ret = -1;
    tU32 u32Length = u32Len;
    if(u32Length > 14)
    {
         u32Length = 14;
    }

    dir = opendir("/proc");
    if(dir)
    {
         pDirEnt = readdir((DIR*)dir);
         while(pDirEnt)
         {
             if((*pDirEnt->d_name>= 0x30 /* 0 */)&&(*pDirEnt->d_name < 0x39 /* 9 */ ))
             {
                 OSAL_szStringCopy(szCommand,"cat /proc/");
                 OSAL_szStringConcat(szCommand,pDirEnt->d_name);
                 OSAL_szStringConcat(szCommand, "/status > /var/opt/bosch/dynamic/PrcInfo.txt");
                 system(szCommand);

                 if((fd = open("/var/opt/bosch/dynamic/PrcInfo.txt", O_RDONLY,0)) != -1)
                 {
                     size = s32GetFileSize(fd);
                     if((pBuffer = (char*)OSAL_pvMemoryAllocate((tU32)size)) != 0)
                     {
                         if(lseek(fd,0,SEEK_SET) != -1)
                         {
                             if(read( fd,pBuffer,(tU32)size) == size)
                             {
                                 pTemp = pBuffer;
/*
Name:    hald-addon-inpu
State:  S (sleeping)
Tgid:    812
Pid:     812
PPid:    737
*/
                                 pTemp = pTemp+5;
                                 pTemp = pcFindPattern(szName,pTemp,u32Length);
                                 if(pTemp)
                                 {
                                     pTemp = pcFindPattern((char*)"Pid:",pTemp,4);/*lint !e1773 */  /*otherwise linker warning */
                                     if(pTemp)
                                     {
                                         /* found line, look for number */
                                         while(1)
                                         {
                                             pTemp++;
                                             if(*pTemp == 0x03 /*ETX*/)break;
                                             if((*pTemp>= 0x30 /* 0 */)&&(*pTemp <= 0x39 /* 9 */ ))
                                             {
                                                 s32Ret = atoi(pTemp);
                                                 break;
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                         OSAL_vMemoryFree(pBuffer);
                     }
                     close(fd);
                 }
                 system("rm /var/opt/bosch/dynamic/PrcInfo.txt");
             }//if((*pDirEnt->d_name
             pDirEnt = readdir(dir);
         }// end while
         closedir(dir);
    }
    return s32Ret;
}

void vDumpMemStatusForProcess(char* pcPid, tBool bErrmem)
{
    char szCommand[200];
    char szPrcName[20] = {'\0'};
    tU32 size;
    char *pBuffer,*pTemp,*pTemp2;
    tS32 fd;
    tS32 s32Pid = *((tS32*)(pcPid));/*lint !e826 */
    tS32 s32Count = 0;

 //  itoa(s32Pid, szPrcName, 10);
    snprintf(szPrcName,20,"%d",s32Pid);

    TraceString("%d",s32Pid);

    OSAL_szStringCopy(szCommand,"cat /proc/");
    OSAL_szStringConcat(szCommand,szPrcName);
    OSAL_szStringConcat(szCommand, "/status > /var/opt/bosch/dynamic/PrcInfo.txt");
    (void)system(szCommand);

    if((fd = open("/var/opt/bosch/dynamic/PrcInfo.txt", O_RDONLY,0)) != -1)
    {
        size = s32GetFileSize(fd);
        if((pBuffer = (char*)OSAL_pvMemoryAllocate((tU32)size)) != 0)
        {
            if(lseek(fd,0,SEEK_SET) != -1)
            {
                if(read( fd,pBuffer,(tU32)size) == size)
                {
                    pTemp = pBuffer;
                    while(1)
                    {
                         pTemp++;
                         if(*pTemp == 0x03 /*ETX*/)break;
                         if((*pTemp == 'V')&&(*(pTemp+1) == 'm'))
                         {
                 /* VmPeak: Peak virtual memory size.
                  * VmSize: Virtual memory size.
                  * VmLck: Locked memory size (see mlock(3)).
                  * VmHWM: Peak resident set size ("high water mark").
                  * VmRSS: Resident set size.
                  * VmData, VmStk, VmExe: Size of data, stack, and text segments.
                  * VmLib: Shared library code size.*/
                             pTemp2 = pTemp;
                             while(1)
                             {
                                pTemp++;
                                s32Count++;
                                if(*pTemp == 0x0a)/* line feed reached */
                                {
                                  *pTemp = '\0';
                                  break;
                                }
                             }
                             TraceString(pTemp2);
                             if(bErrmem)
                             {
                                 vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,pTemp2,strlen(pTemp2),OSAL_STRING_OUT);
                             }
                         }
                    }
                }
            }
            OSAL_vMemoryFree(pBuffer);
        }
        close(fd);
    }
    system("rm /var/opt/bosch/dynamic/PrcInfo.txt");
}


typedef struct
{
 tU32 u32WantedFreeSize;
 tU32 u32FragRate;
 tU32 u32Intervall;
}tRedMemInfo;

void vReadMemStatus(tU32* pMemTotal, tU32* pMemFree,tBool bDisplay)
{
    char* pcReadBuf = NULL;
    tS32 fd;
    int size;
    tS32 s32Val;
    char* pTemp = NULL;
    char* pStart = NULL;

    system("cat /proc/meminfo > /var/opt/bosch/dynamic/MemStatus.txt");
    if(bDisplay)
    {
        vReadFile("/var/opt/bosch/dynamic/MemStatus.txt",FALSE);/*lint !e1773 */  /*otherwise linker warning */
    }
    if((pMemTotal)&&(pMemFree))
    {
        if((fd = open("/var/opt/bosch/dynamic/MemStatus.txt", O_RDONLY,0)) >= 0)
        {
            if((size = s32GetFileSize(fd))== 0)
            {
                    TraceString("File Size 0 Error");
            }
            if((pStart = (char*)OSAL_pvMemoryAllocate((tU32)size)) != NULL)
            {
              pcReadBuf = pStart;
              if((s32Val = lseek(fd,0,SEEK_SET)) == -1)
                {
                    TraceString("Seek File Error");
                }
                else
                {
                    if((s32Val = read( fd,pcReadBuf,(tU32)size)) != size)
                    {
                      TraceString("Read File Error");
                    }
                }
                close(fd);
                if(s32Val != -1)
                {
                    /* search for values */
                    s32Val = 0;
                    pTemp  = NULL;
                    while (1)
                    {
                        pcReadBuf++;
                        if(*pcReadBuf == 0x03 /*ETX*/)break;
                        /* start of value*/
                        if((*pcReadBuf >= 0x30 /* 0 */)&&(*pcReadBuf < 0x39 /* 9 */ ))
                        {
                             pTemp = pcReadBuf;
                             while(1)
                             {
                                 pcReadBuf++;
                                 /* end of value*/
                                 if((*pcReadBuf == ' ')||(*pcReadBuf == 'k'))
                                 {
                                     /* found value */
                                     s32Val++;
                                     *pcReadBuf = '\0';
                                     switch(s32Val)
                                     {
                                        case 1:
                                              *pMemTotal = atoi(pTemp);
                                             break;
                                        case 2:
                                              *pMemFree = atoi(pTemp);
                                             break;
                                        default:
                                             break;
                                     }
                                     pTemp  = NULL;
                                     /* leave inner loop*/
                                     break;
                                 }
                             }// while (1)
                        }// if(
                        if(s32Val >= 2)break;
                    } //while (1)
                }
                OSAL_vMemoryFree(pStart);
            }
            else
            {
                TraceString("Allocation failed");
            }
            close(fd);
        }
        else
        {
            TraceString("file open failed");
        }
    }
    system("rm /var/opt/bosch/dynamic/MemStatus.txt");
}


/*lint -save -e{429} memory should not be freed here*/
void vStartMemConsumer(void* pvArg)
{
 tRedMemInfo *pRedMemInfo = NULL;
 tU32 u32CurrentFreeSize = 0;
 tU32 u32TotalSize = 0;
 tU32 u32WantedFreeSize = 0;
 tU32 u32FragRate = 0;
 tU32 u32Intervall = 0;
 void* pTemp;
 pRedMemInfo = (tRedMemInfo*)pvArg;
 u32WantedFreeSize = pRedMemInfo->u32WantedFreeSize/1024;
 u32FragRate         = pRedMemInfo->u32FragRate;
 u32Intervall        = pRedMemInfo->u32Intervall;


 while(u32WantedFreeSize)
 {
     vReadMemStatus(&u32TotalSize,&u32CurrentFreeSize,FALSE);
     TraceString("Requested Free Size:%d kB,Total Size:%d kB Free Size:%d kB",
                     u32WantedFreeSize,u32TotalSize,u32CurrentFreeSize);
     if(u32WantedFreeSize > u32CurrentFreeSize)
     { 
         TraceString("Requested reduction size reached");
         break;
     }
     else
     { 
         pTemp = (void*)OSAL_pvMemoryAllocate(u32FragRate);
         if(pTemp)
         {
             memset(pTemp,0,u32FragRate);
             TraceString("Requested Free Size:%d kB,Total Size:%d kB Free Size:%d kB",
                              u32WantedFreeSize,u32TotalSize,u32CurrentFreeSize);
         }
         else
         {
             TraceString((const char*)"Malloc failed");
             break;
         }
         OSAL_s32ThreadWait((tS32)u32Intervall);
     }
 }
}
/*lint -restore */     

/*lint -save -e{429} memory should not be freed here*/
void vReduceMemory(tU32 u32MemoryLeftInKB, tU32 u32FragRate, tU32 u32Intervall)
{
 tU32 u32CurrentFreeSize = 0;
 tU32 u32TotalSize = 0;
 tU32 u32WantedFreeSize = 0;
 tU8 au8Buf[MAX_TRACE_SIZE];
 OSAL_trThreadAttribute  attr;
 tRedMemInfo rRedMemInfo;
 void* pTemp = NULL;
 char name[50];
 tU32 Blocksize = sysconf(_SC_PAGE_SIZE);

 vReadMemStatus(&u32TotalSize,&u32CurrentFreeSize,FALSE);

 if(u32TotalSize)
 {
     TraceString("Block Size:%d ,Total Size:%d Free Size:%d",
                     Blocksize,u32TotalSize,u32CurrentFreeSize);
  
     u32WantedFreeSize  = u32MemoryLeftInKB*1024;
     if(u32WantedFreeSize == 0)
     {
         TraceString("Reduce Size to Zero not possible");
     }
     else
     {
        if(u32WantedFreeSize > u32CurrentFreeSize)
        { 
            TraceString("Reduce Size already reached");
        }  
        else
        {  
          if(u32FragRate)
          { 
              rRedMemInfo.u32Intervall        = u32Intervall*1000; //seconds
              rRedMemInfo.u32FragRate         = u32FragRate;         //KByte
              rRedMemInfo.u32WantedFreeSize = u32WantedFreeSize; //KB
              /* create task for application SW */

              snprintf(name,50,"MemConsumer_%u",OSAL_ClockGetElapsedTime());
              attr.szName = (tString)name;/*lint !e1773 */  /*otherwise linker warning */
              attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
              attr.s32StackSize = minStackSize;
              attr.pfEntry = (OSAL_tpfThreadEntry)vStartMemConsumer;
              attr.pvArg = (void*) &rRedMemInfo;
              if(OSAL_ThreadSpawn(&attr) == OSAL_ERROR)
              { 
                        TraceString("Start task failed");
              } 
          }  
          else 
          { 
              pTemp = (void*)OSAL_pvMemoryAllocate((u32CurrentFreeSize - u32WantedFreeSize));
              if(pTemp != NULL)
              {
                 OSAL_M_INSERT_T8(&au8Buf[0],OSAL_CORE_DATA_HEAP);
                 OSAL_M_INSERT_T32(&au8Buf[1],Blocksize);
                 OSAL_M_INSERT_T32(&au8Buf[5],u32TotalSize);
                 OSAL_M_INSERT_T32(&au8Buf[9],u32CurrentFreeSize);
                 LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,13);
              }
          } 
        }
     } 
  } 
}
/*lint -restore */     

void vTraceHeap(void)
{
    tU32 u32CurrentFreeSize = 0;
    tU32 u32TotalSize = 0;
    tU32 Blocksize = sysconf(_SC_PAGE_SIZE);

    vReadMemStatus(&u32TotalSize,&u32CurrentFreeSize,TRUE);

    TraceString("Block size:%d ,Total Size:%d Free Size:%d",
                    Blocksize,u32TotalSize,u32CurrentFreeSize);
}


void vGetPrmStatusInfo(void)
{
    int i;
    int fd;
    char  path[150];
    for(i= 0;i< (PRM_MAX_USB_DEVICES + 1);i++)
    {
        if(pOsalData->u16UsbMediaType[i] == OSAL_C_U16_MEDIA_EJECTED)
        {
             if(i == PRM_MAX_USB_DEVICES)
             {
                 TraceString("PRM SD IF Slot%d:%s",i ,"OSAL_C_U16_MEDIA_EJECTED");
             }
             else
             {
                  TraceString("PRM USB Slot%d:%s",i ,"OSAL_C_U16_MEDIA_EJECTED");
             }
        }
        else
        {
             if(i == PRM_MAX_USB_DEVICES)
             {
                 TraceString("PRM SD IF Slot%d:%s with UUID %s",i ,"OSAL_C_U16_DATA_MEDIA",&pOsalData->u8DeviceUUID[i][0]);
             }
             else
             {
                 snprintf(path,150,"/media/%s/cryptnav",&pOsalData->u8DeviceUUID[i][0]);
                 if((fd = open(path,O_RDONLY))== -1)
                 {
                     TraceString("PRM USB Slot%d:%s with UUID %s",i,"OSAL_C_U16_DATA_MEDIA",&pOsalData->u8DeviceUUID[i][0]);
                 }
                 else
                 {
                     close(fd);
                     TraceString("PRM USB Slot%d:%s with UUID %s  -> cryptnav device",i,"OSAL_C_U16_DATA_MEDIA",&pOsalData->u8DeviceUUID[i][0]);
                 }
             }
        }
    }
}


/*****************************************************************************
*
* FUNCTION:     vOsalIoCallbackHandler
*
* DESCRIPTION: Handling of OSAL IO Callbacks              
*
* PARAMETER:    void* pvBuffer    command via trace
*
* RETURNVALUE: none
*
* HISTORY:
* Date        |    Modification                                 | Authors
* 03.10.05  | Initial revision                              | MRK2HI
* --.--.--  | ----------------                              | -----
*
*****************************************************************************/
void vOsalIoCallbackHandler(void* pvBuffer )
{
    tPU8 pu8Buffer;
    tS32 s32Size;
    tU32 u32Val,u32Val2;
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
    OSAL_tIODescriptor Des = OSAL_ERROR;
#endif

    pu8Buffer = (tPU8)pvBuffer;

    switch ((tS32)pu8Buffer[2]) 
    {
     case MSGPOOL_BLOCKS:
             OSAL_vPrintMessageList(0);
            break;
      case MSGPOOL_ABS_SIZE:
             s32Size = OSAL_s32MessagePoolGetAbsoluteSize();
             TraceString("OSAL_s32MessagePoolGetAbsoluteSize() %d\n", s32Size);
            break;
      case MSGPOOL_CUR_SIZE:
             s32Size = OSAL_s32MessagePoolGetCurrentSize();
             TraceString( "OSAL_s32MessagePoolGetCurrentSize() %d\n", s32Size);
            break;
      case MSGPOOL_MIN_SIZE:
             s32Size = OSAL_s32MessagePoolGetMinimalSize();
             TraceString( "OSAL_s32MessagePoolGetMinimalSize() %d\n", s32Size);
            break;
      case MSGPOOL_MAX_SIZE:
             s32Size = (tS32)OSAL_u32GetMaxMessageSize();
             TraceString( "OSAL_u32GetMaxMessageSize() %d\n", s32Size);
            break;
      case OSAL_EM_TRACE:
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
             s32Size = 1;
             TraceString(" Switch Errmem Backend to Backend %d ",s32Size);
             Des = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
             if(Des != OSAL_ERROR)
             { 
                if(OSAL_s32IOControl(Des, OSAL_C_S32_IOCTRL_ERRMEM_SET_BE, (uintptr_t)&s32Size) != OSAL_OK)
                {
                     TraceString(" Set Errmem Backend %d failed",s32Size);
                }
                OSAL_s32IOClose(Des);
             }
#endif
             s32TraceErrmem(FALSE,0);
            break;
      case OSAL_EM_TRACE2:
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
             s32Size = 2;
             TraceString(" Switch Errmem Backend to Backend %d ",s32Size);
             Des = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
             if(Des != OSAL_ERROR)
             { 
                if(OSAL_s32IOControl(Des, OSAL_C_S32_IOCTRL_ERRMEM_SET_BE, (uintptr_t)&s32Size) != OSAL_OK)
                {
                     TraceString(" Set Errmem Backend %d failed",s32Size);
                }
                OSAL_s32IOClose(Des);
             }
#endif
             s32TraceErrmem(FALSE,0);
            break;
      case OSAL_EM_TRACE_EXTENDED:
             s32TraceErrmem(TRUE,0);
            break;
      case OSAL_EM_TRACE_TO_MS:
             vWriteErrmemToMassstorage((char*)&pu8Buffer[3]);
            break;
      case OSAL_EM_ERASE:
             vEraseErrmem(0);
//             vEraseErrmem(1);
//             vEraseErrmem(2);
            break;
      case OSAL_TOP:
             vGetTopInfo(FALSE);
            break;
      case OSAL_PS:
             vProcTaskInfo((char*)&pu8Buffer[3],FALSE);
            break;
      case OSAL_SET_DEV_TRACE:
             vSetDevTrace((char*)&pu8Buffer[3],(tCString)&pu8Buffer[7]);
             break;
      case OSAL_PRC_MEM:
             TraceString("vDumpMemStatusForProcess for  %s\n", &pu8Buffer[3]);
             vDumpMemStatusForProcess((char*)&pu8Buffer[3],FALSE);
            break;
      case OSAL_PRC_MEM2:
             s32Size = s32GetPidFromName((char*)&pu8Buffer[3],strlen((char*)&pu8Buffer[3]));
             TraceString("vDumpMemStatusForProcess for PID %d\n", s32Size);
             vDumpMemStatusForProcess((char*)&s32Size,FALSE);
            break;
      case OSAL_TRACE_SYSTEM_REG:
              switch(pu8Buffer[3])
              {
                  case 0x00:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE");
                        break;
                  case 0x01:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS");
                        break;
                  case 0x02:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/SYSTEM");
                        break;
                  case 0x03:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS");
                        break;
                  case 0x04:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/BASE");
                        break;
                  case 0x05:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/CONFIG/BASE");
                        break;
                  case 0x06:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/LINUX");
                        break;
                  case 0x07:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/VARIANT");
                        break;
                  case 0x08:
                         vOsalTraceOutRegistry("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/SENSORS");
                        break;
                  default:
                         TraceString("Unknown Registry root");
                        break;
              }
            break;
      case OSAL_TRACE_SYSTEM_REG_PRC:
                  if(pu8Buffer[3] == '\0')
                  {
                         TraceString("Entries in LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS");
                         vOsalTraceOutRegistryDir("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/");
                         TraceString("Entries in LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/CONFIG");
                         vOsalTraceOutRegistryDir("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/CONFIG");
                  }
                  else
                  {
                      char Buffer[128];
                      snprintf(Buffer,128,"/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/%s",(tCString)&pu8Buffer[3]);
                      TraceString(Buffer);
                      vOsalTraceOutRegistry(Buffer);
                      snprintf(Buffer,128,"/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/PROCESS/CONFIG/%s",(tCString)&pu8Buffer[3]);
                      TraceString(Buffer);
                      vOsalTraceOutRegistry(Buffer);
                  }
            break;
      case PRM_INFO:
             vGetPrmStatusInfo();
            break;
      case OSAL_KILL_LOAD_TASK:
             vOsalKillLoadTask(pu8Buffer[3]);
            break;
      case OSAL_SPAWN_LOAD_TASK:
             vOsalSpawnLoadTask(pu8Buffer[3], pu8Buffer[4], pu8Buffer[5]);
            break;
      case OSAL_GET_LOAD_TASK_STATUS:
             vOsalDisplayLoadTaskStatus();
            break;
      case OSAL_REDUCE_MEM:
             memcpy(&u32Val,(void*)(pu8Buffer+3),4);
             memcpy(&u32Val2,(void*)(pu8Buffer+7),4);
             TraceString("Reduce Memory to %d kB in Steps with %d kB and Intervall of %d sec",
                              u32Val,u32Val2,pu8Buffer[11]);

            vReduceMemory(u32Val,u32Val2,pu8Buffer[11]);
            break;
      case OSAL_CORE_DATA_HEAP: // HEAP Data required
             vTraceHeap();
            break;
      default: // do nothing
              TraceString("Unknown Command for LINUX OSAL");
            break;
    }
}

void vDumpErrMem(void)
{
    OSAL_tIODescriptor fderrmem;
    char cFilepath[100] = {0};
    char cBasicPath[100] = {0};
    tBool bPreparation = TRUE;
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
    OSAL_szStringCopy(cBasicPath,"/var/opt/bosch/dynamic/");
#else
    struct stat buf;
    char *filename;
    FILE *f;
    bPreparation = FALSE;
    if (stat("/tmp/errmemd.file", &buf) != -1)
    {
        filename = (char *)malloc(buf.st_size + 1);
        if(filename)
        {
            memset(filename, 0, buf.st_size + 1);
            f = fopen("/tmp/errmemd.file", "rb");
            if (f)
            {
              if (fread(filename, buf.st_size, 1, f) > 0)
              {
                  /* remove \n at end */
                  if (filename[buf.st_size-1] == '\n') 
                  {
                      filename[buf.st_size-1] = 0;
                      OSAL_szStringCopy(cBasicPath,filename);
                      bPreparation = TRUE;
                  }
              }
              fclose(f);
            }
            free(filename);
        }
    }
#endif
    if(bPreparation)
    {
/*      char szCommand[100];
      OSAL_szStringCopy(szCommand,"mount -o remount,rw ");
      OSAL_szStringConcat(szCommand,cBasicPath);
      OSAL_szStringConcat(szCommand, "\n");
      system(szCommand);*/

      cFilepath[0] = '\0';
      OSAL_szStringCopy(cFilepath,"/dev/root/");
      OSAL_szStringConcat(cFilepath,cBasicPath);
      OSAL_szStringConcat(cFilepath,".pro");

      if((fderrmem = OSAL_IOOpen(&cFilepath[0], OSAL_EN_READWRITE)) == OSAL_ERROR)
      {
         /* create file for errmem download */
          if((fderrmem = OSAL_IOCreate(&cFilepath[0], OSAL_EN_READWRITE )) == OSAL_ERROR)
          {
              snprintf(cBasicPath,100,"OSAL_IOCreate failed %u",OSAL_u32ErrorCode());
              TraceString(cBasicPath);
          }
      }
      if(fderrmem != OSAL_ERROR)
      {
          if(s32TraceErrmem(FALSE,fderrmem) == OSAL_OK)
          {
              vEraseErrmem(0);
          }
          OSAL_s32IOClose(fderrmem);  
      }
    }
}

extern sem_t* sem_init_prm;

tBool __attribute__ ((constructor)) bOnOsal2ProcessAttach(tVoid)
{
    pOsalData->u32AttachedProcesses2++;
    return TRUE;
}



void __attribute__((destructor))vOnOsal2ProcessDetach( void )
{
  pOsalData->u32AttachedProcesses2--;
  if(sem_init_prm != SEM_FAILED)
  {
      sem_close(sem_init_prm);
  }  
  if(pOsalData->u32AttachedProcesses2 == 0)
  {
      sem_unlink(OSAL2_NAME_LOCK);
  }
}

/* This function will fetch the filesystem name information from /proc/mounts for the requested folder*/
tCString bGetPartitionInfo(tCString Path)
{
    tCString cRet = "INVALID";
    struct mntent *mnt;
    FILE *mnt_file;
    struct stat stats;
    dev_t device;
    /* /proc/mounts file contains the list of all mounted file systems */
    mnt_file = setmntent("/proc/mounts", "r");
    if(mnt_file && Path)
    {
        if(stat(Path, &stats) != 0)
        {
            TraceString("Stat failed for the requested folder \n");
        }
        else
        {
            device = stats.st_dev; 
            while( NULL != (mnt = getmntent(mnt_file)))
            {
                if(stat(mnt->mnt_dir, &stats) != 0)
                {
                    continue;
                }
                if(stats.st_dev == device)
                {
                    endmntent(mnt_file);
                    return (mnt->mnt_fsname);
                }
            }
        }
    }
    TraceString("Not a valid Folder location \n");
    return cRet;
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalfpe.cpp
|-----------------------------------------------------------------------*/
