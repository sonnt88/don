/*****************************************************************************
| FILE:         osalinit.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Startup-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 17.12.12  | To provide thread exit     |
|           | Entry for Timers           | SWM2KOR
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"
#include "exception_handler.h"

#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
//#define MEASURE_OSAL_START


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static sem_t *init_lock;
static sem_t *initsync_lock;

/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/
OSAL_DECL trGlobalOsalData*  pOsalData       = 0;
OSAL_DECL void*              pOsalResData    = 0;
OSAL_DECL tS32               s32OsalGroupId  = 0;
OSAL_DECL trHandleMpf        MqMemPoolHandle = {0,NULL,NULL,0,0};
OSAL_DECL trHandleMpf        EvMemPoolHandle = {0,NULL,NULL,0,0};
OSAL_DECL trHandleMpf        DescMemPoolHandle = {0,NULL,NULL,0,0};
OSAL_DECL trHandleMpf        FileMemPoolHandle = {0,NULL,NULL,0,0};
OSAL_DECL trHandleMpf        SemMemPoolHandle = {0,NULL,NULL,0,0};

OSAL_DECL trEventElement*        pEventEl  = NULL;
OSAL_DECL trThreadElement*       pThreadEl = NULL;
OSAL_DECL trElementNode*         pNodeEl   = NULL;
OSAL_DECL trSharedMemoryElement* pShMemEl  = NULL;
OSAL_DECL trSemaphoreElement*    pSemEl    = NULL;
OSAL_DECL trMutexElement*        pMutexEl  = NULL;
OSAL_DECL trTimerElement*        pTimerEl  = NULL;
OSAL_DECL trMqueueElement*       pMsgQEl   = NULL;
OSAL_DECL trStdMsgQueue*         pStdMQEl  = NULL;
OSAL_DECL trProcessInfo*         prProcDat = NULL;
OSAL_DECL trOsalLock*            prOsalLock= NULL;
OSAL_DECL trRBTreeData*          prRBTree  = NULL;
OSAL_DECL trOsalLock*            pThreadLock  = NULL;


tS32 s32ExcHdrStatus = 0;
tS32 hLiShMem        = 0;
tS32 hLiResShMem     = 0;
tS32 processIndex    = -1;
trProcessInfo*  gpOsalProcDat = NULL;

tBool bOsalStrace = FALSE;
extern OSAL_tShMemHandle      MsgPoolSharedMem;
extern char process_name[256];

/************************************************************************
|function prototype (scope: module-global)
|-----------------------------------------------------------------------*/
extern tS32 s32EventTableCreate(void );
extern tS32 s32ThreadTableCreate(void);
extern tS32 s32ProcessTableCreate(void);
extern tS32 s32SemaphoreTableCreate(void );
extern tS32 s32MutexTableCreate(void );
extern tS32 s32TimerTableCreate(void );
extern tS32 s32MqueueTableCreate(void );
extern tS32 s32SharedMemoryTableCreate(void );
extern void vRegisterSysCallback(void);
extern void vUnregisterSysCallback(void);
extern void vUnregisterOsalIO_Callback(void);
extern void vTriggerSigRtMin(tS32 s32Pid, char* Name);

//extern void OSAL_InitTable(void);
extern tS32 s32RegistryDeInit(void);
extern tS32 s32RegistryInit(void);
extern tS32 s32RegistryMemUnlink(void);
extern void vSetupRegistry(void);
extern void vNewCallbackHandler(void* pArg);
extern void vGenerateSyncObjects(void);
extern void vDestroySyncObjects(void);
extern tS32 s32DestroyOsalLock(const char* name);
extern void exception_handler_unlock(void);
extern void vSetThreadSignalMask(void);
extern void vSignalHandlerInstall(void);
extern void InitSignalFifo(void);
extern void vRegisterOsalIO_Callback(void);
extern void vCleanUpMqofContext(void);
extern void vCleanUpShMemofContext(void);
extern void vCleanUpSemaphoreofContext(void);
extern void vCleanUpEventofContext(void);
extern void vPrepareOsalFileSystemDevices(void);
extern void InitOsalTrace(void);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
void vSetOsalDataMagic(void)
{
   pOsalData->u32MagicProtAreaMid2 = 0x0aaaaaaa;
   pOsalData->u32Magic1            = 0x1aaaaaaa;
   pOsalData->u32Magic2            = 0x2aaaaaaa;
   pOsalData->u32Magic3            = 0x3aaaaaaa;
   pOsalData->u32Magic4            = 0x4aaaaaaa;
   pOsalData->u32Magic5            = 0x5aaaaaaa;
   pOsalData->u32Magic6            = 0x6aaaaaaa;
   pOsalData->u32Magic7            = 0x7aaaaaaa;
   pOsalData->u32Magic8            = 0x8aaaaaaa;
   pOsalData->u32MagicEnd          = 0x9aaaaaaa;
}

void vCheckOsalDataMagic(void)
{
   tU32 u32Reset = 0;
   if(pOsalData->u32MagicProtAreaMid2 != 0x0aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x0aaaaaaa,pOsalData->u32MagicProtAreaMid2);
       u32Reset++;
   }
   if(pOsalData->u32Magic1 != 0x1aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x1aaaaaaa,pOsalData->u32Magic1);
       u32Reset++;
   }
   if(pOsalData->u32Magic2 != 0x2aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x2aaaaaaa,pOsalData->u32Magic2);
       u32Reset++;
   }
   if(pOsalData->u32Magic3 != 0x3aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x3aaaaaaa,pOsalData->u32Magic3);
       u32Reset++;
   }
   if(pOsalData->u32Magic4 != 0x4aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x4aaaaaaa,pOsalData->u32Magic4);
       u32Reset++;
   }
   if(pOsalData->u32Magic5 != 0x5aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x5aaaaaaa,pOsalData->u32Magic5);
       u32Reset++;
   }
   if(pOsalData->u32Magic6 != 0x6aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x6aaaaaaa,pOsalData->u32Magic6);
       u32Reset++;
   }
   if(pOsalData->u32Magic7 != 0x7aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x7aaaaaaa,pOsalData->u32Magic7);
       u32Reset++;
   }
   if(pOsalData->u32Magic8 != 0x8aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x8aaaaaaa,pOsalData->u32Magic8);
       u32Reset++;
   }
   if(pOsalData->u32MagicEnd != 0x9aaaaaaa)
   {
       vWritePrintfErrmem("vCheckOsalDataMagic Magic 0x%x overwritten with 0x%x \n",0x9aaaaaaa,pOsalData->u32MagicEnd);
       u32Reset++;
   }
   if(u32Reset)
   {
       FATAL_M_ASSERT_ALWAYS();
   }
}

void vReadOsalValFromPram(void)
{
   OSAL_tIODescriptor fd;
   trOsalPramDatat  rOsalPramDatat;

   fd = OSAL_IOOpen("/dev/pram/osal" , OSAL_EN_READWRITE);
   if(OSAL_ERROR != fd)
   {
     pOsalData->bPramActive = TRUE;
     if(OSAL_s32IORead(fd, (tPS8)&rOsalPramDatat, sizeof(rOsalPramDatat)) == OSAL_ERROR)
     {
        TraceString("Cannot Read valid values from PRAM! Error:0x%x",OSAL_u32ErrorCode());
     }
     else
     {
        if((pOsalData->u32MaxEvtResCount < rOsalPramDatat.u32MaxEvtResCount)
         &&(pOsalData->u32MaxEvtResCount < pOsalData->u32MaxNrEventElements))
        {
            pOsalData->u32MaxEvtResCount = rOsalPramDatat.u32MaxEvtResCount;
        }
        if((pOsalData->u32MaxMqResCount < rOsalPramDatat.u32MaxMqResCount)
         &&(pOsalData->u32MaxMqResCount < pOsalData->u32MaxNrMqElements))
        {
            pOsalData->u32MaxMqResCount  = rOsalPramDatat.u32MaxMqResCount;
        }
        if((pOsalData->u32MaxTskResCount < rOsalPramDatat.u32MaxTskResCount)
         &&(pOsalData->u32MaxTskResCount < pOsalData->u32MaxNrThreadElements))
        {
            pOsalData->u32MaxTskResCount = rOsalPramDatat.u32MaxTskResCount;
        }
        if((pOsalData->u32MaxSemResCount < rOsalPramDatat.u32MaxSemResCount)
         &&(pOsalData->u32MaxSemResCount < pOsalData->u32MaxNrSemaphoreElements))
        {
            pOsalData->u32MaxSemResCount = rOsalPramDatat.u32MaxSemResCount;
        }
        if((pOsalData->u32MaxTimResCount < rOsalPramDatat.u32MaxTimResCount)
         &&(pOsalData->u32MaxTimResCount <pOsalData->u32MaxNrTimerElements))
        {
            pOsalData->u32MaxTimResCount= rOsalPramDatat.u32MaxTimResCount;
        }
        if((pOsalData->u32MaxShMResCount < rOsalPramDatat.u32MaxShMResCount)
         &&(pOsalData->u32MaxShMResCount < pOsalData->u32MaxNrSharedMemElements))
        {
            pOsalData->u32MaxShMResCount = rOsalPramDatat.u32MaxShMResCount;
        }
     }
     OSAL_s32IOClose(fd);
   }
   else
   {
       TraceString("OSAL_IOOpen(/dev/pram/osal  failed %d",errno);
   }
}

void vSaveOsalValToPram(void)
{
   OSAL_tIODescriptor fd = -1;
   trOsalPramDatat  rOsalPramDatat;

   if(pOsalData->bPramActive == TRUE)
   {
      if(pOsalData->u32NrOfChanges > 0)
      {
         rOsalPramDatat.u32MaxEvtResCount = pOsalData->u32MaxEvtResCount;
         rOsalPramDatat.u32MaxMqResCount  = pOsalData->u32MaxMqResCount;
         rOsalPramDatat.u32MaxTskResCount = pOsalData->u32MaxTskResCount;
         rOsalPramDatat.u32MaxSemResCount = pOsalData->u32MaxSemResCount;
         rOsalPramDatat.u32MaxTimResCount = pOsalData->u32MaxTimResCount;
         rOsalPramDatat.u32MaxShMResCount = pOsalData->u32MaxShMResCount;

         fd = OSAL_IOOpen("/dev/pram/osal" , OSAL_EN_READWRITE);
         if(OSAL_ERROR != fd)
         {
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0) != OSAL_ERROR)
            {
               if(OSAL_s32IOWrite(fd, (tPCS8)&rOsalPramDatat,sizeof(rOsalPramDatat)) == OSAL_ERROR)
               {
                   TraceString("Cannot save values to PRAM!  Error:0x%x",OSAL_u32ErrorCode());
               }
               else
               {
                  pOsalData->u32UpdateTime = OSAL_ClockGetElapsedTime();
               }
               pOsalData->u32NrOfChanges = 0;
            }
            else
            {
               TraceString("Cannot set position in PRAM!");
            }
            OSAL_s32IOClose(fd);
         }
         else
         {
             TraceString("OSAL_IOOpen(/dev/pram/osal  failed %d",errno);
         }
      }
   }
   else
   {
  //    vReadOsalValFromPram();
   }
}

/************************************************************************
 * FUNCTION:      vDebugFacility
 *
 * DESCRIPTION:   some debug facility
 *
 * PARAMETER:
 *
 *   VOID         pvArg: pointer to arguments
 *
 * RETURNVALUE:
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ************************************************************************/
int* pGetLockAdress(void);
#define TIMEOUT        2000
#define REBOOT_TIMEOUT 20000

void vPrintPrcStatus(void)
{
   if(pPrcTskInfo == NULL)
   {
      if(u32LoadSharedObject((void*)pPrcTskInfo,pOsalData->rLibrary[EN_SHARED_OSAL2].cLibraryNames) == OSAL_E_NOTSUPPORTED)/*lint !e611 */ /* otherwise linker warning */
      {
          TraceString("OSAL PRM cannot be loaded");
      }
   }
   /* print status of all task of the process to error memory*/
   if(pPrcTskInfo != NULL)
   {
       pPrcTskInfo((char*)&pOsalData->s32ExcPid,TRUE);
   }
}


void vSendCallStackGenFromTerm(tS32 PID,int Signal)
{
   OSAL_tMQueueHandle  hTermMq = OSAL_C_INVALID_HANDLE;
   tOsalMbxMsg rMsg;
   rMsg.rOsalMsg.Cmd  = Signal;;
   rMsg.rOsalMsg.ID   = PID;
   if(OSAL_s32MessageQueueOpen(OSAL_TERM_LI_MQ,OSAL_EN_WRITEONLY,&hTermMq) == OSAL_OK)
   {
     // TraceString("OSAL_s32MessageQueueOpen OSAL_TERM_LI_MQ");
      if(OSAL_s32MessageQueuePost(hTermMq,(tPCU8)&rMsg,sizeof(tOsalMbxMsg),0) == OSAL_OK)
      {
         OSAL_s32ThreadWait(0);
      }
      OSAL_s32MessageQueueClose(hTermMq);
   }
}


void  vDebugFacility(void  *pvArg)
{
   tS32  state = OSAL_OK;
   tOsalMbxMsg rec_msg;
//   trThreadElement* pEntry;
   tBool bTerminate = FALSE;
   tU32 u32CurTime = 0;
   DIR* dir = NULL;
   int fd = -1;
   char szName[64] = {0};
   (tVoid)pvArg; // satisfy lint
   tS32 n;

#ifndef OSAL_LINUX_X86
   /* Osal valus from PRam which are held in reset case */
   //vReadOsalValFromPram();
#endif

   if(OSAL_s32MessageQueueCreate(OSAL_TERM_LI_MQ,30,sizeof(tOsalMbxMsg),OSAL_EN_READWRITE,&pOsalData->TermMq) == OSAL_ERROR)
   {
      if(OSAL_u32ErrorCode()!=(tS32)OSAL_E_ALREADYEXISTS)
      {
        FATAL_M_ASSERT_ALWAYS();        
      }
      else
      {   
          if(OSAL_s32MessageQueueOpen(OSAL_TERM_LI_MQ,OSAL_EN_READWRITE,&pOsalData->TermMq)==OSAL_ERROR)
          {
             FATAL_M_ASSERT_ALWAYS();        
          }
      }
   }


   int* ptr = pGetLockAdress();

   if(pOsalData->TermMq)
   {
     while(bTerminate == FALSE)
     {
       n = OSAL_s32MessageQueueWait(pOsalData->TermMq,(tU8*)&rec_msg,sizeof(tOsalMbxMsg),0,TIMEOUT);
       /* a case of timeout do supervision work */
       if (n <= 0)
       {
           /* 1. Check for Reset request because reboot and SIGPWR failed (-> Mediaplayer)*/
          if(pOsalData->s32ResetRequestPid)
          { 
              /* Check if process exist */
              snprintf(szName,64,"%s/OSAL/Processes/%d",VOLATILE_DIR,pOsalData->s32ResetRequestPid);
              if((fd = open(szName, O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC,OSAL_ACCESS_RIGTHS)) != -1)
              {
                  close(fd);
                  vWritePrintfErrmem("Reset Request by OSAL Prozess %d \n",pOsalData->s32ResetRequestPid);
                  eh_reboot();
              }
              else
              {
                  vWritePrintfErrmem("Reset Request Memory overwritten %d \n",pOsalData->s32ResetRequestPid);
              }
              pOsalData->s32ResetRequestPid = 0;
          }

          /* 2. check all magics in OSAL shared memory for overwritten data */
          vCheckOsalDataMagic();

          /* 3. check for problems with callstck generation used spinlock */

          /* take over lock data to avaoid race condition during analysis */
          tU32  u32LocalExcTime = pOsalData->u32ExcTime;
          tS32  s32LocalExcPid = pOsalData->s32ExcPid;
          tS32  s32LocalExcTid = pOsalData->s32ExcTid;

          /* check if lock is in use */
          if((u32LocalExcTime != 0)&&(ptr)&&(s32LocalExcPid))
          {
             char cBuf[200];
             u32CurTime = OSAL_ClockGetElapsedTime();
             /* check if related process has called exit during lock was occupied */
             snprintf(cBuf,20,"/proc/%d",s32LocalExcPid);
             if((dir = opendir(cBuf)) == NULL)
             {
                TraceString("Term ExcHdr Lock held for %d msec by dead PID:%d",u32CurTime - u32LocalExcTime,s32LocalExcPid);
                vWritePrintfErrmem("Term ExcHdr Lock held for %d msec by dead PID:%d \n",u32CurTime - u32LocalExcTime,s32LocalExcPid);

                if(pOsalData->u32ExcHdrActiv)
                {
                    TraceString("Term ExcHdr Lock held Crash happened during Callstack generation in Process PID:%d",s32LocalExcPid);
                    vWritePrintfErrmem("Term ExcHdr Lock held Crash happened during Callstack generation in Process PID:%d \n",s32LocalExcPid);
                    eh_reboot();
                }
                else
                {
                   /* release the lock*/
                   exception_handler_unlock();
                }
             }
             else
             {
                closedir(dir);
                dir = NULL;
                if((u32CurTime - u32LocalExcTime) > 2*TIMEOUT)
                {
                   /* Exception Handler lock is inuse more than 4 seconds */
                   if((*ptr != 0)&&(*ptr != 0x56745612))
                   {
                      /* Shared memory used for signal handler spinlock was overwrittten */
                      TraceString("Term EXC_LOCK_MAGIC overwritten: 0x%x",*ptr);
                      vWritePrintfErrmem("Term EXC_LOCK_MAGIC overwritten: 0x%x \n",*ptr);
                      FATAL_M_ASSERT_ALWAYS();
                   }
                   else
                   {
                      /* check for expected valid lock parameters*/
                      if((u32LocalExcTime)&&(s32LocalExcPid)&&(s32LocalExcTid))/*lint !e774 */ 
                      {
                         TraceString("Term ExcHdr Lock held by Process > %d msec by PID:%d TID:%d",u32CurTime - u32LocalExcTime,s32LocalExcPid,s32LocalExcTid);
                         vWritePrintfErrmem("Term ExcHdr Lock held by Process > %d msec by PID:%d TID:%d \n",u32CurTime - u32LocalExcTime,s32LocalExcPid,s32LocalExcTid);
                         snprintf(szName,64,"/proc/%d/task/%d",s32LocalExcPid,s32LocalExcTid);
                         if((dir = opendir(szName)) == NULL)
                         {
                            vWritePrintfErrmem("Term ExcHdr Cannot Open %s errno:%d \n",szName,errno);
                            /* check is situation is still there */
                            if(( u32LocalExcTime == pOsalData->u32ExcTime)&&(s32LocalExcPid == pOsalData->s32ExcPid)&&(s32LocalExcTid == pOsalData->s32ExcTid))
                            {
                               /* releated task is already dead */
                               exception_handler_unlock();
                               /* reboot to avoid further follow up errors */
                               eh_reboot();
                            }
                            else
                            {
                                vWritePrintfErrmem("Term ExcHdr Race Condition: Situation cleared automatically \n");
                            }
                         }
                         else
                         {
                            closedir(dir);
                            if(( u32LocalExcTime == pOsalData->u32ExcTime)&&(s32LocalExcPid == pOsalData->s32ExcPid)&&(s32LocalExcTid == pOsalData->s32ExcTid))
                            {
                               vTriggerSigRtMin(s32LocalExcPid,(char*)"TERM");/*lint -e1773 */
                            }
                            else
                            {
                               vWritePrintfErrmem("Term ExcHdr Race Condition: Situation cleared automatically \n");
                            }
                         }
                         /* check if exception handler hungs in Callstack generation otherwise Assert generation is active*/
                         if(pOsalData->u32ExcHdrActiv)
                         {
                            TraceString("Term ExcHdr Lock held during Callstack generation in Exception of Process PID:%d",s32LocalExcPid);
                            vWritePrintfErrmem("Term ExcHdr Lock held during Callstack generation in Exception of Process PID:%d \n",s32LocalExcPid);
                            eh_reboot();
                         }
                         else
                         {
                            if((u32CurTime - u32LocalExcTime) > (REBOOT_TIMEOUT))
                            {
                               TraceString("Term ExcHdr Lock held Process PID:%d",s32LocalExcPid);
                               vWritePrintfErrmem("Term ExcHdr Lock held  Process PID:%d \n",s32LocalExcPid);
                               eh_reboot();
                            }
                         }
                      }
                      else
                      {
                         TraceString("Term ExcHdr inconsistent Lock Data Time:%d msec PID:%d TID:%d",u32CurTime - u32LocalExcTime,s32LocalExcPid,s32LocalExcTid);
                         vWritePrintfErrmem("Term ExcHdr inconsistent Lock Data Time:%d msec by PID:%d TID:%d \n",u32CurTime - u32LocalExcTime,s32LocalExcPid,s32LocalExcTid);
                         if(exception_handler_lock() == 0)
                         {
                            TraceString("Term ExcHdr Could clean data");
                            vWritePrintfErrmem("Term ExcHdr Could clean data \n");
                            exception_handler_unlock();
                         }
                         else
                         {
                            if(( u32LocalExcTime == pOsalData->u32ExcTime)&&(s32LocalExcPid == pOsalData->s32ExcPid)&&(s32LocalExcTid == pOsalData->s32ExcTid))
                            {
                               vTriggerSigRtMin(s32LocalExcPid,(char*)"TERM");/*lint -e1773 */
                            }
                            if((u32CurTime - u32LocalExcTime) > (REBOOT_TIMEOUT))
                            {
                               TraceString("Term ExcHdr Lock held Process PID:%d",s32LocalExcPid);
                               vWritePrintfErrmem("Term ExcHdr Lock held  Process PID:%d \n",s32LocalExcPid);
                               eh_reboot();
                            }
                         }
                      }//if(pOsalData->u32ExcTime)&&(pOsalData->s32ExcPid)&&(pOsalData->s32ExcTid))
                   }// if((*ptr != 0)&&(*ptr != 0x56745612))
                }// if((fd = open(cBuf, O_RDONLY,0)) == -1)
             }//if((pOsalData->u32ExcTime != 0)&&(ptr))
          }
          /* save OSAL value if necessary */
          vSaveOsalValToPram();
       }
       else
       {
         /*execute received order */
  //       TraceString("Li Term recceives %d",rec_msg.rOsalMsg.Cmd);
         switch (rec_msg.rOsalMsg.Cmd)
         {
          case MBX_TERM_END:
                state++;
               break;
          case MBX_SUS_TSK:
                OSAL_s32ThreadSuspend((tS32)rec_msg.rOsalMsg.ID);
               break;
          case MBX_CB_TSK_RENICE:
                if(OSAL_s32ThreadPriority((tS32)rec_msg.rOsalMsg.ID,(tS32)rec_msg.rOsalMsg.Res1) == OSAL_ERROR)
                {
                    TraceString("OSAL_s32ThreadPriority with %d for %d failed",(tS32)rec_msg.rOsalMsg.Res1,rec_msg.rOsalMsg.ID);
                }
               break;
          case MBX_TERM_TSK:
               //pEntry =(trThreadElement*)rec_msg.rOsalMsg.ID;
              break;
          case MBX_GEN_CS:
               if((n = u32GetSigRtMinId(rec_msg.rOsalMsg.ID)) == -1)
               {
                  n = SIG_BACKTRACE;
               }
               /* get callstack of receiver */
               if(kill(rec_msg.rOsalMsg.ID,n) == -1)
               {
                  vWritePrintfErrmem("Kill PID:%d SIG_BACKTRACE failed Error:%d  \n",rec_msg.rOsalMsg.ID,errno);
               }
               break;
          case MBX_GEN_CS_USR2:
               TriggerCallstacks(rec_msg.rOsalMsg.ID);
               break;
          case MBX_GEN_TOP:
               system("top -n 1 -b > /dev/errmem");
               break;
          case MBX_ASSERT_SYS:
               FATAL_M_ASSERT_ALWAYS();
              break;
          default:
              break;
         }
         if (state != OSAL_OK)
         {
           bTerminate = TRUE;
           break;
         }
       }
     }
     OSAL_s32MessageQueueClose(pOsalData->TermMq);
     OSAL_s32MessageQueueDelete(OSAL_TERM_LI_MQ);
   }
   TraceString("Term Task terminates");
}

void vStopDebugTask(void)
{
  tOsalMbxMsg rMsg;
  OSAL_tMQueueHandle hMq = 0;
  rMsg.rOsalMsg.Cmd = MBX_TERM_END;
  rMsg.rOsalMsg.ID  = 0;

  if(OSAL_s32MessageQueueOpen(OSAL_TERM_LI_MQ,OSAL_EN_WRITEONLY,&hMq) == OSAL_OK)
  {
     if(OSAL_s32MessageQueuePost(hMq,(tPCU8)&rMsg,sizeof(tOsalMbxMsg),0) == OSAL_ERROR)
    {
    }
    OSAL_s32MessageQueueClose(hMq);
  }
}


/************************************************************************
 * FUNCTION:      vInitTrace
 *
 * DESCRIPTION:   initialize the trace connection, register trace callback
 *
 * PARAMETER:
 *
 *   tPVoid       pVArg: pointer to arguments
 *
 * RETURNVALUE:
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ************************************************************************/
void vInitTrace(void)
{
   LLD_bOpenTrace();

   /* ================================================================= */
   /*           Register Callback for Tracing                  */
   /* ================================================================= */
   vRegisterSysCallback();
   vRegisterOsalIO_Callback();
}

/************************************************************************
 * FUNCTION:      vGetOsalLock
 *
 * DESCRIPTION:
 *
 *
 * PARAMETER:     This function is used to get the process local lock handles
 *
 *   tBool        switch for create or open
 *
 * RETURNVALUE:
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ************************************************************************/
void vGetOsalLock(tBool bCreate)
{
  if(bCreate ==FALSE )
  {
   if(OpenOsalLock(&pOsalData->SyncObjLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->EventTable.rLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
#ifdef SIGNAL_EV_HDR
   if(OpenOsalLock(&pOsalData->EventTable.rPrcLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
#endif
   if(OpenOsalLock(&pOsalData->SemaphoreTable.rLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->MutexTable.rLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->TimerTable.rLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->MqueueTable.rLock) != OSAL_OK)
   {  FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->SharedMemoryTable.rLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->PrmTableLock) < OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(OpenOsalLock(&pOsalData->RegObjLock) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
  }
  else
  {
   if(CreateOsalLock(&pOsalData->SyncObjLock, "SYNC_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->EventTable.rLock, "EV__LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
#ifdef SIGNAL_EV_HDR
   if(CreateOsalLock(&pOsalData->EventTable.rPrcLock, "EV_PRCLOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
#endif
   if(CreateOsalLock(&pOsalData->SemaphoreTable.rLock, "SEM_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->MutexTable.rLock, "MTX_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->TimerTable.rLock, "TIM_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->MqueueTable.rLock, "MQ__LOCK") != OSAL_OK)
   {  FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->SharedMemoryTable.rLock, "SHM_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->PrmTableLock, "PRMTABLE") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
   if(CreateOsalLock(&pOsalData->RegObjLock, "RegObjLock") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
  }
}

void PrintOsalLockStates(void)
{
  trThreadElement *pCurrentEntry;
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->SyncObjLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "SYNC_LOCK",&pOsalData->SyncObjLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->EventTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "EV__LOCK", &pOsalData->EventTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->SemaphoreTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "SEM_LOCK", &pOsalData->SemaphoreTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->MutexTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "MTX_LOCK", &pOsalData->MutexTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->TimerTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "TIM_LOCK", &pOsalData->TimerTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->MqueueTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s)", "MQ__LOCK", &pOsalData->MqueueTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
  pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->SharedMemoryTable.rLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "SHM_LOCK", &pOsalData->SharedMemoryTable.rLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
 pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->PrmTableLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "PRMTABLE", &pOsalData->PrmTableLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
 pCurrentEntry = tThreadTableSearchEntryByID((OSAL_tThreadID)pOsalData->RegObjLock.rInfo.s32Tid[0],FALSE); 
  if((pCurrentEntry))
  {
     vWritePrintfErrmem("OSAL lock %s used by TID:%d (%s) \n", "RegObjLock", &pOsalData->RegObjLock.rInfo.s32Tid[0],pCurrentEntry->szName);
  }
}


void vGetNrOfCpuCores(void)
{
#ifdef OSAL_GEN3
  char cBuffer[1024];
  char* pRet = NULL;
  int count = 0;
  int fd = open("/proc/cpuinfo", O_RDONLY);
  if(fd != -1)
  {
    if(read(fd,cBuffer,1024)>0)
    {
       pRet = cBuffer;
       while((pRet = strstr(pRet,"processor"))!= NULL)
       {
           pRet = strstr(pRet,"processor");
           if(pRet != NULL)
           {
              count++;
              pRet += 10;
           }
           else
           {
              break;
           }
       }
    }  
    close(fd);
  }
  if(count == 1)pOsalData->bUseOsalCsGen = TRUE;
#endif
}


void vInitGlobalData(void)
{
   int i;
#ifdef PROTECTED_OSAL_AREAS
   uintptr_t Size = (uintptr_t)&pOsalData->u32MagicProtAreaMid - (uintptr_t)&pOsalData->u32MagicStart;
   memset((char*)&pOsalData->u32AttachedProcesses,0,Size);
   Size = (uintptr_t)&pOsalData->u32MagicEnd - (uintptr_t)&pOsalData->u32MagicProtAreaMid2;
   memset((char*)&pOsalData->u32MagicProtAreaMid2, 0,Size);
#else
   memset((char*)pOsalData, 0,sizeof(trGlobalOsalData));
#endif

   pOsalData->enAccessSdCard  = SDC_DEFAULT_ACCESS;
   pOsalData->u32UpperCase =  TRUE; 
    
   pOsalData->u16DvdMediaType    = OSAL_C_U16_MEDIA_EJECTED;
   pOsalData->u16CardMediaType1  = OSAL_C_U16_MEDIA_EJECTED;
   pOsalData->u16SdCardMediaType = OSAL_C_U16_MEDIA_EJECTED;

   for(i = 0; i < PRM_MAX_USB_DEVICES+1; i++)
   {
      pOsalData->enAccessUsb[i] = USB_DEFAULT_ACCESS;
      pOsalData->u16UsbMediaType[i] = OSAL_C_U16_MEDIA_EJECTED;
   }
   pOsalData->bPrmRecTaskActiv = TRUE;
   pOsalData->bPrmUsbPwrActiv  = TRUE;
   pOsalData->u32AssertMode = (tU32)ASSERTMODE_RESET;

   pOsalData->u16CheckSrc = 0xffff;
   pOsalData->u16CheckDst = 0xffff;
   pOsalData->u16ServId = 0xffff;
   pOsalData->u16FuncId = 0xffff;
   pOsalData->u32Prio   = 1;

   pOsalData->u32DeviceHandles = DEVDECS_BLOCKCOUNT;
   pOsalData->u32FileHandles   = FILEDECS_BLOCKCOUNT;
   pOsalData->u32MqHandles     = MQHANDLE_BLOCKCOUNT;
   pOsalData->u32EventHandles  = EVDAT_BLOCKCOUNT;
   pOsalData->u32SemHandles    = SEMDAT_BLOCKCOUNT;

   pOsalData->u32ExcStatus = OSAL_EXCHDR_STATUS;
   pOsalData->bLogError = FALSE;

   pOsalData->u32MsgPoolSize = OSAL_MSG_POOL_SIZE;

   pOsalData->u32MaxNrEventElements = MAX_OSAL_EVENTS;
   pOsalData->u32MaxNrThreadElements = MAX_OSAL_THREADS;
   pOsalData->u32MaxNrSharedMemElements = MAX_OSAL_SHARED_MEM;
   pOsalData->u32MaxNrSemaphoreElements = MAX_OSAL_SEMAPHORES;
   pOsalData->u32MaxNrMutexElements = MAX_OSAL_MUTEX;
   pOsalData->u32MaxNrTimerElements = MAX_OSAL_TIMER;
   pOsalData->u32MaxNrMqElements = MAX_OSAL_MSG_QUEUE; 
   pOsalData->u32MaxNrProcElements = MAX_LINUX_PROC;
   pOsalData->u32MaxNrLockElements = MAX_OSAL_LOCK;

   pOsalData->bEvTaceAllWithoutTimeout = TRUE;

   /* Set the initial counting time, OSAL_reference Manual chapter 14.2.6*/
   pOsalData->u32OsalStartTimeInMs = 0; /* do not delete */
   pOsalData->u32OsalStartTimeInMs = OSAL_ClockGetElapsedTime();
   pOsalData->u32TimSignal = OSAL_TIMER_SIGNAL;

   pOsalData->u32ClkMonotonic = 0x00000000;//0x01 = MQ ,0x10 =SEM
#ifdef NO_OSAL_EXIT_HDR
   pOsalData->u32AllowExit = 1;
#endif
   
   pOsalData->u32TimerResolution = LINUX_OS_TIME_RESOLUTION_IN_MS; // default 10msec
   struct timespec res = {0,0};
   if(clock_getres(CLOCK_REALTIME,&res) == 0)
   {
      if(res.tv_nsec > 1000000)
      {
          pOsalData->u32TimerResolution = res.tv_nsec/1000000;
      }
      else
      {
         pOsalData->u32TimerResolution = 1;
      }
   }
   TraceString("Timer Resolution for OSAL is %u msec",pOsalData->u32TimerResolution);

   pOsalData->u32TraceLevel = 0;
   
   pOsalData->u32DynMmapMsgSize = 0xffffffff;

   pOsalData->u32FirstPrc = getpid();
  
   InitOsalTrace();

#ifdef PRM_LIBUSB_CONNECTION
  for(i=0;i< MAX_USB_PORTS;i++)
  {
     pOsalData->prm_rPortState[i].u8PortNr = i + 1;
  }
#endif  
   vSetOsalDataMagic();
#ifdef CS_GEN_VIA_OSAL
   pOsalData->bUseOsalCsGen = TRUE;
#endif
}

void vInitGlobalTables(void)
{
   (void)s32ThreadTableCreate();

   (void)s32ProcessTableCreate();

   (void)s32EventTableCreate();

   (void)s32SemaphoreTableCreate();

   (void)s32MutexTableCreate();

   (void)s32TimerTableCreate();

   (void)s32SharedMemoryTableCreate();

   (void)s32MqueueTableCreate();

}

/***************************************************
* Layout of OSAL Res Shared Memory
****************************************************
  4kB Protected

  u32OffsetEventElements
  u32OffsetEventHandlePool

  u32OffsetThreadElements
  u32OffsetRbTreeData 
  u32OffsetNodeElements

  u32OffsetSharedMemElements

  u32OffsetSemaphoreElements 
  u32OffsetSemHandlePool

  u32OffsetMutexElements 
 
  u32OffsetTimerElements 
   
  u32OffsetMqElements
  u32OffsetStdMqElements    
  u32OffsetMqHandlePool;

  u32OffsetProcElements
  
  u32OffsetLockElements
  u32OffsetThreadLock
  
  u32OffsetDevDescHandlePool 
  u32OffsetFileDescHandlePool

  DataEnd

  Spare
  4kB Protected
****************************************************/
tU32 u32CalculateOsalResOffsets(void )
{
   tU32 Size = pOsalData->u32OsalResMemSize;
   tU32 Temp = 0;
/*	   TraceString("Sizes: E:0x%x T:0x%x RB:0x%x RBN:0x%x Sh:0x%x S:0x%x M:0x%x T:0x%x MQ:0x%x STD:0x%x MSG:0x%x ",
	   sizeof(trEventElement),sizeof(trThreadElement),sizeof(trRBTreeData), sizeof(trElementNode),
	   sizeof(trSharedMemoryElement),sizeof(trSemaphoreElement),sizeof(trMutexElement),sizeof(trTimerElement),
	   sizeof(trMqueueElement),sizeof(trStdMsgQueue),sizeof(OSAL_trMessage)); */
   Size += PROTECTED_PAGE_SIZE;
   pOsalData->u32OffsetEventElements = Size;
   Size += ((pOsalData->u32MaxNrEventElements) * sizeof(trEventElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetEventElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetEventHandlePool = Size;
   Size += u32CalculatePoolMemSize(EVDAT_BLOCKSIZE,pOsalData->u32EventHandles);
#ifdef AREA_TEST
   pOsalData->u32OffsetEventHandleElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   pOsalData->u32OffsetThreadElements = Size;
   Size += ((pOsalData->u32MaxNrThreadElements) * sizeof(trThreadElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetThreadElementsEnd = Size;
   Size += sizeof(tU32);
#endif
#ifdef RBTREE_SEARCH
   pOsalData->u32OffsetRbTreeData = Size;
   Size += sizeof(trRBTreeData);
#ifdef AREA_TEST
   pOsalData->u32OffsetRbTreeDataEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetNodeElements = Size;
   Size += ((pOsalData->u32MaxNrThreadElements+1) * sizeof(trElementNode));
#ifdef AREA_TEST
   pOsalData->u32OffsetNodeElementsEnd = Size;
   Size += sizeof(tU32);
#endif
#endif

   pOsalData->u32OffsetSharedMemElements = Size;
   Size += ((pOsalData->u32MaxNrSharedMemElements) * sizeof(trSharedMemoryElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetSharedMemElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   /* Semaphore releated part */
   pOsalData->u32OffsetSemaphoreElements = Size;
   Size += ((pOsalData->u32MaxNrSemaphoreElements) * sizeof(trSemaphoreElement)); 
#ifdef AREA_TEST
   pOsalData->u32OffsetSemaphoreElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetSemHandlePool = Size;
   Size += u32CalculatePoolMemSize(SEMDAT_BLOCKSIZE,pOsalData->u32SemHandles);
#ifdef AREA_TEST
   pOsalData->u32OffsetSemHandleElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   /* Mutex releated part */
   pOsalData->u32OffsetMutexElements = Size;
   Size += ((pOsalData->u32MaxNrMutexElements) * sizeof(trMutexElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetMutexElementsEnd = Size;
   Size += sizeof(tU32);
#endif
     
   /* Timer releated part */
   pOsalData->u32OffsetTimerElements = Size;
   Size += ((pOsalData->u32MaxNrTimerElements) * sizeof(trTimerElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetTimerElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   /* MQ releated part */
   pOsalData->u32OffsetMqElements = Size;
   Size += ((pOsalData->u32MaxNrMqElements) * sizeof(trMqueueElement));
#ifdef AREA_TEST
   pOsalData->u32OffsetMqElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetStdMqElements = Size;
   Size += ((pOsalData->u32MaxNrMqElements) * sizeof(trStdMsgQueue));
#ifdef AREA_TEST
   pOsalData->u32OffsetStdMqElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetMqHandlePool = Size;
   Size += u32CalculatePoolMemSize(MQHANDLE_BLOCKSIZE,pOsalData->u32MqHandles);
#ifdef AREA_TEST
   pOsalData->u32OffsetMqHandleElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   /* Process releated part */
   pOsalData->u32OffsetProcElements = Size;
   Size += ((pOsalData->u32MaxNrProcElements) * sizeof(trProcessInfo));
#ifdef AREA_TEST
   pOsalData->u32OffsetProcElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   /* Lock related part */
   pOsalData->u32OffsetLockElements = Size;
   pOsalData->u32MaxNrLockElements = (pOsalData->u32MaxNrEventElements * 2) + \
                                      pOsalData->u32MaxNrSemaphoreElements +\
	                                  (C_MAX_JOBS + 1);
   Size += ((pOsalData->u32MaxNrLockElements) * sizeof(trOsalLock));
#ifdef AREA_TEST
   pOsalData->u32OffsetLockElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetThreadLock = Size;
   Size += sizeof(trOsalLock);
#ifdef AREA_TEST
   pOsalData->u32OffsetThreadLockEnd = Size;
   Size += sizeof(tU32);
#endif

   /* IO related part */
   pOsalData->u32OffsetDevDescHandlePool = Size;
   Size += u32CalculatePoolMemSize(DEVDECS_BLOCKSIZE,pOsalData->u32DeviceHandles);
#ifdef AREA_TEST
   pOsalData->u32OffsetDevDescHandleElementsEnd = Size;
   Size += sizeof(tU32);
#endif
   pOsalData->u32OffsetFileDescHandlePool = Size;
   Size += u32CalculatePoolMemSize(FILEDECS_BLOCKSIZE,pOsalData->u32FileHandles);
#ifdef AREA_TEST
   pOsalData->u32OffsetFileDescHandleElementsEnd = Size;
   Size += sizeof(tU32);
#endif

   pOsalData->u32OffsetDataEnd = Size;
   Size += PROTECTED_PAGE_SIZE;
   pOsalData->u32OsalResMemSize = Size;

   /* ensure 4kb alignment to make protection possible */	  
   Temp = Size%PROTECTED_PAGE_SIZE;
   if(Temp)
   {
      pOsalData->u32OsalResMemSize = Size + PROTECTED_PAGE_SIZE - Temp;
   }
   return pOsalData->u32OsalResMemSize;
}

void vProtectOsalResGuardPages(void)
{
#ifdef PROTECTED_OSAL_AREAS
   /* protect gard pages at begin and end of OSAL shared memory*/
   int pagesize = sysconf(_SC_PAGE_SIZE);
   if(pagesize != PROTECTED_PAGE_SIZE)
   {
       TraceString("mprotect is configured with wrong pagesize %d instead of %d",PROTECTED_PAGE_SIZE,pagesize);
   }
   else
   {
      if(mprotect(pOsalResData,PROTECTED_PAGE_SIZE, PROT_NONE) == -1)
      {
         TraceString("Protection of OsalProtAreaStart 0x%x failed errno:%d ",pOsalResData,errno);
      }
      if(mprotect((void*)((uintptr_t)pOsalResData + pOsalData->u32OsalResMemSize - PROTECTED_PAGE_SIZE),PROTECTED_PAGE_SIZE, PROT_NONE) == -1)
      {
         TraceString("Protection of OsalProtAreaEnd 0x%x failed errno:%d ",(void*)((uintptr_t)pOsalResData + pOsalData->u32OsalResMemSize - PROTECTED_PAGE_SIZE),errno);
      }
   }
#endif 
}

void SetTestResLimitMarker(void)
{
#ifdef AREA_TEST
   tU32* pu32Temp; 
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetRbTreeDataEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetNodeElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSharedMemElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemaphoreElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMutexElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetTimerElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetStdMqElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetLockElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadLockEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemHandleElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqHandleElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventHandleElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetDevDescHandleElementsEnd);  
   *pu32Temp = 0xdeadbeef;
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetFileDescHandleElementsEnd);  
   *pu32Temp = 0xdeadbeef;
#endif
}

void vConnectOsalResMemory(tBool bCreate)
{
   if(bCreate)
   {
	  /* we are in the first OSAL process*/
	  /* calculate size of OSAL Res Memory*/
      (void)u32CalculateOsalResOffsets();
	  /* create OSAL Res Memory */
      hLiResShMem = shm_open(OSAL_NAME_RES_SHMEM, O_EXCL|O_RDWR|O_CREAT, OSAL_ACCESS_RIGTHS);
      if(hLiResShMem  == -1)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
      if(s32OsalGroupId)
      {
         /* adapt rights for shared memory */
         if(fchown(hLiResShMem,(uid_t)-1,s32OsalGroupId) == -1)
         {
            vWritePrintfErrmem("vCreateOsalResMemory shm_open -> fchown error %d \n",errno);
         }
         // umask (022) is overwriting the permissions on creation, so we have to set it again
         if(fchmod(hLiResShMem, OSAL_ACCESS_RIGTHS) == -1)
         {
            vWritePrintfErrmem("vCreateOsalResMemory shm_open -> fchmod error %d \n",errno);
         }
      }
      if (ftruncate(hLiResShMem, pOsalData->u32OsalResMemSize) == -1)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
	  /* open OSAL Res Memory for all following processes*/
      if((hLiResShMem = shm_open(OSAL_NAME_RES_SHMEM, O_RDWR ,0))  == -1)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
   }
   
   /* map shared memory into address space */
   pOsalResData = mmap(0x00000000L,pOsalData->u32OsalResMemSize, PROT_READ | PROT_WRITE,GLOBAL_DATA_OPTION,hLiResShMem,0);
   if(pOsalResData == MAP_FAILED)
   {
       FATAL_M_ASSERT_ALWAYS();
   }
   if(pOsalResData != MAP_FAILED)
   {
      SetTestResLimitMarker();
      /* protect guard pages of this shared memory */
      vProtectOsalResGuardPages();
  }

   /* calculate process relative addresses for OSAL resource shared memory*/
   pEventEl  = (trEventElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventElements);
   pThreadEl = (trThreadElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadElements);
   prRBTree  = (trRBTreeData*)((uintptr_t)pOsalResData + pOsalData->u32OffsetRbTreeData);
   pNodeEl   = (trElementNode*)((uintptr_t)pOsalResData + pOsalData->u32OffsetNodeElements); 
   pShMemEl  = (trSharedMemoryElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSharedMemElements);
   pSemEl    = (trSemaphoreElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemaphoreElements);
   pMutexEl  = (trMutexElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMutexElements);
   pTimerEl  = (trTimerElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetTimerElements);
   pMsgQEl   = (trMqueueElement*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqElements);
   pStdMQEl  = (trStdMsgQueue*)((uintptr_t)pOsalResData + pOsalData->u32OffsetStdMqElements);  
   prProcDat = (trProcessInfo*)((uintptr_t)pOsalResData + pOsalData->u32OffsetProcElements);  
   prOsalLock= (trOsalLock*)((uintptr_t)pOsalResData + pOsalData->u32OffsetLockElements);  
   pThreadLock= (trOsalLock*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadLock); 

   gpOsalProcDat = prProcDat;
   
   if(bCreate)
   {
	  /* we are in the first process  prepare Resource table and Handle pools*/
	  /* Prepare resource table to use OSAL resources in Fixed size Memory Pool */
      vInitGlobalTables();
	  /* Prepare internal handle Pools */
      if(OSAL_u32MemPoolFixSizeCreateOnMem("SemHandlePool", SEMDAT_BLOCKSIZE,
                                           &SemMemPoolHandle,/*pOsalData->SemaphoreTable.rLock used*/FALSE,
                                           (void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemHandlePool),
                                           pOsalData->u32OffsetMutexElements -pOsalData->u32OffsetSemHandlePool) == OSAL_ERROR)
      {
         FATAL_M_ASSERT_ALWAYS();										  
      }
      if(OSAL_u32MemPoolFixSizeCreateOnMem("MqHandlePool", MQHANDLE_BLOCKSIZE,&MqMemPoolHandle,/*pOsalData->MqTable.rLock used*/FALSE,
                                           (void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqHandlePool),
                                            pOsalData->u32OffsetProcElements-pOsalData->u32OffsetMqHandlePool) == OSAL_ERROR)
      {
         FATAL_M_ASSERT_ALWAYS();										  											  
      }
      if(OSAL_u32MemPoolFixSizeCreateOnMem("EventHandlePool", EVDAT_BLOCKSIZE, &EvMemPoolHandle,/*pOsalData->EventTable.rLock used*/FALSE,
                                           (void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventHandlePool),
                                           pOsalData->u32OffsetThreadElements-pOsalData->u32OffsetEventHandlePool) == OSAL_ERROR)
       {
		   FATAL_M_ASSERT_ALWAYS();										  
       }
       if(OSAL_u32MemPoolFixSizeCreateOnMem("DevDescPool", DEVDECS_BLOCKSIZE,&DescMemPoolHandle,TRUE,
                                            (void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetDevDescHandlePool),
                                            pOsalData->u32OffsetFileDescHandlePool-pOsalData->u32OffsetDevDescHandlePool) == OSAL_ERROR)
       {
          FATAL_M_ASSERT_ALWAYS();										  
       }
       if(OSAL_u32MemPoolFixSizeCreateOnMem("FileDescPool", FILEDECS_BLOCKSIZE,&FileMemPoolHandle,TRUE,
                                            (void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetFileDescHandlePool),
                                            pOsalData->u32OffsetDataEnd - pOsalData->u32OffsetFileDescHandlePool) == OSAL_ERROR)
       {
           FATAL_M_ASSERT_ALWAYS();										  
       }
   }
   else
   {
        /* we are in a following process  connect  Handle pools*/
       if(OSAL_u32MemPoolFixSizeOpenOnMem("SemHandlePool",&SemMemPoolHandle,(void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemHandlePool)) == OSAL_ERROR)
       {
           FATAL_M_ASSERT_ALWAYS();										  
       }
       if(OSAL_u32MemPoolFixSizeOpenOnMem("MqHandlePool",&MqMemPoolHandle,(void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqHandlePool)) == OSAL_ERROR)
       {
          FATAL_M_ASSERT_ALWAYS();										  											  
       }
       if(OSAL_u32MemPoolFixSizeOpenOnMem("EventHandlePool", &EvMemPoolHandle,(void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventHandlePool)) == OSAL_ERROR)
       {
          FATAL_M_ASSERT_ALWAYS();										  
       }
       if(OSAL_u32MemPoolFixSizeOpenOnMem("DevDescPool", &DescMemPoolHandle,(void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetDevDescHandlePool)) == OSAL_ERROR)
       {
          FATAL_M_ASSERT_ALWAYS();										  
       }
       if(OSAL_u32MemPoolFixSizeOpenOnMem("FileDescPool",&FileMemPoolHandle,(void*)((uintptr_t)pOsalResData + pOsalData->u32OffsetFileDescHandlePool)) == OSAL_ERROR)
       {
          FATAL_M_ASSERT_ALWAYS();										  
       }
   }	   

 //  phMQCbPrc  = (OSAL_tMQueueHandle*)malloc(pOsalData->u32MaxNrProcElements*sizeof(OSAL_tMQueueHandle));
}

#ifdef AREA_TEST
void vCheckAreas(void)
{
   tU32* pu32Temp; 
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetEventElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetThreadElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetRbTreeDataEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetRbTreeDataEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  = (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetNodeElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetNodeElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSharedMemElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetSharedMemElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemaphoreElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetSemaphoreElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMutexElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetMutexElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetTimerElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetTimerElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetMqElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetStdMqElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetStdMqElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetLockElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetLockElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetThreadLockEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetThreadLockEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetSemHandleElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetSemHandleElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetMqHandleElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetMqHandleElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetEventHandleElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetEventHandleElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetDevDescHandleElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetDevDescHandleElementsEnd overwritten 0x0%x",*pu32Temp);
   pu32Temp  =  (tU32*)((uintptr_t)pOsalResData + pOsalData->u32OffsetFileDescHandleElementsEnd);  
   if(*pu32Temp != 0xdeadbeef)TraceString("u32OffsetFileDescHandleElementsEnd overwritten 0x0%x",*pu32Temp);
}
#endif

void TraceHexDumpOfBuffer(char cBuffer, unsigned len)
{
   char cDest[260 * 3] = {0};
   char Destination[4] = {0,0,0,0};
   tU32 i=0,j=0;
   if(len >100)len=100; 
   for(i = 0;i < len ;i++)
   {
      sprintf(Destination,"%02x",(unsigned int)(cBuffer+i));  
      cDest[j] = Destination[0];
      j++;
      cDest[j] = Destination[1];
      j++;
      cDest[j] = ' ';
      j++;
   }
   TraceString(cDest);
}



void vCreateHelperTask(void)
{
   OSAL_trThreadAttribute  attr;
   /* Term Task call PRM , so vInitOsalIO should run before */
   attr.szName = (tString)"Term";/*lint !e1773 */  /*otherwise linker warning */
   attr.u32Priority = OSALCORE_C_U32_PRIORITY_2DI;
   attr.s32StackSize = minStackSize;
   attr.pfEntry = (OSAL_tpfThreadEntry)vDebugFacility;
   attr.pvArg = NULL;
   if(u32PrcExitStep == 0)
   {
      pOsalData->ThreadIDTerm = OSAL_ThreadSpawn(&attr);
      pOsalData->PidTerm = OSAL_ProcessWhoAmI();
   }
}

tS32 s32StartCbHdrTask(tS32 s32Index)
{
   OSAL_trThreadAttribute  attr;
   tS32 Tid = -1;
   char cNameBuf[OSAL_C_U32_MAX_NAMELENGTH];

   if(u32PrcExitStep > 0)return OSAL_ERROR;

   /* Changes in this function by Carsten Resch */
   /* 2 problems were identified: */
   /*     1) Sync Semaphore (CB%d_SYNC_SEM) is created after processspawn */
   /*             So the spawned process cannot open that sem */
   /*             new order:                                 */
   /*               1) Create Thread */
   /*		    2) Create Sem*/
   /*		    3) Activate Thread */
   /*     2) Global variable 's32LocalCbHdrTid' was set before the */
   /*           Callback thread was confirmed to be running */
   /*            (Therefore a second thread could call 'osalMQueueNotify' */
   /*            But the callbackhandler MQ is not yet avalaible             */
   /*        new order:                              */
   /*                  - set 's32LocalCbHdrTid' after the sync sem was obtained */
   if(s32LocalCbHdrTid == -1)
   {
      if(s32Index != OSAL_ERROR)
      {
          intptr_t Idx = (intptr_t)s32Index;
         /* create task for process specific callback handling.
          * this task reads from message queues OSAL_CB_HDR_LI_%d
          * where %d is the index in pOsalData->rProcDat[] */
          snprintf( &cNameBuf[0], sizeof(cNameBuf), "CB_HDR%d", s32Index );
          attr.szName = (tString)cNameBuf;
          attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
          attr.s32StackSize = minStackSize;
          attr.pfEntry = (OSAL_tpfThreadEntry)vNewCallbackHandler;
          attr.pvArg = (void*) Idx;
          if((Tid = OSAL_ThreadCreate(&attr)) == OSAL_ERROR)
          {
             /* check for race conditions */
             if(OSAL_u32ErrorCode() != OSAL_E_ALREADYEXISTS)
             {
                NORMAL_M_ASSERT_ALWAYS();
             }
             while(s32LocalCbHdrTid == -1)
             {
                OSAL_s32ThreadWait(100);
             }
         }
         else
         {
            /* wait for Trigger task is ready */
            OSAL_tSemHandle hSem = 0;
            snprintf(cNameBuf,OSAL_C_U32_MAX_NAMELENGTH,"CB%d_SYNC_SEM",Tid);
            if(OSAL_OK == OSAL_s32SemaphoreCreate(cNameBuf,&hSem,0 ))
            {
               if(OSAL_s32ThreadActivate(Tid) != OSAL_ERROR)
               {
                  if(OSAL_s32SemaphoreWait(hSem,OSAL_C_TIMEOUT_FOREVER)== OSAL_ERROR)
                  {
                     NORMAL_M_ASSERT_ALWAYS();
                  }
               }
               else
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
               OSAL_s32SemaphoreClose(hSem);
               OSAL_s32SemaphoreDelete(cNameBuf);
            }
            else
            {
               if(OSAL_s32ThreadActivate(Tid) == OSAL_ERROR)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            s32LocalCbHdrTid = Tid;
         }
      }
      else
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   return Tid;
}

void InitOsalServices(void)
{

    /* start term task */
   vCreateHelperTask();

   /* load  and instantiate PRM*/
   OSAL_tIODescriptor d;
   tS32 s32Val = 0;
   d = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE );
   if (d != OSAL_ERROR)
   {
      /*dummy access to force prm_init */
      (void)OSAL_s32IOControl(d, OSAL_C_S32_IOCTRL_GET_EXCLUSIVE_ACCESS,(uintptr_t)&s32Val);
      OSAL_s32IOClose(d);
   }

   /* trace does not set explicit priority, inherit from setting in addProcessEntry 
      here callback registrion to trace is done. This has to be done after Signal handling 
      is prepared*/
   vInitTrace();
 
   vSignalHandlerInstall();

}

int ChangeSemRights(tCString SemString, tCString string)
{
     if(s32OsalGroupId)
    {
          if(chown(SemString,(uid_t)-1,s32OsalGroupId) == -1)
         {
            vWritePrintfErrmem("%s  -> chown error %d \n",string,errno);
         }
         if(chmod(SemString, OSAL_ACCESS_RIGTHS) == -1)
         {
           vWritePrintfErrmem("%s  -> chmod error %d \n",string,errno);
         }
         return 0;
     }
     else
     {
        return 0;
     }
 }
 
void vSetOsalMemProtection(void)
{
#ifdef PROTECTED_OSAL_AREAS
   int pagesize = sysconf(_SC_PAGE_SIZE);
   if(pagesize != PROTECTED_PAGE_SIZE)
   {
      TraceString("mprotect is configured with wrong pagesize %d instead of %d",PROTECTED_PAGE_SIZE,pagesize);
   }
   else
   {
      if(mprotect(pOsalData->OsalProtAreaStart,PROTECTED_PAGE_SIZE, PROT_NONE) == -1)
      {
         TraceString("Protection of OsalProtAreaStart 0x%x failed errno:%d ",pOsalData->OsalProtAreaStart,errno);
      }
      if(mprotect(pOsalData->OsalProtAreaMid,PROTECTED_PAGE_SIZE, PROT_NONE) == -1)
      {
         TraceString("Protection of OsalProtAreaMid 0x%x failed errno:%d ",pOsalData->OsalProtAreaMid,errno);
      }
      if(mprotect(pOsalData->OsalProtAreaEnd,PROTECTED_PAGE_SIZE, PROT_NONE) == -1)
      {
         TraceString("Protection of OsalProtAreaEnd 0x%x failed errno:%d ",pOsalData->OsalProtAreaEnd,errno);
      }
   }
#endif
}

void vPrintDataTypes(void)
{
   TraceString("tBool   size:%d Bytes",sizeof(tBool));
   TraceString("tU8     size:%d Bytes",sizeof(tU8));
   TraceString("tS8     size:%d Bytes",sizeof(tS8));
   TraceString("tChar   size:%d Bytes",sizeof(tChar));
   TraceString("tU16    size:%d Bytes",sizeof(tU16));
   TraceString("tS16    size:%d Bytes",sizeof(tS16));
   TraceString("tUShort size:%d Bytes",sizeof(tUShort));
   TraceString("tShort  size:%d Bytes",sizeof(tShort));
   TraceString("tUInt   size:%d Bytes",sizeof(tUInt));
   TraceString("tInt    size:%d Bytes",sizeof(tInt));
   TraceString("tULong  size:%d Bytes",sizeof(tULong));
   TraceString("tLong   size:%d Bytes",sizeof(tLong));
   TraceString("tFloat  size:%d Bytes",sizeof(tFloat));
   TraceString("tDouble size:%d Bytes",sizeof(tDouble));
   TraceString("tLDouble size:%d Bytes",sizeof(tLDouble));
   TraceString("tSize   size:%d Bytes",sizeof(tSize));
//   TraceString("tUBitfield size:%d Bytes",sizeof(tUBitfield));
//   TraceString("tBitfield  size:%d Bytes",sizeof(tBitfield));
   TraceString("tF32    size:%d Bytes",sizeof(tF32));
   TraceString("tS32    size:%d Bytes",sizeof(tS32));
   TraceString("tU32    size:%d Bytes",sizeof(tU32));
   TraceString("tF64    size:%d Bytes",sizeof(tF64));
   TraceString("tS64    size:%d Bytes",sizeof(tS64));
}

void vPrintResources(void)
{
   TraceString("%d Events configured",pOsalData->u32MaxNrEventElements);
   TraceString("%d Threads configured",pOsalData->u32MaxNrThreadElements);
   TraceString("%d SharedMemories configured",pOsalData->u32MaxNrSharedMemElements);
   TraceString("%d Semaphores configured",pOsalData->u32MaxNrSemaphoreElements);
   TraceString("%d Mutexes configured",pOsalData->u32MaxNrMutexElements);
   TraceString("%d Timer configured",pOsalData->u32MaxNrTimerElements);
   TraceString("%d Message Queues configured",pOsalData->u32MaxNrMqElements); 
   TraceString("%d Processes configured",pOsalData->u32MaxNrProcElements);
   TraceString("%d Internal Locks configured",pOsalData->u32MaxNrLockElements); 
   
   
   TraceString("%d Processes configured",pOsalData->u32MaxNrProcElements);
   
   TraceString("Memory for 1 Process:      %d Bytes",sizeof(trProcessInfo));
   TraceString("Memory for 1 Thread:       %d Bytes",sizeof(trThreadElement));
   TraceString("Memory for 1 Event:        %d Bytes",sizeof(trEventElement));
   TraceString("Memory for 1 Semphore:     %d Bytes",sizeof(trSemaphoreElement));
   TraceString("Memory for 1 Mutex:        %d Bytes",sizeof(trMutexElement));
   TraceString("Memory for 1 Timer:        %d Bytes",sizeof(trTimerElement));
   TraceString("Memory for 1 Shared Memory:%d Bytes",sizeof(trSharedMemoryElement));
   TraceString("Memory for 1 Message Queue:%d Bytes",sizeof(trMqueueElement)+sizeof(trStdMsgQueue));
 
   TraceString("Memory for 1 Message Queue Handle:    %d Bytes %d Handles configured",MQHANDLE_BLOCKSIZE,pOsalData->u32MqHandles);
   TraceString("Memory for 1 Event Handle:            %d Bytes %d Handles configured",EVDAT_BLOCKSIZE,pOsalData->u32EventHandles);
   TraceString("Memory for 1 Semaphore Handle:        %d Bytes %d Handles configured",SEMDAT_BLOCKSIZE,pOsalData->u32SemHandles);
   TraceString("Memory for 1 Device Descriptor Handle:%d Bytes %d Handles configured",DEVDECS_BLOCKSIZE,pOsalData->u32DeviceHandles);
   TraceString("Memory for 1 File Descriptor Handle:  %d Bytes %d Handles configured",FILEDECS_BLOCKSIZE,pOsalData->u32FileHandles);
 
   TraceString("Memory for OSAL Shared Memory %d Bytes",sizeof(trGlobalOsalData)
#ifdef PROTECTED_OSAL_AREAS
                - (4*PROTECTED_PAGE_SIZE)
#endif
				);
   TraceString("Memory for OSAL Configurable Resources Shared Memory %d Bytes",pOsalData->u32OsalResMemSize);
   TraceString("Memory for OSAL Configurable Registy Shared Memory %d Bytes",pOsalData->u32RegistryMemSize);
   TraceString("Memory for OSAL Configurable CCA Message Pool Shared Memory %d Bytes",pOsalData->u32MsgPoolSize);
}

void vProtectData(void)
{
#ifdef PROTECTED_OSAL_AREAS
      /* protect OSAL data which are set only once at first startup */
      int pagesize = sysconf(_SC_PAGE_SIZE);
      if(pagesize != PROTECTED_PAGE_SIZE)
      {
         TraceString("mprotect is configured with wrong pagesize %d instead of %d",PROTECTED_PAGE_SIZE,pagesize);
      }
      else
      { 
	     uintptr_t Size = ((uintptr_t)&pOsalData->u32MagicProtAreaMid + sizeof(tU32)) - (uintptr_t)&pOsalData->u32MagicStart;	
         /* ensure 4kb alignment to make protection possible */	  
         int temp = Size%PROTECTED_PAGE_SIZE;
         if(temp)
         {
            Size = Size + PROTECTED_PAGE_SIZE - temp;
         }		
         /* set data which never changed again readonly*/		 
         if(mprotect(&pOsalData->u32MagicStart ,Size, PROT_READ) == -1)
         {
            TraceString("Protection of pOsalData->u32MagicStart 0x%x failed errno:%d ",&pOsalData->u32MagicStart,errno);
         }
      }
#endif 
}

tBool __attribute__ ((constructor)) bOnProcessAttach(tVoid)
{
   tBool bRet= FALSE;
   tBool bFirstAttach = TRUE;
#ifdef MEASURE_OSAL_START		 
   tU32 u32Val = 0;
   u32Val = OSAL_ClockGetElapsedTime();
#endif
   struct group *pOsalGroup = getgrnam(OSAL_GROUP_NAME);
   if(pOsalGroup==NULL)
   {
    //  vWritePrintfErrmem("OSAL getgrnam -> error %d \n",errno);
   }
   else
   {
      s32OsalGroupId = pOsalGroup->gr_gid;
   }

   /* for running temporary multiple test processes there is a gap related to loading semaphore
      when processes are started new when last OSAL process is shutting down. Therefore an
      additional semaphore is added which will remain in the system for the complete power cycle */
   while(1)
   {
      initsync_lock = sem_open(OSAL_NAME_SYNCLOCK,O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
      if (initsync_lock == SEM_FAILED)
      {
         if(errno == EEXIST)
         {
            initsync_lock = sem_open(OSAL_NAME_SYNCLOCK,0);
		    u32SemWait(initsync_lock);
            break;
         }
         else if(errno == EINTR)
         {
            /* try again */
         }
         else
         {
            vWritePrintfErrmem("Attach -> PID:%d sem_open failed error %d \n",getpid(),errno);
            eh_reboot();
         }
      }
      else
      {
         if(ChangeSemRights("/dev/shm/sem."OSAL_NAME_SYNCLOCK,"Attach") == -1)
         {
         }
         break;
      } 
   }
   /*
    * protect against initializing race, the second process has to wait until this is finished
    */
   while(1)
   {
       init_lock = sem_open(OSAL_NAME_LOCK,O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
       if (init_lock != SEM_FAILED)
       {
           /* first process created semaphore */
           break;
       }
       else if(errno == EEXIST)
       {
           /* following process check for available semaphore*/
           break;
       }
       else if(errno == EINTR)
       {
           /* try again */
       }
       else
       {
          vWritePrintfErrmem("Attach -> PID:%d sem_open failed error %d \n",getpid(),errno);
          eh_reboot();
       }
   }
   if (init_lock != SEM_FAILED)
   {
      /* first process attached to OSAL */
      bFirstAttach = TRUE;
      /* adapt rights for shared memory semaphore */
      if(ChangeSemRights("/dev/shm/sem."OSAL_NAME_LOCK,"Attach") == -1)
      {
      }
      /* create shared memory at first process attach */
      hLiShMem = shm_open(OSAL_NAME_SHMEM, O_EXCL|O_RDWR|O_CREAT, OSAL_ACCESS_RIGTHS);
      if(hLiShMem  == -1)
      {
         vWritePrintfErrmem("OSAL Attach shm_open failed error %d \n",errno);
         eh_reboot();
      }
      if(s32OsalGroupId)
      {
         /* adapt rights for shared memory */
         if(fchown(hLiShMem,(uid_t)-1,s32OsalGroupId) == -1)
         {
            vWritePrintfErrmem("Attach shm_open -> fchown error %d \n",errno);
         }
         // umask (022) is overwriting the permissions on creation, so we have to set it again
         if(fchmod(hLiShMem, OSAL_ACCESS_RIGTHS) == -1)
         {
            vWritePrintfErrmem("Attach shm_open -> fchmod error %d \n",errno);
         }
      }
      if (ftruncate(hLiShMem, sizeof(trGlobalOsalData)) == -1)
      {
         vWritePrintfErrmem("OSAL Attach shm_open failed error %d \n",errno);
         eh_reboot();
      }

      /* make directory in volatile file system for OSAL informations */
  /*    if(mkdir(VOLATILE_DIR"/OSAL",S_IFDIR | OSAL_ACCESS_RIGTHS) == -1)
      {
         if(errno != EEXIST)
         {
            vWritePrintfErrmem("Attach  -> mkdir(VOLATILE_DIR/OSAL failed \n");
         }
      }
      else
      {
         if(chmod(VOLATILE_DIR"/OSAL", OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {
             vWritePrintfErrmem("Attach -> /run/OSAL chmod error %d \n",errno);
         }
      }*/
   }
   else
   {
      while(1)
      {
         init_lock = sem_open(OSAL_NAME_LOCK, 0);
         if(init_lock != SEM_FAILED)
         {
             break;
         }
         else if(errno != EINTR)
         {
            vWritePrintfErrmem("Attach -> PID:%d sem_open failed error %d \n",getpid(),errno);
            eh_reboot();
         }
      }
      bFirstAttach = FALSE;
      if(init_lock != SEM_FAILED)
      {
         if(u32SemWait(init_lock) != OSAL_E_NOERROR)
         {
            vWritePrintfErrmem("OSAL Attach sem_wait failed error %d \n",errno);
            eh_reboot();
         }
      }
      else
      {
          vWritePrintfErrmem("Attach -> sem_open failed error %d \n",errno);
      }

      hLiShMem = shm_open(OSAL_NAME_SHMEM, O_RDWR ,0);
      if(hLiShMem  == -1)
      {
          vWritePrintfErrmem("Attach -> shm_open failed error %d \n",errno);
          eh_reboot();
      }
   }

   /* map shared memory into address space */
   pOsalData = (trGlobalOsalData*)mmap( pOsalDataAddress,
                                        sizeof(trGlobalOsalData),
                                        PROT_READ | PROT_WRITE,
                                        GLOBAL_DATA_OPTION,
                                        hLiShMem,
                                        0);
   if (pOsalData != MAP_FAILED)
   {
      if(bFirstAttach)
      {
        InitSignalFifo();
        /* the first process using OSAL shared lib runs here 
         create prepare all OSAL internal data structure */
         vInitGlobalData();
      }
      /* setup OSAL signal handling -> access to pOsalData special configuration does not work for the first process */
      init_exception_handler(NULL);
      /* set thread specific mask for signal handling -> access to pOsalData special configuration does not work for the first process */
      vSetThreadSignalMask();
      /* activate configured guard section in OSAL shared memory -> access to pOsalData*/
      vSetOsalMemProtection();
      bRet = TRUE;
   }
   else
   {
      FATAL_M_ASSERT_ALWAYS();/* reset the system via assert */
      bRet = FALSE;
   }

   /* set function pointer for basic driver in OSAL dispatcher*/
   vSetBasicFuncPointer();

   if(bRet == TRUE)
   {
      if(bFirstAttach)
      {
#ifdef MEASURE_OSAL_START		 
         pOsalData->u32OsalStartTimeInMs = u32Val;
#endif
         /* create handle to OSAL internal Locks */
         vGetOsalLock(TRUE);
         /* setpup registry device */
         if(s32RegistryInit() != OSAL_OK)
         {
             TraceString("OSAL Error: Registry Init failed");
         }
         /* read OSAL internal configuration */
         vSetupRegistry();
          /* build up shared memory for OSAL resources */
         vConnectOsalResMemory(TRUE);
         /* create handles of generic synchronization objects -> Access to OSAL resource Memory*/
         vGenerateSyncObjects();
         /* check numebr of Cores to use ADIT exception handler for callstack generation */
         vGetNrOfCpuCores();
         /* set configured exception handler status */
         s32ExcHdrStatus = pOsalData->u32ExcStatus;
         /*enter process data in OSAL data starukutre and prepare signal handling 
         here some registry values are needed for special configuration */
         processIndex = tProcessTableGetFreeIndex();
         vAddProcessEntry(processIndex);
         /* prepare OSAL message pool for CCA communication, this also done by the CCA framework,
            but we will ensure to get the memory for this central pool */
         //(void)OSAL_s32MessagePoolCreate(pOsalData->u32MsgPoolSize);
/*         getrlimit(RLIMIT_SIGPENDING,&limit);
         TraceString("OSAL MAX_SIGPENDING %d",limit.rlim_max);*/
#ifdef MEASURE_OSAL_START		 
         pOsalData->u32OsalStartTime2InMs = OSAL_ClockGetElapsedTime();
         TraceString("OSAL Start after %d msec Ready after %d msec",pOsalData->u32OsalStartTimeInMs,pOsalData->u32OsalStartTime2InMs);//35msec
#endif
         vProtectData();
#ifdef PRINT_DATATYPES
         vPrintDataTypes();
         vPrintResources();
#endif
      }
      else
      {
         vProtectData();
         /* connect shared memory for OSAL resources */
         vConnectOsalResMemory(FALSE);
         processIndex = tProcessTableGetFreeIndex();
         if (processIndex >= 0)
         {
            InitSignalFifo();
            /* get handle to OSAL internal Locks */
            vGetOsalLock(FALSE);
            /* set configured exception handler status */
            if(s32ExcHdrStatus == 0)s32ExcHdrStatus = pOsalData->u32ExcStatus;
            /* Enter process specific data */
            vAddProcessEntry(processIndex);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
         }
      }                             

#ifdef VIRTMEM_LIMIT
struct rlimit limit;
      limit.rlim_cur = VIRTMEM_LIMIT ;
      limit.rlim_max = VIRTMEM_LIMIT;
      setrlimit(RLIMIT_AS,&limit);
      fprintf(stderr, "Memory Limit set to %d bytes\n",VIRTMEM_LIMIT); fflush(stderr);
      fprintf(stderr, "Process %d started \n",getpid()); fflush(stderr);
#endif
	  
      /* adapt variable pathes for OSAL file system devices */
      vPrepareOsalFileSystemDevices();
  
      pOsalData->u32AttachedProcesses++;
     // TraceString("OSAL Constructor called %d",pOsalData->u32AttachedProcesses);
      pOsalData->u32TskResCount++;
      if(pOsalData->u32MaxTskResCount < pOsalData->u32TskResCount)
      {
         pOsalData->u32MaxTskResCount = pOsalData->u32TskResCount;
      }
    
      /*
       * init finished, let go!
       */
       sem_post(init_lock);
       sem_post(initsync_lock);
  }
   else
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   return bRet;
}


void vOnProcessDetach( void )
{
   TraceString("OSAL %s Destructor called %d Processes active",process_name,pOsalData->u32AttachedProcesses);

   u32SemWait(initsync_lock);
   u32SemWait(init_lock);
   pOsalData->u32AttachedProcesses--;
   pOsalData->u32TskResCount--;

   /* disconnect from CCA message pool */
   if(MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE)
   {
      OSAL_s32MessagePoolClose();
   }
   (void)s32RegistryDeInit();

   if(pOsalData->u32AttachedProcesses == 0)
   {
      if(pOsalData->rMsgPoolStruct.bCreated)
      {
          OSAL_s32MessagePoolDelete();
      }
      (void)s32RegistryMemUnlink();
   }

   vCleanUpMqofContext();
   vCleanUpEventofContext();
   vCleanUpSemaphoreofContext();
   vCleanUpShMemofContext();

   close(hLiShMem);
   close(hLiResShMem);


   if(pOsalData->u32AttachedProcesses == 0)
   {
      if (shm_unlink(OSAL_NAME_SHMEM))
      { TraceString("shm_unlink(%s)",OSAL_NAME_SHMEM);   }
      if (shm_unlink(OSAL_NAME_RES_SHMEM))
      { TraceString("shm_unlink(%s)",OSAL_NAME_RES_SHMEM);   }

      vDestroySyncObjects();

      s32DestroyOsalLock("SYNC_LOCK");
      s32DestroyOsalLock("EV__LOCK");
#ifdef SIGNAL_EV_HDR
      s32DestroyOsalLock("EV_PRCLOCK");
#endif
      s32DestroyOsalLock("SEM_LOCK");
      s32DestroyOsalLock("TIM_LOCK");
      s32DestroyOsalLock("MQ__LOCK");
      s32DestroyOsalLock("SHM_LOCK");
      s32DestroyOsalLock("DevTable");
      s32DestroyOsalLock("Dispatch");
      s32DestroyOsalLock("PRMTABLE");
      s32DestroyOsalLock("RegObjLock");
      vUnregisterSysCallback();
      vUnregisterOsalIO_Callback();
      LLD_vCloseTrace();
   
      char cBuffer[32];
      /* use for create and open always a lock */
      snprintf(cBuffer,32,"/%s","DevDescPool");
      sem_unlink(cBuffer);
      snprintf(cBuffer,32,"/%s","FileDescPool");
      sem_unlink(cBuffer);
      snprintf(cBuffer,32,"/%s","EventHandlePool");
      sem_unlink(cBuffer);
      snprintf(cBuffer,32,"/%s","MqHandlePool");
      sem_unlink(cBuffer);
      snprintf(cBuffer,32,"/%s","SemHandlePool");
      sem_unlink(cBuffer);

      exit_exception_handler();
      sem_post(init_lock);
      sem_close(init_lock);
      sem_unlink(OSAL_NAME_LOCK);
   }
   else
   {
      exit_exception_handler();
      sem_post(init_lock);
      sem_close(init_lock);
   }
   sem_post(initsync_lock); 
   sem_close(initsync_lock); 
}

#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/

