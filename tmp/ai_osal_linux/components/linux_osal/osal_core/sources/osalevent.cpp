/*****************************************************************************
| FILE:         osalevent.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Event-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#ifdef SEM_EV

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define  LINUX_C_U32_EVENT_ID                    ((tU32)0x45564E54)

#ifdef OSAL_SHM_MQ
#define  LINUX_C_EVENT_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH + 2)
#else
#define  LINUX_C_EVENT_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH )
#endif

// If this define is set, a event waiting task is activated, if the event is closed and deleted.
// The waiting task return from OSAL_s32EventWait with OSAL_ERROR. The errorcode is OSAL_E_TIMEOUT.
#define OSAL_AVOID_ZOMBIE_THREADS


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static tCString FlagName[4] = {"AND", "OR", "XOR","REPLACE"};

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

tS32  s32EventTableCreate(tVoid);
tS32  s32EventTableDeleteEntries(void);

static trEventElement* tEventTableGetFreeEntry(tVoid);

static trEventElement* tEventTableSearchEntryByName(tCString coszName);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableCreate
*
* DESCRIPTION: This function creates the Event Table List. If
*              there isn't space it returns a error code.
*
* PARAMETER:   none
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableCreate(tVoid)
{
   tU32 tiIndex;

   pOsalData->EventTable.u32UsedEntries      = 0;
   pOsalData->EventTable.u32MaxEntries       = pOsalData->u32MaxNrEventElements;

   for(tiIndex = 0; tiIndex < pOsalData->EventTable.u32MaxEntries; tiIndex++)
   {
      pEventEl[tiIndex].u32EventID = LINUX_C_U32_EVENT_ID;
      pEventEl[tiIndex].bIsUsed = FALSE;
   }
   return OSAL_OK;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableGetFreeEntry
*
* DESCRIPTION: this function goes through the Event List and returns the
*              first unused EventElement.
*
*
* PARAMETER:   tVoid
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static  trEventElement* tEventTableGetFreeEntry(tVoid)
{
   trEventElement *pCurrent  = pEventEl;
   tU32 used = pOsalData->EventTable.u32UsedEntries;

   if (used < pOsalData->EventTable.u32MaxEntries)
   {
      pCurrent = &pEventEl[used];
      pOsalData->EventTable.u32UsedEntries++;
   } else {
        /* search an entry with !bIsUsed */
        while ( pCurrent < &pEventEl[used]
                && ( pCurrent->bIsUsed == TRUE) )
        {
            pCurrent++;
        }
        if(pCurrent >= &pEventEl[used])
        {
            pCurrent = NULL; /* not found */
        }
   }
   return pCurrent;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableSearchEntryByName
*
* DESCRIPTION: this function goes through the Event List and returns the
*              EventElement with the given name or NULL if all the List has
*              been checked without success.
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static trEventElement *tEventTableSearchEntryByName(tCString coszName)
{
   trEventElement *pCurrent  = pEventEl;
   tU32 used = pOsalData->EventTable.u32MaxEntries;

   /* search an entry with coszName and the current pid */
   while ( pCurrent < &pEventEl[used]
         && ( pCurrent->bIsUsed == FALSE
      ||  (OSAL_s32StringCompare(coszName, pCurrent->szName))) )
   {
      pCurrent++;
   }
   if(pCurrent >= &pEventEl[used])
   {
      pCurrent = NULL;
   }
   return pCurrent;
}

/*****************************************************************************
*
* FUNCTION:    bEventTableDeleteEntryByName
*
* DESCRIPTION: this function goes through the Event List deletes the
*              EventElement with the given name
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************
static tBool bEventTableDeleteEntryByName(tCString coszName)
{
   trEventElement *pCurrent = EventTable.ptrHeader;

   while( (pCurrent!= OSAL_NULL)
       && (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0) )
   {
      pCurrent = pCurrent->pNext;
   }
   if(pCurrent != OSAL_NULL)
   {
      if(pCurrent->bIsUsed == FALSE )
     {
           pCurrent->bIsUsed = 0;
           pCurrent->bToDelete = 0;
           pCurrent->u16ContextID = 0;
           pCurrent->u16OpenCounter = 0;
           pCurrent->u32mask = 0;
           memset(&pCurrent->rEventGroup,0,sizeof(T_CFLG));
           memset(pCurrent->szName,0,LINUX_C_EVENT_MAX_NAMELENGHT);
           pCurrent->hLock = 0;
           pCurrent->hLocalLock = 0;
           return TRUE;
     }
   }

   return FALSE;
}*/

/*****************************************************************************
*
* FUNCTION:    bCleanUpEventofContext
*
* DESCRIPTION: this function goes through the Event List deletes the
*              Event and EventElement with the given context from list
*
* PARAMETER:   tU16 Context ID
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vCleanUpEventofContext(void)
{
   tS32 s32OwnPid = OSAL_ProcessWhoAmI();
   uintptr_t s32EvHandle;
   tS32 s32OpnCount = 0;
   tU32 i;
   trEventElement *pCurrentEntry = NULL;
   char cName[LINUX_C_EVENT_MAX_NAMELENGHT] = {0};
   for(i=0;i<pOsalData->u32EventHandles;i++)
   {
      s32EvHandle = (uintptr_t)pvGetFirstElementForPid(&EvMemPoolHandle,(tU32)s32OwnPid);
      if(s32EvHandle)
      {
         if(((tEvHandle*)s32EvHandle)->s32Tid > 0)
         {
             /* cleanup event */
             pCurrentEntry = ((tEvHandle*)s32EvHandle)->pEntry; /*lint !e613 *//* pointer already checked*/ 
             if(cName[0] == 0)
             {
                 strncpy(cName,pCurrentEntry->szName,LINUX_C_EVENT_MAX_NAMELENGHT);
             }
             s32OpnCount =  pCurrentEntry->u16OpenCounter;
             TraceString("OSAL_s32EventClose for %s",pCurrentEntry->szName);
             if(OSAL_s32EventClose((OSAL_tEventHandle)s32EvHandle) == OSAL_ERROR)
             {
                break;
             }
             if((s32OpnCount-1) == 0)
             {
                   OSAL_s32EventDelete(cName);
             }
         }
         else
         {
             /* defect handle in pool */
             TraceString(" Event pvGetFirstElementForPid gives handle without TID");
             break;
         }
      }
      else
      {
          /* last handle for PID was found */
          break;
      }
   }
}


/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableDeleteEntries
*
* DESCRIPTION: This function deletes all elements from OSAL table,
*
* PARAMETER:
*   u16ContextID                  index to distinguish caller context
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableDeleteEntries(void)
{
   trEventElement *pCurrentEntry = &pEventEl[0];
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;
   tU32 used = pOsalData->EventTable.u32UsedEntries;


   if(LockOsal(&pOsalData->EventTable.rLock) == OSAL_OK)
   {
      /* search the whole table */
      while ( pCurrentEntry < &pEventEl[used])
      {
         if (pCurrentEntry->bIsUsed == TRUE)
         {
               s32DeleteCounter++;
               if (DelSyncObj(pCurrentEntry->hLock) == OSAL_E_NOERROR)
               {
                  pCurrentEntry->bIsUsed = FALSE;
                  s32ReturnValue = s32DeleteCounter;
               }
               else
               {
                  s32ReturnValue = OSAL_ERROR;
                  break;
               }
         }
         else
         {
            pCurrentEntry++;
         }
      }

      UnLockOsal(&pOsalData->EventTable.rLock);
   }
   return(s32ReturnValue);
}

void vTraceEventInfo(trEventElement *pCurrentEntry,tU8 Type,tBool bErrMem,tU32 u32Error,tCString coszName)
{
#ifdef SHORT_TRACE_OUTPUT
    char au8Buf[26 + LINUX_C_EVENT_MAX_NAMELENGHT +8];
    tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
    tS32 s32Len;

    if((Type == POST_OPERATION)||(Type == WAIT_OPERATION))
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO2);
    }
    else
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO);
    }
    OSAL_M_INSERT_T8(&au8Buf[1],(tU8)Type);
    bGetThreadNameForTID(&au8Buf[2],8,(int)u32Val);
    OSAL_M_INSERT_T32(&au8Buf[10],u32Val);
 
    if(pCurrentEntry)
    {
        OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry);
        OSAL_M_INSERT_T32(&au8Buf[18],pCurrentEntry->u32EventBitField);
    }
    else
    {
        OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
        OSAL_M_INSERT_T32(&au8Buf[18],u32Val);
    }
        OSAL_M_INSERT_T32(&au8Buf[22],u32Error);

    s32Len = 26;

    if(pCurrentEntry)
    {
       if(Type == POST_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32PostMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32PostFlag);
          s32Len += 8;
       }
       else if(Type == WAIT_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32WaitMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32WaitEnFlags);
          s32Len += 8;
       }
    }
    else
    {
          OSAL_M_INSERT_T32(&au8Buf[26],u32Error);
          OSAL_M_INSERT_T32(&au8Buf[30],u32Error);
    }

    u32Val = 0;
    if(coszName)
    {
      u32Val = strlen(coszName);
      if( u32Val > LINUX_C_EVENT_MAX_NAMELENGHT )
      {
         u32Val = LINUX_C_EVENT_MAX_NAMELENGHT;
      }
      memcpy(&au8Buf[s32Len],coszName,u32Val);
      au8Buf[s32Len + LINUX_C_EVENT_MAX_NAMELENGHT -1] = 0;
    }

    s32Len += u32Val;
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_FATAL,au8Buf,s32Len);
    if((pOsalData->bLogError)&&(bErrMem))
    {
       if(!pCurrentEntry)
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,s32Len,0);
       }
       else
       {
          if(!pCurrentEntry->bTraceFlg)vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,s32Len,0);
       }
    }

#else
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];
   tU32 u32Mask = 0xffffffff;
   tU32 u32Flag = 0xffffffff;
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   au8Buf[0] = OSAL_STRING_OUT;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
   tCString Operation;
   switch(Type)
   {
     case CREATE_OPERATION:
         Operation = "CREATE_OPERATION";
         break;
     case DELETE_OPERATION:
         Operation = "DELETE_OPERATION";
         break;
     case OPEN_OPERATION:
         Operation = "OPEN_OPERATION";
         break;
     case CLOSE_OPERATION:
         Operation = "CLOSE_OPERATION";
         break;
     case WAIT_OPERATION:
         Operation = "WAIT_OPERATION";
          if(pCurrentEntry)
          {
             u32Mask = pCurrentEntry->u32WaitMask;
             u32Flag = pCurrentEntry->u32WaitEnFlags;
          }
         break;
     case POST_OPERATION:
          Operation = "POST_OPERATION";
          if(pCurrentEntry)
          {
             u32Mask = pCurrentEntry->u32PostMask;
             u32Flag = pCurrentEntry->u32PostFlag;
          }
          break;
     case WAIT_OPERATION_STEP:
          Operation = "WAIT_OPERATION_STEP";
          if(pCurrentEntry)
          {
             u32Mask = pCurrentEntry->u32WaitMask;
             u32Flag = pCurrentEntry->u32WaitEnFlags;
          }
         break;
     case STATUS_OPERATION:
          Operation = "STATUS_OPERATION";
         break;
     default:
         Operation = "Unknown";
         break;
   }
   if(coszName == NULL)coszName ="Unknown";
   if(pCurrentEntry)
   {
       if(u32Flag != 0xffffffff)
       {
#ifdef NO_PRIxPTR
          snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Mask:0x%x Flag:%s Name:%s",
                   Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,(unsigned int)u32Mask,FlagName[u32Flag],coszName);
#else
          snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%" PRIxPTR " BitField:0x%x Error:0x%x Mask:0x%x Flag:%s Name:%s",
                   Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,(unsigned int)u32Mask,FlagName[u32Flag],coszName);
#endif
       }
       else
       {
#ifdef NO_PRIxPTR
          snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Name:%s",
                   Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,coszName);
#else
          snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%" PRIxPTR " BitField:0x%x Error:0x%x Name:%s",
                   Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,coszName);
#endif
       }
   }
   else
   {
#ifdef NO_PRIxPTR
      snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
#else
      snprintf(&au8Buf[1],250,"OSALEVENT %s Task:%s(%d) Handle:0x%" PRIxPTR " BitField:0x%x Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
#endif
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG,TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
#endif
    if((pOsalData->bLogError)&&(bErrMem))
    {
       if(!pCurrentEntry)
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,strlen(&au8Buf[1])+1,0);
       }
       else
       {
          if(!pCurrentEntry->bTraceFlg)vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,strlen(&au8Buf[1])+1,0);
       }
    }
}

tU32 u32GenerateEventHandle(trEventElement *pCurrentEntry,OSAL_tEventHandle* phEvent)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   tEvHandle* pTemp;
   if(EvMemPoolHandle.u32memstart == 0)
   {
      if(OSAL_u32MemPoolFixSizeOpen("EvHandlePool",&EvMemPoolHandle) == OSAL_ERROR)
      {
         FATAL_M_ASSERT_ALWAYS();
      }
   }
   pTemp = (tEvHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&EvMemPoolHandle);
   if(pTemp)
   {
      pTemp->pEntry   = pCurrentEntry;
      pTemp->s32Tid   = OSAL_ThreadWhoAmI();
      *phEvent = (OSAL_tEventHandle)pTemp;
   }
   else
   {
      u32ErrorCode = OSAL_E_NOSPACE;
   }
   return u32ErrorCode;
}

/*****************************************************************************
*
* FUNCTION: OSAL_s32EventCreate
*
* DESCRIPTION: this function creates an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to create.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventCreateOpt(tCString coszName,
                            OSAL_tEventHandle* phEvent,
                            tU32 u32Option)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   /* The name is not NULL */
   if (coszName && phEvent) /* FIX 15/10/2002 */
   {
      /* The Name is not too long */
      if( OSAL_u32StringLength( coszName ) <= (tU32)(LINUX_C_EVENT_MAX_NAMELENGHT-1) )
      {
         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            /* The name is not already in use*/
            pCurrentEntry = tEventTableSearchEntryByName( coszName );
            if(pCurrentEntry == OSAL_NULL)
            {
               pCurrentEntry = tEventTableGetFreeEntry();
               if (pCurrentEntry!= OSAL_NULL)
               {
	          if(pCurrentEntry->u32EventID != LINUX_C_U32_EVENT_ID)
		  {
                      TraceString("OSALEVENT OSAL_s32EventCreate LINUX_C_U32_EVENT_ID overwriten 0x%x",pCurrentEntry->u32EventID );
		  }

                  /* Create Event */
                  if( (pCurrentEntry->hLock = CreSyncObj(coszName, 0) ) != (tU32)OSAL_ERROR )
                  {
                     /* success */
                     pCurrentEntry->bIsUsed   = TRUE;
                     pCurrentEntry->bToDelete = FALSE;
                     pCurrentEntry->u16OpenCounter   = 1;
                     pCurrentEntry->u32WaitMask      = 0;
                     pCurrentEntry->u32WaitEnFlags   = OSAL_EN_EVENTMASK_REPLACE+1;
                     pCurrentEntry->u32EventBitField = 0;
                     pCurrentEntry->bWaitFlag = FALSE;
                     pCurrentEntry->bTraceFlg = pOsalData->bEvTaceAll;
                     pCurrentEntry->s32PID    = OSAL_ProcessWhoAmI();
                     pCurrentEntry->u32Option = u32Option;
                     if(pOsalData->szEventName[0] != 0)
                     {
                        if(!strncmp(coszName,pOsalData->szEventName,strlen(pOsalData->szEventName)))
                        {
                           pCurrentEntry->bTraceFlg = TRUE;
                        }
                     }
                     (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                               coszName, LINUX_C_EVENT_MAX_NAMELENGHT );
                     char Buffer[64];
                     snprintf(Buffer,64,"%s_Local",coszName);
                     if((pCurrentEntry->hLocalLock = (tU32)CreSyncObj(Buffer, 1)) == (tU32)OSAL_ERROR )
                     {
                         FATAL_M_ASSERT_ALWAYS();
                     }
                     if(u32GenerateEventHandle(pCurrentEntry,phEvent) != OSAL_E_NOERROR)
                     {
                         s32ReturnValue = OSAL_ERROR;
                     }
                     else
                     {
                         s32ReturnValue = OSAL_OK;
                     }
                     pOsalData->u32EvtResCount++;
                     if(pOsalData->u32MaxEvtResCount < pOsalData->u32EvtResCount)
                     {
                         pOsalData->u32MaxEvtResCount = pOsalData->u32EvtResCount;
                         if(pOsalData->u32MaxEvtResCount > (pOsalData->u32MaxNrEventElements*9/10))
                         {
                            pOsalData->u32NrOfChanges++;
                         }
                     }
                  }
                  else
                  {
                    FATAL_M_ASSERT_ALWAYS();
                    u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }
             UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode == OSAL_E_ALREADYEXISTS)
      {
         if((pCurrentEntry&&(pCurrentEntry->bTraceFlg == TRUE))||(u32OsalSTrace & 0x00000080))
         {
           vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
         }
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TRUE,u32ErrorCode,coszName);
      }
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceFlg == TRUE)||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return( s32ReturnValue );
}
tS32 OSAL_s32EventCreate( tCString coszName,
                          OSAL_tEventHandle* phEvent )
{
   return OSAL_s32EventCreateOpt(coszName,phEvent,0);
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventDelete
*
* DESCRIPTION: this function removes an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventDelete(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   if (coszName)
   {
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
         pCurrentEntry = tEventTableSearchEntryByName( coszName );
         if( pCurrentEntry != OSAL_NULL )
         {
            if(pCurrentEntry->u32EventID != LINUX_C_U32_EVENT_ID)
            {
               TraceString("OSALEVENT OSAL_s32EventDelete LINUX_C_U32_EVENT_ID overwriten 0x%x",pCurrentEntry->u32EventID );
            }
            if( pCurrentEntry->u16OpenCounter == 0 )
            {
               if((pCurrentEntry->bWaitFlag == TRUE)&&(pCurrentEntry->u32WaitEnFlags != (OSAL_EN_EVENTMASK_REPLACE+1)))
               {
                   OSAL_s32ThreadWait(0);
                   if( pCurrentEntry->bWaitFlag == TRUE )
                   {
                      /* we will delete an event which is currently in use */
                      NORMAL_M_ASSERT_ALWAYS();
                   }
               }
               if((u32ErrorCode = DelSyncObj(pCurrentEntry->hLock)) == OSAL_E_NOERROR)
               {
                  if((u32ErrorCode = DelSyncObj(pCurrentEntry->hLocalLock))!= OSAL_E_NOERROR)
                  {
                      NORMAL_M_ASSERT_ALWAYS();
                  }
                  memset(pCurrentEntry,0,sizeof(trEventElement));
                  pCurrentEntry->u32EventID = LINUX_C_U32_EVENT_ID;
                  //pCurrentEntry->bIsUsed = FALSE;
                  s32ReturnValue = OSAL_OK;
               }
               else
               {
                  u32ErrorCode   = OSAL_E_BUSY;    // return with OSAL_ERROR and E_BUSY. todo correct?
                  pCurrentEntry->bToDelete = TRUE; // can not be deleted, mark as toDelete

                 #ifdef OSAL_AVOID_ZOMBIE_THREADS
                  if( pCurrentEntry->bWaitFlag != 0 )
                  {
                     // last close and a delete called, but a thread is still waiting!
                     RelSyncObj(pCurrentEntry->hLock);  // trigger thread to release the sem, return with OSAL_OK
                     NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
                     /* give waiting task a chance to process */
                     OSAL_s32ThreadWait(100);
                     if((u32ErrorCode = DelSyncObj(pCurrentEntry->hLock)) == OSAL_E_NOERROR)
                     {
                        if((u32ErrorCode = DelSyncObj(pCurrentEntry->hLocalLock))!= OSAL_E_NOERROR)
                        {
                           NORMAL_M_ASSERT_ALWAYS();
                        }
                        pCurrentEntry->bIsUsed = FALSE;
                        s32ReturnValue = OSAL_OK;
                     }
                     else
                     {
                        NORMAL_M_ASSERT_ALWAYS();
                     }
                  }
                 #endif
               }
               pOsalData->u32EvtResCount--;
            }
            else
            {
               pCurrentEntry->bToDelete = TRUE;
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
           u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->EventTable.rLock);
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceFlg == TRUE)||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventOpen
*
* DESCRIPTION: this function returns a valid handle to an OSAL event
*              already created.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventOpen(tCString coszName, OSAL_tEventHandle* phEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   if( coszName && phEvent )
   {
      if(OSAL_u32StringLength(coszName) <= (tU32)(LINUX_C_EVENT_MAX_NAMELENGHT-1))
      {
         /* Lock the event table */
         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            pCurrentEntry = tEventTableSearchEntryByName(coszName);

            if (pCurrentEntry != OSAL_NULL)
            {
               if(pCurrentEntry->u32EventID != LINUX_C_U32_EVENT_ID)
               {
                   TraceString("OSALEVENT OSAL_s32EventOpen LINUX_C_U32_EVENT_ID overwriten 0x%x",pCurrentEntry->u32EventID );
               }
               if( !pCurrentEntry->bToDelete )
               {
                  pCurrentEntry->u16OpenCounter++;
                  if(u32GenerateEventHandle(pCurrentEntry,phEvent) != OSAL_E_NOERROR)
                  {
                      s32ReturnValue = OSAL_ERROR;
                  }
                  else
                  {
                     s32ReturnValue = OSAL_OK;
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_BUSY;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }
            /* unlock the Event table */
            UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
         if((pCurrentEntry&&(pCurrentEntry->bTraceFlg == TRUE))||(u32OsalSTrace & 0x00000080))
         {
           vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,coszName);
         }
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceFlg == TRUE)||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }

   return( s32ReturnValue );
}

static void vPrintEventHandleIssue(OSAL_tEventHandle hEvent)
{
   if(pOsalData->bLogError)
   {
      vWritePrintfErrmem("OSALEVENT memstart:0x%x memend:0x%x Handle:0x%x \n", EvMemPoolHandle.u32memstart,EvMemPoolHandle.u32memend,hEvent);
      NORMAL_M_ASSERT_ALWAYS();
   }
   TraceString("OSALEVENT memstart:0x%x memend:0x%x Handle:0x%x ", EvMemPoolHandle.u32memstart,EvMemPoolHandle.u32memend,hEvent);
}

static tU32 u32CheckEventHandle(OSAL_tEventHandle hEvent)
{
   if((hEvent <= EvMemPoolHandle.u32memstart)||(hEvent >= EvMemPoolHandle.u32memend))
   {
      if(EvMemPoolHandle.pMem)
      {
         trOSAL_MPF* pPtr = (trOSAL_MPF*)EvMemPoolHandle.pMem;
         if(pPtr->u32ErrCnt == 0)
         {
            vPrintEventHandleIssue(hEvent);
            return OSAL_E_INVALIDVALUE;
         }
         else
         {
            /* possible that Handle Memory was allocated via malloc */
            if(( hEvent == 0)||((tU32)hEvent == OSAL_C_INVALID_HANDLE))
            {
               vPrintEventHandleIssue(hEvent);
               return OSAL_E_INVALIDVALUE;
            }
         }
      }
      else
      {
         vPrintEventHandleIssue(hEvent);
         return OSAL_E_INVALIDVALUE;
      }
   }
   return OSAL_E_NOERROR;
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventClose
*
* DESCRIPTION: this function closes an OSAL event.
*
* PARAMETER:   hEvent (I)
*                 event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventClose(OSAL_tEventHandle hEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckEventHandle(hEvent);
   trEventElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      /* Lock the event table */
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
         pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
         if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
            if( pCurrentEntry->u16OpenCounter > 0 )
            {
               pCurrentEntry->u16OpenCounter--;
               s32ReturnValue = OSAL_OK;
               // @@@@ kos2hi: insert POSIX delete mechanism
               if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
               {
                 #ifdef OSAL_AVOID_ZOMBIE_THREADS
                  if((pCurrentEntry->bWaitFlag != 0)&&(pCurrentEntry->u32WaitEnFlags != (OSAL_EN_EVENTMASK_REPLACE+1)))
                  {                                      // last close und a delete called, but a thread is still waiting!
                     RelSyncObj(pCurrentEntry->hLock);  // trigger thread to release the sem, return with OSAL_OK
                     NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
                     /* give waiting task a chance to process */
                     OSAL_s32ThreadWait(100);
                  }
                 // else
                 #endif
                  {
                     if((pCurrentEntry->bWaitFlag == TRUE )&&(pCurrentEntry->u32WaitEnFlags != (OSAL_EN_EVENTMASK_REPLACE+1)))
                     {
                         OSAL_s32ThreadWait(0);
                         if( pCurrentEntry->bWaitFlag == TRUE )
                         {
                            /* we will delete an event which is currently in use */
                            NORMAL_M_ASSERT_ALWAYS();
                         }
                     }
                     if( (u32ErrorCode = DelSyncObj( pCurrentEntry->hLock )) == OSAL_E_NOERROR )
                     {                                      // sem is deleted, return with OSAL_OK
                        if((u32ErrorCode = DelSyncObj(pCurrentEntry->hLocalLock))!= OSAL_E_NOERROR)
                        {
                           NORMAL_M_ASSERT_ALWAYS();
                        }
                        memset(pCurrentEntry,0,sizeof(trEventElement));
                        pCurrentEntry->u32EventID = LINUX_C_U32_EVENT_ID;
//                        pCurrentEntry->bIsUsed = FALSE;
//                       pCurrentEntry->bToDelete = FALSE;
                     }
                     else  // sem could not be deleted internal counter is not zero,
                     {     // sem is still in use and no thread is waiting this is an internal error
                        u32ErrorCode   = OSAL_E_UNKNOWN;
                        s32ReturnValue = OSAL_ERROR;
                        NORMAL_M_ASSERT_ALWAYS();
                     }
                     pOsalData->u32EvtResCount--;
                  }
               }
               ((tEvHandle*)hEvent)->pEntry   = NULL;
               ((tEvHandle*)hEvent)->s32Tid   = 0;
               if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&EvMemPoolHandle,(tEvHandle*)hEvent) == OSAL_ERROR)
               {    NORMAL_M_ASSERT_ALWAYS();  }
               // else: opencount is decremented and not zero, this is ok
            }
            else
            {
               // todo who wants to close the event more than it is opened? normalasssert?
               u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
             if((pCurrentEntry)&&(pCurrentEntry->u32EventID != LINUX_C_U32_EVENT_ID))
             {
                 TraceString("OSALEVENT LINUX_C_U32_EVENT_ID overwriten 0x%x",pCurrentEntry->u32EventID );
             }
             u32ErrorCode = OSAL_E_INVALIDVALUE;  // todo Why not OSAL_E_DOESNOTEXIST;
         }
         /* UnLock the Event table */
         UnLockOsal( &pOsalData->EventTable.rLock );
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceFlg == TRUE)||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }

   return( s32ReturnValue );
}

tU32 u32SetBitfield(trEventElement *pCurrentEntry)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   switch(pCurrentEntry->u32PostFlag)
   {
      case OSAL_EN_EVENTMASK_AND:
           pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & pCurrentEntry->u32PostMask;
          break;
      case OSAL_EN_EVENTMASK_OR:
           pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField | pCurrentEntry->u32PostMask;
          break;
      case OSAL_EN_EVENTMASK_XOR:
           pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField ^ pCurrentEntry->u32PostMask;
          break;
      case OSAL_EN_EVENTMASK_REPLACE:
           pCurrentEntry->u32EventBitField = pCurrentEntry->u32PostMask;
          break;
      default:
           u32ErrorCode = OSAL_E_INVALIDVALUE;
             NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
          break;
   }
   return u32ErrorCode;
}

tU32 u32EvaluateMask(trEventElement *pCurrentEntry)
{
   tU32 u32Mask = 0;
   switch ( pCurrentEntry->u32WaitEnFlags )
   {
      case OSAL_EN_EVENTMASK_AND:
           u32Mask = ( ( pCurrentEntry->u32EventBitField & pCurrentEntry->u32WaitMask ) == pCurrentEntry->u32WaitMask );
          break;
      case OSAL_EN_EVENTMASK_OR:
           u32Mask = ( pCurrentEntry->u32EventBitField & pCurrentEntry->u32WaitMask );
          break;                              // this is only for the trace message end
      default:
           NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
          break;
   }
//   TraceString("Event u32EvaluateMask returns 0x%x ",u32Mask);
   return u32Mask;
}
 

/*****************************************************************************
*
* FUNCTION: OSAL_s32EventWait
*
* DESCRIPTION: This function waits for an OSAL event to occur,
*              where an event occurrence means the link operation
*              (given by enFlags) between the EventField present
*              in the EventGroup structure and the provided EventMask
*              matches. Allowed link operation are AND/OR
*              So the event resets the calling thread only if within
*              the requested timeout one of the following conditions
*              is verified:
*                 EventMask || EventField is TRUE or
*                 EventMask && EventField is true
*              depending on the requested link operation
*
*
* PARAMETER:   hEvent (I)
*                 event handle.
*              mask (I)
*                 event mask.
*              enFlag (I)
*                 event flag.
*              msec (I)
*                 waiting time.
*              pResultMask (->O)
*                 pointer to the previous event mask.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventWait(OSAL_tEventHandle      hEvent,
                       OSAL_tEventMask        mask,
                       OSAL_tenEventMaskFlag  enFlags,
                       OSAL_tMSecond          msec,
                       OSAL_tEventMask        *pResultMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckEventHandle(hEvent);
   trEventElement *pCurrentEntry = NULL;
   OSAL_tMSecond s32Timeout = msec;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
         pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
         if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
            if((pCurrentEntry->bTraceFlg == TRUE)||(u32OsalSTrace & 0x00000080))
            {
                TraceString("OSALEVENT TID:%d Start Wait Mask:0x%x Flag:%s Bitfield:0x%x Event:%s",
                            OSAL_ThreadWhoAmI(),mask,FlagName[enFlags],pCurrentEntry->u32EventBitField,pCurrentEntry->szName);
            }
            WaiSyncObj(pCurrentEntry->hLocalLock, OSAL_C_TIMEOUT_FOREVER);

            if((enFlags != OSAL_EN_EVENTMASK_AND) && (enFlags != OSAL_EN_EVENTMASK_OR))
            {
               u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
            else if((!(pCurrentEntry->u32Option & WAIT_MULTIPLE_TSK))&&(pCurrentEntry->bWaitFlag == TRUE))
            {
               u32ErrorCode = OSAL_E_EVENTINUSE;
               NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
            }
            else
            {
               /* check for first post before first wait */
               if(pCurrentEntry->u32WaitEnFlags == (OSAL_EN_EVENTMASK_REPLACE+1))
               {
                 u32ErrorCode = u32SetBitfield(pCurrentEntry);
               }
               pCurrentEntry->u32WaitMask    = mask;
               pCurrentEntry->u32WaitEnFlags = ( tU32 )enFlags;
               OSAL_tEventMask u32Mask = 0;
               do
               {
                  u32Mask = u32EvaluateMask(pCurrentEntry);
                  if( u32Mask == 0 )
                  {
                     // Flush any outstanding triggers to prevent succeeding EventWaits
                     // without any actual event.  Scenario for this case:
                     // - WAITER waits for some events with timeout
                     // - POSTER posts a waited-for event and triggers the semaphore
                     // - virtually at the same time, the WaitForSingleObject() call
                     //   expires with a timeout error, i.e. the semaphore remains set
                     // The next time WAITER goes to sleep it wakes up immediately,
                     // even though no event has been posted.
                   //  int i= 0;
                     do{
		//				 i++;
		//				 if(i>1)TraceString("OSALEVENT Clean !!!!!!!!!!!!!!!!!!");
                     }while( WaiSyncObj( pCurrentEntry->hLock, OSAL_C_TIMEOUT_NOBLOCKING ) != OSAL_E_TIMEOUT );
					 
                     pCurrentEntry->bWaitFlag = TRUE;

                     if((s32Timeout > 0)&&(s32Timeout < pOsalData->u32TimerResolution))
                     {
                         s32Timeout = pOsalData->u32TimerResolution;
                     }
                     RelSyncObj(pCurrentEntry->hLocalLock);

                     u32ErrorCode = WaiSyncObj( pCurrentEntry->hLock, s32Timeout);

                     WaiSyncObj(pCurrentEntry->hLocalLock, OSAL_C_TIMEOUT_FOREVER);

                     pCurrentEntry->bWaitFlag = FALSE;

                     if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
                     {
                        u32ErrorCode = OSAL_E_TIMEOUT;
                        NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
                     }

                     if((pCurrentEntry->bTraceFlg == TRUE)&&(OSAL_E_TIMEOUT != u32ErrorCode))
                     {
                         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION_STEP,FALSE,u32ErrorCode,pCurrentEntry->szName);
                     }
                  }
               } while( (0 == u32Mask) && (OSAL_E_TIMEOUT != u32ErrorCode) );
            }
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               s32ReturnValue = (tS32)OSAL_OK;
               if( pResultMask != NULL )
               {
                  *pResultMask = pCurrentEntry->u32WaitMask & pCurrentEntry->u32EventBitField;
               }
               if(pCurrentEntry->u32Option & CONSUME_EVENT)
               {
                  pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & ~pCurrentEntry->u32WaitMask;
               }
           }
            RelSyncObj(pCurrentEntry->hLocalLock);
         }
         else //if( pCurrentEntry )
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE; // todo OSAL_E_DOESNOTEXIST?
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode != OSAL_E_TIMEOUT)
      {
         tCString coszName;
         if(pCurrentEntry)coszName = pCurrentEntry->szName;
         else coszName = "NO_NAME";
         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      else
      {
         if(((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE))||(u32OsalSTrace & 0x00000080))
         {
            vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
         }
      }
   }
   else
   {
      if((pCurrentEntry->bTraceFlg == TRUE )||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventPost
*
* DESCRIPTION: this function set an event in OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventPost( OSAL_tEventHandle hEvent,
                        OSAL_tEventMask   mask,
                        OSAL_tenEventMaskFlag enFlags )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckEventHandle(hEvent);
   trEventElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
         pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
         if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
            WaiSyncObj(pCurrentEntry->hLocalLock, OSAL_C_TIMEOUT_FOREVER);

            pCurrentEntry->u32PostMask = mask;
            pCurrentEntry->u32PostFlag = (tU32)enFlags;
            u32ErrorCode = u32SetBitfield(pCurrentEntry);

            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               s32ReturnValue = OSAL_OK;
               if(pCurrentEntry->bWaitFlag == TRUE ) // a thread is suspended and is waiting for events
               {
//                  OSAL_tEventMask u32Mask;                 // this is only for the trace message start
                  if((pCurrentEntry->u32WaitEnFlags == OSAL_EN_EVENTMASK_AND)
                   ||(pCurrentEntry->u32WaitEnFlags == OSAL_EN_EVENTMASK_OR))
                  {
//                     u32Mask = u32EvaluateMask(pCurrentEntry);
                  }
                  else
                  {
                     u32ErrorCode = OSAL_E_INVALIDVALUE;
                  }
                  if( u32ErrorCode == OSAL_E_NOERROR )
                  {
                     if(RelSyncObj( pCurrentEntry->hLock ) == OSAL_E_NOERROR)   // trigger the waiting task (semvalue++)
                     {
                            s32ReturnValue = OSAL_OK;
                     }
                  }
               }
               else
               {
                  /* check for first post before first wait */
                  if(pCurrentEntry->u32WaitEnFlags == (OSAL_EN_EVENTMASK_REPLACE+1))
                  {
                     s32ReturnValue = OSAL_OK;          
                  }
               }
            }
            RelSyncObj(pCurrentEntry->hLocalLock);
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE; // todo doesnotexsist?
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,POST_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bTraceFlg == TRUE )||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,POST_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventStatus
*
* DESCRIPTION: this function returns the status of an OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventStatus(OSAL_tEventHandle hEvent,
                         OSAL_tEventMask   mask,
                         OSAL_tEventMask   *pMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = u32CheckEventHandle(hEvent);
   trEventElement *pCurrentEntry = NULL;

   if(u32ErrorCode == OSAL_E_NOERROR)
   {
         /* Lock the event table */
         pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
         if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
           WaiSyncObj(pCurrentEntry->hLocalLock, OSAL_C_TIMEOUT_FOREVER);
           *pMask = (mask & pCurrentEntry->u32EventBitField);
           RelSyncObj(pCurrentEntry->hLocalLock);
           s32ReturnValue = OSAL_OK;
         }
         else
         {
            s32ReturnValue = OSAL_ERROR;
            u32ErrorCode   = OSAL_E_DOESNOTEXIST;
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bTraceFlg == TRUE )||(u32OsalSTrace & 0x00000080))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked *//*lint !e613 pCurrentEntry already checked */
      }
   }
   return( s32ReturnValue );
}

#ifdef __cplusplus
}
#endif

#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
