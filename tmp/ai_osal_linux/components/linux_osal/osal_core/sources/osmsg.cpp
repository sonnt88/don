/*****************************************************************************
| FILE:         osmsg.cpp
| PROJECT:      Platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) IOSC Connection-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date      | Modification                                 | Author
| 20.12.12  | Initial revision                             | MRK2HI
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif


/**************************************************************************/
/* typedefs                                                               */
/**************************************************************************/

/* aligment type */
typedef long OSAL_MSG_tAlign;

/* header of free or used block */
union OSAL_MSG_uHeader
{
   struct
   {
      tU16 u16MagicField;   /* identify status of the following block OSAL_MSG_MAGIC     -> valid message
                                                                      OSAL_DEL_MSG_MAGIC -> deleted message
                                                                      OSAL_INV_MSG_MAGIC -> marker for begin of guard intervall */
                            /* NOTE: u16MagicField cannot be used for OSAL_DEL_MSG_MAGIC because otherwise CheckMessage() will
                               always detect this as message pool damage and write misleading errmem entries */							
      tU16 u16MsgOffset;    /* offset after OSAL_MSG_uHeader of the message for begin of message 1-11 bytes 
                                      or OSAL_DEL_MSG_MAGIC in case of message marked as deleted
                                      To be handled carefully in message payload lengh calculation !*/
      tS32 s32Offset;       /* offset in the pool */
      tU32 nsize;           /* size in blocks need for the message*/
   } s;
   OSAL_MSG_tAlign x;   /*lint -e754 */  // ensure alignment on different architectures
};

typedef union OSAL_MSG_uHeader OSAL_MSG_tHeader;

/* shared memory header */
typedef struct
{
   tS32 freeOffset;
   tU32 nAbsSize;
   tU32 nCurSize;
   tU32 nMinSize;
   tU32 nMaxMessageSize;
   tU32 n32Counter;
} OSAL_MSG_tSharedHeader;

static OSAL_MSG_tHeader       *pBase = 0;
static OSAL_MSG_tSharedHeader *pSharedRead = 0;
static OSAL_MSG_tSharedHeader *pSharedWrite = 0;
static tCString OSAL_MSG_szMemoryName = "OSAL_MSG_MEMORY_NAME";
static tCString OSAL_MSG_CreSemName   = "OSAL_MSG_CRE_SEM";
static tCString OSAL_MSG_DelSemName   = "OSAL_MSG_DEL_SEM";
static OSAL_tSemHandle   hSemMsgPoolCre = OSAL_C_INVALID_HANDLE;
static OSAL_tSemHandle   hSemMsgPoolDel = OSAL_C_INVALID_HANDLE;
static tBool bMarkerErrMem = FALSE;
static tU32 u32PrcOpenCount = 0;
static tU32 u32LargeMsgCount = 0;

OSAL_tShMemHandle      MsgPoolSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;

/**************************************************************************/
/* variables                                                              */
/**************************************************************************/

   /**************************************************************************/
   /* defines                                                                */
   /**************************************************************************/
#define CHECK_MSG_POOL
#ifdef CHECK_MSG_POOL
// DJ: IMPORTANT: See above. GUARD_BYTES has to be as well a multiple of sizeof(OSAL_MSG_tHeader)
// DJ: IMPORTANT: With the current implementation GUARD_BYTES even has to be exactly == sizeof(OSAL_MSG_tHeader)    
#define GARD_BYTES     sizeof(OSAL_MSG_tHeader)
#else
#define GARD_BYTES     0
#endif

#define POINTER_2_OFFSET( p ) \
   ( ( (uintptr_t) (p) - (uintptr_t) (pBase) ) / \
   (uintptr_t) sizeof( OSAL_MSG_tHeader ) )

#define OFFSET_2_POINTER( o ) \
   ( pBase + (o) )


#define OSAL_MSG_MAGIC     ((tU16) 0xDACA)
#define OSAL_INV_MSG_MAGIC ((tU16) 0xCADA)
#define OSAL_DEL_MSG_MAGIC ((tU16) 0xFFFF)

//#define PRINT_LARGE_MSG             
//#define PRINT_LARGE_MSG_ONLY


   /* --This define is needed for determining the real size of message pool (dirty hack!). 
   This is the minimum message pool size. */
#define OSAL_MSG_INITIAL_POOLSIZE        (4096)

   /* --This define returns if check of the message pool failed. */
#define OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(returnvalue)        \
   { if (pOsalData->rMsgPoolStruct.bCheck && OSAL_s32CheckMessagePool()==OSAL_ERROR) return (returnvalue); }


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
extern void vTraceCcaMsg(OSAL_trMessage* handle, tU32 MsgSize,trMqueueElement *pCurrentEntry,tCString coszName);

/*****************************************************************************
|  Function prototypes (module-local):
|---------------------------------------------------------------------------- */

   static tS32 s32SemCreate( tVoid );
   static tS32 s32SemOpen( tVoid );
   static tS32 s32SemClose( tVoid );
   static tS32 s32SemDelete( tVoid );
   static tS32 s32SemOp( int iOperation, int iType );

   static tVoid vEnterCriticalSectionCre(void);
   static tVoid vLeaveCriticalSectionCre(void);
   static tVoid vEnterCriticalSectionDel(void);
   static tVoid vLeaveCriticalSectionDel(void);
   static tVoid vGetSpace(tPU32 pu32MaxFree, tPU32 pu32MaxUsed);
   static void vDeleteMessages(void);
   static tS32 s32CheckPointer(OSAL_MSG_tHeader *p, tBool bDoRangeCheck, tBool bUseLock);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
   tPU32 pu32GetSharedBaseAdress(void)
   {
       return (tPU32)pBase;
   }

   tU32 u32GetSharedHeaderSize()
   {
       return sizeof(OSAL_MSG_tSharedHeader);
   }
   tU32 u32GetMsgHeaderSize()
   {
       return sizeof(OSAL_MSG_tHeader);
   }

   static tS32 OSAL_s32MessagePoolGetCurrentSizeIntern()
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      if (pSharedRead)
      {
         s32ReturnValue = (tS32) (pSharedRead->nCurSize * sizeof(OSAL_MSG_tHeader));
      }
      return s32ReturnValue;
   }

   static tS32 OSAL_s32MessagePoolGetAbsoluteSizeIntern()
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      if (pSharedRead)
      {
         s32ReturnValue=(tS32) (pSharedRead->nAbsSize * sizeof(OSAL_MSG_tHeader));
      }
      return s32ReturnValue;
   }

   static tS32 OSAL_s32MessagePoolGetMinimalSizeInternal( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      if (pSharedRead)
      {
         s32ReturnValue= (tS32) (pSharedRead->nMinSize * sizeof(OSAL_MSG_tHeader));
      }
      else
      {
         u32ErrorCode=OSAL_E_UNKNOWN;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }


void TraceMsgPoolData(const char* cBuffer,...)
{
  tS32 s32Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  s32Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  if(s32Len != OSAL_ERROR)
  {
     LLD_vTrace(OSAL_C_TR_CLASS_SYS_MSGPOOL, TR_LEVEL_DATA,cBuf,s32Len+1);
  }
}



   /* --This define dumps some information about the message pool to trace output. */
   tVoid OSAL_M_TRACE_MESSAGEPOOL(tChar flag,tU32 pointer,tU32 size,tU32 remaining)
   {  
         tU32 u32MaxFree,u32MaxUsed;
         OSAL_tThreadID tid=OSAL_ThreadWhoAmI();

         vGetSpace(&u32MaxFree,&u32MaxUsed);
         TraceMsgPoolData("%10d\t%10d\t%c\t%#010x\t%10d\t%10d\t%10d\t%10d\t",
         OSAL_ClockGetElapsedTime(),tid,flag,pointer,size,remaining,u32MaxFree,u32MaxUsed);
         if(flag == 'd')
         {
            tU8 i;
            tBool bFirstLoop = FALSE;
            if(((uintptr_t)pointer > (uintptr_t)pBase)
             ||((uintptr_t)pointer < (uintptr_t)pBase + pOsalData->rMsgPoolStruct.u32MemSize))
            {
               tU8* pU8 = (tU8*)OFFSET_2_POINTER(pointer);
               TraceMsgPoolData("'");
               for(i = 12; i < 42; i++)
               {
                  if(bFirstLoop == FALSE)
                  {
                     bFirstLoop = TRUE;
                  }
                  else
                  {
                     TraceMsgPoolData(" ");
                  }
                  TraceMsgPoolData("%02X", pU8[i]);
               }
            }
            TraceMsgPoolData("'");
         }
         TraceMsgPoolData("\n\r");
   }


tS32 s32LinuxMapping(tU32 u32Size)
{
   tS32 s32ReturnValue = OSAL_ERROR;

   ((void)u32Size);  // lint

    /* Check for first connect */
    if(!pSharedWrite && !pSharedRead) 
    {
       if(MsgPoolSharedMem == (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE)
       {
          MsgPoolSharedMem = OSAL_SharedMemoryOpen(OSAL_MSG_szMemoryName, OSAL_EN_READWRITE);
       }
       /* get read pointer to shared memory */
       pSharedRead = (OSAL_MSG_tSharedHeader*) OSAL_pvSharedMemoryMap(MsgPoolSharedMem,   /* Handle */
                                                                      OSAL_EN_READONLY,              /* Rights */
                                                                      pOsalData->rMsgPoolStruct.u32MemSize, /* Size + Header */
                                                                      0);   /* Offset */
       /* get write pointer to shared memory */
       pSharedWrite = (OSAL_MSG_tSharedHeader*) OSAL_pvSharedMemoryMap(MsgPoolSharedMem,   /* Handle */
                                                                       OSAL_EN_READWRITE,             /* Rights */
                                                                       pOsalData->rMsgPoolStruct.u32MemSize, /* Size + Header */
                                                                       0);                            /* Offset */

       /* --check if mapping failed. */
       if (pSharedWrite && pSharedRead) 
       {
            pOsalData->u32Open++;
            u32PrcOpenCount++;
            /* set local base pointer */
            pBase = (OSAL_MSG_tHeader*) ((uintptr_t)pSharedWrite+ sizeof(OSAL_MSG_tSharedHeader));
            if(pBase != (OSAL_MSG_tHeader*) (pSharedWrite+1))
            {
               FATAL_M_ASSERT_ALWAYS();
            }
            s32ReturnValue=OSAL_OK;
      }
   }
   else
   {
        pOsalData->u32Open++;
        u32PrcOpenCount++;
        s32ReturnValue=OSAL_OK;
   }
   return s32ReturnValue;
}

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolCreate
   *----------------------------------------------------------------------
   * DESCRIPTION:  creates the shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    (I) size of the pool
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolCreate(tU32 u32Size)
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR; /* --temporary used error code. */
      tU32 u32NSize, u32HNSize;
      
      /* overwrite with registry configured pool size */	  
      u32Size = pOsalData->u32MsgPoolSize;
	  
      if (u32Size>=OSAL_MSG_INITIAL_POOLSIZE) 
      {
         /* pOsalData->rMsgPoolStruct.bCreated is locked in OSAL constructor */
         if (pOsalData->rMsgPoolStruct.bCreated == FALSE) 
         {
            /* --Create the semaphore. */
            if (s32SemCreate()==OSAL_OK) 
            {
               /* --Enter critical section. */
               vEnterCriticalSectionCre();
             //  TraceString("CCA Message Pool created by PID:%d",getpid());
               /* set check false */
               pOsalData->rMsgPoolStruct.bCheck = FALSE;
               /* calculate block size */
               u32NSize = (u32Size+sizeof(OSAL_MSG_tHeader)-1)/sizeof(OSAL_MSG_tHeader) + 1;
               /* Calculate block size plus header: */
               u32HNSize = (u32NSize+1)*sizeof(OSAL_MSG_tHeader) + sizeof(OSAL_MSG_tSharedHeader);
               /* create shared memory area */
               MsgPoolSharedMem = OSAL_SharedMemoryCreate(OSAL_MSG_szMemoryName,  /* Name */
                                                             OSAL_EN_READWRITE,      /* Rights */
                                                             u32HNSize );               /* Size + Header */
               /* --Created shared memory. */
               if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_ERROR) 
               {
                  /* --Initialize process structure. */
                  if (s32LinuxMapping(u32HNSize)==OSAL_OK) 
                  {
                     // DJ: This initializes the pool with two entries
                     // DJ: The first entry is the beginning of the chained list of free entries.
                     // DJ: This first entry at pBase seems to be never changed
                     // DJ: pBase->s.nsize is set to zero event though the nsize here is in 
                     // DJ: fact one (one OSAL_MSG_tHeader)
                     // DJ: Looks like this is a trick to avoid that this first entry might be combined as 
                     // DJ: left neighbour case of deleting a message entry.
                     /* initialise header pointer */
                     pBase->s.u16MagicField = OSAL_MSG_MAGIC;
                     pBase->s.u16MsgOffset = 0;
                     // DJ: Initialize initial entry s32Offset to 2 instead of one. This is neccessary cause
                     // DJ: we now use xxx->s.s32Offset==-2 to identify deleted messages
                     // DJ: So we need space for one additoinal block here to avoid access to addresses < pbase
                     pBase->s.s32Offset = 2;
                     pBase->s.nsize = 0;
                     /* initialise first block */
                     // DJ: This initializes the one initial free entry. Initially it covers the full msg-pool.
                     // DJ: The offset (xxx->s.S32Offset) is set to 0 and thus this entry is 
                     // DJ: pointing back to the first entry to allow wraparound
                     OFFSET_2_POINTER(pBase->s.s32Offset)->s.u16MagicField = OSAL_MSG_MAGIC;
                     OFFSET_2_POINTER(pBase->s.s32Offset)->s.u16MsgOffset = 0;
                     OFFSET_2_POINTER(pBase->s.s32Offset)->s.s32Offset = 0;
                     OFFSET_2_POINTER(pBase->s.s32Offset)->s.nsize = u32NSize;
#ifdef CHECK_MSG_POOL
                     OSAL_MSG_tHeader* pTmp = OFFSET_2_POINTER(pBase->s.s32Offset);
                     pTmp =  (OSAL_MSG_tHeader*)((uintptr_t)pTmp + ((pTmp->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
                     pTmp->s.u16MagicField = OSAL_INV_MSG_MAGIC;
                     pTmp->s.u16MsgOffset  = 0;
                     pTmp->s.s32Offset     = 0;
                     pTmp->s.nsize         = u32NSize;
#endif
                     /* initialise shared header */
                     pSharedWrite->freeOffset = 0;
                     pSharedWrite->nAbsSize = u32NSize;
                     pSharedWrite->nCurSize = u32NSize;
                     pSharedWrite->nMinSize = u32NSize;
                     pSharedWrite->nMaxMessageSize = 0;
                     pSharedWrite->n32Counter = 1;
                     pOsalData->rMsgPoolStruct.u32MemSize = u32HNSize;
                     pOsalData->rMsgPoolStruct.bCreated = TRUE;
                    /* --ok! */
                     s32ReturnValue=OSAL_OK;
                  } 
                  else 
                  {
                     /* --Delete the shared memory. */
                     OSAL_s32SharedMemoryClose(MsgPoolSharedMem);
                     OSAL_s32SharedMemoryDelete(OSAL_MSG_szMemoryName);
                     MsgPoolSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
                     /* --Internal Error. */
                     u32ErrorCode=OSAL_E_UNKNOWN;
                  }
               } 
               else
               {
                  /* --Internal error. */
                  u32ErrorCode=OSAL_E_UNKNOWN;
               }
               /* --Leave critical section. */
               vLeaveCriticalSectionCre();    /*lint !e746 */    // Angeblich gibt es zu der Funktion keinen Prototypen?

               /* --If something went wrong delete the critical semaphore. */        
               if (s32ReturnValue==OSAL_ERROR) 
               {
                  s32SemClose();
                  MsgPoolSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
                  s32SemDelete();
               }
            }
         } 
         else
         {
            /* --Already exists. */
            u32ErrorCode=OSAL_E_ALREADYEXISTS;
         }
      } 
      else
      {
         /* --Ung\FCltiger Parameter. */
         u32ErrorCode=OSAL_E_INVALIDVALUE;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         if(u32ErrorCode != OSAL_E_ALREADYEXISTS)
         {
            TraceString("OSAL_s32MessagePoolCreate failed Error:%d",u32ErrorCode);
         }
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000100)
      {
         TraceString("OSAL_s32MessagePoolCreate TID:%d Size:%d returns %d",OSAL_ThreadWhoAmI(),u32Size,s32ReturnValue);
      } 
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolOpen
   *----------------------------------------------------------------------
   * DESCRIPTION:  opens an existing shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    NONE
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolOpen( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      tU32 u32NSize, u32HNSize;
      
      // TraceString("OSAL_s32MessagePoolOpen called with open count:%d prc open count:%d",pOsalData->u32Open,u32PrcOpenCount);
     
      /* --Try to open critical section semaphore. */
      if (s32SemOpen()==OSAL_OK) 
      {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();  /*lint !e746 */  // Angeblich gibt es zu der Funktion keinen Prototypen?

         /* calculate block size */
         u32NSize = (pOsalData->u32MsgPoolSize + sizeof(OSAL_MSG_tHeader)-1)/sizeof(OSAL_MSG_tHeader) + 1;
         /* Calculate block size plus header: */
         u32HNSize = (u32NSize+1)*sizeof(OSAL_MSG_tHeader) + sizeof(OSAL_MSG_tSharedHeader);

         /* Map the message pool. */
         if((s32ReturnValue = s32LinuxMapping(u32HNSize)) == OSAL_OK)
         {
               pSharedWrite->n32Counter++;
         }
         else 
         {
               /* --Something went wrong. */
               u32ErrorCode=OSAL_E_UNKNOWN;
         }

         /* --If something failed, reset the critical semaphore handle. */
         if (s32ReturnValue==OSAL_ERROR) 
         {
            s32SemClose();
            MsgPoolSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
         }
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      } 
      else
      {
         /* --Does not exist.*/
         u32ErrorCode=OSAL_E_DOESNOTEXIST;    
      }

      /* --Set error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         TraceString("OSAL_s32MessagePoolOpen failed Error:%d",u32ErrorCode);
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000100)
      {
         TraceString("OSAL_s32MessagePoolOpen TID:%d returns %d ",OSAL_ThreadWhoAmI(),s32ReturnValue);
      }
      /* --Go back. */
      return s32ReturnValue;
   }


   /************************************************************************
   *
   * FUNCTION:     OSAL_PrintMemoryLeaks
   *----------------------------------------------------------------------
   * DESCRIPTION:  undocumented internal function: prints memory leaks
   *                      kein Schutz durch Semaphore; muss vor dem Aufruf gesch\FCtzt sein!!!
   *----------------------------------------------------------------------
   * PARAMETER:    (I) fd: Osal file descriptor
   * RETURNVALUE:  void
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tVoid OSAL_vPrintMemoryLeaks(void)
   {
      tChar szBuffer[1024];
      tChar szSmallBuf[10];
      tU32 u32MsgSize;
      OSAL_MSG_tHeader *p;
      tPU8 pMessage;
      tU32 nsize;
      tU16 u16Co;

      if (pOsalData->rMsgPoolStruct.bCreated != FALSE) 
      {
         if (pSharedRead) 
         {
            nsize = pSharedRead->nAbsSize;
            for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
            {
               if(s32CheckPointer(p,TRUE, FALSE) == OSAL_ERROR)
               {
                  vWritePrintfErrmem("!!! MemPool ERROR: Real 0x%8x, Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p), p->s.nsize);
                  TraceString("!!! MemPool ERROR: Real 0x%8x, Offset %4d, Size %d\r\n",  p, POINTER_2_OFFSET(p), p->s.nsize);
                  return;
               }

               if(p->s.u16MsgOffset == OSAL_DEL_MSG_MAGIC)
               {
                  OSAL_s32PrintFormat( szBuffer, "0, Deleted marked block, Unknown queue, '");
                  u32MsgSize = (uintptr_t) p->s.nsize*sizeof(OSAL_MSG_tHeader);
                  pMessage = (tPU8) (p+1);
                  for (u16Co = 0; u16Co < u32MsgSize && u16Co < 36; u16Co++)
                  {
                     snprintf(szSmallBuf,10,"%.2x ", pMessage[u16Co]);
                     strcat(szBuffer, szSmallBuf);
                  }
                  strcat(szBuffer, "'\r\n");
                  TraceString(szBuffer);
                  if(bMarkerErrMem == TRUE)vWritePrintfErrmem(szBuffer);   /*lint !e641 */
               }
               else if(p->s.s32Offset == -1)
               {
                  OSAL_s32PrintFormat( szBuffer, "0, Used Block, Unknown queue, '");
                  u32MsgSize = (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) - p->s.u16MsgOffset;
                  pMessage = (tPU8) (p+1);
                  for (u16Co = 0; u16Co < u32MsgSize && u16Co < 36; u16Co++)
                  {
                     snprintf(szSmallBuf,10,"%.2x ", pMessage[u16Co]);
                     strcat(szBuffer, szSmallBuf);
                  }
                  strcat(szBuffer, "'\r\n");
                  TraceString(szBuffer);
                  if(bMarkerErrMem == TRUE)vWritePrintfErrmem(szBuffer);   /*lint !e641 */
               }
            } 
         }
         else
         {
            TraceString("MemPool Internal error 1");
            if(bMarkerErrMem == TRUE)vWritePrintfErrmem("MemPool Internal error 1 \n");   /*lint !e641 */
         }
      }
      else
      {
         TraceString("MemPool Internal error 2");
                  if(bMarkerErrMem == TRUE)vWritePrintfErrmem("MemPool Internal error 2 \n");   /*lint !e641 */
      }
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolClose
   *----------------------------------------------------------------------
   * DESCRIPTION:  closes an existing shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    NONE
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolClose( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      tU32 nsize;
      tU32 hnsize;

     // TraceString("OSAL_s32MessagePoolClose called with open count:%d prc open count:%d",pOsalData->u32Open,u32PrcOpenCount);

      /* --Check if pool is opened. */
       if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
       {    
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         pOsalData->u32Open--;
         /* --Check if data is available. */
         if (u32PrcOpenCount && pSharedRead) 
         {
  
            TraceString("OSAL_s32MessagePoolClose for PID:%d",OSAL_ProcessWhoAmI());
            /* --Decrement counter. */
            u32PrcOpenCount--;
            if(u32PrcOpenCount==0) 
            {
               // pool f\FCr diesen Prozess schliessen

               // shared counter dekrementieren
               pSharedWrite->n32Counter--;
               // wenn pool vom letzten Prozess gel\F6scht wird, dann message pool leaks tracen
               if (pSharedWrite->n32Counter == 0)
               {
                 // OSAL_tIODescriptor fd = (OSAL_tIODescriptor) OSAL_C_INVALID_HANDLE;  /*lint !e569 */
                  if (OSAL_s32MessagePoolGetAbsoluteSizeIntern() != OSAL_s32MessagePoolGetCurrentSizeIntern())
                  {
                     vWritePrintfErrmem("Message Pool Leaks after the last OSAL_s32MessagePoolClose: \n");
                  }
                  else
                  {
                     vWritePrintfErrmem("Message Pool shutting down with no leaks. \n");
                  }
                  /* In jedem Fall aber die Print-Funktion aufrufen!
                     Die tut nix, wenn kein Leak da ist. Falls aber die
                     sizes mal verzaehlt worden sind und ausversehen
                     keine Leaks erkannt worden sind, ist es besser nochmal
                     nachzusehen:    */
                  OSAL_vPrintMemoryLeaks();
                  {
                     // Minimal Size ausgeben...
                     tS32 s32Size = OSAL_s32MessagePoolGetMinimalSizeInternal();
                     vWritePrintfErrmem("After Last Close: OSAL_s32MessagePoolGetMinimalSize() = %d",s32Size );
                  }
               }

               /* calculate block size */
               nsize = pSharedRead->nAbsSize;
               /* Calculate block size plus header: */
               hnsize = (nsize+1)*sizeof(OSAL_MSG_tHeader) + sizeof(OSAL_MSG_tSharedHeader);

               /* unmap read memory pointer */
               OSAL_s32SharedMemoryUnmap(pSharedRead, hnsize );
               pSharedRead=OSAL_NULL;

               /* unmap write memory pointer */
               OSAL_s32SharedMemoryUnmap(pSharedWrite, hnsize );
               pSharedWrite=OSAL_NULL;
                /* close shared memory pool */
               OSAL_s32SharedMemoryClose(MsgPoolSharedMem);

               pOsalData->rMsgPoolStruct.bCheck = FALSE;
              
               MsgPoolSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
           }
            /* --Everything is fine. */
            s32ReturnValue=OSAL_OK;
         } 
         else 
         {
            /* --Internal error. */
            u32ErrorCode=OSAL_E_UNKNOWN;
         }
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();            

         /* --If last close call, close the semaphore. */
         if (u32PrcOpenCount == 0) 
         {
            s32SemClose();
         }
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR) 
      {
         TraceString("OSAL_s32MessagePoolClose failed Error:%d",u32ErrorCode);
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000100)
      {
         TraceString("OSAL_s32MessagePoolClose TID:%d returns %d",OSAL_ThreadWhoAmI(),s32ReturnValue);
      }
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  deletes an existing shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    NONE
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolDelete( tVoid )
   {
      //TraceString("OSAL_s32MessagePoolDelete called with open count:%d prc open count:%d",pOsalData->u32Open,u32PrcOpenCount);

      if(pOsalData->rMsgPoolStruct.bCreated == TRUE)
      {
         vEnterCriticalSectionCre();
         /* delete shared memory pool */
         OSAL_s32SharedMemoryDelete( OSAL_MSG_szMemoryName );
         pOsalData->rMsgPoolStruct.bCreated = FALSE;
         vLeaveCriticalSectionCre();
         /* delete critical section semaphore */
         s32SemDelete();
         if(u32OsalSTrace & 0x00000100)
         {
            TraceString("OSAL_s32MessagePoolDelete TID:%d returns %d",OSAL_ThreadWhoAmI(),OSAL_OK);
         }
      }
      else
      {
         TraceString("OSAL_s32MessagePoolDelete TID:%d returns %d ErrorCode",OSAL_ThreadWhoAmI(),OSAL_ERROR,OSAL_E_DOESNOTEXIST);     
         OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
      }
      return OSAL_OK;
   }


void vPrinthexDump(tPCU8 p2,tU32 Len)
{
    char cDest[260 * 3] = {0};
    char Destination[4] = {0,0,0,0};
    tU32 i=0,j=0;
    if(Len > 256)Len = 256;

      for(i = 0;i < Len ;i++)
      {
           sprintf(Destination,"%02x", *(p2+i));  
                   cDest[j] = Destination[0];
                   j++;
                   cDest[j] = Destination[1];
                   j++;
                   cDest[j] = ' ';
                   j++;
      }
      if(Len > 128)
      {
         vWriteToErrMem((tS32)TR_COMP_OSALCORE,(char*)&cDest[0],128,OSAL_STRING_OUT);
         vWriteToErrMem((tS32)TR_COMP_OSALCORE,(char*)&cDest[128],Len-128,OSAL_STRING_OUT);
      }
      else
      {
         vWriteToErrMem((tS32)TR_COMP_OSALCORE,(char*)&cDest[0],j,OSAL_STRING_OUT);
      }
}

// DJ: Tries to find message payload of the physically (!) preceeding entry in pool
// DJ: Interprets message as CCA message and prints out information to ERRMEM
// DJ: Used to possibly identify the causing application in case of corruptions
// DJ: In case of a NON-CCA message the ERRMEN output is undefined (and maybe misleading)
// DJ: ERRMEM output might be undefined as well, if not called from a critical section
void vPrintPreceedingMsgInfo(OSAL_MSG_tHeader *p)
{
   tU8* p2 = NULL;
   char buffer[250];
   tBool bReturn = FALSE;
   OSAL_MSG_tHeader* pTmp = (OSAL_MSG_tHeader*)((uintptr_t)p - (2/*GUARD + 0*Msg + HEADER*/*sizeof(OSAL_MSG_tHeader))); // msg can be zero lentgh 
   while(1)
   {
      if((pTmp->s.u16MagicField == OSAL_MSG_MAGIC))
      {
         p2 = (tU8*)pTmp;
         /* check with length information, that we found the message before */
         if(((uintptr_t)pTmp + (pTmp->s.nsize * sizeof(OSAL_MSG_tHeader))) == (uintptr_t)p)
         {
            bReturn = TRUE;
            break;
         }
      }
      pTmp = (OSAL_MSG_tHeader*)((uintptr_t)pTmp - sizeof(OSAL_MSG_tHeader));
      /* check if we leave the valid range */
      if((uintptr_t)pTmp < (uintptr_t)pBase)
      {
          break;
      }
   }
   if(bReturn == TRUE)
   {
      /* set to start of message */
      p2 = (tPU8)((uintptr_t)p2 + sizeof(OSAL_MSG_tHeader));
      OSAL_s32PrintFormat(buffer,
                          "Check Msg: Header overwritten Preceeding Msg Sender:%u Receiver:%u Size:%u for %u Blocks Type:%u s-sub:%u d-sub:%u Time:%u Serv-ID:%u Func-ID:%u OpCode:%u\n",
                          (tS32)*((tU16*)((uintptr_t)p2)),
                          (tS32)*((tU16*)((uintptr_t)p2+2)),
                          (tS32)*((tU32*)((uintptr_t)p2+4)),
                          (tS32) (p->s.nsize),
                          (tS32)*((tU8*) ((uintptr_t)p2+11)),
                          (tS32)*((tU16*)((uintptr_t)p2+12)),
                          (tS32)*((tU16*)((uintptr_t)p2+14)),
                          (tS32)*((tU32*)((uintptr_t)p2+16)),
                          (tS32)*((tU16*)((uintptr_t)p2+20)),
                          (tS32)*((tU16*)((uintptr_t)p2+24)),
                          (tS32)*((tU8*) ((uintptr_t)p2+26)));
      vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&buffer[0],strlen(buffer),OSAL_STRING_OUT);
   //   vPrinthexDump(p2,(pTmp->s.nsize-1)* sizeof(OSAL_MSG_tHeader));
   }
   else
   {
       OSAL_s32PrintFormat(buffer,
                          "Check Msg: Header overwritten Preceeding Msg Sender: preceeding msg not found (overwritten ?)\n");
      vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&buffer[0],strlen(buffer),OSAL_STRING_OUT);  
   }
}


void vPrintMsgInfo(OSAL_MSG_tHeader *p, tBool bDoubleDelete)
{
   tU8* p2 = NULL;
   char buffer[250];
   OSAL_MSG_tHeader* pTmp = p;

   /* set to start of message */
   p2 = (tPU8)((uintptr_t)pTmp + sizeof(OSAL_MSG_tHeader));
   if(bDoubleDelete)
   {
      OSAL_s32PrintFormat(buffer,
                          "Double Delete for Msg Sender:%u Receiver:%u Size:%u Type:%u s-sub:%u d-sub:%u Time:%u Serv-ID:%u Func-ID:%u OpCode:%u\n",
                          (tS32)*((tU16*)((uintptr_t)p2)),
                          (tS32)*((tU16*)((uintptr_t)p2+2)),
                          (tS32)*((tU32*)((uintptr_t)p2+4)),
                          (tS32)*((tU8*) ((uintptr_t)p2+11)),
                          (tS32)*((tU16*)((uintptr_t)p2+12)),
                          (tS32)*((tU16*)((uintptr_t)p2+14)),
                          (tS32)*((tU32*)((uintptr_t)p2+16)),
                          (tS32)*((tU16*)((uintptr_t)p2+20)),
                          (tS32)*((tU16*)((uintptr_t)p2+24)),
                          (tS32)*((tU8*) ((uintptr_t)p2+26)));
   }
   else
   {
      OSAL_s32PrintFormat(buffer,
                          "Check Msg: Guard overwritten Sender:%u Receiver:%u Size:%u for %u Blocks Type:%u s-sub:%u d-sub:%u Time:%u Serv-ID:%u Func-ID:%u OpCode:%u\n",
                          (tS32)*((tU16*)((uintptr_t)p2)),
                          (tS32)*((tU16*)((uintptr_t)p2+2)),
                          (tS32)*((tU32*)((uintptr_t)p2+4)),
                          (tS32) (p->s.nsize),
                          (tS32)*((tU8*) ((uintptr_t)p2+11)),
                          (tS32)*((tU16*)((uintptr_t)p2+12)),
                          (tS32)*((tU16*)((uintptr_t)p2+14)),
                          (tS32)*((tU32*)((uintptr_t)p2+16)),
                          (tS32)*((tU16*)((uintptr_t)p2+20)),
                          (tS32)*((tU16*)((uintptr_t)p2+24)),
                          (tS32)*((tU8*) ((uintptr_t)p2+26)));
   }
   vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&buffer[0],strlen(buffer),OSAL_STRING_OUT);
   //vPrinthexDump(p2,(pTmp->s.nsize-1)* sizeof(OSAL_MSG_tHeader));
}


// DJ: Tries to restore a corrupted pool entry header from the information in the Guard
// DJ: Returns FALSE if correction was not possible
// DJ: IMPORTANT: Function may be called only from a critical section 
// DJ: IMPORTANT: Shall be called only if CHECK_MSG_POOL is defined
tBool bMsgAreaCorrection(OSAL_MSG_tHeader *p)
{
   OSAL_MSG_tHeader* pTmp = (OSAL_MSG_tHeader*)((uintptr_t)p + (1/*HEADER +0*Msg*/*sizeof(OSAL_MSG_tHeader))); // msg can be zero lenght
   tBool bReturn = FALSE;
   while(1)
   {
      /* check for GUARD info */
      if(pTmp->s.u16MagicField == OSAL_INV_MSG_MAGIC)
      {
         /* check with length information, that we found the message before */
         if(((uintptr_t)p + ((pTmp->s.nsize-1) * sizeof(OSAL_MSG_tHeader))) == (uintptr_t)pTmp)
         {
            /* restore message management information */
            p->s.u16MsgOffset = pTmp->s.u16MsgOffset;
            p->s.s32Offset    = pTmp->s.s32Offset;
            p->s.nsize        = pTmp->s.nsize;
            p->s.u16MagicField = OSAL_MSG_MAGIC;

            bReturn = TRUE;
            break;
         }
      }
      pTmp = (OSAL_MSG_tHeader*)((uintptr_t)pTmp + sizeof(OSAL_MSG_tHeader));
      /* check if we leave the valid range */
      if((uintptr_t)pTmp > (uintptr_t)pBase + pOsalData->rMsgPoolStruct.u32MemSize)
      {
          break;
      }
   }
   return bReturn;
}

// DJ: Checks if p has valid range and if entry pointed to by p has valid header/structure
// DJ: In case of invalid header, function tries to correct structures via call 
// DJ: of bMsgAreaCorrection().
// DJ: Range check is only done, if bDoRangeCheck== TRUE
// DJ: bMsgAreaCorrection() changes pool structures.
// DJ: So s32CheckPointer() shall be either called from inside a critical section with
// DJ: bUseLock==FALSE or from outside a critical section with bUseLock==TRUE
tS32 s32CheckPointer(OSAL_MSG_tHeader *p, tBool bDoRangeCheck, tBool bUseLock)
{
 
    // DJ: p in valid range ?
    if(bDoRangeCheck==TRUE)
    {
      if(((uintptr_t)p < (uintptr_t)pBase)
       ||((uintptr_t)p > (uintptr_t)pBase + pOsalData->rMsgPoolStruct.u32MemSize))
      {
          TraceString("s32AllocMsgMem: Invalid memory range for message.");
          NORMAL_M_ASSERT_ALWAYS();
          OSAL_vSetErrorCode(OSAL_E_UNKNOWN);
          return OSAL_ERROR;
      }
    }

      if(*((tU16*)p)!= OSAL_MSG_MAGIC)
      {
#ifdef CHECK_MSG_POOL
          tBool bRet;
          vPrintPreceedingMsgInfo(p);
          if(bUseLock == TRUE)
          {
              vEnterCriticalSectionCre();
          }
          bRet = bMsgAreaCorrection(p);
          if(bUseLock == TRUE)
          {
              vLeaveCriticalSectionCre();
          }

          if(bRet == FALSE)
#else
          ((void)bUseLock);
#endif 
          {
             /* message overwritten */
             WRITE_LINE_INFO;
             /*  */
             OSAL_vSetErrorCode(OSAL_E_UNKNOWN);
             return OSAL_ERROR;
          }
      }
      return OSAL_OK;
   }

// DJ: Iterates through the sequential chained list of free entries in the pool
// DJ: Allocates one entry with sufficient space (nsize) in the message pool and
// DJ: fills header and Guard. Returns OSAL_ERROR in case that no free space of the given
// DJ: size is found or the pool is corrupted and can't be repaired.
// DJ: IMPORTANT: This function may be called only from a critical section.
tS32 s32AllocMsgMem(tU32 nsize,tU32 size,OSAL_trMessage *pHandle)
{
   OSAL_MSG_tHeader *p, *prevp;
#ifdef CHECK_MSG_POOL
   OSAL_MSG_tHeader *pTmp;
#endif

   /* set start pointer */
   prevp = OFFSET_2_POINTER(pSharedWrite->freeOffset);

   if(s32CheckPointer(prevp,TRUE, FALSE) == OSAL_ERROR)
   {
      return OSAL_ERROR;
   }
   // DJ: This is a loop over ONLY the free entries in the message pool. It seems to be guaranteed
   // DJ: that the addresses (i.e. offsets) of such free entries are in sequence.
   // DJ: If end of pool is reached it wraps around and continues search at the beginning of the pool again.
   // DJ: This loop does not touch at all any entries in the pool with messages.
   for(p=OFFSET_2_POINTER(prevp->s.s32Offset); ;prevp = p, p = OFFSET_2_POINTER(p->s.s32Offset))
   {
      if(s32CheckPointer(p,FALSE,FALSE) == OSAL_ERROR)
      {
         return OSAL_ERROR;
      }
 #ifdef CHECK_MSG_POOL
      /* calculate so that we do not leave space too small for a new message */
      if (p->s.nsize >= (nsize+2)) /* sufficient size */
      {
         if (p->s.nsize == nsize+2) /* exact size */
         {
             nsize=nsize+2; // DJ: special case, more room as required for message, but who cares
#else
      if (p->s.nsize >= nsize) /* sufficient size */
      {
         if (p->s.nsize == nsize) /* exact size */
         {
#endif
            // DJ: Free entry now completely filled, entry is removed from the list of free entries
            prevp->s.s32Offset = p->s.s32Offset;
         }
         else
         {
            // DJ: This allocates space for the message at the END of the free entry and reduces
            // DJ: the remaining size of the free entry.
            p->s.nsize -= nsize;
#ifdef CHECK_MSG_POOL
            // DJ: This is the area where the Guard of the free entry is stored.
            // DJ: Old free entry guard info will be replace with guard of new message entry below
            // DJ: Guard of the free entry has to be recreated to the new end of the free area !!!!
            // DJ: To have space for this guard, we need more space (see comment above)
            pTmp = (OSAL_MSG_tHeader*)(((uintptr_t)p) + ((p->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
            pTmp->s.u16MagicField = OSAL_INV_MSG_MAGIC;
            pTmp->s.u16MsgOffset  = p->s.u16MsgOffset;
            pTmp->s.s32Offset     = p->s.s32Offset;
            pTmp->s.nsize         = p->s.nsize;
#endif
            p += p->s.nsize;
            p->s.nsize = nsize;
         }

         /* offset berechnen; wird verwendet zur exakten Berechnung der Nachrichtengr\F6\DFe */
         p->s.u16MsgOffset = (tU16) (nsize*sizeof(OSAL_MSG_tHeader)-size);

         /* magic field setzen */
         p->s.u16MagicField=OSAL_MSG_MAGIC;


         /* set next pointer of used block to -1 */
         // DJ: This means that the occupied entry now does not point to any free entry anymore
         // DJ: It is effectively taken out of the chained sequence of free entries
         p->s.s32Offset=-1;
  
#ifdef CHECK_MSG_POOL
         /* store message info in GUARD */
         pTmp = (OSAL_MSG_tHeader*)(((uintptr_t)p) + ((nsize-1) * sizeof(OSAL_MSG_tHeader)));
         pTmp->s.u16MagicField = OSAL_INV_MSG_MAGIC;
         pTmp->s.u16MsgOffset  = p->s.u16MsgOffset;
         pTmp->s.s32Offset     = p->s.s32Offset;
         pTmp->s.nsize         = p->s.nsize;
#endif
         /* set new free pointer */
         pSharedWrite->freeOffset = POINTER_2_OFFSET(prevp);
         /* decrement current size */
         pSharedWrite->nCurSize -= nsize;
         /* set minimal size */
         if (pSharedWrite->nCurSize < pSharedWrite->nMinSize)
             pSharedWrite->nMinSize = pSharedWrite->nCurSize;
         /* set max message size */
         if (pSharedWrite->nMaxMessageSize < nsize)
             pSharedWrite->nMaxMessageSize = nsize;
          /* set offset and type */
          // DJ: In fact only this handle points to the new pool entry with the message
          // DJ: This entry it not in any sequence of entries which can be iterated through 
          pHandle->u32Offset = (tU32) POINTER_2_OFFSET(p);

          /* check if bit 0x10 is set in registry key DUMP_MSG_POOL to set PID & TID to 
             message buffer at allocation time */
          if(pOsalData->rMsgPoolStruct.bLogPidTid)
          {
             OSAL_trMessage handle;
             handle.enLocation =  OSAL_EN_MEMORY_SHARED;
             handle.u32Offset  = pHandle->u32Offset;
             tU32* pu32Tmp = (tU32*)OSAL_pu8MessageContentGet(handle,OSAL_EN_READWRITE);/*lint !e826*/
             *pu32Tmp = OSAL_ProcessWhoAmI();
             pu32Tmp++;
             *pu32Tmp = OSAL_ThreadWhoAmI();
          }

          OSAL_M_TRACE_MESSAGEPOOL('c',pHandle->u32Offset,size,
          pSharedWrite->nCurSize*sizeof(OSAL_MSG_tHeader));

          return OSAL_OK;
       }
       // DJ: pool completely searched (incl. wraparound), no free entry found 
       if (p == OFFSET_2_POINTER(pSharedWrite->freeOffset))
       {
          return OSAL_ERROR;
       }
   }
}


void vCheckSituation(tU32 size)
{
   OSAL_tIODescriptor Des = OSAL_ERROR;
   tS32 s32Size;
   char Buffer[250];
   memset((void*)Buffer,0,250);

   s32Size = OSAL_s32MessagePoolGetCurrentSize();
   snprintf(&Buffer[0],250,
            "Requested Msg Size %u -> OSAL_s32MessagePoolGetCurrentSize() %d\n",
            (unsigned int)size , s32Size);
   vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&Buffer[0],(int)strlen(Buffer),OSAL_STRING_OUT);
   TraceString(Buffer);

   if((Des = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,OSAL_EN_READWRITE)) != OSAL_ERROR)
   {
      OSAL_vPrintMessageList(Des);
      (tVoid)OSAL_s32IOClose(Des);
   }
}


   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessageCreate
   *----------------------------------------------------------------------
   * DESCRIPTION:  creates a message in shared memory pool or on the local heap
   *----------------------------------------------------------------------
   * PARAMETER:    (O) pHandle: message handle
   * PARAMETER:    (I) size: size of message
   * PARAMETER:    (I) type: local or shared message type
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessageCreate(OSAL_trMessage *pHandle, tU32 size, OSAL_tenMemoryLocation enLocation)
   {
      tU32 nsize,newsize = size;
      OSAL_MSG_tHeader *p;
      trLargeCcaMsg rMsg = {0};
      tPU8 prMsg;
      tS32 s32Ret = OSAL_ERROR;
	  
      if ((OSAL_NULL == pHandle)||(size == 0))
      {
         NORMAL_M_ASSERT_ALWAYS();
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
         goto s32MessageCreate_return; /*lint !e801*/
      }

      pHandle->enLocation = OSAL_EN_MEMORY_INVALID;

      /* local message */
      if (OSAL_EN_MEMORY_LOCAL == enLocation)
      {
#ifdef CHECK_MSG_POOL_EXT
         Mod = size%4;
         if(Mod == 0)
         {
            newsize = size + 8;
         }
         else
         {
            newsize = size + 12;
         }
         pPtr = (tU32*)OSAL_pvMemoryAllocate(newsize);
         if (0 == pPtr)
         {
            OSAL_vSetErrorCode(OSAL_E_NOSPACE); 
            goto s32MessageCreate_return; /*lint !e801*/
         }
         memcpy(pPtr,&size,4);
         pPtr++;
         p = (OSAL_MSG_tHeader *)pPtr;

         pHandle->u32Offset = (uintptr_t) p;
         pHandle->enLocation = enLocation;
         if(Mod == 0)
         {
            pPtr = (tU32*)((tU32)p + size); 
         }
         else
         {
            pPtr = (tU32*)((tU32)p + size + 8 - Mod); 
         }
         memcpy(pPtr,&Val,4);
#else
         p = (OSAL_MSG_tHeader *) OSAL_pvMemoryAllocate(size);

         if (0 == p)
         {
            NORMAL_M_ASSERT_ALWAYS();
            OSAL_vSetErrorCode(OSAL_E_NOSPACE); 
            goto s32MessageCreate_return; /*lint !e801*/
         }
         pHandle->u32Offset = (uintptr_t) p;
         pHandle->enLocation = enLocation;
#endif
         s32Ret = OSAL_OK;
          //      return OSAL_OK; remove warnings Tn
      }
      else if (OSAL_EN_MEMORY_SHARED == enLocation)/* shared message */
      { 
         if (OSAL_NULL == pSharedWrite)
         {
            TraceString("OSAL_s32MessageCreate: OSAL_NULL == pSharedWrite");
            vWritePrintfErrmem("OSAL_s32MessageCreate: OSAL_NULL == pSharedWrite \n");
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
            goto s32MessageCreate_return; /*lint !e801*/
         }

         /* --Message pool check. */
         OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(OSAL_ERROR);

         /* check if MMAP_MSG_SIZE registry key is set to allocate seperate shared memory
            for a CCA message with a certain size , will help when big messages are allocated 
            and pool is heavily fragmented */
         if(pOsalData->u32MmapMsgSize)
         {
            pHandle->enLocation = OSAL_EN_MEMORY_INVALID;
		
            /* check size of MMAP_MSG_SIZE registry key to allocate seperate shared memory
               for a CCA message with a certain size */	
            if(size >= pOsalData->u32MmapMsgSize)
            {
               /* generate named shared memory */
               snprintf(rMsg.cShmName,16,"/CCAMSG_%d",pOsalData->u32MmapMsgCnt);
               rMsg.pAdress[0] = vpMmapShmToPrc(rMsg.cShmName,size,TRUE);
               if(rMsg.pAdress[0] != NULL)
               {   
                 rMsg.u32Size   = size;
                 rMsg.s32Pid[0] = OSAL_ProcessWhoAmI();
                 rMsg.s32Pid[1] = 0;
                 pOsalData->u32MmapMsgCnt++;
                 /* set marker for deletion of seperate shred memory */
                 enLocation = (OSAL_tenMemoryLocation)(OSAL_EN_MEMORY_SHARED+1);
                 /* check if process has to delete other message shared memories and delete them*/
                 if(u32LargeMsgCount > 0)
                 { 
                    /* check for still mapped, but deleted shared memory to clean process adress space */
                    u32UnmapDeletedShMem();
                 }
                 u32LargeMsgCount++;

                 size = sizeof(trLargeCcaMsg);
               }
               else
               {
                  TraceString("OSAL_s32MessageCreate: vpMmapShmToPrc failed Error:%d Task:%d request %d Bytes.",OSAL_u32ErrorCode(),OSAL_ThreadWhoAmI(),size);
                  vWritePrintfErrmem("OSAL_s32MessageCreate: vpMmapShmToPrc failed Error:%d Task:%d request %d Bytes.",OSAL_u32ErrorCode(),OSAL_ThreadWhoAmI(),size);
                  WRITE_LINE_INFO;
                  /* Try to alloc from Pool */
               }
            }
         }
          
#ifdef CHECK_MSG_POOL
         newsize = size + GARD_BYTES;
#endif
 
         /* calculate block size */
         nsize = (newsize+sizeof(OSAL_MSG_tHeader)-1)/sizeof(OSAL_MSG_tHeader)+1;
         /* Enter critical section */
         vEnterCriticalSectionCre();

         if(s32AllocMsgMem(nsize,newsize,pHandle) == OSAL_ERROR)
         {
            /* we have start a sequence 
               iosc_obtain_semaphore - vEnterCriticalSectionCre - vLeaveCriticalSectionCre - iosc_release_semaphore
               accoding to sequence in OSAL_s32MessageDelete therefore we have to vLeaveCriticalSectionCre*/
            vLeaveCriticalSectionCre();
            /* start sequence for deletion */
            vEnterCriticalSectionDel();
 
            vEnterCriticalSectionCre();
            /* try to delete messages to get sufficient memory en block in pool*/
            if(pOsalData->rMsgPoolStruct.u32HandleCount > 0)
            {
               vDeleteMessages();
            }
            vLeaveCriticalSectionCre();
            vLeaveCriticalSectionDel();
            /* end sequence for deletion */
 
            /* Enter CriticalSection again to allocate message */
            vEnterCriticalSectionCre();
            /* try now again to allocate the message */
            if(s32AllocMsgMem(nsize,newsize,pHandle) == OSAL_ERROR)
            {
                /* Leave critical section */
                vLeaveCriticalSectionCre();
                
                /*Because still not enough memory in pool check if it is allowed to use seperate shared memory set via MIN_DYN_MMAP_MSG_SIZE registry key */
                if(size > pOsalData->u32DynMmapMsgSize)
                {
                   pOsalData->u32MmapMsgSize = size;
                   vWritePrintfErrmem("OSAL_s32MessageCreate: Not enough memory in message pool PID:%d Task:%d request %d Bytes u32MmapMsgSize adapted \n",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),size);
                   return OSAL_s32MessageCreate(pHandle,size,enLocation);
                }
                else
                {
                   TraceString("OSAL_s32MessageCreate: Not enough memory in message pool PID:%d Task:%d request %d Bytes.",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),size);
                   vWritePrintfErrmem("OSAL_s32MessageCreate: Not enough memory in message pool PID:%d Task:%d request %d Bytes.\n",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),size);
                   WRITE_LINE_INFO;
                }
                /* check if bit 0x1 is set in registry key DUMP_MSG_POOL to dump the content of the CCA message pool to errmem once for power cycle */
                if(pOsalData->rMsgPoolStruct.u32InvestigatePool & 0x1)
                {
                   bMarkerErrMem = TRUE;
                   pOsalData->rMsgPoolStruct.u32InvestigatePool |= 0x0;
                   vCheckSituation(newsize);
                   bMarkerErrMem = FALSE;
                }

                OSAL_vSetErrorCode(OSAL_E_NOSPACE);
                goto s32MessageCreate_return;/*lint !e801*/
            }
            else
            {
               pHandle->enLocation = enLocation;
               vLeaveCriticalSectionCre();
               if(pOsalData->bCheckCcaMsg)
               {
                  vTraceCcaMsg(pHandle,0,NULL,"OSAL_s32MessageCreate");
               }
               s32Ret = OSAL_OK;
            }
         }
         else
         {
             pHandle->enLocation = enLocation;
             vLeaveCriticalSectionCre();
             if(pOsalData->bCheckCcaMsg)
             {
                vTraceCcaMsg(pHandle,0,NULL,"OSAL_s32MessageCreate");
             }
             s32Ret = OSAL_OK;
         }    
      }
      else /* invalid */
      {
         TraceString("OSAL_s32MessageCreate: Task:%d Invalid message type. MemType:%d ",OSAL_ThreadWhoAmI(),enLocation);
         vWritePrintfErrmem("OSAL_s32MessageCreate: Task:%d Invalid message type. MemType:%d  \n",OSAL_ThreadWhoAmI(),enLocation);
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
         goto s32MessageCreate_return; /*lint !e801*/
      }
      if(pHandle->enLocation == (OSAL_tenMemoryLocation)(OSAL_EN_MEMORY_SHARED+1))
      {
         prMsg = (tPU8) pSharedWrite +((pHandle->u32Offset + 1) * sizeof( OSAL_MSG_tHeader ) + sizeof( OSAL_MSG_tSharedHeader));
         if(u32OsalSTrace & 0x00000100)
	 {
#ifdef NO_PRIxPTR        
            TraceString("OSAL_s32MessageCreate PID:%d CCA:0x%x Shm:0x%x Size:%d",OSAL_ProcessWhoAmI(),(unsigned int)prMsg,(unsigned int)rMsg.pAdress[0],rMsg.u32Size);
#else
            TraceString("OSAL_s32MessageCreate PID:%d CCA:0x%"PRIxPTR" Shm:0x%"PRIxPTR" Size:%d",OSAL_ProcessWhoAmI(),(uintptr_t)prMsg,(uintptr_t)rMsg.pAdress[0],rMsg.u32Size);
#endif
	 }
         memcpy((char*)prMsg,(char*)&rMsg,sizeof(trLargeCcaMsg));
      }

s32MessageCreate_return:
      if(u32OsalSTrace & 0x00000100)
      {
         if(pHandle)
         {
#ifdef NO_PRIxPTR        
            TraceString("OSAL_s32MessageCreate TID:%d Size:%d Offset:0x%x returns:%d",OSAL_ThreadWhoAmI(),size,pHandle->u32Offset,s32Ret);
#else
            TraceString("OSAL_s32MessageCreate TID:%d Size:%d Offset:0x%"PRIxPTR" returns:%d",OSAL_ThreadWhoAmI(),size,pHandle->u32Offset,s32Ret);
#endif
         }
         else
         {
            TraceString("OSAL_s32MessageCreate TID:%d Size:%d Handle NULL returns:%d",OSAL_ThreadWhoAmI(),size,s32Ret);
         }
      }
      return s32Ret;
   }


void vPrintLargeMsgToErrMem(tPU8 p,tU32 Size)
{
   char buffer[250];
   OSAL_s32PrintFormat(buffer,
                      "Delete large Message Msg Size:%u Sender:%u Receiver:%u for %u Blocks Type:%u s-sub:%u d-sub:%u Time:%u Serv-ID:%u Func-ID:%u OpCode:%u\n",
                      Size,
                      (tS32)*((tU16*)((uintptr_t)p)),
                      (tS32)*((tU16*)((uintptr_t)p+2)),
                      (tS32)*((tU32*)((uintptr_t)p+4)),
                      (tS32)*((tU8*) ((uintptr_t)p+11)),
                      (tS32)*((tU16*)((uintptr_t)p+12)),
                      (tS32)*((tU16*)((uintptr_t)p+14)),
                      (tS32)*((tU32*)((uintptr_t)p+16)),
                      (tS32)*((tU16*)((uintptr_t)p+20)),
                      (tS32)*((tU16*)((uintptr_t)p+24)),
                      (tS32)*((tU8*) ((uintptr_t)p+26)));
   vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&buffer[0],strlen(buffer),OSAL_STRING_OUT);
}

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessageDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  deletes a message in shared memory pool or on the local heap
   *----------------------------------------------------------------------
   * PARAMETER:    (I) handle: message handle
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
// DJ: This function does not directly delete messages, it just marks message to be deleted later in
// DJ: pOsalData->rMsgPoolStruct.rHandleArray[] until pOsalData->rMsgPoolStruct.u32HandleCount == MSG_HANDLE_BUFFER. 
// DJ: It then calls vDeleteMessage() to delete all the message in a bulk. 
   tS32 OSAL_s32MessageDelete(OSAL_trMessage handle)
   {
      OSAL_MSG_tHeader *bp;
      trLargeCcaMsg* prMsg;
      tS32 s32Ret = OSAL_ERROR;
	  
      if(pOsalData->bCheckCcaMsg)
      {
         vTraceCcaMsg(&handle,0,NULL,"OSAL_s32MessageDelete");
      }
     /* local message */
      if (OSAL_EN_MEMORY_LOCAL == (char)handle.enLocation)
      {
#ifdef CHECK_MSG_POOL_EXT
         if(handle.u32Offset == 0)
         {
             NORMAL_M_ASSERT_ALWAYS();
             goto s32MessageDelete_return; /*lint !e801*/
         }
         tU32* pPtr = (tU32*)handle.u32Offset;
         /* goto size info */
         pPtr--;
         memcpy(&tmp,pPtr,4);
         /* goto size offset */
         Mod = tmp%4;
         if(Mod == 0)
         {
            pPtr = (tU32*)((tU32)pPtr + tmp + 4);
         }
         else
         {
            pPtr = (tU32*)((tU32)(handle.u32Offset + tmp + 12 - Mod));
         }

         if(MAGIC_MSG != *pPtr)
         {
            NORMAL_M_ASSERT_ALWAYS();
            if(*((tU32*)pPtr) ==  0x19495cff)
            {
              NORMAL_M_ASSERT_ALWAYS();
              goto s32MessageDelete_return; /*lint !e801*/
            }
            *((tU32*)pPtr) = 0xf0f0f0f0;
          }
#endif
         OSAL_vMemoryFree((tPVoid)handle.u32Offset);
         s32Ret = OSAL_OK;
      }
      else if (OSAL_EN_MEMORY_SHARED <= (char)handle.enLocation)/* shared message */
      {
         if (OSAL_NULL == pSharedWrite)
         {
            TraceString("OSAL_s32MessageDelete: OSAL_NULL == pSharedWrite");
            vWritePrintfErrmem("OSAL_s32MessageDelete: OSAL_NULL == pSharedWrite \n");
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
            goto s32MessageDelete_return; /*lint !e801*/
         }

         /* --Message pool check. */
    //    OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(OSAL_ERROR);

         /* calculate header */
         bp = OFFSET_2_POINTER(handle.u32Offset);
         if(s32CheckPointer(bp,TRUE,TRUE) == OSAL_ERROR)
         {
            // DJ: Message corrupted, we could not repair, so we can't delete this message
            // DJ: but we continue and hope that not complete pool is damaged
            // DJ: IMPORTANT: In case that this happens frequently, we have a memory leak in the pool
             TraceString("OSAL_s32MessageDelete: Invalid shared memory pointer MemType:%d Offset:%d",
                        (char)handle.enLocation,handle.u32Offset);
             vWritePrintfErrmem("OSAL_s32MessageDelete: Invalid shared memory pointer MemType:%d Offset:%d \n",
                        (char)handle.enLocation,handle.u32Offset);
             OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
             goto s32MessageDelete_return; /*lint !e801*/
         }

         if (bp->s.s32Offset >= 0)
         {
            // DJ: This is a free entry, Error
            TraceString("OSAL_s32MessageDelete: Invalid shared memory pointer  free entry.");
            vWritePrintfErrmem("OSAL_s32MessageDelete: Invalid shared memory pointer  free entry. \n");
            OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
            goto s32MessageDelete_return; /*lint !e801*/
         }
         else
         {
            /* check to see if OSAL_s32MessageDelete was called for this message before */
            if(bp->s.u16MsgOffset == OSAL_DEL_MSG_MAGIC)
            {
                TraceString("OSAL_s32MessageDelete: Message already deleted.");
                vPrintMsgInfo(bp,TRUE);
                OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
                goto s32MessageDelete_return;/*lint !e801*/
            }
         }
   
         if((OSAL_tenMemoryLocation)(OSAL_EN_MEMORY_SHARED+1) == (char)handle.enLocation)
         {
            tS32 s32Idx = 0;
            tU32 u32Offset = (handle.u32Offset + 1) * sizeof( OSAL_MSG_tHeader ) + sizeof( OSAL_MSG_tSharedHeader );
            prMsg = (trLargeCcaMsg*)((uintptr_t)pSharedWrite + u32Offset);
            if(u32OsalSTrace & 0x00000100)
            {
               if(prMsg->s32Pid[0] != OSAL_ProcessWhoAmI())
               {
                  s32Idx = 1;
               }
#ifdef NO_PRIxPTR        
                TraceString("OSAL_s32MessageDelete PID:%d TID:%d Shm:0x%x Size:%d ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->pAdress[s32Idx],prMsg->u32Size);
#else              
                TraceString("OSAL_s32MessageDelete PID:%d TID:%d Shm:0x%"PRIxPTR"  Size:%d ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->pAdress[s32Idx],prMsg->u32Size);
#endif        
			}
           vDelShMem(prMsg->pAdress[s32Idx],prMsg->cShmName,prMsg->u32Size);   
         }

         vEnterCriticalSectionDel();
         
         bp->s.u16MsgOffset = OSAL_DEL_MSG_MAGIC;

         pOsalData->rMsgPoolStruct.rHandleArray[pOsalData->rMsgPoolStruct.u32HandleCount] = handle;
         pOsalData->rMsgPoolStruct.u32HandleCount++;
         if(pOsalData->rMsgPoolStruct.u32HandleCount == MSG_HANDLE_BUFFER)
         {
                vEnterCriticalSectionCre();
                vDeleteMessages();
                /* Leave critical section */
                vLeaveCriticalSectionCre();
         }
         vLeaveCriticalSectionDel();
         s32Ret = OSAL_OK;
      }
      else /* invalid */
      {
         TraceString("OSAL_s32MessageDelete: Task:%d Invalid message handle MemType:%d ",
                     OSAL_ThreadWhoAmI(),(char)handle.enLocation);
         vWritePrintfErrmem("OSAL_s32MessageDelete: Task %d Invalid message handle MemType:%d \n",
                     OSAL_ThreadWhoAmI(),(char)handle.enLocation);
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
      }

s32MessageDelete_return:
      if(u32OsalSTrace & 0x00000100)
      {
#ifdef NO_PRIxPTR        
            TraceString("OSAL_s32MessageDelete TID:%d Offset:0x%x returns %d",OSAL_ThreadWhoAmI(),handle.u32Offset,s32Ret);
#else
            TraceString("OSAL_s32MessageDelete TID:%d Offset:0x%"PRIxPTR" returns %d",OSAL_ThreadWhoAmI(),handle.u32Offset,s32Ret);
#endif
      }
      return s32Ret;
   }

// DJ: This function really physically "deletes" the messages adressed by the handles
// DJ: in pOsalData->rMsgPoolStruct.rHandleArray[]. The basic algorithm for deletion is the same as
// DJ: formerly implemented inside OSAL_s32MessageDelete() for a single message.
// DJ: Message are deleted in the same sequence as they were marked for deletion by 
// DJ: OSAL_s32MessageDelete()
// DJ: IMPORTANT: Function may be called only from a critical section
// DJ: With the current implementaion it is in addition called only when protected 
// DJ: by an IOSC semaphore, but it is unclear to me, why this is required.
void vDeleteMessages(void)
{
   OSAL_MSG_tHeader *bp, *p;
   tU32 u32Count;
   tBool bFailed;
#ifdef CHECK_MSG_POOL
   OSAL_MSG_tHeader *pTmpP,*pTmpBP;
#endif

//   OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(OSAL_ERROR);

   for(u32Count = 0;u32Count < pOsalData->rMsgPoolStruct.u32HandleCount;u32Count++)
   {
      bFailed = FALSE;
      /* calculate header */
      bp = OFFSET_2_POINTER(pOsalData->rMsgPoolStruct.rHandleArray[u32Count].u32Offset);
      if(s32CheckPointer(bp,TRUE,FALSE) == OSAL_OK)
      {
         // DJ: Loop over all free entries in message pool, starting from freeOffset
         // DJ: The loop searches for the free entry p, so that p < bp < p->s.s32Offset
         // DJ: Thus it is guaranteed that the new freed entry can be either combined with 
         // DJ: the right and/or left neighbour (when BP is directly adjacent to the neighbour) 
         // DJ: or that the new freed entry can be hooked into the sequence of freed block 
         // DJ: in order of increasing offsets.
         // DJ: bFailed set to FALSE if such entry found and to TRUE if this search fails
         p = OFFSET_2_POINTER(pSharedWrite->freeOffset);
         if(s32CheckPointer(p,TRUE,FALSE) == OSAL_ERROR)
         {
               FATAL_M_ASSERT_ALWAYS();
         }
         for ( ; !(bp > p && bp < OFFSET_2_POINTER(p->s.s32Offset)); p = OFFSET_2_POINTER(p->s.s32Offset))
         {
            if((s32CheckPointer(OFFSET_2_POINTER(p->s.s32Offset),FALSE, FALSE) == OSAL_ERROR))
            {
               FATAL_M_ASSERT_ALWAYS();
            }
            if (bp >= p && bp < p+p->s.nsize) 
            {
               TraceString("vDeleteMessages: Invalid shared memory pointer (MAGIC-FIELD).");
               OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
               pOsalData->rMsgPoolStruct.rHandleArray[u32Count].enLocation = OSAL_EN_MEMORY_INVALID;
               bFailed = TRUE;
               break;
            }
            // DJ: Special condition for the case of the wraparound (continue interating through the 
            // DJ: chained list of free entries at beginning of pool)
            if (p >= OFFSET_2_POINTER(p->s.s32Offset) && (bp > p || bp < OFFSET_2_POINTER(p->s.s32Offset)))
               break; /* free block at one end */
         }
         if((s32CheckPointer(OFFSET_2_POINTER(p->s.s32Offset),FALSE, FALSE) == OSAL_ERROR))
         {
               FATAL_M_ASSERT_ALWAYS();
         }

         /* increment current size */
         if(bFailed==FALSE) pSharedWrite->nCurSize += bp->s.nsize;     

         if(bFailed == FALSE)
         // DJ: Above search was succesful, free the entry and properly sort into the sequence of free entries
         // DJ: or combine with right/left neighbour
         // DJ: If we had already checked above that headers of bp, p and p->s.s32Offset are intact, we 
         // DJ: don't have to do this here again.
         {
#ifdef CHECK_MSG_POOL
            /* check Guard */
            pTmpBP = (OSAL_MSG_tHeader*)((uintptr_t)bp + ((bp->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
            if(pTmpBP->s.u16MagicField != OSAL_INV_MSG_MAGIC)
            {
                vPrintMsgInfo(bp,FALSE);
            }
#endif
            // DJ: bp is directly adjacent to the right neighbor, so we can combine the new free
            // DJ: entry with the right neighbour to create a bigger free block (avoid fragmentation).
            // DJ: Magic field of right neighbour has to be deleted, lenght of new free block to be adjusted
            // DJ: (nsize) and offset of bp set to the offset stored in the right neighbours header........
            if (bp + bp->s.nsize == OFFSET_2_POINTER(p->s.s32Offset)) /* combine with right neighbour */
            {
#ifdef CHECK_MSG_POOL
               /*destroy the old GUARD information */
               pTmpBP = (OSAL_MSG_tHeader*)((uintptr_t)bp + ((bp->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
               pTmpBP->s.u16MagicField = 0;
#endif  
               bp->s.nsize += OFFSET_2_POINTER(p->s.s32Offset)->s.nsize;
               bp->s.s32Offset = OFFSET_2_POINTER(p->s.s32Offset)->s.s32Offset;
               bp->s.u16MsgOffset = 0; // remove deleted marker
            // magic field des rechten Nachbarn loeschen
            OFFSET_2_POINTER(p->s.s32Offset)->s.u16MagicField = 0;
#ifdef CHECK_MSG_POOL
               // DJ: Fill the guard of the new combined free entry
               /* update the information in the GUARD of the memory assigned to the message */
               pTmpBP = (OSAL_MSG_tHeader*)((uintptr_t)bp + ((bp->s.nsize - 1) * sizeof(OSAL_MSG_tHeader)));
               pTmpBP->s.nsize     = bp->s.nsize;
               pTmpBP->s.s32Offset = bp->s.s32Offset;
               pTmpBP->s.u16MsgOffset = bp->s.u16MsgOffset;
#endif
            }
            else
              // DJ: bp is NOT directly adjacent to the right neighbour, so we
              // DJ: only have to link bp to the right neighbor by setting the bp offset and 
              // DJ: we have to correct the guard accordingly
            {
               bp->s.s32Offset = p->s.s32Offset;
               bp->s.u16MsgOffset = 0; // remove deleted marker   
#ifdef CHECK_MSG_POOL
               pTmpBP = (OSAL_MSG_tHeader*)((uintptr_t)bp + ((bp->s.nsize - 1) * sizeof(OSAL_MSG_tHeader)));
               pTmpBP->s.s32Offset = bp->s.s32Offset;
               pTmpBP->s.u16MsgOffset = bp->s.u16MsgOffset;
#endif
            }
            if (p + p->s.nsize == bp) /* combine with left neighbour */
            // DJ: bp is directly adjacent to the left neighbor, so we can combine the new free
            // DJ: entry with the left neighbour to create a bigger free block (avoid fragmentation).
            // DJ: Magic field of bp has to be deleted in this case , lenght of new free block to be adjusted
            // DJ: (nsize) and offset of p set to the offset stored in the bp header
            {
#ifdef CHECK_MSG_POOL
               /*destroy the old GUARD information */
               pTmpP = (OSAL_MSG_tHeader*)((uintptr_t)p + ((p->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
               pTmpP->s.u16MagicField = 0;
#endif
               p->s.nsize += bp->s.nsize;
               p->s.s32Offset = bp->s.s32Offset;
               // Remark: Not required to set bp->s.u16MsgOffset in this case                
               // magic field loeschen
               bp->s.u16MagicField=0;    
#ifdef CHECK_MSG_POOL
               // DJ: Fill the guard of the new combined free entry
               /* update the information in the GUARD of the memory asigned to the message */
               pTmpP = (OSAL_MSG_tHeader*)((uintptr_t)p + ((p->s.nsize - 1) * sizeof(OSAL_MSG_tHeader)));
               pTmpP->s.nsize     = p->s.nsize;
               pTmpP->s.s32Offset = p->s.s32Offset;
#endif
            }
            else
            {
               // DJ: bp is NOT directly adjacent to the left neighbour, so we
               // DJ: only have to link p to the bp by setting the p offset to bp and 
               // DJ: we have to correct the guard accordingly
               p->s.s32Offset = POINTER_2_OFFSET(bp);
#ifdef CHECK_MSG_POOL
               pTmpP = (OSAL_MSG_tHeader*)((uintptr_t)p + ((p->s.nsize - 1) * sizeof(OSAL_MSG_tHeader)));
               pTmpP->s.s32Offset = p->s.s32Offset;
#endif
            }
            OSAL_M_TRACE_MESSAGEPOOL('d',pOsalData->rMsgPoolStruct.rHandleArray[u32Count].u32Offset,0,pSharedWrite->nCurSize*sizeof(OSAL_MSG_tHeader));
            /* set new free pointer */
            pSharedWrite->freeOffset = POINTER_2_OFFSET(p);
         }
         pOsalData->rMsgPoolStruct.rHandleArray[u32Count].enLocation = OSAL_EN_MEMORY_INVALID;
         pOsalData->rMsgPoolStruct.rHandleArray[u32Count].u32Offset  = 0;
      }
      else
      {
            // DJ: Message corrupted, we could not repair, so we can't delete this message
            // DJ: but we continue with other deletes and hope that not complete pool is damaged
            // DJ: IMPORTANT: In case that this happens frequently, we have a memory leak in the pool
      }
   }
   pOsalData->rMsgPoolStruct.u32HandleCount = 0;
}

   /************************************************************************
   *
   * FUNCTION:     OSAL_pu8MessageMap
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns memory address of the message
   *----------------------------------------------------------------------
   * PARAMETER:    (I) handle: message handle
   * PARAMETER:    (I) rights: access rights
   * RETURNVALUE:  address or OSAL_NULL
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tPU8 OSAL_pu8MessageContentGet(OSAL_trMessage handle, OSAL_tenAccess rights)
   {
 #ifdef CHECK_MSG_POOL_EXT
      tU32 Mod,tmp;
      tU32* pPtr;
      char buffer[100];
#endif
#ifdef CHECK_MSG_POOL
      OSAL_MSG_tHeader *pTmpBP;
#endif
      tU32 u32Offset;
      OSAL_MSG_tHeader *bp;
      tPU8 p2 = NULL;
      if(pOsalData->bCheckCcaMsg)
      {
         vTraceCcaMsg(&handle,0,NULL,"OSAL_pu8MessageContentGet");
      }
      /* local message */
      if (OSAL_EN_MEMORY_LOCAL == (char)handle.enLocation)
      { 
 #ifdef CHECK_MSG_POOL_EXT
         tU32* pPtr = (tU32*)handle.u32Offset;
         /* goto size info */
         pPtr--;
         memcpy(&tmp,pPtr,4);
         /* goto size offset */
         Mod = tmp%4;
         if(Mod == 0)
         { 
            if(MAGIC_MSG != *((tU32*)((uintptr_t)pPtr + tmp + 4)))
            {
                NORMAL_M_ASSERT_ALWAYS();
            }
         }
         else
         {
            if(MAGIC_MSG != *((tU32*)(handle.u32Offset + tmp + 12 - Mod)))
            {
                NORMAL_M_ASSERT_ALWAYS();
            }
         }
 #endif
         p2 =(tPU8) handle.u32Offset;
      }
      else if (OSAL_EN_MEMORY_SHARED <= (char)handle.enLocation)/* shared message */
      {
         if (/*OSAL_NULL == OSAL_MSG_pStructure || */OSAL_NULL == pSharedRead)
         {
            TraceString("OSAL_pu8MessageContentGet: OSAL_NULL == pSharedRead");
            vWritePrintfErrmem("OSAL_pu8MessageContentGet: OSAL_NULL == pSharedRead \n");
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
            goto pu8MessageContentGet_return; /*lint !e801*/
         }

         /* --Message pool check. */
  //       OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(OSAL_NULL);

         /* calculate header */
         bp = OFFSET_2_POINTER(handle.u32Offset);

         if(s32CheckPointer(bp,TRUE,TRUE) == OSAL_ERROR)
         {
            // DJ: Message corrupted, we could not repair, so we can't delete this message
            // DJ: but we continue and hope that not complete pool is damaged
            // DJ: IMPORTANT: In case that this happens frequently, we have a memory leak in the pool
             // Assert is already in s32CheckPointer()
            TraceString("OSAL_pu8MessageContentGet: Invalid message handle MemType:%d Offset:%d",
                        (char)handle.enLocation,handle.u32Offset);
            vWritePrintfErrmem("OSAL_pu8MessageContentGet: Invalid message handle MemType:%d Offset:%d \n",
                        (char)handle.enLocation,handle.u32Offset);
            OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
            goto pu8MessageContentGet_return; /*lint !e801*/
         }

#ifdef CHECK_MSG_POOL
         /* check Guard */
         pTmpBP = (OSAL_MSG_tHeader*)((uintptr_t)bp + ((bp->s.nsize-1) * sizeof(OSAL_MSG_tHeader)));
         if(pTmpBP->s.u16MagicField != OSAL_INV_MSG_MAGIC)
         {
             vPrintMsgInfo(bp,FALSE);
         }
#endif

         if (bp->s.s32Offset >= 0)
         {
            // DJ: This is a free entry, Error
            TraceString("OSAL_pu8MessageContentGet: Invalid Shared Memory pointer, free entry. MemType:%d Offset:%d",
                        (char)handle.enLocation,handle.u32Offset);
            vWritePrintfErrmem("OSAL_pu8MessageContentGet: Invalid Shared Memory pointer, free entry. MemType:%d Offset:%d \n",
                        (char)handle.enLocation,handle.u32Offset);
            OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
            goto pu8MessageContentGet_return; /*lint !e801*/
         }
         else
         {
            /* check to see if OSAL_s32MessageDelete was called for this message before */
            if(bp->s.u16MsgOffset == OSAL_DEL_MSG_MAGIC)
            {
               TraceString("OSAL_pu8MessageContentGet: Message already deleted. MemType:%d Offset:%d",
                        (char)handle.enLocation,handle.u32Offset);
               vWritePrintfErrmem("OSAL_pu8MessageContentGet: Message already deleted. MemType:%d Offset:%d \n",
                        (char)handle.enLocation,handle.u32Offset);
               OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
               goto pu8MessageContentGet_return; /*lint !e801*/
            }
         }

         u32Offset = (handle.u32Offset + 1) * sizeof( OSAL_MSG_tHeader ) +
            sizeof( OSAL_MSG_tSharedHeader );

         if (OSAL_EN_READONLY == rights)
         {
            p2 = (tPU8) pSharedRead + u32Offset;
         }
         else
         {
            p2 = (tPU8) pSharedWrite + u32Offset;
         }
         if((OSAL_tenMemoryLocation)(OSAL_EN_MEMORY_SHARED+1) == (char)handle.enLocation)
         {
            trLargeCcaMsg* prMsg = (trLargeCcaMsg*)p2;/*lint !e826 */
            tS32 s32Idx = 0;
            if(prMsg->s32Pid[s32Idx] != OSAL_ProcessWhoAmI())s32Idx = 1;

            if(prMsg->s32Pid[s32Idx] == 0)
            {
               prMsg->s32Pid[s32Idx] = OSAL_ProcessWhoAmI();
               prMsg->pAdress[s32Idx] = vpMmapShmToPrc(prMsg->cShmName,prMsg->u32Size,FALSE);
               if(u32OsalSTrace & 0x00000100)
               {
#ifdef NO_PRIxPTR        
                  TraceString("OSAL_pu8MessageContentGet Map to PID:%d TID:%d Size:%d Shm:0x%x -> New:0x%x ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->u32Size,(uintptr_t)prMsg->pAdress[0],(uintptr_t)prMsg->pAdress[1]);
#else
                  TraceString("OSAL_pu8MessageContentGet Map to PID:%d TID:%d Size:%d Shm:0x%"PRIxPTR" -> New:0x%"PRIxPTR" ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->u32Size,(uintptr_t)prMsg->pAdress[0],(uintptr_t)prMsg->pAdress[1]);
#endif            
               }     
            }
            else
            {
               if(u32OsalSTrace & 0x00000100)
               {
#ifdef NO_PRIxPTR        
                  TraceString("OSAL_pu8MessageContentGet PID:%d TID:%d Size:%d Shm:0x%x ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->u32Size,(uintptr_t)prMsg->pAdress[s32Idx]);
#else
                  TraceString("OSAL_pu8MessageContentGet PID:%d TID:%d Size:%d Shm:0x%"PRIxPTR" ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),prMsg->u32Size,(uintptr_t)prMsg->pAdress[s32Idx]);
#endif            
               }
            }
            p2 = (tPU8)prMsg->pAdress[s32Idx];
         }
      }
      else /* invalid */
      {
         TraceString("OSAL_pu8MessageContentGet: Invalid message handle. MemType:%d",(char)handle.enLocation);
         vWritePrintfErrmem("OSAL_pu8MessageContentGet: Invalid message handle. MemType:%d\n",(char)handle.enLocation);
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
      }

pu8MessageContentGet_return:
      if(u32OsalSTrace & 0x00000100)
      {
#ifdef NO_PRIxPTR        
            TraceString("OSAL_pu8MessageContentGet TID:%d Offset:0x%x",OSAL_ThreadWhoAmI(),handle.u32Offset,p2);
#else
            TraceString("OSAL_pu8MessageContentGet TID:%d Offset:0x%"PRIxPTR" returns 0x%"PRIxPTR" ",OSAL_ThreadWhoAmI(),handle.u32Offset,p2);
#endif
      }
      return p2;
   }


   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolGetAbsoluteSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the absolute size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolGetAbsoluteSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         
         s32ReturnValue = OSAL_s32MessagePoolGetAbsoluteSizeIntern();
         if (s32ReturnValue == OSAL_ERROR)
         {
            u32ErrorCode=OSAL_E_UNKNOWN;
         }

         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolGetCurrentSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the current size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32MessagePoolGetCurrentSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
   
         s32ReturnValue = OSAL_s32MessagePoolGetCurrentSizeIntern();

         if (s32ReturnValue == OSAL_ERROR)
         {
            u32ErrorCode=OSAL_E_UNKNOWN;
         }

         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      }
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32MessagePoolGetMinimalSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the minmal size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   OSAL_DECL tS32 OSAL_s32MessagePoolGetMinimalSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         s32ReturnValue = OSAL_s32MessagePoolGetMinimalSizeInternal();
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_u32GetMaxMessageSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the maximal message size
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   OSAL_DECL tU32 OSAL_u32GetMaxMessageSize( tVoid )
   {
      tU32 u32ReturnValue=(tU32) OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         if (pSharedRead)
            u32ReturnValue=pSharedRead->nMaxMessageSize * sizeof(OSAL_MSG_tHeader);
         else
            u32ErrorCode=OSAL_E_UNKNOWN;
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      if(u32OsalSTrace & 0x00000100)
      {
         TraceString("OSAL_u32GetMaxMessageSize TID:%d returns %d",OSAL_ThreadWhoAmI(),u32ReturnValue);
      }
      return u32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_PrintMessageList
   *----------------------------------------------------------------------
   * DESCRIPTION:  undocumented function: prints message list
   *----------------------------------------------------------------------
   * PARAMETER:    (I) fd: Osal file descriptor
   * RETURNVALUE:  void
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tVoid OSAL_vPrintMessageList(OSAL_tIODescriptor fdesc)
   {
      tChar szBuffer[1024];
      OSAL_MSG_tHeader *p;
      tU32 nsize;
      tU32 u32AbsSize=0,u32CurSize=0;
      ((void)fdesc);
      /* --Check if semaphore handle is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();

         if (pSharedRead) 
         {
            nsize = pSharedRead->nAbsSize;
            u32AbsSize=pSharedRead->nAbsSize * sizeof(OSAL_MSG_tHeader);
            u32CurSize=pSharedRead->nCurSize * sizeof(OSAL_MSG_tHeader);
            TraceString("Blocklist: Absolute size: %d, Current size: %d\r\n",(int)u32AbsSize,(int)u32CurSize);              


            for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
            {
               if(s32CheckPointer(p,TRUE,FALSE) == OSAL_ERROR)
               {

#ifdef NO_PRIxPTR
                  TraceString("!!! ERROR: Real 0x%x, Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p),  p->s.nsize);
#else
                  TraceString("!!! ERROR: Real 0x%" PRIxPTR ", Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p),  p->s.nsize);
#endif
                  vLeaveCriticalSectionCre();
                  return;
               }
				  

               if(p->s.s32Offset != -1)
#ifdef NO_PRIxPTR
                  OSAL_s32PrintFormat( szBuffer, "Free block: Real 0x%, Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p,(tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p), 
                  (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) - p->s.u16MsgOffset, p->s.nsize);
#else
                  OSAL_s32PrintFormat( szBuffer, "Free block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p,(tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p), 
                  (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) - p->s.u16MsgOffset, p->s.nsize);
#endif
               else
               {
                  if (p->s.u16MsgOffset == OSAL_DEL_MSG_MAGIC)
                  {
#ifdef NO_PRIxPTR
                     OSAL_s32PrintFormat( szBuffer, "Deleted marked block: Real 0x%x, Offset %4d (0x%x), Size (incl. u16MsgOffset) %lu, Blocksize %d \r\n", 
                                         (uintptr_t) p, (tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p),
                                         (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) /* we cannot subtract p->s.u16MsgOffset here */ ,  p->s.nsize);
#else
                     OSAL_s32PrintFormat( szBuffer, "Deleted marked block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size (incl. u16MsgOffset) %lu, Blocksize %d \r\n", 
                                         (uintptr_t) p, (tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p),
                                         (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) /* we cannot subtract p->s.u16MsgOffset here */ ,  p->s.nsize);
#endif
                  }
                  else
                  {
#ifdef NO_PRIxPTR
                  OSAL_s32PrintFormat( szBuffer, "Used block: Real 0x%x, Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p, (tU32)POINTER_2_OFFSET(p), (tU32)POINTER_2_OFFSET(p),
                  (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) - p->s.u16MsgOffset, (int) p->s.nsize);
#else
                  OSAL_s32PrintFormat( szBuffer, "Used block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p, (tU32)POINTER_2_OFFSET(p), (tU32)POINTER_2_OFFSET(p),
                  (tU32) p->s.nsize*sizeof(OSAL_MSG_tHeader) - p->s.u16MsgOffset, (int) p->s.nsize);
#endif
                  }
               }  

                TraceString(szBuffer);
            }

            // print memory leaks
            OSAL_vPrintMemoryLeaks();

         }
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      }
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_vCheckMessagePool
   *----------------------------------------------------------------------
   * DESCRIPTION:  undocumented function: checks memory
   *----------------------------------------------------------------------
   * RETURNVALUE:  void
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32CheckMessagePool(void)
   {
      OSAL_MSG_tHeader *p;
      unsigned int nsize;
      tS32 s32Ret = OSAL_OK;

      if (/*OSAL_NULL == OSAL_MSG_pStructure || */OSAL_NULL == pSharedRead)s32Ret = OSAL_ERROR;
      if(s32Ret == OSAL_OK)
      {
        //  pTemp = pPoolMem->pSharedRead;
        nsize = pSharedRead->nAbsSize;
      /* Enter critical section */
 //     vEnterCriticalSectionCre();
        for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
      {
         if (p->s.u16MagicField != OSAL_MSG_MAGIC)
         {
            /* Leave critical section */
               //s32SemOp( 1 );
            TraceString("Message pool check failed.");
            vWritePrintfErrmem("Message pool check failed.\n");
            vLeaveCriticalSectionCre();
            s32Ret = OSAL_ERROR;
            return s32Ret;
         }
      }
      /* Leave critical section */
//      vLeaveCriticalSectionCre();
     }
      return s32Ret;
   }

   tVoid OSAL_vSetCheck(tBool bCheck)
   {
      /* --Check if semaphore is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         if (bCheck == TRUE)
         {
               TraceString("Start Message Pool Checking...\n");   /*lint !e641 */  // Declaration von OSAL-Typen stimmt nicht...
               pOsalData->rMsgPoolStruct.bCheck = TRUE;
         }
         else
         {

              TraceString("Stop Message Pool Checking...\n");    /*lint !e641 */  // Declaration von OSAL-Typen stimmt nicht...
              pOsalData->rMsgPoolStruct.bCheck = FALSE;
         }
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      }
   }

   tVoid OSAL_vSetMessageTrace(OSAL_tIODescriptor fdesc) 
   {
      ((void)fdesc);
      /* --Check if semaphore is available. */
      if (MsgPoolSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSectionCre();
         /* --Write Header. */
         TraceMsgPoolData("%10s\t%10s\t%c\t%10s\t%10s\t%10s\t%10s\t%10s\r\n",
            "elapsed","tid",'?',"at","size","remain","max. free","max. used");
         /* --Leave critical section. */
         vLeaveCriticalSectionCre();
      }
   }

   tVoid vGetSpace(tPU32 pu32MaxFree, tPU32 pu32MaxUsed) 
   {
      OSAL_MSG_tHeader *p;    
      tU32 nsize = pSharedRead->nAbsSize;
      /* --Set to zero. */
      *pu32MaxFree=*pu32MaxUsed=0;

      for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
         /* --Free? */
         // DJ: We also count entries marked to be deleted by s32MessageDelete as free
         if(p->s.s32Offset != -1)
            *pu32MaxFree=(p->s.nsize>*pu32MaxFree ? p->s.nsize : *pu32MaxFree);
         else /* --Used. */
            *pu32MaxUsed=(p->s.nsize>*pu32MaxUsed ? p->s.nsize : *pu32MaxUsed);

      *pu32MaxFree*=sizeof(OSAL_MSG_tHeader);
      *pu32MaxUsed*=sizeof(OSAL_MSG_tHeader);
   }

   tU32 OSAL_u32GetMessageSize(OSAL_trMessage handle)
   {
      OSAL_MSG_tHeader *bp;
      tU32 u32Ret = 0;
      trLargeCcaMsg* prMsg = NULL;
      /* funktioniert nicht bei lokalen Nachrichten */
      if (OSAL_EN_MEMORY_LOCAL == (char)handle.enLocation)
      { 
           goto u32GetMessageSize_return; /*lint !e801*/
      }
      else if (OSAL_EN_MEMORY_SHARED <= (char)handle.enLocation)/* shared message */
      {
         if (/*OSAL_NULL == OSAL_MSG_pStructure || */OSAL_NULL == pSharedRead)
         {
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
            goto u32GetMessageSize_return;  /*lint !e801*/
         }

         /* --Message pool check. */
         OSAL_M_RETURN_ON_FAILEDMESSAGEPOOL(OSAL_NULL);

         /* calculate header */
         bp = OFFSET_2_POINTER(handle.u32Offset);

         if (bp->s.u16MagicField != OSAL_MSG_MAGIC)
         {
            TraceString("OSAL_u32GetMessageSize: Invalid shared memory pointer (MAGIC-FIELD).");
            vWritePrintfErrmem("OSAL_u32GetMessageSize: Invalid shared memory pointer (MAGIC-FIELD).\n");
            OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
            goto u32GetMessageSize_return;  /*lint !e801*/
         }
         if(bp->s.u16MsgOffset == OSAL_DEL_MSG_MAGIC)
         {
            TraceString("OSAL_u32GetMessageSize: Already deleted shared memory pointer (MAGIC-FIELD) .");
            vWritePrintfErrmem("OSAL_u32GetMessageSize: Already deleted shared memory pointer (MAGIC-FIELD).\n");
            OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
            goto u32GetMessageSize_return;  /*lint !e801*/
         }
         else
         {
            u32Ret = (tU32) bp->s.nsize*sizeof(OSAL_MSG_tHeader) - GARD_BYTES - bp->s.u16MsgOffset;
         }
         /* check for separate shared memory */
         if((OSAL_tenMemoryLocation)(OSAL_EN_MEMORY_SHARED+1) == (char)handle.enLocation)
         {
           tU32 u32Offset = (handle.u32Offset + 1) * sizeof( OSAL_MSG_tHeader ) + sizeof( OSAL_MSG_tSharedHeader );
           prMsg = (trLargeCcaMsg*)((uintptr_t)pSharedWrite + u32Offset);
           u32Ret =  prMsg->u32Size;
           tS32 s32Idx = 0;
           if(prMsg->s32Pid[s32Idx] != OSAL_ProcessWhoAmI())s32Idx = 1;

           if(prMsg->s32Pid[s32Idx] == 0)
           {
               prMsg->s32Pid[s32Idx] = OSAL_ProcessWhoAmI();
               prMsg->pAdress[s32Idx] = vpMmapShmToPrc(prMsg->cShmName,prMsg->u32Size,FALSE);
           }
           if(u32OsalSTrace & 0x00000100)
           {
#ifdef NO_PRIxPTR        
              TraceString("OSAL_u32GetMessageSize Map to PID:%d TID:%d Shm:0x%x ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),(uintptr_t)prMsg->pAdress[s32Idx]);
#else
              TraceString("OSAL_u32GetMessageSize Map to PID:%d TID:%d Shm:0x%"PRIxPTR" ",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),(uintptr_t)prMsg->pAdress[s32Idx]);
#endif            
           }
        }
      }
      else /* invalid */
      {
         TraceString("OSAL_u32GetMessageSize: Invalid message handle.");
         vWritePrintfErrmem("OSAL_u32GetMessageSize:Invalid message handle. \n");
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
         goto u32GetMessageSize_return;  /*lint !e801*/
      }

u32GetMessageSize_return:
      if(u32OsalSTrace & 0x00000100)
      {
#ifdef NO_PRIxPTR        
         TraceString("OSAL_u32GetMessageSize TID:%d Size:%d Offset:0x%x",OSAL_ThreadWhoAmI(),u32Ret,handle.u32Offset);
#else
         TraceString("OSAL_u32GetMessageSize TID:%d Size:%d Offset:0x%"PRIxPTR" ",OSAL_ThreadWhoAmI(),u32Ret,handle.u32Offset);
#endif
      }
      return u32Ret;
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemCreate
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
  static tS32 s32SemCreate( tVoid )
   {
      tS32  s32RetVal = OSAL_ERROR;

      if((s32RetVal = OSAL_s32SemaphoreCreate( OSAL_MSG_CreSemName,&hSemMsgPoolCre,1)) == OSAL_OK)
      {
        if((s32RetVal = OSAL_s32SemaphoreCreate( OSAL_MSG_DelSemName,&hSemMsgPoolDel,1)) == OSAL_OK)
        {
            s32RetVal = OSAL_OK;
        }
      }
      return s32RetVal;
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemOpen
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemOpen( tVoid )
   {
      tS32  s32RetVal = OSAL_ERROR;
 
      if((hSemMsgPoolCre == OSAL_C_INVALID_HANDLE)&&(hSemMsgPoolDel == OSAL_C_INVALID_HANDLE))
      {
        if((s32RetVal = OSAL_s32SemaphoreOpen( OSAL_MSG_CreSemName,&hSemMsgPoolCre)) == OSAL_OK)
        {
            if((s32RetVal = OSAL_s32SemaphoreOpen( OSAL_MSG_DelSemName,&hSemMsgPoolDel)) == OSAL_OK)
            {
               s32RetVal = OSAL_OK;
            }
        }
      }
      else
      {
         s32RetVal = OSAL_OK;
      }
      return s32RetVal;
   }
   
  /************************************************************************
   *
   * FUNCTION:     s32SemClose
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemClose( tVoid )
   {
      tS32  s32RetVal = OSAL_ERROR;
      if((s32RetVal = OSAL_s32SemaphoreClose(hSemMsgPoolCre)) == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      if((s32RetVal = OSAL_s32SemaphoreClose(hSemMsgPoolDel)) == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      return s32RetVal;
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemDelete( tVoid )
   {
      tS32  s32RetVal = OSAL_ERROR;
      if((s32RetVal = OSAL_s32SemaphoreDelete(OSAL_MSG_CreSemName)) == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      if((s32RetVal = OSAL_s32SemaphoreDelete(OSAL_MSG_DelSemName)) == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      return s32RetVal;
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemOp
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 20.12.12  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemOp( int iOperation, int iType )
   {
      tS32  s32RetVal = OSAL_ERROR;
      OSAL_tSemHandle hSem = 0;
      
      if(iType == 0)
      {
        hSem = hSemMsgPoolCre;
      }
      else
      {
        hSem = hSemMsgPoolDel;
      }
      
      
      if ( iOperation == -1 )
      {
         s32RetVal = OSAL_s32SemaphoreWait(hSem,OSAL_C_U32_INFINITE );
      }
      else
      {
         s32RetVal = OSAL_s32SemaphorePost(hSem);
      }
      return s32RetVal;
   }


   static tVoid vEnterCriticalSectionCre() 
   {
      s32SemOp(-1,0);
   }

   static tVoid vLeaveCriticalSectionCre() 
   {
      s32SemOp(+1,0);
   }

   static tVoid vEnterCriticalSectionDel() 
   {
      s32SemOp(-1,1);
   }

   static tVoid vLeaveCriticalSectionDel() 
   {
      s32SemOp(+1,1);
   }

#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalmsgpool.c
|-----------------------------------------------------------------------*/
