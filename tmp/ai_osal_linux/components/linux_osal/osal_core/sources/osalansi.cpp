/*****************************************************************************
| FILE:         osalansi.c
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Event-Functions.
|                
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"
#include "exception_handler.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#define MIN(x,y) ((x)<(y)?(x):(y))

// #define ASSERT_BUFFER_SIZE    MAX_TRACE_SIZE // errmmem buffer is smaller!

#define OUTPUT_BUFFER_SIZE    236
// ERRMEM_MAX_ENTRY_LENGTH (=128) - header for trace
#define ASSERT_BUFFER_SIZE    200

// Line 1
#define ASSERT_BUFFER_MODE_LINE1_SIZE  (0x03)
#define ASSERT_BUFFER_EXPR_LINE1_SIZE  (ASSERT_BUFFER_SIZE - ASSERT_BUFFER_MODE_LINE1_SIZE - 1)

// Line 2
#define ASSERT_PROC_NAME_SIZE   16
#define ASSERT_THREAD_NAME_SIZE 16
#define ASSERT_LINE_NUMBER_SIZE  4
#define ASSERT_BUFFER_EXPR_LINE2_SIZE (ASSERT_BUFFER_SIZE - 1 - ASSERT_PROC_NAME_SIZE - \
                                       ASSERT_THREAD_NAME_SIZE - ASSERT_LINE_NUMBER_SIZE - 1)


#define ASSERT_INITIATOR_OSAL       0x00
// #define ASSERT_INITIATOR_STL        0x01

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
char OSAL_vAssert_buf[ OUTPUT_BUFFER_SIZE ];


#ifdef LIBUNWIND_USED
extern void vCheckLibUnwind(void);
extern tU32 u32Unwind(void** addr,tU32 u32Size);
extern int s32UnwindInstalled;
#endif

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

void vWriteUtcTime(int fd)
{
   struct tm mytm;
   int i = 0;
   int old_fd = fd;
   char Buffer[100] = {0};
   time_t secSinceEpoch = time(NULL);

   if(fd == -1)
   {
      fd = open("/dev/errmem", O_WRONLY);
   }

   if(fd != -1)
   {
      Buffer[0] = 0;
      if( NULL != gmtime_r(&secSinceEpoch, &mytm))
      {
         i = snprintf(&Buffer[1],99,"OSAL UTC %d.%d.%d  %d:%d:%d \n",mytm.tm_mday,mytm.tm_mon + 1,mytm.tm_year,mytm.tm_hour,mytm.tm_min,mytm.tm_sec);
      }
      else
      {
         i = snprintf(&Buffer[1],99,"OSALElapsed Time %d \n",(int)secSinceEpoch);
      }
      write(fd, Buffer, i);
      if(old_fd == -1)
      {
         close(fd);
      }
   }
}




/******************************************************FunctionHeaderBegin******
 * FUNCTION    : bGetAssertBehaviour
 * CREATED     : 2009-03-11
 * AUTHOR      : TMS-Rrd
 * DESCRIPTION : get the behaviour to follow after output of assert message
 * SYNTAX      : tVoid teAssertMode bGetAssertBehaviour(tU32 line)
 * ARGUMENTS   : line no
 *                       
 * RETURN VALUE: assert behaviour for current assert 
 *
 * NOTES       :  
 *******************************************************FunctionHeaderEnd******/
static teAssertMode bGetAssertBehaviour(tU32 line)
{
teAssertMode eRes;

    /* the behaviour is controlled by assert mode and SW type (release or debug)

       Release: FATAL_M_ASSERT() is controlled by assert mode; NORMAL_M_ASSERT() triggers no action

       Debug: FATAL_M_ASSERT() and NORMAL_M_ASSERT() have the same behaviour. It is controlled by assert mode.

       By default assert mode is set to ASSERTMODE_RESET.
    */

/* if release SW */
#ifndef __DEBUG_BUILD__
    /* only if it is a FATAL_M_ASSERT() */
    if  (0 == (line & 0x80000000))
#else
    (void)line;
#endif
    {
        eRes = (teAssertMode)pOsalData->u32AssertMode;
    }
#ifndef __DEBUG_BUILD__
    else
    {
        eRes = ASSERTMODE_TASKNOACTION;
    }
#endif

	return (eRes);
}


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : OSAL_trace_callstack 
 * CREATED     : 2006-11-23
 * AUTHOR      : Ulrich Schulz / TMS
 * DESCRIPTION : function traces out the callstack information to TTFis
 * SYNTAX      : static void exc_manager_trace_cs(void)
 * ARGUMENTS   : -
 * RETURN VALUE: -
 *
 * NOTES       : -
 *
 *******************************************************FunctionHeaderEnd******/
 
#include <execinfo.h>

void OSAL_generate_callstack(tBool bTrace)
{
   #define MAX_DEEP 10

   int  i, CallStackDeep;
   void *buffer[MAX_DEEP];
   char **strings;

#ifdef LIBUNWIND_USED
   if(s32UnwindInstalled == 0)
   {
     vCheckLibUnwind();
   }
   if((pOsalData->u32UseLibunwind)&&(s32UnwindInstalled == 2))
   {
      TraceString("OSAL ASSERT using libunwind backtrace\n");
      CallStackDeep = u32Unwind(buffer, MAX_DEEP);
   }
   else
#endif	   
   {
      TraceString("OSAL ASSERT using glibc backtrace\n");
      CallStackDeep = backtrace(buffer, MAX_DEEP);
   }
   strings       = backtrace_symbols( buffer, CallStackDeep );

   if( strings == NULL )
   {
      OSAL_vAssert_buf[0] = 0x07;
      sprintf((char*)(OSAL_vAssert_buf+1), "backtrace error");
      if(bTrace)LLD_vTrace( (int)TR_CLASS_ASSERT, (int)TR_LEVEL_FATAL, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf) );
      vWriteToErrMem( TR_CLASS_ASSERT, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf), 0 );
   }
   else
   {
      for( i = 1; i < CallStackDeep; i++ ) // 1: we don't want to see the function call of OSAL_trace_callstack()
      {
         memset( OSAL_vAssert_buf, 0, OUTPUT_BUFFER_SIZE );
         OSAL_vAssert_buf[0] = 0x07;

         strncpy( OSAL_vAssert_buf+1, strings[i], OUTPUT_BUFFER_SIZE-2 );
         OSAL_vAssert_buf[ OUTPUT_BUFFER_SIZE-1 ] = 0;

#ifdef SHORT_TRACE_OUTPUT 
         if(bTrace)LLD_vTrace( (int)TR_CLASS_ASSERT, (int)TR_LEVEL_FATAL, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf) );
         vWriteToErrMem( TR_CLASS_ASSERT, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf), 0 );
#else
         if(bTrace)TraceString("callstack= [%s]",OSAL_vAssert_buf+1);
         vWritePrintfErrmem("callstack= [%s] \n",OSAL_vAssert_buf+1);
#endif
         if(strlen(strings[i]) > OUTPUT_BUFFER_SIZE)
         {
            memset( OSAL_vAssert_buf, 0, OUTPUT_BUFFER_SIZE );
            OSAL_vAssert_buf[0] = 0x07;

            strncpy( OSAL_vAssert_buf+4, strings[i]+OUTPUT_BUFFER_SIZE, OUTPUT_BUFFER_SIZE-5 );
            OSAL_vAssert_buf[ OUTPUT_BUFFER_SIZE-1 ] = 0;

#ifdef SHORT_TRACE_OUTPUT 
            if(bTrace)LLD_vTrace( (int)TR_CLASS_ASSERT, (int)TR_LEVEL_FATAL, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf) );
             vWriteToErrMem( TR_CLASS_ASSERT, OSAL_vAssert_buf, strlen(OSAL_vAssert_buf), 0 );
#else
            if(bTrace)TraceString("callstack= [%s]",OSAL_vAssert_buf+1);
            vWritePrintfErrmem("callstack= [%s] \n",OSAL_vAssert_buf+1);
#endif 
         }
      }
   }
   free(strings);
}

void OSAL_generate_callstack_secure(tBool bTrace)
{
#if defined __i386__ || defined __x86_64__ || defined __arm__ || defined __arm_64__ || defined __aarch64__
  int lock_return = exception_handler_lock();
  if (lock_return != 0)
  {
     vWritePrintfErrmem("OSAL_generate_callstack_secure -> exception_handler_lock failed for PID:%d \n",getpid());
  }
#else
#error "unknown compiler environment - don't know wheather to use the exception handler or not ..."
#endif
    OSAL_generate_callstack(bTrace);
#if defined __i386__ || defined __x86_64__ || defined __arm__ || defined __arm_64__ || defined __aarch64__
  if (lock_return == 0)
  {
     exception_handler_unlock();
  }
  else
  {
     vWritePrintfErrmem("OSAL_generate_callstack_secure -> exception_handler_unlock failed for PID:%d \n","exception_handler_lock",getpid());
  }
#else
#error "unknown compiler environment - don't know wheather to use the exception handler or not ..."
#endif
}

void OSAL_trace_callstack(void)
{
   OSAL_generate_callstack_secure(FALSE);
}

/*****************************************************************************
*
* FUNCTION:    OSAL_vAssert_Trace
*
* DESCRIPTION: This function is for tracing the Assert reasons.                
*
* PARAMETER:   const char *expr, 
*              const char *file, 
*              tU32 line, 
*              int errmem
*
* RETURNVALUE: none.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.06  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static void OSAL_vAssert_Trace(const char *expr, const char *file, tU32 line,  char initiator)
{
  unsigned int buflen = 0;
  unsigned int exprlen = strlen(expr);


#if defined __i386__ || defined __x86_64__ || defined __arm__ || defined __arm_64__ || defined __aarch64__
  int size;
  int lock_return = exception_handler_lock();
  if (lock_return != 0)
  {
     size = snprintf(OSAL_vAssert_buf,ASSERT_BUFFER_SIZE,"PID:%d ASSERT Print whithout lock \n",getpid());
     vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,OSAL_vAssert_buf,size,OSAL_STRING_OUT);
  }
#else
#error "unknown compiler environment - don't know wheather to use the exception handler or not ..."
#endif
  /* if assert is followed by reset */
  
  /* first line: ASSERT OSAL mode=NORMAL  behave=TASK_NOACTION, expr=[ALWAYS] */
  memset(OSAL_vAssert_buf, 0, ASSERT_BUFFER_SIZE);
  OSAL_vAssert_buf[0] = initiator;
  OSAL_vAssert_buf[1] = (char)((line & 0x80000000) != 0);
  // OSAL_vAssert_buf[2] = (char)pOsalData->eAssertMode;
  OSAL_vAssert_buf[2] = (char)bGetAssertBehaviour(line); // use assert mode coresponding to the line (high bit) coding and debug/release mode
  buflen += ASSERT_BUFFER_MODE_LINE1_SIZE;
  
  strncpy( &OSAL_vAssert_buf[buflen], expr, MIN(ASSERT_BUFFER_EXPR_LINE1_SIZE, exprlen) );
  buflen += ASSERT_BUFFER_EXPR_LINE1_SIZE;
  
  OSAL_vAssert_buf[ASSERT_BUFFER_SIZE - 1] = 0;

#ifdef SHORT_TRACE_OUTPUT 
  LLD_vTrace( (int)TR_CLASS_ASSERT, (int)TR_LEVEL_FATAL, OSAL_vAssert_buf, buflen );
  vWriteToErrMem( TR_CLASS_ASSERT, OSAL_vAssert_buf, buflen, 0 );
#else
  tCString Type,Mode = "SYSTEM_RESET";;
  if(OSAL_vAssert_buf[2] == 0x01)
  { Mode = "SYSTEM_RESET";  }
  else if(OSAL_vAssert_buf[2] == 0x02)
  { Mode = "SYSTEM_SUSPEND";  }
  else if(OSAL_vAssert_buf[2] == 0x03)
  { Mode = "TASK_SUSPEND";  }
  else if(OSAL_vAssert_buf[2] == 0x04)
  { Mode = "TASK_NOACTION";  }
  if  (0 == (line & 0x80000000))
  {
     Type = "FATAL";
  }
  else
  {
     Type = "NORMAL";
  }
  TraceString("OSAL type=%s behave=%s, expr=[%s]",Type,Mode,expr);
  vWritePrintfErrmem("OSAL type=%s behave=%s, expr=[%s] \n",Type,Mode,expr);
#endif

  /* second line: ASSERT Line=302, File=[x:/di_tengine_os/products/TEngine_driver/spm/tengi.... */
  memset(OSAL_vAssert_buf, 0, ASSERT_BUFFER_SIZE);
  OSAL_vAssert_buf[0] = 0x06; // 6 instead of 5 for proc ans task
  buflen = 1;

  // process name  16 Bytes
//   (void) snprintf( &OSAL_vAssert_buf[buflen], ASSERT_PROC_NAME_SIZE, "P_%d", OSAL_ProcessWhoAmI() );

  char* pTmp = strrchr(commandline, '/');
  if(pTmp)
  {
     (void) snprintf( &OSAL_vAssert_buf[buflen], ASSERT_PROC_NAME_SIZE, "%s",pTmp+1);
  }
  else
  {
     (void) snprintf( &OSAL_vAssert_buf[buflen], ASSERT_PROC_NAME_SIZE, "%s",commandline);
  }
  buflen += ASSERT_PROC_NAME_SIZE;

  // task name  16 Bytes
   if( bGetThreadNameForTID(&OSAL_vAssert_buf[buflen], ASSERT_THREAD_NAME_SIZE, OSAL_ThreadWhoAmI() ) == FALSE )
   {
      (void) snprintf(&OSAL_vAssert_buf[buflen], ASSERT_THREAD_NAME_SIZE, "TID=%d", OSAL_ThreadWhoAmI() );
   }   
  buflen += ASSERT_THREAD_NAME_SIZE;

  OSAL_M_INSERT_T32(&OSAL_vAssert_buf[buflen], (line & (~0x80000000)));
  buflen  += ASSERT_LINE_NUMBER_SIZE;


  exprlen = strlen(file);
  if( exprlen >= ASSERT_BUFFER_EXPR_LINE2_SIZE )
  {
      file += exprlen - ASSERT_BUFFER_EXPR_LINE2_SIZE + 1;
  }
  strncpy( &OSAL_vAssert_buf[buflen], file, MIN(ASSERT_BUFFER_EXPR_LINE2_SIZE - 1, exprlen) );
  buflen += ASSERT_BUFFER_EXPR_LINE2_SIZE;
  
#ifdef SHORT_TRACE_OUTPUT 
  LLD_vTrace((int)TR_CLASS_ASSERT, (int)TR_LEVEL_FATAL, OSAL_vAssert_buf, buflen);
  vWriteToErrMem( TR_CLASS_ASSERT, OSAL_vAssert_buf, buflen, 0);
#else
  char Name[ASSERT_THREAD_NAME_SIZE];
  tU32 u32Val;
  if  (0 == (line & 0x80000000))
  {
      u32Val = line & 0x80000000;
  }
  else
  {
      u32Val = line & (~0x80000000);
  }

  bGetThreadNameForTID(&Name[0], ASSERT_THREAD_NAME_SIZE, OSAL_ThreadWhoAmI() ); 
  TraceString("Proc=[%s] Thread=[%s], Line=%u, File=[%s]",
              gpOsalProcDat->pu8AppName,Name,(unsigned int)u32Val,file);
  vWritePrintfErrmem("Proc=[%s] Thread=[%s], Line=%u, File=[%s] \n",
               gpOsalProcDat->pu8AppName,Name,u32Val,file);
#endif

  /* generate callstack only when lock is available */
  if(lock_return == 0)
  {
      OSAL_generate_callstack(TRUE);
  }	
#if defined __i386__ || defined __x86_64__ || defined __arm__ || defined __arm_64__ || defined __aarch64__
  if (lock_return == 0)
  {
     exception_handler_unlock();
  }
  else
  {
     size = snprintf(OSAL_vAssert_buf,ASSERT_BUFFER_SIZE,"PID:%d ASSERT Print whithout lock end \n",getpid());
     vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,OSAL_vAssert_buf,(int)strlen(OSAL_vAssert_buf),OSAL_STRING_OUT);
  }
#else
#error "unknown compiler environment - don't know wheather to use the exception handler or not ..."
#endif
}



/*****************************************************************************
*
* FUNCTION:    OSAL_vAssertFunction
*
* DESCRIPTION: This function is for Assertion handling
*
* PARAMETER:   const char *expr, 
*              const char *file, 
*              tU32 line, 
*
* RETURNVALUE: none.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.06  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void OSAL_vAssertFunction(const char* expr, const char* file, tU32 line)
{
#ifdef OSAL_ERRMEM_TIMESTAMP
    vWriteUtcTime(-1);
#endif
   // Assemble trace message
   OSAL_vAssert_Trace(expr, file, line, ASSERT_INITIATOR_OSAL);

   // Determine handling. In Debug and Release mode,
   // three modes are configurable :
   //   Reset (default)
   //   System suspend
   //   Task suspend
   switch ((bGetAssertBehaviour(line)))
   {
      case ASSERTMODE_RESET:
         TraceString("OSAL ASSERT will cause a reboot");
#ifdef OSAL_GEN3_SIM
         if((line & 0x80000000) == 0)
         {
            for(int i = 0; i < 4; i++)
            {
               TraceString("<<<< <<<< FATAL ASSERT <<<< <<<<");
               OSAL_s32ThreadWait(100);
            }
            /* only call reboot/exit for FATAL ASSERT */
	        /* set maker for valid eh_reboot call */
            pOsalData->bNoRebootCallstack = TRUE;
            eh_reboot();
         }
         else
         {
            TraceString("<<<< <<<< NORMAL ASSERT <<<< <<<<");
         }
         OSAL_s32ThreadWait(2000);
#else
         OSAL_s32ThreadWait(1000);
         /* set maker for valid eh_reboot call */
         pOsalData->bNoRebootCallstack = TRUE;
         eh_reboot();
#endif
         // Never should come here
         break;

      case ASSERTMODE_TASKSUSPEND:
         {
            OSAL_tThreadID tid= OSAL_ThreadWhoAmI();
            OSAL_s32ThreadSuspend(tid);
         }
         // Never should come here
         break;
      case ASSERTMODE_TASKNOACTION:
         // No action means return to caller
         return;
      case ASSERTMODE_SUSPEND:
      default:
         // Suspend system below
         break;
   }
}

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osalansi.c
|-----------------------------------------------------------------------*/

