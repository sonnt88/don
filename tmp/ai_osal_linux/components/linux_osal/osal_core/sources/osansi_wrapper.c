/************************************************************* +MHDR * .MA *

 .FILE             [ osansi.c  ] 

 .SW-COMPONENT      osal

 .DESCRIPTION
  This file contains the implementation of osal ansi wrapper functions

 .COPYRIGHT            [ (c) 2007 Robert Bosch GmbH, Hildesheim           ]
 .AUTHOR               [ F. Wiedemann                                     ]

 .HISTORY

  Date     | Name            Modification
 ----------|---------------------------------------------------------------
  02.02.07 | CM-DI/EST-Wg    include osal.h replaced
************************************************************** -MHDR * .ME */
//#define OSAL_S_IMPORT_INTERFACE_GENERIC
#define OSAL_S_IMPORT_INTERFACE_ANSI
#include "osal_if.h"

tS32 OSAL_s32Absolute(tS32 s32Value)
{
   return abs(s32Value);
}

