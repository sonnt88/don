/******************************************************************************
| FILE:         osalmempool.cpp
| PROJECT:      ADIT GEN1 
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the Memory Pool-Functions.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2009 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
//#define NO_OSAL_USED

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"


#ifdef NO_OSAL_USED

#define OSAL_S_IMPORT_INTERFACE_TYPES
#include "osal_if.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "pthread.h"
#include "fcntl.h"
#include <unistd.h>
#include "semaphore.h"
#include "sys/mman.h"
#include "osalmempool.h"
#include "errno.h"
#include "ostrace.h"
#include <sys/syscall.h>   /* For SYS_xxx definitions */

#define OSAL_STRING_OUT                0xf1

#include "adit-components/trace_interface.h"
#define TRACE_S_ALREADY_INCLUDE_TYPES

tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
{
   return TR_core_bIsClassSelected((U16)u32Class,(TR_tenTraceLevel)u32Level);
}

void LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
{
   TR_core_uwTraceOut(u32Length,u32Class,(TR_tenTraceLevel)u32Level,(U8*)pvData);
}

#else //NO_OSAL_USED

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "ostrace.h"
#endif//NO_OSAL_USED

#ifdef __cplusplus
extern "C" {
#endif 

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
#define POOLMAGIC       0x504F4F4C504F4F4C /* POOL Magic */
#define ALLOCMAGIC      0x414C4C4F414C4C4F /* Malloc Magic */
#else
#define POOLMAGIC       0x504F4F4C /* POOL Magic */
#define ALLOCMAGIC      0x414C4C4F /* Malloc Magic */
#endif

#define GETBLOCK_MPF_FAILED     0xb2 /* u32ErrorCode , OSAL_ThreadWhoAmI */
#define RELBLOCK_MPF_FAILED     0xb3 /* u32ErrorCode , OSAL_ThreadWhoAmI */



#define MPF_INFO                0xb6

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static tBool bPrintOwner = FALSE;
#ifdef NO_OSAL_USED
static tU32 u32Index = 0;
static tU32 s32OsalGroupId = 0;
#endif
/************************************************************************
| function definition (scope: global)
|-----------------------------------------------------------------------*/


/******************************************************************************
 * FUNCTION:      vTraceOsalMpf
 *
 * DESCRIPTION:   gets memory pool index for OSAL mempool
 *
 * PARAMETER:     TR_tenTraceLevel  trace level
 *                UB*               pointer to trace buffer
 *                int               size of trace buffer
 *               
 * RETURNVALUE:   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
void vTraceOsalMpf(TR_tenTraceLevel enLevel, tU8* buf, int size)
{
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MEMPOOL,enLevel) == TRUE)
   {
       LLD_vTrace(OSAL_C_TR_CLASS_SYS_MEMPOOL,enLevel,buf,size); /*lint -e1773*/
   }
}

uintptr_t GetMemPoolOffset(trHandleMpf* pHandle, uintptr_t* pU32Mem)
{
    return (uintptr_t)pU32Mem - (uintptr_t)pHandle->u32memstart;
}


/******************************************************************************
 * FUNCTION:      OSAL_u32MemPoolFixSizeCreate
 *
 * DESCRIPTION:   generates memory pool for OSAL
 *
 * PARAMETER:     tCString      Pool Name
 *                tU32          u32MpfBlockSize,
 *                tU32          u32MpfBlockCnt,
 *                trHandleMpf*  pointer to Pool handle structure
 *                tBool         Flag i f internal lock is needed
 *                
 *               
 * RETURNVALUE:   OSAL_ERROR or OSAL_OK
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.04.13  |   Initial revision                     | MRK2HI
 ***************** ************************************************************/
tS32 OSAL_u32MemPoolFixSizeCreate(tCString coszName, 
                                  tU32 u32MpfBlockSize,
                                  tU32 u32MpfBlockCnt,
                                  trHandleMpf* pHandle,
                                  tBool bLock,
                                  tBool bMalloc)
{
  tU32 blfsz;
  trOSAL_MPF* pPool = NULL;
  char cBuffer[32];

  /* determine pool element size */
  blfsz = u32MpfBlockSize % sizeof(uintptr_t);
  if(blfsz == 0)
  {
     blfsz = (tU32)u32MpfBlockSize + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  else
  {
     blfsz = (tU32)u32MpfBlockSize + (sizeof(uintptr_t) - blfsz) + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  /* extend each element with place for the owner PID */
  blfsz += sizeof(uintptr_t); /* PID info */

  /* use for create and open always a lock */
  snprintf(cBuffer,32,"/%s",coszName);
  pHandle->Sem = sem_open(cBuffer,O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
  if (pHandle->Sem == SEM_FAILED)
  {
#ifdef NO_OSAL_USED
     return OSAL_ERROR;
#else
   //  FATAL_M_ASSERT_ALWAYS();
/* workaround for recovery download start*/
      if(EEXIST == errno)
      {
         pHandle->Sem = sem_open(cBuffer, 0);
         if (pHandle->Sem == SEM_FAILED)
         {
             FATAL_M_ASSERT_ALWAYS();
         }
      }/* workaround for recovery download end */
#endif
  }
  else
  {
#ifndef NO_OSAL_USED
     if(s32OsalGroupId)
     {
        char cBuf[64];
        snprintf(cBuf,64,"/dev/shm/sem.%s",&cBuffer[1]);
        if(chown(cBuf,(uid_t)-1,s32OsalGroupId) == -1)
        {
            vWritePrintfErrmem("OSAL_u32MemPoolFixSizeCreate -> chown error %d \n",errno);
        }
        if(chmod(cBuf, OSAL_ACCESS_RIGTHS) == -1)
        {
            vWritePrintfErrmem("OSAL_u32MemPoolFixSizeCreate -> chmod error %d \n",errno);
        }
     }
#endif
  }

#ifdef NO_OSAL_USED
  int PoolDesc;
  void* pvMapAdress = NULL;
  tU32 u32MapOption = MAP_SHARED;
  tU32 u32Size = (u32MpfBlockCnt * blfsz) + sizeof(trOSAL_MPF);
  //get shared memory
  if( (PoolDesc = shm_open(coszName, O_RDWR|O_EXCL|O_CREAT|O_TRUNC,OSAL_ACCESS_RIGTHS)) != -1)    
  {
     if (ftruncate(PoolDesc,u32Size) == -1)
     {
        return OSAL_ERROR;
     }
     pPool = (trOSAL_MPF*)mmap(pvMapAdress,
                               u32Size,
                               PROT_READ | PROT_WRITE,
                               u32MapOption,
                               PoolDesc,
                               0);
     if((void*)pPool == MAP_FAILED)
     {
        return OSAL_ERROR;
     }
     memset(pPool, 0x00, u32Size);
     close(PoolDesc);
//     rLiShMemMap[u32Index].u32MemIdx  = (tU32)pPool;
//     rLiShMemMap[u32Index].u32PrcIdx  = getpid();
//     rLiShMemMap[u32Index].u32PrcCnt  = 1;
     u32Index++;
  }
  else
  {
     return OSAL_ERROR;
  }
#else
  if((pHandle->hShMem = OSAL_SharedMemoryCreate(coszName,
                                                OSAL_EN_READWRITE,
                                                (u32MpfBlockCnt * blfsz) + sizeof(trOSAL_MPF))) == OSAL_ERROR)
  {
    return OSAL_ERROR;
  }
  if((pPool = (trOSAL_MPF*)OSAL_pvSharedMemoryMap(pHandle->hShMem,
                                                  OSAL_EN_READWRITE,
                                                  (u32MpfBlockCnt * blfsz) + sizeof(trOSAL_MPF),
                                                  0)) == NULL)
  {
    return OSAL_ERROR;
  }
#endif
  /* Fill handle */
  pHandle->pMem            = (void*)pPool;
  pHandle->u32memstart     = (uintptr_t)pPool + sizeof(trOSAL_MPF);
  pHandle->u32memend       = (uintptr_t)pPool + sizeof(trOSAL_MPF) + (blfsz * u32MpfBlockCnt);

  pPool->bLock   = bLock;
  pPool->bMalloc   = bMalloc;

  /* fill internal structure */
  strncpy(pPool->szName,coszName,32);
  pPool->blfsz       = blfsz;
  pPool->mpfcnt      = u32MpfBlockCnt;
  pPool->freecnt     = pPool->mpfcnt;
  pPool->u32OpenCnt  = 1;
  pPool->u32MaxCount = 0;
  pPool->u32CurCount = 0;
  pPool->u32ErrCnt   = 0;
  pPool->u32nextIdx  = 0;

  /* prepare memory */
  uintptr_t *pMem;
  tU32 i;
  for(i = 0;i<pPool->mpfcnt;i++)
  {
     pMem = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (i * pPool->blfsz));
     /* initialize the PID place in the memory*/
     *pMem = 0;
     pMem++;
     /* mark each element as free */
     *pMem = FREEMARKER;
     /* set magic for pool memory at the end of each lement */
     pMem = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (((i+1) * pPool->blfsz) - sizeof(uintptr_t)));
     *pMem = POOLMAGIC;
  }
  
  sem_post(pHandle->Sem);

  return OSAL_OK;
}

tU32 u32CalculatePoolMemSize(tU32 u32MpfBlockSize,
                             tU32 u32MpfBlockCnt)
{
  tU32 	blfsz;
  /* determine pool element size */
  blfsz = u32MpfBlockSize % sizeof(uintptr_t);
  if(blfsz == 0)
  {
     blfsz = (tU32)u32MpfBlockSize + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  else
  {
     blfsz = (tU32)u32MpfBlockSize + (sizeof(uintptr_t) - blfsz) + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  /* extend each element with place for the owner PID */
  blfsz += sizeof(uintptr_t); /* PID info */
  
  return (u32MpfBlockCnt*blfsz) + sizeof(trOSAL_MPF);
  
}

tS32 OSAL_u32MemPoolFixSizeCreateOnMem(tCString coszName, 
                                  tU32 u32MpfBlockSize,
                                  trHandleMpf* pHandle,
                                  tBool bLock,
                                  void* pPoolMem,
								  tU32 u32Size)
{
  tU32 blfsz;
  trOSAL_MPF* pPool = NULL;
  char cBuffer[32];
  tU32 u32MpfBlockCnt;
   
  u32Size -= sizeof(trOSAL_MPF);
  /* determine pool element size */
  blfsz = u32MpfBlockSize % sizeof(uintptr_t);
  if(blfsz == 0)
  {
     blfsz = (tU32)u32MpfBlockSize + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  else
  {
     blfsz = (tU32)u32MpfBlockSize + (sizeof(uintptr_t) - blfsz) + (2*sizeof(uintptr_t))/*size info + magic field*/;
  }
  
  
  /* extend each element with place for the owner PID */
  blfsz += sizeof(uintptr_t); /* PID info */

  u32MpfBlockCnt = u32Size/blfsz;
  /* use for create and open always a lock */
  snprintf(cBuffer,32,"/%s",coszName);
  pHandle->Sem = sem_open(cBuffer,O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
  if (pHandle->Sem == SEM_FAILED)
  {
   //  FATAL_M_ASSERT_ALWAYS();
/* workaround for recovery download start*/
      if(EEXIST == errno)
      {
         pHandle->Sem = sem_open(cBuffer, 0);
         if (pHandle->Sem == SEM_FAILED)
         {
             FATAL_M_ASSERT_ALWAYS();
         }
      }/* workaround for recovery download end */
  }
  else
  {
     if(s32OsalGroupId)
     {
        char cBuf[64];
        snprintf(cBuf,64,"/dev/shm/sem.%s",&cBuffer[1]);
        if(chown(cBuf,(uid_t)-1,s32OsalGroupId) == -1)
        {
            vWritePrintfErrmem("OSAL_u32MemPoolFixSizeCreate -> chown error %d \n",errno);
        }
        if(chmod(cBuf, OSAL_ACCESS_RIGTHS) == -1)
        {
            vWritePrintfErrmem("OSAL_u32MemPoolFixSizeCreate -> chmod error %d \n",errno);
        }
     }
  }

  pHandle->hShMem = 0;
  pPool = (trOSAL_MPF*)pPoolMem;
  /* Fill handle */
  pHandle->pMem            = (void*)pPool;
  pHandle->u32memstart     = (uintptr_t)pPool + sizeof(trOSAL_MPF);
  pHandle->u32memend       = (uintptr_t)pPool + sizeof(trOSAL_MPF) + (blfsz * u32MpfBlockCnt);
  pPool->bMalloc   = TRUE;
  pPool->bLock   = bLock;

  /* fill internal structure */
  strncpy(pPool->szName,coszName,32);
  pPool->blfsz       = blfsz;
  pPool->mpfcnt      = u32MpfBlockCnt;
  pPool->freecnt     = pPool->mpfcnt;
  pPool->u32OpenCnt  = 1;
  pPool->u32MaxCount = 0;
  pPool->u32CurCount = 0;
  pPool->u32ErrCnt   = 0;
  pPool->u32nextIdx  = 0;

  /* prepare memory */
  uintptr_t *pMem;
  tU32 i;
  for(i = 0;i<pPool->mpfcnt;i++)
  {
     pMem = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (i * pPool->blfsz));
     /* initialize the PID place in the memory*/
     *pMem = 0;
     pMem++;
     /* mark each element as free */
     *pMem = FREEMARKER;
     /* set magic for pool memory at the end of each lement */
     pMem = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (((i+1) * pPool->blfsz) - sizeof(uintptr_t)));
     *pMem = POOLMAGIC;
  }
  
  sem_post(pHandle->Sem);

  return OSAL_OK;
}


tS32 OSAL_u32MemPoolFixSizeOpen(tCString coszName, 
                                trHandleMpf* pHandle)
{
  char cBuffer[32];
  trOSAL_MPF* pPool = NULL;
  sem_t*  Sem;
#ifndef NO_SIGNALAWARENESS
  tS32 s32Ret;
#endif

  /* use for create and open always a lock */
  snprintf(cBuffer,32,"/%s",coszName);
  Sem = sem_open(cBuffer, 0);
  if (Sem == SEM_FAILED)
  {
#ifdef NO_OSAL_USED
     return OSAL_ERROR;
#else
     FATAL_M_ASSERT_ALWAYS();
#endif
  }
#ifndef NO_SIGNALAWARENESS
  while(1)
  {
#endif
     s32Ret = sem_wait(Sem);
#ifndef NO_SIGNALAWARENESS
     if(s32Ret != 0)
     {
        /* check for incoming signal */
        if(errno != EINTR)
        {
           return OSAL_ERROR;
        }
     }
     else
     {
         break;
     }
  }
#endif


#ifdef NO_OSAL_USED
  int PoolDesc;
  int size;
  void* pvMapAdress = NULL;
  tU32 u32MapOption = MAP_SHARED;

/*  for(size = 0;size <u32Index;size++)
  {
    if(rLiShMemMap[u32Index].u32PrcIdx == getpid())
    {
        pPool = (void*)rLiShMemMap[u32Index].u32MemIdx;
        rLiShMemMap[u32Index].u32PrcCnt++;
    }
  }*/

  if(pPool == NULL)
  {
     /* memory has to be mapped for current window */
     if( (PoolDesc = shm_open(coszName, O_RDWR,OSAL_ACCESS_RIGTHS)) != -1)
     {
        pPool = (trOSAL_MPF*)mmap(pvMapAdress,
                                  sizeof(trOSAL_MPF),
                                  PROT_READ | PROT_WRITE,
                                  u32MapOption,
                                  PoolDesc,
                                  0);
        if((void*)pPool == MAP_FAILED)
        {
           close(PoolDesc);
           return OSAL_ERROR;
        }
        size = sizeof(trOSAL_MPF) + (pPool->blfsz * pPool->mpfcnt);
/*        if(munmap(pPool, sizeof(trOSAL_MPF)) != 0)
        {
         NORMAL_M_ASSERT_ALWAYS();
        }*/
        pPool = (trOSAL_MPF*)mmap(pvMapAdress,
                                  size,
                                  PROT_READ | PROT_WRITE,
                                  u32MapOption,
                                  PoolDesc,
                                  0);
        close(PoolDesc);
        if((void*)pPool == MAP_FAILED)
        {
           return OSAL_ERROR;
        }
      }
      else
      {
         return OSAL_ERROR;
      }
   }
#else
  if((pHandle->hShMem = OSAL_SharedMemoryOpen(coszName,
                                              OSAL_EN_READWRITE)) == OSAL_ERROR)
  {
      return OSAL_ERROR;
  }
  if((pPool = (trOSAL_MPF*)OSAL_pvSharedMemoryMap(pHandle->hShMem,
                                                  OSAL_EN_READWRITE,
                                                  sizeof(trOSAL_MPF),
                                                  0)) == NULL)
  {
     return OSAL_ERROR;
  }
  if((pPool = (trOSAL_MPF*)OSAL_pvSharedMemoryMap(pHandle->hShMem,
                                                  OSAL_EN_READWRITE,
                                                  (pPool->mpfcnt * pPool->blfsz) + sizeof(trOSAL_MPF),
                                                  0)) == NULL)
  {
     return OSAL_ERROR;
  }
#endif

  /* Fill handle */
  pHandle->pMem               = (void*)pPool;
  pHandle->u32memstart        = (uintptr_t)pPool + sizeof(trOSAL_MPF);
  pHandle->u32memend          = (uintptr_t)pPool + sizeof(trOSAL_MPF) + (pPool->blfsz * pPool->mpfcnt);
  pHandle->Sem = Sem;


  pPool->u32OpenCnt++;

  sem_post(Sem);

  if(pPool->bLock == FALSE)
  {
     sem_close(Sem);
     pHandle->Sem = 0;
  }
  return OSAL_OK;
}

tS32 OSAL_u32MemPoolFixSizeOpenOnMem(tCString coszName, 
                                  trHandleMpf* pHandle,
                                  void* pMem)
{
  char cBuffer[32];
  trOSAL_MPF* pPool = (trOSAL_MPF*)pMem;
  sem_t*  Sem;
#ifndef NO_SIGNALAWARENESS
  tS32 s32Ret;
#endif

  /* use for create and open always a lock */
  snprintf(cBuffer,32,"/%s",coszName);
  Sem = sem_open(cBuffer, 0);
  if (Sem == SEM_FAILED)
  {
     FATAL_M_ASSERT_ALWAYS();
  }
#ifndef NO_SIGNALAWARENESS
  while(1)
  {
#endif
     s32Ret = sem_wait(Sem);
#ifndef NO_SIGNALAWARENESS
     if(s32Ret != 0)
     {
        /* check for incoming signal */
        if(errno != EINTR)
        {
           return OSAL_ERROR;
        }
     }
     else
     {
         break;
     }
  }
#endif


  pHandle->hShMem = 0;

  /* Fill handle */
  pHandle->pMem               = (void*)pPool;
  pHandle->u32memstart        = (uintptr_t)pPool + sizeof(trOSAL_MPF);
  pHandle->u32memend          = (uintptr_t)pPool + sizeof(trOSAL_MPF) + (pPool->blfsz * pPool->mpfcnt);
  pHandle->Sem = Sem;
  pPool->u32OpenCnt++;

  sem_post(Sem);

  if(pPool->bLock == FALSE)
  {
     sem_close(Sem);
     pHandle->Sem = 0;
  }
  return OSAL_OK;
}


/******************************************************************************
 * FUNCTION:      OSAL_s32MsgPoolFixSizeDelete
 *
 * DESCRIPTION:   delete memory pool for OSAL
 *
 * PARAMETER:     trHandleMpf*     pointer to Pool handle structure 
 *               
 * RETURNVALUE:   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 OSAL_s32MemPoolFixSizeDelete (tCString szName)
{
   tS32 s32Ret = OSAL_OK;

   char cBuffer[32];
   /* use for create and open always a lock */
   snprintf(cBuffer,32,"/%s",szName);
   sem_unlink(cBuffer);

#ifdef NO_OSAL_USED
#else
   if(OSAL_s32SharedMemoryDelete(szName) == OSAL_ERROR)
   {
      s32Ret = OSAL_ERROR;
   }
#endif
   return s32Ret;
}

/******************************************************************************
 * FUNCTION:      OSAL_s32MemPoolFixSizeClose
 *
 * DESCRIPTION:   close memory pool for OSAL process
 *
 * PARAMETER:     trHandleMpf*     pointer to Pool handle structure 
 *               
 * RETURNVALUE:   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 OSAL_s32MemPoolFixSizeClose (trHandleMpf* pHandle)
{
   trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
   tS32 s32Ret = OSAL_OK;
   char cBuffer[32] = {0};
   snprintf(cBuffer,32,"/%s",pPool->szName);

   if(pPool->bLock)
   {
      sem_close(pHandle->Sem);
   }

   pPool->u32OpenCnt--;
   if(pPool->u32OpenCnt)pPool->u32OpenCnt--;

#ifdef NO_OSAL_USED
   if(pPool->u32OpenCnt == 0)
   {
 //     rLiShMemMap[u32Index].u32PrcCnt++;

      if(pPool->u32OpenCnt == 0)
      {
          snprintf(cBuffer,32,"%s",pPool->szName);
      }
      if(munmap(pHandle->pMem, (pHandle->u32memend - (tU32)pHandle->pMem)) != 0)
      {
         s32Ret = OSAL_ERROR;
      }
      if(cBuffer[0] == '/')
      {
         if(shm_unlink(cBuffer) != 0)
         {
            s32Ret = OSAL_ERROR;
         }
         if(sem_unlink(cBuffer) != 0)
         {
            s32Ret = OSAL_ERROR;
         }
      }
   }
#else
   if(pHandle->hShMem != 0)
   {
      if(OSAL_s32SharedMemoryClose(pHandle->hShMem) == OSAL_ERROR)
      {
         s32Ret = OSAL_ERROR;
	  }
   }
#endif
   return s32Ret;
}

void vReadFileContent(const void* pvBuffer,tBool bWriteErrmem)
{
#define LINE_LENGTH  250
  tS32 s32Val;
  tS32 fd;
  int size,i;
  int offset = 0;
  char cTraceBuf[LINE_LENGTH+5];
  tU16 u16OpenMode = 0;//O_TEXT;
  unsigned char* pBuffer = NULL;
  tBool bIgnoreSize= FALSE;

   if((fd = open((char*)pvBuffer, O_RDONLY,u16OpenMode)) > OSAL_OK)
   {
      struct stat stat_data;
      if(fstat( fd, &stat_data ) == 0)
      {
         size = stat_data.st_size;
      }
      else
      {
          size = 100000;
          bIgnoreSize = TRUE;
      }
      if((pBuffer = (unsigned char*)malloc((tU32)size)) != 0)
      {
        memset(pBuffer,0,(tU32)size);
        if((s32Val = lseek(fd,0,SEEK_SET)) < OSAL_OK)
        {
//          TraceString((const char*)"Seek File Error");
          s32Val = -1;
        }
        else
        {
          if((s32Val = read( fd,pBuffer,(tU32)size)) != size)
          {  
             if(bIgnoreSize != TRUE)
             {
//                TraceString((const char*)"Read File Error");
                s32Val = -1;
             }
          }
      }
        
      if(s32Val >= 0)
      {
         OSAL_M_INSERT_T8(&cTraceBuf[0],OSAL_STRING_OUT);
         while(size)
         {
            /* search next line if available*/
            for (i = 0; i<LINE_LENGTH ;i++)
            {
              if((*(pBuffer+offset+i) == /*EOF*/0xff) || (*(pBuffer+offset+i) == 0x0a/*EOL*/)) 
              {
                i++;
                break;
              }
              if(offset+i > s32Val)break;
            }
            memcpy(&cTraceBuf[1],pBuffer+offset,(tU32)i);
#ifndef NO_OSAL_USED
            LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,&cTraceBuf[0],(tU32)i);
#endif
            if(bWriteErrmem)
            {
#ifndef NO_OSAL_USED
               vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&cTraceBuf[0],i,OSAL_STRING_OUT);
#endif
            }
            offset += (i);
            if(offset >= s32Val)break;
        }// end while
      }
      
     }
     close(fd);
   }
   if(pBuffer)free(pBuffer);
}

void vPrintAllBlocks(const trHandleMpf* pHandle)
{
#ifndef NO_OSAL_USED
  uintptr_t *pMem;
  trOSAL_MPF* pPool = (trOSAL_MPF*)(pHandle->u32memstart - sizeof(trOSAL_MPF));
  tU32 i,Pid,Size,Magic;
  system("ps auxw > /tmp/process.txt \n");
  for(i = 0;i<pPool->mpfcnt;i++)
  {
     pMem = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (i * pPool->blfsz));
     Pid = *pMem;
     pMem++;
     Size = *pMem;
     Magic = *((tU32*)((tU32)pHandle->u32memstart + (((i+1) * pPool->blfsz) - sizeof(tU32))));
     TraceString("Pool %s Idx:%d Pid:%d Size:%d, Magic:%d",pPool->szName,i,Pid,Size,Magic);
  }
  vReadFileContent("/tmp/process.txt",FALSE);/*lint !e1773 */  /*otherwise linker warning */
#endif
}


/******************************************************************************
 * FUNCTION:      OSAL_pvMemPoolFixSizeGetBlockOfPool
 *
 * DESCRIPTION:   allocate a memory block from the specified OSAL memory pool
 *
 * PARAMETER:     trHandleMpf*     pointer to Pool handle structure 
 *               
 * RETURNVALUE:   VP Pointer to allocated memory 
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
void* OSAL_pvMemPoolFixSizeGetBlockOfPool(const trHandleMpf* pHandle)
{
  void* pBlock = NULL;
  tS32 s32Ret = 0;
  trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tU32 u32AllocSize = pPool->blfsz;
  tU8 u8Buffer[50];
  tU8* pu8Pointer = NULL;
  tBool bMarker = FALSE;
  uintptr_t* pPointer = NULL;
  uintptr_t* pCurrent;

  if(pPool->bLock)
  {
#ifndef NO_SIGNALAWARENESS
     while(1)
     {
#endif
        s32Ret = sem_wait(pHandle->Sem);
#ifndef NO_SIGNALAWARENESS
        if(s32Ret != 0)
        {
           /* check for incoming signal */
           if(errno != EINTR)
           {
             return NULL;
           }
        }
        else
        {
           break;
        }
     }
#endif
  }


  /* get memory from pool */
  if(pPool->freecnt)
  {
     for(;;)
     {  
         pCurrent = (uintptr_t*)(pHandle->u32memstart + (pPool->u32nextIdx * pPool->blfsz));
         if(*((uintptr_t*)((uintptr_t)pCurrent+sizeof(uintptr_t))) == FREEMARKER)
         {
            *pCurrent = 0;
            pPool->u32nextIdx++;
            if(pPool->u32nextIdx == pPool->mpfcnt)
            {
                pPool->u32nextIdx = 0;
            }
            pBlock = (void*)pCurrent;
            pPool->freecnt--;
            break;
         }
         else
         {
            pPool->u32nextIdx++;
            if(pPool->u32nextIdx == pPool->mpfcnt)
            {
                pPool->u32nextIdx = 0;
            }
         }
     }

     /* increment message counter */
     pPool->u32CurCount++;
     if(pPool->u32CurCount > pPool->u32MaxCount)pPool->u32MaxCount = pPool->u32CurCount;
  }
  else
  {
     pPool->u32ErrCnt++;
     OSAL_M_INSERT_T8(&u8Buffer[0],OSAL_STRING_OUT);
     if(!pPool->bMalloc)
     {
        s32Ret = snprintf((char*)&u8Buffer[1],49,"Pool %s is empty no malloc configured Pid:%d!",pPool->szName,getpid());
     }
     else
     {
        bMarker = TRUE;
        s32Ret = snprintf((char*)&u8Buffer[1],49,"Pool %s needs malloc in Pid:%d!",pPool->szName,getpid());
        /* no pool available use standard malloc*/
        pBlock = malloc(u32AllocSize);
        if(bPrintOwner ==FALSE)
        {
            bPrintOwner = TRUE;
       //     vPrintAllBlocks(pHandle);
        }
     }
     if(pPool->u32ErrCnt < 100)
     {
        LLD_vTrace(TR_COMP_OSALCORE,TR_LEVEL_FATAL,(tU8*)u8Buffer,s32Ret+1);
     }
  }

  if(pPool->bLock)sem_post(pHandle->Sem);

  /* check for usable memory block */
  if(pBlock == NULL)
  {
     OSAL_M_INSERT_T8(&u8Buffer[0],OSAL_STRING_OUT);
     s32Ret = snprintf((char*)&u8Buffer[1],49,"no memory from Pool %s!",pPool->szName);
     LLD_vTrace(TR_COMP_OSALCORE,TR_LEVEL_FATAL,(tU8*)u8Buffer,s32Ret+1);
     u32ErrorCode = OSAL_E_NOSPACE;
  }
  
  /* check for errors */
  if(u32ErrorCode != OSAL_E_NOERROR)
  {
#ifdef NO_OSAL_USED
     tU32 u32Tid = (tU32)syscall(SYS_gettid);
#else
     tU32 u32Tid = (tU32)OSAL_ThreadWhoAmI();
#endif
     OSAL_M_INSERT_T8(&u8Buffer[0],GETBLOCK_MPF_FAILED);
     OSAL_M_INSERT_T32(&u8Buffer[1],u32ErrorCode);
     OSAL_M_INSERT_T32(&u8Buffer[5],u32Tid);
     OSAL_M_INSERT_T32(&u8Buffer[9],(tU32)pPool->blfsz);
     vTraceOsalMpf(TR_LEVEL_FATAL,u8Buffer,13);
#ifdef NO_OSAL_USED
#else
     OSAL_vSetErrorCode(u32ErrorCode);
#endif
  }
  else
  {
      /* prepare memblock ,generate pointer to memblock*/
      pu8Pointer  = (tU8*)pBlock;
      pPointer = (uintptr_t*)pBlock;
      /* store Pid info at begin of mem block for analysis*/
      *pPointer = getpid();/*lint !e613 pu32Pointer is here always !=0 */
      pPointer++;/*lint !e613 pu32Pointer is here always !=0 */
      /* store size info at begin of mem block */
      *pPointer = u32AllocSize;/*lint !e613 pu32Pointer is here always !=0 */
      /*determine return value address */
      pPointer++;/*lint !e613 pu32Pointer is here always !=0 */
      pBlock = (void*)pPointer;

      /* store magic at last 4Byte, alignment is in pool implementation ensured*/
      pPointer = (uintptr_t*)((uintptr_t)pu8Pointer + pPool->blfsz - sizeof(uintptr_t));/*lint !e826 */
      if(bMarker)*pPointer = ALLOCMAGIC;
      else       *pPointer = POOLMAGIC;
  }
  return pBlock;
}


/******************************************************************************
 * FUNCTION:      OSAL_s32MemPoolFixSizeRelBlockOfPool
 *
 * DESCRIPTION:   releases a memory block from the OSAL memory pool
 *
 * PARAMETER:     trHandleMpf*     pointer to Pool handle structure 
 *                VP  Pointer to allocated memory 
 *               
 * RETURNVALUE:   OSAL_OK/OSAL_ERROR 
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 OSAL_s32MemPoolFixSizeRelBlockOfPool(const trHandleMpf* pHandle, void* pBlock)
{
  tS32 s32Return    = OSAL_OK;
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tU8 u8Buffer[50]  = {0};
  uintptr_t* puPointer = (uintptr_t*)pBlock;
  uintptr_t* puTemp = (uintptr_t*)pBlock;
  tU8*  pu8Pointer;
  trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
  int i;

  /* go to block start position to get size info*/
  puPointer--;

  pu8Pointer = (tU8*)puPointer;

  /* check for correkt size */
 if(*puPointer != (uintptr_t)pPool->blfsz)
 {
     /* go to magic info and check magic*/
     puTemp = (uintptr_t*)(pu8Pointer + (uintptr_t)pPool->blfsz - sizeof(uintptr_t));/*lint !e826 */
     puTemp--;
     if(*puTemp == ALLOCMAGIC)
     {
         /* go to begin of mem block */
         puPointer--;/*lint !e613 pu32Pointer is here always !=0 */
         /* check for valid pointer and possible mallocs */
         if(pPool->u32ErrCnt > 0)
//       if(((tU32)pu8Pointer < pHandle->u32memstart)&&((tU32)pu8Pointer > pHandle->u32memend))
         {
            /* memory was allocated via standard malloc */
            free((void*)puPointer);/*lint !e424 */
         }
         else
         {
#ifndef NO_OSAL_USED
            TraceString("Mempool wrong free adress 0x% Value:%d",puPointer,pPool->u32ErrCnt);
#endif
         }
     }
     else
     {
        OSAL_M_INSERT_T8(&u8Buffer[0],OSAL_STRING_OUT);
        i = snprintf((char*)&u8Buffer[1],49,"wrong memory for Pool %s!",pPool->szName);
        LLD_vTrace(TR_COMP_OSALCORE,TR_LEVEL_FATAL,(tU8*)u8Buffer,i+1);
        u32ErrorCode = OSAL_E_INVALIDVALUE;
     }
  }
  else
  {
     /* go to magic info and check magic*/
     puTemp = (uintptr_t*)(pu8Pointer + (uintptr_t)pPool->blfsz - sizeof(uintptr_t));/*lint !e826 */
     puTemp--;
     if(*puTemp == POOLMAGIC)
     {
         /* mark block as freed */
        if(pPool->bLock)
        {
#ifndef NO_SIGNALAWARENESS
           while(1)
           {
#endif
              s32Return = sem_wait(pHandle->Sem);
#ifndef NO_SIGNALAWARENESS
              if(s32Return != 0)
              {
                 /* check for incoming signal */
                 if(errno != EINTR)
                 {
                    return OSAL_ERROR;
                 }
              }
              else
              {
                 break;
              }
           }
#endif
        }
        /* release block */
        *puPointer = FREEMARKER;

        /* reset Pid info at begin of mem block */
        puPointer--;/*lint !e613 pu32Pointer is here always !=0 */
        *puPointer = 0;

        pPool->freecnt++;
        pPool->u32CurCount--;
        if(pPool->bLock)sem_post(pHandle->Sem);
     }
     else if(*puTemp == ALLOCMAGIC)
     {
         /* reset Pid info at begin of mem block */
         puPointer--;/*lint !e613 pu32Pointer is here always !=0 */
         /* check for valid pointer and possible mallocs */
         if(pPool->u32ErrCnt > 0)
//       if(((uintptr_t)pu8Pointer < pHandle->u32memstart)&&((uintptr_t)pu8Pointer > pHandle->u32memend))
         {
            /* memory was allocated via standard malloc */
            free((void*)puPointer);/*lint !e424 */
         }
         else
         {
#ifndef NO_OSAL_USED
            TraceString("Mempool wrong free adress 0x% Value:%d",puPointer,pPool->u32ErrCnt);
#endif
         }
     }
     else
     {
         OSAL_M_INSERT_T8(&u8Buffer[0],OSAL_STRING_OUT);
         i = snprintf((char*)&u8Buffer[1],49,"wrong memory free for Pool %s!",pPool->szName);
         LLD_vTrace(TR_COMP_OSALCORE,TR_LEVEL_FATAL,(tU8*)u8Buffer,i+1);
         /* try to repair if overwritten block is used */
         if(((uintptr_t)puPointer > pHandle->u32memstart)&&((uintptr_t)puPointer < pHandle->u32memend))
         {
           /**/
         }
     }
  }

  /* check for errors */
  if(u32ErrorCode != OSAL_E_NOERROR)
  {
#ifdef NO_OSAL_USED
     tU32 u32Tid = (tU32)pthread_self();
#else
      tU32 u32Tid = (tU32)OSAL_ThreadWhoAmI();
#endif
      OSAL_M_INSERT_T8(&u8Buffer[0],RELBLOCK_MPF_FAILED);
      OSAL_M_INSERT_T32(&u8Buffer[1],u32ErrorCode);
      OSAL_M_INSERT_T32(&u8Buffer[5],u32Tid);
      OSAL_M_INSERT_T32(&u8Buffer[9],pPool->blfsz -12);
      vTraceOsalMpf(TR_LEVEL_FATAL,u8Buffer,13);
#ifdef NO_OSAL_USED
#else
     OSAL_vSetErrorCode(u32ErrorCode);
#endif
      s32Return    = OSAL_ERROR;
  }
  return s32Return;
}


/******************************************************************************
 * FUNCTION:      TracePoolInfo
 *
 * DESCRIPTION:   traces the related OSAL memory pool information
 *
 * PARAMETER:     VP   Pointer to allocated memory 
 *               
 * RETURNVALUE:   OSAL_OK/OSAL_ERROR 
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 18.11.09  |   Initial revision                     | MRK2HI
 *****************************************************************************/
void TraceSpecificPoolInfo(const trHandleMpf* pHandle)
{
  tU8 u8Buffer[40];
  tS32 s32Ret;
  OSAL_M_INSERT_T8(&u8Buffer[0],MPF_INFO);
  trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;

  if(pPool->bLock)
  {
#ifndef NO_SIGNALAWARENESS
     while(1)
     {
#endif
         s32Ret = sem_wait(pHandle->Sem);
#ifndef NO_SIGNALAWARENESS
         if(s32Ret != 0)
         {
            /* check for incoming signal */
            if(errno != EINTR)
            {
               return ;
            }
         }
         else
         {
            break;
         }
     }
#endif
  }
  OSAL_M_INSERT_T32(&u8Buffer[1],((tU32)pPool->blfsz - (tU32)(2*sizeof(uintptr_t))));
//  OSAL_M_INSERT_T32(&u8Buffer[1],pPool->rMpf.blfsz);
  OSAL_M_INSERT_T32(&u8Buffer[5],(tU32)pPool->mpfcnt);
  OSAL_M_INSERT_T32(&u8Buffer[9],pPool->u32MaxCount);
  OSAL_M_INSERT_T32(&u8Buffer[13],pPool->u32CurCount);
  OSAL_M_INSERT_T32(&u8Buffer[17],pPool->u32ErrCnt);
  OSAL_M_INSERT_T32(&u8Buffer[21],pPool->u32OpenCnt);
  strncpy((char*)&u8Buffer[25],(const char*)&pPool->szName,8);
  
  if(pPool->bLock)sem_post(pHandle->Sem);

  vTraceOsalMpf(TR_LEVEL_FATAL,u8Buffer,33);
}


void vGetPoolInfo(const trHandleMpf* pHandle,char* Buffer,tU32 u32Len)
{
   trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
   snprintf(Buffer,u32Len,"%s Elements:%d Max In use:%d Current in use:%d",
            pPool->szName,(int)pPool->mpfcnt,(int)pPool->u32MaxCount,(int)pPool->u32CurCount);
}

void vTracePoolInfo(const trHandleMpf* pHandle)
{
    /* Tracestring is not declared if 'NO_OSAL_USED' is defined */
    /* Rather removed the call to avoid dependencies */

#ifndef NO_OSAL_USED
   trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
   TraceString("%s Elements:%d Size:%d Max In use:%d Current in use:%d",
               pPool->szName,pPool->mpfcnt,pPool->blfsz,pPool->u32MaxCount,pPool->u32CurCount);
#endif
}


void* pvGetFirstElementForPid(const trHandleMpf* pHandle,tU32 u32Pid)
{
  trOSAL_MPF* pPool = (trOSAL_MPF*)pHandle->pMem;
  tU32 i;
  uintptr_t* puVal = NULL;

  for(i = 0;i<pPool->mpfcnt;i++)
  {
 //    TraceString("OSAL MemPool Element 0x%x for PID:%d found",pu32Val.*pu32Val);
     puVal = (uintptr_t*)((uintptr_t)pHandle->u32memstart + (i * pPool->blfsz));
     if(*puVal == u32Pid)
     {
       puVal++; //go to size field and check if element is in use
       puVal++; //go to start of element data
//     TraceString("OSAL Pool:0x%x Element 0x%x for PID:%d found",pPool, pu32Val,u32Pid);
       return ((void*)puVal);
     }
  }
  //TraceString("No Element found");
  return NULL;
}

#ifdef __cplusplus
}
#endif

/************************************************************************ 
|end of file osalmempool.cpp
|-----------------------------------------------------------------------*/

