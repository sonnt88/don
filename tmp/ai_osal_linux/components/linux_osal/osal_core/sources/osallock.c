/*****************************************************************************
| FILE:         OsalLock.c
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Timer-Functions.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 10.01.13  | Lint Removal               | SWM2KOR
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"


#ifdef __cplusplus
extern "C" {
#endif 

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/
#define NR_OF_SEM_WAIT           100

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static trOsalLock* arWaitSem[NR_OF_SEM_WAIT];
static tU32 u32SemWaitCount = 0;
tBool bTimOutSemSvActive = FALSE;
OSAL_tTimerHandle hSvTimer = (tU32)-1;

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
extern pthread_once_t  SvSecTimer_is_initialized;
extern OSAL_tTimerHandle hSvTimer;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
extern tU32 u32ExecuteLiCb( tS32 s32TskId, tS32 s32Pid, OSAL_tpfCallback pcallback, tPVoid pcallbackarg, tU8 au8String[] );


void vTraceLock(trOsalLock* pLock)
{
    TraceString("PID:%d Lock:0x%x %s Count:%u Open:%u Used:%d ",getpid(),&pLock->lock,&pLock->rInfo.cName[0],pLock->rInfo.u32Count,pLock->rInfo.u32OpenCount,pLock->rInfo.bUsed);

 /*  TraceString("%d %d %d %d %d %d %d %d %d %d ",
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-10],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-9],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-8],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-7],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-6],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-5],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-4],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-3],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-2],
             pLock->rInfo.cName[OSAL_C_U32_MAX_NAMELENGTH-1]);
*/
}


//#define CHECK_ARRAY_SIZE
#ifdef CHECK_ARRAY_SIZE
static tU32 u32CheckCounter = 0;
extern tU32 u32RecMqCount;
#endif

time_t RefTime = 0;
static tU32 u32SvCount = 0;

void timerfunc(void* Val)
{
   time_t secSinceEpoch = time(NULL);

 #ifdef CHECK_ARRAY_SIZE
   u32CheckCounter++;

   if(u32CheckCounter == 100)
   {
       TraceString("OSALMQ PID:%d %d message Queues in supervision",getpid(),u32RecMqCount);
       TraceString("OSALMQ PID:%d %d semaphores in supervision",getpid(),u32SemWaitCount);
   }
#endif

   if((secSinceEpoch - RefTime) > SV_RESOLUTION/1000)
   {
       if(LLD_bIsTraceActive(TR_COMP_OSALCORE, TR_LEVEL_WARNING) == TRUE)
       {
           TraceString("PID:%d Detection of System Time changed %d",getpid(),secSinceEpoch - RefTime);
       }
       u32SvCount = 60;
   }
   if(u32SvCount > 0)
   {
    //   TraceString("PID:%d Check Resources",getpid(),secSinceEpoch - RefTime);
       vCheckMqTimeout(Val);
       vCheckSemTimeout(Val);
       if(u32SvCount > 0)u32SvCount--;
   }
   RefTime = secSinceEpoch;
}

static intptr_t s32SvTimPid = 0;
void SetupSupervisionTimer(void)
{
   s32SvTimPid = OSAL_ProcessWhoAmI();
   tS32 s32PidEntry = s32FindProcEntry(s32SvTimPid);
   if(s32PidEntry != OSAL_ERROR)
   {
      TraceString("Start Clock Monotonic Supervision Timer for Process %s ",
                  prProcDat[s32PidEntry].pu8CommandLine);
   }
   if(OSAL_s32TimerCreate(timerfunc,
                          (void*)s32SvTimPid, 
                          &hSvTimer) == OSAL_OK)
   {
      RefTime = time(NULL);
      if(OSAL_s32TimerSetTime(hSvTimer,
                              SV_RESOLUTION,
                              SV_RESOLUTION) == OSAL_ERROR)
      {
          NORMAL_M_ASSERT_ALWAYS();
      }
   }
}

void StopSupervisionTimer(void)
{
    if(hSvTimer != (OSAL_tTimerHandle)-1)
    {
      (void)OSAL_s32TimerSetTime(hSvTimer,0,0);
      (void)OSAL_s32TimerDelete(hSvTimer);
    }
}
  

void InitSemWaitToList(void)
{
   int i=0;
   if(bTimOutSemSvActive == TRUE)
   {
      for(i=0;i<NR_OF_SEM_WAIT;i++)
      {
         arWaitSem[i] = NULL;
      }
   }
}

static void vAddSemWaitToList(trOsalLock* pEntry)
{
   int i;
   for(i=0;i<NR_OF_SEM_WAIT;i++)
   {
     if(arWaitSem[i] == NULL)
     {
        arWaitSem[i] = pEntry;
        u32SemWaitCount++;
       // TraceString("SEMWAIT PID:%d Nr:%d",getpid(),i);
        break;
     }
   }
   if(i == (NR_OF_SEM_WAIT-1))
   {
       TraceString("SEM Increase arWaitSem Current Size:%d",NR_OF_SEM_WAIT);
   }
}

static void vRemSemWaitFromList(const trOsalLock* pEntry)
{
   int i=0;
   if(bTimOutSemSvActive == TRUE)
   {
      for(i=0;i<NR_OF_SEM_WAIT;i++)
      {
         if(arWaitSem[i] == pEntry)
         {
            u32SemWaitCount--;
            break;
         }
      }
   }
}

void vCheckSemTimeout(void* Val)
{
   ((void)Val);
   tBool bTriggerCheck = FALSE;
   int SemVal;
   tU32 u32Time = OSAL_ClockGetElapsedTime()/SV_RESOLUTION;

   if(bTimOutSemSvActive == TRUE)
   {
      int i,k,j = 0;
      for(i=0;i<NR_OF_SEM_WAIT;i++)
      {
         if(arWaitSem[i])
         {
            j++;
            for(k=1 ; k < NR_OF_WAITER ;k++)
            {
               /* check for elapsed timeout */
               if((arWaitSem[i]->rInfo.u32Timeout[k])&&(arWaitSem[i]->rInfo.u32Timeout[k] < u32Time))
               {
                  bTriggerCheck = TRUE;
                  break;
               }
            }
            if(bTriggerCheck == TRUE)
            {
               for(k=1 ; k < NR_OF_WAITER ;k++)
               {
                  if((arWaitSem[i]->rInfo.u32Timeout[k]) && (arWaitSem[i]->rInfo.s32Tid[k]))
                  {
                      arWaitSem[i]->rInfo.u32Timeout[k] = 1;
                  }
               }
               if(sem_getvalue(&arWaitSem[i]->lock,&SemVal) == 0)
               {
                  for(k=1 ; k < SemVal;k++)
                  {
                      /* Post trigger */
                      if(sem_post(&arWaitSem[i]->lock) != 0)
                      {
                      }
                  }
               }
               bTriggerCheck = FALSE;
            }
         }
         if(j == (int)u32SemWaitCount)break;
      }
   }
}

tBool bGetTaskName(tS32 Pid,tS32 Tid, char* Name)
{
   tBool bRet = FALSE;
   char buffer[128];
   int fd;
   if(Pid != 0)
   {
      snprintf(buffer,128,"/proc/%d/task/%d/comm",Pid,Tid);
   }
   else
   {
      snprintf(buffer,128,"/proc/%d/comm",Tid);
   }
   if((fd = open(buffer,O_RDONLY)) != -1)
   {
       if(read(fd,Name,OSAL_C_U32_MAX_NAMELENGTH) > 0)
       {
            Name[OSAL_C_U32_MAX_NAMELENGTH-1] = '\0';
            Name[strlen(Name)-1] = 0;
            bRet = TRUE;
       }
       close(fd);
   }
   if(bRet == FALSE)
   {
     strncpy(Name,"unnamed",strlen("unnamed"));
   }
    return bRet;
}

/* __sync_bool_compare_and_swap(ptr, oldval, newval) is a gcc built-in function.  It atomically performs the equivalent of:
          if (*ptr == oldval)
               *ptr = newval;

  It returns true if the test yielded true and *ptr was updated. The alternative here would be to employ the equivalent atomic
  machine-language instructions.
  
  FUTEX 1 
  1 = Free
  0 = occupied
*/

int futex(int *uaddr, int futex_op, int val,
             const struct timespec *timeout, int *uaddr2, int val3)
{
   return syscall(SYS_futex, uaddr, futex_op, val,timeout, uaddr2, val3);
}


//#define TRACE_FUT

int futex_setwait(int *futexp,unsigned int msec,int maskval)
{
   int Ret = 0;
//   struct timespec tim = {0,0};
   unsigned int Temp = (tU32)OSAL_ThreadWhoAmI();
   char Name[TRACE_NAME_SIZE];
     
   while(1)
   {
      if(msec)
      {
     //    (void)u32GemTmoStruct(&tim,msec);
       //  Ret = futex(futexp, FUTEX_WAIT|FUTEX_CLOCK_REALTIME,0,&tim, NULL, 0); Kernel 4.5
      //   Ret = futex(futexp, FUTEX_WAIT_BITSET|FUTEX_CLOCK_REALTIME,*futexp,&tim, NULL, maskval);
 /*__sync_bool_compare_and_swap(ptr, oldval, newval) is a gcc built-in function.  It atomically performs the equivalent of:
          if (*ptr == oldval)
               *ptr = newval;*/
      /*   if(__sync_bool_compare_and_swap(futexp, 0, 1) == -1) *lint !e746 *
         {
            TraceString("OSALFUT futex_setwait __sync_bool_compare_and_swap failed ");
         }
         else*/
         {
            *futexp = 1;
#ifdef TRACE_FUT
            TraceString("OSALFUT Task ID:%d futex_setwait1 *blocked(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
            Ret = futex(futexp, FUTEX_WAIT,1,NULL, NULL, 0);
#ifdef TRACE_FUT
            TraceString("OSALFUT Task ID:%d futex_setwait2 *blocked(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
         }
      }
      else
      {
         TraceString("OSALFUT futex_setwait with invalid Timeout:%d ",msec);
      }
      if (Ret == -1 && errno != EAGAIN)
      {
       //  vWritePrintfErrmem("futex_setwait failed errno:%d",errno);
         if(errno != EINTR)break;
      }
      else
      {
       /*     do{
               OSAL_s32ThreadWait(0);
               Ret = futex(futexp, FUTEX_WAIT,1,NULL, NULL, 0);
            }while(errno == EAGAIN);*/
            break;
      }
   }

   if((u32OsalSTrace & 0x00000100)||(Ret == -1))
   {
      int Error = 0;
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)Temp);
      if(Ret == -1)
      {
         Error = errno;
      }
      TraceString("OSALFUT Task:%s(ID:%d) futex_setwait TMO:%d Error:%d mask:%d locl_val:%d",Name,Temp,msec,Error,maskval,*futexp);
   }
   return Ret;
}

int futex_wait(int *futexp)
{
   int Ret = 0;
   unsigned int Temp = (tU32)OSAL_ThreadWhoAmI();
 //  struct timespec tim = {0,0};

   while (1) 
   {
      /* Is the futex available? */
      if (__sync_bool_compare_and_swap(futexp, 1, 0)) /*lint !e746 */
          break;      /* Yes */

 //     (void)u32GemTmoStruct(&tim,msec);
 //     if((Ret = futex(futexp, FUTEX_WAIT|FUTEX_CLOCK_REALTIME,0,&tim, NULL, 0)) == 0) ->KERNEL_45
#ifdef TRACE_FUT
      TraceString("OSALFUT Task ID:%d futex_wait1 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
      if((Ret = futex(futexp, FUTEX_WAIT,0,NULL, NULL,0)) == 0)
      {
         break;
      }
      else
      {
         if (errno != EINTR)
         {
            break;
         }
      }
   }
#ifdef TRACE_FUT
    TraceString("OSALFUT Task ID:%d futex_wait2 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
   
   if((u32OsalSTrace & 0x00000100)||(Ret == -1))
   {
      Temp = (tU32)OSAL_ThreadWhoAmI();
      int Error = 0;
      char Name[TRACE_NAME_SIZE];
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)Temp);
      if(Ret == -1)
      {
         Error = errno;
      }
      TraceString("OSALFUT Task:%s(ID:%d) futex_wait Error:%d ",Name,Temp,Error);
   }
   return Ret;
}

int futex_wakeup(int *futexp)
{
   int Ret = 0;
   int Error = 0;
   unsigned int Temp = (tU32)OSAL_ThreadWhoAmI();
   
   *futexp = 0;
 //  if (__sync_bool_compare_and_swap(futexp, 1,0))/*lint !e746 */
   {
#ifdef TRACE_FUT
      TraceString("OSALFUT Task ID:%d futex_wakeup1 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
      Ret = futex(futexp,FUTEX_WAKE,0, NULL, NULL, 0);
#ifdef TRACE_FUT
      TraceString("OSALFUT Task ID:%d futex_wakeup2 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
   }
/*   else
   {
       TraceString("OSALFUT futex_wakeup __sync_bool_compare_and_swap failed !!!");
   }*/
   if((u32OsalSTrace & 0x00000100)||(Ret == -1))
   {
      Temp = (tU32)OSAL_ThreadWhoAmI();
      char Name[TRACE_NAME_SIZE];
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)Temp);
      if(Ret == -1)
      {
         Error = errno;
      }
      TraceString("OSALFUT Task:%s(ID:%d) futex_wakeup TMO:%d Wake up %d Tasks Error:%d ",Name,Temp,Ret,Error);
   }
   return Ret;
}


int futex_wakeup2(int *futexp, int mask)
{
   int Ret = 0;
   int Error = 0;

   Ret = futex(futexp, FUTEX_WAKE_BITSET,*futexp, NULL, NULL, mask);

   if((u32OsalSTrace & 0x00000100)||(Ret == -1))
   {
      unsigned int Temp = (tU32)OSAL_ThreadWhoAmI();
      char Name[TRACE_NAME_SIZE];
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)Temp);
      if(Ret == -1)
      {
         Error = errno;
      }
      TraceString("OSALFUT Task:%s(ID:%d) futex_wakeup TMO:%d Wake up %d Tasks Error:%d mask:%d",Name,Temp,Ret,Error,mask);
   }
   return Ret;
}

/*
     int futex(int *uaddr, int futex_op, int val, const struct timespec *timeout or: uint32_t val2 ,   int *uaddr2, int val3);*/

int futex_post(int *futexp,int *Block)
{
  int Ret = 0;
  unsigned int Temp;
  char Name[TRACE_NAME_SIZE];
  
  *futexp = 1;
 // if (__sync_bool_compare_and_swap(futexp, 0, 1))
  {
    if(Block && *Block == 1)
    {
#ifdef TRACE_FUT
       Temp = (tU32)OSAL_ThreadWhoAmI();
       TraceString("OSALFUT Task ID:%d futex_post1 *futexp(0x%" PRIxPTR "):%d *Block(0x%" PRIxPTR "):%d",Temp,futexp,*futexp,Block,*Block);
#endif
       Ret =  futex(Block ,FUTEX_WAKE_OP,1,(struct timespec*)1, futexp,FUTEX_OP(FUTEX_OP_SET ,0,FUTEX_OP_CMP_EQ,1));
#ifdef TRACE_FUT
       TraceString("OSALFUT Task ID:%d futex_post2 *futexp(0x%" PRIxPTR "):%d *Block(0x%" PRIxPTR "):%d",Temp,futexp,*futexp,Block,*Block);
#endif
    }
    else
    {
#ifdef TRACE_FUT
       Temp = (tU32)OSAL_ThreadWhoAmI();
       TraceString("OSALFUT Task ID:%d futex_post1 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);  
#endif
       Ret =  futex(futexp, FUTEX_WAKE, 1, NULL, NULL, 0);
#ifdef TRACE_FUT
         TraceString("OSALFUT Task ID:%d futex_post2 *futexp(0x%" PRIxPTR "):%d ",Temp,futexp,*futexp);
#endif
    }
  }
 /* else
  {
      TraceString("OSALFUT futex_post __sync_bool_compare_and_swap failed !!!");
  }*/

   if((u32OsalSTrace & 0x00000100)||(Ret == -1))
   {
      Temp = (tU32)OSAL_ThreadWhoAmI();
      int Error = 0;
      bGetThreadNameForTID(&Name[0],TRACE_NAME_SIZE,(tS32)Temp);
      if(Ret == -1)
      {
         Error = errno;
      }
      TraceString("OSALFUT Task:%s(ID:%d) futex_post Error:%d Return:%d",Name,Temp,Error,Ret);
   }
   return Ret;
}


/******************************************************************************
 * FUNCTION:      CreateOsalLock
 *
 * DESCRIPTION:   generates a generic Lock for OSAL
 *
 * PARAMETER:     void*  Pointer to Lock argument
 *                UB*    Name of the lock 
 *               
 * RETURNVALUE: 
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 04.02.08  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 CreateOsalLock(trOsalLock* pLock, const char* name)
{
   tS32 s32Ret = OSAL_ERROR;

   strncpy(pLock->rInfo.cName,name,OSAL_C_U32_MAX_NAMELENGTH);
   if( 0 == sem_init(&pLock->lock, 1, 1) )
   {
         pLock->rInfo.u32Count = 0;
         pLock->rInfo.u32OpenCount = 1;
         pLock->rInfo.bUsed    = TRUE;
         s32Ret = OSAL_OK;
   }
   else
   {
      u32ConvertErrorCore(errno);
      FATAL_M_ASSERT_ALWAYS();
   }
   return s32Ret;
}

tS32 OpenOsalLock(trOsalLock* pLock)
{
   tS32 s32Ret = OSAL_ERROR;

   if (pLock->rInfo.bUsed)
   {
      pLock->rInfo.u32OpenCount++;
      s32Ret = OSAL_OK;
   }
   else
   {
      vTraceLock(pLock);
      vPrinthexDump((tPU8)((uintptr_t)&pLock->rInfo - 32),64);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, OSAL_E_DOESNOTEXIST );
      NORMAL_M_ASSERT_ALWAYS();
   }
   return s32Ret;
}


/******************************************************************************
 * FUNCTION:      DeleteOsalLock
 *
 * DESCRIPTION:   deletes a Lock for OSAL
 *
 * PARAMETER:     ID*  Pointer to Lock argument
 *                
 *               
 * RETURNVALUE: 
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 04.02.08  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 DeleteOsalLock(trOsalLock* pLock)
{
   tS32 s32Ret = OSAL_ERROR;

   if(pLock->rInfo.u32Count > 0)
   {
      /* Lock still in use, someone waits */
      FATAL_M_ASSERT_ALWAYS();
   }
   else
   {
      pLock->rInfo.u32Count = 0;
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 CloseOsalLock(trOsalLock* pLock)
{
   pLock->rInfo.u32OpenCount--;
   return OSAL_OK;
}


tS32 s32DestroyOsalLock(const char* name)
{
   tS32 s32Ret = OSAL_OK;
   char szName[OSAL_C_U32_MAX_NAMELENGTH];

   strncpy(szName,name,OSAL_C_U32_MAX_NAMELENGTH);
   if(sem_unlink(szName) == -1)
   {
      s32Ret = OSAL_ERROR;
      u32ConvertErrorCore(errno);
   }
   return s32Ret;
}

tU32 u32SemWait( sem_t *lock)
{
   tU32 u32Ret = OSAL_E_NOERROR;
   while(1)/* lint -e716 */
   {
     if(sem_wait(lock) != 0)
     {
        /* check for incoming signal */
        if(errno != EINTR)
        {
          u32Ret = u32ConvertErrorCore(errno);;
          break;
        }
     }
     else
     {
         break;
     }
   }
   return u32Ret;
}

/******************************************************************************
 * FUNCTION:      LockOsal
 *
 * DESCRIPTION:   generates a generic Lock for OSAL
 *
 * PARAMETER:     ID*    Pointer to Lock argument
 *                TMO    Timer
 *               
 * RETURNVALUE: 
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 04.02.08  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 LockOsal(trOsalLock* pLock)
{
   tS32 s32Ret;

   if (pLock->rInfo.bUsed == FALSE)
   {
       vTraceLock(pLock);
       vPrinthexDump((tPU8)((uintptr_t)&pLock->rInfo - 32),64);
       NORMAL_M_ASSERT_ALWAYS();
   }
   /* and again a variable outside the lock, a CLOSE operation is missing! */
   pLock->rInfo.u32Count++;

   if((s32Ret=u32SemWait(&pLock->lock)) == OSAL_E_NOERROR)
   {
      s32Ret = OSAL_OK;
   }
   else
   {
      TraceString("u32SemWait returns with Error:%d",s32Ret);
	  s32Ret = OSAL_ERROR;
   }

   if(pOsalData->u32RecLockAccess)
   {
      if(s32Ret == OSAL_OK)
      {
          pLock->rInfo.s32Tid[0] = OSAL_ThreadWhoAmI();
      }
   }
   return s32Ret;
}


tU32 u32SemWaitTmo( sem_t *lock ,tU32 u32Timeout)
{
   struct timespec rData  = {0,0};
   tU32 u32Ret = OSAL_E_NOERROR;
   
   if(u32Timeout != OSAL_C_TIMEOUT_NOBLOCKING) 
   {
      if(u32GemTmoStruct(&rData,u32Timeout) != OSAL_E_NOERROR)
      {
         TraceString("Get REALTIME for timeout failed ");
      }
   }
   while(1)/* lint -e716 */
   {
      if(u32Timeout == OSAL_C_TIMEOUT_NOBLOCKING)
      {
         if(sem_trywait(lock) == -1)
         {
            if(EAGAIN == errno)
            { 
               u32Ret = OSAL_E_TIMEOUT;
            }
         }
      }
      else
      {
         if(u32Timeout < pOsalData->u32TimerResolution)
         {
           NORMAL_M_ASSERT_ALWAYS();
           u32Ret = OSAL_E_INVALIDVALUE;
         }
         else
         {
            while(1)
            {
              if(sem_timedwait(lock,&rData) == 0)
              {
                  /* normal completion leave while loop */
                  break;
              }
              else
              {
                 /* check for error without signal*/
                 if(errno != EINTR)
                 {
                    /* error not */
                    u32Ret = u32ConvertErrorCore(errno);;
                    break;
                 }
              }
              /* We are here because of signal. lets try again. */
            }
         }
      }
   }
   return u32Ret;
}

tS32 LockOsalTmo(trOsalLock* pLock,tU32 u32Timeout)
{
   tS32 s32Ret = OSAL_OK;
 
   if (pLock->rInfo.bUsed == FALSE)
   {
       vTraceLock(pLock);
       vPrinthexDump((tPU8)((uintptr_t)&pLock->rInfo - 32),64);
       NORMAL_M_ASSERT_ALWAYS();
   }

   /* and again a variable outside the lock, a CLOSE operation is missing! */
   pLock->rInfo.u32Count++;
   
   if((s32Ret=u32SemWaitTmo(&pLock->lock,u32Timeout)) == OSAL_E_NOERROR)
   {
      s32Ret = OSAL_OK;
   }
   else
   {
      TraceString("u32SemWaitTmo returns with Error:%d",s32Ret);
	  s32Ret = OSAL_ERROR;
   }

   if(pOsalData->u32RecLockAccess)
   {
      if(s32Ret == OSAL_OK)
      {
         pLock->rInfo.s32Tid[0] = OSAL_ThreadWhoAmI();
      }
   }
   return s32Ret;
}



/******************************************************************************
 * FUNCTION:      UnLockOsal
 *
 * DESCRIPTION:   generates a generic Lock for OSAL
 *
 * PARAMETER:     ID*    Pointer to Lock argument
 *                TMO    Timer
 *               
 * RETURNVALUE: 
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 04.02.08  |   Initial revision                     | MRK2HI
 *****************************************************************************/
tS32 UnLockOsal(trOsalLock* pLock)
{
   tS32 s32Ret = OSAL_OK;
   if (pLock->rInfo.bUsed == FALSE)
   {
       vTraceLock(pLock);
       vPrinthexDump((tPU8)((uintptr_t)&pLock->rInfo - 32),64);
       NORMAL_M_ASSERT_ALWAYS();
   }
   pLock->rInfo.u32Count--;
   if(pOsalData->u32RecLockAccess)pLock->rInfo.s32Tid[0] = 0;
   if(sem_post(&pLock->lock) != 0)
   {
      u32ConvertErrorCore(errno);
      FATAL_M_ASSERT_ALWAYS();
      s32Ret = OSAL_ERROR;
   }
   return s32Ret;
}



tS32 CreSyncObj(const char* cName, tU32 u32Init)
{
  tS32 s32Ret = OSAL_ERROR;
  ((tVoid)cName); // satisfy lint

  if(LockOsal(&pOsalData->SyncObjLock) == OSAL_OK)
  {
     if(pOsalData->u32LastLockEntry < pOsalData->u32MaxNrLockElements)
     {
        prOsalLock[pOsalData->u32LastLockEntry].rInfo.bUsed = TRUE;
        if(sem_init(&prOsalLock[pOsalData->u32LastLockEntry].lock, 1, u32Init) == -1)
        {
           NORMAL_M_ASSERT_ALWAYS();
        }
        s32Ret = (tS32)pOsalData->u32LastLockEntry;
        pOsalData->u32LastLockEntry++;
     } 
     else 
     {
        /* search an entry with !bIsUsed*/
        tU32 u32Count;
        for(u32Count=0;u32Count < pOsalData->u32MaxNrLockElements ;u32Count++)
        {
          if(prOsalLock[u32Count].rInfo.bUsed == FALSE)break;
        }
        if(u32Count < pOsalData->u32MaxNrLockElements)
        {
           prOsalLock[u32Count].rInfo.bUsed = TRUE;
           if(sem_init(&prOsalLock[u32Count].lock, 1, u32Init) == -1)
           {
              NORMAL_M_ASSERT_ALWAYS();
           }
           s32Ret = (tS32)u32Count;
        }
     }
     UnLockOsal(&pOsalData->SyncObjLock);
  }
  return s32Ret;
}


tU32 DelSyncObj(tS32 hLock)
{
   tU32 u32Ret = OSAL_E_NOERROR;
   
   if(hLock < 0 || hLock >= (tS32)pOsalData->u32MaxNrLockElements)
   {
      return OSAL_E_INVALIDVALUE;
   }
   if(LockOsal(&pOsalData->SyncObjLock) == OSAL_OK)
   {
/*      if (prOsalLock[hLock].rInfo.u32Count > 0)
      {
          OSAL_s32ThreadWait(0);
          if (prOsalLock[hLock].rInfo.u32Count > 0)
          {
             u32Ret = OSAL_E_BUSY;
             NORMAL_M_ASSERT_ALWAYS();
          }
      }
      if(u32Ret == OSAL_E_NOERROR)*/
      {
         prOsalLock[hLock].rInfo.bUsed = FALSE;

         if(bTimOutSemSvActive == TRUE)
         {
            int i=0;
            if(prOsalLock[hLock].rInfo.bSemSv == TRUE)
            {
               prOsalLock[hLock].rInfo.bSemSv = FALSE;
               for(i=1 ; i < NR_OF_WAITER ;i++)
               {
                  prOsalLock[hLock].rInfo.s32Tid[i] = 0;
               }
               vRemSemWaitFromList(&prOsalLock[hLock]);
            }
         }
         if( sem_destroy(&prOsalLock[hLock].lock) == -1 )
       //  if( sem_init(&prOsalLock[hLock].lock, 1, 0) == -1 )
         {
            FATAL_M_ASSERT_ALWAYS();
         }
      }
      UnLockOsal(&pOsalData->SyncObjLock);
   }
   return u32Ret;
}


tU32 u32UsedSyncObj(void)
{
   tU32 u32Count;
   tU32 u32ObjCount = 0;
   for(u32Count=0;u32Count < pOsalData->u32MaxNrLockElements ;u32Count++)
   {
      if(prOsalLock[u32Count].rInfo.bUsed == TRUE)
      {
          u32ObjCount++;
      }
   }
   return u32ObjCount;
}

void vConvertMSectotimespec(struct timespec* pData,tU32 u32mSec)
{
  pData->tv_sec = (time_t)((time_t)u32mSec/1000);
  pData->tv_nsec = (long)(((long)u32mSec%1000)*1000000);
}


tU32 WaiSyncObj(tS32 hLock, tU32 u32Timeout)
{
  tS32 s32Ret=0;
  tU32 u32Error = OSAL_E_NOERROR;
  struct timespec rData  = {0,0};
  int i = 0;

  if(hLock >= 0 && hLock < (tS32)pOsalData->u32MaxNrLockElements)
  {
    prOsalLock[hLock].rInfo.u32Count++;
    switch(u32Timeout)
    {
      case OSAL_C_TIMEOUT_FOREVER:
#ifndef NO_SIGNALAWARENESS
           while(1)
           {
#endif
              s32Ret = sem_wait(&prOsalLock[hLock].lock);
              if(bTimOutSemSvActive == TRUE)
              {
                 if(prOsalLock[hLock].rInfo.u32Timeout[0] == 1)
                 {
                    errno = EINTR;
                 }
              }
#ifndef NO_SIGNALAWARENESS
              if(s32Ret == 0)
              {
                 /* normal completion leave while loop */
                 break;
              }
              else
              {
                 /* check if interrupt signal has interrupted the wait*/
                 if(errno != EINTR)
                 {
                    s32Ret = u32ConvertErrorCore(errno);
                    break;
                 }
              }
           }// end while
#endif
          break;
      case OSAL_C_TIMEOUT_NOBLOCKING:      
           if(sem_trywait(&prOsalLock[hLock].lock) != 0)
           {
              if(EAGAIN == errno)
              { 
                 s32Ret=0;
                 u32Error = OSAL_E_TIMEOUT;
              }
           }
          break;
      default:
           if(u32Timeout < pOsalData->u32TimerResolution)
           {
              u32Error = OSAL_E_INVALIDVALUE;
           }
           else
           {
			   
              if (u32GemTmoStruct(&rData,u32Timeout)==OSAL_E_NOERROR)  // -> sem_timedwait uses CLOCK_REALTIME
              {
#ifndef NO_SIGNALAWARENESS
                 while(1)
                 {
#endif
                  if(bTimOutSemSvActive == TRUE)
                  {
                      tS32 s32Tid = OSAL_ThreadWhoAmI();


                      if(prOsalLock[hLock].rInfo.bSemSv == FALSE)
                      {
                         if(u32LocalTimerSig == 0)
                         {
                            TraceString("Cannot start supervision");
                         }
                         else
                         { 
                            /* start timeout supervision timer if required */
                            pthread_once(&SvSecTimer_is_initialized,SetupSupervisionTimer);
                         }
                         vAddSemWaitToList(&prOsalLock[hLock]);
                         prOsalLock[hLock].rInfo.s32Tid[0]     = s32Tid;
                         prOsalLock[hLock].rInfo.u32Timeout[0] = (OSAL_ClockGetElapsedTime() + u32Timeout)/SV_RESOLUTION;
                         prOsalLock[hLock].rInfo.bSemSv = TRUE;
                      }
                      else 
                      {
                         for(i=1 ; i< NR_OF_WAITER ;i++)
                         {
                           if(prOsalLock[hLock].rInfo.s32Tid[i] == 0)
                           {
                              prOsalLock[hLock].rInfo.s32Tid[i]     = s32Tid;
                              prOsalLock[hLock].rInfo.u32Timeout[i] = (OSAL_ClockGetElapsedTime() + u32Timeout)/SV_RESOLUTION;
                              break;
                           }
                           if(prOsalLock[hLock].rInfo.s32Tid[i] == s32Tid)
                           {
                              prOsalLock[hLock].rInfo.u32Timeout[i] = (OSAL_ClockGetElapsedTime() + u32Timeout)/SV_RESOLUTION;
                              break;
                           }
                         }
                         if(i == (NR_OF_WAITER-1))
                         {
                             TraceString("Prove number of Task wait with timeout for Sem PID:%d",getpid());
                         }
                      }
                   }
                   s32Ret = sem_timedwait(&prOsalLock[hLock].lock,&rData);
                   if((prOsalLock[hLock].rInfo.bSemSv == TRUE)&&(i< NR_OF_WAITER))
                   {
                      tU32 u32Time = OSAL_ClockGetElapsedTime()/SV_RESOLUTION;
                      if(prOsalLock[hLock].rInfo.u32Timeout[i] == 1)
                      {
                         if(prOsalLock[hLock].rInfo.u32Timeout[i] < u32Time)
                         {
                            TraceString("OSALSEM Timeout via Trigger for %s",prOsalLock[hLock].rInfo.cName);
                            prOsalLock[hLock].rInfo.u32Timeout[i] = 0;
                            errno = EAGAIN;
                         }
                      }
                      else
                      {
                          errno = EINTR;
                      }
                   }
#ifndef NO_SIGNALAWARENESS
                   if(s32Ret == 0)
                   {
                       /* normal completion leave while loop */
                       break;
                   }
                   else
                   {
                      /* check for error without signal*/
                      if(errno != EINTR)
                      {
                         /* error not */
                         s32Ret = u32ConvertErrorCore(errno);
                         break;
                      }
                   }
                   /* We are here because of signal. lets try again. */
                 }//end while
#endif
               }//if(u32Timeout < pOsalData->u32TimerResolution)
               else
               {
                  NORMAL_M_ASSERT_ALWAYS();
                  u32Error = OSAL_E_INVALIDVALUE;
               }//if (clock_gettime...
            }
          break;
    }
    prOsalLock[hLock].rInfo.u32Count--;
    if(s32Ret != 0)
    {
       u32Error = u32ConvertErrorCore(errno);
    }
  }
  else
  {
     u32Error = OSAL_E_DOESNOTEXIST;
  }
  return u32Error;
}

tU32 RelSyncObj(tS32 hLock)
{
  tU32 u32Error = OSAL_E_NOERROR;

  if(hLock >= 0 && hLock < (tS32)pOsalData->u32MaxNrLockElements)
  {
    if(sem_post(&prOsalLock[hLock].lock) != 0)
    {
      NORMAL_M_ASSERT_ALWAYS();
      u32Error = u32ConvertErrorCore(errno);
    }
  }
  else
  {
     u32Error = OSAL_E_DOESNOTEXIST;
  }
  return u32Error;
}


tU32 GetSyncObjVal(tS32 hLock,tS32* ps32Val)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(sem_getvalue(&prOsalLock[hLock].lock,ps32Val) != 0)
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   return u32ErrorCode;
}

void vGenerateSyncObjects(void)
{
   tU32 u32Count;
   tS32 rc,i;

   if(LockOsal(&pOsalData->SyncObjLock) == OSAL_OK)
   {
      for(u32Count= 0;u32Count < pOsalData->u32MaxNrLockElements;u32Count++)
      {
         OSAL_s32PrintFormat(prOsalLock[u32Count].rInfo.cName, "OSAL_%04d",(int) u32Count);
         rc = sem_init(&prOsalLock[u32Count].lock, 1 /* shared */, 1 /* initial */);
         if( rc != -1)
         {
            prOsalLock[u32Count].rInfo.u32Count = 0;
            prOsalLock[u32Count].rInfo.bUsed = FALSE;
            if(prOsalLock[u32Count].rInfo.bSemSv == TRUE)
            {
               for(i=1 ;i<5;i++)
               {
                  prOsalLock[u32Count].rInfo.u32Timeout[i] = 0;
                  prOsalLock[u32Count].rInfo.s32Tid[i] = 0;
               }
               prOsalLock[u32Count].rInfo.bSemSv = FALSE;
           }
         }
         else
         {
            u32ConvertErrorCore(errno);
            FATAL_M_ASSERT_ALWAYS();
         }
      }
      UnLockOsal(&pOsalData->SyncObjLock);
   }
}


void vDestroySyncObjects(void)
{
   tU32 u32Count;
   char szName[32]; 

   if(LockOsal(&pOsalData->SyncObjLock) == OSAL_OK)
   {
      for(u32Count= 0;u32Count < pOsalData->u32MaxNrLockElements;u32Count++)
      {
         OSAL_s32PrintFormat(szName, "OSAL_%04d", (int)u32Count);
         if(sem_unlink(szName) == -1)
         {
            u32ConvertErrorCore(errno);
         }
      }
      UnLockOsal(&pOsalData->SyncObjLock);
   }
}

#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
extern int errmem_fd;

void vWriteToDevErrmem(char* pcBuffer, int Length)
{
   if(write(errmem_fd,pcBuffer,Length) == -1)
   {
      if(errno == EBADF)
      {
         errmem_fd = open("/dev/errmem", O_WRONLY, OSAL_ACCESS_RIGTHS);
         if(errmem_fd != -1)
         {
            if(write(errmem_fd,pcBuffer,Length) == -1)
            {
               TraceString("Cannot write to ErrMem %s errno:%d",pcBuffer,errno);
            }
         }
         else
         {
            TraceString("Cannot open %s for PID_%d errno:%d","/dev/errmem",getpid(),errno);
         }
      }
   }
}
#endif

void vWritePrintfErrmem(tCString pcFormat, ...)
{
   tChar pcBuffer[ERRMEM_MAX_ENTRY_LENGTH] = {0};
   OSAL_tVarArgList argList;
   tU32 s32Size;
   tS32 trClass = TR_COMP_OSALCORE;
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   OSAL_tIODescriptor fd_errmem;
   trErrmemEntry  rErrmemEntry = {0};

   /* check if last process is in termination process complete shutdown scenario*/
   if(pOsalData->u32AttachedProcesses == 0)
   {
         OSAL_VarArgStart(argList, pcFormat);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
         s32Size = OSALUTIL_s32SaveVarNPrintFormat(pcBuffer, ERRMEM_MAX_ENTRY_LENGTH, pcFormat, argList); //lint !e530
         OSAL_VarArgEnd(argList);
         TraceString(pcBuffer);
   }
   else
   {
      fd_errmem = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_WRITEONLY);
      if(fd_errmem != OSAL_ERROR)
      {
         OSAL_VarArgStart(argList, pcFormat);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
         s32Size = OSALUTIL_s32SaveVarNPrintFormat(pcBuffer, ERRMEM_MAX_ENTRY_LENGTH, pcFormat, argList); //lint !e530
         OSAL_VarArgEnd(argList);

         rErrmemEntry.u16Entry = TR_COMP_OSALCORE;
/*    Timestamp is ignored
      if(OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime)!=OSAL_OK ){
         (tVoid) OSAL_pvMemorySet(&rErrmemEntry.rEntryTime, 0, sizeof(rErrmemEntry.rEntryTime));
      }*/
         rErrmemEntry.eEntryType = eErrmemEntryNormal;
         rErrmemEntry.u16EntryLength = (tU16)(s32Size+3);
         if (rErrmemEntry.u16EntryLength > (ERRMEM_MAX_ENTRY_LENGTH - 2))
         {
            rErrmemEntry.u16EntryLength = ERRMEM_MAX_ENTRY_LENGTH - 2;
         }
         rErrmemEntry.au8EntryData[0] = (tU8) (((tU16)trClass) & 0xFF);/*lint !e778*/
         rErrmemEntry.au8EntryData[1] = (tU8) (((tU16)trClass) >> 8) & 0xFF;
         rErrmemEntry.au8EntryData[2] = OSAL_STRING_OUT;

         (tVoid) OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[3], pcBuffer, rErrmemEntry.u16EntryLength);
         if (OSAL_ERROR == OSAL_s32IOWrite( fd_errmem, (tPCS8)&rErrmemEntry, sizeof(rErrmemEntry)))
         {
            TraceString("vWritePrintfErrmem failed \n");
         }
         (tVoid) OSAL_s32IOClose(fd_errmem);
      }
   }
#else
#ifdef OSAL_ERRMEM_TIMESTAMP
   vWriteUtcTime(errmem_fd);
#endif
   pcBuffer[0] = 0; // to indicate binary entry
   pcBuffer[1] = (tChar) (trClass & 0xFF);
   pcBuffer[2] = (tChar) (trClass >> 8) & 0xFF;
   pcBuffer[3] = (tChar) OSAL_STRING_OUT;

   OSAL_VarArgStart(argList, pcFormat);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
   s32Size = OSALUTIL_s32SaveVarNPrintFormat(&pcBuffer[4], ERRMEM_MAX_ENTRY_LENGTH-4, pcFormat, argList); //lint !e530
   OSAL_VarArgEnd(argList);
   if(errmem_fd == -1)
   {
     (void)write(1,pcBuffer,s32Size+4);
   }
   else
   {
      vWriteToDevErrmem(pcBuffer,s32Size+4);
   }
#endif
}


void vWriteToErrMem(tS32 trClass, const char* pBuffer,int Len,tU32 TrcType/*OSAL_STRING_OUT for string*/)     
{
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
  char cBuf[3];
  cBuf[2] = 0;
  if(Len > (ERRMEM_MAX_ENTRY_LENGTH-3))Len = ERRMEM_MAX_ENTRY_LENGTH-3;
  cBuf[0] = (tChar) (trClass & 0xFF);
  cBuf[1] = (tChar) (trClass >> 8) & 0xFF;
  if(TrcType)cBuf[2] = (tChar) TrcType;
  OSAL_tIODescriptor hErrMem = 0;
   /* check if last process is in termination process complete shutdown scenario*/
   if(pOsalData->u32AttachedProcesses == 0)
   {
         TraceString(pBuffer);
   }
   else
   {
      hErrMem = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_WRITEONLY);
      if(hErrMem != OSAL_ERROR)
      {
        trErrmemEntry  rErrmemEntry = {0};
        rErrmemEntry.u16Entry = trClass;
        (tVoid) OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime);
        rErrmemEntry.eEntryType = eErrmemEntryNormal;
        rErrmemEntry.u16EntryLength = (tU16)Len+3;
        (tVoid) OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[0], cBuf,3);
        if(TrcType)OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[3], pBuffer,(tU32)Len);
        else OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[2], pBuffer,(tU32)Len);
        OSAL_s32IOWrite( hErrMem, (tPCS8)&rErrmemEntry, sizeof(rErrmemEntry));
        OSAL_s32IOClose(hErrMem);
     }
  }
#else
  char cWriteBuffer[ERRMEM_MAX_ENTRY_LENGTH];
  if(TrcType != 0)
  {
#ifdef OSAL_ERRMEM_TIMESTAMP
     vWriteUtcTime(errmem_fd);
#endif
  }
  if (Len > (ERRMEM_MAX_ENTRY_LENGTH-4)) Len = ERRMEM_MAX_ENTRY_LENGTH-4;
  cWriteBuffer[0] = 0;
  cWriteBuffer[1] = (tChar) (trClass & 0xFF);
  cWriteBuffer[2] = (tChar) (trClass >> 8) & 0xFF;
  if(TrcType)cWriteBuffer[3] = (tChar) TrcType;
  if(TrcType)OSAL_pvMemoryCopy(&cWriteBuffer[4], pBuffer,(tU32)Len);
  else OSAL_pvMemoryCopy(&cWriteBuffer[3], pBuffer,(tU32)Len);
  vWriteToDevErrmem(cWriteBuffer,Len+3);
#endif
}

tU32 u32ExecuteCallback(tS32              s32OwnPrcId,
                        tS32              callbackpid,      /* PID of process who rgistered the callback */
                        OSAL_tpfCallback  pcallback,        /* callabck function */
                        tPVoid            pcallbackarg,     /* argument for callback function */
                        tU32              callbackargsize)  /* size of argument */
{
  tU32 u32ErrorCode = OSAL_E_INVALIDVALUE;
  tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
  if(s32PidEntry == OSAL_ERROR)
  {
     NORMAL_M_ASSERT_ALWAYS();
  }
  else
  {
     /* check if callback handler task for this process already exists */  
     s32StartCbHdrTask(s32PidEntry);
  }
  
  if((pcallback)&&(callbackpid))
  {
     if(callbackargsize > OSAL_MBX_CB_ARG_MAX_SIZE)
     {
        vSetErrorCode(OSAL_C_THREAD_ID_SELF, OSAL_E_INVALIDVALUE);
        FATAL_M_ASSERT_ALWAYS();
     }

     if(!s32OwnPrcId)
     {
         s32OwnPrcId = OSAL_ProcessWhoAmI();
     }

     /* check for callback in own process */
     if(s32OwnPrcId == callbackpid)
     {
        u32ErrorCode = u32ExecuteLiCb(0,
                                      s32OwnPrcId,
                                      pcallback,
                                      pcallbackarg,
                                      NULL);
     }
     else
     {
        tOsalMbxMsg rMsg;

        if(pcallbackarg == NULL)
        {
           NORMAL_M_ASSERT_ALWAYS();
        }
        else
        {
           rMsg.rOsalMsg.Cmd = MBX_CB_ARG;
           rMsg.rCBArgMsg.u32CallFun = (uintptr_t)pcallback;
           if(callbackargsize < OSAL_MBX_CB_ARG_MAX_SIZE)
           {
              memcpy(rMsg.rCBArgMsg.au8Arg, pcallbackarg, callbackargsize);
           }
           else
           {
               NORMAL_M_ASSERT_ALWAYS();
           }
        }
        rMsg.rOsalMsg.ID  = callbackpid;
         /* send info to central CB dispatcher task for callback execution/dispatching */
        if(OSAL_s32MessageQueuePost(GetPrcLocalMsgQHandle(callbackpid), (tPCU8)&rMsg, sizeof(tOsalMbxMsg), 0) == OSAL_ERROR)
        {
           u32ErrorCode = OSAL_u32ErrorCode();
        }
        else
        {
           u32ErrorCode = OSAL_E_NOERROR;
        }
     }
  }
  return u32ErrorCode;
}

void vWritePrcFsToErrMem(const char* path)   //Lint removal
{
    char szCommand[100];

    OSAL_szStringCopy(szCommand,"cat ");
    OSAL_szStringConcat(szCommand,path);
    OSAL_szStringConcat(szCommand, " >> /dev/errmem\n");
    
    system(szCommand);
}

void TraceWriteLine(const char* file,  tU32 line,tBool bWrite)
{
  tS32 tskid = OSAL_ThreadWhoAmI();
  char TraceBuffer[250] = {0};
  trThreadElement *pCurrentEntry;

  memset(&TraceBuffer[0], 0, 240);
  pCurrentEntry = tThreadTableSearchEntryByID(tskid,TRUE); 
  if((pCurrentEntry))
  {
      snprintf(&TraceBuffer[0],240,"Task:%s(%d) hit File=[%s] at line %d \n",
               pCurrentEntry->szName,
               tskid,
               file,
               (unsigned int)line);
  }
  else
  {
      snprintf(&TraceBuffer[0],240,"Task:%s(%d) hit File=[%s] at line %d \n",
               "Task",
               tskid,
               file,
               (unsigned int)line);
  }
  TraceString(TraceBuffer);
  if(bWrite)
  {
     if(pOsalData->u32AttachedProcesses > 0)
     {
        vWritePrintfErrmem(TraceBuffer);
     }
  }
}

#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osallock.c
|-----------------------------------------------------------------------*/
