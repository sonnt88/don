/*****************************************************************************
| FILE:         osalmutex.c
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Mutex-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define LINUX_C_U32_MUTEX_MAGIC                 ((tU32)0x5345AF00)
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/


tS32 s32MutexTableCreate(void);

tS32 s32MutexTableDeleteEntries(void);

tS32 OSAL_s32MutexCreate_Opt(tCString coszName,
                                       OSAL_tMtxHandle * phMutex,
                                       tU32 u32Opt);

static trMutexElement* tMutexTableGetFreeEntry(tVoid);

static trMutexElement* tMutexTableSearchEntryByName(tCString coszName);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    s32MutexTableDeleteEntries
 *
 * DESCRIPTION: This function deletes all elements from OSAL table,
 *
 * PARAMETER:
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32MutexTableDeleteEntries(void)
{
   trMutexElement *pCurrentEntry = &pMutexEl[0];
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;

   if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
   {
      /* search the whole table */
      while( pCurrentEntry < &pMutexEl[pOsalData->MutexTable.u32MaxEntries] )
      {
         if(pCurrentEntry->bIsUsed == TRUE)
         {
           s32DeleteCounter++;

           s32ReturnValue = pthread_mutex_destroy(&pCurrentEntry->hMutex);

           if( s32ReturnValue == 0)
           {
               s32ReturnValue = OSAL_OK;
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = s32DeleteCounter;
            }
            else
            {
               s32ReturnValue = OSAL_ERROR;
               break;
            }
         }
         else
            pCurrentEntry++;
      }
      UnLockOsal(&pOsalData->MutexTable.rLock);
   }

   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION:    s32MutexTableCreate
 *
 * DESCRIPTION: This function creates the Mutex Table List. If
 *              there isn't space it returns a error code.
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32MutexTableCreate(void)
{
   tU32 tiIndex;

   pOsalData->MutexTable.u32UsedEntries   = 0;
   pOsalData->MutexTable.u32MaxEntries    = pOsalData->u32MaxNrMutexElements;

   for( tiIndex=0; tiIndex <  pOsalData->MutexTable.u32MaxEntries ; tiIndex++ )
   {
       pMutexEl[tiIndex].u32MutexID = LINUX_C_U32_MUTEX_MAGIC;
       pMutexEl[tiIndex].bIsUsed = FALSE;
   }
   return OSAL_OK;
}


/*****************************************************************************
 *
 * FUNCTION:    tMutexTableGetFreeEntry
 *
 * DESCRIPTION: this function goes throught the Mutex List and returns the
 *              first MutexElement.In case no entries are available a new
 *              defined number of entries is attempted to be added.
 *              In case of succes the first new element is returned, otherwise
 *              a NULL pointer is returned.
 *
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trMutexElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static  trMutexElement* tMutexTableGetFreeEntry(tVoid)
   {
   trMutexElement *pCurrent  = &pMutexEl[0];
   tU32 used = pOsalData->MutexTable.u32UsedEntries;

   if (used < pOsalData->MutexTable.u32MaxEntries)
      {
      pCurrent = &pMutexEl[used];
      pOsalData->MutexTable.u32UsedEntries++;
   } else {
      /* search an entry with !bIsUsed, used == MaxEntries */
      while ( pCurrent <= &pMutexEl[used]
         && ( pCurrent->bIsUsed == TRUE) )
         {
         pCurrent++;
      }
      if(pCurrent >= &pMutexEl[used])
      {
         pCurrent = NULL; /* not found */
      }
   }

   return(pCurrent);
}


/*****************************************************************************
 *
 * FUNCTION:    tMutexTableSearchEntryByName
 *
 * DESCRIPTION: this function goes throught the Mutex List and returns the
 *              MutexElement with the given name or NULL if all the List has
 *              been checked without success.
 *
 * PARAMETER:   coszName (I)
 *                 Mutex name wanted.
 *
 * RETURNVALUE: trMutexElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trMutexElement *
tMutexTableSearchEntryByName(tCString coszName)
{
   trMutexElement *pCurrent = &pMutexEl[0];
   tU32 used = pOsalData->MutexTable.u32UsedEntries;

   while( (pCurrent < &pMutexEl[used])
          && ((pCurrent->bIsUsed == FALSE )
              || (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0)) )
   {
      pCurrent++;
   }
   if (pCurrent >= &pMutexEl[used]) {
      pCurrent = NULL;
   }
   return(pCurrent);
}

/*****************************************************************************
*
* FUNCTION:    bCleanUpMutexofContext
*
* DESCRIPTION: this function goes through the Thread List deletes the
*              Mutex and MutexElement with the given context from list
*
* PARAMETER:   to be of used the context is the "getpid()": when the process
*              exits we could clean up "stale" resources
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
*****************************************************************************/
void vCleanUpMutexofContext(void)
{
   trMutexElement *pCurrent = &pMutexEl[0];
   tU32 max = pOsalData->MutexTable.u32MaxEntries;
   tS32 s32Pid = OSAL_ProcessWhoAmI();

   while (pCurrent < &pMutexEl[max])
   {
       if((pCurrent->bIsUsed == TRUE)&&(s32Pid == pCurrent->s32PID))
       {
           while(pCurrent->u16OpenCounter > 0)
           {
                if(OSAL_s32MutexClose((OSAL_tSemHandle)pCurrent) != OSAL_OK)
                {
                   TraceString("OSAL_s32MutexClose for 0x%x failed",pCurrent);
                   break;
                }
                if(OSAL_s32MutexDelete(pCurrent->szName) != OSAL_OK)
                {
                    TraceString("OSAL_s32MutexDelete for %s failed",pCurrent->szName);
                }
           }
       }
       pCurrent++;
  }
}
/*****************************************************************************
 *
 * FUNCTION:     tMutexCheckHandle
 *
 *
 * DESCRIPTION: this function checks the handle for validity
 *
 * PARAMETER:   handle (I)
 *                Mutex handle
 *
 * RETURNVALUE: trMutexElement*
 *                 pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trMutexElement* tMutexCheckHandle(OSAL_tSemHandle handle)
{
   trMutexElement *pCurrent = (trMutexElement*)handle;

   /* Check casted pointer */
   if((pCurrent >= &pMutexEl[0])&&(pCurrent <= &pMutexEl[pOsalData->MutexTable.u32UsedEntries]))
//   if (pCurrent && ((tU32)pCurrent!=OSAL_C_INVALID_HANDLE))
   {
      if( ( pCurrent->u32MutexID != LINUX_C_U32_MUTEX_MAGIC) ||
          ( pCurrent->bIsUsed == FALSE) )
      {
         vWritePrintfErrmem("OSALMUT Wrong Handle -> Used Flag %d ID:%d Handle:0x%x", pCurrent->bIsUsed,pCurrent->u32MutexID,pCurrent);
         pCurrent = OSAL_NULL;
      }
   }
   else
   {
     vWritePrintfErrmem("OSALMUT memstart:0x%x memend:0x%x Handle:0x%x", &pMutexEl[0],&pMutexEl[pOsalData->MutexTable.u32UsedEntries+1],pCurrent);
     pCurrent=OSAL_NULL;
   }
   return pCurrent;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
void vTraceMutInfo(trMutexElement *pCurrentEntry,tU8 Type,tU32 u32Level,tU32 u32Error,tCString coszName)
{
   char name[TRACE_NAME_SIZE];
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
   tCString Operation;
   switch(Type)
   {
     case CREATE_OPERATION:
         Operation = "CREATE_OPERATION";
         break;
     case DELETE_OPERATION:
         Operation = "DELETE_OPERATION";
         break;
     case OPEN_OPERATION:
         Operation = "OPEN_OPERATION";
         break;
     case CLOSE_OPERATION:
         Operation = "CLOSE_OPERATION";
         break;
     case WAIT_OPERATION:
         Operation = "LOCK_OPERATION";
         break;
     case POST_OPERATION:
         Operation = "UNLOCK_OPERATION";
         break;
     case STATUS_OPERATION:
         Operation = "STATUS_OPERATION";
         break;
     default:
         Operation = "Unknown";
         break;
   }
   if(coszName == NULL)coszName ="Unknown";
   if(pCurrentEntry)
   {
#ifdef NO_PRIxPTR
      TraceStringLevel(u32Level,"Mutex %s Task:%s(%d) Handle:0x%x Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(unsigned int)&pCurrentEntry->hMutex,(unsigned int)u32Error,coszName);
#else
      TraceStringLevel(u32Level,"Mutex %s Task:%s(%d) Handle:0x%" PRIxPTR " Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(uintptr_t)&pCurrentEntry->hMutex,(unsigned int)u32Error,coszName);
#endif
   }
   else
   {
       TraceStringLevel(u32Level,"Mutex %s Task:%s(%d) Handle:0x%x Error:0x%x Name:%s",
                Operation,name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
   }
}

/*****************************************************************************
 * FUNCTION: OSAL_s32MutexCreate_Cntxt
 *
 *
 * DESCRIPTION:   this function create a OSAL Mutex by a Nucleus Mutex.
 *
 * PARAMETERS:
 *       coszName (I)
 *          name of Mutex
 *       phMutex (O)
 *          pseudo-handle for Mutex
 *       uCount (I)
 *          payload of Mutex
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MutexCreate(tCString coszName,
                                   OSAL_tMtxHandle * phMutex)
{

   return(OSAL_s32MutexCreate_Opt(coszName,phMutex,0));
}


/*****************************************************************************
 * FUNCTION: OSAL_s32MutexCreate_Cntxt_Opt
 *
 *
 * DESCRIPTION:   this function create a OSAL Mutex by a Nucleus Mutex.
 *
 * PARAMETERS:
 *       coszName (I)
 *          name of Mutex
 *       phMutex (O)
 *          pseudo-handle for Mutex
 *       uCount (I)
 *          payload of Mutex
 *       u16ContextID (I)
 *          context identification
 *       u16Option (I)
 *          Mutex options
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MutexCreate_Opt(tCString coszName,
                                       OSAL_tMtxHandle * phMutex,
                                       tU32 u32Opt)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry = NULL;

   if( phMutex )
   {
      if( coszName )  /* FIX 15/10/2002 */
      {
         if( OSAL_u32StringLength(coszName) < (tU32)OSAL_C_U32_MAX_NAMELENGTH )
         {
            if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
            {
               pCurrentEntry = tMutexTableSearchEntryByName(coszName);
               if( pCurrentEntry == OSAL_NULL )
               {
                  pCurrentEntry = tMutexTableGetFreeEntry ();
                  if( pCurrentEntry != OSAL_NULL )
                  {

                      pthread_mutexattr_init(&pCurrentEntry->mutexattr);
                      if(u32Opt == 1)
                      {
                         pthread_mutexattr_settype(&pCurrentEntry->mutexattr,PTHREAD_MUTEX_RECURSIVE);
                      }
                      else
                      {
                         pthread_mutexattr_settype(&pCurrentEntry->mutexattr,PTHREAD_MUTEX_NORMAL);
                      }
                      pthread_mutexattr_setrobust(&pCurrentEntry->mutexattr,PTHREAD_MUTEX_ROBUST);
                      if(pthread_mutex_init(&pCurrentEntry->hMutex,&pCurrentEntry->mutexattr) == 0)
                      {
                         pCurrentEntry->bIsUsed = TRUE;
                         pCurrentEntry->bToDelete = FALSE;
                         pCurrentEntry->u16OpenCounter = 1;
                         pCurrentEntry->bTraceMut      = pOsalData->bMutexTaceAll;
                         pCurrentEntry->u32LockCount   = 0;
                         pCurrentEntry->s32TID         = 0;
                         pCurrentEntry->u16Option      = u32Opt;
                         pCurrentEntry->s32PID         = OSAL_ProcessWhoAmI();
                        /*
                        if(pOsalData->szCheckSemName[0] != 0)
                        {
                           if(!strncmp(coszName,pOsalData->szCheckSemName,strlen(pOsalData->szCheckSemName)))
                           {
                              pCurrentEntry->bCheckSem = TRUE;
                           }
                        }*/
                        if(pOsalData->szMtxName[0] != 0)
                        {
                           if(!strncmp(coszName,pOsalData->szMtxName,strlen(pOsalData->szMtxName)))
                           {
                              TraceString("OSALMTX Found Trace Mtx %s",coszName);
                              pCurrentEntry->bTraceMut = TRUE;
                           }
                        }
 
                      (void)OSAL_szStringNCopy ((tString)pCurrentEntry->szName,
                                          coszName,OSAL_C_U32_MAX_NAMELENGTH);
                      *phMutex = (uintptr_t)pCurrentEntry;
                      s32ReturnValue = OSAL_OK;
                      pOsalData->u32MutResCount++;
                      if(pOsalData->u32MaxMutResCount < pOsalData->u32MutResCount)
                      {
                         pOsalData->u32MaxMutResCount = pOsalData->u32MutResCount;
                         if(pOsalData->u32MaxMutResCount > (pOsalData->u32MaxNrMutexElements*9/10))
                         {
                            pOsalData->u32NrOfChanges++;
                         }
                      }
                    }
                    else
                    {
                       FATAL_M_ASSERT_ALWAYS();
                       u32ErrorCode = u32ConvertErrorCore(errno);
                    }
                  }
                  else
                  {
                     u32ErrorCode = OSAL_E_NOSPACE;
                  }   
               }
               else
               {
                  u32ErrorCode = OSAL_E_ALREADYEXISTS;
               }   
               UnLockOsal(&pOsalData->MutexTable.rLock);
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_NAMETOOLONG;
         }//if( OSAL_u32StringLength(coszName) < OSAL_C_U32_MAX_NAMELENGTH )
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      } // if( coszName )
   }/*if(u32ErrorCode == OSAL_E_NOERROR)*/
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if((u32ErrorCode == OSAL_E_ALREADYEXISTS)&&(u32OsalSTrace & ~0x00000040))
      {
         vTraceMutInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceMutInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MTX,TR_LEVEL_DATA))
       ||(pCurrentEntry->bTraceMut == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */                     
      {
         vTraceMutInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MutexDelete()
 *
 * DESCRIPTION: this function removes an OSAL Mutex.
 *
 * PARAMETER:   coszName (I)
 *                 Mutex name to be removed.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MutexDelete (tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry = NULL;

   if( coszName )
   {
      if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
      {
         pCurrentEntry =  tMutexTableSearchEntryByName (coszName);
         if( pCurrentEntry != OSAL_NULL )
         {
            if( pCurrentEntry->u16OpenCounter == 0 )
            {
               s32ReturnValue = pthread_mutex_destroy(&pCurrentEntry->hMutex);
               if(s32ReturnValue == 0)
               {
                  pCurrentEntry->bIsUsed = FALSE;
                  s32ReturnValue = OSAL_OK;
                  pOsalData->u32MutResCount--;
               }
               else
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  s32ReturnValue = OSAL_ERROR;
               }
            }
            else
            {
               pCurrentEntry->bToDelete = TRUE;
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->MutexTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      vTraceMutInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MTX,TR_LEVEL_DATA))
      ||(pCurrentEntry->bTraceMut == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceMutInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MutexOpen()
 *
 * DESCRIPTION: this function returns a valid handle to an OSAL Mutex
 *              already created.
 *
 * PARAMETER:   coszName (I)
 *                 Mutex name to be removed.
 *              phMutex (->O)
 *                 pointer to the Mutex handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32MutexOpen (tCString coszName, OSAL_tMtxHandle * phMutex)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry = NULL;

   if( coszName && phMutex )  /* FIX 15/10/2002 */
   {
      if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
      {
         pCurrentEntry =  tMutexTableSearchEntryByName (coszName);
         if( pCurrentEntry != OSAL_NULL )
         {
            if(pCurrentEntry->s32PID != OSAL_ProcessWhoAmI())
            {
               u32ErrorCode = OSAL_E_NOTSUPPORTED;
            }
            else
            {
               if(!pCurrentEntry->bToDelete )
               {
                  pCurrentEntry->u16OpenCounter++;
                  *phMutex = (OSAL_tSemHandle)pCurrentEntry;
                  s32ReturnValue = OSAL_OK;
               }
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->MutexTable.rLock);
      }
      else
      {
         u32ErrorCode = OSAL_E_BUSY;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
         vTraceMutInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceMutInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_MTX,TR_LEVEL_DATA))
       ||(u32OsalSTrace & 0x00000040)|| (pCurrentEntry->bTraceMut == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceMutInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MutexClose()
 *
 * DESCRIPTION: this function closes an OSAL Mutex.
 *
 * PARAMETER:   hMutex (I)
 *                 Mutex handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *****************************************************************************/
tS32 OSAL_s32MutexClose (OSAL_tMtxHandle hMutex)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry = NULL;

   if(LockOsal(&pOsalData->MutexTable.rLock) == OSAL_OK)
   {
      pCurrentEntry = tMutexCheckHandle(hMutex);
      if( pCurrentEntry != OSAL_NULL )
      {
         if( pCurrentEntry->u16OpenCounter > 0 )
         {
            pCurrentEntry->u16OpenCounter--;
            s32ReturnValue = OSAL_OK;
            if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
            {
               s32ReturnValue = pthread_mutex_destroy(&pCurrentEntry->hMutex);
               if(s32ReturnValue == 0)
               {
                   pCurrentEntry->bIsUsed = FALSE;
                   pCurrentEntry->bToDelete = FALSE;
                   s32ReturnValue = OSAL_OK;
                   pOsalData->u32MutResCount--;
               }
               else
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  s32ReturnValue = OSAL_ERROR;
               }
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }

      UnLockOsal(&pOsalData->MutexTable.rLock);
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceMutInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((u32OsalSTrace & 0x00000040)|| (pCurrentEntry->bTraceMut == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceMutInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MutexUnLock()
 *
 * DESCRIPTION:   this function release a Mutex
 *
 *
 * PARAMETER:   hMutex (I)
 *                 Mutex handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *****************************************************************************/
tS32 OSAL_s32MutexUnLock (OSAL_tMtxHandle hMutex)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry=NULL;

   pCurrentEntry = tMutexCheckHandle(hMutex);
   if( pCurrentEntry != OSAL_NULL )
   {
      if(pCurrentEntry->u16Option == 1)
      {
         if(pCurrentEntry->s32TID == OSAL_ThreadWhoAmI())
         {
            if(pCurrentEntry->u32LockCount > 1)
            {  
               pCurrentEntry->u32LockCount--;
               return OSAL_OK;
            }
            else
            {
               pCurrentEntry->s32TID = 0;
               pCurrentEntry->u32LockCount = 0;
            }
         }
      }
      s32ReturnValue = pthread_mutex_unlock(&pCurrentEntry->hMutex);
      if(s32ReturnValue == 0)
      {
         s32ReturnValue = OSAL_OK;
      }
      else
      {
         s32ReturnValue = OSAL_ERROR;
         u32ErrorCode = u32ConvertErrorCore(errno);
      }
   }
   else
   {
       u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      vTraceMutInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((u32OsalSTrace & 0x00000040)||(pCurrentEntry->bTraceMut == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceMutInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32MutexLock()
 *
 * DESCRIPTION:   This function waits for an OSAL Mutex
 *
 *
 * PARAMETER:     hMutex (I)
 *                   Mutex handle.
 *                msec (I)
 *                   max delaytime before timeout.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *****************************************************************************/
tS32 OSAL_s32MutexLock (OSAL_tMtxHandle hMutex, OSAL_tMSecond msec)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trMutexElement *pCurrentEntry = NULL;
   OSAL_tMSecond s32Timeout = msec;
   struct timespec tim = {0,0};
   errno = 0;
   
   pCurrentEntry = tMutexCheckHandle(hMutex);
   if( pCurrentEntry != OSAL_NULL )
   {
      if(pCurrentEntry->u16Option == 1)
      {
         if((pCurrentEntry->s32TID == OSAL_ThreadWhoAmI())&&(pCurrentEntry->u32LockCount))
         {
            pCurrentEntry->u32LockCount++;
            return OSAL_OK;
         }
      }
      if(pCurrentEntry->bTraceMut == TRUE)
      {
             TraceString("OSALMTX TID:%d Start Wait %d msec Mtx %s",OSAL_ThreadWhoAmI(),msec,pCurrentEntry->szName);
      }
      if(msec == OSAL_C_TIMEOUT_FOREVER)
      {
         s32ReturnValue = pthread_mutex_lock(&pCurrentEntry->hMutex);
      }
      else if(msec == OSAL_C_TIMEOUT_NOBLOCKING)
      {
          s32ReturnValue = pthread_mutex_trylock(&pCurrentEntry->hMutex);
      }
      else
      {
          if(s32Timeout < pOsalData->u32TimerResolution)
          {
             s32Timeout = pOsalData->u32TimerResolution;
          }
          u32ErrorCode = u32GemTmoStruct(&tim,s32Timeout);
          s32ReturnValue = pthread_mutex_timedlock(&pCurrentEntry->hMutex,&tim);
      }

      if(s32ReturnValue == 0)
      {
         if(pCurrentEntry->u16Option == 1)
         {
            pCurrentEntry->s32TID = OSAL_ThreadWhoAmI();
            pCurrentEntry->u32LockCount++;
         }
         s32ReturnValue = OSAL_OK;
      }
      else
      {
         if((msec != OSAL_C_TIMEOUT_FOREVER)&&(errno == 0))
         {
            u32ErrorCode = OSAL_E_TIMEOUT;
         }
         else
         {
           u32ErrorCode = u32ConvertErrorCore(errno);
         }
         s32ReturnValue = OSAL_ERROR;
      }
   }
   else
   {
          u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if(u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode != OSAL_E_TIMEOUT)
      {
         tCString coszName;
         if(pCurrentEntry)coszName = pCurrentEntry->szName;
         else coszName = "NO_NAME";
         vTraceMutInfo(pCurrentEntry,WAIT_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }
   else
   {
      if((pCurrentEntry->bTraceMut == TRUE)||(u32OsalSTrace & 0x00000040))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceMutInfo(pCurrentEntry,WAIT_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return s32ReturnValue;
}


#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
