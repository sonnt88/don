/******************************************************************************
| FILE:         osalfpe.cpp
| PROJECT:      Platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION: This is the implementation file for the OSAL for
|              Floating Point Exception handling in Linux
|------------------------------------------------------------------------------
| COPYRIGHT: (c) 2011 Robert Bosch Engineering and Business Solutions India Limited
| HISTORY:      
| Date        | Modification                | Author
| 03.08.2011  | Initial revision            | Anooj Gopi (RBEI/ECF1)
| 26.09.2011  | __USE_GNU macro is removed  | Sudharsanan Sivagnanam (RBEI/ECF1)    |
| --.--.--    | ----------------            | -------, -----
|
|*****************************************************************************/ 

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifndef _GNU_SOURCE    /* This is already defined in g++ */
   #define _GNU_SOURCE
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

//#define __USE_GNU
#include <fenv.h>

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define OSAL_FPE_ALL_EXCEPT (FE_INEXACT|FE_UNDERFLOW|FE_OVERFLOW|FE_INVALID|FE_DIVBYZERO)

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype
|-----------------------------------------------------------------------*/


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/


/*****************************************************************************
*
* FUNCTION: OSAL_vFPEReset
*
* DESCRIPTION: Resets the FPE register for the current thread 
*
* PARAMETER:  None
*
* RETURNVALUE: None
*
* HISTORY:
* Date      |   Modification                         | Authors
* 08.02.11  | Initial revision                       | Anooj Gopi (RBEI/ECF1)
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tVoid OSAL_vFPEReset( tVoid )
{
   feclearexcept(OSAL_FPE_ALL_EXCEPT);
   return;
}

/*****************************************************************************
*
* FUNCTION: OSAL_u8GetFPE
*
* DESCRIPTION: Return a bit pattern of floating point errors for the current thread
*
* PARAMETER:  None
*
* RETURNVALUE: tU8 Floating Point Error bit pattern
*     Bit 0 - I = Inexact operation OSAL_C_U8_FPE_INEXACT
*     Bit 1 - U = Underflow OSAL_C_U8_FPE_UNDERFLOW
*     Bit 2 - O = Overflow OSAL_C_U8_FPE_OVERFLOW
*     Bit 3 - Z = Division by Zero OSAL_C_U8_FPE_DIVZERO
*     Bit 4 - V = Invalid operation OSAL_C_U8_FPE_INVALID
*     Bit 5 - X = reserved (0)
*     Bit 6 - X = reserved (0)
*     Bit 7 - X = reserved (0)
*
* HISTORY:
* Date      |   Modification                         | Authors
* 08.02.11  | Initial revision                       | Anooj Gopi (RBEI/ECF1)
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tU8 OSAL_u8GetFPE( tVoid )
{
   tS32 s32ClibFlag;
   tU8 u8OsalFlag = 0;

   /* Get the Floating Point Exception flags of current thread */
   s32ClibFlag = fetestexcept(OSAL_FPE_ALL_EXCEPT);

   /* Form the OSAL FPE Bitmask from the c library FPE flag */
   if(s32ClibFlag & FE_INEXACT)
      u8OsalFlag |= OSAL_C_U8_FPE_INEXACT;
   if(s32ClibFlag & FE_UNDERFLOW)
      u8OsalFlag |= OSAL_C_U8_FPE_UNDERFLOW;
   if(s32ClibFlag & FE_OVERFLOW)
      u8OsalFlag |= OSAL_C_U8_FPE_OVERFLOW;
   if(s32ClibFlag & FE_DIVBYZERO)
      u8OsalFlag |= OSAL_C_U8_FPE_DIVZERO;
   if(s32ClibFlag & FE_INVALID)
      u8OsalFlag |= OSAL_C_U8_FPE_INVALID;

   return u8OsalFlag;
}

/*****************************************************************************
*
* FUNCTION: OSAL_s32SetFPEMode
*
* DESCRIPTION: Set the Floating point Exception mode for the calling task.
*              FLAG Mode: On FP error, tasks will continue execution
*              but the FP error flags will be updated.
*              Abort Mode: On FP error, task will stop execution and SIGFPE
*              signal will be raised. SIGFPE signal handler implementation
*              determines the further behavior of the system.
*
* PARAMETER: [I] Floating Point exception Mode
*                 OSAL_EN_U8_FPE_MODE_FPE_ABORT - Abort the application on FP error
*                 OSAL_EN_U8_FPE_MODE_FPE_FLAG - Do not abort application on FP error
*
* RETURNVALUE: Error code OSAL_OK on success OSAL_ERROR in case of error
*
* HISTORY:
* Date      |   Modification                         | Authors
* 08.02.11  | Initial revision                       | Anooj Gopi (RBEI/ECF1)
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32SetFPEMode (OSAL_tenFPEMode enMode)
{
   tS32 s32RetVal = OSAL_OK;
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   switch(enMode)
   {
      case OSAL_EN_U8_FPE_MODE_FPE_ABORT:
      {
         /* Enable FPE exception so that signal handler been called on exception */
         if(feenableexcept(OSAL_FPE_ALL_EXCEPT) == -1)
         {
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
         else
         {
            TraceString("feenableexcept succeeded");
         }
         break;
      }
      case OSAL_EN_U8_FPE_MODE_FPE_FLAG:
      {
         /* Disable FPE exception */
         if(fedisableexcept(OSAL_FPE_ALL_EXCEPT) == -1)
         {
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
         else
         {
            TraceString("fedisableexcept succeeded");
         }
         break;
      }
      default:
      {
         /* Invalid mode */
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32RetVal = OSAL_ERROR;
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      TraceString("OSAL_s32SetFPEMode failed Error:%d",u32ErrorCode);
   }

   return s32RetVal;
}

#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalfpe.cpp
|-----------------------------------------------------------------------*/
