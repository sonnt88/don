/*****************************************************************************
| FILE:         osalheap.cpp
| PROJECT:      Platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) OSAL Heap Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Bosch GmbH
| HISTORY:      
| Date      | Modification                                 | Author
| 06.03.13  | Initial revision                             | MRK2HI
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include "osalheap.h"
#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif


/**************************************************************************/
/* typedefs                                                               */
/**************************************************************************/


/**************************************************************************/
/* variables                                                              */
/**************************************************************************/
static tU32                    u32Open;
static OSAL_MEM_tHeader       *pBase = 0;
static OSAL_MEM_tSharedHeader *pSharedRead = 0;
static OSAL_MEM_tSharedHeader *pSharedWrite = 0;

static OSAL_tShMemHandle HeapSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
static OSAL_tSemHandle   SemHandle        = (OSAL_tSemHandle)OSAL_C_INVALID_HANDLE;
static OSAL_tIODescriptor fd              = (intptr_t)OSAL_C_INVALID_HANDLE;
static tString szHeapName                 = NULL;
static tBool bLocalMem                    = FALSE;
static tU32 u32HeapSize                   = 0;
static tU32 u32Guard = 0;

/**************************************************************************/
/* defines                                                                */
/**************************************************************************/
#define KEY_HEAP_SIZE "HEAPSIZE"
#define KEY_GARD_SIZE "GUARDSIZE"

#define POINTER_2_OFFSET( p ) \
   ( ( (uintptr_t) (p) - (uintptr_t) (pBase) ) / \
   (uintptr_t) sizeof( OSAL_MEM_tHeader ) )

#define OFFSET_2_POINTER( o ) \
   ( pBase + (o) )


#define OSAL_MEM_MAGIC     ((tU16) 0xDACA)
#define OSAL_INV_MEM_MAGIC ((tU16) 0xCADA)
#define OSAL_DEL_MEM_MAGIC ((tU16) 0xFFFF)


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/


/*****************************************************************************
|  Function prototypes (module-local):
|---------------------------------------------------------------------------- */
static tS32 s32HeapInstantiate(tU32 u32Size);
static tS32 s32CheckPtr(OSAL_MEM_tHeader *p, tBool bDoRangeCheck, tBool bUseLock);
static tS32 s32SemOp(tS32 s32Operation);
static tS32 s32SemDelete(void);
static tS32 s32SemClose(void);
static tS32 s32SemOpen(tString szName);
static tS32 s32SemCreate(tString szName);
//static tVoid vGetMemSpace(tPU32 pu32MaxFree, tPU32 pu32MaxUsed);
static tVoid vEnterCriticalSection(void);
static tVoid vLeaveCriticalSection(void);
static tVoid vEnterCriticalSection(void);
static tVoid vLeaveCriticalSection(void);
  static tVoid vGetMemSpace(tPU32 pu32MaxFree, tPU32 pu32MaxUsed);
static tVoid OSAL_vPrintMemList(OSAL_tIODescriptor fdesc);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
char TrcBuf[250] = {0};

static void TraceLine(const char* file,  tU32 line)
{
  tS32 tskid = OSAL_ThreadWhoAmI();

  memset(&TrcBuf[0], 0, 240);
  snprintf(&TrcBuf[0],240,"Task:%d hit File=[%s] at line %d \n",
           tskid,
           file,
           line);
  vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&TrcBuf[0],strlen(&TrcBuf[0]),OSAL_STRING_OUT);
}

#define LINE_INFO()   TraceLine(__FILE__, __LINE__ )

/*static tPU32 pu32GetSharedBaseAdress(void)
{
   return (tPU32)pBase;
}*/

   static tS32 OSAL_s32HeapGetCurrentSizeIntern()
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      if (pSharedRead)
      {
         s32ReturnValue = (tS32) (pSharedRead->nCurSize * sizeof(OSAL_MEM_tHeader));
      }
      return s32ReturnValue;
   }

   static tS32 OSAL_s32HeapGetAbsoluteSizeIntern()
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      if (pSharedRead)
      {
         s32ReturnValue=(tS32) (pSharedRead->nAbsSize * sizeof(OSAL_MEM_tHeader));
      }
      return s32ReturnValue;
   }

   static tS32 OSAL_s32HeapGetMinimalSizeInternal( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      if (pSharedRead)
      {
         s32ReturnValue= (tS32) (pSharedRead->nMinSize * sizeof(OSAL_MEM_tHeader));
      }
      else
      {
         u32ErrorCode=OSAL_E_UNKNOWN;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }


void TraceHeapData(const char* cBuffer,...)
{
  tS32 s32Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  s32Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  if(s32Len != OSAL_ERROR)
  {
     LLD_vTrace(OSAL_C_TR_CLASS_SYS_MSGPOOL, TR_LEVEL_DATA,cBuf,s32Len+1);
  }
}



   /* --This define dumps some information about the heap to trace output. */
   tVoid OSAL_M_TRACE_HEAP(tChar flag,tU32 pointer,tU32 size,tU32 remaining)
   {  
         tU32 u32MaxFree,u32MaxUsed;
         OSAL_tThreadID tid=OSAL_ThreadWhoAmI();

         vGetMemSpace(&u32MaxFree,&u32MaxUsed);
         TraceHeapData("%10d\t%10d\t%c\t%#010x\t%10d\t%10d\t%10d\t%10d\t",
         OSAL_ClockGetElapsedTime(),tid,flag,pointer,size,remaining,u32MaxFree,u32MaxUsed);
         if(flag == 'd')
         {
            tU8 i;
            tBool bFirstLoop = FALSE;
            if(((uintptr_t)pointer > (uintptr_t)pBase)
             ||((uintptr_t)pointer < (uintptr_t)pBase + pOsalData->rMsgPoolStruct.u32MemSize))
            {
               tU8* pU8 = (tU8*)OFFSET_2_POINTER(pointer);
               OSALUTIL_s32FPrintf(fd,"'");
               for(i = 12; i < 42; i++)
               {
                  if(bFirstLoop == FALSE)
                  {
                     bFirstLoop = TRUE;
                  }
                  else
                  {
                     TraceHeapData(" ");
                  }
                  TraceHeapData("%02X", pU8[i]);
               }
            }
            TraceHeapData("'");
         }
         TraceHeapData("\n\r");
   }

   /* print message */
   void vPrintMemory(OSAL_tIODescriptor fdesc, const tChar *szBuffer)
   {
     char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
     tU16 u16Len;
     ((void)fdesc);
     OSAL_M_INSERT_T8(&cBuf[0],OSAL_STRING_OUT);
     u16Len = (tU16)strlen(szBuffer);
     if(u16Len > 240) u16Len = 240;
     strncpy(&cBuf[1],szBuffer,u16Len);
     LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,cBuf,u16Len+1);
   }

   /* print error */
   void vPrintMemError(const tChar *szBuffer)
   {
      OSAL_trThreadControlBlock controlBlock;
      if (fd != OSAL_C_INVALID_HANDLE)  /*lint !e737 !e650 */    // INVALID_HANDLE is a tU32, fd a tS32
      {
         OSAL_s32ThreadControlBlock(OSAL_ThreadWhoAmI(), &controlBlock);
         if (controlBlock.szName != NULL)
            OSALUTIL_s32TracePrintf(fd, OSAL_C_U32_TRACE_LEVEL_FATAL_ERROR, OSAL_C_U32_TRACE_CLASS_DEFAULT, "Memory-Error in Thread: %s\n\r", controlBlock.szName);   /*lint !e641 */
         OSALUTIL_s32TracePrintf(fd, OSAL_C_U32_TRACE_LEVEL_FATAL_ERROR, OSAL_C_U32_TRACE_CLASS_DEFAULT, szBuffer);   /*lint !e641 */
      }
      return;
   }


intptr_t LinuxProcessMapping(tU32 u32Size,tString szName)
{
   intptr_t s32ReturnValue = OSAL_ERROR;

   /* Check for first connect */
   if(!pSharedWrite && !pSharedRead && !bLocalMem) 
   {
       HeapSharedMem = OSAL_SharedMemoryOpen(szName,  /* Name */
                                                OSAL_EN_READWRITE);

       /* get read pointer to shared memory */
       pSharedRead = (OSAL_MEM_tSharedHeader*) OSAL_pvSharedMemoryMap(HeapSharedMem,   /* Handle */
                                                                      OSAL_EN_READONLY,              /* Rights */
                                                                      u32Size,                 /* Size + Header */
                                                                      0);   /* Offset */
       /* get write pointer to shared memory */
       pSharedWrite = (OSAL_MEM_tSharedHeader*) OSAL_pvSharedMemoryMap(HeapSharedMem,   /* Handle */
                                                                       OSAL_EN_READWRITE,             /* Rights */
                                                                       u32Size,                 /* Size + Header */
                                                                       0);                            /* Offset */

       /* --check if mapping failed. */
       if (pSharedWrite && pSharedRead) 
       {
         if (s32SemOpen(szName)==OSAL_OK) 
         {
             u32Open++;
            /* --Enter critical section. */
            vEnterCriticalSection();  /*lint !e746 */  // Angeblich gibt es zu der Funktion keinen Prototypen?
            /* set handle counter */
            /* set local base pointer */
            pBase = (OSAL_MEM_tHeader*) ((uintptr_t)pSharedWrite+ sizeof(OSAL_MEM_tSharedHeader));

            if(pBase != (OSAL_MEM_tHeader*) (pSharedWrite+1))
            {
               FATAL_M_ASSERT_ALWAYS();
            }
           //pBase = (OSAL_MEM_tHeader*) (pSharedWrite+1);
            /* --ok! */
            /* synchronize local data with shared Data*/
            vLeaveCriticalSection();
           // s32SemClose();
            s32ReturnValue=HeapSharedMem;
         }
      }
    }
   else
   {
        u32Open++;
        s32ReturnValue=HeapSharedMem;
   }
   return s32ReturnValue;
}

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapCreate
   *----------------------------------------------------------------------
   * DESCRIPTION:  creates the shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    size of the heap
   *               szName of the Heap
   *               given local memory for heap
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapCreate(tU32 u32Size,tString szName,tBool bShared,tBool bDebug)
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR; /* --temporary used error code. */
      tU32 u32NSize, u32HNSize;
      char cBuffer[100+OSAL_C_U32_MAX_NAMELENGTH];

      if((!u32Size)||(strlen(szName) > (tU32)OSAL_C_U32_MAX_NAMELENGTH))return s32ReturnValue;

      /* --Create the semaphore. */
      if (s32SemCreate(szName)==OSAL_OK) 
      {
         /* --Enter critical section. */
         vEnterCriticalSection();
         /* --Trace output. */
         fd = OSAL_IOOpen("/dev/trace", OSAL_EN_WRITEONLY);
         /* calculate block size */
         u32NSize = (u32Size+sizeof(OSAL_MEM_tHeader)-1)/sizeof(OSAL_MEM_tHeader) + 1;
         /* Calculate block size plus header: */
         u32HNSize = (u32NSize+1)*sizeof(OSAL_MEM_tHeader) + sizeof(OSAL_MEM_tSharedHeader);
         if(bShared == FALSE)
         {
            HeapSharedMem = (intptr_t)malloc(u32Size);
            bLocalMem = TRUE;
         }
         else
         {
            /* create shared memory area */
            HeapSharedMem = OSAL_SharedMemoryCreate(szName,  /* Name */
                                                       OSAL_EN_READWRITE,      /* Rights */
                                                       u32HNSize );               /* Size + Header */
         }
         if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_ERROR) 
         {
            /* --Initialize process structure. */
            if (s32HeapInstantiate(u32HNSize)==OSAL_OK) 
            {
               szHeapName = szName;
               u32HeapSize = u32HNSize;
               /* initialise header pointer */
               pBase->s.u16MagicField = OSAL_MEM_MAGIC;
               pBase->s.s32Offset = 2;
               pBase->s.nsize = 0;
               /* initialise first block */
               OFFSET_2_POINTER(pBase->s.s32Offset)->s.u16MagicField = OSAL_MEM_MAGIC;
               OFFSET_2_POINTER(pBase->s.s32Offset)->s.s32Offset = 0;
               OFFSET_2_POINTER(pBase->s.s32Offset)->s.u16MemOffset = 0;
               OFFSET_2_POINTER(pBase->s.s32Offset)->s.nsize = u32NSize;
               if(bDebug)
               {
                  u32Guard = sizeof(OSAL_MEM_tHeader);
                  OSAL_MEM_tHeader* pTmp = OFFSET_2_POINTER(pBase->s.s32Offset);
                  pTmp =  (OSAL_MEM_tHeader*)((uintptr_t)pTmp + ((pTmp->s.nsize-1) * sizeof(OSAL_MEM_tHeader)));
                  pTmp->s.u16MagicField = OSAL_INV_MEM_MAGIC;
                  pTmp->s.s32Offset     = 0;
                  pTmp->s.u16MemOffset  = 0;
                  pTmp->s.nsize         = u32NSize;
               }
               /* initialise shared header */
               pSharedWrite->freeOffset = 0;
               pSharedWrite->nAbsSize = u32NSize;
               pSharedWrite->nCurSize = u32NSize;
               pSharedWrite->nMinSize = u32NSize;
               pSharedWrite->nMaxMemBlkSize = 0;
               pSharedWrite->n32Counter = 1;
               
               /* write size to registry*/
               tS32 s32DevDesc = 0;
               intptr_t s32Val = 0;
               OSAL_trIOCtrlRegistry rReg;
               
               OSAL_IOCreate("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/HEAP",OSAL_EN_READWRITE);
               snprintf(cBuffer,100+OSAL_C_U32_MAX_NAMELENGTH,
                        "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/HEAP/%s",
                        szName);
               OSAL_IOCreate(cBuffer,OSAL_EN_READWRITE);

               if((s32DevDesc = OSAL_IOOpen(cBuffer,OSAL_EN_READWRITE)) == OSAL_OK)
               {
                  s32Val = u32HNSize;
                  rReg.pcos8Name = (const tS8*)KEY_HEAP_SIZE;
                  rReg.ps8Value  = (tU8*)&s32Val;
                  rReg.u32Size   = sizeof(tS32);
                  rReg.s32Type = OSAL_C_S32_VALUE_S32;
                  u32ErrorCode = OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg);
                  s32Val = u32Guard;
                  rReg.pcos8Name = (const tS8*)KEY_GARD_SIZE;
                  rReg.ps8Value  = (tU8*)&s32Val;
                  rReg.u32Size   = sizeof(tS32);
                  rReg.s32Type = OSAL_C_S32_VALUE_S32;
                  u32ErrorCode = OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg);
                  OSAL_s32IOClose(s32DevDesc);
               }
               /* --ok! */
               s32ReturnValue=OSAL_OK;
            }
            else 
            {
               if(bShared)
               {
                  /* --Delete the shared memory. */
                  OSAL_s32SharedMemoryClose(HeapSharedMem);
                  OSAL_s32SharedMemoryDelete(szHeapName);
               }
               HeapSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
               /* --Internal Error. */
               u32ErrorCode=OSAL_E_UNKNOWN;
            }
        } 
        /* --Leave critical section. */
        vLeaveCriticalSection();    /*lint !e746 */    // Angeblich gibt es zu der Funktion keinen Prototypen?
      }
    
      if(s32ReturnValue == OSAL_ERROR)
      {
         s32SemClose();
         s32SemDelete();
      } 

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapOpen
   *----------------------------------------------------------------------
   * DESCRIPTION:  opens an existing shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    name of the heap
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                       | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapOpen( tString szName )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      tU32 u32Size = 0;
      char cBuffer[100+OSAL_C_U32_MAX_NAMELENGTH];

      /* --Check if first call of open. */
      if (HeapSharedMem == (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Try to open critical section semaphore. */
         if (s32SemOpen(szName)==OSAL_OK) 
         {
            /* --Enter critical section. */
            vEnterCriticalSection();  /*lint !e746 */  // Angeblich gibt es zu der Funktion keinen Prototypen?
            /* --Trace output. */
            if(fd == (tS32)OSAL_C_INVALID_HANDLE)
            {
                fd = OSAL_IOOpen("/dev/trace", OSAL_EN_WRITEONLY);
            }

            if(!bLocalMem)
            {
               /* open shared memory area */
               HeapSharedMem = OSAL_SharedMemoryOpen(szName,    /* Name */
                                                        OSAL_EN_READWRITE);       /* Rights */
            }
            if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_ERROR) 
            {
               OSAL_tIODescriptor s32DevDesc = 0;
               tS32 s32Val = 0;
               OSAL_trIOCtrlRegistry rReg;
               snprintf(cBuffer,100+OSAL_C_U32_MAX_NAMELENGTH,
                        "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/HEAP/%s",
                        szName);
               if((s32DevDesc = OSAL_IOOpen(cBuffer,OSAL_EN_READWRITE)) == OSAL_OK)
               {
                  rReg.pcos8Name = (const tS8*)KEY_HEAP_SIZE;
                  rReg.ps8Value  = (tU8*)&u32Size;
                  rReg.u32Size   = sizeof(tS32);
                  rReg.s32Type = OSAL_C_S32_VALUE_S32;
                  u32ErrorCode = OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGGETVALUE,(intptr_t)&rReg);
                  if(!bLocalMem)
                  {
                     rReg.pcos8Name = (const tS8*)KEY_GARD_SIZE;
                     rReg.ps8Value  = (tU8*)&u32Guard;
                     rReg.u32Size   = sizeof(tS32);
                     rReg.s32Type = OSAL_C_S32_VALUE_S32;
                     u32ErrorCode = OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGGETVALUE,(intptr_t)&rReg);
                  }
                  OSAL_s32IOClose(s32DevDesc);
               }
               if(s32HeapInstantiate(0) == OSAL_OK)
               {
                  s32ReturnValue=OSAL_OK;
                  if((pSharedRead == NULL) && (pSharedWrite == NULL))
                  {
                     LinuxProcessMapping(s32Val,szName);
                  }
                  pSharedWrite->n32Counter++;
               }
               else 
               {
                  if(!bLocalMem)
                  {
                     /* --Close the shared memory block. */
                     OSAL_s32SharedMemoryClose(HeapSharedMem);
                  }
                  /* --Something went wrong. */
                  u32ErrorCode=OSAL_E_UNKNOWN;
               }
            } 
            else
               /* --Does not exists. */
               u32ErrorCode=OSAL_E_DOESNOTEXIST;
            /* --Leave critical section. */
            vLeaveCriticalSection();

            /* --If something failed, reset the critical semaphore handle. */
            if (s32ReturnValue==OSAL_ERROR) 
            {
               s32SemClose();
               HeapSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
            }
         } 
         else
            /* --Does not exist.*/
            u32ErrorCode=OSAL_E_DOESNOTEXIST;    
      } 
      else 
      {
         /* --Already opened, just increment the open counter. */
         u32Open++;
         s32ReturnValue=OSAL_OK;
      }
      /* --Set error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Go back. */
      return s32ReturnValue;
   }


   /************************************************************************
   *
   * FUNCTION:     OSAL_vPrintHeapLeaks
   *----------------------------------------------------------------------
   * DESCRIPTION:  undocumented internal function: prints memory leaks
   *                      kein Schutz durch Semaphore; muss vor dem Aufruf gesch�tzt sein!!!
   *----------------------------------------------------------------------
   * PARAMETER:    (I) fd: Osal file descriptor
   * RETURNVALUE:  void
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   void OSAL_vPrintHeapLeaks(OSAL_tIODescriptor fdesc)
   {
      tChar szBuffer[1024];
      tChar szSmallBuf[10];
      tU32 u32MsgSize;
      OSAL_MEM_tHeader *p;
      tPU8 pMessage;
      tU32 nsize;
      tU16 u16Co;

         if (pSharedRead) 
         {
            nsize = pSharedRead->nAbsSize;
            for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
            {
               if(s32CheckPtr(p,TRUE, FALSE) == OSAL_ERROR)
               {
                  vWritePrintfErrmem("!!! MemPool ERROR: Real 0x%8x, Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p), p->s.nsize);
                  TraceString("!!! MemPool ERROR: Real 0x%8x, Offset %4d, Size %d\r\n",  p, POINTER_2_OFFSET(p), p->s.nsize);
                  return;
               }

               if(p->s.u16MemOffset == OSAL_DEL_MEM_MAGIC)// clarif
               {
                  OSAL_s32PrintFormat( szBuffer, "0, Deleted marked block, Unknown queue, '");
                  u32MsgSize = (uintptr_t) p->s.nsize*sizeof(OSAL_MEM_tHeader);
                  pMessage = (tPU8) (p+1);
                  for (u16Co = 0; u16Co < u32MsgSize && u16Co < 36; u16Co++)
                  {
                     snprintf(szSmallBuf,10,"%.2x ", pMessage[u16Co]);
                     strcat(szBuffer, szSmallBuf);
                  }
                  strcat(szBuffer, "'\r\n");
                  vPrintMemory(fdesc, szBuffer);
               }
               else if(p->s.s32Offset == -1)
               {
                  OSAL_s32PrintFormat( szBuffer, "0, Used Block, Unknown queue, '");
                  u32MsgSize = (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) - p->s.u16MemOffset;
                  pMessage = (tPU8) (p+1);
                  for (u16Co = 0; u16Co < u32MsgSize && u16Co < 36; u16Co++)
                  {
                     snprintf(szSmallBuf,10,"%.2x ", pMessage[u16Co]);
                     strcat(szBuffer, szSmallBuf);
                  }
                  strcat(szBuffer, "'\r\n");
                  vPrintMemory(fdesc, szBuffer);
               }
            } 
         }
         else
         {
            OSAL_s32PrintFormat(szBuffer, "Internal error 1\r\n");
            vPrintMemory(fdesc, szBuffer);
         }
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapClose
   *----------------------------------------------------------------------
   * DESCRIPTION:  closes an existing heap
   *----------------------------------------------------------------------
   * PARAMETER:    NONE
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapClose( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      tBool bCloseSemaphore=FALSE;
      tU32 nsize;
      tU32 hnsize;

      /* --Check if pool is opened. */
       if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
       {    
         /* --Enter critical section. */
         vEnterCriticalSection();
         /* --Check if data is available. */
         if (u32Open && pSharedRead) 
         {

            /* --Decrement counter. */
            u32Open--;
            if (u32Open==0) 
            {

               // shared counter dekrementieren
               pSharedWrite->n32Counter--;
               // wenn pool vom letzten Prozess gel�scht wird, dann heap leaks tracen
               if (pSharedWrite->n32Counter == 0)
               {
                 // OSAL_tIODescriptor fd = (OSAL_tIODescriptor) OSAL_C_INVALID_HANDLE;  /*lint !e569 */
                  if (OSAL_s32HeapGetAbsoluteSizeIntern() != OSAL_s32HeapGetCurrentSizeIntern())
                  {
                     vPrintMemory(fd, "Mem Leaks after the last OSAL_s32HeapClose:");
                  }
                  else
                  {
                     vPrintMemory(fd, "Mem pool shutting down with no leaks.");
                  }
                  /* In jedem Fall aber die Print-Funktion aufrufen!
                     Die tut nix, wenn kein Leak da ist. Falls aber die
                     sizes mal verzaehlt worden sind und ausversehen
                     keine Leaks erkannt worden sind, ist es besser nochmal
                     nachzusehen:    */
                  OSAL_vPrintHeapLeaks(fd);
                  {
                     // Minimal Size ausgeben...
                     tC8 szBuffer[128] = {0};
                     tS32 s32Size = OSAL_s32HeapGetMinimalSizeInternal();
                     OSAL_s32PrintFormat( szBuffer, "After Last Close: OSAL_s32HeapGetMinimalSize() = %d", 
                        s32Size );
                     vPrintMemory(fd, szBuffer);
                  }
                  if (fd != OSAL_C_INVALID_HANDLE)  /*lint !e774 !e737 !e650 */  // OSAL_C_INVALID_HANDLE ist ein tU32, fd ein tS32...
                  {
                     OSAL_s32IOClose(fd);
                  }
               }

               /* calculate block size */
               nsize = pSharedRead->nAbsSize;
               /* Calculate block size plus header: */
               hnsize = (nsize+1)*sizeof(OSAL_MEM_tHeader) + sizeof(OSAL_MEM_tSharedHeader);

               /* unmap read memory pointer */
               if(!bLocalMem)OSAL_s32SharedMemoryUnmap(pSharedRead, hnsize );
               pSharedRead=OSAL_NULL;

               /* unmap write memory pointer */
               if(!bLocalMem)OSAL_s32SharedMemoryUnmap(pSharedWrite, hnsize );
               pSharedWrite=OSAL_NULL;
                /* close shared memory pool */
               if(!bLocalMem)OSAL_s32SharedMemoryClose(HeapSharedMem);


               bCloseSemaphore=TRUE;
               /* clsoe file descriptor */
               if (fd != OSAL_ERROR)
               {
                  OSAL_s32IOClose(fd);
                  fd = OSAL_ERROR;
               }
            }
            /* --Everything is fine. */
            s32ReturnValue=OSAL_OK;
         } 
         else 
         {
            /* --Internal error. */
            u32ErrorCode=OSAL_E_UNKNOWN;
         }
         /* --Leave critical section. */
         vLeaveCriticalSection();            

         /* --If last close call, close the semaphore. */
         if (bCloseSemaphore==TRUE) 
         {
            s32SemClose();
            HeapSharedMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
         }
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR) 
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  deletes an existing shared message memory pool
   *----------------------------------------------------------------------
   * PARAMETER:    NONE
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapDelete( tVoid )
   {
      /* delete shared memory pool */
      if(!bLocalMem)OSAL_s32SharedMemoryDelete(szHeapName);
      /* delete critical section semaphore */
      s32SemDelete();
 
      SemHandle        = (OSAL_tSemHandle)OSAL_C_INVALID_HANDLE;
      return OSAL_OK;
   }

// Tries to restore a corrupted memory entry header from the information in the Guard
// Returns FALSE if correction was not possible
// IMPORTANT: Function may be called only from a critical section 
// IMPORTANT: Shall be called only if CHECK_MSG_POOL is defined
tBool bMemAreaCorrection(OSAL_MEM_tHeader *p)
{
   OSAL_MEM_tHeader* pTmp = (OSAL_MEM_tHeader*)((uintptr_t)p + (1/*HEADER +0*Msg*/*sizeof(OSAL_MEM_tHeader))); // msg can be zero lenght
   tBool bReturn = FALSE;
   while(1)
   {
      /* check for GUARD info */
      if(pTmp->s.u16MagicField == OSAL_INV_MEM_MAGIC)
      {
         /* check with length information, that we found the message before */
         if(((uintptr_t)p + ((pTmp->s.nsize-1) * sizeof(OSAL_MEM_tHeader))) == (uintptr_t)pTmp)
         {
            /* restore message management information */
            p->s.u16MemOffset = pTmp->s.u16MemOffset;
            p->s.s32Offset    = pTmp->s.s32Offset;
            p->s.nsize        = pTmp->s.nsize;
            p->s.u16MagicField = OSAL_MEM_MAGIC;

            bReturn = TRUE;
            break;
         }
      }
      pTmp = (OSAL_MEM_tHeader*)((uintptr_t)pTmp + sizeof(OSAL_MEM_tHeader));
      /* check if we leave the valid range */
      if((uintptr_t)pTmp > (uintptr_t)pBase + u32HeapSize)
      {
          break;
      }
   }
   return bReturn;
}

// Checks if p has valid range and if entry pointed to by p has valid header/structure
// In case of invalid header, function tries to correct structures via call 
// of bMsgAreaCorrection().
// Range check is only done, if bDoRangeCheck== TRUE
// bMsgAreaCorrection() changes pool structures.
// So s32CheckPointer() shall be either called from inside a critical section with
// bUseLock==FALSE or from outside a critical section with bUseLock==TRUE
tS32 s32CheckPtr(OSAL_MEM_tHeader *p, tBool bDoRangeCheck, tBool bUseLock)
{
   tBool bRet = TRUE;

   if(bDoRangeCheck==TRUE)
   {
      if(((uintptr_t)p < (uintptr_t)pBase)
       ||((uintptr_t)p > (uintptr_t)pBase + u32HeapSize))
      {
          vPrintMemError("s32CheckPtr: Invalid memory range for message.");
          NORMAL_M_ASSERT_ALWAYS();
          OSAL_vSetErrorCode(OSAL_E_UNKNOWN);
          return OSAL_ERROR;
      }
   }

   if(*((tU16*)p)!= OSAL_MEM_MAGIC)
   {
      bRet = FALSE;
      if(u32Guard)
      {
          if(bUseLock == TRUE)vEnterCriticalSection();
          bRet = bMemAreaCorrection(p);
          if(bUseLock == TRUE)vLeaveCriticalSection();
      }

      if(bRet == FALSE)
      {
          /* memory overwritten */
          NORMAL_M_ASSERT_ALWAYS();
          /*  */
          OSAL_vSetErrorCode(OSAL_E_UNKNOWN);
          return OSAL_ERROR;
      }
   }
   return OSAL_OK;
}

void* pvAllocHeapMem(tU32 nsize,tU32 size)
{
   OSAL_MEM_tHeader *p, *prevp;
   OSAL_MEM_tHeader *pTmp;
   tU32 u32CmpSize = nsize;

   /* set start pointer */
   prevp = OFFSET_2_POINTER(pSharedWrite->freeOffset);

   if(s32CheckPtr(prevp,TRUE, FALSE) == OSAL_ERROR)
   {
       return NULL;
   }

   if(u32Guard)u32CmpSize = nsize+2; 

   // This is a loop over ONLY the free entries in the message pool. It seems to be guaranteed
   // that the addresses (i.e. offsets) of such free entries are in sequence.
   // If end of pool is reached it wraps around and continues search at the beginning of the pool again.
   // This loop does not touch at all any entries in the pool with messages.
   for(p=OFFSET_2_POINTER(prevp->s.s32Offset); ;prevp = p, p = OFFSET_2_POINTER(p->s.s32Offset))
   {
      if(s32CheckPtr(p,FALSE,FALSE) == OSAL_ERROR)return NULL;

      /* calculate so that we do not leave space too small for a new message */
      if (p->s.nsize >= u32CmpSize) /* sufficient size */
      {
         if (p->s.nsize == u32CmpSize) /* exact size */
         {
            if(u32Guard)nsize=nsize+2; // special case, more room as required for message, but who cares
            // Free entry now completely filled, entry is removed from the list of free entries
            prevp->s.s32Offset = p->s.s32Offset;
         }
         else
         {
            // This allocates space for the message at the END of the free entry and reduces
            // the remaining size of the free entry.
            p->s.nsize -= nsize;
            // This is the area where the Guard of the free entry is stored.
            // Old free entry guard info will be replace with guard of new message entry below
            // Guard of the free entry has to be recreated to the new end of the free area !!!!
            // To have space for this guard, we need more space (see comment above)
            if(u32Guard)
            {
               pTmp = (OSAL_MEM_tHeader*)(((uintptr_t)p) + ((p->s.nsize-1) * sizeof(OSAL_MEM_tHeader)));
               pTmp->s.u16MagicField = OSAL_INV_MEM_MAGIC;
               pTmp->s.u16MemOffset  = p->s.u16MemOffset;
               pTmp->s.s32Offset     = p->s.s32Offset;
               pTmp->s.nsize         = p->s.nsize;
            }
            p += p->s.nsize;
            p->s.nsize = nsize;
         }
     
         /* offset berechnen; wird verwendet zur exakten Berechnung der Memorygr��e */
         p->s.u16MemOffset = (tU16) (nsize*sizeof(OSAL_MEM_tHeader)-size);

         /* magic field setzen */
         p->s.u16MagicField=OSAL_MEM_MAGIC;
         /* set next pointer of used block to -1 */
         // This means that the occupied entry now does not point to any free entry anymore
         // It is effectively taken out of the chained sequence of free entries
         p->s.s32Offset=-1;

         if(u32Guard)
         {
            /* store message info in GUARD */
            pTmp = (OSAL_MEM_tHeader*)(((uintptr_t)p) + ((nsize-1) * sizeof(OSAL_MEM_tHeader)));
            pTmp->s.u16MagicField = OSAL_INV_MEM_MAGIC;
            pTmp->s.u16MemOffset  = p->s.u16MemOffset;
            pTmp->s.s32Offset     = p->s.s32Offset;
            pTmp->s.nsize         = p->s.nsize;
         }
         /* set new free pointer */
         pSharedWrite->freeOffset = POINTER_2_OFFSET(prevp);
         /* decrement current size */
         pSharedWrite->nCurSize -= nsize;
         /* set minimal size */
         if (pSharedWrite->nCurSize < pSharedWrite->nMinSize)
             pSharedWrite->nMinSize = pSharedWrite->nCurSize;
         /* set max message size */
         if (pSharedWrite->nMaxMemBlkSize < nsize)
             pSharedWrite->nMaxMemBlkSize = nsize;

         //OSAL_M_TRACE_HEAP('c',(tU32) POINTER_2_OFFSET(p),size,
         //pSharedWrite->nCurSize*sizeof(OSAL_MEM_tHeader));
         return (void*)((uintptr_t)p + sizeof(OSAL_MEM_tHeader));
      }
      // pool completely searched (incl. wraparound), no free entry found 
      if (p == OFFSET_2_POINTER(pSharedWrite->freeOffset))
      {
         return NULL;
      }
   }
}


void vCheckAvailableMem(tU32 size)
{
   OSAL_tIODescriptor Des = OSAL_ERROR;
   tS32 s32Size;
   char Buffer[250];
   memset((void*)Buffer,0,250);

   s32Size = OSAL_s32HeapGetCurrentSize();
   snprintf(&Buffer[0],250,
            "Requested Msg Size %d -> OSAL_s32HeapGetCurrentSize() %d\n",
            size , s32Size);

   Des = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE);
   if(Des != OSAL_ERROR)
      vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&Buffer[0],strlen(Buffer),OSAL_STRING_OUT);

   OSAL_vPrintMemList(Des);
   OSAL_s32IOClose(Des);
}


   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapMemAlloc
   *----------------------------------------------------------------------
   * DESCRIPTION:  creates a message in shared memory pool or on the local heap
   *----------------------------------------------------------------------
   * PARAMETER:    (O) pHandle: message handle
   * PARAMETER:    (I) size: size of message
   * PARAMETER:    (I) type: local or shared message type
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   void* OSAL_pvHeapMemAlloc(tU32 size)
   {
      tU32 nsize,newsize = size;
      void* pRet = NULL;

      /* check for existing heap*/
      if (OSAL_NULL == pSharedWrite)
      {
         LINE_INFO();
         OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
         return NULL;
      }

      newsize = size + u32Guard;

      /* calculate block size */
      nsize = (newsize+sizeof(OSAL_MEM_tHeader)-1)/sizeof(OSAL_MEM_tHeader)+1;
      /* Enter critical section */
      vEnterCriticalSection();
      if((pRet = pvAllocHeapMem(nsize,size)) == NULL)
      {
           LINE_INFO();
           vCheckAvailableMem(size);
           OSAL_vSetErrorCode(OSAL_E_NOSPACE);
      }
      /* Leave critical section */
      vLeaveCriticalSection();
      return pRet;
   }


   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapMemDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  deletes a message in shared memory pool or on the local heap
   *----------------------------------------------------------------------
   * PARAMETER:    (I) memory pointer
   * RETURNVALUE:  OSAL_OK or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
tS32 OSAL_s32HeapMemDelete(void* pvMem)
{
      OSAL_MEM_tHeader *bp,*p,*pTmpBP,*pTmpP;

      /* check for existing heap */
      if((OSAL_NULL == pSharedWrite)&&(pvMem != NULL))
      {
         LINE_INFO();
         OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
         return OSAL_ERROR;
      }

      /* calculate header position*/
      bp = (OSAL_MEM_tHeader*)((uintptr_t)pvMem - sizeof(OSAL_MEM_tHeader));

      if(s32CheckPtr(bp,TRUE,TRUE) == OSAL_ERROR)
      {
         /* check to see if OSAL_s32HeapMemDelete was called for this memory before */
         if(bp->s.u16MemOffset  == OSAL_DEL_MEM_MAGIC)
         {
            vPrintMemError("OSAL_s32HeapMemDelete: MEmory already deleted.");
            LINE_INFO();
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
         }
         else
         {
            vPrintMemError("OSAL_s32HeapMemDelete: s32CheckPtr failed");
            LINE_INFO();
         }
         return OSAL_ERROR;
     }


     vEnterCriticalSection();
     /* mark entry as deleted */
     bp->s.u16MemOffset = OSAL_DEL_MEM_MAGIC;

     p = OFFSET_2_POINTER(pSharedWrite->freeOffset);
     if(s32CheckPtr(p,TRUE,FALSE) == OSAL_ERROR)
     {
         FATAL_M_ASSERT_ALWAYS();
     }
    
     for ( ; !(bp > p && bp < OFFSET_2_POINTER(p->s.s32Offset)); p = OFFSET_2_POINTER(p->s.s32Offset))
     {
         if((s32CheckPtr(OFFSET_2_POINTER(p->s.s32Offset),FALSE, FALSE) == OSAL_ERROR))
         {
            FATAL_M_ASSERT_ALWAYS();
         }
         if (bp >= p && bp < p+p->s.nsize) 
         {
             OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
             NORMAL_M_ASSERT_ALWAYS();
             return OSAL_ERROR;
         }
         if (p >= OFFSET_2_POINTER(p->s.s32Offset) && (bp > p || bp < OFFSET_2_POINTER(p->s.s32Offset)))
               break; /* free block at one end */
      }
      if((s32CheckPtr(OFFSET_2_POINTER(p->s.s32Offset),FALSE, FALSE) == OSAL_ERROR))
      {
               FATAL_M_ASSERT_ALWAYS();
      }

      /* increment current size */
      pSharedWrite->nCurSize += bp->s.nsize;

      if(u32Guard)
      {
            /* check Guard */
            pTmpBP = (OSAL_MEM_tHeader*)((uintptr_t)bp + ((bp->s.nsize-1) * sizeof(OSAL_MEM_tHeader)));
            if(pTmpBP->s.u16MagicField != OSAL_INV_MEM_MAGIC)
            {
               LINE_INFO();
            }
      }
      if (bp + bp->s.nsize == OFFSET_2_POINTER(p->s.s32Offset)) /* combine with right neighbour */
      {
            if(u32Guard)
            {
               /*destroy the old GUARD information */
               pTmpBP = (OSAL_MEM_tHeader*)((uintptr_t)bp + ((bp->s.nsize-1) * sizeof(OSAL_MEM_tHeader)));
               pTmpBP->s.u16MagicField = 0;
            }
            bp->s.nsize += OFFSET_2_POINTER(p->s.s32Offset)->s.nsize;
            bp->s.s32Offset = OFFSET_2_POINTER(p->s.s32Offset)->s.s32Offset;
            bp->s.u16MemOffset = 0; 
            // magic field des rechten Nachbarn loeschen
            OFFSET_2_POINTER(p->s.s32Offset)->s.u16MagicField = 0;
            if(u32Guard)
            {
               /* update the information in the GUARD of the memory assigned to the message */
               pTmpBP = (OSAL_MEM_tHeader*)((uintptr_t)bp + ((bp->s.nsize - 1) * sizeof(OSAL_MEM_tHeader)));
               pTmpBP->s.nsize     = bp->s.nsize;
               pTmpBP->s.s32Offset = bp->s.s32Offset;
               pTmpBP->s.u16MemOffset = bp->s.u16MemOffset;
            }
      }
      else
      {
            bp->s.s32Offset = p->s.s32Offset;
            bp->s.u16MemOffset = 0; 
            if(u32Guard)
            {
               pTmpBP = (OSAL_MEM_tHeader*)((uintptr_t)bp + ((bp->s.nsize - 1) * sizeof(OSAL_MEM_tHeader)));
               pTmpBP->s.s32Offset = bp->s.s32Offset;
               pTmpBP->s.u16MemOffset = bp->s.u16MemOffset;
            }
      }
      if (p + p->s.nsize == bp) /* combine with left neighbour */
      {
            if(u32Guard)
            {
              /*destroy the old GUARD information */
              pTmpP = (OSAL_MEM_tHeader*)((uintptr_t)p + ((p->s.nsize-1) * sizeof(OSAL_MEM_tHeader)));
              pTmpP->s.u16MagicField = 0;
            }
            p->s.nsize += bp->s.nsize;
            p->s.s32Offset = bp->s.s32Offset;
            // Remark: Not required to set bp->s.u16MemOffset in this case                
            // magic field loeschen
            bp->s.u16MagicField=0;    
            if(u32Guard)
            {
              /* update the information in the GUARD of the memory asigned to the message */
              pTmpP = (OSAL_MEM_tHeader*)((uintptr_t)p + ((p->s.nsize - 1) * sizeof(OSAL_MEM_tHeader)));
              pTmpP->s.nsize     = p->s.nsize;
              pTmpP->s.s32Offset = p->s.s32Offset;
            }
      }
      else
      {
            p->s.s32Offset = POINTER_2_OFFSET(bp);
            if(u32Guard)
            {
               pTmpP = (OSAL_MEM_tHeader*)((uintptr_t)p + ((p->s.nsize - 1) * sizeof(OSAL_MEM_tHeader)));
               pTmpP->s.s32Offset = p->s.s32Offset;
            }
      }
      OSAL_M_TRACE_HEAP('d',POINTER_2_OFFSET(pvMem),0,pSharedWrite->nCurSize*sizeof(OSAL_MEM_tHeader));
      /* set new free pointer */
      pSharedWrite->freeOffset = POINTER_2_OFFSET(p);

      /* Leave critical section */
      vLeaveCriticalSection();
      return OSAL_OK;
}


   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapGetAbsoluteSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the absolute size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapGetAbsoluteSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSection();
         
         s32ReturnValue = OSAL_s32HeapGetAbsoluteSizeIntern();
         if (s32ReturnValue == OSAL_ERROR)
         {
            u32ErrorCode=OSAL_E_UNKNOWN;
         }

         /* --Leave critical section. */
         vLeaveCriticalSection();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapGetCurrentSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the current size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapGetCurrentSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSection();
   
         s32ReturnValue = OSAL_s32HeapGetCurrentSizeIntern();

         if (s32ReturnValue == OSAL_ERROR)
         {
            u32ErrorCode=OSAL_E_UNKNOWN;
         }

         /* --Leave critical section. */
         vLeaveCriticalSection();
      }
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_s32HeapGetMinimalSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the minmal size of the message pool
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tS32 OSAL_s32HeapGetMinimalSize( tVoid )
   {
      tS32 s32ReturnValue=OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSection();
         s32ReturnValue = OSAL_s32HeapGetMinimalSizeInternal();
         /* --Leave critical section. */
         vLeaveCriticalSection();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }

      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return s32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_u32GetMaxMessageSize
   *----------------------------------------------------------------------
   * DESCRIPTION:  returns the maximal message size
   *----------------------------------------------------------------------
   * PARAMETER:    none
   * RETURNVALUE:  size or OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tU32 OSAL_u32GetMaxBlockSize( tVoid )
   {
      tU32 u32ReturnValue=(tU32) OSAL_ERROR;
      tU32 u32ErrorCode=OSAL_E_NOERROR;
      /* --Check if handle is available. */
      if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) {
         /* --Enter critical section. */
         vEnterCriticalSection();
         if (pSharedRead)
            u32ReturnValue=pSharedRead->nMaxMemBlkSize * sizeof(OSAL_MEM_tHeader);
         else
            u32ErrorCode=OSAL_E_UNKNOWN;
         /* --Leave critical section. */
         vLeaveCriticalSection();
      } 
      else
      {
         /* --Does not exist. */
         u32ErrorCode=OSAL_E_DOESNOTEXIST;
      }
      /* --Set the error code. */
      if (u32ErrorCode!=OSAL_E_NOERROR)
      {
         OSAL_vSetErrorCode(u32ErrorCode);
      }
      /* --Bye-bye. */
      return u32ReturnValue;
   }

   /************************************************************************
   *
   * FUNCTION:     OSAL_PrintMessageList
   *----------------------------------------------------------------------
   * DESCRIPTION:  undocumented function: prints message list
   *----------------------------------------------------------------------
   * PARAMETER:    (I) fd: Osal file descriptor
   * RETURNVALUE:  void
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   tVoid OSAL_vPrintMemList(OSAL_tIODescriptor fdesc)
   {
      tChar szBuffer[1024];
      OSAL_MEM_tHeader *p;
      tU32 nsize;
      tU32 u32AbsSize=0,u32CurSize=0;
      /* --Check if semaphore handle is available. */
      if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
      {
         /* --Enter critical section. */
         vEnterCriticalSection();

         if (pSharedRead) 
         {
            nsize = pSharedRead->nAbsSize;
            u32AbsSize=pSharedRead->nAbsSize * sizeof(OSAL_MEM_tHeader);
            u32CurSize=pSharedRead->nCurSize * sizeof(OSAL_MEM_tHeader);
            OSAL_s32PrintFormat( szBuffer, "Blocklist: Absolute size: %d, Current size: %d\r\n",(int)u32AbsSize,(int)u32CurSize);              
            vPrintMemory(fdesc, szBuffer);


            for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
            {
               if(s32CheckPtr(p,TRUE,FALSE) == OSAL_ERROR)
               {

#ifdef NO_PRIxPTR
                  TraceString("!!! ERROR: Real 0x%x, Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p),  p->s.nsize);
#else
                  TraceString("!!! ERROR: Real 0x%" PRIxPTR ", Offset %4d, Size %d\r\n", p, POINTER_2_OFFSET(p),  p->s.nsize);
#endif
                  vLeaveCriticalSection();
                  return;
               }


               if(p->s.s32Offset != -1)
#ifdef NO_PRIxPTR
                  OSAL_s32PrintFormat( szBuffer, "Free block: Real 0x%, Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p,(tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p), 
                  (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) - p->s.u16MemOffset, p->s.nsize);
#else
                  OSAL_s32PrintFormat( szBuffer, "Free block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p,(tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p), 
                  (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) - p->s.u16MemOffset, p->s.nsize);
#endif
               else
               {
                  if (p->s.u16MemOffset  == OSAL_DEL_MEM_MAGIC)
                  {
#ifdef NO_PRIxPTR
                     OSAL_s32PrintFormat( szBuffer, "Deleted marked block: Real 0x%x, Offset %4d (0x%x), Size (incl. u16MsgOffset) %lu, Blocksize %d \r\n", 
                                         (uintptr_t) p, (tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p),
                                         (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) /* we cannot subtract p->s.u16MsgOffset here */ ,  p->s.nsize);
#else
                     OSAL_s32PrintFormat( szBuffer, "Deleted marked block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size (incl. u16MsgOffset) %lu, Blocksize %d \r\n", 
                                         (uintptr_t) p, (tU32)POINTER_2_OFFSET(p),(tU32)POINTER_2_OFFSET(p),
                                         (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) /* we cannot subtract p->s.u16MsgOffset here */ ,  p->s.nsize);
#endif
                  }
                  else
                  {
#ifdef NO_PRIxPTR
                  OSAL_s32PrintFormat( szBuffer, "Used block: Real 0x%x, Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p, (tU32)POINTER_2_OFFSET(p), (tU32)POINTER_2_OFFSET(p),
                  (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) - p->s.u16MemOffset, (int) p->s.nsize);
#else
                  OSAL_s32PrintFormat( szBuffer, "Used block: Real 0x%" PRIxPTR ", Offset %4d (0x%x), Size %lu, Blocksize %d \r\n", 
                  (uintptr_t) p, (tU32)POINTER_2_OFFSET(p), (tU32)POINTER_2_OFFSET(p),
                  (tU32) p->s.nsize*sizeof(OSAL_MEM_tHeader) - p->s.u16MemOffset, (int) p->s.nsize);
#endif
                  }
               }  

                TraceString(szBuffer);
            }

            // print memory leaks
            OSAL_vPrintHeapLeaks(fdesc);

         }
         /* --Leave critical section. */
         vLeaveCriticalSection();
      }
   }

   tVoid vGetMemSpace(tPU32 pu32MaxFree, tPU32 pu32MaxUsed) 
   {
      OSAL_MEM_tHeader *p;    
      tU32 nsize = pSharedRead->nAbsSize;
      *pu32MaxFree=*pu32MaxUsed=0;

      for (p = OFFSET_2_POINTER(OFFSET_2_POINTER(0)->s.s32Offset); p < OFFSET_2_POINTER(nsize+1); p += p->s.nsize)
         // We also count entries marked to be deleted by s32MemoryDelete as free
         if(p->s.s32Offset != -1)
            *pu32MaxFree=(p->s.nsize>*pu32MaxFree ? p->s.nsize : *pu32MaxFree);
         else 
            *pu32MaxUsed=(p->s.nsize>*pu32MaxUsed ? p->s.nsize : *pu32MaxUsed);

      *pu32MaxFree*=sizeof(OSAL_MEM_tHeader);
      *pu32MaxUsed*=sizeof(OSAL_MEM_tHeader);
   }

   tU32 OSAL_u32GetMemSize(void* pvMem)
   {
      OSAL_MEM_tHeader *bp = (OSAL_MEM_tHeader*)((uintptr_t)pvMem - sizeof(OSAL_MEM_tHeader));

     if (OSAL_NULL == pSharedRead)
     {
         OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);
         return 0;
     }

     /* --memory pool check. */
  //   OSAL_M_RETURN_ON_FAILEDHEAP(OSAL_NULL);

     if (bp->s.u16MagicField == OSAL_MEM_MAGIC)
     {
         return (tU32) bp->s.nsize*sizeof(OSAL_MEM_tHeader) - u32Guard;
     }
     else if(bp->s.u16MemOffset == OSAL_DEL_MEM_MAGIC)
     {
         vPrintMemError("OSAL_u32GetMemSize: deleted memory pointer (MAGIC-FIELD).");
          return 0;
     }
     else
     {
         vPrintMemError("OSAL_u32GetMemSize: Invalid memory pointer (MAGIC-FIELD).");
         OSAL_vSetErrorCode(OSAL_E_INVALIDVALUE);
          return 0;
     }
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemCreate
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
  static tS32 s32SemCreate(tString szName)
   {      return OSAL_s32SemaphoreCreate( szName,&SemHandle,1);   }

   /************************************************************************
   *
   * FUNCTION:     s32SemOpen
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemOpen(tString szName)
   {      return OSAL_s32SemaphoreOpen( szName,&SemHandle);   }
   
  /************************************************************************
   *
   * FUNCTION:     s32SemClose
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemClose(void)
   {      return OSAL_s32SemaphoreClose(SemHandle);   }

   /************************************************************************
   *
   * FUNCTION:     s32SemDelete
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemDelete(void)
   {      
       tS32 s3Ret = OSAL_s32SemaphoreDelete(szHeapName); 
       if(s3Ret == OSAL_ERROR)
       {
           NORMAL_M_ASSERT_ALWAYS();
       }
       return s3Ret;
   }

   /************************************************************************
   *
   * FUNCTION:     s32SemOp
   *----------------------------------------------------------------------
   * DESCRIPTION:  
   *----------------------------------------------------------------------
   * RETURNVALUE:  OSAL_ERROR / OSAL OK
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.03.13  | Initial revision                             | MRK2HI
   *************************************************************************/
   static tS32 s32SemOp(tS32 s32Operation)
   {
      tS32  s32RetVal = OSAL_ERROR;
      if ( s32Operation == -1 )
      {
         s32RetVal = OSAL_s32SemaphoreWait(SemHandle,OSAL_C_TIMEOUT_FOREVER );
      }
      else
      {
         s32RetVal = OSAL_s32SemaphorePost(SemHandle);
      }
      return s32RetVal;
   }


   /************************************************************************
   *
   * FUNCTION:     s32HeapInstantiate
   *----------------------------------------------------------------------
   * DESCRIPTION: Open the existing Message Pool  
   * 
   * PRECONDITION: This function has to be called after entering the
   * critical section. This function is not thread-safe. 
   *
   *----------------------------------------------------------------------
   * PARAMETER:    
   * RETURNVALUE:  OSAL_OK/OSAL_ERROR
   * HISTORY:
   * Date      |   Modification                         | Author
   * 06.02.02  |   Initial revision                     | Uelschen, ESN2
   *************************************************************************/
   tS32 s32HeapInstantiate(tU32 u32Size) 
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      tU32 u32SharedSize = u32Size;

    /* --Check shared memory handle. */
    if (HeapSharedMem != (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE) 
    {
         u32SharedSize = u32Size;
         if (u32SharedSize && !bLocalMem)
         {
            /* get read pointer to shared memory */
            pSharedRead = (OSAL_MEM_tSharedHeader*) OSAL_pvSharedMemoryMap(HeapSharedMem,   /* Handle */
                                                                           OSAL_EN_READONLY,              /* Rights */
                                                                           u32SharedSize,                 /* Size + Header */
                                                                           0);                            /* Offset */

            /* get write pointer to shared memory */
            pSharedWrite = (OSAL_MEM_tSharedHeader*) OSAL_pvSharedMemoryMap(HeapSharedMem,   /* Handle */
                                                                            OSAL_EN_READWRITE,             /* Rights */
                                                                            u32SharedSize,                 /* Size + Header */
                                                                            0);                            /* Offset */
            /* --check if mapping failed. */
            if (pSharedRead && pSharedWrite) 
            {
               if(u32Guard)memset(pSharedWrite,0,u32SharedSize);

               /* set handle counter */
               u32Open = 1;
               /* set base pointer */
               if(!pBase)pBase = (OSAL_MEM_tHeader*) ((uintptr_t)pSharedWrite+sizeof(OSAL_MEM_tSharedHeader));
               /* --ok! */
               s32ReturnValue=OSAL_OK;
            }
         } 
         else 
         {
            pSharedRead = 0;
            pSharedWrite = 0;
            FATAL_M_ASSERT_ALWAYS();
         }
    }

      /* --If something went wrong -> give back the opend shared memory pointers. */
      if (s32ReturnValue==OSAL_ERROR) 
      {
         /* --Pointer to read section. */
         if (pSharedRead)
            OSAL_s32SharedMemoryUnmap(pSharedRead,u32SharedSize);
         /* --Pointer to write section. */
         if (pSharedWrite)
            OSAL_s32SharedMemoryUnmap(pSharedWrite,u32SharedSize);
      }
      return s32ReturnValue;
   }

   static tVoid vEnterCriticalSection() 
   {      s32SemOp(-1);   }

   static tVoid vLeaveCriticalSection() 
   {      s32SemOp(+1);   }


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalheap.c
|-----------------------------------------------------------------------*/
