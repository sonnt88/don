/*****************************************************************************
| FILE:         osalshmem.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Shared Memory-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef OSAL_SHM_MQ
#define  LINUX_C_SHMEM_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH + 2)
#else
#define  LINUX_C_SHMEM_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH)
#endif

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
typedef struct{
     uintptr_t MemIdx;
     tU32 u32PrcIdx;
     tU32 u32PrcCnt;
}trResToPidMap;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static trResToPidMap* prLiShMemMap = NULL;
static tS32 s32Pid = 0;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

tS32 s32SharedMemoryTableCreate(tVoid);

tS32 s32SharedMemoryTableDeleteEntries(void);

static trSharedMemoryElement* tSharedMemoryTableGetFreeEntry(tVoid);

static trSharedMemoryElement* tSharedMemoryTableSearchEntryByName(tCString coszName);

static trSharedMemoryElement* tSharedMemoryTableSearchEntryByHandle(OSAL_tShMemHandle hShared);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    s32SharedMemoryTableCreate
 *
 * DESCRIPTION: This function creates the SharedMemory Table List. If
 *              there isn't space it returns a error code.
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32SharedMemoryTableCreate(tVoid)
{
   tU32 tiIndex;

   pOsalData->SharedMemoryTable.u32UsedEntries      = 0;
   pOsalData->SharedMemoryTable.u32MaxEntries       = pOsalData->u32MaxNrSharedMemElements;

   for(tiIndex=0; tiIndex <  pOsalData->SharedMemoryTable.u32MaxEntries ; tiIndex++ )
   {
      pShMemEl[tiIndex].bIsUsed = FALSE;
      pShMemEl[tiIndex].hShared = (OSAL_tShMemHandle)tiIndex;
   }
   s32Pid = getpid();
   return OSAL_OK;
}


/*****************************************************************************
 *
 * FUNCTION:    tSharedMemoryTableGetFreeEntry
 *
 * DESCRIPTION: this function goes through the SharedMemory List and returns the
 *              first unused SharedMemoryElement.
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trSharedMemoryElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trSharedMemoryElement* tSharedMemoryTableGetFreeEntry(tVoid)
{
   trSharedMemoryElement *pCurrent  = pShMemEl;
   tU32 used = pOsalData->SharedMemoryTable.u32UsedEntries;

   if (used < pOsalData->SharedMemoryTable.u32MaxEntries)
   {
      pCurrent = &pShMemEl[used];
      pOsalData->SharedMemoryTable.u32UsedEntries++;
   } else {
      /* search an entry with !bIsUsed, used == MaxEntries */
      while ( pCurrent <= &pShMemEl[used]
         && ( pCurrent->bIsUsed == TRUE) )
      {
         pCurrent++;
      }
      if(pCurrent >= &pShMemEl[used])
      {
         pCurrent = NULL; /* not found */
      }
   }
   return pCurrent;
}


/*****************************************************************************
 *
 * FUNCTION:    tSharedMemoryTableSearchEntryByHandle
 *
 * DESCRIPTION: this function goes through the SharedMemory List and returns the
 *              SharedMemoryElement with the given ID or NULL if all the List has
 *              been checked without success.
 *
 * PARAMETER:   OSAL_tSharedMemoryID tid
 *                 thread ID wanted.
 *
 * RETURNVALUE: trSharedMemoryElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trSharedMemoryElement *tSharedMemoryTableSearchEntryByHandle(OSAL_tShMemHandle hShared)
{
   trSharedMemoryElement *pCurrent = pShMemEl;
   pCurrent = pCurrent + ((uintptr_t)hShared - pOsalData->u32MaxNrSharedMemElements);

   if(pCurrent->bIsUsed == FALSE )pCurrent = NULL;
   return pCurrent;
}

/*****************************************************************************
 *
 * FUNCTION:    tSharedMemoryTableSearchEntryByName
 *
 * DESCRIPTION: this function goes through the SharedMemory List and returns the
 *              SharedMemoryElement with the given name or NULL if all the List has
 *              been checked without success.
 *
 * PARAMETER:   coszName (I)
 *                 SharedMemory name wanted.
 *
 * RETURNVALUE: trSharedMemoryElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trSharedMemoryElement *tSharedMemoryTableSearchEntryByName(tCString coszName)
{
   trSharedMemoryElement *pCurrent = pShMemEl;
   tU32 u32Ret = 0;
   for(u32Ret=0;u32Ret<pOsalData->u32MaxNrSharedMemElements;u32Ret++)
   {
      if((pCurrent->bIsUsed == FALSE )
       ||(OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0) )
      {
         pCurrent++;
      }
      else
      {
         break;
      }
   }
   if(u32Ret == pOsalData->u32MaxNrSharedMemElements)
   {
      return NULL;
   }
   else
   {
      return pCurrent;
   }
}

/*****************************************************************************
*
* FUNCTION:    bSharedMemoryTableDeleteEntryByName
*
* DESCRIPTION: this function goes through the Shared Memory List deletes the
*              Shared MemoryElement with the given name 
*
* PARAMETER:   coszName (I)
*                 shared memory segment name wanted.
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
*****************************************************************************
static tBool bSharedMemoryTableDeleteEntryByName(tCString coszName)
{
   trSharedMemoryElement *pCurrent = SharedMemoryTable.ptrHeader;

   while( (pCurrent!= OSAL_NULL)
       && (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0) )
   {
      pCurrent = pCurrent->pNext;
   }
   if(pCurrent!= OSAL_NULL)
   {
      if(pCurrent->bIsUsed == FALSE )
    {
           pCurrent->u16OpenCounter = 0;
           pCurrent->u16ShmMaxValue = 0;
           pCurrent->u16Type = 0;
           pCurrent->bIsUsed = 0;
           pCurrent->bIsMapped = 0;
           pCurrent->bToDelete = 0;
           pCurrent->pvSharedMemoryStartPointer = NULL;
           pCurrent->u32SharedMemorySize = 0;
           pCurrent->hShared = 0;
           memset(pCurrent->szName,0,LINUX_C_SHMEM_MAX_NAMELENGHT);
           return TRUE;
    }
}

   return FALSE;
}*/

/*****************************************************************************
*
* FUNCTION:    bCleanUpShMemofContext
*
* DESCRIPTION: this function goes through the Thread List deletes the
*              Shared Memory and Shared Memory Element with the given context from list
*
* PARAMETER:   tU16 Context ID
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vCleanUpShMemofContext(void)
{
   if(prLiShMemMap)
   {
      trSharedMemoryElement *pCurrent = &pShMemEl[0];
      tU32 used = pOsalData->SharedMemoryTable.u32UsedEntries;
      tS32 s32OwnPid = OSAL_ProcessWhoAmI();

      while (pCurrent < &pShMemEl[used])
      {
         if((pCurrent->bIsUsed == TRUE)
          &&((uintptr_t)pCurrent->hShared < pOsalData->u32MaxNrSharedMemElements)
          &&(s32OwnPid == (tS32)prLiShMemMap[pCurrent->hShared].u32PrcIdx))
         {
            while(prLiShMemMap[pCurrent->hShared].u32PrcCnt > 0)
            {
               TraceString("OSAL_s32SharedMemoryClose for %s",pCurrent->szName);
               if(OSAL_s32SharedMemoryClose((OSAL_tShMemHandle)pCurrent->u32SharedMemoryID) != OSAL_OK)
               {
                  TraceString("OSAL_s32SharedMemoryClose for 0x%x failed",pCurrent);
                  break;
               }
            }
            if(pCurrent->u16OpenCounter == 0)
            {
               if(OSAL_s32SharedMemoryDelete(pCurrent->szName) != OSAL_OK)
               {
                  TraceString("OSAL_s32SharedMemoryDelete for %s failed",pCurrent->szName);
               }
            }
         }
         pCurrent++;
      }
   }
}


/*****************************************************************************
 *
 * FUNCTION:    tSharedMemoryTableSearchEntryByPointer
 *
 * DESCRIPTION: this function goes through the SharedMemory List and returns the
 *              SharedMemoryElement with the given start pointer or NULL if all
 *              the List has been checked without success.
 *
 * PARAMETER:
 *  tPVoid      pvSharedMemory    pointer to Shared Memory wanted.
 *
 * RETURNVALUE: trSharedMemoryElement*
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trSharedMemoryElement *
tSharedMemoryTableSearchEntryByPointer(const void* pvSharedMemory)
{
   trSharedMemoryElement *pCurrent = pShMemEl;
   tU32 u32Count;
   for(u32Count = 0;u32Count<pOsalData->u32MaxNrSharedMemElements;u32Count++)
   {
      if(prLiShMemMap[u32Count].MemIdx == (uintptr_t)pvSharedMemory)break;
   }
   
   if(u32Count < pOsalData->u32MaxNrSharedMemElements)
   {
      pCurrent = pCurrent+ u32Count;
      if(pCurrent->bIsUsed ==FALSE)
      pCurrent = NULL;
   }
   else
   {
      pCurrent = NULL;
   }
   return pCurrent;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32SharedMemoryTableDeleteEntries
*
* DESCRIPTION: This function deletes all elements from OSAL table,
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
*****************************************************************************/
tS32 s32SharedMemoryTableDeleteEntries(void)
{
//   trSharedMemoryElement *pCurrentEntry = pShMemEl;
   tS32 s32ReturnValue = OSAL_OK;
/*   tS32 s32DeleteCounter = 0;
   tU32 used = pOsalData->SharedMemoryTable.u32UsedEntries;
   if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
   {
      * search the whole table *
      while (pCurrentEntry < &pShMemEl[used])
      {
         if(pCurrentEntry->bIsUsed == TRUE)
         {
            s32DeleteCounter++;
            //free shared memory
            free(pCurrentEntry->pvSharedMemoryStartPointer);
            pCurrentEntry->bIsUsed = FALSE;
            pCurrentEntry->u16Type = 0;
            s32ReturnValue = s32DeleteCounter;
         }
         else
            pCurrentEntry++;
      }
      UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
   }*/
   return(s32ReturnValue);
}

void vTraceShMemInfo(trSharedMemoryElement* pCurrentEntry,tU8 Type,tBool bErrMem,tU32 u32Error,tCString coszName)
{
#ifdef SHORT_TRACE_OUTPUT
    char au8Buf[32 + LINUX_C_SHMEM_MAX_NAMELENGHT];
    tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
    OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_SHMEM_INFO);
    OSAL_M_INSERT_T8(&au8Buf[1],(tU8)Type);
    bGetThreadNameForTID(&au8Buf[2],8,(tS32)u32Val);
    OSAL_M_INSERT_T32(&au8Buf[10],u32Val);
    if(pCurrentEntry != NULL)
    {
       OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry->hShared);
       OSAL_M_INSERT_T32(&au8Buf[18],pCurrentEntry->u32SharedMemorySize);
       OSAL_M_INSERT_T32(&au8Buf[22],(tU32)pCurrentEntry->pvSharedMemoryStartPointer);
    }
    else
    {
        u32Val = 0;
        OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
        OSAL_M_INSERT_T32(&au8Buf[18],u32Val);
        OSAL_M_INSERT_T32(&au8Buf[22],u32Val);
    }
    OSAL_M_INSERT_T32(&au8Buf[26],u32Error);
    u32Val = 0;
    if(coszName)
    {
      u32Val = strlen(coszName);
      if( u32Val > LINUX_C_SHMEM_MAX_NAMELENGHT )
      {
         u32Val = LINUX_C_SHMEM_MAX_NAMELENGHT;
      }
      memcpy(&au8Buf[30],coszName,u32Val);
      au8Buf[24 + LINUX_C_SHMEM_MAX_NAMELENGHT -1] = 0;
    }
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_SM, TR_LEVEL_FATAL,au8Buf,u32Val+30);
    if((Type != MAP_OPERATION)&&(Type != UNMAP_OPERATION)&&(bErrMem))
    {
       vWriteToErrMem((TR_tenTraceClass)OSAL_C_TR_CLASS_SYS_SM,&au8Buf[1],u32Val+30,OSAL_TSK_SHMEM_INFO);
    }
#else
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   au8Buf[0] = OSAL_STRING_OUT;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
   tCString Operation;
   switch(Type)
   {
     case CREATE_OPERATION:
         Operation = "CREATE_OPERATION";
         break;
     case DELETE_OPERATION:
         Operation = "DELETE_OPERATION";
         break;
     case OPEN_OPERATION:
         Operation = "OPEN_OPERATION";
         break;
     case CLOSE_OPERATION:
         Operation = "CLOSE_OPERATION";
         break;
     case MAP_OPERATION:
         Operation = "MAP_OPERATION";
         break;
     case UNMAP_OPERATION:
         Operation = "UNMAP_OPERATION";
         break;
     default:
         Operation = "Unknown";
         break;
   }
   if(coszName == NULL)coszName ="Unknown";
   if(pCurrentEntry)
   {
#ifdef NO_PRIxPTR
       snprintf(&au8Buf[1],250,"Shared Mem %s of Task:%s (ID %d) Handle:0x%x Size:%d Start:0x%x Error:%d Name:%s",
                Operation,name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry->hShared,(unsigned int)pCurrentEntry->u32SharedMemorySize,(unsigned int)pCurrentEntry->pvSharedMemoryStartPointer,(unsigned int)u32Error,coszName);
#else
       snprintf(&au8Buf[1],250,"Shared Mem %s of Task:%s (ID %d) Handle:0x%" PRIxPTR " Size:%d Start:0x%" PRIxPTR " Error:%d Name:%s",
                Operation,name,(unsigned int)u32Temp,(uintptr_t)pCurrentEntry->hShared,(unsigned int)pCurrentEntry->u32SharedMemorySize,(uintptr_t)pCurrentEntry->pvSharedMemoryStartPointer,(unsigned int)u32Error,coszName);
#endif
   }
   else
   {
       snprintf(&au8Buf[1],250,"Shared Mem %s of Task:%s (ID %d) Handle:0x%x Size:%d Start:0x%x Error:%d Name:%s",
                Operation,name,(unsigned int)u32Temp,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_SM,TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
   if((Type != MAP_OPERATION)&&(Type != UNMAP_OPERATION)&&(bErrMem))
   {
      vWriteToErrMem((TR_tenTraceClass)OSAL_C_TR_CLASS_SYS_SM,&au8Buf[1],strlen(&au8Buf[1])+1,OSAL_TSK_SHMEM_INFO);
   }
#endif
}


void* vpMmapShmToPrc(tString cName,tU32 u32Size,tBool bCreate)
{
  int Pool;
  void* pMem = NULL;
  /* memory has to be mapped for current window */
  while(1)
  {
     if(bCreate)
     {
       Pool = shm_open(cName, O_RDWR|O_EXCL|O_CREAT,OSAL_ACCESS_RIGTHS);
       if(Pool == -1)
       {
          if(errno != EINTR)
          {
              break;
          }
          //try again
       }
       else
       {
           if (ftruncate(Pool,u32Size) == -1)
           {
              FATAL_M_ASSERT_ALWAYS();
           }
           if(s32OsalGroupId)
           {
              if(fchown(Pool,(uid_t)-1,s32OsalGroupId) == -1)
              {
                vWritePrintfErrmem("vpMmapShmToPrc -> fchown error %d \n",errno);
              }
              if(fchmod(Pool, OSAL_ACCESS_RIGTHS) == -1)
              {
                 vWritePrintfErrmem("vpMmapShmToPrc -> fchmod error %d \n",errno);
              }
           }
           break;
        }
     }
     else
     {
        Pool = shm_open(cName, O_RDWR,OSAL_ACCESS_RIGTHS);
        if(Pool == -1)
        {
           if(errno != EINTR)
           {
              break;
           }
           //try again
        }
        else
        {
           break;
        }
     }
  }
  if(Pool != -1)    
  {
     pMem = mmap(NULL,u32Size,PROT_READ | PROT_WRITE,GLOBAL_DATA_OPTION,Pool,0);
     if(pMem == MAP_FAILED)
     {
          /* return value is proved for NULL */
          pMem = NULL;
          TraceString("vpMmapShmToPrc mmap failed errno:%d",errno);
          vWritePrintfErrmem("vpMmapShmToPrc mmap failed errno:%d \n",errno);
          NORMAL_M_ASSERT_ALWAYS();
     }
     close(Pool);
  }
  else
  {
      TraceString("vpMmapShmToPrc shm_open failed errno:%d",errno);
      vWritePrintfErrmem("vpMmapShmToPrc shm_open failed errno:%d \n",errno);
      NORMAL_M_ASSERT_ALWAYS();
  }
  return pMem;
}

void vDelShMem(void* Adress,tString Name,tU32 u32Size)
{
   if(munmap(Adress,u32Size) != 0)
   {
    //   TraceString("vDelShMem munmap 0x%x size:%d failed errno:%d\n",(unsigned int)Adress,u32Size,errno);
    //   vWritePrintfErrmem("vDelShMem munmap 0x%x size:%d failed errno:%d\n",(unsigned int)Adress,u32Size,errno);  
       NORMAL_M_ASSERT_ALWAYS();
   }
   if(shm_unlink(Name) != 0)
   {
       TraceString("vDelShMem shm_unlink failed errno:%d ",errno);
       vWritePrintfErrmem("vDelShMem shm_unlink failed errno:%d \n",errno);
       NORMAL_M_ASSERT_ALWAYS();
   }
}

char *strnstr(const char *haystack, const char *needle, size_t len)
{
        int i;
        size_t needle_len;

        /* segfault here if needle is not NULL terminated */
        if (0 == (needle_len = strlen(needle)))
                return (char *)haystack; /*lint !e1773 */

        for (i=0; i<=(int)(len-needle_len); i++)
        {
                if ((haystack[0] == needle[0]) &&
                        (0 == strncmp(haystack, needle, needle_len)))
                        return (char *)haystack;/*lint !e1773 */

                haystack++;
        }
        return NULL;
}

tU32 u32UnmapDeletedShMem(void)
{
    tU32 u32Ret = 0;
    char *buffer = NULL;
    char *tmp;
    struct stat rStat;
    tU32 u32Offset = 0;
    tU32 u32Len = 0;
	
    char szCommand[128];
    snprintf(szCommand,128,"cat /proc/%d/maps > /tmp/map.txt",getpid());
    system(szCommand);

   // int fd = open("/proc/self/maps", O_RDONLY);
    int fd = open("/tmp/map.txt", O_RDONLY);
    if(fd >= 0)
    {
       fstat(fd,&rStat);
       buffer = (char*)malloc(rStat.st_size);
       if(buffer)
       {
         if((u32Len=read(fd,buffer,rStat.st_size)) > 0)
         {
           while(1)
           {
              u32Len = 0;
              tmp = strstr((char*)((uintptr_t)buffer+u32Offset), "(deleted)");               
              if(tmp == NULL)
              {
                 if(u32Ret == 0)
                 {
                    if(u32OsalSTrace & 0x00000010)
                    {
                       TraceString("Cannot find deleted shared memory!");
                    }
                 }
                 break; 
              }
              u32Offset = (tU32)((uintptr_t)tmp - (uintptr_t)buffer + strlen("(deleted)"));
              do {
                tmp--;
                u32Len++;
              }while(*tmp != '\n');
              if(strnstr(tmp+1,SHM_BIGCCA_PATH,u32Len))
              {
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
    unsigned long long ulVal1 = 0;
    unsigned long long ulVal2 = 0;
//7ffff7fdf000-7ffff7fe0000 rw-s 00000000 00:14 28161                      /run/shm/sem.OL4hGS (deleted)
                 ulVal1 = strtoull((char*)((uintptr_t)tmp+1),NULL,16); 
                 if ((errno == ERANGE && (ulVal1 == (tU32)ULLONG_MAX || ulVal1 == (tU32)LONG_MIN))
                  || (errno != 0 && ulVal1 == 0)) 
                 {  
                    TraceString("strtoul failed errno:%d",errno);
                 }
                 ulVal2 = strtoull((char*)((uintptr_t)tmp+1+13),NULL,16); 
                 if ((errno == ERANGE && (ulVal2 == (tU32)LONG_MAX || ulVal2 == (tU32)LONG_MIN))
                  || (errno != 0 && ulVal2 == 0)) 
                 {  
                    TraceString("strtoul failed errno:%d",errno);
                 }
#else
    unsigned long ulVal1 = 0;
    unsigned long ulVal2 = 0;
          //      TraceString("Valid %s",tmp+1);
// 6d9ea000-6da02000 rw-s 00000000 00:0e 9979       /dev/shm/CCAMSG_1 (deleted)
                 ulVal1 = strtoul(tmp+1,NULL,16);
                 if ((errno == ERANGE && (ulVal1 == (tU32)LONG_MAX || ulVal1 == (tU32)LONG_MIN))
                  || (errno != 0 && ulVal1 == 0)) 
                 {  
                    TraceString("strtoul failed errno:%d",errno);
                 }
                 ulVal2 = strtoul(tmp+1+9,NULL,16);
                 if ((errno == ERANGE && (ulVal2 == (tU32)LONG_MAX || ulVal2 == (tU32)LONG_MIN))
                  || (errno != 0 && ulVal2 == 0)) 
                 {  
                    TraceString("strtoul failed errno:%d",errno);
                 }
#endif
                 if((ulVal1)&&(ulVal2))
                 {
                    if(u32OsalSTrace & 0x00000010)
                    {
#ifdef NO_PRIxPTR        
                       TraceString("unmap 0x%x - 0x%x",ulVal1,ulVal2);
#else
                       TraceString("unmap 0x%"PRIxPTR" - 0x%"PRIxPTR" ",ulVal1,ulVal2);
#endif
                    }
                    if(munmap((void*)ulVal1,ulVal2-ulVal1) == -1)
                    {
#ifdef NO_PRIxPTR        
                        TraceString("unmap 0x%x - 0x%x failed errno %d",ulVal1,ulVal2,errno);
#else
                        TraceString("unmap 0x%"PRIxPTR" - 0x%"PRIxPTR" failed errno %d",ulVal1,ulVal2,errno);
#endif
                    }
                    else
                    {
                       u32Ret++;
                    }
                 }
              }
              else
              {
                // TraceString("InValid %s",tmp+1);
              }
           }//while(1)   
        }
        else
        {
           TraceString("read %d bytes failed errno:%d",rStat.st_size,errno);
        }
      }
      else
      {
         TraceString("malloc %d bytes failed errno:%d",rStat.st_size,errno);
      }
   }
 
   if(buffer)free(buffer);
   system("rm /tmp/map.txt");
   close(fd);
   
   if(u32OsalSTrace & 0x00000010)
   {
      char name[TRACE_NAME_SIZE];
      tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
      TraceString("Shared Mem u32UnmapDeletedShMem of Task:%s (ID %d) %d Areas unmapped",name,(unsigned int)u32Temp,u32Ret);
   }
   return u32Ret;
}

 

void* vpGetValidLiPrcMemPtr(trSharedMemoryElement* pCurrentEntry,tBool bOpen)
{
   if((pCurrentEntry->hShared < 0)||(pOsalData->u32MaxNrSharedMemElements < pCurrentEntry->hShared))
   {
	   NORMAL_M_ASSERT_ALWAYS();
	   TraceString("Index in OSAL shared Memory overwritten 0x%x !!!!",pCurrentEntry->hShared); 
	   return NULL;
   }

   /* check if Pointer for current process is available */
   if(prLiShMemMap[pCurrentEntry->hShared].MemIdx)
   {
       if(bOpen)
       {
           prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt++;
       }
   }
   else
   {
      if(prLiShMemMap[pCurrentEntry->hShared].u32PrcIdx == 0)
      {
         prLiShMemMap[pCurrentEntry->hShared].MemIdx = (uintptr_t)vpMmapShmToPrc(pCurrentEntry->szName,
                                                                                 pCurrentEntry->u32SharedMemorySize,
                                                                                 FALSE);
         /* memory has to be mapped for current window */
         if(prLiShMemMap[pCurrentEntry->hShared].MemIdx == 0)
         {
            vWritePrintfErrmem("vpGetValidLiPrcMemPtr mmap %d bytes failed errno:%d  ",pCurrentEntry->u32SharedMemorySize,errno);
            NORMAL_M_ASSERT_ALWAYS();
         }
         prLiShMemMap[pCurrentEntry->hShared].u32PrcIdx  = s32Pid;
         prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt  = 1;
      }
      if(!bOpen)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
   }
   return (void*)prLiShMemMap[pCurrentEntry->hShared].MemIdx;
}

tBool bDelLiPrcMemPtr(trSharedMemoryElement* pCurrentEntry)
{
   tBool bReturn = FALSE;

   if((pCurrentEntry->hShared < 0)||(pOsalData->u32MaxNrSharedMemElements < pCurrentEntry->hShared))
   {
	   NORMAL_M_ASSERT_ALWAYS();
	   TraceString("Index in OSAL shared Memory overwritten 0x%x !!!!",pCurrentEntry->hShared); 
	   return FALSE;
   }

   /* check if Pointer for current process is available */
   if(prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt > 0)
   {
      prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt--;
   }
   if((prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt == 0)
    &&(prLiShMemMap[pCurrentEntry->hShared].MemIdx))
   {
      if(munmap((void*)prLiShMemMap[pCurrentEntry->hShared].MemIdx,pCurrentEntry->u32SharedMemorySize) != 0)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      prLiShMemMap[pCurrentEntry->hShared].u32PrcIdx = 0;
      prLiShMemMap[pCurrentEntry->hShared].MemIdx = 0;
   }
   if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
   {
      if(shm_unlink(pCurrentEntry->szName) != 0)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      bReturn = TRUE;
   }
   return bReturn;
}


tS32 s32CheckForIoscShm(tCString coszName)
{
    tS32 s32Ret;
    if(strncmp("NOIOSC",coszName,6) == 0)
    {
       s32Ret = OSAL_ERROR;
    }
    else
    {
       s32Ret = OSAL_OK;
    }
    return s32Ret;
}

void vInitShMemMap(void)
{
   if(prLiShMemMap == NULL)
   {
      if((prLiShMemMap = (trResToPidMap*)malloc(pOsalData->u32MaxNrSharedMemElements * sizeof(trResToPidMap))) == NULL)
      {
         FATAL_M_ASSERT_ALWAYS();
      }
      memset(prLiShMemMap,0,pOsalData->u32MaxNrSharedMemElements * sizeof(trResToPidMap));
  }
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SharedMemoryCreate
 *
 * DESCRIPTION: this function creates an OSAL SharedMemory using the NUCLEUS
 *              memory pool.
 *
 * PARAMETER:
 *
 *   tCString       coszName (I) : name of the Shared Memory area to create.
 *   OSAL_tenAccess enAccess     : access rights;
 *   tU32 u32Size                : size in bytes.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_tShMemHandle Handle of Shared Memory if everything
 *                                     goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
OSAL_tShMemHandle OSAL_SharedMemoryCreate(tCString coszName,
                                          OSAL_tenAccess enAccess,
                                          tU32 u32Size)
{
   OSAL_tShMemHandle s32ReturnValue=(OSAL_tShMemHandle)OSAL_ERROR;
   tU32 u32ErrorCode=OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry = NULL;

   vInitShMemMap();
   
   /* The name is not NULL */
   if((coszName != NULL) && (enAccess <= OSAL_EN_READWRITE) && (u32Size != 0) )
   {
         /* The Name is not too long */
         if( OSAL_u32StringLength(coszName) < (tU32)(LINUX_C_SHMEM_MAX_NAMELENGHT))
         {
            if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
            {
               /* The name is not already in use*/
               pCurrentEntry = tSharedMemoryTableSearchEntryByName(coszName);
               if( pCurrentEntry == OSAL_NULL )
               {
                  pCurrentEntry = tSharedMemoryTableGetFreeEntry();
                  if( pCurrentEntry != OSAL_NULL )
                  {
                      //get shared memory
                      (void)OSAL_szStringNCopy((tString)pCurrentEntry->szName,
                                                coszName, LINUX_C_SHMEM_MAX_NAMELENGHT);
                       prLiShMemMap[pCurrentEntry->hShared].MemIdx = (uintptr_t)vpMmapShmToPrc(pCurrentEntry->szName,u32Size,TRUE);
                       if ((void*)prLiShMemMap[pCurrentEntry->hShared].MemIdx == 0)
                       {
                          u32ErrorCode=OSAL_E_NOSPACE;
                          FATAL_M_ASSERT_ALWAYS();
                       }
                       else
                       {
                          prLiShMemMap[pCurrentEntry->hShared].u32PrcIdx  = s32Pid;
                          prLiShMemMap[pCurrentEntry->hShared].u32PrcCnt  = 1;
                          pCurrentEntry->bIsUsed = TRUE;
                          pCurrentEntry->bToDelete = FALSE;
                          pCurrentEntry->u16OpenCounter = 1;
                          pCurrentEntry->s32PID = OSAL_ProcessWhoAmI();
                          pCurrentEntry->u32SharedMemoryID = pOsalData->u32MaxNrSharedMemElements + (uintptr_t)pCurrentEntry->hShared;
                          pCurrentEntry->pvSharedMemoryStartPointer = (void*)prLiShMemMap[pCurrentEntry->hShared].MemIdx;
                          pCurrentEntry->u32SharedMemorySize = u32Size;
                          s32ReturnValue = (OSAL_tShMemHandle)pCurrentEntry->u32SharedMemoryID;
                          pCurrentEntry->bIsMapped = pOsalData->bShMemTaceAll;
                      }
                  }
                  else
                  {
                    u32ErrorCode = OSAL_E_NOSPACE;
                    NORMAL_M_ASSERT_ALWAYS();
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_ALREADYEXISTS;
               }
               UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_NAMETOOLONG;
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode != OSAL_E_ALREADYEXISTS)
      {
         vTraceShMemInfo(pCurrentEntry,CREATE_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      else
      {
          if((pCurrentEntry &&(pCurrentEntry->bIsMapped == TRUE))||(u32OsalSTrace & 0x00000010))
          {
             vTraceShMemInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
          }
      }
   }
   else
   {
      if((pCurrentEntry->bIsMapped == TRUE)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceShMemInfo(pCurrentEntry,CREATE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
      pOsalData->u32ShMResCount++;
      if(pOsalData->u32MaxShMResCount < pOsalData->u32ShMResCount)
      {
          pOsalData->u32MaxShMResCount = pOsalData->u32ShMResCount;
          if(pOsalData->u32MaxShMResCount > (pOsalData->u32MaxNrSharedMemElements*9/10))
          {
           pOsalData->u32NrOfChanges++;
          }
       }
   }

   return( s32ReturnValue );
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SharedMemoryDelete
 *
 * DESCRIPTION: this function removes an OSAL Shared Memory using the NUCLEUS
 *              delete memory pool.
 *
 * PARAMETER:   coszName (I)
 *                 Shared Memory name to be removed.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SharedMemoryDelete(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry = NULL;
   if( coszName )
   {
         if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
         {
            pCurrentEntry = tSharedMemoryTableSearchEntryByName(coszName);
            if( pCurrentEntry != OSAL_NULL )
            {
               pCurrentEntry->bToDelete = TRUE;
               if( pCurrentEntry->u16OpenCounter == 0 )
               {
                  /* free is done during close*/
                  bDelLiPrcMemPtr(pCurrentEntry);
                  pCurrentEntry->bIsUsed = FALSE;
                  s32ReturnValue = OSAL_OK;
                  pOsalData->u32ShMResCount--;
               }
               else
               {
                  //u32ErrorCode = OSAL_E_BUSY;
                  s32ReturnValue = OSAL_OK;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }
            UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      vTraceShMemInfo(pCurrentEntry,DELETE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bIsMapped == TRUE)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceShMemInfo(pCurrentEntry,DELETE_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_SharedMemoryOpen
 *
 * DESCRIPTION: this function returns a valid handle to an OSAL SharedMemory
 *              already created.
 *
 * PARAMETER:   coszName (I)
 *                 SharedMemory name to be removed.
 *              phSharedMemory (->O)
 *                 pointer to the SharedMemory handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
OSAL_tShMemHandle OSAL_SharedMemoryOpen(tCString coszName, OSAL_tenAccess enAccess)
{
   tS32 s32ReturnValue  = OSAL_ERROR;
   tU32 u32ErrorCode    = OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry = NULL;

   vInitShMemMap();

   if( coszName && (enAccess <= OSAL_EN_READWRITE) )
   {
      if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
      {
         pCurrentEntry = tSharedMemoryTableSearchEntryByName(coszName);
         if( pCurrentEntry != OSAL_NULL )
         {
//            if( !pCurrentEntry->bToDelete )
            {
               pCurrentEntry->u16OpenCounter++;
               if(vpGetValidLiPrcMemPtr(pCurrentEntry,TRUE) != NULL)
               {
                  s32ReturnValue = pCurrentEntry->u32SharedMemoryID;
               }
               else
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);  
               }
            }
  /*          else
            {
               NORMAL_M_ASSERT_ALWAYS();
               u32ErrorCode = OSAL_E_BUSY;
            }*/
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode != OSAL_E_DOESNOTEXIST)
      {
         vTraceShMemInfo(pCurrentEntry,OPEN_OPERATION,TRUE,u32ErrorCode,coszName);
      }
      else
      {
         if((pCurrentEntry && (pCurrentEntry->bIsMapped == TRUE))||(u32OsalSTrace & 0x00000010))
         {
            vTraceShMemInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,coszName);
         }
      }
   }
   else
   {
      if((pCurrentEntry->bIsMapped == TRUE)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceShMemInfo(pCurrentEntry,OPEN_OPERATION,FALSE,u32ErrorCode,coszName);
      }
   }
   return( (OSAL_tShMemHandle)s32ReturnValue );  /* @@@@ 04.09.2002 Ka: Case because of quick */
}


/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SharedMemoryClose
 *
 * DESCRIPTION: this function closes an OSAL SharedMemory.
 *
 * PARAMETER:   hSharedMemory (I)
 *                 SharedMemory handle.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32SharedMemoryClose(OSAL_tShMemHandle hShared)
{
   tS32 s32ReturnValue=OSAL_ERROR;
   tU32 u32ErrorCode=OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry =NULL;
//   if( hShared >= 0 )
   {
         if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
         {
            pCurrentEntry = tSharedMemoryTableSearchEntryByHandle(hShared);
            if( pCurrentEntry != OSAL_NULL )
            {
               if( pCurrentEntry->u16OpenCounter > 0 )
               {
                  pCurrentEntry->u16OpenCounter--;
                  bDelLiPrcMemPtr(pCurrentEntry);
                  s32ReturnValue = OSAL_OK;
                  if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
                  {
                     pCurrentEntry->bIsUsed = FALSE;
                     pOsalData->u32ShMResCount--;
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOPERMISSION;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
            UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
         }
   }
/*   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }*/

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
       tCString coszName;
       if(pCurrentEntry)coszName = pCurrentEntry->szName;
       else coszName = "NO_NAME";
       vSetErrorCode( OSAL_C_THREAD_ID_SELF,u32ErrorCode);
       vTraceShMemInfo(NULL,CLOSE_OPERATION,TRUE,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry->bIsMapped == TRUE)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceShMemInfo(pCurrentEntry,CLOSE_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return( s32ReturnValue );
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SharedMemoryMap
 *
 * DESCRIPTION: this function gives a pointer to the start of the shared memory.
 *              The value of this pointer is process dependent.
 *
 * PARAMETER:
 *
 *   OSAL_tShMemHandle     hShared:   SharedMemory handle;
 *   OSAL_tenAccess        enAccess:  access rights;
 *   tU32                  u32Lenght: lenght;
 *   tU32                  u32OffSet: offset at the start of the area.
 *
 * RETURNVALUE:
 *
 *   tPvoid:               pointer or OSAL_DOESNOTEXIST;
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tPVoid OSAL_pvSharedMemoryMap(OSAL_tShMemHandle hShared,
                              OSAL_tenAccess enAccess,
                              tU32 u32Lenght,
                              tU32 u32OffSet)
{
   tPVoid pReturnPointer = OSAL_NULL;
   tU32 u32ErrorCode     = OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry = NULL;

   if(((intptr_t)hShared >= 0) && (enAccess > 0) && (enAccess <= OSAL_EN_READWRITE))
   {
         if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
         {
            pCurrentEntry = tSharedMemoryTableSearchEntryByHandle(hShared);

            if((pCurrentEntry!= OSAL_NULL)&&(pCurrentEntry->bIsUsed))
            {
               if( pCurrentEntry->u32SharedMemorySize >= (u32OffSet + u32Lenght) )
               {
                   pReturnPointer = (tPVoid)((uintptr_t)vpGetValidLiPrcMemPtr(pCurrentEntry,FALSE) + u32OffSet);
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
               }
            }
            else
            {
              u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }

            UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
         }
   }
   else
   {
#ifdef NO_PRIxPTR
         TraceString("Shared Mem MAP_OPERATION of Task ID %d Handle:0x%x Error:%d",OSAL_ThreadWhoAmI(),hShared,OSAL_E_INVALIDVALUE);
#else
         TraceString("Shared Mem MAP_OPERATION of Task ID %d Handle:0x%"PRIxPTR" Error:%d",OSAL_ThreadWhoAmI(),hShared,OSAL_E_INVALIDVALUE);
#endif
         u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceShMemInfo(pCurrentEntry,MAP_OPERATION,FALSE,u32ErrorCode,coszName);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF,u32ErrorCode);
   }
   else
   {
      if((pCurrentEntry->bIsMapped)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceShMemInfo(pCurrentEntry,MAP_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }
   return(pReturnPointer);
}

/*****************************************************************************
 *
 * FUNCTION: OSAL_s32SharedMemoryUnMap
 *
 * DESCRIPTION: this function releases a pointer requested to
 *              OSAL_pvSharedMemoryMap() again.
 *
 * PARAMETER:
 *   tpVoid                pvShareMemory:   pointer to SharedMemory;
 *   tU32                  u32Size: size of the area;
 *
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
 /*lint -save -e{818}*/
tS32 OSAL_s32SharedMemoryUnmap(tPVoid pvSharedMemory, tU32 u32Size)
 /*lint -restore */    
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trSharedMemoryElement *pCurrentEntry = NULL;
   /* satisfy lint*/
   (void)u32Size;
   u32ErrorCode = OSAL_E_NOERROR;
   if( pvSharedMemory != OSAL_NULL )
   {
         if(LockOsal(&pOsalData->SharedMemoryTable.rLock) == OSAL_OK)
         {
            pCurrentEntry = tSharedMemoryTableSearchEntryByPointer(pvSharedMemory);
            /* we cannot identify the pCurrentEntry, because it is not ensured that pvSharedMemory points
               to the begin of the shared memory segment -> therfore we generate no error */
            s32ReturnValue = OSAL_OK;
            if(!pCurrentEntry)
            {
               /* generate an error code to ensure traceability*/
               u32ErrorCode = OSAL_E_NOACCESS;
            }
            UnLockOsal(&pOsalData->SharedMemoryTable.rLock);
         }
   }
   else
   {
       u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vSetErrorCode( OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      vTraceShMemInfo(pCurrentEntry,UNMAP_OPERATION,FALSE,u32ErrorCode,coszName);
   }
   else
   {
       if((pCurrentEntry->bIsMapped)||(u32OsalSTrace & 0x00000010))/*lint !e613 pCurrentEntry already checked */
       {
            vTraceShMemInfo(pCurrentEntry,UNMAP_OPERATION,FALSE,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
       }
   }
   return(s32ReturnValue);
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
