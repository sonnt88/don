/*****************************************************************************
| FILE:         oserror.c
| PROJECT:      BMW_L6
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Error-Functions.
|                
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/
     
/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
   /* const Error-Strings, returned by OSAL_coszErrorText */
tCString szErrorString_NOPERMISSION     =         "The thread has no permission to call the function"; 
tCString szErrorString_INVALIDVALUE     =         "One parameter of the function is invalid e.g.too large";
tCString szErrorString_NOSPACE          =         "No system resources are available e.g. no free space";
tCString szErrorString_BUSY             =         "The system can not release the resource, as this is still used";
tCString szErrorString_INTERRUPT        =         "An Interruption has occurred whereas the thread waits";
tCString szErrorString_NOACCESS         =         "The access rights tom the resource are invalid";
tCString szErrorString_ALREADYEXISTS    =         "The resource exists already";
tCString szErrorString_DOESNOTEXIST     =         "Access to the resource can not be made,as this does not exist";
tCString szErrorString_MAXFILES         =         "The process has already too many file descriptors";
tCString szErrorString_NOFILEDESCRIPTOR =         "In the system no resources are available";
tCString szErrorString_NAMETOOLONG      =         "The name of the resource e.g. data file name is too long";
tCString szErrorString_BADFILEDESCRIPTOR=         "The specified data file descriptor does not exist";
tCString szErrorString_MSGTOOLONG       =         "The individual message is too long";
tCString szErrorString_QUEUEFULL        =         "The message box can not accept further messages";
tCString szErrorString_WRONGPROCESS     =         "The specified process does not exist";
tCString szErrorString_WRONGTHREAD      =         "The specified thread does not exist";
tCString szErrorString_EVENTINUSE       =         "A thread already waits for the evevnt";
tCString szErrorString_WRONGFUNC        =         "The specified function does not exist";
tCString szErrorString_NOTINTERRUPTCALLABLE=      "The function has been called by an interrupt level";
tCString szErrorString_INPROGRESS       =         "The function is not yet completed";
tCString szErrorString_TIMEOUT          =         "The function is aborted on account of a time out";
tCString szErrorString_NOTSUPPORTED     =         "The device does not support the specified function";
tCString szErrorString_CANCELED         =         "The asynchronous order has been aborted successfully";
tCString szErrorString_UNKNOWN          =         "An unknown error has occurred";
tCString szErrorString_DEFAULT          =         "No text translation for this error available!";
tCString szErrorString_NO_ERROR         =         "No error has occurred";
tCString szErrorString_THREAD_CREATE_FAILED =     "Thread create failed";
tCString szErrorString_MQINUSE          =         "MQINUSE: Description unknown!";
tCString szErrorString_ALREADYOPENED =            "device or file already opened";
tCString szErrorString_LOW_PRIORITY =             "PRM: Low priority, exclusive access not given";
tCString szErrorString_IN_EXCL_ACCESS =           "PRM/Dispatcher: In exclusive access";
tCString szErrorString_TEMP_NOT_AVAILABLE =       "Device temporary not available";
tCString szErrorString_MEDIA_NOT_AVAILABLE =      "Media not available";
tCString szErrorString_EJECT_LOCKED =             "Eject locked";
tCString szErrorString_NOTINITIALIZED =           "Not initialized";
tCString szErrorString_IOERROR =                  "IO error";
tCString szErrorString_NOT_MOUNTED =              "medium not mounted";
tCString szErrorString_ACCESS_FAILED =            "Access failed";
tCString szErrorString_WRITE_PROTECTED =          "Write protect active";
tCString szErrorString_DECRYPTION_FAILED =        "Decryption failed";


/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
extern tBool bMainTbcEntered;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function implementation (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function implementation (scope: global) 
|-----------------------------------------------------------------------*/

tU32 u32ConvertErrorCore(tS32 s32ErrCd)
{
 tU32 u32Return = OSAL_E_UNKNOWN;
 switch (s32ErrCd)
 {    
    case EPERM: 
         u32Return = OSAL_E_NOPERMISSION;            // 1    /* Operation not permitted */
        break;
    case ENOENT:         
         u32Return = OSAL_E_DOESNOTEXIST;            // 2    /* No such file or directory */
        break;
    case ESRCH:                                      
         u32Return =  OSAL_E_WRONGPROCESS;          //  3  /* No such process */
        break;
    case EINTR:          
         u32Return = OSAL_E_INTERRUPT;              // 4    /* Interrupted system call */
        break;
    case EIO:            
         u32Return = OSAL_E_IOERROR;                // 5    /* I/O error */
        break;
    case ENXIO:          
         u32Return = OSAL_E_MEDIA_NOT_AVAILABLE;    // 6    /* No such device or address */
        break;
    case E2BIG:          
         u32Return = OSAL_E_INVALIDVALUE;            // 7    /* Arg list too long */
        break;
 //   case ENOEXEC:   
 //        u32Return = OSAL_E_INVALIDVALUE;           //    8  /* Exec format error */
 //       break;                                           
    case EBADF:          
         u32Return = OSAL_E_BADFILEDESCRIPTOR;        // 9    /* Bad file number */
        break;
 //   case ECHILD:        
 //        u32Return = OSAL_E_INVALIDVALUE;           //    10  /* No child processes */
 //       break;                                           
    case EAGAIN:         
         u32Return = OSAL_E_TIMEOUT;            // 11    /* Try again */
        break;
    case ENOMEM:         
         u32Return = OSAL_E_NOSPACE;                // 12    /* Out of memory */
        break;
    case EACCES:         
         u32Return = OSAL_E_NOACCESS;                // 13    /* Permission denied */
        break;
    case EFAULT:         
         u32Return = OSAL_E_NOACCESS;                // 14    /* Bad address */
        break;
 //   case ENOTBLK:        
 //        u32Return = OSAL_E_INVALIDVALUE;           // 15  /* Block device required */
 //       break;                                           
    case EBUSY:          
         u32Return = OSAL_E_BUSY;                    // 16    /* Device or resource busy */
        break;
    case EEXIST:         
         u32Return = OSAL_E_ALREADYEXISTS;            // 17    /* File exists */
        break;
 //   case EXDEV:        
 //        u32Return = OSAL_E_INVALIDVALUE;           // 18  /* Cross-device link */
 //       break;                                           
    case ENODEV:         
         u32Return = OSAL_E_IOERROR;                 // 19    /* No such device */
        break;
    case ENOTDIR:
         u32Return = OSAL_E_INVALIDVALUE;            // 20    /* Not a directory */
        break;
    case EISDIR:
         u32Return = OSAL_E_INVALIDVALUE;            // 21    /* Is a directory */
       break;
    case EINVAL:         
         u32Return = OSAL_E_INVALIDVALUE;            // 22    /* Invalid argument */
        break;
    case ENFILE:         
         u32Return = OSAL_E_MAXFILES;                // 23    /* File table overflow */
        break;
    case EMFILE:         
         u32Return = OSAL_E_MAXFILES;                 // 24    /* Too many open files */
        break;
  // case ENOTTY      25  /* Not a typewriter */
  // case ETXTBSY     26  /* Text file busy */
  // case EFBIG       27  /* File too large */
    case ENOSPC:         
         u32Return = OSAL_E_NOSPACE;                   // 28    /* No space left on device */
        break;
  // case ESPIPE      29  /* Illegal seek */
    case EROFS:
         u32Return = OSAL_E_WRITE_PROTECTED;       // 30  /* Read-only file system */
        break;
  // case EMLINK      31  /* Too many links */
  // case EPIPE       32  /* Broken pipe */
  // case EDOM        33  /* Math argument out of domain of func */
  // case ERANGE      34  /* Math result not representable */
    case EDEADLK:
         u32Return = OSAL_E_NOTINITIALIZED;         // 35  /* Resource deadlock would occur */
        break;
    case ENAMETOOLONG:   
         u32Return = OSAL_E_NAMETOOLONG;            //     36    /* File name too long */
        break;
  // case ENOLCK      37  /* No record locks available */
    case ENOSYS:         
         u32Return = OSAL_E_NOTSUPPORTED;            // 38    /* Function not implemented */
        break;
    case ENOTSUP:        
         u32Return = OSAL_E_NOTSUPPORTED;            //      /* Operation not supported on transport endpoint */
        break;
    case ECANCELED:      
         u32Return = OSAL_E_CANCELED;
        break;
    case ETIMEDOUT:      
         u32Return =  OSAL_E_TIMEOUT;                 // 110    /* Connection timed out */
        break;
    case EMSGSIZE:/* message smaller than queue size, queue size is smaller than msg size 
                     is checked in osalmqueue */
         u32Return = OSAL_E_MSGTOOLONG; //90  /* Message too long */
        break;
    case EBADMSG: // message corrupt
         u32Return = OSAL_E_MQINUSE;
        break;
    default:
         TraceString("u32ConvertErrorCore converts %d to OSAL_E_UNKNOWN",s32ErrCd);
         u32Return = OSAL_E_UNKNOWN;
        break;
 }         
 return u32Return;
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_coszErrorText
 *
 * DESCRIPTION: This function gives for an error code the corresponding 
 *              description (in English) as string.
 *
 * PARAMETER:   tU32 u32ErrorCode: error code
 *
 * RETURNVALUE: tCString : String or OSAL_NULL in case of error.
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tCString OSAL_coszErrorText( tU32 u32Ecode )
{
   tCString szErrorString = (tCString)NULL;
   switch (u32Ecode)
   {
      case OSAL_E_NOERROR:
         szErrorString = szErrorString_NO_ERROR;
         break;
      case OSAL_E_NOPERMISSION:
         /*function call not allowed*/ 
         szErrorString = szErrorString_NOPERMISSION;
         break;
      case OSAL_E_INVALIDVALUE:
         /*wrong parameter */
         szErrorString = szErrorString_INVALIDVALUE;
         break;
      case OSAL_E_NOSPACE:
         /* no more ressources(memory)*/ 
         szErrorString = szErrorString_NOSPACE;
         break;
      case OSAL_E_BUSY:
         /* resource is in use */ 
         szErrorString = szErrorString_BUSY;
         break;
      case OSAL_E_INTERRUPT:
         /* interrupt during wait */ 
         szErrorString = szErrorString_INTERRUPT;
         break;
      case OSAL_E_NOACCESS:
         /* no acces rights  */
         szErrorString = szErrorString_NOACCESS;
         break;
      case OSAL_E_ALREADYEXISTS:
         /* ressource already exists */ 
         szErrorString = szErrorString_ALREADYEXISTS;
         break;
      case OSAL_E_DOESNOTEXIST:
         /* ressource doesn't exist */ 
         szErrorString = szErrorString_DOESNOTEXIST;
         break;
      case OSAL_E_MAXFILES:
         /* to much descriptors in use */
         szErrorString = szErrorString_MAXFILES;
         break;
      case OSAL_E_NOFILEDESCRIPTOR:
         /* no more resources(filedescr.) */
         szErrorString = szErrorString_NOFILEDESCRIPTOR;
         break;
      case OSAL_E_NAMETOOLONG:
         /* name for file or descr. too long */ 
         szErrorString = szErrorString_NAMETOOLONG;
         break;
      case OSAL_E_BADFILEDESCRIPTOR:
         /* not existing filedescriptor */ 
         szErrorString = szErrorString_BADFILEDESCRIPTOR;
         break;
      case OSAL_E_MSGTOOLONG:
         /* message is to long */
         szErrorString = szErrorString_MSGTOOLONG;
         break;
      case OSAL_E_QUEUEFULL:
         /* message queue is full */ 
         szErrorString = szErrorString_QUEUEFULL;
         break;
      case OSAL_E_WRONGTHREAD:
         /* thread doesn't exist */
         szErrorString = szErrorString_WRONGTHREAD;
         break;
      case OSAL_E_EVENTINUSE:
         /* a task is already waiting for event */ 
         szErrorString = szErrorString_EVENTINUSE;
         break;
      case OSAL_E_WRONGFUNC:
         /* function doesn't exist */ 
         szErrorString = szErrorString_WRONGFUNC;
         break;
      case OSAL_E_NOTINTERRUPTCALLABLE:
         /* function call is not allowed */
         szErrorString = szErrorString_NOTINTERRUPTCALLABLE;
         break;
      case OSAL_E_INPROGRESS:
         /* function is not ready */ 
         szErrorString = szErrorString_INPROGRESS;
         break;
      case OSAL_E_TIMEOUT:
         /* function abort because of timeout */
         szErrorString = szErrorString_TIMEOUT;
         break;
      case OSAL_E_NOTSUPPORTED:
         /* service not supported */
         szErrorString = szErrorString_NOTSUPPORTED;
         break;
      case OSAL_E_UNKNOWN:
         /* unknown error */ 
         szErrorString = szErrorString_UNKNOWN;
         break;
      case OSAL_E_THREAD_CREATE_FAILED:
         /* thread create failed */
         szErrorString = szErrorString_THREAD_CREATE_FAILED;
         break;
      case OSAL_E_WRONGPROCESS:
         /* process doesn't exist */
         szErrorString = szErrorString_WRONGPROCESS;
         break;
      case OSAL_E_CANCELED:
         /* asynchronous order successfully aborted */ 
         szErrorString = szErrorString_UNKNOWN;
         break;
      case OSAL_E_MQINUSE:
         /*  */ 
         szErrorString = szErrorString_MQINUSE;
         break;
      case OSAL_E_ALREADYOPENED:
         /*  */ 
         szErrorString = szErrorString_ALREADYOPENED;
         break;
      case OSAL_E_LOW_PRIORITY:
         /*  */ 
         szErrorString = szErrorString_LOW_PRIORITY;
         break;
      case OSAL_E_IN_EXCL_ACCESS:
         /*  */ 
         szErrorString = szErrorString_IN_EXCL_ACCESS;
         break;
      case OSAL_E_TEMP_NOT_AVAILABLE:
         /*  */ 
         szErrorString = szErrorString_TEMP_NOT_AVAILABLE;
         break;
      case OSAL_E_MEDIA_NOT_AVAILABLE:
         /*  */ 
         szErrorString = szErrorString_MEDIA_NOT_AVAILABLE;
         break;
      case OSAL_E_EJECT_LOCKED:
         /*  */ 
         szErrorString = szErrorString_EJECT_LOCKED;
         break;
      case OSAL_E_NOTINITIALIZED:
         /*  */ 
         szErrorString = szErrorString_NOTINITIALIZED;
         break;
      case OSAL_E_IOERROR:
         /*  */ 
         szErrorString = szErrorString_IOERROR;
         break;
      case OSAL_E_NOT_MOUNTED:
         /*  */ 
         szErrorString = szErrorString_NOT_MOUNTED;
         break;
      case OSAL_E_ACCESS_FAILED:
         /*  */ 
         szErrorString = szErrorString_ACCESS_FAILED;
         break;
      case OSAL_E_WRITE_PROTECTED:
         /*  */ 
         szErrorString = szErrorString_WRITE_PROTECTED;
         break;
      case OSAL_E_DECRYPTION_FAILED:
         /*  */ 
         szErrorString = szErrorString_DECRYPTION_FAILED;
         break;

      default:
         szErrorString = szErrorString_DEFAULT;
         break;
   }
   return szErrorString;
}


/*****************************************************************************
 *
 * FUNCTION:    OSAL_u32ErrorCode
 *
 * DESCRIPTION: this function returns the error code that as occurred last.
 *              The error is bound to the calling thread and filed in the 
 *              rispective thread control block.
 *
 * PARAMETER:   none
 *
 * RETURNVALUE: tU32 above mentioned return code
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tU32 OSAL_u32ErrorCode( tVoid )
{  
   tU32 u32ErrorCode = OSAL_E_UNKNOWN; 
   trThreadElement *pCurrentEntry = NULL;
   OSAL_tThreadID tid;
   
   if ((tid = OSAL_ThreadWhoAmI()) != OSAL_ERROR)
   {
      pCurrentEntry = tThreadTableSearchEntryByID(tid,FALSE);
      if (pCurrentEntry != OSAL_NULL)
      {
         u32ErrorCode = pCurrentEntry->u32ErrorCode;
      }
      else
      {
         u32ErrorCode = OSAL_E_UNKNOWN;
         if(bMainTbcEntered)
         {
             pCurrentEntry = CreatePseudoThreadEntry("OSAL_u32ErrorCode",OSAL_E_UNKNOWN);
             if(pCurrentEntry)
             {
                 u32ErrorCode = pCurrentEntry->u32ErrorCode;
             }
          }
      }
   }
   return(u32ErrorCode);
}

/*****************************************************************************
 *
 * FUNCTION:    OSAL_vSetErrorCode
 *
 * DESCRIPTION: this function sets the error code of the calling thread.
 *              The error handling routine is not called.
 *
 * PARAMETER:   tU32 u32ErrorCode: error code
 *
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tVoid OSAL_vSetErrorCode( tU32 u32ErrorCode )
{   
   trThreadElement *pCurrentEntry;
   OSAL_tThreadID tid;

   if ((tid = OSAL_ThreadWhoAmI()) != OSAL_ERROR)
   {
      pCurrentEntry = tThreadTableSearchEntryByID(tid,FALSE);
      if (pCurrentEntry != OSAL_NULL)
      {
         pCurrentEntry->u32ErrorCode = u32ErrorCode;
      }
      else
      {
         if(bMainTbcEntered)
         {
           CreatePseudoThreadEntry("OSAL_vSetErrorCode",u32ErrorCode);
         }
      }
   }
}



/*****************************************************************************
 *
 * FUNCTION:    OSAL_coszErrorText
 *
 * DESCRIPTION: This function gives for an error code the corresponding 
 *              description (in English) as string.
 *
 * PARAMETER:   tU32 u32ErrorCode: error code
 *
 * RETURNVALUE: tCString : String or OSAL_NULL in case of error.
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/

/*****************************************************************************
 *
 * FUNCTION:    OSAL_vErrorHook
 *
 * DESCRIPTION: This function sets for the calling thread a new error handling
 *              routine. This function is called exactly if in an OSAL-Function
 *              on account of an error situation the error code is set again.
 *              Through this it is possible to react centrally to possible
 *              error situations, in stead of checking each return value of 
 *              a function. During calling the error code is transferred 
 *              to the error handling routine. 
 *              The routine is bound to the calling thread.
 *              The old ErrorHook is returned if the second param of the 
 *              function is not NULL. 
 * PARAMETER:   const OSAL_tpfErrorHook  NewHook: new handling routine, 
 *              OSAL_tpfErrorHook* pOldHook      old handling routine or 
 *                                                  OSAL_NULL
 * RETURNVALUE: none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tVoid OSAL_vErrorHook( const OSAL_tpfErrorHook  NewHook,
                             OSAL_tpfErrorHook* pOldHook )
{
   OSAL_tThreadID tid;
   trThreadElement *pCurrentEntry;
   
   if ((tid = OSAL_ThreadWhoAmI()) != OSAL_ERROR)
   {
      pCurrentEntry = tThreadTableSearchEntryByID(tid,FALSE);
      if (pCurrentEntry != OSAL_NULL)
      {
         if(pOldHook != OSAL_NULL)
         {
            *pOldHook = pCurrentEntry->pfErrorHook;
         }
         pCurrentEntry->pfErrorHook = NewHook;
      }   
   /*
     else
     {
        *pOldHook  = (OSAL_tpfErrorHook)NULL;
        pCurrentEntry->pfErrorHook = (OSAL_tpfErrorHook)NULL;
     } 
   */
   }
}

/*****************************************************************************
 *
 * FUNCTION:    OSAL_s32CallErrorHook
 *
 * DESCRIPTION: This function executes the error handling routine of the 
 *              calling thread, in case it has been set earlier. It is to be 
 *                          noted that it is not possible to call another error hook 
 *                          function recursively within an error handling routine.
 *
 * PARAMETER:   tU32 u32ErrorCode: error code
 *
 * RETURNVALUE:  s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |  Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 OSAL_s32CallErrorHook( tU32 u32ErrorCode )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tThreadID tid  = OSAL_ERROR;
   trThreadElement *pCurrentEntry;

   /* Check if the ErrorCode is set */
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      if ((tid = OSAL_ThreadWhoAmI()) != OSAL_ERROR)
      {  
         pCurrentEntry = tThreadTableSearchEntryByID(tid,FALSE);
         if(pCurrentEntry != OSAL_NULL)
         {
            /* Check the ErrorHook function is valid and besides
               the ErrorHook function is not called by another ErrorHook function */
            if (pCurrentEntry->pfErrorHook && 
                pCurrentEntry->u32ErrorHooksCalled==0)
            {
               pCurrentEntry->u32ErrorHooksCalled++;
               pCurrentEntry->pfErrorHook( u32ErrorCode );
               pCurrentEntry->u32ErrorHooksCalled--;
               s32ReturnValue=OSAL_OK;
            }
         }
      }
   }

   return(s32ReturnValue);
}

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osalerror.c
|-----------------------------------------------------------------------*/
