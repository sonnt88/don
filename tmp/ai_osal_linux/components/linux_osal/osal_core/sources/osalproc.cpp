/******************************************************************************
| FILE:         osalproc.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Thread-Functions.
|                
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/ 

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include <sys/prctl.h>
#include <dlfcn.h>
#include <unistd.h>
/* for Linux specific gettid syscall */
#include <sys/syscall.h>
#include "ostrace.h"
#include "exception_handler.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#define LINUX_C_THREAD_MAX_NAMELENGTH (OSAL_C_U32_MAX_NAMELENGTH - 1)

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
OSAL_DECL tU32 minStackSize = 0x20000;
#else
OSAL_DECL tU32 minStackSize = 0x10000;
#endif
tU32 u32PrcExitStep = 0;
tS32 s32ResetPid = -1;
tBool bMainTbcEntered = FALSE;
tU32 u32OsalSTrace = 0;
tBool RealTimPrioActive = FALSE;
OSAL_tSemHandle  hLicenceToKillSem = OSAL_C_INVALID_HANDLE;

static OSALCALLBACKFUNC   pExitFunc[5] = {NULL,NULL,NULL,NULL,NULL};
static tU32*              pArg[5]      = {NULL,NULL,NULL,NULL,NULL};

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
extern tBool bTimOutMqSvActive;
extern tBool bTimOutSemSvActive;
extern tBool bAIO;
extern char process_name[256];

/************************************************************************
|function prototype
|-----------------------------------------------------------------------*/
tS32 s32ThreadTableCreate(void);
tS32 s32ThreadTableDeleteEntries(void);
trThreadElement* tThreadTableGetFreeEntry(tVoid);
tBool bThreadTableDeleteEntryByName(tCString coszName);

#ifdef SIGNAL_EV_HDR
extern tU32 u32ProcessRecIpcSignal(tS32 s32Sig,siginfo_t* pInfo);
#endif

extern void InitRecMqToList(void);
extern void InitSemWaitToList(void);
extern void InitOsalServices(void);
extern void vUnLoadModulsForExit(void);
extern void StopSupervisionTimer(void);
extern void vStopDebugTask(void);
extern void vOnProcessDetach(void);
extern tS32 s32ShutdownAsyncTask(void);

/************************************************************************ 
|function implementation (scope: module-local) 
|-----------------------------------------------------------------------*/

static void clearThreadEntry(trThreadElement*  pCurrent)
{
#ifdef RBTREE_SEARCH
    /* free tree node */
    vRemoveElement(prRBTree,pCurrent->kernel_tid);
#endif

    memset(pCurrent->szName,0,OSAL_C_U32_MAX_NAMELENGTH);
    pCurrent->u32Priority = 0;
    pCurrent->tStartTime = 0;
    pCurrent->u32TaskStack = 0;
    pCurrent->u32ErrorCode = OSAL_E_NOERROR;
    pCurrent->u32ErrorHooksCalled = 0;
    pCurrent->pfErrorHook = NULL;
    pCurrent->bIsSetReadyStatus = FALSE;
    pCurrent->bIsSetSuspendedStatus = FALSE;
    pCurrent->bMarkedDeleted = FALSE;
    pCurrent->bIsUsed = FALSE;
  //  pCurrent->pfOsalEntry = NULL;
  //  pCurrent->pvArg = NULL;
    pCurrent->tid = 0;
    pCurrent->pid = 0;
    pCurrent->kernel_tid = 0;
}

void vPostSystemInfo(tS32 s32Value)
{
   trPrmMsg rMsg;
   OSAL_tMQueueHandle hPrm;
   rMsg.u32EvSignal = SIGNAL_SYS_STATINFO;
   rMsg.u32Val      = (tU32)s32Value;

   if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
   {
      if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      OSAL_s32MessageQueueClose(hPrm );
   }
}

tS32 s32CleanupProcessTable(void)
{
   tS32 desc,s32Ret = OSAL_ERROR;
   tU32 n;
   char cPath[100];
   for (n = 0; n < pOsalData->u32MaxNrProcElements; n++)
   {
      if (prProcDat[n].u32Pid)
      {
         snprintf(cPath,100,"/proc/%d",(unsigned int)prProcDat[n].u32Pid);
         desc = open(cPath,O_RDONLY);
         if(desc != -1)
         {
             close(desc);
         }
         else
         {
            prProcDat[n].u32Pid = 0;
            prProcDat[n].u32Tid = 0;
            if(s32Ret == OSAL_ERROR)s32Ret = n;
         }
      }
   }
   return s32Ret;
}

tS32 s32CleanupThreadTable(tS32 s32Pid)
{
  tS32 s32Count=0;
  /* ensure thread table is locked */
  trThreadElement *pCurrentEntry = NULL;
  tS32 s32Tid = OSAL_ThreadWhoAmI();
  tU32 tiIndex;
  for(tiIndex=0;  tiIndex <  pOsalData->ThreadTable.u32MaxEntries ; tiIndex++)
  {
    if((pThreadEl[tiIndex].pid == s32Pid)
     &&(pThreadEl[tiIndex].bIsUsed == TRUE)
     &&(pThreadEl[tiIndex].tid != (tU32)s32Tid))
    {
       pCurrentEntry = &pThreadEl[tiIndex];
       TraceString("Pid:%d Task %s (%d) make clean Index:%d",s32Pid,pCurrentEntry->szName,pCurrentEntry->kernel_tid,tiIndex);
       clearThreadEntry(pCurrentEntry);
       s32Count++;
    }
  }
  return s32Count;
}


void vCleanupPrcResources(siginfo_t info)
{
  tS32 s32Pid = OSAL_ERROR;
  tS32 status;
  tU32 n;
  tBool bMarker;
#ifdef SIGNAL_TRACE
  TraceString("PRC HDR sigwaitinfo received SIGCHLD");
#endif
  errno = 0;
  do {
     s32Pid = waitpid(0, &status, WNOHANG);// wait for any child process and return immediately if no child has exited. 
  } while (s32Pid == -1 && (errno == EINTR));

  /* waitpid(): on success, returns the process ID of the child whose state has changed; if WNOHANG was specified and one 
     or more child(ren) specified by pid exist, but have not yet changed state, then 0 is returned. On error, -1 is returned.
     info.si_pid is pid of terminated process, it is not POSIX */
  if(s32Pid == 0)s32Pid = info.si_pid;
  bMarker = FALSE;
  if( LockOsal( &pOsalData->ProcessTable.rLock ) == OSAL_OK )
  {
     for (n = 0; n < pOsalData->u32MaxNrProcElements; n++)
     {
        if (prProcDat[n].u32Pid == (tU32)s32Pid)
        {
           TraceString("PID:%d detects OSAL Process %s with Pid:%d terminated \n",
                       getpid(),&prProcDat[n].pu8CommandLine[0],s32Pid);
           bMarker = TRUE;
           // TODO: store the exit code? release the entry?
           prProcDat[n].u32Pid = 0;
           prProcDat[n].u32Tid = 0;
           if(s32ResetPid == s32Pid)
           {
               vWritePrintfErrmem("PID:%d detects OSAL Process %s with Pid:%d terminated \n",
                       getpid(),&prProcDat[n].pu8CommandLine[0],s32Pid);
               OSAL_s32ThreadWait(100);
               FATAL_M_ASSERT_ALWAYS();
           }
        }
    }
    UnLockOsal( &pOsalData->ProcessTable.rLock );
  }
  /* ensure thread table is consistent */
  if( LockOsal( pThreadLock ) == OSAL_OK )
  {
     n = s32CleanupThreadTable(s32Pid);
     UnLockOsal( pThreadLock );
     if(n)
     {
        TraceString("vCleanupPrcResources ThreadTable Cleanup done for %d Threads",n);
     }
  }
  if((s32Pid != 0)&&(s32Pid != -1))
  {
     if(bMarker == FALSE)
     {
         TraceString("PID:%d detects Process %d terminated",getpid(),s32Pid);
     }
     vPostSystemInfo(s32Pid);
  }
}


static void vSignalHandler(void *arg)
{
  tS32 s32Ret=0;
  tS32 s32Data[4]={0,0,0,0};
  tS32 fifo=open(VOLATILE_DIR"/OSAL/EXC_HDR_FIFO", O_RDONLY);
  ((void)arg);

  if(fifo != -1)
  {
     while (1) 
     {
        s32Ret = read(fifo,(char*)&s32Data[0],4*sizeof(tS32));
        if(s32Ret > 0)
        {
           if(s32Data[0] == SIGCHLD)
           {
              TraceString("SIGCHLD received in PID:%d for Terminated PID:%d with status:%d",
                           s32Data[3],s32Data[1],s32Data[2]);
         /*     info.si_pid = s32Data[1];
              vCleanupPrcResources(info);*/
           }
           else
           {
              TraceString("Unknown Signal %d received in PID:%d for Terminated PID:%d with status:%d",
                          s32Data[0],s32Data[3],s32Data[1],s32Data[2]);
           }
        }
        else
        {
            TraceString("PRC HDR read fifo failed errno:%d",errno);
        }
     }
  }
  else
  {
     TraceString("PRC HDR open fifo failed errno:%d",errno);
  }
}
 
#ifdef OLD_SIGNAL_HDR_TASK

static void vSignalHandler(void *arg)
{
   sigset_t set;
   siginfo_t info;
   int sig;
   char cBuffer[100];

   ((void)arg);
   sigemptyset(&set);
   sigaddset(&set, SIGHUP);
   sigaddset(&set, SIGCHLD);
#ifdef SIGNAL_EV_HDR
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_AND);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_OR);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_XOR);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_REPLACE);

   /* To force rescheduling when OSAL event signal is received this task is set to real time priority */
   struct sched_param param;
   int sched_policy;
   // todo change priority
   if(pthread_getschedparam( pthread_self(), &sched_policy, &param) == 0)
   {
      param.sched_priority = 1;
      sched_policy = SCHED_RR;  // SCHED_RR,SCHED_FIFO, SCHED_OTHER
      if(pthread_setschedparam( pthread_self(), sched_policy, &param) != 0)
      {
          TraceString("Signal handler task Setting prio failed");
      }
   } 
#endif

   while (1) {
      sig = sigwaitinfo(&set, &info);
      switch (sig)
      {
      case SIGCHLD:
           vCleanupPrcResources(info);
         break;
      case SIGHUP:
         /* leave in for debug/test, until vOnProcessDetach() defined as destructor
          * TODO: find a way for "proper" tear down (just remove sem and shmem)
          */
#ifdef SIGNAL_TRACE
         TraceString("PRC HDR sigwaitinfo received SIGHUP");
#endif
         vOnProcessDetach();
         _exit(0);
         break;
#ifdef SIGNAL_EV_HDR
      case OSAL_SIGNAL_EVENT_PRC_REPLACE:
      case OSAL_SIGNAL_EVENT_PRC_AND:
      case OSAL_SIGNAL_EVENT_PRC_OR:
      case OSAL_SIGNAL_EVENT_PRC_XOR:
#ifdef SIGNAL_TRACE
           TraceString("PRC HDR sigwaitinfo received OSAL Event signal");
#endif
           u32ProcessRecIpcSignal(sig,&info);
          break;
#endif
      case -1:
           TraceString("PRC HDR PID:%d sigwaitinfo failed errno:%d",getpid(),errno);
         break;
      default:
           TraceString("PRC HDR PID:%d sigwaitinfo received unhandled signal %d",getpid(),sig);
           vWritePrintfErrmem("PRC HDR PID:%d sigwaitinfo received unhandled signal %d \n");
         break;
      }
   }

}
#endif

void vSetThreadSignalMask(void)
{
   sigset_t set;
   sigemptyset(&set);
   sigaddset(&set, pOsalData->u32TimSignal);
   sigaddset(&set, pOsalData->u32TimSignal-1);
   sigaddset(&set, pOsalData->u32TimSignal-2);
   sigaddset(&set, OSAL_CB_HDR_SIGNAL);
   sigaddset(&set, SIGHUP);
   if(!pOsalData->u32CatchSigchild)
   {
      sigaddset(&set, SIGCHLD);
   }
#ifdef SIGNAL_EV_HDR
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_AND);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_OR);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_XOR);
   sigaddset(&set, OSAL_SIGNAL_EVENT_PRC_REPLACE);
#endif
   if (pthread_sigmask( SIG_BLOCK, &set, NULL) != 0)
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
}

void vSignalHandlerInstall(void)
{
   OSAL_trThreadAttribute  attr;
   char cNameBuf[OSAL_C_U32_MAX_NAMELENGTH];
   /*
    * prior to any pthread* call the PROCESS signal mask has to block
    * the signal used on sigwaitinfo
    * this is normally done in the vWrapperEntry function , but we are in Prozess attach function
    */
 //  vSetThreadSignalMask(); shifted to process attach

   snprintf( &cNameBuf[0], sizeof(cNameBuf), "SIG_HDR%d", getpid() );

   attr.szName = (tString)cNameBuf;
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = minStackSize;
   attr.pfEntry = (OSAL_tpfThreadEntry)vSignalHandler;
   attr.pvArg = NULL;
   (void) OSAL_ThreadSpawn(&attr);
}


/******************************************************************************
 * FUNCTION:      vWrapperEntry
 *
 * DESCRIPTION:   this function is used by TENGINE to call OSAl Thread function,  
 *                to adapt OSAL arguments to TENGINE arguments
 *
 * PARAMETER:
 *                UNSIGNED  tid  ID of the thread used to identify OSAL 
 *                                    pointer 
 *               
 *                VOID *pvArg         pointer to untyped argument
 *
 * RETURNVALUE:   
 *
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ******************************************************************************/
static void* vWrapperEntry(void* pvArg)
{
   trThreadElement*  pCurrentEntry;
   pCurrentEntry = (trThreadElement* )pvArg;
   char cName[16];

   vSetThreadSignalMask();
 
   if (pCurrentEntry->szName[0] != 0)
   {
      init_exception_handler(pCurrentEntry->szName);
   }
   else
   {
      init_exception_handler("unnamed");
   }

   /*
    * this value will be shown in OSAL_GET_RESOURCES TASKS
    * gettid(2) has no prototype/wrapper in glibc, we have to call it via syscall()
    */
   pCurrentEntry->kernel_tid = syscall(SYS_gettid);

   if(sem_post(&pCurrentEntry->hTempSemHandle) != 0) 
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   (void)OSAL_szStringNCopy(cName,
                           (tString)pCurrentEntry->szName,
                            15);
   cName[15] = '\0';
   if(prctl(PR_SET_NAME,cName,0,0,0) == OSAL_ERROR)
   {
      /* error*/
   }

   /*int State = 0;
   * PTHREAD_CANCEL_DEFERRED is default *
   if(pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &State) == 0)
   {
	   TraceString("Change pthread_setcancelstate form %d to %d",State,PTHREAD_CANCEL_ENABLE);
   }
   * PTHREAD_CANCEL_DEFERRED is default *
   if(pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &State) == 0)
   {
	   TraceString("Change pthread_setcanceltype form %d to %d",State,PTHREAD_CANCEL_ASYNCHRONOUS);
   }*/
	 
   if (pCurrentEntry->pfOsalEntry)
   {
      while(1)
      {
         if(sem_wait(&pCurrentEntry->hSemHandle) == -1)
         {
             if(errno != EINTR)
             {
                 vWritePrintfErrmem("vWrapperEntry sem_wait with errno:%d",errno);
                 FATAL_M_ASSERT_ALWAYS();
             }
         }
         else
         {
            /* now thread is activated ,semaphore not needed furthermore */
            sem_destroy(&pCurrentEntry->hSemHandle);
            pCurrentEntry->pfOsalEntry(pCurrentEntry->pvArg);  /* call OSAL thread entry */
            break;
         }
      }
   } 
   else 
   {
       /* someone else trampled over this entry (in past it was the terminator */
       NORMAL_M_ASSERT_ALWAYS();
   }
   pOsalData->u32TskResCount--;
   OSAL_vThreadExit();
   return NULL;
}

void vErrorHookFunc(tU32 u32ErrorCode)
{
#ifdef TRACE_ERROR_CODES
   char au8Buf[5];
   int slen = 0;
   OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_CORE_ERROR);
   OSAL_M_INSERT_T32(&au8Buf[1],u32ErrorCode);
 //  slen = strlen(szName);
 //  memcpy (&au8Buf[5],szName,slen+1);
   LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_ERROR,au8Buf,9+slen);
#else
   ((tVoid)u32ErrorCode); // satisfy lint
#endif
}


/*****************************************************************************
 *
 * FUNCTION:    s32ThreadTableCreate
 *
 * DESCRIPTION: This function creates the Thread Table List. If 
 *              there isn't space it returns a error code.
 *
 * PARAMETER:   tThreadTable *aThreadTable 
 *                 pointer to the Thread table List
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32ThreadTableCreate(tVoid)
{
   tU32 tiIndex;
   tS32 s32ReturnValue = OSAL_OK;

   pOsalData->ThreadTable.u32UsedEntries   = 0;
   pOsalData->ThreadTable.u32MaxEntries    = pOsalData->u32MaxNrThreadElements;

   for(tiIndex=0;  tiIndex <  pOsalData->ThreadTable.u32MaxEntries ; tiIndex++)
   {
//      pThreadEl[tiIndex].bIsUsed = FALSE;
//      pThreadEl[tiIndex].bMarkedDeleted = FALSE;
//     pThreadEl[tiIndex].tid = 0;
//      pThreadEl[tiIndex].kernel_tid = 0;
        memset(&pThreadEl[tiIndex],0,sizeof(trThreadElement));
   }
   
   if(CreateOsalLock(pThreadLock, "TSK_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }
#ifdef RBTREE_SEARCH
   /* prepare indexing*/
   for(tiIndex=0;  tiIndex < pOsalData->u32MaxNrThreadElements; tiIndex++)
   {
      pThreadEl[tiIndex].s32Index = tiIndex;
   }
   /* init RB tree structure */ 
   vInitNodes(pOsalData->u32MaxNrThreadElements+1,prRBTree,&pNodeEl[0],pThreadLock);
#endif

   if(mkdir(VOLATILE_DIR"/OSAL/Processes",S_IFDIR | OSAL_ACCESS_RIGTHS) == -1)
   {
       if(errno != EEXIST)
       {
           TraceString("OSAL mkdir /.../Processes failed error:%d",errno);
       }
   }
   else
   {
      if(chmod(VOLATILE_DIR"/OSAL/Processes", OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
      {
          vWritePrintfErrmem("ThreadTableCreate -> /run/OSAL/Processes  chmod error %d \n",errno);
      }
   }

   return(s32ReturnValue);
}

tU32 u32ThreadTableCleanup(void)
{
   tU32 u32Count = 0;
   tU32 tiIndex;
   char Buffer[100];
   int fd = 0;
   trThreadElement* pCurrentEntry;

   for(tiIndex=0;  tiIndex <  pOsalData->u32MaxNrThreadElements; tiIndex++)
   {
      snprintf(Buffer,100,"/proc/%d/task/%d",
               pThreadEl[tiIndex].pid,pThreadEl[tiIndex].kernel_tid);
      if((fd = open(Buffer, O_RDONLY)) == -1)
      {
         pCurrentEntry = tThreadTableSearchEntryByID(pThreadEl[tiIndex].kernel_tid,TRUE);
         if(pCurrentEntry != OSAL_NULL)
         {
            clearThreadEntry(pCurrentEntry);
            u32Count++;
         }
      }
      else
      {
         close(fd);
      }
   }
   TraceString("%d Thread entries removed from OSAL thread table",u32Count);
   return u32Count;
}


/*****************************************************************************
 *
 * FUNCTION:    tThreadTableGetFreeEntry
 *
 * DESCRIPTION: this function goes through the Thread List and returns the
 *              first ThreadElement.In case no entries are available a new 
 *              defined number of entries is attempted to be added. 
 *              In case of success the first new element is returned, otherwise
 *              a NULL pointer is returned. 
 *
 *
 * PARAMETER:   tVoid
 *
 * RETURNVALUE: trThreadElement*  
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
trThreadElement* tThreadTableGetFreeEntry(tVoid)
{
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;
 
   if (used < pOsalData->ThreadTable.u32MaxEntries)
   {
       pCurrent = &pThreadEl[used];
       pOsalData->ThreadTable.u32UsedEntries++;
   } else {
       /* search an entry with !bIsUsed, used == u32MaxEntries*/
       while ( pCurrent <= &pThreadEl[used]
             && ( pCurrent->bIsUsed == TRUE) )
       {
          pCurrent++;
       }
       if(pCurrent >= &pThreadEl[used])
       {
          pCurrent = NULL; /* not found */
       } 
   }

   if(pCurrent == NULL)
   {
     if(u32ThreadTableCleanup())
     {
        return  tThreadTableGetFreeEntry();
     }
   }
   return pCurrent;
}


/*****************************************************************************
 *
 * FUNCTION:    tThreadTableSearchEntryByID
 *
 * DESCRIPTION: this function goes through the Thread List and returns the
 *              ThreadElement with the given ID or NULL if all the List has 
 *              been checked without success.
 *              NOTE: the search is restricted to the calling process scope!
 *              The pthread_t id is only unique per process.
 *              The kernel_tid is unique system wide.
 *
 *
 * PARAMETER:   OSAL_tThreadID tid
 *              thread ID wanted.
 *
 * RETURNVALUE: trThreadElement*
 *                 the looked for entry pointer or OSAL_NULL
 *
*
 *****************************************************************************/
 trThreadElement *tThreadTableSearchEntryByID(OSAL_tThreadID tid,tBool bLocked)
{
   trThreadElement *pCurrent  = NULL;
#ifdef RBTREE_SEARCH
   if(!prRBTree->bInValidRbTree)
   {
      trElementNode* pElement = pSearchElement(prRBTree,tid,bLocked);
      if(pElement)
      {
         pCurrent = &pThreadEl[pElement->u32Index];
         if(pCurrent->bIsUsed == FALSE)pCurrent  = NULL;
      }
   }
   if(pCurrent == NULL)
   {
      if(prRBTree->bInValidRbTree)
      {
#endif
         ((void)bLocked);
         tU32 used = pOsalData->ThreadTable.u32UsedEntries;
         pCurrent  = pThreadEl;
         /* search an entry with tid and the current pid */
         while ( pCurrent <= &pThreadEl[used]
             &&((pCurrent->bIsUsed == FALSE)||((OSAL_tThreadID)pCurrent->kernel_tid != tid)) )
         {
            pCurrent++;
         }
         if(pCurrent >= &pThreadEl[used])
         {
            pCurrent = NULL;
         }
#ifdef RBTREE_SEARCH
      }
   }
#endif
   return pCurrent;
}

/*****************************************************************************
 *
 * FUNCTION:    tThreadTableSearchEntryByName
 *
 * DESCRIPTION: this function goes through the Thread List and returns the
 *              ThreadElement with the given name or NULL if all the List has 
 *              been checked without success.
 *              The namespace is per process - a thread name has to be unique
 *              only in a process
 *
 * PARAMETER:   coszName (I)
 *                 thread name wanted.
 *
 * RETURNVALUE: trThreadElement*  
 *                 free entry pointer or OSAL_NULL
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
static trThreadElement *tThreadTableSearchEntryByName(tCString coszName)
{
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;

   /* search an entry with coszName and the current pid */
   while ( pCurrent <= &pThreadEl[used]
         && ((pCurrent->bIsUsed == FALSE)||(OSAL_s32StringCompare(coszName, pCurrent->szName))) )
   {
      pCurrent++;
   }
   if(pCurrent >= &pThreadEl[used])
   {
      pCurrent = NULL;
   }
   return pCurrent;
}

/*****************************************************************************
*
* FUNCTION:    bThreadTableDeleteEntryByName
*
* DESCRIPTION: this function goes through the Thread List deletes the
*              ThreadElement with the given name 
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool bThreadTableDeleteEntryByName(tCString coszName)
{
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;

   /* search an entry with coszName and the current pid */
   while ( pCurrent <= &pThreadEl[used]
         && ((pCurrent->bIsUsed == FALSE)||(OSAL_s32StringCompare(coszName, pCurrent->szName))) )
   {
      pCurrent++;
   }
   if(pCurrent >= &pThreadEl[used])
   {
      return FALSE;
   } 
   else 
   {
      clearThreadEntry(pCurrent);
      return TRUE;
   }
}

/*****************************************************************************
*
* FUNCTION:    bCleanUpThreadofContext
*
* DESCRIPTION: this function goes through the Thread List deletes the
*              Thread and ThreadElement with the given context from list
*
* PARAMETER:   tS32 the pid as Context
*
* RETURNVALUE: tBool  
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool bCleanUpThreadofContext(tS32 pid)
{
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;
   tBool bResult = TRUE;

   /* search an entry with coszName and the current pid */
   while ( pCurrent <= &pThreadEl[used] )
   {
      if ( ( pCurrent->bIsUsed == TRUE) && (pid == pCurrent->pid) )
      {
         if (OSAL_s32ThreadDelete(pCurrent->kernel_tid) != OSAL_OK)
         {
            bResult = FALSE;
         }
      }
      pCurrent++;
   }
   return bResult;
}

 
/*****************************************************************************
*
* FUNCTION:    CreatePseudoThreadEntry
*
* DESCRIPTION: this function creates a pseudo task in the thread table
*              to use OSAL functions from task which are not spawned by OSAL
*
* PARAMETER:   tU32 u32Func         Function in which the pseudo Handle is generated
*              tU32 u32ErrorCode    occurred error code which should be stored
*
* RETURNVALUE: trThreadElement*     pointer to created entry or NULL
*
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
trThreadElement* CreatePseudoThreadEntry(tCString szFunc, tU32 u32ErrorCode)
{
   trThreadElement *pCurrentEntry = NULL;
   tS32 tid = OSAL_ThreadWhoAmI();

   TraceString("Non OSAL Task %d calls %s",tid,szFunc);
 
   if( LockOsal( pThreadLock ) == OSAL_OK )
   {
      if ((pCurrentEntry = tThreadTableSearchEntryByID( tid,TRUE)) == OSAL_NULL) // check if created, before
      {
         pCurrentEntry = tThreadTableGetFreeEntry();
         if( pCurrentEntry != OSAL_NULL )
         {
            FILE* fdstr = NULL;
            int TID   = 0;
            char cName[PATH_MAX] = {0};
            snprintf(cName,PATH_MAX,"/proc/%d/task/%d/stat",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI());
            fdstr = fopen(cName, "r");
            if(fdstr)
            {
               fscanf(fdstr,"%d ",&TID);     // 1. TID
               fscanf(fdstr,"%s ",&cName[0]);// 2. Taskname
               fclose(fdstr);
               (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                          cName,
                                          OSAL_C_U32_MAX_NAMELENGTH-1);
            }
            else
            {
               (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                           "orphan",
                                           OSAL_C_U32_MAX_NAMELENGTH );
            }
            pCurrentEntry->pvArg                 = NULL;
            pCurrentEntry->pfOsalEntry           = NULL;
            pCurrentEntry->pfTaskEntry           = NULL;
            pCurrentEntry->u32TaskStack          = 0;
            pCurrentEntry->pid                   = OSAL_ProcessWhoAmI();
            pCurrentEntry->tid                   = pthread_self();
            pCurrentEntry->kernel_tid            = tid;
            pCurrentEntry->u32Priority           = 0;      // Time sharing prio

            pCurrentEntry->u32ErrorCode          = u32ErrorCode;
            pCurrentEntry->u32ErrorHooksCalled   = 0;
            pCurrentEntry->pfErrorHook           = (OSAL_tpfErrorHook)OSAL_NULL;

            pCurrentEntry->bIsSetReadyStatus     = TRUE;
            pCurrentEntry->bIsSetSuspendedStatus = FALSE;
            pCurrentEntry->bMarkedDeleted         = FALSE;
            pCurrentEntry->bIsUsed               = TRUE;
            pCurrentEntry->tStartTime            = OSAL_ClockGetElapsedTime();
#ifdef RBTREE_SEARCH
            vInsertElement(prRBTree,pCurrentEntry->kernel_tid,pCurrentEntry->s32Index);
#endif
         }
      }
      else
      {
          pCurrentEntry->u32ErrorCode          = u32ErrorCode;
      }
   }
   else
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   UnLockOsal( pThreadLock );
   return pCurrentEntry;
}


/******************************************************************************
* FUNCTION:      vSetErrorCode 
*
* DESCRIPTION:   this function set the error codes into the system table
*                of threads;
*
* PARAMETER:
*   OSAL_tThreadID tid            : OSAL identifier of thread
*   tu32           u32ErrorCode   : error code to store in the 
*                                   specific field 
*
* RETURNVALUE:   NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tVoid vSetErrorCode( OSAL_tThreadID tid, tU32 u32ErrorCode )
{
   trThreadElement *pCurrentEntry = NULL; 

   /* tid is called with tid = OSAL_C_THREAD_ID_SELF
      and OSAL_C_THREAD_ID_SELF = OSAL_ThreadWhoAmI(); */

   if( (tid != 0) && (tid != OSAL_ERROR) )
   {
      pCurrentEntry = tThreadTableSearchEntryByID(tid,FALSE);
      if(pCurrentEntry)
      {
         pCurrentEntry->u32ErrorCode = u32ErrorCode;

         if((u32ErrorCode != OSAL_E_NOERROR) 
            &&(pCurrentEntry->pfErrorHook != (OSAL_tpfErrorHook)NULL) 
            &&(pCurrentEntry->u32ErrorHooksCalled == 0))
         {
            pCurrentEntry->u32ErrorHooksCalled++;
            pCurrentEntry->pfErrorHook( u32ErrorCode );
            pCurrentEntry->u32ErrorHooksCalled--;
         }
      }
      else
      {
         if(bMainTbcEntered == TRUE)
         {
            CreatePseudoThreadEntry("vSetErrorCode",u32ErrorCode);
         }
         else
         {
            TraceString("Create Pseudo Entry denied in vSetErrorCode PID:%d -> Shutdown",OSAL_ProcessWhoAmI());
         }
      }
   }
   else
   {
       TraceString("vSetErrorCode with TID:%d called",tid);
   }
}


/************************************************************************ 
|function implementation (scope: global) 
|-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    s32ThreadTableDeleteEntries
 *
 * DESCRIPTION: This function deletes all elements from OSAL table,
 *              which are labeled with corresponding u16ContextID
 *
 * PARAMETER:   
 *   u16ContextID                  index to distinguish caller context             
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.            
 *              
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
 *****************************************************************************/
tS32 s32ThreadTableDeleteEntries(void)
{
   tS32 s32ReturnValue = OSAL_OK;
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;

   /* search the used table */
   while ( pCurrent <= &pThreadEl[used])
   {
      if (pCurrent->bIsUsed == TRUE)
      {
         s32ReturnValue++;
         OSAL_s32ThreadDelete(pCurrent->kernel_tid);
      }
      pCurrent++;
   }
   return(s32ReturnValue);
}

void* pvLoadModule(const OSAL_trProcessAttribute* prAttr,tOSAL_s32PrcStart* pFunc)
{
   char *error;
   dlerror();

   void* pModuleHandle;

   pModuleHandle = dlopen(prAttr->szAppName, RTLD_NOW | RTLD_GLOBAL);

   if(pModuleHandle == NULL)
   {
      if( (error = dlerror()) != NULL)
      {
        fprintf(stderr,"\nCould not start process: %s\n",error);
      }
      else
      {
         //ELog(ADDINFO, E_FATAL, "Could not start process %s (no error msg)", m_oAttr.szAppName);
      }
   }
   else
   {
      tOSAL_s32PrcStart pEntry = (tOSAL_s32PrcStart)(dlsym(pModuleHandle, (const char*)"OSAL_s32Boot"));/*lint !e611 */
      *pFunc = pEntry;
      if( pFunc == NULL )
      {
         error = dlerror();
         if (error != NULL)
         {
            // ELog(ADDINFO, E_FATAL, "Message: %s", error);
         }
         dlclose(pModuleHandle);
         pModuleHandle = OSAL_NULL;
      }
   }
   return pModuleHandle;
}

static int parse_arguments(const OSAL_trProcessAttribute *pcorAttr, tString **arg_vector)
{
   tU32 argc = 2;
   tString *argv;
   tChar sep[] = " \t\n";
   tPS8 token;
   tS32 s32Length;
   tU32 u32Index = 0;
   tString tempString = OSAL_NULL;

   if (OSAL_NULL == arg_vector)
   {
      return 0;
   }
   if(pcorAttr->szCommandLine != OSAL_NULL)
   {
      s32Length = OSAL_u32StringLength(pcorAttr->szCommandLine);
      if(s32Length > 0)
      {
         tempString = (tString)OSAL_pvMemoryAllocate(s32Length+1);
         if(tempString != OSAL_NULL)
         {
            OSAL_szStringCopy(tempString, pcorAttr->szCommandLine);
         }
      }
      if(tempString != OSAL_NULL)
      {
         token = OSAL_ps8StringToken(tempString, sep);
         while(token != OSAL_NULL)
         {
            ++argc;
            token = OSAL_ps8StringToken(OSAL_NULL, sep);
         }
      }
   }

   argv = (tString*) OSAL_pvMemoryAllocate( sizeof(tString) * argc );

   if(argv != NULL)
   {
      argv[1] = NULL;
      s32Length = 0;
      if(pcorAttr->szAppName != OSAL_NULL)
         s32Length = OSAL_u32StringLength(pcorAttr->szAppName);
      if(s32Length > 0)
      {
         argv[0] = (tString)OSAL_pvMemoryAllocate(s32Length+1);
         if(argv[0] != OSAL_NULL)
         {
            OSAL_szStringCopy(argv[0], pcorAttr->szAppName);
         }
      }

      if( (pcorAttr->szCommandLine != OSAL_NULL) &&
          (tempString != OSAL_NULL))
      {
         OSAL_szStringCopy(tempString, pcorAttr->szCommandLine);

         u32Index = 1;
         token = OSAL_ps8StringToken(tempString, sep);
         while( (token != OSAL_NULL) &&
                (u32Index < argc) )
         {
            // Copy token
            s32Length = OSAL_u32StringLength(token);
            if(s32Length > 0)
            {
               argv[u32Index] = (tString)OSAL_pvMemoryAllocate(s32Length+1);
               if(argv[u32Index] != OSAL_NULL)
               {
                  OSAL_szStringCopy(argv[u32Index], token);
               }
            }
            token = OSAL_ps8StringToken(OSAL_NULL, sep);
            ++u32Index;
         }
         /* last entry must be NULL for execvp */
         argv[ argc-1 ] = (char*)NULL;
      }
   }
   else
   {
      argc = 0;
   }
   if (tempString != OSAL_NULL)
   {
      OSAL_vMemoryFree(tempString);
   }
   *arg_vector = argv;

   return argc;
}


static void OSAL_SharedLibEntryThread(void *arg)
{
    tOSAL_s32PrcStart pFunc = (tOSAL_s32PrcStart) arg;/*lint !e611 */
    // Call OSAL_s32Boot
    if(pFunc != OSAL_NULL){
        (*(pFunc))(0, NULL);
    }
    return;
}

/*****************************************************************************
 *
 * FUNCTION:    s32ThreadSetAffinity
 *
 * DESCRIPTION: This function set a process's cpu affinity mask
 *              Under linux there are 2 cpu available where the thread can
 *              be bind to: cpu0 or cpu1.
 *              The thread id can be 0 - if thread id is zero, then the
 *              calling thread is used.
 *
 * PARAMETER:   tS32 s32ThreadId     Id of process.
 *              tS32 s32Cpu          0 or 1. Bind to cpu 0 or cpu 1.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 16.03.11  | Initial revision                       | RAD3HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32ThreadSetAffinity(tS32 s32ThreadId, tS32 s32Cpu)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_UNKNOWN;
   cpu_set_t set;
   tS32 s32Return;

   if ((s32ThreadId >=  0) && (s32Cpu == CPU0 || s32Cpu == CPU1))
   {
               CPU_ZERO(&set); /* clear all CPUs*/

               if( s32Cpu == CPU0 )
                   CPU_SET( CPU0, &set );  /* allow core #0 */
               else if( s32Cpu == CPU1 )
                   CPU_SET( CPU1, &set );  /* allow core #1 */

      if (s32ThreadId > 0)
         s32Return = pthread_setaffinity_np(s32ThreadId, sizeof( cpu_set_t ), &set);
             else
                s32Return = pthread_setaffinity_np(0, sizeof( cpu_set_t ), &set);

             if (s32Return == OSAL_ERROR)
                u32ErrorCode = u32ConvertErrorCore(errno);
             else
                s32ReturnValue = OSAL_OK;
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      NORMAL_M_ASSERT_ALWAYS();
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      OSAL_vSetErrorCode(u32ErrorCode);
      TraceString("s32ThreadSetAffinity Error:%d",u32ErrorCode);
   }

   return(s32ReturnValue);
}


/*****************************************************************************
 *
 * FUNCTION:    s32ProcessSetAffinity
 *
 * DESCRIPTION: This function set a process's cpu affinity mask
 *              Under linux there are 2 cpu available where the procces can
 *              be bind to: cpu0 or cpu1.
 *              The process id can be 0 - if process id is zero, then the
 *              calling process is used.
 *
 * PARAMETER:   tS32 s32ProcessId    Id of process.
 *              tS32 s32Cpu          0 or 1. Bind to cpu 0 or cpu 1.
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 16.03.11  | Initial revision                       | RAD3HI
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32ProcessSetAffinity(tS32 s32ProcessId, tS32 s32Cpu)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   cpu_set_t set;
   tS32 tS32Result=OSAL_ERROR;

   if ((s32ProcessId >=  0) &&  (s32Cpu == CPU0 || s32Cpu == CPU1))
   {
      CPU_ZERO(&set);           /* clear all CPUs*/

      if( s32Cpu == 0 )
         CPU_SET( CPU0, &set );   /* allow core #0 */
      else if( s32Cpu == 1 )
         CPU_SET( CPU1, &set );   /* allow core #1 */

      if (s32ProcessId > 0)
         tS32Result = sched_setaffinity(s32ProcessId, sizeof(cpu_set_t), &set);
      else
         tS32Result = sched_setaffinity(0, sizeof(cpu_set_t), &set); /* calling ProcessId */

      if (tS32Result == OSAL_ERROR)
         u32ErrorCode = u32ConvertErrorCore(errno);
      else
         s32ReturnValue = OSAL_OK;
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      NORMAL_M_ASSERT_ALWAYS();
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      OSAL_vSetErrorCode(u32ErrorCode);
      TraceString("s32ProcessSetAffinity Error:%d",u32ErrorCode);
   }

   return(s32ReturnValue);
}

/*****************************************************************************
 *
 * FUNCTION:    s32ProcessTableCreate
 *
 * DESCRIPTION: This function creates the Process Table List. If
 *              there isn't space it returns a error code.
 *
 * PARAMETER:
 *
 * RETURNVALUE: s32ReturnValue
 *                 it is the function return value:
 *                 - OSAL_OK if everything goes right;
 *                 - OSAL_ERROR otherwise.
 * HISTORY:
 * Date      |   Modification                         | Authors
 * --.--.--  | ----------------                       | -----
 *
 *****************************************************************************/
tS32 s32ProcessTableCreate(tResourceTable *pTable)
{
   tU32 tiIndex;
   tS32 s32ReturnValue = OSAL_OK;

   ((tVoid)pTable); // satisfy lint

   pOsalData->ProcessTable.u32UsedEntries   = 0;
   pOsalData->ProcessTable.u32MaxEntries    = pOsalData->u32MaxNrProcElements;

   if(CreateOsalLock(&pOsalData->ProcessTable.rLock, "PRC_LOCK") != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }

   for(tiIndex=0;  tiIndex <  pOsalData->ProcessTable.u32MaxEntries ; tiIndex++)
   {
     prProcDat[tiIndex].u32Pid = 0;
     prProcDat[tiIndex].u32Tid = 0;
   }

   return(s32ReturnValue);
}

tS32 tProcessTableGetFreeIndex(tVoid)
{
   tU32 tiIndex = 0;
   tBool bEntryFound = FALSE;

   if(LockOsal( &pOsalData->ProcessTable.rLock ) == OSAL_OK )
   {
      for(tiIndex=0;  tiIndex <  pOsalData->u32MaxNrProcElements; tiIndex++)
      {
        if((prProcDat[tiIndex].u32Pid == 0)&&(prProcDat[tiIndex].u32Tid == 0))
        { 
            pOsalData->ProcessTable.u32UsedEntries++;
            bEntryFound = TRUE;
            break;
        }
      }
      UnLockOsal( &pOsalData->ProcessTable.rLock );
   }
   if(bEntryFound == TRUE)
   {
      return tiIndex;
   }
   else
   {
      tiIndex = s32CleanupProcessTable();
      if(tiIndex >= pOsalData->u32MaxNrProcElements)
      {
         FATAL_M_ASSERT_ALWAYS();
         return -1;
      }
      else
      {
         return tiIndex;
      }
   }
}

tS32 s32FindProcEntry(tS32 s32Pid)
{
   tU32 i;
   for (i=0;i < pOsalData->u32MaxNrProcElements;i++)
   {
      if(prProcDat[i].u32Pid == (tU32)s32Pid)return i;
   }
   return OSAL_ERROR;
}

void vEnterProcessToVolatile(tS32 s32Pid , tBool bEnter)
{
    char szName[64];
    int fd;
    /* */
    snprintf(szName,64,"%s/OSAL/Processes/%d",VOLATILE_DIR,s32Pid);
    if(bEnter)
    {
       if((fd = open(szName, O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC,OSAL_ACCESS_RIGTHS)) != -1)
       {
          if(s32OsalGroupId)
          {
             if(chown(szName,(uid_t)-1,s32OsalGroupId) == -1)
             {
                vWritePrintfErrmem("vEnterProcessToVolatile -> chown error %d Task:%s \n",errno,szName);
             }
             if(chmod(szName, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
             {
                vWritePrintfErrmem("vEnterProcessToVolatile %s chmod error %d \n",chmod, errno);
             }
          }
          snprintf(szName,64,"SIG_BACKTRACE:%d\n",SIG_BACKTRACE);
          if(write(fd,szName,strlen(szName)) < 0)
          {
             snprintf(szName,64,"%s/OSAL/Processes/%d",VOLATILE_DIR,s32Pid);
             TraceString("Write to %s failed",szName);
          }
          (void)write(fd,commandline,strlen(commandline));
          (void)write(fd,"\n",strlen("\n"));
          close(fd);
       }
    }
    else
    {
      (void)remove(szName);
    }
}

#ifdef SIGTERM_HANDLER
void vCancelThreadsOfPrc(tS32 s32Pid)
{
   trThreadElement *pCurrent  = pThreadEl;
   tU32 used = pOsalData->ThreadTable.u32UsedEntries;
   tS32 s32Tid = OSAL_ProcessWhoAmI();

   /* search an entry with !bIsUsed, used == u32MaxEntries*/
   while ( pCurrent <= &pThreadEl[used]
      && ( pCurrent->bIsUsed == TRUE) )
   {
       if((pCurrent->pid == s32Pid)&&(pCurrent->kernel_tid != (tU32)s32Tid))
       {
         if(pthread_cancel( (pthread_t)pCurrent->tid ) == -1)
          {
              TraceString("Cancel Thread %d in process %d failed",pCurrent->kernel_tid,pCurrent->pid);
          }
       }
       pCurrent++;
   }
}


void Terminator( tVoid *pArgument )
{
   ((void)pArgument);
   char Buffer[64];
   snprintf(Buffer,64,"%d-TerminatorSem",OSAL_ProcessWhoAmI());
   if(OSAL_s32SemaphoreCreate(Buffer,&hLicenceToKillSem,0) == OSAL_OK)
   {
      if(OSAL_s32SemaphoreWait(hLicenceToKillSem,OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
      {
         TraceString("Task %s get trigger via Licence to kill semaphore",Buffer);
         vCancelThreadsOfPrc(getpid());
         OSAL_vProcessExit();
      }
   }
}
#endif

void vCheckPrcSpecConf(void)
{
   int i;
   /* check for OSAL main process which instantiate PRM, Supervision task and signal handler task*/
   if(pOsalData->s32MainOsalPrcPid == 0)
   {
      if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->OsalMainApp))
      {
         pOsalData->s32MainOsalPrcPid = gpOsalProcDat->u32Pid;
         TraceString("PID:%d Main OSAL PRC %s",gpOsalProcDat->u32Pid,gpOsalProcDat->pu8AppName);
         InitOsalServices();
      }
      else if(strstr(gpOsalProcDat->pu8AppName,"procoedt"))
      {
         pOsalData->s32MainOsalPrcPid = gpOsalProcDat->u32Pid;
         TraceString("PID:%d Main OSAL PRC %s",gpOsalProcDat->u32Pid,gpOsalProcDat->pu8AppName);
         InitOsalServices();
      }
   }
   
   /* check for various process specific configuration which changes OSAL default handling */
   /* complete OSAL exception handling is active */
   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      if(pOsalData->rBoschExcHdrApps[i].szAppName[0])
      {
         if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->rBoschExcHdrApps[i].szAppName))
         {
            TraceString("PID:%d Bosch ExcHdr handler for Process %s",gpOsalProcDat->u32Pid,gpOsalProcDat->pu8AppName);
            s32ExcHdrStatus = 1;
            /* entry found leave while loop */
            break;
         }
      }
      else
      {
         /* no further entry leave while loop */
         break;
      }
   }
   /* OSAL timer handling without signals is active */
   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      if(pOsalData->rNoSigTimerApp[i].szAppName[0])
      {
         if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->rNoSigTimerApp[i].szAppName))
         {
            u32LocalTimerSig = 0;
            bTimOutMqSvActive  = FALSE;
            bTimOutSemSvActive = FALSE;
            /* entry found leave while loop */
            break;
         }
      }
      else
      {
         /* no further entry leave while loop */
         break;
      }
   }
   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      if(pOsalData->rRtApp[i].szAppName[0])
      {
         if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->rRtApp[i].szAppName))
         {             
            RealTimPrioActive = TRUE;
            /* entry found leave while loop */
            break;
         }
      }
      else
      {
         /* no further entry leave while loop */
         break;
      }
   }
}

void vCheckPrcDbgSpecConf(void)
{
   int i;
   if(pOsalData->u32StraceAll)
   {
      u32OsalSTrace = pOsalData->u32STraceLevel;
   }
   else
   {
      if(pOsalData->szSTraceProcess[0])
      {
         if(strstr("*.*",(const char*)pOsalData->szSTraceProcess))
         {
            pOsalData->u32StraceAll = 1;
            u32OsalSTrace = pOsalData->u32STraceLevel;
         }
         else
         {
            if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->szSTraceProcess))
            {
               u32OsalSTrace = pOsalData->u32STraceLevel;
               TraceString("PID:%d OSAL STRACE for %s",gpOsalProcDat->u32Pid,gpOsalProcDat->pu8AppName);
         
               if(u32OsalSTrace & 0x00000002)
               {
                  for(i=0;i<OSAL_EN_DEVID_LAST;i++)
                  {
                     if(OSAL_EN_DEVID_TRACE == i)
                     {
                        if(u32OsalSTrace & 0x10000000)
                        {
                           pOsalData->rDevTrace[i].cActive = 1;
                           pOsalData->rDevTrace[i].s32Pid  = gpOsalProcDat->u32Pid;
                        }
                     }
                     else
                     {
                        pOsalData->rDevTrace[i].cActive = 1;
                        pOsalData->rDevTrace[i].s32Pid  = gpOsalProcDat->u32Pid;
                     }
                  }
               }
            }
         }
      }
   }
   if(pOsalData->CheckTimerApp[0])
   {
      if(strstr(gpOsalProcDat->pu8AppName,(const char*)pOsalData->CheckTimerApp))
      {
         pOsalData->s32CheckTimPid = gpOsalProcDat->u32Pid;
         TraceString("PID:%d Supervise Timer of %s",pOsalData->s32CheckTimPid,gpOsalProcDat->pu8AppName);
      }
   }
}


tS32 vAddProcessEntry(tU32 u32Count)
{
   char *buffer;
/*   int bufsiz;
   FILE *fp;*/
   trThreadElement *pCurrentEntry = NULL;
   char *threadName;
   pthread_attr_t      thread_attr;
#ifdef OSAL_SETS_PRIORITY
   sched_param priority;
   int policy;
#endif

   if (u32Count < pOsalData->u32MaxNrProcElements)
   {
      pOsalData->u32PrcResCount++;
      if(pOsalData->u32MaxPrcResCount < pOsalData->u32PrcResCount)pOsalData->u32MaxPrcResCount = pOsalData->u32PrcResCount;
 
      /* set default timer handling */
      u32LocalTimerSig = pOsalData->u32TimSignal;

      if(pOsalData->u32ClkMonotonic & 0x00000001)
      {
         bTimOutMqSvActive = TRUE;
      }
      if(pOsalData->u32ClkMonotonic & 0x00000010)
      {
         bTimOutSemSvActive = TRUE;
      } 

      InitRecMqToList();
      InitSemWaitToList();

 
      /*
       * for fast and easy access, each process has it's own
       * pointer to it's processEntry
       */
      gpOsalProcDat = &prProcDat[u32Count];

      gpOsalProcDat->u32Pid = (tU32)OSAL_ProcessWhoAmI();
      gpOsalProcDat->u32Tid = (tU32)OSAL_ThreadWhoAmI();

      vEnterProcessToVolatile(gpOsalProcDat->u32Pid ,TRUE);

/*      buffer = gpOsalProcDat->pu8CommandLine;
      bufsiz = sizeof(gpOsalProcDat->pu8CommandLine);
      fp = fopen("/proc/self/cmdline", "r");
      if (fp) 
      {
         if (fread(buffer, 1, bufsiz-1, fp) > 0)
         {
            buffer[bufsiz-1] = 0;
            bufsiz = strlen(gpOsalProcDat->pu8CommandLine);
            int i=0;
            for (i=bufsiz-1;i >= 0;i--)
            {
               if(buffer[i] == '/')
               {
                   strncpy(gpOsalProcDat->pu8AppName,&buffer[i+1],sizeof(gpOsalProcDat->pu8AppName));
                   break;
               }
            }
         }
         else
         {
            strcpy(buffer, "<no name>");
            strncpy(gpOsalProcDat->pu8AppName,"<no name>",strlen("<no name>"));
         }
         fclose(fp);
      }
      else
      {
         strcpy(buffer, "<no name>");
         strncpy(gpOsalProcDat->pu8AppName,"<no name>",strlen("<no name>"));
      }*/

      strncpy(gpOsalProcDat->pu8CommandLine,commandline,strlen(commandline));
      strncpy(gpOsalProcDat->pu8AppName,process_name,sizeof(process_name));
//      TraceString("%s",gpOsalProcDat->pu8CommandLine);
//      TraceString("%s",gpOsalProcDat->pu8AppName);

  /*    buffer = gpOsalProcDat->pu8AppName;
      bufsiz = sizeof(gpOsalProcDat->pu8AppName);
      fp = fopen("/proc/self/comm", "r");
      if (fp) {
         if (fread(buffer, 1, bufsiz-1, fp) > 0)
            buffer[bufsiz-1] = 0;
         else
            strcpy(buffer, "<no comm>");
         fclose(fp);
      }*/

      /* fill the osal thread table with the initial process thread values */
      threadName = gpOsalProcDat->pu8AppName;
      if(LockOsal(pThreadLock) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableGetFreeEntry();
         if( pCurrentEntry != OSAL_NULL )
         {
            //OSAL_pvMemorySet( pCurrentEntry, 0, sizeof(trThreadElement));
            (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                     threadName,
                                     OSAL_C_U32_MAX_NAMELENGTH );

            pCurrentEntry->pvArg                 = NULL;
            pCurrentEntry->pfOsalEntry           = NULL;
            pthread_attr_init(&thread_attr );
            if (pthread_attr_getstacksize(&thread_attr, (size_t*)&pCurrentEntry->u32TaskStack))
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            pCurrentEntry->pid                   = OSAL_ProcessWhoAmI();
            pCurrentEntry->tid                   = pthread_self();
            pCurrentEntry->kernel_tid            = OSAL_ThreadWhoAmI();
#ifdef RBTREE_SEARCH
            vInsertElement(prRBTree,pCurrentEntry->kernel_tid,pCurrentEntry->s32Index);
#endif

#ifdef OSAL_SETS_PRIORITY
            priority.sched_priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
            if (sched_setscheduler(pCurrentEntry->pid, SCHED_RR, &priority))
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            pCurrentEntry->u32Priority = priority.sched_priority;
#else
            pCurrentEntry->u32Priority = 0;
#endif

            pCurrentEntry->u32ErrorCode          = 0;
            pCurrentEntry->u32ErrorHooksCalled   = 0;
            pCurrentEntry->pfErrorHook           = (OSAL_tpfErrorHook)OSAL_NULL;
            pCurrentEntry->bIsSetReadyStatus     = TRUE;
            pCurrentEntry->bIsSetSuspendedStatus = FALSE;
            pCurrentEntry->bMarkedDeleted        = FALSE;
            pCurrentEntry->bIsUsed               = TRUE;
        }
         bMainTbcEntered = TRUE;
         UnLockOsal( pThreadLock );
      }

      /* check for process specific default behavior configuration */
      vCheckPrcSpecConf();

      /* check for process specific analysing configuration */
      vCheckPrcDbgSpecConf();
	  

      /* export OSAL_MIN_STACKSIZE=128000   */
      if(pOsalData->u32UseLibunwind)minStackSize = 0x100000;
      buffer = getenv("OSAL_MIN_STACKSIZE");
      if (buffer)
      {
         errno = 0;
         minStackSize = strtol(buffer, NULL, 0);
         if (errno)
            minStackSize = 0x100000;
      }

#ifdef SIGTERM_HANDLER
    //  if(strstr(gpOsalProcDat->pu8AppName,"PROCNAV"))
      {
         /* install a threads which terminates the navigation application 
            via OSAL_vProcessExit to archieve a cleanup */
         OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
         char Buffer[64];
         snprintf(Buffer,64,"%d-Terminator",OSAL_ProcessWhoAmI());
         trAtr.szName       = Buffer;
         trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
         trAtr.s32StackSize = 4096;
         trAtr.pfEntry      = (OSAL_tpfThreadEntry)Terminator;
         trAtr.pvArg        = NULL;
         OSAL_ThreadSpawn(&trAtr);
      }
#endif      
   }
   else
   {
     FATAL_M_ASSERT_ALWAYS();
   }
   return u32Count;
}

tS32 s32GetAbsolutNiceValue(tS32 s32Prio,tS32 s32Tid)
{
      char buffer[240];
      tS32 s32CurNiceLevel = 0;
      tS32 s32NewNiceLevel = s32Prio;
      snprintf(buffer,240,"/proc/%d/task/%d",getpid(),s32Tid);

      s32CurNiceLevel = (tS32)u32ScanPidTaskTidStat(buffer,FALSE,19);
      if(s32CurNiceLevel != 0)
      {
          if(s32CurNiceLevel < 0)
          {
             s32NewNiceLevel = s32Prio - s32CurNiceLevel;
          }
          else
          {
             s32NewNiceLevel = s32Prio + s32CurNiceLevel;
          }
      }
      return s32NewNiceLevel;
}


void vSetNiceLevel(const OSAL_trProcessAttribute *pcorAttr)
{            
    if(pOsalData->u32Prio >= 1)
    {
       tS32 s32NiceLevel = (tS32)pcorAttr->u32Priority;
       if((s32NiceLevel >= -20)&&(s32NiceLevel <= 19))
       { 
          s32NiceLevel = s32GetAbsolutNiceValue((tS32)pcorAttr->u32Priority,OSAL_ThreadWhoAmI());
          errno = 0;
          if(nice(s32NiceLevel) == -1)
          {
             if(errno != 0)
             {
                NORMAL_M_ASSERT_ALWAYS();
             }
          }
       }
       else
       {
           if(LLD_bIsTraceActive( OSAL_C_TR_CLASS_SYS_PRC, TR_LEVEL_ERROR) )
           {
               TraceString("OSAL_ProcessSpawn ->  No valid Nice Level given Name %s Prio %d",
                           pcorAttr->szName, s32NiceLevel);
           }
       }
    }
    else
    {
        if(LLD_bIsTraceActive( OSAL_C_TR_CLASS_SYS_PRC, TR_LEVEL_COMPONENT) )
        {
           TraceString("OSAL_ProcessSpawn ->  No Nice Level configuration configured via OSAL Name %s Prio %d ",
                       pcorAttr->szName,(tS32)pcorAttr->u32Priority);
        }
    }
}

void vSetCgroup(tString szCGroup_path, tS32 s32Pid)
{
#define MAX_PID_LEN 7 // 65535 = 7
#define FILENAME_LEN_ADD 7 //"/tasks" = 7
   char string_pid[MAX_PID_LEN]="";
   int fd=0;
   int written_bytes=0;
   int path_len=0;
   int string_len=0;
   tString filename = OSAL_NULL;

         if(szCGroup_path && szCGroup_path[0])
         {
            //need to change the CGroup
            path_len=OSAL_u32StringLength(szCGroup_path) + FILENAME_LEN_ADD;
            filename = (tString)OSAL_pvMemoryAllocate(path_len);
            if(filename != OSAL_NULL){
               (void)OSAL_szStringCopy(filename,szCGroup_path);
               (void)OSAL_szStringConcat(filename,"/tasks");
               // now the path is "/<cgrouppath>/tasks"
               fd=open(filename,O_WRONLY | O_APPEND | O_DSYNC);
               if (fd > 0){
                  // open of the tasks file successful
                  sprintf((char *)&string_pid,"%d",s32Pid);        // itoa of pid;
                  string_len=OSAL_u32StringLength(&string_pid);
                  // now write the (string)PID to the file ----> causing the forked process running in this cgroup
                  written_bytes=write(fd,&string_pid,string_len);
                  if (written_bytes != -1){
                        if (written_bytes == string_len){
                            if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_DATA))
                            {
                               TraceString("successful changed CGroup\n");
                            }
                           // successfully added the process/thread to the cgroup
                        }else {
                           // error too few bytes written
                           // failed to add the process to the cgroup
                        }
                  }else {
                     // failed to add the process to the cgroup
                     TraceString("failed to write Process to CGroup");
                  }
                  close(fd);
               } else{
                  // could not open file for write
                  TraceString("failed to open file");
               }
            } else {
               TraceString("failed to allocate memory");
            }
            if (filename != OSAL_NULL){
               OSAL_vMemoryFree(filename);
            }
         }
}
/******************************************************************************
* FUNCTION:      OSAL_ProcessSpawn
*
* DESCRIPTION:   This Function creates a new process and execute the
*                OSAL_s32Boot function of the specified application.
*                The name of the first thread correspond to the process name.
*
* PARAMETER:
*
*   const OSAL_trProcessAttribute*    pointer to a record that contains
*                                     the attributes of new process.
*
* RETURNVALUE:
*
*   OSAL_tProcessID : pid of the Process or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
* 10.08.15  | CFG3_1405 : Updating return value with | AHM5KOR
*           | error code in case of invalid parameter|
******************************************************************************/
OSAL_tProcessID OSAL_ProcessSpawn(const OSAL_trProcessAttribute *pcorAttr)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_UNKNOWN;
   tU32 u32Index;
   tU32 argc;
   intptr_t pHandle = 0;
   tS32 s32Pid = -1;
   tString *argv = 0;
   void* hModul;
   char *suffix;
   tOSAL_s32PrcStart pFunc = NULL;
   OSAL_trThreadAttribute th_attr;
   tBool bProcess = TRUE;
   argc = parse_arguments(pcorAttr, &argv );
   suffix = strrchr(pcorAttr->szAppName, '.');

   if(suffix)
   {
      if((0 == strcmp(suffix, ".so"))
      || (0 == strcmp(suffix, ".SO")))
      {
          bProcess = FALSE;
      }
   }
   if(bProcess == FALSE)
   {
      hModul = pvLoadModule(pcorAttr,&pFunc);

      if(hModul != OSAL_NULL)
      {
          // Call OSAL_s32Boot
          if( pFunc != OSAL_NULL )
          {
              if( OSAL_s32StringCompare( pcorAttr->szAppName, "libprocbase_so.so") == 0 )
              {
                  // In case lib proc base is spawn we must not return 
                  // Call OSAL_s32Boot
                  (*(pFunc))(0, NULL);
              }
              else
              {
                  // For all other processes we have to return from this function
                  // So the lib's entry point must be called in an extra thread

                  th_attr.u32Priority  = 0;
                  th_attr.s32StackSize = minStackSize;
                  th_attr.pvArg   = (void*) pFunc;/*lint !e611 */
                  th_attr.szName  = pcorAttr->szAppName;
                  th_attr.pfEntry = OSAL_SharedLibEntryThread;
                  
                  if(OSAL_ThreadSpawn(&th_attr) != OSAL_ERROR){
                      u32ErrorCode   = OSAL_E_NOERROR;
                  }else{
                      NORMAL_M_ASSERT_ALWAYS();
                      u32ErrorCode = OSAL_u32ErrorCode();                  
                  }
              }
          }
      }
   } else {
       /* CFG3-1405 : Return Error if the file is not present.
       * To check for invalid parametrs passed.
       */ 
      pHandle = open( argv[0], O_RDONLY|O_CLOEXEC, 0 );
      if(pHandle < 0)
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
         vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
         TraceString("OSAL_ProcessSpawn Error - %d",u32ErrorCode);
         return s32ReturnValue;
      }

      close(pHandle);

      /*
       * look if we still have a slot for a process available.
       * the processes still race for a slot in bOnProcessAttach
       */
      if (pOsalData->ProcessTable.u32UsedEntries < pOsalData->ProcessTable.u32MaxEntries)
      {
          if(pOsalData->ProcessTable.u32UsedEntries == (pOsalData->ProcessTable.u32MaxEntries -1))
          {
              vWritePrintfErrmem("Max Count %d OSAL processes reached \n", 
                                 pOsalData->ProcessTable.u32UsedEntries);
          }
         s32Pid = fork();
      }
      else
      {
         errno = EAGAIN;
         u32ErrorCode = OSAL_E_NOSPACE;
         vWritePrintfErrmem("Wrong Configuration for OSAL processes \n");
      }
      if(s32Pid < 0 )
      {  /* fork failed */
          NORMAL_M_ASSERT_ALWAYS();
          u32ErrorCode = u32ConvertErrorCore(errno);
      }
      else
      {
         if (s32Pid > 0)
         {
             // Parent: fork() was successful; so update the error code for the caller (SPM)
             u32ErrorCode = OSAL_E_NOERROR;
         } else {
            /* bind process to cpu if in argument list*/
            if ( argc > 2)
            {
               if (OSAL_OK == strcmp(pcorAttr->szCommandLine, BINDCPU0))
               {
                  s32ProcessSetAffinity(s32Pid, CPU0);
               }
               else if (OSAL_OK == strcmp(pcorAttr->szCommandLine, BINDCPU1))
               {
                  s32ProcessSetAffinity(s32Pid, CPU1);
               }
            }

            // Set the cgroup of the process spawned
            // if the set of cgroup does not work the spawn will occure in the cgroup where the process was before
            // this would be the default behaviour
            if(pOsalData->u32CGroup)
            {
            //   vSetCgroup(pcorAttr->szCGroup_path, s32Pid);
            }

            vSetNiceLevel(pcorAttr);

	        /* try to execute the cmd in argv[0] in the child process */
            execvp (argv[0], argv);

            vWritePrintfErrmem("exec(%s) failed: %d Name %s Prio %d AppName %s CmdLine %s \n",
                               argv[0], errno, pcorAttr->szName, (unsigned int)pcorAttr->u32Priority,
                               pcorAttr->szAppName, pcorAttr->szCommandLine);
             NORMAL_M_ASSERT_ALWAYS();
             s32ReturnValue = OSAL_ERROR;
            /* the child program could not be started, stop the forked process */
            _exit(1);  /* avoid exit handler! */
         }
      }
   }

   // Free argument list
   u32Index = argc;
   while( (argv != NULL) &&
          (u32Index-- > 0))
   {
      if(argv[u32Index] != OSAL_NULL)
      {
         OSAL_vMemoryFree(argv[u32Index]);
         argv[u32Index] = OSAL_NULL;
      }
   }
   OSAL_vMemoryFree(argv);

   if(u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      TraceString("OSAL_ProcessSpawn Error:%d",u32ErrorCode);
   }
   else
   {
       if (u32OsalSTrace & 0x00000004)
       {
          TraceString("OSAL_ProcessSpawn Name:%s PID:%d",pcorAttr->szName,s32Pid);
       }
       s32ReturnValue = s32Pid;
   }
   
   return s32ReturnValue;
}

/******************************************************************************
* FUNCTION:      OSAL_s32ProcessDelete
*
* DESCRIPTION:   This Function terminate a process.All threads assigned to 
*                the process are terminated likewise. 
*
* PARAMETER:
*
*    OSAL_tProcessID   pid: Id of the process;
*
*
* RETURNVALUE:   s32ReturnValue
*                it is the function return value: 
*                   - OSAL_OK if everything goes right;
*                   - OSAL_ERROR otherwise.
*   
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ProcessDelete(OSAL_tProcessID pid)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   
   if (getpid() == pid)
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   else
   {
      s32ReturnValue = kill(pid, 0);
      if(s32ReturnValue == OSAL_ERROR)
      {
         u32ErrorCode = u32ConvertErrorCore(errno);
      }
   }
   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      TraceString("OSAL_ProcessDelete Error:%d",u32ErrorCode);
   }
   else
   {
       if (u32OsalSTrace & 0x00000004)
       {
          TraceString("OSAL_ProcessDelete PID:%d",pid);
       }
   }
   return( s32ReturnValue );
}


/******************************************************************************
* FUNCTION:      OSAL_ProcessWhoAmI
*
* DESCRIPTION:   This Function returns the process ID of the currently 
*                running process
*
* PARAMETER:     none
*
*
* RETURNVALUE:   Process ID
*   
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
OSAL_tProcessID OSAL_ProcessWhoAmI(tVoid)
{

static OSAL_tProcessID myPID=0; /* 0 is init process, can't be used as normal process ID */

 //  if (!myPID) {
       myPID=getpid();
 //  }
   return myPID;
}


tS32 OSAL_s32ProcessJoin(OSAL_tProcessID pid)
{
   tS32 s32Result = OSAL_OK;
 //  tS32 status;
   char Path[50];
   DIR* dirproc =0;

   snprintf(Path,50,"/proc/%d",pid);
   while(1)
   {
      if((dirproc = opendir(Path)) != NULL)
      {
         closedir(dirproc);
      }
      else
      {
          if(errno == ENOENT)
          {
             s32Result = OSAL_OK;
             break;
          }
          else
          {
             TraceString("Unexpected Error detected");
             s32Result = OSAL_ERROR;
             break;
          }
      }
      OSAL_s32ThreadWait(1000);
   }


#ifdef ONLY_CHILD
#ifndef NO_SIGNALAWARENESS
   while(1)
   {
#endif
      s32Result = waitpid(pid, &status, 0);
#ifndef NO_SIGNALAWARENESS
      if(s32Result != 0)
      {
         /* check for incoming signal */
         if(errno != EINTR)
         {
        //     FATAL_M_ASSERT_ALWAYS();
             s32Result = OSAL_ERROR;
         }
      }
      else
      {
         s32Result = OSAL_OK;
         break;
      }
   }
#endif
#endif
   if((s32Result == OSAL_ERROR)||(u32OsalSTrace & 0x00000004))
   {
      TraceString("OSAL_s32ProcessJoin PID:%d Result;%d",pid,s32Result);
   }
   return s32Result;
}

static tU32 u32PrcExitCode = 0;

void OSAL_vSetProcessExitCode(tU32 u32ExitCode)
{
    u32PrcExitCode = u32ExitCode;
}

tS32 s32RegisterExitFunction(OSALCALLBACKFUNC pFunc,tU32* arg)
{
   tS32 s32Ret = OSAL_ERROR;
   int i;
   for(i=0 ; i<5;i++)
   {
      if(pExitFunc[i] == NULL)
      {
         pExitFunc[i] = pFunc;
         pArg[i] = arg;
         if(LLD_bIsTraceActive(TR_COMP_OSALCORE, TR_LEVEL_DATA))
         {
             TraceString("s32RegisterExitFunction PID:%d Tid:%d 0x%x with %d",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),pFunc,pArg[i]);
         }
         s32Ret = OSAL_OK;
         break;
      }
   }
   if(i == 4)TraceString("s32RegisterExitFunction PID:%d Tid:%d 0x%x with %d failed",OSAL_ProcessWhoAmI(),OSAL_ThreadWhoAmI(),pFunc,pArg[i]);
   if((s32Ret == OSAL_ERROR)||(u32OsalSTrace & 0x00000004))
   {
      TraceString("s32RegisterExitFunction PID:%d Result;%d",OSAL_ProcessWhoAmI(),s32Ret);
   }
   return s32Ret;
}

void vExecutExitFunction(void)
{
   int i;
   for(i=0 ; i<5;i++)
   {
      if(pExitFunc[i] != NULL)
      {
         if(LLD_bIsTraceActive(TR_COMP_OSALCORE, TR_LEVEL_DATA))
         {
              TraceString("vExecutExitFunction PID:%d 0x%x with %d",OSAL_ProcessWhoAmI(),pExitFunc[i],pArg[i]);
         }
         pExitFunc[i](pArg[i]);
      }
   }
}

tS32 s32TriggerShutdownPrm(void)
{
  OSAL_tMQueueHandle hMq;
  trPrmMsg rMsg;
  tS32 s32Ret = OSAL_ERROR;
  rMsg.s32ResID = RES_ID_LAST;
  if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hMq ) != OSAL_ERROR )
  {  
     if(OSAL_s32MessageQueuePost( hMq, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 ) == OSAL_OK)
     {
        // s32Ret = OSAL_s32ThreadJoin(PrmRecThreadID,1000);
        OSAL_s32ThreadWait(200);
     }
     OSAL_s32MessageQueueClose(hMq);
  }
  return s32Ret;
}

/******************************************************************************
* FUNCTION:      OSAL_vProcessExit
* 
* DESCRIPTION:   This Function ends an ongoing process.All Threads are
*                terminated by the OS.
*
* PARAMETER:     none 
*
*
* RETURNVALUE:   none
*   
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tVoid OSAL_vProcessExit(tVoid)
{
   tS32 status = 0;
   tU32 n = 0;
   trThreadElement *pCurrentEntry = NULL;

   /*suppress starting additional worker threads */
   u32PrcExitStep = 1;
   OSAL_s32ThreadWait(0);

   tBool bTraceShutdown = LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_USER_4);
   if(u32OsalSTrace & 0x00000004)bTraceShutdown = TRUE;

   if(bTraceShutdown)TraceString("OSAL_vProcessExit Start Exit Handler");
   /* call registered exit function from other components */   
   vExecutExitFunction();

   if(bTraceShutdown)TraceString("OSAL_vProcessExit StopSupervisionTimer");
   StopSupervisionTimer();

   /* cleanup OSAL internal structure */
   if( (status = OSAL_ThreadWhoAmI() ) != OSAL_ERROR )
   {
      if(bTraceShutdown)TraceString("OSAL_vProcessExit shutdown for CB Hdr Task ");
       /* Trigger shutdown for CB Hdr Task */
      tOsalMbxMsg rMsg = {0};
      rMsg.rOsalMsg.Cmd = MBX_TERM_TSK;
      pCurrentEntry = tThreadTableSearchEntryByID(status,FALSE);
      if((s32LocalCbHdrTid != -1)&&(pCurrentEntry != NULL))
      {
         for (n = 0; n < pOsalData->u32MaxNrProcElements; n++)
         {
            if (prProcDat[n].u32Pid == (tU32)pCurrentEntry->pid)break;
         }
         if(n < pOsalData->u32MaxNrProcElements)
         {
            if(OSAL_s32MessageQueuePost(hMQCbPrc[n],(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
            {
                NORMAL_M_ASSERT_ALWAYS();
            }
         }
         OSAL_s32ThreadJoin(s32LocalCbHdrTid,1000);
      }
   
      /* If the main OSAL process terminates shutdown PRM and Helper task */
      TraceString("Main PID:%d",pOsalData->s32MainOsalPrcPid);
      if(pOsalData->s32MainOsalPrcPid == OSAL_ProcessWhoAmI())
      {
         if(bTraceShutdown)TraceString("OSAL_vProcessExit shutdown OSAL Main Process Tasks ");
         s32TriggerShutdownPrm();
         OSAL_s32ThreadJoin(pOsalData->s32PrmMainPrcId,1000);
         vStopDebugTask();
         OSAL_s32ThreadJoin(pOsalData->ThreadIDTerm,1000);
      }

      /* shutdown asynchronous IO subsystem if it run in the same context */
      if(bAIO == TRUE)
      {
         if(bTraceShutdown)TraceString("OSAL_vProcessExit shutdown OSAL Async Tasks ");
         s32ShutdownAsyncTask();
      }

      if(bTraceShutdown)TraceString("OSAL_vProcessExit start vUnLoadModulsForExit");
      /* Unload all manual loaded shared libraries before timer system is terminated */
      vUnLoadModulsForExit();

      /* now all OSAL external code is executed */
      u32PrcExitStep = 2;

      /* Trigger shutdown for Timer Task */
      if(s32LocalTimerTid != -1)
      {
         if(bTraceShutdown)TraceString("OSAL_vProcessExit shutdown Timer Task");
         union sigval value = {0};
         pCurrentEntry = tThreadTableSearchEntryByID(s32LocalTimerTid,FALSE);
         if(pCurrentEntry)
         {
            if(pthread_sigqueue(pCurrentEntry->tid,pOsalData->u32TimSignal-1,value) == -1)
            {
            }
         }
         OSAL_s32ThreadJoin(s32LocalTimerTid,1000);
      }

      if(LockOsal(pThreadLock) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableSearchEntryByID(status,TRUE);
         if(pCurrentEntry)
         {
 //            vCancelThreadsOfPrc(pCurrentEntry->pid);
            for (n = 0; n < pOsalData->u32MaxNrProcElements; n++)
            {
               if (prProcDat[n].u32Pid == (tU32)pCurrentEntry->pid)
               {
                  TraceString("Processs %s (%d)terminates",prProcDat[n].pu8CommandLine,pCurrentEntry->pid);
                  vEnterProcessToVolatile(prProcDat[n].u32Pid ,FALSE);
                  prProcDat[n].u32Pid = 0;
                  prProcDat[n].u32Tid = 0;
               }
            }
      //     n = s32CleanupThreadTable(pCurrentEntry->pid);
             s32CleanupThreadTable(pCurrentEntry->pid); 
         }
         UnLockOsal( pThreadLock );
         if(n > 1)
         {
       //     TraceString("OSAL_vProcessExit ThreadTable Cleanup done for %d Threads",n);
         }
      }
   }
   pOsalData->u32PrcResCount--;

   /* stop generating Pseudo TCB for this process in a case on an error*/
   bMainTbcEntered = FALSE;

   /* cleanup all other OSAL resources including global objects */
   vOnProcessDetach();

   _exit(u32PrcExitCode);
}

/******************************************************************************
* FUNCTION:      OSAL_s32ProcessList
*
* DESCRIPTION:   This Function returns all processes registered in the system
*
* PARAMETER:
*
*                tS32 pas32List[] list of process ID;
*                tS32 s32Length   maximal size of the list
*
*
* RETURNVALUE:   number of IDs in the list 
*   
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ProcessList(tS32 pas32List[],tS32 s32Length)
{
   tU32 n;
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;

   if( (pas32List != NULL) && (s32Length > 0) )
   {
      s32ReturnValue = 0;
      if( LockOsal( &pOsalData->ProcessTable.rLock ) == OSAL_OK )
      {

         for (n = 0; n< (tU32)s32Length && n <pOsalData->u32MaxNrProcElements; n++)
         {
            if (prProcDat[n].u32Pid || prProcDat[n].u32Tid)
            {
               pas32List[n] = prProcDat[n].u32Pid;
               s32ReturnValue++;
            }
         }
         UnLockOsal( &pOsalData->ProcessTable.rLock );
      }
      u32ErrorCode = OSAL_E_NOERROR;
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   
   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      TraceString("OSAL_s32ProcessList PID:%d Error:%d",getpid(),u32ErrorCode);
   }
   else
   {
      if(u32OsalSTrace & 0x00000004)
      {
         TraceString("OSAL_s32ProcessList PID:%d Error:%d",OSAL_ProcessWhoAmI(),u32ErrorCode);
      }
   }
   return s32ReturnValue;
}


/******************************************************************************
* FUNCTION:      OSAL_s32ProcessControlBlock
*
* DESCRIPTION:   This Function returns a process control block
*
* PARAMETER:
*
*   OSAL_tProcessID             pid  : Id of the process;
*   OSAL_trProcessControlBlock  prPcb: Pointer to control block structure;
*
* RETURNVALUE:   s32ReturnValue
*                it is the function return value: 
*                   - OSAL_OK if everything goes right;
*                   - OSAL_ERROR otherwise.
*   
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ProcessControlBlock(OSAL_tProcessID pid, OSAL_trProcessControlBlock* prPcb)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   tU32 n;

   if( (pid >= 0) && (prPcb) )
   {
      if (LockOsal(&pOsalData->ProcessTable.rLock) == OSAL_OK)
      {
         for (n = 0; n< pOsalData->u32MaxNrProcElements && prProcDat[n].u32Pid != (tU32)pid; n++)
            ;
         if (n < pOsalData->u32MaxNrProcElements )
         {
            prPcb->id = prProcDat[n].u32Pid;
            prPcb->szName = prProcDat[n].pu8AppName;
            prPcb->runningTime = 0;
            s32ReturnValue = OSAL_OK;
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGPROCESS;
         }
         UnLockOsal(&pOsalData->ProcessTable.rLock);
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   
   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      TraceString("OSAL_s32ProcessControlBlock Error:%d",u32ErrorCode);
   }
   else
   {
      if(u32OsalSTrace & 0x00000004)
      {
         TraceString("OSAL_s32ProcessControlBlock PID:%d ",OSAL_ProcessWhoAmI());
      }
   }

   return s32ReturnValue;
}


tU32 u32SetRealtimePriority(trThreadElement *pCurrentEntry, tS32 s32Prio)
{
   tU32 u32Error = OSAL_E_NOERROR;
   // struct sched_param is used to store the scheduling priority
   struct sched_param params;
   // We'll set the priority to the maximum.
   params.sched_priority = sched_get_priority_max(SCHED_FIFO);

   if(s32Prio <= params.sched_priority)
   {
      params.sched_priority = s32Prio;
      // Attempt to set thread real-time priority to the SCHED_FIFO policy
      if(pthread_setschedparam(pCurrentEntry->tid, SCHED_FIFO, &params) != 0)
      {
          TraceString("OSAL_s32ThreadPriority Set RT Prio for %s failed errno:%d",pCurrentEntry->szName,errno);
          u32Error = u32ConvertErrorCore( errno );
      }
      else
      {
         pCurrentEntry->u32Priority = s32Prio;
         if(LLD_bIsTraceActive( OSAL_C_TR_CLASS_SYS_PRC, TR_LEVEL_COMPONENT)||(u32OsalSTrace & 0x00000004))
         {
            TraceString("OSAL_s32ThreadPriority Set RT Prio %d for %s(%d)",s32Prio,pCurrentEntry->szName,s32Prio,pCurrentEntry->kernel_tid);
         }
      }
   }
   else
   {
       TraceString("Max Scheduling Prio:%d for %s requested Prio:%d",params.sched_priority,"SCHED_FIFO",s32Prio);
       u32Error = OSAL_E_INVALIDVALUE;
   }
   return u32Error;
}


/******************************************************************************
* FUNCTION:      OSAL_ThreadCreate
*
* DESCRIPTION:   creates an OSAL Thread
*
* PARAMETER:
*
*   OSAL_trThreadAttribute*     pointer to a record that contains
*                               the attributes of new thread.
*
* RETURNVALUE:   
*   
*   OSAL_tThreadID : id of the Thread or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
OSAL_tThreadID OSAL_ThreadCreate(const OSAL_trThreadAttribute* pcorAttr)
{  
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR; 
   trThreadElement *pCurrentEntry = NULL;
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];

   if((pcorAttr == NULL)
    ||((RealTimPrioActive)&&((tS32)pcorAttr->u32Priority < 0 )&&(pcorAttr->u32Priority > 99)))   /*Check priority*/
   {
       u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   
   /* Check Parameters */
   if((u32ErrorCode == OSAL_E_NOERROR)
	&&(pcorAttr)
	&&(pcorAttr->szName)                                               /*Check Name */
    &&(pcorAttr->pfEntry != NULL))                                    
//      && (pcorAttr->s32StackSize > 1)   /* given stack size is not evaluated by  */
    {
      if (OSAL_u32StringLength((char*)pcorAttr->szName) <= (tU32)LINUX_C_THREAD_MAX_NAMELENGTH)
      {
         if(LockOsal(pThreadLock) == OSAL_OK)
         {       
            /*Check if OSAL_NAME already exists*/
            if(tThreadTableSearchEntryByName((tString)pcorAttr->szName)== OSAL_NULL)
            {
               /*Check if an entry in the thread table is free or add new entries*/
               pCurrentEntry = tThreadTableGetFreeEntry();
               if(pCurrentEntry != OSAL_NULL)
               {
                  tS32 stacksize;
                  pthread_mutexattr_t mutex_attr;
                  pthread_attr_t      thread_attr;

                  u32ErrorCode = OSAL_E_UNKNOWN;
                  //  OSAL_pvMemorySet(pCurrentEntry, 0, sizeof(trThreadElement));
                  //pCurrentEntry->u32Priority    = 37;// pcorAttr->u32Priority;
                  pCurrentEntry->u32Priority    = 0;// pcorAttr->u32Priority;
                  pCurrentEntry->pvArg          = (tPVoid*)pcorAttr->pvArg; 
                  pCurrentEntry->pfOsalEntry    = pcorAttr->pfEntry;
                  pCurrentEntry->pfTaskEntry    = vWrapperEntry;
                  (void)OSAL_szStringNCopy((tString)pCurrentEntry->szName,
                                             pcorAttr->szName,
                                             OSAL_C_U32_MAX_NAMELENGTH);
                  pthread_mutexattr_init(&mutex_attr);
                  pthread_mutexattr_settype( &mutex_attr, PTHREAD_MUTEX_RECURSIVE );

                  if(pthread_mutex_init(&pCurrentEntry->hMutex, &mutex_attr) != OSAL_OK)
                  {
                     NORMAL_M_ASSERT_ALWAYS();
                  }
                  pthread_mutexattr_destroy(&mutex_attr);

                  if(pthread_cond_init(&pCurrentEntry->cond,NULL) != OSAL_OK)
                  {
                     NORMAL_M_ASSERT_ALWAYS();
                  }
                  if (sem_init(&pCurrentEntry->hSemHandle,0,0) != 0)
                  {
                     NORMAL_M_ASSERT_ALWAYS();
                  }
                  if (sem_init(&pCurrentEntry->hTempSemHandle,0,0) != 0)
                  {
                         NORMAL_M_ASSERT_ALWAYS();
                  }
                  pthread_attr_init(&thread_attr );
                  {
                     sched_param priority;

                     pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
                     pthread_attr_setscope(&thread_attr, PTHREAD_SCOPE_SYSTEM);
//                   pthread_attr_setschedpolicy(&thread_attr,SCHED_RR);
                     priority.sched_priority = pCurrentEntry->u32Priority;
                     pthread_attr_setschedparam(&thread_attr, &priority);//needed for SCHED_FIFO or SCHED_RR
                     pthread_attr_setschedpolicy(&thread_attr,SCHED_OTHER);
                  }
                  ////printf("detached\n");
#ifdef  OSAL_PTHREAD_CREATE_JOINABLE
                  pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
#else
                  pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_DETACHED);
#endif
                  stacksize = pcorAttr->s32StackSize;
                  if (stacksize < (tS32)minStackSize)
                  {
                      stacksize = minStackSize;
                  }
                  if (pthread_attr_setstacksize(&thread_attr, stacksize) != 0)
                  {
                     TraceString("pthread_attr_setstacksize failed with MinOsal:%d MinLinux:%d %d",minStackSize,PTHREAD_STACK_MIN,errno);
                     s32ReturnValue = minStackSize;
                     // NORMAL_M_ASSERT_ALWAYS();
                  }

                  if( pthread_create( &pCurrentEntry->tid,
                                      &thread_attr,
                                      vWrapperEntry,
                                      (void*)pCurrentEntry ) == 0 )
                  {
                      u32ErrorCode   = OSAL_E_NOERROR;
                      pCurrentEntry->pid = OSAL_ProcessWhoAmI();
                      /* Set Error Management Variables */
                      pCurrentEntry->u32ErrorCode = OSAL_E_NOERROR;
                      pCurrentEntry->u32ErrorHooksCalled = 0;
                      #ifdef STANDARD_ERROR_HOOK
                      pCurrentEntry->pfErrorHook = &vErrorHookFunc;
                      #else
                      pCurrentEntry->pfErrorHook = (OSAL_tpfErrorHook)OSAL_NULL;
                      #endif
                      /* Set Auxiliary s32Status Flags */
                      pCurrentEntry->bIsSetReadyStatus     = FALSE;
                      pCurrentEntry->bIsSetSuspendedStatus = FALSE;
                      pCurrentEntry->bMarkedDeleted        = FALSE;
                      pCurrentEntry->bIsUsed               = TRUE;
                      if(pthread_attr_getstacksize(&thread_attr, (size_t*)&stacksize))
                      {
                          NORMAL_M_ASSERT_ALWAYS();
                      }
                      else
                      {
                         if(s32ReturnValue == (tS32)minStackSize)
                         {
                            TraceString("Thread created with StackSize :%d",stacksize);
                         }
                      }
                      pCurrentEntry->u32TaskStack   = stacksize;
                      /* now wait until unique kernel_tid is entered ,
                         therefore new task has to run */
                      if(u32SemWait(&pCurrentEntry->hTempSemHandle) != OSAL_E_NOERROR)
                      {
                         FATAL_M_ASSERT_ALWAYS();
                      }

                      sem_destroy(&pCurrentEntry->hTempSemHandle);

                      /* No errors during execution it returns unique ThreadID */ 
                      s32ReturnValue = pCurrentEntry->kernel_tid;
#ifdef RBTREE_SEARCH
                      vInsertElement(prRBTree,pCurrentEntry->kernel_tid,pCurrentEntry->s32Index);
#endif
                      if(RealTimPrioActive == TRUE)
                      {
                         u32SetRealtimePriority(pCurrentEntry,pcorAttr->u32Priority); 
                      }

                      pOsalData->u32TskResCount++;
                      if(pOsalData->u32MaxTskResCount < pOsalData->u32TskResCount)
                      {
                         pOsalData->u32MaxTskResCount = pOsalData->u32TskResCount;
                         if(pOsalData->u32MaxTskResCount > (pOsalData->u32MaxNrThreadElements*9/10))
                         {
                           pOsalData->u32NrOfChanges++;
                         }
                      }
                  }
                  else
                  {
                     u32ErrorCode = u32ConvertErrorCore(errno);
                     FATAL_M_ASSERT_ALWAYS();
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
                  NORMAL_M_ASSERT_ALWAYS();
               }
           }
           else
           {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
           }
           UnLockOsal(pThreadLock);
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
      
   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pcorAttr)
      {
         snprintf(&au8Buf[1],250,"ThreadCreate Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,pcorAttr->szName,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"ThreadCreate Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",(unsigned int)u32ErrorCode);
      }
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      /* OSAL_E_ALREADYEXISTS can happen for CB_HDR tasks when race condition occurs */
      if((pOsalData->bLogError)&&(u32ErrorCode != OSAL_E_ALREADYEXISTS))
      {
         vWriteToErrMem(TR_COMP_OSALCORE,au8Buf,strlen(&au8Buf[1])+1,0);
      }
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT )||(u32OsalSTrace & 0x00000004))
      {
         au8Buf[0] = OSAL_STRING_OUT;
         tU32 u32Temp = OSAL_ThreadWhoAmI();
         bGetThreadNameForTID(&name[0],8,(tS32)u32Temp);
         snprintf(&au8Buf[1],250,"ThreadCreate Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pcorAttr->szName,(pCurrentEntry->kernel_tid),(unsigned int)u32ErrorCode);/*lint !e613 */
         LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      }
   }
   return (s32ReturnValue);
}


/******************************************************************************
* FUNCTION:      OSAL_s32ThreadActivate()
*
* DESCRIPTION:   This function activates a Thread generated earlier through 
*                OSAL_ThreadCreate i.e. the state of the thread change in 
*                ready.
*
*
* PARAMETER:
*
*    OSAL_tThreadID tThreadId        Thread ID of the generated thread   
*
*
* RETURNVALUE:   s32ReturnValue
*                   it is the function return value: 
*                   - OSAL_OK if everything goes right;
*                   - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadActivate( OSAL_tThreadID tid )
{
   /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trThreadElement *pCurrentEntry = NULL;
   char au8Buf[251];
   char name[TRACE_NAME_SIZE];

   /* Check Parameters          */
   if( (tid != 0) && (tid != OSAL_ERROR) )    /* Check ThreadID Boundaries */
   {
      if(LockOsal( pThreadLock ) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableSearchEntryByID( tid,TRUE);
         if( (pCurrentEntry != OSAL_NULL)  /* check if the thread entry exists */
             && (pCurrentEntry->bIsSetReadyStatus == FALSE) ) /* Check if the Thread is in "Initialized" status */
         {
            if(sem_post(&pCurrentEntry->hSemHandle) == 0) 
            {
               /* Update table entry records                */
               pCurrentEntry->tStartTime = OSAL_ClockGetElapsedTime();
               pCurrentEntry->bIsSetReadyStatus = TRUE;

               /* No errors during execution it returns OSAL_OK */
               s32ReturnValue = OSAL_OK;
            }
            else
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
               //u32ErrorCode = OSAL_E_UNKNOWN; 
               FATAL_M_ASSERT_ALWAYS();
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGTHREAD;
         }// if  (pCurrentEntry != OSAL_NULL && pCurrentEntry->bIsSetReadyStatus == FALSE)

         UnLockOsal(pThreadLock);
      }
   }  
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   } // if(tid >= OSAL_OK) 

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      tU32 u32Level = TR_LEVEL_ERROR;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&au8Buf[1],250,"ThreadActivate Task:%s(%d) for Task:%s failed Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"ThreadActivate Task:%s(%d) for Task:%s failed Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",(unsigned int)u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000004)u32Level = TR_LEVEL_FATAL;
      
      LLD_vTrace(TR_COMP_OSALCORE, u32Level,au8Buf,strlen(&au8Buf[1])+1);
      if(pOsalData->bLogError)
      {
         vWriteToErrMem(TR_COMP_OSALCORE,au8Buf,strlen(&au8Buf[1])+1,0);
      }
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT )||(u32OsalSTrace & 0x00000004))
      {
         au8Buf[0] = OSAL_STRING_OUT;
         tU32 u32Temp = OSAL_ThreadWhoAmI();
         bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
         snprintf(&au8Buf[1],250,"ThreadActivate Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,(pCurrentEntry->kernel_tid),(unsigned int)u32ErrorCode);/*lint !e613 when u32ErrorCode == OSAL_E_NOERROR valid pCurrentEntry*/
         LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      }
   }

   return(s32ReturnValue);
}


/******************************************************************************
* FUNCTION:      OSAL_ThreadSpawn
*
* DESCRIPTION:   This service is a wrapper function to 
*                OSAL_ThreadCreate_Cntxt().
* 
* PARAMETER:
*
*   OSAL_trThreadAttribute*     pointer to a record that contains
*                               the attributes of new thread
*
*
* RETURNVALUE:
*
*   OSAL_tThreadID : id of the Thread or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
OSAL_tThreadID OSAL_ThreadSpawn(const OSAL_trThreadAttribute* pcorAttr)
{
   /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   OSAL_tThreadID tid;

   /* Create Thread */
   if((tid = OSAL_ThreadCreate(pcorAttr)) != OSAL_ERROR)
   {
      /* Activate Thread */
      if(OSAL_s32ThreadActivate(tid) != OSAL_ERROR)
      {
         s32ReturnValue = tid;
      }
   }

   return (s32ReturnValue);
}


/******************************************************************************
* FUNCTION:      OSAL_s32ThreadDelete()
*
* DESCRIPTION:   Delete a thread and release the stack again 
*
*
* PARAMETER:
*
*   OSAL_tThreadID tThreadId     Thread ID of the thread to be delete   
*
*
* RETURNVALUE:   
*
*   OSAL_OK or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadDelete( OSAL_tThreadID tid )
{
   /*Define Local Variables */
   tS32             s32ReturnValue = OSAL_ERROR;
   tU32             u32ErrorCode   = OSAL_E_NOERROR;
   OSAL_tThreadID   currtid = OSAL_ThreadWhoAmI();
   trThreadElement *pCurrentEntry = NULL; 
   char name[TRACE_NAME_SIZE];


   /* 1. Check Parameters                                      */
   if( ( tid != 0 ) && (tid != OSAL_ERROR) )                  /* Check ThreadID Boundaries */ 
   {
      if( LockOsal( pThreadLock ) == OSAL_OK )
      {
         pCurrentEntry = tThreadTableSearchEntryByID( tid,TRUE );
         /* check if the thread entry exists */
         if( (pCurrentEntry != OSAL_NULL)
               && ((OSAL_tThreadID)pCurrentEntry->kernel_tid != currtid)  // delete only other threads!
               && !pCurrentEntry->bMarkedDeleted
               )
         {
            pCurrentEntry->bMarkedDeleted = TRUE;
            /* this only checks if the thread still exists */
            if( pthread_kill((pthread_t)pCurrentEntry->tid, 0) == 0 )
            {
               pCurrentEntry->bMarkedDeleted = TRUE;
               #if 0
               if ( 0 != pthread_cancel( (pthread_t)pCurrentEntry->tid ))
               {
                  u32ErrorCode = OSAL_E_WRONGTHREAD;
               }
               else
               {
               #endif
                  if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT ) )
                  {
                     tU32 u32Temp = OSAL_ThreadWhoAmI();
                     bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
                     TraceString("ThreadDelete Task:%s(%d) deletes Task:%d Error:0x%x ", 
                                 name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry->kernel_tid,(unsigned int)u32ErrorCode);
                  }
                  clearThreadEntry(pCurrentEntry);
                  s32ReturnValue = OSAL_OK;
//               }
            }
            else
            {
                  if(errno == ESRCH)
                  {
                     u32ErrorCode = OSAL_E_WRONGTHREAD;
                     clearThreadEntry(pCurrentEntry);
                  }
                  else
                  {
                     u32ErrorCode = u32ConvertErrorCore( errno );
                  }
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGTHREAD;
  //       TraceString( "OSAL_s32ThreadDelete handle:%d ",pCurrentEntry);
         }
         UnLockOsal( pThreadLock );
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   } // if(tid > 0)

   if((u32ErrorCode != OSAL_E_NOERROR )||(u32OsalSTrace & 0x00000004))
   {
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         TraceString("ThreadDelete Task:%s(%d) deletes Task:%d Error:0x%x ", 
                     name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry->kernel_tid,(unsigned int)u32ErrorCode);
      }
      else
      {
         TraceString("ThreadDelete Task:%s(%d) deletes Task:%d Error:0x%x ", 
                      name,(unsigned int)u32Temp,(unsigned int)0,(unsigned int)u32ErrorCode);
      }
      if(u32ErrorCode != OSAL_E_NOERROR )vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   return( s32ReturnValue );
}


/******************************************************************************
* FUNCTION:      OSAL_vThreadExit()
*
* DESCRIPTION:   This function closes the current thread (Return).
*                The stack is released
*
* PARAMETER:     none
*
*
* RETURNVALUE:   none 
*
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tVoid OSAL_vThreadExit (tVoid)
{
   /*Define Local Variables */
   OSAL_tThreadID    tid = OSAL_ThreadWhoAmI();
   trThreadElement*  pCurrentEntry = NULL;

   if(LockOsal(pThreadLock) == OSAL_OK)
   {
      /* Identifies the calling Thread */
      if( tid != OSAL_ERROR )
      {
         pCurrentEntry = tThreadTableSearchEntryByID(tid,TRUE);
      }
      /* check if the thread entry exists */
      if(pCurrentEntry != OSAL_NULL)
      {
         if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT ) )
         {
            TraceString("Task ID %d Name %s is DEAD",tid,pCurrentEntry->szName);
         }
         clearThreadEntry(pCurrentEntry);
      }
      else
      {
         TraceString("Unregistered Task %d is DEAD",tid);
      }
      UnLockOsal(pThreadLock);
   }

   pthread_exit(NULL);
}

/******************************************************************************
* FUNCTION:      OSAL_s32ThreadSuspend()
*
* DESCRIPTION:   This function suspend the Thread specified as parameter.
*                The suspending is additive i.e. a Thread in "delayed" or
*                "pended" s32Status gets an additional suspended-Attribute.
*                If the thread has filled certain resuorces e.g for inter
*                process communication, then they are not released through
*                the function. Here under circumstances a Deadlock-Situation
*                may be formed.
*
* PARAMETER:
*
*   OSAL_tThreadID tThreadId      Thread ID
*
*
* RETURNVALUE:   s32ReturnValue
*                   it is the function return value: 
*                   - OSAL_OK if everything goes right;
*                   - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadSuspend(OSAL_tThreadID tid)
{
   /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trThreadElement *pCurrentEntry = NULL;
   OSAL_tThreadID currtid = OSAL_ThreadWhoAmI();
   char au8Buf[251];
   char name[TRACE_NAME_SIZE];


   /* Check Parameters */
   if( ( tid != 0 ) && (tid != OSAL_ERROR))   /* Check ThreadID Boundaries */
   {
      if(tid != currtid)
      {
         u32ErrorCode = OSAL_E_WRONGTHREAD;
         TraceString("Task %d try supend Task %d -> not allowed",currtid,tid);     
      }
      else
      {
         if(LockOsal(pThreadLock) == OSAL_OK)
         {
            pCurrentEntry = tThreadTableSearchEntryByID(tid,TRUE);

            UnLockOsal(pThreadLock);

            /* check if the thread entry exists and is not "Deleted" - OEDT test 012 depends on this */
            if(pCurrentEntry != OSAL_NULL && !pCurrentEntry->bMarkedDeleted)
            {
               if(0 == pthread_mutex_lock( &pCurrentEntry->hMutex ))
               {
                  pCurrentEntry->bIsSetSuspendedStatus = TRUE;
                  if(pthread_cond_wait(&pCurrentEntry->cond, &pCurrentEntry->hMutex) == OSAL_OK)
                  {
                      /* No errors during execution it returns OSAL_OK */
                      s32ReturnValue = OSAL_OK;
                  }
                  if(0 != pthread_mutex_unlock( &pCurrentEntry->hMutex ))
                  {
                     u32ErrorCode = u32ConvertErrorCore(errno);
                  }
               }
               else
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_WRONGTHREAD;
            }
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32ErrorCode != OSAL_E_NOERROR))
   {
      s32ReturnValue = OSAL_ERROR;
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      tU32 u32Level = TR_LEVEL_ERROR;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&au8Buf[1],250,"ThreadSuspend Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,tid,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"ThreadSuspend Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",tid,(unsigned int)u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000004)u32Level = TR_LEVEL_FATAL;
      
      LLD_vTrace(TR_COMP_OSALCORE, u32Level,au8Buf,strlen(&au8Buf[1])+1);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT )||(u32OsalSTrace & 0x00000004))
      {
         au8Buf[0] = OSAL_STRING_OUT;
          tU32 u32Temp = OSAL_ThreadWhoAmI();
         bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
         TraceString("ThreadSuspend Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                     name,(unsigned int)u32Temp,pCurrentEntry->szName,tid,(unsigned int)u32ErrorCode);/*lint !e613 */
      }
   }
   return(s32ReturnValue);
}

/******************************************************************************
* FUNCTION:      OSAL_s32ThreadResume()
*
* DESCRIPTION:   In This function the Thread is reset from the "suspended" 
*            status. A possible "delayed" or "pended" 
*                                       status is retained.
* PARAMETER:
*
*  OSAL_tThreadID tThreadId       Thread ID
*
*
* RETURNVALUE:   
*
*   OSAL_OK or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadResume(OSAL_tThreadID tid)
{
   /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trThreadElement *pCurrentEntry = NULL;
   char au8Buf[251];
   char name[TRACE_NAME_SIZE];

   /* Check Parameters */
   if( ( tid != 0 ) && (tid != OSAL_ERROR) )  /* Check ThreadID Boundaries */
   {
      /* Lock the thread table */
      if(LockOsal(pThreadLock) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableSearchEntryByID( tid ,TRUE);
         /* check if the thread entry exists */
         if((pCurrentEntry != OSAL_NULL) && (pCurrentEntry->bIsSetSuspendedStatus))
         {
            if(0 == pthread_mutex_lock( &pCurrentEntry->hMutex ))
            {
               if(pthread_cond_signal(&pCurrentEntry->cond) == OSAL_OK)
               {
                  /*
                   * This operation is done to communicate to OSAL that the Thread is
                   * resumed
                   */
                  pCurrentEntry->bIsSetSuspendedStatus = FALSE; 

                  /* No errors during execution it returns OSAL_OK */
                  s32ReturnValue = OSAL_OK;
               }
            }
            if(0 != pthread_mutex_unlock( &pCurrentEntry->hMutex ))
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGTHREAD;
         }
         /* UnLock the thread table */
         UnLockOsal(pThreadLock);
      }  
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32ErrorCode != OSAL_E_NOERROR))
   {
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      tU32 u32Level = TR_LEVEL_ERROR;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&au8Buf[1],250,"ThreadResume Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,tid,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"ThreadResume Task:%s(%d) for Task:%s(%d)  Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",tid,(unsigned int)u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000004)u32Level = TR_LEVEL_FATAL;
      
      LLD_vTrace(TR_COMP_OSALCORE, u32Level,au8Buf,strlen(&au8Buf[1])+1);
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
   else
   {
      if( LLD_bIsTraceActive( TR_COMP_OSALCORE, TR_LEVEL_COMPONENT )||(u32OsalSTrace & 0x00000004))
      {
         au8Buf[0] = OSAL_STRING_OUT;
         tU32 u32Temp = OSAL_ThreadWhoAmI();
         bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
         TraceString("ThreadResume Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                     name,(unsigned int)u32Temp,"Unknown",pCurrentEntry->kernel_tid,(unsigned int)u32ErrorCode);/*lint !e613 */
      }
   }

   return(s32ReturnValue); 
}

/******************************************************************************
* FUNCTION:      OSAL_s32ThreadWait()
*
* DESCRIPTION:   This function suspended the calling thread for a period 
*                delayed status.
*
* PARAMETER:
*
*   OSAL_tMSecond   msec        Time interval in ms
*
*
* RETURNVALUE:   
*
*   OSAL_OK or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadWait( OSAL_tMSecond msec )
{  
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(msec == 0)
   {
       pthread_yield();
       return OSAL_OK;
   }
   if( msec > 0 )
   {
      if (msec == OSAL_C_U32_INFINITE)
      {
        sleep (OSAL_C_U32_INFINITE);
      }
      else
      {
         if(msec < pOsalData->u32TimerResolution)
         {
           u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
         else
         {
             struct timespec tim = {0,0};
             struct timespec remain;
            if(!clock_gettime(CLOCK_REALTIME, &tim))
            {
#ifndef NO_SIGNALAWARENESS
            while(1)
            {
#endif
               tim.tv_sec = msec / 1000 ;
               tim.tv_nsec = (msec%1000)*1000000;
               if(tim.tv_nsec >= 1000000000)
               {
                  tim.tv_sec += 1;
                  tim.tv_nsec = tim.tv_nsec - 1000000000;
               }
               s32ReturnValue = nanosleep(&tim, &remain);
#ifndef NO_SIGNALAWARENESS
               if(s32ReturnValue == 0)
               {
                  s32ReturnValue = OSAL_OK;
                  break;
               }
               else if(errno != EINTR)
               {
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  break;
               }
               /* We are here because of signal. lets try again. */
            }
        }// end while
#endif
         }
      }
   }

   if(u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF,u32ErrorCode);
   }
   return s32ReturnValue;
}


/******************************************************************************
 * FUNCTION:      OSAL_ThreadWhoAmI()
 *
 * DESCRIPTION:  This function returns the threadID for the current Thread.
 *               Service contains no multitasking protection.
 *
 * PARAMETER:
 *
 *   nil
 *
 *
 * RETURNVALUE:   
 *
 *   Thread ID or OSAL_ERROR
 *
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
OSAL_tThreadID OSAL_ThreadWhoAmI(tVoid)
{   
 //  return (OSAL_tThreadID)pthread_self();
    return syscall(SYS_gettid);
}

/******************************************************************************
* FUNCTION:      OSAL_s32ThreadPriority()
*
* DESCRIPTION:  This function changes the priority of a thread.The process 
*               priority is retained.
*
* PARAMETER:
*
*   OSAL_tThreadID tThreadId     Thread ID of the thread
*   tU32           u32Priority      new priority;
*
* RETURNVALUE:   
*
*  OSAL_OK or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadPriority( OSAL_tThreadID tid, tU32 u32Priority )
{
   /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   tS32 s32Prio = (tS32)u32Priority;
   char Buffer[251];

   trThreadElement *pCurrentEntry = NULL;
   /* Lock the thread table */
   if( LockOsal( pThreadLock ) == OSAL_OK )
   {
         pCurrentEntry = tThreadTableSearchEntryByID(tid,TRUE);
         /* check if the thread entry exists */
         if(pCurrentEntry != OSAL_NULL && !pCurrentEntry->bMarkedDeleted)
         {
            if(pOsalData->u32Prio >= 1)
            {
               /* check for setting nice level */
               if((s32Prio <= 19)&&(s32Prio >= -20))
               {
                  /* setting nice level for myself I have to use helper task */				   
                  if(tid == OSAL_ThreadWhoAmI())
                  {
                     OSAL_tMQueueHandle  hTermMq = OSAL_C_INVALID_HANDLE;
                     tOsalMbxMsg rMsg;
                     rMsg.rOsalMsg.Cmd  = MBX_CB_TSK_RENICE;
                     rMsg.rOsalMsg.ID   = tid;
                     rMsg.rOsalMsg.Res1 = (tU32)s32Prio;
                     if(OSAL_s32MessageQueueOpen(OSAL_TERM_LI_MQ,OSAL_EN_WRITEONLY,&hTermMq) == OSAL_OK)
                     {
                       // TraceString("OSAL_s32MessageQueueOpen OSAL_TERM_LI_MQ");
                        if(OSAL_s32MessageQueuePost(hTermMq,(tPCU8)&rMsg,sizeof(tOsalMbxMsg),0) == OSAL_OK)
                        {
                           TraceString("OSAL_s32ThreadPriority forward renice prio:%d  tid:%d",s32Prio,tid);
                           OSAL_s32ThreadWait(0);
                           pCurrentEntry->u32Priority = u32Priority;
                           s32ReturnValue = OSAL_OK;
                        }
                        OSAL_s32MessageQueueClose(hTermMq);
                     }
                     else
                     {
                         TraceString("OSAL_s32ThreadPriority OSAL_s32MessageQueueOpen OSAL_TERM_LI_MQ failed");
                     }
                  }
                  else
                  {
                     s32Prio = (tS32)u32Priority;
                     if(LLD_bIsTraceActive( OSAL_C_TR_CLASS_SYS_PRC, TR_LEVEL_COMPONENT)||(u32OsalSTrace & 0x00000004))
                     {
                         TraceString("OSAL_s32ThreadPriority renice -n %d %d",s32Prio,tid);
                     }
                     snprintf(Buffer,100,"renice -n %d %d \n",s32Prio,tid);
                     system(Buffer);
                     pCurrentEntry->u32Priority = u32Priority;
                     s32ReturnValue = OSAL_OK;
                  }
               }
               else
               {
                  /* no nice level requested check for valid values*/ 
                  if(pOsalData->u32Prio == 1)
                  {
                     if(pOsalData->s32MainOsalPrcPid == OSAL_ProcessWhoAmI())
                     {
                        if((u32Priority <= (tU32)-20)||(u32Priority > OSAL_C_U32_THREAD_PRIORITY_HIGHEST))
                        {
                           u32ErrorCode = OSAL_E_INVALIDVALUE;
                           s32ReturnValue = OSAL_ERROR;
                        }
                        else
                        {  
                           if((u32ErrorCode = u32SetRealtimePriority(pCurrentEntry,u32Priority)) == OSAL_E_NOERROR)
                           {
                               s32ReturnValue = OSAL_OK;
                           }
                        }
                     }
                     else
                     {
                           u32ErrorCode = OSAL_E_INVALIDVALUE;
                           s32ReturnValue = OSAL_OK;// to have no different behaviour for applications
                     }
                  }
                  else if(pOsalData->u32Prio == 2)
                  {
                     if((u32Priority <= (tU32)-20)||(u32Priority > OSAL_C_U32_THREAD_PRIORITY_HIGHEST))
                     {
                        u32ErrorCode = OSAL_E_INVALIDVALUE;
                        s32ReturnValue = OSAL_ERROR;
                     }
                     else
                     {  
                        if((u32ErrorCode = u32SetRealtimePriority(pCurrentEntry,u32Priority)) == OSAL_E_NOERROR)
                        {
                           s32ReturnValue = OSAL_OK;
                        }
                     }
                  }
                  else
                  {
                     TraceString("No Realtime Prio Setting is configured");
                     s32ReturnValue = OSAL_OK;
                  }
               }
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGTHREAD;
         }
         /* UnLock the thread table */
         UnLockOsal( pThreadLock );
   }

   if((u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000004))
   {
      char name[TRACE_NAME_SIZE];
      OSAL_M_INSERT_T8(&Buffer[0],OSAL_STRING_OUT);
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      tU32 u32Level = TR_LEVEL_ERROR;
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&Buffer[1],250,"SetThreadPriority Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,pCurrentEntry->kernel_tid,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&Buffer[1],250,"SetThreadPriority Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",(unsigned int)u32ErrorCode);
      }
      if(u32OsalSTrace & 0x00000004)u32Level = TR_LEVEL_FATAL;
      
      LLD_vTrace(TR_COMP_OSALCORE, u32Level,Buffer,strlen(&Buffer[1])+1);
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
   }
 
   return(s32ReturnValue);
}


/******************************************************************************
* FUNCTION:     OSAL_s32ThreadControlBlock()
*
* DESCRIPTION:  In this function the Thread control block information is
*               written in to the transferred structure:Thread control 
*               information can not be changed
*
* PARAMETER:
*
*   OSAL_tThreadID             tThreadId     ID of the thread
*
*   OSAL_trThreadControlBlock  *prThreadControlBlock Pointer of thread 
*                                                    control block 
*                                                    structure.
* RETURNVALUE:   
*
*   OSAL_OK or OSAL_ERROR
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadControlBlock( OSAL_tThreadID tid,
                                 OSAL_trThreadControlBlock* prThreadControlBlock)
{
  /*Define Local Variables */
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trThreadElement *pCurrentEntry = NULL;

   /* Check Parameters */
   if( (tid != 0) && (tid != OSAL_ERROR) && (prThreadControlBlock))   /* Check ThreadID Boundaries */
   {
      /* Lock the thread table */
      if(LockOsal(pThreadLock) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableSearchEntryByID(tid,TRUE);
         /* check if the thread entry exists */
         if((pCurrentEntry != OSAL_NULL) && (!pCurrentEntry->bMarkedDeleted))
         {
            prThreadControlBlock->szName       = (tString)pCurrentEntry->szName;
            prThreadControlBlock->u32ErrorCode = pCurrentEntry->u32ErrorCode;
            prThreadControlBlock->startTime    = pCurrentEntry->tStartTime;
            prThreadControlBlock->id           = pCurrentEntry->kernel_tid;
            prThreadControlBlock->u32Priority  = pCurrentEntry->u32Priority;
            prThreadControlBlock->runningTime  = 0;
            prThreadControlBlock->u32CurrentPriority = (tU32)pCurrentEntry->u32Priority;
            prThreadControlBlock->s32StackSize = (tS32)pCurrentEntry->u32TaskStack;
            prThreadControlBlock->s32UsedStack = 0;
            prThreadControlBlock->sliceTime    = 0;/* for USER mode tasks */

  /*          if(pCurrentEntry->bIsSetReadyStatus == FALSE)
            {
                prThreadControlBlock->enStatus     = OSAL_EN_THREAD_INITIALIZED;
            }
            else
            {
                if(pCurrentEntry->bIsSetSuspendedStatus == FALSE)
                {
                    prThreadControlBlock->enStatus = OSAL_EN_THREAD_RUNNING;
                }
                else
                {
                    prThreadControlBlock->enStatus = OSAL_EN_THREAD_SUSPENDED;
                }
            }*/
            clockid_t ClkID;
            if ( pthread_getcpuclockid(pCurrentEntry->tid,&ClkID) == 0)
            {
               struct timespec ts;

               if ( clock_gettime(ClkID,&ts) == 0 )
               {
                  prThreadControlBlock->runningTime =  ts.tv_sec * 1000;
                  if(ts.tv_nsec)
                  {
                     prThreadControlBlock->runningTime += ts.tv_nsec/1000000;
                  }
               }
            }
            s32ReturnValue = OSAL_OK;
         }
         else
         {
            u32ErrorCode = OSAL_E_WRONGTHREAD;
         }
         /* UnLock the thread table */
         UnLockOsal(pThreadLock);
     }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if((u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000004))
   {  
      char au8Buf[251];
      char name[TRACE_NAME_SIZE];
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&au8Buf[1],250,"GetThreadControlBlock Task:%s(%d) for Task:%s(%d) Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,pCurrentEntry->kernel_tid,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"GetThreadControlBlock Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",(unsigned int)u32ErrorCode);
      }
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }

   return(s32ReturnValue);
}


/******************************************************************************
* FUNCTION:      OSAL_s32Threadlist
*
* DESCRIPTION:   This Function gives a list of the current Thread IDs of the 
*                current process
*
* PARAMETER:
*
*                tS32 pas32List[] list of thread IDs;
*                tS32 s32Length   maximal number of elements of the list;
*
*
* RETURNVALUE:   number of IDs in the list
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadList(tS32 pas32List[], tS32 s32Lenght)
{
   tS32 iIndex = 0;
   tU32 u32ErrorCode = OSAL_E_NOERROR; 
   trThreadElement *pCurrent = &pThreadEl[0];

   if(pas32List == OSAL_NULL)
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   else
   {
     /* Lock the thread table */
     if(LockOsal(pThreadLock) == OSAL_OK)
     {
       while((pCurrent <= &pThreadEl[pOsalData->ThreadTable.u32MaxEntries]) && (iIndex < s32Lenght))
       {
         if(pCurrent->bIsUsed== TRUE)
         {
            pas32List[iIndex]=pCurrent->kernel_tid;
            iIndex++;
         }
         pCurrent++;
       }
       /* UnLock the thread table */
       UnLockOsal(pThreadLock);
     }
   }

   if((u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000004))
   {
      char au8Buf[251];
      char name[TRACE_NAME_SIZE];
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      snprintf(&au8Buf[1],250,"GetThreadList Task:%s(%d) for Task:%s Error:0x%x ",
               name,(unsigned int)u32Temp,pCurrent->szName,(unsigned int)u32ErrorCode);
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      if(u32ErrorCode != OSAL_E_NOERROR)vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }
   return (iIndex);
}

/******************************************************************************
* FUNCTION:      OSAL_s32ThreadJoin
*
* DESCRIPTION:   This Function waits on the given thread to finish
*
* PARAMETER:     tS32 OSAL thread ID of the 
*                OSAL_tMSecond Timeout for waiting
*
*
* RETURNVALUE:   number of IDs in the list
*
* HISTORY:
* Date      |   Modification                         | Authors
* 21.03.10  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
******************************************************************************/
tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trThreadElement *pCurrentEntry = NULL;
   tU32 runtime;

   if( ( tid != 0 ) && (tid != OSAL_ERROR) )  /* Check ThreadID Boundaries */
   {
      /* Lock the thread table */
      if(LockOsal(pThreadLock) == OSAL_OK)
      {
         pCurrentEntry = tThreadTableSearchEntryByID(tid,TRUE);
         if(pCurrentEntry != OSAL_NULL)
         {
            runtime = OSAL_ClockGetElapsedTime() - pCurrentEntry->tStartTime;
         }
         else
         {
            runtime = 10;
         }
         /* unlock the thread table */
         UnLockOsal( pThreadLock );

         if(pCurrentEntry != OSAL_NULL) /* check if the thread entry exists */
         {
#ifdef  OSAL_PTHREAD_CREATE_JOINABLE
            ((tVoid)msec); // satisfy lint parameter is for future use
            if( pthread_join((pthread_t)pCurrentEntry->tid, NULL ) == 0 )
            {
               s32ReturnValue = OSAL_OK;
            }
            else
            {
               u32ErrorCode = u32ConvertErrorCore(errno);
            }
#else
            char buffer[128];
            int fd = -1;
            tU32 u32Waitmsec = 0;
            /* ensure minimal runtime to have an proc file system entry */
            if(runtime < 10)
            {
               OSAL_s32ThreadWait(10-runtime);
            }

            snprintf(buffer,128,"/proc/%d/task/%d/",OSAL_ProcessWhoAmI(),tid);
            while(1)
            {
               if((fd = open(buffer,O_RDONLY)) != -1)
               {
                  close(fd);
                  if(u32Waitmsec >= msec)
                  {  
                      u32ErrorCode = OSAL_E_TIMEOUT;
                      break;
                  }
                  OSAL_s32ThreadWait(100);
                  u32Waitmsec += 100;  
               }
               else
               {
                  if(errno != EINTR)
                  {
                     if(errno != ENOENT)
                     {
                        TraceString("OSAL_s32ThreadJoin open %s failed %d",buffer,errno);
                        u32ErrorCode = OSAL_E_UNKNOWN;
                     }
                     else
                     {
                        s32ReturnValue = OSAL_OK;
                     }
                     break;
                  }
               }
            }
#endif
         }
         else
         {
            u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
     }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if(u32ErrorCode != OSAL_E_NOERROR)
   {
      char au8Buf[251];
      char name[TRACE_NAME_SIZE];
      au8Buf[0] = OSAL_STRING_OUT;
      tU32 u32Temp = OSAL_ThreadWhoAmI();
      bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
      if(pCurrentEntry)
      {
         snprintf(&au8Buf[1],250,"ThreadJoin Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,pCurrentEntry->szName,(unsigned int)u32ErrorCode);
      }
      else
      {
         snprintf(&au8Buf[1],250,"ThreadJoin Task:%s(%d) for Task:%s Error:0x%x ",
                  name,(unsigned int)u32Temp,"Unknown",(unsigned int)u32ErrorCode);
      }
      LLD_vTrace(TR_COMP_OSALCORE, TR_LEVEL_FATAL,au8Buf,strlen(&au8Buf[1])+1);
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
   }
   else
   {
      if(LLD_bIsTraceActive(TR_COMP_OSALCORE, TR_LEVEL_DATA)||(u32OsalSTrace & 0x00000004))
      {
         TraceString("OSAL_s32ThreadJoin for TID %d ends %u",tid,u32ErrorCode);
      }
   }
   return s32ReturnValue;
}



/*****************************************************************************
*
* FUNCTION: bGetThreadNameForTID
*
* DESCRIPTION: this function copies file name in a given buffer related to the Task ID
*
* PARAMETER:  char*   pointer to buffer
*             tS32    Buffer size
*             tS32    Task ID
*
* RETURNVALUE: tBool
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tBool bGetThreadNameForTID( char* pBuffer, tS32 s32Size, tS32 s32TID )
{
   tS32  s32len;
   tBool bRet = FALSE;
   trThreadElement *pCurrent = &pThreadEl[0];

   pCurrent = tThreadTableSearchEntryByID(s32TID,TRUE);
   if(pCurrent != OSAL_NULL)  /* check if the thread entry exists */
   {
         s32len = (tS32)strlen( pCurrent->szName );
         if( s32len > s32Size )
         {
            s32len = s32Size;
         }
         strncpy( pBuffer, pCurrent->szName, (tU32)s32len );
         if(s32Size <= s32len)*(pBuffer+s32len-1)= 0x00;/*lint !e661 boundaries guaranted by parameter */
         bRet = TRUE;
   }
   if( bRet == FALSE )
   {
      s32len = (tS32)strlen("NON_OSAL");
         if( s32len > s32Size )
         {
            s32len = s32Size;
         }
      strncpy( pBuffer, "NON_OSAL", s32len);
      *(pBuffer+s32len)= 0x00;
   }
   return bRet;
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file osalproc.c
|-----------------------------------------------------------------------*/
