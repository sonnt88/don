/*****************************************************************************
| FILE:         osalmemory.c
| PROJECT:       
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Memory-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

#include <sys/mman.h>

#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define MMAP_MAGIC  0x4D4D4150
#ifndef MMAP_FAST
#define MMAP_NAME_LENGTH  32
#endif
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
#ifndef MMAP_FAST
static tU32 u32Count = 0;
#endif
/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
void vReactOnAllocError(tU32 u32size)
{
   trThreadElement *pCurrentEntry;
   char au8Buf[20];
   tS32 s32Tid = OSAL_ThreadWhoAmI();
   au8Buf[0] = EN_MEM_ALLOC;
   OSAL_M_INSERT_T32(&au8Buf[1],u32size);
   OSAL_M_INSERT_T32(&au8Buf[5],(tU32)s32Tid);

   pCurrentEntry = tThreadTableSearchEntryByID(s32Tid,TRUE);
   if(pCurrentEntry)
   {
     /* Store Task-Name */
     memcpy((void*)&au8Buf[9],(void*)pCurrentEntry->szName,10);
   }
   else
   {
      memcpy((void*)&au8Buf[9],"LI_TASK\n",8);
   }
   LLD_vTrace((tU32)TR_COMP_OSALCORE, (tU32)TR_LEVEL_FATAL, au8Buf,20);
   vWriteToErrMem((tS32)TR_COMP_OSALCORE,au8Buf,(int)strlen(au8Buf),OSAL_STRING_OUT);
}


void* OSAL_pvMemoryMap(tU32 u32size)
{
   void* pRet;
   uintptr_t* pMem;

#ifdef MMAP_FAST
  tU32 u32MapSize = u32size + (2* sizeof(uintptr_t));
  if((pRet = mmap(0, u32MapSize, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0)) == (caddr_t)-1)
  {
      TraceString("OSAL_pvMemoryMap Error %d",u32ConvertErrorCore(errno));
      vReactOnAllocError(u32MapSize);
      pRet = 0;
  }
  else
  {
     pMem = (uintptr_t*)pRet;
     *pMem = MMAP_MAGIC;
     pMem++;
     *pMem = u32MapSize;
     pMem++;
     pRet = (void*)pMem;
  }
#else
   char szName[MMAP_NAME_LENGTH];
   int fd = -1;
   u32Count++;
   tU32 u32MapSize = u32size + (3* sizeof(tU32) + MMAP_NAME_LENGTH);

   snprintf(szName,MMAP_NAME_LENGTH,"osal_%d_%d",OSAL_ThreadWhoAmI(),u32Count);
#ifdef OSAL_MMAP_SHMEM
   if((fd = shm_open(szName, O_RDWR|O_EXCL|O_RDWR|O_CREAT|O_TRUNC,OSAL_ACCESS_RIGTHS)) != -1)    
   {
       if(ftruncate(fd,u32MapSize) != -1)
       {
#else
   if((fd = open(szName,O_RDWR| O_CREAT|O_TRUNC, 0777)) != -1)
   {
      /*Gr��e der Datei festlegen*/
      if((lseek(fd, u32MapSize-1, SEEK_SET) != -1)
       &&(write(fd,"",1) != -1))
      {
#endif
         if((pRet = mmap(NULL,u32MapSize, PROT_READ|PROT_WRITE,MMAP_FLAGS, fd, 0)) == (caddr_t)-1)
         {
            TraceString("OSAL_pvMemoryMap %s Error %d",szName,u32ConvertErrorCore(errno));
            vReactOnAllocError(u32MapSize);
            pRet = 0;
         }
         else
         {
//            TraceString("OSAL_pvMemoryMap Name %s Size:%d FD:%d pMem:0x%x",szName,u32MapSize,fd,pRet);
//            OSAL_s32ThreadWait(200);

            pMem = (uintptr_t*)pRet;
            *pMem = MMAP_MAGIC;
            pMem++;
            *pMem = u32MapSize;
            pMem++;
            *pMem = fd;
            pMem++;
            strncpy((char*)pu32Mem,szName,strlen(szName));
            /* calculate adress given to user */
            pMem = (uintptr_t*)((uintptr_t)pMem + MMAP_NAME_LENGTH);
            pRet = (void*)pu32Mem;
         }
      }
      else
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
#endif
   return pRet;
}

void OSAL_pvMemoryUnMap(tPVoid pBlock)
{
   uintptr_t  u32Size;
   /* goto start of management data */
#ifdef MMAP_FAST
   uintptr_t* puMem = (uintptr_t*)((uintptr_t)pBlock - (2*sizeof(uintptr_t)));
#else
   uintptr_t  fd;
   char  szName[MMAP_NAME_LENGTH];
   uintptr_t* puMem = (uintptr_t*)((uintptr_t)pBlock - (MMAP_NAME_LENGTH + (3*sizeof(uintptr_t))));
#endif
   void* pMem  = (void*)puMem;

   /* check for MMAP magic */
   if((uintptr_t)*puMem == MMAP_MAGIC)
   {
      puMem++;
      /* extract size*/
      u32Size = *puMem;
#ifndef MMAP_FAST
      puMem++;
      /* extract file descriptor */
      fd = *puMem;
      pMem++;
      /* extract name copy because we have to unmap before unlink */
      strncpy(szName,(char*)puMem,MMAP_NAME_LENGTH);
//      TraceString("OSAL_pvMemoryUnMap Name %s Size:%d FD:%d pMem:0x%x",szName,u32Size,fd,pMem);
//      OSAL_s32ThreadWait(200);
#endif
      if(munmap(pMem,u32Size) == -1)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
#ifndef MMAP_FAST
      if(close(fd) == -1)
      {
          NORMAL_M_ASSERT_ALWAYS();
      }
#ifdef OSAL_MMAP_SHMEM
      shm_unlink(szName);
#else
      unlink(szName);
#endif
#endif
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
}

tBool bUsageMmap(tU32 Size)
{
#ifdef OSAL_MMAP_SIZE
   if(Size > OSAL_MMAP_SIZE)
   { return TRUE;   }
   else
#else
    ((void)Size);
#endif
   { return FALSE;   }
}

/******************************************************************************
 * FUNCTION:      OSAL_pvMemoryAllocate
 *
 * DESCRIPTION:   This function reserve on the Heap-memory a block with
 *                          the specific size
 *
 * PARAMETER:
 *
 *   u32size                     Size of the memory block.
 *
 * RETURNVALUE:
 *
 *    tPVoid       Pointer to start memory block or OSAL_NULL in case of error
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ******************************************************************************/
/*void* OSAL_pvMemoryAllocate(size_t u32size)
{
   void* pRet=NULL;
   if(u32size)
   {
#ifdef OSAL_MMAP_SIZE
      tU32* pu32Mem;
      if(bUsageMmap(u32size)) 
      {
         pRet = OSAL_pvMemoryMap(u32size);
      }
      else
      {
         u32size = u32size + (2*sizeof(u32size));
#endif
         pRet = malloc((size_t)u32size);
#ifdef OSAL_MMAP_SIZE
         if(pRet)
         {
            pu32Mem = (tU32*)pRet;
            *pu32Mem = MMAP_MAGIC;
            pu32Mem++;
            *pu32Mem = u32size;
            pu32Mem++;
            pRet = (void*)pu32Mem;
         }
      }
#endif
      if (pRet == NULL)
      {
         vReactOnAllocError(u32size);
      }
   }

   return pRet;
}*/

/******************************************************************************
 * FUNCTION:      OSAL_vMemoryFree
 *
 * DESCRIPTION:   This function releases on the Heap-memory a block with
 *                          the specific size
 *
 * PARAMETER:
 *
 *   tPVoid       pBlock:    Pointer to start memory block.
 *
 * RETURNVALUE:
 *
 *    none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 03.10.05  | Initial revision                       | MRK2HI
 * --.--.--  | ----------------                       | -----
 *
 ******************************************************************************/
/*tVoid OSAL_vMemoryFree(tPVoid pBlock)
{ 
   if (pBlock)
   {
#ifdef OSAL_MMAP_SIZE
     tU32* pu32Mem = (tU32*)pBlock;
     tU32 u32Size;
     pu32Mem--
     u32Size = *(pu32Mem);
     pu32Mem--;
     if(*pu32Mem == MMAP_MAGIC)
     {
        if(munmap(pBlock,u32Size) == -1)
        {
          NORMAL_M_ASSERT_ALWAYS();
        }
     }
     else
#endif
     {
        free(pBlock);
     }
   }
   return;
}*/


#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osalmemory.c
|-----------------------------------------------------------------------*/
