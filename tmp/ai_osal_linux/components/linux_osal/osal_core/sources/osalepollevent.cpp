/*****************************************************************************
| FILE:         osalepollevent.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Event-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 14.04.14  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#ifdef EPOLL_EV

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

#define  LINUX_C_U32_EVENT_ID                    ((tU32)0x45564E54)

#define  LINUX_C_EVENT_MAX_NAMELENGHT (OSAL_C_U32_MAX_NAMELENGTH - 1)

//#define  TACE_EVENT

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

tS32  s32EventTableCreate(void);
tS32  s32EventTableDeleteEntries(void);

static trEventElement* tEventTableGetFreeEntry(tVoid);

static trEventElement* tEventTableSearchEntryByName(tCString coszName);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableCreate
*
* DESCRIPTION: This function creates the Event Table List. If
*              there isn't space it returns a error code.
*
* PARAMETER:   none
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableCreate(tVoid)
{
   tU32 tiIndex;
   tS32 s32ReturnValue = OSAL_OK;

   pOsalData->EventTable.u32UsedEntries      = 0;
   pOsalData->EventTable.u32MaxEntries       = pOsalData->u32MaxNrEventElements ;

   for(tiIndex = 0; tiIndex < pOsalData->EventTable.u32MaxEntries; tiIndex++)
   {
      pEventEl[tiIndex].u32EventID = LINUX_C_U32_EVENT_ID;
      pEventEl[tiIndex].bIsUsed = FALSE;
      pEventEl[tiIndex].u32Index = tiIndex;
   }

   if(mkdir(VOLATILE_DIR"/OSAL/Events",S_IFDIR | OSAL_ACCESS_RIGTHS) == -1)
   {
       TraceString("OSAL mkdir /.../OSAL_Events failed");
   }
   else
   {
      if(chmod(VOLATILE_DIR"/OSAL/Events", OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
      {
          vWritePrintfErrmem("ThreadTableCreate -> /.../OSAL_Events  chmod error %d \n",errno);
      }
   }

   return s32ReturnValue;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableGetFreeEntry
*
* DESCRIPTION: this function goes through the Event List and returns the
*              first unused EventElement.
*
*
* PARAMETER:   tVoid
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static  trEventElement* tEventTableGetFreeEntry(tVoid)
{
   trEventElement *pCurrent  = pEventEl;
   tU32 used = pOsalData->EventTable.u32UsedEntries;

   if (used < pOsalData->EventTable.u32MaxEntries)
   {
      pCurrent = &pEventEl[used];
      pOsalData->EventTable.u32UsedEntries++;
   } else {
        /* search an entry with !bIsUsed */
        while ( pCurrent < &pEventEl[used]
                && ( pCurrent->bIsUsed == TRUE) )
        {
            pCurrent++;
        }
        if(pCurrent >= &pEventEl[used])
        {
            pCurrent = NULL; /* not found */
        }
   }
   return pCurrent;
}


/*****************************************************************************
*
* FUNCTION:    tEventTableSearchEntryByName
*
* DESCRIPTION: this function goes through the Event List and returns the
*              EventElement with the given name or NULL if all the List has
*              been checked without success.
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: trEventElement*
*                 free entry pointer or OSAL_NULL
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static trEventElement *tEventTableSearchEntryByName(tCString coszName)
{
   trEventElement *pCurrent  = pEventEl;
   tU32 used = pOsalData->EventTable.u32MaxEntries;

   /* search an entry with coszName and the current pid */
   while ( pCurrent < &pEventEl[used]
         && ( pCurrent->bIsUsed == FALSE
      ||  (OSAL_s32StringCompare(coszName, pCurrent->szName))) )
   {
      pCurrent++;
   }
   if(pCurrent >= &pEventEl[used])
   {
      pCurrent = NULL;
   }
   return pCurrent;
}

/*****************************************************************************
*
* FUNCTION:    bEventTableDeleteEntryByName
*
* DESCRIPTION: this function goes through the Event List deletes the
*              EventElement with the given name
*
* PARAMETER:   coszName (I)
*                 event name wanted.
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************
static tBool bEventTableDeleteEntryByName(tCString coszName)
{
   trEventElement *pCurrent = EventTable.ptrHeader;

   while( (pCurrent!= OSAL_NULL)
       && (OSAL_s32StringCompare(coszName,(tString)pCurrent->szName) != 0) )
   {
      pCurrent = pCurrent->pNext;
   }
   if(pCurrent != OSAL_NULL)
   {
      if(pCurrent->bIsUsed == FALSE )
     {
           pCurrent->bIsUsed = 0;
           pCurrent->bToDelete = 0;
           pCurrent->u16ContextID = 0;
           pCurrent->u16OpenCounter = 0;
           pCurrent->u32mask = 0;
           memset(&pCurrent->rEventGroup,0,sizeof(T_CFLG));
           memset(pCurrent->szName,0,OSAL_C_U32_MAX_NAMELENGTH);
           pCurrent->hLock = 0;
           return TRUE;
     }
   }

   return FALSE;
}*/

/*****************************************************************************
*
* FUNCTION:    bCleanUpEventofContext
*
* DESCRIPTION: this function goes through the Event List deletes the
*              Event and EventElement with the given context from list
*
* PARAMETER:   tU16 Context ID
*
* RETURNVALUE: tBool
*                TRUE = success FALSE=failed
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vCleanUpEventofContext(void)
{
   tS32 s32OwnPid = OSAL_ProcessWhoAmI();
   tS32 s32EvHandle;
   tS32 s32OpnCount = 0;
   trEventElement *pCurrentEntry = NULL;
   char cName[OSAL_C_U32_MAX_NAMELENGTH] = {0};
   while(1)
   {
      s32EvHandle = (tS32)pvGetFirstElementForPid(&EvMemPoolHandle,(tS32)s32OwnPid);
      if(s32EvHandle)
      {
         if(((tEvHandle*)s32EvHandle)->s32Tid > 0)
         {
    //         TraceString("Valid Handle Found %d",s32EvHandle);
             pCurrentEntry = ((tEvHandle*)s32EvHandle)->pEntry; /*lint !e613 *//* pointer already checked*/ 
             if(cName[0] == 0)
             {
                 strncpy(cName,pCurrentEntry->szName,OSAL_C_U32_MAX_NAMELENGTH);
             }
             s32OpnCount =  pCurrentEntry->u16OpenCounter;
             if(OSAL_s32EventClose(s32EvHandle) == OSAL_ERROR)
             {
                 break;
             }
             if((s32OpnCount-1) == 0)
             {
                 OSAL_s32EventDelete(cName);
             }
         }
      }
      else
      {
          break;
      }
   }
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    s32EventTableDeleteEntries
*
* DESCRIPTION: This function deletes all elements from OSAL table,
*
* PARAMETER:
*   u16ContextID                  index to distinguish caller context
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32EventTableDeleteEntries(void)
{
   trEventElement *pCurrentEntry = &pEventEl[0];
   tS32 s32ReturnValue = OSAL_OK;
   tS32 s32DeleteCounter = 0;
   tU32 used = pOsalData->EventTable.u32UsedEntries;


   if(LockOsal(&pOsalData->EventTable.rLock) == OSAL_OK)
   {
      /* search the whole table */
      while ( pCurrentEntry < &pEventEl[used])
      {
         if (pCurrentEntry->bIsUsed == TRUE)
         {
               s32DeleteCounter++;
               pCurrentEntry->bIsUsed = FALSE;
               s32ReturnValue = s32DeleteCounter;
         }
         else
         {
            pCurrentEntry++;
         }
      }

      UnLockOsal(&pOsalData->EventTable.rLock);
   }
   return(s32ReturnValue);
}

void vTraceEventInfo(trEventElement *pCurrentEntry,tU8 Type,tU32 u32Level,tU32 u32Error,tCString coszName)
{
#ifdef SHORT_TRACE_OUTPUT
    char au8Buf[26 + OSAL_C_U32_MAX_NAMELENGTH +8];
    tU32 u32Val = (tU32)OSAL_ThreadWhoAmI();
    tS32 s32Len;

    if((Type == POST_OPERATION)||(Type == WAIT_OPERATION))
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO2);
    }
    else
    {
       OSAL_M_INSERT_T8(&au8Buf[0],(tU8)OSAL_TSK_FLG_INFO);
    }
    OSAL_M_INSERT_T8(&au8Buf[1],(tU8)Type);
    bGetThreadNameForTID(&au8Buf[2],8,(int)u32Val);
    OSAL_M_INSERT_T32(&au8Buf[10],u32Val);
 
    if(pCurrentEntry)
    {
        OSAL_M_INSERT_T32(&au8Buf[14],(tU32)pCurrentEntry);
        OSAL_M_INSERT_T32(&au8Buf[18],pCurrentEntry->u32EventBitField);
    }
    else
    {
        OSAL_M_INSERT_T32(&au8Buf[14],u32Val);
        OSAL_M_INSERT_T32(&au8Buf[18],u32Val);
    }
        OSAL_M_INSERT_T32(&au8Buf[22],u32Error);

    s32Len = 26;

    if(pCurrentEntry)
    {
       if(Type == POST_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32PostMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32PostFlag);
          s32Len += 8;
       }
       else if(Type == WAIT_OPERATION)
       {
          OSAL_M_INSERT_T32(&au8Buf[26],pCurrentEntry->u32WaitMask);
          OSAL_M_INSERT_T32(&au8Buf[30],pCurrentEntry->u32WaitEnFlags);
          s32Len += 8;
       }
    }
    else
    {
          OSAL_M_INSERT_T32(&au8Buf[26],u32Error);
          OSAL_M_INSERT_T32(&au8Buf[30],u32Error);
    }

    u32Val = 0;
    if(coszName)
    {
      u32Val = strlen(coszName);
      if( u32Val > OSAL_C_U32_MAX_NAMELENGTH )
      {
         u32Val = OSAL_C_U32_MAX_NAMELENGTH;
      }
      memcpy(&au8Buf[s32Len],coszName,u32Val);
      au8Buf[s32Len + OSAL_C_U32_MAX_NAMELENGTH -1] = 0;
    }

    s32Len += u32Val;
    LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG, u32Level,au8Buf,s32Len);
    if((pOsalData->bLogError)&&(u32Level == TR_LEVEL_FATAL))
    {
       if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == FALSE))
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,s32Len,0);
       }
       else
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,s32Len,0);
       }
    }
#else
   char au8Buf[251]={0}; 
   char name[TRACE_NAME_SIZE];
   tU32 u32Mask = 0xffffffff;
   tU32 u32Flag = 0xffffffff;
   tU32 u32Temp = (tU32)OSAL_ThreadWhoAmI();
   au8Buf[0] = OSAL_STRING_OUT;
   bGetThreadNameForTID(&name[0],TRACE_NAME_SIZE,(tS32)u32Temp);
   tCString Operation;
   switch(Type)
   {
     case CREATE_OPERATION:
         Operation = "CREATE_OPERATION";
         break;
     case DELETE_OPERATION:
         Operation = "DELETE_OPERATION";
         break;
     case OPEN_OPERATION:
         Operation = "OPEN_OPERATION";
         break;
     case CLOSE_OPERATION:
         Operation = "CLOSE_OPERATION";
         break;
     case WAIT_OPERATION:
         Operation = "WAIT_OPERATION";
         break;
     case POST_OPERATION:
          Operation = "POST_OPERATION";
          if(pCurrentEntry)
          {
             u32Mask = pCurrentEntry->u32PostMask;
             u32Flag = pCurrentEntry->u32PostFlag;
          }
          break;
     case WAIT_OPERATION_STEP:
         Operation = "WAIT_OPERATION_STEP";
          if(pCurrentEntry)
          {
             u32Mask = pCurrentEntry->u32WaitMask;
             u32Flag = pCurrentEntry->u32WaitEnFlags;
          }
         break;
     default:
         Operation = "Unknown";
         break;
   }
   if(coszName == NULL)coszName ="Unknown";
   if(pCurrentEntry)
   {
       if(u32Flag != 0xffffffff)
       {
          snprintf(&au8Buf[1],250,"Event %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Mask:0x%x Flag:%d Name:%s",
                   Operation,name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,(unsigned int)u32Mask,(unsigned int)u32Flag,coszName);
       }
       else
       {
          snprintf(&au8Buf[1],250,"Event %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Name:%s",
                   Operation,name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry,(unsigned int)pCurrentEntry->u32EventBitField,
                   (unsigned int)u32Error,coszName);
       }
   }
   else
   {
      snprintf(&au8Buf[1],250,"Event %s Task:%s(%d) Handle:0x%x BitField:0x%x Error:0x%x Name:%s",
               Operation,name,(unsigned int)u32Temp,(unsigned int)pCurrentEntry,(unsigned int)u32Error,(unsigned int)u32Error,coszName);
   }
   LLD_vTrace(OSAL_C_TR_CLASS_SYS_FLG,u32Level,au8Buf,strlen(&au8Buf[1])+1);
    if((pOsalData->bLogError)&&(u32Level == TR_LEVEL_FATAL))
    {
       if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == FALSE))
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,strlen(&au8Buf[1])+1,0);
       }
       else
       {
          vWriteToErrMem(OSAL_C_TR_CLASS_SYS_FLG,au8Buf,strlen(&au8Buf[1])+1,0);
       }
    }
#endif
}

/*****************************************************************************
*
* FUNCTION: OSAL_s32EventCreate
*
* DESCRIPTION: this function creates an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to create.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventCreate( tCString coszName,
                          OSAL_tEventHandle* phEvent )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   /* The name is not NULL */
   if (coszName && phEvent) /* FIX 15/10/2002 */
   {
      /* The Name is not too long */
      if( OSAL_u32StringLength( coszName ) <= (tU32)LINUX_C_EVENT_MAX_NAMELENGHT )
      {
         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            /* The name is not already in use*/
            pCurrentEntry = tEventTableSearchEntryByName( coszName );
            if(pCurrentEntry == OSAL_NULL)
            {
               pCurrentEntry = tEventTableGetFreeEntry();
               if (pCurrentEntry!= OSAL_NULL)
               {
                  /* Create Event */
                  tS32 fifo = -1;
                  OSAL_szStringCopy(&pCurrentEntry->szFileName[0], VOLATILE_DIR"/OSAL/Events/");
                  OSAL_szStringConcat(pCurrentEntry->szFileName,coszName);
                  unlink((char*)pCurrentEntry->szFileName);
                  fifo = mkfifo((char*)pCurrentEntry->szFileName, OSAL_ACCESS_RIGTHS);
                  if(fifo != -1)
                  {
                     if(s32OsalGroupId)
                     {
                        char Name[OSAL_C_U32_MAX_NAMELENGTH];
                        tS32 Pid = OSAL_ProcessWhoAmI();
                        tS32 Tid = OSAL_ThreadWhoAmI();
                        bGetTaskName(Pid, Tid, Name);
                        if(chown((char*)pCurrentEntry->szFileName,(uid_t)-1,s32OsalGroupId) == -1)
                        {
                           vWritePrintfErrmem("OSAL_s32EventCreate mkfifo -> chown error %d Task:%s \n",errno,Name);
                        }
                        
                        if(chmod((char*)pCurrentEntry->szFileName, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
                        {
                           vWritePrintfErrmem("OSAL_s32EventCreate mkfifo %s chmod error %d Task:%s\n",pCurrentEntry->szFileName, errno,Name);
                        }
                     }
                     pOsalData->u32EvtResCount++;
                     if(pOsalData->u32MaxEvtResCount < pOsalData->u32EvtResCount)pOsalData->u32MaxEvtResCount = pOsalData->u32EvtResCount;
                     if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))
                     {
                         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_DATA,u32ErrorCode,coszName);
                     }
                     /* success */
                     pCurrentEntry->hLock = (tU32)-1;
                     pCurrentEntry->s32RecProc = -1;
                     pCurrentEntry->bIsUsed   = TRUE;
                     pCurrentEntry->bToDelete = FALSE;
                     pCurrentEntry->u16OpenCounter   = 1;
                     pCurrentEntry->u32WaitMask      = 0;
                     pCurrentEntry->u32WaitEnFlags   = 0;
                     pCurrentEntry->u32EventBitField = 0;
                     pCurrentEntry->bWaitFlag = FALSE;
                     pCurrentEntry->bTraceFlg = pOsalData->bEvTaceAll;
                     pCurrentEntry->s32PID    = OSAL_ProcessWhoAmI();
                     if(pOsalData->szEventName[0] != 0)
                     {
                        if(!strncmp(coszName,pOsalData->szEventName,strlen(coszName)))
                        {
                           pCurrentEntry->bTraceFlg = TRUE;
                        }
                     }
                     (void)OSAL_szStringNCopy( (tString)pCurrentEntry->szName,
                                               coszName, OSAL_C_U32_MAX_NAMELENGTH );
                     tEvHandle* pTemp = (tEvHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&EvMemPoolHandle);
                     if(pTemp)
                     {
                         pTemp->pEntry   = pCurrentEntry;
                         pTemp->s32Tid   = OSAL_ThreadWhoAmI();
                         pTemp->fifo_r   = -1;
                         pTemp->fifo_w   = -1;
                     }
                     else
                     {
                         NORMAL_M_ASSERT_ALWAYS();
                     }
                     *phEvent = (OSAL_tEventHandle)pTemp;
                     s32ReturnValue = OSAL_OK;
                     pOsalData->u32EvtResCount++;
                     if(pOsalData->u32MaxEvtResCount < pOsalData->u32EvtResCount)
                     {
                         pOsalData->u32MaxEvtResCount = pOsalData->u32EvtResCount;
                         if(pOsalData->u32MaxEvtResCount > (pOsalData->u32MaxNrEventElements *9/10))
                         {
                            pOsalData->u32NrOfChanges++;
                         }
                     }
                     if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))||(pCurrentEntry->bTraceFlg == TRUE))
                     {
                        vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
                     }
                  }
                  else
                  {
                    FATAL_M_ASSERT_ALWAYS();
                    u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_NOSPACE;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }
             UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode == OSAL_E_ALREADYEXISTS)
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,CREATE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventDelete
*
* DESCRIPTION: this function removes an OSAL event.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventDelete(tCString coszName)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   if (coszName)
   {
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
         pCurrentEntry = tEventTableSearchEntryByName( coszName );
         if( pCurrentEntry != OSAL_NULL )
         {
            if( pCurrentEntry->u16OpenCounter == 0 )
            {
               pCurrentEntry->bIsUsed = FALSE;
               if(pCurrentEntry->bWaitFlag)
               {
                  WRITE_LINE_INFO;
                  TraceString("Event %s try to delete by Task %d during wait !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
                  OSAL_s32ThreadWait(500);
               }
#ifdef EPOLL
               /* check if event was already used */
               if(pCurrentEntry->hLock != -1)
               {
                  if(close(pCurrentEntry->hLock) != -1)
                  {
                     pCurrentEntry->s32RecProc = -1;
                     pCurrentEntry->hLock = -1;
                     s32ReturnValue = OSAL_OK;
                     pOsalData->u32EvtResCount--;
                  }
                  else
                  {
                     u32ErrorCode   = OSAL_E_BUSY;    // return with OSAL_ERROR and E_BUSY. todo correct?
                     pCurrentEntry->bToDelete = TRUE; // can not be deleted, mark as toDelete
                  }
              }
              else
#endif
              {
                     pCurrentEntry->s32RecProc = -1;
                     s32ReturnValue = OSAL_OK;
                     pOsalData->u32EvtResCount--;
              }
              if(unlink((char*)pCurrentEntry->szFileName) == -1)
              {
                  TraceString("Event Del %s unlink by Task %d failed !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
              }
            }
            else
            {
               pCurrentEntry->bToDelete = TRUE;
               s32ReturnValue = OSAL_OK;
            }
         }
         else
         {
           u32ErrorCode = OSAL_E_DOESNOTEXIST;
         }
         UnLockOsal(&pOsalData->EventTable.rLock);
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN; // if we step over the assert, we need this code
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))||(pCurrentEntry->bTraceFlg == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,DELETE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventOpen
*
* DESCRIPTION: this function returns a valid handle to an OSAL event
*              already created.
*
* PARAMETER:   coszName (I)
*                 event name to be removed.
*              phEvent (->O)
*                 pointer to the event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventOpen(tCString coszName, OSAL_tEventHandle* phEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   if( coszName && phEvent )
   {
      if(OSAL_u32StringLength(coszName) <= (tU32)LINUX_C_EVENT_MAX_NAMELENGHT )
      {
         /* Lock the event table */
         if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
         {
            pCurrentEntry = tEventTableSearchEntryByName(coszName);

            if (pCurrentEntry != OSAL_NULL)
            {
               if( !pCurrentEntry->bToDelete )
               {
                  pCurrentEntry->u16OpenCounter++;
                  tEvHandle* pTemp = (tEvHandle*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&EvMemPoolHandle);
                  if(pTemp)
                  {
                     pTemp->pEntry   = pCurrentEntry;
                     pTemp->fifo_r   = -1;
                     pTemp->fifo_w   = -1;
                     pTemp->s32Tid   = OSAL_ThreadWhoAmI();
                     *phEvent = (OSAL_tEventHandle)pTemp;
                     s32ReturnValue = OSAL_OK;
                  }
                  else
                  {
                     FATAL_M_ASSERT_ALWAYS();
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_BUSY;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_DOESNOTEXIST;
            }
            /* unlock the Event table */
            UnLockOsal(&pOsalData->EventTable.rLock);
         }
         else
         {
            FATAL_M_ASSERT_ALWAYS();
            u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NAMETOOLONG;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if ( u32ErrorCode != OSAL_E_NOERROR )
   {
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(u32ErrorCode == OSAL_E_DOESNOTEXIST)
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_ERROR,u32ErrorCode,coszName);
      }
      else
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      s32ReturnValue = OSAL_ERROR;
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))||(pCurrentEntry->bTraceFlg == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,OPEN_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventClose
*
* DESCRIPTION: this function closes an OSAL event.
*
* PARAMETER:   hEvent (I)
*                 event handle.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventClose(OSAL_tEventHandle hEvent)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;

   /* Parameter check */
   if( hEvent )
   {
      /* Lock the event table */
      if( LockOsal( &pOsalData->EventTable.rLock ) == OSAL_OK )
      {
         pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;

         if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
         {
            if( pCurrentEntry->u16OpenCounter > 0 )
            {
               pCurrentEntry->u16OpenCounter--;
               if(((tEvHandle*)hEvent)->fifo_r)close(((tEvHandle*)hEvent)->fifo_r);
               if(((tEvHandle*)hEvent)->fifo_w)close(((tEvHandle*)hEvent)->fifo_w);
               ((tEvHandle*)hEvent)->fifo_r = -1;
               ((tEvHandle*)hEvent)->fifo_w = -1;
               s32ReturnValue = OSAL_OK;
               // @@@@ kos2hi: insert POSIX delete mechanism
               if( !pCurrentEntry->u16OpenCounter && pCurrentEntry->bToDelete )
               {
                  pCurrentEntry->bIsUsed = FALSE;
                  if(pCurrentEntry->bWaitFlag)
                  {
                     WRITE_LINE_INFO;
                     TraceString("Event %s try to delete by Task %d during wait !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
                     OSAL_s32ThreadWait(500);
                  }
#ifdef EPOLL
                  if(pCurrentEntry->hLock != -1)
                  {
                     if(close(pCurrentEntry->hLock) != -1)
                     {
                         NORMAL_M_ASSERT_ALWAYS();
                     }
                  }
#endif
                  pCurrentEntry->s32RecProc = -1;
                  pCurrentEntry->hLock = (tU32)-1;
                  pCurrentEntry->bToDelete = FALSE;
                  pOsalData->u32EvtResCount--;
                  if(unlink((char*)pCurrentEntry->szFileName) == -1)
                  {
                     TraceString("Event Cls %s unlink by Task %d failed !!!",pCurrentEntry->szName,OSAL_ThreadWhoAmI());
                  }
               }
               ((tEvHandle*)hEvent)->pEntry   = NULL;
               ((tEvHandle*)hEvent)->s32Tid   = 0;
               ((tEvHandle*)hEvent)->fifo_r   = -1;
               ((tEvHandle*)hEvent)->fifo_w   = -1;

               if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&EvMemPoolHandle,(tEvHandle*)hEvent) == OSAL_ERROR)
               {    NORMAL_M_ASSERT_ALWAYS();  }
            }
            else 
            {
               u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;  // todo Why not OSAL_E_DOESNOTEXIST;
         }
         /* UnLock the Event table */
         UnLockOsal( &pOsalData->EventTable.rLock );
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
         u32ErrorCode = OSAL_E_UNKNOWN;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      tCString coszName;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode );
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((LLD_bIsTraceActive(OSAL_C_TR_CLASS_SYS_FLG, TR_LEVEL_DATA))||(pCurrentEntry->bTraceFlg == TRUE))/*lint !e613 pCurrentEntry already checked */
      {
         vTraceEventInfo(pCurrentEntry,CLOSE_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);/*lint !e613 pCurrentEntry already checked */
      }
   }

   return( s32ReturnValue );
}

tU32 u32CheckCondition(trEventElement* pCurrentEntry,
                       OSAL_tEventMask   mask,
                       OSAL_tenEventMaskFlag enFlags )
{
  tU32 u32Mask = 0;
  /* check if return condition is full filled */
//  TraceString("u32CheckCondition mask: 0x%x EventBitField  0x%x start",mask,pCurrentEntry->u32EventBitField);
  switch (enFlags)
  {
    case OSAL_EN_EVENTMASK_AND:
         u32Mask = ( ( pCurrentEntry->u32EventBitField & mask ) == mask);
        break;
    case OSAL_EN_EVENTMASK_OR:
         u32Mask = ( pCurrentEntry->u32EventBitField & mask);
        break;                              // this is only for the trace message end
    default:
         NORMAL_M_ASSERT_ALWAYS();
        break;
  }
//  TraceString("u32CheckCondition mask: 0x%x EventBitField  0x%x return %d ",mask,pCurrentEntry->u32EventBitField,u32Mask);
  return u32Mask;
}

tU32 u32SignalSetBitFields(trEventElement *pCurrentEntry,
                         OSAL_tEventMask   mask,
                         tU32 u32Signal,
                         tS32 s32Flag)
{
    tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(u32Signal)
   {
//   TraceString("vSignalSetBitFields Signal:0x%x EventBitField 0x%x start",u32Signal,pCurrentEntry->u32EventBitField);
      switch(u32Signal)
      {
       case OSAL_EN_EVENTMASK_AND:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & mask;
           break;
       case OSAL_EN_EVENTMASK_OR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField | mask;
           break;
       case OSAL_EN_EVENTMASK_XOR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField ^ mask;
           break;
       case OSAL_EN_EVENTMASK_REPLACE:
            pCurrentEntry->u32EventBitField = mask;
           break;
       default:
            u32ErrorCode = OSAL_E_INVALIDVALUE;
          break;
      }
//      TraceString("vSignalSetBitFields Signal:0x%x EventBitField 0x%x end",u32Signal,pCurrentEntry->u32EventBitField);
   }
   else
   {
//      TraceString("vSignalSetBitFields Flag:0x%x EventBitField 0x%x start",s32Flag,pCurrentEntry->u32EventBitField);
      switch( s32Flag )
      {
       case OSAL_EN_EVENTMASK_AND:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField & mask;
           break;
       case OSAL_EN_EVENTMASK_OR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField | mask;
           break;
       case OSAL_EN_EVENTMASK_XOR:
            pCurrentEntry->u32EventBitField = pCurrentEntry->u32EventBitField ^ mask;
           break;
       case OSAL_EN_EVENTMASK_REPLACE:
            pCurrentEntry->u32EventBitField = mask;
           break;
       default:
            u32ErrorCode = OSAL_E_INVALIDVALUE;
          break;
      }
//      TraceString("vSignalSetBitFields Flag:0x%x EventBitField 0x%x end",s32Flag,pCurrentEntry->u32EventBitField);
   }
   return u32ErrorCode;
}

#ifdef FAST_RET
void vStoreToBuffer(trEventElement *pCurrentEntry, char* pBuffer, tU32 DataSets)
{
   pCurrentEntry->pu32Storage = (tU32*)malloc(DataSets * 8);
   if(pCurrentEntry->pu32Storage)
   {
      memcpy(pCurrentEntry->pu32Storage,pBuffer,DataSets*8);
      pCurrentEntry->u32StoredDataSets = DataSets;
   }
}

void vReadFromBuffer(trEventElement *pCurrentEntry)
{
   tPU32 pu32Mask;
   tPU32 pu32Flag;
   tU32 i;
   tPU32 u32Tmp;
   if(pCurrentEntry->pu32Storage)
   {
      u32Tmp = pCurrentEntry->pu32Storage;
      for(i=0;i<pCurrentEntry->u32StoredDataSets;i++)
      {
         pu32Mask = u32Tmp;
         u32Tmp++;
         pu32Flag = u32Tmp;
         u32Tmp++;
         u32SignalSetBitFields(pCurrentEntry,*pu32Mask,*pu32Flag,0);
      }
      free(pCurrentEntry->pu32Storage);
      pCurrentEntry->pu32Storage = NULL;
      pCurrentEntry->u32StoredDataSets = 0;
   }
}
#endif

tU32 u32ReadDataFromPipe(tEvHandle* phEvent,trEventElement *pCurrentEntry,tU32* pu32Mask, tBool bRetFast)
{
   tS32 s32Ret=0;
   tU32 i,u32Tmp = 0;
   tU32 Data[360] = {0};
   tU32 u32ErrorCode = OSAL_E_NOERROR;
#ifdef TACE_EVENT
   TraceString("read %s handle %d ",pCurrentEntry->szName,fifo);
#endif
#ifdef FAST_RET
   if(pCurrentEntry->pu32Storage)
   {
      vReadFromBuffer(pCurrentEntry);
      if((*pu32Mask = u32CheckCondition(pCurrentEntry,pCurrentEntry->u32WaitMask,(OSAL_tenEventMaskFlag)pCurrentEntry->u32WaitEnFlags )) == 0)
      {
         if((pCurrentEntry->bTraceFlg == TRUE)&&(OSAL_E_TIMEOUT != u32ErrorCode))
         {
            vTraceEventInfo(pCurrentEntry,WAIT_OPERATION_STEP,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
         return u32ErrorCode;
      }
   }
#endif
   while(1)
   {
      s32Ret = read(phEvent->fifo_r,(char*)&Data[0],360*sizeof(tU32));
      if(s32Ret > 0)
      {
          /* check for call from OSAL_s32EventStatus */
          if(bRetFast == FALSE)
          {
             pCurrentEntry->bUpdate = TRUE;
          }
          /* calculate number of received events */
          u32Tmp = s32Ret/8;
          pCurrentEntry->u32Count -= u32Tmp;

          if(u32Tmp > 50)
          {
             TraceString("%s process %d events(data sets)",pCurrentEntry->szFileName,u32Tmp);
          }
#ifdef TACE_EVENT
          TraceString("%d Bytes -> %d Datasets received",s32Ret,u32Tmp);
#endif
          for(i= 0; i< u32Tmp;i++)
          {
#ifdef TACE_EVENT
             TraceString("Received mask:0x%x flag:%d ",Data[i*2],Data[(i*2)+1]);
#endif
             /* evaluate incoming signal and store related bitmask */
             u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,Data[i*2]/*mask*/,Data[(i*2)+1]/*Flag*/,0);
#ifdef FAST_RET
            if(bRetFast == TRUE)
            {
               *pu32Mask = u32CheckCondition(pCurrentEntry,pCurrentEntry->u32WaitMask,(OSAL_tenEventMaskFlag)pCurrentEntry->u32WaitEnFlags );
               /* check return condition fullfilled */
               if(*pu32Mask == 0)
               {
                  if((pCurrentEntry->bTraceFlg == TRUE)&&(OSAL_E_TIMEOUT != u32ErrorCode))
                  {
                     vTraceEventInfo(pCurrentEntry,WAIT_OPERATION_STEP,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
                  }
                  /* check for pending events , because we have to return */
                  u32Tmp = u32Tmp - 1 - i;
                  if(u32Tmp)
                  {
                     /* condition fullfilled but still posted events are available , so store them to pipe again*/
                     if(write(phEvent->fifo_w,(char*)&Data[(i+1)*2],8*u32Tmp) == -1)
                     {
                        /* write failed store to intermedeate buffer */
                        vWritePrintfErrmem("Write to fifo of Event:%s failed Handle:%d Error:%d \n",pCurrentEntry->szName,phEvent->fifo_w,errno);
                        vStoreToBuffer(pCurrentEntry,(char*)&Data[(i+1)*2],u32Tmp);
                     }
                     else
                     {
                        WRITE_LINE_INFO;
                        pCurrentEntry->u32Count += u32Tmp;
                     }
                     break;
                  }
               }
            }
#endif
         }// end for
         *pu32Mask = u32CheckCondition(pCurrentEntry,pCurrentEntry->u32WaitMask,(OSAL_tenEventMaskFlag)pCurrentEntry->u32WaitEnFlags );
         break;
      }
      else
      {
         /* check for read error on pipe */
         if(s32Ret == -1)
         {
            if(errno == EINTR)
            {
               TraceString("read %s error %d handle %d",pCurrentEntry->szFileName,errno,phEvent->fifo_r);
            }
            else
            {
               if((u32ErrorCode = u32ConvertErrorCore(errno)) == OSAL_E_TIMEOUT)
               {
                   pCurrentEntry->u32Count = 0;
               }
               /* check if data were available or a Event status request was happened */
               if(pCurrentEntry->u32Count)
               {
                  TraceString("read %s error %d handle %d entry:%d",pCurrentEntry->szFileName,errno,phEvent->fifo_r, pCurrentEntry->u32Count);
                  vWritePrintfErrmem("read %s error %d handle %d entry:%d \n",pCurrentEntry->szFileName,errno,phEvent->fifo_r, pCurrentEntry->u32Count);
               }
               break;
            }
         }
      }
   }
   return u32ErrorCode;
}

/*****************************************************************************
*
* FUNCTION: OSAL_s32EventWait
*
* DESCRIPTION: This function waits for an OSAL event to occur,
*              where an event occurrence means the link operation
*              (given by enFlags) between the EventField present
*              in the EventGroup structure and the provided EventMask
*              matches. Allowed link operation are AND/OR
*              So the event resets the calling thread only if within
*              the requested timeout one of the following conditions
*              is verified:
*                 EventMask || EventField is TRUE or
*                 EventMask && EventField is true
*              depending on the requested link operation
*
*
* PARAMETER:   hEvent (I)
*                 event handle.
*              mask (I)
*                 event mask.
*              enFlag (I)
*                 event flag.
*              msec (I)
*                 waiting time.
*              pResultMask (->O)
*                 pointer to the previous event mask.
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventWait(OSAL_tEventHandle      hEvent,
                       OSAL_tEventMask        mask,
                       OSAL_tenEventMaskFlag  enFlags,
                       OSAL_tMSecond          msec,
                       OSAL_tEventMask        *pResultMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
   OSAL_tEventMask u32Mask = 0;

   /* parameter check */
   if((hEvent) && (hEvent != OSAL_C_INVALID_HANDLE)
    &&((enFlags == OSAL_EN_EVENTMASK_AND)||(enFlags == OSAL_EN_EVENTMASK_OR) ))
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         if( (enFlags != OSAL_EN_EVENTMASK_AND) && (enFlags != OSAL_EN_EVENTMASK_OR) )
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
         }
         else if( pCurrentEntry->bWaitFlag == TRUE )
         {
            u32ErrorCode = OSAL_E_EVENTINUSE;
            NORMAL_M_ASSERT_ALWAYS();// indicate wrong API usage
         }
         else
         {
            pCurrentEntry->bWaitFlag = TRUE;
            pCurrentEntry->u32WaitMask    = mask;
            pCurrentEntry->u32WaitEnFlags = ( tU32 )enFlags;

            /* prepare epoll handling end*/
            /* step 1 check for first wait call */
            if(pCurrentEntry->s32RecProc == -1)
            {
               LockOsal( &pOsalData->EventTable.rLock );
               pCurrentEntry->s32RecTsk = OSAL_ThreadWhoAmI();
               pCurrentEntry->s32RecProc = OSAL_ProcessWhoAmI();
               /* check if event is already set */
               if(pCurrentEntry->bUpdate == TRUE)
               {
                  /* check if return condition is full filled */
                  u32Mask = u32CheckCondition(pCurrentEntry,mask,enFlags );
                  pCurrentEntry->bUpdate = FALSE;
#ifdef TRACE_EVENT
                  TraceString("Tsk:%d Event Wait bUpdate First Time PRC=%d 0x%x for %s \0",OSAL_ThreadWhoAmI(),OSAL_ProcessWhoAmI(),mask,pCurrentEntry->szName);
#endif
               }
#ifdef TRACE_EVENT
               else
               {
                  TraceString("Tsk:%d Event Wait first time PRC=%d 0x%x for %s \0",OSAL_ThreadWhoAmI(),OSAL_ProcessWhoAmI(),mask,pCurrentEntry->szName);
               }
#endif
               /* check  if Post message is called at the same time*/
               if(pCurrentEntry->bPost == TRUE)
               {
                   pCurrentEntry->bUpdate = TRUE;
               }
               UnLockOsal( &pOsalData->EventTable.rLock );
            }// end if(pCurrentEntry->s32RecProc == -1)

            /* when receiver has cleared the event or posted an event self */
            if((pCurrentEntry->bUpdate == TRUE)||(pCurrentEntry->u32EventBitField))
            {
                  /* check if return condition is full filled */
                  u32Mask = u32CheckCondition(pCurrentEntry,mask,enFlags );
                  pCurrentEntry->bUpdate = FALSE;
            }
            /* check for pipe descriptor start*/
            if(((tEvHandle*)hEvent)->fifo_r == -1)
            {
              if(((((tEvHandle*)hEvent)->fifo_r=open((char*)pCurrentEntry->szFileName, O_RDONLY | O_NONBLOCK)) == -1)
               &&((((tEvHandle*)hEvent)->fifo_w=open((char*)pCurrentEntry->szFileName, O_WRONLY)) == -1))
              {
#ifdef TACE_EVENT
                  TraceString("open((char*)pCurrentEntry->szFileName, O_RDONLY | O_NONBLOCK ) failed");
#endif
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  WRITE_LINE_INFO;
              }
#ifdef TACE_EVENT
              else
              {
                  TraceString("open(%s, O_RDONLY | O_NONBLOCK succeeded %d ",pCurrentEntry->szName,((tEvHandle*)hEvent)->fifo);
              }
#endif
#ifdef EPOLL
              if(u32ErrorCode == OSAL_E_NOERROR)
              {
                 struct epoll_event events;
                 events.events = EPOLLIN;
                 pCurrentEntry->hLock = epoll_create1(EPOLL_CLOEXEC);
                 if(pCurrentEntry->hLock == -1)
                 {
                    u32ErrorCode = u32ConvertErrorCore(errno);
#ifdef TACE_EVENT
                    TraceString("open(%s fifo:%d epoll_create1 failed errno%d ",pCurrentEntry->szName,((tEvHandle*)hEvent)->fifo,errno);
#endif
                    FATAL_M_ASSERT_ALWAYS();
                 }
                 else
                 {
                    if(s32OsalGroupId)
                    {
                       if(fchown(pCurrentEntry->hLock,-1,s32OsalGroupId) == -1)
                       {
                          vWritePrintfErrmem("OSAL_s32EventWait -> fchown error %d \n",errno);
                       }
                       if(fchmod(Pool, OSAL_ACCESS_RIGTHS) == -1)
                       {
                          vWritePrintfErrmem("OSAL_s32EventWait -> fchmod error %d \n",errno);
                       }
                    }
                 }
                 if (epoll_ctl(pCurrentEntry->hLock, EPOLL_CTL_ADD,((tEvHandle*)hEvent)->fifo, &events) == -1)
                 {
                    u32ErrorCode = u32ConvertErrorCore(errno);
#ifdef TACE_EVENT
                    TraceString("open(%s fifo:%d epoll_ctl failed errno%d ",pCurrentEntry->szName,((tEvHandle*)hEvent)->fifo,errno);
#endif
                    FATAL_M_ASSERT_ALWAYS();
                 }
              }
#endif
            }//end if(((tEvHandle*)hEvent)->fifo_r == -1)
            if(u32Mask == 0)
            { 
               tU32 u32CurTime =0;
               if(msec != OSAL_C_TIMEOUT_FOREVER)
               {
                   u32CurTime = OSAL_ClockGetElapsedTime();
               }
               /* start while loop until mask condition reached */
               do
               {
#ifdef EPOLL
                  struct epoll_event events;
                  ev.data.fd = (int)((tEvHandle*)hEvent)->fp;
#else
                  struct pollfd pollfd_struc;
                  pollfd_struc.fd=((tEvHandle*)hEvent)->fifo_r;
                  pollfd_struc.events=POLLIN;
#endif
#ifndef NO_SIGNALAWARENESS
                  /* start while loop to handle other diturbing signals */
                  while(1)
                  {
#endif

#ifdef EPOLL
                     s32ReturnValue = epoll_wait(pCurrentEntry->hLock, &events,1,msec);
#else 
                     s32ReturnValue = poll(&pollfd_struc,1,msec);
#endif
#ifdef TACE_EVENT
                     TraceString("epoll %s returns %d ",pCurrentEntry->szName,s32ReturnValue);
#endif
#ifndef NO_SIGNALAWARENESS
                     if(s32ReturnValue == 0)
                     {
                        /* timeout completion leave while loop */
                        u32ErrorCode = OSAL_E_TIMEOUT;
                        break;
                     }
                     else if(s32ReturnValue > 0)
                     {
                        /* normal completion leave while loop */
                        if(pCurrentEntry->u32Count > 0)
                        {
                             /* data set available leave function */
                             break;
                        }
                        else
                        {
                           /* race condition no data set available*/
                           if(msec != OSAL_C_TIMEOUT_FOREVER)
                           {
                               /* wait again*/
                           }
                           else
                           {
                              if(msec != OSAL_C_TIMEOUT_NOBLOCKING)
                              {
                                 /* calculate new wait time and wait again*/
                                 /* look if requested timeout really happend */
                                 tU32 u32NewTime = OSAL_ClockGetElapsedTime();
                                 if((u32NewTime - u32CurTime) < msec)
                                 {
                                    msec = msec -(u32NewTime - u32CurTime);
                                 }
                              }
                              else
                              {
                                 break;
                              }
                           }
                        }
                     }
                     else
                     {
                        /* check if interrupt signal has interrupted the wait*/
                        if(errno != EINTR)
                        {
                           u32ErrorCode = u32ConvertErrorCore(errno);
                           break;
                        }
                     }
                  }// end while
#endif
                  if((u32ErrorCode == OSAL_E_NOERROR)
#ifdef EPOLL
                   && (events.events == EPOLLIN)
#endif
                    )
                  {
#ifdef TACE_EVENT
                     TraceString("EPOLLIN");
#endif
                     /* read data from pipe and return as soon as wait condition is reached */
                     if((u32ErrorCode = u32ReadDataFromPipe((tEvHandle*)hEvent,pCurrentEntry,&u32Mask,TRUE)) == OSAL_E_TIMEOUT)
                     {
                         /* race condition */
                         if(msec == OSAL_C_TIMEOUT_FOREVER)
                         {
                            /* force new wait */
                            u32ErrorCode = OSAL_E_UNKNOWN;
                         }
                         else
                         {
                             if(msec != OSAL_C_TIMEOUT_NOBLOCKING)
                             {
                                /* look if requested timeout really happend */
                                tU32 u32NewTime = OSAL_ClockGetElapsedTime();
                                if((u32NewTime - u32CurTime) < msec)
                                {
                                   msec = msec -(u32NewTime - u32CurTime);
                                   /* force new wait */
                                   u32ErrorCode = OSAL_E_UNKNOWN;
                                }
                             }
                         }
                     }
                  }
               } while( (0 == u32Mask) && (OSAL_E_TIMEOUT != u32ErrorCode) );
            }// if(u32Mask == 0)
           
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               s32ReturnValue = (tS32)OSAL_OK;
               if( pResultMask != NULL )
               {
                  *pResultMask = pCurrentEntry->u32WaitMask & pCurrentEntry->u32EventBitField;
               }
            }
            pCurrentEntry->bWaitFlag = FALSE;
         }
      }
      else //if( pCurrentEntry )
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE; // todo OSAL_E_DOESNOTEXIST?
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( u32ErrorCode != OSAL_E_NOERROR )
   {
      s32ReturnValue = OSAL_ERROR;
      vSetErrorCode( OSAL_C_THREAD_ID_SELF, u32ErrorCode);
      if(u32ErrorCode != OSAL_E_TIMEOUT)
      {
         tCString coszName;
         if(pCurrentEntry)coszName = pCurrentEntry->szName;
         else coszName = "NO_NAME";
         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
      }
      else
      {
         if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE))
         {
            vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
      }
   }
   else
   {
      s32ReturnValue = OSAL_OK;
      if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE ))
      {
         vTraceEventInfo(pCurrentEntry,WAIT_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
      }
   }

   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventPost
*
* DESCRIPTION: this function set an event in OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventPost( OSAL_tEventHandle hEvent,
                        OSAL_tEventMask   mask,
                        OSAL_tenEventMaskFlag enFlags )
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;
   trEventElement *pCurrentEntry = NULL;
   tU32 Data[2] = {mask,(tU32)enFlags};
   tS32 s32Tsk = OSAL_ThreadWhoAmI();

   /* parameter check */
   if( (hEvent) && (hEvent != OSAL_C_INVALID_HANDLE) )
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         pCurrentEntry->u32PostMask = mask;
         pCurrentEntry->u32PostFlag = enFlags;
         /* check for valid pipe descriptor */

         /* check for no active receiver task , OSAL_s32EventWait was not called yet */
         if(pCurrentEntry->s32RecProc == -1) 
         {
            pCurrentEntry->bPost = TRUE;
            /* store event mask info and Lock, because we don' t know if wait 
               is called by receiver task at the same time*/
            LockOsal( &pOsalData->EventTable.rLock );
            /* check if in OSAL_s32EventWait s32RecProc is set before lock was taken*/
            if(pCurrentEntry->s32RecProc == -1)
            {
#ifdef TRACE_EVENT
               TraceString("Tsk:%d 0x%x store for %s \0",s32Tsk, mask,pCurrentEntry->szName);
#endif
               u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,mask,0,enFlags );
               pCurrentEntry->bUpdate = TRUE;
            }
#ifdef TRACE_EVENT
            else
            {
               TraceString("Tsk:%d  0x%x not stored for %s \0",s32Tsk, mask,pCurrentEntry->szName);
            }
#endif
            pCurrentEntry->bPost = FALSE;
            UnLockOsal( &pOsalData->EventTable.rLock );
            s32ReturnValue = OSAL_OK;
         }
         else if(pCurrentEntry->s32RecTsk == s32Tsk)/* check for clear event */
         {
            /* Lock not needed, because we know post is called by receiver task */
            u32ErrorCode = u32SignalSetBitFields(pCurrentEntry,mask,0,enFlags );
            pCurrentEntry->bUpdate = TRUE; //for check of an event sent to myself*/
            s32ReturnValue = OSAL_OK;
         }
         else 
         {
            if(((tEvHandle*)hEvent)->fifo_w == -1)
            {
               /* No O_NONBLOCK flag, because write() shall block the calling thread until the data can be accepted */
               if((((tEvHandle*)hEvent)->fifo_w=open((char*)pCurrentEntry->szFileName, O_WRONLY)) == -1)
               {
#ifdef TACE_EVENT
                  TraceString("open((char*)pCurrentEntry->szFileName, O_WRONLY ) failed Error:%d",errno);
#endif
                  u32ErrorCode = u32ConvertErrorCore(errno);
                  WRITE_LINE_INFO;
               }
#ifdef TACE_EVENT
               else
               {
                  TraceString("open(%s , O_WRONLY )succeeded %d ",pCurrentEntry->szName,((tEvHandle*)hEvent)->fifo);
               }
#endif
            }
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
#ifdef TACE_EVENT
               TraceString("write %s handle %d Send mask:0x%x flag:%d",pCurrentEntry->szName,((tEvHandle*)hEvent)->fifo,Data[0],Data[1]);
#endif
               if(write(((tEvHandle*)hEvent)->fifo_w,(char*)&Data[0],2*sizeof(tU32)) != -1)
               {
                   pCurrentEntry->u32Count++;
                   s32ReturnValue = OSAL_OK;
               }
               else
               {
#ifdef TACE_EVENT
                  TraceString("write event %s failed errno %d ",pCurrentEntry->szName,errno);
#endif
                  WRITE_LINE_INFO;
                  u32ErrorCode = u32ConvertErrorCore(errno);
               }
            }
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_UNKNOWN;
         FATAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE ))
      {
         if(pOsalData->bEvTaceAllWithoutTimeout)
         {
            if((mask == 0xffffffff)&&(enFlags == OSAL_EN_EVENTMASK_AND))
            {
                /* clear of a timeout is happened */
            }
            else
            {
               vTraceEventInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
            }
         }
         else
         {
            vTraceEventInfo(pCurrentEntry,POST_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
         }
      }
   }
   return( s32ReturnValue );
}


/*****************************************************************************
*
* FUNCTION: OSAL_s32EventStatus
*
* DESCRIPTION: this function returns the status of an OSAL event group.
*
* PARAMETER:
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 OSAL_s32EventStatus(OSAL_tEventHandle hEvent,
                         OSAL_tEventMask   mask,
                         OSAL_tEventMask   *pMask)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   tU32 u32ErrorCode   = OSAL_E_NOERROR;


   trEventElement *pCurrentEntry = NULL;
   tU32 u32Mask = 0;

   if( (hEvent) && (hEvent != OSAL_C_INVALID_HANDLE) )
   {
      pCurrentEntry = ((tEvHandle*)hEvent)->pEntry;
      if((pCurrentEntry != OSAL_NULL)&&(pCurrentEntry->bIsUsed)&&(pCurrentEntry->u32EventID == LINUX_C_U32_EVENT_ID))
      {
         /* if somebody is polling on the Event */
         if(pCurrentEntry->u32Count)
         {
            /* valid read handle needed */
            if(((tEvHandle*)hEvent)->fifo_r == -1)
            {
               if((((tEvHandle*)hEvent)->fifo_r=open((char*)pCurrentEntry->szFileName, O_RDONLY | O_NONBLOCK )) == -1)
               {
                  WRITE_LINE_INFO;
                  u32ErrorCode = u32ConvertErrorCore(errno);
               }
            }
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
               u32ErrorCode = u32ReadDataFromPipe((tEvHandle*)hEvent,pCurrentEntry,&u32Mask,FALSE);
            }
         }
         *pMask = (mask & pCurrentEntry->u32EventBitField);
         s32ReturnValue = OSAL_OK;
      }
      else
      {
        s32ReturnValue = OSAL_ERROR;
        u32ErrorCode   = OSAL_E_DOESNOTEXIST;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if (u32ErrorCode != OSAL_E_NOERROR)
   {
      vSetErrorCode(OSAL_C_THREAD_ID_SELF,u32ErrorCode);
      tCString coszName;
      if(pCurrentEntry)coszName = pCurrentEntry->szName;
      else coszName = "NO_NAME";
      vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,TR_LEVEL_FATAL,u32ErrorCode,coszName);
   }
   else
   {
      if((pCurrentEntry)&&(pCurrentEntry->bTraceFlg == TRUE ))
      {
         vTraceEventInfo(pCurrentEntry,STATUS_OPERATION,/*TR_LEVEL_USER_1*/TR_LEVEL_FATAL,u32ErrorCode,pCurrentEntry->szName);
      }
   }
   return( s32ReturnValue );
}




#ifdef __cplusplus
}
#endif

#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
