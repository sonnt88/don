/*****************************************************************************
| FILE:			osalevent_utest.cpp
| PROJECT:		platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:	 (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date		| Modification					| Author
| 03.10.05  | Initial revision			  | Carsten Resch
| --.--.--  | ----------------			  | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;

#define SEMAPHORE_START_VALUE			  1
#define SEMAPHORE_START_VALUE_MAX	  65535	 
#define SEMAPHORE_NAME						 "Semaphore"
#define GLOBAL_SEMAPHORE_START_VALUE  1
#define GLOBAL_SEMAPHORE_START_VALUE_ 3
#define GLOBAL_SEMAPHORE_NAME			  "GSemaphore"
#define GLOBAL_SEMAPHORE_NAME_		  "Mutex"
#ifdef OSAL_SHM_MQ
#define SEMAPHORE_NAME_MAX				"Semaphore with name equal to   33"
#define SEMAPHORE_NAME_EXCEEDS_MAX		"Semaphore with name exceeding 33   characters"
#else
#define SEMAPHORE_NAME_MAX				"Semaphore with name equal to 31"
#define SEMAPHORE_NAME_EXCEEDS_MAX		"Semaphore with name exceeding 31 characters"
#endif
#define SEMTEXT								  OSAL_C_STRING_DEVICE_FFS"/semtext.txt"
#define THIRTY_MILLISECONDS			  30
#define ONE_SECOND						  1000
#define TWO_SECONDS						 2000
#define THREE_SECONDS					  3000
#define FIVE_SECONDS						5000
#define STRESS_WAIT						 10000
#define SEMAPHORE_COUNT_INVALID		 ((tS32)-1)

#undef	  INIT_VAL

#define INIT_VAL							 (tU8)0x00 // Binary : 0000 0000
#define VALID_VAL							(tU8)0x07	 // Binary : 0000 0111
#define CF1_MASK							 (tU8)0x01 // Binary : 0000 0001
#define CF2_MASK							 (tU8)0x02	 // Binary : 0000 0010
#define CF3_MASK							 (tU8)0x04	 // Binary : 0000 0100
#define MAX_SEMAPHORES					 (tU8)25
#define NO_OF_POST_THREADS				10
#define NO_OF_WAIT_THREADS				15
#define NO_OF_CLOSE_THREADS			  1
#define NO_OF_CREATE_THREADS			 2

#undef  SRAND_MAX
#define SRAND_MAX							(tU32)65535
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX							 0x7FFFFFFF
#undef	  MAX_LENGTH
#define MAX_LENGTH						  256
#define TOT_THREADS						 (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)


/*Data Types*/
typedef struct SemaphoreTableEntry_
{
	 OSAL_tSemHandle hSem;
	 tChar hSemName[MAX_LENGTH];
	 tBool hToBeDeleted;
}SemaphoreTableEntry;

static OSAL_tSemHandle globalsemHandle	  = 0;
static OSAL_tSemHandle globalsemHandle_	 = 0;
static tU8  u8Byte								 = INIT_VAL;



/*ThreadWaitPost*/
tVoid ThreadSemTest(const tVoid *pvArg )
{
	tChar ThreadBuffer[10];
	tU8 u8Count			 = 100;

	  
	memset( ThreadBuffer,'\0',10 );
	(tVoid)OSAL_szStringCopy( ThreadBuffer,"Threaddat" );

	while( u8Count-- )
	{
	 EXPECT_NE(OSAL_ERROR,OSAL_s32IOWrite(*((OSAL_tIODescriptor*)pvArg),(tPCS8)ThreadBuffer,10));
	}	 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(globalsemHandle));
	/*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}


tVoid ControlFlow_1( tVoid *pvArg )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	/*Wait on semaphore*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	/*Wait for 2 seconds*/
	OSAL_s32ThreadWait( TWO_SECONDS);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
	u8Byte |= CF1_MASK;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(globalsemHandle_));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(globalsemHandle));
	OSAL_s32ThreadWait(FIVE_SECONDS);
}

tVoid ControlFlow_2( tVoid *pvArg )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	 /*Wait for 2 seconds*/
	OSAL_s32ThreadWait( TWO_SECONDS );
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
	u8Byte |= CF2_MASK;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalsemHandle_));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalsemHandle));
	OSAL_s32ThreadWait( FIVE_SECONDS);
}

/*Thread 3*/
tVoid ControlFlow_3( tVoid *pvArg )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	OSAL_s32ThreadWait( TWO_SECONDS );
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
	u8Byte |= CF3_MASK;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalsemHandle_));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalsemHandle));
	OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*********************************************************************/
/* Test Cases for OSALSemaphore API											 */
/*********************************************************************/

TEST(OsalCoreSemaphoreTest, u32CreateCloseDelSemVal)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}


TEST(OsalCoreSemaphoreTest, u32CreateSemNameNULL)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreCreate(OSAL_NULL,&semHandle,SEMAPHORE_START_VALUE));
}

TEST(OsalCoreSemaphoreTest, u32CreateSemHandleNULL)
{
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,OSAL_NULL,SEMAPHORE_START_VALUE));
}

TEST(OsalCoreSemaphoreTest, u32CreateCloseDelSemNameMAX)
{
	OSAL_tSemHandle semHandle	  = 0;
	/*Create the Semaphore within the system*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME_MAX, &semHandle, SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete( SEMAPHORE_NAME_MAX));
}


TEST(OsalCoreSemaphoreTest, u32CreateSemWithNameExceedsMAX)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_ERROR ,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME_EXCEEDS_MAX, &semHandle, SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}


TEST(OsalCoreSemaphoreTest, u32CreateTwoSemSameName)
{
	OSAL_tSemHandle semHandle1	 = 0;
	OSAL_tSemHandle semHandle2	 = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle1,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle2,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle1));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}

TEST(OsalCoreSemaphoreTest, u32DelSemWithNULL)
{
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreDelete(OSAL_NULL));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32DelSemWithNonExistingName)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	/*Scenario 1*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
	/*Scenario 2*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreDelete("WhoAmI"));
	EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode()); 
}


TEST(OsalCoreSemaphoreTest, u32DelSemCurrentlyUsed)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
}

TEST(OsalCoreSemaphoreTest, u32OpenSemWithNameNULL)
{
	OSAL_tSemHandle semHandle	  = 0;
	OSAL_tSemHandle semHandle_	 = 0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen(OSAL_NULL,&semHandle));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));

	/*Scenario 2*/
	/*Open the semaphore passing name as OSAL_NULL, on a handle that was never used for create/open in the system*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen(OSAL_NULL,&semHandle_));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32OpenSemWithHandleNULL)
{
	OSAL_tSemHandle semHandle	  = 0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose( semHandle));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen( SEMAPHORE_NAME,OSAL_NULL));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	/*Scenario 2*/
	/*Open the semaphore passing handle as OSAL_NULL, on a semaphore name that was never used for create/open in the system*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen( "Unknown",OSAL_NULL));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32OpenSemWithNameExceedsMAX)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen( SEMAPHORE_NAME_EXCEEDS_MAX,&semHandle));
	EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32OpenSemWithNonExistingName)
{
	OSAL_tSemHandle semHandle	  = 0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen(SEMAPHORE_NAME,&semHandle));
	EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
	/*Scenario 2*/
	/*Open the semaphore, on a semaphore name that was never used for create/open in the system*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreOpen( "NeverKnownToSystem",&semHandle));
	EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32CloseSemWithInvalidHandle)
{
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32WaitPostSemValid)
{
	tU8  u8MainCount						= 100;
	OSAL_tIODescriptor FDsemtext			= 0;
	OSAL_trThreadAttribute trAttr	  = {OSAL_NULL};
	tChar MainThreadBuffer[10];
  
	globalsemHandle = 0;
	memset( MainThreadBuffer,'\0',10 );
	(tVoid)OSAL_szStringCopy( MainThreadBuffer,"MainThdat" );

	FDsemtext = OSAL_IOCreate( SEMTEXT, OSAL_EN_READWRITE );
	EXPECT_NE(OSAL_ERROR,FDsemtext);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( GLOBAL_SEMAPHORE_NAME,&globalsemHandle,GLOBAL_SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	
	trAttr.szName		 = (char*)"SemaphoreWPTest";
	trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize	 = 2048;/*2MB stack*/
	trAttr.pfEntry		= (OSAL_tpfThreadEntry)ThreadSemTest;
	trAttr.pvArg		  = &FDsemtext;

	EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	/*Write into file in Ramdisk from main thread now*/
	while( u8MainCount-- )
	{
		EXPECT_NE(OSAL_ERROR,OSAL_s32IOWrite( FDsemtext,(tPCS8)MainThreadBuffer,10 ));
	}
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(globalsemHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(GLOBAL_SEMAPHORE_NAME));

	/*Close the file on Ramdisk*/
	EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(FDsemtext));
	EXPECT_EQ(OSAL_OK,OSAL_s32IORemove( SEMTEXT));
}

TEST(OsalCoreSemaphoreTest, u32WaitSemWithInvalidHandle)
{
	OSAL_tSemHandle semHandle	  = 0;
	OSAL_tSemHandle semHandle_	 = 0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait( semHandle,TWO_SECONDS));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	/*Scenario 2*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait( semHandle_,TWO_SECONDS));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32ReleaseSemWithInvalidHandle)
{
	OSAL_tSemHandle semHandle	  =  0;
	OSAL_tSemHandle semHandle_	 =  0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	/*Post on a non existent handle(invalidated)*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	/*Scenario 2*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphorePost(semHandle_));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreSemaphoreTest, u32GetRegularSemValue)
{
	tS32 s32Count					  = SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue(semHandle,&s32Count));
	/*Do a simple wait on semaphore - Take one resource instance*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( semHandle,OSAL_C_TIMEOUT_FOREVER));
	s32Count = SEMAPHORE_COUNT_INVALID;/*Invalidate the previous count value*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue( semHandle,&s32Count));
	EXPECT_EQ(0,s32Count);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	s32Count = SEMAPHORE_COUNT_INVALID;/*Invalidate the previous count value*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue( semHandle,&s32Count));
	EXPECT_EQ(SEMAPHORE_START_VALUE,s32Count);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}

TEST(OsalCoreSemaphoreTest, u32GetSemValueInvalidHandle)
{
	tS32 s32Count					  =  SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle	  =  0;
	OSAL_tSemHandle semHandle_	 =  0;
	/*Scenario 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreGetValue(semHandle,&s32Count));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	/*Scenario 2*/
	s32Count =	  SEMAPHORE_COUNT_INVALID;/*Invalidate the count value*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreGetValue( semHandle_,&s32Count));
	EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

#ifdef CHECK_TEST
TEST(OsalCoreSemaphoreTest, u32WaitPostSemValidExceedsMAX)
{
	tS32 s32Count				 = SEMAPHORE_COUNT_INVALID;
	OSAL_tSemHandle semHandle = 0;
	 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE_MAX));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue(semHandle,&s32Count));
	EXPECT_EQ(SEMAPHORE_START_VALUE_MAX,s32Count);
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_E_QUEUEFULL,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}
#endif
		  
TEST(OsalCoreSemaphoreTest, u32AccessSemMpleThreads)
{
	tS32 s32Count				 = SEMAPHORE_COUNT_INVALID;
	tS32 s32loop						  = 0;
	OSAL_trThreadAttribute trAttr_1 = {OSAL_NULL};
	OSAL_trThreadAttribute trAttr_2 = {OSAL_NULL};
	OSAL_trThreadAttribute trAttr_3 = {OSAL_NULL};
	 
	globalsemHandle  = 0;
	globalsemHandle_ = 0;
	u8Byte = INIT_VAL;

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(GLOBAL_SEMAPHORE_NAME_,&globalsemHandle_,GLOBAL_SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(GLOBAL_SEMAPHORE_NAME,&globalsemHandle,GLOBAL_SEMAPHORE_START_VALUE_));		  
	
	trAttr_1.szName		 = (char*)"ControlFlow_1";
	trAttr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr_1.s32StackSize = 2048;/*2MB stack*/
	trAttr_1.pfEntry		= (OSAL_tpfThreadEntry)ControlFlow_1;
	trAttr_1.pvArg		  = OSAL_NULL;
	trAttr_2.szName		 = (char*)"ControlFlow_2";
	trAttr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr_2.s32StackSize = 2048;/*2MB stack*/
	trAttr_2.pfEntry		= (OSAL_tpfThreadEntry)ControlFlow_2;
	trAttr_2.pvArg		  = OSAL_NULL;
	trAttr_3.szName		 = (char*)"ControlFlow_3";
	trAttr_3.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr_3.s32StackSize = 2048;/*2MB stack*/
	trAttr_3.pfEntry		= (OSAL_tpfThreadEntry)ControlFlow_3;
	trAttr_3.pvArg		  = OSAL_NULL;

	EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_1));
	EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_2));
	EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_3));
	/*Busy wait*/
	do
	{
		 /*Set semaphore value*/
		 s32Count =  SEMAPHORE_COUNT_INVALID;
		 OSAL_s32ThreadWait( TWO_SECONDS );
		 EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue( globalsemHandle,&s32Count));
		 s32loop++;
	}while( GLOBAL_SEMAPHORE_START_VALUE_ != s32Count && s32loop < 10);						  
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(globalsemHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(GLOBAL_SEMAPHORE_NAME));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(globalsemHandle_));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(GLOBAL_SEMAPHORE_NAME_));
	EXPECT_EQ(VALID_VAL ,u8Byte);
}

TEST(OsalCoreSemaphoreTest, u32NoBlockingSemWait)
{
	tS32 s32SemCount			 = SEMAPHORE_COUNT_INVALID; 
	OSAL_tSemHandle semHandle = 0; 

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle, OSAL_C_TIMEOUT_NOBLOCKING));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue(semHandle,&s32SemCount));
	EXPECT_EQ(INIT_VAL,s32SemCount);
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait(semHandle, OSAL_C_TIMEOUT_NOBLOCKING));
	EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}

TEST(OsalCoreSemaphoreTest, u32BlockingDurationSemWait)
{
	OSAL_tSemHandle semHandle  = 0;
	OSAL_tMSecond u32StartTime = 0;  
	OSAL_tMSecond u32EndTime	= 0;  
	OSAL_tMSecond u32Duration  = 0;  

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME,&semHandle,0 ));
	u32StartTime = OSAL_ClockGetElapsedTime();
	/*Wait on the Semaphore with semaphore count as zero and wait time as one second*/
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait( semHandle,ONE_SECOND));
	u32EndTime = OSAL_ClockGetElapsedTime();
	EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

	if( u32EndTime > u32StartTime )
	{
		/*Calculate duration of wait*/
		u32Duration = u32EndTime - u32StartTime;
		EXPECT_LE(( ONE_SECOND - THIRTY_MILLISECONDS ),u32Duration);
		EXPECT_LE(u32Duration,(ONE_SECOND + THIRTY_MILLISECONDS));
	}
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}


TEST(OsalCoreSemaphoreTest, u32WaitPostSemAfterDel)
{
	OSAL_tSemHandle semHandle  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(SEMAPHORE_NAME, &semHandle,SEMAPHORE_START_VALUE));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
	/*Wait on the Semaphore - Should pass*/	 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_NOBLOCKING));
	EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
}


TEST(OsalCoreSemaphoreTest, u32RecursiveLock)
{
	OSAL_tSemHandle semHandle  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate_Opt(SEMAPHORE_NAME, &semHandle,1,1));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
  
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}


tVoid ThreadSemWait(const tVoid *pvArg )
{
	OSAL_tSemHandle semHandle  = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreOpen(SEMAPHORE_NAME, &semHandle));
	EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreWait(semHandle,1000)); 
	EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
}

TEST(OsalCoreSemaphoreTest, u32RecursiveLock2)
{
	OSAL_tSemHandle semHandle  = 0;

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate_Opt(SEMAPHORE_NAME, &semHandle,1,1));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait(semHandle,OSAL_C_TIMEOUT_FOREVER)); 

	OSAL_s32ThreadWait( TWO_SECONDS );

	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost(semHandle));
  
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(SEMAPHORE_NAME));
}


/*ThreadWaitPost*/
tVoid ThreadSemSpeicialTest(const tVoid *pvArg )
{
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
	/*Delay the ThreadExit call in the wrapper function*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}



TEST(OsalCoreSemaphoreTest, u32WaitPostDelSemValid)
{
	OSAL_tIODescriptor FDsemtext			= 0;
	OSAL_trThreadAttribute trAttr	  = {OSAL_NULL};
  
	globalsemHandle = 0;
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( GLOBAL_SEMAPHORE_NAME,&globalsemHandle,GLOBAL_SEMAPHORE_START_VALUE));
	
	trAttr.szName		 = (char*)"SemaphoreWPTest";
	trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
	trAttr.s32StackSize	 = 2048;/*2MB stack*/
	trAttr.pfEntry		= (OSAL_tpfThreadEntry)ThreadSemSpeicialTest;
	trAttr.pvArg		  = &FDsemtext;

	EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));

	OSAL_s32ThreadWait(800);
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalsemHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(globalsemHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(GLOBAL_SEMAPHORE_NAME));

}
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
