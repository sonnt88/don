/*****************************************************************************
| FILE:         osaltimer_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal timer implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;

#define EVENT_TIMER_MT         0x00000001
#define MAX_TIMER_CALLBACK_CHECK 12
#define CALLBACK_WAIT_TIME        1
#define DUMMY_WAIT                10
#define INVAL_VALUE               ((tS32)-1)

#define DELAY               	  2000
#define WAIT_FOR_DELETE           4000
#define LOAD_WAIT                 1
#define EIGHT_MILLI_SECONDS       8
#define TEN_MILLI_SECONDS         10
#define TWENTY_MILLI_SECONDS      20
#define PHASE_TIME         		  300
#define CYCLE_TIME         		  900

#undef  EVENT_NAME

#define EVENT_NAME                "Event_Timer"
#define TIMER_ONE_EVENT           0x00000001
#define TIMER_TWO_EVENT           0x00000002
#undef  EXCEPTION
#define EXCEPTION                 0
#undef  MUTEX
#define MUTEX                     "Mutex"
#define TOLERANCE_TIME            25
#undef  MAX_LENGTH
#define MAX_LENGTH                256
#define CALLBACK_COUNT            150
#define LOAD                      30
#define WAIT_TIME                 3000

/*****************************************************************
| Global Variables
|----------------------------------------------------------------*/
static OSAL_tTimerHandle tHandle_1            = 0;
static OSAL_tTimerHandle tHandle_2            = 0;
static OSAL_tEventHandle globalevHandle       = 0;
static OSAL_tSemHandle   globalTimersemHandle = 0;
static tU32 u32Counter                        = 1;
static tU32 u32Count                          = 0;
static tU32 u32CallBack_count                 = 0; //Used to check the timer call back execution
static trOsalLock   TimerLimitCheckLock ;



static OSAL_tSemHandle globalIndexsemHandle   = 0;	

static OSAL_tTimerHandle tmHandleArray[LOAD]    = {0};
static tU32 u32CounterArray[LOAD]               = {0};
static tU32 u32StartElapsedTime[LOAD]           = {0};
static tU32 u32EndElapsedTime[LOAD]             = {0};
static tS32 s32Drift[LOAD]                      = {0};
static tU8  u8LimitCheck[LOAD]                  = {0};
static tU32 globIndex                           = 0;
static OSAL_tMSecond u32StartTime               = 0;
static OSAL_tMSecond u32EndTime                 = 0;

/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/
void vCheckTimeDiff(tU32 tU32Value, tU32 _u32StartTime, tU32 _u32EndTime)
{
   EXPECT_LT(_u32EndTime, _u32StartTime);
   EXPECT_LT((_u32EndTime - _u32StartTime),tU32Value);
   EXPECT_GT((_u32EndTime - _u32StartTime),(tU32Value + 100));
}

void vPrintTime()
{
   OSAL_trTimeDate rtcGetParam = { 0 };

   EXPECT_EQ(OSAL_OK,OSAL_s32ClockGetTime(&rtcGetParam));
}

void oedt_vChangeTimeCallback(void* u32Idx)
{
   u32EndTime = OSAL_ClockGetElapsedTime();
   vPrintTime();
}

void vChangeTimeForward(tU32 u32Seconds)
{
   OSAL_trTimeDate rtcSetParam = { 0 };
   OSAL_trTimeDate rtcGetParam = { 0 };

   /*Initialize Structure*/
   rtcSetParam.s32Second = u32Seconds;
   rtcSetParam.s32Minute = 0;
   rtcSetParam.s32Hour = 10;
   rtcSetParam.s32Day = 1;
   rtcSetParam.s32Month = 1;
   rtcSetParam.s32Year = 111;
   rtcSetParam.s32Daylightsaving = 0;
   rtcSetParam.s32Weekday = 0;
   rtcSetParam.s32Yearday = 1;
   rtcSetParam.s32Daylightsaving = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32ClockSetTime(&rtcSetParam));
   EXPECT_EQ(OSAL_OK,OSAL_s32ClockGetTime(&rtcGetParam));


   EXPECT_NE(rtcGetParam.s32Hour,10);
   EXPECT_NE(rtcGetParam.s32Day,1); 
   EXPECT_NE(rtcGetParam.s32Month,1);
   EXPECT_NE(rtcGetParam.s32Second,10);
   EXPECT_NE(rtcGetParam.s32Year,111);
}

void vSetTime(tVoid)
{
   OSAL_trTimeDate rtcSetParam = { 0 };
   OSAL_trTimeDate rtcGetParam = { 0 };

   /*Initialize Structure*/
   rtcSetParam.s32Second = 0;
   rtcSetParam.s32Minute = 0;
   rtcSetParam.s32Hour = 10;
   rtcSetParam.s32Day = 1;
   rtcSetParam.s32Month = 1;
   rtcSetParam.s32Year = 111;
   rtcSetParam.s32Daylightsaving = 0;
   rtcSetParam.s32Weekday = 0;
   rtcSetParam.s32Yearday = 1;
   rtcSetParam.s32Daylightsaving = 0;

   EXPECT_EQ(OSAL_OK ,OSAL_s32ClockSetTime(&rtcSetParam));
   EXPECT_EQ(OSAL_OK ,OSAL_s32ClockGetTime(&rtcGetParam));
 
   EXPECT_NE(rtcGetParam.s32Hour, 10);
   EXPECT_NE(rtcGetParam.s32Day , 1) ;
   EXPECT_NE(rtcGetParam.s32Month ,1);
   EXPECT_NE(rtcGetParam.s32Year,111);
}

void vCreateTimer(tVoid)
{
   OSAL_tTimerHandle hTimer;

   EXPECT_EQ(OSAL_OK ,OSAL_s32TimerCreate(oedt_vChangeTimeCallback, 0, &hTimer));
   u32StartTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_OK ,OSAL_s32TimerSetTime(hTimer, (OSAL_tMSecond) (10*1000), 0));
   vPrintTime();
   vChangeTimeForward(5);
   vPrintTime();
   OSAL_s32ThreadWait(12 * 1000);
   vPrintTime();
   vCheckTimeDiff(10*1000, u32StartTime, u32EndTime);
   EXPECT_EQ(OSAL_OK ,OSAL_s32TimerDelete(hTimer));
}


/*Update the counter value*/
void CounterCallBack( tPVoid argv )
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
  u32Count++;
}

/*Is called over periodically with a defined time span*/
void TimerCallback(tPVoid argv)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   /*Simply wait for 1 ms*/
   OSAL_s32ThreadWait( CALLBACK_WAIT_TIME );
}

tVoid TimerCallback_1(tPVoid argv) 
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_1" );
}

tVoid TimerCallback_2(tPVoid argv) 
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_2" );
}

/*Thread_Timer_1*/
tVoid Thread_Timer_1( tPVoid argv )
{
	/*Declarations*/
	OSAL_tpfCallback Callback_1  = OSAL_NULL;
	OSAL_tMSecond msec           = 0;
	OSAL_tMSecond interval       = 0;

	/*Init*/
	Callback_1 = (OSAL_tpfCallback)TimerCallback_1;
   	
	if( OSAL_ERROR == OSAL_s32TimerCreate( Callback_1,OSAL_NULL,&tHandle_1 ) )
	{
		OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		/*Critical section*/
		*( (tU32 *)argv ) += 450;
		/*Critical section*/
		OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	else
	{
		/*Set*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_1,TEN_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 750;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );
	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_1,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		        /*Critical section*/
				*( (tU32 *)argv ) += 950;
				/*Critical section*/
		        OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		/*Reset*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_1,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 150;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_1,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		        /*Critical section*/
				*( (tU32 *)argv ) += 250;
				/*Critical section*/
		        OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		if( OSAL_ERROR == OSAL_s32TimerDelete( tHandle_1 ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
		    /*Critical section*/
			*( (tU32 *)argv ) += 1150;
			/*Critical section*/
		    OSAL_s32SemaphorePost( globalTimersemHandle );		
		}
	}

	/*Signal the main thread of thread action completion*/
	if( OSAL_ERROR == OSAL_s32EventPost( globalevHandle,TIMER_ONE_EVENT,OSAL_EN_EVENTMASK_OR ) )
	{
	   OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	   /*Critical section*/
	   *( (tU32 *)argv ) += 2150;
	   /*Critical section*/
	   OSAL_s32SemaphorePost( globalTimersemHandle );			
	}
	
	OSAL_s32ThreadWait( DELAY );
}

/*Thread_Timer_2*/
tVoid Thread_Timer_2( tPVoid argv )
{
   /*Declarations*/
	OSAL_tpfCallback Callback_2  = OSAL_NULL;
	OSAL_tMSecond msec           = 0;
	OSAL_tMSecond interval       = 0;

	/*Init*/
	Callback_2 = (OSAL_tpfCallback)TimerCallback_2;
   	
	if( OSAL_ERROR == OSAL_s32TimerCreate( Callback_2,OSAL_NULL,&tHandle_2 ) )
	{
		OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	    /*Critical section*/
		*( (tU32 *)argv ) += 450;
		/*Critical section*/
	    OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	else
	{
		/*Set*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_2,TEN_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 750;
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );	
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_2,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	            /*Critical section*/
				*( (tU32 *)argv ) += 950;
				/*Critical section*/
	            OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		/*Reset*/
		if( OSAL_ERROR == OSAL_s32TimerSetTime( tHandle_2,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 150;	
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );
		}
		else
		{
			if( OSAL_ERROR == OSAL_s32TimerGetTime( tHandle_2,&msec,&interval ) )
			{
				OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	            /*Critical section*/
				*( (tU32 *)argv ) += 250;
				/*Critical section*/
	            OSAL_s32SemaphorePost( globalTimersemHandle );		
			}
		}

		if( OSAL_ERROR == OSAL_s32TimerDelete( tHandle_2 ) )
		{
			OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	        /*Critical section*/
			*( (tU32 *)argv ) += 1150;		
			/*Critical section*/
	        OSAL_s32SemaphorePost( globalTimersemHandle );
		}
	}

	/*Signal the main thread of thread action completion*/
	if( OSAL_ERROR == OSAL_s32EventPost( globalevHandle,TIMER_TWO_EVENT,OSAL_EN_EVENTMASK_OR ) )
	{
	   OSAL_s32SemaphoreWait( globalTimersemHandle,OSAL_C_TIMEOUT_FOREVER );
	   /*Critical section*/
	   *( (tU32 *)argv ) += 2150;			
	   /*Critical section*/
	   OSAL_s32SemaphorePost( globalTimersemHandle );
	}
	
	OSAL_s32ThreadWait( DELAY );
}

/*Timer to test for Phase Time and Cycle Time duration*/
tVoid CyclePhaseCheck( tPVoid argv )
{
   if( u32Counter < 3 )
   {
      *( ( OSAL_tMSecond *)argv + u32Counter ) = OSAL_ClockGetElapsedTime( );
   }
   /*Increment the Counter*/
   u32Counter++;
}

/*********************************************************************/
/* Test Cases for OSAL Timer API                                     */
/*********************************************************************/

TEST(OsalCoreTimerTest, u32CreateTimerWithCallbackNULL)
{
   OSAL_tTimerHandle tmHandle    = 0;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerCreate(OSAL_NULL,OSAL_NULL,&tmHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreTimerTest, u32DelTimerNonExistent)
{
   OSAL_tTimerHandle tmHandle    = 0;
   OSAL_tTimerHandle tmHandle_   = 0;
   OSAL_tpfCallback Callback     = OSAL_NULL;

   Callback = (OSAL_tpfCallback)TimerCallback;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK, OSAL_s32TimerCreate(Callback,OSAL_NULL,&tmHandle ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandle));
   EXPECT_EQ(OSAL_ERROR ,OSAL_s32TimerDelete( tmHandle ));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode( ));
   /*Scenario 2*/
   EXPECT_EQ(OSAL_ERROR ,OSAL_s32TimerDelete( tmHandle_));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

void TimerCallback_CHK11(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_11 called" );
}

tVoid TimerCallback_CHK10(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_10 called" );
}

tVoid TimerCallback_CHK9(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
  TraceString("Message : Timer_9 called" );
}

tVoid TimerCallback_CHK8(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_8 called" );
}

tVoid TimerCallback_CHK7(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_7 called" );
}

tVoid TimerCallback_CHK6(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_6 called" );
}

tVoid TimerCallback_CHK5(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_5 called" );
}

tVoid TimerCallback_CHK4(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_4 called" );
}

tVoid TimerCallback_CHK3(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_3 called" );
}

tVoid TimerCallback_CHK2(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_2 called" );
}

tVoid TimerCallback_CHK1(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_1 called" );
}

tVoid TimerCallback_CHK0(tPVoid argv) 
{
   if(LockOsal(&TimerLimitCheckLock) == OSAL_OK)
   {
      u32CallBack_count++;
      UnLockOsal(&TimerLimitCheckLock);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(argv);
   TraceString("Message : Timer_0 called" );
}

TEST(OsalCoreTimerTest, u32MaxCallHandleCheck)
{
   tU32 i=0;
   OSAL_tTimerHandle tHandle_CallBackCHK[MAX_TIMER_CALLBACK_CHECK]         ={0};
   //Creating and opening the Lock
   EXPECT_EQ(OSAL_OK,CreateOsalLock(&TimerLimitCheckLock, "TimerCallBckLmtChck"));
   EXPECT_EQ(OSAL_OK,OpenOsalLock(&TimerLimitCheckLock));

   OSAL_tpfCallback Callback_CHK[MAX_TIMER_CALLBACK_CHECK]={0};
   // Intasilizing the universal data
   EXPECT_EQ(OSAL_OK,LockOsal(&TimerLimitCheckLock));
   {
      u32CallBack_count =0;
      UnLockOsal(&TimerLimitCheckLock);
   }
   /*Init*/
   Callback_CHK[0] = (OSAL_tpfCallback)TimerCallback_CHK0;
   Callback_CHK[1] = (OSAL_tpfCallback)TimerCallback_CHK1;
   Callback_CHK[2] = (OSAL_tpfCallback)TimerCallback_CHK2;
   Callback_CHK[3] = (OSAL_tpfCallback)TimerCallback_CHK3;
   Callback_CHK[4] = (OSAL_tpfCallback)TimerCallback_CHK4;
   Callback_CHK[5] = (OSAL_tpfCallback)TimerCallback_CHK5;
   Callback_CHK[6] = (OSAL_tpfCallback)TimerCallback_CHK6;
   Callback_CHK[7] = (OSAL_tpfCallback)TimerCallback_CHK7;
   Callback_CHK[8] = (OSAL_tpfCallback)TimerCallback_CHK8;
   Callback_CHK[9] = (OSAL_tpfCallback)TimerCallback_CHK9;
   Callback_CHK[10] = (OSAL_tpfCallback)TimerCallback_CHK10;
   Callback_CHK[11] = (OSAL_tpfCallback)TimerCallback_CHK11;
   // Creation the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate( Callback_CHK[i],OSAL_NULL,&tHandle_CallBackCHK[i] ));
   }
   // Setting the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tHandle_CallBackCHK[i],5,0 ));
   }
   OSAL_s32ThreadWait( 50);
   // deleting the Timers
   for(i=0;i<MAX_TIMER_CALLBACK_CHECK;i++)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tHandle_CallBackCHK[i]));
   }
   OSAL_s32ThreadWait( 500);
   EXPECT_EQ(OSAL_OK,LockOsal(&TimerLimitCheckLock));
   {
      if(u32CallBack_count!=MAX_TIMER_CALLBACK_CHECK)
      {
         TraceString("Bug not all timers are callback exccuted only=%d",u32CallBack_count );
      }
      UnLockOsal(&TimerLimitCheckLock);
   }
   u32CallBack_count=0;
}

TEST(OsalCoreTimerTest, u32DelTimerWithHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerDelete( OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE, OSAL_u32ErrorCode());
}

TEST(OsalCoreTimerTest, u32CreateDelTimer)
{
   OSAL_tTimerHandle tmHandle    = 0;
   OSAL_tpfCallback Callback     = OSAL_NULL;
   Callback = (OSAL_tpfCallback)TimerCallback;

   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(Callback,OSAL_NULL,&tmHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete(tmHandle));
}

TEST(OsalCoreTimerTest, u32SetTimeTimerStTimeOrIntToNULL)
{
   tU32 u32Count_1               = 0;
   tU32 u32Count_2               = 0;

   OSAL_tTimerHandle tmHandle    = 0;
   OSAL_tpfCallback Callback     = OSAL_NULL;

   Callback = (OSAL_tpfCallback)CounterCallBack;
   EXPECT_EQ(OSAL_OK, OSAL_s32TimerCreate(Callback,OSAL_NULL, &tmHandle));
   /*Scenario 1 - Pass Timer handle as OSAL_NULL*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerSetTime( OSAL_NULL,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   /*Scenario 1*/
   /*Collect the Global Count Value*/
   u32Count_1 = u32Count;
   /*Scenario 2 - Valid timer handle,Zero phase time,Non zero valid cycle time*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,0,TEN_MILLI_SECONDS ));

   /*Wait for Time period greater than 10 milliseconds callback should not be called as making phase 0 stops the timer */
   OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );
   /*Collect the Global Count Value now*/
   u32Count_2 = u32Count;

   /*Check the counter for changes, if there are  changes, callback was executed.rav8kor  */
   EXPECT_EQ(u32Count_1 , u32Count_2 );
   u32Count_1 = 0;
   u32Count_2 = 0;
   u32Count_1 = u32Count;

		/*Scenario 3 - Valid timer handle,Non zero phase time,Zero cycle time*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,0 ));
   /*Wait for Time period greater than 10 milliseconds Until this time the callback should execute atleast once*/
   OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );
   u32Count_2 = u32Count;
   /*Check the counter for changes, if there are no changes, callback was not executed*/
   EXPECT_NE(u32Count_1,u32Count_2 );
   /*Reinitialize the Counters*/
   u32Count_1 = 0;
   u32Count_2 = 0;
   /*Collect the Global Count Value*/
   u32Count_1 = u32Count;

   /*Scenario 4 - Valid timer handle,Zero phase time,Zero cycle time*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,0,0 ) );
   /*Wait for Time period greater than 10 milliseconds Until this time the callback should execute atleast once*/
   OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );
   /*Collect the Global Count Value now*/
   u32Count_2 = u32Count;
   /*Check the counter for changes, if there are no changes, callback was not executed*/
   EXPECT_EQ(u32Count_1,u32Count_2 );
   /*Reinitialize the Counters*/
   u32Count_1 = 0;
   u32Count_2 = 0;
   /*Collect the Global Count Value*/
   u32Count_1 = u32Count;
   /*Scenario 4 - Valid timer handle,Zero phase time,Zero cycle time*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) );
   /*Wait for Time period greater than 10 milliseconds Until this time the callback should execute atleast once*/
   OSAL_s32ThreadWait( TEN_MILLI_SECONDS*10 );
   /*Collect the Global Count Value now*/
   u32Count_2 = u32Count;
   /*Check the counter for changes, if there are no changes, callback was not executed*/
   EXPECT_NE(u32Count_1,u32Count_2 );
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandle ) );
   /*Reinitialize the global counter*/
   u32Count = 0;
}

TEST(OsalCoreTimerTest, u32SetTimeTimerNonExistent)
{
   OSAL_tTimerHandle tmHandle    = 0;
   OSAL_tpfCallback Callback     = OSAL_NULL;
   Callback = (OSAL_tpfCallback)TimerCallback;
   
    /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(Callback,OSAL_NULL,&tmHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete(tmHandle));
   /*Try to set time for a timer which has been deleted from the system*/
   EXPECT_EQ(OSAL_ERROR ,OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());

   /*As of now, this section of code crashes the system...... whether this section is to be kept or removed, needs clarification
     To support that this section should be removed is the fact that OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
	#if EXCEPTION 
	/*Scenario 2*/
	EXPECT_EQ(OSAL_ERROR ,OSAL_s32TimerSetTime( tmHandle_,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS ) );
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
	#endif
}



TEST(OsalCoreTimerTest, u32GetTimeTimerNonExistent)
{
   OSAL_tTimerHandle tmHandle    = 0;
   #if EXCEPTION
   OSAL_tTimerHandle tmHandle_   = -7;
   #endif
   OSAL_tMSecond msec            =	TEN_MILLI_SECONDS;
   OSAL_tMSecond interval        = TEN_MILLI_SECONDS;
   OSAL_tpfCallback Callback     = (OSAL_tpfCallback)TimerCallback;;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(Callback,OSAL_NULL,&tmHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandle ) );
   /*Try to get time for a timer which has been deleted from the system*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerGetTime( tmHandle,&msec,&interval));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
    /*As of now, this section of code crashes the system......
	whether this section is to be kept or removed, needs clarification
	To support that this section should be removed is the fact that
	OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
   #if EXCEPTION
	/*Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerGetTime( tmHandle_,&msec,&interval));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
   #endif
}

TEST(OsalCoreTimerTest, u32GetTimeTimerWithDiffParam)
{
   OSAL_tTimerHandle tmHandle    = 0;
   #if EXCEPTION
   OSAL_tTimerHandle tmHandle_   = -7;
   #endif
   OSAL_tMSecond msec            = 0;
   OSAL_tMSecond interval        = 0; 
   OSAL_tpfCallback Callback     = (OSAL_tpfCallback)TimerCallback;

   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(Callback,OSAL_NULL, &tmHandle));
		/*Set the Timer first 
		Timer Type  : Periodic Timer
		Description :
		This means, once the timer API to trigger timer is called, only after an interval(phase time),
		is the handler routine run,after this phase time, for every cycle time, the handler routine
		is run.
		TKernel API :
		Cyclic Handler
		*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS));
   /* 1 ------ Get Time using OSAL_NULL handle*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerGetTime(OSAL_NULL,&msec,&interval));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   /* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerGetTime( OSAL_C_INVALID_HANDLE,&msec,&interval));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
	/*As of now, this section of code crashes the system......
	    whether this section is to be kept or removed, needs clarification
	    To support that this section should be removed is the fact that
	    OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
   #if EXCEPTION
    /* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
   EXPECT_EQ(OSAL_ERROR, OSAL_s32TimerGetTime( tmHandle_,&msec,&interval));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
   #endif
   /* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address),valid phase time address*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,&interval));
   /* 5 ------ Get Time using valid handle,valid cycle time address, OSAL_NULL(phase time address)*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,&msec,OSAL_NULL));
   /* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),OSAL_NULL(phase time address)*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,OSAL_NULL));
   /*Set the Timer first Timer Type  : Single Timer
		Description :
		This means, once the timer API to trigger timer is called,the handler routine is run
		immediately TKernel API :Alarm */
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,EIGHT_MILLI_SECONDS,0 ) );
    /* 1 ------ Get Time using OSAL_NULL handle*/
   EXPECT_EQ(OSAL_ERROR, OSAL_s32TimerGetTime( OSAL_NULL,&msec,&interval ) );
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode( ) );
   /* 2 ------ Get Time using OSAL_C_INVALID_HANDLE as handle*/
   EXPECT_EQ(OSAL_ERROR, OSAL_s32TimerGetTime( OSAL_C_INVALID_HANDLE,&msec,&interval ) );
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode( ) );
    /*As of now, this section of code crashes the system......
	    whether this section is to be kept or removed, needs clarification
	    To support that this section should be removed is the fact that
	    OSAL_tTimerHandle is defined as a tU32.... - tny1kor*/
   #if EXCEPTION
   /* 3 ------ Get Time using an INVALID handle but not OSAL_C_INVALID_HANDLE*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerGetTime( tmHandle_,&msec,&interval ) );
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode( ) );
   #endif
   /* 4 ------ Get Time using valid handle,OSAL_NULL(cycle time address), valid phase time address*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,&interval ) );
   /* 5 ------ Get Time using valid handle,valid cycle time address,OSAL_NULL(phase time address)*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,&msec,OSAL_NULL ) );
   /* 6 ------ Get Time using valid handle,OSAL_NULL(cycle time address),OSAL_NULL(phase time address)*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerGetTime( tmHandle,OSAL_NULL,OSAL_NULL ) );
   /*Shutdown activities - Delete the timer*/
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandle ) );
}

TEST(OsalCoreTimerTest, u32SetResetTwoTimers)
{
   OSAL_tEventMask   evMask        = 0;
   OSAL_trThreadAttribute trAtr_1  = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_2  = {OSAL_NULL};

   /*Create a mutex as threads to be spawned access a common memory location*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( MUTEX,&globalTimersemHandle,1));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate( EVENT_NAME,&globalevHandle));
   trAtr_1.szName       = (char*)"Thread_Timer_1";
   trAtr_1.u32Priority	 = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_1.s32StackSize = 4096;
   trAtr_1.pfEntry      = (OSAL_tpfThreadEntry)Thread_Timer_1;
   trAtr_1.pvArg        = NULL;
   trAtr_2.szName       = (char*)"Thread_Timer_2";
   trAtr_2.u32Priority	 = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_2.s32StackSize = 4096;
   trAtr_2.pfEntry      = (OSAL_tpfThreadEntry)Thread_Timer_2;
   trAtr_2.pvArg        = NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_1 ) );
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_2 ) );
   /*Wait until signal from the two threads*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( globalevHandle,TIMER_ONE_EVENT|TIMER_TWO_EVENT,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&evMask ) );
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(globalevHandle,~(evMask),OSAL_EN_EVENTMASK_AND));
   /*Close the event and delete it*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( globalevHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( EVENT_NAME ));
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose( globalTimersemHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete( MUTEX));
}

TEST(OsalCoreTimerTest, u32GetElaspedTimeFromRef)
{
   OSAL_tMSecond  _u32StartTime   = 0;
   OSAL_tMSecond  _u32EndTime     = 0;
   OSAL_tMSecond  _u32ElapsedTime = 0;
   /*Catch offset time - Reference Time*/
   _u32StartTime = OSAL_ClockGetElapsedTime( );
   OSAL_s32ThreadWait( DUMMY_WAIT );
   /*Catch offset time again*/
   _u32EndTime   = OSAL_ClockGetElapsedTime( );
   /*Compute the elapsed time*/
   EXPECT_GT(_u32EndTime, _u32StartTime );
   _u32ElapsedTime = _u32EndTime - _u32StartTime;
   /*Elapsed Time must be atleast 1 millisecond*/
   EXPECT_GE( _u32ElapsedTime,DUMMY_WAIT );
}

TEST(OsalCoreTimerTest, u32SetOsalTimerInvalStruct)
{
   OSAL_trTimeDate rtcInvalParam  = {INVAL_VALUE,INVAL_VALUE,INVAL_VALUE,INVAL_VALUE,
	                            INVAL_VALUE,INVAL_VALUE,INVAL_VALUE,INVAL_VALUE,2*INVAL_VALUE };

   /*Attempt to Set Time field with invalid values*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ClockSetTime( &rtcInvalParam ) );
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode( ) );
}

TEST(OsalCoreTimerTest, u32GetOsalTimerWithoutPrevSet)
{
   OSAL_trTimeDate rtcParam       = {0};
   tU8* aWeekdays[] = {(tU8*)"SUN",(tU8*)"MON",(tU8*)"TUE",(tU8*)"WED",(tU8*)"THU",(tU8*)"FRI",(tU8*)"SAT"};
   /*Attempt to Get Time field without a previous set*/
//   EXPECT_EQ(OSAL_ERROR,OSAL_s32ClockGetTime( &rtcParam));
//   EXPECT_EQ(OSAL_E_UNKNOWN ,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32ClockGetTime( &rtcParam));// somebody else could set the time
    TraceString("%d - %d - %d  %d : %d : %d %s[%d]",
               rtcParam.s32Year,rtcParam.s32Month,rtcParam.s32Day,rtcParam.s32Hour,rtcParam.s32Minute,rtcParam.s32Second,aWeekdays[rtcParam.s32Weekday],rtcParam.s32Weekday);
}

TEST(OsalCoreTimerTest, u32SetOsalTimerGetOsalTimer)
{
   OSAL_trTimeDate rtcSetParam    = {0};
   OSAL_trTimeDate rtcGetParam    = {0};
   tU8* aWeekdays[] = {(tU8*)"SUN",(tU8*)"MON",(tU8*)"TUE",(tU8*)"WED",(tU8*)"THU",(tU8*)"FRI",(tU8*)"SAT"};
   /*Initialize Structure*/
   rtcSetParam.s32Second         = 59;
   rtcSetParam.s32Minute         = 59;
   rtcSetParam.s32Hour           = 23;
   rtcSetParam.s32Day            = 28;
   rtcSetParam.s32Month          = 2;
   rtcSetParam.s32Year           = 2012 - 1900;
   rtcSetParam.s32Daylightsaving = 0;
   rtcSetParam.s32Weekday        = 0;
   rtcSetParam.s32Yearday        = 59;
   rtcSetParam.s32Daylightsaving = 0;
   TraceString("%d - %d - %d  %d : %d : %d %s[%d]",       rtcSetParam.s32Year,rtcSetParam.s32Month,rtcSetParam.s32Day,rtcSetParam.s32Hour,rtcSetParam.s32Minute,rtcSetParam.s32Second,aWeekdays[rtcSetParam.s32Weekday],rtcGetParam.s32Weekday);

#ifdef GEN3ARM
   EXPECT_EQ(OSAL_OK,OSAL_s32ClockSetTime( &rtcSetParam ) ); //only possible for root user
#else
//   EXPECT_EQ(OSAL_ERROR,OSAL_s32ClockSetTime( &rtcSetParam ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32ClockSetTime( &rtcSetParam ) );
#endif
   EXPECT_EQ(OSAL_OK,OSAL_s32ClockGetTime( &rtcGetParam ) );
   TraceString("%d - %d - %d  %d : %d : %d %s[%d]",rtcGetParam.s32Year,rtcGetParam.s32Month,rtcGetParam.s32Day,rtcGetParam.s32Hour,rtcGetParam.s32Minute,rtcGetParam.s32Second,aWeekdays[rtcGetParam.s32Weekday],rtcGetParam.s32Weekday);
}

TEST(OsalCoreTimerTest, u32GetOsalTimerNULL)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32ClockGetTime( OSAL_NULL ) );
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode( ) );
}

TEST(OsalCoreTimerTest, u32CyclePhaseTimeCheck)
{
   OSAL_tTimerHandle tmHandle    = 0;
   OSAL_tpfCallback Callback     = (OSAL_tpfCallback)CyclePhaseCheck;
   OSAL_tMSecond au32TimeData[3] = {0};
   OSAL_tMSecond u32PhaseTime    = 0;
   OSAL_tMSecond u32CycleTime    = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate( Callback,au32TimeData,&tmHandle ) );
   au32TimeData[0] = OSAL_ClockGetElapsedTime( );
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandle,PHASE_TIME,CYCLE_TIME ) );
   /*Busy Wait*/
   while( 3 > u32Counter )
   {
      OSAL_s32ThreadWait( DELAY );
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandle ) );
   /*Phase Time*/
   if( au32TimeData[1] > au32TimeData[0] )
   {
	u32PhaseTime = au32TimeData[1] - au32TimeData[0];
   }
   /*Cycle Time*/
   if( au32TimeData[2] > au32TimeData[1] )
   {
     u32CycleTime = au32TimeData[2] - au32TimeData[1];
   }
   /*Cycle Time check*/
   EXPECT_GT(u32CycleTime ,( CYCLE_TIME - TOLERANCE_TIME ) );
   EXPECT_LT(u32CycleTime,( CYCLE_TIME + TOLERANCE_TIME ) );
    /*Phase Time Check*/
   EXPECT_LT(u32PhaseTime,( PHASE_TIME + TOLERANCE_TIME ) );
   EXPECT_GT(u32PhaseTime,( PHASE_TIME - TOLERANCE_TIME ) );
   /*Reinitialize Counter*/
   u32Counter = 1;
} 

/*****************************************************************************
* FUNCTION    :	   vCounterCallBack_()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCounterCallBack_( tPVoid pArg )
{
   /*Update the counter*/
   tU32 *pInt    	= OSAL_NULL;
   tU32 u32Index 	= 0;

   /*Collect the inputs*/
   pInt  = ( tU32* )pArg;
   *pInt = *pInt+1;
   if( *pInt == CALLBACK_COUNT )
   {
      /*Calculate index*/
      u32Index = (tU32)( pInt - u32CounterArray );
      /*In case of rollback*/
      if( !u8LimitCheck[u32Index] )
      {
         /*Find Elapsed time*/
	 u32EndElapsedTime[u32Index] = OSAL_ClockGetElapsedTime( );
	 /*Set the Limit check to indicate to the corresponding thread that that thread may post to the main thread*/
         u8LimitCheck[u32Index]      = 1;
      }
   }
}
 
/*****************************************************************************
* FUNCTION    :	   vPerformanceThread()
* PARAMETER   :    Pointer to unsigned 32 bit integer
* RETURNVALUE :    none
* DESCRIPTION :    Thread to Load the system			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vPerformanceThread( tVoid* pvArg )
{
   OSAL_tpfCallback Callback     = (OSAL_tpfCallback)vCounterCallBack_;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate( Callback,&u32CounterArray[globIndex], &tmHandleArray[globIndex]));
   {
       /*Find Elapsed time*/	
      u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );
      /*Start Timer*/
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TWENTY_MILLI_SECONDS ));
   } 
   /*Post on the Semaphore*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( globalIndexsemHandle));
   /*Wait to see if main thread is ready to terminate the spawned threads
	This code is to keep the system load going on*/
	//while( !u8Multicast )
	{
		//OSAL_s32ThreadWait( LOAD_WAIT );
	}
	
   /*wait till all threads are exited with time out for 100 secs*/
//   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( hEvSyncTimerMT,EVENT_TIMER_MT,OSAL_EN_EVENTMASK_AND,100000,OSAL_NULL )) ; 
	/*Wait instead wait one multiple instances for the same event*/
	OSAL_s32ThreadWait( DELAY );

}

#ifdef TO_BE_CHECKED
TEST(OsalCoreTimerTest, u32TimerPerformanceMT_Time)
{
   tU32 u32LoopCount              = 0;
   /*CALLBACK_COUNT Timer Callback calls will be made*/
   tU32 u32ExactTimerTime         = EIGHT_MILLI_SECONDS+TWENTY_MILLI_SECONDS*( CALLBACK_COUNT - 1 );
   tU32 u32ActualTimerTime        = 0;
   tChar acBuf[MAX_LENGTH]        = {"\0"};
   OSAL_trThreadAttribute trAtr[LOAD];
   /*Initilaise the array of thread attributes*/
   memset(trAtr,0,sizeof(trAtr));

   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( "SYNC_GLOB_INDEX_SEM",&globalIndexsemHandle,0));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate("TimerMTEve",&hEvSyncTimerMT));
   while( u32LoopCount < LOAD )
   {
      /*Fill the buffer with a name*/
      OSAL_s32PrintFormat( acBuf,"Timer_Thread_%u",(unsigned int)u32LoopCount );
      trAtr[u32LoopCount].pvArg        = OSAL_NULL;
      trAtr[u32LoopCount].s32StackSize = 2048;/*2 KB Minimum*/
      trAtr[u32LoopCount].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      trAtr[u32LoopCount].pfEntry      = vPerformanceThread;
      trAtr[u32LoopCount].szName       = acBuf;
				  
      tID[u32LoopCount] = OSAL_ThreadSpawn( &trAtr[u32LoopCount] ); 
      EXPECT_NE(OSAL_ERROR,tID[u32LoopCount]);
			
      /*Wait on the Global index sync semaphore variable*/
      EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( globalIndexsemHandle,OSAL_C_TIMEOUT_FOREVER ));
      /*Increment the Global Counter*/
      globIndex++;
      /*Clear the buffer*/
      memset( acBuf,0,MAX_LENGTH );
      /*Increment Counter*/
      u32LoopCount++;	
   }
	
   /*Wait until all timers have reached the timer count*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
      while( u8LimitCheck[u32LoopCount] != 1 )
      {
      }	
      /*Wait and enter delayed state*/
      OSAL_s32ThreadWait(10);	 
      u32LoopCount++;
   } 
   /*Initialize loop count*/
   u32LoopCount = 0;
   /*Delete all the timers from the system*/
   while( u32LoopCount < LOAD )
   {
       EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandleArray[u32LoopCount]));
       u32LoopCount++;
   }

   /*Initialize loop count*/
   u32LoopCount = 0;
   /*Say main thread is ready to exit the threads spawned with entry - vPerformanceThread */
	//u8Multicast = 1;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( hEvSyncTimerMT,EVENT_TIMER_MT,OSAL_EN_EVENTMASK_OR ));
	
   OSAL_s32ThreadWait(200);	 

   /*rav8kor - commented as threads has to gracefully exit but not deleted*/
   /*	
	while( u32LoopCount < LOAD )
	{
		
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID[u32LoopCount] ) )
		{
			u32Ret = u32Ret + ( u32LoopCount+1 )*100;
		}

		
		u32LoopCount++;
	}*/

	/*Close the Semaphore and Remove it*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(  globalIndexsemHandle ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete( "SYNC_GLOB_INDEX_SEM" ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( hEvSyncTimerMT ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( "TimerMTEve" ) );

   /*Drift Computation*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
	if( u32EndElapsedTime[u32LoopCount] > u32StartElapsedTime[u32LoopCount] ) 
	{
            u32ActualTimerTime = u32EndElapsedTime[u32LoopCount] - u32StartElapsedTime[u32LoopCount];
        }
        else
        {
            u32ActualTimerTime = ( 0xFFFFFFFF - u32StartElapsedTime[u32LoopCount] )	+ u32EndElapsedTime[u32LoopCount]+1;
        }
        /*Trace out the drift*/	  
//	TraceString("Drift time( in milli seconds ) : %u\n",u32ActualTimerTime-u32ExactTimerTime );
        /*Calculate the Drift*/
        s32Drift[u32LoopCount] = (tS32)( u32ActualTimerTime-u32ExactTimerTime );
        /*Increment the Loop*/
        u32LoopCount++;
    }
    /*Query Drift*/
    /*rav8kor - the drift can be max 2ms as there is a 1msec uncertainity while measuurement is started and ended*/
    u32LoopCount = 0;
    while( u32LoopCount < LOAD )
    {
      EXPECT_LE(-2,s32Drift[u32LoopCount]);
      EXPECT_GE(2,s32Drift[u32LoopCount]);
      /*Increment the counter*/
      u32LoopCount++;
    }
	
   /*Reinitialize all the global variables*/
   /*Thread ID,Timer Handle,Counter,Start Elapsed Time,End Elapsed Time,Drift,Limit Check*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
      tID[u32LoopCount] 					= 0;
      tmHandleArray[u32LoopCount] 		= 0;
      u32CounterArray[u32LoopCount] 		= 0;
      u32StartElapsedTime[u32LoopCount] 	= 0;
      u32EndElapsedTime[u32LoopCount] 	= 0;
      s32Drift[u32LoopCount] 				= 0;
      u8LimitCheck[u32LoopCount] 			= 0;
      u32LoopCount++;
   }
	
    /*Reinitialize global variables*/
    globalIndexsemHandle   			  = 0;
    globIndex                         = 0;
    u8Multicast                       = 0;
    OSAL_s32ThreadWait( WAIT_FOR_DELETE << 1 );
}
#endif


TEST(OsalCoreTimerTest, u32TimerPerformanceST_Time)
{
   OSAL_tpfCallback Callback      = (OSAL_tpfCallback)vCounterCallBack_;
   tU32 u32LoopCount              = 0;
   /*CALLBACK_COUNT Timer Callback calls will be made*/
   tU32 u32ExactTimerTime         = EIGHT_MILLI_SECONDS+TWENTY_MILLI_SECONDS*( CALLBACK_COUNT - 1 );
   tU32 u32ActualTimerTime        = 0;
   while( globIndex < LOAD )
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(Callback,&u32CounterArray[globIndex],&tmHandleArray[globIndex]));
      u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );
      EXPECT_EQ(OSAL_OK,OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TWENTY_MILLI_SECONDS));
      globIndex++;
   }
   /*Wait until all timers have reached the timer count*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
      while( u8LimitCheck[u32LoopCount] != 1 )
      {
         /*Wait and enter delayed state*/
         OSAL_s32ThreadWait(10);	 
      }
      u32LoopCount++;
   }
   /*Delete all the timers from the system*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
       EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete( tmHandleArray[u32LoopCount]));
       u32LoopCount++;
   }
   /*Drift Computation*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
     if( u32EndElapsedTime[u32LoopCount] > u32StartElapsedTime[u32LoopCount] ) 
     {
        u32ActualTimerTime = u32EndElapsedTime[u32LoopCount] - u32StartElapsedTime[u32LoopCount];
     }
     else
     {
        u32ActualTimerTime = ( 0xFFFFFFFF - u32StartElapsedTime[u32LoopCount] )	+ u32EndElapsedTime[u32LoopCount]+1;
     }
   //  TraceString("Drift time( in milli seconds ) : %u",u32ActualTimerTime-u32ExactTimerTime );
     s32Drift[u32LoopCount] = (tS32)( u32ActualTimerTime-u32ExactTimerTime );
     u32LoopCount++;
   }
   /*Query Drift*/
   /*rav8kor - the drift can be max 2ms as there is a 1msec uncertainity while measuurement is started and ended*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
      EXPECT_LE(-2,s32Drift[u32LoopCount]);
      EXPECT_GE(2,s32Drift[u32LoopCount]);
      u32LoopCount++;
   }
   /*Reinitialize Parameters*/
   u32LoopCount = 0;
   while( u32LoopCount < LOAD )
   {
       s32Drift[u32LoopCount] = 0;
       u32StartElapsedTime[u32LoopCount] = 0;
       u32EndElapsedTime[u32LoopCount] = 0;
       tmHandleArray[u32LoopCount] = 0;
       u32CounterArray[u32LoopCount] = 0;
       u8LimitCheck[u32LoopCount] = 0;
       u32LoopCount++;
   }
   globIndex                         = 0;
}


#ifdef CHECK_TEST
/*****************************************************************************
* FUNCTION    :	   vCriticalCounterCallBack_()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCriticalCounterCallBack_( tPVoid pArg )
{
   /*Update the counter*/
   tU32 *pInt    	= ( tU32* )pArg;
   /*Increment the counter*/
   *pInt = *pInt+1;
}

TEST(OsalCoreTimerTest, u32TimerPerformanceST_Counter)
{
   OSAL_tpfCallback Callback    = (OSAL_tpfCallback)vCriticalCounterCallBack_;
   tU32 u32Variance                            = 0;
   tDouble tdPredictSetTimePart   	  = 0.0;
   tDouble tdPredictDelTimePart   	  = 0.0;
   tDouble tdVariance                            = 0.0;
   OSAL_tMSecond u32StartSetTime   			  = 0;
   OSAL_tMSecond u32EndSetTime     			  = 0;
   OSAL_tMSecond u32StartDelTime                 = 0;
   OSAL_tMSecond u32EndDelTime                   = 0;
   OSAL_tMSecond u32PredictSetTime               = 0;
   OSAL_tMSecond u32PredictDelTime               = 0;
   OSAL_tMSecond u32PredictedTimeForTimer[LOAD]  = {0};
   OSAL_tMSecond u32PredictedCountForTimer[LOAD] = {0};

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Create all timers*/
	while( globIndex < LOAD )
	{
		/*Create a Timer*/
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	    		                         	 	Callback,
	        		                     		&u32CounterArray[globIndex], 
	            		                 		&tmHandleArray[globIndex] ) )
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment global index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;


	/** Predict time for triggering all the timers, for using in future **/
	/*Capture the elapsed time now*/
	u32StartSetTime   = OSAL_ClockGetElapsedTime( );
	/*Trigger all the timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );
		/*Increment global counter*/
		globIndex++;
	}
	/*Capture the elapsed time now*/
	u32EndSetTime     = OSAL_ClockGetElapsedTime( );
	/*Predict set time*/
	u32PredictSetTime = u32EndSetTime - u32StartSetTime;
	/** Predict time for triggering all the timers, for using in future **/	

	/*Initialize the Global counter*/
	globIndex    = 0;

	/** Predict time for deleting all the timers, for using in future **/
	/*Capture the elapsed time now*/
	u32StartDelTime   = OSAL_ClockGetElapsedTime( );
	/*Delete all the timers*/
	while( globIndex < LOAD )
	{
		/*Delete Timer*/
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		/*Increment global counter*/
		globIndex++;
	}
	/*Capture the elapsed time now*/
	u32EndDelTime     = OSAL_ClockGetElapsedTime( );
	/*Predict delete time*/
	u32PredictDelTime = u32EndDelTime - u32StartDelTime;
	/** Predict time for deleting all the timers, for using in future **/

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Initialize Timer Handle array and counter array*/
	while( globIndex < LOAD )
	{
		tmHandleArray[globIndex]   = 0;
		u32CounterArray[globIndex] = 0;

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Create all timers*/
	while( globIndex < LOAD )
	{
		/*Create a Timer*/
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	    		                         	 	Callback,
	        		                     		&u32CounterArray[globIndex], 
	            		                 		&tmHandleArray[globIndex] ) )
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment global index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Compute Average for one Predicted Set Time and one Predicted Del Time*/
	tdPredictSetTimePart = ( tDouble )u32PredictSetTime/( tDouble )LOAD;
	tdPredictDelTimePart = ( tDouble )u32PredictDelTime/( tDouble )LOAD;

	/*Update the Predicted time of run for each timer*/
	while( globIndex < LOAD )
	{
		/*Get the variance*/
		tdVariance = ( ( LOAD - ( globIndex + 1 ) )*tdPredictSetTimePart )+ ( globIndex*tdPredictDelTimePart );

		if(	( tDouble )( tdVariance - ( tU32 )tdVariance ) > 0.5 )
		{
			u32Variance = ( tU32 )tdVariance + 1;
		}
		else
		{
			u32Variance = ( tU32 )tdVariance;
		}

		u32PredictedTimeForTimer[globIndex] = WAIT_TIME + u32Variance;

		globIndex++;
		u32Variance = 0;
	}

	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Update the Predicted count for each timer*/
	while( globIndex < LOAD )
	{
		u32PredictedCountForTimer[globIndex] = ( u32PredictedTimeForTimer[globIndex] - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS + 1;
		globIndex++;
	}
	
	/*Initialize the Global counter*/
	globIndex    = 0;		 

	/*Trigger all the timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );
		/*Increment global counter*/
		globIndex++;
	}

	/*Wait until the couter is ready*/
	OSAL_s32ThreadWait( WAIT_TIME );

   
	/*Initialize the Global counter*/
	globIndex    = 0;

	/*Delete all the Timers*/
	while( globIndex < LOAD )
	{
		/*Start Timer*/
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		/*Increment global counter*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Verify the Counters*/
	while( globIndex < LOAD )
	{
		if( u32PredictedCountForTimer[globIndex] != u32CounterArray[globIndex] )
		{
			u32Ret += 1;
		}

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Initialize Timer Handle array and counter array*/
	while( globIndex < LOAD )
	{
		tmHandleArray[globIndex]   = 0;
		u32CounterArray[globIndex] = 0;

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex = 0;

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   vCriticalCounterCallBack__()
* PARAMETER   :    Pointer to counter value within the specific index
* RETURNVALUE :    none
* DESCRIPTION :    Timer callback	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vCriticalCounterCallBack__( tPVoid pArg )
{
	/*Definition*/
	tU32* pInt    = ( tU32* )pArg;
	tU32 u32Index = (tU32)(pInt - u32CounterArray);

	/*Increment*/
	if( !u32EndElapsedTime[u32Index] )
	{
		*pInt = *pInt + 1;
	}

	/*Check*/
	if( u8TimerSwitch )
	{
		/*Calculate the End elapsed time*/
		u32EndElapsedTime[u32Index] = OSAL_ClockGetElapsedTime( );	
	}
}

/*****************************************************************************
* FUNCTION    :	   vPerformanceThread_()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread to Load the system			       	
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tVoid vPerformanceThread_( tPVoid pArg )
{
 	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pArg);
 	/*Find Elapsed time*/	
	u32StartElapsedTime[globIndex] = OSAL_ClockGetElapsedTime( );

	/*Start Timer*/
	OSAL_s32TimerSetTime( tmHandleArray[globIndex],EIGHT_MILLI_SECONDS,TEN_MILLI_SECONDS );

	/*Post on semaphore*/
	OSAL_s32SemaphorePost( globalIndexsemHandle );

	/*Keep the system load continued*/
	while( u8TimerSwitch != 1 )
	{
		OSAL_s32ThreadWait( LOAD_WAIT );
	}

	/*Do a wait also for the end elapsed time to be over*/
	while( !u32EndElapsedTime[globIndex] )
	{
		OSAL_s32ThreadWait( LOAD_WAIT );
	}

	/*Wait for 4 seconds*/
        /*	OSAL_s32ThreadWait( WAIT_FOR_DELETE );	 	*/
        /* Dont wait here */
}

/*****************************************************************************
* FUNCTION    :	   u32TimerPerformanceMT_Counter()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_TMR_017
* DESCRIPTION :    Create 30 timers in 30 threads and verify drift.
				   in terms of updates of the counter value, incremented
				   in the Timer callback, with the elapsed time as base.
				   
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  March 20,2008
*******************************************************************************/
tU32 u32TimerPerformanceMT_Counter( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				 = 0;
	OSAL_tpfCallback Callback    = OSAL_NULL;
	tChar acBuf[MAX_LENGTH]      = {"\0"};
	tU32 u32CountForTimers[LOAD] = {0};
	OSAL_trThreadAttribute trAtr[LOAD];

	/*Initilaise the array of thread attributes*/
	OSAL_pvMemorySet(trAtr,0,sizeof(trAtr));

	/*Initialize the Global counter*/
	globIndex 					= 0;
	/*Initialize the callback*/
	Callback 					= (OSAL_tpfCallback)vCriticalCounterCallBack__;


	/*Create a Wait Post Semaphore*/
	if( OSAL_ERROR == OSAL_s32SemaphoreCreate( 
							 					"SYNC_GLOB_INDEX_SEM",
				    							 &globalIndexsemHandle,
							                     0
						                     ) )
	{																	   
		/*Return error*/
		return 2;
	}

	/*Create all the timers in the system*/
	while( globIndex < LOAD )
	{
		if( OSAL_ERROR == OSAL_s32TimerCreate( 
	        	  	                     		Callback,
	                	             			&u32CounterArray[globIndex], 
	                    	         			&tmHandleArray[globIndex] ) )	
		{
			/*Update error code*/
			u32Ret = u32Ret + ( globIndex + 1 )*100;

			/*Remove all the earlier timers*/
			while( globIndex )
			{
				/*Decrement the Global index*/
				globIndex--;
				/*Delete the timer*/
				OSAL_s32TimerDelete( tmHandleArray[globIndex] );
			}

			/*Return immediately*/
			return u32Ret;
		}

		/*Increment the Global Index*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Spawn Threads*/
	while( globIndex < LOAD )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Timer_Thread_%u",(unsigned int)globIndex );
		
		/*Fill in the Thread Attribute structure*/
		trAtr[globIndex].pvArg        = OSAL_NULL;
		trAtr[globIndex].s32StackSize = 2048;/*2 KB Minimum*/
		trAtr[globIndex].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		trAtr[globIndex].pfEntry      = vPerformanceThread_;
		trAtr[globIndex].szName       = acBuf; 
		
		/*Spawn the thread*/
		tID[globIndex] = OSAL_ThreadSpawn(  &trAtr[globIndex] );

	   	/*Wait on semaphore*/
		OSAL_s32SemaphoreWait( globalIndexsemHandle,OSAL_C_TIMEOUT_FOREVER );

		/*Clear the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );

	   	/*Increment the Global Index*/
		globIndex++;
	} 

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Wait until wait Time*/
	OSAL_s32ThreadWait( WAIT_TIME );
	
	/*Initialize switch - Relieve Thread Load*/
	u8TimerSwitch = 1;

	/*Wait until end elapsed times are updated*/
	while( globIndex < LOAD )
	{
		for(;;)
		{
			/*Wait until wait Time*/
			/* with out the below statement (Wait), the release version has the instruction 
			"beq v0,zero,0x9D070" which points to the same location. this is like endless loop*/
		        OSAL_s32ThreadWait( 0 );
		  	if((u32EndElapsedTime[globIndex]))
			{
			  break;
			}
 	
		} 	

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Delete Timers*/
	while( globIndex < LOAD )
	{
		OSAL_s32TimerDelete( tmHandleArray[globIndex] );
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Delete all the Threads*/
	while( globIndex < LOAD )
	{
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tID[globIndex] ) )
		{
			u32Ret = u32Ret + ( globIndex + 1 )*100;
		}

		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Compute the count values for every timer using the elapsed time*/
	while( globIndex < LOAD )
	{
		/*Compute the Count based on elapsed time*/
		if(  u32EndElapsedTime[globIndex] > u32StartElapsedTime[globIndex] )
		{
			u32CountForTimers[globIndex] = ( ( u32EndElapsedTime[globIndex] - u32StartElapsedTime[globIndex] ) - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS +1;
		}
		else
		{
			u32CountForTimers[globIndex] = 	( ( ( 0xFFFFFFFF - u32StartElapsedTime[globIndex] )	+ u32EndElapsedTime[globIndex]+1 ) - EIGHT_MILLI_SECONDS )/TEN_MILLI_SECONDS +1;
		}
			
		/*Increment counter*/
		globIndex++;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Check counter increments*/
	while( globIndex < LOAD )
	{
		/*Check counter values*/
		if( u32CountForTimers[globIndex] != u32CounterArray[globIndex] )
		{
			u32Ret += 1;
		}

		/*Increment the count*/
		globIndex++;
	} 

	/*Close the Semaphore and Remove it*/
	if( OSAL_ERROR != OSAL_s32SemaphoreClose(  globalIndexsemHandle ) )
	{
		/*Remove the Semaphore*/
		if( OSAL_ERROR == OSAL_s32SemaphoreDelete( "SYNC_GLOB_INDEX_SEM" ) )
		{
			u32Ret += 150;
		}
	}
	else
	{
		u32Ret += 250;
	}

	/*Initialize the Global counter*/
	globIndex 					= 0;

	/*Reinitialize array*/
	while( globIndex < LOAD )
	{
		u32StartElapsedTime[globIndex] = 0;
		u32EndElapsedTime[globIndex]   = 0;
		tmHandleArray[globIndex] 	   = 0;
		u32CounterArray[globIndex] 	   = 0;

		globIndex++;
	}

	/*Initialize the Global counter and Timer Switch*/
	globIndex 					= 0;
	u8TimerSwitch               = 0;


	/*Wait until threads have finished*/
	OSAL_s32ThreadWait( WAIT_FOR_DELETE << 1 );
        
	/*Return the error code*/
	return u32Ret;
}

#endif
/*****************************************************************************
* FUNCTION    :    u32ChangeSystemTimeTest()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :
* DESCRIPTION :    set time to x, create timer with callback in 10s.
*                  While timer is running set system time to x+5s.
*                  Callback should end after 10s.

* HISTORY     :    Created by Dainius Ramanauskas(CM-AI/PJ-CF31)  Juni 01,2011
*******************************************************************************/
tU32 u32ChangeSystemTimeTest(tVoid)
{
   tU32 u32Ret = 0;

   vSetTime();

   vCreateTimer();

   return u32Ret;
}
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
