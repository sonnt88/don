/*****************************************************************************
| FILE:         osal_core_unit_test_mock.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE UNIT TEST
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the mocking functions for osal core unit test
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"


using namespace std;


OsalCoreUnitTestMock osal_core_mock; 

//char commandline[256] = "generated/Unit-Test-Proces.out";
//char process_name[256] = "Unit-Test-Proces.out";

extern "C" {

/*
void eh_reboot(void){
  osal_core_mock.eh_reboot();
}
void init_exception_handler(const char *name){
  osal_core_mock.init_exception_handler(name);
}
void exit_exception_handler(void){
  osal_core_mock.exit_exception_handler();
}

int exception_handler_lock(void){
  osal_core_mock.exception_handler_lock();
}
void exception_handler_unlock(void){
  osal_core_mock.exception_handler_unlock();
}

int* pGetLockAdress(void){
  osal_core_mock.pGetLockAdress();
}

void InitSignalFifo(void){
  osal_core_mock.InitSignalFifo();
}

*/



/*
tS32 ERRMEM_S32IOOpen(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16 app_id){
  osal_core_mock.ERRMEM_S32IOOpen(s32Id, szName, enAccess, pu32FD, app_id);
}
tS32 ERRMEM_S32IOClose(tS32 s32ID, tU32 u32FD){
  osal_core_mock.ERRMEM_S32IOClose(s32ID, u32FD);
}

tS32 ERRMEM_s32IOControl(tS32 s32ID, tU32 u32fd, tS32 io_func, tS32 param){
  osal_core_mock.ERRMEM_s32IOControl(s32ID, u32fd, io_func, param);
}

tS32 ERRMEM_s32IORead(tS32 s32ID, tU32 u32FD, tPS8 buffer,tU32 size, tU32 *ret_size){
  osal_core_mock.ERRMEM_s32IORead(s32ID, u32FD, buffer,size, ret_size);
}

tS32 ERRMEM_s32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 buffer,tU32 size, tU32 *ret_size){
  osal_core_mock.ERRMEM_s32IOWrite(s32ID, u32FD, buffer,size, ret_size);
}

void HandleMaxErrMemFilesize(int u32Size){
  osal_core_mock.HandleMaxErrMemFilesize(u32Size);
}
*/

static OSAL_CORE* g_pOSAL_CORE = NULL;


void OSAL_CORE::SetUp(void)
{

}

void OSAL_CORE::TearDown(void)
{
}

tS32 main(int argc, char** argv) 
{
   g_pOSAL_CORE = new OSAL_CORE;

   if(g_pOSAL_CORE)
   {
      ::testing::InitGoogleTest(&argc, argv);
 
      ::testing::AddGlobalTestEnvironment(g_pOSAL_CORE);

      int retVal = RUN_ALL_TESTS();
      TraceString("OSAL Core Test ends with %d",retVal);
   }
   OSAL_vProcessExit();
}

} // extern "C"

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
