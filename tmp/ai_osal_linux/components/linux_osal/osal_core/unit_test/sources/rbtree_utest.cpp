/*****************************************************************************
| FILE:         rbtree_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

#include <stdlib.h>

using namespace std;

#define ENTRIES_USED_FOR_TEST 200

/*********************************************************************/
/* Test Cases for OSAL RB Tree API                                     */
/*********************************************************************/
static trRBTreeData rTestDat;
static trElementNode Elements[ENTRIES_USED_FOR_TEST+1];

TEST(RBTreeTest, RBTreeTest_Test1)
{
  memset(&rTestDat,0,sizeof(trRBTreeData));
  memset(&Elements[0],0,sizeof(trElementNode)*(ENTRIES_USED_FOR_TEST+1));
  vInitNodes(ENTRIES_USED_FOR_TEST+1,&rTestDat,Elements,NULL);

  trElementNode* pElement;
  int i;
  
  for(i=0;i<ENTRIES_USED_FOR_TEST;i++)
  {
		  vInsertElement(&rTestDat,i,i);
		  vRemoveElement(&rTestDat,i);
		  vInsertElement(&rTestDat,i,i);
  }

  EXPECT_EQ(ENTRIES_USED_FOR_TEST, rTestDat.u32UsedElements);

  for(i=0;i<ENTRIES_USED_FOR_TEST;i++)
  {
	  pElement = pSearchElement(&rTestDat,i,TRUE);
	  EXPECT_TRUE(pElement != NULL);
	  if(pElement)
		  EXPECT_EQ(i, pElement->u32Index);
  }

  for(i=0;i<100;i++)
  {
	  vRemoveElement(&rTestDat,i);
	  pElement = pSearchElement(&rTestDat,i,TRUE);
	  EXPECT_EQ(NULL, (void*) pElement);
  }

  EXPECT_EQ(ENTRIES_USED_FOR_TEST - 100,rTestDat.u32UsedElements);

  for(i=100;i<ENTRIES_USED_FOR_TEST;i++)
  {
	  pElement = pSearchElement(&rTestDat,i,TRUE);
	  EXPECT_TRUE(pElement != NULL);
	  if(pElement)
		  EXPECT_EQ(i, pElement->u32Index);
		  vRemoveElement(&rTestDat,i);
  }
  
  EXPECT_EQ(0, rTestDat.u32UsedElements);
}


int gen_rand_val(unsigned int min, unsigned int max){

	unsigned int ret = rand();
	double db_rand = ret;
	db_rand = db_rand / (double) RAND_MAX;
	db_rand *= ((max-min) + 1); 
	db_rand += min;
	
	ret = (int) db_rand;
	if(ret > max) ret = max;
	if(ret < min) ret = min;

	return (int) ret;
}


/*********************************************************************/
/* Test Cases for OSAL RB Tree API                                   */
/*********************************************************************/
TEST(RBTreeTest, RBTreeTest_Test2)
{
  trElementNode* pElement;
  int i;
  int n_elem_at_start = rTestDat.u32UsedElements;
  int n_added_elem   = ENTRIES_USED_FOR_TEST;
  int idx;
  int test_data_arr[ENTRIES_USED_FOR_TEST];
  int my_cnt = 0;
  int add_new = 0;

  memset(&rTestDat,0,sizeof(trRBTreeData));
  memset(&Elements[0],0,sizeof(trElementNode)*(ENTRIES_USED_FOR_TEST+1));
  vInitNodes(ENTRIES_USED_FOR_TEST+1,&rTestDat,Elements,NULL);

  for(i=0;i<ENTRIES_USED_FOR_TEST;i++) 
  {
	  test_data_arr[i] = gen_rand_val(0,0xFFFFFFFF);
		  vInsertElement(&rTestDat,i,test_data_arr[i]);
		  vRemoveElement(&rTestDat,i);
		  vInsertElement(&rTestDat,i,test_data_arr[i]);
  }

  EXPECT_EQ(n_elem_at_start + n_added_elem, rTestDat.u32UsedElements);

  while(my_cnt < 1000000){
	  my_cnt++;
	  idx = gen_rand_val(0,ENTRIES_USED_FOR_TEST-1);
	  pElement = pSearchElement(&rTestDat,idx,TRUE);
	  
	  if(  test_data_arr[idx] != -1) {
		  /* Sometimes failure here */
		  EXPECT_TRUE(pElement != NULL);
		  if(pElement)
			  EXPECT_EQ(test_data_arr[idx], pElement->u32Index);
		  else
			  printf("Error: idx=%d \n", idx);
	  } else {
		  EXPECT_TRUE(pElement == NULL);
	  }
	  
	  idx = gen_rand_val(0,ENTRIES_USED_FOR_TEST-1);

	  vRemoveElement(&rTestDat,idx);
      add_new = gen_rand_val(0, 20);
	  if(add_new > 10) {
			  test_data_arr[idx] = gen_rand_val(0,0xFFFFFFFF);
			  vInsertElement(&rTestDat,idx,test_data_arr[idx]);
			  
		  }else{
			  test_data_arr[idx] = -1;
			  n_added_elem--;
		  }
	  /* Sometimes failure here , because sometimes entries with same key are overwritten*/
	// EXPECT_EQ(n_elem_at_start + n_added_elem, pOsalData->u32UsedElements);

  }
}

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
