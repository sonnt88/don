/******************************************************************************
 * FILE               : _FFS3_CommonFS_TestFuncs.c
 *
 * SW-COMPONENT :   osal io unit test 
 *
 * DESCRIPTION  :   This file implements the file system test cases for the FFS3  
 *                          
 * AUTHOR(s)        :   Sriranjan (RBEI/ECM1)
 *
 * HISTORY          :
 *-----------------------------------------------------------------------------
 *  Date              |                                          |   Author & comments
 * --.--.--           |   Initial revision                       |   ------------
 * 31.03.15           | Ported from OEDT                         |  SWM2KOR
 *------------------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "FFS3_CommonFS_TestFuncs.h"
#include "FS_TestFuncs.h"

#include "osal_core_unit_test_mock.h"


using namespace std;



/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenClosedevice( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_001
* DESCRIPTION   :   Opens and closes FFS3 device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST(OsalIoFFS3Test, u32FFS3_CommonFSOpenClosedevice)
{ 
   (tVoid)u32FSOpenClosedevice(OEDTTEST_C_STRING_DEVICE_FFS3);
   
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpendevInvalParm( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_002
* DESCRIPTION   :   Try to Open FFS3 device with invalid parameters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSOpendevInvalParm)
{
  (tVoid)u32FSOpendevInvalParm(OEDTTEST_C_STRING_DEVICE_FFS3);
   
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReOpendev( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_003
* DESCRIPTION   :   Try to Re-Open FFS3 device
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSReOpendev) 
{
   (tVoid)u32FSReOpendev(OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSClosedevAlreadyClosed( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_005
* DESCRIPTION   :   Close FFS3 device which is already closed
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSClosedevAlreadyClosed) 
{
   (tVoid)u32FSClosedevAlreadyClosed(OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenClosedir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_006
* DESCRIPTION   :   Try to Open and closes a directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSOpenClosedir) 
{
   (tVoid)u32FSOpenClosedir(OEDT_C_STRING_DEVICE_FFS3_ROOT);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpendirInvalid( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_007
* DESCRIPTION   :   Try to Open invalid directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSOpendirInvalid) 
{
   tPS8 dev_name[2] ;
	dev_name[0]= (tPS8)OEDT_C_STRING_FFS3_NONEXST;
	dev_name[1]=(tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
   
   (tVoid)u32FSOpendirInvalid(dev_name );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDelDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_008
* DESCRIPTION   :   Try create and delete directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCreateDelDir) 
{
   (tVoid)u32FSCreateDelDir(OEDT_C_STRING_DEVICE_FFS3_ROOT);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDelSubDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_009
* DESCRIPTION   :   Try create and delete sub directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCreateDelSubDir) 
{
   (tVoid)u32FSCreateDelSubDir(OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRmNonExstngDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_011
* DESCRIPTION   :   Try to remove non exsisting directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSRmNonExstngDir) 
{
   (tVoid)u32FSRmNonExstngDir(OEDTTEST_C_STRING_DEVICE_FFS3);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRmDirUsingIOCTRL( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_012
* DESCRIPTION   :   Delete directory with which is not a directory (with files) 
*                       using IOCTRL
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
*                   Ported from OEDT to osal unit test on Mar 31,2015 
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSRmDirUsingIOCTRL) 
{
   tPS8 dev_name[2];
   dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
   dev_name[1]=(tPS8)OEDT_C_STRING_FFS3_FILE1;
   
   (tVoid)u32FSRmDirUsingIOCTRL( dev_name );
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetDirInfo( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_013
* DESCRIPTION   :   Get directory information
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSGetDirInfo) 
{
   (tVoid)u32FSGetDirInfo(OEDTTEST_C_STRING_DEVICE_FFS3, TRUE);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSOpenDirDiffModes( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_014
* DESCRIPTION   :   Try to open directory in different modes
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSOpenDirDiffModes) 
{
    tPS8 dev_name[2];
    dev_name[0]=(tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
    dev_name[1]=(tPS8)SUBDIR_PATH_FFS3;
    
    (tVoid)u32FSOpenDirDiffModes(dev_name, TRUE);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSReOpenDir( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_015
* DESCRIPTION   :   Try to reopen directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSReOpenDir)
{
   tPS8 dev_name[2];
   dev_name[0]= (tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
   dev_name[1]= (tPS8)OEDT_C_STRING_FFS3_DIR1;
   
   (tVoid)u32FSReOpenDir(dev_name, TRUE);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateDirMultiTimes( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_017
* DESCRIPTION   :   Try to Create directory with similar name 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
*                   Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCreateDirMultiTimes)
{
   tPS8 dev_name[3];
   dev_name[0]=(tPS8)OEDT_C_STRING_DEVICE_FFS3_ROOT;
   dev_name[1]=(tPS8)SUBDIR_PATH_FFS3;
   dev_name[2]=(tPS8)SUBDIR_PATH2_FFS3;

   (tVoid)u32FSCreateDirMultiTimes(dev_name, TRUE);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateRemDirInvalPath( )
* PARAMETER     :   none
* RETURNVALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_018
* DESCRIPTION   :   Create/ remove directory with Invalid path
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Oct 28, 2008 
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCreateRemDirInvalPath)
{
   tPS8 dev_name[2];
   dev_name[0]= (tPS8)OEDTTEST_C_STRING_DEVICE_FFS3;
   dev_name[1]= (tPS8)OEDT_C_STRING_FFS3_DIR_INV_PATH;						
   
   (tVoid)u32FSCreateRemDirInvalPath(dev_name);
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateRmNonEmptyDir( )
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCreateRmNonEmptyDir)
{
   (tVoid)u32FSCreateRmNonEmptyDir();
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCopyDir( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_020
* DESCRIPTION   :   Copy the files in source directory to destination directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSCopyDir)
{
    (tVoid)u32FSCopyDir();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSMultiCreateDir( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_021
* DESCRIPTION   :   Create multiple sub directories
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSMultiCreateDir)
{
    (tVoid) u32FSMultiCreateDir();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCreateSubDir( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_022
* DESCRIPTION   :   Attempt to create sub-directory within a directory opened
*                       in READONLY modey
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSCreateSubDir)
{
    (tVoid) u32FSCreateSubDir();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSDelInvDir ( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_023
* DESCRIPTION   :   Delete invalid Directory
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSDelInvDir)
{
    (tVoid) u32FSDelInvDir();
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSCopyDirRec( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_024
* DESCRIPTION   :   Copy directories recursively
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008 
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSCopyDirRec)
{
    (tVoid) u32FSCopyDirRec();
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSRemoveDir ( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_025
* DESCRIPTION   :   Remove directories recursively
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSRemoveDir)
{
    (tVoid) u32FSRemoveDir();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileCreateDel( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_026
* DESCRIPTION   :  Create/ Delete file  with correct parameters
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileCreateDeld)
{
    (tVoid) u32FSFileCreateDel();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenClose( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_027
* DESCRIPTION   :   Open /close File
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileOpenClose)
{
    (tVoid) u32FSFileOpenClose();
}   


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenInvalPath( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_028
* DESCRIPTION   :  Open file with invalid path name (should fail)
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileOpenInvalPath)
{
    (tVoid) u32FSFileOpenInvalPath();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileOpenInvalParam( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_029
* DESCRIPTION   :   Open a file with invalid parameters (should fail), 
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileOpenInvalParam)
{
    (tVoid) u32FSFileOpenInvalParam();
}

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReOpen( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_030
* DESCRIPTION   :  Try to open and close the file which is already opened
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSFileReOpen)
{
    (tVoid) u32FSFileReOpen();
}


TEST (OsalIoFFS3Test, u32FFS3_CommonFSFileCreateDouble)
{
	 (tVoid) u32FSFileCreateDouble();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileRead( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_031
* DESCRIPTION   :   Read data from already exsisting file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileRead)
{   
    (tVoid) u32FSFileRead();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileWrite( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_032
* DESCRIPTION   :  Write data to an existing file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSFileWrite)
{   
    (tVoid) u32FSFileWrite();   
}   

/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetPosFrmBOF( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_033
* DESCRIPTION   :  Get File Position from begining of file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSGetPosFrmBOF)
{
    (tVoid) u32FSGetPosFrmBOF();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSGetPosFrmEOF( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_034
* DESCRIPTION   :   Get File Position from end of file
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test,  u32FFS3_CommonFSGetPosFrmEOF)
{
    (tVoid) u32FSGetPosFrmEOF();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadNegOffsetFrmBOF( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_035
* DESCRIPTION   :   Read with a negative offset from BOF
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSFileReadNegOffsetFrmBOF)
{
	 (tVoid) u32FSFileReadNegOffsetFrmBOF();
}


/*****************************************************************************
* FUNCTION      :   u32FFS3_CommonFSFileReadOffsetBeyondEOF( )
* PARAMETER     :   none
* (tVoid)VALUE   :   tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE     :   TU_OEDT_FFS3_CFS_036
* DESCRIPTION   :   Try to read more no. of bytes than the file size (beyond EOF)
* HISTORY       :   Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008  
******************************************************************************/
TEST (OsalIoFFS3Test, u32FFS3_CommonFSFileReadOffsetBeyondEOF)
{
    (tVoid) u32FSFileReadOffsetBeyondEOF();
}

/* EOF */
