/*****************************************************************************
| FILE:         osalproc_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2015 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"


#include "osal_core_unit_test_mock.h"

using namespace std;
 
#define EVENT_MAX_THREADS         0x00000001
#define INVALID_PRIORITY   160
#define INVALID_STACK_SIZE 1
#define VALID_STACK_SIZE     2048/*2KB, minimum is 1KB*/
#define FIVE_SECONDS         5000
#define SIX_SECONDS          6000
#define SEVEN_SECONDS        7000
#define TEN_SECONDS          10000
#define TWO_SECONDS          2000
#define MAX_THREADS          50
#define THREAD_NAME_SIZE     10
#define LIST_SIZE            300
#define EXCEPTION            0
#define PRIORITY             100
#define INVAL_PRIORITY       400
#define SIZE_INVAL           ((tS32)-15)
#define INVALID_PID          ((tS32)-100)
#define DEFAULT_PID          (tS32)0
#define KERNEL_MODE          0
#define  SHM_AREA "/shmarea"
#define  MYSHMSIZE 128

#define MESSAGEQUEUE_MP_CONTROL_MAX_MSG     10
#define MESSAGEQUEUE_MP_CONTROL_MAX_LEN     sizeof(tU32)
#define MESSAGEQUEUE_MP_P1_CONTROL_NAME     "MQ_P1_CTRL"
#define MESSAGEQUEUE_MP_P2_CONTROL_NAME     "MQ_P2_CTRL"
#define MESSAGEQUEUE_MP_RESPONSE_NAME       "MQ_RSPNS"


typedef enum
{
    MP_TEST_SPMVOLT = 1,
    MP_TEST_TESTRTC,
    MP_TEST_TESTMQ,
    MP_TEST_TESTMQ_IPC_PERF,
    MP_TEST_TESTMQ_IPC_CONTENT,
    MP_TEST_TESTMQ_OPEN_CLOSE_TWICE,

    MP_TEST_FFD_WRS,

    MP_TEST_KDS_WRITE_FULL,
    MP_TEST_KDS_READ_ENTRY,

    MP_TEST_MSGPOOL_OPEN,
    MP_TEST_MSGPOOL_CHECK_SIZE,

    MP_TEST_END
} MP_TEST_MESSAGE;

/*****************************************************************
Global variables
*****************************************************************/
OSAL_tSemHandle u32SemHandle_Who_Am_I = 0;
OSAL_tSemHandle u32SemThreadState     = 0;
OSAL_tEventHandle hEvMaxThreads       = 0;
tU8  u8Switch                         = 0;

pthread_mutex_t MaxThreadCreateMutex = PTHREAD_MUTEX_INITIALIZER;
tS32 s32MaxThreadCreateCounter       = 0;

/*****************************************************************
| Helper Defines and Macros (scope: module-local)
|----------------------------------------------------------------*/
/*Thread Body*/
tVoid Thread(tPVoid pvArg)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   OSAL_s32ThreadWait(1000);
}
/*Thread Body*/
tVoid ThreadTcB(tPVoid pvArg)
{
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	tS32 ret;
	OSAL_s32ThreadWait(10);
    OSAL_tThreadID trID = OSAL_ThreadWhoAmI();
    EXPECT_EQ(OSAL_OK,(ret = OSAL_s32ThreadSuspend(trID)));
    OSAL_s32ThreadWait(3000);
}

/*Thread Body*/
/*lint -e818 */
tVoid Thread_Body(tVoid* pvArg )
{
   ((void)pvArg);
	/*rav8kor - LINT info supressed as pvArg cannot be const as per the OSAL call back declaration*/
   /*rav8kor - post event when the last thread is created */

   pthread_mutex_lock(&MaxThreadCreateMutex);
   s32MaxThreadCreateCounter++;
   pthread_mutex_unlock(&MaxThreadCreateMutex);

   if (s32MaxThreadCreateCounter == (MAX_THREADS))
   {
      OSAL_s32EventPost(hEvMaxThreads, EVENT_MAX_THREADS, OSAL_EN_EVENTMASK_OR);
   }
}
/*lint +e818 */

/*Thread Body*/
tVoid ThreadWhoAmI_Body( tPVoid pvArg )
{
	

	/*Get the Thread ID*/
	*(OSAL_tThreadID *)pvArg = OSAL_ThreadWhoAmI( );

	/*Post on the Semaphore*/
	OSAL_s32SemaphorePost( u32SemHandle_Who_Am_I );

	/*Wait for Termination*/
	OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*Thread Body*/
tVoid Thread_State_Test( tPVoid pvArg )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
	
			   
  	if( u8Switch )
	{
		/*Wait for a Post*/
		OSAL_s32SemaphoreWait( u32SemThreadState,OSAL_C_TIMEOUT_FOREVER );
	}
	
	/*Wait and enter delayed state*/
	OSAL_s32ThreadWait( TEN_SECONDS );
}

/*Infinite Thread*/
tVoid Infinite_Thread( tVoid * ptr )
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	for(;;)
	{
	}
}

tVoid Initialize_TCB( OSAL_trThreadControlBlock* tCB )
{
	/*Initialize TCB*/
	tCB->szName             = OSAL_NULL;
	tCB->u32Priority        = 0;
	tCB->u32CurrentPriority = 0;
	tCB->s32StackSize       = 0;
	tCB->s32UsedStack       = 0;
	tCB->startTime          = 0;
	tCB->sliceTime          = 0;
	tCB->runningTime        = 0;
	tCB->u32ErrorCode       = 0;

	return;
}
	

/*********************************************************************/
/* Test Cases for OSAL Process/Thread API                            */
/*********************************************************************/
TEST(OsalCoreProcTest, u32PRCreateThreadNameNULL)
{
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   trAttr.szName                 =  (tString)OSAL_NULL; /*Thread name set to NULL*/
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32CreateThreadNameTooLong)
{
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   tS8 tStr[55]                  = "Thread name set to very long to test name length limit";
   trAttr.szName                 =  (tString)tStr; /*Thread name set to a very long string*/
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}   

TEST(OsalCoreProcTest, u32CreateThreadWrongPriority)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   tS8 tStr[10]                  = "Threada";

   trAttr.szName                 =  (tString)tStr; /*Thread name set to a very long string*/
   trAttr.u32Priority            =  INVALID_PRIORITY;/*Invalid Priority*/
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
#ifdef OSAL_SET_PRIORITY
   EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
#else
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
#endif
}

TEST(OsalCoreProcTest, u32CreateThreadLowStack)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   tS8 tStr[10]                  = "Threadb";
   trAttr.szName                 =  (tString)tStr; /*Thread name set to a very long string*/
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  INVALID_STACK_SIZE;/*Undersized stack*/
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr)); // stacksize not evaluated
//   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32CreateThreadEntryFuncNULL)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   tS8 tStr[10]                  = "Threadc";
   trAttr.szName                 =  (tString)tStr; /*Thread name set to a very long string*/
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  OSAL_NULL;/*Thread code segment set to OSAL_NULL*/
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32DeleteThreadNotExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadd";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   /* Scenario 1*/
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadDelete(trID_));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}	

TEST(OsalCoreProcTest, u32DeleteThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32CreateDeleteThreadValidParam)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   tS8 tStr[10]                  = "Threade";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   /* Scenario 1*/
   EXPECT_NE(OSAL_ERROR,(trID = OSAL_ThreadSpawn(&trAttr)));
   EXPECT_NE(OSAL_ERROR,OSAL_s32ThreadDelete(trID));
}

TEST(OsalCoreProcTest, u32CreateTwoThreadSameName)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   tS8 tStr[10]                  = "Threadf";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;

   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ActivateThreadNonExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadd";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   /* Scenario 1*/
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadActivate(trID));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadActivate(trID_));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ActivateThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadActivate(trID));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ActivateThreadTwice)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   tS8 tStr[10]                  = "Threadg";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
    EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadCreate(&trAttr)));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadActivate(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadActivate(trID));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32SuspendThreadNonExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadh";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   /* Scenario 1*/
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadSuspend(trID));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadSuspend(trID_));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}


TEST(OsalCoreProcTest, u32SuspendThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadSuspend(trID));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ResumeThreadNonExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadi";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   /* Scenario 1*/
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadResume(trID));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadResume(trID_));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ResumeThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadResume(trID));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}



TEST(OsalCoreProcTest, u32ChgPriorityThreadNonExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadj";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   /* Scenario 1*/
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadPriority(trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadPriority(trID_,OSAL_C_U32_THREAD_PRIORITY_LOWEST));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ChgPriorityThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadPriority(trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ChgPriorityThreadWithWrongVal)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   tS8 tStr[10]                  = "Threadk";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadCreate(&trAttr)));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadPriority(trID,INVALID_PRIORITY )); // is ignored
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());// but error code is set
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadActivate(trID));
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
}


TEST(OsalCoreProcTest, u32ReadTCBThreadNonExisting)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_trThreadControlBlock tCB = {OSAL_NULL};
   OSAL_tThreadID trID           = 0;
   OSAL_tThreadID trID_          = 7;
   tS8 tStr[10]                  = "Threadl";
   trAttr.szName                 =  (tString)tStr; 
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  Thread;
   trAttr.pvArg                  =  OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadSpawn(&trAttr)));
   /* Scenario 1*/
   OSAL_s32ThreadWait(2000);
//   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadDelete(trID));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadControlBlock(trID,&tCB));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
   /* Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadControlBlock(trID_,&tCB));
   EXPECT_EQ(OSAL_E_WRONGTHREAD,OSAL_u32ErrorCode());
}

TEST(OsalCoreProcTest, u32ReadTCBThreadWithNegID)
{   
   OSAL_tThreadID trID           = -1;
   OSAL_trThreadControlBlock tCB = {OSAL_NULL};
   EXPECT_EQ(OSAL_ERROR,OSAL_s32ThreadControlBlock(trID,&tCB));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreProcTest, u32ReadTCBThreadDifferentStatus)
{   
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};
   OSAL_tThreadID trID    = 0;
   trAttr.szName                 =  (tString)"Threadm";
   trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize           =  VALID_STACK_SIZE;
   trAttr.pfEntry                =  ThreadTcB;
   trAttr.pvArg                  =  OSAL_NULL;
   OSAL_trThreadControlBlock tCB = {OSAL_NULL};
   tU32 u32RunTime;
   EXPECT_NE(OSAL_ERROR,(trID=OSAL_ThreadCreate(&trAttr)));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadControlBlock(trID,&tCB));
//   EXPECT_EQ(trID.enStatus,OSAL_EN_THREAD_INITIALIZED);
   Initialize_TCB( &tCB );
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadActivate(trID));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadControlBlock(trID,&tCB));
 //  EXPECT_EQ(trID.enStatus,OSAL_EN_THREAD_RUNNING);
 
   OSAL_s32ThreadWait(1000);
  
   Initialize_TCB( &tCB );
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadControlBlock(trID,&tCB));
 //  EXPECT_EQ(trID.enStatus,OSAL_EN_THREAD_SUSPENDED);
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadResume(trID));
   OSAL_s32ThreadWait(0);
   Initialize_TCB( &tCB );
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadControlBlock(trID,&tCB));
  // EXPECT_EQ(trID.enStatus,OSAL_EN_THREAD_RUNNING);
   Initialize_TCB( &tCB );


   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadPriority(trID,OSAL_C_U32_THREAD_PRIORITY_LOWEST));
   EXPECT_EQ(OSAL_OK,OSAL_s32ThreadControlBlock(trID,&tCB));
   EXPECT_EQ(OSAL_C_U32_THREAD_PRIORITY_LOWEST,tCB.u32CurrentPriority);
   Initialize_TCB( &tCB );
}

TEST(OsalCoreProcTest, u32MaxThreadCreate)
{   
   tU32 u32Count                         = 0;
   OSAL_trThreadAttribute trAtrM         = {OSAL_NULL};
   s32MaxThreadCreateCounter = 0;
   hEvMaxThreads = 0;
   tS8 ps8ThreadName[16];

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate("MaxThEve",&hEvMaxThreads));
   while(u32Count < MAX_THREADS )
   {	 
      sprintf((tString) ps8ThreadName, "%s%03u", "test21_", u32Count);
      trAtrM.szName                 =  (tString)ps8ThreadName;
      trAtrM.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      trAtrM.s32StackSize           =  VALID_STACK_SIZE;
      trAtrM.pfEntry                =  Thread_Body;
      trAtrM.pvArg                  =  &u32Count;
      EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtrM));
      u32Count++;
   }

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(hEvMaxThreads,EVENT_MAX_THREADS,OSAL_EN_EVENTMASK_AND,5000,OSAL_NULL));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(hEvMaxThreads));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete("MaxThEve"));
}

TEST(OsalCoreProcTest, u32ThreadWhoAmI)
{   
   OSAL_tThreadID u32MainThreadID       = OSAL_ThreadWhoAmI();
   OSAL_tThreadID tID[MAX_THREADS]      = {0};
   OSAL_tThreadID tIDWAI[MAX_THREADS]   = {0};
   OSAL_trThreadAttribute trAtr         = {OSAL_NULL};
   tU32 u32Count                        = 0;
   tU32 u32Counter                      = 0;
   tU32 u32ReCounter                    = 0;
   tS32 s32SemaphoreCount               = 0;
   tS8 ps8ThreadName[16];
   tID[0]          = u32MainThreadID;
   tIDWAI[0]       = u32MainThreadID;
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate( "Semaphore_Who_Am_I",&u32SemHandle_Who_Am_I,0));
   while( u32Count<10 )
   {
      sprintf((tString) ps8ThreadName, "%s%03u", "test22_", u32Count);
      trAtr.szName                 =  (tString)ps8ThreadName;
      trAtr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      trAtr.s32StackSize           =  VALID_STACK_SIZE;
      trAtr.pfEntry                =  ThreadWhoAmI_Body;
      trAtr.pvArg                  =  &tIDWAI[u32Count];
      EXPECT_NE(OSAL_ERROR,(tID[u32Count] = OSAL_ThreadSpawn(&trAtr)));
      u32Count++;
   }
   while(( u32Count ) != (tU32)s32SemaphoreCount)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue(u32SemHandle_Who_Am_I,&s32SemaphoreCount));
      OSAL_s32ThreadWait( 20 );
   }
		
   /*Check ID returned by Thread Spawn and ID returned from OSAL_ThreadWhoAmI*/
   while( u32Counter < u32Count )
   {
      EXPECT_EQ(tID[u32Counter],tIDWAI[u32Counter]);
      u32Counter++;
   }
   /*Reset u32Counter*/
   u32Counter = 1;

   /*Check for ID repetition in IDs returned by OSAL_ThreadWhoAmI*/
   while( u32ReCounter < u32Count )
   {
      while( u32Counter < u32Count )
      {
        EXPECT_NE( tIDWAI[u32ReCounter],tIDWAI[u32Counter]);
        u32Counter++;
      }
      u32ReCounter++;
     u32Counter = u32ReCounter+1;
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(u32SemHandle_Who_Am_I));
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete("Semaphore_Who_Am_I"));
}

TEST(OsalCoreProcTest, u32ThreadListWithListNULL)
{   
   tS32 s32Length            = LIST_SIZE;	
   EXPECT_EQ(0,OSAL_s32ThreadList(OSAL_NULL,s32Length));
}

TEST(OsalCoreProcTest, u32ThreadListWithListSizeNULL)
{   
   tS32 pas32List[LIST_SIZE] = {0};
   tS32 s32Length            = LIST_SIZE;
   tU32 u32Count = 0;
   while( u32Count < LIST_SIZE )
   {
	pas32List[u32Count] = 0;
        u32Count++;
   }
   	
   EXPECT_NE(0,OSAL_s32ThreadList(pas32List,s32Length));

   while( u32Count < LIST_SIZE )
   {
	EXPECT_NE(0,pas32List[u32Count]);
        u32Count++;
   }
}

TEST(OsalCoreProcTest, u32ThreadListWithValidParam)
{   
   tS32 pas32List[LIST_SIZE] = {0};
   tS32 s32Length            = (tS32)LIST_SIZE;	
   tS32 s32ReturnParam       = -1;
   tS32 s32Count             = 0;
   s32ReturnParam = OSAL_s32ThreadList( pas32List,s32Length );
   EXPECT_NE(-1,s32ReturnParam);
   EXPECT_GT(s32Length,s32ReturnParam);
   /*Array Check*/
   while( s32Count < s32ReturnParam )
   {
     EXPECT_NE(pas32List[s32Count],0);
     s32Count++;
   }
}

#ifdef TESTER
tU32 GetProcessResponse(OSAL_tMQueueHandle mqHandle)
{
    tU32    u32Ret = 0;
    tU32    inmsg, procnum, procerr;

    if(OSAL_s32MessageQueueWait(mqHandle, (tPU8)&inmsg, sizeof(inmsg), OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0)
        u32Ret += 1000;
    procnum = inmsg >> 16;
    procerr = inmsg & 0x0000ffff;
    if(procerr != 0)
    {
        u32Ret += procerr * 10000;
    }

    return u32Ret;
}



tU32 u32SpawnMultiProcesses( tVoid )
{
	/*Definitions*/
	tU32   u32Ret                   = 0;
    OSAL_tMQueueHandle mqHandleP1   = OSAL_C_INVALID_HANDLE;
    OSAL_tMQueueHandle mqHandleP2   = OSAL_C_INVALID_HANDLE;
    OSAL_tMQueueHandle mqHandleR    = OSAL_C_INVALID_HANDLE;
    tU32    outmsg;
	OSAL_tProcessID	prID_1,prID_2   = INVALID_PID;
	OSAL_trProcessAttribute	prAtr   = {OSAL_NULL};
//pid_t  w1,w2;
//int    statusp1=0;
//int    statusp2=0;
	int    i;
	tU32   testset[] =
	{
        MP_TEST_SPMVOLT,
        MP_TEST_TESTRTC,
        MP_TEST_TESTMQ,
        MP_TEST_TESTMQ_IPC_PERF,
        MP_TEST_TESTMQ_IPC_CONTENT,
        MP_TEST_TESTMQ_OPEN_CLOSE_TWICE,

        MP_TEST_FFD_WRS,

        MP_TEST_KDS_WRITE_FULL,
        MP_TEST_KDS_READ_ENTRY,

        MP_TEST_MSGPOOL_OPEN,
        MP_TEST_MSGPOOL_CHECK_SIZE,

        MP_TEST_END
	};
	tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";

	// Create or open MQ to control first process
    if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_P1_CONTROL_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleP1) == OSAL_ERROR)
        if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_P1_CONTROL_NAME, OSAL_EN_WRITEONLY, &mqHandleP1) == OSAL_ERROR)
            u32Ret += 1;

    // Create or open MQ to control second process
    if(u32Ret == 0)
        if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_P2_CONTROL_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleP2) == OSAL_ERROR)
            if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_P2_CONTROL_NAME, OSAL_EN_WRITEONLY, &mqHandleP2) == OSAL_ERROR)
                u32Ret += 2;

    // Create or open MQ for both processes to send their responses
    if(u32Ret == 0)
        if(OSAL_s32MessageQueueCreate(MESSAGEQUEUE_MP_RESPONSE_NAME, MESSAGEQUEUE_MP_CONTROL_MAX_MSG, MESSAGEQUEUE_MP_CONTROL_MAX_LEN, OSAL_EN_READWRITE, &mqHandleR) == OSAL_ERROR)
            if(OSAL_s32MessageQueueOpen(MESSAGEQUEUE_MP_RESPONSE_NAME, OSAL_EN_READONLY, &mqHandleR) == OSAL_ERROR)
                u32Ret += 3;

    // Spawn first process
    if(u32Ret == 0)
    {
        prAtr.szCommandLine  = (char*)"bindcpu0";
        prAtr.szName         = "procp1_out.out";
        OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
        prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
        prAtr.u32Priority    = PRIORITY;
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
        if( OSAL_ERROR == ( prID_1 = OSAL_ProcessSpawn( &prAtr ) ) )
        {
            printf("spawn p1 error %d\n", OSAL_u32ErrorCode());
            u32Ret += 10;
        }
    }

    // Spawn second process
    if(u32Ret == 0)
    {
        prAtr.szCommandLine  = (char*)"bindcpu1";
        prAtr.szName         = "procp2_out.out";
        OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
        prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
        prAtr.u32Priority 	 = PRIORITY;
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
        if( OSAL_ERROR == ( prID_2 = OSAL_ProcessSpawn( &prAtr ) ) )
        {
            printf("spawn p2 error %d\n", OSAL_u32ErrorCode());
            u32Ret += 20;
        }
    }


    if(u32Ret == 0)
    {
        // Start all tests listed in testset[]
        for(i = 0; i < sizeof(testset)/sizeof(testset[0]); i++)
        {
            outmsg = testset[i];

            // Tell first process to start test
            if(OSAL_s32MessageQueuePost(mqHandleP1, (tPCU8)&outmsg, sizeof(outmsg), 2) != OSAL_OK)
                u32Ret += 100;
            // Tell second process to start test
            if(OSAL_s32MessageQueuePost(mqHandleP2, (tPCU8)&outmsg, sizeof(outmsg), 2) != OSAL_OK)
                u32Ret += 200;

            // Wait for processes to respond
            // TODO: All OSAL_s32MessageQueueWait use OSAL_C_TIMEOUT_FOREVER. So if one process gets stuck, the whole thing hangs
            u32Ret += GetProcessResponse(mqHandleR);
            u32Ret += GetProcessResponse(mqHandleR);
        }
    }


    // Close & delete MQs
    if(mqHandleP1 != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleP1) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_P1_CONTROL_NAME);

    if(mqHandleP2 != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleP2) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_P2_CONTROL_NAME);

    if(mqHandleR != OSAL_C_INVALID_HANDLE)
        if(OSAL_s32MessageQueueClose(mqHandleR) != OSAL_ERROR)
            OSAL_s32MessageQueueDelete(MESSAGEQUEUE_MP_RESPONSE_NAME);

    return u32Ret;
}


/*****************************************************************************
* FUNCTION    :      u32ProcessSpawnValid()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_029
* DESCRIPTION :    Spawn Test Process and Delete.
               Requires build of ProcOEDT_PF_Test.out and its placement in
               "/nor0" path as a prerequisite
* HISTORY     :      Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32SpawnPerformanceProcess( tVoid )
{
   /*Definitions*/
   tU32 u32Ret                       = 0;
   OSAL_tProcessID   prID            = INVALID_PID;
   OSAL_trProcessAttribute prAtr     = {OSAL_NULL};
   pid_t w;
   int status;
   tChar caProcessPath[OSAL_C_U32_MAX_PATHLENGTH]="/";

   prAtr.szCommandLine  = (char*)"";
   prAtr.szName         = "procperf_out.out";
   OSAL_szStringNCopy(caProcessPath, OEDT_TESTPROCESS_BINARY_PATH , OSAL_C_U32_MAX_PATHLENGTH);
   prAtr.szAppName      = OSAL_szStringNConcat(caProcessPath, prAtr.szName, (OSAL_C_U32_MAX_PATHLENGTH-OSAL_u32StringLength(caProcessPath)-1));
   OEDT_HelperPrintf(TR_LEVEL_FATAL, "Spawning Process =%s",caProcessPath);
   prAtr.u32Priority     = PRIORITY;

   if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
   {
      printf("spawn error \n");
   }
   else
   {
      //printf("spawn ok \n");
   }



   w = waitpid(prID, &status, WUNTRACED | WCONTINUED);
   if (w == -1) {
      perror("waitpid perf");
   }

   /*Return the error code*/
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessSpawnInvalidParam()
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_030
* DESCRIPTION :    Spawn a Process with invalid parameter
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessSpawnInvalidParam( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                   = 0;
	
	OSAL_tProcessID	prID          = INVALID_PID;
	OSAL_trProcessAttribute	prAtr = {OSAL_NULL};
	
	/*INVALID PRIORITY*/
	/*Fill in the ProcessAttributes*/ 
	prAtr.szCommandLine	 = 0;
	prAtr.szAppName	     = "TESTPROCESS";
	prAtr.szName		 = "/host/DEBUG/ProcOEDT_PF_Test.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
#ifndef TSIM_OSAL
	prAtr.u32Priority 	 = INVAL_PRIORITY;
#endif /*#ifndef TSIM_OSAL*/
	

	/*Spawn the LFS Process*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
	{
		/*Get the error code
		for sake of query, in future when more clarifications arrive as regards
		the true error code, code will be extended to catch the actual error*/
		
	}
	else
	{
		/*Process Spawn passed!*/
		u32Ret += 1000;
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32Ret += 100;
			
		}
	}
	/*INVALID PRIORITY*/

	/*INVALID PATH - Path to some binary never existent*/
	/*Fill in the ProcessAttributes*/ 
	prAtr.szCommandLine	 = 0;
	prAtr.szAppName	     = "LFSTEST";
	prAtr.szName		 = "/nand0/ProcDA.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
#ifndef TSIM_OSAL
	prAtr.u32Priority 	 = PRIORITY;
#endif /*#ifndef TSIM_OSAL*/

	/*Spawn the LFS Process*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( &prAtr ) ) )
	{
		/*Get the error code
		for sake of query, in future when more clarifications arrive as regards
		the true error code, code will be extended to catch the actual error*/
		
	}
	else
	{
		/*Process Spawn passed! - critical failure*/
		u32Ret += 2000;
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32Ret += 200;
			
		}
	}
	/*INVALID PATH - Path to some binary never existent*/

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessWhoAmI
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_031
* DESCRIPTION :    Test for API OSAL_ProcessWhoAmI
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessWhoAmI( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	OSAL_tProcessID	prID = INVALID_PID;

	/*Call ProcessWhoAmI API*/
	prID = OSAL_ProcessWhoAmI( );

	/*Check for the ID returned*/
	if( INVALID_PID == prID )
	{
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessSelfDelete
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_032
* DESCRIPTION :    Try to delete the invoking process
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessSelfDelete( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	OSAL_tProcessID prID = INVALID_PID;
	
	prID = OSAL_ProcessWhoAmI( );
	
	/*Delete the currently running process*/
	if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
	{
		
		//Do nothing		;
	}
   	else
	{
		/*Process Delete interface returned success!*/
		u32Ret += 10000;
	}

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessListValid
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_033
* DESCRIPTION :    Retrive a list of Processes running in a system.
				   ProcOEDT_PF_Test.out must be aviliable in /host/debug/ path
				   as a prerequisite before running the case.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/	
tU32 u32ProcessListValid( tVoid )
{
	/*Definitions*/
	tU32 u32Ret 						= 0;
	tU32 u32Count                       = 0;
	tU32 u32ChangeCount                 = 0;
	tS32 as32List[T_ENGINE_MAX_PROCESS] = {
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
										  };
	tS32 s32Length 						=  T_ENGINE_MAX_PROCESS;
	tS32 s32ReturnedLength              = -1;
#if KERNEL_MODE
	OSAL_trProcessAttribute	prAtr       = {OSAL_NULL};
	OSAL_tProcessID prID                = INVALID_PID;
#endif


	/*Get number of processes registered in the system*/
	s32ReturnedLength = OSAL_s32ProcessList( as32List,s32Length );

	/*Check the returned length*/
	if( s32ReturnedLength > s32Length )
	{
		u32Ret += 1000;
	}

	/*Check for the Array modifications*/
	while( u32Count < T_ENGINE_MAX_PROCESS )
	{
		/*Find the number of Processes registered in the system*/
		if( DEFAULT_PID != as32List[u32Count] )
		{
		   ++u32ChangeCount;
		}

		/*Increment count*/
		++u32Count;
	}

	/*Check for Number of registered Processes returned by OSAL_s32ProcessList
	and the number of modified IDs in the Array list*/
	if( (tS32)u32ChangeCount != s32ReturnedLength )
	{
		/*No Process ID added to array*/
		u32Ret += 2000;
	}

	#if KERNEL_MODE
	/*Spawn a process*/
	/*Fill in the ProcessAttributes
	- Requires build of ProcOEDT_PF_Test and its placement in 
	"/host/DEBUG" path as a prerequisite*/ 
	prAtr.szAppName	     = "TESTPROCESS";
	prAtr.szCommandLine	 = 0;
	prAtr.szName		 = "/host/DEBUG/ProcOEDT_PF_Test.out";
	/*Priority Ranges are availiable in : TKSE-Spec-S006-01.00.00_en.pdf*/
	prAtr.u32Priority 	 = PRIORITY;

	/*Reinitialize the array and temperory variables*/
	as32List[0] = DEFAULT_PID;as32List[1] = DEFAULT_PID;
	as32List[2] = DEFAULT_PID;as32List[3] = DEFAULT_PID;
	as32List[4] = DEFAULT_PID;as32List[5] = DEFAULT_PID;
	as32List[6] = DEFAULT_PID;as32List[7] = DEFAULT_PID;
	as32List[8] = DEFAULT_PID;as32List[9] = DEFAULT_PID;
	u32Count          = 0;
	u32ChangeCount    = 0;
	s32ReturnedLength = -1;

	/*Spawn a process - No check to see if process spawned or not
	as ProcessList API is not interested in whether Process spawned is
	actually spawned or not*/
	if( OSAL_ERROR == ( prID = OSAL_ProcessSpawn( (const)&prAtr ) ) )
	{
		u32ECode = OSAL_u32ErrorCode( );
	}
	else
	{
		/*Collect the Process List*/
		s32ReturnedLength = OSAL_s32ProcessList( as32List,s32Length );

		/*Query the return code*/
		if( s32ReturnedLength < 1 )
		{
			/*At least 1 process was registered*/
			u32Ret += 10000;
		}
	
		/*Scan the array*/
		while( u32Count < T_ENGINE_MAX_PROCESS )
		{
			/*Find the number of Processes registered in the system*/
			if( DEFAULT_PID != as32List[u32Count] )
			{
		   		++u32ChangeCount;
			}

			/*Increment count*/
			++u32Count;
		}
	
		/*Check the change count*/
		if( u32ChangeCount < 1 )
		{
			/*At least 1 process was registered*/
			u32Ret += 5000;
		} 	

		/*Compare ChangeCount and returned no of registered processes, they
		are expected to be the same*/
		if( (tS32)u32ChangeCount != s32ReturnedLength )
		{
			u32Ret += 3000;
		}

		/*Delete the Process*/
		if( OSAL_ERROR == OSAL_s32ProcessDelete( prID ) )
		{
			u32ECode = OSAL_u32ErrorCode( );
		}
	}
	#endif

	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32ProcessListInvalidList
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_034
* DESCRIPTION :    Try using Process List with List as NULL.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessListInvalidList( tVoid )
{
	/*Definitions*/
	tU32 u32Ret          = 0;
	tS32 s32Length       = T_ENGINE_MAX_PROCESS;
	tS32 s32ReturnLength = -1;

	/*Call the Process List API with NULL as list*/
    s32ReturnLength = OSAL_s32ProcessList( OSAL_NULL,s32Length ); 

	/*Query the status of s32ReturnLength*/
	if( s32ReturnLength > 0 )
	{
		u32Ret += 1000;
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32ProcessListInvalidSize
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_035
* DESCRIPTION :    Try using Process Size as -15
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32ProcessListInvalidSize( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                         = 0;
	tU32 u32Count                       = 0;
	tU32 u32ChangeCount                 = 0;
	tS32 s32Length                      = SIZE_INVAL;
	tS32 s32ReturnLength                = -1;
	tS32 as32List[T_ENGINE_MAX_PROCESS] = {
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
											DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,DEFAULT_PID,
										  };

	/*Call the Process List API with NULL as list*/
    s32ReturnLength = OSAL_s32ProcessList( as32List,s32Length ); 

	/*Query the status of s32ReturnLength*/
	if( s32ReturnLength > 0 )
	{
		u32Ret += 1000;
	}

	/*Query the status of the array*/
	while( u32Count < T_ENGINE_MAX_PROCESS )
	{
		/*Check the Array content*/
		if( DEFAULT_PID == as32List[u32Count] )
		{
		   ++u32ChangeCount	;
		}

		/*Increment count*/
		++u32Count;
	}

	/*Check the change count , Array is not expected to change*/
	if( u32ChangeCount < T_ENGINE_MAX_PROCESS )
	{
		/*Array did change with an invalid(negative) size passed!*/
		u32Ret += 2000;
	}

	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32GetPCBValidParam
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_036
* DESCRIPTION :    Get the Process Control Block for a valid Process.
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32GetPCBValidParam( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;
	OSAL_trProcessControlBlock prCB	= {OSAL_NULL};

	/*Get the current Process ID*/
	prID = OSAL_ProcessWhoAmI( );

	/*Get the Process Control Block for the Current Process*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,&prCB ) )
	{	
		u32Ret += 1000;
	}
	
	/*Return the error code*/
	return u32Ret;
}

/*****************************************************************************
* FUNCTION    :	   u32QueryPCBInvalidID
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_037
* DESCRIPTION :    Try to Get the Process Control Block for an invalid Process ID
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32QueryPCBInvalidID( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;
	OSAL_tProcessID	prID_           = T_ENGINE_MAX_PROCESS+10;
	OSAL_trProcessControlBlock prCB	= {OSAL_NULL};

	/*Get the Process Control Block for a negative Process ID*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,&prCB ) )
	{	
		if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 50;
		}	
	}
	else
	{
		/*PCB Query Passed! - Should not pass*/
		u32Ret += 200;
	}

	/*Get the Process Control Block for a Process ID greater than max*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID_,&prCB ) )
	{	
		if( OSAL_E_WRONGPROCESS != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 150;
		}	
	}
	else
	{
		/*PCB Query Passed! - Should not pass*/
		u32Ret += 300;
	}
	
	/*Return the error code*/
	return u32Ret;
}


/*****************************************************************************
* FUNCTION    :	   u32QueryPCBInvalidAddress
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_038
* DESCRIPTION :    Try to get the Process Control Block for 
				   a Process with address for Process Control Block structure as
				   NULL
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Dec 4, 2007
*******************************************************************************/
tU32 u32QueryPCBInvalidAddress( tVoid )
{
	/*Definitions*/
	tU32 u32Ret                     = 0;
	OSAL_tProcessID	prID            = INVALID_PID;

	/*Get the current Process ID*/
	prID = OSAL_ProcessWhoAmI( );

	/*Get the Process Control Block for the Current Process*/
	if( OSAL_ERROR == OSAL_s32ProcessControlBlock( prID,OSAL_NULL ) )
	{	
	   if( OSAL_E_INVALIDVALUE != OSAL_u32ErrorCode( ) )
		{
			u32Ret += 50;
		}
	}
	else
	{
		/*Call to Process Control Block should not pass!*/
		u32Ret += 100;
	}
	
	/*Return the error code*/
	return u32Ret;
}											

/*****************************************************************************
* FUNCTION    :	   u32SpawnThreadInfiniteLoop
* PARAMETER   :    none
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE   :    TU_OEDT_OSAL_CORE_THRD_039
* DESCRIPTION :    Spawn a Thread whose body has an infinite loop
* HISTORY     :	   
*					Thread Priority change from Midel to low by 
*					Anoop Chandran(RBEI\ECM1) 27/09/2008 					
*Created by Tinoy Mathews(RBIN/EDI3)  21.01.08
*******************************************************************************/											
tU32 u32SpawnThreadInfiniteLoop( tVoid )
{
	/*Declarations*/
	tU32 u32Ret 				  = 0;
	OSAL_tThreadID tid            = 0;
	OSAL_trThreadAttribute trAttr  = {OSAL_NULL};

	/*Fill in the thread attributes*/
	trAttr.szName                 =  "Infinite_Thread";
	trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_LOWEST;
	trAttr.s32StackSize           =  VALID_STACK_SIZE;
	trAttr.pfEntry                =  Infinite_Thread;
	trAttr.pvArg                  =  OSAL_NULL;

	/*Spawn the thread*/
	if( OSAL_ERROR == ( tid = OSAL_ThreadSpawn( &trAttr ) ) )
	{
		u32Ret += 1000;
	}
	else
	{
		if( OSAL_ERROR == OSAL_s32ThreadDelete( tid ) )
		{
			u32Ret += 2000;
		}
	}
	
	/*Return the error code*/
	return u32Ret;
}                                                                         

#endif                                                                                                                                            
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
