/*****************************************************************************
| FILE:         osalmqueue_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal message queue implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2015 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;

#define MAX_MESG                    1
#define MAX_MESG3                    3
#define MAX_LEN                     20
#define MAX_EXC_LEN                 24
#define MESSAGEQUEUE_NAME 			"Message Queue"
#define MQ_MESSAGE_EXC_LEN			"-------|-------|-------|-------|"
#ifdef OSAL_SHM_MQ
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|---"
#else
#define MQ_MESSAGE_NAME_ECX			"-------|-------|-------|-------|-"
#endif
#define MQ_MESSAGE 					"----|----|----|----|"
#define MQ_MSG						"Message Queue Data"
#define MQ_PRIO0 					0
#define MQ_PRIO1                    1
#define MQ_PRIO2 					2
#define MQ_PRIO3                    3
#define MQ_PRIO4 					4
#define MQ_PRIO5                    5
#define MQ_PRIO6 					6
#define MQ_PRIO7 					7
#define INVAL_PRIO                  20 /*>>7*/
#define MQ_BUFF_SIZE				21
#define MQ_THR_NAME_1 				"MQ_Thread_1"
#define MQ_THR_NAME_2 				"MQ_Thread_2"
#define MQ_THR_NAME_NOTIFY_1 		"MQ_Thread_Notify1"
#define MQ_THR_NAME_NOTIFY_2 		"MQ_Thread_Notify2"
#define MQ_THR_NAME_NOTIFY_3 		"MQ_Thread_Notify3"
#define MQ_EVE_NAME1				"MQ_Event1"
#define MQ_EVE_NAME2				"MQ_Event2"
#define MQ_EVE_NAME2_CB			"MQ_Event2_callback"
#define MQ_EVE_NAME3_CB			"MQ_Event3_callback"
#define MQ_EVE1						1
#define MQ_EVE2						2
#define MQ_EVE3						4
#define MQ_READY_EVE1               1
#define MQ_READY_EVE2               2
#define MQ_READY_EVE3               4
#define MQ_DONE_EVE1                8
#define MQ_DONE_EVE2                16
#define MQ_DONE_EVE3                32
#define MQ_EVE						7
#define MQ_TR_STACK_SIZE			2048
#define MQ_COUNT_MAX 				5
#define MQ_HARRAY 					20
#define MQ_MESSAGE_TO_POST          "In a message queue"
#define MESSAGE_20BYTES             "ABCDEFGHIJKLMNOPQRS"
#define MAX_LENGTH                  256
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                    0x7FFFFFFF
#define SRAND_MAX                   (tU32)65535
#define MAX_MESSAGE_QUEUES          20
#define MAX_NO_MESSAGES             20
#define FIVE_SECONDS                5000

#define NO_OF_POST_THREADS       10
#define NO_OF_WAIT_THREADS       15
#define NO_OF_CLOSE_THREADS      1
#define NO_OF_CREATE_THREADS     2
#define TOT_THREADS              (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)
/*rav8kor - disable*/
#define RUN_TEST                5
#define VALID_STACK_SIZE        2048
#define MSG_QUEUE_TIMEOUT_VAL   1598 

static OSAL_tEventMask    gevMask;

/*MessageQueueTableEntry mesgque_StressList[MAX_MESSAGE_QUEUES];
tU32 u32GlobMsgCounter										  	   = 0;
OSAL_trThreadAttribute msgqpost_ThreadAttr[NO_OF_POST_THREADS];
OSAL_trThreadAttribute msgqwait_ThreadAttr[NO_OF_WAIT_THREADS];
OSAL_trThreadAttribute msgqclose_ThreadAttr[NO_OF_CLOSE_THREADS];
OSAL_trThreadAttribute msgqcreate_ThreadAttr[NO_OF_CREATE_THREADS];
OSAL_tThreadID msgqpost_ThreadID[NO_OF_POST_THREADS]               = {OSAL_NULL};
OSAL_tThreadID msgqwait_ThreadID[NO_OF_WAIT_THREADS]               = {OSAL_NULL};
OSAL_tThreadID msgqclose_ThreadID[NO_OF_CLOSE_THREADS]             = {OSAL_NULL};
OSAL_tThreadID msgqcreate_ThreadID[NO_OF_CREATE_THREADS]           = {OSAL_NULL};*/


void u32MQPostThread(void)
{
   OSAL_tMQueueHandle MQHandle = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_READWRITE, &MQHandle))   ;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(MQHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(MQHandle))   ;
}


/*********************************************************************/
/* Test Cases for OSAL Mesage Queue API                              */
/*********************************************************************/

TEST(OsalCoreMqueueTest, u32MsgQuePostMsgExcMaxLen)
{
   OSAL_tMQueueHandle mqHandle     = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,OSAL_EN_READWRITE,&mqHandle));
   /*Post a message into the message queue exceeding the defined maximum size for the message queue*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueuePost(mqHandle,(tPCU8)MQ_MESSAGE_EXC_LEN,MAX_EXC_LEN,MQ_PRIO2));
   EXPECT_EQ(OSAL_E_MSGTOOLONG,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32CreateMsgQueWithHandleNULL)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMqueueTest, u32MsgQueCreateMsgExcMaxLen)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueCreate(MQ_MESSAGE_NAME_ECX,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}

void  u32MsgQueCreateDiffModes( tVoid )
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));

   enAccess         = OSAL_EN_READONLY;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));

   enAccess         = OSAL_EN_WRITEONLY;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));

   enAccess         = OSAL_EN_BINARY;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMqueueTest, u32MsgQueCreateSameName)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
 
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32MsgQueDeleteNameNULL)
{
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueDelete(OSAL_NULL)); 
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreMqueueTest, u32MsgQueDeleteNameInval)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreMqueueTest, u32MsgQueDeleteWithoutClose)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR,OSAL_u32ErrorCode());//Handle is invalid at the second time
}

TEST(OsalCoreMqueueTest, u32MsgQueOpenDiffMode)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   tChar MQ_Buffer[MQ_BUFF_SIZE]   = { 0 };
   
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MAX_MESG,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(mqHandle,(tPCU8)MQ_MESSAGE,MAX_LEN,MQ_PRIO2));
   EXPECT_NE(OSAL_ERROR,OSAL_s32MessageQueueWait(mqHandle,(tPU8)MQ_Buffer,MAX_LEN,OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(0,OSAL_s32StringCompare(MQ_MESSAGE, MQ_Buffer));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
	   
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY,&mqHandle));
   /*wait has to fail as handle has only write only access*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(mqHandle,(tPCU8)MQ_MESSAGE,MAX_LEN,MQ_PRIO2));
   EXPECT_EQ(0,OSAL_s32MessageQueueWait(mqHandle,(tPU8)MQ_Buffer,MAX_LEN,OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(OSAL_E_NOPERMISSION,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   /*Open the message queue  in OSAL_EN_READONLY mode*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_READONLY,&mqHandle));
   memset( MQ_Buffer, '\0', MQ_BUFF_SIZE );
   /*Post a message into the message queue.This has to fail as  handle only has read only access*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueuePost( mqHandle, (tPCU8)MQ_MESSAGE,MAX_LEN,MQ_PRIO2));
   EXPECT_EQ(OSAL_E_NOPERMISSION,OSAL_u32ErrorCode());
   EXPECT_NE(0,OSAL_s32MessageQueueWait(mqHandle,(tPU8)MQ_Buffer,MAX_LEN,OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING));

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

tVoid u32MQThread1(tPVoid pvArg)
{
   tS32 s32Count	           = 0;
   OSAL_tMQueueHandle mqHandle     = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];
   OSAL_tEventHandle MQEveHandle1  = 0;

  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   memset(MQ_msg,0,sizeof(MQ_msg));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME,OSAL_EN_WRITEONLY,&mqHandle));
  
   for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
   {
      OSAL_s32PrintFormat(MQ_msg[s32Count], "MQ_MSG%d",MQ_PRIO2 );
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( mqHandle,(tPCU8)MQ_msg[s32Count],MAX_LEN,(tU32)s32Count));
   }//end of  for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
    
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME1,&MQEveHandle1));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle1,MQ_EVE1,OSAL_EN_EVENTMASK_OR)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle1));

}

tVoid u32MQThread2(tPVoid pvArg)
{
   tS32 s32Count		           = 0;
   OSAL_tMQueueHandle mqHandle             = 0;
   tChar MQ_msg[ MQ_HARRAY ][ MQ_HARRAY ];
   OSAL_tEventHandle MQEveHandle1 = 0;

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   memset(MQ_msg,0,sizeof(MQ_msg));

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME,OSAL_EN_READONLY,&mqHandle));
   for( ; MQ_COUNT_MAX > s32Count; ++s32Count )
   {
      EXPECT_NE(0,OSAL_s32MessageQueueWait(mqHandle,(tPU8)MQ_msg[ s32Count ],MAX_LEN,OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER));
   }//end of  for( ; MQ_COUNT_MAX > u32Count; ++u32Count )

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME1,&MQEveHandle1));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle1,MQ_EVE2,OSAL_EN_EVENTMASK_OR)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle1));
}


TEST(OsalCoreMqueueTest, u32MsgQueTwoThreadMsgPost)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   OSAL_trThreadAttribute  attr1   = { 0 };
   OSAL_trThreadAttribute  attr2   = { 0 };
   OSAL_tEventHandle MQEveHandle1 = 0;
   gevMask = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,enAccess,&mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME1,&MQEveHandle1));
  
   attr1.szName = (char*)MQ_THR_NAME_1;
   attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr1.s32StackSize = MQ_TR_STACK_SIZE;
   attr1.pfEntry = u32MQThread1; 
   attr1.pvArg = OSAL_NULL;
   attr2.szName = (char*)MQ_THR_NAME_2;
   attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr2.s32StackSize = MQ_TR_STACK_SIZE;
   attr2.pfEntry = u32MQThread2; 
   attr2.pvArg = OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr1));
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr2));

   EXPECT_NE(OSAL_ERROR,OSAL_s32EventWait(MQEveHandle1,MQ_EVE1|MQ_EVE2,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER ,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle1,~(gevMask),OSAL_EN_EVENTMASK_AND));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle1));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME1 ));
}

tVoid vMQCallBack2(void *Arg)
{
   (void)Arg;
   OSAL_tEventHandle MQEveHandle2_cb;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME2_CB, &MQEveHandle2_cb));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost ( MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_OR ));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2_cb));
   TraceString("vMQCallBack2 executed");
}

tVoid vMQCallBack3(void *Arg)
{
   (void)Arg;
   OSAL_tEventHandle MQEveHandle3_cb;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME3_CB, &MQEveHandle3_cb));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost ( MQEveHandle3_cb, MQ_EVE1, OSAL_EN_EVENTMASK_OR ));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle3_cb));
   TraceString("vMQCallBack3 executed");
}



tVoid u32MQThreadNotify1(tPVoid pvArg)
{
   OSAL_tMQueueHandle mqHandle = 0;
   OSAL_tEventHandle  MQEveHandle2 = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME2, &MQEveHandle2));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle));
   OSAL_s32ThreadWait(2000);
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE1 | MQ_DONE_EVE1, OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
 }

tVoid u32MQThreadNotify2(tPVoid pvArg)
{
   OSAL_tMQueueHandle mqHandle = 0;
   OSAL_tEventHandle  MQEveHandle2 = 0;
   OSAL_tEventHandle  MQEveHandle2_cb = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME2_CB, &MQEveHandle2_cb));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME2, &MQEveHandle2));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueNotify(mqHandle, (OSAL_tpfCallback)vMQCallBack2, OSAL_NULL ));
   /* signal ready for MQ post */
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE2, OSAL_EN_EVENTMASK_OR));
   /* wait for callback signal */
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));
   /* cleanup */
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueNotify(mqHandle, OSAL_NULL, OSAL_NULL ));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2_cb));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME2_CB));


   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, MQ_DONE_EVE2, OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
}

tVoid u32MQThreadNotify3(tPVoid pvArg)
{
    OSAL_tMQueueHandle mqHandle = 0;
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
    OSAL_tEventHandle  MQEveHandle2 = 0;
    OSAL_tEventHandle  MQEveHandle3_cb = 0;

    EXPECT_EQ(OSAL_OK, OSAL_s32MessageQueueOpen(MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY, &mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(MQ_EVE_NAME2, &MQEveHandle2));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME3_CB, &MQEveHandle3_cb));
     // we need some delay because thread 2 should be start the notifier
    OSAL_s32ThreadWait(500);
    /* try to register callback after one is already installed */
    EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueNotify(mqHandle, (OSAL_tpfCallback)vMQCallBack3, OSAL_NULL ));
    EXPECT_EQ(OSAL_E_BUSY,OSAL_u32ErrorCode());
    /* signal test proceed */
    EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, MQ_READY_EVE3, OSAL_EN_EVENTMASK_OR));
    /* no callback from cb will come */
    EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait(MQEveHandle3_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &gevMask));
    EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
    EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, MQ_DONE_EVE3, OSAL_EN_EVENTMASK_OR));
    /* cleanup */
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle3_cb));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME3_CB));
}

TEST(OsalCoreMqueueTest, u32MsgQueSingleThreadMsgNotify)
{
    OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
    OSAL_trThreadAttribute attr1    = { 0 };
    gevMask                         = 0;
    OSAL_tEventHandle  MQEveHandle2 = 0;

    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2));
 
    attr1.szName = (char*)MQ_THR_NAME_NOTIFY_1;
    attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    attr1.s32StackSize = MQ_TR_STACK_SIZE;
    attr1.pfEntry = u32MQThreadNotify1;
    attr1.pvArg = OSAL_NULL;
    EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr1));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST)+1,MQ_PRIO1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32EventWait(MQEveHandle2, MQ_EVE1, OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER, &gevMask));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME2));
    OSAL_s32ThreadWait(3000);

}

TEST(OsalCoreMqueueTest, u32MsgQueSingleThreadMsgNotifyCallback)
{
   OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle     = 0;
   OSAL_trThreadAttribute attr2    = { 0 };
   gevMask                         = 0;
   OSAL_tEventHandle  MQEveHandle2 = 0;

    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2));

    attr2.szName = (char*)MQ_THR_NAME_NOTIFY_2;
    attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    attr2.s32StackSize = MQ_TR_STACK_SIZE;
    attr2.pfEntry = u32MQThreadNotify2;
    attr2.pvArg = OSAL_NULL;
    EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr2));
    EXPECT_NE(OSAL_ERROR,OSAL_s32EventWait(MQEveHandle2, MQ_READY_EVE2, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST)+1,MQ_PRIO1 ));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(MQEveHandle2, MQ_DONE_EVE2, OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER, &gevMask));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME2));
     OSAL_s32ThreadWait(3000);
}

TEST(OsalCoreMqueueTest, u32MsgQueThreadMsgNotify)
{
    OSAL_tenAccess enAccess         = OSAL_EN_READWRITE;
    OSAL_tMQueueHandle mqHandle     = 0;
    OSAL_trThreadAttribute attr1    = { 0 };
    OSAL_trThreadAttribute attr2    = { 0 };
    OSAL_trThreadAttribute attr3    = { 0 };
    OSAL_tEventHandle  MQEveHandle2 = 0;
    gevMask                         = 0;

    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess, &mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2));

    attr1.szName = (char*)MQ_THR_NAME_NOTIFY_1;
    attr1.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    attr1.s32StackSize = MQ_TR_STACK_SIZE;
    attr1.pfEntry = u32MQThreadNotify1;
    attr1.pvArg = OSAL_NULL;
    attr2.szName = (char*)MQ_THR_NAME_NOTIFY_2;
    attr2.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    attr2.s32StackSize = MQ_TR_STACK_SIZE;
    attr2.pfEntry = u32MQThreadNotify2;
    attr2.pvArg = OSAL_NULL;
    attr3.szName = (char*)MQ_THR_NAME_NOTIFY_3;
    attr3.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    attr3.s32StackSize = MQ_TR_STACK_SIZE;
    attr3.pfEntry = u32MQThreadNotify3;
    attr3.pvArg = OSAL_NULL;
    EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr1));
    EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr2));
    EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&attr3));

    EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(MQEveHandle2, MQ_READY_EVE1 | MQ_READY_EVE2 | MQ_READY_EVE3, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost(mqHandle, (tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength(MQ_MESSAGE_TO_POST) + 1, MQ_PRIO1 ));
    EXPECT_NE(OSAL_ERROR,OSAL_s32EventWait(MQEveHandle2, MQ_DONE_EVE1 | MQ_DONE_EVE2 | MQ_DONE_EVE3, OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(MQEveHandle2, ~(gevMask), OSAL_EN_EVENTMASK_AND));
    OSAL_s32ThreadWait(10000);
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(mqHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(MQEveHandle2));
    EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(MQ_EVE_NAME2));
    OSAL_s32ThreadWait(1000);
}

TEST(OsalCoreMqueueTest, u32MsgQueQueryStatus)
{
   tU32 u32MaxMessage      = 0;
   tU32 u32MaxLength       = 0;
   tU32 u32Message         = 0;
   tU8  au8Buffer[MAX_LEN]	= {0};
   OSAL_tMQueueHandle hMQ  = 0;

    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,&u32MaxLength,&u32Message));
    EXPECT_EQ(0,u32Message);
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,OSAL_u32StringLength( MQ_MESSAGE_TO_POST)+1,MQ_PRIO2));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,&u32MaxLength,&u32Message));
    EXPECT_EQ(1,u32Message);
    EXPECT_NE(OSAL_ERROR,OSAL_s32MessageQueueWait( hMQ,au8Buffer,MAX_LEN,OSAL_NULL,OSAL_C_TIMEOUT_FOREVER));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,&u32MaxLength,&u32Message));
    EXPECT_EQ(0,u32Message);

    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
    EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueStatus( hMQ,&u32MaxMessage,&u32MaxLength,&u32Message));
    EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR,OSAL_u32ErrorCode());
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete( MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32MsgQueQueryStatusMMParamNULL)
{
   tU32 u32MaxLength       = 0;
   tU32 u32Message         = 0;
   OSAL_tMQueueHandle hMQ  = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus(hMQ,OSAL_NULL,&u32MaxLength,&u32Message));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32MsgQueQueryStatusMLParamNULL)
{
   tU32 u32MaxMessage      = 0;
   tU32 u32Message         = 0;
   OSAL_tMQueueHandle hMQ  = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus(hMQ,&u32MaxMessage,OSAL_NULL,&u32Message));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}
void vMsgQueQueryStatusCMParamNULL( tVoid )
{
   tU32 u32MaxMessage      = 0;
   tU32 u32MaxLength       = 0;
   OSAL_tMQueueHandle hMQ  = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueStatus(hMQ,&u32MaxMessage,&u32MaxLength,OSAL_NULL));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32MsgQueQueryStatusAllParamNULL)
{
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueStatus( OSAL_NULL,OSAL_NULL,OSAL_NULL,OSAL_NULL));
}

TEST(OsalCoreMqueueTest, u32MsgQuePostMsgInvalPrio)
{
   OSAL_tMQueueHandle hMQ  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ ) );
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueuePost( hMQ,(tPCU8)MQ_MESSAGE_TO_POST,OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1,INVAL_PRIO));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

TEST(OsalCoreMqueueTest, u32MsgQuePostMsgBeyondQueLimit)
{
   tU8  u8Count = 0;
   OSAL_tMQueueHandle hMQ  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,MQ_COUNT_MAX,MAX_LEN,OSAL_EN_READWRITE,&hMQ));
   /*Post 5 messages in the Message Queue*/
   do
   {
      /*Plus 1 to the length to accomodate the NULL string terminator*/
      EXPECT_NE(OSAL_ERROR,OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,OSAL_u32StringLength(MESSAGE_20BYTES)+1,MQ_PRIO2));
      u8Count++;
   }while( MQ_COUNT_MAX != u8Count );

   /*Post it one extra time - Should fail*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueuePost( hMQ,(tPCU8)MESSAGE_20BYTES,OSAL_u32StringLength(MESSAGE_20BYTES)+1,MQ_PRIO2)); //kill -SIGRTMIN is not handled
   EXPECT_EQ(OSAL_E_QUEUEFULL,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}

#ifdef CHECK_TEST


/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tS32 s32RandomValGen( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );

	/*Return the Positive Random Number*/
	return s32Rand;
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandTime()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Wait for Random time frame
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vWaitRandTime( tU32 u32TimeModulus )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32TimeModulus +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}

/*****************************************************************************
* FUNCTION    :	   vChangeThreadPrio()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Change Thread Priorities Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tVoid vChangeThreadPrio( tVoid )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectMsgQueIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Queue Indexes Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
*******************************************************************************/
tU8 u8SelectMsgQueIndexRandom( tVoid )
{
	/*Declarations*/
	tU8  u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the Message Queue Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_MESSAGE_QUEUES );
	/*Return the Message Queue Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   u8SelectMessagePriority()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Message Priority for a message to
  				   be sent to a Message Queue.
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 17 Dec,2007
				   Updated By Tinoy Mathews( RBIN/ECM1 ) 20 Dec,2007
*******************************************************************************/
tU32 u32SelectMessagePriority( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = RAND_MAX;

	/*Find Random number*/
	s32Rand = s32RandomValGen( );

	/*Return the Message Priority*/
	return (tU32)( s32Rand%((tS32)MQ_PRIO7+1) );
}

TEST(OsalCoreMqueueTest, u32MessageQueueStressTest)
{
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = "\0";


	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "B_SEM",&baseSem, 1 );

	/*Create MAX_MESSAGE_QUEUES number of messages in the system*/
	while( u8Index < MAX_MESSAGE_QUEUES )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_MessageQueue_%d",u8Index );

		/*Get the name into Message Name field*/
		(tVoid)OSAL_szStringCopy( mesgque_StressList[u8Index].coszName,acBuf );
		/*Fill in the length of a message unit*/
		mesgque_StressList[u8Index].u32MaxLength   = MAX_LEN;
		/*Fill in the number of message units in the message queue*/
		mesgque_StressList[u8Index].u32MaxMessages = MAX_NO_MESSAGES;
		/*Set the access mode to the message queue*/
		mesgque_StressList[u8Index].enAccess       = OSAL_EN_READWRITE;

		/*Create a Message Queue within the system*/
		if( OSAL_ERROR == OSAL_s32MessageQueueCreate(
													  mesgque_StressList[u8Index].coszName,
													  mesgque_StressList[u8Index].u32MaxMessages,
													  mesgque_StressList[u8Index].u32MaxLength,
													  mesgque_StressList[u8Index].enAccess,
													  &mesgque_StressList[u8Index].phMQ
													 ) )
		{

			/*Return immediately with failure*/
			return (10000+u8Index);
		}
		/*Reinitialize the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment the index*/
		u8Index++;
	}

	/*Start all Post Threads*/
	vEntryFunction( Post_Message,"Post_Msgq_Thr",NO_OF_POST_THREADS,msgqpost_ThreadAttr,msgqpost_ThreadID );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_Message,"Wait_Msgq_Thr",NO_OF_WAIT_THREADS,msgqwait_ThreadAttr,msgqwait_ThreadID );
	/*Start the Close Thread*/
	vEntryFunction( Close_Message,"Close_Msgq_Thr",NO_OF_CLOSE_THREADS,msgqclose_ThreadAttr,msgqclose_ThreadID );
	/*Start all Create Threads*/
	vEntryFunction( Create_Message,"Create_Msgq_Thr",NO_OF_CREATE_THREADS,msgqcreate_ThreadAttr,msgqcreate_ThreadID );

	/*Wait for OSAL_C_TIMEOUT_FOREVER*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( msgqpost_ThreadID[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( msgqwait_ThreadID[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( msgqclose_ThreadID[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}

	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( msgqcreate_ThreadID[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Reset the Global Thread Counter*/
	u32GlobMsgCounter = 0;

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( baseSem );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEM" );

	/*Return error code - Success if control reaches here*/
	return 0;
}

/*****************************************************************************
* FUNCTION    :   MsgQueSimplePostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue.
               2). Post to Message Queue
               3). Wait on Message Queue
               4). Go to 1). and do Test again n-Times
               5). Close and Delete Message Queue

* HISTORY     :
* 03.03.2011   Dainius Ramanauskas (CM-AI/PJ-CF31)
*******************************************************************************/

tU32 MsgQueSimplePostWaitPerfTest(tVoid )
{
   tU32 u32Ret = 0;
   OSAL_tMQueueHandle mqHandle = 0;
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   int loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &mqHandle))
   {

      while (loop < RUN_TEST)
      {
         if (OSAL_ERROR == OSAL_s32MessageQueuePost(mqHandle, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ))
         {
            u32Ret += loop+1000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 1200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 1300;
            return u32Ret;
         }
         if( 0 == OSAL_s32MessageQueueWait(mqHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
         {
            u32Ret += loop+2000;
            if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 2200;
            if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 2300;
            return u32Ret;
         }
         loop++;
      }
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(mqHandle)) u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)) u32Ret += 300;
   return u32Ret;
}

/*****************************************************************************
* FUNCTION    :   MsgQueThreadPostWaitPerfTest()

* PARAMETER   :   none

* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error

* TEST CASE   :

* DESCRIPTION :
               1). Create Message Queue
               2). Create Thread
               3). Wait on Message Queue
               4). Thread should post to Message Queue
               5). Go to 2). and do Test again n-Times
               6). Close and Delete Message Queue

* HISTORY     :
* 03.03.2011   Dainius Ramanauskas (CM-AI/PJ-CF31)
*******************************************************************************/

tU32 MsgQueThreadPostWaitPerfTest(tVoid )
{
   OSAL_trThreadAttribute threadAttr = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };
   tS8 ps8ThreadName[16];
   tU32 u32Ret = 0;
   tU32 u32Loop = 0;

   if (OSAL_ERROR != OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &MQHandle))
   {
      while (u32Loop < RUN_TEST)
      {
         sprintf((tString) ps8ThreadName, "%s%03u", "MQ_TH_", u32Loop);
         threadAttr.u32Priority = 0;
         threadAttr.szName = (tString) ps8ThreadName;
         threadAttr.pfEntry = (OSAL_tpfThreadEntry) u32MQPostThread;
         threadAttr.pvArg = OSAL_NULL;
         threadAttr.s32StackSize = VALID_STACK_SIZE;

         if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
         {

            if (OSAL_ERROR != OSAL_s32MessageQueueWait(MQHandle, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER))
            {
               u32Loop++;
            }
            else
            {
               u32Ret += u32Loop + 2000;
               break;
            }
         }
         else
         {
            u32Ret += u32Loop + 4000;
            break;
         }
      } // while
   }
   else
   {
      u32Ret += 100;
   }

   if (OSAL_ERROR == OSAL_s32MessageQueueClose(MQHandle))
      u32Ret += 200;
   if (OSAL_ERROR == OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME))
      u32Ret += 300;
   return u32Ret;
}

/******************************************************************************
 *FUNCTION     :u32MQOpenDoubleCloseOnce
 *DESCRIPTION  :Open Twice, Close once and use sh memory with threads
 *PARAMETER    :none
 *RETURNVALUE  :tU32, status value in case of error
 *TEST CASE    :
 *HISTORY      :07.07.2011  Dainius Ramanauskas
 *
 * PROCESS                      THREAD
 * --------------------------------------------------
 * create sh mem handle_1
 *
 *                              open sh mem handle_2
 *                              map handle_2
 *                              memset handle_2
 *                              close handle_2
 * map handle_1
 * memset handle_1
 * unmap handle_1
 * close handle_1
 *
 * delete sh mem
 *
*****************************************************************************/
tU32 u32MQOpenDoubleCloseOnce()
{
   tS32 s32ReturnValue = 0;
   tS32 s32ReturnValueThread = 0;
   OSAL_tMQueueHandle handle_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };
   tChar MQ_Buffer[MAX_LEN] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) Thread_MQ_Memory;
   threadAttr.pvArg = &s32ReturnValueThread;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   if ((OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &handle_1) != OSAL_ERROR))
   {
      if (OSAL_ERROR != OSAL_ThreadSpawn(&threadAttr))
      {
         OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
         if (OSAL_s32MessageQueuePost(handle_1, (tPCU8) MESSAGE_20BYTES, MAX_LEN, MQ_PRIO2 ) != OSAL_ERROR)
         {
            if (OSAL_s32MessageQueueWait(handle_1, (tPU8) MQ_Buffer, MAX_LEN, OSAL_NULL, (OSAL_tMSecond) OSAL_C_TIMEOUT_FOREVER) != OSAL_ERROR)
            {
               if (OSAL_s32MessageQueueClose(handle_1) != OSAL_ERROR)
               {
                  if (OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME) != OSAL_ERROR)
                  {
                     // test ok
                  }
                  else
                     s32ReturnValue += 700;
               }
               else
                  s32ReturnValue += 500;
            }
            else
               s32ReturnValue += 400;
         }
         else
            s32ReturnValue += 300;
      }
      else
         s32ReturnValue += 200;
   }
   else
      s32ReturnValue += 100;


   return s32ReturnValue;
}

/*****************************************************************************
* FUNCTION    :	   helper function for u32MsgQueNotifyEvent()
******************************************************************************/
void u32ThreadForSendingMessage( void* pArg )
{
  OSAL_tMQueueHandle mqhandle = 0;
  u32Tr1Ret = 0;
  ((void)pArg);
  if ( OSAL_s32MessageQueueOpen( MESSAGEQUEUE_NAME, OSAL_EN_WRITEONLY,	&mqhandle) == OSAL_OK )
  {
    if ( OSAL_s32MessageQueuePost(mqhandle,(tPCU8)MQ_MESSAGE_TO_POST, OSAL_u32StringLength( MQ_MESSAGE_TO_POST )+1, MQ_PRIO1 ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueuePost() failed with error '%i'", OSAL_u32ErrorCode());
      u32Tr1Ret = 1000;
    }
    
  } else {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueueOpen() failed with error '%i'", OSAL_u32ErrorCode());
    u32Tr1Ret += 2000;  
  }

  if ( OSAL_s32MessageQueueClose (mqhandle) == OSAL_ERROR )
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR (u32ThreadForSendingMessage): OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
    u32Tr1Ret += 4000;
  }
  
  OSAL_vThreadExit();
}



/*****************************************************************************
* FUNCTION    :	  u32MsgQueNotifyEvent()
* PARAMETER   :   none
* RETURNVALUE :   tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :   see https://hi-cmts.apps.intranet.bosch.com:8443/browse/CF3PF-760
* HISTORY     :   Martin Langer (ESE, CM-AI/PJ-CF33) on Jan 9th, 2011
*******************************************************************************/
tU32 u32MsgQueNotifyEvent( tVoid )
{
  tU32 u32Ret = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  OSAL_tEventHandle hEvent = 0;
  OSAL_trThreadAttribute  attr = { 0 };
  OSAL_tThreadID ThreadID = 0;
  trMqEventInf rEvent;

  u32Tr1Ret = 0;

  if ( OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, enAccess,	&MQHandle ) == OSAL_OK )
  {

    if ( OSAL_s32EventCreate( "TestName", &hEvent ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventCreate() failed with error '%i'", OSAL_u32ErrorCode());
      u32Ret = 1;
    } else {

      rEvent.coszName = "TestName";
      rEvent.mask = 0x11001100;
      rEvent.enFlags = OSAL_EN_EVENTMASK_OR;

      if ( OSAL_s32MessageQueueNotify( MQHandle, (OSAL_tpfCallback)0xffffffff, (tPVoid)&rEvent ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueNotify() failed with error '%i'", OSAL_u32ErrorCode());
        u32Ret += 2;
      }

      // message queue is prepared, start posting a msg in subthread to get an event in this main thread

      attr.szName = MQ_THR_NAME_NOTIFY_2;
      attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      attr.s32StackSize = MQ_TR_STACK_SIZE;
      attr.pfEntry = u32ThreadForSendingMessage;
      attr.pvArg = OSAL_NULL;

      ThreadID = OSAL_ThreadSpawn( &attr );
      if ( ThreadID == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_ThreadSpawn() failed with error '%i'", OSAL_u32ErrorCode());
        u32Ret += 64;
      }

      if ( OSAL_s32EventWait ( hEvent, rEvent.mask, rEvent.enFlags, OSAL_C_TIMEOUT_NOBLOCKING | 10000  , &gevMask ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR THREAD2: OSAL_s32EventWait() failed with error '%i'", OSAL_u32ErrorCode());
        u32Tr2NtRet += 128;
      }

      if ( OSAL_s32EventClose ( hEvent ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventClose() failed with error %i", OSAL_u32ErrorCode());
        u32Ret += 4;
      }

      if ( OSAL_s32EventDelete( "TestName" ) == OSAL_ERROR )
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32EventDelete() failed with error %i", OSAL_u32ErrorCode());
        u32Ret += 8;
      }
    }

    if ( OSAL_s32MessageQueueNotify( MQHandle, (OSAL_tpfCallback)0xffffffff, NULL ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueNotify() failed with error '%i'", OSAL_u32ErrorCode());
      u32Ret += 512;
    }

    if ( OSAL_s32MessageQueueClose ( MQHandle ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueClose() failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 16;
    }

    if ( OSAL_s32MessageQueueDelete ( MESSAGEQUEUE_NAME ) == OSAL_ERROR )
    {
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueDelete() failed with error %i", OSAL_u32ErrorCode());
      u32Ret += 32;
    }

    if ( OSAL_s32ThreadDelete( ThreadID ) == OSAL_ERROR )
    {
      // don't throw an error if thread finished itself
      if (OSAL_u32ErrorCode() != OSAL_E_WRONGTHREAD)
      {
        OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: delete thread '%i' failed with error %i", ThreadID, OSAL_u32ErrorCode());
        u32Ret += 256;
      }
    }
  }

  if (u32Tr1Ret != 0)
  {
    OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: received error '%i' from subthread u32ThreadForSendingMessage()", u32Tr1Ret);
    u32Ret += u32Tr1Ret;
  }
  return u32Ret;
}



/******************************************************************************
 *FUNCTION      :u32MsgQueNotifyRemove
 *
 *DESCRIPTION   :Creates a message queue and an event
 *               Registers a callback for notification that will set event
 *                  flags when triggered
 *               Posts a message to the queue and checks if the callback was
 *                  executed by waiting for an event.
 *               Cleans message queue and event
 *               Posts a message to the queue and checks if the callback was
 *                  executed again by waiting for an event.
 *               Removes that callback
 *               Posts a message to the queue and checks if the callback was
 *                  executed by waiting for an event. While waiting for the
 *                  event an error of OSAL_E_TIMEOUT is expected
 *               Closes & deletes the event and the message queue
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tU32   0 on success or
 *                      error code in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 03 05
 *****************************************************************************/
tU32 u32MsgQueNotifyRemove(tVoid)
{
    tU32                retval      = 0;
    OSAL_tMQueueHandle  mqhandle    = 0;
    OSAL_tEventMask     evmask      = 0;
    tU8                 msgbuffer[MAX_LEN];

    if((OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME, MAX_MESG3, MAX_LEN, OSAL_EN_READWRITE, &mqhandle) == OSAL_ERROR))
    {
        retval += 1;
    }
    else
    {
        if(OSAL_s32EventCreate(MQ_EVE_NAME2, &MQEveHandle2_cb) == OSAL_ERROR)
        {
            retval += 2;
        }
        else
        {
            // Register a callback
            if(OSAL_s32MessageQueueNotify(mqhandle, vMQCallBack2, NULL) == OSAL_ERROR)
            {
                retval += 4;
            }

            // Trigger the callback by posting a message
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 8;
            }

            // Check if the callback was executed by checking the event flags
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) == OSAL_ERROR)
            {
                retval += 16;
            }



            // Clear event flags
            OSAL_s32EventPost(MQEveHandle2_cb, 0, OSAL_EN_EVENTMASK_AND);
            // Clear message queue
            OSAL_s32MessageQueueWait(mqhandle, msgbuffer, sizeof(msgbuffer), NULL, OSAL_C_TIMEOUT_FOREVER);

            // Try to trigger the callback again
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 32;
            }

            // Check if the callback was executed again by checking the event flags
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) == OSAL_ERROR)
            {
                retval += 64;
            }



            // Clear event flags
            OSAL_s32EventPost(MQEveHandle2_cb, 0, OSAL_EN_EVENTMASK_AND);
            // Clear message queue
            OSAL_s32MessageQueueWait(mqhandle, msgbuffer, sizeof(msgbuffer), NULL, OSAL_C_TIMEOUT_FOREVER);

            // Remove the callback
            if(OSAL_s32MessageQueueNotify(mqhandle, NULL, NULL) == OSAL_ERROR)
            {
                retval += 128;
            }

            // Try to trigger the callback
            if(OSAL_s32MessageQueuePost(mqhandle, MQ_MESSAGE_TO_POST, sizeof(MQ_MESSAGE_TO_POST), 1) == OSAL_ERROR)
            {
                retval += 256;
            }

            // Check if the callback was executed by checking the event flags
            // Since the callback was removed, an error is expected
            if(OSAL_s32EventWait(MQEveHandle2_cb, MQ_EVE1, OSAL_EN_EVENTMASK_AND, 1000, &evmask) != OSAL_ERROR)
            {
                retval += 512;
            }
            else
            {
                tU32    status = OSAL_u32ErrorCode();
                if(status != OSAL_E_TIMEOUT)
                    retval += 1024;
            }

            // Clean up event
            if(OSAL_s32EventClose(MQEveHandle2_cb) == OSAL_ERROR)
            {
                retval += 2048;
            }
            if(OSAL_s32EventDelete(MQ_EVE_NAME2) == OSAL_ERROR)
            {
                retval += 4096;
            }
        }

        // Clean up message queue
        if(OSAL_s32MessageQueueClose(mqhandle) == OSAL_ERROR)
        {
            retval += 8192;
        }
        if(OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME) == OSAL_ERROR)
        {
            retval += 16384;
        }
    }

    return retval;
}
/******************************************************************************
*FUNCTION      :u32MsgQueTimeoutTest
*
*DESCRIPTION : This is a test for timeout feature to call OSAL_s32MessageQueueWait
*
*            1. Creates a message queue
*            2. Calls a Wait on the message queue with a specific timeout value
*            3. If a timeout occours then test is passed
*
*PARAMETER     :none
*
*RETURNVALUE   :tU32   0 on success or
*                      error code in case of an error
*
*HISTORY:      :Created by rmm1kor on 29/05/2012
*****************************************************************************/

tU32 u32MsgQueTimeoutTest(tVoid)
{
   tS32 u32RetVal = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tMQueueHandle mqHandle= 0;
   tU8 u8Buf[MAX_LEN];
   // Create Message queue
   
   if(OSAL_OK == (OSAL_s32MessageQueueCreate(MESSAGEQUEUE_NAME,
                                             MQ_COUNT_MAX,
                                             MAX_LEN,
                                             enAccess,
                                             &mqHandle)))
   {
      // Call a wait on the message queue with finite timeout value. This is expected to timeout.
      if(0 == OSAL_s32MessageQueueWait(mqHandle, u8Buf, MAX_LEN, OSAL_NULL, MSG_QUEUE_TIMEOUT_VAL))
      {
         if(OSAL_u32ErrorCode()== OSAL_E_TIMEOUT) // It timed-out Test case is passed
         {
            u32RetVal = 0;
            OEDT_HelperPrintf(TR_LEVEL_USER_4, "Sucess:  Wait on message queue timedout as expected.");
         }
         else
         {
            u32RetVal = 2;
            OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: Message queue Wait failed expected error OSAL_E_TIMEOUT but got : ",OSAL_u32ErrorCode());
         }
      }
      else
      {
         u32RetVal += 4;
         OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: Message queue Wait Passed !!!! Expcted to FAIL !!! ");
      }

      if(OSAL_s32MessageQueueClose(mqHandle)==OSAL_ERROR)
      {
         u32RetVal += 8;
         OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: closing message queue failed in u32MsgQueTimeoutTest");
      }

      if(OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME)== OSAL_ERROR)
      {
         u32RetVal += 16;
         OEDT_HelperPrintf(TR_LEVEL_ERRORS, "ERROR: OSAL_s32MessageQueueDelete failed in u32MsgQueTimeoutTest");
      }
   }
   else
   {
      u32RetVal += 32;
      OEDT_HelperPrintf(TR_LEVEL_FATAL, "ERROR: OSAL_s32MessageQueueCreate Failed !!!");
   }
   retur
#endif
TEST(OsalCoreMqueueTest, u32MsgQuePostTimeout)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char Buffer[10] = {1,2,3,4,5,6,7,8,9,0};
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate( MESSAGEQUEUE_NAME,3,20,OSAL_EN_READWRITE,&hMQ ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)Buffer,10,0));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)Buffer,10,0));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)Buffer,10,0));
   tU32 StartTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueuePost( hMQ,(tPCU8)Buffer,10,0));
   tU32 EndTime = OSAL_ClockGetElapsedTime();
   EXPECT_GE(EndTime-StartTime,3000);
   EXPECT_EQ(OSAL_E_QUEUEFULL,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(MESSAGEQUEUE_NAME));
}


TEST(OsalCoreMqueueTest, u32RingBufferParameterCheck)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8SendBuf[20];
 
   EXPECT_EQ(OSAL_ERROR,OSAL_s32CreateRingBuffer(NULL,500,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32CreateRingBuffer("TEST_RNGBUF",0,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READWRITE,NULL));

   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READONLY,&hMQ));
   EXPECT_EQ(0,OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,20,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_E_NOPERMISSION,OSAL_u32ErrorCode());

   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));

   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_WRITEONLY,&hMQ));
   EXPECT_EQ(0,OSAL_u32ReadFromRingBuffer(hMQ,s8SendBuf,20,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_E_NOPERMISSION,OSAL_u32ErrorCode());

   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}


TEST(OsalCoreMqueueTest, u32RingBufferCreateDelete)
{
   OSAL_tMQueueHandle hMQ  = 0;
 
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}

TEST(OsalCoreMqueueTest, u32RingBufferReceiveNoBlocking)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8RecBuf[20];
   
   memset(s8RecBuf,0,20);
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(0,OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}

TEST(OsalCoreMqueueTest, u32RingBufferStatus)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8RecBuf[20];
   tU32 u32Message  =0;
   memset(s8RecBuf,0,20);
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(0,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   EXPECT_EQ(0,u32Message);
   EXPECT_EQ(20,OSAL_u32WriteToRingBuffer(hMQ,s8RecBuf,20,OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(0,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   EXPECT_EQ(1,u32Message);
   EXPECT_EQ(20,OSAL_u32WriteToRingBuffer(hMQ,s8RecBuf,20,OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(0,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   EXPECT_EQ(2,u32Message);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}


TEST(OsalCoreMqueueTest, u32RingBufferSendReceive)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8SendBuf[20];
   char s8RecBuf[20];
   
   memset(s8SendBuf,0,20);
   memset(s8RecBuf,0,20);
   
   strncpy((char*)s8SendBuf,"This is a Test",strlen("This is a Test"));
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",500,OSAL_EN_READWRITE,&hMQ));
   EXPECT_EQ(strlen(s8SendBuf),OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen(s8SendBuf),OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(strlen(s8SendBuf),OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen(s8SendBuf),OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(strlen(s8SendBuf),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(0,strcmp(s8RecBuf,s8SendBuf));
   EXPECT_EQ(strlen(s8SendBuf),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(0,strcmp(s8RecBuf,s8SendBuf));
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}


void ThreadForSendingMsg( void* pArg )
{
  OSAL_tMQueueHandle hMQ = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[20];
  memset(s8SendBuf,0,20);
  strncpy((char*)s8SendBuf,"This is a Test",strlen("This is a Test"));

  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("TEST_RNGBUF",OSAL_EN_READWRITE,&hMQ));

  TraceString("Thread1ForSendingMessage starts");
  for(i=0;i<10000;i++)
  {
     EXPECT_EQ(strlen(s8SendBuf),OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen(s8SendBuf),OSAL_C_TIMEOUT_FOREVER));
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
}

/*
TEST(OsalCoreMqueueTest, u32RingBufferMultipleSender)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8RecBuf[20];
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
   tU32 u32Message =0 ;
 
   memset(s8RecBuf,0,20);
   
   attr.szName = (tString)"SendRngBuf1";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForSendingMsg;
   attr.pvArg = OSAL_NULL;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",100,OSAL_EN_READWRITE,&hMQ));
   OSAL_s32ThreadWait(1000);
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
   attr.szName = (tString)"SendRngBuf2";
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   for(i=0;i<20000;i++)
   {
      EXPECT_EQ(strlen("This is a Test"),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,5000));
      EXPECT_EQ(0,strcmp(s8RecBuf,"This is a Test"));
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   TraceString("%d messages still in ringbuffer",u32Message);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
   OSAL_s32ThreadWait(1000);
}*/




void ThreadForSendingMessage( void* pArg )
{
  OSAL_tMQueueHandle hMQ = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[20];
  memset(s8SendBuf,0,20);
  strncpy((char*)s8SendBuf,"This is a Test",strlen("This is a Test"));

  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("TEST_RNGBUF",OSAL_EN_READWRITE,&hMQ));

  for(i=0;i<20;i++)
  {
     EXPECT_EQ(strlen(s8SendBuf),OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen(s8SendBuf),OSAL_C_TIMEOUT_FOREVER));
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
}


TEST(OsalCoreMqueueTest, u32RingBufferMultipleSendReceive)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8RecBuf[20];
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
   tU32 u32Message =0 ;
 
   memset(s8RecBuf,0,20);
   
   attr.szName = (tString)"SendRngBuf";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForSendingMessage;
   attr.pvArg = OSAL_NULL;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",100,OSAL_EN_READWRITE,&hMQ));
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   OSAL_s32ThreadWait(1000);
   for(i=0;i<20;i++)
   {
      EXPECT_EQ(strlen("This is a Test"),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,5000));
      EXPECT_EQ(0,strcmp(s8RecBuf,"This is a Test"));
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   TraceString("%d messages still in ringbuffer",u32Message);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
   OSAL_s32ThreadWait(1000);
}



void ThreadForSendingVarMessage( void* pArg )
{
  OSAL_tMQueueHandle hMQ = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[200];
  memset(s8SendBuf,0,200);
 
  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("TEST_RNGBUF",OSAL_EN_READWRITE,&hMQ));
  snprintf((char*)s8SendBuf,200,"Test");
  for(i=0;i<150;i++)
  {
     strcat((char*)s8SendBuf,"N");
     EXPECT_EQ(strlen(s8SendBuf),OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen(s8SendBuf),OSAL_C_TIMEOUT_FOREVER));
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
}


TEST(OsalCoreMqueueTest, u32RingBufferMultipleSendReceiveVar)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8RecBuf[200];
   char s8RefBuf[200];
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
   tU32 u32Message =0 ;
 
   memset(s8RecBuf,0,200);
   memset(s8RefBuf,0,200);
   
   attr.szName = (tString)"SendRngBuf";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForSendingVarMessage;
   attr.pvArg = OSAL_NULL;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("TEST_RNGBUF",1000,OSAL_EN_READWRITE,&hMQ));
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
   snprintf((char*)s8RefBuf,200,"Test");
 
   OSAL_s32ThreadWait(1000);
   for(i=0;i<150;i++)
   {
      strcat((char*)s8RefBuf,"N");
      EXPECT_EQ(strlen(s8RefBuf),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,200,5000));
      EXPECT_EQ(0,strcmp(s8RecBuf,s8RefBuf));
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   TraceString("%d messages still in ringbuffer",u32Message);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("TEST_RNGBUF"));
}


tBool bSignal = FALSE;
void vDevMediaNotiTestHandler( void* pArg )
{
   TraceString("vDevMediaNotiTestHandler called");
   bSignal = TRUE;
}


void ThreadForReadingMessage( void* pArg )
{
  OSAL_tMQueueHandle hMQ = 0;
  ((void)pArg);
  char s8RecBuf[20];
  memset(s8RecBuf,0,20);
  strncpy((char*)s8RecBuf,"This is a Test",strlen("This is a Test"));

  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("NOTIFY",OSAL_EN_READWRITE,&hMQ));
  EXPECT_EQ(OSAL_OK,OSAL_u32RingBufferNotify(hMQ,vDevMediaNotiTestHandler,NULL));
  while(bSignal == FALSE)
  {
     OSAL_s32ThreadWait(100);
  }
  EXPECT_EQ(strlen("This is a Test"),OSAL_u32ReadFromRingBuffer(hMQ,s8RecBuf,20,5000));

  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
}


TEST(OsalCoreMqueueTest, u32RingBufferNotify)
{
   OSAL_tMQueueHandle hMQ  = 0;
   char s8SendBuf[20];
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   tU32 u32Message =0 ;
 
   memset(s8SendBuf,0,20);
   strncpy((char*)s8SendBuf,"This is a Test",strlen("This is a Test"));
  
   attr.szName = (tString)"ReadRngBuf";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForReadingMessage;
   attr.pvArg = OSAL_NULL;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("NOTIFY",100,OSAL_EN_READWRITE,&hMQ));
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   OSAL_s32ThreadWait(1000);
   EXPECT_NE(OSAL_ERROR,OSAL_u32WriteToRingBuffer(hMQ,s8SendBuf,strlen("This is a Test"),OSAL_C_TIMEOUT_FOREVER));

   OSAL_s32ThreadWait(1000);

   EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ,&u32Message));
   TraceString("%d messages still in ringbuffer",u32Message);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("NOTIFY"));
}



#define ROUNDS 100000
#define MSG_SIZE 8

/*int Mask = 23;
int lock_value = 1;

TEST(OsalCoreMqueueTest, u32FutexSetWaitTimeout)
{
  tU32 Start = OSAL_ClockGetElapsedTime();
  EXPECT_EQ(-1,futex_setwait(&lock_value,3000,Mask));
  tU32 End = OSAL_ClockGetElapsedTime();
  EXPECT_GE(3030,End-Start);
  EXPECT_LE(3000,End-Start);
}

void ThreadForPost( void* pArg )
{
  OSAL_s32ThreadWait(3000);
  EXPECT_EQ(0,futex_post(&lock_value));
}

TEST(OsalCoreMqueueTest, u32FutexWait)
{
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   attr.szName = (tString)"FutexWait";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForPost;
   attr.pvArg = OSAL_NULL;
   lock_value = 0;
   tU32 Start = OSAL_ClockGetElapsedTime();
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
   
   EXPECT_EQ(0,futex_wait(&lock_value));
   tU32 End = OSAL_ClockGetElapsedTime();
   EXPECT_GE(3030,End-Start);
   EXPECT_LE(3000,End-Start);
  lock_value = 1;
}


void ThreadForWait( void* pArg )
{
  OSAL_tEventHandle evHandle  = 0;
  EXPECT_EQ(OSAL_OK, OSAL_s32EventOpen("FutexTest",&evHandle));
  EXPECT_EQ(0,futex_setwait(&lock_value,OSAL_C_TIMEOUT_FOREVER,Mask));

  EXPECT_EQ(OSAL_OK, OSAL_s32EventPost(evHandle,0x1,OSAL_EN_EVENTMASK_OR));
  EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
}


TEST(OsalCoreMqueueTest, u32FutexWaitAndWakeup)
{
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   OSAL_tEventHandle evHandle  = 0;

   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate("FutexTest",&evHandle));
 
   attr.szName = (tString)"SendEvent";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForWait;
   attr.pvArg = OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
  
   OSAL_s32ThreadWait(1000);
    
   futex_wakeup(&lock_value,Mask);

   EXPECT_EQ(OSAL_OK, OSAL_s32EventWait(evHandle,0x1, OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,NULL));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete("FutexTest"));  
}*/

void ThreadForMessagePerf( void* pArg )
{
  OSAL_tMQueueHandle hMQ1 = 0;
  OSAL_tMQueueHandle hMQ2 = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[MSG_SIZE];
  char s8RecBuf[MSG_SIZE];
  memset(s8SendBuf,'A',MSG_SIZE);
  
  EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen ("MQ1",OSAL_EN_READWRITE,&hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueOpen ("MQ2",OSAL_EN_READWRITE,&hMQ2));

  EXPECT_NE(OSAL_ERROR,OSAL_s32MessageQueuePost(hMQ1,(tPCU8)s8SendBuf,8,0));
  for(i=0;i<ROUNDS;i++)
  {
     OSAL_s32MessageQueueWait(hMQ2,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER);
     OSAL_s32MessageQueuePost(hMQ1,(tPCU8)s8SendBuf,MSG_SIZE,0);
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ2));
}


TEST(OsalCoreMqueueTest, u32MultipleSendReceivePerf)
{
   OSAL_tMQueueHandle hMQ1  = 0;
   OSAL_tMQueueHandle hMQ2  = 0;
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
  char s8SendBuf[MSG_SIZE];
  char s8RecBuf[MSG_SIZE];
  memset(s8SendBuf,'B',MSG_SIZE);
   
   attr.szName = (tString)"UserMQ";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForMessagePerf;
   attr.pvArg = OSAL_NULL;
   OSAL_s32ThreadWait(3000); 
   
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate("MQ1",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate("MQ2",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ2));
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   OSAL_s32ThreadWait(1000);
   EXPECT_NE(OSAL_ERROR,OSAL_s32MessageQueueWait(hMQ1,(tPU8)s8RecBuf,8,0,OSAL_C_TIMEOUT_FOREVER));
   
   tU32 StartTime = OSAL_ClockGetElapsedTime();
   
   for(i=0;i<ROUNDS;i++)
   {
     OSAL_s32MessageQueuePost(hMQ2,(tPCU8)s8SendBuf,MSG_SIZE,0);
     OSAL_s32MessageQueueWait(hMQ1,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER);
   }
   tU32 EndTime = OSAL_ClockGetElapsedTime();
   TraceString("Send/receive rounds %d with msg size:%d need %d msec",ROUNDS,8,EndTime-StartTime);
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete("MQ1"));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ2));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete("MQ2"));

   OSAL_s32ThreadWait(3000); 
}

void ThreadForSendingMessagePerf( void* pArg )
{
  OSAL_tMQueueHandle hMQ1 = 0;
  OSAL_tMQueueHandle hMQ2 = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[20];
  char s8RecBuf[20];
  memset(s8SendBuf,'A',20);
  tU32 u32Message = 0;

  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("RNGBUF1",OSAL_EN_READWRITE,&hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32OpenRingBuffer("RNGBUF2",OSAL_EN_READWRITE,&hMQ2));

  
  EXPECT_NE(OSAL_ERROR,OSAL_u32WriteToRingBuffer(hMQ1,s8SendBuf,8,OSAL_C_TIMEOUT_FOREVER));
  for(i=0;i<ROUNDS;i++)
  {
     if(OSAL_u32ReadFromRingBuffer(hMQ2,s8RecBuf,MSG_SIZE,3000) == 0)
     {
         EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ2,&u32Message));
         TraceString("%d messages still in ringbuffer",u32Message);
     }
     OSAL_u32WriteToRingBuffer(hMQ1,s8SendBuf,MSG_SIZE,OSAL_C_TIMEOUT_FOREVER);
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ2));
}


TEST(OsalCoreMqueueTest, u32RingBufferMultipleSendReceivePerf)
{
   OSAL_tMQueueHandle hMQ1  = 0;
   OSAL_tMQueueHandle hMQ2  = 0;
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
  tU32 u32Message = 0;
  char s8SendBuf[MSG_SIZE];
  char s8RecBuf[MSG_SIZE];
  memset(s8SendBuf,'B',MSG_SIZE);

  OSAL_s32ThreadWait(3000); 
   
   attr.szName = (tString)"SendRngBufPerf";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForSendingMessagePerf;
   attr.pvArg = OSAL_NULL;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("RNGBUF1",5000,OSAL_EN_READWRITE,&hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32CreateRingBuffer("RNGBUF2",5000,OSAL_EN_READWRITE,&hMQ2));
   
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   OSAL_s32ThreadWait(1000);
   EXPECT_NE(OSAL_ERROR,OSAL_u32ReadFromRingBuffer(hMQ1,s8RecBuf,MSG_SIZE,OSAL_C_TIMEOUT_FOREVER));
   
   tU32 StartTime = OSAL_ClockGetElapsedTime();
   
   for(i=0;i<ROUNDS;i++)
   {
     OSAL_u32WriteToRingBuffer(hMQ2,s8SendBuf,MSG_SIZE,OSAL_C_TIMEOUT_FOREVER);
     if(OSAL_u32ReadFromRingBuffer(hMQ1,s8RecBuf,MSG_SIZE,3000) == 0)
     {
         EXPECT_EQ(OSAL_OK,OSAL_s32RingBufferStatus(hMQ1,&u32Message));
         TraceString("%d messages still in ringbuffer",u32Message);
     }
   }
   tU32 EndTime = OSAL_ClockGetElapsedTime();
   TraceString("Send/receive rounds %d with msg size:%d need %d msec",ROUNDS,MSG_SIZE,EndTime-StartTime);
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("RNGBUF1"));
   EXPECT_EQ(OSAL_OK,OSAL_s32CloseRingBuffer(hMQ2));
   EXPECT_EQ(OSAL_OK,OSAL_s32DeleteRingBuffer("RNGBUF2"));
   OSAL_s32ThreadWait(3000); 
}


#ifdef OSAL_SHM_MQ
/////////////////////////////////////////////////////7
TEST(OsalCoreMqueueTest, u32MultipleCreOpnClsDelNew1)
{
   OSAL_tMQueueHandle hMQ1 = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueCreate("MQ1",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueOpen("MQ1", OSAL_EN_READWRITE, &hMQ1))   ;
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueDelete("MQ1"));
}

TEST(OsalCoreMqueueTest, u32MultipleSendReceiveNew1)
{
   OSAL_tMQueueHandle hMQ1 = 0;
   char s8SendBuf[100] = {'T','e','s','t','M','s','g','\0'};
   char s8RecBuf[100];
   
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueCreate("MQ1",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ1));
   EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueuePost(hMQ1,(tPCU8)s8SendBuf,MSG_SIZE,0));
   EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueueWait(hMQ1,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(0,strncmp(s8SendBuf,s8RecBuf,MSG_SIZE));
   EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueuePost(hMQ1,(tPCU8)s8SendBuf,MSG_SIZE,0));
   EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueueWait(hMQ1,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(0,strncmp(s8SendBuf,s8RecBuf,MSG_SIZE));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueDelete("MQ1"));
}

void ThreadForMessageNewPerf( void* pArg )
{
  OSAL_tMQueueHandle hMQ1 = 0;
  OSAL_tMQueueHandle hMQ2 = 0;
  ((void)pArg);
  int i;
  char s8SendBuf[100];
  char s8RecBuf[100];
  memset(&s8SendBuf[0],'A',MSG_SIZE);
  
  EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueOpen("MQ1",OSAL_EN_READWRITE,&hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueOpen("MQ2",OSAL_EN_READWRITE,&hMQ2));

  EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueuePost(hMQ1,(tPCU8)s8SendBuf,MSG_SIZE,0));
  for(i=0;i<ROUNDS;i++)
  {
     OSAL_s32ShMemMessageQueueWait(hMQ2,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER);
     OSAL_s32ShMemMessageQueuePost(hMQ1,(tPCU8)s8SendBuf,MSG_SIZE,0);
  }
  EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ1));
  EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ2));
}


TEST(OsalCoreMqueueTest, u32MultipleSendReceiveNewPerf)
{
   OSAL_tMQueueHandle hMQ1  = 0;
   OSAL_tMQueueHandle hMQ2  = 0;
   OSAL_trThreadAttribute  attr = { 0 };
   OSAL_tThreadID ThreadID = 0;
   int i;
  char s8SendBuf[MSG_SIZE];
  char s8RecBuf[MSG_SIZE];
  memset(s8SendBuf,'B',MSG_SIZE);
   
   
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueCreate("MQ1",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueCreate("MQ2",80,MSG_SIZE,OSAL_EN_READWRITE,&hMQ2));
   OSAL_s32ThreadWait(3000); 
   
   attr.szName = (tString)"SendNewPerf";
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = MQ_TR_STACK_SIZE;
   attr.pfEntry = ThreadForMessageNewPerf;
   attr.pvArg = OSAL_NULL;
   EXPECT_NE(OSAL_ERROR,ThreadID = OSAL_ThreadSpawn( &attr ));
 
   OSAL_s32ThreadWait(1000);
   EXPECT_NE(OSAL_ERROR,OSAL_s32ShMemMessageQueueWait(hMQ1,(tPU8)s8RecBuf,8,0,OSAL_C_TIMEOUT_FOREVER));
   
   tU32 StartTime = OSAL_ClockGetElapsedTime();
   
   for(i=0;i<ROUNDS;i++)
   {
     OSAL_s32ShMemMessageQueuePost(hMQ2,(tPCU8)s8SendBuf,MSG_SIZE,0);
     OSAL_s32ShMemMessageQueueWait(hMQ1,(tPU8)s8RecBuf,MSG_SIZE,0,OSAL_C_TIMEOUT_FOREVER);
   }
   tU32 EndTime = OSAL_ClockGetElapsedTime();
   TraceString("Send/receive rounds %d with msg size:%d need %d msec",ROUNDS,8,EndTime-StartTime);
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ1));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueDelete("MQ1"));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueClose(hMQ2));
   EXPECT_EQ(OSAL_OK,OSAL_s32ShMemMessageQueueDelete("MQ2"));

   OSAL_s32ThreadWait(3000); 
}

#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
