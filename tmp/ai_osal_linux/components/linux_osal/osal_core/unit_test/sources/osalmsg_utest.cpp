/*****************************************************************************
| FILE:         osalmsg_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2015 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;
 
#define MSGPL_POOLSIZE_FIVE_TH 5000
#define MSGPL_POOLSIZE_SIX_TH 6000

#define MSGPL_MSG_SIZE 1000
#define MSGPL_INVAL_LOCATION 1000 // According to osmsg.h

#define MSGPL_FLAG_UP 1
#define MSGPL_MAX 20
#define MSGPL_MIN 0
#ifdef TEST
/*********************************************************************/
/* Test Cases for OSAL Mesae API                                     */
/*********************************************************************/
TEST(OsalCoreMsgTest, u32MSGPLCGetAbsolute_CurrentSize)
{
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolCreate(0x1FA000));
   tU32 u32RetSize1 = (tU32)OSAL_s32MessagePoolGetAbsoluteSize();
   tU32 u32RetSize2 = (tU32)OSAL_s32MessagePoolGetCurrentSize();
   EXPECT_EQ(u32RetSize1,u32RetSize2);
   EXPECT_NE(u32RetSize1,0);
}

TEST(OsalCoreMsgTest, u32MSGPLOpen)
{
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}

TEST(OsalCoreMsgTest, u32MSGPLCreateMessageNULL)
{
   tU32 u32Size = 16;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageCreate(OSAL_NULL, u32Size, OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}  

TEST(OsalCoreMsgTest, u32MSGPLCreateSizeZero)
{
   OSAL_trMessage OSAL_msg;
   tU32 u32Size =0;
    
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageCreate( &OSAL_msg,u32Size, OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}

TEST(OsalCoreMsgTest, u32MSGPLCreateLocationInVal)
{
   OSAL_trMessage OSAL_msg;
   tU32 u32Size = 16;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageCreate( &(OSAL_msg), u32Size,OSAL_EN_MEMORY_INVALID));	  
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}

TEST(OsalCoreMsgTest, u32MSGPLGetSMSize)
{
   tU32 u32RetSizeBeforeCreate = 0;
   tU32 u32RetSizeAfterCreate = 0;
   tU32 u32RetSizeAfterDelete = 0;
   OSAL_trMessage OSAL_msg[50];
   tU32 u32Size = 16;
   int i;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   u32RetSizeBeforeCreate = (tU32)OSAL_s32MessagePoolGetCurrentSize();
   for (i=0;i<50;i++)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &(OSAL_msg[i]), u32Size,OSAL_EN_MEMORY_SHARED));	  
   }
   u32RetSizeAfterCreate = (tU32)OSAL_s32MessagePoolGetCurrentSize();
   for (i=0;i<50;i++)
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg[i]));
   }
   u32RetSizeAfterDelete = (tU32)OSAL_s32MessagePoolGetCurrentSize();
   EXPECT_LT(u32RetSizeAfterCreate,u32RetSizeAfterDelete);
#ifndef Gen3ArmMake // on ARM target we have no clean OSAL environment
   EXPECT_EQ(u32RetSizeBeforeCreate,u32RetSizeAfterDelete);
#endif
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
 }


TEST(OsalCoreMsgTest, u32MSGPLGetSMContent)
{
   tPU8 MsgPtr = NULL;
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};

   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   /* Get a pointer to a message with unitialized OSAL_trMessage structure*/
   EXPECT_EQ(NULL,(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}  


TEST(OsalCoreMsgTest, u32MSGPLGetSMContentDiffMode)
{
   tU32 u32Size = 0;
   tPU8 MsgPtr = NULL;
   OSAL_trMessage OSAL_msg;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   u32Size = MSGPL_MSG_SIZE;
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &(OSAL_msg), u32Size,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE(0,(uintptr_t)(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_WRITEONLY)));
   EXPECT_NE(0,(uintptr_t)(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READONLY)));
   EXPECT_NE(0,(uintptr_t)(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete(OSAL_msg));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}  

TEST(OsalCoreMsgTest, u32MSGPLGetLMSize)
{
   tU32 u32Size = 0;
   OSAL_trMessage OSAL_msg;

    EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
    u32Size = MSGPL_MSG_SIZE;
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &(OSAL_msg), u32Size,OSAL_EN_MEMORY_LOCAL));	  
    EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg));
    EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}

TEST(OsalCoreMsgTest, u32MSGPLGetLMContentDiffMode)
{
   tU32 u32Size = MSGPL_MSG_SIZE;
   tPU8 MsgPtr = NULL;
   OSAL_trMessage OSAL_msg;
   u32Size = MSGPL_POOLSIZE_FIVE_TH;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg, u32Size,OSAL_EN_MEMORY_LOCAL));	 
   /* To Get a pointer to a message with valid access parameter(OSAL_EN_WRITEONLY)*/
   EXPECT_NE(0,(uintptr_t)(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_WRITEONLY)));
   /* To Get a pointer to a message with valid access parameter(OSAL_EN_READONLY)*/
   EXPECT_NE(0,(uintptr_t)(MsgPtr = OSAL_pu8MessageContentGet( OSAL_msg, OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete(OSAL_msg));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose()); 	
}  

TEST(OsalCoreMsgTest, u32MSGPLDeleteMessageNULL)
{
   /*rav8kor - uninitialised declaration gives lint warning.hence invalid memory location is used*/
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};
  
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageDelete( OSAL_msg));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreMsgTest, u32MSGPLMsgCreateWithNoMSGPLInLM)
{
   OSAL_trMessage OSAL_msg;

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate(&OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete(OSAL_msg));
}


TEST(OsalCoreMsgTest, u32MSGPLCheckMSGPL)
{
   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32CheckMessagePool());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}	


TEST(OsalCoreMsgTest, u32MSGPLGetMessageSizeMsgInLM)
{
   OSAL_trMessage OSAL_msg;
	
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate(&OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL));
   EXPECT_EQ(0,OSAL_u32GetMessageSize( OSAL_msg));// is not supported for LOCAL memory
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete(OSAL_msg));
}

TEST(OsalCoreMsgTest, u32MSGPLGetMessageSizeMsgInSM)
{
   OSAL_trMessage OSAL_msg;

   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate(&OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(MSGPL_MSG_SIZE,OSAL_u32GetMessageSize( OSAL_msg));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete(OSAL_msg));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}

TEST(OsalCoreMsgTest, u32MSGPLGetMessageSizeNoMsg)
{
   OSAL_trMessage OSAL_msg = {OSAL_EN_MEMORY_INVALID,0};
   EXPECT_EQ(0,OSAL_u32GetMessageSize( OSAL_msg ));
}

TEST(OsalCoreMsgTest, u32MSGPLGetMinimalSize)
{
   tS32 s32Size     = 0;
   tS32 s32SizeNext = 0;
   OSAL_trMessage OSAL_msg;
   OSAL_trMessage OSAL_msgNext;

   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE(OSAL_ERROR,(s32Size = OSAL_s32MessagePoolGetMinimalSize()));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msgNext,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE(OSAL_ERROR,( s32SizeNext = OSAL_s32MessagePoolGetMinimalSize()));
   /*Compare two sizes - s32SizeNext must be lesser than s32Size*/
   EXPECT_LT(s32SizeNext,s32Size );
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msgNext));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}

TEST(OsalCoreMsgTest, u32MSGPLGetMinimalSizeCreatMSGLM)
{
   tS32 s32Size     = 0;
   tS32 s32SizeNext = 0;
   OSAL_trMessage OSAL_msg;
   OSAL_trMessage OSAL_msgNext;
	
   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE(OSAL_ERROR,( s32Size = OSAL_s32MessagePoolGetMinimalSize()));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msgNext,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_LOCAL));
   EXPECT_NE(OSAL_ERROR,( s32SizeNext = OSAL_s32MessagePoolGetMinimalSize()));
   EXPECT_EQ(s32SizeNext,s32Size);
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
} 

TEST(OsalCoreMsgTest, u32MSGPLGetMessageMaxSizeInvalid)
{
   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_NE(OSAL_ERROR,OSAL_u32GetMaxMessageSize());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}

TEST(OsalCoreMsgTest, u32MSGPLGetMessageMaxSizeValid)
{
   tU32 u32Size_1   = 0;
   tU32 u32Size_2  = 0;
   OSAL_trMessage OSAL_msg_1;
   OSAL_trMessage OSAL_msg_2;
   OSAL_trMessage OSAL_msg_3;

   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_NE(OSAL_ERROR, ( u32Size_1 = OSAL_u32GetMaxMessageSize()));
   /*Create 3 messages in the Message Pool*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_1,MSGPL_MSG_SIZE,OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_2,MSGPL_MSG_SIZE-100,OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_3,MSGPL_MSG_SIZE-200,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE(OSAL_ERROR,( u32Size_2 = OSAL_u32GetMaxMessageSize()));
   EXPECT_LT(MSGPL_MSG_SIZE,u32Size_2 );
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_3));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_2));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}
#endif
TEST(OsalCoreMsgTest, u32MSGPLGetBigMessage)
{
   OSAL_trMessage OSAL_msg_1;
   OSAL_trMessage OSAL_msg_2;
   OSAL_trMessage OSAL_msg_3;
   tU32 u32MsgSize1 = 100*1024;
   tU32 u32MsgSize2 = 101*1024;
   tU32 u32MsgSize3 = 102*1024;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tPU8 pu8Val=0;
   
   pOsalData->u32MmapMsgSize = 99*1024;
   
   EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_1,u32MsgSize1,OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_2,u32MsgSize2,OSAL_EN_MEMORY_SHARED));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_3,u32MsgSize3,OSAL_EN_MEMORY_SHARED));
   EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_1,enAccess)));
   memset(pu8Val,'1',u32MsgSize1);
   EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_2,enAccess)));
   memset(pu8Val,'2',u32MsgSize2);
   EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_3,enAccess)));
   memset(pu8Val,'3',u32MsgSize3);
  
   EXPECT_EQ(u32MsgSize1,OSAL_u32GetMessageSize(OSAL_msg_1));// is not supported for LOCAL memory
   EXPECT_EQ(u32MsgSize2,OSAL_u32GetMessageSize(OSAL_msg_2));// is not supported for LOCAL memory
   EXPECT_EQ(u32MsgSize3,OSAL_u32GetMessageSize(OSAL_msg_3));// is not supported for LOCAL memory

   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_3));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_2));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
}
 
 
   
TEST(OsalCoreMsgTest, u32MSGPLRrcGetBigMessage)
{
   OSAL_trMessage OSAL_msg_1;
   OSAL_trMessage OSAL_msg_2;
   OSAL_trMessage OSAL_msg_3;
   tU32 u32MsgSize1 = 100*1024;
   tU32 u32MsgSize2 = 101*1024;
   tU32 u32MsgSize3 = 102*1024;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tPU8 pu8Val=0;

   OSAL_trProcessAttribute rAttr = { 0 };
 
   char Name[64];
   char* pCwdName = get_current_dir_name();
   strcpy(Name,pCwdName);
   strcat(Name,"/unittest_testprc_out.out");

   rAttr.szAppName   = (tString)Name;
   rAttr.szName      = (tString)Name;
   rAttr.szCommandLine = (tString)" -t 1";

    
   OSAL_tProcessID  PID = OSAL_ProcessSpawn(&rAttr);
   if (PID != OSAL_ERROR) 
   {
      pOsalData->u32MmapMsgSize = 99*1024;
   
      EXPECT_EQ(OSAL_OK, OSAL_s32MessagePoolOpen());
   
      OSAL_tMQueueHandle hMQ  = 0;
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate( "BigMsg",20,sizeof(OSAL_trMessage),OSAL_EN_READWRITE,&hMQ ) );
	  OSAL_s32ThreadWait(5000);

      EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_1,u32MsgSize1,OSAL_EN_MEMORY_SHARED));
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_2,u32MsgSize2,OSAL_EN_MEMORY_SHARED));
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_3,u32MsgSize3,OSAL_EN_MEMORY_SHARED));
      EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_1,enAccess)));
      memset(pu8Val,'1',u32MsgSize1);
      EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_2,enAccess)));
      memset(pu8Val,'2',u32MsgSize2);
      EXPECT_NE((tPU8)OSAL_NULL,(pu8Val = OSAL_pu8MessageContentGet(OSAL_msg_3,enAccess)));
      memset(pu8Val,'3',u32MsgSize3);
  
      EXPECT_EQ(u32MsgSize1,OSAL_u32GetMessageSize(OSAL_msg_1));
      EXPECT_EQ(u32MsgSize2,OSAL_u32GetMessageSize(OSAL_msg_2));
      EXPECT_EQ(u32MsgSize3,OSAL_u32GetMessageSize(OSAL_msg_3));

      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)&OSAL_msg_1,sizeof(OSAL_trMessage),0));
	  OSAL_s32ThreadWait(100);
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)&OSAL_msg_2,sizeof(OSAL_trMessage),0));
	  OSAL_s32ThreadWait(100);
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueuePost( hMQ,(tPCU8)&OSAL_msg_3,sizeof(OSAL_trMessage),0));
   
	  OSAL_s32ThreadWait(1000);
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageCreate( &OSAL_msg_1,u32MsgSize1,OSAL_EN_MEMORY_SHARED));
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageDelete( OSAL_msg_1));
	  
      EXPECT_EQ(OSAL_OK,OSAL_s32MessagePoolClose());
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(hMQ));
      EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete("BigMsg"));
	  
	 // OSAL_s32ProcessJoin(PID);
   }
}
 
 
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
