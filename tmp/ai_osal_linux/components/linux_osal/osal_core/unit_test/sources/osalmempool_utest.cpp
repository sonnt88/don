/*****************************************************************************
| FILE:         osalmempool_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;



/*********************************************************************/
/* Test Cases for OSAL Memory Pool API                               */
/*********************************************************************/

TEST(OsalCoreMemPoolTest, u32OpenClose)
{
    trHandleMpf Handle;
    EXPECT_EQ(OSAL_OK,OSAL_u32MemPoolFixSizeCreate("TestPool",12,5,&Handle,TRUE,TRUE));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeDelete ("TestPool"));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeClose (&Handle));
}

TEST(OsalCoreMemPoolTest, u32AllocOverLimit)
{
    trHandleMpf Handle;
    void* pPtr[6];
    EXPECT_EQ(OSAL_OK,OSAL_u32MemPoolFixSizeCreate("TestPool",12,5,&Handle,TRUE,TRUE));

    EXPECT_NE(0,(uintptr_t)(pPtr[0] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));
    EXPECT_NE(0,(uintptr_t)(pPtr[1] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));
    EXPECT_NE(0,(uintptr_t)(pPtr[2] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));
    EXPECT_NE(0,(uintptr_t)(pPtr[3] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));
    EXPECT_NE(0,(uintptr_t)(pPtr[4] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));
    EXPECT_NE(0,(uintptr_t)(pPtr[5] = OSAL_pvMemPoolFixSizeGetBlockOfPool(&Handle)));

    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[0]));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[1]));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[2]));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[3]));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[4]));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeRelBlockOfPool(&Handle,pPtr[5]));

    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeDelete ("TestPool"));
    EXPECT_EQ(OSAL_OK,OSAL_s32MemPoolFixSizeClose (&Handle));
}

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
