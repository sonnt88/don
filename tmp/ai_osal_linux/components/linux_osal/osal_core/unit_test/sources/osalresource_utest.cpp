/*****************************************************************************
| FILE:         rbtree_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

#include <stdlib.h>

using namespace std;

/*********************************************************************/
/* Test Cases for OSAL RB Tree API                                     */
/*********************************************************************/
/*TEST(OSALRESOURCE, Resource_SemHandlePool)
{
  tU32 u32ToOpen;
  tU32 u32Count = 0;
  trOSAL_MPF* pPool = (trOSAL_MPF*)&SemMemPoolHandle.pMem;
  OSAL_tSemHandle hSem;
  EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate("TestMaxSemPool",&hSem,0));

  u32ToOpen = pPool->freecnt;
  
  OSAL_tSemHandle* pHandleArray = (OSAL_tSemHandle*)malloc(u32ToOpen*sizeof(OSAL_tSemHandle));
  if(pHandleArray)
  {
     for(u32Count=0;u32Count<u32ToOpen;u32Count++)
	 {
        EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreOpen("TestMaxSemPool",&pHandleArray[u32Count]));
	 }
     for(u32Count=0;u32Count<u32ToOpen;u32Count++)
	 {
     	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(pHandleArray[u32Count]));
	 }
   	 EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete("TestMaxSemPool"));
  }

  EXPECT_EQ(u32ToOpen+1,pPool->freecnt);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}*/


TEST(OSALRESOURCE, Resource_Sema)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrSemaphoreElements - pOsalData->u32SemResCount;
  tU32 u32Count = 0;
  
  OSAL_tSemHandle* pHandleArray = (OSAL_tSemHandle*)malloc(u32ToCreate*sizeof(OSAL_tSemHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestSema_%d",u32Count);
        EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate(Name,&pHandleArray[u32Count],0));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestSema_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_s32SemaphoreCreate(Name,&pHandleArray[u32Count],0));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestSema_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrSemaphoreElements - pOsalData->u32SemResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

/*
TEST(OSALRESOURCE, Resource_EventHandlePool)
{
  tU32 u32ToOpen;
  tU32 u32Count = 0;
  trOSAL_MPF* pPool = (trOSAL_MPF*)&EvMemPoolHandle.pMem;
  OSAL_tEventHandle hEvent;
  EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate("TestMaxEventPool",&hEvent));

  u32ToOpen = pPool->freecnt;
  
  OSAL_tEventHandle* pHandleArray = (OSAL_tEventHandle*)malloc(u32ToOpen*sizeof(OSAL_tEventHandle));
  if(pHandleArray)
  {
     for(u32Count=0;u32Count<u32ToOpen;u32Count++)
	 {
        EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen("TestMaxEventPool",&pHandleArray[u32Count]));
	 }
     for(u32Count=0;u32Count<u32ToOpen;u32Count++)
	 {
     	EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(pHandleArray[u32Count]));
	 }
   	 EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete("TestMaxEventPool"));
  }

  EXPECT_EQ(u32ToOpen+1,pPool->freecnt);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}*/

TEST(OSALRESOURCE, Resource_Event)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrEventElements - pOsalData->u32EvtResCount;
  tU32 u32Count = 0;
  
  OSAL_tEventHandle* pHandleArray = (OSAL_tEventHandle*)malloc(u32ToCreate*sizeof(OSAL_tEventHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
        EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(Name,&pHandleArray[u32Count]));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_s32EventCreate(Name,&pHandleArray[u32Count]));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrEventElements - pOsalData->u32EvtResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

TEST(OSALRESOURCE, Resource_Mutex)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrMutexElements - pOsalData->u32MutResCount;
  tU32 u32Count = 0;
  
  OSAL_tMtxHandle* pHandleArray = (OSAL_tMtxHandle*)malloc(u32ToCreate*sizeof(OSAL_tMtxHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestMut_%d",u32Count);
        EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(Name,&pHandleArray[u32Count]));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestMut_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexCreate(Name,&pHandleArray[u32Count]));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestMut_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrMutexElements - pOsalData->u32MutResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}
/*
TEST(OSALRESOURCE, Resource_Mq)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrEventElements - pOsalData->u32MqResCount;
  tU32 u32Count = 0;
  
  OSAL_tMessageQueueHandle* pHandleArray = (OSAL_tEventHandle*)malloc(u32ToCreate*sizeof(OSAL_tEventHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
        EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(Name,&pHandleArray[u32Count]));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_s32EventCreate(Name,&pHandleArray[u32Count]));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestEvent_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrEventElements - pOsalData->u32EvtResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}*/

TEST(OSALRESOURCE, Resource_MQ)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrMqElements - pOsalData->u32MqResCount;
  tU32 u32Count = 0;
  
  OSAL_tMQueueHandle* pHandleArray = (OSAL_tMQueueHandle*)malloc(u32ToCreate*sizeof(OSAL_tMQueueHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"ResTestMQ_%d",u32Count);
        EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueCreate(Name,20,20,OSAL_EN_READWRITE,&pHandleArray[u32Count]));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"ResTestMQ_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_s32MessageQueueCreate(Name,20,20,OSAL_EN_READWRITE,&pHandleArray[u32Count]));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"ResTestMQ_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32MessageQueueDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrMqElements - pOsalData->u32MqResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

TEST(OSALRESOURCE, Resource_ShMem)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrSharedMemElements - pOsalData->u32ShMResCount;
  tU32 u32Count = 0;
  
  OSAL_tShMemHandle* pHandleArray = (OSAL_tShMemHandle*)malloc(u32ToCreate*sizeof(OSAL_tShMemHandle));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestShMem_%d",u32Count);
        EXPECT_NE(OSAL_NULL,pHandleArray[u32Count] = OSAL_SharedMemoryCreate(Name,OSAL_EN_READWRITE,4096));
	 }
	 snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestShMem_%d",u32Count);
     EXPECT_EQ(OSAL_ERROR,OSAL_SharedMemoryCreate(Name,OSAL_EN_READWRITE,4096));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
		snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestShMem_%d",u32Count);
     	EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(pHandleArray[u32Count]));
    	EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(Name));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrSharedMemElements - pOsalData->u32ShMResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

tVoid TestTimerCallback(tPVoid argv) 
{
	//Dummy
}


TEST(OSALRESOURCE, Resource_Timer)
{
  tU32 u32ToCreate = pOsalData->u32MaxNrTimerElements - pOsalData->u32TimResCount;
  tU32 u32Count = 0;
  
  OSAL_tTimerHandle* pHandleArray = (OSAL_tTimerHandle*)malloc(u32ToCreate*sizeof(OSAL_tTimerHandle));
  if(pHandleArray)
  {
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
       EXPECT_EQ(OSAL_OK,OSAL_s32TimerCreate(&TestTimerCallback,(void*)&u32Count,&pHandleArray[u32Count]));
	 }
     EXPECT_EQ(OSAL_ERROR,OSAL_s32TimerCreate(&TestTimerCallback,(void*)&u32Count,&pHandleArray[u32Count]));
	 EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
	 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
     	EXPECT_EQ(OSAL_OK,OSAL_s32TimerDelete(pHandleArray[u32Count]));
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrTimerElements - pOsalData->u32TimResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

tVoid TestThread(tPVoid pvArg)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   OSAL_s32ThreadWait(2000);
}

TEST(OSALRESOURCE, Resource_Thread)
{
  char Name[OSAL_C_U32_MAX_NAMELENGTH];
  tU32 u32ToCreate = pOsalData->u32MaxNrThreadElements - pOsalData->u32TskResCount;
  tU32 u32Count = 0;
  OSAL_trThreadAttribute trAttr = {OSAL_NULL};
 
  trAttr.u32Priority            =  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
  trAttr.s32StackSize           =  4096;
  trAttr.pfEntry                =  TestThread;
  trAttr.pvArg                  =  OSAL_NULL;
  
  OSAL_tThreadID* pHandleArray = (OSAL_tThreadID*)malloc(u32ToCreate*sizeof(OSAL_tThreadID));
  if(pHandleArray)
  {
	  
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
     {
        snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestTask_%d",u32Count);
        trAttr.szName = Name;
        EXPECT_NE(OSAL_ERROR,pHandleArray[u32Count]=OSAL_ThreadSpawn(&trAttr));
     }
     snprintf(Name,OSAL_C_U32_MAX_NAMELENGTH,"TestTask_%d",u32Count);
     trAttr.szName = Name;
     EXPECT_EQ(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
     EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());

     OSAL_s32ThreadWait(4000);
 
     for(u32Count=0;u32Count<u32ToCreate;u32Count++)
	 {
   // 	OSAL_s32ThreadDelete(pHandleArray[u32Count]);
	 }
  }
  EXPECT_EQ(u32ToCreate,pOsalData->u32MaxNrThreadElements - pOsalData->u32TskResCount);
  delete pHandleArray;
#ifdef AREA_TEST
  vCheckAreas();
#endif
}

#ifdef TODO_TEST
pOsalData->u32MaxNrProcElements;       /* max resource count set via osal.reg  */
pOsalData->u32PrcResCount; // OSAL Resource counter
 #endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
