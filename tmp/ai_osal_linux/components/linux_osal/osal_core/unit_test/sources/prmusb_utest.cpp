/*****************************************************************************
| FILE:         prmusb_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#ifdef PRM_LIBUSB_CONNECTION 

#include "osal_core_unit_test_mock.h"

#include <stdlib.h>

using namespace std;

/*********************************************************************/
/* Test Cases for OSAL RB Tree API                                     */
/*********************************************************************/

TEST(PrmUsbTest, PrmUsbTest_ScanPorts)
{
    OSAL_tIODescriptor  fd;
    UsbPortState rPortState; 
    tS32 s32PortCount,i;
    EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY )));
    EXPECT_EQ(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT, (intptr_t)&s32PortCount),OSAL_OK);
    TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT found %d ports",s32PortCount);
    for(i=0;i<= s32PortCount;i++)
    {
      rPortState.u8PortNr = (tU8)i; //is first port 0 or 1???
      EXPECT_NE(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (intptr_t)&rPortState),OSAL_ERROR);
    }
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(fd));
}
#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/

