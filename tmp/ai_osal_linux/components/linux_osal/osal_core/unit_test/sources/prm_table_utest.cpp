/*****************************************************************************
| FILE:         prm_table_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is unit test implementation for PRM 
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 02.06.16  | Initial revision           | Vinutha Eshwar Shantha
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

#include <stdlib.h>
#include "prm_table_utest.h"

using namespace std;
using namespace testing;

//#define TRACE
class PrmTableTest : public Test
{
  protected: 
  
   PrmTableTest()
   {     
   }
   virtual void SetUp()
   {
      char command[50] = {0};

      strncpy( command, " mkdir -p  /tmp/.automounter/device_db",sizeof("sudo mkdir -p  /tmp/.automounter/device_db") );
      system(command);
   }
   virtual void TearDown()
   {
      char command[50] = {0};
	 
      strncpy( command, " rm -rf  /tmp/.automounter/",sizeof("rm -rf  /tmp/.automounter/") );
      system(command);
   }
   ~PrmTableTest()
   {      
   }  
};

/*********************************************************************/
    /* Test Case for PRM driver open and close */
/*********************************************************************/
TEST_F(PrmTableTest, u32PRM_DrvOpnClose)
{
   OSAL_tIODescriptor  fd = 0;
  
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY )));   
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

/*********************************************************************/
/*  Test Case to test if prm is initialised */
/*********************************************************************/
TEST_F(PrmTableTest, vCheckPrmSuccess)
{
   tU16 testValue = OSAL_C_U16_INCORRECT_MEDIA;
   
   #ifdef TRACE
   TraceString("BEFORE vCheckPrm value = %d\n",testValue);
   #endif
   
   vCheckPrm_wrapper();
   testValue = pOsalData->u16SdCardType;
   
   #ifdef TRACE
   TraceString("AFTER vCheckPrm value = %d\n",testValue);
   #endif
   
   EXPECT_EQ(testValue,OSAL_C_U16_MEDIA_EJECTED);
}


/*********************************************************************/
            /* Test Case to check for prm initialisation  */
/*********************************************************************/	
TEST_F(PrmTableTest, prm_vInitfun)
{
   tU16 testValue = OSAL_C_U16_INCORRECT_MEDIA;
   
   #ifdef TRACE
   TraceString("BEFORE prm_vInit value = %d\n",testValue);
   #endif
   
   prm_vInit_wrapper();
   testValue = pOsalData->u16SdCardType;
   
   #ifdef TRACE
   TraceString("AFTER prm_vInit value = %d\n",testValue);
   #endif
   
   EXPECT_EQ(testValue,OSAL_C_U16_MEDIA_EJECTED);  
}

/*********************************************************************/
            /* Test Case to check for prmtasks start  */
/*********************************************************************/	
TEST_F(PrmTableTest, vStartPrmTasksfun)
{
   tS32 testValue = OSAL_C_U16_INCORRECT_MEDIA;
   
   #ifdef TRACE
   TraceString("BEFORE vStartPrmTasks value = %d\n",pOsalData->s32PrmMainPrcId);
   #endif
   
   vStartPrmTasks_wrapper();
   testValue = pOsalData->s32PrmMainPrcId;
  
   #ifdef TRACE
   TraceString("AFTER vStartPrmTasks value = %d\n",testValue);
   #endif
   
   EXPECT_NE(pOsalData->s32PrmMainPrcId,OSAL_ERROR);  
}

/*********************************************************************/
/*  Test Case to Scan exixting mount points    */
/*********************************************************************/
TEST_F(PrmTableTest, scan_existing_mountsSuccess)
{
   int ret = -1;
   ret = scan_existing_mounts_wrapper();
   EXPECT_EQ(EXIT_SUCCESS,ret);
}

TEST_F(PrmTableTest, scan_existingmountsFailure)
{
   int ret = -1;
   ret = scan_existing_mounts_wrapper();
   EXPECT_EQ(EXIT_SUCCESS,ret);
}

/*********************************************************************/
/*  Test Case to test if a function is prm function */
/*********************************************************************/
TEST_F(PrmTableTest, prm_bIsAPrmFunSucccess)
{
   tBool ret = FALSE;
   EXPECT_EQ(TRUE,(ret = prm_bIsAPrmFun_wrapper( OSAL_C_S32_IOCTRL_PRM_MAX_VALUE)));
   EXPECT_EQ(TRUE,(ret = prm_bIsAPrmFun_wrapper( OSAL_C_S32_IOCTRL_PRM_MIN_VALUE)));
}

TEST_F(PrmTableTest, prm_bIsAPrmFunFailure)
{		
	tBool ret = TRUE;
	EXPECT_EQ(FALSE,(ret = prm_bIsAPrmFun_wrapper( OSAL_C_S32_IOCTRL_PRM_MAX_VALUE+1 )));
	EXPECT_EQ(FALSE,(ret = prm_bIsAPrmFun_wrapper( OSAL_C_S32_IOCTRL_PRM_MIN_VALUE-1)));
}

/*********************************************************************/
            /* Test Case for PRM_ACTIVATE_SIGNAL  */
/*********************************************************************/	
TEST_F(PrmTableTest, PRM_ACTIVATE_SIGNAL)
{
   OSAL_tIODescriptor fd;
   tU32 s32Val = 0;
   	
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

TEST_F(PrmTableTest, prm_u32PrmPRM_ACTIVATE_SIGNAL)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_E_NOERROR, prm_u32Prm_wrapper(fd, OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

/*********************************************************************/
            /* Test Case for PRM for REL_EXCLUSIVE_ACCESS  */
/*********************************************************************/	
TEST_F(PrmTableTest, REL_EXCLUSIVE_ACCESS)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_ERROR, OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REL_EXCLUSIVE_ACCESS, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

TEST_F(PrmTableTest, prm_u32PrmREL_EXCLUSIVE_ACCESS)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, prm_u32Prm_wrapper(fd, OSAL_C_S32_IOCTRL_REL_EXCLUSIVE_ACCESS, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

/*********************************************************************/
        /* Test Case to test for Success of PRM open file */
/*********************************************************************/
TEST_F(PrmTableTest, EX_OPEN_FILE)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_ERROR, OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_EX_OPEN_FILE, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

TEST_F(PrmTableTest, prm_u32PrmEX_OPEN_FILE)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, prm_u32Prm_wrapper(fd, OSAL_C_S32_IOCTRL_EX_OPEN_FILE, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

/*********************************************************************/
            /* Test Case to get prm media info */
/*********************************************************************/	
TEST_F(PrmTableTest, PRM_GETMEDIAINFO)
{
    OSAL_tIODescriptor  fd;
	tU32 s32Val = 0;
	
    EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_GETMEDIAINFO, (uintptr_t)&s32Val));	
    EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

TEST_F(PrmTableTest, prm_u32PrmfuncPRM_GETMEDIAINFO)
{
	OSAL_tIODescriptor	fd;
	tU32 s32Val = 0;
		
	EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));  
	EXPECT_EQ(OSAL_E_INVALIDVALUE,prm_u32Prm_wrapper(fd, OSAL_C_S32_IOCTRL_PRM_GETMEDIAINFO, (uintptr_t)&s32Val));	  
	EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

/*********************************************************************/
        /* Test Case to test for prm trace function  */
/*********************************************************************/	
TEST_F(PrmTableTest, vPrmTtfisTracefunc)
{
   char buf[200];
   strncpy(buf,"UNIT TESTING",sizeof("UNIT TESTING"));
   vPrmTtfisTrace( (tU8)TR_LEVEL_COMPONENT, "%s", buf );
}

/*********************************************************************/
    /* Test Case to test for Success of deletion of error memory */
/*********************************************************************/
TEST_F(PrmTableTest, PRM_TRIGGER_DEL_ERRMEM)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_TRIGGER_DEL_ERRMEM, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}

TEST_F(PrmTableTest, prm_u32PrmPRM_TRIGGER_DEL_ERRMEM)
{
   OSAL_tIODescriptor	fd;
   tU32 s32Val = 0;
		
   EXPECT_NE(OSAL_ERROR,(fd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READWRITE )));
   EXPECT_EQ(OSAL_E_NOERROR, prm_u32Prm_wrapper(fd, OSAL_C_S32_IOCTRL_PRM_TRIGGER_DEL_ERRMEM, (uintptr_t)&s32Val));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(fd));
}
