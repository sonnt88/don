
/******************************************************************************
 * FILE         : FS_TestFuncs.cpp
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the file system test cases for the CDROM,
 *                FFS1, FFS2, FFS3, USBH, RamDisk, SDCard, USBDNL
 *
 * AUTHOR(s)    :  Sriranjan U (RBEI/ECM1) and Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                                          | Author & comments
 * --.--.--       | Initial revision                         | ------------
 * 31.03.15       | Ported from OEDT                         |  SWM2KOR
 *
 * -----------------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "osal_core_unit_test_mock.h"
#include "FS_TestFuncs.h"
#include "FFS3_CommonFS_TestFuncs.h"


using namespace std;


/*****************************************************************************
* FUNCTION     :  u32FSOpenClosedevice()
* PARAMETER    :  device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_001
* DESCRIPTION  :  Opens and closes all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSOpenClosedevice(const char * dev_name)
{
   OSAL_tIODescriptor hDevice = 0;
   
   hDevice = OSAL_IOOpen((tCString)dev_name,OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR,hDevice);
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDevice));
   
   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSOpendevInvalParm()
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_002
* DESCRIPTION  :  Try to Open all filesystem devices with invalid parameters
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSOpendevInvalParm(const char* dev_name)
{
   OSAL_tIODescriptor hDevice = 0;
   
   /* Try to open the device with an invalid device name*/
   hDevice = OSAL_IOOpen((tCString)OEDT_C_STRING_DEVICE_INVAL,OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR,hDevice);
    /* If successfully opened, indicate an error and close the device */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hDevice));
   /* Try to open the device with an invalid access mode */
   hDevice = OSAL_IOOpen((tCString)dev_name ,(OSAL_tenAccess)OEDT_INVALID_ACCESS_MODE);
   /* If successfully opened, indicate an error and close the device */
   EXPECT_EQ(OSAL_ERROR,hDevice);
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hDevice));

   return 0;
}
/*****************************************************************************
* FUNCTION     :  u32FSReOpendev()
* PARAMETER    :  none
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_003
* DESCRIPTION  :  Try to Re-Open all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSReOpendev(const char* dev_name)
{
   OSAL_tIODescriptor hDevice1 = 0;
   OSAL_tIODescriptor hDevice2 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   hDevice1 = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(OSAL_ERROR,hDevice1);
   /* Re-open the device, should be successfull */
   hDevice2 = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(OSAL_ERROR,hDevice2);
  /* If open for the second time is successfull, close the second
      device handle */
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDevice2));
   /* Now close the first device handle */
   EXPECT_NE(OSAL_ERROR ,OSAL_s32IOClose(hDevice1));

   return 0;
}
/*****************************************************************************
* FUNCTION     :  u32FSClosedevAlreadyClosed()
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_005
* DESCRIPTION  :  Try to Re-Open all filesystem devices
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32  u32FSClosedevAlreadyClosed(const char* dev_name)
{
   OSAL_tIODescriptor hDevice = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   hDevice = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(OSAL_ERROR,hDevice);
   /* Close the device with the device handle obtained in open */
   EXPECT_NE(OSAL_ERROR ,OSAL_s32IOClose(hDevice));
       /* Re Closing , Try to close with the same handle.
         Must not be possible */
   EXPECT_EQ(OSAL_ERROR ,OSAL_s32IOClose(hDevice));     

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSOpenClosedir()
* PARAMETER    :  Root directory name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_006
* DESCRIPTION  :  Try to Open and closes a directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32  u32FSOpenClosedir(const char* dev_name)
{
   OSAL_tIODescriptor hDir = 0;

   hDir = OSAL_IOOpen((tCString)dev_name,(OSAL_tenAccess)OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR,hDir);
   /* Close the directory */
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDir));

   return 0;
}
/*****************************************************************************
* FUNCTION     :  u32FSOpendirInvalid()
* PARAMETER    :  Root directory name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_007
* DESCRIPTION  :  Try to Open invalid directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSOpendirInvalid(const tPS8* dev_name)
{
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;

   /*Try to open a invalid directory  */
   hDir1 = OSAL_IOOpen ((tCString)dev_name[0],enAccess);
   EXPECT_EQ(OSAL_ERROR,hDir1);
  /* If open is successful, indicate an error and close the directory */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hDir1));

   return 0;
}


/*****************************************************************************
* FUNCTION     :  u32FSCreateDelDir()
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_008
* DESCRIPTION  :  Try create and delete directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
*                      Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32  u32FSCreateDelDir(const char* dev_name)
{
   OSAL_tIODescriptor hDir2 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_trIOCtrlDirent rDirent = {""};

   /* Open directory in readwrite mode */
   hDir2 = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(OSAL_ERROR,hDir2);
   /* Copy sub-directory name to structure variable */
  (tVoid)OSAL_szStringCopy((tString)(rDirent.s8Name),
                           (tCString)(SUBDIR_NAME));
  /* Create sub-directory */
   EXPECT_NE(OSAL_s32IOControl(hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
                               (intptr_t)&rDirent),OSAL_ERROR);
               /* Remove sub-directory */
   EXPECT_NE(OSAL_s32IOControl(hDir2,OSAL_C_S32_IOCTRL_FIORMDIR,
                                  (intptr_t)&rDirent),OSAL_ERROR);
              /* Close directory */
   EXPECT_NE(OSAL_s32IOClose(hDir2),OSAL_ERROR);

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSCreateDelSubDir()
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_009
* DESCRIPTION  :  Try create and delete sub directory in each root direcroty like /nor0/MYNEW/MYNEW1
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                 Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSCreateDelSubDir(const char* dev_name)
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tChar szSubdirName [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW";
   tChar szSubdirName1 [OEDT_MAX_TOTAL_DIR_PATH_LEN] = "MYNEW1";
   OSAL_trIOCtrlDirent rDirent = {""};
   tChar temp_ch[OEDT_MAX_TOTAL_DIR_PATH_LEN] = {""};

   hDir = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,szSubdirName);
   EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
                               (intptr_t)&rDirent),OSAL_ERROR);
   (tVoid)OSAL_szStringCopy((tString)(temp_ch),(tCString)(dev_name));
   (tVoid)OSAL_szStringConcat(temp_ch,"/MYNEW");
   hDir1 = OSAL_IOOpen((tCString)temp_ch,enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   memset(rDirent.s8Name,'\0',sizeof(rDirent.s8Name));
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,szSubdirName1);
   EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                (intptr_t)&rDirent),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
                                  (intptr_t)&rDirent),OSAL_ERROR);      
   EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);
   memset(rDirent.s8Name,'\0',sizeof(rDirent.s8Name));
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,szSubdirName);
   EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
                               (intptr_t)&rDirent),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hDir),OSAL_ERROR);

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSRmNonExstngDir()
* PARAMETER    :  Device name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_011
* DESCRIPTION  :  Try to remove non exsisting directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                 Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSRmNonExstngDir(const char* dev_name)
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   hDir = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,(tCString)SUBDIR_NAME_INVAL);
   EXPECT_EQ(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
                               (intptr_t)&rDirent),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hDir),OSAL_ERROR);

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSRmDirUsingIOCTRL()
* PARAMETER    :  Devicename and Filename name
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_012
* DESCRIPTION  :  Delete directory with which is not a directory (with files)
*                 using IOCTRL
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSRmDirUsingIOCTRL(const tPS8* dev_name)
{
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   OSAL_tIODescriptor hFile = 0, hDir1 = 0;
   OSAL_trIOCtrlDirent rDirent = {""};

   hDir1 = OSAL_IOOpen ((tCString)dev_name[0],enAccess);
   EXPECT_NE(hDir1,OSAL_ERROR);
   hFile = OSAL_IOCreate ((tCString)dev_name[1],enAccess);
   EXPECT_NE(hFile,OSAL_ERROR);
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,(tCString)dev_name[1]);
   EXPECT_EQ(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
                                  (intptr_t)&rDirent),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hFile),OSAL_ERROR);         
   EXPECT_NE(OSAL_s32IORemove((tCString)dev_name[1]),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);

   return 0;     
}
/*****************************************************************************
* FUNCTION     :  u32FSGetDirInfo()
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_013
* DESCRIPTION  :  Get directory information
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSGetDirInfo(const char* dev_name, tBool bCreateRemove)
{
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_tenAccess enAccess;
   OSAL_trIOCtrlDirent rDirent1 = {""}, rDirent2 = {""};
   OSAL_trIOCtrlDir rDirCtrl;
   
   if (bCreateRemove)
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }
   (tVoid)OSAL_szStringCopy(rDirent1.s8Name,(tCString)SUBDIR_NAME);
   hDir1 = OSAL_IOOpen((tCString)dev_name,enAccess);
   EXPECT_NE(hDir1,OSAL_ERROR);
   if (bCreateRemove)
   {
      EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
                               (intptr_t)&rDirent1),OSAL_ERROR);
   }

   /* Get DIR info for root DIR so that it displays the newly created Sub-dir */
   (tVoid)OSAL_szStringCopy(rDirent2.s8Name,(tCString)dev_name);
   rDirCtrl.fd = hDir1;
   rDirCtrl.s32Cookie = 0;
   rDirCtrl.dirent = rDirent2;
   EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIOREADDIR,
                                  (intptr_t)&rDirCtrl),OSAL_ERROR);
   if (bCreateRemove)
   {
      EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
                                        (intptr_t)&rDirent1),OSAL_ERROR);
   }
   EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSOpenDirDiffModes()
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_014
* DESCRIPTION  :  Try to open directory in different modes
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*              :  Update the testcase,anc3kor,14-04-2009
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSOpenDirDiffModes(const tPS8* dev_name, tBool bCreateRemove)
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_tIODescriptor hDir1 = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess;

   if(bCreateRemove)
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }
   hDir = OSAL_IOOpen((tCString)dev_name[0],enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   if(bCreateRemove)
   {
      (tVoid)OSAL_szStringCopy(rDirent.s8Name,(tCString)SUBDIR_NAME);
      EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  (intptr_t)&rDirent),OSAL_ERROR); 
   }
   /*In Read only mode*/
   enAccess = OSAL_EN_READONLY;
   hDir1 = OSAL_IOOpen ((tCString)dev_name[1],enAccess);
   EXPECT_NE(hDir1,OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);
   if(bCreateRemove)
   {
      /*In Read write mode*/
      enAccess = OSAL_EN_READWRITE;
      hDir1 = OSAL_IOOpen ((tCString)dev_name[1],enAccess);
      EXPECT_NE(hDir1,OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);
      /*In Write only mode*/
      enAccess = OSAL_EN_WRITEONLY;
      hDir1 = OSAL_IOOpen ((tCString)dev_name[1],enAccess);
      EXPECT_NE(hDir1,OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
                                    (intptr_t)&rDirent),OSAL_ERROR);
   }
   EXPECT_NE(OSAL_s32IOClose(hDir),OSAL_ERROR);

   return 0;
}


/*****************************************************************************
* FUNCTION     :  u32FSReOpenDir()
* PARAMETER    :  dev_name: device name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_015
* DESCRIPTION  :  Try to reopen directory
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 10, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSReOpenDir(const tPS8* dev_name, tBool bCreateRemove)
{
   OSAL_trIOCtrlDir* hDir1 = OSAL_NULL, *hDir2 = OSAL_NULL;
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess ;

   if(bCreateRemove)
   {
      enAccess = OSAL_EN_READWRITE;
   }
   else
   {
      enAccess = OSAL_EN_READONLY;
   }
   hDir = OSAL_IOOpen((tCString)dev_name[0],enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   if(bCreateRemove)
   {
      (tVoid)OSAL_szStringCopy(rDirent.s8Name,(tCString)SUBDIR_NAME);
      /* Create Dir */
      EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                  (intptr_t)&rDirent),OSAL_ERROR);
   }
   hDir1 = OSALUTIL_prOpenDir((tCString)dev_name[1]);
   EXPECT_TRUE(hDir1 != OSAL_NULL);   
  /* Open the same directory again */
   hDir2 = OSALUTIL_prOpenDir((tCString)dev_name[1]);
   EXPECT_TRUE(hDir2 != OSAL_NULL);   
   /* After opening twice, close the first handle */
   EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CloseDir(hDir2));
    /* Close the first handle */
   EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CloseDir(hDir1));        
   if(bCreateRemove)
   {
      EXPECT_NE(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
                                     (intptr_t)&rDirent),OSAL_ERROR);
   }
   EXPECT_NE(OSAL_s32IOClose(hDir),OSAL_ERROR);

   return 0;
}



/*****************************************************************************
* FUNCTION     :  u32FSCreateDirMultiTimes()
* PARAMETER    :  dev_name: device name, and filename
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_017
* DESCRIPTION  :  Try to Create directory with similar name
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Nov 28, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSCreateDirMultiTimes(const tPS8* dev_name,tBool bCreateRemove)
{
   OSAL_tIODescriptor hDir1 = 0,hDir2 = 0,hDir3 = 0;
   OSAL_trIOCtrlDirent rDirent1 = {""},rDirent2 = {""},rDirent3 = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   hDir1 = OSAL_IOOpen ((tCString)dev_name[0],enAccess);
   EXPECT_NE(hDir1,OSAL_ERROR);
   if (bCreateRemove)
   {
      (tVoid)OSAL_szStringCopy(rDirent1.s8Name,(tCString)SUBDIR_NAME);
      EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                    (intptr_t)&rDirent1),OSAL_ERROR);
      (tVoid)OSAL_szStringCopy(rDirent2.s8Name,(tCString)SUBDIR_NAME2);
      hDir2 = OSAL_IOOpen((tCString)dev_name[1],enAccess);
      EXPECT_NE(hDir2,OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                       (intptr_t)&rDirent2),OSAL_ERROR);      
      (tVoid)OSAL_szStringCopy(rDirent3.s8Name,(tCString)SUBDIR_NAME3);
      hDir3 = OSAL_IOOpen((tCString)dev_name[2],enAccess);
      EXPECT_NE(hDir3,OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir3,OSAL_C_S32_IOCTRL_FIOMKDIR,
                                       (intptr_t)&rDirent3),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir3,OSAL_C_S32_IOCTRL_FIORMDIR,
                                        (uintptr_t)&rDirent3),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOClose(hDir3),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir2,OSAL_C_S32_IOCTRL_FIORMDIR,
                                        (uintptr_t)&rDirent2),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOClose(hDir2),OSAL_ERROR);
      EXPECT_NE(OSAL_s32IOControl(hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,
                                  (uintptr_t)&rDirent1),OSAL_ERROR);
   }
   EXPECT_NE(OSAL_s32IOClose(hDir1),OSAL_ERROR);

   return 0;
}

/*****************************************************************************
* FUNCTION     :  u32FSCreateRemDirInvalPath()
* PARAMETER    :  dev_name: device name and file name
*                 bCreateRemove: TRUE ->create remove active
*                             FALSE->create remove not active
* RETURNVALUE  :  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE    :  TU_OEDT_FS_018
* DESCRIPTION  :  Create/ remove directory with Invalid path
* HISTORY      :  Created by Sriranjan (RBEI/ECM1) on Dec 01, 2008
*                      Updated by Sriranjan (RBEI/ECF1) on Sep 11, 2009
*                 Ported from OEDT to osal unit test on Mar 31,2015
******************************************************************************/
tU32 u32FSCreateRemDirInvalPath(const tPS8* dev_name)
{
   OSAL_tIODescriptor hDir = 0;
   OSAL_trIOCtrlDirent rDirent = {""};
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   hDir = OSAL_IOOpen((tCString)dev_name[0],enAccess);
   EXPECT_NE(hDir,OSAL_ERROR);
   (tVoid)OSAL_szStringCopy(rDirent.s8Name,(tCString)dev_name[1]);
   /*  Error is expected iocontrol should fail for invalid path*/
   EXPECT_EQ(OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,
                                 (uintptr_t)&rDirent),OSAL_ERROR);
   EXPECT_NE(OSAL_s32IOClose(hDir),OSAL_ERROR);

   return 0;
}


/*****************************************************************************
* FUNCTION:        u32FileSystemCreateRmNonEmptyDir( )
******************************************************************************/
tU32 u32FSCreateRmNonEmptyDir(void)
{
    OSAL_tIODescriptor hDir1 = 0,hDir2 = 0;
    OSAL_trIOCtrlDirent rDirent1,rDirent2;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    EXPECT_NE(OSAL_ERROR,(hDir1 = OSAL_IOOpen(OEDTTEST_C_STRING_DEVICE_FFS3, enAccess)));
    (void)OSAL_szStringCopy( (tString)rDirent1.s8Name, (tCString)SUBDIR_NAME );      
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent1));
    EXPECT_NE(OSAL_ERROR,(hDir2 = OSAL_IOOpen(SUBDIR_PATH_FFS3, enAccess)));
    (tVoid)OSAL_szStringCopy ( rDirent2.s8Name, SUBDIR_NAME2 );    
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl( hDir2,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent2));
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl( hDir2,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent2));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hDir2 ));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hDir1 ));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemCopyDir( )
*******************************************************************************/
tU32 u32FSCopyDir(void)
{
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tIODescriptor hFile1 = 0;
    OSAL_tIODescriptor hFile2 = 0;
    OSAL_trIOCtrlDir* pDir;
    OSAL_trIOCtrlDirent* pEntry;
    tS32 hDir1 = 0;
    tS32 hDir2 = 0;
    tChar* arrFileName_tmp[2]={OSAL_NULL};
    tChar arrFileName[2][256];


    EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OEDTTEST_C_STRING_DEVICE_FFS3, enAccess )));
    /*Create the source directory  in readwrite mode */
    EXPECT_NE(OSAL_ERROR,(hDir1 = OSALUTIL_s32CreateDir (hDevice,"/"DIR1)));
    /*Create the destination directory  in readwrite mode */
    EXPECT_NE(OSAL_ERROR,(hDir2 = OSALUTIL_s32CreateDir (hDevice,"/"DIR))); 
    /*Create the first file in source directory */
    EXPECT_NE(OSAL_ERROR,(hFile1 = OSAL_IOCreate(FILE13_FFS3, enAccess)));
    /*Create second file in source directory*/
    EXPECT_NE(OSAL_ERROR,(hFile2 = OSAL_IOCreate (FILE14_FFS3 ,enAccess)));
    (tVoid)OSAL_szStringCopy ( arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3 );
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR1); /* Source     Directory */
    (tVoid)OSAL_szStringCopy ( arrFileName[1], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[1],"/"DIR ); /* Source     Directory */
    arrFileName_tmp[0]= arrFileName[0];
    arrFileName_tmp[1]= arrFileName[1];
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOCOPYDIR,(intptr_t)arrFileName_tmp ));

    EXPECT_NE((OSAL_trIOCtrlDir*)NULL,(pDir = OSALUTIL_prOpenDir(arrFileName_tmp[1])));
    EXPECT_NE((OSAL_trIOCtrlDirent*)NULL,(pEntry = OSALUTIL_prReadDir(pDir)));
    while (pEntry != NULL)
    {
       pEntry = OSALUTIL_prReadDir(pDir);                                  
    }
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CloseDir (pDir));
    /*Close the first file in source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile1));
    /*Close the second file in source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile2));
                                    
    /*Remove the first file in the source directory*/
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(FILE13_FFS3));
    /*Remove the second file in the source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(FILE14_FFS3));                   
    /* Remove the first copied file in the destination directory*/
    (tVoid)OSAL_szStringCopy (arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File13.txt" );
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(arrFileName[0]));
    /*Remove the second copied file in the destination  directory*/
    (tVoid)OSAL_szStringCopy (arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File14.txt");
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(arrFileName[0]));
    /*Remove the source directory */
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32RemoveDir(hDevice,DIR1));
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32RemoveDir(hDevice,DIR));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDevice));

    return 0;
}


/*****************************************************************************
* FUNCTION:        u32FileSystemMultiCreateDir( )
******************************************************************************/
tU32 u32FSMultiCreateDir(void)
{
    OSAL_tIODescriptor hDir = 0;
    OSAL_trIOCtrlDirent rDirent;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
 
    EXPECT_NE(OSAL_ERROR,(hDir = OSAL_IOOpen (OEDTTEST_C_STRING_DEVICE_FFS3, enAccess))); /* open the root dir */
    (tVoid)OSAL_szStringCopy ( rDirent.s8Name, SUBDIR_NAME3 );      
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent));
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hDir ));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemCreateSubDir( )
******************************************************************************/
tU32 u32FSCreateSubDir(tVoid)
{
    OSAL_tIODescriptor hDir = 0;
    OSAL_tIODescriptor hDir1 = 0;
    OSAL_trIOCtrlDirent rDirent;

    EXPECT_NE(OSAL_ERROR,(hDir = OSAL_IOOpen (OEDTTEST_C_STRING_DEVICE_FFS3,OSAL_EN_READWRITE)));
    (tVoid)OSAL_szStringCopy ( rDirent.s8Name,SUBDIR_NAME );
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl ( hDir,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent));
    EXPECT_NE(OSAL_ERROR,(hDir1 = OSAL_IOOpen ( SUBDIR_PATH_FFS3, OSAL_EN_READONLY )));
    (tVoid)OSAL_szStringCopy(rDirent.s8Name, SUBDIR_NAME2 );
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl ( hDir1,OSAL_C_S32_IOCTRL_FIOMKDIR,(intptr_t)&rDirent));
    (tVoid)OSAL_szStringCopy(rDirent.s8Name, SUBDIR_NAME);
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDir1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDir,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDir));
    return 0;
}


/*****************************************************************************
* FUNCTION:        u32FileSystemDelInvDir ( )
******************************************************************************/
tU32  u32FSDelInvDir(void)
{
    OSAL_tIODescriptor hDir = 0;
    OSAL_trIOCtrlDirent rDirent;

    EXPECT_NE(OSAL_ERROR,(hDir = OSAL_IOOpen (OEDTTEST_C_STRING_DEVICE_FFS3,OSAL_EN_READWRITE)));
    (tVoid)OSAL_szStringCopy ( rDirent.s8Name,OEDT_C_STRING_FFS2_DIR_INV_NAME);      
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl( hDir,OSAL_C_S32_IOCTRL_FIORMDIR,(intptr_t)&rDirent));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemCopyDirRec( )
******************************************************************************/
tU32  u32FSCopyDirRec(void)
{
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tIODescriptor hFile1 = 0;
    OSAL_tIODescriptor hFile2 = 0;
    OSAL_trIOCtrlDir* pDir;
    OSAL_trIOCtrlDirent* pEntry;
    tS32 hDir1 = 0;
    tS32 hDir2 = 0;
    tChar* arrFileName_tmp[2]={OSAL_NULL};
    tChar arrFileName[2][256];
 
    EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen( OEDTTEST_C_STRING_DEVICE_FFS3, enAccess )));
    /*Create the source directory  in readwrite mode */
    EXPECT_NE(OSAL_ERROR,(hDir1 = OSALUTIL_s32CreateDir (hDevice,"/"DIR1)));
    /*Create the destination directory    in readwrite mode */
    EXPECT_NE(OSAL_ERROR,(hDir2 =OSALUTIL_s32CreateDir (hDevice,"/"DIR))); 
    /*Create the first file in source directory */
    EXPECT_NE(OSAL_ERROR,(hFile1 = OSAL_IOCreate(FILE13_FFS3, enAccess)));
    /*Create second file in source directory*/
    EXPECT_NE(OSAL_ERROR,(hFile2 = OSAL_IOCreate (FILE14_FFS3,enAccess)));
    (tVoid)OSAL_szStringCopy ( arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR1 ); /* Source        Directory */
    (tVoid)OSAL_szStringCopy ( arrFileName[1], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[1],"/"DIR ); /* Source      Directory */
    arrFileName_tmp[0]= arrFileName[0];
    arrFileName_tmp[1]= arrFileName[1];
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOCOPYDIR,(intptr_t)arrFileName_tmp ));
    EXPECT_NE((OSAL_trIOCtrlDir*)OSAL_NULL,(pDir = OSALUTIL_prOpenDir (arrFileName_tmp[1])));
    EXPECT_NE((OSAL_trIOCtrlDirent*)OSAL_NULL,(pEntry = OSALUTIL_prReadDir(pDir)));
    while (pEntry != NULL)
    {
       pEntry = OSALUTIL_prReadDir(pDir);                                  
    }
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CloseDir (pDir));
    /*Close the first file in source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile1));
    /*Close the second file in source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile2));
    /*Remove the first file in the source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(FILE13_FFS3));
    /*Remove the second file in the source directory*/ 
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(FILE14_FFS3));                    
    /* Remove the first copied file in the destination directory*/
    (tVoid)OSAL_szStringCopy (arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File13.txt" );
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(arrFileName[0]));
    /*Remove the second copied file in the destination  directory*/
    (tVoid)OSAL_szStringCopy (arrFileName[0], OEDTTEST_C_STRING_DEVICE_FFS3);
    (tVoid)OSAL_szStringConcat(arrFileName[0],"/"DIR"/File14.txt");
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(arrFileName[0]));
    /*Remove the source directory */
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32RemoveDir(hDevice,DIR1));
    /*Remove the destination directory*/
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32RemoveDir(hDevice,DIR));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDevice));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FSRemoveDir( )
******************************************************************************/
tU32 u32FSRemoveDir(void)
{
    OSAL_tIODescriptor hDevice = 0;
    OSAL_tIODescriptor hFile1 = 0;
    OSAL_tIODescriptor hFile2 = 0;
    tChar temp_ch[200];
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    /*Open the device in read write mode */
    EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OEDTTEST_C_STRING_DEVICE_FFS3, enAccess)));
    /*Create the directory in the device */
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CreateDir(hDevice, DIR ));
    /*Create some files in read write mode */
    EXPECT_NE(OSAL_ERROR,(hFile1 = OSAL_IOCreate(FILE11_FFS3 , enAccess)));
    EXPECT_NE(OSAL_ERROR,OSALUTIL_s32CreateDir(hDevice, DIR"/"DIR_RECR1 ));
	/*Create another file */
    EXPECT_NE(OSAL_ERROR,(hFile2 = OSAL_IOCreate (FILE12_RECR2_FFS3 ,enAccess)));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile2));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE,(intptr_t) DIR ));
    /*Remove the first file in the dir*/
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IORemove(FILE11_FFS3));
    /*Remove the  second file in the dir*/
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IORemove(FILE12_RECR2_FFS3));					
    /*Check for directory removal*/
    (tVoid)OSAL_szStringCopy ( temp_ch,OEDTTEST_C_STRING_DEVICE_FFS3 );
    (tVoid)OSAL_szStringConcat(temp_ch, "/"DIR );
    EXPECT_EQ(OSAL_ERROR,OSALUTIL_s32RemoveDir(hDevice, temp_ch));
    /*Close the device */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hDevice));
    return 0;
}

/*****************************************************************************
* FUNCTION:		u32FSFileCreateDel( )
******************************************************************************/
tU32 u32FSFileCreateDel(void)
{
	OSAL_trIOCtrlDir* hDir = NULL;
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    EXPECT_NE((OSAL_trIOCtrlDir*)OSAL_NULL,(hDir = OSALUTIL_prOpenDir(OEDTTEST_C_STRING_DEVICE_FFS3)));
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate(OEDTTEST_C_STRING_DEVICE_FFS3"/Test,txt", enAccess)));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose( hFile ));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(OEDTTEST_C_STRING_DEVICE_FFS3"/Test,txt"));
	return 0;
}


/*****************************************************************************
* FUNCTION:		u32FSFileOpenClose( )
******************************************************************************/
tU32 u32FSFileOpenClose(void)
{
	OSAL_tIODescriptor hFile = 0;
	OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate (OSAL_TEXT_FILE_FIRST_FFS3, enAccess)));
	EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OSAL_TEXT_FILE_FIRST_FFS3, enAccess )));
	EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
	EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove (OSAL_TEXT_FILE_FIRST_FFS3));
	return 0;
}	


/*****************************************************************************
* FUNCTION:		u32FSFileOpenInvalPath( )
******************************************************************************/
tU32 u32FSFileOpenInvalPath(void)
{
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READONLY;
    EXPECT_EQ(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_C_STRING_FILE_INVPATH_FFS3,enAccess )));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FSFileOpenInvalParam( )
******************************************************************************/
tU32 u32FSFileOpenInvalParam(void)
{
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_INVALID_ACCESS_MODE;

    EXPECT_EQ(OSAL_ERROR,(hFile = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST_FFS3, enAccess)));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemFileReOpen( )
******************************************************************************/
tU32  u32FSFileReOpen(void)
{
    OSAL_tIODescriptor hFile1 = 0,hFile2 = 0, hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
 
        /* File create*/
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate(OSAL_TEXT_FILE_FIRST_FFS3, enAccess)));
    EXPECT_NE(OSAL_ERROR,(hFile1 = OSAL_IOOpen( OSAL_TEXT_FILE_FIRST_FFS3, enAccess)));
    /* Re Opening The File, should be possible */
    EXPECT_NE(OSAL_ERROR,(hFile2 = OSAL_IOOpen(OSAL_TEXT_FILE_FIRST_FFS3, enAccess)));
    /* close the second handle of the file */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose( hFile2));
    /* Close the first handle of the file */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile1));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(OSAL_TEXT_FILE_FIRST_FFS3));
    return 0;
}


/*****************************************************************************
* FUNCTION:          u32FSFileCreateDouble( )
******************************************************************************/
tU32 u32FSFileCreateDouble(void)
{     
     OSAL_tIODescriptor hFile1 = 0;
     OSAL_tIODescriptor hFile2 = 0;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

     EXPECT_NE(OSAL_ERROR,(hFile1 = OSAL_IOCreate (OEDT_FFS3_COMNFILE, enAccess)));
     EXPECT_NE(OSAL_ERROR,(hFile2 = OSAL_IOCreate (OEDT_FFS3_COMNFILE, enAccess)));
    // EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());

     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile1));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile2));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(OEDT_FFS3_COMNFILE));
     return 0;
}


/*****************************************************************************
* FUNCTION:        u32FSFileRead( )
******************************************************************************/
tU32 u32FSFileRead(void)
{    
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tS8 *ps8ReadBuffer = NULL;     
    tCS8 DataToWrite[15 + 1] = "Writing data";
    tU32 u32BytesToRead = 10;
    tS32 s32BytesRead = 0;    
    tU32 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;    

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate (OEDT_FFS3_COMNFILE, enAccess)));
    EXPECT_NE(OSAL_ERROR,(s32BytesWritten = OSAL_s32IOWrite(hFile, DataToWrite,u8BytesToWrite)));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_COMNFILE,enAccess )));
    EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate( u32BytesToRead + 1)));
    EXPECT_NE(OSAL_ERROR,(s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,u32BytesToRead )));
    EXPECT_EQ(s32BytesRead,(tS32)u32BytesToRead);
    OSAL_vMemoryFree( ps8ReadBuffer );
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(OEDT_FFS3_COMNFILE));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemFileWrite( )
******************************************************************************/
tU32 u32FSFileWrite(void)
{    
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
   tCS8 DataToWrite[15 + 1] = "Writing data";
   tU32 u8BytesToWrite = 15; /* Number of bytes to write */
   tS32 s32BytesWritten = 0;    
 
   EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate (OEDT_FFS3_WRITEFILE, enAccess)));
   EXPECT_NE(OSAL_ERROR,(s32BytesWritten = OSAL_s32IOWrite (hFile, DataToWrite,u8BytesToWrite)));
   EXPECT_EQ(s32BytesWritten,(tS32)u8BytesToWrite);
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
   EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(OEDT_FFS3_WRITEFILE));
   return 0;    
}    


/*****************************************************************************
* FUNCTION:        u32FSCreateCommonFile()
******************************************************************************/
void vFSCreateCommonFile(tCString Name)
{    
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;     
    tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Writing some test data to the data file";
    tU8 u8BytesToWrite = 40;
    tS32 s32BytesWritten = 0;
  
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate (Name, enAccess)));
    /* Write data to file */
    EXPECT_NE(OSAL_ERROR,(s32BytesWritten = OSAL_s32IOWrite (hFile, DataToWrite,(tU32) u8BytesToWrite)));
    /* Check status of write */
    EXPECT_EQ(s32BytesWritten,u8BytesToWrite );
    /* Close the common file */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
}
/*****************************************************************************
* FUNCTION:        u32FSRemoveCommonFile()
******************************************************************************/
void vFSRemoveCommonFile(tCString Name)
{
  EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove(Name));
}

/*****************************************************************************
* FUNCTION:        u32FSGetPosFrmBOF( )
******************************************************************************/
tU32 u32FSGetPosFrmBOF(void)
{
    OSAL_tIODescriptor hFile = 0;
    tS32 s32PosToSet = 10; 
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
     intptr_t s32FilePos = 0;
 
    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    /*Open the commmon file*/
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, enAccess )));
    /*Seek the file to desired position*/
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOSEEK, s32PosToSet));
    /*Get position of file pointer from beginning of common file*/
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOWHERE,(intptr_t)&s32FilePos ));
    /*Close the common file*/    
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile ));
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FSGetPosFrmEOF( )
******************************************************************************/
tU32 u32FSGetPosFrmEOF(void)
{
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
     intptr_t s32FilePos = 0;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    /*Open the common file*/
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE,enAccess )));
    /*Call the control function*/
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&s32FilePos ));
    /*Close the common file*/
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile ));
    /*Remove the common file*/
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
   
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FSFileReadNegOffsetFrmBOF( )
******************************************************************************/
tU32   u32FSFileReadNegOffsetFrmBOF(void)
{
    OSAL_tIODescriptor hFile=0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    tS32 ReadRet =0;
     intptr_t s32PosToSet = 0;
    tS8 *ps8ReadBuffer = OSAL_NULL;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, enAccess )));
    s32PosToSet = OEDT_CDROM_NEGATIVE_POS_TO_SET;
    /* Seek to the byte position specified */
     EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));

    /*Dynamically Allocate Memory for the read buffer */
    EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer = (tS8 *)OSAL_pvMemoryAllocate (OEDT_CDROM_BYTES_TO_READ)));
    EXPECT_NE(OSAL_ERROR,ReadRet = OSAL_s32IORead(hFile,ps8ReadBuffer,OEDT_CDROM_BYTES_TO_READ));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    OSAL_vMemoryFree(ps8ReadBuffer);
    /*Remove the common file*/ 

    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
   
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FSFileReadOffsetBeyondEOF( )
******************************************************************************/
tU32 u32FSFileReadOffsetBeyondEOF(void)
{
    OSAL_tIODescriptor hFile = 0;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
     intptr_t s32PosToSet = 0; 
    tS32 s32FileOff_frm_end = OEDT_CDROM_OFFSET_BEYOND_END;
    tS8 *ps8ReadBuffer = OSAL_NULL;
    tS32 s32BytesRead = 0;
     intptr_t s32FileSz = 0;
    tU32 u32BytesToRead = 10;
 
    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, enAccess )));
     EXPECT_NE(OSAL_ERROR,(s32PosToSet = OSALUTIL_s32FGetSize(hFile)));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, enAccess )));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&s32FileSz));
    /* Set position at EOF */
    EXPECT_EQ(s32PosToSet,s32FileSz);
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
 
    EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate(u32BytesToRead +1)));
    EXPECT_NE(OSAL_ERROR,(s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,u32BytesToRead)));
    EXPECT_EQ(s32BytesRead, 0 );
    
    /* This will point to the position from where read has to begin, i.e. at the specified offset from the end */
	s32PosToSet += s32FileOff_frm_end;
     EXPECT_NE(OSAL_ERROR,(OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet)));
    
    OSAL_vMemoryFree( ps8ReadBuffer );
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
 
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
   
    return 0;
}

/*****************************************************************************
* FUNCTION:          u32FSFileReadOffsetFrmBOF( )
******************************************************************************/
tU32 u32FSFileReadOffsetFrmBOF(void)
{     
     OSAL_tIODescriptor hFile = 0;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
     tS8 *ps8ReadBuffer = NULL;      
     tU32 u32BytesToRead = 10; 
     tS32 s32BytesRead = 0;
     tS32 s32PosToSet = 0;

     vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess )));
     /* Set position at an Offset from BOF */
     s32PosToSet = 5;
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
     EXPECT_NE((tS8 *)OSAL_NULL, (ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1)));
     EXPECT_NE(OSAL_ERROR,s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,u32BytesToRead));
     EXPECT_EQ(s32BytesRead,(tS32)u32BytesToRead);
     OSAL_vMemoryFree( ps8ReadBuffer );
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));

     vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
     return 0;
}

/*****************************************************************************
* FUNCTION:          u32FSFileReadOffsetFrmEOF( )
******************************************************************************/
tU32 u32FSFileReadOffsetFrmEOF(void)
{     
     OSAL_tIODescriptor hFile = 0;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;          
     tS8 *ps8ReadBuffer = NULL;      
     tU32 u32BytesToRead = 10;
     intptr_t s32FilePos = 0;
     tS32 s32BytesRead = 0;
     intptr_t s32PosToSet = 0;
     tS32 s32offFrmEOF = 10;  /* This is the offset from EOF */

     vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, enAccess )));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&s32FilePos));
     s32PosToSet = s32FilePos - s32offFrmEOF;
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
     EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate(u32BytesToRead +1)));
     EXPECT_NE(OSAL_ERROR,(s32BytesRead = OSAL_s32IORead(hFile,ps8ReadBuffer,u32BytesToRead)));
     EXPECT_EQ(s32BytesRead,(tS32)u32BytesToRead);
     OSAL_vMemoryFree( ps8ReadBuffer );
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
     vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
     return 0;
}

/*****************************************************************************
* FUNCTION:          u32FSFileReadEveryNthByteFrmBOF( )
******************************************************************************/
tU32 u32FSFileReadEveryNthByteFrmBOF(void)
{
     OSAL_tIODescriptor hFile = 0;
     intptr_t s32FilePos = 0;
     tS32 s32BytesRead = 0;
     tS8 *ps8ReadBuffer = OSAL_NULL;
     OSAL_tenAccess enAccess = OSAL_EN_READONLY;
     intptr_t s32OffsetFrmStart = 0;
     tU32 u32BytesToRead = 10;/*Number of  bytes to read */
     
     vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
     s32OffsetFrmStart = 4;    /* This is the value of 'N' */
     while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&s32FilePos))
     {
         if(s32FilePos <= 0)break;

         EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32OffsetFrmStart));
  
         ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate(u32BytesToRead +1);
         EXPECT_NE(OSAL_ERROR,(s32BytesRead = OSAL_s32IORead (hFile,ps8ReadBuffer,u32BytesToRead)));
         OSAL_vMemoryFree( ps8ReadBuffer );
         s32OffsetFrmStart += s32OffsetFrmStart;
    }
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose( hFile));
 
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:          u32FileSystemFileReadEveryNthByteFrmEOF( )
******************************************************************************/
tU32 u32FSFileReadEveryNthByteFrmEOF(void)
{
     OSAL_tIODescriptor hFile = 0;
     intptr_t s32PosToSet = 0;
     intptr_t s32FilePos=0;
     tS32 s32BytesRead=0;
     tS8 *ps8ReadBuffer = OSAL_NULL;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
     intptr_t u32FileSz = 0;
     tS32 s32OffSet = 5; /* This is the offset from EOF */
     tU32 u32BytesToRead = 10;/*Number of  bytes to read */
  

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&u32FileSz));
     s32PosToSet = (tS32)u32FileSz - s32OffSet;
     while( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOWHERE,(intptr_t)&s32FilePos) != OSAL_ERROR && (s32PosToSet) > 0 )                              
     {
          if( OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet) == OSAL_ERROR )
          {
                break;
          }
          else
          {
              EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer = (tS8 *) OSAL_pvMemoryAllocate( u32BytesToRead +1)));
              EXPECT_NE(OSAL_ERROR,(s32BytesRead = OSAL_s32IORead (hFile, ps8ReadBuffer,u32BytesToRead)));
              if ( s32BytesRead == (tS32)OSAL_ERROR )
              {
                    break;
              }
              OSAL_vMemoryFree( ps8ReadBuffer );
              s32PosToSet -= s32OffSet; 
          }
     }
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));

     vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
     return 0;
}


/*****************************************************************************
* FUNCTION:          u32FSGetFileCRC( )
******************************************************************************/
tU32 u32FSGetFileCRC(void)
{
     OSAL_tIODescriptor hFile = 0; 
     intptr_t s32FilePosFrmEnd = 0;
     intptr_t s32FilePosFrmStart = 0;
     tS8 as8ReadBuffer [2];
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
      
     vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOWHERE,(intptr_t)&s32FilePosFrmStart ));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIONREAD,(intptr_t)&s32FilePosFrmEnd ));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOSEEK,s32FilePosFrmStart + s32FilePosFrmEnd - 2));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IORead(hFile, as8ReadBuffer, 2));
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
  
     vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
     return 0;
}

static OSAL_tSemHandle hSemAsyncFS;
/************************************************************************
*FUNCTION:        vOEDTFSAsyncCallback 
 ************************************************************************/
tVoid vOEDTFSAsyncCallback ( OSAL_trAsyncControl *prAsyncCtrl)
{
     EXPECT_EQ(OSAL_u32IOErrorAsync((OSAL_trAsyncControl*)prAsyncCtrl),OSAL_E_NOERROR);
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphorePost(hSemAsyncFS));    
} 

/*****************************************************************************
* FUNCTION:          AsyncRead()
******************************************************************************/
tU32 AsyncRead(OSAL_tIODescriptor hFile, tU32 u32BytesToRead)
{
     OSAL_trAsyncControl rAsyncCtrl = {0};         
     tS8 *ps8ReadBuffer = NULL;
     tS32 s32BytesRead = 0;
     tChar szSemName []    = "SemAsyncFS";
     tS32 s32RetVal = 0;

     /*Allocate memory*/
     EXPECT_NE((tS8 *)OSAL_NULL,(ps8ReadBuffer =  (tS8 *)OSAL_pvMemoryAllocate ( u32BytesToRead+1 )));
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphoreCreate (szSemName,&hSemAsyncFS,0));
    
     /*Fill the asynchronous control structure*/
     rAsyncCtrl.id = hFile;
     rAsyncCtrl.s32Offset = 0;
     rAsyncCtrl.pvBuffer = ps8ReadBuffer;
     rAsyncCtrl.u32Length = u32BytesToRead;
     rAsyncCtrl.pCallBack = (OSAL_tpfCallback)vOEDTFSAsyncCallback;
     rAsyncCtrl.pvArg = &rAsyncCtrl;
     /* If IOReadAsync returns OSAL_ERROR, Callback function is not called so semaphore is not waited upon. */
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOReadAsync(&rAsyncCtrl));
      /* Wait for 5 seconds for asynchronous read to get over. If semaphore not posted by the callback within 
          5 seconds,then cancel the asynchronous read.             */
     EXPECT_NE(OSAL_ERROR,s32RetVal = OSAL_s32SemaphoreWait (hSemAsyncFS,5000));
     if (( s32RetVal != OSAL_OK )&&( OSAL_u32ErrorCode() == OSAL_E_TIMEOUT ))
     {
          EXPECT_NE(OSAL_ERROR,OSAL_s32IOCancelAsync (rAsyncCtrl.id,&rAsyncCtrl));
     }
     else
     {
          EXPECT_NE(OSAL_ERROR,s32RetVal = (tS32)OSAL_u32IOErrorAsync ( &rAsyncCtrl ));
            EXPECT_NE(s32RetVal,(tS32)OSAL_E_INPROGRESS);                
            EXPECT_NE(s32RetVal,(tS32)OSAL_E_CANCELED);          
            //if (!(s32RetVal == (tS32)OSAL_E_INPROGRESS || s32RetVal == (tS32) OSAL_E_CANCELED))
          EXPECT_NE(OSAL_ERROR,(s32BytesRead = (tS32) OSAL_s32IOReturnAsync(&rAsyncCtrl)));
          EXPECT_EQ(s32BytesRead,(tS32)u32BytesToRead);
      }                             
     OSAL_vMemoryFree (ps8ReadBuffer);
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphoreClose ( hSemAsyncFS ));
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphoreDelete( szSemName ) );
     return 0;
}

/*****************************************************************************
* FUNCTION:          u32FSReadAsync()
******************************************************************************/  
tU32 u32FSReadAsync(tVoid)
{    
    OSAL_tIODescriptor hFile= 0;
    tU32 u32BytesToRead = 10;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
    AsyncRead(hFile, u32BytesToRead);
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile ));
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}


/*****************************************************************************
* FUNCTION:          AsyncWrite()
******************************************************************************/
tU32 AsyncWrite(OSAL_tIODescriptor hFile, tU32 u32BytesToWrite)
{
     OSAL_trAsyncControl rAsyncCtrl = {0};         
     tS32 s32BytesWritten = 0;
     tS8 *ps8WriteBuffer = NULL;
     tChar szSemName []    = "SemAsyncFS";
     tS32 s32RetVal         = 0;

     /*Allocate memory*/
     EXPECT_NE((tS8 *)OSAL_NULL,(ps8WriteBuffer =  (tS8 *)OSAL_pvMemoryAllocate ( u32BytesToWrite+1 )));
     OSAL_pvMemorySet(ps8WriteBuffer,'a',u32BytesToWrite);
         /*     Create semaphore for event signalling used by async callback.      */
     EXPECT_NE(OSAL_ERROR,(s32RetVal = OSAL_s32SemaphoreCreate (szSemName,&hSemAsyncFS,0)));
     /*Fill the asynchronous control structure*/
     rAsyncCtrl.s32Offset = 0;
     rAsyncCtrl.id = hFile;
     rAsyncCtrl.pvBuffer = ps8WriteBuffer;
     rAsyncCtrl.u32Length = u32BytesToWrite;
     rAsyncCtrl.pCallBack = (OSAL_tpfCallback) vOEDTFSAsyncCallback;
     rAsyncCtrl.pvArg = &rAsyncCtrl;
     /* If IOWriteAsync returns OSAL_ERROR, Callback function is not called so semaphore is not waited upon.*/
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOWriteAsync ( &rAsyncCtrl));
     /*  Wait for 5 seconds for asynchronous write to get over. If semaphore not posted by the callback within 5 seconds,
          then cancel the asynchronous write.                      */
     EXPECT_NE(OSAL_ERROR,(s32RetVal = OSAL_s32SemaphoreWait(hSemAsyncFS,5000)));
     if( s32RetVal != OSAL_OK )
     {
          EXPECT_NE(OSAL_u32ErrorCode(),OSAL_E_TIMEOUT);
          EXPECT_NE(OSAL_ERROR,OSAL_s32IOCancelAsync (rAsyncCtrl.id,&rAsyncCtrl));
     }
     else
     {

          EXPECT_NE(OSAL_ERROR,(s32RetVal = (tS32)OSAL_u32IOErrorAsync ( &rAsyncCtrl )));
 //         if (!(s32RetVal == (tS32)OSAL_E_INPROGRESS||s32RetVal == (tS32)OSAL_E_CANCELED))
          EXPECT_NE(OSAL_ERROR,(s32BytesWritten = (tS32)OSAL_s32IOReturnAsync (&rAsyncCtrl)));
          EXPECT_EQ(s32BytesWritten,(tS32)u32BytesToWrite);
     }                             
     OSAL_vMemoryFree (ps8WriteBuffer);
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphoreClose(hSemAsyncFS));
     EXPECT_NE(OSAL_ERROR,OSAL_s32SemaphoreDelete(szSemName));
     return 0;
}

/*****************************************************************************
* FUNCTION:          u32FSWriteAsync()
******************************************************************************/  
tU32 u32FSWriteAsync(tVoid)
{     
     OSAL_tIODescriptor hFile= 0;
     tU32 u32BytesToWrite = 10;
     OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

     vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

     EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
     AsyncWrite(hFile, u32BytesToWrite);
     EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    
     vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
     return 0;
}


/*****************************************************************************
* FUNCTION:        u32FileSystemFileMultipleHandles()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    TU_OEDT_FileSystem_047
* DESCRIPTION:  Open file multiple times
* HISTORY:        Created by Shilpa Bhat (RBIN/ECM1) on 23 Oct, 2008
******************************************************************************/  
tU32 u32FileSystemFileMultipleHandles(tVoid)
{
    OSAL_tIODescriptor hFd1=0;
    OSAL_tIODescriptor hFd2=0;
     
    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

    EXPECT_NE(OSAL_ERROR,(hFd1 = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, OSAL_EN_READONLY)));
    EXPECT_NE(OSAL_ERROR,(hFd2 = OSAL_IOOpen(OEDT_FFS3_WRITEFILE,OSAL_EN_READONLY)));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFd2));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFd1));

    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:        WriteToBuffer()
******************************************************************************/  
tU32 WriteToBuffer(OSAL_tIODescriptor hFile)
{
    intptr_t  iPos   = 0;
    tCS8 DatatoWrite[]= "abcdefg";
    
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOWrite(hFile, DatatoWrite, ((tU32)(-1))));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOWHERE, (intptr_t)&iPos));
    EXPECT_EQ(iPos, 0);
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemWriteFileWithInvalidSize()
******************************************************************************/  
tU32 u32FileSystemWriteFileWithInvalidSize(tVoid)
{
    OSAL_tIODescriptor hFile= 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
    WriteToBuffer(hFile);
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
   
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:        WriteToInvalBuffer()
******************************************************************************/  
tU32 WriteToInvalBuffer(OSAL_tIODescriptor hFile)
{
    tS32 s32RetVal = 0;
    tU32  u32BytesToWrite = 3;
    tS8* pBuffer = OSAL_NULL;

    EXPECT_EQ(OSAL_ERROR,(s32RetVal = OSAL_s32IOWrite(hFile, pBuffer, u32BytesToWrite)));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemWriteFileInvalidBuffer()
******************************************************************************/  
tU32 u32FileSystemWriteFileInvalidBuffer(tVoid)
{
    OSAL_tIODescriptor hFile= 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
    WriteToInvalBuffer(hFile);
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
    
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

#ifdef NewTest

/*****************************************************************************
* FUNCTION:        u32FileSystemWriteFileStepByStep()
******************************************************************************/  
tU32 u32FileSystemWriteFileStepByStep(tVoid)
{
    OSAL_tIODescriptor hFile= 0;
    tS32 iCnt=0;
    tU32 iStepWidth = 262144; /* 256 kB */ 
    tPS8 pBuffer = (tPS8) OSAL_NULL;
    tU32 iFileIdx=0;
    tU32 iFullSize = 1048576; /* 1MB */ 
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    
    EXPECT_NE(OSAL_NULL,((tPS8)pBuffer = (tPS8)OSAL_pvMemoryAllocate(iStepWidth)));
    OSAL_pvMemorySet(pBuffer, 'a', iStepWidth);

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen (OEDT_FFS3_WRITEFILE, enAccess)));
    iFileIdx = 0;
    while((iFileIdx < iFullSize) )
    {
        EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(OEDT_FFS3_WRITEFILE, OSAL_EN_READWRITE)));
        EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOSEEK,(tS32)iFileIdx));
        EXPECT_NE(OSAL_ERROR,(iCnt = OSAL_s32IOWrite(hFile, pBuffer, iStepWidth)));
        EXPECT_EQ(iCnt , (tS32)iStepWidth);
        EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
        iFileIdx += iStepWidth;
    }
    free((tS8*)pBuffer);

    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemGetFreeSpace()
******************************************************************************/  
tU32 u32FileSystemGetFreeSpace (tVoid)
{
    OSAL_tIODescriptor hFile=0;
    OSAL_trIOCtrlDeviceSize rSize = {0};
    char* namebuffer;
    
    EXPECT_NE(OSAL_NULL,((tU8 *)namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29)));
    OSAL_pvMemorySet(namebuffer,'\0',29);        
    file_name(count,namebuffer,OSAL_NULL);
                /* Open device */
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE)));
    rSize.u32High = 0;
    rSize.u32Low  = 0;
    /* Get free space */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE, (tS32)&rSize));
    EXPECT_NE(rSize.u32Low ,0) ;
    /* Close device */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile ));
    OSAL_vMemoryFree (namebuffer);
    return 0;
}
    
/*****************************************************************************
* FUNCTION:        u32FileSystemGetTotalSpace()
******************************************************************************/  
tU32 u32FileSystemGetTotalSpace (tVoid)
{
    OSAL_tIODescriptor hFile = 0;
    OSAL_trIOCtrlDeviceSize rSize;
    char* namebuffer;
   
    EXPECT_NE(OSAL_NULL,(namebuffer = (tU8 *)OSAL_pvMemoryAllocate(29)));
    OSAL_pvMemorySet(namebuffer,'\0',29);        
    file_name(count,namebuffer,OSAL_NULL);
                /* Open device */
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen(namebuffer, OSAL_EN_READWRITE)));
    rSize.u32High = 0;
    rSize.u32Low  = 0;
    /* Get total space */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOTOTALSIZE,(tS32)&rSize));
    EXPECT_NE(rSize.u32Low+rSize.u32High,0);
    /* Close device */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose(hFile));
    OSAL_vMemoryFree (namebuffer);
    return 0;
}
    
/*****************************************************************************
* FUNCTION:        u32FileSystemFileOpenCloseNonExstng()
******************************************************************************/  
tU32 u32FileSystemFileOpenCloseNonExstng(tVoid)
{
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0;
 
    /* Open a non-existing file */
    EXPECT_EQ(OSAL_ERROR,(hFile = OSAL_IOOpen ( szFilePath,enAccess)));
    
    return 0;
}
/*****************************************************************************
* FUNCTION:        DelFileWithoutClse()
******************************************************************************/  
tU32 DelFileWithoutClse(const tU8 * strDevName)
{
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
    OSAL_tIODescriptor hFile = 0;
    tCS8 DataToWrite[OEDT_FS_FILE_PATH_LEN + 1] = "Writing data";
    tU8 u8BytesToWrite = 15; /* Number of bytes to write */
    tS32 s32BytesWritten = 0;

    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen ( strDevName,enAccess)));
    /* Write File */    
    EXPECT_NE(OSAL_ERROR,s32BytesWritten = OSAL_s32IOWrite(hFile, DataToWrite,(tU32) u8BytesToWrite));
    /* Remove file without closing */
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IORemove ( strDevName));
            /* If successful, indicate error and close file */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose( hFile ));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove ((tString)strDevName ));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemFileDelWithoutClose()
******************************************************************************/  
tU32 u32FileSystemFileDelWithoutClose(tVoid)
{
    tU32 count = 0;

    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    file_name(count,OEDT_FFS3_WRITEFILE);
    DelFileWithoutClse(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:        SetFilePos()
******************************************************************************/  
tU32 SetFilePos(const tU8 * strDevName)
{
    OSAL_tIODescriptor hFile = 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
       tS32 s32PosToSet = 0;

    /* Open the common file */
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen ((tString)strDevName,enAccess )));
    /* With +ve Offset */    
    s32PosToSet = 10;
    /* Seek to positive offset */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
    /* Close file */        
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
    
    /*Open the common file*/
    EXPECT_EQ(OSAL_ERROR,(hFile = OSAL_IOOpen ((tString)strDevName,enAccess )));
    /* With -ve Offset from BOF */
    s32PosToSet = -7;
    /*Seek through the common file with neg offset from file beginning*/
    EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile ) == OSAL_ERROR)
 
    /*Open the common file*/
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOOpen ((tString)strDevName,enAccess)));
    /*Check if open is successful*/
    /* Set position at EOF */
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIONREAD,(tS32)&s32PosToSet));
    /*Seek within the common file*/
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
    /* Try Setting position beyond EOF */
    s32PosToSet = 10;
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hFile,OSAL_C_S32_IOCTRL_FIOSEEK,s32PosToSet));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
    return 0;
}

/*****************************************************************************
* FUNCTION:        u32FileSystemSetFilePosDiffOff()
******************************************************************************/  
tU32 u32FileSystemSetFilePosDiffOff(tVoid)
{    
    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    SetFilePos(OEDT_FFS3_WRITEFILE);
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}
/*****************************************************************************
* FUNCTION:        CreateFileMulti()
******************************************************************************/  
tU32 CreateFileMulti(const tU8 * strDevName)
{
    OSAL_tIODescriptor hFile= 0;
    OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
 
    EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate ((tString)strDevName, enAccess)));
    EXPECT_NE(OSAL_ERROR,OSAL_s32IOClose ( hFile));
    return 0;
}
/*****************************************************************************
* FUNCTION:        u32FileSystemCreateFileMultiTimes()
******************************************************************************/  
tU32 u32FileSystemCreateFileMultiTimes(void)
{
    vFSCreateCommonFile(OEDT_FFS3_WRITEFILE);
    CreateFileMulti(OEDT_FFS3_WRITEFILE);
    vFSRemoveCommonFile(OEDT_FFS3_WRITEFILE);
    return 0;
}

/*****************************************************************************
* FUNCTION:        CreateFileUniCode()
******************************************************************************/  
tU32 CreateFileUniCode(const tU8* strDevName)
{
   OSAL_tIODescriptor hFile = 0;
   OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

   /* Creation of file with chinese characters */
   EXPECT_NE(OSAL_ERROR,(hFile = OSAL_IOCreate ((tString)strDevName, enAccess)));
   EXPECT_NE(OSAL_ERROR,OSAL_ERROR == OSAL_s32IOClose ( hFile ) );
   /* Remove file */
   EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove ((tString)strDevName ) );
   return 0;
}
#endif
/* EOF */

