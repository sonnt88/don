/*****************************************************************************
| FILE:         rbtree_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include <stdlib.h>

using namespace std;


/*********************************************************************/
/* Test Cases for OSAL RB Tree API                                     */
/*********************************************************************/
/*
TEST(MULTIPRC, MPRC_Test1)
{

	OSAL_trProcessAttribute rProcessAttribute = { 0 };
    OSAL_tProcessID pid = OSAL_ERROR;

	rProcessAttribute.szAppName   = (tString)commandline;
	rProcessAttribute.szName      = (tString)commandline;
	rProcessAttribute.u32Priority = 0;

	OSAL_tSemHandle semHandle	  = 0;
	EXPECT_EQ(OSAL_OK,pid = OSAL_s32SemaphoreCreate("MultiProcessTest",&semHandle,0));
	if(pid == OSAL_OK) 
	{
    //   EXPECT_NE(OSAL_ERROR,pid = OSAL_ProcessSpawn(&rProcessAttribute));

       if(pid == OSAL_ERROR) 
	   {
	      TraceString("OSAL_ProcessSpawn %s failed",rProcessAttribute.szAppName);
	   }
	   else
	   {
		  EXPECT_EQ(OSAL_OK,OSAL_s32ProcessJoin(pid));
	   }
	}
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(semHandle));
	EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete("MultiProcessTest"));

}*/


tVoid u32MQThreadBigMsg(tPVoid pvArg)
{
   OSAL_tMQueueHandle hMQ  = 0;
   OSAL_trMessage OSAL_msg;
   OSAL_tenAccess enAccess = OSAL_EN_READONLY;
   tPU8 pu8Val;
   if(OSAL_OK != OSAL_s32MessageQueueOpen("BigMsg", OSAL_EN_READWRITE, &hMQ))
   {
	   TraceString("OSAL_s32MessageQueueOpen failed %d",OSAL_u32ErrorCode());
   }

   if(sizeof(OSAL_trMessage) != OSAL_s32MessageQueueWait(hMQ,(tPU8)&OSAL_msg,sizeof(OSAL_trMessage),OSAL_NULL,5000))
   {
	   TraceString("OSAL_s32MessageQueueWait failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_NULL == (pu8Val = OSAL_pu8MessageContentGet(OSAL_msg,enAccess)))
   {
	   TraceString("OSAL_pu8MessageContentGet failed %d",OSAL_u32ErrorCode());
   }
   if(100*1024 != OSAL_u32GetMessageSize(OSAL_msg))// is not supported for LOCAL memory
   {
	   TraceString("OSAL_u32GetMessageSize failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_OK != OSAL_s32MessageDelete( OSAL_msg))
   {
	   TraceString("OSAL_s32MessageDelete failed %d",OSAL_u32ErrorCode());
   }

   if(sizeof(OSAL_trMessage) != OSAL_s32MessageQueueWait(hMQ,(tPU8)&OSAL_msg,sizeof(OSAL_trMessage),OSAL_NULL,5000))
   {
	   TraceString("OSAL_s32MessageQueueWait failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_NULL == (pu8Val = OSAL_pu8MessageContentGet(OSAL_msg,enAccess)))
   {
	   TraceString("OSAL_pu8MessageContentGet failed %d",OSAL_u32ErrorCode());
   }
   if(101*1024 != OSAL_u32GetMessageSize(OSAL_msg))
   {
	   TraceString("OSAL_u32GetMessageSize failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_OK != OSAL_s32MessageDelete( OSAL_msg))
   {
	   TraceString("OSAL_s32MessageDelete failed %d",OSAL_u32ErrorCode());
   }
   
   if(sizeof(OSAL_trMessage) != OSAL_s32MessageQueueWait(hMQ,(tPU8)&OSAL_msg,sizeof(OSAL_trMessage),OSAL_NULL,5000))
   {
	   TraceString("OSAL_s32MessageQueueWait failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_NULL == (pu8Val = OSAL_pu8MessageContentGet(OSAL_msg,enAccess)))
   {
	   TraceString("OSAL_pu8MessageContentGet failed %d",OSAL_u32ErrorCode());
   }
   if(102*1024 != OSAL_u32GetMessageSize(OSAL_msg))
   {
	   TraceString("OSAL_u32GetMessageSize failed %d",OSAL_u32ErrorCode());
   }
   if(OSAL_OK != OSAL_s32MessageDelete( OSAL_msg))
   {
	   TraceString("OSAL_s32MessageDelete failed %d",OSAL_u32ErrorCode());
   }
   
 
   if(OSAL_OK != OSAL_s32MessageQueueClose(hMQ))
   {
	   TraceString("OSAL_s32MessageQueueClose failed %d",OSAL_u32ErrorCode());
   }
}


int main(int argc,char** argv)
{
   int opt,test = -1;
 
   /* check for test option ./osalprc.out -t 1000 */
   while ((opt = getopt (argc, argv, "t:")) != -1)
   {
      switch (opt)
      {
         case 't':
              test = atoi(optarg);
            break;
         default:
               TraceString("Usage: %s [-t testnumber] [-n] name",argv[0]);
            break;
      }
   }
   if(test != -1)
   {
      TraceString("Process execute test number %d ",test);
      OSAL_s32MessagePoolOpen();
	  switch(test)
	  {
		  case 1:
		       u32MQThreadBigMsg(NULL);
		      break;
		  default:
               TraceString("Unknown test number %d ",test);
		      break;
			  
	  }
      OSAL_s32MessagePoolClose();
   }
   OSAL_vProcessExit();
 
}
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
