/*****************************************************************************
| FILE:         osalevent_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;

#define SEMAPHORE_NAME 			      "TestMutex"
#define GLOBAL_SEMAPHORE_START_VALUE  1
#define GLOBAL_SEMAPHORE_START_VALUE_ 3
#define GLOBAL_SEMAPHORE_NAME 	      "GMutex"
#define GLOBAL_SEMAPHORE_NAME_        "Mutex"
#define SEMAPHORE_NAME_MAX            "Semaphore with name equal to 31"
#define SEMAPHORE_NAME_EXCEEDS_MAX	  "Semaphore with name exceeding 31 characters"
#define SEMTEXT						  OSAL_C_STRING_DEVICE_FFS"/semtext.txt"
#define THIRTY_MILLISECONDS           30
#define ONE_SECOND                    1000
#define TWO_SECONDS                   2000
#define THREE_SECONDS                 3000
#define FIVE_SECONDS                  5000
#define STRESS_WAIT                   10000
#define SEMAPHORE_COUNT_INVALID       ((tS32)-1)

#undef 	INIT_VAL

#define INIT_VAL                      (tU8)0x00 // Binary : 0000 0000
#define VALID_VAL                     (tU8)0x07	// Binary : 0000 0111
#define CF1_MASK                      (tU8)0x01 // Binary : 0000 0001
#define CF2_MASK					  (tU8)0x02	// Binary : 0000 0010
#define CF3_MASK					  (tU8)0x04	// Binary : 0000 0100
#define MAX_SEMAPHORES                (tU8)25
#define NO_OF_POST_THREADS            10
#define NO_OF_WAIT_THREADS            15
#define NO_OF_CLOSE_THREADS           1
#define NO_OF_CREATE_THREADS          2

#undef  SRAND_MAX
#define SRAND_MAX                     (tU32)65535
/*rav8kor - commented as standard macro from osansi.h*/
//#define RAND_MAX                      0x7FFFFFFF
#undef 	MAX_LENGTH
#define MAX_LENGTH                    256
#define TOT_THREADS                   (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)


/*Data Types*/
typedef struct SemaphoreTableEntry_
{
	OSAL_tMtxHandle hSem;
	tChar hSemName[MAX_LENGTH];
	tBool hToBeDeleted;
}SemaphoreTableEntry;

static OSAL_tMtxHandle globalsemHandle     = 0;
static OSAL_tMtxHandle globalsemHandle_    = 0;
static tU8  u8Byte                         = INIT_VAL;



/*ThreadWaitPost*/
tVoid ThreadMtxTest(const tVoid *pvArg )
{
   TraceString("ThreadMtxTest start");
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(globalsemHandle));
   TraceString("ThreadMtxTest end");
   /*Delay the ThreadExit call in the wrapper function*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}


tVoid MtxControlFlow_1( tVoid *pvArg )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   /*Wait on semaphore*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
   /*Wait for 2 seconds*/
   OSAL_s32ThreadWait( TWO_SECONDS);
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
   u8Byte |= CF1_MASK;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(globalsemHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(globalsemHandle));
   OSAL_s32ThreadWait(FIVE_SECONDS);
}

tVoid MtxControlFlow_2( tVoid *pvArg )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
    /*Wait for 2 seconds*/
   OSAL_s32ThreadWait( TWO_SECONDS );
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
   u8Byte |= CF2_MASK;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle));
   OSAL_s32ThreadWait( FIVE_SECONDS);
}

/*Thread 3*/
tVoid MtxControlFlow_3( tVoid *pvArg )
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvArg);
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
   OSAL_s32ThreadWait( TWO_SECONDS );
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle_,OSAL_C_TIMEOUT_FOREVER));
   u8Byte |= CF3_MASK;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle));
   OSAL_s32ThreadWait( FIVE_SECONDS );
}

/*********************************************************************/
/* Test Cases for OSAL Mutex API                                     */
/*********************************************************************/

TEST(OsalCoreMutexTest, u32CreateCloseDelMtxVal)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
}


TEST(OsalCoreMutexTest, u32CreateMtxNameNULL)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexCreate(OSAL_NULL,&semHandle));
}

TEST(OsalCoreMutexTest, u32CreateMtxHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexCreate(SEMAPHORE_NAME,OSAL_NULL));
}

TEST(OsalCoreMutexTest, u32CreateCloseDelMtxNameMAX)
{
   OSAL_tMtxHandle semHandle     = 0;
   /*Create the Semaphore within the system*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME_MAX, &semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete( SEMAPHORE_NAME_MAX));
}


TEST(OsalCoreMutexTest, u32CreateMtxWithNameExceedsMAX)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_ERROR ,OSAL_s32MutexCreate(SEMAPHORE_NAME_EXCEEDS_MAX, &semHandle));
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}


TEST(OsalCoreMutexTest, u32CreateTwoMtxSameName)
{
   OSAL_tMtxHandle semHandle1    = 0;
   OSAL_tMtxHandle semHandle2    = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle1));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle2));
   EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
}

TEST(OsalCoreMutexTest, u32DelMtxWithNULL)
{
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexDelete(OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32DelMtxWithNonExistingName)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   /*Scenario 1*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
   /*Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexDelete("WhoAmI"));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode()); 
}


TEST(OsalCoreMutexTest, u32DelMtxCurrentlyUsed)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
}

TEST(OsalCoreMutexTest, u32OpenMtxWithNameNULL)
{
   OSAL_tMtxHandle semHandle     = 0;
   OSAL_tMtxHandle semHandle_    = 0;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen(OSAL_NULL,&semHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));

   /*Scenario 2*/
   /*Open the semaphore passing name as OSAL_NULL, on a handle that was never used for create/open in the system*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen(OSAL_NULL,&semHandle_));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32OpenMtxWithHandleNULL)
{
   OSAL_tMtxHandle semHandle     = 0;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose( semHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen( SEMAPHORE_NAME,OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   /*Scenario 2*/
   /*Open the semaphore passing handle as OSAL_NULL, on a semaphore name that was never used for create/open in the system*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen( "Unknown",OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32OpenMtxWithNameExceedsMAX)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen( SEMAPHORE_NAME_EXCEEDS_MAX,&semHandle));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32OpenMtxWithNonExistingName)
{
   OSAL_tMtxHandle semHandle     = 0;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
   /*Scenario 2*/
   /*Open the semaphore, on a semaphore name that was never used for create/open in the system*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexOpen( "NeverKnownToSystem",&semHandle));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32CloseMtxWithInvalidHandle)
{
   OSAL_tMtxHandle semHandle     = 0;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32WaitPostMtxValid)
{
   OSAL_trThreadAttribute trAttr     = {OSAL_NULL};
   globalsemHandle = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate( GLOBAL_SEMAPHORE_NAME,&globalsemHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
   
   trAttr.szName       = (char*)"MutexWPTest";
   trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize	= 2048;/*2MB stack*/
   trAttr.pfEntry      = (OSAL_tpfThreadEntry)ThreadMtxTest;
   trAttr.pvArg        = NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   OSAL_s32ThreadWait(1000);
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle));
   OSAL_s32ThreadWait( 10);
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock( globalsemHandle,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock( globalsemHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(globalsemHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(GLOBAL_SEMAPHORE_NAME));
}

TEST(OsalCoreMutexTest, u32WaitMtxWithInvalidHandle)
{
   OSAL_tMtxHandle semHandle     = 0;
   OSAL_tMtxHandle semHandle_    = 0;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock( semHandle,TWO_SECONDS));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   /*Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock( semHandle_,TWO_SECONDS));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreMutexTest, u32ReleaseMtxWithInvalidHandle)
{
   OSAL_tMtxHandle semHandle     =  0;
   OSAL_tMtxHandle semHandle_    =  0;
   /*Scenario 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
   /*Post on a non existent handle(invalidated)*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   /*Scenario 2*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexUnLock(semHandle_));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}



#ifdef CHECK_TEST
TEST(OsalCoreMutexTest, u32WaitPostMtxValidExceedsMAX)
{
   tS32 s32Count             = SEMAPHORE_COUNT_INVALID;
   OSAL_tMtxHandle semHandle = 0;
	
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(SEMAPHORE_NAME,&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_E_QUEUEFULL,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(SEMAPHORE_NAME));
}
		
TEST(OsalCoreMutexTest, u32AccessMtxMpleThreads)
{
   tS32 s32Count             = SEMAPHORE_COUNT_INVALID;
   OSAL_tMtxHandle semHandle = 0;

   tS32 s32loop                    = 0;
   OSAL_trThreadAttribute trAttr_1 = {OSAL_NULL};
   OSAL_trThreadAttribute trAttr_2 = {OSAL_NULL};
   OSAL_trThreadAttribute trAttr_3 = {OSAL_NULL};
    
   globalsemHandle  = 0;
   globalsemHandle_ = 0;
   u8Byte = INIT_VAL;

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(GLOBAL_SEMAPHORE_NAME_,&globalsemHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate(GLOBAL_SEMAPHORE_NAME,&globalsemHandle));
   
   trAttr_1.szName       = "MtxControlFlow_1";
   trAttr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr_1.s32StackSize = 2048;/*2MB stack*/
   trAttr_1.pfEntry      = (OSAL_tpfThreadEntry)MtxControlFlow_1;
   trAttr_1.pvArg        = OSAL_NULL;
   trAttr_2.szName       = "MtxControlFlow_2";
   trAttr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr_2.s32StackSize = 2048;/*2MB stack*/
   trAttr_2.pfEntry      = (OSAL_tpfThreadEntry)MtxControlFlow_2;
   trAttr_2.pvArg        = OSAL_NULL;
   trAttr_3.szName       = "MtxControlFlow_3";
   trAttr_3.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr_3.s32StackSize = 2048;/*2MB stack*/
   trAttr_3.pfEntry      = (OSAL_tpfThreadEntry)MtxControlFlow_3;
   trAttr_3.pvArg        = OSAL_NULL;

   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_1));
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_2));
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr_3));
   /*Busy wait*/
   do
   {
       /*Set semaphore value*/
       s32Count =  SEMAPHORE_COUNT_INVALID;
       OSAL_s32ThreadWait( TWO_SECONDS );
       EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreGetValue( globalsemHandle,&s32Count));//TODO
       s32loop++;
   }while( GLOBAL_SEMAPHORE_START_VALUE_ != s32Count && s32loop < 10);					
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(globalsemHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(GLOBAL_SEMAPHORE_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(globalsemHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete(GLOBAL_SEMAPHORE_NAME_));
   EXPECT_EQ(VALID_VAL ,u8Byte);
}
#endif


tVoid ThreadMtxNoBlockingTest(const tVoid *pvArg )
{
   OSAL_tMtxHandle semHandle = 0; 

   TraceString("ThreadMtxTest start");
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexOpen("Mutex15",&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER));
   OSAL_s32ThreadWait( 500 );
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   TraceString("ThreadMtxTest end");
   /*Delay the ThreadExit call in the wrapper function*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}

TEST(OsalCoreMutexTest, u32NoBlockingMtxWait)
{
   OSAL_tMtxHandle semHandle = 0; 
   OSAL_trThreadAttribute trAttr = {OSAL_NULL};

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate("Mutex15",&semHandle));
    
   trAttr.szName       = "MtxNoBlockingTest";
   trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize = 2048;/*2MB stack*/
   trAttr.pfEntry      = (OSAL_tpfThreadEntry)ThreadMtxNoBlockingTest;
   trAttr.pvArg        = OSAL_NULL;
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAttr));
   OSAL_s32ThreadWait( 100 );
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock(semHandle, OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete("Mutex15"));
   OSAL_s32ThreadWait( TWO_SECONDS );
}

tVoid ThreadMtxDurationTest(const tVoid *pvArg )
{
   OSAL_tMtxHandle semHandle = 0; 
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexOpen("Mutex16",&semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER));
   OSAL_s32ThreadWait( 4000 );
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
}

TEST(OsalCoreMutexTest, u32BlockingDurationMtxWait)
{
   OSAL_tMtxHandle semHandle  = 0;
   OSAL_tMSecond u32StartTime = 0;  
   OSAL_tMSecond u32EndTime   = 0;  
   OSAL_tMSecond u32Duration  = 0;  

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate("Mutex16",&semHandle));

   OSAL_trThreadAttribute trAttr     = {OSAL_NULL};
   trAttr.szName       = (char*)"MutexDurationTest";
   trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize	= 2048;/*2MB stack*/
   trAttr.pfEntry      = (OSAL_tpfThreadEntry)ThreadMtxDurationTest;
   trAttr.pvArg        = NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   OSAL_s32ThreadWait(200);

   u32StartTime = OSAL_ClockGetElapsedTime();
   /*Wait on the Semaphore with semaphore count as zero and wait time as one second*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock( semHandle,ONE_SECOND));
   u32EndTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

   if( u32EndTime > u32StartTime )
   {
      /*Calculate duration of wait*/
      u32Duration = u32EndTime - u32StartTime;
      EXPECT_LE(( ONE_SECOND - THIRTY_MILLISECONDS ),u32Duration);
      EXPECT_LE(u32Duration,(ONE_SECOND + THIRTY_MILLISECONDS));
   }
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete("Mutex16"));
   OSAL_s32ThreadWait( 4000 );
}


TEST(OsalCoreMutexTest, u32WaitPostWaitWaitNonBlock)
{
   OSAL_tMtxHandle semHandle  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate("Mutex17", &semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete("Mutex17"));
   /*Wait on the Semaphore - Should pass*/	
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_NOBLOCKING));
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
}


TEST(OsalCoreMutexTest, u32RecursiveLock)
{
   OSAL_tMtxHandle semHandle  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate_Opt("Mutex18", &semHandle,1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
  
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete("Mutex18"));
}


tVoid ThreadMtxWait(const tVoid *pvArg )
{
   OSAL_tMtxHandle semHandle  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexOpen("Mutex19", &semHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32MutexLock(semHandle,1000)); 
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
}

TEST(OsalCoreMutexTest, u32RecursiveLock2)
{
   OSAL_tMtxHandle semHandle  = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexCreate_Opt("Mutex19", &semHandle,1));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 


   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexLock(semHandle,OSAL_C_TIMEOUT_FOREVER)); 

   OSAL_s32ThreadWait( TWO_SECONDS );

   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexUnLock(semHandle));
  
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexClose(semHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32MutexDelete("Mutex19"));
}


pthread_mutex_t     hMutex;        /* linux Mutex handle*/
pthread_mutexattr_t mutexattr;
 

tVoid ThreadMtxDurationTest2(const tVoid *pvArg )
{
   tS32 s32ReturnValue;
   EXPECT_EQ(0,s32ReturnValue = pthread_mutex_lock(&hMutex));
   OSAL_s32ThreadWait( 4000 );
   EXPECT_EQ(0,s32ReturnValue = pthread_mutex_unlock(&hMutex));
}

tU32 u32GemTmoStruct2(struct timespec *tim,OSAL_tMSecond msec)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   if(!clock_gettime(CLOCK_REALTIME, tim))
   {
      /*kept below time calculation outside while(1) because everytime its keep on 
        incrementing timeout sja3kor*/
        /* If the Timers option is supported, the timeout shall be based on the CLOCK_REALTIME clock; 
          if the Timers option is not supported, the timeout shall be based on the system clock as returned by the time() function.*/
       tim->tv_sec += msec / 1000 ;
       tim->tv_nsec += (msec%1000)*1000000;
       if(tim->tv_nsec >= 1000000000)
       {
          tim->tv_sec += 1;
          tim->tv_nsec = tim->tv_nsec - 1000000000;
       }
   }
   else
   {
      u32ErrorCode = u32ConvertErrorCore(errno);
   }
   return u32ErrorCode;
}

TEST(OsalCoreMutexTest, u32BlockingDurationMtxWait2)
{
   tS32 s32ReturnValue;
   OSAL_tMSecond u32StartTime = 0;  
   OSAL_tMSecond u32EndTime   = 0;  
   OSAL_tMSecond u32Duration  = 0;  

   EXPECT_EQ(0,pthread_mutexattr_init(&mutexattr));
   EXPECT_EQ(0,pthread_mutexattr_settype(&mutexattr,PTHREAD_MUTEX_NORMAL));
   EXPECT_EQ(0,pthread_mutexattr_setrobust(&mutexattr,PTHREAD_MUTEX_ROBUST));
   EXPECT_EQ(0,pthread_mutex_init(&hMutex,&mutexattr));

   OSAL_trThreadAttribute trAttr     = {OSAL_NULL};
   trAttr.szName       = (char*)"MutexDurationTest";
   trAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAttr.s32StackSize	= 2048;/*2MB stack*/
   trAttr.pfEntry      = (OSAL_tpfThreadEntry)ThreadMtxDurationTest2;
   trAttr.pvArg        = NULL;
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAttr));
   OSAL_s32ThreadWait(200);

   struct timespec tim = {0,0};
   u32GemTmoStruct2(&tim,1000);

   u32StartTime = OSAL_ClockGetElapsedTime();
   EXPECT_NE(0,s32ReturnValue = pthread_mutex_trylock(&hMutex));
   /*Wait on the Semaphore with semaphore count as zero and wait time as one second*/
   EXPECT_NE(0,s32ReturnValue = pthread_mutex_timedlock(&hMutex,&tim));

   u32EndTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(ETIMEDOUT,s32ReturnValue);

   if( u32EndTime > u32StartTime )
   {
      /*Calculate duration of wait*/
      u32Duration = u32EndTime - u32StartTime;
      EXPECT_LE(( 1000 - THIRTY_MILLISECONDS ),u32Duration);
      EXPECT_LE(u32Duration,(1000 + THIRTY_MILLISECONDS));
   }

   OSAL_s32ThreadWait( 4000 );
   EXPECT_EQ(OSAL_OK,s32ReturnValue = pthread_mutex_destroy(&hMutex));
}



/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
