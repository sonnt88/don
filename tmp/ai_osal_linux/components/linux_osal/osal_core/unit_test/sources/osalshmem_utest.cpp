/*****************************************************************************
| FILE:         osalshmem_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal shared memory implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "osal_core_unit_test_mock.h"

using namespace std;

#define SH_SIZE 100
#define SH_NAME "SH_MEM"
#define SH_INVAL_NAME "-------|-------|-------|-------|-------|"   /* name length = 40 */

#define SH_MEMSIZE 1000
#define SH_MEMSIZE_512 		512
#undef  VALID_STACK_SIZE
#define VALID_STACK_SIZE 	2048
#define SH_HARRAY 9
#define SH_MAX 9
#define SH_MIN 0
#define INVAL_HANDLE -1
#define OSAL_INVALID_ACCESS 0x0200
#define MAP_OFFSET    20
#define MAP_LENGTH    30
#define LENGTH_127          127
#define ZERO_LENGTH   0
#define ZERO_OFFSET         0
#define OFFSET_0            0
#define OFFSET_128          128
#define OFFSET_256          256
#define OFFSET_384          384
#define OFFSET_511          511
#define OFFSET_800          800
#define PATTERN_THR1        0x00000001
#define PATTERN_THR2        0x00000002
#define PATTERN_THR3        0x00000004
#define PATTERN_THR4        0x00000008
#undef  THREE_SECONDS
#define THREE_SECONDS       3000

#undef 	DEBUG_MODE

#define DEBUG_MODE          0

#define VALID_STACK_SIZE        2048
#define SH_SIZE  100

static OSAL_tShMemHandle ghShared;
static OSAL_tEventHandle gSHEventHandle;
static OSAL_tEventMask   gevMask;

/*********************************************************************/
/* Test Cases for OSAL Shared Memory API                             */
/*********************************************************************/

TEST(OsalCoreShMemTest, u32SHCreateDeleteNameNULL)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate(OSAL_NULL, OSAL_EN_READONLY, SH_SIZE);

   EXPECT_EQ(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32SharedMemoryDelete( OSAL_NULL ));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHCreateNameVal)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
}

TEST(OsalCoreShMemTest, u32SHCreateDeleteNameExceedNameLength)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate(SH_INVAL_NAME, OSAL_EN_READONLY, SH_SIZE);

   EXPECT_EQ(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32SharedMemoryDelete( SH_INVAL_NAME));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}


TEST(OsalCoreShMemTest, u32SHCreateNameSameNames)
{
   OSAL_tShMemHandle SHHandle = 0;
   OSAL_tShMemHandle SHHandle1 = 0;


   /* Create the shared Memory*/
   SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   SHHandle1 = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_EQ(OSAL_ERROR,SHHandle1);
   EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));
}

TEST(OsalCoreShMemTest, u32SHCreateSizeZero)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_MIN);
   EXPECT_EQ(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());      
}

TEST(OsalCoreShMemTest, u32SHDeleteNameInval)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32SharedMemoryDelete( SH_NAME));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHDeleteNameInUse)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
}

TEST(OsalCoreShMemTest, u32SHOpenCloseDiffModes)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   OSAL_tShMemHandle SHHandle_RW = 0;
   OSAL_tShMemHandle SHHandle_W = 0;
   OSAL_tShMemHandle SHHandle_R = 0;
   OSAL_tShMemHandle SHHandle_Inval = 0;

   EXPECT_NE(OSAL_ERROR,SHHandle);
   /*Open the shared memory with OSAL_EN_READWRITE) */ 
   SHHandle_RW = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR,SHHandle_RW);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle_RW));
   /*Open the shared memory with OSAL_EN_WRITEONLY) */ 
   SHHandle_W = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_WRITEONLY);
   EXPECT_NE(OSAL_ERROR,SHHandle_W);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle_W));
   /*Open the shared memory with OSAL_EN_READONLY) */ 
   SHHandle_R = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR,SHHandle_R);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle_R));

   /*Open the shared memory with OSAL_EN_BINARY( invalid )) */ 
   SHHandle_Inval = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_BINARY);
   EXPECT_EQ(OSAL_ERROR,SHHandle_Inval);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
 
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
}

TEST(OsalCoreShMemTest, u32SHOpenNameNULL)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryOpen( OSAL_NULL, OSAL_EN_READWRITE);
   EXPECT_EQ(OSAL_ERROR,SHHandle );
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHOpenNameInVal)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));

   /*Open the shared memory with OSAL_EN_READWRITE )*/ 
   EXPECT_EQ(OSAL_ERROR,(SHHandle = OSAL_SharedMemoryOpen( SH_NAME, OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHCloseNameInVal)
{
   OSAL_tShMemHandle SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
   EXPECT_NE(OSAL_ERROR,SHHandle);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));

   /*Close the shared memory*/ 
   EXPECT_EQ(OSAL_ERROR,OSAL_s32SharedMemoryClose(SHHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHCreateMaxSegment)
{
   tChar SH_name[ SH_HARRAY ][ SH_HARRAY ];
   tS32 s32Count = 0; 
   tS32 s32Count1 = 0; 
   OSAL_tShMemHandle SHHandle[SH_HARRAY] = {0};
   memset(SH_name,0,sizeof(SH_name));

   /* Create  MAX No of shared Memory  */
   for( ; s32Count < SH_MAX; ++s32Count )
   {
      OSAL_s32PrintFormat( SH_name[ s32Count ], "SH_NAME%d", s32Count );
      SHHandle[ s32Count ] = OSAL_SharedMemoryCreate(SH_name[ s32Count ],OSAL_EN_READWRITE,SH_MEMSIZE );
      EXPECT_NE(OSAL_ERROR,SHHandle[ s32Count ]);
   }

   --s32Count;

   /* close and Delete the shared Memory   */
   for( s32Count1 = s32Count; SH_MIN <= s32Count1; --s32Count1 )
   {
      EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle[ s32Count1 ]));
      EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_name[ s32Count1 ]));
   }
}

TEST(OsalCoreShMemTest, u32SHMemoryMapNonExistentHandle)
{
   OSAL_tShMemHandle hShared  = 0;
   OSAL_tShMemHandle hShared_ = INVAL_HANDLE;
   /*Scenario 1*/
   EXPECT_NE(OSAL_ERROR,( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(hShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
   /*Attempt a Memory Map!*/ 
   EXPECT_EQ(OSAL_NULL ,OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,MAP_LENGTH,MAP_OFFSET));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
   
   /*Scenario 2*/
   /*Attempt a Memory Map on an invalid handle!*/ 
   EXPECT_EQ(OSAL_NULL ,OSAL_pvSharedMemoryMap( hShared_,OSAL_EN_READWRITE,MAP_LENGTH,MAP_OFFSET));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreShMemTest, u32SHMemoryMapLimitCheck)
{
   OSAL_tShMemHandle hShared  =0;
   EXPECT_NE(OSAL_ERROR,( hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE)));
   /*At the end of Shared Memory*/
   EXPECT_NE(0 ,(uintptr_t)OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,ZERO_LENGTH,SH_MEMSIZE));
   /*Beyond Limit of Shared Memory*/
   EXPECT_EQ(OSAL_NULL ,OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,MAP_LENGTH,SH_MEMSIZE));
   EXPECT_EQ(OSAL_E_NOSPACE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(hShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
}

TEST(OsalCoreShMemTest, u32SHMemoryMapDiffAccessModes)
{
   OSAL_tShMemHandle hShared  = 0;
   tPVoid ptr1                = OSAL_NULL;
   tPVoid ptr2                = OSAL_NULL;
   tPVoid ptr3                = OSAL_NULL;

   EXPECT_NE(OSAL_ERROR,(hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE)));
   /*MEMORY MAP WITH MODE OSAL_EN_READONLY*/
   EXPECT_NE(0 ,(uintptr_t)(ptr1 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READONLY,MAP_LENGTH,MAP_OFFSET)));
   /*MEMORY MAP WITH MODE OSAL_EN_WRITEONLY*/
   EXPECT_NE(0 ,(uintptr_t)(ptr2 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_WRITEONLY,MAP_LENGTH,MAP_OFFSET)));
   /*MEMORY MAP WITH MODE OSAL_EN_READWRITE*/
   EXPECT_NE(0 ,(uintptr_t)(ptr3 = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,MAP_LENGTH,MAP_OFFSET)));
   /*Check Address Returned - Should not be dependant on Access Modes.*/
   EXPECT_EQ(ptr1,ptr2 );
   EXPECT_EQ(ptr1 ,ptr3 );

   /*MEMORY MAP WITH MODE OSAL_INVALID_ACCESS*/
   EXPECT_EQ(OSAL_NULL,OSAL_pvSharedMemoryMap( hShared,(OSAL_tenAccess)OSAL_INVALID_ACCESS,MAP_LENGTH,MAP_OFFSET));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());

   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(hShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
}
		
TEST(OsalCoreShMemTest, u32SHMemoryScanMemory)
{
   tChar tcSeeker            = '\0';
   OSAL_tShMemHandle hShared = 0;
   tVoid * ptr               = OSAL_NULL;
   tU32 u32Offset = 0;
   EXPECT_NE(OSAL_ERROR,(hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512)));
   /*Initialize the Shared Memory*/
   EXPECT_NE(0 ,(uintptr_t)(ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,ZERO_LENGTH,ZERO_OFFSET)));
   if(ptr)
   {  
      memset( ptr,'\0',SH_MEMSIZE_512 );
      memset( ptr,'A',( SH_MEMSIZE_512-1 ) );
   }
   
   /*Scan the Shared Memory*/
   while( u32Offset < ( SH_MEMSIZE_512-1 ) )
   {
      /*Scan Memory - Return address of location u32Offset bytes from Shared Memory Base*/
      EXPECT_NE(0 ,(uintptr_t)(ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,ZERO_LENGTH,u32Offset)));
      if(ptr)
      {  
         /*Access Shared Memory Location contents*/
         tcSeeker = *( (tChar *)ptr );
         /*Check for content*/
         EXPECT_EQ(tcSeeker,'A' );
      } 
      /*Increment offset*/
      u32Offset++;
   }

   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose( hShared ) );
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME ) );
}

TEST(OsalCoreShMemTest, u32SHMemoryScanMemoryBeyondLimit)
{
   OSAL_tShMemHandle hShared = 0;

   EXPECT_NE(OSAL_ERROR,(hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE)));
   EXPECT_NE(0 ,(uintptr_t)OSAL_pvSharedMemoryMap(hShared,OSAL_EN_READWRITE,ZERO_LENGTH,SH_SIZE-2));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(hShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
}

TEST(OsalCoreShMemTest, u32UnmapNonMappedAddress)
{
   tVoid * ptr = OSAL_NULL;
   /*Allocate memory dynamically*/
   ptr = malloc( (tU32)1 );

   /*Unmap a non mapped location*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryUnmap( ptr, 1 ) ) ;
		/* Error, unmap will pass with all non zero adderss 
		  this is known limitation of osal */
   if (ptr != OSAL_NULL) 
   {
     free(ptr);
     ptr = OSAL_NULL;
   }
}

TEST(OsalCoreShMemTest, u32UnmapNULLAddress)
{
	/*Unmap a non mapped location*/
  EXPECT_EQ(OSAL_ERROR,OSAL_s32SharedMemoryUnmap( OSAL_NULL,1));
  EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreShMemTest, u32UnmapMappedAddress)
{
   tVoid * ptr               = OSAL_NULL;
   OSAL_tShMemHandle hShared = 0;

   EXPECT_NE(OSAL_ERROR,(hShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_SIZE)));
   EXPECT_NE(0 ,(uintptr_t)( ptr = OSAL_pvSharedMemoryMap( hShared,OSAL_EN_READWRITE,SH_SIZE,0)));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryUnmap(ptr,SH_SIZE));

   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(hShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
}
	



void Thread_SH_Memory(tVoid* pvArg)
{
   OSAL_tShMemHandle handle_2 = 0;
   tPVoid map_2 = 0;

   EXPECT_NE(OSAL_ERROR,(handle_2 = OSAL_SharedMemoryOpen(SH_NAME, OSAL_EN_READWRITE)));
   EXPECT_NE(0 ,(uintptr_t)(map_2 = OSAL_pvSharedMemoryMap(handle_2, OSAL_EN_READWRITE, SH_SIZE, 0)));
   if(map_2)memset(map_2, 1, SH_SIZE);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryUnmap(map_2, SH_SIZE));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(handle_2));
}
		
TEST(OsalCoreShMemTest, u32SHOpenDoubleCloseOnce)
{
   OSAL_tShMemHandle handle_1 = 0;
   tPVoid map_1 = 0;
   OSAL_trThreadAttribute threadAttr = { 0 };
   tS8 ps8ThreadName[16] = { 0 };

   sprintf((tString) ps8ThreadName, "%s%03u", "SH_TEST_", 1);
   threadAttr.u32Priority = 0;
   threadAttr.szName = (tString) ps8ThreadName;
   threadAttr.pfEntry = (OSAL_tpfThreadEntry) Thread_SH_Memory;
   threadAttr.pvArg = NULL;
   threadAttr.s32StackSize = VALID_STACK_SIZE;


   EXPECT_NE(OSAL_ERROR,(handle_1 = OSAL_SharedMemoryCreate(SH_NAME, OSAL_EN_READONLY, SH_SIZE)));
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&threadAttr));
 
   OSAL_s32ThreadWait(1000);  // todo create mq or event for syncronisation
   EXPECT_NE(0 ,(uintptr_t)(map_1 = OSAL_pvSharedMemoryMap(handle_1, OSAL_EN_READWRITE, SH_SIZE, 0)));
   memset(map_1, 1, SH_SIZE);
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryUnmap(map_1, SH_SIZE));
 
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose( handle_1) );
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME ) );
}

TEST(OsalCoreShMemTest, u32SHCreateNameValLoop)
{
  OSAL_tShMemHandle SHHandle = 0;
  int loop = 0;
   
  for (loop=0; loop< 5000; loop++)
  {
    SHHandle = 0;
   
    /* Create the shared Memory with vaild Size */
    SHHandle = OSAL_SharedMemoryCreate( SH_NAME, OSAL_EN_READONLY, SH_SIZE);
    
    EXPECT_NE(OSAL_ERROR,SHHandle );
    EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(SHHandle));
    EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete( SH_NAME));
  }
}

	
tVoid Thread_SH_0_127( tVoid * ptr )
{
   tVoid *shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,LENGTH_127,OFFSET_0 );
   EXPECT_NE(0 ,(uintptr_t)shptr);
   if(shptr)memset( shptr,'a',LENGTH_127+1 );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gSHEventHandle,PATTERN_THR1,OSAL_EN_EVENTMASK_OR ) );
}
tVoid Thread_SH_128_255( tVoid * ptr )
{
   tVoid *shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,LENGTH_127,OFFSET_128 );
   EXPECT_NE(0 ,(uintptr_t)shptr);
   if(shptr)memset( shptr,'b',LENGTH_127+1 );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gSHEventHandle,PATTERN_THR2,OSAL_EN_EVENTMASK_OR ) );
}

tVoid Thread_SH_256_383( tVoid * ptr )
{
   tVoid *shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,LENGTH_127,OFFSET_256 );
   EXPECT_NE(0 ,(uintptr_t)shptr);
   if(shptr)memset( shptr,'c',LENGTH_127+1 );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gSHEventHandle,PATTERN_THR3,OSAL_EN_EVENTMASK_OR ) );
}

tVoid Thread_SH_384_511( tVoid * ptr )
{
   tVoid *shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,LENGTH_127,OFFSET_384 );
   EXPECT_NE(0 ,(uintptr_t)shptr);
   if(shptr)memset( shptr,'d',LENGTH_127+1 );
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gSHEventHandle,PATTERN_THR4,OSAL_EN_EVENTMASK_OR ) );
}

TEST(OsalCoreShMemTest, u32SHMemoryThreadAccess)
{
   OSAL_trThreadAttribute trAtr_1  = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_2  = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_3  = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_4  = {OSAL_NULL};
   /*Shared memory Pointer*/
   tVoid * shptr                   = OSAL_NULL;
   gevMask = 0;
   trAtr_1.szName                 	=  (char*)"Thread_SH_0_127";
   trAtr_1.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_1.s32StackSize           	=  VALID_STACK_SIZE;
   trAtr_1.pfEntry                	=  Thread_SH_0_127;
   trAtr_1.pvArg                  	=  NULL;
   trAtr_2.szName                 	=  (char*)"Thread_SH_128_255";
   trAtr_2.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_2.s32StackSize           	=  VALID_STACK_SIZE;
   trAtr_2.pfEntry                	=  Thread_SH_128_255;
   trAtr_2.pvArg                  	=  NULL;
   trAtr_3.szName                 	=  (char*)"Thread_SH_256_383";
   trAtr_3.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_3.s32StackSize           	=  VALID_STACK_SIZE;
   trAtr_3.pfEntry                	=  Thread_SH_256_383;
   trAtr_3.pvArg                  	=  NULL;
   trAtr_4.szName                 	=  (char*)"Thread_SH_384_511";
   trAtr_4.u32Priority            	=  OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_4.s32StackSize           	=  VALID_STACK_SIZE;
   trAtr_4.pfEntry                	=  Thread_SH_384_511;
   trAtr_4.pvArg                  	=  NULL;
								 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate( "SH_MEM_EVENT",&gSHEventHandle ) );
   EXPECT_NE(OSAL_ERROR,( ghShared = OSAL_SharedMemoryCreate( SH_NAME,OSAL_EN_READWRITE,SH_MEMSIZE_512 ) ) );
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_1 ) );
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_2 ) );
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_3 ) );
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn( &trAtr_4 ) );

   /*4 THREAD EVENT WAIT ( 1,2,3,4 Passed )*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(gSHEventHandle, PATTERN_THR1|PATTERN_THR2|PATTERN_THR3|PATTERN_THR4,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gSHEventHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));
    
   /*Check for data in shared memory*/
   EXPECT_NE(0 ,(uintptr_t)(shptr = OSAL_pvSharedMemoryMap( ghShared,OSAL_EN_READWRITE,( SH_MEMSIZE_512-1 ),OFFSET_0 )));
   EXPECT_EQ( *((char*)shptr + 0 ) ,  'a' );
   EXPECT_EQ( *((char*)shptr + 127 ) ,'a' );
   EXPECT_EQ( *((char*)shptr + 128 ) ,'b' );
   EXPECT_EQ( *((char*)shptr + 255 ) ,'b' );
   EXPECT_EQ( *((char*)shptr + 256 ) ,'c' );
   EXPECT_EQ( *((char*)shptr + 383 ) ,'c' );
   EXPECT_EQ( *((char*)shptr + 384 ) ,'d' );
   EXPECT_EQ( *((char*)shptr + 510 ) ,'d' );

   /*Close and Delete the Event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( gSHEventHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( "SH_MEM_EVENT"));

   /*Close and Delete the Shared Memory*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryClose(ghShared));
   EXPECT_EQ(OSAL_OK,OSAL_s32SharedMemoryDelete(SH_NAME));
   /*Reinitialize the Global Variables*/
   gSHEventHandle = 0;
   ghShared       = 0;	
}


/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
