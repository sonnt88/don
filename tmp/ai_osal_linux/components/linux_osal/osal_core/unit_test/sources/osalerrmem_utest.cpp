/*****************************************************************************
| FILE:         osalerrmem_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal error memory implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2015 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "osal_core_unit_test_mock.h"

using namespace std;
 

#define REG_INVAL_PARAM                             OSAL_C_INVALID_HANDLE            /* dont use any number , it must be OSAL_C_... */
#define OEDT_REGISTRY_INVALID_ACCESS_MODE                            25                      //RBIN

/*********************************************************************/
/* Test Cases for OSAL Error Memory Device                           */
/*********************************************************************/
TEST(OsalCoreErrmemTest, ErrmemDevOpenClose)
{
   OSAL_tIODescriptor hDevice = 0;
   /* Open the device in read-write mode */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM ,OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
}


TEST(OsalCoreErrmemTest, ErrmemDevOpenCloseDiffModes)
{
   OSAL_tIODescriptor hDevice = 0;
   /* Open the device in read-write mode */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM ,OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
   /* Open the device in read-only mode */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM ,OSAL_EN_READONLY)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
   /* Open the device in read-only mode */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM ,OSAL_EN_WRITEONLY)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
}
   
TEST(OsalCoreErrmemTest, ErrmemDevOpenCloseInvalParam)
{
   OSAL_tIODescriptor hDevice = 0;
   EXPECT_EQ(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM ,(OSAL_tenAccess)OEDT_REGISTRY_INVALID_ACCESS_MODE )));
   /* Close device with invalid parameter */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose( (OSAL_tIODescriptor)REG_INVAL_PARAM));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hDevice));
}

TEST(OsalCoreErrmemTest, ErrmemDevCloseAlreadyClosed)
{
   OSAL_tIODescriptor hDevice = 0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE));
   /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
   /* Close device already closed */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hDevice));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR,OSAL_u32ErrorCode());
}


TEST(OsalCoreErrmemTest, ErrmemDevClearErrmem)
{
   OSAL_tIODescriptor hDevice = 0;
   intptr_t val=0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE)));
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR, (intptr_t)(&val)));
    /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
}


TEST(OsalCoreErrmemTest, ErrmemDevWriteReadErrmem)
{
   OSAL_tIODescriptor hDevice = 0;
   tS32 s32Size = 0;
   trErrmemEntry  rWriteEntry = {0};
   trErrmemEntry  rReadEntry = {0};
   rWriteEntry.u16Entry = TR_COMP_OSALCORE;
   (tVoid) OSAL_s32ClockGetTime(&rWriteEntry.rEntryTime);
   rWriteEntry.eEntryType = eErrmemEntryNormal;
   rWriteEntry.u16EntryLength = strlen("Test for Error Memory \n");
   (tVoid) OSAL_pvMemoryCopy(&rWriteEntry.au8EntryData[0],"Test for Error Memory \n",strlen("Test for Error Memory \n"));

   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE)));
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_SET_BE,(uintptr_t) &s32Size));
#endif
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,(uintptr_t)&s32Size));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
 
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE)));
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOWrite(hDevice, (tPS8)&rWriteEntry, sizeof(rWriteEntry)));
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_ERRMEM_FLUSH,(uintptr_t)&s32Size));
   OSAL_s32ThreadWait(500);
#endif   
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE)));
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   rReadEntry.u16EntryLength = 1;
   while(rReadEntry.u16EntryLength != rWriteEntry.u16EntryLength )
   {
      EXPECT_EQ(sizeof(rReadEntry), s32Size = OSAL_s32IORead(hDevice, (tPS8)(&rReadEntry), sizeof(rReadEntry)));
   //   TraceString((char*)&rReadEntry.au8EntryData[0]);
      if(s32Size == OSAL_ERROR)break;
   }
#endif
   
   EXPECT_EQ(OSAL_OK,strncmp((char*)&rWriteEntry.au8EntryData[0],(char*)&rReadEntry.au8EntryData[0],rReadEntry.u16EntryLength));
   //TraceString((char*)&rWriteEntry.au8EntryData[0]);
   //TraceString((char*)&rReadEntry.au8EntryData[0]);
   /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
}

TEST(OsalCoreErrmemTest, ErrmemDevClearReadErrmem)
{
   OSAL_tIODescriptor hDevice = 0;
   intptr_t val=0;
   trErrmemEntry  rErrmemEntry = {0};
   rErrmemEntry.u16Entry = TR_COMP_OSALCORE;
   (tVoid) OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime);
   rErrmemEntry.eEntryType = eErrmemEntryNormal;
   rErrmemEntry.u16EntryLength = strlen("Test for Error Memory \n");
   (tVoid) OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[0],"Test for Error Memory \n",strlen("Test for Error Memory \n"));

   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,(hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ERRMEM,OSAL_EN_READWRITE)));

   EXPECT_NE(OSAL_ERROR,OSAL_s32IOControl (hDevice, OSAL_C_S32_IOCTRL_ERRMEM_CLEAR,(intptr_t)(&val))); 
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IORead( hDevice, (tPS8)&rErrmemEntry, sizeof(rErrmemEntry)));
#else
   while(1)
   {
	 if(OSAL_s32IORead( hDevice, (tPS8)&rErrmemEntry, sizeof(rErrmemEntry)) == OSAL_ERROR)break;
	 if(rErrmemEntry.u16EntryLength == 1)
	 {
		 TraceString("ErrMem received ID:%d",rErrmemEntry.au8EntryData[0]);
	 }
	 else
	 {
		 //break;
	 }
   }   
#endif
   EXPECT_EQ(OSAL_E_IOERROR,OSAL_u32ErrorCode());
   /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hDevice));
}


/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
