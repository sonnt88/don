/*****************************************************************************
| FILE:         osalevent_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for osal event implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"


#include "osal_core_unit_test_mock.h"

using namespace std;

#define EVENT_NAME 		 "EventTest"
#define EVENT_NAME_GLOBAL        "GlobalEvent"
#ifdef OSAL_SHM_MQ
#define EVENT_NAME_MAX 		     "Event named to the max length  33"
#else
#define EVENT_NAME_MAX 		     "Event named to the max length31"
#endif
#define EVENT_NAME_EXCEEDS_MAX   "Event named to the length greater than max"
#define OEDT_EVENT_DWC "OEDT_EVENT_DELETE_WITHOUT_CLOSE"

#define MUTEX                    "Mutex"
#define MAX_32BIT                0xFFFFFFFF

#define INIT_VAL                 0x00000000
#define INITIAL_EVENT_FIELD      0x00000000
#define EVENT_MASK_PATTERN       0x00000001
#define THREAD_POST_PATTERN      0x00000001 
#define THREAD_IN_USE            0x00101010
#undef  ONE_SECOND 
#define ONE_SECOND               1000
#undef  TWO_SECONDS    
#define TWO_SECONDS              2000
#undef  THREE_SECONDS
#define THREE_SECONDS            3000
#undef  FIVE_SECONDS
#define FIVE_SECONDS             5000
#define TWENTY_MILLISECONDS      20
#undef  THIRTY_MILLISECONDS
#define THIRTY_MILLISECONDS      30
#define OEDT_FFS1_EVENTTEXT				 OSAL_C_STRING_DEVICE_FFS1"/eventtxt.txt"
#define THREAD_ONE_EVENT         0x00000001
#define THREAD_TWO_EVENT         0x00000002
#define THREAD_THREE_EVENT       0x00000004
#define THREAD_FINAL_EVENT       0x00000008
#define TERMINATE_THREADS	     0x00000010

#define MAX_PATTERNS             (tU32)32
#define PATTERN_1              	 0x00000001	/*1*/
#define PATTERN_2              	 0x00000002	/*2*/
#define PATTERN_4             	 0x00000004	/*3*/
#define PATTERN_8            	 0x00000008	/*4*/
#define PATTERN_16          	 0x00000010	/*5*/
#define	PATTERN_32        		 0x00000020	/*6*/
#define PATTERN_64        		 0x00000040	/*7*/
#define PATTERN_128   			 0x00000080	/*8*/
#define PATTERN_256   			 0x00000100	/*9*/
#define PATTERN_512   			 0x00000200	/*10*/
#define PATTERN_1024   			 0x00000400	/*11*/
#define PATTERN_2048   			 0x00000800	/*12*/
#define PATTERN_4096   			 0x00001000	/*13*/
#define PATTERN_8192   			 0x00002000	/*14*/
#define PATTERN_16384   	     0x00004000	/*15*/
#define PATTERN_32768   		 0x00008000	/*16*/
#define PATTERN_65536   		 0x00010000	/*17*/
#define PATTERN_131072			 0x00020000 /*18*/
#define PATTERN_262144			 0x00040000 /*19*/
#define PATTERN_524288			 0x00080000 /*20*/
#define PATTERN_1048576			 0x00100000	/*21*/
#define PATTERN_2097152          0x00200000	/*22*/
#define PATTERN_4194304          0x00400000	/*23*/
#define PATTERN_8388608          0x00800000	/*24*/
#define PATTERN_16777216		 0x01000000	/*25*/
#define PATTERN_33554432         0x02000000	/*26*/
#define PATTERN_67108864		 0x04000000	/*27*/
#define PATTERN_134217728        0x08000000	/*28*/
#define PATTERN_268435456        0x10000000	/*29*/
#define PATTERN_536870912        0x20000000	/*30*/
#define PATTERN_1073741824		 0x40000000	/*31*/
#define PATTERN_2147483648       0x80000000	/*32*/
#define REPLACE_PATTERN          0x55555555

//#define RAND_MAX                 0x7FFFFFFF
#define MAX_EVENTS               30
#undef  MAX_LENGTH
#define MAX_LENGTH               256
#undef  DEBUG_MODE 
/*rav8kor - disable debug mode*/
#define DEBUG_MODE               0

#undef  NO_OF_POST_THREADS
#define NO_OF_POST_THREADS       10
#undef  NO_OF_WAIT_THREADS
#define NO_OF_WAIT_THREADS       15
#undef  NO_OF_CLOSE_THREADS
#define NO_OF_CLOSE_THREADS      1
#undef  NO_OF_CREATE_THREADS
#define NO_OF_CREATE_THREADS     2
#undef  TOT_THREADS
#define TOT_THREADS              (NO_OF_POST_THREADS+NO_OF_WAIT_THREADS+NO_OF_CLOSE_THREADS+NO_OF_CREATE_THREADS)

typedef struct EventTableEntry_
{
	OSAL_tEventHandle hEve;
	tChar hEventName[MAX_LENGTH];
}EventTableEntry;




/***************************************************************************************
|Global Data 
|--------------------------------------------------------------------------------------*/
static OSAL_tEventHandle  gevHandle        = 0;
static OSAL_tEventHandle  gevHandle_       = 0;
static OSAL_tEventMask    gevMask          = 0;
static OSAL_tIODescriptor gRFDescriptor    = 0; /*RF - Ramdisk File*/

static OSAL_tSemHandle baseSemHan                                        = 0;

tVoid WaitPostThread( const tVoid *pArg ) 
{
	/*Thread Action*/
	/*Wait for a while, to stress OSAL_s32EventWait*/
	OSAL_s32ThreadWait( ONE_SECOND );

	EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(   *( OSAL_tEventHandle* )pArg ,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_OR));

	/*Wait for the main thread to terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}	

/*****************************************************************************
* FUNCTION    :	   ThreadWaitPostTest
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Thread to test the Wait and Post event mechanisms
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tVoid ThreadWaitPostTest( tVoid *pArg )
{
	/*Thread Action*/
	/*Wait for a while, to stress OSAL_s32EventWait*/
	OSAL_s32ThreadWait( TWO_SECONDS );

	/*Thread Action*/
	EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle ,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_OR));
	
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_One
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Creates file in Ramdisk, supervises the thread which does the 
                   file write operation and the file close, delete operations
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
* 					Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tVoid Thread_One( tVoid *pArg )
{
      
   /*Create a file in Ramdisk*/
   gRFDescriptor = OSAL_IOCreate( OEDT_FFS1_EVENTTEXT,OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR,gRFDescriptor);

   /*Wait for two seconds to confirm all the threads are waiting*/
   OSAL_s32ThreadWait( TWO_SECONDS );

   /*Post an event pattern to trigger Thread 2*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,THREAD_TWO_EVENT,OSAL_EN_EVENTMASK_OR));
		 
   /*Wait for a response event from Thread 2*/
   EXPECT_EQ(OSAL_OK, OSAL_s32EventWait(gevHandle_,THREAD_ONE_EVENT,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

   /*Post an event pattern to trigger Thread 3*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,THREAD_THREE_EVENT, OSAL_EN_EVENTMASK_OR));
   /*Wait for a response event from Main Thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
					OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER, &gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

   /*Post the init pattern*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR));

   /*Wait and terminate*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Two
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Does the file write operation,communicates with the thread
                   which creates the file with the help of event mechanism
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
*******************************************************************************/
tVoid Thread_Two( tVoid *pArg )
{
   tChar t2Buffer[10];
   tU8 u8Count = 10;
   /*Initialize the buffer*/
   memset( t2Buffer,0,10 );
   (tVoid)OSAL_szStringCopy( t2Buffer,"ThreadThr" );
   /*Wait for a signal from Thread 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,THREAD_TWO_EVENT,OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&gevMask));

   /*Write into Ramdisk file*/
   while( u8Count-- )
   {
      OSAL_s32IOWrite( gRFDescriptor,(tPCS8)t2Buffer,9 );
   }

	/*Post an event pattern to trigger Thread 1*/
	EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,THREAD_ONE_EVENT,OSAL_EN_EVENTMASK_OR));

	/*Wait for a response event from Main Thread*/
	EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
					   OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));
	/*Clear the event*/
	EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

	/*Post the init pattern*/
	EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR));

	/*Wait and terminate*/
	OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Three
* PARAMETER   :    Pointer to void
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Does the file close and delete operations,communicates with
                   the main thread with the help of event mechanism
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Oct 26,2007
* 						Update by Anoop Chandran(RBIN/EDI3) on 29 Oct, 2008
*******************************************************************************/
tVoid Thread_Three( tVoid *pArg )
{
   /*Wait for a signal from Thread 1*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,THREAD_THREE_EVENT,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));

   /*Close the file handle*/
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose( gRFDescriptor));

   /*Remove the file from Ramdisk*/
   EXPECT_NE(OSAL_ERROR,OSAL_s32IORemove( OEDT_FFS1_EVENTTEXT));

   /*Post an event pattern to trigger MainThread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT, OSAL_EN_EVENTMASK_OR));

   /*Wait for a response event from Main Thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,
   					 OSAL_EN_EVENTMASK_AND, OSAL_C_TIMEOUT_FOREVER, &gevMask));

   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask), OSAL_EN_EVENTMASK_AND));

   /*Post the init pattern*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_OR));

   /*Wait and terminate*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   Thread_Del
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Waits for a pattern
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid Thread_Del(const tVoid *pvArg )
{
   /*Wait for Pattern*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait( *( OSAL_tEventHandle* )pvArg,THREAD_IN_USE,OSAL_EN_EVENTMASK_AND,
					OSAL_C_TIMEOUT_FOREVER,	OSAL_NULL ));

   /*Thread Wait*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   XOR_Thread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with XOR Flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid XOR_Thread(const tVoid * ptr )
{
	int n;
	tU32 u32MaskPattern = 0x00000001;

	OSAL_s32ThreadWait( TWO_SECONDS );

	/*Loop Post 32 Times, 32 bit patterns*/
	for (n = 0; n < 32; n++)
	{
        OSAL_s32ThreadWait( 0 );
		/*Post the pattern to the event Field*/
		EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr, u32MaskPattern,OSAL_EN_EVENTMASK_XOR));
         	/*Update the Mask Pattern*/
	 	u32MaskPattern = u32MaskPattern<<1;
	}

	/*Wait for Termination from the Spawning Process*/
	OSAL_s32ThreadWait( TWO_SECONDS );

}

/*****************************************************************************
* FUNCTION    :	   Replace_Thread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with Replace Flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid Replace_Thread(const tVoid * ptr )
{
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,REPLACE_PATTERN,OSAL_EN_EVENTMASK_REPLACE ));
   /*Wait for Termination from the Spawning Process*/
   OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   ORWaitThread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts Mask Pattern with OR flag to update the internal
                   event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid ORWaitThread(const tVoid * ptr )
{
   tU32 u32MaskPattern = 0x00000001;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,u32MaskPattern,OSAL_EN_EVENTMASK_OR ));
   u32MaskPattern  = u32MaskPattern<<1;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,u32MaskPattern,OSAL_EN_EVENTMASK_OR));		
   u32MaskPattern  = u32MaskPattern<<1;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,u32MaskPattern,OSAL_EN_EVENTMASK_OR ));

   /*Post on Semaphore to signal main task*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphorePost( baseSemHan));

  /*Wait for Termination from the Spawning Process*/
  OSAL_s32ThreadWait( TWO_SECONDS );
}

/*****************************************************************************
* FUNCTION    :	   NoBlockingThread
* PARAMETER   :    Pointer to void( contains start address of Event Handle )
* RETURNVALUE :    tU32, "0" on success  or "non-zero" value in case of error
* DESCRIPTION :    Posts the pattern EVENT_MASK_PATTERN to the event field
* HISTORY     :	   Created by Tinoy Mathews(RBIN/EDI3)  Jan 4,2008
*******************************************************************************/
tVoid NoBlockingThread(const tVoid * ptr )
{
   /*Post the pattern EVENT_MASK_PATTERN*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( *( OSAL_tEventHandle* )ptr,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_OR ) );
   /*Wait for some time*/
   OSAL_s32ThreadWait( THREE_SECONDS );
}


TEST(OsalCoreEventTest, OsalEventOpen_invalidName_error_OSAL_E_UNKNOWN)
{
  OSAL_tEventHandle hEvent;
  tS32 ret = OSAL_s32EventOpen("EventTestEvent", &hEvent);
  EXPECT_EQ(OSAL_ERROR, ret);

}

TEST(OsalCoreEventTest, u32CreateEventNameNULL)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventCreate(OSAL_NULL,&evHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32CreateEventNameMAX)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME_MAX,&evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME_MAX));
}


TEST(OsalCoreEventTest, u32CreateEventNameExceedsMAX)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventCreate(EVENT_NAME_EXCEEDS_MAX,&evHandle));
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32CreateTwoEventsSameName)
{
   OSAL_tEventHandle evHandle  = 0;
   OSAL_tEventHandle evHandle_	= 0;
   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventCreate(EVENT_NAME,&evHandle_));
   EXPECT_EQ(OSAL_E_ALREADYEXISTS,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32CreateEventRegularName)
{
   OSAL_tEventHandle evHandle  = 0;
   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32DelEventHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventDelete(OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32DelEventNonExisting)
{
   OSAL_tEventHandle evHandle  = 0;
   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventDelete(EVENT_NAME));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32DelEventNameExceedsMAX)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventDelete( EVENT_NAME_EXCEEDS_MAX));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}


TEST(OsalCoreEventTest, u32DelEventInUse)
{
   OSAL_tEventHandle evHandle  = 0;
   OSAL_trThreadAttribute trAtr = {OSAL_NULL};

   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
		
   trAtr.szName       = (char*)"Thread_Del_In_Use";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)Thread_Del;
   trAtr.pvArg        = &evHandle;
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAtr));

   OSAL_s32ThreadWait( ONE_SECOND );
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
   /*Through an Event Close remove the event related resources from the system - so since the event delete was done*/
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose( evHandle ));
   /*Attempt to Reclose the Event, to check if the Event field has actually been removed from the system*/
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventClose( evHandle )); 
  // EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME)); /* needed for event based implementation*/

   OSAL_s32ThreadWait( ONE_SECOND );
}

TEST(OsalCoreEventTest, u32OpenEventExisting)
{
   OSAL_tEventHandle evHandle  = 0;

   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventOpen(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32OpenEventNameNULL)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventOpen(OSAL_NULL,&evHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32OpenEventHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventOpen(EVENT_NAME,OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}


TEST(OsalCoreEventTest, u32OpenEventNonExisting)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventHandle evHandle_   = 0;
   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventOpen(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());


   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventOpen( "UnknownEvent",&evHandle_ ));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32OpenEventNameExceedsMAX)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventOpen(EVENT_NAME_EXCEEDS_MAX,&evHandle));
   EXPECT_EQ(OSAL_E_NAMETOOLONG,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32CloseEventHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventClose(OSAL_NULL));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32CloseEventHandleInval)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32GetEventStatusHandleValid)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventMask   evMask   = MAX_32BIT;
   OSAL_tEventMask   evMask_  = 0;

   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventStatus( evHandle,evMask,&evMask_));
   EXPECT_EQ(INITIAL_EVENT_FIELD,evMask_ );
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32GetEventStatusHandleNULL)
{
   OSAL_tEventMask   evMask   = MAX_32BIT;
   OSAL_tEventMask   evMask_  = 0;

   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventStatus( OSAL_NULL,evMask,&evMask_));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32GetEventStatusHandleInval)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventMask   evMask   = MAX_32BIT;
   OSAL_tEventMask   evMask_  = 0;

   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventStatus(evHandle,evMask,&evMask_));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32PostEventHandleNULL)
{
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventPost(OSAL_NULL,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32PostEventHandleInval)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventPost(evHandle,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32WaitAndPostEventVal)
{
   OSAL_tEventMask EM            = 0;
   OSAL_tEventMask	evMask        = MAX_32BIT;
   OSAL_tEventMask evMask_       = MAX_32BIT;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};

   EXPECT_EQ(OSAL_OK, OSAL_s32EventCreate(EVENT_NAME_GLOBAL, &gevHandle));

   trAtr.szName       = (char*)"EventWaitPostThread";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)ThreadWaitPostTest;
   trAtr.pvArg        = NULL;
		
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventWait(gevHandle,THREAD_POST_PATTERN, OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(gevHandle,evMask,&evMask_ ));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(gevHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME_GLOBAL));
}

TEST(OsalCoreEventTest, u32WaitEventHandleNULL)
{
   OSAL_tEventMask   EM  = 0;

   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventWait(OSAL_NULL,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32WaitEventHandleInval)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventMask   EM  = 0;

   EXPECT_EQ(OSAL_ERROR, OSAL_s32EventWait(evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&EM));
   EXPECT_EQ(OSAL_E_INVALIDVALUE,OSAL_u32ErrorCode());
}

TEST(OsalCoreEventTest, u32WaitEventWithoutTimeout)
{
   OSAL_tEventMask EM            = 0;		
   OSAL_tEventHandle evHandle    = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_NOBLOCKING,&EM));
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32WaitEventWithTimeout)
{
   OSAL_tEventMask EM            = 0;		
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tMSecond   u32StartTime  = 0;
   OSAL_tMSecond   u32EndTime  = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   u32StartTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,TWENTY_MILLISECONDS,&EM));
   u32EndTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

   EXPECT_GE(( u32EndTime - u32StartTime ),TWENTY_MILLISECONDS);

   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}


TEST(OsalCoreEventTest, u32PostWaitEvent)
{
   OSAL_tEventMask EM            = 0;		
   OSAL_tEventHandle evHandle    = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_OR));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,1000,&EM));
 
   EXPECT_EQ(OSAL_OK, OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK, OSAL_s32EventDelete(EVENT_NAME));
}


#ifdef CHECK_TEST
TEST(OsalCoreEventTest, u32SyncEventMultipleThreads)
{
   OSAL_trThreadAttribute trAtr_1 = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_2 = {OSAL_NULL};
   OSAL_trThreadAttribute trAtr_3 = {OSAL_NULL};

   gevMask       = 0;
   gRFDescriptor = 0;		
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate( EVENT_NAME,&gevHandle_));

   trAtr_1.szName       = (char*)"Thread_one";/*lint !e1773 */  /*otherwise linker warning */
   trAtr_1.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_1.s32StackSize = 4096;
   trAtr_1.pfEntry      = (OSAL_tpfThreadEntry)Thread_One;
   trAtr_1.pvArg        = NULL;
   trAtr_2.szName       = (char*)"Thread_two";/*lint !e1773 */  /*otherwise linker warning */
   trAtr_2.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_2.s32StackSize = 4096;
   trAtr_2.pfEntry      = (OSAL_tpfThreadEntry)Thread_Two;
   trAtr_2.pvArg        = NULL;
   trAtr_3.szName       = (char*)"Thread_three";/*lint !e1773 */  /*otherwise linker warning */
   trAtr_3.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr_3.s32StackSize = 4096;
   trAtr_3.pfEntry      = (OSAL_tpfThreadEntry)Thread_Three;
   trAtr_3.pvArg        = NULL;

   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr_1));
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr_2));
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr_3));

   OSAL_s32ThreadWait( ONE_SECOND );

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT,
             OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

   OSAL_s32ThreadWait( ONE_SECOND );
					
   /*Post the Thread terminate event pattern,signal to all threads to wait for termination from the main thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,OSAL_EN_EVENTMASK_OR));

   /*Wait on INIT EVENT PATTERN*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(gevHandle_,INITIAL_EVENT_FIELD, OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));
   /*Post the Thread terminate event pattern,signal to all threads  to wait for termination from the main thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,OSAL_EN_EVENTMASK_OR));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask)); 
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

   /*Post the Thread terminate event pattern,signal to all threads to wait for termination from the main thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost( gevHandle_,THREAD_ONE_EVENT|THREAD_TWO_EVENT|THREAD_THREE_EVENT|THREAD_FINAL_EVENT|TERMINATE_THREADS,OSAL_EN_EVENTMASK_OR));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( gevHandle_,INITIAL_EVENT_FIELD,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(gevHandle_,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( gevHandle_));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( EVENT_NAME));
   OSAL_s32ThreadWait( ONE_SECOND );
}
#endif

TEST(OsalCoreEventTest, u32XORFlagPostEvent)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventMask   evMask      = MAX_32BIT;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
   gevMask = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   
   trAtr.szName       = (char*)"XOR_Thread";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)XOR_Thread;
   trAtr.pvArg        = &evHandle;	 

   /*Create the Thread*/
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAtr));

   /*Wait until all 32 patterns are obtained*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,evMask,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32ReplaceFlagPostEvent)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
   gevMask = 0;
   
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));

   trAtr.szName       = (char*)"Replace_Thread";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)Replace_Thread;
   trAtr.pvArg        = &evHandle;	 
	
	/*Create the Thread*/
   EXPECT_NE(OSAL_ERROR ,OSAL_ThreadSpawn(&trAtr));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,REPLACE_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32ORFlagWaitEvent)
{
   OSAL_tEventHandle evHandle    = 0;
   OSAL_tEventMask   evMask      = 0x00000001;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
   tU32 u32eveMask               = MAX_32BIT;
   tU32 u32eveMask_			  = MAX_32BIT;
   
   gevMask = 0;
   /*Create a Global Semaphore*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreCreate("GLOBAL_SEM",&baseSemHan,0));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate( EVENT_NAME,&evHandle));
   
   trAtr.szName       = (char*)"ORWaitTest";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)ORWaitThread;
   trAtr.pvArg        = &evHandle;

   /*Create the Thread*/
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));
   
   /*Wait on Semaphore until posted by the thread*/
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreWait( baseSemHan,OSAL_C_TIMEOUT_FOREVER)); 

   /*Wait with OR flag option is obtained*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,evMask,OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   /*Check the event flag status*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus( evHandle,u32eveMask,&u32eveMask_));
   EXPECT_EQ(0x00000006,u32eveMask_);

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));

   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreClose(baseSemHan));
   EXPECT_EQ(OSAL_OK,OSAL_s32SemaphoreDelete("GLOBAL_SEM"));
}


TEST(OsalCoreEventTest, u32NoBlockingEventWait)
{
   tU32 u32eveMask               = MAX_32BIT;
   tU32 u32eveMask_              = MAX_32BIT;
   OSAL_tEventHandle evHandle    = 0;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};

   gevMask = 0;

   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));
   /*Scenario 1*/
   /*Fill in thread attributes*/
   trAtr.szName       = (char*)"NoBlockingWaitTest";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)NoBlockingThread;
   trAtr.pvArg        = &evHandle;

   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));
   
		
   /*Wait until the pattern in the event field is
     EVENT_MASK_PATTERN.This is because as the subsequent Wait call
     has the OSAL_C_TIMEOUT_NOBLOCKING, it will not wait for any pattern*/
   do
   {
      /*Wait*/
      OSAL_s32ThreadWait( 30 );
      /*Get Status*/
      EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus( evHandle,u32eveMask,&u32eveMask_));
    }while( EVENT_MASK_PATTERN != u32eveMask_ );
		
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,EVENT_MASK_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_NOBLOCKING,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   /*Scenario 1*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait( evHandle,PATTERN_2048,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_NOBLOCKING,&gevMask));
   /*Failure is an expected behaviour*/	
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());

   /*Scenario 2*/
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
}

TEST(OsalCoreEventTest, u32BlockingDurationEventWait)
{
   OSAL_tEventHandle evHandle = 0;
   OSAL_tMSecond u32StartTime = 0;
   OSAL_tMSecond u32EndTime   = 0;
   OSAL_tMSecond u32Duration  = 0;

   gevMask = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));

   u32StartTime = OSAL_ClockGetElapsedTime();
   /*Do a Idle Wait for an Event Pattern*/
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait(evHandle,PATTERN_2048,OSAL_EN_EVENTMASK_AND,ONE_SECOND,&gevMask));
   u32EndTime = OSAL_ClockGetElapsedTime();

   /*Update Error Code*/
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
   /*Calculate Duration*/
   u32Duration = u32EndTime - u32StartTime;
   /*Do Duration Test only if the error code is OSAL_E_TIMEOUT and EndTime > StartTime*/
   EXPECT_LE((ONE_SECOND - THIRTY_MILLISECONDS),u32Duration);
   EXPECT_LE(u32Duration,(ONE_SECOND + THIRTY_MILLISECONDS));
		
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
}


TEST(OsalCoreEventTest, u32WaitPostEventAfterDelete)
{
   OSAL_tEventHandle evHandle 	  = 0;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};

   gevMask = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));

   /*Delete the Event before close*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
   /*Fill in the Thread Fields*/
   trAtr.szName       = (char*)"OperateAfterDel";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)WaitPostThread;
   trAtr.pvArg        = &evHandle;	

		/*Spawn the Thread*/
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   /*Clear the event*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
}


TEST(OsalCoreEventTest, u32EventDeletewithoutclose)
{
   OSAL_tEventHandle evHandle    = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(OEDT_EVENT_DWC, &evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( OEDT_EVENT_DWC));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( evHandle));
   
   /*Try to create the event again with same name*/
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(OEDT_EVENT_DWC,&evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( OEDT_EVENT_DWC));
}


void PostDelayedThread(void* pArg)
{
   OSAL_tEventHandle evHandle 	  = 0;
   uintptr_t u32Timeout = (uintptr_t)(pArg);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(EVENT_NAME,&evHandle));
   OSAL_s32ThreadWait(u32Timeout+1000);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
}

TEST(OsalCoreEventTest, u32PostEventAfterTimeout)
{
   OSAL_tEventHandle evHandle 	  = 0;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
   OSAL_tMSecond u32StartTime = 0;
   OSAL_tMSecond u32EndTime   = 0;
   uintptr_t u32Timeout = 2000;
   gevMask = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));

   trAtr.szName       = (char*)"PostDelayed";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)PostDelayedThread;
   trAtr.pvArg        = (void*)u32Timeout;	
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));

   u32StartTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,u32Timeout,&gevMask));
   u32EndTime = OSAL_ClockGetElapsedTime();
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
   EXPECT_GE((u32EndTime-u32StartTime),2000);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait( evHandle,THREAD_POST_PATTERN,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(gevMask),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
   OSAL_s32ThreadWait(u32Timeout+1000);
}


TEST(OsalCoreEventTest, u32EventOption)
{
   OSAL_tEventHandle evHandle    = 0;
   gevMask = 0;
   tU32 Mask = 0;
 
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreateOpt("EventConsume", &evHandle,CONSUME_EVENT));
  
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x1010,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x1010,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(evHandle,0,&Mask));
   EXPECT_EQ(Mask,0);
 
   EXPECT_EQ(OSAL_ERROR,OSAL_s32EventWait(evHandle,0x1010,OSAL_EN_EVENTMASK_AND,2000,&gevMask));
   EXPECT_EQ(OSAL_E_TIMEOUT,OSAL_u32ErrorCode());
  

   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x1111,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x1000,OSAL_EN_EVENTMASK_OR,1000,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(evHandle,0x111,&Mask));
   EXPECT_EQ(Mask,0x111);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x100,OSAL_EN_EVENTMASK_OR,1000,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(evHandle,0x11,&Mask));
   EXPECT_EQ(Mask,0x11);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x10,OSAL_EN_EVENTMASK_OR,1000,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(evHandle,0x1,&Mask));
   EXPECT_EQ(Mask,1);
   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x1,OSAL_EN_EVENTMASK_OR,1000,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventStatus(evHandle,0,&Mask));
   EXPECT_EQ(Mask,0);

  
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose( evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete( "EventConsume"));
}


void PostStressThread(void* pArg)
{
   OSAL_tEventHandle evHandle 	  = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventOpen(EVENT_NAME,&evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00000001,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00000010,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00000100,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00001000,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00010000,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x00100000,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x01000000,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,0x10000000,OSAL_EN_EVENTMASK_OR));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
}

TEST(OsalCoreEventTest, u32PostEventStress)
{
   OSAL_tEventHandle evHandle 	  = 0;
   OSAL_trThreadAttribute trAtr  = {OSAL_NULL};
   uintptr_t u32Timeout = 2000;
   gevMask = 0;
   EXPECT_EQ(OSAL_OK,OSAL_s32EventCreate(EVENT_NAME,&evHandle));

   trAtr.szName       = (char*)"PostStress";/*lint !e1773 */  /*otherwise linker warning */
   trAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   trAtr.s32StackSize = 4096;
   trAtr.pfEntry      = (OSAL_tpfThreadEntry)PostStressThread;
   trAtr.pvArg        = (void*)u32Timeout;	
   EXPECT_NE(OSAL_ERROR,OSAL_ThreadSpawn(&trAtr));
   OSAL_s32ThreadWait(200);

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x00001111,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(0x00001111),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventWait(evHandle,0x11110000,OSAL_EN_EVENTMASK_AND,OSAL_C_TIMEOUT_FOREVER,&gevMask));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventPost(evHandle,~(0x11110000),OSAL_EN_EVENTMASK_AND));

   EXPECT_EQ(OSAL_OK,OSAL_s32EventClose(evHandle));
   EXPECT_EQ(OSAL_OK,OSAL_s32EventDelete(EVENT_NAME));
}

#ifdef STRESSTEST
/******************************************************************************/
/*STRESS TEST EVENT OSAL API*/
/******************************************************************************/

/*****************************************************************************
* FUNCTION    :	   s32RandomNoGenerate()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Get a  Random Number
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tS32 s32RandomNoGenerate( tVoid )
{
	/*Declarations*/
	tS32 s32Rand = 0x7FFFFFFF;

	/*Until Positive Number*/
	do
	{
		/*Random Number Generate*/
		s32Rand = OSAL_s32Random( );
	}while( s32Rand < 0 );
	
	/*Return the Positive Random Number*/
	return s32Rand;
}	

/*****************************************************************************
* FUNCTION    :	   vChangeThreadPrioRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Change Thread Priorities Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid vChangeThreadPrioRandom( tVoid )
{
	/*Declarations*/
	tU32 u32RandPriority;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Find true priority*/
	u32RandPriority = (tU32)( fRand*(tDouble)OSAL_C_U32_THREAD_PRIORITY_NORMAL )+\
					  OSAL_C_U32_THREAD_PRIORITY_NORMAL;

	/*Assign the Priority to the current thread*/
	OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI( ),u32RandPriority );
}

/*****************************************************************************
* FUNCTION    :	   vWaitRandomTime()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Wait for Random time frame
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid vWaitRandomTime( tU32 u32TimeModulus )
{
	/*Declarations*/
	tU32 u32ActualWait;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand          = (tDouble)s32Rand/(tDouble)RAND_MAX;
	/*Calculate the time for wait*/
	u32ActualWait  = (tU32)( fRand*(tDouble)u32TimeModulus +0.5);
	/*Wait random*/
	OSAL_s32ThreadWait( u32ActualWait );
}

/*****************************************************************************
* FUNCTION    :	   u8SelectEventIndexRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Indexes Randomly
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tU8 u8SelectEventIndexRandom( tVoid )
{
	/*Declarations*/
	tU8  u8Index;
	tS32 s32Rand = RAND_MAX;
	tDouble fRand;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Calculate random fraction*/
	fRand           = (tDouble)s32Rand/(tDouble)RAND_MAX; 
	/*Calculate the Event Index*/
	u8Index		    = (tU8)( fRand*(tDouble)MAX_EVENTS );
	/*Return the Event Index*/
	return u8Index;
}

/*****************************************************************************
* FUNCTION    :	   u32SelectEventPatternRandom()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Select Event Patterns Randomly 
				   - For Both Post and Wait
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tU32 u32SelectEventPatternRandom( tVoid )
{
	/*Declarations*/
	tS32 s32Rand           = RAND_MAX;
	tU32 u32evPatternIndex = 0xFFFFFFFF;
	tU32 u32evPattern      = 0x00000000;

	/*Find Random number*/
	s32Rand = s32RandomNoGenerate( );

	/*Find calculate the event Pattern Index*/
	u32evPatternIndex = ( (tU32)s32Rand%MAX_PATTERNS );

	/*Return the True Pattern*/
	switch( u32evPatternIndex )
	{
		case 0: u32evPattern = PATTERN_1;			break;
		case 1:	u32evPattern = PATTERN_2;			break;
		case 2:	u32evPattern = PATTERN_4;			break;
		case 3:	u32evPattern = PATTERN_8;			break;
		case 4:	u32evPattern = PATTERN_16;			break;
		case 5:	u32evPattern = PATTERN_32;			break;
		case 6:	u32evPattern = PATTERN_64;			break;
		case 7:	u32evPattern = PATTERN_128;			break;
		case 8:	u32evPattern = PATTERN_256;			break;
		case 9:	u32evPattern = PATTERN_512;			break;
		case 10:u32evPattern = PATTERN_1024;		break;
		case 11:u32evPattern = PATTERN_2048;		break;
		case 12:u32evPattern = PATTERN_4096;		break;
		case 13:u32evPattern = PATTERN_8192;		break;
		case 14:u32evPattern = PATTERN_16384;		break;
		case 15:u32evPattern = PATTERN_32768;		break;
		case 16:u32evPattern = PATTERN_65536;		break;
		case 17:u32evPattern = PATTERN_131072;		break;
		case 18:u32evPattern = PATTERN_262144;		break;
		case 19:u32evPattern = PATTERN_524288;		break;
		case 20:u32evPattern = PATTERN_1048576;		break;
		case 21:u32evPattern = PATTERN_2097152;		break;
		case 22:u32evPattern = PATTERN_4194304;		break;
		case 23:u32evPattern = PATTERN_8388608;		break;
		case 24:u32evPattern = PATTERN_16777216;	break;
		case 25:u32evPattern = PATTERN_33554432;	break;
		case 26:u32evPattern = PATTERN_67108864;	break;
		case 27:u32evPattern = PATTERN_134217728;	break;
		case 28:u32evPattern = PATTERN_268435456;	break;
		case 29:u32evPattern = PATTERN_536870912;	break;
		case 30:u32evPattern = PATTERN_1073741824;	break;
		case 31:u32evPattern = PATTERN_2147483648;	break;
		default: break;
	}

	/*Return the pattern*/
	return u32evPattern;
}

/*****************************************************************************
* FUNCTION    :	   Post_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Post Event Thread 
				   - To post an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Post_EventThread( tVoid *ptr )
{
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;

	#if DEBUG_MODE
	tU32 u32eveMask  = MAX_32BIT;
	tU32 u32eveMask_ = MAX_32BIT;
	#endif
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		#if DEBUG_MODE
		/*Get Event Status at the Randomly Selected Index - Before Open*/
		OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
		#endif

		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressList[u8EventIndex].hEventName,&event_StressList[u8EventIndex].hEve ) )
		{
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Post_EventThread:EVENT %s open Successful",
				event_StressList[u8EventIndex].hEventName
			);
			
			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Open*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
			
			/*Select the Post Pattern Randomly*/
			u32PostPattern = u32SelectEventPatternRandom( );

			/*Post Randomly selected pattern to event pattern a Random Index*/
			if( OSAL_ERROR != OSAL_s32EventPost( event_StressList[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR ))
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Successful",
					event_StressList[u8EventIndex].hEventName
				);
					
			}
		   else
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Post_EventThread:EVENT %s Post Failed",
					event_StressList[u8EventIndex].hEventName
				);
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Post*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Close Event Field at the Randomly Selected Index*/
			if( u8SelectEventIndexRandom( ) < (MAX_EVENTS/2) )
			{
		   		vWaitRandomTime( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );	
		   		vWaitRandomTime( FIVE_SECONDS );
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Close*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Wait_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - Wait Event Thread 
				   - To Wait for an event pattern at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Wait_EventThread( tVoid *ptr )
{ 
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32WaitPattern = 0x00000000;

	#if DEBUG_MODE
	tU32 u32eveMask     = MAX_32BIT;
	tU32 u32eveMask_    = MAX_32BIT;
	#endif
	gevMask = 0;
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);
	
	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select the Event Index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		#if DEBUG_MODE
		/*Get Event Status at the Randomly Selected Index - Before Open*/
		OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
		#endif

		/*Open the Event field associated with the index*/
		if( OSAL_OK == OSAL_s32EventOpen( event_StressList[u8EventIndex].hEventName,&event_StressList[u8EventIndex].hEve ) )
		{
			OEDT_HelperPrintf
			(
				TR_LEVEL_USER_4,
				"Wait_EventThread:EVENT %s open Successful",
				event_StressList[u8EventIndex].hEventName
			);

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Open*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		 
			/*Select the Wait Pattern Randomly*/
			u32WaitPattern = u32SelectEventPatternRandom( );
		 

			/*Wait for a Pattern*/
			if(OSAL_OK ==
				OSAL_s32EventWait( 
								   event_StressList[u8EventIndex].hEve,
								   u32WaitPattern,
								   OSAL_EN_EVENTMASK_OR,
								   OSAL_C_TIMEOUT_FOREVER,
							   	&gevMask 
							  		 )
				)
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Released",
						event_StressList[u8EventIndex].hEventName
					);
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Wait_EventThread:EVENT %s Wait Release failed",
						event_StressList[u8EventIndex].hEventName
					);
				}
			/*Clear the event*/
			OSAL_s32EventPost
			( 
			event_StressList[u8EventIndex].hEve,
			~(gevMask),
		   OSAL_EN_EVENTMASK_AND
							  );

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Post*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Close Event Field at the randomly selected index*/
			if( u8SelectEventIndexRandom( ) < (MAX_EVENTS/2) )
			{
		   		vWaitRandomTime( FIVE_SECONDS );
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );
			}
			else
			{
		   		OSAL_s32EventClose( event_StressList[u8EventIndex].hEve );	
		   		vWaitRandomTime( FIVE_SECONDS );
			}

			#if DEBUG_MODE
			/*Get Event Status at the Randomly Selected Index - After Close*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif
		}/*Open Event Successful*/
	}/*Endless loop*/
}

/*****************************************************************************
* FUNCTION    :	   Close_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Delete and Close an Event field at Random Event indices 
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Close_EventThread( tVoid *ptr )
{	
	/*Declarations*/
	tU8  u8EventIndex;
	tU32 u32PostPattern;
	tU8  u8Count 		= 0;

	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	for(;;)
	{
		/*Change the Thread Priority Randomly*/
		vChangeThreadPrioRandom( );

		/*Select event index Randomly*/
		u8EventIndex = u8SelectEventIndexRandom( );

		/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHan, OSAL_C_TIMEOUT_FOREVER );

		/*Check if the event handle is invalid*/
		if( event_StressList[u8EventIndex].hEve != OSAL_C_INVALID_HANDLE )
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );
			
			#if DEBUG_MODE
			/*Query Event Status - Before Delete*/
			OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
			#endif

			/*Delete the Event*/
			if( OSAL_s32EventDelete( event_StressList[u8EventIndex].hEventName ) == OSAL_OK )
			{
				OEDT_HelperPrintf
				(
					TR_LEVEL_USER_4,
					"Close_EventThread:EVENT %s Delete Successful",
					event_StressList[u8EventIndex].hEventName
				);

				#if DEBUG_MODE
				/*Query Event Status - After Delete*/
				OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
				#endif

				/*Post Randomly 200 times*/
				while( u8Count < 200 )
				{
					/*Select the Post Pattern Randomly*/
					u32PostPattern = u32SelectEventPatternRandom( );

					/*Post a Pattern to event pattern a random index*/
					if (OSAL_OK == OSAL_s32EventPost( event_StressList[u8EventIndex].hEve,u32PostPattern,OSAL_EN_EVENTMASK_OR ))
					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Successful",
							event_StressList[u8EventIndex].hEventName
						);
							
					}
				   else
					{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Post Failed",
							event_StressList[u8EventIndex].hEventName
						);
					}


					/*Increment the counter*/
					u8Count++;
				}

				/*Initialize count*/
				u8Count = 0;

				/*Delete the event resources*/
				if ( OSAL_OK != OSAL_s32EventClose( event_StressList[u8EventIndex].hEve ))
				{
						OEDT_HelperPrintf
						(
							TR_LEVEL_USER_4,
							"Close_EventThread:EVENT %s Close Failed",
							event_StressList[u8EventIndex].hEventName
						);
				
				}
				else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s Close Failed",
						event_StressList[u8EventIndex].hEventName
					);
					/*Invalidate the event handle for reuse*/
					event_StressList[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Close_EventThread:EVENT %s handle invalid",
						event_StressList[u8EventIndex].hEventName
					);

				}
				#if DEBUG_MODE
				/*Query Event Status - After Close*/
				OSAL_s32EventStatus( event_StressList[u8EventIndex].hEve,u32eveMask,&u32eveMask_ );
				#endif
			
				/*Invalidate the event handle for reuse*/
				event_StressList[u8EventIndex].hEve = OSAL_C_INVALID_HANDLE;

			}/*Event Delete Success*/
		}/*Handle Valid*/
		else
		{
		   OSAL_s32SemaphorePost( baseSemHan );
		}	
	}/*Loop Infinitely*/
}

/*****************************************************************************
* FUNCTION    :	   Create_EventThread()
* PARAMETER   :    none
* RETURNVALUE :    none
* DESCRIPTION :    Thread Body - 
				   Create an Event field at Random Event indices
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
tVoid Create_EventThread( tVoid *ptr )
{ 
	 /*Declarations*/
	 tU8 u8Index;
	 OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ptr);

	 for(;;)   
   	 {
        /*Randomly modify the priority of the thread*/
		vChangeThreadPrioRandom( );

		/*Select event index randomly*/
      	u8Index = u8SelectEventIndexRandom();

      	/*Wait for Synchronization*/
		OSAL_s32SemaphoreWait( baseSemHan, OSAL_C_TIMEOUT_FOREVER );

      	/*Check whether handle to the corresponding index is invalid*/
      	if (event_StressList[u8Index].hEve == OSAL_C_INVALID_HANDLE)
      	{
         	/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );

         	/*Create the event*/
         	if (OSAL_OK == OSAL_s32EventCreate( event_StressList[u8Index].hEventName,&event_StressList[u8Index].hEve ))
				{
					
			 		OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Created Successful",
						event_StressList[u8Index].hEventName
					);
						
				}
			   else
				{
					OEDT_HelperPrintf
					(
						TR_LEVEL_USER_4,
						"Create_EventThread:EVENT %s Create Failed",
						event_StressList[u8Index].hEventName
					);
				}

      	}
		else
		{
			/*Post*/
			OSAL_s32SemaphorePost( baseSemHan );
		}

		/*Wait randomly*/
      	vWaitRandomTime( FIVE_SECONDS );
     }
}

/*****************************************************************************
* FUNCTION    :	   vEntryFunction()
* PARAMETER   :    Function pointer,Thread name,No of Threads,Thread Attribute 
                   Array,Thread ID Array
* RETURNVALUE :    none
* DESCRIPTION :    Helper Function - Generic Interface to all Threads			       	
* HISTORY     :	   Created By Tinoy Mathews( RBIN/ECM1 ) 14 Dec,2007
*******************************************************************************/
static tVoid vEntryFunction( void (*ThreadEntry) (void *),const tC8 *threadName,
                      		tU8 u8NoOfThreads, OSAL_trThreadAttribute tr_Attr[],
                      		OSAL_tThreadID tr_ID[])
{
	/*Declarations*/
	tU8  u8Index                        = 0;
	tChar aBuf[TOT_THREADS][MAX_LENGTH];

	OSAL_pvMemorySet(aBuf,0,sizeof(aBuf));

	/*Spawn the threads*/
	while( u8Index < u8NoOfThreads )
	{
		/*Catch the thread name into buffer element*/
		OSAL_s32PrintFormat( aBuf[u32GlobEveCounter],"%s_%d",threadName,u8Index );

		/*Fill in the thread attributes*/
		tr_Attr[u8Index].u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
		tr_Attr[u8Index].pvArg        = OSAL_NULL;
		tr_Attr[u8Index].s32StackSize = 2048;
		tr_Attr[u8Index].szName       = aBuf[u32GlobEveCounter];
		tr_Attr[u8Index].pfEntry      = ThreadEntry;

		/*Spawn the thread*/
		if( OSAL_ERROR == ( tr_ID[u8Index] = OSAL_ThreadSpawn( &tr_Attr[u8Index] ) ) )
		{
			//do nothing
		}

		/*Increment the index*/
		u8Index++;

		/*Increment the Global Counter*/
		u32GlobEveCounter++;
	}
}

/*****************************************************************************
* FUNCTION    :	   u32EventStressTest()

* PARAMETER   :    none

* RETURNVALUE :    tU32, "0" ,OSAL_E_TIMEOUT on success.
				   System Crash on failure.

* TEST CASE   :    TU_OEDT_OSAL_CORE_EVT_035

* DESCRIPTION :    1). Set the Seed for Random function based on 
                       Elasped Time.
				   2). Create 30 Event Fields in the system.
				   3). Create 10 Threads which does Post operation 
				       on 30 Events Randomly.
				   4). Create 15 Threads which does Wait operation 
				       on 30 Events Randomly.
				   5). Create 1 Thread which does Delete and Close 
				       operation on 30 Events Randomly.
				   6). Create 2 Threads which does a Re- Create on 
				       30 Events Randomly,if deleted earlier.
				   7). Set a Wait for OSAL_C_TIMEOUT_FOREVER.
				   8). Delete all the 28 Threads.
				   9). Return success.
				   
				   NOTE: This case is to be run individually.The case requires
				   much wait time,since it intends to crash OSAL Event 
				   API Interface.In case a TIMEOUT error results, and no
				   system crash happens, on a relative time basis,OSAL Event
				   API Interface is Robust.Timeout Time can be reconfigured in the 
				   file "oedt_osalcore_TestFuncs.h" under the define 
				   STRESS_EVENT_DURATION.Whether this case should be run 
				   can be configured in the same file as part of the define
				   STRESS_EVENT.
					   			       	
* HISTORY     :	  Created By Tinoy Mathews( RBIN/ECM1 ) 11 Dec,2007
*******************************************************************************/
tU32 u32EventStressTest( tVoid )
{
	/*Definitions*/
	OSAL_tMSecond elapsedTime = 0;
	tU8 u8Index               = 0;
	tU8 u8PostThreadCounter   = 0;
	tU8 u8WaitThreadCounter   = 0;
	tU8 u8CloseThreadCounter  = 0;
	tU8 u8CreateThreadCounter = 0;
	tChar acBuf[MAX_LENGTH]   = "\0";


	/*Get the elapsed Time*/
	elapsedTime = OSAL_ClockGetElapsedTime( );

	/*Seed for OSAL_s32Random( ) function*/
	OSAL_vRandomSeed( elapsedTime%SRAND_MAX );

	/*Create the base semaphore*/
	OSAL_s32SemaphoreCreate( "BASE_SEM",&baseSemHan, 1 );

	/*Create MAX_EVENTS number of events in the system*/
	while( u8Index < MAX_EVENTS )
	{
		/*Fill the buffer with a name*/
		OSAL_s32PrintFormat( acBuf,"Stress_Event_%d",u8Index );
		/*Get the name into Event Name field*/
		(tVoid)OSAL_szStringCopy( event_StressList[u8Index].hEventName,acBuf );
		/*Create an Event in the system*/
		if( OSAL_ERROR == OSAL_s32EventCreate( event_StressList[u8Index].hEventName,&event_StressList[u8Index].hEve ) )
		{
			
			/*Return immediately with failure*/
			return (10000+u8Index);
		}
		/*Reinitialize the buffer*/
		OSAL_pvMemorySet( acBuf,0,MAX_LENGTH );
		/*Increment the index*/
		u8Index++;
	}

	/*Start all Post Threads*/
	vEntryFunction( Post_EventThread,"Post_Ev_Thread",NO_OF_POST_THREADS,evepost_ThreadAttr,evepost_ThreadID );
	/*Start all Wait Threads*/
	vEntryFunction( Wait_EventThread,"Wait_Ev_Thread",NO_OF_WAIT_THREADS,evewait_ThreadAttr,evewait_ThreadID );
	/*Start the Close Thread*/
	vEntryFunction( Close_EventThread,"Close_Ev_Thread",NO_OF_CLOSE_THREADS,eveclose_ThreadAttr,eveclose_ThreadID );
	/*Start all Create Threads*/
	vEntryFunction( Create_EventThread,"Create_Ev_Thread",NO_OF_CREATE_THREADS,evecreate_ThreadAttr,evecreate_ThreadID );

	/*Wait for OSAL_C_TIMEOUT_FOREVER*/
	OSAL_s32ThreadWait( OSAL_C_TIMEOUT_FOREVER );

	/*Delete all Post Threads*/
	while( u8PostThreadCounter < NO_OF_POST_THREADS )
	{
		/*Delete the Post Thread*/
		OSAL_s32ThreadDelete( evepost_ThreadID[u8PostThreadCounter] );
		/*Increment the post counter*/
		u8PostThreadCounter++;
	}

	/*Delete all Wait Threads*/
	while( u8WaitThreadCounter < NO_OF_WAIT_THREADS )
	{
		/*Delete the Wait Thread*/
		OSAL_s32ThreadDelete( evewait_ThreadID[u8WaitThreadCounter] );
		/*Increment the wait counter*/
		u8WaitThreadCounter++;
	}

	/*Delete all Close Threads*/
	while( u8CloseThreadCounter < NO_OF_CLOSE_THREADS )
	{
		/*Delete the Close Thread*/
		OSAL_s32ThreadDelete( eveclose_ThreadID[u8CloseThreadCounter] );
		/*Increment the close counter*/
		u8CloseThreadCounter++;
	}
		
	/*Delete all Create Threads*/
	while( u8CreateThreadCounter < NO_OF_CREATE_THREADS )
	{
		/*Delete the Create Thread*/
		OSAL_s32ThreadDelete( evecreate_ThreadID[u8CreateThreadCounter] );
		/*Increment the create counter*/
		u8CreateThreadCounter++;
	}

	/*Reset the Global Thread Counter*/
	u32GlobEveCounter = 0;

	/*Close the base semaphore*/
	OSAL_s32SemaphoreClose( baseSemHan );
	/*Delete the base semaphore*/
	OSAL_s32SemaphoreDelete( "BASE_SEM" );

	/*Return error code - Success if control reaches here*/
	return 0;
	
}	
#endif


/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
