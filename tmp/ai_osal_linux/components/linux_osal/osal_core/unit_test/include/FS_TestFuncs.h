/******************************************************************************
 * FILE				:	FS_TestFuncs.h
 *
 * SW-COMPONENT		:	OSAL IO UNIT TEST 
 *
 * DESCRIPTION		:	This file contains the prototype and some Macros that 
 *						will be used in the file oedt_FS_TestFuncs.c   
 *               		for the FileSystem device.
 *                          
 * AUTHOR(s)		:	Sriranjan U (RBEI/ECM1), Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * Date				|							         |	Author & comments
 * --.--.--			|	Initial revision		      | 	------------
 * 31.03.15       |  Ported from OEDT           |  SWM2KOR
 *------------------------------------------------------------------------------*/

#ifndef FS_TESTFUNCS_HEADER
#define FS_TESTFUNCS_HEADER

#define OEDT_C_STRING_DEVICE_INVAL "/dev/Invalid"
#define OEDT_INVALID_ACCESS_MODE -100

#define SUBDIR_NAME 		"NewDir"
#define SUBDIR_NAME_INVAL 	"/Inval"
#define SUBDIR_NAME2 		"NewDir2"
#define SUBDIR_NAME3 		"NewDir3"
#define DIR1		"File_Source"
#define DIR			"File_Dir"
#define DIR_RECR1	"File_Dir2" 


#define FILE13_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File13.txt"
#define FILE14_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File14.txt"

#define OEDT_C_STRING_FFS2_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS3"/*@#**"

#define OEDT_MAX_TOTAL_DIR_PATH_LEN 		260
#define OEDT_FS_FILE_PATH_LEN 					44
#define OEDT_CDROM_NEGATIVE_POS_TO_SET -75
#define OEDT_CDROM_BYTES_TO_READ 20
#define OEDT_CDROM_OFFSET_BEYOND_END 559


/* Function Prototypes for the functions defined in Oedt_FS_TestFuncs.c */

tU32 u32FSOpenClosedevice(const char * dev_name );
tU32 u32FSOpendevInvalParm(const char *  dev_name );
tU32 u32FSReOpendev(const char *  dev_name );
tU32 u32FSOpendevDiffModes(const char *  dev_name );
tU32 u32FSClosedevAlreadyClosed(const char *  dev_name );
tU32 u32FSOpenClosedir(const char *  dev_name );
tU32 u32FSOpendirInvalid(const tPS8 * dev_name );
tU32 u32FSCreateDelDir(const char *  dev_name );
tU32 u32FSCreateDelSubDir(const char *  dev_name );
tU32 u32FSRmNonExstngDir(const char *  dev_name );
tU32 u32FSRmDirUsingIOCTRL(const tPS8 *  dev_name );
tU32 u32FSGetDirInfo(const char * dev_name, tBool bCreateRemove);
tU32 u32FSOpenDirDiffModes(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSReOpenDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSCreateDirMultiTimes(const tPS8 *  dev_name, tBool bCreateRemove); 
tU32 u32FSCreateRemDirInvalPath(const tPS8 *  dev_name);
tU32 u32FSCreateRmNonEmptyDir(void);
tU32 u32FSCopyDir(void);
tU32 u32FSMultiCreateDir(void);
tU32 u32FSCreateSubDir(void);
tU32 u32FSDelInvDir(void);
tU32 u32FSCopyDirRec(void);
tU32 u32FSRemoveDir(void);
tU32 u32FSFileCreateDel(void);
tU32 u32FSFileOpenClose(void);
tU32 u32FSFileOpenInvalPath(void);
tU32 u32FSFileOpenInvalParam(void);
tU32 u32FSFileReOpen(void);
tU32 u32FSFileCreateDouble(void);
tU32 u32FSFileRead(void);
tU32 u32FSFileWrite(void);
tU32 u32FSGetPosFrmBOF(void);
tU32 u32FSGetPosFrmEOF(void);
tU32 u32FSFileReadNegOffsetFrmBOF(void);
tU32 u32FSFileReadOffsetBeyondEOF(void);
tU32 u32FSFileReadOffsetFrmBOF(void);
tU32 u32FSFileReadOffsetFrmEOF(void);
tU32 u32FSFileReadEveryNthByteFrmBOF(void);
tU32 u32FSFileReadEveryNthByteFrmEOF(void);
tU32 u32FSGetFileCRC(void);
tU32 u32FSReadAsync(tVoid);
tU32 u32FSWriteAsync(tVoid);

#endif


