#ifndef PRM_TABLE_UTEST_H 
#define PRM_TABLE_UTEST_H

#ifdef __cplusplus
extern "C" {
#endif

extern int scan_existing_mounts( void );
extern void vCheckPrm(void);
extern void vStartPrmTasks(void);
extern void vPrmTtfisTrace(tU8 u8Level, const char *pcFormat, ...);
extern tVoid prm_vInit(void);
extern tBool prm_bIsAPrmFun( tS32 s32Fun );
extern tU32 prm_u32Prm(OSAL_tIODescriptor rDevId, tS32 s32Fun, intptr_t s32Arg);

int scan_existing_mounts_wrapper( void )
{
   return scan_existing_mounts();
}

void vCheckPrm_wrapper(void)
{
   return vCheckPrm();
}

void vStartPrmTasks_wrapper(void)
{
   return vStartPrmTasks();
}

tVoid prm_vInit_wrapper(void)
{
   return prm_vInit();
}

tBool prm_bIsAPrmFun_wrapper( tS32 s32Fun )
{
   return prm_bIsAPrmFun(s32Fun);
}

tU32 prm_u32Prm_wrapper(OSAL_tIODescriptor rDevId, tS32 s32Fun, intptr_t s32Arg)
{
   return prm_u32Prm( rDevId,  s32Fun,  s32Arg);
}

#ifdef __cplusplus
}
#endif
#endif


