/******************************************************************************
 * FILE				  : FFS3_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT	: OEDT_FrmWrk 
 *
 * DESCRIPTION	: This file defines the macros and prototypes for the 
 *					      filesystem test cases for the FFS3
 *               File is ported from oedt to unit test
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			  |					                    |	Author & comments
 * --.--.--			|	Initial revision                   |	------------
 * 17 Jan, 2012	|	version 1.0			                 |	Martin Langer CM-AI/PJ-CF33
 * 16 Mar, 2015   |  version 1.1                        |  Deepak Kumar (RBEI/ECF5)
 * 30 Mar,2015    |  version 2.0  - ported from OEDT    |  SWM2KOR
 *-----------------------------------------------------------------------------
 */


#ifndef FFS3_COMMON_FS_TESTFUNCS_HEADER
#define FFS3_COMMON_FS_TESTFUNCS_HEADER

#define OEDTTEST_C_STRING_DEVICE_FFS3 "/dev/ffs3"
// #endif

#undef OEDT_C_STRING_FFS3_DIR1 
#undef OEDT_C_STRING_FFS3_NONEXST 
#undef OEDT_C_STRING_FFS3_FILE1
#undef SUBDIR_PATH_FFS3	
#undef SUBDIR_PATH2_FFS3   
#undef OEDT_C_STRING_FFS3_DIR_INV_NAME 
#undef OEDT_C_STRING_FFS3_DIR_INV_PATH 
#undef	CREAT_FILE_PATH_FFS3 
#undef OSAL_TEXT_FILE_FIRST_FFS3 
#undef OEDT_C_STRING_FILE_INVPATH_FFS3 
#undef OEDT_FFS3_COMNFILE 
#undef OEDT_FFS3_WRITEFILE 
#undef FILE12_RECR2_FFS3 
#undef FILE11_FFS3 
#undef FILE12_FFS3 

#undef OEDT_C_STRING_DEVICE_FFS3_ROOT 

#undef FILE13_FFS3    
#undef FILE14_FFS3    

#undef OEDT_FFS3_CFS_DUMMYFILE			   
#undef OEDT_FFS3_CFS_TESTFILE			   
#undef OEDT_FFS3_CFS_INVALIDFILE		   
#undef OEDT_FFS3_CFS_UNICODEFILE		   
#undef OEDT_FFS3_CFS_INVALCHAR_FILE		
#undef OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_SYNC  
#undef OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_ASYNC 

#define OEDT_C_STRING_FFS3_DIR1 OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir"
#define OEDT_C_STRING_FFS3_NONEXST OEDTTEST_C_STRING_DEVICE_FFS3"/Invalid"
#define OEDT_C_STRING_FFS3_FILE1 OEDTTEST_C_STRING_DEVICE_FFS3"/FILEFIRST.txt"
#define SUBDIR_PATH_FFS3	OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir"
#define SUBDIR_PATH2_FFS3	OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir/NewDir2"
#define OEDT_C_STRING_FFS3_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS3"/*@#**"
#define OEDT_C_STRING_FFS3_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_FFS3"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/Dummydir/Dummy.txt"

#define OEDT_FFS3_COMNFILE OEDTTEST_C_STRING_DEVICE_FFS3"/common.txt"

#define OEDT_FFS3_WRITEFILE OEDTTEST_C_STRING_DEVICE_FFS3"/WriteFile.txt"

#define FILE12_RECR2_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File11.txt"
#define FILE12_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_FFS3_ROOT OEDTTEST_C_STRING_DEVICE_FFS3"/"

#define FILE13_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File13.txt"
#define FILE14_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File14.txt"

#define OEDT_FFS3_CFS_DUMMYFILE				OEDTTEST_C_STRING_DEVICE_FFS3"/Dummy.txt"
#define OEDT_FFS3_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_FFS3"/Test.txt"
#define OEDT_FFS3_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_FFS3"/DIR/Test.txt"
#define OEDT_FFS3_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_FFS3"/¶«æ±ª¶.txt"
#define OEDT_FFS3_CFS_INVALCHAR_FILE		OEDTTEST_C_STRING_DEVICE_FFS3"//*/</>>.txt"
#define OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_SYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS3"/ioctrl_save_now_FFS3_sync.txt"
#define OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_ASYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS3"/ioctrl_save_now_FFS3_async.txt"
	 											  
#endif