

#ifndef __OSAL_CORE_UNIT_TEST_MOCK_H_
#define __OSAL_CORE_UNIT_TEST_MOCK_H_



class OsalCoreUnitTestMock {
 public:
/*  MOCK_METHOD0(eh_reboot,                     void(void));
  MOCK_METHOD1(init_exception_handler,        void(const char *name));
  MOCK_METHOD0(exit_exception_handler,        void(void));
  MOCK_METHOD0(exception_handler_lock,        int(void));
  MOCK_METHOD0(exception_handler_unlock,      void(void));
  MOCK_METHOD0(pGetLockAdress,                int*(void));
  MOCK_METHOD0(InitSignalFifo,            void(void));
*/



/*
  MOCK_METHOD5(ERRMEM_S32IOOpen,              tS32(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16 app_id));
  MOCK_METHOD2(ERRMEM_S32IOClose,             tS32(tS32 s32ID, tU32 u32FD));
  MOCK_METHOD4(ERRMEM_s32IOControl,           tS32(tS32 s32ID, tU32 u32fd, tS32 io_func, tS32 param));
  MOCK_METHOD5(ERRMEM_s32IORead,              tS32(tS32 s32ID, tU32 u32FD, tPS8 buffer,tU32 size, tU32 *ret_size));
  MOCK_METHOD5(ERRMEM_s32IOWrite,             tS32(tS32 s32ID, tU32 u32FD, tPCS8 buffer,tU32 size, tU32 *ret_size));
  MOCK_METHOD1(HandleMaxErrMemFilesize,        void(int u32Size));
*/

};   

class OSAL_CORE : public testing::Environment
{
	public:
		OSAL_CORE() {}
		virtual ~OSAL_CORE() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();
};


#endif     // __OSAL_CORE_UNIT_TEST_MOCK_H_
