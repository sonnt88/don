/************************************************************************
| FILE:         Osal_Conf.h
| PROJECT:      platform
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for the OSAL Abstraction Layer to
|               configure the OSAL Layer
|               modules of OSAL core.
|
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:
| Date      | Modification                   | Author
| 03.10.05  | Initial revision               | MRK2HI
| --.--.--  | ----------------               | -------, -----
| 07.09 .12 | Changedthe OSFILE_BLOCKSIZE    | sja3kor
| --.--.--  | ----------------               | -------, -----
| 26.09.12  |Feature PRM_LIBUSB_CONNECTION   |sja3kor
|           |defined for OSAL_GEN2 section   |
  29.11.12  |Removed the enum structure      |mdh4cob
| 11.12.12  | To Add signals used in Timer   | SWM2KOR
| 21.01.13  | Added Macro MQ_MEASUREMENT     | SWM2KOR 
| 24.01.13  |Defined PRM_LIBUSB_CONNECTION   | dhs2cob
|           |   for LSIM
| 19.03.13  |Defined                         | nro2kor
|           |GEN3_ODOMETER_ACTIVE for GEN3
| 09.04.2014| LSIM-Gen3 CD Audio and control 
|             related unused-macros removed  | sgo1cob
| 03.06.14  | Enable PRMLIB USB POWER	     | kgr1kor
| 27.04.16  | Defined AM_BIN for Gen3 SIM    | sja1kor
|*****************************************************************************/
#if !defined OSAL_CONF_HEADER
#define OSAL_CONF_HEADER

/******************************************************************************
** #include
******************************************************************************/
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <poll.h>
#include <sys/epoll.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <mqueue.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/statvfs.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sched.h>
#include <asm/param.h>
#include <setjmp.h>
#include <grp.h>
#include <limits.h>
#include <sys/wait.h>
#include <inttypes.h>
#include <sys/shm.h>
#include <sys/syscall.h>
#include <dlfcn.h>
#include <linux/futex.h>

/*********************************************************************************************/
/* Definition of special OSAL Core features                                                  */
/*********************************************************************************************/

#define OSAL_NAME_SHMEM   "OSAL-arena"
#define OSAL_NAME_RES_SHMEM "OSAL_RES_MEM"
#define OSAL_NAME_LOCK    "OSAL-init"
#define OSAL_NAME_SYNCLOCK    "OSAL-initsync"
#define OSAL2_NAME_LOCK   "OSAL2-init"

#define OSAL_SHARED_LIB_PATH   "/opt/bosch/base/lib/"

#define OSAL_EXCHDR_STATUS  1         /* Bosch exception handler is active */

//#define MEASURE_OSAL_START

/* constants */
/* minimum allowed size of a thread stack, otherwise you get 8MB (the default "ulimit -s" value) */
#define RBTREE_SEARCH                        /* use RB tree algorithm for thread ID (set/get error) */

#define MAX_MSG_CBHDR          20


#define OSALIO_PRIORITY_THREAD_PRM_LIBUSB_TA        30
#define OSALIO_PRIORITY_THREAD_PRM_LIBUSBQ_TA       30

/* in past: Priority must be higher than SPM -> ThreadSuspend Problem */
#define OSALCORE_C_U32_PRIORITY_2DI              (OSAL_C_U32_THREAD_PRIORITY_NORMAL)

#define OSAL_MSG_POOL_SIZE 0x001F4000   //default CCA Message pool size

#define LINUX_OS_TIME_RESOLUTION_IN_MS   10  // default resolution for LINUX timer
//#define TRACE_ASYNCIO              // activate trace output for asynchronous IO

#define tkill(x, sig) syscall(SYS_tkill, (x), (sig))
#define tgkill(x, sig) syscall(SYS_tgkill, (x), getpid(),(sig))

/* what signal the timerThread uses */
#define OSAL_TIMER_SIGNAL                  40 // changed due valgrind from SIGRTMAX 

#define SIG_BACKTRACE                    SIGRTMIN /*signal for callstack generation allowed signals from SIGRTMIN up to SIGRTMAX -3 */

/*
#define OSAL_SIGNAL_EVENT_AND            62
#define OSAL_SIGNAL_EVENT_OR             61
#define OSAL_SIGNAL_EVENT_XOR            60
#define OSAL_SIGNAL_EVENT_REPLACE        59

#define OSAL_SIGNAL_EVENT_PRC_AND        58
#define OSAL_SIGNAL_EVENT_PRC_OR         57
#define OSAL_SIGNAL_EVENT_PRC_XOR        56
#define OSAL_SIGNAL_EVENT_PRC_REPLACE    55
*/
#define OSAL_CB_HDR_SIGNAL               54 /*signal to interrupt callack execution in callbackhandler task */
//#define OSAL_EXIT_SIGNAL                 53 /*signal to indicate proper shutdown of an OSAL based process */
#define OSAL_STACK_SIGNAL                52 /*signal for stack info generation*/

#define OSAL_TIMER_THREAD_PRIORITY          (70)


/* generic OSAL Core defines */
#define BLOCKING_TIMEOUT  3000
#define MQ_NOTIFY_VIA_OSAL                   // MQ notify via OSAL callback handler task
#define SV_RESOLUTION 1000                   // resolution for supervison of mq_timedwait to reliaze timeout via CLOCK_MONOTONIC
#define USE_EVENT_AS_CB
#define OSAL_SHM_MQ
#ifdef OSAL_SHM_MQ
//#define OSAL_SHM_MQ_FOR_LINUX_MQ
#endif
//#define USE_FUTEX

//#define AREA_TEST


/* OSAL Core file&debug operation defines */
//#define CHECK_COPY                           // activates security mechanism for OSAL Core download
#define OSALCORE_FILECOPY_BUFFERSIZE 1000000 // maximum allocated memory for file copy operations in OSAL Core
#define DELAY_PRO_RW_CYCLE              100  // delay in ms after file copy operations defined with OSALCORE_FILECOPY_BUFFERSIZE

/* OSAL message queue related defines */
//#define MAX_MSG_COUNT                      // possibility to Count the highest OSAL Message queue load
#define BLOCKING_TIMEOUT             3000    // Timeout for blocking OSAL message queue

#define TIMER_STORAGE_SIZE            100    // number of timer signals which could stored temporally

/* OSAL timer related defines */
#define MEASURE_CALLBACKS                    // automatic Measurement of timer callbacks

#define OSAL_TERM_LI_MQ          "LI_TERM_MQ"
#define ASYNCLOCK     "ASYNCLOC"
#define ASYNCJOBTRIG  "JOB_TRIG"

#define REGISTRY_MEM_SIZE  500000  // maximum Bytes available for registry entries


//#define SEMAPHORE_WARN_LEVEL    1200
#define PROTECTED_OSAL_AREAS

/*********************************************************************************************/


/*********************************************************************************************/
/* Definition of special OSAL IO features                                                  */
/*********************************************************************************************/
#define MAX_MOUNTPOINT_STRING_SIZE 256 // size of the longest string in MountPoint[] (see dispatcher_table.c)
/* File system related defines */
//#define READWRITE_SEGMENTED                 // read&write large files in segments
#define READ_BUFFER_LENGTH          10240   // buffer length for segmented reading
#define FILECOPY_BUFFERSIZE         1000000 // maximum allocated buffer size for file copy operations

/*********************************************************************************************/

#define pOsalDataAddress (void*)0x00000000L  /* central OSAL shared memory has no fix address for mapping*/
#define GLOBAL_DATA_OPTION  MAP_SHARED


//#define EPOLL_EV                    /* activate epoll based event implementation */
//#define SIGNAL_EV_HDR               /* activate signal based event implementation */
#if !defined (EPOLL_EV) && !defined(SIGNAL_EV_HDR)
#define SEM_EV
#endif

//#define SIGNAL_TRACE_EVENT          /* traces to follow event signals */
//#define SIGNAL_TRACE_TIMER          /* traces to follow timer signals */
//#define SIGNAL_TRACE                /* traces to follow process signals */
//#define FRAMEBUFFER_TEST            /* data for screen test active */
#define MMAP_FLAGS MAP_SHARED/*|MAP_UNINITIALIZED  Option for shared memory mapping*/
//#define OSAL_MMAP_SIZE   5*1024     /* activate mmap for OSAL_pvMemoryAllocate*/
//#define VIRTMEM_LIMIT               /* possibility to limit process adress space */

#define MMAP_FAST                     /* mmap with MAP_ANONYMOUS optione without file descriptor handling */
#ifndef MMAP_FAST
#define OSAL_MMAP_SHMEM               /* use shared memory instead of file IO for OSAL MMAP API */
#endif

#define AES_ENCRYPTION_AVAILABLE    /* AES encryption suppport from OSAL registry device */


#define OSAL_FILE_ACCESS_RIGTHS  S_IRWXU | S_IRWXG | S_IRWXO  /* defines access option for all Linux 
                                                                file objects via OSAL */
#define OSAL_ACCESS_RIGTHS       S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP /* 0660  defines access option for all Linux  objects via OSAL (MQ, Share Mem)  S_IWUSR | S_IRUSR | S_IRGRP| S_IWGRP | S_IROTH | S_IWOTH 0666 */

#define OSAL_ACCESS_RIGTHS_RO       S_IRUSR | S_IRGRP /* 0440  */
#define OSAL_ACCESS_RIGTHS_WO       S_IWUSR | S_IWGRP /* 0220  */
#define OSAL_ACCESS_RIGTHS_RW       S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP /* 0660 */

#define OSAL_MEDIA_ACCESS_RIGTHS               0750
#define OSAL_VOLATILE_OSAL_ACCESS_RIGTHS       0750
#define OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS  01770


#define OSAL_GROUP_NAME    "eco_osal"
#define MEDIA_GROUP_NAME   "media"

#define PROTECTED_PAGE_SIZE         0x1000 /* 4KB*/

#ifdef __LINT__  /* switch on right configuration for Lint run*/
#define GEN3ARM
#endif

/*********************************************************************************************/
/*                   OSAL_GEN3 GEN3 ARM specific section                                     */
/*********************************************************************************************/
#ifdef GEN3ARM
#define VARIANT_S_FTR_ADIT_SUPPORTED    // ADIT exception handler IF
#define EXC_ARM
#define LIBUNWIND_USED                  /* libunwind is used for callstack generation */
#define NO_PRIxPTR
#define SHORT_TRACE_OUTPUT               // 
//#define OSAL_ERRMEM_TIMESTAMP         // set timestamp via OSAL to error memory for each entry
#define AM_BIN                          // ADIT automounter with API is available
#define SET_ERRMEM_TIME                 // Set time to errmem driver
#define PRM_LIBUSB_CONNECTION          /* prm_usbpower.c (overcurrrent and undervoltage) module*/
#define VOLATILE_DIR       "/run"
#define SHM_BIGCCA_PATH    "/dev/shm/CCAMSG"
#endif /* GEN3ARM */

/*********************************************************************************************/
/*                   OSAL_OSAL_GEN4 (64Bit) ARM specific section                                     */
/*********************************************************************************************/
#ifdef GEN4ARM
#define VARIANT_S_FTR_ADIT_SUPPORTED    // ADIT exception handler IF
#define EXC_ARM
//#define PRINT_DATATYPES
#define LIBUNWIND_USED                  /* libunwind is used for callstack generation */
#define WRITE_TO_STDOUT                 /* write each exception handler erremm entry additional to standard out */
#define AM_BIN                          // ADIT automounter with API is available
#define SET_ERRMEM_TIME                 // Set time to errmem driver
#define VOLATILE_DIR       "/run"
#define SHM_BIGCCA_PATH    "/dev/shm/CCAMSG"
#endif /* GEN4ARM */


/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/

/*********************************************************************************************/
/*                   OSAL_LINUX_X86 specific section Ubuntu                                  */
/*********************************************************************************************/
#ifdef _LINUXX86MAKE_
//#define LIBUNWIND_USED              /* libunwind is used for callstack generation */
#define EXC_X86
#define NO_PRIxPTR
#define CS_GEN_VIA_OSAL             /* callstack generation via OSAL signal handler */
#define WRITE_TO_STDOUT             /* write each exception handler erremm entry additional to standard out */
#define UBUNTU_ENV                      // nativ UBUNTU enviroment is used
#define VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
#define VOLATILE_DIR       "/tmp"
#define SHM_BIGCCA_PATH    "/run/shm/CCAMSG"
#define NO_OSAL_EXIT_HDR
//#define SIGTERM_HANDLER
#endif /* OSAL_LINUX_X86 */

/*********************************************************************************************/
/*                   OSAL 64Bit OSAL_LINUX_X86 specific section                                     */
/*********************************************************************************************/
#ifdef _LINUXX86_64_
#define LIBUNWIND_USED                /* libunwind is used for callstack generation */
#define EXC_X86
#define PRINT_DATATYPES
#define CS_GEN_VIA_OSAL             /* callstack generation via OSAL signal handler */
#define WRITE_TO_STDOUT             /* write each exception handler erremm entry additional to standard out */
#define UBUNTU_ENV                      // nativ UBUNTU enviroment is used
#define VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
#define VOLATILE_DIR       "/tmp"
#define SHM_BIGCCA_PATH    "/run/shm/CCAMSG"
#define NO_OSAL_EXIT_HDR
//#define SIGTERM_HANDLER
#endif /* _LINUXX86_64_ */


/*********************************************************************************************/
/*                   OSAL 64Bit Broxton specific section                                     */
/*********************************************************************************************/
#ifdef GEN4INTEL
#define EXC_X86
#define VARIANT_S_FTR_ADIT_SUPPORTED    // ADIT exception handler IF
//#define PRINT_DATATYPES
#define LIBUNWIND_USED                  /* libunwind is used for callstack generation */
#define WRITE_TO_STDOUT                 /* write each exception handler erremm entry additional to standard out */
#define AM_BIN                          // ADIT automounter with API is available
#define SET_ERRMEM_TIME                 // Set time to errmem driver
#define VOLATILE_DIR       "/run"
#define SHM_BIGCCA_PATH    "/dev/shm/CCAMSG"
#endif /* GEN4INTEL */


/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/

/*********************************************************************************************/
/*                   OSAL_GEN3_SIM specific section                                          */
/*********************************************************************************************/
#ifdef OSAL_GEN3_SIM
#define VARIANT_S_FTR_ADIT_SUPPORTED    // ADIT exception handler IF
#define EXC_X86
#define LIBUNWIND_USED                /* libunwind is used for callstack generation */
#define NO_PRIxPTR
#define SHORT_TRACE_OUTPUT               // 
#define AM_BIN                          // ADIT automounter with API is available
#define VOLATILE_DIR       "/run"
#define SHM_BIGCCA_PATH    "/run/shm/CCAMSG"
#endif /* OSAL_GEN3_SIM */

/*********************************************************************************************/
/*                   OSAL_GEN4_SIM specific section                                          */
/*********************************************************************************************/
#ifdef OSAL_GEN4_SIM
#define EXC_X86
#define VARIANT_S_FTR_ADIT_SUPPORTED    // ADIT exception handler IF
//#define PRINT_DATATYPES
#define LIBUNWIND_USED                  /* libunwind is used for callstack generation */
#define AM_BIN                          // ADIT automounter with API is available
#define VOLATILE_DIR       "/run"
#define SHM_BIGCCA_PATH    "/dev/shm/CCAMSG"
#endif /* OSAL_GEN3_SIM */

/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/


/*********************************************************************************************/
/*                   Generic OSAL resource configuration                                     */
/*********************************************************************************************/
/* defines for OSAL Resources */
#define MAX_LINUX_PROC       100              /* maximum number of processes start via OSAL */
#define MAX_OSAL_EVENTS      500
#define MAX_OSAL_THREADS     600
#define MAX_OSAL_SHARED_MEM   50
#define MAX_OSAL_SEMAPHORES 1400
#define MAX_OSAL_TIMER       400
#define MAX_OSAL_MSG_QUEUE   300
#define MAX_OSAL_MUTEX       100

#ifdef SEM_EV
#define CONF_DEP_LOCKS    MAX_OSAL_EVENTS *2 /*locks needed for event implementation based on semaphores */
#else //SEM_EV
#define CONF_DEP_LOCKS    0 
#endif //SEM_EV

#define MAX_OSAL_LOCK  MAX_OSAL_SEMAPHORES + (MAX_OSAL_EVENTS *2) + (C_MAX_JOBS + 1)

/* defines for Block Pools defult sizes, can be overwritenn in osal.reg */ 
#define DEVDECS_BLOCKCOUNT    800  // number of device descriptor in OSAL mempool
#define FILEDECS_BLOCKCOUNT   256  // number of file descriptors in OSAL mempool
#define MQHANDLE_BLOCKCOUNT   1500 // number of OSAL MQ handles in OSAL mempool
#define EVDAT_BLOCKCOUNT      800
#define SEMDAT_BLOCKCOUNT     2000

#define MQSTDDAT_BLOCKCOUNT   MAX_OSAL_MSG_QUEUE  // maximum number of OSAL MQ itself

#define DEVDECS_BLOCKSIZE    sizeof(OsalDeviceDescriptor)
#define FILEDECS_BLOCKSIZE   sizeof(OSAL_tfdIO)
#define MQHANDLE_BLOCKSIZE   sizeof(tMQUserHandle)
#define MQSTDDAT_BLOCKSIZE   sizeof(trStdMsgQueue)
#define EVDAT_BLOCKSIZE      sizeof(tEvHandle)
#define SEMDAT_BLOCKSIZE     sizeof(tSemHandle)
/*********************************************************************************************/

#else
#error OsalConf.h included several times
#endif
