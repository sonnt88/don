/************************************************************************
| FILE:         osalmempool.h
| PROJECT:      platform
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for RB tree definitions
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2016 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 10.07.16  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (OSAL_RBTREE_HEADER)
   #define OSAL_RBTREE_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef struct
{
       tS32              key;
       intptr_t          Offsetl;
       intptr_t          Offsetr;
       intptr_t          Offsetp;
       tU32              u32Index;
       char              bRed;
}trElementNode;

typedef struct
{
  tBool            bInValidRbTree;
  tBool            bChangeInProgress;
  intptr_t         MaxOffset;
  intptr_t         MinOffset;
  intptr_t         HeadOffset;
  tU32             u32UsedElements;
  tU32             u32MaxElements;
  intptr_t         OffsetData;
  intptr_t         OffSetLock;
}trRBTreeData;


/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************/
/* function for global memory pool configuration                        */
/************************************************************************/

/************************************************************************/
/* function for using specific memory pools                             */
/************************************************************************/

OSAL_DECL void vInitNodes(tU32 NrOfEl,trRBTreeData* pDat,trElementNode* pElements,trOsalLock* pLock);
OSAL_DECL void vInsertElement(trRBTreeData* pDat,tS32 key,tS32 s32Data);
OSAL_DECL void vRemoveElement(trRBTreeData* pDat,tS32 key);
OSAL_DECL trElementNode* pSearchElement(trRBTreeData* pDat,tS32 key,tBool bLock);


#ifdef __cplusplus
}
#endif

#else
#error osalrbtree.h included several times
#endif
