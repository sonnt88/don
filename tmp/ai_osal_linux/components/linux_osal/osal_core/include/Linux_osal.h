/************************************************************************
| FILE:         Linux_osal.h
| PROJECT:      platform
| SW-COMPONENT: OSAL CORE
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for the OSAL Abstraction Layer for
|               common internal data. This Header has to be included by all
|               modules of OSAL core.
|
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
  29.11.12  | Adding enum structure      | mdh4cob
| --.--.--  | ----------------           | -------, -----
| 17.12.12  | Fix for MMS CFAGII-501.    |sja3kor 
|           | Added macro.               |  
| 10.01.13  | Lint Removal               | SWM2KOR        
| 21.01.13  | Fix for GMNGA48007         | SWM2KOR |
| 20.03.13  | Fix for NIKAI 3163         | SWM2KOR     
| 11.11.13  | Adding new CallBack for u32|
|           | parameter as the Argument  | SWM2KOR   
| 20.05.14  |Added a member s32DeviceIdx |
|           |to structure trGlobalOsalData  | pmh5kor
| 20.06.14  | FIx for SUZUKI Ticket CFG3-742 - Added a new      |
|           | argument s32DevID to function pointer crypt_ctrl  | 
|           | and removed an a argument s32ID                   |pmh5kor
| 25.07.14  | Added filedescriptor for downloadtrace2usb|SJA3kOR
|*****************************************************************************/
#if !defined LINUX_OSAL_HEADER
#define LINUX_OSAL_HEADER


#ifdef __cplusplus
extern "C" {
#endif 

/******************************************************************************
** #include
******************************************************************************/
#include "mqueue.h"
#include "osal_public.h"


/* Osal Function Pointer Table Entries */
typedef tS32 (*drv_io_open)(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
typedef tS32 (*drv_io_close)(tS32 s32ID, uintptr_t u32FD);
typedef tS32 (*drv_io_control)(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
typedef tS32 (*drv_io_write)(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
typedef tS32 (*drv_io_read)(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

typedef tU32 (*crypt_ctrl)( void* vpPtr, tS32 s32DevID);
typedef tS32 (*crypt_sign)(tCString szPath);
typedef tU32 (*aes_encrypt)(tCString szFileName,void* vpData,tU32 u32Size);

typedef void (*dbg_func)(void* pPtr);
typedef tU32 (*u32PrmFunc)(OSAL_tIODescriptor rDevId, tS32 s32Fun, uintptr_t s32Arg);
typedef void (*CbExc)(OSAL_tProcessID pid, OSAL_tThreadID tid,tU8 u8MediumType);
typedef void (*PrcTskInfo)(char* pcPid, tBool bErrMemEntry);

typedef tS32 (*GetFunctionPointer)(void* pModuleHandle);

typedef void (*TRACE_CALLBACK)(void*);
typedef int (*pFuncIsClassSelected)(unsigned short trClass, TR_tenTraceLevel enLevel);
typedef unsigned int (*pFuncTraceOut)(unsigned int  uwCompId, unsigned int  uwSockId, unsigned int  uwBufferLen,unsigned char* p_ubDataBuffer); /*lint !e1773 */
typedef unsigned int  (*pFuncTraceBinOutput)(unsigned int  uwCompId, unsigned int  uwSockId, unsigned int  uwBufferLen,unsigned char* p_ubDataBuffer);
typedef int (*pFunc_chan_acess_bRegChan)(TR_tenTraceChan chan_id,TRACE_CALLBACK p_Func);
typedef int (*pFunc_chan_acess_bUnRegChan)(TR_tenTraceChan chan_id, TRACE_CALLBACK p_Func);
typedef void (*pFunc_get_blockmode_status)(unsigned int  *TraceBlockMode, unsigned int  *ProxyBlockMode);



typedef struct
{
  drv_io_open      pFuncOpn;
  drv_io_close     pFuncCls;
  drv_io_control   pFuncCtl;
  drv_io_write     pFuncWri;
  drv_io_read      pFuncRea;
}trOsalDrvFunc;



#include "dispatcher_defs.h"



/******************************************************************************
** #define
******************************************************************************/
#define  MQ_STD                                  1

#ifndef OSAL_C_S32_IOCTRL_ERRMEM_FLUSH
#define OSAL_C_S32_IOCTRL_ERRMEM_FLUSH         ((tS32)8)
#define OSAL_C_S32_IOCTRL_ERRMEM_SET_LOGLEVEL  ((tS32)9)
#endif
/*#ifndef OSAL_C_S32_IOCTRL_ERRMEM_SET_BE
#define OSAL_C_S32_IOCTRL_ERRMEM_STOP_BE 8
#define OSAL_C_S32_IOCTRL_ERRMEM_SET_BE  7
#endif*/

/* General use macros */
#define TIME_SLICE_DEFAULT          10

#define PRM_MAX_USB_DEVICES   9
#define PRM_MAX_CRYPT_DEVICES 2
#define PRM_SIZE_OF_UUID_MSG 64

#include "prm.h"

#undef OSAL_C_U32_THREAD_PRIORITY_HIGHEST
#undef OSAL_C_U32_THREAD_PRIORITY_NORMAL
#undef OSAL_C_U32_THREAD_PRIORITY_LOWEST
#define OSAL_C_U32_THREAD_NO_REALTIME_PRIORITY (0)
#define OSAL_C_U32_THREAD_PRIORITY_HIGHEST (99)
#define OSAL_C_U32_THREAD_PRIORITY_NORMAL  OSAL_C_U32_THREAD_NO_REALTIME_PRIORITY
#define OSAL_C_U32_THREAD_PRIORITY_LOWEST  ( 0)

#define SYSTEM_REG_PATH             "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/SYSTEM"
#define REG_KEY_MALLOC_RESET        "MALLOC_RESET_ACTIVE"

#define MQ_EVENT_NOTIFY   0xffffffff

#define  EN_MQ_POST_FULL     0       // Trace 100% full
#define  EN_MQ_POST_FULL90   1       // Trace 90% full
#define  EN_MQ_POST_FULL70   2       // Trace 70% full
#define  EN_MQ_POST_OVERFLOW 3       // 

#define TRACE_NAME_SIZE 16

// defines for Profiling
#define OSAL_CORE_DATA_PROC     0x0a
#define OSAL_CORE_DATA_EV       0x0b
#define OSAL_CORE_DATA_SEM      0x0c
#define OSAL_CORE_DATA_TIM      0x0d
#define OSAL_CORE_DATA_MEMPOOL  0x0e
#define OSAL_CORE_DATA_MQ       0x0f
#define OSAL_CORE_DATA_MTX      0x10
#define OSAL_CORE_DATA_SHMEM    0x11
#define OSAL_TSK_CONNECT_TO_MQ  0x12
#define OSAL_TSK_DISCONNECT_MQ  0x21
#define OSAL_TSK_DELETE_MQ      0x22

#define OSAL_TSK_SEM_INFO       0xba
#define OSAL_TSK_MTX_INFO       0xbb
#define OSAL_TSK_FLG_INFO       0xbc
#define OSAL_TSK_FLG_INFO2      0xbf
#define OSAL_TSK_FLG_INFO3      0xbe
#define OSAL_TSK_SHMEM_INFO     0xbd
#define OSAL_TSK_TIM_INFO       0xbe

#define OSAL_TIM_SET_INFO       0xc1
#define OSAL_TIM_GET_INFO       0xc2

#define CREATE_OPERATION        1
#define OPEN_OPERATION          2
#define CLOSE_OPERATION         3
#define DELETE_OPERATION        4
#define WAIT_OPERATION          5
#define POST_OPERATION          6
#define STATUS_OPERATION        7
#define WAIT_OPERATION_STEP     8
#define MAP_OPERATION           9
#define UNMAP_OPERATION         10

#define OSAL_PS                 0x0a
#define OSAL_CORE_DATA_HEAP     0x09
#define OSAL_CORE_DATA_MEM      0x08
#define OSAL_TOP                0x07
#define OSAL_PRC_MEM            0x06
#define OSAL_PRC_MEM2           0x05
#define OSAL_SET_SHELL_CMD      0x2a

#define OSAL_CORE_RESOURCE_DATA 0x12
#define OSAL_COPY_DIR           0x14
#define OSAL_MK_DIR             0x15
#define OSAL_RM_DIR             0x16
#define OSAL_COPY_FILE          0x17
#define OSAL_RM_FILE            0x18
#define OSAL_READ_DIR           0x19
#define OSAL_READ_FILE          0x26
#define OSAL_RM_SELECTED_FILES  0x20
#define OSAL_DIR_CONTENT_SIZE   0x11
#define OSAL_FB_CHECK           0x21

#define OSAL_ERROR_TIMER_DURATION 0x36

#define OSAL_TT_FOR_FULL_MQ     0x3a

#define OSAL_TIMER_ALREADY_STOPPED       0x70

#define OSAL_MQ_BLOCKED         0x77
#define OSAL_MQ_BLOCKED_RELEASE 0x78
#define OSAL_MQ_SEND            0x76
#define OSAL_MQ_RECEIVE         0x79
#define OSAL_MQ_CALLBACK        0x7a
#define OSAL_MQ_REGISTER        0x7b
#define OSAL_MQ_CREATE          0x7c
#define OSAL_MQ_PRC_TIME        0x7d
#define OSAL_MQ_STATUS          0x7e
#define OSAL_CORE_TRACE_MQ      0x5f
#define OSAL_CORE_TRACE_SEM     0xa0
#define OSAL_CORE_TRACE_FLG     0xa1
#define OSAL_CORE_TRACE_TIM     0xa2
#define OSAL_GET_MSGQPOOL_INFO  0xa3
#define OSAL_CORE_TRACE_SHMEM   0xa4
#define OSAL_CORE_TRACE_TIM_PRC 0xa5
#define OSAL_CORE_TRACE_MTX     0xa6

#define OSAL_MQ_SET_SV_CCA      0xb0
#define OSAL_MQ_SET_FILTER_CCA  0xb1
#define OSAL_SET_OSAL_STRACE    0xb3

#define OSAL_SET_ASSERT_MODE    0x51
#define OSAL_GET_ASSERT_MODE    0x52
#define OSAL_TEST_ASSERT        0x53

#if !defined (OSAL_GEN3_SIM)
enum
{
   OSAL_USB_UV_ALLACTIVE = 0x56,
   OSAL_USB_UV_ALLINACTIVE = 0x57,
   OSAL_USB_UV_ACTIVE = 0x58,
   OSAL_USB_UV_INACTIVE = 0x59,
   OSAL_USB_OC_ACTIVE = 0x60,
   OSAL_USB_OC_INACTIVE = 0x61,
   OSAL_USB_SIG_UNDEF = 0x62
};
#endif

#define OSAL_TRACE_SYSTEM_REG   0x12
#define OSAL_TRACE_SYSTEM_REG_PRC  0x13

#define MSGPOOL_BLOCKS          0x60
#define MSGPOOL_ABS_SIZE        0x61
#define MSGPOOL_CUR_SIZE        0x62
#define MSGPOOL_MIN_SIZE        0x63
#define MSGPOOL_MAX_SIZE        0x64
#define MSGPOOL_SIZE            0x65
#define MSGPOOL_CREATE_FAILED   0x66
#define MSGPOOL_NOT_CREATED     0x67

#define OSAL_START_PROC         0x1a
#define OSAL_CORE_DATA_MQ_STAT  0x1f

#define OSAL_CORE_MQ_FILL_LEVEL 0x22

#define OSAL_EM_TRACE           0x27
#define OSAL_EM_TRACE2          0x30
#define OSAL_EM_TRACE_EXTENDED  0x2D
#define OSAL_EM_TRACE_TO_MS     0x2E
#define OSAL_EM_ERASE           0x28
#define OSAL_SET_DEV_TRACE      0x29
#define OSAL_STACK_CHECK        0x6B

#define PRM_INFO                0x69
#define RESOURCE_INFO           0x6a

#define OSAL_SPAWN_LOAD_TASK      0x70
#define OSAL_KILL_LOAD_TASK       0x71
#define OSAL_GET_LOAD_TASK_STATUS 0x72

#define OSAL_REDUCE_MEM           0x76
#define OSAL_BLOCK_CPU            0x78
#define OSAL_CHECK_SIGNAL         0x79
#define OSAL_CALLSTACKS_BY_NAME   0x80
#define OSAL_CALLSTACKS_BY_PID    0x81

/*SPM volt trace command*/
#define OSAL_SPM_VOLTAGE          0x1
#define OSAL_SPM_CURRENT          0x2

#define OSAL_MANUAL             0xf0
#define OSAL_STRING_OUT         0xf1

#define OSAL_GET_OSAL_PROC_INFO 0x77


#define PRC_TYPE_USERPRC 3
#define OSAL_SET_EMPTY_POOL_CHECK  0x76

#define PRM_TASK_START            0x69
#define PRM_TASK_END              0x6A
#define PRM_NR_USB_DEV            0x70


#define BINDCPU0 "bindcpu0"
#define BINDCPU1 "bindcpu1"
#define CPU0 0
#define CPU1 1

#define OSAL_C_LONG_MSG_TIME_ARR_SIZE 5   //added macro sja3kor
#define OSAL_C_MSG_TIME_ARR_SIZE 10


enum {
EN_SHARED_KDS =0,
EN_SHARED_SENSOR,
EN_SHARED_OD,
EN_SHARED_WUP,
EN_SHARED_VOLT,
EN_SHARED_EOL ,
EN_SHARED_ADR3,
EN_CRYPTMODUL,
EN_SHARED_ACOUSTIC,
EN_SHARED_OSAL2,
EN_SHARED_TOUCH,
EN_SHARED_BASE,
EN_SHARED_AARS_DAB,
EN_SHARED_AARS_SSI32,
EN_SHARED_AARS_MTD,
EN_SHARED_AARS_AMFM,
EN_SHARED_LAST 
};


/******************************************************************************
** Prototypes of global structures
******************************************************************************/


/*************************** Processes *****************************************/
typedef tS32 (*tOSAL_s32PrcStart)(tS32 , tString []);

/*************************** THREADS *****************************************/
/* Thread Table Entry */
typedef struct trThreadElement
{
   tChar   szName[OSAL_C_U32_MAX_NAMELENGTH];  /* OSALNAME of Thread */
   tU32    u32Priority;                        /* priority not relevant in current Linux implementation*/
   OSAL_tMSecond  tStartTime;                  /* how many milliseconds after system start the thread is started*/
   tU32    u32TaskStack;                       /* stack size minimum size is set to 64kb */

   /* Variables used for error management    */
   tU32    u32ErrorCode;
   tU32    u32ErrorHooksCalled;
   OSAL_tpfErrorHook pfErrorHook;

    /* Auxiliary flags for auxiliary s32Status  */
   tBool     bIsSetReadyStatus;
   tBool     bIsSetSuspendedStatus;
   tBool     bMarkedDeleted;
   tBool     bIsUsed;

   OSAL_tpfThreadEntry pfOsalEntry;        /* application task implementation */
   tPVoid  *pvArg;                         /* parameter for starting task */
   void* (*pfTaskEntry)(void*);            /* OSAL task wrapper function for each task */
   pthread_t                  tid;        /* the ID the pthread library uses */
   tS32                       pid;         /* the "task group id" (TGID) on Linux */
   tU32                       kernel_tid;  /* the ID the OS scheduler uses */
   sem_t                      hSemHandle;  /* lock to sync startup of thread create->activate */
   sem_t                      hTempSemHandle;
   pthread_cond_t             cond;        /* condition variable for following mutex variable */
   pthread_mutex_t            hMutex;      /* mutex for suspend and resume */
   tS32                       s32Index;
}  trThreadElement;


/* Thread Table */
typedef struct tThreadTable
{
   tU32              u32UsedEntries;
   tU32              u32MaxEntries;
   trOsalLock*       prLock;
}  tThreadTable;


typedef struct {
    tU32       type;   /* Message type (MS_SYS5) */
    tU32       size;   /* Message size */
    void*   pArg;                   /*  */
    OSAL_tpfCallback pCallBack;   /*  */
} CallbackNotifyMsg;

/*************************** EVENTS ******************************************/
/* Event Table Entry */
typedef struct trEventElement
{
   tU32                  u32EventID;  /* OSAL internal Event handle ID */
   tBool                 bIsUsed;     /* Flag indicates structure is in use */
   tBool                 bToDelete;   /* Flag indicates event should be deleted */
   tU16                  u16OpenCounter; /* open counter */
   tS32                  s32PID;      /* event creation PID */
#ifdef SEM_EV
   tU32                  hLock;        /* internal lock for clearing event mask*/
   tU32                  hLocalLock;        /* internal lock for clearing event mask*/
#else
#ifdef SIGNAL_EV_HDR
   tS32                  s32RecProc;  /* event receiver process ID */
   tS32                  s32RecTsk;   /* event receiver task ID */
   sigset_t              rSet;        /* linux signal set used by the event */
   tBool                 bUpdate;     /* indicator post before receiver is connected */
   tBool                 bPost;     /* indicator post */
   tU32                  u32Index;    /* internal structure index*/
   tU32                  u32PrcSndMask;/* event mask send to other process*/
#else //EPOLL_EV
   tU32                  szFileName[OSAL_C_U32_MAX_NAMELENGTH + 17 /* "/tmp/OSAL_Events/" */];  /* name of the event */
   tU32                  hLock;        /* internal lock for clearing event mask*/
   tS32                  s32RecProc;  /* event receiver process ID */
   tS32                  s32RecTsk;   /* event receiver task ID */
   tBool                 bUpdate;     /* indicator post before receiver is connected */
   tU32                  u32Index;    /* internal structure index*/
   tU32                  u32Count;    /* pending events */
   tBool                 bPost;     /* indicator post */
   tPU32                 pu32Storage; /* pointer to intermediate storage */
   tU32                  u32StoredDataSets;/* number of sets in intermediate storage */
#endif
#endif
   tBool                 bWaitFlag;    /* in operation indicator */
   tU32                  u32EventBitField;/* current active bitmask for the event */
   tChar                 szName[OSAL_C_U32_MAX_NAMELENGTH+2];/* event name */
   tBool                 bTraceFlg;   /* flag for tracing operation of this element */
   tU32                  u32PostMask; /* for trace analysis */
   tU32                  u32PostFlag; /* for trace analysis */
   tU32                  u32WaitMask;  /* mask of the receiving task */
   tU32                  u32WaitEnFlags;/* flags of the waiting task */
   tU32                  u32Option;
} trEventElement;

/* event handle given to application */
typedef struct
{
    trEventElement* pEntry;   /* pointer to the related event object*/
    tS32            s32Tid;   /* user task id */
#ifdef EPOLL_EV
    tS32            fifo_r;
    tS32            fifo_w;
#endif
}tEvHandle;

/* Event Table */
typedef struct
{
   tU32              u32UsedEntries;
   tU32              u32MaxEntries;
   trOsalLock        rLock;
#ifdef SIGNAL_EV_HDR
   trOsalLock        rPrcLock;
#endif
}  tEventTable;

/* Semaphore Table Entry */
typedef struct trSemaphoreElement
{
   tU32  u32SemaphoreID;/* OSAL internal semaphore handle ID */
   tU16  u16OpenCounter;/* element open counter */
   tU16  u16SemMaxValue;/* max counter value for semaphore*/ 
   tU32  hSemaphore;    /* linux semaphore handle*/
   tBool bIsUsed;       /* Flag indicates structure is in use */
   tBool bToDelete;     /* Flag indicates semaphore should be deleted */
   tU16  u16SemValue;  /* semaphore counter value*/
   tBool bTraceSem;    /* flag for tracing operation of this element */
   tBool bCheckSem;    /* flag supervison of this element */
   tS32  s32PID;       /* owner of the semaphore */
   tS32  s32TID;       /* owner of the semaphore */
   tChar szName[OSAL_C_U32_MAX_NAMELENGTH+2];/* semaphore name */
   tU16  u16Option;
   tU32 u32LockCount;
} trSemaphoreElement;

/*Semaphore Table*/
typedef struct
{
   tU32               u32UsedEntries;
   tU32               u32MaxEntries;
   trOsalLock         rLock;
}  tSemaphoreTable;

/* Semaphore handle given to application */
typedef struct
{
    trSemaphoreElement* pEntry;   /* pointer to the related event object*/
    tS32                s32Tid;   /* user task id */
    tS32                s32Cnt;   /* user task id */
}tSemHandle;


/* Mutex Table Entry */
typedef struct trMutexElement
{
   tU32                u32MutexID;    /* OSAL internal Mutex handle ID */
   tU16                u16OpenCounter;/* element open counter */
   pthread_mutex_t     hMutex;        /* linux Mutex handle*/
   pthread_mutexattr_t mutexattr;
   tBool            bIsUsed;       /* Flag indicates structure is in use */
   tBool            bToDelete;     /* Flag indicates Mutex should be deleted */
   tBool            bTraceMut;     /* flag for tracing operation of this element */
   tBool            bCheckMut;     /* flag supervison of this element */
   tS32             s32PID;        /* owner of the Mutex */
   tS32             s32TID;        /* owner of the Mutex */
   tChar            szName[OSAL_C_U32_MAX_NAMELENGTH];/* Mutex name */
   tU16  u16Option;
   tU32 u32LockCount;
} trMutexElement;

/*Mutex Table*/
typedef struct
{
   tU32               u32UsedEntries;
   tU32               u32MaxEntries;
   trOsalLock         rLock;
}  tMutexTable;




 /* Timer Table Entry */
typedef struct trTimerElement
{
  tU32              u32TimerID; /* OSAL timer magic */
  tU32              u32Timeout; /* timeout for first timer elapse time*/
  tU32              u32Interval;/* intervall when timer should expire periodically*/
  timer_t           hTimer;     /* Linux timer ID */
  tS32              idTask;     /* task which set up the timer */
  tS32              Pid;        /* process which set up the timer */
  OSAL_tpfCallback pfOsalCallback;/* callback function connected to the timer*/
  tPVoid pvArgOfOsalCallback;  /* Parameter for callback function*/
  tBool             bTraceOn;  /* flag for tracing operation of this element */
  tBool             bIsUsed;   /* flag for timer is in use*/
  tBool             bIsSetting;/* flag if timer is activated*/
} trTimerElement;

/*Timer Table*/
typedef struct
{
   tU32              u32UsedEntries;
   tU32              u32MaxEntries;
   trOsalLock        rLock;
}  tTimerTable;



/* Mqueue Table Entry */
typedef struct MsgHdr
{
   uintptr_t NextIndex;
   tU32      msgsize;
   tU8       Message[1];
} MsgHdr;

typedef struct
{
   tU16             u16OpenCounter;
   tU16             u16WaitCounter;
   tU32             MaxLength;
   tU32             ActualMessage;
   tBool            bToDelete;
   tU32             u32RecTsk;
   tU32             u32RecPrc;
   tS32             s32LiHandle;
   tU32             MaxMessages;
   tU32             u32Marker;
#ifdef OSAL_SHM_MQ
   intptr_t         PrioMsgListOffset[OSAL_C_U32_MQUEUE_PRIORITY_LOWEST+1];
   tU32  u32RBlock;
   tU32  u32WBlock;
#endif
   tBool            bBlocked;
   tU32             u32MsgPrcTim[OSAL_C_MSG_TIME_ARR_SIZE];
   tU32             u32MsgTimStamp[OSAL_C_MSG_TIME_ARR_SIZE];
   tU32             u32MsgCount[OSAL_C_MSG_TIME_ARR_SIZE];
   tU32             u32IdxMPT;
   tU32             u32MessagePrcessing;
   tU32             u32LongMsgPrcTim[OSAL_C_LONG_MSG_TIME_ARR_SIZE];
   tU32             u32LongMsgTimStamp[OSAL_C_LONG_MSG_TIME_ARR_SIZE];
   tU32             u32IdxLMPT;
} trStdMsgQueue;

enum
{
	MEASUREMENT_SWITCHOFF,
	MSG_PROCESSING_ACTIVE,
	MSG_PROCESSING_INACTIVE
};

typedef struct trMqueueElement
{
   tU32                    u32MqueueID;
   tBool                   bIsUsed;
   tBool                   bTraceCannel;
   tU16                    u16Type;
   tU32                    u32EntryIdx;
   tS32                    callbacktid;
   tS32                    callbackpid;
   tS32                    callbackpididx;
   tBool                   bTriggerCb;
   tBool                   bNotify;
   OSAL_tpfCallback        pcallback;
   tPVoid                  pcallbackarg;
   tU32                    u32BFD_Count;
#ifdef USE_EVENT_AS_CB
   OSAL_tEventHandle       hEvent;
   OSAL_tEventMask         mask;
   OSAL_tenEventMaskFlag   enFlags;
#endif
   tBool                   bOverflow;
   tU32                    u32Timeout;
   tBool                   bSvActive;
   tS32                    s32PID;      /* event creation PID */
#ifdef MAX_MSG_COUNT
   tU32                    u32Counter;
   tU32                    u32MaxMsgCount;
#endif
   tChar                   szName[OSAL_C_U32_MAX_NAMELENGTH+2];
} trMqueueElement;

/*Mqueue Table*/
typedef struct
{
   tU32               u32UsedEntries;
   tU32               u32MaxEntries;
   trOsalLock         rLock;
}  tMqueueTable;

typedef struct
{
   tU32                    u32Magic;
   OSAL_tenAccess          enAccess;
   tS32                    s32TaskId;
   struct trMqueueElement  *pOrigin;
#ifdef OSAL_SHM_MQ
   void*               pAdress;
   OSAL_tShMemHandle   hShMem;
   OSAL_tSemHandle     hLockSem;
   OSAL_tEventHandle   hEvent;
#endif  
}  tMQUserHandle;

/* Shared Memory Table Entry */
typedef struct trSharedMemoryElement
{
   tU16                  u16OpenCounter;/* open count for this element*/
   tBool                 bIsUsed;       /* indicator elemnt is in use*/
   tBool                 bIsMapped;     /* flag for tracing this element*/
   tBool                 bToDelete;     /* flag element should be deleted when possible*/
   tPVoid                pvSharedMemoryStartPointer;/* start adress generated in the creation process*/
   tU32                  u32SharedMemoryID;/* OSAL shared memory ID*/
   tU32                  u32SharedMemorySize; /* size of the memory*/
   OSAL_tShMemHandle     hShared;        /* Linux shared memory ID*/
   tS32                  s32PID;         /* Shared memory creation PID */
   tChar                 szName[OSAL_C_U32_MAX_NAMELENGTH+2];/* name of the shared memory*/
} trSharedMemoryElement;

/*Shared Memory Table*/
typedef struct
{
   tU32                     u32UsedEntries;
   tU32                     u32MaxEntries;
   trOsalLock               rLock;
}  tSharedMemoryTable;



   /* Information struct for the system load task 'bremse.exe */
    /* Added by Resch, Carsten */
    typedef struct _tag_SystemLoadTaskInfo{
        tBool          b_entry_used;        /* flag to indicate if the task is spawned */
        tChar          szTaskName[32];      /* Task name OSALLoadTask00-20 */
        tInt           n_priority;          /* Tasks priority */
        tInt           n_system_load;       /* the target system load to consume */
        tInt           n_sleep_duration;    /* the duration of sleep */
        OSAL_tThreadID n_thread_id;         /* threads OSAL ID */
        tUInt          ui_awake_time;       /* the real awake time */
        tUInt          ui_sleep_time;       /* the real sleep time */
        tBool          b_terminate_task;    /* if the task should be terminated */
    }OSAL_trSystemLoadTaskInfo;
    
#define OSAL_LOAD_TASK_BASE_NAME         ((tCString) "OSALLoadTask")
#define OSAL_MAX_NUM_SYS_LOAD_TASKS     ((tU16) 20)


/*************************** TRACES ******************************************/
/* traces of OSAL Core*/
enum
{
   EN_MQ_POST,
   EN_MQ_RESULT,
   EN_PROC_STACK_ERROR,
   EN_MEM_ALLOC,
   EN_MEM_FREE,
   EN_MEM_DUMP,
   EN_MEM_PROF_DUMP
};


enum
{
   EN_REG_OPENDIR_ERROR,
   EN_REG_GETVAL_ERROR,
   EN_REG_GETSTR_ERROR,
   EN_REG_GET_ERROR,
   EN_REG_OPEN_DEV,
   EN_REG_CLOSE_DEV
};



enum
{
  FUNC_OSAL_SET_ERROR_CODE,
  FUNC_OSAL_GET_ERROR_CODE,
  FUNC_OSAL_FIRST_THREAD
};


  typedef enum {
       OSAL_EN_THREAD_INITIALIZED,         /* initialized */
       OSAL_EN_THREAD_RUNNING,             /* running */
       OSAL_EN_THREAD_READY,               /* ready */
       OSAL_EN_THREAD_PENDED,              /* pended */
       OSAL_EN_THREAD_DELAYED,             /* delayed */
       OSAL_EN_THREAD_SUSPENDED,           /* suspended */
       OSAL_EN_THREAD_SUSPENDED_PENDED,    /* suspended+pended */
       OSAL_EN_THREAD_SUSPENDED_DELAYED,   /* suspended+delayed */
       OSAL_EN_THREAD_PENDED_DELAYED,      /* CE: Sl/Blk */
           OSAL_EN_THREAD_INVALIDSTATUS        /* added for WinCE */ 
   } OSAL_tenThreadState;

typedef enum /* attention this enum has to be aligned to TE enum in Gen2*/
 {
     MBX_INVALID,
     MBX_SUS_TSK,
     MBX_TERM_END,
     MBX_TERM_TSK,
     MBX_ASSERT_SYS,
     MBX_CB_PRM,
     MBX_MQ_NOTIFY,
     MBX_CB_PRM2,
     MBX_CB_ARG,
     MBX_CB_U32PARM, // Callback used for tU32 paramater as the Argument
     MBX_CB_TSK_RENICE,
     MBX_GEN_CS,
     MBX_GEN_CS_USR2,
     MBX_STRACE,
     MBX_GEN_TOP,
     MBX_RB_NOTIFY,
  }eMsgTypes;

#define OSAL_MAGIC   0x79836576

typedef struct
{  tU32  Cmd;
   tU32  ID;
   tU32  Res1;
   OSAL_tpfCallback pFunc;
   void* pArg;
}tOsalMsg;

typedef struct
{
   tU32  Cmd;
}trMsgHeader;

typedef struct
{
   trMsgHeader     rHead;
   char            szName[16];
   OSAL_trTimeDate rTime;
}trTimeMsg;


typedef struct
{
   trMsgHeader rHead;
   tU32 u32Pid;         // process id
   uintptr_t u32CallFun;     // function pointer
   tU32 u32Param1;      // aditional param, like insert, eject, ... or old one param format
   tU32 u32Param2;      // aditional param, like insert, eject, ... or old one param format
   tU8  au8UUID[ PRM_SIZE_OF_UUID_MSG ];   // uuid of device
}trPrmCBMsg;

// For MBX_CB_ARG - a message with a small structure as argument
// Maximum size of payload is currently set to a value that doesn't exceeed the size of a trPrmCBMsg
#define OSAL_MBX_CB_ARG_MAX_SIZE    (sizeof(trPrmCBMsg) - 2 * sizeof(tU32) - sizeof(trMsgHeader))
typedef struct
{
    trMsgHeader rHead;
    tU32        u32Pid;     // process id
    uintptr_t        u32CallFun; // function pointer
    tU8         au8Arg[OSAL_MBX_CB_ARG_MAX_SIZE];
}trCBArgMsg;
/* Used for MBX_CB_U32PARM: The callback handling for u32parameter as the Argument*/
/* Callback used for tU32 paramater as the Argument*/
typedef void (*OSAL_tu32ParmCallbackFunc) (tU32 u32Data);
typedef struct
{
    trMsgHeader rHead;
    tU32        u32Pid;     // process id
    uintptr_t        u32CallFun; // function pointer
    tU32        u32Param1; // Paramater for ADR3 State
}tru32ParmCBMsg;

typedef union
{
   tOsalMsg    rOsalMsg;
   trPrmCBMsg  rPrmMsg;
   trCBArgMsg  rCBArgMsg;
   tru32ParmCBMsg ru32ParmCBMsg;
}tOsalMbxMsg; 

typedef struct
{
   char szMqName[OSAL_C_U32_MAX_NAMELENGTH];
   tU32 u32Pid;
   tU32 u32Tid;
   tU32 u32Priority;
   trOsalLock rAsyncIOLock;
   char pu8AppName[256];               /* path to executable */
   char pu8CommandLine[256];
   tS32 exitCode;
} trProcessInfo;

typedef struct
{
   tU32          u32UsedEntries;
   tU32          u32MaxEntries;
   trOsalLock    rLock;
} tResourceTable;



#define MAX_BOSCH_CONF_APP   10
typedef struct{
  char szAppName[OSAL_C_U32_MAX_NAMELENGTH];
}trBoschExcHdrApps;


typedef struct{
 OSALCALLBACKFUNC  pNotifyCallback;           /* Callback function                   */
 tPU32             pNotifyu32Data;            /* parameter of the callback function  */
 tS32              s32NotifyPrcId;
}trPrmSysStatus;

#define MSG_HANDLE_BUFFER  50
typedef struct
{
   tBool             bCreated;
   tBool             bCheck;
   tU32              u32InvestigatePool;
   tBool             bLogPidTid;
   tU32              u32MemSize;
   tU32              u32HandleCount;
   OSAL_trMessage    rHandleArray[MSG_HANDLE_BUFFER];
} OSAL_MSG_POOL_tStructure;

typedef struct
{
   char cShmName[16];
   tU32 u32Size;
   OSAL_tShMemHandle ShmHandle[2];
   tS32 s32Pid[2];
   void* pAdress[2];
}trLargeCcaMsg;

typedef struct{
 tU32 u32MaxEvtResCount;
  tU32 u32MaxMqResCount;
  tU32 u32MaxTskResCount;
  tU32 u32MaxSemResCount;
  tU32 u32MaxTimResCount;
  tU32 u32MaxShMResCount;
}trOsalPramDatat;

typedef struct{
  char cActive;
  tS32 s32Pid;
}trDevTrace;

typedef struct
{
   char  cLibraryNames[256];
}trLibrary;

typedef struct{
  uintptr_t StartAdress; /* Shared memory start address including tOsalRgBufDat */
  uintptr_t MemStart;
  uintptr_t MemEnd;
  OSAL_tShMemHandle hShMem;
  OSAL_tSemHandle   hLockSem;
  OSAL_tEventHandle hEvent;
  tU32      u32Magic;
  OSAL_tenAccess enAccess;
}tOsalRgBufHandle;

typedef struct{
  tU32  u32Size;        /* requested Size */
  tU32  u32ReadOffset;
  tU32  u32WriteOffset;
  tU32  u32MemFree;
  tU32  u32OpnCnt;
  tU32  u32ActualMessage;
  tU32  u32RBlock;
  tU32  u32WBlock;
  tU32  u32RBlockPost;
  tU32  u32WBlockPost;
  tChar szName[OSAL_C_U32_MAX_NAMELENGTH+2];
  
  tS32                    callbackpid;
  tS32                    callbackpididx;
  tBool                   bNotify;
  OSAL_tpfCallback        pcallback;
  tPVoid                  pcallbackarg;
}tOsalRgBufDat;


typedef struct
{
#ifdef PROTECTED_OSAL_AREAS
  char __attribute__((aligned(PROTECTED_PAGE_SIZE))) OsalProtAreaStart[PROTECTED_PAGE_SIZE];
#endif
  /* area of variables not changed during runtime, protected after load of the first process */
  tU32  u32MagicStart;
  tU32  u32OsalStartTimeInMs;  /* variable for measurement */
  tU32  u32OsalStartTime2InMs; /* variable for measurement */

  tU32  u32ConfigCheckPid;         /* process ID who read the osal.reg */
  tU32  u32ExcStatus;              /* flag to activate OSAL exception handle for all processes set via osal.reg*/
  tBool bFifoReady;                /* flag to indicate installed signal handler fifo */
  tBool bLogError;                 /* flag to log all critical OSAL API errors to errmem set via osal.reg  */
  tBool bUseOsalCsGen;             /* flag for call stack generation in OSAL on single core CPU */
  tU32  u32UseLibunwind;           /* flag for libunwind is used always for call stack generation set via osal.reg  */
  
  tU32 u32RegistryMemSize;         /* size of registry memory set via Linux environment */
  tU32 u32RegistryLookUpMemSize;   /* size of registry memory additional used for lookup functionality */
  tU32 u32OsalResMemSize;          /* size of OSAL resource shared memory  */
  tU32 u32MaxNrLockElements;       /* max resource count set via osal.reg  */

  tU32 u32MaxNrEventElements;      /* max resource count set via osal.reg  */
  tU32 u32MaxNrThreadElements;     /* max resource count set via osal.reg  */
  tU32 u32MaxNrSharedMemElements;  /* max resource count set via osal.reg  */
  tU32 u32MaxNrSemaphoreElements;  /* max resource count set via osal.reg  */
  tU32 u32MaxNrMutexElements;      /* max resource count set via osal.reg  */
  tU32 u32MaxNrTimerElements;      /* max resource count set via osal.reg  */
  tU32 u32MaxNrMqElements;         /* max resource count set via osal.reg  */
  tU32 u32MaxNrProcElements;       /* max resource count set via osal.reg  */

  tU32 u32OffsetEventElements;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetThreadElements;    /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetNodeElements;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetRbTreeData;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSharedMemElements; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSemaphoreElements; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMutexElements;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetTimerElements;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMqElements;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetStdMqElements;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetProcElements;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetLockElements;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetThreadLock;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSemHandlePool;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMqHandlePool;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetEventHandlePool;   /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetDevDescHandlePool; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetFileDescHandlePool;/* offset for data in OSAL resource shared memory */
  tU32 u32OffsetDataEnd;

#ifdef AREA_TEST
  tU32 u32OffsetEventElementsEnd;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetThreadElementsEnd;    /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetNodeElementsEnd;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetRbTreeDataEnd;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSharedMemElementsEnd; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSemaphoreElementsEnd; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMutexElementsEnd;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetTimerElementsEnd;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMqElementsEnd;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetStdMqElementsEnd;     /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetProcElementsEnd;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetLockElementsEnd;      /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetThreadLockEnd;        /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetSemHandleElementsEnd; /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetMqHandleElementsEnd;  /* offset for data in OSAL resource shared memory */
  tU32 u32OffsetEventHandleElementsEnd;/* offset for data in OSAL resource shared memory */
  tU32 u32OffsetDevDescHandleElementsEnd;/* offset for data in OSAL resource shared memory */
  tU32 u32OffsetFileDescHandleElementsEnd;/* offset for data in OSAL resource shared memory */
#endif
  char szSTraceProcess[64];        /* process name for OSAL STRACE supervision of specific process set via osal.reg */
  tU32 u32STraceLevel;             /* filter value for STRACE */

  /* osalmsg*/
  tU32 u32MsgPoolSize;             /* size for CCA message pool set via osal.reg */
  tU32 u32DynMmapMsgSize;          /* CCA message size for separate shared memory */

  trLibrary rLibrary[EN_SHARED_LAST];/* path to OSAL driver package shared libraries set via osal.reg */

  tU32 u32ClkMonotonic;           /* special flag for clock monotonic behavior on OSAL MQ or semaphore set via osal.reg */

  tU32 u32DeviceHandles;          /* pool size for device descriptor handle pool set via osal.reg */
  tU32 u32FileHandles;            /* pool size for file handle pool set via osal.reg */
  tU32 u32MqHandles;              /* pool size for message queue handle pool set via osal.reg */
  tU32 u32EventHandles;           /* pool size for event handle pool set via osal.reg */
  tU32 u32SemHandles;             /* pool size for semaphore handle pool set via osal.reg */

  // osalevent
  tBool bEvTaceAllWithoutTimeout;              /* special flage for event implementation with signals */
  char szEventName[OSAL_C_U32_MAX_NAMELENGTH]; /* event name for OSAL event supervision of specific event set via osal.reg */
 
  // osalproc
  char OsalMainApp[OSAL_C_U32_MAX_NAMELENGTH]; /* process name for OSAL main process (PRM, supervision etc.) set via osal.reg */
  tU32 u32CGroup;                              /* flag for cproup support in OSAL set via osal.reg */
  tU32 u32Prio;                                /* flag for priority support in OSAL -> nice level, RT etc. set via osal.reg */

  /* osalmqueue  */
  char szMqName[OSAL_C_U32_MAX_NAMELENGTH];    /* message queue name for OSAL message queue supervision of specific message queue set via osal.reg */
  tU32 u32NoMqBlockEntry;                      /* flag to avoid entries in errmem for blocked queue sceanrios */
  tU32 u32MqPostTmo;                           /* timeout for posting a message to a full message queue set by re*/
  tU32 u32MqResource;                          /* memory available for Linux message queues */
  
  // osalshmem
  char szShMemName[OSAL_C_U32_MAX_NAMELENGTH]; /* shared memory name for OSAL shared memory supervision of specific shared memory set via osal.reg */

  // osalsmphr
  char szSemName[OSAL_C_U32_MAX_NAMELENGTH];   /* semaphore name for OSAL semaphore supervision of specific semaphore set via osal.reg */
  char szCheckSemName[OSAL_C_U32_MAX_NAMELENGTH];/* semaphore name for OSAL semaphore supervision of specific semaphore timeout check set via osal.reg */
  tU32 u32CheckSemTmo;                         /* timeout for OSAL semaphore supervision of specific semaphore timeout check set via osal.reg */

  /* mutex */
  char szMtxName[OSAL_C_U32_MAX_NAMELENGTH];   /* mutex name for OSAL mutex supervision of specific mutex set via osal.reg */

  /* timer */ 
  tU32              u32TimSignal;                        /* signal number used for OSAL timer set via osal.reg */
  trBoschExcHdrApps rNoSigTimerApp[MAX_BOSCH_CONF_APP];  /* processes configured for not using signals for OSAL timer set via osal.reg */
  char              CheckTimerApp[OSAL_C_U32_MAX_NAMELENGTH];/* process name for OSAL timer supervision of specific process set via osal.reg */
  tU32              u32TimerResolution;                  /* timer resolution value determined at first OSAL process startup */
  tU32              u32UseOsalTime;                      /* setting time only in OSAL instead of System time*/
  
  /* exception handler */
  tU32               u32CatchSigchild;                    /* catch and evaluate SIGCHILD in special handler */
  trBoschExcHdrApps  rExitApp[MAX_BOSCH_CONF_APP];        /* processes where OSAL does not override exit handler set via osal.reg */
  trBoschExcHdrApps  rRtApp[MAX_BOSCH_CONF_APP];          /* processes where OSAL allow real time priorities set via osal.reg */
  trBoschExcHdrApps  rBoschExcHdrApps[MAX_BOSCH_CONF_APP];/* processes where OSAL take over exception handling set via osal.reg */

  /* osfile */  
  tU32 u32FsToLocal;                                      /* flag to buildup automatically folder structure used by OSAL file system devices set via osal.reg*/
  char szNavDatPath[MAX_MOUNTPOINT_STRING_SIZE];          /* path for configurable OSAL data device set via osal.reg */
  tU32  u32UpperCase;                                     /* flag for upper case work around needed by travel map navigation */

  /* OSAL IO */  
  char  szDevTrace[64];                                   /* OSAL device to be supervised set via osal.reg */;
  char  szErrMemDumpPath[MAX_MOUNTPOINT_STRING_SIZE];     /* path for error memory dump to file system other than dev media set via osal.reg */

  tU32 u32RecLockAccess;       /* flag for record also OSAL internal Lock status */
  tU32 u32StraceAll;                                      /* flag to switch on OSAL strace for all processes set via osal.reg*/
  
  tU32 u32AllowExit;                                      /* flag to allow exit calls from application as default behavior set via osal.reg*/
  tU32 u32OsalTrace;                                      /* flag to set OSAL intern trace solution as default behavior set via osal.reg*/
  
  tU32  u32TraceLevel;                                    /* trace level for OSAL internal trace set via osal.reg*/
  char  szPathTraceLog[256];                              /* path to trace log file of OSAL internal trace set via osal.reg*/
  tU32  u32WritToFile;                                    /* flag for different kind of logfile of OSAL internal trace set via osal.reg*/
  tU32  u32Class[50];                                    /* trace classes to be activated of OSAL internal trace set via osal.reg*/
  tU32  u32Level[50];                                    /* trace level to be activated for the classes of OSAL internal trace set via osal.reg*/
  tU32  u32NrOfClass;                                     /* number of activated trace classes for OSAL internal trace*/

  tS32 u32FirstPrc;
  tU32 u32MagicProtAreaMid;
#ifdef PROTECTED_OSAL_AREAS
  char __attribute__((aligned(PROTECTED_PAGE_SIZE))) OsalProtAreaMid[PROTECTED_PAGE_SIZE];
#endif
  tS32  ExhLockArea;           /* memory used for OSAL exception handler spinlock */
  tU32  u32MagicProtAreaMid2;
  /* area of variables changed during runtime */
  tU32  u32AttachedProcesses;  /* counter for processes loaded OSAL */
  tU32  u32AttachedProcesses2; /* counter for processes loaded OSAL addon*/
  tS32  u32ErrmemBE;           /* value for active errmem backend */
 
  trOsalLock  SyncObjLock;
  trOsalLock  PrmTableLock;
  trOsalLock  RegObjLock;

  /* exception handler */
  tS32  s32ResetRequestPid;    /* value for supervision task which process want a reboot*/
  tU32  u32ExcHdrActiv;        /* check for active exception processing */
  tU32  u32ExcTime;            /* value for supervison off blocked exception handler lock -> blocked Time*/
  tS32  s32ExcPid;             /* value for supervison off blocked exception handler lock -> blocking PID */
  tS32  s32ExcTid;             /* value for supervison off blocked exception handler lock -> blocking TID*/
  tBool bNoRebootCallstack;

  tU32  u32Magic1;
  // osalansi
  tU32 u32AssertMode;                    /* assert mode used for OSAL assert */

  // osalinit
  OSAL_tThreadID ThreadIDTerm;
  OSAL_tProcessID PidTerm;
  OSAL_tMQueueHandle TermMq;

  /* osalmsg */
  OSAL_MSG_POOL_tStructure rMsgPoolStruct; /* data of CCA message pool */
 
  // osaltime 
  tTimerTable       TimerTable;
  tBool             bTraceTimerAll;
  tU32              u32Seconds;          /* value of OSAL internal second timer */
  OSAL_tTimerHandle hSecTimer;           /* timer handle of OSAL seond timer */
  tS32              s32OsalInternOffset; /* When OSAL uses internal time different to system UTC */
  tS32              s32CheckTimPid;      /* PID for OSAL timer supervision of specific process set via osalproc.cpp*/
  
  tU32  u32Magic2;
  
  /* mutex */
  tMutexTable       MutexTable;
  tBool             bMutexTaceAll;
  
  /* osalsmphr */
  tSemaphoreTable     SemaphoreTable;
  tBool bSemTaceAll;
  tU32 u32WarnLevel;

  /* osalshmem */
  tSharedMemoryTable     SharedMemoryTable;
  tBool bShMemTaceAll;

  /* osalproc */
  tThreadTable     ThreadTable;
  tS32             s32MainOsalPrcPid;

  /* osalevent */
  tEventTable     EventTable;
  tBool bEvTaceAll;

  tU32  u32Magic3;
  
  // osalmqueue 
  tMqueueTable     MqueueTable;
  tBool bMqTaceAll;   // activate all traces on MQs
  tBool bCheckCcaMsg; // activate flag for CCA message supervision 
  tBool bTraceAllCca; // trace all CCA messages when bCheckCcaMsg is set 
  tBool bTraceFilteredCca;   // trace all CCA messages when bCheckCcaMsg is set 
  tU16  u16CheckSrc;  // trace all CCA messages when bCheckCcaMsg is set with related source ID
  tU16  u16CheckDst;  // trace all CCA messages when bCheckCcaMsg is set with related destination ID
  tU16  u16ServId;    // trace all CCA messages when bCheckCcaMsg is set with related service ID
  tU16  u16FuncId;
  tPU32 pu32Shared;
  tU32 u32OverflowPid;
  tU32 u32OverflowTid;

  tU32  u32Magic4;

  tU32 u32EvtResCount; // OSAL Resource counter
  tU32 u32MaxEvtResCount; // OSAL Resource counter
  tU32 u32MqResCount; // OSAL Resource counter
  tU32 u32MaxMqResCount; // OSAL Resource counter
  tU32 u32TskResCount; // OSAL Resource counter
  tU32 u32MaxTskResCount; // OSAL Resource counter
  tU32 u32PrcResCount; // OSAL Resource counter
  tU32 u32MaxPrcResCount; // OSAL Resource counter
  tU32 u32SemResCount; // OSAL Resource counter
  tU32 u32MaxSemResCount; // OSAL Resource counter
  tU32 u32MutResCount; // OSAL Resource counter
  tU32 u32MaxMutResCount; // OSAL Resource counter
  tU32 u32TimResCount; // OSAL Resource counter
  tU32 u32MaxTimResCount; // OSAL Resource counter
  tU32 u32ShMResCount; // OSAL Resource counter
  tU32 u32MaxShMResCount; // OSAL Resource counter

  tBool bPramActive;
  tU32  u32UpdateTime;
  tU32  u32NrOfChanges;

  /* CCA Message Pool */
  tU32  u32Open;            /* CCA pool open count */
  tU32  u32MmapMsgSize;     /* flag to use seperate shared memory when pool is out of memory */
  tU32  u32MmapMsgCnt;      /* counter for seperate shared memory of CCA messages */
  tU32  u32LostRegBytes;    /* wasted/overwritten registry bytes*/

  OSAL_tIODescriptor fderrmem;
  OSAL_tIODescriptor fddownloadlog; 

  tResourceTable ProcessTable;

  tU32  u32Magic5;
 
  //osfile
  OSAL_tenAccess enAccessSdCard;
  OSAL_tenAccess enAccessUsb[ PRM_MAX_USB_DEVICES +1 ]; //not used
  tBool bTraceAll;
  tBool bWriteProtected;
  tU16  u16DvdMediaType;
  tU16  u16CardMediaType1;
  tU16  u16SdCardMediaType;
  tBool bStopRecursiveDelete;

  // dispatcher_table
  tBool bIsInitialised; // Trip Replay init
  tBool bCdActive;
  
  tU32  u32Magic6;
  
  // PRM
  tU32  u32AmVersion;
  tU32  u32CryptcardId;
  trCallbackTabEntry arCallbackTable[ C_MAX_APPLICATIONS ];

  OSALCALLBACKFUNC  pSysNotifyCallback;           /* Callback function                   */
  tPU32             pSysNotifyu32Data;            /* parameter of the callback function  */
  tS32              s32SysNotifyPrcId;
  tS32              s32PrmMainPrcId;
  trPrmSysStatus    rPrmSysStatus[5];

  tU32  u32PrmAppId;
  tBool bRealCardIf;
  tBool bPrmRecTaskActiv;
  tBool bPrmUsbPwrActiv;
  tBool bTraceLevel4Active;
 
  tU32 u32PrmUSBGlobalPortCount;
 
  tU16  u16UsbMediaType[PRM_MAX_USB_DEVICES + 1];   // USB inserted, ejected, unknown
  tU32  u32Magic7;
  tU8   u8DeviceUUID[   PRM_MAX_USB_DEVICES + 1][ PRM_SIZE_OF_UUID_MSG ];
  tU32  u32Magic8;
  tU16  u16SdCardType;

  tU8   u8DeviceSdCardStatus;     // Sd-Card(No Crytnav folder)
  tU8   u8DeviceCryptCardStatus;  // Cryptcard Status
  tS32  s32UsbIdxForCard;      // 
 
  tU8   u8DeviceCryptnavStatus;        // dev/cryptnav
  tU16  u16CryptnavMediaType;
 
  tU8   u8DeviceDevMediaCount;   // dev/media connected device count
 
  tU32 u32LastLockEntry;

#ifdef PRM_LIBUSB_CONNECTION
 #define MAX_USB_PORTS      4
 UsbPortState prm_rPortState[MAX_USB_PORTS];
#endif

   trDevTrace rDevTrace[OSAL_EN_DEVID_LAST];
   tU32  u32MagicEnd;
#ifdef PROTECTED_OSAL_AREAS
   char __attribute__((aligned(PROTECTED_PAGE_SIZE))) OsalProtAreaEnd[PROTECTED_PAGE_SIZE];
#endif
} trGlobalOsalData;

extern trGlobalOsalData*  pOsalData;
extern void*              pOsalResData;

extern trEventElement*        pEventEl;
extern trThreadElement*       pThreadEl;
extern trElementNode*         pNodeEl;
extern trSharedMemoryElement* pShMemEl;
extern trSemaphoreElement*    pSemEl;
extern trMutexElement*        pMutexEl;
extern trTimerElement*        pTimerEl;
extern trMqueueElement*       pMsgQEl;
extern trStdMsgQueue*         pStdMQEl;
extern trProcessInfo*         prProcDat;
extern trOsalLock*            prOsalLock;
extern trRBTreeData*          prRBTree;
extern trOsalLock*            pThreadLock;

extern tS32 s32OsalGroupId;
extern trHandleMpf        DescMemPoolHandle;
extern trHandleMpf        FileMemPoolHandle;
extern trHandleMpf        MqMemPoolHandle;
extern trHandleMpf        EvMemPoolHandle;
extern trHandleMpf        SemMemPoolHandle;
extern PrcTskInfo         pPrcTskInfo;
extern char commandline[256];

extern trProcessInfo  *gpOsalProcDat;
extern tS32 s32ExcHdrStatus;
extern tU32 u32LocalTimerSig;
extern tS32 s32LocalCbHdrTid;
extern tS32 s32LocalTimerTid;
extern trOsalDrvFunc rOsalDrvFuncTable[OSAL_EN_DEVID_LAST];


extern OSAL_tMQueueHandle hMQCbPrc[MAX_LINUX_PROC];

extern tU32 u32PrcExitStep;
extern tS32 processIndex;
extern tU32 u32OsalSTrace;
extern tU32 minStackSize;
/* ---------------------------------------------------------*/
/* OSAL export for driver packages start                    */
/* ---------------------------------------------------------*/
/* OSAL AddOn API export */
OSAL_DECL void vSysCallbackHandler(void* pvBuffer );
OSAL_DECL void vOsalIoCallbackHandler(void* pvBuffer );
OSAL_DECL tU32 prm_u32Prm(OSAL_tIODescriptor rDevId, tS32 s32Fun, intptr_t s32Arg);
OSAL_DECL void vProcTaskInfo(char* pcPid, tBool bErrMemEntry);
OSAL_DECL void TraceCallbackexcute(OSAL_tProcessID pid, OSAL_tThreadID tid,tU8 u8MediumType);


OSAL_DECL tS32 s32StartCbHdrTask(tS32 s32Pid);
OSAL_DECL tS32 s32FindProcEntry(tS32 s32Pid);
OSAL_DECL OSAL_tMQueueHandle GetPrcLocalMsgQHandle(tU32 u32PrcId);
OSAL_DECL OSAL_tenFSDevID OSAL_get_drive_id( OSAL_tIODescriptor descriptor );
OSAL_DECL int ChangeSemRights(tCString SemString, tCString string);
OSAL_DECL tU32 u32LoadSharedObject(void* pFunc,char* szName);
OSAL_DECL void vSetDevTrace(char* pcPid, tCString coszName);
OSAL_DECL void vTraceOpenFiles(tBool flag);

OSAL_DECL void vCheckSignalMask(tU32 u32Sig,tCString szMaskType,tBool bPrintMask);


/* OSAL export for driver packages end                    */
/* ---------------------------------------------------------*/



void vSetErrorCode( OSAL_tThreadID, tU32);
void s32ProveAccess(tU32 u32Ctrl,tU16 u16AccessMode);
tS32 s32EnterErrMem(const char* pBuffer);
void vSetBasicFuncPointer(void);
void TraceIOString(tCString cBuffer);
void vCheckLibUnwind(void);

tBool bGetThreadNameForTID(char* pBuffer, tS32 s32Size, tS32 s32TID);
void vSendCallStackGenFromTerm(tS32 PID,int Signal);
int TriggerCallstacks(int Pid);
void PrintOsalLockStates(void);

void* vpMmapShmToPrc(tString cName,tU32 u32Size,tBool bCreate);
void vDelShMem(void* Adress,tString Name,tU32 u32Size);
tU32 u32UnmapDeletedShMem(void);

tS32 CreSyncObj(const char* cName,tU32 u32Init);
tS32 OpnSyncObj(const char* cName);
tU32 DelSyncObj(tS32 hLock);
tU32 WaiSyncObj(tS32 hLock, tU32 u32Timeout);
tU32 RelSyncObj(tS32 hLock);
tU32 GetSyncObjVal(tS32 hLock,tS32* ps32Val);
void vConvertMSectotimespec(struct timespec* pData,tU32 u32mSec);
void vWriteUtcTime(int fd);
tS32 s32CleanupThreadTable(tS32 s32Pid);
tU32 u32GemTmoStruct(struct timespec *tim,OSAL_tMSecond msec);

trThreadElement* CreatePseudoThreadEntry(tCString szFunc, tU32 u32ErrorCode);
tBool bCleanUpThreadofContext(tS32 pid);
OSAL_DECL tS32 s32GetFileSize(tS32 fd);

tS32 vAddProcessEntry(tU32 index);
tS32 tProcessTableGetFreeIndex(tVoid);
tS32 s32GetPidFromName(char* szName, tU32 u32Len);
void vTraceHeap(void);
tBool bRemount(char* Path,const char* pcOption);
tCString bGetPartitionInfo(tCString Path);

void vSaveOsalValToPram(void);
void vCheckSemTimeout(void* Val);
void vCheckMqTimeout(void* Val);
void SetupSupervisionTimer(void);
tS32 u32GetSigRtMinId(tS32 s32PrcId);
OSAL_DECL tU32 u32UsedSyncObj(void);
#define OSAL_MIN(a,b) ((a) < (b) ? (a) : (b))

OSAL_DECL tVoid OSAL_s32DefineModul(uint h, tPVoid pOpen, tPVoid pClose, tPVoid pIOControl, tPVoid pRead, tPVoid pWrite);

trThreadElement *tThreadTableSearchEntryByID(OSAL_tThreadID tid,tBool bLocked);
trThreadElement* tThreadTableGetFreeEntry(tVoid);

/*OSAL_DECL int futex_wakeup(int *futexp);
OSAL_DECL int futex_setwait(int *futexp,unsigned int msec, int mask);
OSAL_DECL int futex_wait(int *futexp);
OSAL_DECL int futex_post(int *futexp,int *Block);*/
#ifdef AREA_TEST
OSAL_DECL void vCheckAreas(void);
#endif

#ifdef __cplusplus
}
#endif

#else
#error Linux_osal.h included several times
#endif

