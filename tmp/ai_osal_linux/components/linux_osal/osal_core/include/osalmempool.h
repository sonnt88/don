/************************************************************************
| FILE:         osalmempool.h
| PROJECT:      platform
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for MemPool definitions
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 10.11.09  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (MEMPOOL_HEADER)
   #define MEMPOOL_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#define FREEMARKER      0xCFCFCFCF /* POOL Magic */



/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef struct trHandleMpf /* process local structure */
{
    tS32                hShMem;           /* handle of shared memory */
    void*               pMem;             /* start adress of allocated memory */
    sem_t*              Sem;              /* semaphore for pool locking */
    uintptr_t           u32memstart;      /* start adress of pool memory */
    uintptr_t           u32memend;        /* end adress of pool memory */
}trHandleMpf;


typedef struct trOSAL_MPF /* system global structure */
{
  tU32        mpfcnt;     /* NumbtS32 of blocks in whole memory pool */
  tU32        blfsz;      /* Fixed size memory block size (byte) requested + 3*tU32 */
  tU32        freecnt;    /* number of free elements */
  tU32        u32MaxCount;/* maximum number of used elements */
  tU32        u32CurCount;/* current number of used elements */
  tU32        u32ErrCnt;  /* number of mallocs used in case of empty pool */
  tU32        u32OpenCnt; /* open count of the pool */
  tU32        u32nextIdx; /* internal offset index */
  char        szName[32]; /* pool name */
  tBool       bLock;      /* flag if a seperate lock has to beused */
  tBool       bMalloc;    /* flag if malloc �should used in case of empty pool*/
} trOSAL_MPF;


/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************/
/* function for global memory pool configuration                        */
/************************************************************************/

/************************************************************************/
/* function for using specific memory pools                             */
/************************************************************************/
OSAL_DECL tS32 OSAL_u32MemPoolFixSizeCreate(tCString coszName, 
                                  tU32 u32MpfBlockSize,
                                  tU32 u32MpfBlockCnt,
                                  trHandleMpf* pHandle,
                                  tBool bLock,
                                  tBool bMalloc);
OSAL_DECL tS32 OSAL_u32MemPoolFixSizeOpen(tCString coszName, 
                                trHandleMpf* pHandle);

OSAL_DECL tS32 OSAL_s32MemPoolFixSizeDelete (tCString szName);
OSAL_DECL tS32 OSAL_s32MemPoolFixSizeClose (trHandleMpf* pHandle);

/* Memory operation on specific Memory pool */
OSAL_DECL void* OSAL_pvMemPoolFixSizeGetBlockOfPool(const trHandleMpf* pHandle);
OSAL_DECL tS32 OSAL_s32MemPoolFixSizeRelBlockOfPool(const trHandleMpf* pHandle, void* pBlock);


OSAL_DECL tU32 u32CalculatePoolMemSize(tU32 u32MpfBlockSize,
                             tU32 u32MpfBlockCnt);
OSAL_DECL tS32 OSAL_u32MemPoolFixSizeCreateOnMem(tCString coszName, 
                                  tU32 u32MpfBlockSize,
                                  trHandleMpf* pHandle,
                                  tBool bLock,
                                  void* pPoolMem,
								  tU32 u32Size);
OSAL_DECL tS32 OSAL_u32MemPoolFixSizeOpenOnMem(tCString coszName, 
                                  trHandleMpf* pHandle,
                                  void* pMem);

/************************************************************************/
/* function to get pool information                                      */
/************************************************************************/
OSAL_DECL void TraceSpecificPoolInfo(const trHandleMpf* pHandle);
uintptr_t GetMemPoolOffset(trHandleMpf* pHandle, uintptr_t* pU32Mem);
OSAL_DECL void vTracePoolInfo(const trHandleMpf* pHandle);
void* pvGetFirstElementForPid(const trHandleMpf* pHandle,tU32 u32Pid);

#ifdef __cplusplus
}
#endif

#else
#error osalmempool.h included several times
#endif
