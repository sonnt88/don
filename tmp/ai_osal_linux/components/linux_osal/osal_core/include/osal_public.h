#if !defined OSAL_PUBLIC_HEADER
#define OSAL_PUBLIC_HEADER

#include "osallock.h"
#include "osalmempool.h"
#include "osalrbtree.h"

#ifdef __cplusplus
extern "C" {
#endif 
/* ---------------------------------------------------------*/
/* OSAL export for applcations  start                       */
/* ---------------------------------------------------------*/
OSAL_DECL void LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length);
OSAL_DECL tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level);
OSAL_DECL tU32 u32ConvertErrorCore(tS32 s32ErrCd);
OSAL_DECL void vWriteToErrMem(tS32 trClass, const char* pBuffer,int Len,tU32 TrcType/*OSAL_STRING_OUT for string*/);
OSAL_DECL void vWritePrintfErrmem(tCString pcFormat, ...);
OSAL_DECL void TraceString(const char* cBuffer,...);
OSAL_DECL void TraceStringLevel(tU32 u32Level, const char* cBuffer,...);
OSAL_DECL void TraceStringClassLevel(tU32 u32Class,tU32 u32Level, const char* cBuffer,...);
OSAL_DECL tS32 u32GetSigRtMinId(tS32 s32Pid);
OSAL_DECL void vWritePrcFsToErrMem(const char* path);  //Lint removal
OSAL_DECL void vDumpMemStatusForProcess(char* pcPid, tBool bErrmem);
OSAL_DECL void vGetTopInfo(tBool bErrMemEntry);
OSAL_DECL void vReadMemStatus(tU32* pMemTotal, tU32* pMemFree,tBool bDisplay);
OSAL_DECL void vGetPoolInfo(const trHandleMpf* pHandle,char* Buffer,tU32 u32Len);
OSAL_DECL void TraceWriteLine(const char* file,  tU32 line,tBool bWrite);
OSAL_DECL tU32 u32ScanPidTaskTidStat(char* Path,tBool bErrMemEntry,tS32 s32Idx);
OSAL_DECL tBool bGetTaskName(tS32 Pid,tS32 Tid, char* Name);
OSAL_DECL void TraceToConsole(const char* cBuffer,...);
OSAL_DECL void TriggerCallstackGenerationByPid(tS32 s32Pid);
OSAL_DECL void TriggerCallstackGenerationByName(char* PrcName);
OSAL_DECL void OSAL_trace_callstack(void);
OSAL_DECL void vReadFile(const void* pvBuffer,tBool bWriteErrmem);

#define TRACE_LINE_INFO   TraceWriteLine(__FILE__, __LINE__,FALSE)
#define WRITE_LINE_INFO   TraceWriteLine(__FILE__, __LINE__,TRUE)
OSAL_DECL void vPrinthexDump(tPCU8 p2,tU32 Len);
OSAL_DECL tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec);
OSAL_DECL tS32 OSAL_s32ProcessJoin(OSAL_tProcessID pid);
OSAL_DECL void OSAL_vSetProcessExitCode(tU32 u32ExitCode);
OSAL_DECL tS32 s32RegisterExitFunction(OSALCALLBACKFUNC pFunc,tU32* arg);


/* OSAL MMAP API export */
OSAL_DECL void* OSAL_pvMemoryMap(tU32 u32size);
OSAL_DECL void OSAL_pvMemoryUnMap(tPVoid pBlock);

/* OSAL Extended Semaphore API export */
OSAL_DECL tS32 OSAL_s32SemaphoreCreate_Opt(tCString coszName,OSAL_tSemHandle * phSemaphore,
                                       tU32 uCount,tU16 u16Option);
#define SEM_DEFAULT      0
#define SEM_RECURSIVE    1
/* OSAL Extended Event API export */
OSAL_DECL tS32 OSAL_s32EventCreateOpt( tCString coszName,
                          OSAL_tEventHandle* phEvent,
                          tU32 u32Option);
#define CONSUME_EVENT     0x001
#define WAIT_MULTIPLE_TSK 0x010

/* OSAL Extended Mesage Queue API export */
OSAL_DECL tS32 OSAL_s32MessageQueueWaitMonotonic(OSAL_tMQueueHandle hMQ,
                                                 tPU8 pu8Buffer,
                                                 tU32 u32Length,
                                                 tPU32 pu32Prio,
                                                 OSAL_tMSecond msec);


/* OSAL Extended Mutex API export */
typedef uintptr_t OSAL_tMtxHandle;
OSAL_DECL tS32 OSAL_s32MutexCreate(tCString coszName,OSAL_tMtxHandle * phMutex);
OSAL_DECL tS32 OSAL_s32MutexCreate_Opt(tCString coszName,OSAL_tMtxHandle * phMutex,tU32 u32Opt);
OSAL_DECL tS32 OSAL_s32MutexDelete (tCString coszName);
OSAL_DECL tS32 OSAL_s32MutexOpen (tCString coszName, OSAL_tMtxHandle * phMutex);
OSAL_DECL tS32 OSAL_s32MutexClose (OSAL_tMtxHandle hMutex);
OSAL_DECL tS32 OSAL_s32MutexUnLock (OSAL_tMtxHandle hMutex);
OSAL_DECL tS32 OSAL_s32MutexLock (OSAL_tMtxHandle hMutex, OSAL_tMSecond msec);

OSAL_DECL tS32 OSAL_s32CreateRingBuffer(tCString coszName, tU32 u32Size,OSAL_tenAccess enAccess,OSAL_tMQueueHandle* pHandle);
OSAL_DECL tS32 OSAL_s32OpenRingBuffer(tCString coszName,OSAL_tenAccess enAccess,OSAL_tMQueueHandle* pHandle);
OSAL_DECL tS32 OSAL_s32CloseRingBuffer(OSAL_tMQueueHandle Handle);
OSAL_DECL tS32 OSAL_s32DeleteRingBuffer(tCString coszName);
OSAL_DECL tU32 OSAL_u32WriteToRingBuffer(OSAL_tMQueueHandle Handle,char* Buffer, tU32 Size,tU32 u32TimeOut);
OSAL_DECL tU32 OSAL_u32ReadFromRingBuffer(OSAL_tMQueueHandle Handle,char* Buffer, tU32 BufSize,tU32 u32TimeOut);
OSAL_DECL tS32 OSAL_u32RingBufferNotify(OSAL_tMQueueHandle Handle,OSAL_tpfCallback pCallback,tPVoid pvArg);
OSAL_DECL tS32 OSAL_s32RingBufferStatus( OSAL_tMQueueHandle Handle,tPU32 pu32Message);

#ifdef OSAL_SHM_MQ
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueCreate(tCString coszName, tU32 u32MaxMessages, tU32 u32MaxLength,OSAL_tenAccess enAccess, OSAL_tMQueueHandle* phMQ);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueOpen(tCString coszName, OSAL_tenAccess enAccess, OSAL_tMQueueHandle* phMQ);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueClose(OSAL_tMQueueHandle hMQ);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueDelete(tCString coszName);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueuePost(OSAL_tMQueueHandle hMQ, tPCU8 pcou8Msg, tU32 u32Length, tU32 u32Prio);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueWait(OSAL_tMQueueHandle hMQ, tPU8 pu8Buffer, tU32 u32Length, tPU32 pu32Prio, OSAL_tMSecond msec);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueNotify(OSAL_tMQueueHandle hMQ,OSAL_tpfCallback pCallback,tPVoid pvArg);
OSAL_DECL tS32 OSAL_s32ShMemMessageQueueStatus( OSAL_tMQueueHandle hMQ,tPU32 pu32MaxMessage,tPU32 pu32MaxLength,tPU32 pu32Message);
#endif

/* ---------------------------------------------------------*/
/* OSAL export for application end                          */
/* ---------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#else
#error osal_public.h included several times
#endif
