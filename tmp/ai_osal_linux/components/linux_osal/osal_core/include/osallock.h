/************************************************************************
| FILE:         osalmempool.h
| PROJECT:      platform
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for RB tree definitions
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2016 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 10.07.16  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (RBTREE_HEADER)
   #define RBTREE_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#define NR_OF_WAITER 5


/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef struct{
   tU32   u32Count;
   tU32   u32OpenCount;
   char   cName[OSAL_C_U32_MAX_NAMELENGTH];
   tBool  bUsed;
   tBool  bSemSv;
   tU32   u32Timeout[NR_OF_WAITER];
   tS32   s32Tid[NR_OF_WAITER];
} trOsalLockInfo;

typedef struct{
   sem_t            lock;
   trOsalLockInfo   rInfo;
} trOsalLock;

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************/
/* function for global OSAL Lock   configuration                        */
/************************************************************************/

/************************************************************************/
/* function for using specific OSAL Lock functions                      */
/************************************************************************/
OSAL_DECL tS32 CreateOsalLock(trOsalLock *lock, const char* name);
OSAL_DECL tS32 OpenOsalLock(trOsalLock *pLock);
OSAL_DECL tS32 CloseOsalLock(trOsalLock *pLock);
OSAL_DECL tS32 DeleteOsalLock(trOsalLock *lock);
OSAL_DECL tS32 LockOsal(trOsalLock *lock);
OSAL_DECL tS32 LockOsalTmo(trOsalLock* pLock,tU32 u32Timeout);
OSAL_DECL tS32 UnLockOsal(trOsalLock *lock);
OSAL_DECL tU32 u32SemWaitTmo(sem_t *lock ,tU32 u32Timeout);
OSAL_DECL tU32 u32SemWait(sem_t *lock);


#ifdef __cplusplus
}
#endif

#else
#error rbtree.h included several times
#endif
