
/************************************************************************
| FILE:         osaliosc.h
| PROJECT:      ADIT GEN 2
| SW-COMPONENT: OSAL
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for Heap connection definitons
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Bosch
| HISTORY:      
| Date      | Modification               | Author
| 10.03.13  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (OSALHEAP_HEADER)
   #define OSALHEAP_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#define CHECK_HEAP

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
/* aligment type */
typedef long OSAL_MEM_tAlign;

/* header of free or used block */
union OSAL_MEM_uHeader
{
   struct
   {
      tU16 u16MagicField;
      tU16 u16MemOffset;
      tS32 s32Offset;
      tU32 nsize;
   } s;
   OSAL_MEM_tAlign x;   /*lint -e754 */  // unused pointer -> yes!
};

typedef union OSAL_MEM_uHeader OSAL_MEM_tHeader;

/* shared memory header */
typedef struct
{
   tS32 freeOffset;
   tU32 nAbsSize;
   tU32 nCurSize;
   tU32 nMinSize;
   tU32 nMaxMemBlkSize;
   tU32 n32Counter;
} OSAL_MEM_tSharedHeader;


/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tS32 OSAL_s32HeapCreate(tU32 u32Size,tString szName,tBool bShared,tBool bDebug);
tS32 OSAL_s32HeapOpen( tString szName );
tS32 OSAL_s32HeapClose(void);
tS32 OSAL_s32HeapDelete(void);

void* OSAL_pvHeapMemAlloc(tU32 size);
tS32  OSAL_s32HeapMemDelete(void* pvMem);

tS32 OSAL_s32HeapGetAbsoluteSize(void);
tS32 OSAL_s32HeapGetCurrentSize(void);
tS32 OSAL_s32HeapGetMinimalSize(void);
tU32 OSAL_u32GetMaxBlockSize(void);
tU32 OSAL_u32GetMemSize(void* pvMem);


void OSAL_vPrintHeapLeaks(OSAL_tIODescriptor fdesc);

/************************************************************************/
/* function for global memory pool cconfigurtion                        */
/************************************************************************/


#ifdef __cplusplus
}
#endif

#else
#error osalheap.h included several times
#endif
