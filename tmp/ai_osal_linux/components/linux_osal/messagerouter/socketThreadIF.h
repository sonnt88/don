#ifndef __SOCKETTHREADIF_H__
#define __SOCKETTHREADIF_H__

class tclSocketThread
{
private:
   typedef void (*fpThreadEntry) ( void* );
   fpThreadEntry m_pThreadEntry;
   void* m_pvArg;
public:
   bool bStartThread(fpThreadEntry ThreadFunction,void* ThreadArg);
private:
   static void ThreadEntry( void* pvArg );
};

#endif //__SOCKETTHREADIF_H__
