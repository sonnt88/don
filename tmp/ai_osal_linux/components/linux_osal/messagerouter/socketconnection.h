/************************************************************************
 * FILE        :  socketconnection.h
 * SW-COMPONENT:  OSAL-RPC-SERVER - Message Router
 *----------------------------------------------------------------------
 * DESCRIPTION :  Header file of socket connection class
 *----------------------------------------------------------------------
 * COPYRIGHT   :  (c) 2009-2010 Robert Bosch Engg. & Business Solns. Ltd.
 * HISTORY     :
 * Date       |   Modification            | Author
 *----------------------------------------------------------------------
 * 01/02/2009 |   Initial Version         | Soj Thomas, RBEI/ECF
 *************************************************************************/
#ifndef __SOCKET_CONNCETION_H__
#define __SOCKET_CONNCETION_H__

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/**************************************************************************/
/* includes                                                               */
/**************************************************************************/

#if OSAL_OS == OSAL_LINUX
    #include <sys/socket.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <unistd.h>
    #define  SOCKET             int
    #define  INVALID_SOCKET  (SOCKET)(~0)
    #define  SOCKET_ERROR            (-1)
    #define  closesocket(x) close(x)
#else
    #include "winsock2.h"
    #include "stdlib.h"
#endif

#define OSALRPC_INTERFACE_SERVER
#define OSALRPC_INTERFACE_COMMANDCODES
#include "osalrpcserver_if.h"
#include "socketThreadIF.h"
/**************************************************************************/
/* Defines                                                                */
/**************************************************************************/
#define OSALRPC_THREAD              "RPCSERVER_"
#define OSALRPC_THREAD_PRIO         70
#define OSALRPC_THREAD_STACKSIZE    10000
#define OSALRPC_SEMAPHORE           "RPCSERVER_"
#define OSALRPC_PACKET_SIZE         12

class tclSocketConnection : public tclIMessageSender 
{
   tS32 m_s32SockDesc;
   tS32 m_hThreadId;
   tclIMessageParser *m_poMessageParser;
   tBool m_bSocketClose;
   tclSocketThread *m_pSocketThread;
public:
   tclSocketConnection(tS32 s32SocketDesc);
   ~tclSocketConnection();
   tBool bInit();
   tVoid vCleanUp();
   tS32 s32SendMessage(tChar *pau8Buffer,tU32 u32BufferLength);
   static tVoid vSocketThread(tPVoid pvArg);
   tVoid vSetMessageParser(tclIMessageParser *pMessageParser);
   tVoid vCloseSocket();

}; 
#endif  //__SOCKET_CONNCETION_H__
