/************************************************************************
 * FILE        :  socketgroup.cpp
 * SW-COMPONENT:  Messagerouter (OSAL-RPC-SERVER)
 *----------------------------------------------------------------------
 * DESCRIPTION :  Header file of the socketgroup class
 *----------------------------------------------------------------------
 * COPYRIGHT   :  (c) 2009-2010 Robert Bosch Engg. & Business Solns. Ltd.
 * HISTORY     :
 * Date       |   Modification            | Author
 *----------------------------------------------------------------------
 * 01/02/2009 |   Initial Version         | Susmith MR, RBEI/ECF
 * 01/05/2009 |   Add headers             | Soj Thomas, RBEI/ECF
 *************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "ostrace.h"
#include "socketgroup.h"

extern char  szIpAddress[32];


tU32 tclSocketGroup::m_u32NoConnection = 0;
std::vector<tclSocketConnection *> tclSocketGroup::arSocketConnection;

/******************************************************************************
 FUNCTION:   | tclSocketGroup::tclSocketGroup()
-------------------------------------------------------------------------------
 DESCRIPTION:| Constructor
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| none
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tclSocketGroup::tclSocketGroup( IELog* pLog ):m_pLog( pLog )
{
    arSocketConnection.clear();
    m_socket = INVALID_SOCKET;
}

/******************************************************************************
 FUNCTION:   | tclSocketGroup::~tclSocketGroup()
-------------------------------------------------------------------------------
 DESCRIPTION:| Destructor
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| none
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tclSocketGroup::~tclSocketGroup()
{
}//lint !e1540
/******************************************************************************
 FUNCTION:   | tclSocketGroup::u32GetRegValue()
-------------------------------------------------------------------------------
 DESCRIPTION:| Read the registry values 
-------------------------------------------------------------------------------
 PARAMETERS: | 
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tU32 tclSocketGroup::u32GetRegValue(const tChar *RegEntryName, const tChar *KeyName, tU32 u32DefaultValue)
{
    tU32 u32RetVal = u32DefaultValue;

    OSAL_tIODescriptor fd = OSAL_IOOpen (RegEntryName, OSAL_EN_READONLY);
    if (fd != OSAL_ERROR)
    {
        tU32 val;
        OSAL_trIOCtrlRegistry arg;
        arg.pcos8Name = (tS8 *) KeyName;
        arg.ps8Value = (tU8 *) &val;
        arg.u32Size = sizeof(val);
        if (OSAL_s32IOControl (fd, OSAL_C_S32_IOCTRL_REGQUERYVALUE, (intptr_t) &arg) == OSAL_OK)
        {
         u32RetVal = val;
        }
        OSAL_s32IOClose(fd);
    }

    return u32RetVal;
}
/******************************************************************************
 FUNCTION:   | tclSocketGroup::~vSocketServerGroupInit()
-------------------------------------------------------------------------------
 DESCRIPTION:| Initializes Sockets and setup client server connection groups,
             | starts the socket server thread that waits for new connection.
-------------------------------------------------------------------------------
 PARAMETERS: | tS32 s32Port : [I] Socket port
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketGroup::vSocketServerGroupInit( tS32 s32Port )
{
#if (OSAL_OS == OSAL_WINNT)
   WSADATA wsaDataTemp;
   WSAStartup( MAKEWORD(2,2), &wsaDataTemp );
#endif

   m_pLog->vLog( __FILE__, __LINE__, IELog::E_MESSAGE, "Create as Server" );
   m_socket = socket( AF_INET, SOCK_STREAM, 0 );
   if ( m_socket == INVALID_SOCKET )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not open socket" );
      return;
   }

   struct sockaddr_in destination;
   memset( &destination,0,sizeof(destination) );
   destination.sin_family = AF_INET;
   destination.sin_port = htons( s32Port );

#ifdef OSAL_LINUX_ARM
   destination.sin_addr.s_addr=inet_addr(szIpAddress);
#endif
   if ( bind(m_socket, (struct sockaddr *)&destination, sizeof(destination)) )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not bind socket" );
      return;
   }

   if ( listen(m_socket,QUEUE_LENGTH_MAX) )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_MESSAGE, "Socket running" );
      return;
   }

#if ( OSAL_OS == OSAL_WINNT )
   HANDLE hThread = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)vSocketServer, (LPVOID)m_socket, 0, NULL );
  if ( hThread != NULL )
   {
      tChar acBuffer[1024] = {'\0'};
      sprintf(acBuffer,"Socket Server Thread Started: Port Number <%d> ",s32Port);
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_MESSAGE,acBuffer);
      CloseHandle( hThread );
   }
   else
     m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not create socket thread");
#else
   OSAL_tThreadID dwThreadID;
   OSAL_trThreadAttribute  rAttr;

   tChar  szCreationName[32] = "\0";

   OSAL_s32PrintFormat( (tString)szCreationName, "ServerThread" );
   rAttr.szName = szCreationName;
   rAttr.s32StackSize = u32GetRegValue("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/OSAL/THREADS","SOCKET_STCK",10240); 
   rAttr.u32Priority = u32GetRegValue("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/OSAL/THREADS","SOCKET_PRIO",OSAL_C_U32_THREAD_PRIORITY_NORMAL);
   rAttr.pfEntry = (OSAL_tpfThreadEntry)vSocketServer;
   rAttr.pvArg = (tPVoid)m_socket;

   dwThreadID = OSAL_ThreadSpawn( &rAttr );

   if ( dwThreadID == OSAL_ERROR )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not create socket thread");
   }
   else
       printf("Socket Server Thread Started: Port Number <%d>\n",s32Port);
#endif
}

/******************************************************************************
 FUNCTION:   | tclSocketGroup::vSocketServer()
-------------------------------------------------------------------------------
 DESCRIPTION:| Main socket server thread proc that waits for new connections.
-------------------------------------------------------------------------------
 PARAMETERS: | tPVoid pvArg : [->I] Thread start argument
-------------------------------------------------------------------------------
 RETURN TYPE:| none
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketGroup::vSocketServer( tPVoid pvArg )
{
   SOCKET sockServer = (SOCKET)*((uintptr_t*)pvArg);

   for (;;)
   {
      SOCKET clientSocket;
      clientSocket=accept( sockServer, 0, 0 );

      if ( clientSocket == INVALID_SOCKET ) break;
      tclOSAL_RPCMessageParser *poParser = new tclOSAL_RPCMessageParser();
      if (poParser != OSAL_NULL)
      {
         tclSocketConnection *pSockConnection=new tclSocketConnection( (tS32)clientSocket );
         if (pSockConnection != OSAL_NULL)
         {
            poParser->vSetMessageSender( pSockConnection );
            pSockConnection->vSetMessageParser( poParser );
            if (pSockConnection->bInit())
            {
               arSocketConnection.push_back(pSockConnection);
               m_u32NoConnection++;
            }
            else
            {
               delete pSockConnection;
               delete poParser;
            }
         }
         else
         {
            delete poParser;
            //m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not Allocate Memory for SocketConnection");
         }
      }
      else
      {
         //m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not Allocate Memory for Parser");
      }
   }
}

/******************************************************************************
 FUNCTION:   | tclSocketGroup::vSocketClientGroupInit()
-------------------------------------------------------------------------------
 DESCRIPTION:| 
             | 
-------------------------------------------------------------------------------
 PARAMETERS: | tS32 s32Port : [I] Socket port
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketGroup::vSocketClientGroupInit( tS32 s32Port, tPChar pszIPAddress )
{
   if( (NULL == pszIPAddress)|| (0 >= strlen(pszIPAddress)) )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Invalid IP Address %s", pszIPAddress );
      return;
   }
   if( 0 >= s32Port )
   {
      m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR,"Invalid Port %d", s32Port );
      return;
   }

   int iter;
   for( iter = 0; iter < MAX_CONNECTIONS; iter++ ) /* HARD CODED Number of Client connections! */
   {
      struct hostent *hp;
      hp = gethostbyname ( pszIPAddress );

      if ( hp == NULL ) 
      {
         break;
      }

      SOCKET sClient;
      sClient = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
      if ( sClient != INVALID_SOCKET )
      {
         struct sockaddr_in destination;
         memcpy( (char *)&destination.sin_addr, (char *)hp->h_addr, hp->h_length );
         destination.sin_port = htons( s32Port );
         destination.sin_family = AF_INET;

         if( SOCKET_ERROR == connect( sClient,
            (sockaddr*)&destination,
            sizeof(destination)) )
         {
            m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR,"Could not connect to socket server IP: %s,Port: %d", pszIPAddress, s32Port );
            return;
         }

         tclOSAL_RPCMessageParser *poParser = new tclOSAL_RPCMessageParser();
         if (poParser != OSAL_NULL)
         {
            tclSocketConnection *pSockConnection=new tclSocketConnection( (tS32)sClient );
            if (pSockConnection != OSAL_NULL)
            {
               poParser->vSetMessageSender( pSockConnection );
               pSockConnection->vSetMessageParser( poParser );
               pSockConnection->bInit();
               arSocketConnection.push_back(pSockConnection);
               m_u32NoConnection++;
            }
            else
            {
               delete  []poParser;
               //m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not Allocate Memory for SocketConnection");
            }
         }//lint !e429
         else
         {
            //m_pLog->vLog( __FILE__, __LINE__, IELog::E_ERROR, "Could not Allocate Memory for Parser");
         }
      }
   }
}

/******************************************************************************
 FUNCTION:   | tclSocketGroup::vSocketGroupShutDown()
-------------------------------------------------------------------------------
 DESCRIPTION:| Shutdown the socket server
-------------------------------------------------------------------------------
 PARAMETERS: | tS32 s32Port : [I] Socket port
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketGroup::vSocketGroupShutDown()
{
    std::vector<tclSocketConnection *>::iterator iter;
    for(iter = arSocketConnection.begin();iter!=arSocketConnection.end();iter++)
    {
        (*iter)->vCleanUp();
    }
    closesocket( m_socket );
}

/******************************************************************************
 FUNCTION:   | tclSocketGroup::s32PopSocketConnectionptr
-------------------------------------------------------------------------------
 DESCRIPTION:| Removes pSocketConnection from the array of Ssocket Connection 
-------------------------------------------------------------------------------
 PARAMETERS: | tclSocketConnection *pSocketConnection
-------------------------------------------------------------------------------
 RETURN TYPE:| OSAL_ERROR or OSAL_OK
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tS32 tclSocketGroup::s32PopSocketConnectionptr(tclSocketConnection *pSocketConnection)
{
    tS32 s32ReturnValue= OSAL_ERROR;
    std::vector<tclSocketConnection *>::iterator iter;
    for(iter = arSocketConnection.begin();iter!=arSocketConnection.end();iter++)
    {
        if (pSocketConnection == (*iter))
        {
            arSocketConnection.erase(iter);
            s32ReturnValue = OSAL_OK;
            break;
        }
    }
    return s32ReturnValue;
}

