/************************************************************************
 * FILE        :  socketgroup.h
 * SW-COMPONENT:  Messagerouter (OSAL-RPC-SERVER)
 *----------------------------------------------------------------------
 * DESCRIPTION :  Header file of the socketgroup class
 *----------------------------------------------------------------------
 * COPYRIGHT   :  (c) 2009-2010 Robert Bosch Engg. & Business Solns. Ltd.
 * HISTORY     :
 * Date       |   Modification            | Author
 *----------------------------------------------------------------------
 * 01/02/2009 |   Initial Version         | Susmith MR, RBEI/ECF
 * 01/05/2009 |   Add headers             | Soj Thomas, RBEI/ECF
 *************************************************************************/
#ifndef __SOCKET_GROUP_H__
#define __SOCKET_GROUP_H__

/**************************************************************************/
/* includes                                                               */
/**************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#define OSALRPC_INTERFACE_SERVER
#define OSALRPC_INTERFACE_COMMANDCODES
#include "osalrpcserver_if.h"
#include "errorlog_if.h"

#include <vector>
#include "socketconnection.h"

#define MAX_CONNECTIONS 8
#define QUEUE_LENGTH_MAX 10

class tclSocketGroup
{
private:
   SOCKET m_socket;
   static tU32 m_u32NoConnection;
   static std::vector<tclSocketConnection *> arSocketConnection;
   IELog* m_pLog;
   tU32 u32GetRegValue(const tChar *RegEntryName, const tChar *KeyName, tU32 u32DefaultValue);  
public:
   tclSocketGroup( IELog* pLog );
   ~tclSocketGroup();
   tVoid vSocketClientGroupInit( tS32 s32Port, tPChar pszIPAddress = "localhost" );
   tVoid vSocketServerGroupInit( tS32 s32Port );
   tVoid vSocketGroupShutDown();
   static tVoid vSocketServer( tPVoid pvArg );
   static tS32 s32PopSocketConnectionptr(tclSocketConnection *pSocketConnection);
};
#endif //__SOCKET_GROUP_H__
