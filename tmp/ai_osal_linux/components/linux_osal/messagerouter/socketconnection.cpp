/************************************************************************
 * FILE        :  socketconnection.h
 * SW-COMPONENT:  OSAL-RPC-SERVER - Message Router
 *----------------------------------------------------------------------
 * DESCRIPTION :  Implementation file of socket connection class
 *----------------------------------------------------------------------
 * COPYRIGHT   :  (c) 2009-2010 Robert Bosch Engg. & Business Solns. Ltd.
 * HISTORY     :
 * Date       |   Modification            | Author
 *----------------------------------------------------------------------
 * 01/02/2009 |   Initial Version         | Soj Thomas, RBEI/ECF
 *************************************************************************/

#include "socketconnection.h"
#include "socketgroup.h"

/******************************************************************************
 FUNCTION:   | tclSocketConnection::tclSocketConnection()
-------------------------------------------------------------------------------
 DESCRIPTION:| Constructor
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| none
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tclSocketConnection::tclSocketConnection(tS32 s32SocketDesc)
{
   m_s32SockDesc=s32SocketDesc;
   m_bSocketClose = FALSE;
   m_pSocketThread = NULL;
   m_poMessageParser=NULL;
   m_hThreadId=OSAL_ERROR;
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::~tclSocketConnection()
-------------------------------------------------------------------------------
 DESCRIPTION:| Destructor
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| nonw
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tclSocketConnection::~tclSocketConnection()
{
   if (m_poMessageParser != OSAL_NULL) delete m_poMessageParser;
   vCloseSocket();
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::vCloseSocket()
-------------------------------------------------------------------------------
 DESCRIPTION:| close the socket connection
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketConnection::vCloseSocket()
{
   m_bSocketClose = TRUE;
   closesocket(m_s32SockDesc);
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::vSetMessageParser()
-------------------------------------------------------------------------------
 DESCRIPTION:| Link the socket connection to a message parser
-------------------------------------------------------------------------------
 PARAMETERS: | tclIMessageParser *pMessageParser : [->I] Message Parser object
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketConnection::vSetMessageParser( tclIMessageParser *pMessageParser )
{
   if(pMessageParser!=OSAL_NULL)
      m_poMessageParser = pMessageParser;
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::vSocketThread()
-------------------------------------------------------------------------------
 DESCRIPTION:| Thread proc to receive RPC messages
-------------------------------------------------------------------------------
 PARAMETERS: | tPVoid pvArg : [->I] Arguments for the thread proc
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketConnection::vSocketThread( tPVoid pvArg )
{
   tS32 s32ReceivedBytes = -1; 
   tclSocketConnection *pSocketConnection=OSAL_NULL;
   if ( pvArg != NULL )
   {
      pSocketConnection = static_cast<tclSocketConnection*>(pvArg);
      if (  (pSocketConnection->m_poMessageParser != OSAL_NULL)
         && (pSocketConnection->m_s32SockDesc != -1) )
      {
         while( !pSocketConnection->m_bSocketClose )
         {
            tChar au8Buffer[OSALRPC_PACKET_SIZE] = { '\0' };

            s32ReceivedBytes = recv( pSocketConnection->m_s32SockDesc, (char*)&au8Buffer, OSALRPC_PACKET_SIZE, 0 );

            if ( s32ReceivedBytes > 0 ) // We've got message!
               (pSocketConnection->m_poMessageParser)->bParseMessage( (tPU8)au8Buffer, s32ReceivedBytes );
            else
               pSocketConnection->m_bSocketClose = TRUE;
         }
      }
   }

   if ( pSocketConnection != OSAL_NULL )
   {
       if (tclSocketGroup::s32PopSocketConnectionptr(pSocketConnection)==OSAL_OK)
       {
           if ( pSocketConnection->m_pSocketThread ) 
              delete pSocketConnection->m_pSocketThread;
           delete pSocketConnection;
       }
   }
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::bInit()
-------------------------------------------------------------------------------
 DESCRIPTION:| Startup the socket conection
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| tBool : true if successful, false otherwise
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tBool tclSocketConnection::bInit()
{
#if OSAL_OS!=OSAL_WINCE
   m_pSocketThread = new tclSocketThread();
   if ( m_pSocketThread != NULL && (m_pSocketThread->bStartThread( vSocketThread,(void*)this )) )
      return true;
   else
      return false;
#else
   return false;
#endif
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::vCleanUp()
-------------------------------------------------------------------------------
 DESCRIPTION:| Shutdown the socket connection
-------------------------------------------------------------------------------
 PARAMETERS: | none
-------------------------------------------------------------------------------
 RETURN TYPE:| tVoid
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tVoid tclSocketConnection::vCleanUp()
{
   m_bSocketClose = TRUE;
   send( m_s32SockDesc, "Terminate", 0, 0 );
}

/******************************************************************************
 FUNCTION:   | tclSocketConnection::s32SendMessage()
-------------------------------------------------------------------------------
 DESCRIPTION:| Send the message using a socket
-------------------------------------------------------------------------------
 PARAMETERS: | tChar *pau8Buffer    : [->I] Message buffer
             | tU32 u32BufferLength : [I] Length of message buffer
-------------------------------------------------------------------------------
 RETURN TYPE:| tS32 : Number of bytes that were sent
-------------------------------------------------------------------------------
 HISTORY:    | Date      | Author            | Modification
-------------------------------------------------------------------------------
             | 01.02.09  | Susmith, RBEI/ECF | Initial version
******************************************************************************/
tS32 tclSocketConnection::s32SendMessage( tChar *pau8Buffer, tU32 u32BufferLength )
{
   tS32 u16SendBytes = OSAL_ERROR;

   if ( (m_s32SockDesc != -1) && (pau8Buffer!=OSAL_NULL) )
      u16SendBytes = send( m_s32SockDesc, pau8Buffer, u32BufferLength, 0 );

   return u16SendBytes;
}
