#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <netinet/tcp.h>
#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"

#define SCC_IMX6_COMM_BUFFR_LENGHT 256
#define SCC_IMX6_ERRMEM_BUFFR_LENGHT (SCC_IMX6_COMM_BUFFR_LENGHT + 64)
#define SCC_IMX6_DLT_MSG_HDR_SIZE     17
#define TRUE 1
#define FALSE 0
#define BOOL unsigned int

/* signal handler start */
#define STATUS_NOT_RUNNING 3
/* signal handler end */

/* Synchronisation changes start */
#define STATUSBUFF_LENGHT 3
#define ACK_BUFF_LENGHT 5
#define SCC_ERROR_MEMORY_C_MSG_ID 0x20
#define SCC_ERROR_MEMORY_C_HOST_ACTIVE 0x01
/* Timestamp and version info extension start*/
#define SCC_ERROR_MEMORY_C_VERSION_1 0x01
#define SCC_ERROR_MEMORY_C_VERSION_2 0x02

#define SCC_ERROR_MEMORY_C_CONFIRMATION 0x30
#define SCC_ERROR_MEMORY_OFFSET_CONFIRMATION 0

#define SCC_ERROR_MEMORY_R_MSG_ID 0x21
#define SCC_ERROR_MEMORY_R_HOST_ACTIVE 0x01
/* Timestamp and version info extension start*/
#define SCC_ERROR_MEMORY_R_VERSION_1 0x01
#define SCC_ERROR_MEMORY_R_VERSION_2 0x02

#define SCC_ERROR_MEMORY_OFFSET_MSG_ID 0 
#define SCC_ERROR_MEMORY_OFFSET_STATUS 1 
#define SCC_ERROR_MEMORY_OFFSET_VERSION 2

/* Synchronisation changes end */

/* Timestamp and version info extension start*/
int v850errmem_ver = 0;
int imxerrmem_ver = 0;

/* signal handler start */
static struct sigaction d_errmem_action;
int sockfd = -1;
/* signal handler end */

static ssize_t error_mem_write(const char* cbuffer, int n_bytes);
/*lint fix */
static int fill_errmem_buffer(const char *inc_data, int n_inc_data, char *errmem_buffer, int n_size_errmem);
/*lint fix */		

sk_dgram *dgram;

/*****************************************************************************
*
* FUNCTION:    bIMX6_Comm_SocketSetup
*
* DESCRIPTION: This function creates the socket for inc communication.
*                *
* PARAMETER:   psockfd - socket file descriptor
*
* RETURNVALUE: bSocketSetup
*                 it is the function return value:
*                 - TRUE if socket is created succefully;
*                 - FALSE otherwise.
*****************************************************************************/
BOOL bIMX6_Comm_SocketSetup(int *psockfd)
{
   int fd,ret,errsv;
   BOOL bSocketSetup = FALSE;
   struct hostent *local, *remote;
   struct sockaddr_in local_addr ={0}, remote_addr={0};
   
   *psockfd  = -1;
   
   /* Get the host details for scc-local*/
   local = gethostbyname("scc-local");
   if(NULL == local)
   {
      errsv = errno;
      fprintf(stderr, "gethostbyname(scc-local) failed: %d\n", errsv);
      return bSocketSetup;
   }
   else
   {
      local_addr.sin_family = AF_INET;
      memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr,(unsigned int)local->h_length);
      local_addr.sin_port = htons(ERROR_MEMORY_PORT);
   } 
   /* Get the host details for scc*/
   remote = gethostbyname("scc");
   if(NULL == remote)
   {
      errsv = errno;
      fprintf(stderr, "gethostbyname(scc) failed: %d\n", errsv);
      return bSocketSetup;
   }
   else
   {
      remote_addr.sin_family = AF_INET;
      memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr,(unsigned int)remote->h_length);
      remote_addr.sin_port = htons(ERROR_MEMORY_PORT);
   }
   /* Socket creation*/
   fd = socket(AF_BOSCH_INC_AUTOSAR,(int)SOCK_STREAM, 0);
   if(-1 == fd)
   {
      errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      return bSocketSetup;
   }
   /* Dgram Initialization*/
   if((dgram = dgram_init(fd, SCC_IMX6_COMM_BUFFR_LENGHT, NULL)) == NULL)
   {
      fprintf(stderr, "dgram_init failed\n");
      close(fd);
      return bSocketSetup;
   }
   /** Binding to local address*/
   //lint -e64
   ret = bind(fd,(struct sockaddr*)&local_addr ,sizeof(local_addr));
   if(ret < 0)
   {
      errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      dgram_exit(dgram);
      close(fd);
      return bSocketSetup;
   }
   /* Connecting to the scc*/
   //lint -e64   
   ret = connect(fd,(struct sockaddr*)&remote_addr, sizeof(remote_addr));
   if(ret < 0)
   {
      errsv = errno;
      fprintf(stderr, "connect failed: %d\n", errsv);
      dgram_exit(dgram);
      close(fd);
      return bSocketSetup;
   }

   bSocketSetup = TRUE;
   fprintf(stderr, " Socket set-up is Successful \n");
   *psockfd = fd;

   return bSocketSetup;
}
/*****************************************************************************
*
* FUNCTION:    status_send
*
* DESCRIPTION: This function sends status of IMX to v850
*                *
* PARAMETER:   NA
*
* RETURNVALUE: NA
*****************************************************************************/

/* Synchronisation changes start */
void status_send (void)
{
   int dgram_ret ;
   unsigned char statusbuff[STATUSBUFF_LENGHT]={0};
   imxerrmem_ver = 2;


   statusbuff[SCC_ERROR_MEMORY_OFFSET_MSG_ID] = SCC_ERROR_MEMORY_C_MSG_ID;
   statusbuff[SCC_ERROR_MEMORY_OFFSET_STATUS] = SCC_ERROR_MEMORY_C_HOST_ACTIVE;
/* Timestamp and version info extension start*/
   if (imxerrmem_ver == 1)
   	statusbuff[SCC_ERROR_MEMORY_OFFSET_VERSION] = SCC_ERROR_MEMORY_C_VERSION_1;
   else if (imxerrmem_ver == 2)
   	statusbuff[SCC_ERROR_MEMORY_OFFSET_VERSION] = SCC_ERROR_MEMORY_C_VERSION_2;

	

   /* Sending Status start from imx to v850 */
   dgram_ret = dgram_send(dgram, statusbuff, sizeof(statusbuff));
   if(dgram_ret < 0) {
      printf("dgram_send failed****: %d\n", errno);
   }
   else if(0 == dgram_ret) {
      printf("IMX Disconnected******\n");
   }
   else if(dgram_ret != sizeof(statusbuff)) {
      printf(" Simulator not able to send complete sendbuf*****\n");
   }
   /* End of sending message to v850 */
}
/*****************************************************************************
*
* FUNCTION:    ack_send
*
* DESCRIPTION: This function sends acknowledgement to v850
*                *
* PARAMETER:   recv_buff ,n
*
* RETURNVALUE: dgram_ret
*****************************************************************************/
int ack_send(char *recv_buff, int n)
{
   int dgram_ret;
   int i = 0;
   unsigned char ackbuf[ACK_BUFF_LENGHT]={0};
   ackbuf[SCC_ERROR_MEMORY_OFFSET_CONFIRMATION] = SCC_ERROR_MEMORY_C_CONFIRMATION;
   if (v850errmem_ver == 1) {
    if(n >= 4) {
       for(i=0; i<=3; i++) {
          ackbuf[i+1] = *(recv_buff+i);
       }
    }
    else {
       for(i=0; i<=3; i++) {
          if (n > i) {
             ackbuf[i+1] = *(recv_buff+i);
          }
          else
          {
             ackbuf[i+1] = 0;
             printf("IMX ackBuf size < 4 and ackbuf[%d+1]: %d: \n", i, ackbuf[i+1]);
          }
       }
    }
   /* Timestamp and version info extension start*/
   } else if (v850errmem_ver == 2) {
    if(n >= 4) {
       for(i=4; i<=7; i++) {
          ackbuf[i-3] = *(recv_buff+i);
       }
    }
    else {
       for(i=4; i<=7; i++) {
          if (n+3 >= i) {
             ackbuf[i-3] = *(recv_buff+i);
          }
          else
          {
             ackbuf[i-3] = 0;
             printf("IMX ackBuf size < 4 and ackbuf[%d+1]: %d: \n", i, ackbuf[i-3]);
          }
       }
    }
   }
   dgram_ret = dgram_send(dgram, ackbuf, ACK_BUFF_LENGHT);
   printf("IMX sendbuf: %x: n:%d\n",ackbuf[SCC_ERROR_MEMORY_OFFSET_CONFIRMATION],n);
   if(dgram_ret < 0) {
      printf("dgram_send failed****: %d\n", errno);
   }
   else if(0 == dgram_ret) {
      printf("\nIMX Disconnected******");
   }
   else if(dgram_ret != ACK_BUFF_LENGHT) {
      printf("\n Simulator not able to send complete sendbuf*****");
   }
   return dgram_ret;
}
/* Synchronisation changes end */

/* signal handler start */
void d_errmem_handler (int signum)
{
        if (signum == SIGTERM)
        {
                printf("catch the sigal SIGTERM:%d\n",signum);
            dgram_exit(dgram);
            close(sockfd);
                exit(STATUS_NOT_RUNNING);
        }
        else if (signum == SIGHUP) 
        {
                printf("catch the sigal SIGHUP:%d\n",signum);
        }
}


int register_signal(void)
{
    int ret_sigaction = -1;
    d_errmem_action.sa_handler = d_errmem_handler;
    sigemptyset (&d_errmem_action.sa_mask);
    d_errmem_action.sa_flags = 0;
    
    ret_sigaction = sigaction (SIGTERM, &d_errmem_action, NULL);
    if (ret_sigaction == -1)
    {
        printf("sigaction fails\n");
    }

    ret_sigaction = sigaction (SIGHUP, &d_errmem_action, NULL);
    if (ret_sigaction == -1)
    {
        printf("sigaction fails\n");
    }
    return ret_sigaction;
}
/* signal handler end */

int main()
{
   pid_t pid;
   pid = fork();
   if(pid < 0)
   {
       printf("fork failed\n");
       exit(EXIT_FAILURE);
   }
   if(pid > 0)
   {
       printf("exited parent process\n");
       exit(EXIT_SUCCESS);
   }
   BOOL bSCC_Scoket_initOkay ;
   int n;
   /* we need 1 additional byte for the errmem */
   /* to indicate binary data */
   char recvbuf[SCC_IMX6_COMM_BUFFR_LENGHT]={0};
   int pollrc = -1;
   struct pollfd fds[1];
   int timeout = -1;
   ssize_t errmem_rt;
   /* Synchronisation changes start */
   int sync_match = 1;
   int ret_ack = 0;
   /* Synchronisation changes end */

   printf(".... daemon_scc_recv_errmem start ....");
   bSCC_Scoket_initOkay = bIMX6_Comm_SocketSetup(&sockfd);
   if(TRUE == bSCC_Scoket_initOkay)
   {
      /* signal handler start */
      int ret_signal = -1;
      ret_signal = register_signal();
      if (ret_signal == -1)
      {
          printf("Installation of signal failed\n");
      }
      /* signal handler end */
      /* Synchronisation changes start */
      status_send();
      /* Synchronisation changes end */
      fds[0].fd = sockfd;
      fds[0].events = POLLIN;
      while(1)
      {
         memset(recvbuf, 0, sizeof(recvbuf));
         fds[0].revents =0; 
         /* Synchronisation changes start */
         int errmem = 1;
         /* Synchronisation changes end */  
         pollrc = poll(fds,1,timeout);
         if(pollrc < 0 && errno == EINTR)
         {
              printf("signal need to handle\n");
              continue;
         }
         if(pollrc < 0)
         {
            printf("\nPoll Failed");
            break;
         }
         if(0 == pollrc)
         {
            printf("\nPoll Timed out");
            continue;
         }
         if(fds[0].revents & POLLIN)
         {		 
            do {
               /* we need 1 additional byte for the errmem */
               /* to indicate binary data */
               recvbuf[0] = 0;
               n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
            } while(n < 0 && errno == EAGAIN);
            
            if(n <= 0)
            {
               fprintf(stderr, "recv failed: %d\n", errno);
            }
            /* Synchronisation changes start */
            if (sync_match == 1) {
               if ((recvbuf[0] == SCC_ERROR_MEMORY_R_MSG_ID) && (recvbuf[1] == SCC_ERROR_MEMORY_R_HOST_ACTIVE) && ((recvbuf[2] == SCC_ERROR_MEMORY_R_VERSION_1) || (recvbuf[2] == SCC_ERROR_MEMORY_R_VERSION_2))) {
                  sync_match = 0;
                  errmem = 0;
                  /* Timestamp and version info extension start*/
                  v850errmem_ver = recvbuf[2];

                  printf("IMX match: string matches send from v850 side\n");
               }
               else {
                  printf("IMX match: string does not matches send from v850 side\n");
               }
            }
            if(n > 0){
               if (errmem == 1) {
                  errmem_rt = error_mem_write(recvbuf, n);
                  printf("Number of bytes written nto Error Memory: %d\n",errmem_rt);
               }
            }
            /* Synchronisation changes end */
            /* Synchronisation changes start */
            if (errmem == 1) {
               /* Synchronisation changes end */
               ret_ack = ack_send(recvbuf, n);
               if (ret_ack <= 0) {
                  printf("ack_send : ack sending failed\n");
               } 
            }
         }
      }/* while */
   }/* if */
   else	{printf("\nFalied to initiate INC communication\n");}
   
   dgram_exit(dgram);
   close(sockfd);
   
   printf("daemon_scc_recv_errmem exit" );
   return 0;
}/* main */

/*****************************************************************************
*
* FUNCTION:    error_mem_write
*
* DESCRIPTION: This function writes into error memory.
*                *
* PARAMETER:   buffer - v850 fatal error string pointer.
*
* RETURNVALUE: bytes_written - number of bytes written into error memory
*****************************************************************************/
ssize_t error_mem_write(const char *buffer, int n_bytes)
{
   int errmem_fd;
   /*lint fix */
   ssize_t bytes_written = 0;
   /*lint fix */
   int n_errmem_size ;
   char errmem_buffer[SCC_IMX6_ERRMEM_BUFFR_LENGHT];

   errmem_fd = open("/dev/errmem", O_WRONLY);
   if(-1 == errmem_fd)
   {
      printf("\nCannot Open /dev/errmem.\n");
      exit(EXIT_FAILURE);
   }
   
   n_errmem_size = fill_errmem_buffer(buffer,
   n_bytes,
   errmem_buffer,
   SCC_IMX6_ERRMEM_BUFFR_LENGHT);
   if(n_errmem_size > 0) {
      bytes_written = write(errmem_fd, errmem_buffer, n_errmem_size);
      printf("Bytes Written : %d\n",bytes_written);
   } else
   fprintf(stderr, "\nFailed to fill errmem_buffer (err=%d)" , n_errmem_size);
   
   memset(errmem_buffer, 0, sizeof(errmem_buffer));
   close(errmem_fd);
   return(bytes_written);
}


int fill_errmem_buffer(const char *inc_data, int n_inc_data, char *errmem_buffer, int n_size_errmem){
   

   /* Size Check */
   if( n_size_errmem < (n_inc_data + SCC_IMX6_DLT_MSG_HDR_SIZE)) {
      /* Truncate */
      n_inc_data = n_size_errmem - SCC_IMX6_DLT_MSG_HDR_SIZE;
      fprintf(stderr, "\n DLT Message too large, truncated ... \n ");
   }
   
   memset(errmem_buffer, 0, SCC_IMX6_DLT_MSG_HDR_SIZE);	
   errmem_buffer[2] = 0xa8; /* DLT Message Header */
   errmem_buffer[4] = 'S';  /* ECU Field 'SCC' */
   errmem_buffer[5] = 'C';
   errmem_buffer[6] = 'C';
/* Timestamp and version info extension start*/
  if (v850errmem_ver == 1) { 
   errmem_buffer[17] = inc_data[2];
   errmem_buffer[18] = inc_data[3];
   errmem_buffer[19] = inc_data[0];
   errmem_buffer[20] = inc_data[1];

   memcpy(&(errmem_buffer[21]),
   &(inc_data[4]),
   n_inc_data-4);
   } else if (v850errmem_ver == 2) {
   errmem_buffer[12] = inc_data[0];
   errmem_buffer[13] = inc_data[1];
   errmem_buffer[14] = inc_data[2];
   errmem_buffer[15] = inc_data[3];
   
   errmem_buffer[17] = inc_data[6];
   errmem_buffer[18] = inc_data[7];
   errmem_buffer[19] = inc_data[4];
   errmem_buffer[20] = inc_data[5];

   memcpy(&(errmem_buffer[21]),
   &(inc_data[8]),
   n_inc_data-8);
  }
  if (v850errmem_ver == 1)  
   return (n_inc_data + SCC_IMX6_DLT_MSG_HDR_SIZE);
  else if (v850errmem_ver == 2)
   return (n_inc_data + SCC_IMX6_DLT_MSG_HDR_SIZE - 4);
  else
   return -1;
}
