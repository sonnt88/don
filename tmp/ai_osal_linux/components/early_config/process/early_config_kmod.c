/* ******************************************************FileHeaderBegin** *//**
 * @file        early_config_kmod.c
 *
 * global function:
 *    s32EarlyConfig_KmodSetDriverName():
 *        load the linux driver 
 *        
 * local function:
 *
 * @date        2015-27-07
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <libkmod.h>
#include "system_types.h"
#include "system_definition.h"
#include "early_config_private.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#define EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT    150
/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/     

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tBool bEarlyConfig_KmodCheckDriverload(char* PpcDriverName);
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_KmodSetDriverName()
*
* DESCRIPTION: load the driver over kmod
*
* PARAMETERS: PpcDriverName: name of the driver, which should be loaded    
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
tS32 s32EarlyConfig_KmodSetDriverName(char* PpcDriverName)
{
  tS32                Vs32ReturnCode=0;	

#ifdef SYSTEM_MODPROBE
  /*no error return*/
  char VcCmd[EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT];
  memset(VcCmd,0,EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT);
  //snprintf(VcCmd,EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT,"/sbin/modprobe -r -q %s;/sbin/modprobe %s",PpcDriverName,PpcDriverName);
  snprintf(VcCmd,EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT,"/sbin/modprobe %s",PpcDriverName,PpcDriverName);	
  system(VcCmd);	 
#else
  struct kmod_ctx    *VsCtxDriver;
  struct kmod_module *VsModDriber;
  VsCtxDriver = kmod_new(NULL, NULL);
  if(VsCtxDriver!=NULL)
  {
    Vs32ReturnCode = kmod_module_new_from_name(VsCtxDriver, PpcDriverName, &VsModDriber);
    if (Vs32ReturnCode < 0) 
    { /*error */
	  fprintf(stderr, "early_config_err():%d kmod_module_new_from_name() fails \n", Vs32ReturnCode);
    }
	else
	{
	  int ViKmodState;
	  ViKmodState=kmod_module_get_initstate(VsModDriber);		
	  /*check module loaded*/
	  if(ViKmodState > 0) 
	  {		   
	    fprintf(stderr, "early_config_err(): module already loaded\n");
	  }
	  else
	  {
	    Vs32ReturnCode=kmod_module_insert_module(VsModDriber,0,kmod_module_get_options(VsModDriber));			
		if(Vs32ReturnCode<0)
		{
		  fprintf(stderr, "early_config_err():%d kmod_module_insert_module() fails \n",Vs32ReturnCode);
		}			
	  }
	}	
  }
  else
  {
    fprintf(stderr, "early_config_err():kmod_new fails() \n");
  }
  /*if not success error memory entry*/
  if(Vs32ReturnCode<0)
  {/* error memory entry could not load driver*/
    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_NO_CONFIG_FOR_DRIVER,PpcDriverName,strlen(PpcDriverName)+1);          
  }
  else
  {
     M_DEBUG_STR_STR("early_config: %s load driver success \n",PpcDriverName);
  }
#endif
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_KmodStartDriverWithScript()
*
* DESCRIPTION: start the driver with a shell script
*
* PARAMETERS: PpcDriverName: name of the driver, which should be loaded    
*             PpcConfigFileName: name of the configuration file
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
tS32 s32EarlyConfig_KmodStartDriverWithScript(char* PpcDriverName,char* PpcConfigFileName)
{
  tS32                Vs32ReturnCode=0;	
 
  char VcCmd[EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT];
  memset(VcCmd,0,EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT);
  snprintf(VcCmd,EARLY_CONFIG_DRIVER_MAX_CMD_LENGHT,"/lib/firmware/%s/load_firmware.sh %s %s",PpcDriverName,PpcDriverName,PpcConfigFileName);
  //call command
  M_DEBUG_STR_STR("early_config: function call: system('%s') \n",VcCmd);
  system(VcCmd);
  //check if driver load
  if(bEarlyConfig_KmodCheckDriverload(PpcDriverName)==TRUE)
  {
    M_DEBUG_STR_STR("early_config: success driver '%s' is loaded \n",PpcDriverName);
  }
  else
  {
    fprintf(stderr, "early_config_err(): driver '%s' not loaded\n",PpcDriverName);
	Vs32ReturnCode=-ENODEV;
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tBool bEarlyConfig_KmodCheckDriverload()
*
* DESCRIPTION: check if driver load
*
* PARAMETERS: PpcDriverName: name of the driver, which should be loaded    
*
* RETURNS: TRUE load: FALSE not load
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
static tBool bEarlyConfig_KmodCheckDriverload(char* PpcDriverName)
{
  tBool  VbLoad=FALSE;
  struct kmod_ctx    *VsCtxDriver;
  struct kmod_module *VsModDriber;
  VsCtxDriver = kmod_new(NULL, NULL);
  if(VsCtxDriver!=NULL)
  {
    if (kmod_module_new_from_name(VsCtxDriver, PpcDriverName, &VsModDriber) >= 0)   
	{
	  int ViKmodState;
	  ViKmodState=kmod_module_get_initstate(VsModDriber);		
	  /*check module loaded*/
	  if(ViKmodState > 0) 
	  {		   
	    VbLoad=TRUE; 
	  }
	}
  }
  return(VbLoad);
}
